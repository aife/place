package com.atexo.dume.mapper;

import com.atexo.dume.dto.RequirementDTO;
import com.atexo.dume.dto.RequirementGroupDTO;
import com.atexo.dume.model.*;
import com.atexo.dume.toolbox.consts.CustomResponseDataType;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.RequirementGroupType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.atexo.dume.mapper.XMLConstants.*;

@Mapper(uses = RequirementMapper.class)
public interface RequirementGroupMapper {
    RequirementMapper requirementMapper = Mappers.getMapper(RequirementMapper.class);

    @Mapping(ignore = true, target = "requirementList")
    @Mapping(ignore = true, target = "subGroups")
    RequirementGroupDTO toDTO(RequirementGroup requirementGroup);

    default RequirementGroupDTO toDTO(RequirementGroupResponse requirementGroupResponse) {
        if (requirementGroupResponse == null) {
            return null;
        }
        RequirementGroupDTO requirementGroupDTO = toDTO(requirementGroupResponse.getRequirementGroup());
        if(requirementGroupResponse.getRequirementResponseList()!=null) {
            requirementGroupDTO.setRequirementList(
                    requirementGroupResponse.getRequirementResponseList()
                            .stream()
                            .map(requirementMapper::toDTO)
                            .collect(Collectors.toList()));
        }
        requirementGroupDTO.setIdRequirementGroupResponse(requirementGroupResponse.getId());
        requirementGroupDTO.setSubGroups(new ArrayList<>());
        if (!CollectionUtils.isEmpty(requirementGroupResponse.getSubGroups())) {
            requirementGroupDTO.getSubGroups().addAll(requirementGroupResponse.getSubGroups().stream()
                    .map(this::toDTO)
                    .collect(Collectors.toList()));
        }

        return requirementGroupDTO;
    }

    @Mapping(ignore = true, target = "requirementList")
    /**
     * Update des RequirementResponse à partir des RequirementDTO
     */
    default void toEntityRequirementGroup(RequirementGroupDTO requirementGroupDTO, RequirementGroupResponse requirementGroupResponse) {

        if (!CollectionUtils.isEmpty(requirementGroupDTO.getRequirementList())) {
            // initialisation de la list des  requirement a ajouté
            List<RequirementResponse> requirementResponseListToAdd = new ArrayList<>();
            updateOrAddRequirement(requirementGroupDTO, requirementGroupResponse, requirementResponseListToAdd);
            requirementGroupResponse.getRequirementResponseList().addAll(requirementResponseListToAdd);
        }
        // Traitement des subGroups
        if (!CollectionUtils.isEmpty(requirementGroupDTO.getSubGroups())) {
            // initialisation de la list des  requirementGroup a ajouté or a mettre a jour
            List<RequirementGroupResponse> requirementGroupResponseListToAddOrUpdate = new ArrayList<>();
            addOrUpdateRequirementGroup(requirementGroupDTO, requirementGroupResponse, requirementGroupResponseListToAddOrUpdate);
            requirementGroupResponse.getSubGroups().clear();
            requirementGroupResponse.getSubGroups().addAll(requirementGroupResponseListToAddOrUpdate);
        }
    }

    default void addOrUpdateRequirementGroup(RequirementGroupDTO requirementGroupDTO, RequirementGroupResponse requirementGroupResponse, List<RequirementGroupResponse> requirementGroupResponseListToAddOrUpdate) {
        for (RequirementGroupDTO groupDTO : requirementGroupDTO.getSubGroups()) {
            if (groupDTO.getIdRequirementGroupResponse() == null) {
                // Ajout d'un nouveau subgroup
                ajoutRequirementGroup(requirementGroupResponse, requirementGroupResponseListToAddOrUpdate, groupDTO);
            } else {
                // Update des Anciens
                updateDesAnciens(requirementGroupResponse, requirementGroupResponseListToAddOrUpdate, groupDTO);
            }
        }
    }

    default void updateDesAnciens(RequirementGroupResponse requirementGroupResponse, List<RequirementGroupResponse> requirementGroupResponseListToAddOrUpdate, RequirementGroupDTO groupDTO) {
        for (RequirementGroupResponse groupResponse : requirementGroupResponse.getSubGroups()) {
            if (Objects.equals(groupResponse.getId(), groupDTO.getIdRequirementGroupResponse())) {
                toEntityRequirementGroup(groupDTO, groupResponse);
                requirementGroupResponseListToAddOrUpdate.add(groupResponse);
            }
        }
    }

    default void updateOrAddRequirement(RequirementGroupDTO requirementGroupDTO, RequirementGroupResponse requirementGroupResponse, List<RequirementResponse> requirementResponseListToAdd) {
        for (RequirementDTO requirementDTO : requirementGroupDTO.getRequirementList()) {
            if (requirementDTO.getIdRequirementResponse() == null) {
                // Ajout des Requirement
                ajoutDesRequirement(requirementGroupResponse, requirementMapper, requirementResponseListToAdd, requirementDTO);
            } else {
                // Update des requirement
                requirementGroupResponse.getRequirementResponseList().forEach(requirementResponse -> {
                    if (Objects.equals(requirementDTO.getIdRequirementResponse(), requirementResponse.getId())) {
                        requirementMapper.toEntityRequirementResponse(requirementResponse, requirementDTO);
                    }
                });
            }
        }
    }

    /**
     * Ajout d'un nouveau Requirement Group dans les sous groupes
     *
     * @param requirementGroupResponse          requirementGroup Response template
     * @param requirementGroupResponseListToAdd la liste des requirementGroupResponse a ajouté
     * @param groupDTO                          le nouveau requirementGroup a ajoute
     */
    default void ajoutRequirementGroup(RequirementGroupResponse requirementGroupResponse, List<RequirementGroupResponse> requirementGroupResponseListToAdd, RequirementGroupDTO groupDTO) {
        RequirementGroupResponse requirementGroupResponseToAdd = new RequirementGroupResponse();
        requirementGroupResponseToAdd.setRequirementResponseList(new ArrayList<>());
        requirementGroupResponseToAdd.setParent(requirementGroupResponse);

        Optional<RequirementGroup> optional = requirementGroupResponse.getRequirementGroup().getSubGroups().stream().
                filter(requirementGroup -> Objects.equals(requirementGroup.getUuid(), groupDTO.getUuid()))
                .findFirst();
        if (optional.isPresent()) {
            requirementGroupResponseToAdd.setRequirementGroup(
                    optional.get()
            );
        }

        toEntityRequirementGroup(groupDTO, requirementGroupResponseToAdd);
        requirementGroupResponseListToAdd.add(requirementGroupResponseToAdd);
    }

    /**
     * Lors de l'ajout d'un nouveau requirement , on le crée et l'ajout a la liste desrequirementResponseListToAdd
     * ce fonction sera declanche quand on ajout un nouveau RequirementGroup
     *
     * @param requirementGroupResponse     le requirement template
     * @param requirementMapper            le mapper
     * @param requirementResponseListToAdd la liste des requirement a ajouté
     * @param requirementDTO               le nouveau requirementDTO a ajouter
     */
    default void ajoutDesRequirement(RequirementGroupResponse requirementGroupResponse, RequirementMapper requirementMapper, List<RequirementResponse> requirementResponseListToAdd, RequirementDTO requirementDTO) {
        RequirementResponse requirementResponseToAdd = new RequirementResponse();
        requirementResponseToAdd.setResponse(new Response());
        requirementMapper.toEntityRequirementResponse(requirementResponseToAdd, requirementDTO);
        requirementResponseToAdd.setRequirementGroupResponse(requirementGroupResponse);

        Optional<Requirement> optional = requirementGroupResponse.getRequirementGroup().getRequirementList()
                .stream().filter(requirementResponse ->
                        Objects.equals(requirementDTO.getUuid(), requirementResponse.getUuid())
                ).findFirst();
        if (optional.isPresent()) {
            requirementResponseToAdd.setRequirement(optional.get());
        }

        requirementResponseListToAdd.add(requirementResponseToAdd);
    }

    // XML
    // Requirement group
    @Mapping(target = "ID.schemeID", constant = CRITERION_RELATED_IDS)
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeVersionID", constant = V_1_0)
    @Mapping(target = "ID.value", source = "uuid")
    @Mapping(target = "fulfillmentIndicatorType", ignore = true)
    @Mapping(target = "fulfillmentIndicator", ignore = true)
    RequirementGroupType toRequirementGroupType(RequirementGroup reqGroup);

    default String getPiFromRequirementGroup(RequirementGroup requirementGroup) {
        if (requirementGroup.getFulfillmentIndicator() != null) {
            return requirementGroup.getFulfillmentIndicator() ? FULLFILLEMENT_TRUE : FULLFILLEMENT_FALSE;
        }
        return null;
    }

    default Boolean requirementFilter(Requirement requirement) {
        return !Objects.equals(requirement.getResponseType(), CustomResponseDataType.INFOS.value())
                && !Objects.equals(requirement.getResponseType(), CustomResponseDataType.INFO_TITLE.value());
    }

    default RequirementGroupType toFinalRequirementGroupType(RequirementGroup requirementGroup) {
        RequirementGroupType requirementGroupType = toRequirementGroupType(requirementGroup);
        requirementGroupType.setPi(getPiFromRequirementGroup(requirementGroup));
        requirementGroupType.getRequirement().addAll(requirementGroup.getRequirementList().stream()
                .filter(this::requirementFilter)
                .map(requirementMapper::toRequirementType).collect(Collectors.toList()));
        requirementGroupType.getRequirementGroup().addAll(requirementGroup.getSubGroups()
                .stream()
                .map(this::toFinalRequirementGroupType)
                .collect(Collectors.toList()));
        return requirementGroupType;
    }

    default RequirementGroupType toFinalRequirementGroupWithResponse(RequirementGroupResponse requirementGroup) {
        RequirementGroupType requirementGroupType = toRequirementGroupType(requirementGroup.getRequirementGroup());
        requirementGroupType.setPi(getPiFromRequirementGroup(requirementGroup.getRequirementGroup()));
        if (!CollectionUtils.isEmpty(requirementGroup.getRequirementResponseList())) {
            requirementGroupType.getRequirement().addAll(requirementGroup.getRequirementResponseList()
                                                                         .stream()
                                                                         .filter(requirement -> requirementFilter(requirement.getRequirement()))
                                                                         .map(requirementMapper::toFinalRequirementWithResponse)
                                                                         .collect(Collectors.toList()));
        }
        if (!CollectionUtils.isEmpty(requirementGroup.getSubGroups())) {
            requirementGroupType.getRequirementGroup().addAll(requirementGroup.getSubGroups()
                                                                              .stream()
                                                                              .map(this::toFinalRequirementGroupWithResponse)
                                                                              .collect(Collectors.toList()));
        }
        return requirementGroupType;
    }

    /**
     * Fonction récursive qui permet de créer un objet response correspondant pour chaque requirement/requirementGroup
     * et qui peuple les responses avec les valeurs venant du xml.
     *
     * @param criteriaResponse
     * @param requirementGroupTypes
     * @param requirementGroups
     * @param parent
     * @return
     */
    default List<RequirementGroupResponse> createResponseFromXml(CriteriaResponse criteriaResponse, List<RequirementGroupType> requirementGroupTypes,
                                                                 List<RequirementGroup> requirementGroups, RequirementGroupResponse parent) {

        List<RequirementGroupResponse> requirementGroupResponseList = new ArrayList<>();

        int i = -1;

        for (RequirementGroupType requirementGroupType : requirementGroupTypes) {

            i++;

            RequirementGroupResponse requirementGroupResponse = new RequirementGroupResponse();

            RequirementGroup requirementGroup;

            // Comportement spécifique : on ne garde que 3 groupes "chiffre d'affaire", et on doit les mapper
            // séquentiellement sur chacun des requirementGroups qui ont le même uuid
            if (parent == null &&
                    ("CRITERION.SELECTION.ECONOMIC_FINANCIAL_STANDING.TURNOVER.GENERAL_YEARLY"
                            .equals(criteriaResponse.getCriteria().getCode())
                            || "CRITERION.SELECTION.ECONOMIC_FINANCIAL_STANDING.TURNOVER.SPECIFIC_YEARLY"
                            .equals(criteriaResponse.getCriteria().getCode()))) {
                List<RequirementGroup> groupList = requirementGroups.stream().filter(
                        requirementGroup1 -> requirementGroup1.getUuid().equals(requirementGroupType.getID().getValue().trim())
                ).collect(Collectors.toList());
                if (i > groupList.size() - 1) {
                    continue;
                }
                requirementGroup = groupList.get(i);
            } else {
                Optional<RequirementGroup> optional = requirementGroups.stream().filter(
                        requirementGroup1 -> requirementGroup1.getUuid().equals(requirementGroupType.getID().getValue().trim())
                ).findFirst();
                requirementGroup = optional.isPresent() ? optional.get() : null;
            }

            requirementGroupResponse.setRequirementGroup(requirementGroup);
            requirementGroupResponse.setCriteriaResponse(criteriaResponse);
            requirementGroupResponse.setParent(parent);
            if (requirementGroup != null) {
                updateRequirementGroupResponse(requirementGroupType, requirementGroupResponse, requirementGroup);
                if (!CollectionUtils.isEmpty(requirementGroupType.getRequirementGroup())) {
                    requirementGroupResponse.setSubGroups(createResponseFromXml(null,
                            requirementGroupType.getRequirementGroup(), requirementGroup.getSubGroups(), requirementGroupResponse));
                }

            }
            requirementGroupResponseList.add(requirementGroupResponse);
        }
        return requirementGroupResponseList;
    }

    default void updateRequirementGroupResponse(RequirementGroupType requirementGroupType, RequirementGroupResponse requirementGroupResponse, RequirementGroup requirementGroup) {
        if (!CollectionUtils.isEmpty(requirementGroup.getRequirementList())) {
            requirementGroupResponse.setRequirementResponseList(new ArrayList<>());
            for (Requirement requirement : requirementGroup.getRequirementList()) {

                // Traiter les requirements d'information
                requirementMapper.traiterRequirements(requirementGroupType, requirementGroupResponse, requirement);
            }
        }
    }
}
