package com.atexo.dume.mapper;

import com.atexo.dume.dto.RepresentantLegalDTO;
import com.atexo.dume.model.Country;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.RepresentantLegal;
import grow.names.specification.ubl.schema.xsd.espd_commonaggregatecomponents_1.EconomicOperatorPartyType;
import grow.names.specification.ubl.schema.xsd.espd_commonaggregatecomponents_1.NaturalPersonType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.AddressType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ContactType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PersonType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PowerOfAttorneyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BirthDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.atexo.dume.mapper.XMLConstants.COUNTRY_CODE_IDENTIFIER;
import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.V_1_0_2;

@Mapper(uses = EnumMapper.class)
public interface RepresentantLegalMapper {

    RepresentantLegalDTO toDTO(RepresentantLegal representantLegal);

    RepresentantLegal toEntity(RepresentantLegalDTO representantLegalDTO);

    void udpateRepresentantLegal(RepresentantLegalDTO representantLegalDTO, @MappingTarget RepresentantLegal representantLegal);

    default NaturalPersonType toNaturalPersonType(RepresentantLegal representantLegal) {
        if (representantLegal == null) {
            return null;
        }
        NaturalPersonType naturalPersonType = new NaturalPersonType();
        // Fonction
        DescriptionType fonctionDescription = new DescriptionType();
        fonctionDescription.setValue(representantLegal.getFonction());
        naturalPersonType.setNaturalPersonRoleDescription(fonctionDescription);

        naturalPersonType.setPowerOfAttorney(new PowerOfAttorneyType());
        // details
        DescriptionType descriptionType = new DescriptionType();
        descriptionType.setValue(representantLegal.getDetails());
        naturalPersonType.getPowerOfAttorney().getDescription().add(descriptionType);

        naturalPersonType.getPowerOfAttorney().setAgentParty(new PartyType());
        naturalPersonType.getPowerOfAttorney().getAgentParty().getPerson().add(toPersonType(representantLegal));

        return naturalPersonType;

    }

    @Mapping(source = "nom", target = "familyName.value")
    @Mapping(source = "prenom", target = "firstName.value")
    @Mapping(source = "dateNaissance", target = "birthDate")
    @Mapping(source = "lieuNaissance", target = "birthplaceName.value")
    @Mapping(source = "email", target = "contact.electronicMail.value")
    @Mapping(source = "telephone", target = "contact.telephone.value")
    @Mapping(target = "residenceAddress.country.identificationCode.value", source = "pays")
    @Mapping(target = "residenceAddress.country.identificationCode.listID", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "residenceAddress.country.identificationCode.listName", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "residenceAddress.country.identificationCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "residenceAddress.country.identificationCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "residenceAddress.streetName.value", source = "rue")
    @Mapping(target = "residenceAddress.cityName.value", source = "ville")
    @Mapping(target = "residenceAddress.postalZone.value", source = "codePostal")
    PersonType toPersonType(RepresentantLegal representantLegal);

    default BirthDateType toBirthDateType(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        BirthDateType birthDateType = new BirthDateType();

        birthDateType.setValue(new org.joda.time.LocalDate(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
        return birthDateType;
    }

    default List<RepresentantLegal> getRepresentantLegals(EconomicOperatorPartyType economicOperatorParty,
                                                          DumeResponse dumeResponse) {
        List<RepresentantLegal> representantLegals = new ArrayList<>();
        if (economicOperatorParty != null && !CollectionUtils.isEmpty(economicOperatorParty.getRepresentativeNaturalPerson())) {
            for (NaturalPersonType naturalPersonType : economicOperatorParty.getRepresentativeNaturalPerson()) {
                RepresentantLegal representantLegal = new RepresentantLegal();
                representantLegal.setDumeResponse(dumeResponse);

                toRepresentantLegal(naturalPersonType, representantLegal);

                if (naturalPersonType.getNaturalPersonRoleDescription() != null) {
                    representantLegal.setFonction(naturalPersonType.getNaturalPersonRoleDescription().getValue());
                }
                representantLegals.add(representantLegal);
            }
        } else {
            RepresentantLegal representantLegal = new RepresentantLegal();
            representantLegal.setDumeResponse(dumeResponse);
            representantLegals.add(representantLegal);
        }
        return representantLegals;
    }

    default void toRepresentantLegal(NaturalPersonType naturalPersonType, RepresentantLegal representantLegal) {
        if (naturalPersonType.getPowerOfAttorney() != null) {
            PowerOfAttorneyType powerOfAttorneyType = naturalPersonType.getPowerOfAttorney();
            if (powerOfAttorneyType.getDescription() != null && !CollectionUtils.isEmpty(powerOfAttorneyType.getDescription())) {
                representantLegal.setDetails(powerOfAttorneyType.getDescription().get(0).getValue());
            }

            if (powerOfAttorneyType.getAgentParty() != null && !CollectionUtils.isEmpty(powerOfAttorneyType.getAgentParty().getPerson())) {
                PersonType personType = powerOfAttorneyType.getAgentParty().getPerson().get(0);

                if (personType.getBirthDate() != null) {
                    org.joda.time.LocalDate joda = personType.getBirthDate().getValue();
                    representantLegal.setDateNaissance(java.time.LocalDate.of(joda.getYear(), joda.getMonthOfYear(), joda.getDayOfMonth()));
                }

                if (personType.getBirthplaceName() != null) {
                    representantLegal.setLieuNaissance(personType.getBirthplaceName().getValue());
                }

                if (personType.getFamilyName() != null) {
                    representantLegal.setNom(personType.getFamilyName().getValue());
                }

                if (personType.getFirstName() != null) {
                    representantLegal.setPrenom(personType.getFirstName().getValue());
                }

                updateResidentAddressRepresantantLegal(representantLegal, personType);

                updateContactRepresentantLegal(representantLegal, personType);

            }

        }
    }

    default void updateContactRepresentantLegal(RepresentantLegal representantLegal, PersonType personType) {
        if (personType.getContact() != null) {
            ContactType contactType = personType.getContact();

            if (contactType.getTelephone() != null) {
                representantLegal.setTelephone(contactType.getTelephone().getValue());
            }

            if (contactType.getElectronicMail() != null) {
                representantLegal.setEmail(contactType.getElectronicMail().getValue());
            }
        }
    }

    default void updateResidentAddressRepresantantLegal(RepresentantLegal representantLegal, PersonType personType) {
        if (personType.getResidenceAddress() != null) {
            AddressType addressType = personType.getResidenceAddress();
            if (addressType.getCountry() != null && addressType.getCountry().getIdentificationCode() != null) {
                    representantLegal.setPays(Country.findByIso2Code(
                        addressType.getCountry().getIdentificationCode().getValue().trim()));
            }

            if (addressType.getStreetName() != null) {
                representantLegal.setRue(addressType.getStreetName().getValue());
            }

            if (addressType.getPostalZone() != null) {
                representantLegal.setCodePostal(addressType.getPostalZone().getValue());
            }

            if (addressType.getCityName() != null) {
                representantLegal.setVille(addressType.getCityName().getValue());
            }
        }
    }

}
