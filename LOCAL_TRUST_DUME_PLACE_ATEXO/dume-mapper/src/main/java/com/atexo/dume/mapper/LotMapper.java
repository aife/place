package com.atexo.dume.mapper;

import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.model.Lot;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ProcurementProjectLotType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;

/**
 * Mapper for SNLot object : convert a normal SNLot object to LotDTO Object.
 *
 * @author ama.
 */

@Mapper
public interface LotMapper {


    /**
     * This method will convert a {@link LotDTO} object using a {@link Lot} object
     * based on defined mappings.
     *
     * @param lot the object to be mapped
     * @return LotDTO Object.
     */
    @Mapping(source = "lotNumber", target = "lotNumber")
    @Mapping(source = "lotName", target = "lotName")
    @Mapping(source = "id", target = "internalLotIdentifier")
    LotDTO lotToLotDTO(final Lot lot);

    void updateLotByLotDTO(LotDTO lotDTO, @MappingTarget Lot lot);

    Lot toEntity(final LotDTO lotDTO);


    //XML
    default List<ProcurementProjectLotType> toLots(Set<Lot> lotSet) {
        List<ProcurementProjectLotType> lots = new ArrayList<>();
        ProcurementProjectLotType procurementProjectLotType = new ProcurementProjectLotType();

        procurementProjectLotType.setID(new IDType());
        procurementProjectLotType.getID().setSchemeAgencyID(EU_COM_GROW);
        StringBuilder stringBuilder = new StringBuilder("Lot(s)");
        if (CollectionUtils.isEmpty(lotSet)) {
            stringBuilder.append(" 0");
        } else {
            for (Lot lot : lotSet) {
                stringBuilder.append(" ");
                stringBuilder.append(lot.getLotNumber()).append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
        }
        procurementProjectLotType.getID().setValue(stringBuilder.toString());
        lots.add(procurementProjectLotType);
        return lots;
    }

    default String toLotDTO(Set<Lot> lots) {
        StringBuilder stringBuilder = new StringBuilder("Lot(s)");
        if (!CollectionUtils.isEmpty(lots)) {
            List<Lot> lotArray = new ArrayList<>(lots);
            Collections.sort(lotArray, (o1, o2) -> StringUtils.compare(o1.getLotNumber(), o2.getLotNumber()));
            for (Lot lot : lotArray) {
                stringBuilder.append(" ");
                stringBuilder.append(lot.getLotNumber()).append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            return stringBuilder.toString();
        } else {
            return "";
        }
    }


}
