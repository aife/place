package com.atexo.dume.mapper;

import com.atexo.dume.dto.CriterionDTO;
import com.atexo.dume.dto.ECertisResponseDTO;
import com.atexo.dume.dto.EvidenceDTO;
import com.atexo.dume.dto.sn.response.SNCriterion;
import com.atexo.dume.dto.sn.response.SNCriterionsAndProof;
import com.atexo.dume.dto.sn.response.SNEvidence;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface ECertisMapper {

    @Mapping(source = "criterion", target = "criterion")
    @Mapping(source = "evidences", target = "evidences")
    ECertisResponseDTO toEcertis(SNCriterionsAndProof proof);

    CriterionDTO toCriterion(SNCriterion criterion);

    EvidenceDTO toEvidence(SNEvidence evidence);

    List<EvidenceDTO> toEvidenceList(List<SNEvidence> evidences);
}
