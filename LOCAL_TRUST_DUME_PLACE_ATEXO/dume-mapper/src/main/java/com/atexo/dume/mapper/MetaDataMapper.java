package com.atexo.dume.mapper;

import com.atexo.dume.dto.MetaDataDTO;
import com.atexo.dume.model.MetaData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for {@link MetaData}  object : convert a normal SNLot object to {@link MetaDataDTO} Object.
 *
 * @author ama.
 */

@Mapper
public interface MetaDataMapper {

    /**
     * This method will convert a {@link MetaData} object using a {@link MetaDataDTO} object
     * based on defined mappings.
     *
     * @param metaData the object to be mapped
     * @return MetaDataDTO Object.
     */
    @Mapping(source = "bidClosingDate", target = "bidClosingDate")
    @Mapping(source = "organization", target = "organization")
    @Mapping(source = "consultationIdentifier", target = "consultationIdentifier")
    @Mapping(source = "siret", target = "siret")
    @Mapping(source = "userInscriptionIdentifier", target = "userInscriptionIdentifier")
    @Mapping(source = "simplified", target = "simplified")
    MetaDataDTO metaDataToMetaDataDTO(final MetaData metaData);
}
