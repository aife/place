package com.atexo.dume.mapper;

import com.atexo.dume.dto.LegislationReferenceDTO;
import com.atexo.dume.model.LegislationReference;
import isa.names.specification.ubl.schema.xsd.ccv_commonaggregatecomponents_1.LegislationType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.V_1_0_2;

@Mapper
public interface LegislationReferenceMapper {

    LegislationReferenceDTO toDTO(LegislationReference legislationReference);

    LegislationReference toEntity(LegislationReferenceDTO legislationReferenceDTO);

    // xml
    // LegislationReference
    @Mapping(target = "title.value", source = "title")
    @Mapping(target = "description.value", source = "description")
    @Mapping(target = "jurisdictionLevelCode.listID", constant = "CriterionJurisdictionLevel")
    @Mapping(target = "jurisdictionLevelCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "jurisdictionLevelCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "jurisdictionLevelCode.value", constant = "EU_DIRECTIVE")
    @Mapping(target = "article.value", source = "article")
    LegislationType toLegislationType(LegislationReference legislationReference);
}
