package com.atexo.dume.mapper;

import com.atexo.dume.dto.ResponseDTO;
import com.atexo.dume.model.Response;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

/**
 * Mapper SNResponse to/From ResponseDTO
 */
@Mapper
public interface ResponseMapper {

    ResponseDTO toResponseDto(Response response);


    void updateResponseFromDTO(ResponseDTO responseDTO, @MappingTarget Response response);

}
