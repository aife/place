package com.atexo.dume.mapper;

public final class XMLConstants {


    public static final String EU_COM_GROW = "EU-COM-GROW";
    public static final String CRITERIA_ID = "CriteriaID";
    public static final String COUNTRY_CODE_IDENTIFIER = "CountryCodeIdentifier";
    public static final String V_1_0_2 = "1.0.2";
    public static final String V_1_0 = "1.0";
    public static final String V_1_1 = "1.1";
    public static final String CRITERION_RELATED_IDS = "CriterionRelatedIDs";
    public static final String FULLFILLEMENT_TRUE = "GROUP_FULFILLED.ON_TRUE";
    public static final String FULLFILLEMENT_FALSE = "GROUP_FULFILLED.ON_FALSE";
    public static final String TED = "TeD";
    public static final String COM_GROW_TEMPORARY_ID = "COM-GROW-TEMPORARY-ID";
    public static final String DG_GROW = "DG GROW (European Commission)";
    public static final String TED_CN = "TED_CN";
    public static final String REFERENCES_TYPE_CODES = "ReferencesTypeCodes";

    private XMLConstants() {
    }
}
