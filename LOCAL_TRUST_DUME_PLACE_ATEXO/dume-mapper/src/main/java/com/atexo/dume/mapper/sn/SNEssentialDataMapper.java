package com.atexo.dume.mapper.sn;

import com.atexo.dume.dto.donneesessentielles.xml.*;
import com.atexo.dume.dto.sn.essentialData.*;
import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.model.DumePlatform;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

import static com.atexo.dume.toolbox.rest.NationalServiceOperations.SAVE_ESSENTIAL_DATA;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.UPDATE_ESSENTIAL_DATA;

@Mapper
public interface SNEssentialDataMapper {


    @Mapping(target = "plateforme.idPlateforme", source = "platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", expression = "java(java.util.Optional.ofNullable(contract).map(MarcheType::getAcheteur).map(AcheteurType::getNom).map(rs -> (rs !=null && rs.length() > 100 ? rs.substring(0,100) : rs )).orElse(null))")
    @Mapping(target = "idDemandeur", source = "contract.acheteur.id")
    @Mapping(target = "idConsultation", source = "consultationId")
    @Mapping(target = "refFonctionnelleConsultation", source = "contract.consultation.numero")
    @Mapping(target = "lot", source = "contract.lot.numeroLot")
    @Mapping(target = "typeDE", constant = "MP")
    @Mapping(target = "id", source = "contract.id")
    @Mapping(target = "acheteur", source = "contract.acheteur")
    @Mapping(target = "nature", source = "contract.nature")
    @Mapping(target = "objet", source = "contract.objet")
    @Mapping(target = "codeCPV", source = "contract.codeCPV")
    @Mapping(target = "procedure", source = "contract.procedure")
    @Mapping(target = "lieuExecution", source = "contract.lieuExecution")
    @Mapping(target = "dureeMois", source = "contract.dureeMois")
    @Mapping(target = "dateNotification", source = "contract.dateNotification")
    @Mapping(target = "datePublicationDonnees", source = "contract.datePublicationDonnees")
    @Mapping(target = "montant", source = "contract.montant")
    @Mapping(target = "valeurGlobale", source = "contract.valeurGlobale")
    @Mapping(target = "formePrix", source = "contract.formePrix")
    @Mapping(target = "titulaires", source = "contract.titulaires.titulaire")
    @Mapping(target = "modifications", source = "contract.modifications.modification")
    @Mapping(target = "operation", constant = SAVE_ESSENTIAL_DATA)
    @Mapping(target = "accessible", constant = "true")
    @Mapping(target = "autoriteConcedante", source = "contract.acheteur")
    @Mapping(target = "concessionnaires", source = "contract.titulaires.titulaire")
    @Mapping(target = "dateSignature", source = "contract.dateSignature")
    @Mapping(target = "dateDebutExecution", source = "contract.dateDebutExecution")
    @Mapping(target = "montantSubventionPublique", source = "contract.montantSubventionPublique")
    @Mapping(target = "donneesExecution", source = "contract.donneesExecution.donneesAnnuelles")
    SNEssentialDataDTO toSNSaveEssentialData(DumePlatform platform, MarcheType contract, String consultationId);


    default SNEssentialDataDTO toFinalSNSaveEssentialData(DumePlatform platform, MarcheType contract, String consultationId) {
        SNEssentialDataDTO essentialDataDTO = toSNSaveEssentialData(platform, contract, consultationId);
        if (contract.getTypeContrat() == null || contract.getTypeContrat() == TypeContratType.MARCHE_PUBLIC) {
            essentialDataDTO.setTypeDE("MP");
            essentialDataDTO.setAutoriteConcedante(null);
            essentialDataDTO.setDateSignature(null);
            essentialDataDTO.setDateDebutExecution(null);
            essentialDataDTO.setMontantSubventionPublique(null);
            essentialDataDTO.setDonneesExecution(null);
            essentialDataDTO.setConcessionnaires(null);
        } else {
            essentialDataDTO.setTypeDE("CC");
            essentialDataDTO.setAcheteur(null);
            essentialDataDTO.setDateNotification(null);
            essentialDataDTO.setCodeCPV(null);
            essentialDataDTO.setFormePrix(null);
            essentialDataDTO.setTitulaires(null);
        }
        return essentialDataDTO;
    }

    @Mapping(target = "plateforme.idPlateforme", source = "platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", expression = "java(java.util.Optional.ofNullable(contract).map(MarcheType::getAcheteur).map(AcheteurType::getNom).map(rs -> (rs != null && rs.length() > 100 ? rs.substring(0,100) : rs )).orElse(null))")
    @Mapping(target = "idDemandeur", source = "contract.acheteur.id")
    @Mapping(target = "idConsultation", ignore = true)
    @Mapping(target = "refFonctionnelleDE", source = "snNumber")
    @Mapping(target = "modifications", source = "contract.modifications.modification")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "lot", ignore = true)
    @Mapping(target = "acheteur", ignore = true)
    @Mapping(target = "nature", ignore = true)
    @Mapping(target = "objet", ignore = true)
    @Mapping(target = "codeCPV", ignore = true)
    @Mapping(target = "procedure", ignore = true)
    @Mapping(target = "lieuExecution", ignore = true)
    @Mapping(target = "dureeMois", ignore = true)
    @Mapping(target = "dateNotification", ignore = true)
    @Mapping(target = "datePublicationDonnees", ignore = true)
    @Mapping(target = "montant", ignore = true)
    @Mapping(target = "formePrix", ignore = true)
    @Mapping(target = "titulaires", ignore = true)
    @Mapping(target = "operation", constant = UPDATE_ESSENTIAL_DATA)
    @Mapping(target = "accessible", constant = "true")
    @Mapping(target = "autoriteConcedante", ignore = true)
    @Mapping(target = "concessionnaires", ignore = true)
    @Mapping(target = "dateSignature", ignore = true)
    @Mapping(target = "dateDebutExecution", ignore = true)
    @Mapping(target = "montantSubventionPublique", ignore = true)
    @Mapping(target = "donneesExecution", source = "contract.donneesExecution.donneesAnnuelles")
    SNEssentialDataDTO toSNUpdateEssentialData(DumePlatform platform, MarcheType contract, String snNumber);

    default SNEssentialDataDTO toFinalSNUpdateEssentialData(DumePlatform platform, MarcheType contract, String snNumber) {
        SNEssentialDataDTO essentialDataDTO = toSNUpdateEssentialData(platform, contract, snNumber);
        if (contract.getTypeContrat() == TypeContratType.CONTRAT_DE_CONCESSION) {
            essentialDataDTO.setTitulaires(null);
            if (essentialDataDTO.getModifications() != null) {
                for (ModificationDTO modificationDTO : essentialDataDTO.getModifications()) {
                    modificationDTO.setTitulaires(null);
                }
            }
        } else {
            essentialDataDTO.setDonneesExecution(null);
            essentialDataDTO.setConcessionnaires(null);
        }
        return essentialDataDTO;
    }

    @Mapping(target = "id", source = "id")
    @Mapping(target = "nom", expression = "java(java.util.Optional.ofNullable(acheteur).map(AcheteurType::getNom).map(rs -> (rs != null && rs.length() > 255 ? rs.substring(0,255) : rs )).orElse(null))")
    AcheteurDTO toAcheteur(AcheteurType acheteur);

    @Mapping(target = "typeCode", source = "lieuExecution.typeCode")
    LieuExecutionDTO toToLieuExecution(LieuExecutionType lieuExecution);


    @Mapping(target = "typeIdentifiant", expression = "java(java.util.Optional.ofNullable(titulaire.getTypeIdentifiant()).map(ti -> ti.value()).orElse(null))")
    @Mapping(target = "id", expression = "java( titulaire.getTypeIdentifiant() == com.atexo.dume.dto.donneesessentielles.xml.TypeIdentifiantType.HORS_UE ? java.util.Optional.ofNullable(titulaire.getId()).map(id -> id.length() > 18 ? id.substring(0,18) : id ).orElse(null) : titulaire.getId() )")
    TitulaireDTO toTitulaire(TitulaireType titulaire);

    List<TitulaireDTO> toListTitulaire(List<TitulaireType> titulaire);

    @Mapping(target = "titulaires", source = "titulaires.titulaire")
    @Mapping(target = "concessionnaires", source = "titulaires.titulaire")
    ModificationDTO toModificationDto(ModificationType modification);

    List<ModificationDTO> toListModificationDto(List<ModificationType> modifications);

    @Mapping(target = "tarifs", source = "tarifs.tarif")
    DonneesAnnuellesDTO toDonneesExecutionDto(DonneesAnnuellesType donneeAnnuelle);

    List<DonneesAnnuellesDTO> toListDonneesExecutionDto(List<DonneesAnnuellesType> donneesAnnuelles);

    TarifDTO toTarifDto(TarifType tarif);

    List<TarifDTO> toListTarifDto(List<TarifType> tarif);

    /**
     * Mapping des enumerations
     */

    default String toSNProcedure(ProcedureType procedureTyp) {
        if (procedureTyp == null) {
            return null;
        }
        switch (procedureTyp) {
            case PROCÉDURE_ADAPTÉE:
                return "PROCEDURE ADAPTEE";
            case APPEL_D_OFFRES_OUVERT:
                return "APPEL D'OFFRES OUVERT";
            case APPEL_D_OFFRES_RESTREINT:
                return "APPEL D'OFFRES RESTREINT";
            case PROCÉDURE_CONCURRENTIELLE_AVEC_NÉGOCIATION:
                return "PROCEDURE CONCURRENTIELLE AVEC NEGOCIATION";
            case PROCÉDURE_NÉGOCIÉE_AVEC_MISE_EN_CONCURRENCE_PRÉALABLE:
                return "PROCEDURE NEGOCIEE AVEC MISE EN CONCURRENCE PREALABLE";
            case MARCHÉ_NÉGOCIÉ_SANS_PUBLICITÉ_NI_MISE_EN_CONCURRENCE_PRÉALABLE:
                return "MARCHE NEGOCIE SANS PUBLICITE NI MISE EN CONCURRENCE PREALABLE";
            case DIALOGUE_COMPÉTITIF:
                return "DIALOGUE COMPETITIF";
            case PROCÉDURE_NÉGOCIÉE_OUVERTE:
                return "PROCEDURE NEGOCIEE OUVERTE";
            case PROCÉDURE_NON_NÉGOCIÉE_OUVERTE:
                return "PROCEDURE NON NEGOCIEE OUVERTE";
            case PROCÉDURE_NÉGOCIÉE_RESTREINTE:
                return "PROCEDURE NEGOCIEE RESTREINTE";
            case PROCÉDURE_NON_NÉGOCIÉE_RESTREINTE:
                return "PROCEDURE NON NEGOCIEE RESTREINTE";
            default:
                return "";
        }

    }


    default String toSNNature(NatureType natureType) {
        if (natureType == null) {
            return null;
        }
        switch (natureType) {
            case MARCHÉ:
                return "MARCHE";
            case MARCHÉ_DE_PARTENARIAT:
                return "MARCHE DE PARTENARIAT";
            case ACCORD_CADRE:
                return "ACCORD-CADRE";
            case MARCHÉ_SUBSÉQUENT:
                return "MARCHE SUBSEQUENT";
            case CONCESSION_DE_TRAVAUX:
                return "CONCESSION DE TRAVAUX";
            case CONCESSION_DE_SERVICE:
                return "CONCESSION DE SERVICE";
            case CONCESSION_DE_SERVICE_PUBLIC:
                return "CONCESSION DE SERVICE PUBLIC";
            case DÉLÉGATION_DE_SERVICE_PUBLIC:
                return "DELEGATION DE SERVICE PUBLIC";
            default:
                return "";
        }
    }


    default String toSNTypeCode(TypeCodeType typeCode) {
        if (typeCode == null) {
            return null;
        }
        switch (typeCode) {
            case CODE_POSTAL:
                return "CODE POSTAL";
            case CODE_COMMUNE:
                return "CODE COMMUNE";
            case CODE_ARRONDISSEMENT:
                return "CODE ARRONDISSEMENT";
            case CODE_CANTON:
                return "CODE CANTON";
            case CODE_DÉPARTEMENT:
                return "CODE DEPARTEMENT";
            case CODE_RÉGION:
                return "CODE REGION";
            case CODE_PAYS:
                return "CODE PAYS";
            default:
                return "";
        }
    }


    default String toSNFormePrix(FormePrixType formePrixType) {
        if (formePrixType == null) {
            return null;
        }
        switch (formePrixType) {
            case FERME:
                return "FERME";
            case FERME_ET_ACTUALISABLE:
                return "FERME ET ACTUALISABLE";
            case RÉVISABLE:
                return "REVISABLE";
            default:
                return "";
        }
    }


}
