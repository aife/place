package com.atexo.dume.mapper;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.PublicationStatus;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.Lot;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.util.CollectionUtils;

import java.util.*;

import static com.atexo.dume.mapper.XMLConstants.*;

/**
 * Mapper for {@link DumeRequestContext}  object : convert a normal SNLot object to {@link DumeRequestContextDTO} Object.
 *
 * @author ama.
 */

@Mapper(uses = {LotMapper.class, MetaDataMapper.class, DumeRequestMapper.class})
public interface DumeRequestContextMapper {

    /**
     * This method will convert a {@link DumeRequestContext} object using a {@link DumeRequestContextDTO} object
     * based on defined mappings.
     *
     * @param dumeRequestContext the object to be mapped
     * @return {@link DumeRequestContextDTO} Object.
     */
    @Mapping(source = "officialName", target = "officialName")
    @Mapping(source = "country", target = "country")
    @Mapping(source = "consultRef", target = "consultRef")
    @Mapping(source = "procedureType", target = "procedureType")
    @Mapping(source = "entitled", target = "entitled")
    @Mapping(source = "metadata", target = "metadata")
    @Mapping(source = "lotSet", target = "lotDTOSet")
    @Mapping(source = "dumeDocuments", target = "dumeRequestSets")
    @Mapping(source = "applyMarketFilter", target = "applyMarketFilter")
    DumeRequestContextDTO dumeReqContextToDumeReqContextDTO(final DumeRequestContext dumeRequestContext);

    /**
     * This method will convert a {@link DumeRequestContextDTO} object using a {@link DumeRequestContext} object
     * based on defined mappings.
     *
     * @param dumeRequestContextDTO the object to be mapped
     * @return {@link DumeRequestContext} Object.
     */
    @InheritInverseConfiguration
    DumeRequestContext contextRqDTOToContextRq(DumeRequestContextDTO dumeRequestContextDTO);

    /**
     * This method will convert  a List of {@link DumeRequestContext} Object to a List of {@link DumeRequestContextDTO} object
     *
     * @param dumeRequestContextList the input list to be converted
     * @return a List of {@link DumeRequestContextDTO}.
     */
    List<DumeRequestContextDTO> dumeReqContextListToDumeReqContextDTOList(final List<DumeRequestContext> dumeRequestContextList);


    /**
     * This method will convert a List of {@link DumeRequestContext} object using a List of {@link DumeRequestContextDTO} object
     * based on defined mappings.
     *
     * @param dumeRequestContextDTOList the object to be mapped
     * @return {@link List<DumeRequestContext>} Object.
     */
    @InheritInverseConfiguration
    List<DumeRequestContext> contextRqDTOsListToContextRqList(List<DumeRequestContextDTO> dumeRequestContextDTOList);

    @Mapping(target = "officialName", source = "officialName")
    @Mapping(target = "country", source = "country")
    @Mapping(target = "consultRef", source = "consultRef")
    @Mapping(target = "procedureType", source = "procedureType")
    @Mapping(target = "entitled", source = "entitled")
    @Mapping(target = "metadata", source = "metadata")
    @Mapping(target = "dumeDocuments", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "default", ignore = true)
    @Mapping(target = "applyMarketFilter", ignore = true)
    void updateContextFromContextDTOWithoutDumeRequests(DumeRequestContextDTO contextDTO, @MappingTarget DumeRequestContext dumeRequestContext);

    /**
     * TThis method will update the context and lots
     *
     * @param contextDTO         the new value
     * @param dumeRequestContext to update
     */
    default void updateContextFromDTOWithLot(DumeRequestContextDTO contextDTO, DumeRequestContext dumeRequestContext) {
        LotMapper lotMapper = Mappers.getMapper(LotMapper.class);
        updateContextFromContextDTOWithoutDumeRequests(contextDTO, dumeRequestContext);
        Set<Lot> lotsToUpdateOrAdd = new HashSet<>();
        if (!CollectionUtils.isEmpty(contextDTO.getLotDTOSet())) {
            // Traitement de la gestion des lots
            for (LotDTO lotDTO : contextDTO.getLotDTOSet()) {
                Optional<Lot> lotFound = dumeRequestContext.getLotSet().stream()
                        .filter(lot -> Objects.equals(lot.getLotNumber(), lotDTO.getLotNumber()))
                        .findFirst();
                // Si lot exist alors on le mis a jour
                // Sinon on ajouter un nouveau
                if (lotFound.isPresent()) {
                    lotMapper.updateLotByLotDTO(lotDTO, lotFound.get());
                    lotsToUpdateOrAdd.add(lotFound.get());
                } else {
                    Lot lotToAdd = lotMapper.toEntity(lotDTO);
                    lotToAdd.setContext(dumeRequestContext);
                    lotsToUpdateOrAdd.add(lotToAdd);
                }
            }
        }
        dumeRequestContext.getLotSet().clear();
        dumeRequestContext.getLotSet().addAll(lotsToUpdateOrAdd);
    }
    @Mapping(target = "publishedDumeList", source = "dumeDocuments")
    @Mapping(target = "statut", constant = "OK")
    @Mapping(target = "standard", source = "default", defaultValue = "false")
    PublicationStatus toPublicationStatuts(DumeRequestContext dumeRequestContext);

    default List<DocumentReferenceType> toDocumentReferenceTypeList(DumeRequestContext dumeRequestContext) {
        if (dumeRequestContext == null) {
            return Collections.emptyList();
        }

        List<DocumentReferenceType> documentReferenceTypeList = new ArrayList<>();

        DocumentReferenceType documentReferenceType = toDocumentReferenceType(dumeRequestContext);
        DescriptionType descriptionType = new DescriptionType();
        descriptionType.setValue(dumeRequestContext.getObject());
        documentReferenceType.getAttachment().getExternalReference().getDescription().add(descriptionType);

        documentReferenceTypeList.add(documentReferenceType);

        return documentReferenceTypeList;
    }

    @Mapping(target = "ID.schemeID", constant = COM_GROW_TEMPORARY_ID)
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeAgencyName", constant = DG_GROW)
    @Mapping(target = "ID.schemeVersionID", constant = V_1_1)
    @Mapping(target = "documentTypeCode.listID", constant = REFERENCES_TYPE_CODES)
    @Mapping(target = "documentTypeCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "documentTypeCode.listVersionID", constant = V_1_0)
    @Mapping(target = "documentTypeCode.value", constant = TED_CN)
    @Mapping(target = "attachment.externalReference.fileName.value", source = "entitled")
    DocumentReferenceType toDocumentReferenceType(DumeRequestContext dumeRequestContext);
}
