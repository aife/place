package com.atexo.dume.mapper;

import com.atexo.dume.dto.CriteriaCategorieDTO;
import com.atexo.dume.model.CriteriaCategorie;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Mapper CriteriaCategorie to/From CriteriaCategorieDTO
 */
@Mapper
public interface CriteriaCategorieMapper {

    CriteriaCategorieDTO toDTO(CriteriaCategorie criteriaCategorie);

    List<CriteriaCategorieDTO> toDTOList(List<CriteriaCategorie> criteriaCategories);

    CriteriaCategorie toEntity(CriteriaCategorieDTO criteriaCategorie);
}
