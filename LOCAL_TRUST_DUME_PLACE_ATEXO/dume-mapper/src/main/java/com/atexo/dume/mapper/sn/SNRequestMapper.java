package com.atexo.dume.mapper.sn;

import com.atexo.dume.dto.sn.request.SNCritereDumeOE;
import com.atexo.dume.dto.sn.request.SNECertisDTO;
import com.atexo.dume.dto.sn.request.SNLot;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.Lot;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atexo.dume.toolbox.consts.NationalServiceFormatEnum.DUME_PDF;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.*;

@Mapper
public interface SNRequestMapper {
    @Mapping(target = "plateforme.idPlateforme", source = "request.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "request.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "request.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "request.context.metadata.siret")
    @Mapping(target = "idConsultation",
            expression = "java(request.getContext().getPlatform().getId() +\"-\" + request.getContext().getMetadata().getConsultationIdentifier())")
    @Mapping(target = "libelleConsultation", source = "request.context.consultRef")
    @Mapping(target = "refFonctionnelleConsultation", source = "request.context.consultRef")
    @Mapping(target = "identifiantA", source = "request.context.metadata.siret")
    @Mapping(target = "DLRO", source = "request.context.metadata.bidClosingDate", dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ")
    @Mapping(target = "typeIdA", constant = "1")
    @Mapping(target = "idTypeProcedure", source = "request.context.metadata.procedureTypeIdentifier")
    @Mapping(target = "idNatureMarche", source = "request.context.metadata.natureMarketIdentifier")
    @Mapping(target = "idTypeMarche", source = "request.context.metadata.marketTypeIdentifier")
    @Mapping(target = "idStatutDume", source = "statut")
    @Mapping(target = "lots", source = "request.context.lotSet")
    @Mapping(target = "operation", constant = SAVE_DUME)
    @Mapping(target = "dumeA.xmlDume", source = "xml")
    SNRequestDTO toSnRequestDto(DumeRequest request, String statut, String xml);

    @Mapping(target = "plateforme.idPlateforme", source = "request.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "request.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "request.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "request.context.metadata.siret")
    @Mapping(target = "dume.idDume", source = "request.numDumeSN")
    @Mapping(target = "format", source = "format")
    @Mapping(target = "operation", source = "endPoint")
    SNRequestDTO toSnDownloadRequestDto(DumeRequest request, String format, String endPoint);

    @Mapping(target = "plateforme.idPlateforme", source = "context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "context.metadata.siret")
    @Mapping(target = "dume.xmlDume", source = "xml")
    @Mapping(target = "operation", constant = GET_FORMATED_DUME)
    @Mapping(target = "format", constant = DUME_PDF)
    SNRequestDTO toSnDownlaodResponsePDF(String xml, DumeRequestContext context);

    @Mapping(target = "plateforme.idPlateforme", source = "response.dumeRequest.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "response.dumeRequest.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "response.dumeRequest.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "response.dumeRequest.context.metadata.siret")
    @Mapping(target = "operation", constant = SEARCH_DUMES)
    @Mapping(target = "dumesOE", source = "critereOE")
    @Mapping(target = "identifiantsOE", source = "identifiants")
    @Mapping(target = "siretOE", ignore = true)
    SNRequestDTO toSnSearch(DumeResponse response, List<SNCritereDumeOE> critereOE, List<SNCritereDumeOE> identifiants);

    @Mapping(target = "plateforme.idPlateforme", source = "response.dumeRequest.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "response.dumeRequest.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "response.dumeRequest.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "response.dumeRequest.context.metadata.siret")
    @Mapping(target = "operation", constant = GET_E_CERTIS)
    @Mapping(target = "criterion", source = "uuid")
    @Mapping(target = "langue", source = "languageCode")
    @Mapping(target = "nationalEntities", source = "countryCode")
    @Mapping(target = "siretOE", ignore = true)
    SNECertisDTO toSnECertis(DumeResponse response, String uuid, String countryCode, String languageCode);

    @Mapping(target = "idDume", source = "numeroSN")
    @Mapping(target = "idStatutDume", source = "status")
    SNCritereDumeOE toSNDumeOE(String numeroSN, String status);

    @Mapping(target = "identifiantOE", source = "siretOE")
    SNCritereDumeOE toIdentifiantOE(DumeResponse dumeResponse);


    @Mapping(target = "libelle", expression = "java(limitLength(lot.getLotName(), 255))")
    @Mapping(target = "numeroLot", source = "lotNumber")
    SNLot toLot(Lot lot);

    default List<SNLot> toLotsSN(Set<Lot> lotSet) {
        List<SNLot> snLotList = new ArrayList<>();
        if (CollectionUtils.isEmpty(lotSet)) {
            SNLot fake = new SNLot();
            fake.setLibelle("Fake Lot");
            fake.setNumeroLot("0");
            snLotList.add(fake);
        } else {
            snLotList.addAll(lotSet.stream().map(this::toLot).collect(Collectors.toList()));
        }
        return snLotList;
    }

    @Mapping(target = "plateforme.idPlateforme", source = "request.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "request.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "request.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "request.context.metadata.siret")
    @Mapping(target = "dumeA.idDume", source = "request.numDumeSN")
    @Mapping(target = "typeDonnees", constant = "1")
    @Mapping(target = "siretOE", source = "siretOE")
    @Mapping(target = "operation", constant = MERGE_DUMES_WITH_ENTREPRISE_DATA)
    SNRequestDTO toSNFusionRequestDTO(DumeRequest request, String siretOE);

    @Mapping(target = "plateforme.idPlateforme", source = "dumeResponse.dumeRequest.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "dumeResponse.dumeRequest.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "dumeResponse.dumeRequest.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "dumeResponse.dumeRequest.context.metadata.siret")
    @Mapping(target = "idConsultation",
            expression = "java(dumeResponse.getDumeRequest().getContext().getPlatform().getId() +\"-\" + dumeResponse.getDumeRequest().getContext().getMetadata().getConsultationIdentifier())")
    @Mapping(target = "libelleConsultation", source = "dumeResponse.dumeRequest.context.consultRef")
    @Mapping(target = "refFonctionnelleConsultation", source = "dumeResponse.dumeRequest.context.consultRef")
    @Mapping(target = "identifiantA", source = "dumeResponse.dumeRequest.context.metadata.siret")
    @Mapping(target = "DLRO", source = "dumeResponse.dumeRequest.context.metadata.bidClosingDate", dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ")
    @Mapping(target = "idTypeProcedure", source = "dumeResponse.dumeRequest.context.metadata.procedureTypeIdentifier")
    @Mapping(target = "idNatureMarche", source = "dumeResponse.dumeRequest.context.metadata.natureMarketIdentifier")
    @Mapping(target = "idTypeMarche", source = "dumeResponse.dumeRequest.context.metadata.marketTypeIdentifier")
    @Mapping(target = "idTypeResponse", constant = "4")
    @Mapping(target = "identifiantOE", source = "dumeResponse.siretOE")
    @Mapping(target = "siretOE", ignore = true)
    @Mapping(target = "lots", source = "dumeResponse.lotSet")
    @Mapping(target = "operation", constant = SAVE_DUME)
    @Mapping(target = "idStatutDume", source = "statut")
    @Mapping(target = "dumeOE.xmlDume", source = "xml")
    @Mapping(target = "dumeOE.idDume", source = "dumeResponse.numeroSN")
    @Mapping(target = "typeIdOE", source = "dumeResponse.typeOE")
    @Mapping(target = "idGroupement", source = "dumeResponse.groupementLabel")
    @Mapping(target = "siretNP1", source = "dumeResponse.siretMandataire")
    SNRequestDTO fromDumeResponseToSnRequestDTO(DumeResponse dumeResponse, String statut, String xml);


    @Mapping(target = "plateforme.idPlateforme", source = "response.dumeRequest.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "response.dumeRequest.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "response.dumeRequest.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "response.dumeRequest.context.metadata.siret")
    @Mapping(target = "dume.idDume", source = "numeroSN")
    @Mapping(target = "format", source = "format")
    @Mapping(target = "operation", source = "endPoint")
    @Mapping(target = "siretOE", ignore = true)
    SNRequestDTO toSnDownloadResponseDto(DumeResponse response, String format, String endPoint,String numeroSN);


    @Mapping(target = "plateforme.idPlateforme", source = "response.dumeRequest.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "response.dumeRequest.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "response.dumeRequest.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "response.dumeRequest.context.metadata.siret")
    @Mapping(target = "dumeA.idDume", source = "response.dumeRequest.numDumeSN")
    @Mapping(target = "dumeOE.idDume", source = "idDumeOE")
    @Mapping(target = "operation", source = "endPoint")
    @Mapping(target = "siretOE", ignore = true)
    SNRequestDTO toSNFusionDumeAWithDumeOEDefiniedByNumSN(DumeResponse response, String idDumeOE, String endPoint);

    @Mapping(target = "plateforme.idPlateforme", source = "context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "operation", constant = GET_DUME_MODEL)
    SNRequestDTO toSnGetModel(DumeRequestContext context);

    @Mapping(target = "plateforme.idPlateforme", source = "context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "context.metadata.siret")
    @Mapping(target = "operation", constant = UPDATE_METADATA)
    @Mapping(target = "colonneAModifier", constant = "DLRO")
    @Mapping(target = "valeurAModifier", source = "context.metadata.bidClosingDate", dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ")
    @Mapping(target = "dume.idDume", source = "numeroSN")
    SNRequestDTO toSNUpdateMetadonneDLRO(DumeRequestContext context, String numeroSN);

    @Mapping(target = "plateforme.idPlateforme", source = "request.context.platform.snPlatformIdentifier")
    @Mapping(target = "plateforme.idTechniquePlateforme", source = "request.context.platform.snPlatformTechnicalIdentifier")
    @Mapping(target = "plateforme.typeIdPlateforme", constant = "1")
    @Mapping(target = "rsDemandeur", source = "request.context.platform.platLabel")
    @Mapping(target = "idDemandeur", source = "request.context.metadata.siret")
    @Mapping(target = "idConsultation", expression = "java(request.getContext().getPlatform().getId() +\"-\" + request.getContext().getMetadata().getConsultationIdentifier())")
    @Mapping(target = "libelleConsultation", source = "request.context.consultRef")
    @Mapping(target = "refFonctionnelleConsultation", source = "request.context.consultRef")
    @Mapping(target = "identifiantA", source = "request.context.metadata.siret")
    @Mapping(target = "nomIdentifiantA", source = "request.context.metadata.organization")
    @Mapping(target = "DLRO", source = "request.context.metadata.bidClosingDate", dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ")
    @Mapping(target = "idTypeProcedure", source = "request.context.metadata.procedureTypeIdentifier")
    @Mapping(target = "idNatureMarche", source = "request.context.metadata.natureMarketIdentifier")
    @Mapping(target = "idTypeMarche", source = "request.context.metadata.marketTypeIdentifier")
    @Mapping(target = "lots", source = "request.context.lotSet")
    @Mapping(target = "operation", constant = SAVE_SIMPLIFIED_DUME)
    SNRequestDTO toSnSimplifiedRequestDto(DumeRequest request);

    default String limitLength(String field, int length) {
        return Optional.ofNullable(field).map(nom -> (nom != null && nom.length() > length ? nom.substring(0, length) : nom)).orElse(null);
    }
}
