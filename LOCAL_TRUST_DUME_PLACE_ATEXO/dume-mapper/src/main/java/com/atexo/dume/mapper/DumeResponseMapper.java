package com.atexo.dume.mapper;

import com.atexo.dume.dto.CriteriaDTO;
import com.atexo.dume.dto.DumeResponseDTO;
import com.atexo.dume.dto.sn.response.SNSearchOEResponse;
import com.atexo.dume.model.Criteria;
import com.atexo.dume.model.CriteriaResponse;
import com.atexo.dume.model.DumeResponse;
import com.atexo.dume.model.ExclusionCriteria;
import com.atexo.dume.model.OtherCriteria;
import com.atexo.dume.model.SelectionCriteria;
import grow.names.specification.ubl.schema.xsd.espdresponse_1.ESPDResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyIdentificationType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NameType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.atexo.dume.mapper.XMLConstants.COUNTRY_CODE_IDENTIFIER;
import static com.atexo.dume.mapper.XMLConstants.EU_COM_GROW;
import static com.atexo.dume.mapper.XMLConstants.TED;
import static com.atexo.dume.mapper.XMLConstants.V_1_0_2;

/**
 * Mapper DumeResponse to/From DumeResponseDTO
 */
@Mapper(uses = {LegislationReferenceMapper.class, CriteriaCategorieMapper.class, RequirementMapper.class, LotMapper.class,
        RepresentantLegalMapper.class, EconomicPartyInfoMapper.class, CriteriaMapper.class, DumeRequestContextMapper.class})
public interface DumeResponseMapper {

    LotMapper lotMapper = Mappers.getMapper(LotMapper.class);
    CriteriaMapper criteriaMapper = Mappers.getMapper(CriteriaMapper.class);
    EconomicPartyInfoMapper economicPartyInfoMapper = Mappers.getMapper(EconomicPartyInfoMapper.class);
    RepresentantLegalMapper representantLegalMapper = Mappers.getMapper(RepresentantLegalMapper.class);
    DumeRequestMapper dumeRequestMapper = Mappers.getMapper(DumeRequestMapper.class);


    @Mapping(source = "exclusionCriteria", target = "exclusionCriteriaList")
    @Mapping(source = "otherCriteria", target = "otherCriteriaList")
    @Mapping(source = "dumeRequest.context.country", target = "country")
    @Mapping(source = "dumeRequest.context.entitled", target = "entitled")
    @Mapping(source = "dumeRequest.context.object", target = "object")
    @Mapping(source = "dumeRequest.context.procedureType", target = "procedureType")
    @Mapping(source = "dumeRequest.context.consultRef", target = "consultRef")
    @Mapping(source = "dumeRequest.context.officialName", target = "officialName")
    @Mapping(source = "dumeRequest.numDumeSN", target = "numDumeAcheteurSN")
    @Mapping(source = "lotSet", target = "lots")
    @Mapping(source = "numeroSN", target = "numeroSN")
    DumeResponseDTO toResponseDto(DumeResponse dumeResponse);


    default void updateDumeReponseUsingDTO(DumeResponse dumeResponse, DumeResponseDTO dumeResponseDTO) {

        dumeResponse.setConfirmed(dumeResponseDTO.getConfirmed());
        dumeResponse.setMicro(dumeResponseDTO.getMicro());


        for (CriteriaResponse criteriaResponse : dumeResponse.getCriteriaResponseList()) {
            // Selection
            if (criteriaResponse.getCriteria() instanceof SelectionCriteria) {
                updateCriteria(dumeResponseDTO.getSelectionCriteriaList(), criteriaResponse);
            }
            // Exclusion
            if (criteriaResponse.getCriteria() instanceof ExclusionCriteria) {
                updateCriteria(dumeResponseDTO.getExclusionCriteriaList(), criteriaResponse);
            }
            // Other
            if (criteriaResponse.getCriteria() instanceof OtherCriteria) {
                updateCriteria(dumeResponseDTO.getOtherCriteriaList(), criteriaResponse);
            }
        }

        economicPartyInfoMapper.updateEconomicPartyInfo(dumeResponseDTO.getEconomicPartyInfo(), dumeResponse.getEconomicPartyInfo());

        dumeResponse.getRepresentantLegals().forEach(representantLegal ->
            dumeResponseDTO.getRepresentantLegals().forEach(representantLegalDTO -> {
                if (Objects.equals(representantLegal.getId(), representantLegalDTO.getId())) {
                    representantLegalMapper.udpateRepresentantLegal(representantLegalDTO, representantLegal);
                }
            })
        );
    }

    default void updateCriteria(List<? extends CriteriaDTO> criteriaList, CriteriaResponse criteriaResponse) {
        for (CriteriaDTO criteriaDTO : criteriaList) {
            if (Objects.equals(criteriaDTO.getIdCriteriaResponse(), criteriaResponse.getId())) {
                criteriaMapper.mapCriteria(criteriaDTO, criteriaResponse);
            }
        }
    }

    //XML
    // UBL Version
    @Mapping(target = "UBLVersionID.schemeAgencyID", constant = "OASIS-UBL-TC")
    @Mapping(target = "UBLVersionID.value", constant = "2.1")

    // customization ID
    @Mapping(target = "customizationID.schemeName", constant = "CustomizationID")
    @Mapping(target = "customizationID.schemeAgencyID", constant = "BII")
    @Mapping(target = "customizationID.schemeVersionID", constant = "3.0")
    @Mapping(target = "customizationID.value", constant = "urn:www.cenbii.eu:transaction:biitrns070:ver3.0")

    // ID
    @Mapping(target = "ID.schemeID", constant = "ISO/IEC 9834-8:2008 - 4UUID")
    @Mapping(target = "ID.schemeAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "ID.schemeAgencyName", constant = "DG GROW (European Commission)")
    @Mapping(target = "ID.schemeVersionID", constant = "1.1")
    @Mapping(target = "ID.value", expression = "java(java.util.UUID.randomUUID().toString() )")

    //Copy Indicator
    @Mapping(target = "copyIndicator.value", constant = "false")

    // Version ID
    @Mapping(target = "versionID.schemeVersionID", constant = EU_COM_GROW)
    @Mapping(target = "versionID.value", constant = V_1_0_2)

    // issue date & issue time
    // JDK8 Date Time API not supported by mapstruct
    @Mapping(target = "issueDate.value", expression = "java(org.joda.time.LocalDate.now())", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "issueTime.value", expression = "java(org.joda.time.LocalTime.now())", dateFormat = "hh:mm:ss")


    //Contract Folder ID
    @Mapping(target = "contractFolderID.schemeVersionID", constant = TED)
    @Mapping(target = "contractFolderID.value", source = "dumeRequest.context.consultRef")

    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.value", source = "dumeRequest.context.country")
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listID", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listName", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "contractingParty.party.postalAddress.country.identificationCode.listAgencyID", constant = EU_COM_GROW)
    // EconomicOperatorParty
    @Mapping(target = "economicOperatorParty.SMEIndicator.value", source = "micro")
    @Mapping(target = "economicOperatorParty.representativeNaturalPerson", source = "representantLegals")

    @Mapping(target = "economicOperatorParty.party.contact.name.value", source = "economicPartyInfo.contact")
    @Mapping(target = "economicOperatorParty.party.contact.electronicMail.value", source = "economicPartyInfo.email")
    @Mapping(target = "economicOperatorParty.party.contact.telephone.value", source = "economicPartyInfo.telephone")

    @Mapping(target = "economicOperatorParty.party.postalAddress.country.identificationCode.value", source = "economicPartyInfo.pays")
    @Mapping(target = "economicOperatorParty.party.postalAddress.country.identificationCode.listID", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "economicOperatorParty.party.postalAddress.country.identificationCode.listName", constant = COUNTRY_CODE_IDENTIFIER)
    @Mapping(target = "economicOperatorParty.party.postalAddress.country.identificationCode.listVersionID", constant = V_1_0_2)
    @Mapping(target = "economicOperatorParty.party.postalAddress.country.identificationCode.listAgencyID", constant = EU_COM_GROW)
    @Mapping(target = "economicOperatorParty.party.postalAddress.postalZone.value", source = "economicPartyInfo.codePostal")
    @Mapping(target = "economicOperatorParty.party.postalAddress.streetName.value", source = "economicPartyInfo.rue")
    @Mapping(target = "economicOperatorParty.party.postalAddress.cityName.value", source = "economicPartyInfo.ville")
    @Mapping(target = "economicOperatorParty.party.websiteURI.value", source = "economicPartyInfo.siteInternet")
    @Mapping(target = "additionalDocumentReference", source = "dumeRequest.context")
    ESPDResponseType toEspdResponse(DumeResponse response);

    default ESPDResponseType toFinalResponse(final DumeResponse response, List<Criteria> criteriaList) {
        ESPDResponseType espdResponseType = toEspdResponse(response);
        // lots si pas de lot on ajoute un fake lot
        espdResponseType.getProcurementProjectLot().addAll(lotMapper.toLots(response.getLotSet()));
        espdResponseType.getCriterion().addAll(response.getCriteriaResponseList().stream().filter(criteriaResponse -> criteriaList.contains(criteriaResponse.getCriteria())).map(criteriaMapper::toFinalCriterionWithResponse).collect(Collectors.toList()));
        espdResponseType.getContractingParty().getParty().getPartyName().add(dumeRequestMapper.toContractingParty(response.getDumeRequest()));

        // TVA
        PartyIdentificationType vat = new PartyIdentificationType();
        IDType vatValue = new IDType();
        vatValue.setSchemeAgencyID(EU_COM_GROW);
        vatValue.setValue(response.getEconomicPartyInfo().getNumTVA());
        vat.setID(vatValue);
        espdResponseType.getEconomicOperatorParty().getParty().getPartyIdentification().add(vat);

        // TVA Etranger
        PartyIdentificationType vatExt = new PartyIdentificationType();
        IDType vatExtValue = new IDType();
        vatExtValue.setSchemeAgencyID(EU_COM_GROW);
        vatExtValue.setValue(response.getEconomicPartyInfo().getNumEtrange());
        vatExt.setID(vatExtValue);
        espdResponseType.getEconomicOperatorParty().getParty().getPartyIdentification().add(vatExt);

        // PartyName
        PartyNameType partyNameType = new PartyNameType();
        NameType nameType = new NameType();
        nameType.setValue(response.getEconomicPartyInfo().getNom());
        partyNameType.setName(nameType);
        espdResponseType.getEconomicOperatorParty().getParty().getPartyName().add(partyNameType);

        return espdResponseType;
    }

    // mapper pour les recherches OE
    @Mapping(source = "dumeOEResponse.dumeOEId", target = "numeroSN")
    @Mapping(source = "consultation.referenceFonctionnelle", target = "consultRef")
    DumeResponseDTO toResponseDto(SNSearchOEResponse dumeOESearch);
}
