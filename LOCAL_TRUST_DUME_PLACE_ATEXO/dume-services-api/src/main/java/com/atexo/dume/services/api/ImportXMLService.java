package com.atexo.dume.services.api;

import java.io.InputStream;

/**
 * Interface des Service pour l'import du XML
 */
public interface ImportXMLService {
    /**
     * Importer un DumeRequestModel pour un domaine precis
     * @param xml le model
     * @param domaine le domaine
     * @return
     */
    Boolean importDumeRequestModelDomaine(InputStream xml, String domaine);

    /**
     * Importer un DumeRequestModel pour une platforme precise
     *
     * @param xml      le model
     * @param platform
     * @return
     */
    Boolean importDumeRequestModelPlatform(InputStream xml, String platform);
}
