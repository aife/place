package com.atexo.dume.services.api;

import com.atexo.dume.dto.CriteriaCategorieDTO;

import java.util.List;

/**
 * Interface des Service pour les catégories de critère
 */
public interface CriteriaCategorieService {

    /**
     * Service qui permet de récuperer tous les dtos des catégories de critère de selection
     *
     * @return List de CriteriaCategorieDTO qui correspondent aux critères de sélection
     */
    List<CriteriaCategorieDTO> getAllSelectionCriteriaCategorieDTO();


}
