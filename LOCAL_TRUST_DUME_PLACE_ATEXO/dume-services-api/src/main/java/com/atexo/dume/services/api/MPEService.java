package com.atexo.dume.services.api;

import com.atexo.dume.dto.donneesessentielles.xml.MarchesType;
import com.atexo.dume.model.DumePlatform;

/**
 * WebService de synchronisation MPE
 */
public interface MPEService {
    /**
     * @param platform plateforme MPE
     * @return la liste des marchés modifiés à la date du jour (paramètre date_modification_min)
     */
    MarchesType getModifiedContract(DumePlatform platform);
}
