package com.atexo.dume.services.api;

import com.atexo.dume.model.DumePlatform;

public interface SchedularTask {

    void publishDumeOE(Long idDumeResponse);

    void publishDumeAcheteur(Long idDumeRequest);

    void getPDFOE(Long idDumeResponse);

    void getPDFAcheteur(Long idDumeRequest);

    void updateDLROAcheteur(Long idDumeRequest);

    void replaceDumeAcheteur(Long idDumeRequest);

    void downloadTemplateXMLOE(Long idDumeRequest);

    void publishDonneesEssentielles(DumePlatform platform);
}
