package com.atexo.dume.services.api;

import com.atexo.dume.model.DumeDomain;
import com.atexo.dume.model.DumePlatform;
import com.atexo.dume.model.profile.DumeDomainPropValue;
import com.atexo.dume.model.profile.DumePlatPropValue;
import com.atexo.dume.model.profile.DumeProfileProp;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;

/**
 * Contract for ProfilePropertyValue operations.
 * <p>
 * {@link DumeProfileProp} represent the value of a DUME profile Param.
 * <p>For the moment : only 3 level of hierarchy are defined
 * *   User level
 * *   Platform Level
 * *   Domain Level
 * <p>
 */
public interface ProfileParamService {


    /**
     * This method will get the value of dume property as defined at domain level
     *
     * @param key           property key
     * @param domIdentifier the dom identifier
     * @return String value of the property
     */
    String getDomProfileValue(final String key, final Long domIdentifier);

    /**
     * This method will get the value of dume property as defined at platform level
     *
     * @param key            property key
     * @param platIdentifier the platform identifier
     * @return String value of the property
     */
    String getPlatProfileValue(final String key, final Long platIdentifier);

    /**
     * This method will get the value of dume property as defined at user level
     *
     * @param key           property key
     * @param usrIdentifier the user identifier
     * @return String value of the property
     */
    String getUsrProfileValue(final String key, final Long usrIdentifier);

    /**
     * This method will get the value of dume property using priority logic usr>platform>domain
     *
     * @param key                property key
     * @param usrIdentifier      the user identifier
     * @param platformIdentifier the platform identifier
     * @param domainIdentifier   the domain identifier
     * @return String value of the property
     */
    String getValue(final String key, final Long usrIdentifier, final Long platformIdentifier, final Long domainIdentifier);


    /**
     * This method will add a new DumeProfileParam to DB.
     * The DumeProfileParam to be saved need at least one value at domain level and to hava isDomain level as TRUE.
     *
     * @param profileProp {@see DumeProfileProp} Object.
     * @return {@see DumeProfileProp} with the db identifier.
     */
    DumeProfileProp createDumeProfileProperty(@NonNull final DumeProfileProp profileProp);

    DumeDomainPropValue getDomPropValueOrCreateByDumeProfileProp(String label, @NotNull DumeDomain dumeDomain);


    DumePlatPropValue getPlatPropValueOrCreateByDumeProfileProp(String label, @NotNull DumePlatform dumePlatform);


}

