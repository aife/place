package com.atexo.dume.services.api;

import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.DumeRequestDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeRequestContext;
import com.atexo.dume.model.Lot;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * La Class de Service de dumeRequest
 */

public interface DumeRequestService {

    /**
     * Creation d'un DumeRequest (à l'etat actuel) on
     * ajoute tous les critéres (selection, execlusion)
     *
     * @param context {@link DumeRequestContext}
     * @return DumeRequest
     */
    DumeRequest createDumeRequest(final DumeRequestContext context) throws ServiceException, DumeResourceNotFoundException;

    /**
     * Retourne un DTO construit sur le DumeRequest récupéré en base à l'aide de l'id.
     * @return un DumeRequestDTO ou null si le DumeRequest n'a pas été trouvé.
     *
     * @param id L'id du dumeRequest
     * @return Le DTO
     */
    DumeRequestDTO getDumeRequestDTOById(Long id) throws DumeResourceNotFoundException;

    /**
     * Mettre à jour les données heritées d'un dume Request à partir son id
     *
     * @param id      est l'id du dumeRequest
     * @param context les nouvelles données héritées
     * @return
     */
    Long updateContextDumeRequestById(Long id, DumeRequestContextDTO context);

    /**
     * génère le XML qui sera transmis au service national d'un dume Request à partir son id
     *
     * @param id      est l'id du dumeRequest
     * @return contenu XML selon l'id Request
     */
    String generateDumeRequestXML(Long id) throws ServiceException, DumeResourceNotFoundException;

    /**
     * Suppression d'un dumeRequest à partir son Id
     *
     * @param id du DumeRequest
     * @return True si tout ca se passe sinon false;
     */
    Boolean deleteDumeRequestById(Long id) throws DumeResourceNotFoundException;

    /**
     * Service permettant de mettre à jour un DumeRequest
     *
     * @param dumeRequestDTO Le DTO du dumeRequest à update
     * @return le dumeRequestDTO mis à jour
     */
    DumeRequest updateDumeRequestByDTO(DumeRequestDTO dumeRequestDTO) throws ServiceException, DumeResourceNotFoundException;

    /**
     * Récuperation de tous les dumeRequest
     *
     * @return list des dto
     */
    List<DumeRequestDTO> getAllDumeRequestDTO();

    Optional<DumeRequest> getDumeRequestById(Long idDumeRequest);

    /**
     * This method will set the default criteria
     *
     * @param dumeRequest
     */
    void setCriteriaDefaut(DumeRequest dumeRequest);

    /**
     * Publish all the Dume Requests with status : {@link com.atexo.dume.model.EtatDume.A_PUBLIER}
     */
    void publishAllDumeRequests();

    /**
     * Store all the retrieved PDF files (after publication ) in the File System
     */

    void retrieveAllDumeRequestsPDF();

    /**
     * @param numeroSN numéro du Service national retrieved after publishing a DUME Response
     * @return the PDF file associated to a DUME
     */

    File getPDFFile(String numeroSN) throws DumeResourceNotFoundException;

    /**
     * This method will update all the dumeRequest with Status DLRO_MODIFIEE and synchronize it
     * with the SN
     */
    void updateDLROIntoSN();

    void remplaceDumeWithSN();

    /**
     * This methods will download a template of Response for all published DumeRequest flaged with
     * XML_OE_RETRIEVED is false
     */
    void downloadTemplateOE();

    /**
     * THis method check for dumeRequest if there is need to purge the relation Criteria.
     * Il faut purger les lots dans les cas suivants :
     *  <ul>
     *      <li>Ajout d'un lot</li>
     *      <li>Suppresion d'un lot assiocé à un critére</li>
     *      <li>Modification d'un lot associé à un critére</li>
     *  </ul>
     *  On ne purge pas dans les cas suivants :
     *  <ul>
     *      <li>Suppression d'un lot non associé</li>
     *      <li>Modification d'un lot non associé </li>
     *  </ul>
     * @param dumeDocument
     * @param lotDTOS
     * @return True if needed Else False
     */
    Boolean checkPurgeCriteriaLot(DumeRequest dumeDocument, Set<LotDTO> lotDTOS);

    /**
     * This method purge the relation criteria-lot for all selection criteria of the dume
     * @param dumeRequest
     * @return
     */
    DumeRequest purgeCriteriaLot(DumeRequest dumeRequest);

    /**
     * This method check if at least a lot has a criteria
     * Le dume est valide dans les cas suivants :
     * <ul>
     *     <li>Si il y a aucun lot associé </li>
     *     <li>il existe au moins un critère associé a tous les lots (lotList est vide) </li>
     *     <li>les lots associés sont égaux à la liste des lots attachée au Context </li>
     * </ul>
     * @param dumeRequest
     * @param lotSet
     * @return
     */
    Set<Lot> checkValidLot(DumeRequest dumeRequest, Set<Lot> lotSet);

    /**
     * This method will generate the XML of dume Request by NUMERO_SN
     *
     * @param numeroSN the numeroSN to find
     * @return
     */
    byte[] generateXMLByNumeroSN(String numeroSN) throws DumeResourceNotFoundException, ServiceException;
}
