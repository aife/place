package com.atexo.dume.services.api;

import com.atexo.dume.dto.CriteriaLotDTO;
import com.atexo.dume.dto.ExclusionCriteriaDTO;
import com.atexo.dume.dto.OtherCriteriaDTO;
import com.atexo.dume.dto.SelectionCriteriaDTO;
import com.atexo.dume.model.*;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;

import java.util.List;

/**
 * Interface des Service pour les critérias
 */
public interface CriteriaService {

    /**
     * Service qui permet de récuperer tous les dtos des critéres de selection
     *
     * @return List des CritéresDTO
     */
    List<SelectionCriteriaDTO> getAllSelectionCriteriaDTO();

    /**
     * Service qui permet de récuperer tous les dtos des critéres de exclusion
     *
     * @return List des CritéresDTO
     */
    List<ExclusionCriteriaDTO> getAllExclusionCriteriaDTO();

    /**
     * Service qui permet de récuperer tous les dtos d'autre critéres
     *
     * @return
     */
    List<OtherCriteriaDTO> getAllOtherCriteriaDTO();

    /**
     * This method get all Selection without the criteria ALL_SATISFIED
     *
     * @return List of the SelectionCriteria
     */
    List<SelectionCriteria> getAllSelectionCriteriaWithoutALLCriteria();

    /**
     * This method get all the exclusion criteria
     *
     * @return List of the exclusionCriteria
     */
    List<ExclusionCriteria> getAllExclusionCriteria();

    /**
     * This method will get all the selection criteria by the list code
     * @param codes list of the codes to find
     * @return List of the criteria to be found
     */
    List<SelectionCriteria> getAllSelectionCriteriaByCodeIn(List<String> codes);

    /**
     * This method will get the criteria selection by code
     *
     * @param code of the the criteria
     * @return The matched criteria
     */
    SelectionCriteria getCriteriaSelectionByCode(String code);

    /**
     * This method will get the criteria exclusion by code
     *
     * @param code of the the criteria
     * @return The matched criteria
     */
    ExclusionCriteria getCriteriaExclusionByCode(String code);

    /**
     * This method will get the criteria other by code
     *
     * @param code of the the criteria
     * @return The matched criteria
     */
    OtherCriteria getCriteriaOtherByCode(String code);

    /**
     * Méthode de mise à jour d'un criteriaSelectionDocument à partir du CriteriaLotDTO
     * On fait la mise à jour de la façon suivante :
     * <ul>
     * <li>Création d'un nouveau criteriaSelectionDocument au cas où il y a ajout</li>
     * <li>Mise à jour des lots si le critère existe</li>
     * </ul>
     *
     * @param dumeRequest
     * @param criteriaLotDTO
     * @return
     * @throws DumeResourceNotFoundException
     */
    CriteriaSelectionDocument getCriteriaSelection(DumeRequest dumeRequest, CriteriaLotDTO criteriaLotDTO) throws DumeResourceNotFoundException;

    /**
     * Renvoie les critères de sélection selon la nature de marché de la consultation
     *
     * @param idNatureMarket
     * @return The matched criteria
     */
    List<SelectionCriteriaDTO> getAllSelectionCriteriaDTOByNature(String idNatureMarket);
}
