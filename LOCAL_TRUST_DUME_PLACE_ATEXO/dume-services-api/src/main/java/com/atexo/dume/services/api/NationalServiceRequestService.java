package com.atexo.dume.services.api;

import com.atexo.dume.dto.sn.request.SNECertisDTO;
import com.atexo.dume.dto.sn.request.SNEssentialDataDTO;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.services.exception.NationalServiceException;
import org.springframework.http.ResponseEntity;

/**
 * Contract for all SN services/operation.
 *
 * @author ama
 */
public interface NationalServiceRequestService {

    <T> ResponseEntity<T> restAcheteur(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException;

    <T> ResponseEntity<T> restOE(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException;

    <T> ResponseEntity<T> restMetadonnees(Long idPlatform, SNRequestDTO snRequestDTO, Class clazz) throws NationalServiceException;

    <T> ResponseEntity<T> restEssentialData(Long idPlatform, SNEssentialDataDTO snEssentialDataDTO, Class clazz) throws NationalServiceException;

    <T> ResponseEntity<T> restECertis(Long idPlatform, SNECertisDTO snRequestDTO, Class clazz) throws NationalServiceException;
}
