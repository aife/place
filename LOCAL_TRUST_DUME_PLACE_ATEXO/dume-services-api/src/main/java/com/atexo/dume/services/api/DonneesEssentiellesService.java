package com.atexo.dume.services.api;

import com.atexo.dume.dto.donneesessentielles.RetourStatusContract;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;

/**
 * Interface de gestion de publication des données de contrat MPE auprès du Service National (SN)
 */
public interface DonneesEssentiellesService {

    void publishAllDonneesEssentielles();

    RetourStatusContract checkStatutContrat(String contratNumber) throws DumeResourceNotFoundException;

    RetourStatusContract checkStatutContrat(String contratNumber, String initialContractUuid) throws DumeResourceNotFoundException;
}
