package com.atexo.dume.services.api;

import com.atexo.dume.dto.*;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Regroupe les services liés au DumeResponse
 */

public interface DumeResponseService {

    /**
     * Retourne un DTO construit sur le DumeResponse récupéré en base à l'aide de l'id.
     *
     * @param id L'id du dumeRequest
     * @return Le DTO
     */
    DumeResponseDTO getDumeResponseDTOById(Long id) throws DumeResourceNotFoundException;

    /**
     * Permet de mettre à jour les réponses d'un dume response
     *
     * @param dumeResponseDTO le dume response contenant les réponses à mettre à jour
     * @return le dume response mis à jour
     */
    DumeResponseDTO updateDumeResponseByDTO(DumeResponseDTO dumeResponseDTO) throws DumeResourceNotFoundException;

    /**
     * @param dumeRequestContextDTO
     * @return
     */
    DumeResponseRetourCreationDTO createDumeResponseBy(DumeRequestContextDTO dumeRequestContextDTO) throws IOException, ServiceException, DumeResourceNotFoundException;


    /**
     *  Crée un nouveau contenu d'une réponse à partir d'un numéro SN
     * @param idResponse ID of {@link com.atexo.dume.model.DumeResponse}
     * @param snNumber    SN Number
     * @return
     */
    DumeResponseDTO createDumeResponseFromExisitingSNNumber(Long idResponse, String snNumber) throws ServiceException, DumeResourceNotFoundException;


    /**
     * retourn le contenu XML de ma réponse à partir de l'ID Dume SNResponse
     *
     * @param identifierDumeReponse identifiant du DumeResponse
     * @return le contenu XML selon l'id SNResponse
     */
    String generateDumeResponseXML(Long identifierDumeReponse) throws ServiceException, DumeResourceNotFoundException;

    /**
     * This will check if the dumeOe is valid
     *
     * @param idDumeRequest the identifier of the dume
     * @return Validation status
     */
    RetourStatus validateDumeOE(Long idDumeRequest) throws DumeResourceNotFoundException;

    /**
     * This will method change the state of the dumeOE to A_PUBLIER
     *
     * @param idDumeResponse the identifier of the DumeResponse
     * @return null if the id not found else return the Validation status with status OK
     */
    RetourStatus publishDumeOE(Long idDumeResponse) throws DumeResourceNotFoundException;

    /**
     * Change le statut de plusieurs dume OE à A_PUBLIER dans le cadre d'une publication par groupement.
     * Modifie quelques attributs des dume pour refléter le caractère de groupement.
     *
     * @param publicationGroupementDTO DTO contenant les dumes oe à publier
     * @return RetourStatus avec OK si tout se passe bien
     * @throws DumeResourceNotFoundException
     * @throws ServiceException
     */
    RetourStatus publishDumeOEGroupement(PublicationGroupementDTO publicationGroupementDTO) throws DumeResourceNotFoundException, ServiceException;

    /**
     * This method will return a publication statuts of the DumeResponse with idDumeResponse
     *
     * @param idDumeResponse the identifier of the dumeRequest
     * @return null if not found else Publication status with OK if the state of the Dumerequest is published
     */
    PublicationStatus recupererStatusPublicationDumeOe(Long idDumeResponse) throws DumeResourceNotFoundException;

    /**
     * @param numeroSN numéro du Service national retrieved after publishing a DUME Response
     * @return the PDF file associated to a DUME
     */
    File getPDFFile(String numeroSN) throws DumeResourceNotFoundException;

    /**
     * This method will udpate the dumeOE then request a sn to get the pdf
     *
     * @param dumeResponseDTO the dumeResponseDTO to update and download pdf
     * @return the pdf en byte[]
     * @throws Exception
     */
    byte[] downloadPDFAfterAsaveDumeOE(DumeResponseDTO dumeResponseDTO) throws ServiceException, DumeResourceNotFoundException;

    void publishAllDumeResponses();

    void retrieveAllDumesResponsePDF();

    /**
     * This service will do some checking
     * <ul>
     *     <li>Check if SN is reachable</li>
     *     <li>Check if DumeResponse is publicated</li>
     *     <li>check if DumeResponse is Validated</li>
     * </ul>
     * @param dumeResponseIdentifier {@see Long} the identifier to be checked.
     * @return object of {@see RetourStatus}.
     */
    RetourStatus checkAccessForGivenResponse(final Long dumeResponseIdentifier) throws DumeResourceNotFoundException, ServiceException;

    Long updateMetadonneesAndLot(final DumeRequestContextDTO dumeRequestContextDTO, final Long idDumeResponse) throws DumeResourceNotFoundException;

    /**
     * @param identifierDumeOperateurEconomique identifiant de l'opérateur économique
     * @param snNumber                          numéro du service national
     * @return
     * @throws ServiceException
     */
    List<DumeResponseDTO> findDumeOEFromSN(Long identifierDumeOperateurEconomique, String snNumber) throws ServiceException;

    /**
     *  génère le flux XML d'une DUME OE à partir du numéro SN de la réponse
     * @param snNumber numéro SN
     * @return
     */
    byte[] generateDumeResponseXML(String snNumber) throws DumeResourceNotFoundException, ServiceException;

    /**
     * génère le flux XML d'une DUME OE à partir de l'identifiant de la réponse
     * @param identifierDumeOperateurEconomique identifiant de l'opérateur économique
     * @return
     * @throws DumeResourceNotFoundException
     * @throws ServiceException
     */
    byte[] generateDumeResponseXMLById(Long identifierDumeOperateurEconomique) throws DumeResourceNotFoundException, ServiceException;

    /**
     * Renvoie les preuves e-certis pour un critère donné selon son UUID
     *
     * @param identifierDumeOperateurEconomique identifiant de l'opérateur économique
     * @param criteriaUUID                      UUID du critère {@link com.atexo.dume.model.Criteria}
     * @param countryCode                       code du pays selon le mapping
     * @return
     */
    List<ECertisResponseDTO> getECertisProof(Long identifierDumeOperateurEconomique, String criteriaUUID, String countryCode) throws DumeResourceNotFoundException, ServiceException;
}
