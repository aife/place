package com.atexo.dume.services.api;

import com.atexo.dume.dto.donneesessentielles.xml.MarcheType;
import com.atexo.dume.services.exception.ServiceException;

/**
 * Interface pour la gestion des contrats de données essentielles
 */
public interface ContractService {

    /**
     * Cette methode va interepreter l xml (MarcheType) pour créer/mise à jour un contrat
     *
     * @param marche     the object to be proceeded
     * @param idPlatform the platform  id
     * @throws ServiceException
     */
    void processContract(MarcheType marche, Long idPlatform);
}
