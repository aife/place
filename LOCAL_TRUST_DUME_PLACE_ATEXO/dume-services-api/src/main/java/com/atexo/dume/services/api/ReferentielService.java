package com.atexo.dume.services.api;

import com.atexo.dume.dto.EnumDTO;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface ReferentielService {

    List<EnumDTO> getCountryEnum();

    List<EnumDTO> getCurrencyEnum();

    List<EnumDTO> getMarketEnum();

    Boolean updateDescriptionCriteria(InputStream inputStream) throws IOException;

    Boolean updateDescriptionRequirement(InputStream inputStream);
}
