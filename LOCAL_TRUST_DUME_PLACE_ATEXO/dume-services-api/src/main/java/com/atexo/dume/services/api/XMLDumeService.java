package com.atexo.dume.services.api;

import com.atexo.dume.model.DumeRequest;
import com.atexo.dume.model.DumeResponse;

/**
 * A service for dealing with the XML
 */
public interface XMLDumeService {

    String generateXMLAcheteur(DumeRequest dumeRequest);

    /**
     * this method generate the XML in String format of the dume Response
     *
     * @param dumeResponse to be converted to XML
     * @return the xml in String format
     * @throws Exception
     */
    String generateXMLOE(DumeResponse dumeResponse);
}
