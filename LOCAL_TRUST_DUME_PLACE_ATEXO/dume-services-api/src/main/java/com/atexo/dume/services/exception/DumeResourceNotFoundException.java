package com.atexo.dume.services.exception;

import java.text.MessageFormat;

/**
 * DumeResourceNotFoundException is exeption to handle if the resources is not found
 */
public class DumeResourceNotFoundException extends Exception {

    private static final String NOT_FOUND_MESSAGE = "L''objet {0} ayant pour identifiant {1} n''existe pas";

    public DumeResourceNotFoundException(String resourceType, Long idResource) {
        super(MessageFormat.format(NOT_FOUND_MESSAGE, resourceType, idResource));
    }

    public DumeResourceNotFoundException(String resourceType, String customIdentifier) {
        super(MessageFormat.format(NOT_FOUND_MESSAGE, resourceType, customIdentifier));
    }
}
