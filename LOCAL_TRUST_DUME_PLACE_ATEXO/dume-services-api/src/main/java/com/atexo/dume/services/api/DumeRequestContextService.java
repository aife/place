package com.atexo.dume.services.api;


import com.atexo.dume.dto.DumeRequestContextDTO;
import com.atexo.dume.dto.LotDTO;
import com.atexo.dume.dto.PublicationStatus;
import com.atexo.dume.dto.RetourStatus;
import com.atexo.dume.services.exception.DumeResourceNotFoundException;
import com.atexo.dume.services.exception.ServiceException;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

/**
 * Contract for all {@link DumeRequestContextDTO} services.
 *
 * @author ama
 */
public interface DumeRequestContextService {

    /**
     * This methow will create a DUMERequestContext object .
     * @param contextDTO {@see DumeRequestContextDTO}.
     * @return {@see Long} : internal identifier of the context.
     */
    DumeRequestContextDTO saveContext(final DumeRequestContextDTO contextDTO) throws ServiceException, DumeResourceNotFoundException;

    /**
     * This method will get {@link DumeRequestContextDTO} dumeRequestContextDto by Id
     *
     * @param id is the identifier of {@link com.atexo.dume.model.DumeRequestContext}
     * @return {@link DumeRequestContextDTO} the dto or null if the identifier does'nt exist
     */
    DumeRequestContextDTO getDumeRequestContextById(final Long id) throws DumeResourceNotFoundException;

    /**
     * This method will update the dumeRequestContext only the inherited data and metadata
     *
     * @param dumeRequestContextDTO {@link DumeRequestContextDTO}
     * @param idContext             the identifier of {@link com.atexo.dume.model.DumeRequestContext}
     * @return the identifier of the updated {@link com.atexo.dume.model.DumeRequestContext} or null if the identifier does'nt exist
     */
    Long updateContextWithoutRequest(DumeRequestContextDTO dumeRequestContextDTO, Long idContext) throws DumeResourceNotFoundException;

    /**
     * This method is used the front-end to update the dumeRequestContextDTO {@link com.atexo.dume.model.DumeRequestContext}
     *
     * @param dumeRequestContextDTO is the the dumeRequestContextDTO to update
     * @return the updateDTO or null if the identifier does'nt exist
     */
    DumeRequestContextDTO updateDumeContextFromDTO(final @NotNull DumeRequestContextDTO dumeRequestContextDTO) throws ServiceException, DumeResourceNotFoundException;

    /**
     * This method will delete the DumeRequestContext
     *
     * @param idContext is the identifier to delete
     * @return true if the identifier exist and it has been deleted or false the identifier does'nt exist
     */
    Boolean deleteContextDumeRequest(Long idContext) throws DumeResourceNotFoundException;

    /**
     * This method will get all the dumeRequestContext created
     *
     * @return List of the dumeRequestContextDto
     */

    List<DumeRequestContextDTO> getAllDumeRequestContext();

    /**
     * This method will check if all DumeRequest associated to DumeContexteRequest are confirmed
     *
     * @param idContextDume the identifier of the DumeContexteRequest
     * @return The validation status
     */
    RetourStatus checkValidContextDume(Long idContextDume) throws DumeResourceNotFoundException;

    /**
     * this will change the state of all dumeAcheteurs to a state A_PUBLIER
     *
     * @param idContexteDume the identifier of the DumeContext
     * @return null if the identifier is not found else the RetourStatus with status OK
     */
    RetourStatus publishAllDumeRequest(Long idContexteDume) throws DumeResourceNotFoundException, ServiceException;

    /**
     * This will get the publication status of the context
     * if all the dumeRequest associeted are published then this will return Statut Ok with the list
     * the numberSn associetd else it will return NON_PUBLIE  statut
     *
     * @param idContexteDume the identifier
     * @return
     */
    PublicationStatus recupererStatusPublicationDumeAcheteur(Long idContexteDume) throws DumeResourceNotFoundException;

    /**
     * This method will update the DLRO and change the status into DLRO_MODIFIEE
     *
     * @param idContexteDumeAcheteur the identifier of the context
     * @param dlro                   new Data
     * @throws DumeResourceNotFoundException if the context not found
     * @returntrue if everything is Fine
     */
    Boolean updateDLRO(Long idContexteDumeAcheteur, ZonedDateTime dlro) throws DumeResourceNotFoundException;

    /**
     * This method will return all ids of the DUME OE affected by the updated
     *
     * @param idRequestContext
     * @return
     */
    RetourStatus updateRequestContextAfterPublication(Long idRequestContext) throws DumeResourceNotFoundException, ServiceException;

    /**
     *  This method check if there is a need to have purge for the Criteria lot relation
     * @param idRequestContext the idContext
     * @param lotDTOS new Lot
     * @return
     * @throws DumeResourceNotFoundException
     */
    RetourStatus checkPurgeCriteriaLot(Long idRequestContext, Set<LotDTO> lotDTOS) throws DumeResourceNotFoundException;

    /**
     * Sends the flow corresponding to a {@link com.atexo.dume.model.DumeRequestContext} by retrieving it directly from the SN
     *
     * @param identifierDumeRequest
     * @return
     */
    byte[] downloadPdf(Long identifierDumeRequest) throws DumeResourceNotFoundException, ServiceException;
}
