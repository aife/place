package com.atexo.dume.mocks.mpe;

import com.atexo.dume.dto.donneesessentielles.xml.TicketType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.UUID;

@Controller
public class MPEController {

    private static final String FORMAT_ETENDU_END_POINT = "/donnees-essentielles/contrat/format-etendu";

    @Value("classpath:donneesEssentielles/xml_format_etendu.xml")
    private Resource formatEtendu;

    @GetMapping(value = "/api.php/ws/authentification/connexion/" + "/{login}/{password}", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<TicketType> connect(@PathVariable String login, @PathVariable String password) {
        TicketType tokenBean = new TicketType();
        tokenBean.setTicket(UUID.randomUUID().toString());
        return new ResponseEntity<>(tokenBean, HttpStatus.OK);
    }

    @PostMapping(value = "app.php/api/v1" + FORMAT_ETENDU_END_POINT, consumes = {"multipart/form-data", "application/x-www-form-urlencoded"}, produces = MediaType.APPLICATION_XML_VALUE)
    ResponseEntity<Resource> getDonneesEssentielles(@ModelAttribute Object form) {
        return new ResponseEntity<>(formatEtendu, HttpStatus.OK);
    }
}
