package com.atexo.dume.mocks;

import com.github.tomakehurst.wiremock.WireMockServer;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class MpeMock {

    public static void main(String[] args) {
        final WireMockServer wireMockServer = new WireMockServer(options().port(8080).withRootDirectory("dume-mocks/src/main/resources/")
        );
        wireMockServer.start();
        wireMockServer.addStubMapping(stubFor(get(urlEqualTo("/mpe"))
                .willReturn(aResponse().withStatus(200)
                        .withHeader("Access-Control-Allow-Origin","*")
                        .withHeader("Access-Control-Allow-Headers","Accept, Content-Type, Content-Encoding, Server, Transfer-Encoding")
                        .withHeader("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTIONS")
                        .withBodyFile("main.html"))));
    }
}
