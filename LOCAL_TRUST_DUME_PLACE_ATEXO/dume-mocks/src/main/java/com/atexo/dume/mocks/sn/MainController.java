package com.atexo.dume.mocks.sn;

import com.atexo.dume.dto.sn.request.SNCritereDumeOE;
import com.atexo.dume.dto.sn.request.SNRequestDTO;
import com.atexo.dume.dto.sn.response.*;
import com.atexo.dume.toolbox.rest.NationalServiceOperations;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.atexo.dume.toolbox.consts.NationalServiceFormatEnum.DUME_PDF;
import static com.atexo.dume.toolbox.rest.NationalServiceOperations.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("dume/")
public class MainController {

    @Value("classpath:request/espd-request.pdf")
    private Resource pdfFileReq;

    @Value("classpath:response/espd-response.pdf")
    private Resource pdfFileResp;


    @Value("classpath:response/espd-response.xml")
    private Resource dumeOeXmlResp;

    @Value("classpath:ecertis/proof.json")
    private Resource eCertisProofResp;

    @RequestMapping(value = "/dumeA", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> dumeA(@RequestBody SNRequestDTO req) throws IOException {
        return dummy(req, pdfFileReq, null, false);
    }


    @RequestMapping(value = "/dumeOE", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> dumeOe(@RequestBody SNRequestDTO req) throws IOException {
        return dummy(req, pdfFileResp, dumeOeXmlResp, true);
    }

    @RequestMapping(value = "/ecertis", method = POST, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Object> ecertis(@RequestBody SNRequestDTO req) throws IOException {
        if ("recupererEvidence".equalsIgnoreCase(req.getOperation())) {
            return new ResponseEntity<>(eCertisProofResp, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<Object> dummy(SNRequestDTO snRequestDTO, Resource file, Resource xml, boolean isOe) throws IOException {

        if (snRequestDTO.getOperation().equalsIgnoreCase(NationalServiceOperations.SAVE_DUME)) {
            SNDumeSavedResponseDTO saved = new SNDumeSavedResponseDTO();
            saved.setResponse(new SNResponse());
            if (!isOe) {
                saved.getResponse().setIdDume(UUID.randomUUID().toString());
            } else {

                saved.getResponse().setDumeOE(UUID.randomUUID().toString());
            }
            return new ResponseEntity(saved, HttpStatus.OK);
        } else if (snRequestDTO.getOperation().equalsIgnoreCase(GET_FORMATED_DUME)) {
            SNDumeSavedResponseDTO responseDTO = new SNDumeSavedResponseDTO();
            responseDTO.setResponse(new SNResponse());
            if (isOe) {
                if (DUME_PDF.equalsIgnoreCase(snRequestDTO.getFormat()))
                    responseDTO.getResponse().setDumeOE(new String(Base64Utils.encode(IOUtils.toByteArray(file.getInputStream()))));
                else
                    // XML
                    responseDTO.getResponse().setDumeOE(new String(Base64Utils.encode(IOUtils.toByteArray(xml.getInputStream()))));
            } else {
                responseDTO.getResponse().setDumeA(new String(Base64Utils.encode(IOUtils.toByteArray(file.getInputStream()))));
            }

            return new ResponseEntity(responseDTO, HttpStatus.OK);
        } else if (snRequestDTO.getOperation().equalsIgnoreCase(MERGE_DUMES_WITH_ENTREPRISE_DATA)) {
            SNDumeFusionResponseDTO responseDTO = new SNDumeFusionResponseDTO();
            responseDTO.setResponse(new SNFusionResponse());
            SNDumeOEResponse snDumeOEResponse = new SNDumeOEResponse();
            snDumeOEResponse.setXmlDume(new String(Base64Utils.encode(IOUtils.toByteArray(xml.getInputStream()))));
            responseDTO.getResponse().setDumeOE(snDumeOEResponse);
            return new ResponseEntity(responseDTO, HttpStatus.OK);
        } else if (snRequestDTO.getOperation().equalsIgnoreCase(SEARCH_DUMES)) {
            SNSearchResponseDTO responseDTO = new SNSearchResponseDTO();
            responseDTO.setResponse(new SNResponse());
            Map<String, SNSearchOEResponse> map = new HashMap<>();
            if (snRequestDTO.getDumesOE() != null) for (SNCritereDumeOE snDumeOE : snRequestDTO.getDumesOE()) {
                String idDume = snDumeOE.getIdDume();
                String refConsultation = "REF-" + UUID.randomUUID();
                SNDumeOEResponse dumeOEResponse = new SNDumeOEResponse();
                SNConsultationResponse consultationResponse = new SNConsultationResponse();
                dumeOEResponse.setAccessible(true);
                dumeOEResponse.setDumeOEId(idDume);

                consultationResponse.setReferenceFonctionnelle(refConsultation);

                SNSearchOEResponse snSearchOEResponse = new SNSearchOEResponse();
                snSearchOEResponse.setDumeOEResponse(dumeOEResponse);
                snSearchOEResponse.setConsultation(consultationResponse);
                map.put(UUID.randomUUID().toString(), snSearchOEResponse);
            }
            responseDTO.getResponse().setResponsesRecherche(map);
            return new ResponseEntity(responseDTO, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
