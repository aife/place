# Build

# Dépendances

Les logiciels suivant doivent être disponibles sur le poste réalisant l'opération de build : 

- Maven
- Java 6
- node
- npm
- yarn

# Compilation

La commande doit être lancée à la racine du dossier du module : 

- `mvn clean install -DskipTests`