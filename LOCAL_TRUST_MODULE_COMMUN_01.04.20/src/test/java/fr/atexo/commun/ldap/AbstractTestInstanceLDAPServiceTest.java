/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Pour tester l'instance "Test".
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public abstract class AbstractTestInstanceLDAPServiceTest extends AbstractLDAPServiceTest {

    protected int getNombreTotalIdentifiants() {
        return 5;
    }

    protected String getLoginIdentifiantExistant() {
        return "joe";
    }

    protected String[] getProfilsIdentifiantsAvecProfils() {
        return new String[]{"cto", "ceo"};
    }

    protected int getNombreIdentifiantsAvecProfils() {
        return 2;
    }

    protected String[] getProfilsIdentifiantsAvecProfils2() {
        return new String[]{"ceo"};
    }

    protected int getNombreIdentifiantsAvecProfils2() {
        return 1;
    }

    protected String getNomAttribut() {
        return "mail";
    }

    protected String getValeurAttribut() {
        return "joe.pentaho@pentaho.org";
    }

    protected String[] getProfilsIdentifiantExistant() {
        return new String[]{"ceo", "Admin"};
    }

    protected int getNombreProfilsIdendifiantExistant() {
        return 2;
    }
}
