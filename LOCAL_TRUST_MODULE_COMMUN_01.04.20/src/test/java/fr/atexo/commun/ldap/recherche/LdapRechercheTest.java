package fr.atexo.commun.ldap.recherche;

import fr.atexo.commun.ldap.*;
import fr.atexo.commun.ldap.securite.recherche.RechercheLdapGenerique;
import fr.atexo.commun.ldap.securite.recherche.RechercheLdapParametresFactoryImpl;
import fr.atexo.commun.ldap.securite.transforme.RechercheResultatUnique;
import fr.atexo.commun.ldap.securite.transforme.RechercheResultatsMultiples;
import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;

import javax.naming.directory.SearchControls;
import java.util.List;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
@Ignore
public class LdapRechercheTest extends TestCase {

    private String url = "ldap://linuxserv3:389/";
    private String utilisateurConnexion = "cn=Manager,dc=fr,dc=atexo";
    private String motDePasseConnexion = "secret";
    private String utilisateurBase = "ou=Services,dc=ad_vdn,dc=fr,dc=atexo";
    private String utilisateurBaseFiltre = "(&(objectClass=inetOrgPerson)(businessCategory=*,ou=Parapheur,ou=Roles,dc=ad_vdn,dc=fr,dc=atexo))";
    private String utilisateurRoleBase = "ou=Parapheur,ou=Roles,dc=ad_vdn,dc=fr,dc=atexo";
    private String utilisateurRoleAttribute = "uid";
    private String[] utilisateurRoleAttributes = {"cn", "uid", "displayName", "givenName", "homePhone", "mail"};
    private String utilisateurRoleAttributesSplit = "cn|uid|displayName|givenName|homePhone|mail|businessCategory";
    private String utilisateurRoleAttributesSeparateur = "|";

    @Test
    public void testRechercherTousLesUtilisateursUnique() throws Exception {

        DefaultSpringSecurityContextSource defaultSpringSecurityContextSource = new DefaultSpringSecurityContextSource(url);
        defaultSpringSecurityContextSource.setUserDn(utilisateurConnexion);
        defaultSpringSecurityContextSource.setPassword(motDePasseConnexion);
        defaultSpringSecurityContextSource.afterPropertiesSet();

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(2);

        RechercheLdapParametresFactoryImpl ldapSearchParamsFactory = new RechercheLdapParametresFactoryImpl(utilisateurBase, utilisateurBaseFiltre, searchControls);

        RechercheResultatUnique rechercheResultatEnListeAttributValeur = new RechercheResultatUnique(utilisateurRoleAttribute);

        RechercheLdapGenerique rechercheLdapGenerique = new RechercheLdapGenerique(defaultSpringSecurityContextSource, ldapSearchParamsFactory, rechercheResultatEnListeAttributValeur);

        List list = rechercheLdapGenerique.recherche(new Object[0]);

        assertTrue(list != null && !list.isEmpty());

    }

    @Test
    public void testRechercherTousLesUtilisateursMultiple() throws Exception {

        DefaultSpringSecurityContextSource defaultSpringSecurityContextSource = new DefaultSpringSecurityContextSource(url);
        defaultSpringSecurityContextSource.setUserDn(utilisateurConnexion);
        defaultSpringSecurityContextSource.setPassword(motDePasseConnexion);
        defaultSpringSecurityContextSource.afterPropertiesSet();

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(2);

        RechercheLdapParametresFactoryImpl ldapSearchParamsFactory = new RechercheLdapParametresFactoryImpl(utilisateurBase, utilisateurBaseFiltre, searchControls);

        RechercheResultatsMultiples rechercheResultatsMultiples = new RechercheResultatsMultiples(utilisateurRoleAttributes);

        RechercheLdapGenerique rechercheLdapGenerique = new RechercheLdapGenerique(defaultSpringSecurityContextSource, ldapSearchParamsFactory, rechercheResultatsMultiples);

        List list = rechercheLdapGenerique.recherche(new Object[0]);

        assertTrue(list != null && !list.isEmpty());

    }

    @Test
    public void testRechercherTousLesUtilisateursMultipleAvecSeparateur() throws Exception {

        DefaultSpringSecurityContextSource defaultSpringSecurityContextSource = new DefaultSpringSecurityContextSource(url);
        defaultSpringSecurityContextSource.setUserDn(utilisateurConnexion);
        defaultSpringSecurityContextSource.setPassword(motDePasseConnexion);
        defaultSpringSecurityContextSource.afterPropertiesSet();

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(2);

        RechercheLdapParametresFactoryImpl ldapSearchParamsFactory = new RechercheLdapParametresFactoryImpl(utilisateurBase, utilisateurBaseFiltre, searchControls);

        RechercheResultatsMultiples rechercheResultatsMultiples = new RechercheResultatsMultiples(utilisateurRoleAttributesSplit, utilisateurRoleAttributesSeparateur);

        RechercheLdapGenerique rechercheLdapGenerique = new RechercheLdapGenerique(defaultSpringSecurityContextSource, ldapSearchParamsFactory, rechercheResultatsMultiples);

        List list = rechercheLdapGenerique.recherche(new Object[0]);

        assertTrue(list != null && !list.isEmpty());

    }

    @Test
    public void testRechercher() {

        LDAPParametresConnexion parametresConnexion = new LDAPParametresConnexion(url, utilisateurConnexion, motDePasseConnexion);

        LDAPParametresCompte parametresCompte = new LDAPParametresCompte(utilisateurBase, utilisateurRoleBase);
        parametresCompte.setChampRoleObjetCompte("businessCategory");
        LDAPServiceImpl ldapService = new LDAPServiceImpl(parametresConnexion, parametresCompte);
        List<IdentifiantCompte> identifiantComptes = ldapService.getIdentifiants(utilisateurBaseFiltre);

        assertTrue(identifiantComptes != null && !identifiantComptes.isEmpty());

        for (IdentifiantCompte identifiantCompte : identifiantComptes) {

            DetailsCompte detailsCompte = ldapService.getDetailsCompte(identifiantCompte, utilisateurRoleAttributes);
            assertTrue(detailsCompte != null);
        }

    }

    @Test
    public void testRechercherUtilisateurSpecifique() {

        String cnFiltre = "(cn=Landry Minoza)";

        LDAPParametresConnexion parametresConnexion = new LDAPParametresConnexion("ldap://linuxserv3:389", utilisateurConnexion, motDePasseConnexion);

        LDAPParametresCompte parametresCompte = new LDAPParametresCompte(utilisateurBase, utilisateurRoleBase);
        parametresCompte.setChampRoleObjetCompte("businessCategory");
        LDAPServiceImpl ldapService = new LDAPServiceImpl(parametresConnexion, parametresCompte);
        IdentifiantCompte identifiantCompte = ldapService.getIdentifiant(cnFiltre);

        assertTrue(identifiantCompte != null);

    }

    @Test
    public void testModifierCompte() {

        LDAPParametresConnexion parametresConnexion = new LDAPParametresConnexion(url, utilisateurConnexion, motDePasseConnexion);

        parametresConnexion.setAd(true);

        LDAPParametresCompte parametresCompte = new LDAPParametresCompte("ou=Services,dc=ad_nca,dc=fr,dc=atexo", "ou=Roles,dc=ad_nca,dc=fr,dc=atexo");
        LDAPServiceImpl ldapService = new LDAPServiceImpl(parametresConnexion, parametresCompte);

        try {
            ldapService.modifierCompte(new IdentifiantCompte(ldapService.getParametresCompte(), "user"),"password",new AttributLDAP[]{new AttributLDAP("sn","user")});
        } catch (Exception e) {
            assertTrue(false);
        }
        assertTrue(true);
    }
}
