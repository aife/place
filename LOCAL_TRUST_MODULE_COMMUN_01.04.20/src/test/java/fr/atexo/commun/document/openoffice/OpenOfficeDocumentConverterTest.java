package fr.atexo.commun.document.openoffice;

import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeInjoignableException;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeRelanceException;
import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import junit.framework.TestCase;
import org.artofsolving.jodconverter.document.DefaultDocumentFormatRegistry;
import org.artofsolving.jodconverter.document.DocumentFormat;
import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;

/**
 * Classe de test pour la conversion de documents.
 * 
 * @author Louis Champion
 * @version $Revision$ $Date: 2013-02-18 15:11:17 +0100 (lun., 18 févr. 2013) $
 */

@Ignore
public class OpenOfficeDocumentConverterTest extends TestCase {

	private boolean convertirDocument(File documentSource, File documentDestination) throws FileNotFoundException, ConnectException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		DefaultDocumentFormatRegistry defaultDocumentFormatRegistry = new DefaultDocumentFormatRegistry();
		DocumentFormat documentDestinationFormat = defaultDocumentFormatRegistry.getFormatByExtension("pdf");

		OpenOfficeDocumentConverteur openOfficeDocumentConverteur = new OpenOfficeDocumentConverteur(OfficeConnectionProtocol.SOCKET, true, 8100);

		openOfficeDocumentConverteur.convertirDocument(documentSource, documentDestination, documentDestinationFormat);

		return documentDestination != null && documentDestination.exists() && documentDestination.length() > 0;
	}

	@Test
	public void testConvertirDoc() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/document.doc");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("document_doc_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirOdt() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/document.odt");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("document_odt_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirPpt() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/powerpoint.ppt");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("powerpoint_ppt_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirOdp() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/powerpoint.odp");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("powerpoint_odp_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirOds() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/excel.ods");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("excel_ods_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirXls() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/excel.xls");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("excel_xls_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirHtml() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/html.html");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("html_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirHtmlComplexe() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/htmlComplex.html");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("html_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirPng() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/imgpng.png");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("png_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

	@Test
	public void testConvertirJpg() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/imgjpg.jpg");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("jpg_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}


	@Test
	public void testConvertirJpeg() throws IOException, ServiceOpenOfficeRelanceException, ServiceOpenOfficeInjoignableException {

		URL urlDocumentSource = ClasspathUtil.searchURL("fr/atexo/commun/document/openoffice/TMC.jpeg");
		File documentSource = new File(urlDocumentSource.getPath());

		File fichierDestination = FichierTemporaireUtil.creerFichierTemporaire("jpg_", ".pdf");

		boolean resultat = convertirDocument(documentSource, fichierDestination);

		assertTrue(resultat);
		assertTrue(fichierDestination.exists() && fichierDestination.isFile());

		fichierDestination.delete();
		assertTrue(!fichierDestination.exists());
	}

}
