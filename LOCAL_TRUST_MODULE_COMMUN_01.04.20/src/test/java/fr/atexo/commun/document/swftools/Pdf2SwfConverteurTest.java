package fr.atexo.commun.document.swftools;

import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import fr.atexo.commun.util.PropertiesUtil;
import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * Classe de test pour la conversion pour l'execution de commandes systèmes pdf2swf.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
@Ignore
public class Pdf2SwfConverteurTest extends TestCase {

    private static String CLEF_COMMANDE = "pdf2swf.command";
    private static String CLEF_FLASH_VERSION = "flash.version";

    @Test
    public void testConvertir() throws IOException {

        InputStream fichierProprietes = ClasspathUtil.searchResource("fr/atexo/commun/document/swftools/pdf2swf.properties");
        Properties proprietes = PropertiesUtil.getProprietes(fichierProprietes);

        URL urlSource = ClasspathUtil.searchURL("fr/atexo/commun/util/exec/pdf.pdf");
        File fichierSource = null;
        try {
            fichierSource = new File(urlSource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fichierSource = new File(urlSource.getPath());
        }

        File fichierDestination = new File(FichierTemporaireUtil.getRepertoireTemporaire() + "/pdf.swf");

        Pdf2SwfConfiguration pdf2SwfConfiguration = new Pdf2SwfConfiguration(proprietes.getProperty(CLEF_COMMANDE), Integer.valueOf(proprietes.getProperty(CLEF_FLASH_VERSION)));

        Pdf2SwfConverteur.convertir(pdf2SwfConfiguration, fichierSource, fichierDestination);

        assertTrue(fichierDestination.exists());

        fichierDestination.delete();
        assertTrue(!fichierDestination.exists());

    }


}
