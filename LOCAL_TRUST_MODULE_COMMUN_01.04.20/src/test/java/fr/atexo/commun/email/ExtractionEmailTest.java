package fr.atexo.commun.email;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import fr.atexo.commun.email.extraction.EmailContenu;
import fr.atexo.commun.email.extraction.EmailExtractionUtil;
import fr.atexo.commun.email.extraction.EmailProtocolType;
import fr.atexo.commun.util.FichierTemporaireUtil;
import junit.framework.TestCase;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
//@Ignore("Besoin d'une boite email valide...")
public class ExtractionEmailTest extends TestCase {

    private String urlServeurPop3 = "pop.gmail.com";
    private String urlServeurImap = "imap.gmail.com";
    private String email = "test.courrier@atexo.com";
    private String motDePasse = "test.courrier";

    static {
        EmailExtractionUtil.setProprieteSysteme(EmailExtractionUtil.MAIL_MIME_DECODE_TEXT_STRICT, false);
        EmailExtractionUtil.setProprieteSysteme(EmailExtractionUtil.MAIL_MIME_ENCODE_FILENAME, true);
        EmailExtractionUtil.setProprieteSysteme(EmailExtractionUtil.MAIL_MIME_DECODE_FILENAME, true);
    }

    @Test
    public void testConnectionImapGmail() {
        testConnectionGmail(EmailProtocolType.IMAPS, urlServeurImap);
    }

    @Test
    public void testConnectionPop3Gmail() {
        testConnectionGmail(EmailProtocolType.POP3S, urlServeurPop3);
    }

    public void testConnectionGmail(EmailProtocolType emailProtocolType, String urlServeur) {

        EmailExtractionUtil util = new EmailExtractionUtil(emailProtocolType, email, motDePasse);

        try {
            util.connecter(urlServeur);
            util.ouvrirRepertoire("Inbox", false);
            Message[] messages = util.getMessages();
            util.recupererInformationsComplementaires(messages);

            if (messages != null && messages.length > 0) {

                for (int i = 0; i < messages.length; i++) {

                    System.out.println("---------------------- NOUVEAU MESSAGE ------------------------------");

                    Message message = messages[i];

                    EmailContenu emailContenu = util.recupererEmailContenu(message, true);

                    System.out.print("Expediteur(s) : ");
                    for (String address : emailContenu.getExpediteurs()) {
                        System.out.print(address);
                    }
                    System.out.println("");

                    System.out.print("Destinataire(s) : ");
                    for (String address : emailContenu.getDestinataires()) {
                        System.out.print(address);
                    }
                    System.out.println("");

                    System.out.println("Objet : " + emailContenu.getSujet());
                    System.out.println("Date envoi : " + emailContenu.getDateEnvoi());
                    System.out.println("Date reception : " + emailContenu.getDateReception());
                    if (emailContenu.getCorpTexte() != null) {
                        try {
                            System.out.println("Corp Message (Texte) : " + (String) emailContenu.getCorpTexte().getContent());
                        } catch (Exception e) {
                        }
                    }
                    if (emailContenu.getCorpHtml() != null) {
                        try {
                            System.out.println("Corp Message (HTML) : " + (String) emailContenu.getCorpTexte().getContent());
                        } catch (Exception e) {
                        }
                    }

                    for (List<Part> parts : emailContenu.getMapPiecesJointes().values()) {

                        for (Part part : parts) {

                            File file = FichierTemporaireUtil.creerFichierTemporaire("email_piecejointe_", ".tmp");
                            FileUtils.copyInputStreamToFile(part.getInputStream(), file);

                        }

                    }
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
