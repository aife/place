package fr.atexo.commun.email;

import java.io.Serializable;
import java.util.Date;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class EmailBean implements Serializable {

        private String utilisateurPrenom;
        private String utilisateurNom;
        private Date dateCreation;
        private String expediteurCivilite;
        private String expediteurPrenom;
        private String expediteurNom;

        public EmailBean(String utilisateurPrenom, String utilisateurNom, Date dateCreation, String expediteurCivilite, String expediteurPrenom, String expediteurNom) {
            this.utilisateurPrenom = utilisateurPrenom;
            this.utilisateurNom = utilisateurNom;
            this.dateCreation = dateCreation;
            this.expediteurCivilite = expediteurCivilite;
            this.expediteurPrenom = expediteurPrenom;
            this.expediteurNom = expediteurNom;
        }

        public String getUtilisateurPrenom() {
            return utilisateurPrenom;
        }

        public void setUtilisateurPrenom(String utilisateurPrenom) {
            this.utilisateurPrenom = utilisateurPrenom;
        }

        public String getUtilisateurNom() {
            return utilisateurNom;
        }

        public void setUtilisateurNom(String utilisateurNom) {
            this.utilisateurNom = utilisateurNom;
        }

        public Date getDateCreation() {
            return dateCreation;
        }

        public void setDateCreation(Date dateCreation) {
            this.dateCreation = dateCreation;
        }

        public String getExpediteurCivilite() {
            return expediteurCivilite;
        }

        public void setExpediteurCivilite(String expediteurCivilite) {
            this.expediteurCivilite = expediteurCivilite;
        }

        public String getExpediteurPrenom() {
            return expediteurPrenom;
        }

        public void setExpediteurPrenom(String expediteurPrenom) {
            this.expediteurPrenom = expediteurPrenom;
        }

        public String getExpediteurNom() {
            return expediteurNom;
        }

        public void setExpediteurNom(String expediteurNom) {
            this.expediteurNom = expediteurNom;
        }
}
