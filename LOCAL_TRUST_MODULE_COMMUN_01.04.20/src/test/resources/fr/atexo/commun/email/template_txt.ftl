Fichier template_txt.ftl.
Bonjour ${email.utilisateurPrenom} ${email.utilisateurNom},

Voici un exemple de contenu de mail daté du ${email.dateCreation?date}.

Cordialement,
${email.expediteurCivilite} ${email.expediteurPrenom} ${email.expediteurNom}