package fr.atexo.commun.image;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;

import com.drew.metadata.exif.ExifIFD0Directory;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.imageio.*;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by MZO on 28/04/2016.
 */
public class ImageUtils {

    /**
     * Méthode qui détérmine la résolution en DPI d'une image
     *
     * @param bytes
     * @return
     */
    public static double[] getImageResolutionInDPI(byte[] bytes) {
        double[] result = null;

        ByteArrayInputStream inputStream = null;
        try {
            // on essaie avec les balises EXIF
            inputStream = new ByteArrayInputStream(bytes);
            try {
                Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(inputStream));
                if (metadata != null) {
                    Directory exifIFD0Directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
                    if (exifIFD0Directory != null) {
                        double xResolution = exifIFD0Directory.getDouble(ExifIFD0Directory.TAG_X_RESOLUTION);
                        double yResolution = exifIFD0Directory.getDouble(ExifIFD0Directory.TAG_Y_RESOLUTION);
                        return new double[]{xResolution, yResolution};
                    }
                }
            } catch (Exception e) {}

            inputStream.reset();

            // on essaie avec JAVA Image IO ( pour les images sans tag EXIF )
            ImageInputStream iis = ImageIO.createImageInputStream(inputStream);
            ImageReader imageReader = ImageIO.getImageReaders(iis).next();

            if (imageReader != null) {
                imageReader.setInput(iis);
                double hdpi = 96, vdpi = 96;
                double mm2inch = 25.4;

                NodeList lst;
                Element node = (Element) imageReader.getImageMetadata(0).getAsTree("javax_imageio_1.0");
                lst = node.getElementsByTagName("HorizontalPixelSize");
                if (lst != null && lst.getLength() == 1) {
                    hdpi =  (mm2inch / Float.parseFloat(((Element) lst.item(0)).getAttribute("value")));
                }

                lst = node.getElementsByTagName("VerticalPixelSize");
                if (lst != null && lst.getLength() == 1) {
                    vdpi = (mm2inch / Float.parseFloat(((Element) lst.item(0)).getAttribute("value")));
                }
                return new double[]{hdpi, vdpi};
            }

        } catch (Exception e) {
            return null;
        } finally {
            IOUtils.closeQuietly(inputStream);
        }

        return null;
    }

    /**
     * Méthode qui enregistre un flux png avec les métadonnées DPI
     *
     * @param bufferedImage
     * @param xResolution
     * @param yResolution
     * @param outputStream
     * @throws IOException
     */
    public static void writePng(BufferedImage bufferedImage, double xResolution, double yResolution, OutputStream outputStream) throws IOException {

        ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();

        ImageWriteParam writeParam = writer.getDefaultWriteParam();

        ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
        IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);

        if (metadata.isReadOnly() || !metadata.isStandardMetadataFormatSupported()) {
            return;
        }

        double dotsPerMilliX = 1.0 * xResolution / 10 / 2.54;
        double dotsPerMilliY = 1.0 * yResolution / 10 / 2.54;

        IIOMetadataNode horiz = new IIOMetadataNode("HorizontalPixelSize");
        horiz.setAttribute("value", Double.toString(dotsPerMilliX));

        IIOMetadataNode vert = new IIOMetadataNode("VerticalPixelSize");
        vert.setAttribute("value", Double.toString(dotsPerMilliY));

        IIOMetadataNode dim = new IIOMetadataNode("Dimension");
        dim.appendChild(horiz);
        dim.appendChild(vert);

        IIOMetadataNode root = new IIOMetadataNode("javax_imageio_1.0");
        root.appendChild(dim);

        metadata.mergeTree("javax_imageio_1.0", root);

        ImageOutputStream stream = ImageIO.createImageOutputStream(outputStream);

        writer.setOutput(stream);

        writer.write(metadata, new IIOImage(bufferedImage, null, metadata), writeParam);
    }
}
