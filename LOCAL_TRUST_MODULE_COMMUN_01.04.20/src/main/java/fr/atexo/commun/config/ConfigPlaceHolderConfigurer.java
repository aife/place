package fr.atexo.commun.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.List;
import java.util.Properties;

/**
 * Cette classe permet de remplacer les variables declarés dans spring via l'instruction ${mavariable}.
 * cette variable sera externalisé dans un fichier properties
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ConfigPlaceHolderConfigurer extends PropertyPlaceholderConfigurer {

    /**
     * Messages de log.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ConfigPlaceHolderConfigurer.class);

    private String path;
    
    private Properties props;

    /**
     * Liste des noms de fichier de configuration.
     */
    private List<String> filename;

    /**
     * Chargement des proprietes de configuration depuis les fichiers de configuration.
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory valeur)
            throws BeansException {
        // Creation du conteneur des proprietes de configuration
        Config.init(path, filename);
        props = Config.getProperties();
        if (props != null) {
            Properties[] propertiesArray = new Properties[1];
            propertiesArray[0] = props;
            setPropertiesArray(propertiesArray);
        }
        super.postProcessBeanFactory(valeur);
    }
    
    public String getValeur(String cle) {
        return props.getProperty(cle);
    }

    /**
     * @param filename Liste des noms de fichier de configuration.
     */
    public void setFilename(List<String> filename) {
        this.filename = filename;
    }

    /**
     *
     * @param path le chemin spécifique dans le classpath externalisé où chercher le fichier
     */
    public void setPath(String path) {
        this.path = path;
    }
}
