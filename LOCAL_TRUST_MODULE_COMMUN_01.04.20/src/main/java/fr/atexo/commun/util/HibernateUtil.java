package fr.atexo.commun.util;

import org.hibernate.proxy.HibernateProxy;

/**
 * Classe utilitaire pour hibernate.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class HibernateUtil {

    private HibernateUtil() {

    }

    /**
     *  Permet de récupérer un entity bean à partir de sa version proxy.
     *
     * @param proxy
     * @param <T>
     * @return
     */
    public static <T> T getProxiedObject(T proxy) {
        if (proxy instanceof HibernateProxy) {
            return (T) ((HibernateProxy) proxy).getHibernateLazyInitializer().getImplementation();
        }
        return proxy;
    }
}
