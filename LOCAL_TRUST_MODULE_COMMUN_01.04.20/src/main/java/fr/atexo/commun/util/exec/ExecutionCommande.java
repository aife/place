package fr.atexo.commun.util.exec;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import fr.atexo.commun.exception.AtexoRuntimeException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This acts as a session similar to the <code>java.lang.Process</code>, but
 * logs the system standard and error streams.
 * <p>
 * The bean can be configured to execute a commandes directly, or be given a map
 * of commands keyed by the <i>os.name</i> Java system property.  In this map,
 * the default key that is used when no match is found is the
 * <b>{@link #KEY_OS_DEFAULT *}</b> key.
 * <p>
 * Use the {@link #setProprietesRepertoire(String) processDirectory} property to change the default location
 * from which the commandes executes.  The process's environment can be configured using the
 * {@link #setProprietesProcessus(java.util.Map) processProperties} property.
 * <p>
 * Commands may use placeholders, e.g.
 * <pre><code>
 *    find
 *    -name
 *    ${filename}
 * </code></pre>
 * The <b>filename</b> property will be substituted for any supplied value prior to
 * each execution of the commandes.  Currently, no checks are made to get or check the
 * properties contained within the commandes string.  It is up to the client code to
 * dynamically extract the properties required if the required properties are not
 * known up front.
 * <p>
 * Sometimes, a variable may contain several arguments.  .  In this case, the arguments
 * need to be tokenized using a standard <tt>StringTokenizer</tt>.  To force tokenization
 * of a value, use:
 * <pre><code>
 *    SPLIT:${userArgs}
 * </code></pre>
 * You should not use this just to split up arguments that are known to require tokenization
 * up front.  The <b>SPLIT:</b> directive works for the entire argument and will not do anything
 * if it is not at the beginning of the argument.  Do not use <b>SPLIT:</b> to break up arguments
 * that are fixed, so avoid doing this:
 * <pre><code>
 *    SPLIT:ls -lih
 * </code></pre>
 * Instead, break the commandes up explicitly:
 * <pre><code>
 *    ls
 *    -lih
 * </code></pre>
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ExecutionCommande {

    /** la clef à utiliser quand on désire exécuter une commande pour n'importe quel système d'exploitation : <b>*</b> */
    public static final String KEY_OS_DEFAULT = "*";

    private static final String KEY_OS_NAME = "os.name";
    private static final int BUFFER_SIZE = 1024;
    private static final String VARIABLE_OUVERTURE = "${";
    private static final String VARIABLE_FERMETURE = "}";
    public static final String DIRECTIVE_COUPURE = "SPLIT:";


    private static Log logger = LogFactory.getLog(ExecutionCommande.class);

    private String[] commandes;
    private Charset charset;
    private boolean attendreFinExecution;
    private Map<String, String> proprietesParDefaut;
    private String[] proprietesProcessus;
    private File proprietesRepertoire;
    private Set<Integer> codeErreurs;

    /**
     * Constructeur par défaut.  Initialise l'instance avec des propriétés par défaut.
     */
    public ExecutionCommande()
    {
        this.charset = Charset.defaultCharset();
        this.attendreFinExecution = true;
        proprietesParDefaut = Collections.emptyMap();
        proprietesProcessus = null;
        proprietesRepertoire = null;

        // erreurs par défaut
        this.codeErreurs = new HashSet<Integer>(2);
        codeErreurs.add(1);
        codeErreurs.add(2);
    }

    public String toString()
    {

        StringBuffer sb = new StringBuffer(256);
        sb.append("ExecutionCommande:\n")
          .append("   commande :    ");
        if (commandes == null)
        {
            sb.append(commandes).append("\n");
        }
        else
        {
            for (String cmdStr : commandes)
            {
                sb.append(cmdStr).append(" ");
            }
            sb.append("\n");
        }
        sb.append("   env props:  ").append(proprietesProcessus).append("\n")
          .append("   dir :        ").append(proprietesRepertoire).append("\n")
          .append("   os :         ").append(System.getProperty(KEY_OS_NAME)).append("\n");
        return sb.toString();
    }

    /**
     * Spécifie les commandes à exécuter qu'importe le système d'exploitation.
     *
     * @param commandes Un tableau de String correspondant à l'ensemble des commandes qui seront exécutées.
     * @since 3.0
     */
    public void setCommandes(String[] commandes)
    {
        this.commandes = commandes;
    }

    /**
     * Spécifie le charset des flux de sortie standard et d'erreur générés par les commandes exécutés.
     * Par défault : {@link Charset#defaultCharset()}.
     *
     * @param charsetCode   le charsert sélectionné.
     * @throws java.nio.charset.UnsupportedCharsetException dans le cas où le charset n'est pas reconnu par Java.
     */
    public void setCharset(String charsetCode)
    {
        this.charset = Charset.forName(charsetCode);
    }

    /**
     * Spécifie si on doit attendre la fin de l'exécution de la commande.  Si la fin normale de l'exécution n'est pas attendu alors la valeur de retour de la sortie <i>standard</i> et <i>d'erreur</i>
     * ne peuvent être pertinente tant que la commande est toujours en cours d'exécution.
     *
     * @param attendreFinExecution     <tt>true</tt> (par défaut) est d'attendre la fin de l'exécution de la commande,
     *                              ou <tt>false</tt> afin de retourner le code de sortie 0 qu'importe le réel retour.
     *
     * @since 2.1
     */
    public void setAttendreFinExecution(boolean attendreFinExecution)
    {
        this.attendreFinExecution = attendreFinExecution;
    }

    /**
     * Supply a choice of commands to execute based on a mapping from the <i>os.name</i> system
     * property to the commandes to execute.  The {@link #KEY_OS_DEFAULT *} key can be used
     * to get a commandes where there is not direct match to the operating system key.
     * <p>
     * Each commandes is an array of strings, the first of which represents the commandes and all subsequent
     * entries in the array represent the arguments.  All elements of the array will be checked for
     * the presence of any substitution parameters (e.g. '{dir}').  The parameters can be set using the
     * {@link #setProprietesParDefaut(Map) defaults} or by passing the substitution values into the
     * {@link #executer(Map)} commandes.
     * <p>
     * If parameters passed may be multiple arguments, or if the values provided in the map are themselves
     * collections of arguments (not recommended), then prefix the value with <b>SPLIT:</b> to ensure that
     * the value is tokenized before being passed to the commandes.  Any values that are not split, will be
     * passed to the commandes as single arguments.  For example:<br>
     * '<b>SPLIT: dir . ..</b>' becomes '<b>dir</b>', '<b>.</b>' and '<b>..</b>'.<br>
     * '<b>SPLIT: dir ${path}</b>' (if path is '<b>. ..</b>') becomes '<b>dir</b>', '<b>.</b>' and '<b>..</b>'.<br>
     * The splitting occurs post-subtitution.  Where the arguments are known, it is advisable to avoid
     * <b>SPLIT:</b>.
     *
     * @param commandesParOS          a map of commandes string arrays, keyed by operating system names
     *
     * @see #setProprietesParDefaut(Map)
     *
     * @since 3.0
     */
    public void setCommandsAndArguments(Map<String, String[]> commandesParOS)
    {
        // get the current OS
        String serverOs = System.getProperty(KEY_OS_NAME);
        // attempt to find a match
        String[] command = commandesParOS.get(serverOs);
        if (command == null)
        {
            // go through the commands keys, looking for one that matches by regular expression matching
            for (String osName : commandesParOS.keySet())
            {
                // Ignore * options.  It is dealt with later.
                if (osName.equals(KEY_OS_DEFAULT))
                {
                    continue;
                }
                // Do regex match
                if (serverOs.matches(osName))
                {
                    command = commandesParOS.get(osName);
                    break;
                }
            }
            // if there is still no commandes, then check for the wildcard
            if (command == null)
            {
                command = commandesParOS.get(KEY_OS_DEFAULT);
            }
        }
        // check
        if (command == null)
        {
            throw new AtexoRuntimeException(
                    "No command found for OS " + serverOs + " or '" + KEY_OS_DEFAULT + "': \n" +
                    "   commands: " + commandesParOS);
        }
        this.commandes = command;
    }

    /**
     * Set additional runtime properties (environment properties) that will used
     * by the executing process.
     * <p>
     * Any keys or properties that start and end with <b>${...}</b> will be removed on the assumption
     * that these are unset properties.  <tt>null</tt> values are translated to empty strings.
     * All keys and values are trimmed of leading and trailing whitespace.
     *
     * @param processProperties     Runtime process properties
     *
     * @see Runtime#exec(String, String[], java.io.File)
     */
    public void setProprietesProcessus(Map<String, String> processProperties)
    {
        ArrayList<String> processPropList = new ArrayList<String>(processProperties.size());
        for (Map.Entry<String, String> entry : processProperties.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key == null)
            {
                continue;
            }
            if (value == null)
            {
                value = "";
            }
            key = key.trim();
            value = value.trim();
            if (key.startsWith(VARIABLE_OUVERTURE) && key.endsWith(VARIABLE_FERMETURE))
            {
                continue;
            }
            if (value.startsWith(VARIABLE_OUVERTURE) && value.endsWith(VARIABLE_FERMETURE))
            {
                continue;
            }
            processPropList.add(key + "=" + value);
        }
        this.proprietesProcessus = processPropList.toArray(new String[processPropList.size()]);
    }

    /**
     * Supply a choice of commands to execute based on a mapping from the <i>os.name</i> system
     * property to the commandes to execute.  The {@link #KEY_OS_DEFAULT *} key can be used
     * to get a commandes where there is not direct match to the operating system key.
     *
     * @param commandesParOS a map of commandes string keyed by operating system names
     *
     * @deprecated          Use {@link #setCommandsAndArguments(Map)}
     */
    public void setCommandMap(Map<String, String> commandesParOS)
    {
        // This is deprecated, so issue a warning
        logger.warn(
                "The bean RuntimeExec property 'commandMap' has been deprecated;" +
        		" use 'commandsAndArguments' instead.  See https://issues.alfresco.com/jira/browse/ETHREEOH-579.");
        Map<String, String[]> fixed = new LinkedHashMap<String, String[]>(7);
        for (Map.Entry<String, String> entry : commandesParOS.entrySet())
        {
            String os = entry.getKey();
            String unparsedCmd = entry.getValue();
            StringTokenizer tokenizer = new StringTokenizer(unparsedCmd);
            String[] cmd = new String[tokenizer.countTokens()];
            for (int i = 0; i < cmd.length; i++)
            {
                cmd[i] = tokenizer.nextToken();
            }
            fixed.put(os, cmd);
        }
        setCommandsAndArguments(fixed);
    }

    /**
     * Set the default commandes-line properties to use when executing the commandes.
     * These are properties that substitute variables defined in the commandes string itself.
     * Properties supplied during execution will overwrite the default properties.
     * <p>
     * <code>null</code> properties will be treated as an empty string for substitution
     * purposes.
     *
     * @param proprietesParDefaut property values
     */
    public void setProprietesParDefaut(Map<String, String> proprietesParDefaut)
    {
        this.proprietesParDefaut = proprietesParDefaut;
    }


    /**
     * Set the runtime location from which the commandes is executed.
     * <p>
     * If the value is an unsubsititued variable (<b>${...}</b>) then it is ignored.
     * If the location is not visible at the time of setting, a warning is issued only.
     *
     * @param proprietesRepertoire          the runtime location from which to execute the commandes
     */
    public void setProprietesRepertoire(String proprietesRepertoire)
    {
        if (proprietesRepertoire.startsWith(VARIABLE_OUVERTURE) && proprietesRepertoire.endsWith(VARIABLE_FERMETURE))
        {
            this.proprietesRepertoire = null;
        }
        else
        {
            this.proprietesRepertoire = new File(proprietesRepertoire);
            if (!this.proprietesRepertoire.exists())
            {
                logger.warn(
                        "The runtime process directory is not visible when setting property 'processDirectory': \n" +
                        this);
            }
        }
    }

    /**
     * A comma or space separated list of values that, if returned by the executed commandes,
     * indicate an error value.  This defaults to <b>"1, 2"</b>.
     *
     * @param codeErreurs the error codes for the execution
     */
    public void setErrorCodes(String codeErreurs)
    {
        this.codeErreurs.clear();
        StringTokenizer tokenizer = new StringTokenizer(codeErreurs, " ,");
        while(tokenizer.hasMoreElements())
        {
            String errCodeStr = tokenizer.nextToken();
            // attempt to convert it to an integer
            try
            {
                int errCode = Integer.parseInt(errCodeStr);
                this.codeErreurs.add(errCode);
            }
            catch (NumberFormatException e)
            {
                throw new AtexoRuntimeException(
                        "Property 'errorCodes' must be comma-separated list of integers: " + codeErreurs);
            }
        }
    }

    /**
     * Executes the commandes using the default properties
     *
     * @see #executer(Map)
     */
    public ExecutionResultat executer()
    {
        return executer(proprietesParDefaut);
    }

    /**
     * Executes the statement that this instance was constructed with.
     * <p>
     * <code>null</code> properties will be treated as an empty string for substitution
     * purposes.
     *
     * @return Returns the full execution results
     */
    public ExecutionResultat executer(Map<String, String> properties)
    {
        int defaultFailureExitValue = codeErreurs.size() > 0 ? ((Integer) codeErreurs.toArray()[0]) : 1;

        // check that the commandes has been set
        if (commandes == null)
        {
            throw new AtexoRuntimeException("Runtime command has not been set: \n" + this);
        }

        // create the properties
        Runtime runtime = Runtime.getRuntime();
        Process process = null;
        String[] commandToExecute = null;
        try
        {
            // execute the commandes with full property replacement
            commandToExecute = getCommand(properties);

            process = runtime.exec(commandToExecute, proprietesProcessus, proprietesRepertoire);
        }
        catch (IOException e)
        {
            // The process could not be executed here, so just drop out with an appropriate error state
            String execOut = "";
            String execErr = e.getMessage();
            int exitValue = defaultFailureExitValue;
            ExecutionResultat result = new ExecutionResultat(null, commandToExecute, codeErreurs, exitValue, execOut, execErr);
            if (logger.isDebugEnabled())
            {
                logger.debug(result);
            }
            return result;
        }

        // create the stream gobblers
        InputStreamReaderThread stdOutGobbler = new InputStreamReaderThread(process.getInputStream(), charset);
        InputStreamReaderThread stdErrGobbler = new InputStreamReaderThread(process.getErrorStream(), charset);

        // start gobbling
        stdOutGobbler.start();
        stdErrGobbler.start();

        // wait for the process to finish
        int exitValue = 0;
        try
        {
            if (attendreFinExecution)
            {
                exitValue = process.waitFor();
            }
        }
        catch (InterruptedException e)
        {
            // process was interrupted - generate an error message
            stdErrGobbler.ajouterAuBuffer(e.toString());
            exitValue = defaultFailureExitValue;
        }

        if (attendreFinExecution)
        {
            // ensure that the stream gobblers get to finish
            stdOutGobbler.attendreFinExecution();
            stdErrGobbler.attendreFinExecution();
        }

        // get the stream values
        String execOut = stdOutGobbler.getBuffer();
        String execErr = stdErrGobbler.getBuffer();

        // construct the return value
        ExecutionResultat result = new ExecutionResultat(process, commandToExecute, codeErreurs, exitValue, execOut, execErr);

        // done
        if (logger.isDebugEnabled())
        {
            logger.debug(result);
        }
        return result;
    }

    /**
     * @return Returns the commandes that will be executed if no additional properties
     *      were to be supplied
     */
    public String[] getCommandes()
    {
        return getCommand(proprietesParDefaut);
    }

    /**
     * Get the commandes that will be executed post substitution.
     * <p>
     * <code>null</code> properties will be treated as an empty string for substitution
     * purposes.
     *
     * @param properties the properties that might be executed with
     * @return Returns the commandes that will be executed should the additional properties
     *      be supplied
     */
    public String[] getCommand(Map<String, String> properties)
    {
        Map<String, String> execProperties = null;
        if (properties == proprietesParDefaut)
        {
            // we are just using the default properties
            execProperties = proprietesParDefaut;
        }
        else
        {
            execProperties = new HashMap<String, String>(proprietesParDefaut);
            // overlay the supplied properties
            execProperties.putAll(properties);
        }
        // Perform the substitution for each element of the commandes
        ArrayList<String> adjustedCommandElements = new ArrayList<String>(20);
        for (int i = 0; i < commandes.length; i++)
        {
            StringBuilder sb = new StringBuilder(commandes[i]);
            for (Map.Entry<String, String> entry : execProperties.entrySet())
            {
                String key = entry.getKey();
                String value = entry.getValue();
                // ignore null
                if (value == null)
                {
                    value = "";
                }
                // progressively replace the property in the commandes
                key = (VARIABLE_OUVERTURE + key + VARIABLE_FERMETURE);
                int index = sb.indexOf(key);
                while (index > -1)
                {
                    // replace
                    sb.replace(index, index + key.length(), value);
                    // get the next one
                    index = sb.indexOf(key, index + 1);
                }
            }
            String adjustedValue = sb.toString();
            // Now SPLIT: it
            if (adjustedValue.startsWith(DIRECTIVE_COUPURE))
            {
                String unsplitAdjustedValue = sb.substring(DIRECTIVE_COUPURE.length());
                StringTokenizer tokenizer = new StringTokenizer(unsplitAdjustedValue);
                while (tokenizer.hasMoreTokens())
                {
                    adjustedCommandElements.add(tokenizer.nextToken());
                }
            }
            else
            {
                adjustedCommandElements.add(adjustedValue);
            }
        }
        // done
        return adjustedCommandElements.toArray(new String[adjustedCommandElements.size()]);
    }

    /**
     * Object to carry the results of an execution to the caller.
     *
     * @author Derek Hulley
     */
    public static class ExecutionResultat
    {
        private final Process processus;
        private final String[] commandes;
        private final Set<Integer> codeErreurs;
        private final int valeurDeSortie;
        private final String sortieStandard;
        private final String sortieErreur;

        /**
         *
         * @param processus           the process attached to Java - <tt>null</tt> is allowed
         */
        private ExecutionResultat(
                final Process processus,
                final String[] commandes,
                final Set<Integer> codeErreurs,
                final int valeurDeSortie,
                final String sortieStandard,
                final String sortieErreur)
        {
            this.processus = processus;
            this.commandes = commandes;
            this.codeErreurs = codeErreurs;
            this.valeurDeSortie = valeurDeSortie;
            this.sortieStandard = sortieStandard;
            this.sortieErreur = sortieErreur;
        }

        @Override
        public String toString()
        {
            String out = sortieStandard.length() > 250 ? sortieStandard.substring(0, 250) : sortieStandard;
            String err = sortieErreur.length() > 250 ? sortieErreur.substring(0, 250) : sortieErreur;

            StringBuilder sb = new StringBuilder(128);
            sb.append("Execution result: \n")
              .append("   os:         ").append(System.getProperty(KEY_OS_NAME)).append("\n")
              .append("   command:    ").append(Arrays.deepToString(commandes)).append("\n")
              .append("   succeeded:  ").append(isExecuterAvecSucces()).append("\n")
              .append("   exit code:  ").append(valeurDeSortie).append("\n")
              .append("   out:        ").append(out).append("\n")
              .append("   err:        ").append(err);
            return sb.toString();
        }

        /**
         * A helper method to force a kill of the process that generated this result.  This is
         * useful in cases where the process started is not expected to exit, or doesn't exit
         * quickly.  If the {@linkplain ExecutionCommande#setAttendreFinExecution(boolean) "wait for completion"}
         * flag is <tt>false</tt> then the process may still be running when this result is returned.
         *
         * @return
         *      <tt>true</tt> if the process was killed, otherwise <tt>false</tt>
         */
        public boolean tuerProcessus()
        {
            if (processus == null)
            {
                return true;
            }
            try
            {
                processus.destroy();
                return true;
            }
            catch (Throwable e)
            {
                logger.warn(e.getMessage());
                return false;
            }
        }

        /**
         * @param exitValue the commandes exit value
         * @return Returns true if the code is a listed failure code
         *
         * @see #setErrorCodes(String)
         */
        private boolean isEnErreur(int exitValue)
        {
            return codeErreurs.contains((Integer)exitValue);
        }

        /**
         * @return Returns true if the commandes was deemed to be successful according to the
         *      failure codes returned by the execution.
         */
        public boolean isExecuterAvecSucces()
        {
            return !isEnErreur(valeurDeSortie);
        }

        public int getValeurDeSortie()
        {
            return valeurDeSortie;
        }

        public String getSortieStandard()
        {
            return sortieStandard;
        }

        public String getSortieErreur()
        {
            return sortieErreur;
        }
    }

    /**
     * Gobbles an <code>InputStream</code> and writes it into a
     * <code>StringBuffer</code>
     * <p>
     * The reading of the input stream is buffered.
     */
    public static class InputStreamReaderThread extends Thread
    {
        private final InputStream is;
        private final Charset charset;
        private final StringBuffer buffer;
        private boolean isRunning;
        private boolean completed;

        /**
         * @param is Flux d'entrée à lire, il sera lu par l'intermédiaire d'un buffer.
         */
        public InputStreamReaderThread(InputStream is, Charset charset)
        {
            super();
            setDaemon(true); // must not hold up the VM if it is terminating
            this.is = is;
            this.charset = charset;
            this.buffer = new StringBuffer(BUFFER_SIZE);
            this.isRunning = false;
            this.completed = false;
        }

        @Override
        public synchronized void run()
        {
            // marque le thread comme en cours d'exécution
            isRunning = true;
            completed = false;

            byte[] bytes = new byte[BUFFER_SIZE];
            InputStream tempIs = null;
            try
            {
                tempIs = new BufferedInputStream(is, BUFFER_SIZE);
                int count = -2;
                while (count != -1)
                {
                    // en cours de lecture ?
                    if (count > 0)
                    {
                        String toWrite = new String(bytes, 0, count, charset.name());
                        buffer.append(toWrite);
                    }
                    // lecture de la suite du tableau de bytes
                    count = tempIs.read(bytes);
                }
                // ok
                isRunning = false;
                completed = true;
            }
            catch (IOException e)
            {
                throw new AtexoRuntimeException("Impossible de lire le flux", e);
            }
            finally
            {
                // cloture le flux d'entrée
                if (tempIs != null)
                {
                    try
                    {
                        tempIs.close();
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }

        /**
         * Attendre la fin d'exécution de la commande.
         * <p>
         * <b>Ne pas oublier de <code>démarrer</code> le thread d'abord.
         */
        public synchronized void attendreFinExecution()
        {
            while (!completed && !isRunning)
            {
                try
                {
                    // release our lock and wait a bit
                    this.wait(200L); // 200 ms
                }
                catch (InterruptedException e)
                {
                }
            }
        }

        /**
         * @param msg Le message à ajouter au buffer.
         */
        public void ajouterAuBuffer(String msg)
        {
            buffer.append(msg);
        }

        public boolean isComplete()
        {
            return completed;
        }

        /**
         * @return Retourne l'état courrant du buffer.
         */
        public String getBuffer()
        {
            return buffer.toString();
        }
    }
}
