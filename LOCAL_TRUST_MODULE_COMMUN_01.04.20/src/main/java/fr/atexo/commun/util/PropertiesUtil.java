package fr.atexo.commun.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Classe utilitaire permettant de lire gérer les fichiers de propriétés.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class PropertiesUtil {

    /**
     * Classe statique uniquement
     */
    private PropertiesUtil() {
    }

    /**
     * Permet de récupérer la valeur d'un propriété stocké dans un fichier de propriétés.
     *
     * @param nomFichierProprietes
     * @param nomPropriete
     *
     * @return
     *
     * @throws IOException
     */
    public static String getValeurPropriete(String nomFichierProprietes, String nomPropriete) throws IOException {
        InputStream inputStream = ClasspathUtil.searchResource(nomFichierProprietes);
        return getProprietes(inputStream).getProperty(nomPropriete);
    }

    /**
     * Permet de récupérer la valeur d'un propriété stocké dans un fichier de propriétés. 
     *
     * @param fichierProprietes
     * @return
     * @throws IOException
     */
    public static Properties getProprietes(InputStream fichierProprietes) throws IOException {

        Properties properties = new Properties();
        properties.load(fichierProprietes);

        return properties;
    }
}
