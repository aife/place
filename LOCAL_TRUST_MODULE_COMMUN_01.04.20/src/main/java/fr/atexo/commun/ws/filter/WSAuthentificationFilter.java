package fr.atexo.commun.ws.filter;

import org.apache.commons.codec.binary.Base64;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Filtre en charge de l'authentification des accès aux couche WS.
 */
public abstract class WSAuthentificationFilter implements Filter {

    private static Logger Log = Logger.getLogger(WSAuthentificationFilter.class.getName());

    private final static String PARAM_LOGIN = "login";
    private final static String PARAM_MOT_PASSE = "motpasse";
    private final static String AUTHORIZATION_HEADER = "Authorization";
    private final static String AUTHORIZATION_SEPARATOR = ":";
    private final static String AUTHORIZATION_PREFIXE = "BASIC ";

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String[] headerContent = getHeaderContent(request);
        String login = null;
        String pwd = null;
        if (headerContent != null) {
            login = headerContent[0];
            pwd = headerContent[1];
        } else {
            login = request.getParameter(PARAM_LOGIN);
            pwd = request.getParameter(PARAM_MOT_PASSE);
        }
        if (login == null || pwd == null) {
            Log.severe("Une erreur est survenue lors de l'authentification avec login : " + login + " et password :" + pwd + ". Verifier que le login et mot de passe sont non null.");
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        if (!login(login, pwd)) {
            Log.severe("Une erreur est survenue lors de l'authentification avec login : " + login + " et password :" + pwd + ". Verifier que le login et mot de passe technique sont correctes.");
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        chain.doFilter(request, response);
    }

    private String[] getHeaderContent(ServletRequest request) {
        String authContent = ((HttpServletRequest) request).getHeader(AUTHORIZATION_HEADER);
        if (authContent == null || authContent.isEmpty()) {
            return null;
        }
        authContent = authContent.substring(AUTHORIZATION_PREFIXE.length());
        String decodeHeaderString = new String(Base64.decodeBase64(authContent.getBytes()));
        if (decodeHeaderString.indexOf(AUTHORIZATION_SEPARATOR) < 0) {
            return null;
        }
        return decodeHeaderString.split(AUTHORIZATION_SEPARATOR);
    }

    public abstract boolean login(String login, String pwd);
}
