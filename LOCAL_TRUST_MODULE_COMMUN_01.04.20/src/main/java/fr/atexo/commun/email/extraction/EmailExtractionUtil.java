package fr.atexo.commun.email.extraction;

import com.sun.mail.pop3.POP3Store;
import fr.atexo.commun.exception.mail.RepertoireInvalideException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.mail.*;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.ParseException;
import javax.mail.search.FlagTerm;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Classe d'extraction de mail depuis des Serveurs en Pop3 / Pop3s / Imap /
 * Imaps. La liste des propriétés systèmes et leur explication peut être trouvée
 * : http://docs.oracle.com/javaee/6/api/javax/mail/internet/package-summary.
 * html
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class EmailExtractionUtil {

    private static final Log LOG = LogFactory.getLog(EmailExtractionUtil.class);

    public static final String MAIL_MIME_ADDRESS_STRICT = "mail.mime.address.strict";

    public static final String MAIL_MIME_CHARSET = "mail.mime.charset";

    public static final String MAIL_MIME_DECODE_TEXT_STRICT = "mail.mime.decodetext.strict";

    public static final String MAIL_MIME_ENCODE_EOL_STRICT = "mail.mime.encodeeol.strict";

    public static final String MAIL_MIME_DECODE_FILENAME = "mail.mime.decodefilename";

    public static final String MAIL_MIME_ENCODE_FILENAME = "mail.mime.encodefilename";

    public static final String MAIL_MIME_DECODE_PARAMETERS = "mail.mime.decodeparameters";

    public static final String MAIL_MIME_ENCODE_PARAMETERS = "mail.mime.encodeparameters";

    public static final String MAIL_MIME_MULTIPART_IGNORE_MISSING_END_BOUNDARY = "mail.mime.multipart.ignoremissingendboundary";

    public static final String MAIL_MIME_MULTIPART_IGNORE_MISSING_BOUNDARY_PARAMETER = "mail.mime.multipart.ignoremissingboundaryparameter";

    public static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain";

    public static final String CONTENT_TYPE_TEXT_HTML = "text/html";

    protected Session session = null;
    protected Store boiteReception = null;
    protected String compteUtilisateur, compteMotDePasse;
    protected Folder repertoire;

    private EmailProtocolType emailProtocolType;

    /**
     * Constructeur.
     *
     * @param emailProtocolType le type de protocole utilisé pour se connecter au serveur de
     *                          mails
     */
    public EmailExtractionUtil(EmailProtocolType emailProtocolType) {
        this.emailProtocolType = emailProtocolType;
    }

    /**
     * Constructeur
     *
     * @param emailProtocolType le type de protocole utilisé pour se connecter au serveur de
     *                          mails
     * @param compteUtilisateur le compte mail
     * @param compteMotDePasse  le mot de passe du compte mail
     */
    public EmailExtractionUtil(EmailProtocolType emailProtocolType, String compteUtilisateur, String compteMotDePasse) {
        this(emailProtocolType);
        setCompteUtilisateur(compteUtilisateur, compteMotDePasse);
    }

    /**
     * Permet de setter des propriété système qui seront pris en compte par
     * l'api Javamail. L'ensemble des propriétés sont déclaré en static afin de
     * faciliter leur accessibilité.
     *
     * @param propriete la propriété
     * @param valeur    la valeur de la propriété
     */
    public static void setProprieteSysteme(String propriete, boolean valeur) {
        System.setProperty(propriete, String.valueOf(valeur));
    }

    /**
     * Permet d'initialiser les informations du compte mail de l'utilisateur.
     *
     * @param compteUtilisateur le compte mail
     * @param compteMotDePasse  le mot de passe du compte mail
     */
    public void setCompteUtilisateur(String compteUtilisateur, String compteMotDePasse) {
        this.compteUtilisateur = compteUtilisateur;
        this.compteMotDePasse = compteMotDePasse;
    }

    /**
     * Permet de connecter sur le serveur de mail du compte de l'utilisateur.
     *
     * @param urlServeur l'url d'accès du serveur mail (dépendra du type de protocol
     *                   utilisé)
     * @throws MessagingException
     */
    public void connecter(String urlServeur) throws MessagingException {
        connecter(emailProtocolType, urlServeur, emailProtocolType.getPortParDefaut(), compteUtilisateur, compteMotDePasse);
    }

    /**
     * Permet de connecter sur le serveur de mail du compte de l'utilisateur.
     *
     * @param urlServeur l'url d'accès du serveur mail (dépendra du type de protocol
     *                   utilisé)
     * @param port       le port sur lequel interroger le serveur
     * @throws MessagingException
     */
    public void connecter(String urlServeur, Integer port) throws MessagingException {
        connecter(emailProtocolType, urlServeur, port, compteUtilisateur, compteMotDePasse);
    }

    /**
     * Permet de connecter sur le serveur de mail du compte de l'utilisateur.
     *
     * @param urlServeur        l'url d'accès du serveur mail (dépendra du type de protocol
     *                          utilisé)
     * @param port              le port sur lequel interroger le serveur
     * @param compteUtilisateur le compte mail
     * @param compteMotDePasse  le mot de passe du compte mail
     * @throws MessagingException
     */
    public void connecter(String urlServeur, Integer port, String compteUtilisateur, String compteMotDePasse) throws MessagingException {
        connecter(emailProtocolType, urlServeur, port, compteUtilisateur, compteMotDePasse);
    }

    /**
     * Permet de connecter sur le serveur de mail du compte de l'utilisateur.
     *
     * @param type              le type de protocole
     * @param urlServeur        l'url d'accès du serveur mail (dépendra du type de protocol
     *                          utilisé)
     * @param port              le port sur lequel interroger le serveur
     * @param compteUtilisateur le compte mail
     * @param compteMotDePasse  le mot de passe du compte mail
     * @throws MessagingException
     */
    private void connecter(EmailProtocolType type, String urlServeur, Integer port, String compteUtilisateur, String compteMotDePasse) throws MessagingException {
        setCompteUtilisateur(compteUtilisateur, compteMotDePasse);
        Properties properties = new Properties();

        int portSelectionne = port != null ? port.intValue() : type.getPortParDefaut();

        if (type.isSsl()) {
            String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
            properties.setProperty("mail." + type.getProtocol() + ".socketFactory.class", SSL_FACTORY);
            properties.setProperty("mail." + type.getProtocol() + ".socketFactory.fallback", "false");
            properties.setProperty("mail." + type.getProtocol() + ".socketFactory.port", String.valueOf(portSelectionne));
        }
        properties.setProperty("mail." + type.getProtocol() + ".port", String.valueOf(portSelectionne));

        properties.setProperty(MAIL_MIME_ADDRESS_STRICT, "false");

        URLName url = new URLName(type.getProtocol(), urlServeur, portSelectionne, "", compteUtilisateur, compteMotDePasse);

        session = Session.getInstance(properties, null);

        switch (type) {
            case POP3:
                boiteReception = new POP3Store(session, url, type.getNom(), portSelectionne, type.isSsl());
                break;
            case POP3S:
                boiteReception = new POP3Store(session, url, type.getNom(), portSelectionne, type.isSsl());
                break;
            case IMAP:
                boiteReception = new IMAPStore(session, url, type.getNom(), portSelectionne, type.isSsl());
                break;
            case IMAPS:
                boiteReception = new IMAPStore(session, url, type.getNom(), portSelectionne, type.isSsl());
                break;
        }

        boiteReception.connect();
    }

    /**
     * Permet de se déconnecter du serveur mail
     *
     * @throws MessagingException
     */
    public void deconnecter() throws MessagingException {
        boiteReception.close();
    }

    /**
     * Permet d'ouvrir le répertoire afin de pouvoir lister son contenu.
     *
     * @param nomRepertoire le nom du répertoire devant être listé.
     * @throws RepertoireInvalideException
     * @throws MessagingException
     */
    public Folder ouvrirRepertoire(String nomRepertoire, boolean creerRepertoire) throws RepertoireInvalideException, MessagingException {

        repertoire = boiteReception.getDefaultFolder();
        repertoire = repertoire.getFolder(nomRepertoire);

        if (!repertoire.exists()) {
            if (creerRepertoire) {
                LOG.warn("Création du répertoire " + nomRepertoire + " dans la boite de réception du compte " + compteUtilisateur);
                repertoire.create(Folder.HOLDS_MESSAGES);
            } else {
                repertoire = null;
            }
        }

        if (repertoire == null) {
            throw new RepertoireInvalideException("Le répertoire " + nomRepertoire + " est invalide");
        }

        // try to open read/write and if that fails try read-only
        try {
            repertoire.open(Folder.READ_WRITE);
        } catch (MessagingException ex) {
            repertoire.open(Folder.READ_ONLY);
        }

        return repertoire;
    }

    /**
     * Permet de fermer le répertoire.
     *
     * @param supprimerMessages supprimer les messages supprimés du répertoire.
     * @throws MessagingException
     */
    public void fermerRepertoire(boolean supprimerMessages) throws MessagingException {
        fermerRepertoire(repertoire, supprimerMessages);
    }

    /**
     * Permet de fermer le répertoire.
     *
     * @param repertoire        le répertoire à fermer.
     * @param supprimerMessages supprimer les messages supprimés du répertoire.
     * @throws MessagingException
     */
    public void fermerRepertoire(Folder repertoire, boolean supprimerMessages) throws MessagingException {
        if (repertoire != null) {
            repertoire.close(supprimerMessages);
        }
    }

    /**
     * Renvoi le nombre total de mails se trouvant dans le répertoire.
     *
     * @return le nombre total de mails du répertoire.
     * @throws MessagingException
     */
    public int getNombreMessages() throws MessagingException {
        return repertoire.getMessageCount();
    }

    /**
     * Renvoi le nombre total de nouveaux mails se trouvant dans le répertoire.
     *
     * @return le nombre total de nouveaux mails du répertoire.
     * @throws MessagingException
     */
    public int getNombreNouveauxMessages() throws MessagingException {
        return repertoire.getNewMessageCount();
    }

    /**
     * Renvoi le nombre total de mails non lus se trouvant dans le répertoire.
     *
     * @return le nombre total de mails non lus du répertoire.
     * @throws MessagingException
     */

    public int getNombreMessagesNonLus() throws MessagingException {
        return repertoire.getUnreadMessageCount();
    }

    /**
     * Renvoi le nombre total de mails supprimés se trouvant dans le répertoire.
     *
     * @return le nombre total de mails supprimés du répertoire.
     * @throws MessagingException
     */
    public int getNombreMessagesSupprimes() throws MessagingException {
        return repertoire.getDeletedMessageCount();
    }

    /**
     * Renvoi la liste des répertoires se trouvant dans le répertoire.
     *
     * @return la liste des répertoires
     * @throws MessagingException
     */
    public Folder[] getSousRepertoires() throws MessagingException {
        return repertoire.list();
    }

    /**
     * Renvoi l'ensemble des messages se trouvant dans le répertoire.
     *
     * @return les messages du répertoire.
     * @throws MessagingException
     */
    public Message[] getMessages() throws MessagingException {
        return repertoire.getMessages();
    }

    /**
     * Recherhe des messages par des flag "custom"
     *
     * @param flags
     * @param set
     * @return
     * @throws MessagingException
     */
    public Message[] searchMessagesByFlag(Flags flags, boolean set) throws MessagingException {
        return repertoire.search(new FlagTerm(flags, set));
    }

    /**
     * Renvoi une plage de messages se trouvant dans le répertoire.
     *
     * @param numeroDebut le numéro du premier message
     * @param numeroFin   le numéro du dernier message
     * @return les messages du répertoire
     * @throws MessagingException
     */
    public Message[] getMessages(int numeroDebut, int numeroFin) throws MessagingException {
        return repertoire.getMessages(numeroDebut, numeroFin);
    }

    /**
     * Renvoi le message numéro X se trouvant dans le répertoire.
     *
     * @param numero le numétro du message à récupérer.
     * @return le message numéro X se trouvant dans le répertoire.
     * @throws MessagingException
     */
    public Message getMessage(int numero) throws MessagingException {
        return repertoire.getMessage(numero);
    }

    /**
     * Récupére les informations complémentaires des messages.
     *
     * @param messages la liste des messages
     * @throws MessagingException
     */
    public void recupererInformationsComplementaires(Message[] messages) throws MessagingException {
        FetchProfile fp = new FetchProfile();
        fp.add(FetchProfile.Item.ENVELOPE);
        repertoire.fetch(messages, fp);
    }

    /**
     * Méthode permettant de parcourir l'ensemble du contenu d'un part afin de
     * retrouver sous la forme d'une map l'ensemble des parties de son contenu
     * pour les types de mime passés.
     *
     * @param part      le part à parcourir
     * @param mimeTypes les type de mime des partie que l'on veut récupérer.
     */
    public Map<String, List<Part>> rechercherPartParMimeTypes(Part part, String... mimeTypes) {
        Map<String, List<Part>> parts = new HashMap<String, List<Part>>();
        rechercherPartParMimeTypes(part, parts, mimeTypes);
        return parts;
    }

    /**
     * Méthode permettant de parcourir l'ensemble du contenu d'un part afin de
     * regroupe l'ensembles des parties par type mime dans une map.
     *
     * @param part                          le part à parcourir
     * @param exclureCharsetDansContentType exlue ou non le charset du content-type
     */
    public Map<String, List<Part>> recupererTousPartParMimeTypes(Part part, boolean exclureCharsetDansContentType) {
        Map<String, List<Part>> parts = new HashMap<String, List<Part>>();
        recupererTousPartParMimeTypes(part, parts, exclureCharsetDansContentType);
        return parts;
    }

    /**
     * Méthode permettant de parcourir l'ensemble du contenu d'un part afin de
     * retrouver sous la forme d'une map l'ensemble des parties de son contenu
     * pour les types de mime passés.
     *
     * @param part      le part à parcourir
     * @param mapParts  la map contenant le resultat de la recherche
     * @param mimeTypes les type de mime des partie que l'on veut récupérer.
     */
    private void rechercherPartParMimeTypes(Part part, Map<String, List<Part>> mapParts, String... mimeTypes) {
        try {
            try {
                if (part.isMimeType("multipart/*")) {
                    Multipart mp = (Multipart) part.getContent();
                    for (int i = 0; mp != null && i < mp.getCount(); i++) {
                        rechercherPartParMimeTypes(mp.getBodyPart(i), mapParts, mimeTypes);
                    }
                } else {
                    for (String mimeType : mimeTypes) {
                        if (part.isMimeType(mimeType)) {
                            List<Part> parts = mapParts.get(mimeType.toLowerCase());
                            if (parts == null) {
                                parts = new ArrayList<Part>();
                                mapParts.put(mimeType, parts);
                            }
                            parts.add(part);
                        }
                    }
                }
            } catch (UnsupportedEncodingException ex) {
                LOG.warn(part.getContentType(), ex);
            }

        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (MessagingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * Méthode permettant de parcourir l'ensemble du contenu d'un part afin de
     * regroupe l'ensembles des parties par type mime dans une map.
     *
     * @param part                          le part à parcourir
     * @param mapParts                      la map contenant le resultat de la recherche
     * @param exclureCharsetDansContentType exlue ou non le charset du content-type
     */
    private void recupererTousPartParMimeTypes(Part part, Map<String, List<Part>> mapParts, boolean exclureCharsetDansContentType) {
        try {
            try {
                if (part.isMimeType("multipart/*")) {
                    Multipart mp = (Multipart) part.getContent();
                    for (int i = 0; mp != null && i < mp.getCount(); i++) {
                        recupererTousPartParMimeTypes(mp.getBodyPart(i), mapParts, exclureCharsetDansContentType);
                    }
                } else {
                    String mimeType = part.getContentType();
                    if (mimeType != null && exclureCharsetDansContentType) {
                        String[] mimeTypeSplite = mimeType.split("[;]");
                        mimeType = mimeTypeSplite[0].toLowerCase();
                    }

                    List<Part> parts = mapParts.get(mimeType);
                    if (parts == null) {
                        parts = new ArrayList<Part>();
                        mapParts.put(mimeType, parts);
                    }
                    parts.add(part);
                }
            } catch (UnsupportedEncodingException ex) {
                LOG.warn(part.getContentType(), ex);
            }

        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (MessagingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * Récupére le contenu texte brute d'un message (première occurance)
     *
     * @param mapParts les parts formant le message
     * @param exclure  exclu ou non le part de la liste (dans le cas où l'on aurait
     *                 des pieces jointe du même content-type)
     * @return le contenu du message en texte brute.
     */
    public String recupererContenuMessageTextPlain(Map<String, List<Part>> mapParts, boolean exclure) {
        return recupererContenuMessageText(mapParts, CONTENT_TYPE_TEXT_PLAIN, exclure);
    }

    /**
     * Récupére le contenu html d'un message (première occurance)
     *
     * @param mapParts les parts formant le message
     * @param exclure  exclu ou non le part de la liste (dans le cas où l'on aurait
     *                 des pieces jointe du même content-type)
     * @return le contenu du message en html.
     */
    public String recupererContenuMessageTextHtml(Map<String, List<Part>> mapParts, boolean exclure) {
        return recupererContenuMessageText(mapParts, CONTENT_TYPE_TEXT_HTML, exclure);
    }

    /**
     * Récupére le contenu texte ou html d'un message (première occurance)
     *
     * @param mapParts    les parts formant le message
     * @param contentType le content-type à récupérer.
     * @param exclure     exclu ou non le part de la liste (dans le cas où l'on aurait
     *                    des pieces jointe du même content-type)
     * @return le contenu du message en fonction du content-type que l'on
     * souhaite récupérer.
     */
    private String recupererContenuMessageText(Map<String, List<Part>> mapParts, String contentType, boolean exclure) {
        Part part = recupererPartMessageText(mapParts, contentType, exclure);
        return extraireContenuMessage(part);
    }

    /**
     * Récupére le contenu texte ou html d'un message (première occurance)
     *
     * @param part le part formant le message
     * @return le contenu du message en fonction du content-type que l'on
     * souhaite récupérer.
     */
    public String extraireContenuMessage(Part part) {
        String message = null;
        if (part != null) {
            try {
                message = (String) part.getContent();
            } catch (Exception e) {
                LOG.warn("Erreur lors de la tentative de récupération du contenu du message : " + e.getMessage());
            }
        }

        return message;
    }

    /**
     * Récupére le Part texte ou html d'un message (première occurance)
     *
     * @param mapParts    les parts formant le message
     * @param contentType le content-type à récupérer.
     * @param exclure     exclu ou non le part de la liste (dans le cas où l'on aurait
     *                    des pieces jointe du même content-type)
     * @return le contenu du message en fonction du content-type que l'on
     * souhaite récupérer.
     */
    private Part recupererPartMessageText(Map<String, List<Part>> mapParts, String contentType, boolean exclure) {

        Part part = null;
        if (mapParts != null && mapParts.containsKey(contentType)) {

            List<Part> parts = mapParts.get(contentType);

            if (!parts.isEmpty()) {
                part = parts.get(0);

                if (exclure) {
                    parts.remove(part);
                    if (parts.isEmpty()) {
                        mapParts.remove(contentType);
                    }
                }
            }

        }

        return part;
    }

    /**
     * Extrait le contenu d'un mail afin de contruire un objet EmailContenu.
     *
     * @param message                       le message à traiter
     * @param exclureCharsetDansContentType exlue ou non le charset du content-type
     * @return
     * @throws MessagingException
     */
    public EmailContenu recupererEmailContenu(Message message, boolean exclureCharsetDansContentType) throws MessagingException {

        EmailContenu emailContenu = null;

        if (message != null) {

            List<String> emetteurs = extraireAdresseElectronique((InternetAddress[]) message.getFrom());
            List<String> destinataires = extraireAdresseElectronique((InternetAddress[]) message.getRecipients(Message.RecipientType.TO));
            emailContenu = new EmailContenu(message.getSubject(), message.getSentDate(), message.getReceivedDate(), destinataires, emetteurs);

            List<String> destinatairesCopies = extraireAdresseElectronique((InternetAddress[]) message.getRecipients(Message.RecipientType.CC));
            emailContenu.setDestinatairesCopies(destinatairesCopies);

            List<String> destinatairesCopiesCaches = extraireAdresseElectronique((InternetAddress[]) message.getRecipients(Message.RecipientType.BCC));
            emailContenu.setDestinatairesCopiesCaches(destinatairesCopiesCaches);

            Map<String, List<Part>> mapParts = recupererTousPartParMimeTypes(message, exclureCharsetDansContentType);

            Part html = recupererPartMessageText(mapParts, CONTENT_TYPE_TEXT_HTML, true);
            emailContenu.setCorpHtml(html);

            Part plain = recupererPartMessageText(mapParts, CONTENT_TYPE_TEXT_PLAIN, true);
            emailContenu.setCorpTexte(plain);

            emailContenu.setMapPiecesJointes(mapParts);
        }

        return emailContenu;
    }

    /**
     * Extrait les adresses électronique à partir d'une liste d'InternetAddress
     *
     * @param adressesElectroniques la liste d'InternetAddress
     * @return la liste des adresses électroniques
     */
    public List<String> extraireAdresseElectronique(InternetAddress[] adressesElectroniques) {

        List<String> adresses = new ArrayList<String>();
        if (adressesElectroniques != null && adressesElectroniques.length > 0) {

            for (int i = 0; i < adressesElectroniques.length; i++) {
                adresses.add(adressesElectroniques[i].getAddress());
            }
        }

        return adresses;

    }

    /**
     * Récupére le contenu texte ou html d'un message
     *
     * @param mapParts    les parts formant le message
     * @param contentType le content-type à récupérer.
     * @return le contenu du message en fonction du content-type que l'on
     * souhaite récupérer.
     */
    public Map<String, List<Part>> recupererContenuPiecesJointes(Map<String, List<Part>> mapParts, String contentType) {

        Map<String, List<Part>> map = new HashMap<String, List<Part>>();

        for (String mimeType : mapParts.keySet()) {
            try {
                ContentType ct = new ContentType(mimeType);
                boolean verification = ct.match(contentType);

                if (verification) {
                    map.put(mimeType, mapParts.get(mimeType));
                }

            } catch (ParseException e) {
            }

        }

        return map;
    }

    public void deplacerMessages(String repertoireDestination) throws MessagingException {
        Folder repertoire = boiteReception.getFolder(repertoireDestination);

    }

    public Store getBoiteReception() {
        return boiteReception;
    }

    public Folder getRepertoire() {
        return repertoire;
    }
}
