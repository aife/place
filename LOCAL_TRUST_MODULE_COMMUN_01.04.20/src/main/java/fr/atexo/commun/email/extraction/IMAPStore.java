package fr.atexo.commun.email.extraction;

import javax.mail.Session;
import javax.mail.URLName;

/**
 * Surcharge du IMAPStore afin de pouvoir utiliser le contructeur mis en protected.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class IMAPStore extends com.sun.mail.imap.IMAPStore {

    public IMAPStore(Session session, URLName url, String name, int defaultPort, boolean isSSL) {
        super(session, url, name, defaultPort, isSSL);
    }
}