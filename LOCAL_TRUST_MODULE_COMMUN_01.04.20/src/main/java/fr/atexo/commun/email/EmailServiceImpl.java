package fr.atexo.commun.email;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

/**
 * Classe du service d'envoi d'emails.
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class EmailServiceImpl implements EmailService {

  private static final Log LOG = LogFactory.getLog(EmailServiceImpl.class);

  public void envoyerEmails(ServeurSmtpConnexion serveurSmtpConnexion, Email... emails) throws EmailException {

    LOG.debug("Envoi de mails...");

    org.apache.commons.mail.Email email = null;

    for (Email emailTemporaire : emails) {

      // cas html
      if (emailTemporaire.isEnvoiHtml()) {

        email = new HtmlEmail();
        renseignerInformationsCommunes(email, emailTemporaire, serveurSmtpConnexion);

        // contenu HTML
        ((HtmlEmail) email).setHtmlMsg(emailTemporaire.getContenuHtml());

        // contenu alternatif
        ((HtmlEmail) email).setTextMsg(emailTemporaire.getContenuTexte());

        if (emailTemporaire.contientPiecesJointes()) {

          for (EmailPieceJointe emailPieceJointe : emailTemporaire.getPiecesJointes()) {

            EmailAttachment emailAttachment = new EmailAttachment();
            emailAttachment.setName(emailPieceJointe.getNom());
            emailAttachment.setDescription(emailPieceJointe.getDescription());
            emailAttachment.setPath(emailPieceJointe.getChemin());
            emailAttachment.setURL(emailPieceJointe.getUrl());
            emailAttachment.setDisposition(emailPieceJointe.getDisposition());
            ((MultiPartEmail) email).attach(emailAttachment);
          }
        }
      }
      // cas texte
      else {

        if (emailTemporaire.contientPiecesJointes()) {
          email = new MultiPartEmail();
          for (EmailPieceJointe emailPieceJointe : emailTemporaire.getPiecesJointes()) {
            EmailAttachment emailAttachment = new EmailAttachment();
            emailAttachment.setName(emailPieceJointe.getNom());
            emailAttachment.setDescription(emailPieceJointe.getDescription());
            emailAttachment.setPath(emailPieceJointe.getChemin());
            emailAttachment.setURL(emailPieceJointe.getUrl());
            emailAttachment.setDisposition(emailPieceJointe.getDisposition());
            ((MultiPartEmail) email).attach(emailAttachment);
          }
        } else {
          email = new SimpleEmail();
        }

        renseignerInformationsCommunes(email, emailTemporaire, serveurSmtpConnexion);

        // contenu texte
        email.setMsg(emailTemporaire.getContenuTexte());
      }

      String messageId = email.send();
      LOG.debug("Email envoyé : " + messageId);

    }
  }

  /**
   * Perment de renseigner les informations communes entre un mail au format html ou texte et avec
   * ou sans pièces jointes.
   * 
   * @param email l'email à paramétrer.
   * @param emailTemporaire le bean contenant les informations sur l'email à envoyer.
   * @param serveurSmtpConnexion le bean contenant les informations de connexion au serveur smtp.
   * @throws EmailException
   */
  private void renseignerInformationsCommunes(org.apache.commons.mail.Email email, Email emailTemporaire, ServeurSmtpConnexion serveurSmtpConnexion) throws EmailException {

    // configuration serveur smtp
    email.setHostName(serveurSmtpConnexion.getHote());
    email.setSSL(serveurSmtpConnexion.isSsl());
    email.setSmtpPort(serveurSmtpConnexion.getPort());
    email.setDebug(serveurSmtpConnexion.isDebug());

    email.setTLS(serveurSmtpConnexion.isTls());
    if (serveurSmtpConnexion.getUtilsateurAuthentification() != null) {
      email.setAuthenticator(new DefaultAuthenticator(serveurSmtpConnexion.getUtilsateurAuthentification(), serveurSmtpConnexion.getMotDePasseAuthentification()));
    }

    // information mail
    email.setFrom(emailTemporaire.getExpediteur());

    for (String destinataire : emailTemporaire.getDestinataires()) {
      email.addTo(destinataire);
    }

    if (emailTemporaire.getDestinatairesCopie() != null) {
      for (String cc : emailTemporaire.getDestinatairesCopie()) {
        email.addCc(cc);
      }
    }

    email.setSubject(emailTemporaire.getSujet());
    email.setSentDate(new Date());

    if (emailTemporaire.getEncoding() != null) {
      email.setCharset(emailTemporaire.getEncoding());
    }

  }

}
