package fr.atexo.commun.email;

import java.io.Serializable;
import java.util.Set;

/**
 * Classe contenant les emails qui seront envoyer par le biais de la classe de service
 * EmailServiceImpl.
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class Email implements Serializable {

  private String expediteur;
  private Set<String> destinataires;
  private Set<String> destinatairesCopie;
  private String sujet;
  private String contenuTexte;
  private String contenuHtml;
  private boolean envoiTexte;
  private boolean envoiHtml;
  private String encoding;
  private Set<EmailPieceJointe> piecesJointes;

  public Email(String expediteur, Set<String> destinataires, String sujet, String contenuTexte, String contenuHtml) {
    this.expediteur = expediteur;
    this.destinataires = destinataires;
    this.sujet = sujet;
    this.contenuTexte = contenuTexte;
    this.contenuHtml = contenuHtml;

    if (this.contenuTexte != null) {
      this.envoiTexte = true;
    }

    if (this.contenuHtml != null) {
      this.envoiHtml = true;
    }
  }

  public Email(String expediteur, Set<String> destinataires, String sujet, String contenuTexte, String contenuHtml, Set<EmailPieceJointe> piecesJointes) {
    this(expediteur, destinataires, sujet, contenuTexte, contenuHtml);
    this.piecesJointes = piecesJointes;
  }

  public String getExpediteur() {
    return expediteur;
  }

  public void setExpediteur(String expediteur) {
    this.expediteur = expediteur;
  }

  public Set<String> getDestinataires() {
    return destinataires;
  }

  public void setDestinataires(Set<String> destinataires) {
    this.destinataires = destinataires;
  }

  public Set<String> getDestinatairesCopie() {
    return destinatairesCopie;
  }

  public void setDestinatairesCopie(Set<String> destinatairesCopie) {
    this.destinatairesCopie = destinatairesCopie;
  }

  public String getSujet() {
    return sujet;
  }

  public void setSujet(String sujet) {
    this.sujet = sujet;
  }

  public String getContenuTexte() {
    return contenuTexte;
  }

  public void setContenuTexte(String contenuTexte) {
    this.contenuTexte = contenuTexte;
  }

  public String getContenuHtml() {
    return contenuHtml;
  }

  public void setContenuHtml(String contenuHtml) {
    this.contenuHtml = contenuHtml;
  }

  public boolean isEnvoiTexte() {
    return envoiTexte;
  }

  public void setEnvoiTexte(boolean envoiTexte) {
    this.envoiTexte = envoiTexte;
  }

  public boolean isEnvoiHtml() {
    return envoiHtml;
  }

  public void setEnvoiHtml(boolean envoiHtml) {
    this.envoiHtml = envoiHtml;
  }

  public String getEncoding() {
    return encoding;
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  public Set<EmailPieceJointe> getPiecesJointes() {
    return piecesJointes;
  }

  public void setPiecesJointes(Set<EmailPieceJointe> piecesJointes) {
    this.piecesJointes = piecesJointes;
  }

  public boolean contientPiecesJointes() {
    return piecesJointes != null || (piecesJointes != null && !piecesJointes.isEmpty());
  }
}
