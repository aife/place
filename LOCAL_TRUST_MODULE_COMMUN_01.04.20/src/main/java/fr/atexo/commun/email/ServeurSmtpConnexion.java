package fr.atexo.commun.email;

import java.io.Serializable;

/**
 * Classe contenant les paramètres de connexion au serveur SMTP qui seront utilisés par le biais de la classe de service EmailServiceImpl.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ServeurSmtpConnexion implements Serializable {

    private String hote;
    private int port;
    private boolean tls;
    private boolean ssl;
    private boolean debug;
    private String utilsateurAuthentification;
    private String motDePasseAuthentification;

    public ServeurSmtpConnexion(String hote, int port) {
        this.hote = hote;
        this.port = port;
    }

    public ServeurSmtpConnexion(String hote, int port, boolean tls, boolean ssl, String utilsateurAuthentification, String motDePasseAuthentification) {
        this.hote = hote;
        this.port = port;
        this.tls = tls;
        this.ssl = ssl;
        this.utilsateurAuthentification = utilsateurAuthentification;
        this.motDePasseAuthentification = motDePasseAuthentification;
    }

    public String getHote() {
        return hote;
    }

    public void setHote(String hote) {
        this.hote = hote;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isTls() {
        return tls;
    }

    public void setTls(boolean tls) {
        this.tls = tls;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getUtilsateurAuthentification() {
        return utilsateurAuthentification;
    }

    public void setUtilsateurAuthentification(String utilsateurAuthentification) {
        this.utilsateurAuthentification = utilsateurAuthentification;
    }

    public String getMotDePasseAuthentification() {
        return motDePasseAuthentification;
    }

    public void setMotDePasseAuthentification(String motDePasseAuthentification) {
        this.motDePasseAuthentification = motDePasseAuthentification;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }
}
