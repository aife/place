/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Classe immuable représentant le nom d'un attribut d'un compte et sa valeur correspondante.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public final class AttributLDAP {

    private final String nom;
    private final String valeur;

    /**
     * Crée un nouvel attribut
     *
     * @param nom    le nom de l'attribut
     * @param valeur la valeur de l'attribut
     */
    public AttributLDAP(String nom, String valeur) {
        this.nom = nom;
        this.valeur = valeur;
    }

    /**
     * Retourne le nom de l'attribut.
     *
     * @return le nom de l'attribut
     */
    public String getNom() {
        return nom;
    }

    /**
     * Retorune la valeur de l'attribut
     *
     * @return la valeur de l'attribut
     */
    public String getValeur() {
        return valeur;
    }

    @Override
    public String toString() {
        return "LDAPAttribut{" + "nom='" + nom + '\'' + ", valeur='" + valeur + '\'' + '}';
    }
}
