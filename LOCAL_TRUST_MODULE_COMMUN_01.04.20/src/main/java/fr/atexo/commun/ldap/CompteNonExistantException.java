/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Exception lancée quand un compte n'existe pas alors qu'il est présupposé qu'il existe.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public class CompteNonExistantException extends Exception {

    private static final long serialVersionUID = -8206682599023747629L;

    /**
     * Construit l'exception.
     *
     * @param login le login non existant
     */
    CompteNonExistantException(String login) {
        super("Le compte ayant comme login \"" + login + "\" n'existe pas");
    }
}
