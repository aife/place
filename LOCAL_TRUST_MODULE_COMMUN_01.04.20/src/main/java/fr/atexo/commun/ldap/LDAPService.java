/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.util.List;

import javax.naming.NamingException;

/**
 * <p>
 * Le service qui permet d'accéder aux fonctions d'identification, de modification
 * et de récupération d'information à partir de l'annuaire LDAP.
 * </p>
 * <p> Attention, la notion de LDAP <b>"bidirectionnel"</b> est importante si vous souhaitez utiliser les méthodes
 * {@link #getDetailsComptes()} et {@link #getDetailsCompte(IdentifiantCompte, String[])}. un LDAP est dit "bidirectionnel" si
 * chaque compte contient ses rôles, et chaque rôle ses comptes. C'est le cas des {@literal Active Directory} mais pas de tous
 * les {@literal Open LDAP}. Si un LDAP n'est pas bidirectionel, ces deux méthodes fonctionneront mais les profils des comptes
 * ne seront pas retournés dans les objets {@link DetailsCompte}. C'est pourquoi il est préférable d'utiliser la méthode
 * {@link #getDetailsRestreintsAuxProfils(String[])}
 * qui elle retourne les profils de chaque compte quel que soit le type du serveur LDAP.
 * </p>
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public interface LDAPService {

    /**
     * S'authentifie au serveur LDAP.
     *
     * @param id         l'identifiant du compte à authentifier
     * @param motDePasse le mot de passe
     * @return {@code true} si l'authentification est réussie, {@code false} sinon
     */
    boolean login(IdentifiantCompte id, String motDePasse);

    /**
     * Crée un compte sur le serveur LDAP.
     *
     * @param id         l'identifiant du nouveau compte
     * @param motDePasse le mot de passe du nouveau compte
     * @param attributs  les attributs du nouveau compte
     * @throws NamingException         si une erreur de connexion au serveur LDAP se produit
     * @throws CompteExistantException si le compte existe déjà
     */
    void creerCompte(IdentifiantCompte id, String motDePasse, AttributLDAP... attributs)
            throws NamingException, CompteExistantException;

    /**
     * Supprime le compte spécifié.
     *
     * @param id l'identifiant du compte
     * @throws NamingException            si une erreur de connexion au serveur LDAP se produit
     * @throws CompteNonExistantException si le compte n'existe pas sur le serveur LDAP
     */
    void supprimerCompte(IdentifiantCompte id) throws NamingException, CompteNonExistantException;

    /**
     * Modifie les attributs du compte sans modifier le mot de passe.
     *
     * @param id                l'identifiant du compte
     * @param attributsMisAJour les attributs mis à jour
     * @throws NamingException            si une erreur de connexion au serveur LDAP se produit
     * @throws CompteNonExistantException si le compte existe déjà
     */
    void modifierCompte(IdentifiantCompte id, AttributLDAP... attributsMisAJour) throws NamingException, CompteNonExistantException;

    /**
     * Modifie le compte et le mot de passe associé.
     *
     * @param id                l'identifiant du compte
     * @param nouveauMotDePasse le nouveau mot de passe
     * @param attributsMisAJour les attributs mis à jour
     * @throws NamingException            si une erreur de connexion au serveur LDAP se produit
     * @throws CompteNonExistantException si le compte existe déjà
     */
    void modifierCompte(IdentifiantCompte id, String nouveauMotDePasse,
                        AttributLDAP... attributsMisAJour) throws NamingException, CompteNonExistantException;

    /**
     * Retourne les identifiants de tous les comptes correspondants au filtre par défaut.
     *
     * @return les identifiants de tous les comptes
     */
    List<IdentifiantCompte> getIdentifiants();

    /**
     * Retourne les identifiants de tous les comptes correspondants au filtre spécifié.
     *
     * @param filtre le filtre LDAP
     * @return les identifiants de tous les comptes correspondants au filtre spécifié
     */
    List<IdentifiantCompte> getIdentifiants(String filtre);

    /**
     * Retourne l'identifiant du compte correspondant au filtre par défaut.
     *
     * @return l'identifiant du compte correspondant au filtre par défaut.
     */
    IdentifiantCompte getIdentifiant();


    /**
     * Retourne l'identifiant du compte correspondant au filtre spécifié.
     *
     * @param filtre le filtre LDAP
     * @return l'identifiant du compte correspondant au filtre spécifié
     */
    IdentifiantCompte getIdentifiant(String filtre);

    /**
     * Retourne les identifiants des comptes possédants les profils spécifiés.
     *
     * @param profils les profils dont on souhaite connaître les comptes
     * @return les identifiants des comptes possédants les profils spécifiés
     */
    List<IdentifiantCompte> getIdentifiantsRestreintsAuxProfils(String... profils);

    /**
     * Retourne les comptes possédants au moins un des profils spécifié.
     *
     * @param profils les profils dont on souhaite connaître les comptes
     * @return les comptes possédants les profils spécifiés
     */
    List<DetailsCompte> getDetailsRestreintsAuxProfils(String... profils);

    /**
     * Retourne tous les comptes correspondants au filtre par défaut. Chaque {@link DetailsCompte} ne contient
     * les profils correspondant à son compte que si le LDAP est bidirectionnel.
     * <p/>
     * Il est donc préférable d'utiliser la méthode {@link #getDetailsRestreintsAuxProfils}, qui elle récupère les profils
     * de chaque compte dans tous les cas.
     *
     * @return tous les comptes correspondants au filtre par défaut
     */
    List<DetailsCompte> getDetailsComptes();

    /**
     * Retourne le détail du compte spécifié. Les profils ne sont inclus dans {@link DetailsCompte} que si le LDAP est
     * bidirectionnel.
     *
     * @param id                       l'identifiant du compte demandé
     * @param attributsSupplementaires les attributs facultatifs supplémentaires en plus de ceux spécifiés
     *                                 dans {@link LDAPParametresCompte#attributComptesRecuperes}
     * @return le détails du compte
     */
    DetailsCompte getDetailsCompte(IdentifiantCompte id, String... attributsSupplementaires);

    /**
     * Retourne les paramètres de compte.
     *
     * @return les paramètres de compte
     */
    LDAPParametresCompte getParametresCompte();

    /**
     * Retourne les paramètres de connexion.
     *
     * @return les paramètres de connexion
     */
    LDAPParametresConnexion getParametresConnexion();


}


