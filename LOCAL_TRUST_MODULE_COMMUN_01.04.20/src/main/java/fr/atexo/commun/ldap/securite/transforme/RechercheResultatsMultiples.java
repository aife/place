/*
 * This program is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software 
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this 
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html 
 * or from the Free Software Foundation, Inc., 
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2007 - 2009 Pentaho Corporation.  All rights reserved.
 *
 */
package fr.atexo.commun.ldap.securite.transforme;

import org.apache.commons.collections.Transformer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Classe de recherche ldap permettant de renvoyer un nombre indéfini d'informations.
 */
public class RechercheResultatsMultiples implements Transformer, InitializingBean {

    // ~ Static fields/initializers ============================================
    private static final Log LOG = LogFactory.getLog(RechercheResultatsMultiples.class);

    // ~ Instance fields =======================================================

    private String[] nomAttributs;

    // ~ Constructors ==========================================================

    public RechercheResultatsMultiples(final String[] nomAttributs) {
        this.nomAttributs = nomAttributs;
    }

    public RechercheResultatsMultiples(final String nomAttributs, final String separateur) {
        this.nomAttributs = nomAttributs.split("[" + separateur + "]");
    }

    // ~ Methods ===============================================================


    public void afterPropertiesSet() throws Exception {
        Assert.hasLength(nomAttributs.toString());
    }

    public Object transform(final Object obj) {

        Object transformed = obj;

        if (obj instanceof SearchResult) {
            transformed = new HashSet();
            Set valueSet = (Set) transformed;
            SearchResult res = (SearchResult) obj;

            Map<String, Object> results = new HashMap<String, Object>();

            for (int i = 0; i < nomAttributs.length; i++) {

                String attributeName = nomAttributs[i];

                Attributes attributes = res.getAttributes();
                Attribute attribute = attributes.get(attributeName);

                if (attribute != null) {

                    try {
                        NamingEnumeration values = attribute.getAll();

                        Object value = null;

                        // dans le cas où il s'agit d'une propriété avec un nombre indéfini de valeurs (ex :businessCategory,memberOf)
                        if (attribute.size() > 1) {
                            value = new Object[attribute.size()];
                        }

                        int j = 0;
                        while (values.hasMore()) {

                            if (attribute.size() > 1) {
                                ((Object[]) value)[j] = values.next();
                            } else {
                                value = values.next();
                            }

                            results.put(attributeName, value);
                            j++;
                        }
                    } catch (NamingException e) {
                        LOG.error(e.getMessage(), e);
                    }
                } else {
                    results.put(attributeName, null);
                }
            }

            valueSet.add(results);

            return transformed;

        }
        return transformed;

    }
}
