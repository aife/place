package fr.atexo.commun.modele;

import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable, Cloneable {

    private static final long serialVersionUID = -3094135292468457952L;

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private Long id;
    @Version
    private Long version;

    //the method toString could be expansive so we cache it
    private transient String toString;

    /**
     * Retourne l'id de l'entité.
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        Long idEntity = getId();
        return idEntity == null ? super.hashCode() : idEntity.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        Class thisClass = Hibernate.getClass(this);
        Class objClass = Hibernate.getClass(obj);
        if (!thisClass.equals(objClass)) {
            return false;
        }
        Long idEntity = getId();
        Long otherEntityId = ((AbstractEntity) obj).getId();
        if (idEntity == null) {
            if (otherEntityId != null) {
                return false;
            } else {
                return this == obj;
            }
        } else if (!idEntity.equals(otherEntityId)) {
            return false;
        }
        return true;
    }

    public Class getNoProxyClass() {
        return Hibernate.getClass(this);
    }

    @Override
    public String toString() {
        if (toString == null) {
            if (!Hibernate.isInitialized(this)) {
                Class thisClass = Hibernate.getClass(this);
                toString = thisClass.getSimpleName() + "[" + getId() + "]";
            } else {
                toString = getClass().getSimpleName() + "[" + getId() + "]";
            }
        }
        return toString;
    }

    public String getLabel() {
        return toString();
    }

    public AbstractEntity clone() {
        try {
            return (AbstractEntity) super.clone();
        } catch (CloneNotSupportedException e) {
            //should not happen
            return null;
        }
    }
}
