package fr.atexo.commun.modele;

import org.hibernate.annotations.Index;

import javax.persistence.*;
import java.util.Date;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
@MappedSuperclass
public class ReferentielValeur<X extends Referentiel<X, Y>, Y extends ReferentielValeur<X, Y>> extends AbstractEntity implements Comparable<ReferentielValeur> {

    @Column(name = "LIBELLE_VAL_REF", nullable = false)
    private String libelle;

    @Index(name = "IDX_CODE")
    private String code;

    @Column(name = "VALEUR_STRING_1")
    private String valeurString1;

    @Column(name = "VALEUR_STRING_2")
    private String valeurString2;

    @Column(name = "VALEUR_STRING_3")
    private String valeurString3;

    @Column(name = "VALEUR_STRING_4")
    private String valeurString4;

    @Column(name = "VALEUR_STRING_5")
    private String valeurString5;

    @Column(name = "VALEUR_STRING_6")
    private String valeurString6;

    @Column(name = "VALEUR_STRING_7")
    private String valeurString7;

    @Column(name = "VALEUR_STRING_8")
    private String valeurString8;

    @Column(name = "VALEUR_STRING_9")
    private String valeurString9;

    @Column(name = "VALEUR_STRING_10")
    private String valeurString10;

    @Column(name = "VALEUR_STRING_11")
    private String valeurString11;

    @Column(name = "VALEUR_STRING_12")
    private String valeurString12;

    @Column(name = "VALEUR_STRING_13")
    private String valeurString13;

    @Column(name = "VALEUR_STRING_14")
    private String valeurString14;

    @Column(name = "VALEUR_STRING_15")
    private String valeurString15;

    @Column(name = "VALEUR_STRING_16")
    private String valeurString16;

    @Column(name = "VALEUR_DATE_1")
    private Date valeurDate1;

    @Column(name = "VALEUR_DATE_2")
    private Date valeurDate2;

    @Column(name = "VALEUR_DATE_3")
    private Date valeurDate3;

    @Column(name = "VALEUR_DOUBLE_1")
    private Double valeurDouble1;

    @Column(name = "VALEUR_DOUBLE_2")
    private Double valeurDouble2;

    @Column(name = "VALEUR_DOUBLE_3")
    private Double valeurDouble3;

    @Column(name = "VALEUR_DOUBLE_4")
    private Double valeurDouble4;

    @Column(name = "VALEUR_DOUBLE_5")
    private Double valeurDouble5;

    @Column(name = "VALEUR_DOUBLE_6")
    private Double valeurDouble6;

    @Column(name = "VALEUR_DOUBLE_7")
    private Double valeurDouble7;

    @Column(name = "VALEUR_DOUBLE_8")
    private Double valeurDouble8;

    @Column(name = "VALEUR_DOUBLE_9")
    private Double valeurDouble9;

    @Column(name = "VALEUR_BOOLEAN_1")
    private Boolean valeurBoolean1;

    @Column(name = "VALEUR_BOOLEAN_2")
    private Boolean valeurBoolean2;

    @Column(name = "VALEUR_BOOLEAN_3")
    private Boolean valeurBoolean3;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_REF")
    private X referentiel;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PARENT")
    private Y parent;

    @Column(name = "ACTIF")
    private boolean actif;

    @Column(name = "ALIMENTATION_INTERFACE")
    private Boolean alimentationInterface;


    public ReferentielValeur() {
        setActif(true);
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the référentiel
     */
    public X getReferentiel() {
        return this.referentiel;
    }

    /**
     * @param referentiel the référentiel to set
     */
    public void setReferentiel(X referentiel) {
        this.referentiel = referentiel;
    }

    public Y getParent() {
        return parent;
    }

    public void setParent(Y parent) {
        this.parent = parent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValeurString1() {
        return valeurString1;
    }

    public void setValeurString1(String valeurString1) {
        this.valeurString1 = valeurString1;
    }

    public String getValeurString2() {
        return valeurString2;
    }

    public void setValeurString2(String valeurString2) {
        this.valeurString2 = valeurString2;
    }

    public String getValeurString3() {
        return valeurString3;
    }

    public void setValeurString3(String valeurString3) {
        this.valeurString3 = valeurString3;
    }

    public String getValeurString4() {
        return valeurString4;
    }

    public void setValeurString4(String valeurString4) {
        this.valeurString4 = valeurString4;
    }

    public String getValeurString5() {
        return valeurString5;
    }

    public void setValeurString5(String valeurString5) {
        this.valeurString5 = valeurString5;
    }

    public String getValeurString6() {
        return valeurString6;
    }

    public void setValeurString6(String valeurString6) {
        this.valeurString6 = valeurString6;
    }

    public String getValeurString7() {
        return valeurString7;
    }

    public void setValeurString7(String valeurString7) {
        this.valeurString7 = valeurString7;
    }

    public String getValeurString8() {
        return valeurString8;
    }

    public void setValeurString8(String valeurString8) {
        this.valeurString8 = valeurString8;
    }

    public String getValeurString9() {
        return valeurString9;
    }

    public void setValeurString9(String valeurString9) {
        this.valeurString9 = valeurString9;
    }

    public String getValeurString10() {
        return valeurString10;
    }

    public void setValeurString10(String valeurString10) {
        this.valeurString10 = valeurString10;
    }

    public String getValeurString11() {
        return valeurString11;
    }

    public void setValeurString11(String valeurString11) {
        this.valeurString11 = valeurString11;
    }

    public String getValeurString12() {
        return valeurString12;
    }

    public void setValeurString12(String valeurString12) {
        this.valeurString12 = valeurString12;
    }

    public String getValeurString13() {
        return valeurString13;
    }

    public void setValeurString13(String valeurString13) {
        this.valeurString13 = valeurString13;
    }

    public String getValeurString14() {
        return valeurString14;
    }

    public void setValeurString14(String valeurString14) {
        this.valeurString14 = valeurString14;
    }

    public String getValeurString15() {
        return valeurString15;
    }

    public void setValeurString15(String valeurString15) {
        this.valeurString15 = valeurString15;
    }

    public String getValeurString16() {
        return valeurString16;
    }

    public void setValeurString16(String valeurString16) {
        this.valeurString16 = valeurString16;
    }

    public Date getValeurDate1() {
        return valeurDate1;
    }

    public void setValeurDate1(Date valeurDate1) {
        this.valeurDate1 = valeurDate1;
    }

    public Date getValeurDate2() {
        return valeurDate2;
    }

    public void setValeurDate2(Date valeurDate2) {
        this.valeurDate2 = valeurDate2;
    }

    public Date getValeurDate3() {
        return valeurDate3;
    }

    public void setValeurDate3(Date valeurDate3) {
        this.valeurDate3 = valeurDate3;
    }

    public Double getValeurDouble1() {
        return valeurDouble1;
    }

    public void setValeurDouble1(Double valeurDouble1) {
        this.valeurDouble1 = valeurDouble1;
    }

    public Double getValeurDouble2() {
        return valeurDouble2;
    }

    public void setValeurDouble2(Double valeurDouble2) {
        this.valeurDouble2 = valeurDouble2;
    }

    public Double getValeurDouble3() {
        return valeurDouble3;
    }

    public void setValeurDouble3(Double valeurDouble3) {
        this.valeurDouble3 = valeurDouble3;
    }

    public Double getValeurDouble4() {
        return valeurDouble4;
    }

    public Double getValeurDouble5() {
        return valeurDouble5;
    }

    public Double getValeurDouble6() {
        return valeurDouble6;
    }

    public Double getValeurDouble7() {
        return valeurDouble7;
    }

    public Double getValeurDouble8() {
        return valeurDouble8;
    }

    public Double getValeurDouble9() {
        return valeurDouble9;
    }

    public void setValeurDouble4(Double valeurDouble4) {
        this.valeurDouble4 = valeurDouble4;
    }

    public void setValeurDouble5(Double valeurDouble5) {
        this.valeurDouble5 = valeurDouble5;
    }

    public void setValeurDouble6(Double valeurDouble6) {
        this.valeurDouble6 = valeurDouble6;
    }

    public void setValeurDouble7(Double valeurDouble7) {
        this.valeurDouble7 = valeurDouble7;
    }

    public void setValeurDouble8(Double valeurDouble8) {
        this.valeurDouble8 = valeurDouble8;
    }

    public void setValeurDouble9(Double valeurDouble9) {
        this.valeurDouble9 = valeurDouble9;
    }

    public Boolean getValeurBoolean1() {
        return valeurBoolean1;
    }

    public void setValeurBoolean1(Boolean valeurBoolean1) {
        this.valeurBoolean1 = valeurBoolean1;
    }

    public Boolean getValeurBoolean2() {
        return valeurBoolean2;
    }

    public void setValeurBoolean2(Boolean valeurBoolean2) {
        this.valeurBoolean2 = valeurBoolean2;
    }

    public Boolean getValeurBoolean3() {
        return valeurBoolean3;
    }

    public void setValeurBoolean3(Boolean valeurBoolean3) {
        this.valeurBoolean3 = valeurBoolean3;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public Boolean isAlimentationInterface() {
        return alimentationInterface;
    }

    public void setAlimentationInterface(Boolean alimentationInterface) {
        this.alimentationInterface = alimentationInterface;
    }

    //business

    public int compareTo(ReferentielValeur o) {
        if (this == o || equals(o)) {
            return 0;
        }
        if(o == null) {
            return 1;
        }
        return getLibelle().compareTo(o.getLibelle());
    }

    @Override
    @Transient
    public String getLabel() {
        return getLibelle();
    }

    @Transient
    public String getLibelleAndCode() {
        return getLibelle() + " [" + getCode() + "]";
    }

    @Transient
    public String getCodeAndLibelle() {
        return getCode() + " - " + getLibelle();
    }

    @Transient
    public String getCodeSubstringFrom_AndLibelle() {
        return code.length() > 1 ? code.substring(code.indexOf("_") + 1) + " - " + getLibelle() : getLibelle();
    }

    @Transient
    public String getCodeSubstringFromUnderScore() {
        return code.length() > 1 ? code.substring(code.indexOf("_") + 1) : code;
    }

    @Transient
    public String getCodeSubstringUntil_AndLibelle() {
        if (code.indexOf("_") == -1) {
            return code;
        }
        return code.substring(0, code.indexOf("_")) + " - " + getLibelle();
    }

    @Transient
    public String getCodeSubstringUntilExercice() {
        if (code.indexOf("_") == -1) {
            return code;
        }
        return code.substring(0, code.indexOf("_"));
    }

    @Transient
    public String getCodeSubstringBetwen_AndLibelle() {
        String temp = code.substring(code.indexOf("_") + 1);
        if (temp.indexOf("_") == -1) {
            return temp + " - " + getLibelle();
        }
        return temp.substring(0, temp.indexOf("_")) + " - " + getLibelle();
    }

    @Transient
    public String getCodeSubstringBetwenUnderScore() {
        String temp = code.substring(code.indexOf("_") + 1);
        if (temp.indexOf("_") == -1) {
            return temp + " - " + getLibelle();
        }
        return temp.substring(0, temp.indexOf("_"));
    }

    @Override
    public Y clone() {
        return (Y) super.clone();
    }

    @Override
    public String toString() {
        return getLibelle();
    }
}
