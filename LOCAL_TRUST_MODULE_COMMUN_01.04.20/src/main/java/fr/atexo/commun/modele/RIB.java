package fr.atexo.commun.modele;

import javax.persistence.*;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
@MappedSuperclass
public class RIB extends AbstractEntity {

    private String codeBanque;
    private String codeGuichet;
    private String codeCompte;
    private String cle;
    private String domiciliation;
    private String titulaireCompte;
    private boolean etranger;
    private String iban;
    private String ribComplet;
    @Column(name = "REF_REGLEMENT")
    private String referenceReglement;
    @Column(name = "ACTIF", nullable = false)
    private Boolean ribActif;
    private Boolean usuel;
    private Long idFinance;

    private char trig;

    public RIB() {
        this.etranger = false;
        this.ribActif = true;
    }

    public String getCodeBanque() {
        return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
        this.codeBanque = codeBanque.toUpperCase();
    }

    public String getCodeGuichet() {
        return codeGuichet;
    }

    public void setCodeGuichet(String codeGuichet) {
        this.codeGuichet = codeGuichet;
    }

    public String getCodeCompte() {
        return codeCompte;
    }

    public void setCodeCompte(String codeCompte) {
        if (codeCompte != null) {
            this.codeCompte = codeCompte.toUpperCase();
        } else {
            this.codeCompte = null;
        }
    }

    public String getCle() {
        return cle;
    }

    public void setCle(String cle) {
        this.cle = cle;
    }

    public String getDomiciliation() {
        return domiciliation;
    }

    public void setDomiciliation(String domicialiation) {
        this.domiciliation = domicialiation;
    }

    public String getTitulaireCompte() {
        return titulaireCompte;
    }

    public void setTitulaireCompte(String titulaireCompte) {
        this.titulaireCompte = titulaireCompte;
    }

    public Boolean getRibActif() {
        return ribActif;
    }

    public void setRibActif(Boolean actif) {
        ribActif = actif;
    }

    public boolean isEtranger() {
        return etranger;
    }

    public void setEtranger(boolean etranger) {
        this.etranger = etranger;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getRibComplet() {
        return ribComplet;
    }

    public char getTrig() {
        return trig;
    }

    public void setTrig(char trig) {
        this.trig = trig;
    }

    public String getReferenceReglement() {
        return referenceReglement;
    }

    public void setReferenceReglement(String referenceReglement) {
        this.referenceReglement = referenceReglement;
    }

    public Boolean getUsuel() {
        return usuel;
    }

    public void setUsuel(Boolean usuel) {
        this.usuel = usuel;
    }

    public Long getIdFinance() {
        return idFinance;
    }

    public void setIdFinance(Long idFinance) {
        this.idFinance = idFinance;
    }

    //business

    @Transient
    public void setRibComplet(String ribComplet) {
        this.ribComplet = ribComplet;
    }

    @Transient
    public void setRibFrancais(String codeBanque, String codeGuichet, String codeCompte, String cle, String domiciliation, String titulaireCompte, String refReglement) {
        setEtranger(false);
        setCodeBanque(codeBanque);
        setCodeGuichet(codeGuichet);
        setCodeCompte(codeCompte);
        setCle(cle);
        setDomiciliation(domiciliation);
        setTitulaireCompte(titulaireCompte);
        setRibComplet(codeBanque + codeGuichet + getCodeCompte() + cle);
        setIban(codeBanque + "|" + codeGuichet + "|" + getCodeCompte() + "|" + cle);
        setReferenceReglement(refReglement);
    }

    @Transient
    public void setRibEtranger(String iban, String refReglement) {
        setEtranger(true);
        setIban(iban);
        this.codeBanque = null;
        this.codeGuichet = null;
        this.codeCompte = null;
        this.cle = null;
        this.domiciliation = null;
        this.titulaireCompte = null;
        this.ribComplet = null;
        setReferenceReglement(refReglement);
    }

    @Transient
    public String getDisplay() {

        if (isEtranger()) {
            return getIban();
        } else {
            String separateur = ", ";
            StringBuilder sb = new StringBuilder();
            sb.append(getDomiciliation());
            sb.append(separateur);
            sb.append(getCodeBanque());
            sb.append(separateur);
            sb.append(getCodeGuichet());
            sb.append(separateur);
            sb.append(getCodeCompte());
            sb.append(separateur);
            sb.append(getCle());
            return sb.toString();
        }
    }

    @Override
    @Transient
    public String toString() {
        return getIban();
    }

    @Transient
    public String getRefReglementAndIban() {
        return referenceReglement + " - " + iban;
    }

}
