package fr.atexo.commun.exception.mail;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class RepertoireInvalideException extends Exception {

    public RepertoireInvalideException(String message) {
        super(message);
    }
}
