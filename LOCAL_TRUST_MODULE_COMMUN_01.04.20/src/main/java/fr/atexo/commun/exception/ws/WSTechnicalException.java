package fr.atexo.commun.exception.ws;

/**
 * Exception envoyé lors de l'appel d'un web service dans le cas ou une erreur Technique est survenu
 */
public class WSTechnicalException extends WSException {

    public WSTechnicalException(String error) {
        super(error);
    }

    public WSTechnicalException(String error, Throwable e) {
        super(error, e);
    }
}
