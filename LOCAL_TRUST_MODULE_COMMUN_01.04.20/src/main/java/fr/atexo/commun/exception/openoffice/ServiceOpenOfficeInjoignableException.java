package fr.atexo.commun.exception.openoffice;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ServiceOpenOfficeInjoignableException extends ServiceOpenOfficeException {

    public ServiceOpenOfficeInjoignableException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceOpenOfficeInjoignableException(String message) {
        super(message);
    }
}
