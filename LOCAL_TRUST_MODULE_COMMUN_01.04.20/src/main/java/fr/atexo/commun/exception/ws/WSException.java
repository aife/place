package fr.atexo.commun.exception.ws;

/**
 * Exception envoyé lors de l'appel d'un web service
 */
public class WSException extends Exception {

    private String error;

    public WSException(String error) {
        super(error);
        this.error = error;
    }

    public WSException(String error, Throwable e) {
        super(error, e);
        this.error = error;
    }

    public String getError() {
        return error;
    }
}