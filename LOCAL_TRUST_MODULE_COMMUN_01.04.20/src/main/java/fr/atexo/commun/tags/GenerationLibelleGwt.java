package fr.atexo.commun.tags;

import org.apache.commons.lang.LocaleUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.*;

/**
 * Tag jsp recherchant un fichier de properties dans le classpath pour générer
 * un objet javascript utilisé par gwt pour la recherche de libellé.
 *
 * @author Léon BARSAMIAN.
 */
public class GenerationLibelleGwt extends TagSupport {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -2223707326621943570L;

    /**
     * Loggeur.
     */
    private static Log log = LogFactory.getLog(GenerationLibelleGwt.class);

    private static String DEBUT_JAVASCRIPT = "<script type=\"text/javascript\" language=\"javascript\">";

    private static String FIN_JAVASCRIPT = "</script>";

    /**
     * Nom du fichier de propriété à lire.
     */
    private String nomResource;

    /**
     * Nom de l'objet javascript généré dans la page par le tag. Le nom doit
     * commencer par une majuscule.
     */
    private String nomObjet;

    /**
     * nom du fichier Resource qui est externalisé pour le client. il ecrase les cles du fichier par defaut.
     */
    private String nomResourceClient;

    /**
     * nom de la locale associée
     */
    private Locale locale;

    @Override
    public int doStartTag() throws JspException {
        try {
            ResourceBundle messagesDefaut = null;

            if (locale != null) {
                messagesDefaut = PropertyResourceBundle.getBundle(nomResource, locale);
            } else {
                messagesDefaut = PropertyResourceBundle.getBundle(nomResource);
            }
            ResourceBundle messagesClient = null;
            Set<String> clesSet = new HashSet<String>();
            Enumeration<String> cles = null;
            try {
                if (nomResourceClient != null) {
                    if (locale != null) {
                        messagesClient = PropertyResourceBundle.getBundle(nomResourceClient, locale);
                        if (messagesClient != null && !messagesClient.getLocale().toString().equals(locale.toString())) {
                            messagesClient = null;
                        }

                    } else {
                        messagesClient = PropertyResourceBundle.getBundle(nomResourceClient);
                    }
                }
            } catch (MissingResourceException e) {
                log.debug("Fichier resource client introuvable " + e.getMessage());
            }
            pageContext.getOut().print(DEBUT_JAVASCRIPT);
            if (locale != null) {
                pageContext.getOut().print("var " + nomObjet + "_" + locale.toString() + " = {\n");
            } else {
                pageContext.getOut().print("var " + nomObjet + " = {\n");
            }
            if (messagesClient != null) {
                cles = messagesClient.getKeys();
                while (cles.hasMoreElements()) {
                    StringBuffer sb = new StringBuffer();
                    String cle = cles.nextElement();
                    clesSet.add(cle);
                    String valeur = messagesClient.getString(cle);
                    sb.append(cle);
                    sb.append(" : ");
                    valeur = traiterValeur(valeur);
                    sb.append(valeur);
                    if (cles.hasMoreElements()) {
                        sb.append(",");
                        sb.append("\n");
                    }
                    pageContext.getOut().print(sb);
                }
            }
            cles = messagesDefaut.getKeys();
            if (cles.hasMoreElements() && messagesClient != null && !messagesClient.keySet().isEmpty()) {
                pageContext.getOut().print(",");
                pageContext.getOut().print("\n");
            }
            while (cles.hasMoreElements()) {
                String cle = cles.nextElement();
                if (clesSet.contains(cle)) {
                    continue;
                }
                StringBuffer sb = new StringBuffer();
                clesSet.add(cle);
                String valeur = messagesDefaut.getString(cle);
                sb.append(cle);
                sb.append(" : ");
                valeur = traiterValeur(valeur);
                sb.append(valeur);
                if (cles.hasMoreElements()) {
                    sb.append(",");
                }
                sb.append("\n");
                pageContext.getOut().print(sb);
            }
            pageContext.getOut().print("\n }");
            pageContext.getOut().print(FIN_JAVASCRIPT);

        } catch (MissingResourceException e) {
            log.error(e, e.fillInStackTrace());
            throw new JspException("Fichier resource introuvable ", e);
        } catch (IOException e) {
            log.error(e, e.fillInStackTrace());
            throw new JspException("Erreur d'ecriture dans la jsp ", e);
        }
        return Tag.SKIP_BODY;
    }

    /**
     * Filtre la valeur pour echaper les doubles-quote et l'encadre. la valeur
     * Titre devienda "Titre"
     *
     * @param valeur la chaine à filtrer.
     * @return la chaine filtrée.
     */
    private String traiterValeur(final String valeur) {
        String resultat = valeur.replaceAll("\\\"", "\\\\\"");
        resultat = "\"" + resultat + "\"";
        return resultat;
    }

    /**
     * @param valeur Nom du fichier de propriété à lire.
     */
    public final void setNomResource(final String valeur) {
        this.nomResource = valeur;
    }

    /**
     * @param valeur Nom de l'objet javascript généré dans la page par le tag. Le
     *               nom doit commencer par une majuscule.
     */
    public final void setNomObjet(final String valeur) {
        this.nomObjet = valeur;
    }

    /**
     * @param valeur Nom de l'objet javascript généré dans la page par le tag. Le
     *               nom doit commencer par une majuscule.
     */
    public final void setNomResourceClient(final String valeur) {
        this.nomResourceClient = valeur;
    }

    public final void setTypeLocale(String typeLocale) {
        this.locale = LocaleUtils.toLocale(typeLocale);
    }
}
