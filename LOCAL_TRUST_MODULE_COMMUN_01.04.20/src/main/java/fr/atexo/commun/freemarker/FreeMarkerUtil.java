package fr.atexo.commun.freemarker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.commun.exception.ConfigurationException;
import fr.atexo.commun.util.FichierTemporaireUtil;
import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Commentaire
 * 
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class FreeMarkerUtil implements Serializable {

	private static final long serialVersionUID = -4921723972811389241L;

	// déclaration du log
	private static final Log LOG = LogFactory.getLog(FreeMarkerUtil.class);

	private static final String TEMPLATE_NOM_TEMPORAIRE = "freemarker_template_";

	/**
	 * Permet de remplacer le contenu du template par les paramètres.
	 * 
	 * @param cheminVersRepertoireTemplate
	 *            chemin vers le répertoire où sont contenu tous les templates
	 *            (doit contenir un / à la fin).
	 * @param nomFichierTemplate
	 *            le nom du fichier template à utiliser (template.ftl par
	 *            exemple).
	 * @param locale
	 *            la locale qui sera utilisée (dans le cas de la locale
	 *            Locale.FRENCH alors le premier fichier qui sera recherché sera
	 *            template_fr_FR.ftl puis template_fr.ftl puis template.ftl)
	 * @param parametres
	 *            les paramètres qui sont utilisés pour remplacer les variables
	 *            déclaré dans le template.
	 * @param echapperXml
	 *            <code>true</code> si on doit échapper des caractères spéciaux
	 *            xml, sinon <code>false</code>
	 * @param conserverTemplate
	 *            <code>true</code> si on dans le cas de template statiques, on
	 *            désire conserver les templates une fois généré, sinon
	 *            <code>false</code>
	 * @return Le fichier sur disque contenant le résultat de la transformation.
	 * @throws IOException
	 * @throws TemplateException
	 */
	public static File executeTransformation(String cheminVersRepertoireTemplate, String nomFichierTemplate, Locale locale, Map<String, Object> parametres, boolean echapperXml, boolean conserverTemplate) throws IOException, TemplateException {

		File repertoireTemplate = new File(cheminVersRepertoireTemplate);
		File fichierTemplate = new File(cheminVersRepertoireTemplate + nomFichierTemplate);

		if (!repertoireTemplate.isDirectory()) {
			throw new ConfigurationException("Le répertoire [" + repertoireTemplate.getAbsolutePath() + "] stockant les templates n'est pas un répertoire");
		}

		if (!fichierTemplate.isFile()) {
			throw new ConfigurationException("Le template [" + fichierTemplate.getAbsolutePath() + "] n'est pas accessible");
		}

		/*
		 * Ajout de la balise escape pour les caracteres xml pour open office
		 */
		if (echapperXml) {
			List<String> linesEscape = new ArrayList<String>();
			linesEscape.add("<#escape x as x?xml>");
			linesEscape.addAll(FileUtils.readLines(fichierTemplate));
			linesEscape.add("</#escape>");
			FileUtils.writeLines(fichierTemplate, linesEscape);
		}

		Configuration configuration = TemplateConfiguration.getConfiguration(repertoireTemplate);
		configuration.setObjectWrapper(ObjectWrapper.DEFAULT_WRAPPER);
		configuration.setEncoding(locale, "UTF-8");

		Template template = null;

		if (conserverTemplate) {
			template = TemplateConfiguration.getTemplate(nomFichierTemplate);
			if (template == null) {
				template = configuration.getTemplate(nomFichierTemplate, locale);
				TemplateConfiguration.getTemplates().put(nomFichierTemplate, template);
			}

		} else {
			template = configuration.getTemplate(nomFichierTemplate, locale);
		}

		File fichier = FichierTemporaireUtil.creerFichierTemporaire(TEMPLATE_NOM_TEMPORAIRE + String.valueOf(new Date().getTime()), ".tmp");
		FileOutputStream outputStream = new FileOutputStream(fichier);

		Writer out = new OutputStreamWriter(outputStream, "UTF-8");

		template.process(parametres, out);
		out.close();

		return fichier;

	}

	/**
	 * Permet de remplacer le contenu du template par les paramètres (se basant
	 * sur la locale par défaut pour trouver le bon fichier template à charger).
	 * Par défaut on échappe les caractères xml et on ne sauvergarde pas les
	 * templates statiques.
	 * 
	 * @param cheminVersRepertoireTemplate
	 *            chemin vers le répertoire où sont contenu tous les templates
	 *            (doit contenir un / à la fin).
	 * @param nomFichierTemplate
	 *            le nom du fichier template à utiliser (template.ftl par
	 *            exemple).
	 * @param parametres
	 *            les paramètres qui sont utilisés pour remplacer les variables
	 *            déclaré dans le template.
	 * @return Le fichier sur disque contenant le résultat de la transformation.
	 * @throws IOException
	 * @throws TemplateException
	 */
	@Deprecated
	public static File executeTransformation(String cheminVersRepertoireTemplate, String nomFichierTemplate, Locale locale, Map<String, Object> parametres) throws IOException, TemplateException {
		return executeTransformation(cheminVersRepertoireTemplate, nomFichierTemplate, Locale.getDefault(), parametres, true, false);
	}

	/**
	 * Permet de remplacer le contenu du template par les paramètres (se basant
	 * sur la locale par défaut pour trouver le bon fichier template à charger).
	 * 
	 * @param cheminVersRepertoireTemplate
	 *            chemin vers le répertoire où sont contenu tous les templates
	 *            (doit contenir un / à la fin).
	 * @param nomFichierTemplate
	 *            le nom du fichier template à utiliser (template.ftl par
	 *            exemple).
	 * @param parametres
	 *            les paramètres qui sont utilisés pour remplacer les variables
	 *            déclaré dans le template.
	 * @param echapperXml
	 *            <code>true</code> si on doit échapper des caractères spéciaux
	 *            xml, sinon <code>false</code>
	 * @return Le fichier sur disque contenant le résultat de la transformation.
	 * @throws IOException
	 * @throws TemplateException
	 */
	public static File executeTransformation(String cheminVersRepertoireTemplate, String nomFichierTemplate, Map<String, Object> parametres, boolean echapperXml, boolean conserverTemplate) throws IOException, TemplateException {
		return executeTransformation(cheminVersRepertoireTemplate, nomFichierTemplate, Locale.getDefault(), parametres, echapperXml, conserverTemplate);
	}

	/**
	 * @param beansMap
	 * @param scriptFreeMarker
	 * @return
	 * @throws Exception
	 */
	public static String executeDefaultScriptFreeMarker(Map<String, Object> beansMap, String scriptFreeMarker) throws Exception {
		return executeDefaultScriptFreeMarker(beansMap, scriptFreeMarker, false);
	}

	/**
	 * @param beansMap
	 * @param scriptFreeMarker
	 * @param throwsException
	 * @return
	 * @throws Exception
	 */
	public static String executeDefaultScriptFreeMarker(Map<String, Object> beansMap, String scriptFreeMarker, boolean throwsException) throws Exception {
		LOG.info("Appel de la fonction executeDefaultScriptFreeMarker");
		if (scriptFreeMarker == null) {
			LOG.error("Erreur lors de l'exécution d'un script FreeMarker. Le script passé comme paramètre au service FreeMarker est null");
			return null;
		}

		Template template = new Template("template_freeMarker.ftl", new StringReader(scriptFreeMarker), getConfiguration());
		Writer out = new StringWriter();
		LOG.info("Exécution d'un script FreeMarker");

		try {
			template.process(beansMap, out);
		} catch (TemplateException e) {
			LOG.error("Une erreur est survenue lors de l'execution des scripts FreeMarker.");
			LOG.error(e, e.fillInStackTrace());
			if (throwsException) {
				throw e;
			}
		}
		return out.toString();
	}

	/**
	 * @param beansMap
	 * @param scriptFreeMarker
	 * @param writer
	 * @throws Exception
	 */
	public static void executeDefaultScriptFreeMarker(Map<String, Object> beansMap, String scriptFreeMarker, Writer writer) throws Exception {
		executeDefaultScriptFreeMarker(beansMap, scriptFreeMarker, writer, false);
	}

	/**
	 * @param beansMap
	 * @param scriptFreeMarker
	 * @param writer
	 * @param throwsException
	 * @throws Exception
	 */
	public static void executeDefaultScriptFreeMarker(Map<String, Object> beansMap, String scriptFreeMarker, Writer writer, boolean throwsException) throws Exception {
		LOG.info("Appel de la fonction executeDefaultScriptFreeMarker");
		if (scriptFreeMarker == null) {
			LOG.error("Erreur lors de l'exécution d'un script FreeMarker. Le script passé comme paramètre au service FreeMarker est null");
			return;
		}

		Template template = new Template("template_freeMarker.ftl", new StringReader(scriptFreeMarker), getConfiguration());
		LOG.info("Exécution d'un script FreeMarker");

		try {
			template.process(beansMap, writer);
		} catch (TemplateException e) {
			LOG.error("Une erreur est survenue lors de l'execution des scripts FreeMarker.");
			LOG.error(e, e.fillInStackTrace());
			if (throwsException) {
				throw e;
			}
		}
	}

	/**
	 * @return
	 */
	public static Configuration getConfiguration() {
		return TemplateConfiguration.getConfiguration();
	}
}
