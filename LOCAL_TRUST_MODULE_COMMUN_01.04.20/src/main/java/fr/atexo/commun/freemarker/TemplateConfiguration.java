package fr.atexo.commun.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Classe permettant d'obtenir un singleton de la configuration des templates
 * utilisés par freemarker.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class TemplateConfiguration implements Serializable {

	private static Configuration configuration;
	private static Map<String, Template> templates;

	public static Configuration getConfiguration() {
		if (configuration == null) {
			configuration = new Configuration();
			configuration.setDateFormat("dd/MM/yyyy");
			configuration.setDateTimeFormat("dd/MM/yyyy HH:mm");
			configuration.setNumberFormat("0.##");
			configuration.setLocale(Locale.getDefault());
			configuration.setDefaultEncoding("UTF-8");
		}

		return configuration;
	}

	public static Configuration getConfiguration(String cheminVersRepertoireTemplate) throws IOException {
		getConfiguration();
		configuration.setDirectoryForTemplateLoading(new File(cheminVersRepertoireTemplate));
		return configuration;
	}

	public static Configuration getConfiguration(File repertoireTemplate) throws IOException {
		getConfiguration();
		configuration.setDirectoryForTemplateLoading(repertoireTemplate);
		return configuration;
	}

	public static Map<String, Template> getTemplates() throws IOException {
		if (templates == null) {
			templates = new HashMap<String, Template>();
		}

		return templates;
	}

	public static Template getTemplate(String nomFichierTemplate) throws IOException {
		return getTemplates().get(nomFichierTemplate);
	}
}
