package fr.atexo.commun.springsecurity.authentification.entrypoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ExternalHttpUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    @Override
    protected String buildRedirectUrlToLoginPage(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {
        String urlHttpRedirection = determineUrlToUseForThisRequest(request, response, authException);
        return urlHttpRedirection;
    }
}
