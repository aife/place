package fr.atexo.commun.springsecurity.authentification.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.codec.Base64;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.NullRememberMeServices;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.Assert;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
class StaticBasicAuthenticationFilter extends BasicAuthenticationFilter {

    private String username;

    private String password;

    private String[] grantedAuthorities;

    private RememberMeServices rememberMeServices = new NullRememberMeServices();

    public void setPassword(String password) {
        Assert.notNull(password, "password cannot be null");
        this.password = password;
    }

    public void setUsername(String username) {
        Assert.notNull(rememberMeServices, "username cannot be null");
        this.username = username;
    }

    public void setGrantedAuthorities(String[] grantedAuthorities) {
        Assert.notNull(grantedAuthorities, "grantedAuthorities cannot be null");
        this.grantedAuthorities = grantedAuthorities;
    }

    public void setRememberMeServices(RememberMeServices rememberMeServices) {
        Assert.notNull(rememberMeServices, "rememberMeServices cannot be null");
        this.rememberMeServices = rememberMeServices;
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        final boolean debug = logger.isDebugEnabled();
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;

        String header = request.getHeader("Authorization");

        if ((header != null) && header.startsWith("Basic ")) {
            byte[] base64Token = header.substring(6).getBytes("UTF-8");
            String token = new String(Base64.decode(base64Token), getCredentialsCharset(request));

            String username = "";
            String password = "";
            int delim = token.indexOf(":");

            if (delim != -1) {
                username = token.substring(0, delim);
                password = token.substring(delim + 1);
            }

            if (debug) {
                logger.debug("Basic Authentication Authorization header found for user '" + username + "'");
            }

            if (authenticationIsRequired(username)) {

                Authentication authentication = null;

                if (this.username.equals(username) && this.password.equals(password)) {

                    List<GrantedAuthority> authorisations = new ArrayList<GrantedAuthority>();
                    if (grantedAuthorities != null) {
                        for (String authorisation : grantedAuthorities) {
                            authorisations.add(new GrantedAuthorityImpl(authorisation));
                        }
                    }

                    authentication = new UsernamePasswordAuthenticationToken(username, password, authorisations);


                } else {
                    // Authentication failed
                    SecurityContextHolder.getContext().setAuthentication(null);
                    String error = "Static Authentication request for user: " + username + " failed";
                    AuthenticationException authenticationException = null;

                    if (!this.username.equals(username)) {
                        authenticationException = new UsernameNotFoundException("UsernameNotFoundException : " + error);
                    } else {
                        authenticationException = new BadCredentialsException("BadCredentialsException : " + error);
                    }

                    if (debug) {
                        logger.debug("Static Authentication request for user: " + username + " failed: ");
                    }

                    onUnsuccessfulAuthentication(request, response, authenticationException);
                    rememberMeServices.loginFail(request, response);
                    getAuthenticationEntryPoint().commence(request, response, authenticationException);
                    return;
                }

                // Authentication success
                if (debug) {
                    logger.debug("Authentication success: " + authentication.toString());
                }

                SecurityContextHolder.getContext().setAuthentication(authentication);
                onSuccessfulAuthentication(request, response, authentication);
            }
        } else {
            AuthenticationException authenticationException = new AuthenticationServiceException("Basic Authentication Mandatory....");
            SecurityContextHolder.getContext().setAuthentication(null);
            onUnsuccessfulAuthentication(request, response, authenticationException);
            rememberMeServices.loginFail(request, response);
            getAuthenticationEntryPoint().commence(request, response, authenticationException);
            return;

        }

        chain.doFilter(request, response);
    }

    private boolean authenticationIsRequired(String username) {
        // Only reauthenticate if username doesn't match SecurityContextHolder and user isn't authenticated
        // (see SEC-53)
        Authentication existingAuth = SecurityContextHolder.getContext().getAuthentication();

        if (existingAuth == null || !existingAuth.isAuthenticated()) {
            return true;
        }

        // Limit username comparison to providers which use usernames (ie UsernamePasswordAuthenticationToken)
        // (see SEC-348)

        if (existingAuth instanceof UsernamePasswordAuthenticationToken && !existingAuth.getName().equals(username)) {
            return true;
        }

        // Handle unusual condition where an AnonymousAuthenticationToken is already present
        // This shouldn't happen very often, as BasicProcessingFitler is meant to be earlier in the filter
        // chain than AnonymousAuthenticationFilter. Nevertheless, presence of both an AnonymousAuthenticationToken
        // together with a BASIC authentication request header should indicate reauthentication using the
        // BASIC protocol is desirable. This behaviour is also consistent with that provided by form and digest,
        // both of which force re-authentication if the respective header is detected (and in doing so replace
        // any existing AnonymousAuthenticationToken). See SEC-610.
        if (existingAuth instanceof AnonymousAuthenticationToken) {
            return true;
        }

        return false;
    }


}
