//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package fr.atexo.commun.document.libreoffice;

import fr.atexo.commun.exception.ConfigurationException;
import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;
import fr.atexo.commun.util.exec.ExecutionCommande.ExecutionResultat;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LibreOfficeConverteur {

    private static String SYNTAXE_COMMANDE = "${soffice.exe} --headless --convert-to pdf --outdir ${destinationDir} ${source}";

    public LibreOfficeConverteur() {
        
    }

    public static ExecutionResultat convertir(LibreOfficeConfiguration libreOfficeConfiguration, File fichierSource, File fichierDestination) throws IOException {
        ExecutionResultat resultat = null;
        if (libreOfficeConfiguration != null && libreOfficeConfiguration.getLibreOffice() != null) {
            if (fichierSource != null && fichierSource.exists()) {
                ExecutionCommande commande = new ExecutionCommande();
                commande.setCommandes(new String[]{"SPLIT:" + SYNTAXE_COMMANDE});
                Map<String, String> proprietesParDefaut = new HashMap();
                proprietesParDefaut.put("soffice.exe", libreOfficeConfiguration.getLibreOffice());
                proprietesParDefaut.put("source", fichierSource.getAbsolutePath());
                proprietesParDefaut.put("destinationDir", fichierDestination.getParentFile().getAbsolutePath());

                try {
                    resultat = commande.executer(proprietesParDefaut);
                } catch (Throwable throwable) {
                    throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, throwable);
                }

                String generatedFileName = FilenameUtils.getBaseName(fichierSource.getName()) + ".pdf";
                File generatedFile = new File(fichierDestination.getParentFile().getAbsolutePath() + File.separator + generatedFileName);

                if (!resultat.isExecuterAvecSucces()) {
                    throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
                } else if (!generatedFile.exists()) {
                    throw new ContenuIOException("Erreur lors de la transformation - le fichier de destination n'existe pas : \n" + resultat);
                } else {

                    if (fichierDestination.exists()) {
                        fichierDestination.delete();
                    }

                    generatedFile.renameTo(fichierDestination);
                    return resultat;
                }
            } else {
                throw new ContenuIOException("Erreur avec le fichier source : Celui-ci n'existe pas");
            }
        } else {
            throw new ConfigurationException("Erreur de configuration du service libreoffice : " + libreOfficeConfiguration.toString());
        }
    }
}
