package fr.atexo.commun.document.openoffice;

import org.artofsolving.jodconverter.document.DefaultDocumentFormatRegistry;
import org.artofsolving.jodconverter.document.DocumentFamily;
import org.artofsolving.jodconverter.document.DocumentFormat;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Classe référençant l'ensemble des formats de document supporté par le module
 * de conversion de documents.
 *
 * @author Louis Champion
 * @version $Revision$ $Date: 2013-03-14 09:15:28 +0100 (jeu., 14 mars
 *          2013) $
 */
public class AtexoDocumentFormatRegistry extends DefaultDocumentFormatRegistry {

    /**
     * Constructeur
     */
    public AtexoDocumentFormatRegistry() {
        super();

        final DocumentFormat xPdf = new DocumentFormat("Portable Document Format", "pdf", "application/x-pdf");
        initDocumentFormatPdf(xPdf);
        addFormat(xPdf);

        final DocumentFormat acrobatPdf = new DocumentFormat("Portable Document Format", "pdf", "application/acrobat");
        initDocumentFormatPdf(acrobatPdf);
        addFormat(acrobatPdf);

        final DocumentFormat vndPdf = new DocumentFormat("Portable Document Format", "pdf", "applications/vnd.pdf");
        initDocumentFormatPdf(vndPdf);
        addFormat(vndPdf);

        final DocumentFormat textPdf = new DocumentFormat("Portable Document Format", "pdf", "text/pdf");
        initDocumentFormatPdf(textPdf);
        addFormat(textPdf);

        final DocumentFormat textXpdf = new DocumentFormat("Portable Document Format", "pdf", "text/x-pdf");
        initDocumentFormatPdf(textXpdf);
        addFormat(textXpdf);

        // ajout support xlsx impliquant d'utiliser le service open office 3.2+
        final DocumentFormat xls = new DocumentFormat("Microsoft Excel 2007 XML", "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        xls.setInputFamily(DocumentFamily.SPREADSHEET);
        xls.setStoreProperties(DocumentFamily.SPREADSHEET, Collections.singletonMap("FilterName", "MS Excel 2007 XML"));
        addFormat(xls);

        // ajout support docx impliquant d'utiliser le service open office 3.2+
        final DocumentFormat mswordx = new DocumentFormat("Microsoft Word 2007 XML", "docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        mswordx.setInputFamily(DocumentFamily.TEXT);
        mswordx.setStoreProperties(DocumentFamily.TEXT, Collections.singletonMap("FilterName", "MS Word 2007 XML"));
        addFormat(mswordx);

        // ajout support pptx impliquant d'utiliser le service open office 3.2+
        final DocumentFormat mspowerpointx = new DocumentFormat("Microsoft PowerPoint 2007 XML", "pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        mspowerpointx.setInputFamily(DocumentFamily.PRESENTATION);
        mspowerpointx.setStoreProperties(DocumentFamily.PRESENTATION, Collections.singletonMap("FilterName", "MS PowerPoint 2007 XML"));
        addFormat(mspowerpointx);

        final DocumentFormat mspowerpoint = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/ms-powerpoint");
        initDocumentFormatPpt(mspowerpoint);
        addFormat(mspowerpoint);

        final DocumentFormat mspowerpnt = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/mspowerpnt");
        initDocumentFormatPpt(mspowerpnt);
        addFormat(mspowerpnt);

        final DocumentFormat vndmspowerpoint = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/vnd-mspowerpoint");
        initDocumentFormatPpt(vndmspowerpoint);
        addFormat(vndmspowerpoint);

        final DocumentFormat vndmsoffice = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/vnd.ms-office");
        initDocumentFormatPpt(vndmsoffice);
        addFormat(vndmsoffice);

        final DocumentFormat powerpoint = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/powerpoint");
        initDocumentFormatPpt(powerpoint);
        addFormat(powerpoint);

        final DocumentFormat xpowerpoint = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/x-powerpoint");
        initDocumentFormatPpt(xpowerpoint);
        addFormat(xpowerpoint);

        final DocumentFormat xm = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/x-m");
        initDocumentFormatPpt(xm);
        addFormat(xm);

        final DocumentFormat vndpowerpoint = new DocumentFormat("Microsoft PowerPoint", "ppt", "application/vnd.powerpoint");
        initDocumentFormatPpt(vndpowerpoint);
        addFormat(vndpowerpoint);

        final DocumentFormat png = new DocumentFormat("Portable Network Graphics", "png", "image/png");
        addFormat(png);

        final DocumentFormat jpg = new DocumentFormat("Joint Photographic Experts Group", "jpg", "image/jpeg");
        addFormat(jpg);

        final DocumentFormat jpeg = new DocumentFormat("Joint Photographic Experts Group", "jpeg", "image/jpeg");
        addFormat(jpeg);

    }

    protected void initDocumentFormatPpt(DocumentFormat ppt) {
        ppt.setInputFamily(DocumentFamily.PRESENTATION);
        ppt.setStoreProperties(DocumentFamily.PRESENTATION, Collections.singletonMap("FilterName", "MS PowerPoint 97"));
    }

    public Set<DocumentFormat> getFormatsByFileExtension(String extension) {
        if (extension == null) {
            return null;
        }

        Set<DocumentFormat> documentFormatSet = new HashSet<DocumentFormat>();

        String lowerExtension = extension.toLowerCase();

        for (Iterator it = getDocumentFormats().iterator(); it.hasNext(); ) {
            DocumentFormat format = (DocumentFormat) it.next();
            if (format.getExtension().equals(lowerExtension)) {
                documentFormatSet.add(format);
            }
        }
        return documentFormatSet;
    }

    public DocumentFormat getFormatByMimeType(Set<DocumentFormat> documentFormats, String contentType) {

        if (contentType == null || documentFormats == null || documentFormats.isEmpty()) {
            return null;
        }

        for (DocumentFormat documentFormat : documentFormats) {

            if (contentType.equals(documentFormat.getMediaType())) {
                return documentFormat;
            }
        }

        return null;
    }
}
