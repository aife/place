package fr.atexo.commun.document.openoffice;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.artofsolving.jodconverter.OfficeDocumentConverter;
import org.artofsolving.jodconverter.document.DocumentFormat;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.ExternalOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeConnectionException;
import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;
import org.artofsolving.jodconverter.office.OfficeManager;

import fr.atexo.commun.commande.CommandeConfiguration;
import fr.atexo.commun.commande.CommandeLanceur;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeInjoignableException;
import fr.atexo.commun.exception.openoffice.ServiceOpenOfficeRelanceException;

/**
 * Classe permettant de convertir un type de document en un autre type de
 * document en se connectant au service open office.
 * 
 * @author Louis Champion
 * @version $Revision$ $Date: 2013-02-18 15:13:32 +0100 (lun., 18 févr.
 *          2013) $
 */
public class OpenOfficeDocumentConverteur {

	private static final Log LOG = LogFactory.getLog(OpenOfficeDocumentConverteur.class);

	private static OfficeManager officeManager = null;

	private String commandeRelanceService;

	/**
	 * Initialise la connexion vers le service open office via le protocole
	 * défini..
	 * 
	 * @return
	 */
	public static OfficeManager getOfficeManager(OfficeConnectionProtocol protocol) {

		if (officeManager == null) {
			LOG.info("Initialisation de la connexion vers le service open office");
			officeManager = new DefaultOfficeManagerConfiguration().setConnectionProtocol(protocol).buildOfficeManager();
		}
		return officeManager;
	}

	public static OfficeManager getExternalOfficeManager(OfficeConnectionProtocol protocol, Integer port) {

		if (officeManager == null) {
			LOG.info("Initialisation de la connexion vers le service open office");
			officeManager = port != null ? new ExternalOfficeManagerConfiguration().setConnectionProtocol(protocol).setPortNumber(port).buildOfficeManager() : new ExternalOfficeManagerConfiguration().setConnectionProtocol(protocol).buildOfficeManager();
		}
		return officeManager;
	}

	/**
	 * Initialise la connexion vers le service open office
	 */
	public OpenOfficeDocumentConverteur(OfficeConnectionProtocol protocol, boolean externe, Integer port) {
		if (externe) {
			getExternalOfficeManager(protocol, port);
		} else {
			getOfficeManager(protocol);
		}
	}

	public void setCommandeRelanceService(String commandeRelanceService) {
		this.commandeRelanceService = commandeRelanceService;
	}

	/**
	 * Converti un type de document en un autre type de document en se
	 * connectant au service open office.
	 * 
	 * @param documentSource
	 * @param documentDestination
	 * @param documentDestinationFormat
	 * @throws ServiceOpenOfficeInjoignableException
	 * @throws ServiceOpenOfficeRelanceException
	 */
	public void convertirDocument(File documentSource, File documentDestination, DocumentFormat documentDestinationFormat) throws ServiceOpenOfficeInjoignableException, ServiceOpenOfficeRelanceException {
		convertirDocument(documentSource, documentDestination, documentDestinationFormat, false);
	}

	/**
	 * 
	 * Converti un type de document en un autre type de document en se
	 * connectant au service open office.
	 * 
	 * @param documentSource
	 * @param documentDestination
	 * @param documentDestinationFormat
	 * @param attendreRelance
	 * @throws ServiceOpenOfficeInjoignableException
	 * @throws ServiceOpenOfficeRelanceException
	 */
	public void convertirDocument(File documentSource, File documentDestination, DocumentFormat documentDestinationFormat, boolean attendreRelance) throws ServiceOpenOfficeInjoignableException, ServiceOpenOfficeRelanceException {

		LOG.info("Etablissement de la connexion vers le service open office");
		// connexion à l'instance du service open office
		try {
			officeManager.start();
		} catch (OfficeConnectionException officeConnectionException) {
			LOG.warn("Erreur de connexion au Service Open Office");
			if (StringUtils.isNotBlank(commandeRelanceService)) {
				LOG.error("Tentative de relance du service Open Office avec la commande : " + commandeRelanceService);
				CommandeConfiguration commandeConfiguration = new CommandeConfiguration(commandeRelanceService);
				try {
					CommandeLanceur.lancer(commandeConfiguration, attendreRelance);
					LOG.error("Le service Open office a été relancé avec succès...");
					throw new ServiceOpenOfficeRelanceException("Tentative de relance du Service Open Office via le scripts de relance : " + commandeRelanceService);
				} catch (IOException e) {
					LOG.error("Le service Open office n'a pas pu être relancé...");
					throw new ServiceOpenOfficeInjoignableException("Service Open Office impossible à relancer avec relance automatique configurée", e);
				}

			} else {
				throw new ServiceOpenOfficeInjoignableException("Service Open Office injoignable avec relance automatique non configurée", officeConnectionException);
			}
		}

		LOG.info("Conversion du document au  vers le format " + documentDestinationFormat.getExtension());
		// conversion du document
		OfficeDocumentConverter converter = new OfficeDocumentConverter(officeManager);
		converter.convert(documentSource, documentDestination, documentDestinationFormat);

		LOG.info("Clôture de la connexion vers le service open office");

		// clôture de la connexion
		officeManager.stop();
	}
}
