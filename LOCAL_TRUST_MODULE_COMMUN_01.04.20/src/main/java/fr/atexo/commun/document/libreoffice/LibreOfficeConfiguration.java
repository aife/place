//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package fr.atexo.commun.document.libreoffice;

public class LibreOfficeConfiguration {
    private String libroffice;

    public LibreOfficeConfiguration() {
    }

    public String getLibreOffice() {
        return this.libroffice;
    }

    public void setLibroffice(String libroffice) {
        this.libroffice = libroffice;
    }

    public String toString() {
        return "LibreOfficeConfiguration [libreoffice=" + this.libroffice + "]";
    }
}
