package fr.atexo.commun.document.openoffice.commande;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class OpenOfficeLanceur {

    private static String SYNTAXE_COMMANDE_SOCKET = "${soffice.exe} -accept=socket,host=localhost,port=${port}\\;urp\\;StarOffice.ServiceManager -headless -nocrashreport -nodefault -nofirststartwizard -nolockcheck -nologo -norestore &";

    private static String SYNTAXE_COMMANDE_PIPE = "${soffice.exe} -accept=pipe,name=office\\;urp\\;StarOffice.ServiceManager -headless -nocrashreport -nodefault -nofirststartwizard -nolockcheck -nologo -norestore &";

    public static ExecutionCommande.ExecutionResultat lancer(OpenOfficeConfiguration configuration) throws IOException {

        ExecutionCommande.ExecutionResultat resultat = null;

        if (configuration != null && configuration.getSoffice() != null && configuration.getProtocol() != null) {

            Map<String, String> proprietesParDefaut = new HashMap<String, String>();
            proprietesParDefaut.put("soffice.exe", configuration.getSoffice());
            ExecutionCommande commande = new ExecutionCommande();
            switch (configuration.getProtocol()) {
                case PIPE:
                    commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE_PIPE});
                    break;
                case SOCKET:
                    commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE_SOCKET});
                    proprietesParDefaut.put("port", String.valueOf(configuration.getPort()));
                    break;
            }

            // exécution de la commande
            try {
                resultat = commande.executer(proprietesParDefaut);
            }
            catch (Throwable e) {
                throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
            }

            // vérification
            if (!resultat.isExecuterAvecSucces()) {
                throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
            }
        }


        return resultat;
    }
}
