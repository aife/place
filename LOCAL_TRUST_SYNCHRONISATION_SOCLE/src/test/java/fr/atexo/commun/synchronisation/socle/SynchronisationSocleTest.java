package fr.atexo.commun.synchronisation.socle;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.xml.bind.JAXBException;

import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.commun.synchronisation.socle.beans.Transaction;
import junit.framework.TestCase;

/**
 * Test unitaire pour vérifier la connexion au WS mpe.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
@Ignore("Besoin du webservice accessible H24...")
public class SynchronisationSocleTest extends TestCase {

    private static String URL_WS = "http://local-cloud-marches.com/index.php5?soap=InterfacesSocleMPE";

    private static String JETON_SS0 = "50aa168654a7d";

    @Test
    public void testRecuperationTransactionEntities() throws JAXBException, UnsupportedEncodingException {

        SynchronisationSocleUtil synchronisationSocleUtil = new SynchronisationSocleUtil(URL_WS);
        Transaction transactionRequest = synchronisationSocleUtil.getTransactionRequestEntities();
        assertNotNull(transactionRequest);
        Transaction transactionResponse = synchronisationSocleUtil.getTransactionResponse(transactionRequest);
        assertNotNull(transactionResponse);
    }

    @Test
    public void testRecuperationTransactionAuthenticationToken() throws JAXBException, UnsupportedEncodingException {
        SynchronisationSocleUtil synchronisationSocleUtil = new SynchronisationSocleUtil(URL_WS);
        Transaction transactionRequest = synchronisationSocleUtil.getTransactionRequestAuthenticationToken(SynchronisationSocleUtil.AUTHENTIFICATION_TYPE_ENTITY,JETON_SS0);
        assertNotNull(transactionRequest);
        Transaction transactionResponse = synchronisationSocleUtil.getTransactionResponse(transactionRequest);
        assertNotNull(transactionResponse);
    }

    @Test
    public void testRecuperationTransactionCompanies() throws JAXBException, UnsupportedEncodingException {
        SynchronisationSocleUtil synchronisationSocleUtil = new SynchronisationSocleUtil(URL_WS);
        Transaction transactionRequest = synchronisationSocleUtil.getTransactionRequestCompanies(new Date(), true);
        assertNotNull(transactionRequest);
        Transaction transactionResponse = synchronisationSocleUtil.getTransactionResponse(transactionRequest);
        assertNotNull(transactionResponse);
    }

    @Test
    public void testRecuperationTransactionTenders() throws JAXBException, UnsupportedEncodingException {
        SynchronisationSocleUtil synchronisationSocleUtil = new SynchronisationSocleUtil(URL_WS);
        Transaction transactionRequest = synchronisationSocleUtil.getTransactionRequestTenders("AAPC");
        assertNotNull(transactionRequest);
        Transaction transactionResponse = synchronisationSocleUtil.getTransactionResponse(transactionRequest);
        assertNotNull(transactionResponse);
    }
}
