package fr.atexo.commun.springsecurity.authentification.filter;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.util.Assert;

import fr.atexo.commun.synchronisation.socle.SynchronisationSocleUtil;
import fr.atexo.commun.synchronisation.socle.beans.Transaction;

/**
 * Un filtre d'authentification permettant de récupérer un jeton "token" passé par le socle en get.
 * <p/>
 * La propriété <tt>tokenRequestGet</tt> est le nom du paramétre get de la requête contenant le jeton. Par défaut la valeur est sso.
 * <p/>
 * Si le parametre est manquant, <tt>getPreAuthenticatedPrincipal</tt> lancera une exception. On peut bypasser ce comportement en
 * surchargeant la propriété <tt>exceptionIfRequestGetMissing</tt>.
 *
 * @author Luke Taylor
 * @since 2.0
 */
public class SocleTokenAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    public static final String SPRING_SECURITY_LAST_USERNAME_KEY = "SPRING_SECURITY_LAST_USERNAME";

    public static final String SOCLE_SSO_TOKEN_KEY = "SOCLE_SSO_TOKEN";

    private String tokenRequestGet = "sso";

    private String tokenRequestSession = SOCLE_SSO_TOKEN_KEY;

    private String urlWebserviceSocle;

    private boolean continueIfRequestGetMissing = true;

    /**
     * Read and returns the header named by <tt>principalRequestHeader</tt> from the request.
     *
     * @throws org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException
     *          if the header is missing and <tt>exceptionIfHeaderMissing</tt>
     *          is set to <tt>true</tt>.
     */
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

        String principal = null;
        Date dateValidite = null;
        String tokenSession = null;
        String token = request.getParameter(tokenRequestGet);

        if (request.getSession() != null) {
            tokenSession = (String) request.getSession().getAttribute(tokenRequestSession);
        }

        if (token == null && tokenSession == null && continueIfRequestGetMissing) {
            if (logger.isErrorEnabled()) {
                logger.error("Aucune valeur ou aucun paramêtre" + tokenRequestGet + " contenant le jeton sso n'a été trouvé ");
            }
            return null;
        }

        if (token == null && tokenSession != null) {
            token = tokenSession;
        }

        if (token != null) {

            request.getSession().setAttribute(tokenRequestSession, token);

            SynchronisationSocleUtil synchronisationSocleUtil = new SynchronisationSocleUtil(urlWebserviceSocle);
            Transaction transactionRequest = synchronisationSocleUtil.getTransactionRequestAuthenticationToken(SynchronisationSocleUtil.AUTHENTIFICATION_TYPE_ENTITY, token, null);

            Transaction transactionResponse = synchronisationSocleUtil.getTransactionResponse(transactionRequest);

            if (transactionResponse != null && transactionResponse.getResponse() != null && transactionResponse.getResponse().getAuthentication() != null) {

                Transaction.Response.Authentication authentication = transactionResponse.getResponse().getAuthentication();
                if (authentication.getUser() != null) {
                    principal = authentication.getUser().getLogin();
                    if (authentication.getToken().getValidityDate() != null) {
                        dateValidite = authentication.getToken().getValidityDate().toGregorianCalendar().getTime();
                    }
                }
            }

            if (principal == null) {
                if (logger.isErrorEnabled()) {
                    logger.error("Le token sso " + token + " ne correspond à aucun utilisateur connu ou bien celui-ci est expiré");
                }
            }

            if (dateValidite != null && dateValidite.before(new Date())) {
                if (logger.isErrorEnabled()) {
                    logger.error("Le token sso " + token + " de l'utilisateur " + principal + " est expiré à la date du " + dateValidite);
                }
            }
        } else {
            if (logger.isErrorEnabled()) {
                logger.error("Aucun token sso disponible");
            }
        }


        return principal;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {
        super.successfulAuthentication(request, response, authResult);

        // Place the last username attempted into HttpSession for views
        HttpSession session = request.getSession(false);

        if (session != null) {
            // suppression du TextEscapeUtils.escapeEntities(username)
            request.getSession().setAttribute(SPRING_SECURITY_LAST_USERNAME_KEY, ((UserDetails) authResult.getPrincipal()).getUsername());
        }
    }

    /**
     * Credentials aren't usually applicable, but if a <tt>credentialsRequestHeader</tt> is set, this
     * will be read and used as the credentials value. Otherwise a dummy value will be used.
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "N/A";
    }

    public void setTokenRequestGet(String tokenRequestGet) {
        Assert.hasText(tokenRequestGet, "principalRequestGet must not be empty or null");
        this.tokenRequestGet = tokenRequestGet;
    }

    public void setUrlWebserviceSocle(String urlWebserviceSocle) {
        Assert.hasText(urlWebserviceSocle, "urlWebserviceSocle must not be empty or null");
        this.urlWebserviceSocle = urlWebserviceSocle;
    }

    /**
     * Defines whether an exception should be raised if the principal header is missing. Defaults to <tt>true</tt>.
     *
     * @param continueIfRequestGetMissing set to <tt>false</tt> to override the default behaviour and allow
     *                                    the request to proceed if no header is found.
     */
    public void setContinueIfRequestGetMissing(boolean continueIfRequestGetMissing) {
        this.continueIfRequestGetMissing = continueIfRequestGetMissing;
    }
}
