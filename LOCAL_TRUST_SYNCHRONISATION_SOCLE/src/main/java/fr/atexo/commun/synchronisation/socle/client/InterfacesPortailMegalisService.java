/**
 * InterfacesPortailMegalisService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package fr.atexo.commun.synchronisation.socle.client;

public interface InterfacesPortailMegalisService extends javax.xml.rpc.Service {

    public java.lang.String getInterfacesPortailMegalisPortAddress();

    public InterfacesPortailMegalisPortType getInterfacesPortailMegalisPort() throws javax.xml.rpc.ServiceException;

    public InterfacesPortailMegalisPortType getInterfacesPortailMegalisPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
