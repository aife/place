package fr.atexo.commun.synchronisation.socle;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.rpc.ServiceException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

import fr.atexo.commun.synchronisation.socle.beans.Category;
import fr.atexo.commun.synchronisation.socle.beans.ProcedureType;
import fr.atexo.commun.synchronisation.socle.beans.TenderStatus;
import fr.atexo.commun.synchronisation.socle.beans.Token;
import fr.atexo.commun.synchronisation.socle.beans.Transaction;
import fr.atexo.commun.synchronisation.socle.client.InterfacesPortailMegalisPortType;
import fr.atexo.commun.synchronisation.socle.client.InterfacesPortailMegalisServiceLocator;
import fr.atexo.commun.util.FichierTemporaireUtil;

/**
 * Classe d'utilitaire permettant de se connecter au Webservice du Socle et retournant la version instancié Objet du contenu du fichier
 * XML zippé renvoyé par le Webservice.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class SynchronisationSocleUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SynchronisationSocleUtil.class);

    private static final String PATH_JAXB_BEAN = "fr.atexo.commun.synchronisation.socle.beans";

    private static final String PREFIXE_NOM_FICHIER_REPONSE_SOCLE = "fichier_synchro_socle";

    public static final String ENCODING = "UTF-8";

    public static final String AUTHENTIFICATION_TYPE_ENTITY = "ENTITY";

    public static final String AUTHENTIFICATION_TYPE_COMPANY = "COMPANY";

    private String urlWebServiceSocle;

    private String repertoireTemporaire;

    /**
     * Constructeur.
     * Dans ce cas de figure le fichier XML sera crée dans le répertoire
     * temporaire défini par la variable système java.io.tmpdir.
     *
     * @param urlWebServiceSocle l'url du webservice
     */
    public SynchronisationSocleUtil(String urlWebServiceSocle) {
        this.urlWebServiceSocle = urlWebServiceSocle;
    }

    /**
     * Constructeur.
     * Dans ce cas de figure le fichier XML sera crée dans le répertoire spécifié.
     * Si le répertoire n'existe pas alors on tentera de créer le dit répertoire.
     *
     * @param repertoireTemporaire le chemin vers le répertoire temporaire devant stocké le fichier XML qui sera extrait du fichier zip
     * @param urlWebServiceSocle   l'url du webservice
     */
    public SynchronisationSocleUtil(String repertoireTemporaire, String urlWebServiceSocle) {
        this.repertoireTemporaire = repertoireTemporaire;
        this.urlWebServiceSocle = urlWebServiceSocle;
    }

    /**
     * Le chemin vers le répertoire temporaire.
     *
     * @return
     */
    public String getRepertoireTemporaire() {
        return repertoireTemporaire;
    }

    /**
     * L'url du webservice
     *
     * @return
     */
    public String getUrlWebServiceSocle() {
        return urlWebServiceSocle;
    }

    /**
     * Permet de créer un objet Transaction afin de récupérer une liste de Services / Agents / Représentants du Pouvoir Adjudicateur.
     * Cette requête permet de rechercher l'ensemble des données des annuaires des personnes publiques ayant été insérées ou modifiées depuis une date donnée.
     * Si la date est renseignée alors le serveur retourne l'ensemble des données qui ont été modifiées ou ajoutées depuis cette date, si elle n'est pas fournit
     * alors il retourne toutes les informations.La suppression de la données n'est pas gérée dans cette fonction.
     *
     * @param date              la date depuis laquelle on désire récupérer les versions qui ont été modifié
     * @param acronymeOrganisme identifiant de l'organisme
     * @return
     */
    public Transaction getTransactionRequestEntities(Date date, String acronymeOrganisme) {
        Transaction transaction = getTransactionRequest();
        transaction.getRequest().setEntities(new Transaction.Request.Entities());
        transaction.getRequest().getEntities().setOrganism(acronymeOrganisme);
        transaction.getRequest().getEntities().setFromDate(getXMLGregorianCalendar(date));
        return transaction;
    }

    public Transaction getTransactionRequestEntities() {
        return getTransactionRequestEntities(null, null);
    }

    /**
     * Cette requête permet de rechercher des annonces de consultations sur la plate-forme selon les critères passés en paramètres.
     * Si aucun critère n'est renseigné toutes les annonces en ligne et visibles des entreprises au moment de la recherche sont retournées.
     * Un critère dont le nom est inconnu sera ignoré. Si aucune consultation n'est trouvée pour les critères passés en paramètres alors aucun résultat n'est renvoyé.
     *
     * @param type              la valeur doit être fixée à <code>AAPC</code>
     * @param acronymeOrganisme identifiant de l'organisme
     * @param reference         référence de l'annonce que l'on souhaite rechercher
     * @param procedureType     type de la procédure sur laquelle doit porter la recherche (Appel d'offres ouvert, Marché négocié, etc.)
     * @param categorie         categorie de la consultation sur laquelle doit porter la recherche (Travaux, Fournitures, Services)
     * @param dateRemisePliMin  date de remise des plis minimale
     * @param dateRemisePliMax  date de remise des plis maximale
     * @param lieuExecution     lieu d'exécution sur laquelle doit porter la recherche, il peut s'agir d'une liste de code de départements (numéro du département alphanumérique)
     *                          ou d'un code pays (norme ISO-3166-ALPHA2)
     * @param cpv               codes cpv sur lesquelles doivent porter la recherche (Les valeurs doivent correspondrent à un numéro de code CPV (cf norme en vigueur).
     *                          Il faut mettre les valeurs les unes à la suite des autres séparées par virgules)
     * @param jetonSso          le jeton sso dans le cas d'une interconnexion avec la PF en socle (permet ensuite de vérifier les périmètres de vision et d'actions)
     * @param etatConsultation  état de la consultation (préparation, en ligne, etc.)
     * @return
     */
    public Transaction getTransactionRequestTenders(String type, String acronymeOrganisme, String reference, ProcedureType procedureType, Category categorie, Date dateRemisePliMin, Date dateRemisePliMax, String lieuExecution, String cpv, String jetonSso, TenderStatus etatConsultation) throws IllegalArgumentException {

        if (type != null) {
            Transaction transaction = getTransactionRequest();
            transaction.getRequest().setTender(new Transaction.Request.Tender());
            transaction.getRequest().getTender().setType(type);
            transaction.getRequest().getTender().setOrganism(acronymeOrganisme);
            transaction.getRequest().getTender().setReference(reference);
            transaction.getRequest().getTender().setProcedureType(procedureType);
            transaction.getRequest().getTender().setCategory(categorie);
            transaction.getRequest().getTender().setMinClosureDate(getXMLGregorianCalendar(dateRemisePliMin));
            transaction.getRequest().getTender().setMaxClosureDate(getXMLGregorianCalendar(dateRemisePliMax));
            transaction.getRequest().getTender().setDeliveryPlace(lieuExecution);
            transaction.getRequest().getTender().setSso(jetonSso);
            transaction.getRequest().getTender().setTenderStatus(etatConsultation);


            return transaction;
        } else {
            throw new IllegalArgumentException("Le paramétres type est obligatoire");
        }
    }

    public Transaction getTransactionRequestTenders(String type) throws IllegalArgumentException {
        return getTransactionRequestTenders(type, null, null, null, null, null, null, null, null, null, null);
    }

    /**
     * Cette requête permet de rechercher l'ensemble des données des annuaires des opérateurs économiques ayant été insérées ou modifiées
     * depuis une date donnée. Si la date est renseignée alors le serveur retourne l'ensemble des données qui ont été modifiées ou ajoutées
     * depuis cette date, si elle n'est pas fournit alors il retourne toutes les informations.
     * Cette requête ne concerne que les opérateurs économiques qui sont enregistrés au niveau de la plate-forme.
     *
     * @param date                    la date depuis laquelle on désire récupérer les versions qui ont été modifié
     * @param defensePortalRegistered valeur fixée à <code>true</code>
     * @return
     */
    public Transaction getTransactionRequestCompanies(Date date, boolean defensePortalRegistered) {
        Transaction transaction = getTransactionRequest();
        transaction.getRequest().setCompanies(new Transaction.Request.Companies());
        transaction.getRequest().getCompanies().setDefensePortalRegistered(new Boolean(defensePortalRegistered));
        transaction.getRequest().getCompanies().setFromDate(getXMLGregorianCalendar(date));
        return transaction;
    }

    public Transaction getTransactionRequestCompanies() {
        return getTransactionRequestCompanies(null, true);
    }

    /**
     * Cette requête permet d'authentifier un utilisateur via un login / mot de passe.
     *
     * @param type       <code>ENTITY</code> : utilisateur de type Acheteur rattaché à une entité d'achat
     *                   <code>COMPANY</code>: utilisateur de type opérateur économique rattaché à un établissement
     * @param login      le login de l'utilisateur
     * @param motDePasse le mot de passe de l'utilisateur
     * @return
     */
    public Transaction getTransactionRequestAuthenticationPassword(String type, String login, String motDePasse) throws IllegalArgumentException {

        if (type != null && login != null && motDePasse != null) {

            Transaction transaction = getTransactionRequestAuthentication(type);
            transaction.getRequest().getAuthentication().setPassword(new Transaction.Request.Authentication.Password());

            transaction.getRequest().getAuthentication().setToken(new Token());


            return transaction;
        } else {
            throw new IllegalArgumentException("Les paramétres type, login et motDePasse sont obligatoires");
        }
    }

    /**
     * Cette requête permet d'authentifiier un utilisateur via la validation d'un token.
     *
     * @param type         <code>ENTITY</code> : utilisateur de type Acheteur rattaché à une entité d'achat
     *                     <code>COMPANY</code>: utilisateur de type opérateur économique rattaché à un établissement
     * @param idJeton      identifiant unique du jeton
     * @param dateValidite date de validité maximale du jeton, si cette date est expirée alors le jeton n'est plus valide
     * @return
     */
    public Transaction getTransactionRequestAuthenticationToken(String type, String idJeton, Date dateValidite) throws IllegalArgumentException {
        if (type != null && idJeton != null) {
            Transaction transaction = getTransactionRequestAuthentication(type);
            transaction.getRequest().getAuthentication().setToken(new Token());
            transaction.getRequest().getAuthentication().getToken().setId(idJeton);
            transaction.getRequest().getAuthentication().getToken().setValidityDate(getXMLGregorianCalendar(dateValidite));

            return transaction;
        } else {
            throw new IllegalArgumentException("Les paramétres type, idJeton sont obligatoires");
        }
    }

    public Transaction getTransactionRequestAuthenticationToken(String type, String idJeton) throws IllegalArgumentException {
        return getTransactionRequestAuthenticationToken(type, idJeton, null);
    }

    private Transaction getTransactionRequestAuthentication(String type) {
        Transaction transaction = getTransactionRequest();
        transaction.getRequest().setAuthentication(new Transaction.Request.Authentication());
        transaction.getRequest().getAuthentication().setType(type);

        return transaction;
    }

    /**
     * La requete de transaction instanciée sans filtre.
     *
     * @return la requete de transaction vide.
     */
    public Transaction getTransactionRequest() {
        Transaction transaction = new Transaction();
        transaction.setRequest(new Transaction.Request());
        transaction.setVersion(new BigDecimal(1));
        transaction.setTimeStamp(getXMLGregorianCalendar(new Date()));
        return transaction;
    }

    public String getStringTransactionRequest(Transaction transactionRequest, boolean sortieFormatee) throws JAXBException, UnsupportedEncodingException {

        JAXBContext jc = JAXBContext.newInstance(PATH_JAXB_BEAN);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(sortieFormatee));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        marshaller.marshal(transactionRequest, outputStream);

        String resultat = outputStream.toString(ENCODING);

        return resultat;
    }

    /**
     * Récupére l'objet Transaction correspondant au retour de la requete transmise au Webservice.
     * Si une erreur à lieu lors de la récupération alors la transaction sera nulle et l'erreur associée sera logué.
     *
     * @param transactionRequest la requete transimise au Webservice
     * @return la transaction
     */
    public Transaction getTransactionResponse(Transaction transactionRequest) {
        Transaction response = null;

        if (urlWebServiceSocle != null) {

            InterfacesPortailMegalisServiceLocator locator = new InterfacesPortailMegalisServiceLocator();
            locator.setInterfacesPortailMegalisPortEndpointAddress(urlWebServiceSocle);

            FileOutputStream fileOutputStream = null;
            InputStream inputStream = null;
            BufferedInputStream bufferedInputStream = null;
            BufferedOutputStream bufferedOutputStream = null;
            ZipEntry entry = null;
            ZipInputStream zipInputStream = null;
            File file = null;
            try {
                InterfacesPortailMegalisPortType service = locator.getInterfacesPortailMegalisPort();
                String xmlRequestWs = getStringTransactionRequest(transactionRequest, false);

                if (xmlRequestWs != null) {

                    String result = service.processXml(xmlRequestWs);
                    
                    if (result != null) {

                        byte[] resultDecoded = Base64.decodeBase64(result);
                        inputStream = new ByteArrayInputStream(resultDecoded);
                        bufferedInputStream = new BufferedInputStream(inputStream);
                        zipInputStream = new ZipInputStream(bufferedInputStream);

                        try {

                            JAXBContext jc = JAXBContext.newInstance(PATH_JAXB_BEAN);
                            Unmarshaller unmarshaller = jc.createUnmarshaller();

                            while ((entry = zipInputStream.getNextEntry()) != null) {

                                int bufferSize = 2048;
                                int count;
                                byte data[] = new byte[bufferSize];

                                if (repertoireTemporaire == null) {
                                    file = FichierTemporaireUtil.creerFichierTemporaire(PREFIXE_NOM_FICHIER_REPONSE_SOCLE, ".xml");
                                } else {
                                    File repertoireTemporaireFile = new File(repertoireTemporaire);
                                    if (!repertoireTemporaireFile.exists()) {
                                        repertoireTemporaireFile.createNewFile();
                                    }
                                    file = FichierTemporaireUtil.creerFichierTemporaire(PREFIXE_NOM_FICHIER_REPONSE_SOCLE, ".xml", repertoireTemporaireFile);
                                }
                                fileOutputStream = new FileOutputStream(file);
                                bufferedOutputStream = new BufferedOutputStream(fileOutputStream, bufferSize);

                                while ((count = zipInputStream.read(data, 0, bufferSize)) != -1) {
                                    bufferedOutputStream.write(data, 0, count);
                                }
                                bufferedOutputStream.flush();
                                bufferedOutputStream.close();
                                fileOutputStream.close();

                                response = (Transaction) unmarshaller.unmarshal(file);

                                FileUtils.forceDelete(file);
                            }

                        } catch (FileNotFoundException e) {
                            LOG.error(e.getMessage(), e);
                        } catch (IOException e) {
                            LOG.error(e.getMessage(), e);
                        } finally {
                            IOUtils.closeQuietly(inputStream);
                            IOUtils.closeQuietly(bufferedInputStream);
                            IOUtils.closeQuietly(zipInputStream);
                            if (file != null && file.exists()) {
                                try {
                                    FileUtils.forceDelete(file);
                                } catch (IOException e) {
                                    LOG.error(e.getMessage(), e);
                                }
                            }
                        }

                    } else {
                        LOG.warn("Synchronisation avec le socle impossible car aucun contenu n'a été retourné");
                    }

                } else {
                    return null;
                }

            } catch (ServiceException e) {
                LOG.error(e.getMessage(), e);
            } catch (RemoteException e) {
                LOG.error(e.getMessage(), e);
            } catch (JAXBException e) {
                LOG.error(e.getMessage(), e);
            } catch (UnsupportedEncodingException e) {
                LOG.error(e.getMessage(), e);
            }
        } else {
            LOG.warn("Synchronisation avec le socle impossible car l'url du WS est non définie");
        }

        return response;
    }

    /**
     * Parse l'objet Transaction afin d'obtenir la liste des id des entity ayant une hiérarchie.
     *
     * @param transaction la transaction
     * @return la map contenant l'arborescence.
     */
    public Map<Integer, Set<Integer>> getHierarchieEntitiesIds(final Transaction transaction, boolean exclureSansEnfants) {

        Map<Integer, Set<Integer>> hierarchieEntities = new HashMap<Integer, Set<Integer>>();

        if (transaction != null && transaction.getResponse() != null) {

            Transaction.Response.Entities entities = transaction.getResponse().getEntities();
            if (entities != null) {
                List<Transaction.Response.Entities.Entity> entityList = entities.getEntity();
                for (Transaction.Response.Entities.Entity entity : entityList) {
                    Set<Integer> entityEnfants = hierarchieEntities.get(entity.getId());
                    if (entityEnfants == null) {
                        Set<Integer> enfants = new HashSet<Integer>();
                        hierarchieEntities.put(entity.getId(), enfants);
                    }

                    if (entity.getParentId() != 0) {
                        if (hierarchieEntities.get(entity.getParentId()) != null && !hierarchieEntities.get(entity.getParentId()).contains(entity.getId())) {
                            hierarchieEntities.get(entity.getParentId()).add(entity.getId());
                        } else {
                            Set<Integer> enfants = new HashSet<Integer>();
                            hierarchieEntities.put(entity.getParentId(), enfants);
                        }
                    }
                }

                if (exclureSansEnfants) {
                    Iterator<Map.Entry<Integer, Set<Integer>>> it = hierarchieEntities.entrySet().iterator();

                    while (it.hasNext()) {
                        Map.Entry<Integer, Set<Integer>> entry = it.next();
                        if (entry.getValue().isEmpty()) {
                            it.remove();
                        }
                    }
                }
            }
        }

        return hierarchieEntities;
    }

    /**
     * Parse l'objet Transaction afin de contruire l'arborescence compléte des entités.
     *
     * @param transaction la transaction
     * @return la map contenant l'arborescence.
     */
    public Map<Integer, Set<Transaction.Response.Entities.Entity>> getEntitiesHierarchie(final Transaction transaction, boolean exclureSansEnfants) {

        Map<Integer, Set<Transaction.Response.Entities.Entity>> hierarchieEntities = new HashMap<Integer, Set<Transaction.Response.Entities.Entity>>();

        if (transaction != null && transaction.getResponse() != null) {

            Transaction.Response.Entities entities = transaction.getResponse().getEntities();
            if (entities != null) {

                List<Transaction.Response.Entities.Entity> entityList = entities.getEntity();

                for (Transaction.Response.Entities.Entity entity : entityList) {

                    Set<Transaction.Response.Entities.Entity> entityEnfants = hierarchieEntities.get(entity.getId());

                    if (entityEnfants == null) {
                        Set<Transaction.Response.Entities.Entity> enfants = new HashSet<Transaction.Response.Entities.Entity>();
                        hierarchieEntities.put(entity.getId(), enfants);
                    }

                    if (entity.getParentId() != 0) {

                        Set<Transaction.Response.Entities.Entity> enfantsParent = hierarchieEntities.get(entity.getParentId());

                        if (enfantsParent == null) {
                            enfantsParent = new HashSet<Transaction.Response.Entities.Entity>();
                            hierarchieEntities.put(entity.getParentId(), enfantsParent);
                        }
                        enfantsParent.add(entity);
                    }
                }

                if (exclureSansEnfants) {
                    Iterator<Map.Entry<Integer, Set<Transaction.Response.Entities.Entity>>> it = hierarchieEntities.entrySet().iterator();

                    while (it.hasNext()) {
                        Map.Entry<Integer, Set<Transaction.Response.Entities.Entity>> entry = it.next();
                        if (entry.getValue().isEmpty()) {
                            it.remove();
                        }
                    }
                }
            }
        }

        return hierarchieEntities;
    }

    /**
     * Permet d'instancier un XMLGregorianCalendar à partir d'une Date.
     *
     * @param date la date
     * @return le XMLGregorianCalendar
     */
    public XMLGregorianCalendar getXMLGregorianCalendar(Date date) {
        XMLGregorianCalendar xmlCalendar = null;
        if (date != null) {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            xmlCalendar = new XMLGregorianCalendarImpl(calendar);
        }
        return xmlCalendar;
    }    
}
