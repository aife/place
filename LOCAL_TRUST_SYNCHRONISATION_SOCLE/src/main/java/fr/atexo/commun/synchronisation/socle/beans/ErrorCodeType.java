//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2012.04.20 at 10:56:40 AM CEST 
//


package fr.atexo.commun.synchronisation.socle.beans;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ErrorCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ErrorCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="TECHNICAL_ERROR"/>
 *     &lt;enumeration value="INVALID_MESSAGE"/>
 *     &lt;enumeration value="MISSING_PARAMETERS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ErrorCodeType")
@XmlEnum
public enum ErrorCodeType {

    TECHNICAL_ERROR,
    INVALID_MESSAGE,
    MISSING_PARAMETERS;

    public String value() {
        return name();
    }

    public static ErrorCodeType fromValue(String v) {
        return valueOf(v);
    }

}
