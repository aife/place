package fr.atexo.commun.synchronisation.socle.client;

public class InterfacesPortailMegalisPortTypeProxy implements InterfacesPortailMegalisPortType {

    private String _endpoint = null;

    private InterfacesPortailMegalisPortType interfacesPortailMegalisPortType = null;

    public InterfacesPortailMegalisPortTypeProxy() {
        _initInterfacesPortailMegalisPortTypeProxy();
    }

    public InterfacesPortailMegalisPortTypeProxy(String endpoint) {
        _endpoint = endpoint;
        _initInterfacesPortailMegalisPortTypeProxy();
    }

    private void _initInterfacesPortailMegalisPortTypeProxy() {
        try {
            interfacesPortailMegalisPortType = (new InterfacesPortailMegalisServiceLocator()).getInterfacesPortailMegalisPort();
            if (interfacesPortailMegalisPortType != null) {
                if (_endpoint != null)
                    ((javax.xml.rpc.Stub) interfacesPortailMegalisPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                else
                    _endpoint = (String) ((javax.xml.rpc.Stub) interfacesPortailMegalisPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
            }

        }
        catch (javax.xml.rpc.ServiceException serviceException) {
        }
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (interfacesPortailMegalisPortType != null)
            ((javax.xml.rpc.Stub) interfacesPortailMegalisPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

    }

    public InterfacesPortailMegalisPortType getInterfacesPortailMegalisPortType() {
        if (interfacesPortailMegalisPortType == null)
            _initInterfacesPortailMegalisPortTypeProxy();
        return interfacesPortailMegalisPortType;
    }

    public java.lang.String processXml(java.lang.String xml) throws java.rmi.RemoteException {
        if (interfacesPortailMegalisPortType == null)
            _initInterfacesPortailMegalisPortTypeProxy();
        return interfacesPortailMegalisPortType.processXml(xml);
    }


}