<html>
    <body>
        Fichier template_html_fr_FR.ftl.
        Bonjour <h1>${email.utilisateurPrenom} ${email.utilisateurNom}</h1>,<br />
        <p>Voici un exemple de contenu de mail daté du ${email.dateCreation?date}.</p>
        Cordialement,<br />
        ${email.expediteurCivilite} ${email.expediteurPrenom} ${email.expediteurNom}<br />
    </body>
</html>  