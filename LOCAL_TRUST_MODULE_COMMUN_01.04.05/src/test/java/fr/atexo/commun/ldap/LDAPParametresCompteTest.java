/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Test de l'objet {@link LDAPParametresCompte}
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
@Ignore
public class LDAPParametresCompteTest extends TestCase {

    @Test
    public void testClone() {
        LDAPParametresCompte c = new LDAPParametresCompte("test1", "test2");
        List<String> s1 = Arrays.asList("a", "b");
        List<String> s2 = Arrays.asList("c", "d");
        c.setAttributComptesRecuperes(s1);
        c.setObjectClasses(s2);
        LDAPParametresCompte clone = c.clone();
        assertEquals(clone.getRepertoireCompte(), "test1");
        assertEquals(clone.getRepertoireRole(), "test2");
        assertEquals(clone.getAttributComptesRecuperes(), s1);
        assertEquals(clone.getObjectClasses(), s2);
        assertFalse(clone.getAttributComptesRecuperes() == s1);
        assertFalse(clone.getObjectClasses() == s2);
    }

}
