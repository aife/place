/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Pour tester l'instance "VDN".
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public abstract class AbstractVDNInstanceLDAPServiceTest extends AbstractLDAPServiceTest {

    protected int getNombreTotalIdentifiants() {
        return 13;
    }

    protected String getLoginIdentifiantExistant() {
        return "administrateur";
    }

    protected String[] getProfilsIdentifiantsAvecProfils() {
        return new String[]{"SUB_ADMIN_COMPTES", "SUB_COMMISSION"};
    }

    protected int getNombreIdentifiantsAvecProfils() {
        return 2;
    }

    protected String[] getProfilsIdentifiantsAvecProfils2() {
        return new String[]{"SUB_COMMISSION"};
    }

    protected int getNombreIdentifiantsAvecProfils2() {
        return 2;
    }

    protected String getNomAttribut() {
        return "givenName";
    }

    protected String getValeurAttribut() {
        return "administrateur";
    }

    protected String[] getProfilsIdentifiantExistant() {
        return new String[]{"SUB_ADMIN_COMPTES", "SUB_COMMISSION"};
    }

    protected int getNombreProfilsIdendifiantExistant() {
        return 17;
    }

}
