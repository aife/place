/**
 * $Id$
 */
package fr.atexo.commun.ldap.simple;

import java.util.Arrays;
import java.util.List;

import org.junit.Ignore;

import fr.atexo.commun.ldap.AbstractTestInstanceLDAPServiceTest;
import fr.atexo.commun.ldap.LDAPParametresCompte;
import fr.atexo.commun.ldap.LDAPParametresConnexion;
import fr.atexo.commun.ldap.LDAPServiceImpl;

/**
 * Pour tester l'instance "Test" en java core.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
@Ignore
public class TestInstanceSimpleLDAPServiceTest extends AbstractTestInstanceLDAPServiceTest {

    protected LDAPParametresConnexion getParametresConnexion() {
        LDAPParametresConnexion l = new LDAPParametresConnexion("ldap://linuxserv3:389/", "cn=Manager,dc=fr,dc=atexo", "secret");
        l.setConnectPool("false");
        return l;
    }

    protected String getRepertoireCompte() {
        return "ou=users,dc=ldaptest,dc=fr,dc=atexo";
    }

    protected List<String> getAttributsComptesRecuperes() {
        return Arrays.asList("mail");
    }

    protected LDAPParametresCompte getParametresCompte() {
        LDAPParametresCompte p = new LDAPParametresCompte(getRepertoireCompte(), "ou=roles,dc=ldaptest,dc=fr,dc=atexo");
        p.getAttributComptesRecuperes().addAll(getAttributsComptesRecuperes());
        p.setChampCompteObjetRole("roleOccupant");
        p.setChampRoleObjetCompte("businessCategory");
        p.setChampIdentifiantCompte("uid");
        p.setFiltreCompte("(uid=*)");
        return p;
    }

    protected LDAPServiceImpl getService() {
        return new LDAPServiceImpl(getParametresConnexion(), getParametresCompte());
    }

}
