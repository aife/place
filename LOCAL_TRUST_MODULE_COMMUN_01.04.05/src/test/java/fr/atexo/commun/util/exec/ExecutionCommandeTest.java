package fr.atexo.commun.util.exec;

import fr.atexo.commun.commande.CommandeConfiguration;
import fr.atexo.commun.commande.CommandeLanceur;
import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe de test pour la conversion pour l'execution de commandes systèmes.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
@Ignore
public class ExecutionCommandeTest extends TestCase {

    @Test
    public void testExecutionCommandePdf2Swf() throws IOException {

        String commandeComplete = "${swf.exe} -f -t -G -T ${flashVersion} ${source} -o ${destination}";
        String binaire = "pdf2swf";
        String versionFlash = "9";

        ExecutionCommande commande = new ExecutionCommande();
        commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + commandeComplete});

        Map<String, String> proprietesParDefaut = new HashMap<String, String>();
        proprietesParDefaut.put("swf.exe", binaire);
        proprietesParDefaut.put("flashVersion", versionFlash);

        URL urlSource = ClasspathUtil.searchURL("fr/atexo/commun/util/exec/pdf.pdf");
        File fichierSource = null;
        try {
            fichierSource = new File(urlSource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fichierSource = new File(urlSource.getPath());
        }

        File fichierDestination = new File(FichierTemporaireUtil.getRepertoireTemporaire() + "/pdf.swf");

        proprietesParDefaut.put("source", fichierSource.getAbsolutePath());
        proprietesParDefaut.put("destination", fichierDestination.getAbsolutePath());

        // exécution de la commande
        ExecutionCommande.ExecutionResultat resultat = null;
        try {
            resultat = commande.executer(proprietesParDefaut);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        assertTrue(resultat.isExecuterAvecSucces());
        assertTrue(fichierDestination.exists() && fichierDestination.length() != 0);

        fichierDestination.delete();
        assertTrue(!fichierDestination.exists());
    }

    @Test
    public void testExecutionCommandeOpenOffice() throws IOException {

        String commande = "soffice -accept=socket,host=localhost,port=8100\\;urp\\;StarOffice.ServiceManager -headless -nocrashreport -nodefault -nofirststartwizard -nolockcheck -nologo -norestore";

        CommandeConfiguration commandeConfiguration = new CommandeConfiguration(commande);


        // exécution de la commande
        ExecutionCommande.ExecutionResultat resultat = null;
        try {
            resultat = CommandeLanceur.lancer(commandeConfiguration, false);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }

        assertTrue(resultat.isExecuterAvecSucces());
    }
}