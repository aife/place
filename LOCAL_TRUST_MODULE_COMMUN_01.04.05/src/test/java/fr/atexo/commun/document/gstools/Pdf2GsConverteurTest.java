package fr.atexo.commun.document.gstools;

import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import fr.atexo.commun.util.PropertiesUtil;
import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: bruno
 * Date: 26 oct. 2010
 * Time: 16:22:58
 * To change this template use File | Settings | File Templates.
 */
@Ignore
public class Pdf2GsConverteurTest extends TestCase {

    private static String CLEF_COMMANDE = "pdf2gs.command";
    private static String CLEF_PAGE_DEBUT = "pageDebut.param";
    private static String CLEF_PAGE_FIN = "pageFin.param";
    private static String CLEF_DEVICE = "device.nom_special";
    private static String CLEF_TEXT_ALPHA_BITS = "textAlphaBits.param";
    private static String CLEF_GRAPHICAL_ALPHA_BITS = "graphicalAlphaBits.param";

    @Test
    public void testConvertir() throws Exception {

        InputStream fichierProprietes = ClasspathUtil.searchResource("fr/atexo/commun/document/gstools/pdf2gs.properties");
        Properties proprietes = PropertiesUtil.getProprietes(fichierProprietes);

        URL urlSource = ClasspathUtil.searchURL("fr/atexo/commun/util/exec/pdf.pdf");
        File fichierSource = null;
        try {
            fichierSource = new File(urlSource.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            fichierSource = new File(urlSource.getPath());
        }

        Pdf2GsConfiguration pdf2GsConfiguration = new Pdf2GsConfiguration(proprietes.getProperty(CLEF_COMMANDE), Integer.parseInt(proprietes.getProperty(CLEF_PAGE_DEBUT)), Integer.parseInt(proprietes.getProperty(CLEF_PAGE_FIN)), proprietes.getProperty(CLEF_DEVICE), proprietes.getProperty(CLEF_TEXT_ALPHA_BITS), proprietes.getProperty(CLEF_GRAPHICAL_ALPHA_BITS));
        String nomFichierDestination = null;
        if (pdf2GsConfiguration.getPageFin() - pdf2GsConfiguration.getPageDebut() == 0) {
            nomFichierDestination = FichierTemporaireUtil.getRepertoireTemporaire() + "/pdf.png";
        } else if (pdf2GsConfiguration.getPageFin() - pdf2GsConfiguration.getPageDebut() >= 1) {
            nomFichierDestination = FichierTemporaireUtil.getRepertoireTemporaire() + "/pdf%d.png";
        } else {
            throw new Exception("Numero de la derniere page doit être plus grand que celui de la premiere page");
        }

        try {
            Pdf2GsConverteur pdf2GsConverteur = new Pdf2GsConverteur();
            pdf2GsConverteur.convertir(pdf2GsConfiguration, fichierSource, nomFichierDestination);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // assertTrue(fichierDestination.exists());

    }
}
    