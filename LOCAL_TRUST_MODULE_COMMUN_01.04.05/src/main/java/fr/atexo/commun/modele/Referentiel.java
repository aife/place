package fr.atexo.commun.modele;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
@MappedSuperclass
public class Referentiel<X extends Referentiel<X, Y>, Y extends ReferentielValeur<X, Y>> extends AbstractEntity {

    @Column(nullable = false)
    private String libelle;

    @Column(nullable = true, unique = true)
    private String code;

    private Boolean natif;

    @OneToMany(mappedBy = "referentiel", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @OrderBy("code ASC")
    private List<Y> listeReferentielValeurs;

    @ManyToOne(fetch = FetchType.LAZY)
    private X parent;

    private transient List<Y> referentielsValeursActifs;

    public Referentiel() {
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return this.libelle;
    }

    /**
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the listeReferentielValeurs
     */
    public List<Y> getListeReferentielValeurs() {
        if (listeReferentielValeurs == null) {
            listeReferentielValeurs = new ArrayList<Y>();
        }
        return this.listeReferentielValeurs;
    }

    /**
     * @param listeReferentielValeurs the listeReferentielValeurs to set
     */
    public void setListeReferentielValeurs(List<Y> listeReferentielValeurs) {
        this.listeReferentielValeurs = listeReferentielValeurs;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public X getParent() {
        return parent;
    }

    public void setParent(X parent) {
        this.parent = parent;
    }

    // transient
    public List<Y> getReferentielsValeursActifs() {
        if (referentielsValeursActifs == null) {
            referentielsValeursActifs = new ArrayList<Y>();
            for (Y r : getListeReferentielValeurs()) {
                if (r.isActif()) {
                    referentielsValeursActifs.add(r);
                }
            }
        }
        return referentielsValeursActifs;
    }


    public Boolean getNatif() {
        if (natif == null) {
            natif = false;
        }
        return natif;
    }

    public void setNatif(Boolean natif) {
        this.natif = natif;
    }
}
