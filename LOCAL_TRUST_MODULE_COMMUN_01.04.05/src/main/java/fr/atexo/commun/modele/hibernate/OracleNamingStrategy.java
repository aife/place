package fr.atexo.commun.modele.hibernate;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class OracleNamingStrategy extends AtexoNamingStrategy {

    static {
        RESERVED_KEYWORDS.add("DATE");
    }

    @Override
    protected String format(String className) {
        String name = className;
        int index = name.lastIndexOf('.');
        if (index != -1) {
            name = name.substring(index + 1);
        }
        return super.format(name);
    }
}
