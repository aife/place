/**
 * $Id$
 */
package fr.atexo.commun.modele.hibernate;

import org.hibernate.cfg.EJB3NamingStrategy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class AtexoNamingStrategy extends EJB3NamingStrategy {

    protected static final Set<String> RESERVED_KEYWORDS = new HashSet<String>();
    private static final Map<String, String> DIMINUTIVES_MAP = new HashMap<String, String>();
    private static final String SEPARATOR = "_";

    static {
        RESERVED_KEYWORDS.add("OPTION");

        DIMINUTIVES_MAP.put("PROPERTY", "PROP");
        DIMINUTIVES_MAP.put("VALUE", "VAL");
        DIMINUTIVES_MAP.put("COMPONENT", "COMP");
        DIMINUTIVES_MAP.put("DEFINITION", "DEF");
    }

    protected String format(String className) {
        if (className.length() == 0) {
            return className;
        }

        StringBuilder sb = new StringBuilder(className.length() + 5);
        sb.append(Character.toUpperCase(className.charAt(0)));
        for (int i = 1; i < className.length(); i++) {
            char c = className.charAt(i);
            if (c == '.') {
                //cas des entites Embedded
                sb.append(SEPARATOR);
            } else {
                if (Character.isUpperCase(c)) {
                    if (i - 1 > 0 && !Character.isUpperCase(className.charAt(i - 1))) {
                        sb.append(SEPARATOR);
                    }
                    sb.append(c);
                } else {
                    sb.append(Character.toUpperCase(c));
                }
            }
        }
        return sb.toString();
    }

    private String normalize(String className) {
        String result = format(className);

        for (Map.Entry<String, String> e : DIMINUTIVES_MAP.entrySet()) {
            result = result.replace(e.getKey(), e.getValue());
        }

        if (RESERVED_KEYWORDS.contains(result)) {
            result = result + SEPARATOR;
        }

        return result;
    }

    private boolean isToBeNormalized(String className) {
        return !className.contains(SEPARATOR);
    }

    @Override
    public String classToTableName(String className) {
        return isToBeNormalized(className) ? normalize(className) : className;
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        return isToBeNormalized(propertyName) ? normalize(propertyName) : propertyName;
    }

    @Override
    public String columnName(String columnName) {
        return isToBeNormalized(columnName) ? normalize(columnName) : columnName;
    }

    @Override
    public String tableName(String tableName) {
        return isToBeNormalized(tableName) ? normalize(tableName) : tableName;
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable,
                                      String associatedEntity, String associatedEntityTable,
                                      String propertyName) {
        return new StringBuilder(tableName(ownerEntityTable)).append("_")
                .append(tableName(propertyName != null ? propertyName : associatedEntityTable)
                ).toString();
    }

    @Override
    public String foreignKeyColumnName(String propertyName, String propertyEntityName,
                                       String propertyTableName, String referencedColumnName) {
        assert propertyTableName != null || propertyName != null;
        return referencedColumnName + "_" + normalize(propertyName == null ? propertyTableName : propertyName);
    }

    @Override
    public String logicalColumnName(String columnName, String propertyName) {
        assert columnName != null || propertyName != null;
        return normalize(columnName != null && columnName.length() != 0 ? columnName : propertyName);
    }

    @Override
    public String logicalCollectionTableName(String tableName, String ownerEntityTable,
                                             String associatedEntityTable, String propertyName) {
        if (tableName != null) {
            return tableName;
        } else {
            return new StringBuilder(tableName(ownerEntityTable)).append(SEPARATOR)
                    .append(tableName(propertyName != null ? propertyName : associatedEntityTable)
                    ).toString();
        }
    }

}
