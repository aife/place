package fr.atexo.commun.ws;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import fr.atexo.commun.exception.ws.WSBusinessException;
import fr.atexo.commun.exception.ws.WSTechnicalException;

import javax.ws.rs.core.MultivaluedMap;
import java.util.Map;

/**
 * Classe permettant d'executer un ws post et get
 */
public abstract class WSClientManager {

    public abstract WSClientContext getContextClient();

    /**
     * Cette methode permet d'executer un web service get.
     * Cette methode instancie un client jersey ensuite elle initialise l'uri utilisé pour l'appel du web service en
     * ajoutant les parametres d'authentification(login/mot de passe) et la liste des parametres.
     *
     * @param mimeType    : type de donnéres retournées
     * @param claz        : class de l'objet retourné
     * @param serviceName : nom du service web
     * @param methode     : methode appelée
     * @param paramsMap   : map contenant la liste des parameteres
     * @return
     * @throws fr.atexo.commun.exception.ws.WSBusinessException
     *
     * @throws fr.atexo.commun.exception.ws.WSTechnicalException
     *
     */
    public Object executeGetService(String mimeType, Class claz, String serviceName, String methode, Map<String, String> paramsMap) throws WSBusinessException, WSTechnicalException {

        Object response = null;
        try {
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource service = client.resource(getContextClient().getBaseURI());

            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            if (paramsMap != null) {
                for (String key : paramsMap.keySet()) {
                    params.add(key, paramsMap.get(key));
                }
            }

            //Ajouter login et mot de passe
            params.add("login", getContextClient().getLogin());
            params.add("motpasse", getContextClient().getPassword());

            response = service.path(serviceName).path(methode).queryParams(params).accept(mimeType).get(claz);
        } catch (Exception e) {
            throw new WSTechnicalException("Une erreur technique est  survenue lors de l'appel du service: " + serviceName + "/" + methode + "params :" + paramToString(paramsMap), e);
        }

        if (response == null) {
            throw new WSTechnicalException("Une erreur technique est  survenue lors de l'appel du service: " + serviceName + "/" + methode + "params :" + paramToString(paramsMap));
        }

        return response;
    }


    /**
     * Cette methode permet d'executer un web service post.
     * Cette methode instancie un client jersey ensuite elle initialise l'uri utilisé pour l'appel du web service en
     * ajoutant les parametres d'authentification(login/mot de passe) et l'objet envoyé dans le contenu de la requete
     *
     * @param mimeType    : type de donnéres retournées
     * @param claz        : class de l'objet retourné
     * @param serviceName : nom du service web
     * @param methode     : methode appelée
     * @param dto         : objet envoyé dans le contenu de la requete
     * @return
     * @throws fr.atexo.commun.exception.ws.WSBusinessException
     *
     * @throws fr.atexo.commun.exception.ws.WSTechnicalException
     *
     */

    public Object executePostService(String mimeType, Class claz, String serviceName, String methode, Object dto, Map<String, String> paramsMap) throws WSBusinessException, WSTechnicalException {

        Object response = null;
        try {
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource service = client.resource(getContextClient().getBaseURI());

            //Ajouter login et mot de passe
            MultivaluedMap<String, String> params = new MultivaluedMapImpl();
            if (paramsMap != null) {
                for (String key : paramsMap.keySet()) {
                    params.add(key, paramsMap.get(key));
                }
            }
            params.add("login", getContextClient().getLogin());
            params.add("motpasse", getContextClient().getPassword());


            response = service.path(serviceName).path(methode).queryParams(params).type(mimeType).post(claz, dto);
        } catch (Exception e) {
            throw new WSTechnicalException("Une erreur technique est  survenue lors de l'appel du service: " + serviceName + "/" + methode, e);
        }

        if (response == null) {
            throw new WSTechnicalException("Une erreur technique est  survenue lors de l'appel du service: " + serviceName + "/" + methode);
        }

        return response;
    }

    /**
     * Cette methode permet de transformer la liste des parametres en String
     *
     * @param paramsMap
     * @return
     */
    public String paramToString(Map<String, String> paramsMap) {
        String params = "";
        if (paramsMap != null) {
            for (String key : paramsMap.keySet()) {
                params += key + ":" + paramsMap.get(key) + " ";
            }
        }
        return params;
    }
}
