package fr.atexo.commun.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 * Classe utilitaire pour la génération de lignes / cellules dans excel.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class PoiUtil {

    public static HSSFRow duppliquerLigne(HSSFWorkbook workbook, HSSFSheet sheet, HSSFRow ligne, int numeroLigne, int nombresCellules, boolean style, boolean type) {

        HSSFRow nouvelleLigne = sheet.createRow(numeroLigne);

        HSSFCellStyle rowStyle = workbook.createCellStyle();
        if (rowStyle != null) {
            nouvelleLigne.setRowStyle(rowStyle);
        }
        nouvelleLigne.setHeight(ligne.getHeight());
        nouvelleLigne.setHeightInPoints(ligne.getHeightInPoints());
        nouvelleLigne.setZeroHeight(ligne.getZeroHeight());

        for (int j = 0; j < nombresCellules; j++) {

            HSSFCell nouvelleCellule = nouvelleLigne.createCell(j);

            HSSFCell ancienneCellule = ligne.getCell(j);

            if (style && ancienneCellule != null) {

                HSSFCellStyle ancienStyle = ancienneCellule.getCellStyle();
                HSSFCellStyle nouveauStyle = workbook.createCellStyle();

                if (ancienStyle != null) {
                    nouveauStyle.setAlignment(ancienStyle.getAlignment());
                    nouveauStyle.setVerticalAlignment(ancienStyle.getVerticalAlignment());

                    nouveauStyle.setBorderBottom(ancienStyle.getBorderBottom());
                    nouveauStyle.setBorderTop(ancienStyle.getBorderTop());
                    nouveauStyle.setBorderRight(ancienStyle.getBorderRight());
                    nouveauStyle.setBorderLeft(ancienStyle.getBorderLeft());

                    nouveauStyle.setBottomBorderColor(ancienStyle.getBottomBorderColor());
                    nouveauStyle.setTopBorderColor(ancienStyle.getTopBorderColor());
                    nouveauStyle.setRightBorderColor(ancienStyle.getRightBorderColor());
                    nouveauStyle.setLeftBorderColor(ancienStyle.getLeftBorderColor());
                    nouveauStyle.setFont(ancienStyle.getFont(workbook));

                    nouveauStyle.setDataFormat(ancienStyle.getDataFormat());

                    nouveauStyle.setFillBackgroundColor(ancienStyle.getFillBackgroundColor());
                    nouveauStyle.setFillForegroundColor(ancienStyle.getFillForegroundColor());
                    nouveauStyle.setFillPattern(ancienStyle.getFillPattern());

                    nouveauStyle.setHidden(ancienStyle.getHidden());
                    nouveauStyle.setIndention(ancienStyle.getIndention());

                    nouvelleCellule.setCellStyle((CellStyle) nouveauStyle);
                }

            }

            if (type && ancienneCellule != null) {
                nouvelleCellule.setCellType(ancienneCellule.getCellType());
            }
        }

        return nouvelleLigne;

    }

    private static HSSFCell genererCellule(HSSFRow ligne, int numeroColonne, Object valeur, boolean personnaliser, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {

        HSSFCell cellule = ligne.getCell(numeroColonne);

        if (cellule == null) {
            cellule = ligne.createCell(numeroColonne);
        }

        HSSFCellStyle style = cellule.getCellStyle();
        if (personnaliser && style != null) {
            style.setAlignment(alignement);
            style.setBorderRight(bordureDroit);
            style.setBorderLeft(bordureGauche);
            style.setBorderTop(bordureHaut);
            style.setBorderBottom(bordureBas);
        }

        if (valeur == null) {
            cellule.setCellType(Cell.CELL_TYPE_BLANK);
        }

        if (valeur instanceof String) {
            HSSFRichTextString text = new HSSFRichTextString((String) valeur);
            cellule.setCellValue(text);
        } else if (valeur instanceof Integer) {
            cellule.setCellValue((Integer) valeur);
        } else if (valeur instanceof Double) {
            cellule.setCellValue((Double) valeur);
        } else if (valeur instanceof Date) {
            cellule.setCellValue((Date) valeur);
        } else if (valeur instanceof Calendar) {
            cellule.setCellValue((Calendar) valeur);
        } else if (valeur instanceof Boolean) {
            cellule.setCellValue((Boolean) valeur);
        }

        return cellule;

    }

    public static HSSFCell genererCelluleString(HSSFRow ligne, int numeroColonne, String valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleString(HSSFRow ligne, int numeroColonne, String valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }

    public static HSSFCell genererCelluleInt(HSSFRow ligne, int numeroColonne, Integer valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleInt(HSSFRow ligne, int numeroColonne, Integer valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }

    public static HSSFCell genererCelluleDouble(HSSFRow ligne, int numeroColonne, Double valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleDouble(HSSFRow ligne, int numeroColonne, Double valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }

    public static HSSFCell genererCelluleCalendar(HSSFRow ligne, int numeroColonne, Calendar valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleCalendar(HSSFRow ligne, int numeroColonne, Calendar valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }

    public static HSSFCell genererCelluleDate(HSSFRow ligne, int numeroColonne, Date valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleDate(HSSFRow ligne, int numeroColonne, Date valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }

    public static HSSFCell genererCelluleBoolean(HSSFRow ligne, int numeroColonne, Boolean valeur) {
        return genererCellule(ligne, numeroColonne, valeur, false, HSSFCellStyle.ALIGN_GENERAL, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE, HSSFCellStyle.BORDER_NONE);
    }

    public static HSSFCell genererCelluleBoolean(HSSFRow ligne, int numeroColonne, Boolean valeur, short alignement, short bordureDroit, short bordureGauche, short bordureHaut, short bordureBas) {
        return genererCellule(ligne, numeroColonne, valeur, true, alignement, bordureDroit, bordureGauche, bordureHaut, bordureBas);
    }
}
