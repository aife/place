/**
 * $Id$
 */
package fr.atexo.commun.util;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class GeneriqueUtil {

    // déclaration du log
    private static final Log LOG = LogFactory.getLog(GeneriqueUtil.class);

    private static ThreadLocal<Map<MultiKey, DateFormat>> DATE_FORMAT_CACHE = new ThreadLocal<Map<MultiKey, DateFormat>>();
    private static ThreadLocal<Map<NumberFormatKey, NumberFormat>> NUMBER_FORMAT_CACHE = new ThreadLocal<Map<NumberFormatKey, NumberFormat>>();

    private final static class NumberFormatKey implements Serializable {

        private static final long serialVersionUID = 2037297636548592627L;

        private final Locale locale;
        private final int minDigit;
        private final int maxDigit;
        private final boolean groupingUsed;

        private final int hashCode;

        private NumberFormatKey(Locale locale, int minDigit, int maxDigit, boolean groupingUsed) {
            this.locale = locale;
            this.minDigit = minDigit;
            this.maxDigit = maxDigit;
            this.groupingUsed = groupingUsed;
            hashCode = calculateHashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            NumberFormatKey that = (NumberFormatKey) o;

            if (groupingUsed != that.groupingUsed) return false;
            if (maxDigit != that.maxDigit) return false;
            if (minDigit != that.minDigit) return false;
            if (!locale.equals(that.locale)) return false;

            return true;
        }

        private int calculateHashCode() {
            int result = locale.hashCode();
            result = 31 * result + minDigit;
            result = 31 * result + maxDigit;
            result = 31 * result + (groupingUsed ? 1 : 0);
            return result;
        }

        @Override
        public int hashCode() {
            return hashCode;
        }
    }

    /**
     * Méthode vérifiant si une Collection n'est pas vide
     *
     * @param l
     * @return boolean
     */
    @SuppressWarnings("unchecked")
    public static boolean isEmpty(Collection c) {
        return c == null || c.isEmpty();
    }

    /**
     * Méthode vérifiant si une chaîne de caractères n'est pas vide
     *
     * @param s
     * @return boolean
     */
    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0 || s.trim().length() == 0;
    }


    private static DateFormat getDateFormat(String pattern, Locale locale) {
        Map<MultiKey, DateFormat> map = DATE_FORMAT_CACHE.get();
        if (map == null) {
            map = new HashMap<MultiKey, DateFormat>();
            DATE_FORMAT_CACHE.set(map);
        }
        MultiKey key = new MultiKey(pattern, locale);
        DateFormat format = map.get(key);
        if (format == null) {
            format = new SimpleDateFormat(pattern, locale);
            map.put(key, format);
        }
        return format;
    }

    public static String formatSimpleDate(Date date) {
        DateFormat myDateFormat = getDateFormat("dd/MM/yyyy", Locale.FRENCH);
        return myDateFormat.format(date);
    }

    public static String formatSimpleTime(Time time) {
        DateFormat myDateFormat = getDateFormat("HH:mm", Locale.FRENCH);
        return myDateFormat.format(time);
    }

    public static String format(double d) {
        return getNumberFormat(2, 2, true).format(d);
    }

    private static NumberFormat getNumberFormat(int minFractionDigit, int maxFractionDigit, boolean groupinfUsed) {
        Map<NumberFormatKey, NumberFormat> map = NUMBER_FORMAT_CACHE.get();
        if (map == null) {
            map = new HashMap<NumberFormatKey, NumberFormat>();
            NUMBER_FORMAT_CACHE.set(map);
        }
        NumberFormatKey key = new NumberFormatKey(Locale.FRENCH, minFractionDigit, maxFractionDigit, true);
        NumberFormat format = map.get(key);
        if (format == null) {
            format = NumberFormat.getInstance(Locale.FRENCH);
            format.setMinimumFractionDigits(minFractionDigit);
            format.setMaximumFractionDigits(maxFractionDigit);
            format.setGroupingUsed(groupinfUsed);

            map.put(key, format);
        }
        return format;
    }

    private static NumberFormat getImportNumberFormat() {
        return getNumberFormat(0, 10, false);
    }

    public static String formatImport(double d) throws ParseException {
        return getImportNumberFormat().format(d);
    }

    public static double parseDouble(String stringToParse) throws ParseException {
        if (stringToParse == null) {
            return 0;
        }
        return getNumberFormat(2, 2, true).parse(stringToParse.replace(" ", "").replace(".", ",")).doubleValue();
    }

    public static Date parseSimpleDate(String date) {
        if (GeneriqueUtil.isEmpty(date)) {
            return null;
        }
        try {
            DateFormat myDateFormat = getDateFormat("dd/MM/yyyy", Locale.FRENCH);
            return myDateFormat.parse(date);
        }
        catch (ParseException e) {

            LOG.info(e.getMessage(), e);
            return null;
        }
    }

}
