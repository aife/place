package fr.atexo.commun.document.wkhtmltopdftools;

public class WkhtmltopdfConfiguration {

  private String wkhtmltopdf;



  public String getWkhtmltopdf() {
    return wkhtmltopdf;
  }

  public void setWkhtmltopdf(String wkhtmltopdf) {
    this.wkhtmltopdf = wkhtmltopdf;
  }

  @Override
  public String toString() {
    return "WkhtmltopdfConfiguration [wkhtmltopdf=" + wkhtmltopdf + "]";
  }



}
