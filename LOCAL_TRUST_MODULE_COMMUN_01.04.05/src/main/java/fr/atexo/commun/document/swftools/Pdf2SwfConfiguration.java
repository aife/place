package fr.atexo.commun.document.swftools;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class Pdf2SwfConfiguration {

    private String pdf2swf;
    private int versionFlash;

    public Pdf2SwfConfiguration() {
    }

    public Pdf2SwfConfiguration(String pdf2swf, int versionFlash) {
        this.pdf2swf = pdf2swf;
        this.versionFlash = versionFlash;
    }

    public String getPdf2swf() {
        return pdf2swf;
    }

    public void setPdf2swf(String pdf2swf) {
        this.pdf2swf = pdf2swf;
    }

    public int getVersionFlash() {
        return versionFlash;
    }

    public void setVersionFlash(int versionFlash) {
        this.versionFlash = versionFlash;
    }

    @Override
    public String toString() {
        return "pdf2swf=" + this.pdf2swf + " / versionFlash=" + this.versionFlash;
    }
}
