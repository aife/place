package fr.atexo.commun.document.openoffice.commande;

import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;

/**
 * Created by IntelliJ IDEA.
 * User: bruno
 * Date: 26 oct. 2010
 * Time: 15:40:34
 * To change this template use File | Settings | File Templates.
 */
public class OpenOfficeConfiguration {

    private String soffice;
    private OfficeConnectionProtocol protocol;
    private Integer port;

    public OpenOfficeConfiguration() {
    }

    public OpenOfficeConfiguration(String soffice, OfficeConnectionProtocol protocol, Integer port) {
        this.soffice = soffice;
        this.protocol = protocol;
        this.port = port;
    }

    public String getSoffice() {
        return soffice;
    }

    public void setSoffice(String soffice) {
        this.soffice = soffice;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public OfficeConnectionProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(OfficeConnectionProtocol protocol) {
        this.protocol = protocol;
    }

    @Override
    public String toString() {
        return "soffice=" + this.soffice + " / protocol=" + this.protocol;
    }
}
