/**
 * $Id$
 */
package fr.atexo.commun.excel;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class ExcelException extends RuntimeException {

	private static final long serialVersionUID = 2129001428276377075L;

	public ExcelException(Exception e) {
		super(e);
	}

}