package fr.atexo.commun.email;

import freemarker.template.TemplateException;
import org.apache.commons.mail.EmailException;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

/**
 * Interface du service d'envoi d'emails.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public interface EmailService {

    /**
     * Permet d'envoyer des emails (html,texte avec ou sans piece jointes)
     *
     * @param serveurSmtpConnexion les paramètres de connexion au serveur smtp d'envoi d'emails.
     * @param emails               les emails à envoyer.
     */
    void envoyerEmails(ServeurSmtpConnexion serveurSmtpConnexion, Email... emails) throws EmailException;
}
