package fr.atexo.commun.email.extraction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Part;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class EmailContenu {

    private String sujet;

    private Date dateEnvoi;

    private Date dateReception;

    private List<String> expediteurs = new ArrayList<String>();

    private List<String> destinataires = new ArrayList<String>();

    private List<String> destinatairesCopies = new ArrayList<String>();

    private List<String> destinatairesCopiesCaches = new ArrayList<String>();

    private Part corpTexte;

    private Part corpHtml;

    private Map<String, List<Part>> mapPiecesJointes = new HashMap<String, List<Part>>();

    public EmailContenu(String sujet, Date dateEnvoi, Date dateReception, List<String> destinataires, List<String> expediteurs) {
        this.sujet = sujet;
        this.dateEnvoi = dateEnvoi;
        this.dateReception = dateReception;
        this.destinataires = destinataires;
        this.expediteurs = expediteurs;
    }

    public Part getCorpHtml() {
        return corpHtml;
    }

    public void setCorpHtml(Part corpHtml) {
        this.corpHtml = corpHtml;
    }

    public Part getCorpTexte() {
        return corpTexte;
    }

    public void setCorpTexte(Part corpTexte) {
        this.corpTexte = corpTexte;
    }

    public Date getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public List<String> getDestinataires() {
        return destinataires;
    }

    public void setDestinataires(List<String> destinataires) {
        this.destinataires = destinataires;
    }

    public List<String> getDestinatairesCopies() {
        return destinatairesCopies;
    }

    public void setDestinatairesCopies(List<String> destinatairesCopies) {
        this.destinatairesCopies = destinatairesCopies;
    }

    public List<String> getDestinatairesCopiesCaches() {
        return destinatairesCopiesCaches;
    }

    public void setDestinatairesCopiesCaches(List<String> destinatairesCopiesCaches) {
        this.destinatairesCopiesCaches = destinatairesCopiesCaches;
    }

    public List<String> getExpediteurs() {
        return expediteurs;
    }

    public void setExpediteurs(List<String> expediteurs) {
        this.expediteurs = expediteurs;
    }

    public Map<String, List<Part>> getMapPiecesJointes() {
        return mapPiecesJointes;
    }

    public void setMapPiecesJointes(Map<String, List<Part>> mapPiecesJointes) {
        this.mapPiecesJointes = mapPiecesJointes;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }
}
