package fr.atexo.commun.email.extraction;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public enum EmailProtocolType {

    POP3("pop3", "pop3", 110, false), POP3S("pop3s", "pop3", 995, true),
    IMAP("imap", "imap", 143, false), IMAPS("imaps", "imap", 993, true);

    private String nom;

    private String protocol;

    private int portParDefaut;

    private boolean ssl;

    EmailProtocolType(String nom, String protocol, int portParDefaut, boolean ssl) {
        this.nom = nom;
        this.protocol = protocol;
        this.portParDefaut = portParDefaut;
        this.ssl = ssl;
    }

    public String getNom() {
        return nom;
    }

    public int getPortParDefaut() {
        return portParDefaut;
    }

    public String getProtocol() {
        return protocol;
    }

    public boolean isSsl() {
        return ssl;
    }
}
