package fr.atexo.commun.email;

import java.net.URL;

/**
 * Classe contenant les information sur la piece Jointe d'un email.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class EmailPieceJointe {

    /** Definition of the part being an attachment */
    public static final String ATTACHMENT = "attachment";

    /** Definition of the part being inline */
    public static final String  INLINE = "inline";
    
    /** The name of this attachment. */
    private String nom = "";

    /** The description of this attachment. */
    private String description = "";

    /** The path to this attachment (ie c:/path/to/file.jpg). */
    private String chemin = "";

    private URL url;

    private String disposition = EmailPieceJointe.ATTACHMENT;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }
}
