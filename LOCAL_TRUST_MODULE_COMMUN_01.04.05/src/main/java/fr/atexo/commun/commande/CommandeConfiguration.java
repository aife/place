package fr.atexo.commun.commande;

import org.artofsolving.jodconverter.office.OfficeConnectionProtocol;

/**
 * Created by IntelliJ IDEA.
 * User: bruno
 * Date: 26 oct. 2010
 * Time: 15:40:34
 * To change this template use File | Settings | File Templates.
 */
public class CommandeConfiguration {

    private String commande;

    public CommandeConfiguration() {
    }

    public CommandeConfiguration(String commande) {
        this.commande = commande;
    }

    public String getCommande() {
        return commande;
    }

    public void setCommande(String commande) {
        this.commande = commande;
    }

    @Override
    public String toString() {
        return "commande=" + this.commande;
    }
}
