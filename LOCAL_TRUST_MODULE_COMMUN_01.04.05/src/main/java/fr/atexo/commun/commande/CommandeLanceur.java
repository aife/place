package fr.atexo.commun.commande;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class CommandeLanceur {

    private static String SYNTAXE_COMMANDE = "${commande.exe}";

    public static ExecutionCommande.ExecutionResultat lancer(CommandeConfiguration configuration, boolean attendreFinExecution) throws IOException {

        ExecutionCommande.ExecutionResultat resultat = null;

        if (configuration != null && configuration.getCommande() != null) {

            Map<String, String> proprietesParDefaut = new HashMap<String, String>();
            proprietesParDefaut.put("commande.exe", configuration.getCommande());
            ExecutionCommande commande = new ExecutionCommande();
            commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE});
            commande.setAttendreFinExecution(attendreFinExecution);

            // exécution de la commande
            try {
                resultat = commande.executer(proprietesParDefaut);
            }
            catch (Throwable e) {
                throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
            }

            // vérification
            if (!resultat.isExecuterAvecSucces()) {
                throw new ContenuIOException("Erreur lors de l'exécution de la commande - statut d'erreur : \n" + resultat);
            }
        }


        return resultat;
    }
}
