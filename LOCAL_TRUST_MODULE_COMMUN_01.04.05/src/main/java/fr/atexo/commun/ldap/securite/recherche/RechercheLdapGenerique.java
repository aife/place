/*
 * This program is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software 
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this 
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html 
 * or from the Free Software Foundation, Inc., 
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2007 - 2009 Pentaho Corporation.  All rights reserved.
 *
 */
package fr.atexo.commun.ldap.securite.recherche;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchResult;

import org.apache.commons.collections.Transformer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ldap.core.ContextSource;
import org.springframework.util.Assert;

public class RechercheLdapGenerique implements RechercheLdap, InitializingBean {

    // ~ Static fields/initializers ============================================
    private static final Log logger = LogFactory.getLog(RechercheLdapGenerique.class);

    // ~ Instance fields =======================================================

    /**
     * Generates disposable instances of search parameters. Factory should be
     * thread-safe (i.e. no mutable instance variables).
     *
     * @see RechercheLdapParametres
     */
    private RechercheLdapParametresFactory rechercheLdapParametres;

    /**
     * Transforms LDAP search results into custom objects. Can be chained
     * together. Transformer should be thread-safe (i.e. no mutable instance
     * variables).
     */
    private Transformer resultats;

    /**
     * Transforms filter arguments before passing to the
     * <code>paramsFactory</code>. On such example is transforming a
     * <code>GrantedAuthority</code> instance into a <code>String</code>
     * instance for searching. Transformer should be thread-safe (i.e. no
     * mutable instance variables).
     * <p>
     * <strong>Note that depending on the matching rules of the directory
     * server, case in strings may or may not matter.</strong>
     * </p>
     */
    private Transformer filtreArguments;

    private ContextSource contexteSource;

    // ~ Constructors ==========================================================

    public RechercheLdapGenerique(final ContextSource contexteSource,
                                  final RechercheLdapParametresFactory rechercheLdapParametres) {
        this(contexteSource, rechercheLdapParametres, null, null);
    }

    public RechercheLdapGenerique(final ContextSource contexteSource,
                                  final RechercheLdapParametresFactory rechercheLdapParametres, final Transformer resultats) {
        this(contexteSource, rechercheLdapParametres, resultats, null);
    }

    public RechercheLdapGenerique(final ContextSource contexteSource,
                                  final RechercheLdapParametresFactory rechercheLdapParametres, final Transformer resultats,
                                  final Transformer filtreArguments) {
        super();
        this.contexteSource = contexteSource;
        this.rechercheLdapParametres = rechercheLdapParametres;
        this.resultats = resultats;
        this.filtreArguments = filtreArguments;
    }

    // ~ Methods ===============================================================

    public List recherche(final Object[] filtreArguments) {
        Object[] transformedArgs = filtreArguments;
        // transform the filterArgs
        if (null != this.filtreArguments) {
            transformedArgs = (Object[]) this.filtreArguments.transform(filtreArguments);
        }
        RechercheLdapParametres params = rechercheLdapParametres.creerParametres(transformedArgs);
        // use a set internally to store intermediate results
        Set results = new HashSet();
        NamingEnumeration matches = null;
        try {
            matches = contexteSource.getReadOnlyContext().search(params.getBase(), params.getFiltre(),
                    params.getFiltreArguments(), params.getControleRecherches());
        } catch (NamingException e1) {
            if (RechercheLdapGenerique.logger.isErrorEnabled()) {
                // TODO: Throw an exception here
                RechercheLdapGenerique.logger.error("Directory search failed", e1); //$NON-NLS-1$
            }
            return new ArrayList(results);
        }
        try {
            while (matches.hasMore()) {
                SearchResult result = (SearchResult) matches.next();
                if (null != resultats) {
                    results.addAll((Collection) resultats.transform(result));
                } else {
                    results.add(result);
                }
            }
        } catch (NamingException e) {
            if (RechercheLdapGenerique.logger.isErrorEnabled()) {
                // TODO: Throw an exception here
                RechercheLdapGenerique.logger.error("Enumerating directory search results failed", e); //$NON-NLS-1$
            }
        }
        return new ArrayList(results);
    }

    public void afterPropertiesSet() throws Exception {
        Assert.notNull(contexteSource);
        Assert.notNull(rechercheLdapParametres);
    }
}
