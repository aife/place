package fr.atexo.commun.ldap.securite;

import org.springframework.security.core.GrantedAuthority;

public interface IUtilisateurProfilListeService {

  /**
   * Returns all authorities known to the provider. Cannot return
   * <code>null</code>
   * @return the authorities (never <code>null</code>)
   */
  public GrantedAuthority[] getTousProfils();

  /**
   * Returns all user names known to the provider. Cannot return
   * <code>null</code>
   * @return the users (never <code>null</code>)
   */
  public String[] getTousUtilisateurs();

  /**
   * Returns all known users in the specified role. Cannot return
   * <code>null</code>
   * @param authority The authority to look users up by. Cannot be <code>null</code>
   * @return the users. (never <code>null</code>)
   */
  public String[] getUtilisateursPourProfil(GrantedAuthority authority);

  /**
   * Returns all authorities granted for a specified user.
   * @param username The name of the user to look up authorities for
   * @return the authorities. (Never <code>null</code>)
   */
  public GrantedAuthority[] getProfilsPourUtilisateur(String username);

}
