/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Représente les détails d'un compte, incluant ses profils et ses attributs supplémentaires
 * - dont la liste est définie par la propriété : {@link fr.atexo.commun.ldap.LDAPParametresCompte#getAttributComptesRecuperes}.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public final class DetailsCompte {

    private final IdentifiantCompte identifiantCompte;
    private final Set<String> profils;
    private final Map<String, AttributLDAP> attributs;

    DetailsCompte(IdentifiantCompte identifiantCompte) {
        this.identifiantCompte = identifiantCompte;
        profils = new HashSet<String>();
        attributs = new HashMap<String, AttributLDAP>();
    }

    void ajoutProfil(String profil) {
        profils.add(profil);
    }

    void ajoutAttribut(AttributLDAP attribut) {
        attributs.put(attribut.getNom(), attribut);
    }

    /**
     * Retourne l'identifiant du compte.
     *
     * @return L'identifiant du compte
     */
    public IdentifiantCompte getIdentifiantCompte() {
        return identifiantCompte;
    }

    /**
     * Retourne la liste des profils
     *
     * @return La liste des profils
     */
    public Set<String> getProfils() {
        return Collections.unmodifiableSet(profils);
    }

    /**
     * Retourne la valeur de l'attribut du compte
     *
     * @param nom le nom de l'attribut
     * @return la valeur de l'attribut
     */
    public String getAttribut(String nom) {
        AttributLDAP a = attributs.get(nom);
        return a == null ? null : a.getValeur();
    }

    /**
     * Retourne la liste des attributs du comptes.
     *
     * @return la liste des attributs du comptes
     */
    public Collection<AttributLDAP> getAttributs() {
        return attributs.values();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DetailsCompte that = (DetailsCompte) o;

        if (!identifiantCompte.equals(that.identifiantCompte)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return identifiantCompte.hashCode();
    }

    @Override
    public String toString() {
        return "DetailsCompte{"
                + "identifiantCompte="
                + identifiantCompte
                + ", profils=" + profils
                + ", attributs=" + attributs + '}';
    }
}
