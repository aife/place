/*
 * This program is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software 
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this 
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html 
 * or from the Free Software Foundation, Inc., 
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2007 - 2009 Pentaho Corporation.  All rights reserved.
 *
 */
package fr.atexo.commun.ldap.securite;

import java.util.*;

import fr.atexo.commun.ldap.securite.recherche.RechercheLdap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsService;
import org.springframework.util.Assert;

public class UtilisateurProfilListeService implements IUtilisateurProfilListeService, InitializingBean {

    // ~ Static fields/initializers ======================================================================================

    // ~ Instance fields =================================================================================================

    private RechercheLdap rechercheUtilisateurs;

    private RechercheLdap rechercheProfils;

    private RechercheLdap rechercheUtilisateursPourProfil;

    /**
     * Case-sensitive by default.
     */
    private Comparator<GrantedAuthority> grantedAuthorityComparator;

    /**
     * Case-sensitive by default.
     */
    private Comparator<String> usernameComparator;

    /**
     * Used only for <code>getAuthoritiesForUser</code>. This is preferred
     * over an <code>LdapSearch</code> in
     * <code>authoritiesForUserSearch</code> as it keeps roles returned by
     * <code>UserDetailsService</code> and roles returned by
     * <code>DefaultLdapUserRoleListService</code> consistent.
     */
    private LdapUserDetailsService userDetailsService;

    // ~ Constructors ====================================================================================================

    public UtilisateurProfilListeService() {
        super();
    }

    public UtilisateurProfilListeService(final Comparator<String> usernameComparator, final Comparator<GrantedAuthority> grantedAuthorityComparator) {
        super();
        this.usernameComparator = usernameComparator;
        this.grantedAuthorityComparator = grantedAuthorityComparator;
    }

    // ~ Methods =========================================================================================================

    public void afterPropertiesSet() throws Exception {
    }

    public GrantedAuthority[] getTousProfils() {
        List<GrantedAuthority> results = rechercheProfils.recherche(new Object[0]);
        if (null != grantedAuthorityComparator) {
            Collections.sort(results, grantedAuthorityComparator);
        }
        return results.toArray(new GrantedAuthority[0]);
    }

    public String[] getTousUtilisateurs() {
        List<String> results = rechercheUtilisateurs.recherche(new Object[0]);
        if (null != usernameComparator) {
            Collections.sort(results, usernameComparator);
        }
        return results.toArray(new String[0]);
    }

    public String[] getUtilisateursPourProfil(final GrantedAuthority authority) {
        List<String> results = rechercheUtilisateursPourProfil.recherche(new Object[]{authority});
        if (null != usernameComparator) {
            Collections.sort(results, usernameComparator);
        }
        return results.toArray(new String[0]);
    }

    public GrantedAuthority[] getProfilsPourUtilisateur(final String username) {
        UserDetails user = userDetailsService.loadUserByUsername(username);
        List<GrantedAuthority> results = (List<GrantedAuthority>) user.getAuthorities();
        if (null != grantedAuthorityComparator) {
            Collections.sort(results, grantedAuthorityComparator);
        }
        return results.toArray(new GrantedAuthority[0]);
    }

    public void setAllUsernamesSearch(final RechercheLdap allUsernamesRecherche) {
        this.rechercheUtilisateurs = allUsernamesRecherche;
    }

    public void setAllAuthoritiesSearch(final RechercheLdap allAuthoritiesRecherche) {
        this.rechercheProfils = allAuthoritiesRecherche;
    }

    public void setUsernamesInRoleSearch(final RechercheLdap usernamesInRoleRecherche) {
        this.rechercheUtilisateursPourProfil = usernamesInRoleRecherche;
    }

    public void setUserDetailsService(final LdapUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public void setGrantedAuthorityComparator(final Comparator<GrantedAuthority> grantedAuthorityComparator) {
        Assert.notNull(grantedAuthorityComparator);
        this.grantedAuthorityComparator = grantedAuthorityComparator;
    }

    public void setUsernameComparator(final Comparator<String> usernameComparator) {
        Assert.notNull(usernameComparator);
        this.usernameComparator = usernameComparator;
    }

}
