/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * L'implémentation du service qui permet d'accéder aux fonctions d'identification, de modification
 * et de récupération d'information à partir de l'annuaire LDAP.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public class LDAPServiceImpl implements LDAPService, Serializable {

    private static final long serialVersionUID = -2426243339086898962L;

    private static final Log LOG = LogFactory.getLog(LDAPServiceImpl.class);

    private final LDAPParametresConnexion parametresConnexion;
    private final LDAPParametresCompte parametresCompte;

    /**
     * Permet de créer une nouvelle instance du service.
     *
     * @param parametresConnexion les paramètres de connexion
     * @param parametresCompte    les paramètres du compte
     */
    public LDAPServiceImpl(LDAPParametresConnexion parametresConnexion, LDAPParametresCompte parametresCompte) {
        this.parametresConnexion = parametresConnexion;
        this.parametresCompte = parametresCompte;
    }

    /**
     * {@inheritDoc}
     */
    public LDAPParametresCompte getParametresCompte() {
        return parametresCompte;
    }

    /**
     * {@inheritDoc}
     */
    public LDAPParametresConnexion getParametresConnexion() {
        return parametresConnexion;
    }

    private Hashtable<String, String> getEnvironnement() {
        Hashtable<String, String> environnement = new Hashtable<String, String>();
        environnement.put(Context.INITIAL_CONTEXT_FACTORY, parametresConnexion.getInitialContextFactory());
        environnement.put(Context.PROVIDER_URL, parametresConnexion.getUrl());
        environnement.put(Context.SECURITY_AUTHENTICATION, parametresConnexion.getSecurityAuthentification());
        environnement.put(Context.SECURITY_PRINCIPAL, parametresConnexion.getSecurityPrincipal());
        environnement.put(Context.SECURITY_CREDENTIALS, parametresConnexion.getSecurityCredentials());
        environnement.put("com.sun.jndi.ldap.connect.pool", parametresConnexion.getConnectPool());
        if (parametresConnexion.isAd()) { 
            environnement.put(Context.REFERRAL, "follow");
        }
        if (parametresConnexion.isSsl()) {
            environnement.put(Context.SECURITY_PROTOCOL, "ssl");
        }
        if (parametresConnexion.isIgnorerCertificat()) {
            environnement.put("java.naming.ldap.factory.socket", "fr.atexo.commun.ldap.ssl.SSLSocketFactoryVide");
        }
        return environnement;
    }

    private boolean existe(IdentifiantCompte id) {
        DirContext ctx = null;
        try {
            ctx = new InitialDirContext(getEnvironnement());
            try {
                SearchControls constraints = new SearchControls();
                constraints.setReturningAttributes(new String[0]);
                constraints.setSearchScope(SearchControls.OBJECT_SCOPE);
                NamingEnumeration results = ctx.search(id.getCheminComplet(), "(" + parametresCompte.getChampObjectClass() + "=*)", constraints);
                return results != null && results.hasMore();
            }
            finally {
                ctx.close();
            }
        }
        catch (NamingException e) {
            LOG.debug(e.getMessage(), e);
            return false;
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean login(IdentifiantCompte id, String motDePasse) {
        DirContext ctx = null;
        try {
            Hashtable<String, String> environnement = getEnvironnement();
            environnement.put(Context.SECURITY_PRINCIPAL, id.getCheminComplet());
            environnement.put(Context.SECURITY_CREDENTIALS, motDePasse);
            ctx = new InitialDirContext(environnement);
            return true;
        }
        catch (Exception e) {
            LOG.debug(e.getMessage(), e);
            return false;
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void creerCompte(IdentifiantCompte id, String motDePasse, AttributLDAP... attributs)
            throws NamingException, CompteExistantException {
        DirContext ctx = null;
        try {
            if (existe(id)) {
                throw new CompteExistantException(id.getLogin());
            }

            ctx = new InitialDirContext(getEnvironnement());
            BasicAttributes attrs = new BasicAttributes();

            BasicAttribute ocs = new BasicAttribute(parametresCompte.getChampObjectClass());
            for (String s : parametresCompte.getObjectClasses()) {
                ocs.add(s);
            }
            attrs.put(ocs);

            if (!parametresConnexion.isAd()) {
                BasicAttribute pwd = new BasicAttribute(parametresCompte.getChampMotDePasse(), motDePasse);
                attrs.put(pwd);
            }

            for (AttributLDAP a : attributs) {
                BasicAttribute gn = new BasicAttribute(a.getNom(), a.getValeur());
                attrs.put(gn);
            }

            ctx.createSubcontext(id.getCheminComplet(), attrs);

            //modification du mot de passe à posteriori pour AD
            if (parametresConnexion.isAd()) {
                try {
                    modifierCompte(id, motDePasse);
                } catch (CompteNonExistantException e) {
                    //ne devrait pas arriver
                    LOG.error(e.getMessage(), e);
                }
            }
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void supprimerCompte(IdentifiantCompte id) throws NamingException, CompteNonExistantException {
        if (!existe(id)) {
            throw new CompteNonExistantException(id.getLogin());
        }
        DirContext ctx = new InitialDirContext(getEnvironnement());
        ctx.unbind(id.getCheminComplet());
    }

    /**
     * {@inheritDoc}
     */
    public void modifierCompte(IdentifiantCompte id, AttributLDAP... attributsMisAJour) throws NamingException, CompteNonExistantException {
        modifierCompte(id, null, attributsMisAJour);
    }

    /**
     * {@inheritDoc}
     */
    public void modifierCompte(IdentifiantCompte id, String nouveauMotDePasse,
                               AttributLDAP... attributsMisAJour) throws NamingException, CompteNonExistantException {

        DirContext ctx = null;
        try {
            if (!existe(id)) {
                throw new CompteNonExistantException(id.getLogin());
            }

            //on met à jour le mot de passe et les attributs
            ctx = new InitialDirContext(getEnvironnement());
            List<ModificationItem> mods = new ArrayList<ModificationItem>();
            for (AttributLDAP a : attributsMisAJour) {
                mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(a.getNom(), a.getValeur())));
            }
            if (nouveauMotDePasse != null) {

                if (parametresConnexion.isAd()) {
                    String newQuotedPassword = "\"" + nouveauMotDePasse + "\"";
                    try {
                        byte[] newUnicodePassword = newQuotedPassword.getBytes("UTF-16LE");
                        mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                                new BasicAttribute(parametresCompte.getChampMotDePasse(), newUnicodePassword)));
                    }
                    catch (UnsupportedEncodingException e) {
                        LOG.error(e.getMessage(), e);
                    }
                } else {
                    mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                            new BasicAttribute(parametresCompte.getChampMotDePasse(), nouveauMotDePasse)));
                }
            }
            if (parametresConnexion.isAd()) {
                //Correction de Mantis 0005479 (NCA) :
                //il faut que le creation du compte dans ADAM positionne l'attribut msDS-UserAccountDisabled à FALSE. Il est par défaut à TRUE
                mods.add(new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("msDS-UserAccountDisabled", "FALSE")));
            }

            ctx.modifyAttributes(id.getCheminComplet(), mods.toArray(new ModificationItem[mods.size()]));
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<IdentifiantCompte> getIdentifiants() {
        return getIdentifiants(parametresCompte.getFiltreCompte());
    }

    /**
     * {@inheritDoc}
     */
    public List<IdentifiantCompte> getIdentifiants(String filtre) {
        List<IdentifiantCompte> result = new ArrayList<IdentifiantCompte>();
        DirContext ctx = null;
        try {

            ctx = new InitialDirContext(getEnvironnement());
            SearchControls ctls = new SearchControls();
            ctls.setReturningAttributes(new String[]{parametresCompte.getChampIdentifiantCompte()});
            ctls.setSearchScope(parametresCompte.getProfondeurRecherche());

            NamingEnumeration results = ctx.search(parametresCompte.getRepertoireCompte(), filtre, ctls);
            while (results != null && results.hasMore()) {
                SearchResult sr = (SearchResult) results.next();
                BasicAttribute a = (BasicAttribute) sr.getAttributes().get(parametresCompte.getChampIdentifiantCompte());
                result.add(new IdentifiantCompte(sr.getNameInNamespace(), String.valueOf(a.get())));
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
        return result;
    }

    public IdentifiantCompte getIdentifiant(String filtre) {
        List<IdentifiantCompte> identifiantComptes = getIdentifiants(filtre);
        return identifiantComptes != null && !identifiantComptes.isEmpty() ? identifiantComptes.get(0) : null;
    }

    public IdentifiantCompte getIdentifiant() {
        List<IdentifiantCompte> identifiantComptes = getIdentifiants();
        return identifiantComptes != null && !identifiantComptes.isEmpty() ? identifiantComptes.get(0) : null;
    }

    /**
     * {@inheritDoc}
     */
    public List<DetailsCompte> getDetailsComptes() {
        List<IdentifiantCompte> ids = getIdentifiants();
        List<DetailsCompte> details = new ArrayList<DetailsCompte>(ids.size());
        for (IdentifiantCompte id : ids) {
            details.add(getDetailsCompte(id));
        }
        return details;
    }

    private List<IdentifiantCompte> getIdentifiantsDuProfil(String profil) {
        List<IdentifiantCompte> result = new ArrayList<IdentifiantCompte>();
        DirContext ctx = null;
        try {

            ctx = new InitialDirContext(getEnvironnement());
            SearchControls ctls = new SearchControls();
            String[] returningAttributs = new String[]{parametresCompte.getChampCompteObjetRole()};
            ctls.setReturningAttributes(returningAttributs);
            ctls.setSearchScope(SearchControls.OBJECT_SCOPE);

            NamingEnumeration results = ctx.search(parametresCompte.getChampIdentifiantRole() + "=" + profil + "," + parametresCompte.getRepertoireRole(), parametresCompte.getFiltreRole(), ctls);
            while (results != null && results.hasMore()) {
                SearchResult sr = (SearchResult) results.next();
                //récupération du rôle
                BasicAttribute a = (BasicAttribute) sr.getAttributes().get(parametresCompte.getChampCompteObjetRole());
                if (a != null) {
                    for (int i = 0; i < a.size(); i++) {
                        String r = String.valueOf(a.get(i));
                        result.add(new IdentifiantCompte(r));
                    }
                }
            }
        } catch (NamingException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public List<IdentifiantCompte> getIdentifiantsRestreintsAuxProfils(String... profils) {
        Set<IdentifiantCompte> result = new HashSet<IdentifiantCompte>();
        for (String p : profils) {
            result.addAll(getIdentifiantsDuProfil(p));
        }
        return new ArrayList<IdentifiantCompte>(result);
    }

    /**
     * {@inheritDoc}
     */
    public List<DetailsCompte> getDetailsRestreintsAuxProfils(String... profils) {
        Map<IdentifiantCompte, DetailsCompte> map = new HashMap<IdentifiantCompte, DetailsCompte>();
        for (String p : profils) {
            List<IdentifiantCompte> ids = getIdentifiantsDuProfil(p);
            for (IdentifiantCompte id : ids) {
                DetailsCompte details = map.get(id);
                if (details == null) {
                    details = getDetailsCompte(id);
                    map.put(id, details);
                }
                details.ajoutProfil(p);
            }
        }
        return new ArrayList<DetailsCompte>(map.values());
    }

    /**
     * {@inheritDoc}
     */
    public DetailsCompte getDetailsCompte(IdentifiantCompte id, String... attributsSupplementaires) {
        DetailsCompte result = new DetailsCompte(id);
        DirContext ctx = null;
        try {

            ctx = new InitialDirContext(getEnvironnement());
            SearchControls ctls = new SearchControls();
            String[] returningAttributs = new String[attributsSupplementaires.length + parametresCompte.getAttributComptesRecuperes().size() + 1];
            returningAttributs[0] = parametresCompte.getChampRoleObjetCompte();
            List<String> parametres = parametresCompte.getAttributComptesRecuperes();
            System.arraycopy(parametres.toArray(new String[parametres.size()]), 0, returningAttributs, 1, parametres.size());
            System.arraycopy(attributsSupplementaires, 0, returningAttributs, returningAttributs.length - attributsSupplementaires.length, attributsSupplementaires.length);
            ctls.setReturningAttributes(returningAttributs);
            ctls.setSearchScope(SearchControls.OBJECT_SCOPE);

            NamingEnumeration results = ctx.search(id.getCheminComplet(), parametresCompte.getFiltreCompte(), ctls);
            while (results != null && results.hasMore()) {
                SearchResult sr = (SearchResult) results.next();
                //récupération du rôle
                BasicAttribute a = (BasicAttribute) sr.getAttributes().get(parametresCompte.getChampRoleObjetCompte());
                if (a != null) {
                    for (int i = 0; i < a.size(); i++) {
                        String r = String.valueOf(a.get(i));
                        r = r.substring(r.indexOf("=") + 1, r.indexOf(','));
                        result.ajoutProfil(r);
                    }
                }

                //récupération des attributs
                for (String at : parametres) {
                    BasicAttribute ba = (BasicAttribute) sr.getAttributes().get(at);
                    if (ba != null) {
                        String s = String.valueOf(ba.get());
                        if (s != null) {
                            result.ajoutAttribut(new AttributLDAP(at, s));
                        }
                    }
                }
                for (String at : attributsSupplementaires) {
                    BasicAttribute ba = (BasicAttribute) sr.getAttributes().get(at);
                    if (ba != null) {
                        String s = String.valueOf(ba.get());
                        if (s != null) {
                            result.ajoutAttribut(new AttributLDAP(at, s));
                        }
                    }
                }
            }
        } catch (NamingException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (ctx != null) {
                try {
                    ctx.close();
                }
                catch (NamingException e2) {
                    LOG.error(e2.getMessage(), e2);
                }
            }
        }
        LOG.debug(result);
        return result;
    }
}
