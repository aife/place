/*
 * This program is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software 
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this 
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html 
 * or from the Free Software Foundation, Inc., 
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2007 - 2009 Pentaho Corporation.  All rights reserved.
 *
 */
package fr.atexo.commun.ldap.securite.recherche;

import javax.naming.directory.SearchControls;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class RechercheLdapParametresFactoryImpl implements RechercheLdapParametresFactory, InitializingBean {

  // ~ Static fields/initializers ============================================

  // private static final Log logger = LogFactory.getLog(LdapSearchParamsFactoryImpl.class);

  // ~ Instance fields =======================================================

  private String base;

  private String filtre;

  private SearchControls controleRecherches;

  // ~ Constructors ==========================================================

  public RechercheLdapParametresFactoryImpl() {
    super();
  }

  // ~ Methods ===============================================================

  public RechercheLdapParametresFactoryImpl(final String base, final String filtre) {
    this(base, filtre, new SearchControls());
  }

  public RechercheLdapParametresFactoryImpl(final String base, final String filtre, final SearchControls controleRecherches) {
    this.base = base;
    this.filtre = filtre;
    this.controleRecherches = controleRecherches;
  }

  public void afterPropertiesSet() throws Exception {
    Assert.notNull(base);
    Assert.hasLength(filtre);
  }

  /**
   * Private so it cannot be instantiated except by this class.
   */
  private class RechercheLdapParametresImpl implements RechercheLdapParametres {
    private String base;

    private String filtre;

    private Object[] filtreArguments;

    private SearchControls controleRecherches;

    private RechercheLdapParametresImpl(final String base, final String filter, final Object[] filtreArguments,
        final SearchControls searchControls) {
      this.base = base;
      this.filtre = filter;
      this.filtreArguments = filtreArguments;
      this.controleRecherches = searchControls;
    }

    public String getBase() {
      return base;
    }

    public String getFiltre() {
      return filtre;
    }

    public Object[] getFiltreArguments() {
      return filtreArguments;
    }

    public SearchControls getControleRecherches() {
      return controleRecherches;
    }

  }

  public RechercheLdapParametres creerParametres(final Object[] filtreArguments) {
    return new RechercheLdapParametresImpl(base, filtre, filtreArguments, controleRecherches);
  }

}
