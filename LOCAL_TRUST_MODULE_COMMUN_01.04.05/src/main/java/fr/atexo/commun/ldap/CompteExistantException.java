/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Exception lancée quand un compte existe alors qu'il est présupposé qu'il n'existe pas.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public class CompteExistantException extends Exception {

    private static final long serialVersionUID = 5539259501520815659L;

    /**
     * Construit l'exception.
     *
     * @param login le login du compte existant
     */
    public CompteExistantException(String login) {
        super("Le compte ayant comme login \"" + login + "\" existe déjà");
    }
}
