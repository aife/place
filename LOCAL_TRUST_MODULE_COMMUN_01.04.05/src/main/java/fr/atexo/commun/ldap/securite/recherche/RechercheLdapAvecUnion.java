/*
 * This program is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License, version 2.1 as published by the Free Software 
 * Foundation.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this 
 * program; if not, you can obtain a copy at http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html 
 * or from the Free Software Foundation, Inc., 
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * Copyright 2007 - 2009 Pentaho Corporation.  All rights reserved.
 *
 */
package fr.atexo.commun.ldap.securite.recherche;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Iterates over <code>LdapSearch</code> instances in <code>searches</code>
 * and unions the results.
 *
 * @author mlowery
 */
public class RechercheLdapAvecUnion implements RechercheLdap, InitializingBean {
    // ~ Static fields/initializers ============================================

    // private static final Log logger = LogFactory.getLog(UnionizingLdapSearch.class);

    // ~ Instance fields =======================================================

    private Set recherches;

    // ~ Constructors ==========================================================

    public RechercheLdapAvecUnion() {
        super();
    }

    public RechercheLdapAvecUnion(final Set recherches) {
        this.recherches = recherches;
    }

    // ~ Methods ===============================================================

    public List recherche(final Object[] filtreArguments) {
        Set resultats = new HashSet();
        Iterator iter = recherches.iterator();
        while (iter.hasNext()) {
            resultats.addAll(((RechercheLdap) iter.next()).recherche(filtreArguments));
        }
        return new ArrayList(resultats);
    }

    public void setRecherches(final Set recherches) {
        this.recherches = recherches;
    }

    public void afterPropertiesSet() throws Exception {
        Assert.notEmpty(recherches);
    }

}
