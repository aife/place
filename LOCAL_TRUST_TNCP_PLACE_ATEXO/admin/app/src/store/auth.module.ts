import AuthService from '../services/auth.service';
import UserModel from '../models/user-model';
import api from '@/services/api';

const initialState = {
    status: '',
    token: localStorage.getItem('token'),
    user: undefined
};

export const auth = {
    namespaced: true,
    state: initialState,
    mutations: {
        auth_request(state: any) {
            state.status = 'loading';
        },
        auth_success(state: any, user: UserModel) {
            state.status = 'success';
            state.user = user;
        },
        auth_error(state: any) {
            state.status = 'error';
        },
        setToken(state: any, token: string) {
            state.token = token;
        },
        logout(state: any) {
            state.status = null;
            state.token = null;
            state.user = null;
        },
    },
    actions: {
        login: function (context: any, user: UserModel) {
            context.commit('auth_request');
            return AuthService.login(user)
                .then((resp: { token: any; }) => {
                    if (resp) {
                        const token = resp.token;
                        localStorage.setItem('token', token);
                        context.commit('setToken', token);
                        api.defaults.headers.common['Authorization'] = 'Bearer ' + token;
                    }
                    return Promise.resolve(resp);
                })
                .catch((err: any) => {
                    context.commit('auth_error');
                    localStorage.removeItem('token');
                    return Promise.reject(err);
                });

        },
        whoami(context: any) {
            context.commit('auth_request');
            return AuthService.whoami()
                .then((resp: UserModel) => {
                    context.commit('auth_success', resp);
                    return Promise.resolve(resp);
                })
                .catch((err: any) => {
                    context.commit('auth_error');
                    localStorage.removeItem('token');
                    return Promise.reject(err);
                });
        },
        logout(context: any) {
            context.commit('logout');
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            delete api.defaults.headers.common['Authorization'];
        }
    },
    getters: {
        isLoggedIn: (state: any) => !!state.user || !!state.token,
        authStatus: (state: any) => state.status,
    }
};
