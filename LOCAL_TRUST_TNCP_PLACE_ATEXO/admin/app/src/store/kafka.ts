import KafkaService from "@/services/kafka.service";
import {EvenementAtexo} from "@/models/api/flink.api";


export const kafka = {
    actions: {
        getMessages(context: any) {
            return KafkaService.getMessages().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, sendRandomConsultation(context: any) {
            return KafkaService.sendRandomConsultation().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, sendConsultation(context: any, consultation: EvenementAtexo) {
            return KafkaService.sendConsultation(consultation).then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        },getConsultation(context: any) {
            return KafkaService.getConsultation().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, submitJob(context: any) {
            return KafkaService.submitJob().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, rerunAbandon(context: any) {
            return KafkaService.rerunAbandon().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, getJobs(context: any) {
            return KafkaService.getJobs().then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }, killJob(context: any, jobId: any) {
            return KafkaService.killJob(jobId.jid).then(
                (response: { data: string }) => {
                    return Promise.resolve(response.data);
                },
                (error: any) => {
                    return Promise.reject(error);
                }
            );
        }
    }
}
