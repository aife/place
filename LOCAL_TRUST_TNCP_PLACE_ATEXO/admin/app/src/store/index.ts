import Vue from 'vue'

import Vuex from 'vuex'
import {monitoring} from "./monitoring.module"
import {auth} from "./auth.module"
import {kafka} from "./kafka"

Vue.use(Vuex)

export default new Vuex.Store({

    modules: {monitoring, auth, kafka}
})
