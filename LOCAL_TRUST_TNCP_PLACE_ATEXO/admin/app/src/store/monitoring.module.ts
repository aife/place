import MonitoringService from "../services/monitoring-service";
import downloadFileFromResponse from "@/http-response.util";

export const monitoring = {
    namespaced: true,
    state: {
    },
    mutations: {
    },
    actions: {
        getAllLogs(context:any){
            return MonitoringService.getAllLogs().then(
                (response: any)=> {

                    return Promise.resolve(response);
                },
                (error: any)=> {

                    return Promise.reject(error);
                }
            );
        },
        downloadLog(context:any,nom:string){
            return MonitoringService.downloadLog(nom).then(
                (response: any)=> {
                    downloadFileFromResponse(response);

                    return Promise.resolve(response);
                },
                (error: any)=> {

                    return Promise.reject(error);
                }
            )
        }
    }


}