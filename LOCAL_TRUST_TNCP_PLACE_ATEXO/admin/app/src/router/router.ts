import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';

const ifAuthenticated = (to: any,
                         from: any,
                         next: any) => {
    if (store.getters['auth/isLoggedIn'] || store.getters['auth/authStatus'] ==='loading') {
        next();
        return;
    }
    next('/login?redirectUrl=' + to.path);
};

Vue.use(Router);

const router = new Router({
  mode: 'history', //removes # (hashtag) from url
  base: '/administration',
  fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState

    routes: [
        {
            path: '/',
            redirect: {name: 'dashboard'}
        },
        {
            path: '/simulateur',
            name: 'simulateur',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/Simulateur.vue')
        },  {
            path: '/kafka-sender',
            name: 'kafka',
            beforeEnter: ifAuthenticated,
            component: () => import('/src/views/KafkaSender.vue')
        },
        {
            path: '/dashboard',
            beforeEnter: ifAuthenticated,
            name: 'dashboard',
            component: () => import('/src/views/Home.vue')
        },
        {
            path: '/swagger',
            beforeEnter: ifAuthenticated,
            name: 'swagger',
            component: () => import('/src/views/Swagger.vue')
        },
        {
            path: '/javamelody',
            beforeEnter: ifAuthenticated,
            name: 'javamelody',
            component: () => import('/src/views/Javamelody.vue')
        },
        {
            path: '/logs',
            beforeEnter: ifAuthenticated,
            name: 'logs',
            component: () => import('/src/views/Logs.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('/src/views/Login.vue')
        }
    ],
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});

export default router;
