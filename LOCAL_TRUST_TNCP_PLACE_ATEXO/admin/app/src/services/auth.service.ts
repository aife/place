import UserModel from '../models/user-model';
import api from './api';

const API_LOGIN = '/administration/user/auth';
const API_WHO_AM_I = '/administration/user/whoami';

class AuthService {
    login(user: UserModel) {
        return api
            .post(API_LOGIN, {
                username: user.username,
                password: user.password
            })
            .then((response: { data: { token: string; }; }) => {
                return response.data;
            });
    }

    whoami() {
        return api
            .get(API_WHO_AM_I)
            .then((response: { data: UserModel }) => {
                if (response.data) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {

        localStorage.removeItem('user');
    }

}

export default new AuthService();
