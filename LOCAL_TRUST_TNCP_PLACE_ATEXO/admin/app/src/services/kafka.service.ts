import api from "./api";
import {EvenementAtexo} from "@/models/api/flink.api";

class KafkaService {

    sendRandomConsultation() {
        return api
            .post("./kafka/random", null)
    }

    sendConsultation(consultation: EvenementAtexo) {
        return api
            .post("./kafka/consultation", consultation)
    }

    getConsultation() {
        return api
            .get("./kafka/consultation")
    }

    getMessages() {
        return api
            .get("./kafka")
    }

    submitJob() {
        return api
            .post("./kafka/jobs-flink", null)
    }
    rerunAbandon() {
        return api
            .post("./kafka/re-run-abandon", null)
    }

    getJobs() {
        return api
            .get("./kafka/jobs-flink")
    }

    killJob(jobId: string) {
        return api
            .delete("./kafka/jobs-flink/" + jobId)
    }

}

export default new KafkaService();
