/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.18.565 on 2022-09-21 15:28:59.

export interface JobStatusMessage extends Serializable {
    jobId: JobID;
    jobName: string;
    jobState: JobStatus;
    startTime: number;
}

export interface JobDetailsInfo extends ResponseBody {
    jobId: JobID;
    name: string;
    jobStatus: JobStatus;
    startTime: number;
    endTime: number;
    duration: number;
    maxParallelism: number;
    now: number;
    timestamps: { [P in JobStatus]?: number };
    jobVertexInfos: JobVertexDetailsInfo[];
    jobVerticesPerState: { [P in ExecutionState]?: number };
    jsonPlan: string;
    stoppable: boolean;
}

export interface EvenementAtexo extends Serializable {
    uuid: string;
    flux: InterfaceEnum;
    type: TypeEnum;
    etape: EtapeEnum;
    dateEnvoi: Date;
    idObjetSource: string;
    idObjetDestination: string;
    url: string;
    token: string;
    uuidPlateforme: string;
    retry: number;
    login: string;
    password: string;
}

export interface JobID extends AbstractID {
}

export interface Serializable {
}

export interface JobVertexDetailsInfo {
    jobVertexID: JobVertexID;
    name: string;
    maxParallelism: number;
    parallelism: number;
    executionState: ExecutionState;
    startTime: number;
    endTime: number;
    duration: number;
    tasksPerState: { [P in ExecutionState]?: number };
    jobVertexMetrics: IOMetricsInfo;
}

export interface ResponseBody {
}

export interface AbstractID extends Comparable<AbstractID>, Serializable {
    upperPart: number;
    lowerPart: number;
    bytes: any;
}

export interface JobVertexID extends AbstractID, VertexID {
}

export interface IOMetricsInfo {
    bytesRead: number;
    bytesReadComplete: boolean;
    bytesWritten: number;
    bytesWrittenComplete: boolean;
    recordsRead: number;
    recordsReadComplete: boolean;
    recordsWritten: number;
    recordsWrittenComplete: boolean;
}

export interface VertexID extends Serializable {
}

export interface Comparable<T> {
}

export type JobStatus = "INITIALIZING" | "CREATED" | "RUNNING" | "FAILING" | "FAILED" | "CANCELLING" | "CANCELED" | "FINISHED" | "RESTARTING" | "SUSPENDED" | "RECONCILING";

export type ExecutionState = "CREATED" | "SCHEDULED" | "DEPLOYING" | "RUNNING" | "FINISHED" | "CANCELING" | "CANCELED" | "FAILED" | "RECONCILING" | "INITIALIZING";

export type InterfaceEnum = "TNCP_SORTANTE";

export type TypeEnum = "PUBLICATION_CONSULTATION" | "MODIFICATION_CONSULTATION" | "MODIFICATION_DCE";

export type EtapeEnum = "AJOUT_CONSULTATION" | "AJOUT_LOT" | "AJOUT_DCE" | "VALIDATION_CONSULTATION" | "MODIFICATION_CONSULTATION" | "MODIFICATION_DCE" | "MODIFICATION_RC";
