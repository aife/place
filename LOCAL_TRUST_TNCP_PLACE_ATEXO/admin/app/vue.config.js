module.exports = {

    devServer: {
        proxy: {
            "/administration/": {
                target: "http://localhost:8080/",
                changeOrigin: true
            }
        },
        port: 4202
    },
    publicPath: '/administration/'
}
