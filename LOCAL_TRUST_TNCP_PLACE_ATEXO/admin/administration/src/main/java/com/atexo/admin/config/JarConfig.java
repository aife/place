package com.atexo.admin.config;

import lombok.Data;

import java.util.List;

@Data
public class JarConfig {
    private String jarFile;
    private boolean absolute;
    private String mainClass;
    private List<String> args;
}
