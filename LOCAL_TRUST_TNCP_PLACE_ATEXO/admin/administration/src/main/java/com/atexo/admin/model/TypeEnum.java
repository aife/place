package com.atexo.admin.model;

public enum TypeEnum {
    PUBLICATION_CONSULTATION, MODIFICATION_CONSULTATION, MODIFICATION_DCE
}
