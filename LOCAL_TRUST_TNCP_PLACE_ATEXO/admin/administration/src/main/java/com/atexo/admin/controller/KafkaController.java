package com.atexo.admin.controller;

import com.atexo.admin.model.EvenementAtexo;
import com.atexo.admin.model.MpeMessage;
import com.atexo.admin.model.TaxiFare;
import com.atexo.admin.services.FlinkExecutorService;
import com.atexo.admin.services.KafkaSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobID;
import org.apache.flink.runtime.rest.messages.job.JobDetailsInfo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RequestMapping("kafka")
@RestController
public class KafkaController {

    private final KafkaSender kafkaSender;
    private final FlinkExecutorService flinkExecutorService;
    public static final int SLEEP_MILLIS_PER_EVENT = 1000;
    private Instant limitingTimestamp = Instant.MAX;

    public KafkaController(KafkaSender kafkaSender, FlinkExecutorService flinkExecutorService) {
        this.kafkaSender = kafkaSender;
        this.flinkExecutorService = flinkExecutorService;
    }

    @GetMapping
    public Map<String, List<String>> getMessages() {
        return kafkaSender.getMessages();
    }

    @PostMapping("consultation")
    public String sendRandomObjects(@RequestBody EvenementAtexo consultation) throws JsonProcessingException {
        if (!StringUtils.hasText(consultation.getUuid())) {
            consultation.setUuid(UUID.randomUUID().toString());
        }
        consultation.setDateEnvoi(Instant.now());
        MpeMessage mpeMessage = new MpeMessage();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());
        mpeMessage.setBody(objectMapper.writeValueAsString(consultation));
        kafkaSender.send(mpeMessage);
        return "OK";
    }

    @GetMapping("consultation")
    public EvenementAtexo getRandomObjects() {
        EvenementAtexo evenementAtexo = new EvenementAtexo();
        evenementConsultation.setParams();
        return EvenementAtexo;
    }

    @PostMapping("re-run-abandon")
    public void rerunAbandon() {
        kafkaSender.rerunAbandon();
    }

    @PostMapping("random")
    public String sendRandomObjects() throws InterruptedException, JsonProcessingException {
        for (int id = 1; id < 3; id++) {
            if (id % 2 == 0) {
                TaxiFare fare = new TaxiFare(id);

                
                if (fare.startTime.toInstant().compareTo(limitingTimestamp) >= 0) {
                    continue;
                }
                log.info("Sending message TaxiFare ...");

                kafkaSender.send(fare);
                
                Thread.sleep(SLEEP_MILLIS_PER_EVENT);
            } else {
                EvenementAtexo consultation = new EvenementAtexo();
                consultation.setParams();
                log.info("Sending message Consultation...");
                MpeMessage mpeMessage = new MpeMessage();
                mpeMessage.setBody(new ObjectMapper().writeValueAsString(consultation));
                kafkaSender.send(consultation);
                
                Thread.sleep(SLEEP_MILLIS_PER_EVENT);
            }
        }
        return "OK";
    }

    @PostMapping("jobs-flink")
    public List<JobID> startFlink() throws Exception {
        return flinkExecutorService.submit();
    }

    @GetMapping("jobs-flink")
    public List<JobDetailsInfo> getAllJobs() throws Exception {
        return flinkExecutorService.getAllJobs();
    }

    @DeleteMapping("jobs-flink/{jobID}")
    public String killJob(@PathVariable String jobID) throws Exception {
        flinkExecutorService.killJob(jobID);
        return "OK";
    }

}
