package com.atexo.admin.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenitificationRequest {
    private String login;
    private String password;
}
