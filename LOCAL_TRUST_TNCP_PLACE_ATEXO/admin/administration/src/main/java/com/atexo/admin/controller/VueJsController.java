package com.atexo.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class VueJsController {


    @GetMapping(value = {"/", "/dashboard", "/login", "/simulateur", "/swagger", "/javamelody", "/logs", "/kafka-sender"})
    public String vueJs() {
        return "index";
    }


}
