
package com.atexo.admin.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@JsonSerialize
public class EvenementAtexo implements Serializable {

    private String uuid;
    private InterfaceEnum flux;
    private TypeEnum type;
    private EtapeEnum etape;

    private Instant dateEnvoi;
    private String idObjetSource;
    private String idObjetDestination;
    private String url;
    private String token;
    private String uuidPlateforme;
    private int retry = 0;
    private String login = "mpe_tncp_int";
    private String password = "mpe_tncp_int";

    public void setParams() {
        List<String> ids = Arrays.asList("50004", "508128", "508127");
        this.flux = InterfaceEnum.TNCP_SORTANTE;
        this.type = TypeEnum.PUBLICATION_CONSULTATION;
        this.idObjetSource = ids.get(getRandomNumberInRange(0, 2));
        this.uuidPlateforme = "mpe_develop_mock";
        this.url = "https://mpe-release.local-trust.com";
        String url = this.url + "/api/v2/token";

        MpeToken mpeToken = new RestTemplate().postForEntity(url,
                AuthenitificationRequest.builder().login(login).password(password).build(), MpeToken.class).getBody();
        this.token = mpeToken.getToken();
        this.retry = 0;
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
