package com.atexo.admin.services.impl;

import com.atexo.admin.services.KafkaSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class KafkaSenderImpl implements KafkaSender, ConsumerSeekAware {
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.queue}")
    private String queue;

    Map<String, List<String>> messages = new HashMap<>();


    public KafkaSenderImpl(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }


    @Override
    public <T> void send(T fare, int partition) throws JsonProcessingException {
        kafkaTemplate.send(queue, partition, "consultation", new ObjectMapper().writeValueAsString(fare));
        log.info("Send msg ==========> " + fare);

    }

    @Override
    public <T> void send(T fare) throws JsonProcessingException {
        kafkaTemplate.send(queue, "consultation", new ObjectMapper().writeValueAsString(fare));
        log.info("Send msg ==========> " + fare);

    }

    @Override
    public Map<String, List<String>> getMessages() {
        return messages;
    }

    @Override
    public void rerunAbandon() {
        List<String> abandon = messages.get("Abandon");
        if (abandon != null)
            abandon
                    .forEach(s -> kafkaTemplate.send(queue, "consultation", s));
    }

    @KafkaListener(topics = "tncpConsultationsErreur", groupId = "mock-flink")
    public void listenErreur(@Payload String message,
                             @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Received Message in group Erreur: {} {}", partition, message);
        if (messages.containsKey("Erreur")) {
            messages.get("Erreur").add(message);
        } else {
            messages.put("Erreur", new ArrayList<>(Collections.singleton(message)));
        }
    }

    @KafkaListener(topics = "tncpConsultationsAbandon", groupId = "mock-flink")
    public void listenAbandon(@Payload String message,
                              @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Received Message in group Abandon: {} {}", partition, message);
        if (messages.containsKey("Abandon")) {
            messages.get("Abandon").add(message);
        } else {
            messages.put("Abandon", new ArrayList<>(List.of(message)));
        }
    }

    @KafkaListener(topics = "tncpSuivi", groupId = "mock-flink")
    public void listenSuivi(@Payload String message,
                            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Received Message in group Suivi: {} {}", partition, message);
        if (messages.containsKey("Suivi")) {
            messages.get("Suivi").add(message);
        } else {
            messages.put("Suivi", new ArrayList<>(List.of(message)));
        }
    }

    @KafkaListener(topics = "tncpConsultationsSucces", groupId = "mock-flink")
    public void listenSucces(@Payload String message,
                             @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Received Message in group Succes: {} {}", partition, message);
        if (messages.containsKey("Succes")) {
            messages.get("Succes").add(message);
        } else {
            messages.put("Succes", new ArrayList<>(List.of(message)));
        }
    }

    @KafkaListener(topics = "tncpConsultations", groupId = "mock-flink")
    public void listenWaiting(@Payload String message,
                              @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        log.info("Received Message in group Waiting: {} {}", partition, message);
        if (messages.containsKey("Waiting")) {
            messages.get("Waiting").add(message);
        } else {
            messages.put("Waiting", new ArrayList<>(List.of(message)));
        }
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
    //    assignments.keySet()
    //            .forEach(partition -> callback.seekToBeginning(partition.topic(), partition.partition()));
    }

    @Override
    public void onIdleContainer(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
    }

    @Override
    public void registerSeekCallback(ConsumerSeekCallback callback) {
    }
}
