package com.atexo.admin.controller;


import com.atexo.admin.model.AtexoUserDetails;
import com.atexo.admin.model.JwtResponse;
import com.atexo.admin.model.LoginRequest;
import com.atexo.admin.model.SignupRequest;
import com.atexo.admin.services.IJwtService;
import com.atexo.admin.services.IUserPort;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class AuthController {

    private final AuthenticationManager authenticationManager;

    private final IJwtService jwtUtils;
    private final IUserPort iUserPort;

    @Value("${admin.login}")
    private String login;

    @Value("${admin.password}")
    private String password;

    @Value("${admin.email}")
    private String email;

    public AuthController(AuthenticationManager authenticationManager, IJwtService jwtUtils, IUserPort iUserPort) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.iUserPort = iUserPort;
    }

    @PostConstruct
    public void initialize() {
        iUserPort.registerUser(SignupRequest.builder().username(login)
                .password(password).email(email).build());
    }


    @PostMapping("/auth")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        AtexoUserDetails userDetails = (AtexoUserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(JwtResponse.builder()
                .token(jwt)
                .id(userDetails.getId())
                .username(userDetails.getUsername())
                .email(userDetails.getEmail()).build());

    }

    @GetMapping("/whoami")
    public AtexoUserDetails getAuthenticatedUser(@AuthenticationPrincipal AtexoUserDetails userDetails) {
        return userDetails;

    }
}
