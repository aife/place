package com.atexo.admin.services;

import org.apache.flink.api.common.JobID;
import org.apache.flink.runtime.rest.messages.job.JobDetailsInfo;

import java.util.List;

public interface FlinkExecutorService {
    List<JobID> submit() throws Exception;

    List<JobDetailsInfo> getAllJobs() throws Exception;

    void killJob(String jobID) throws Exception;

}
