package com.atexo.admin.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogDetailFichier implements Serializable {
    private String nom;
    private String dateCreation;
    private String dernierAccess;
    private String dateModification;
    private String taille;

}
