package com.atexo.admin.services;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;
import java.util.Map;

public interface KafkaSender {

    <T> void send(T fare, int partition) throws JsonProcessingException;

    <T> void send(T fare) throws JsonProcessingException;

    Map<String, List<String>> getMessages();

    void rerunAbandon();

}
