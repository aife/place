package com.atexo.admin.services;

import com.atexo.admin.model.LogDetailFichier;

import java.io.FileInputStream;
import java.util.List;


public interface ApiService {
    List<LogDetailFichier> getLogsMonitoring();
    FileInputStream getFile(String nom);
}
