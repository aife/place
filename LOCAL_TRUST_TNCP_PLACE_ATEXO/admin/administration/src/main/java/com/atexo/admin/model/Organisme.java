package com.atexo.admin.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Organisme implements Serializable {

	private static final long serialVersionUID = -1536166194229082448L;
	private Integer id;

	private String organisme;

	private String plateforme;

}
