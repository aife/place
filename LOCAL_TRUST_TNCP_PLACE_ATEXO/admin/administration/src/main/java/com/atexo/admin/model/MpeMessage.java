package com.atexo.admin.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class MpeMessage {
    private String body;
    private List<String> properties = new ArrayList<>();
    private Map<String, String> headers = new HashMap<>();
}
