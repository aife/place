package com.atexo.admin.services.impl;


import com.atexo.admin.config.FlinkProperties;
import com.atexo.admin.config.JarConfig;
import com.atexo.admin.services.FlinkExecutorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobID;
import org.apache.flink.client.deployment.StandaloneClusterId;
import org.apache.flink.client.program.PackagedProgram;
import org.apache.flink.client.program.PackagedProgramUtils;
import org.apache.flink.client.program.rest.RestClusterClient;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.JobManagerOptions;
import org.apache.flink.configuration.RestOptions;
import org.apache.flink.runtime.jobgraph.JobGraph;
import org.apache.flink.runtime.rest.messages.job.JobDetailsInfo;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class FlinkExecutorServiceImpl implements FlinkExecutorService {


    private final FlinkProperties flinkProperties;


    public FlinkExecutorServiceImpl(FlinkProperties flinkProperties) {
        this.flinkProperties = flinkProperties;
    }

    @Override
    public List<JobID> submit() throws Exception {
        String clusterHost = flinkProperties.getJobManagerUrl();
        int clusterPort = flinkProperties.getJobManagerPort();

        Configuration config = new Configuration();
        config.setString(JobManagerOptions.ADDRESS, clusterHost);
        config.setInteger(RestOptions.PORT, clusterPort);
        List<JobID> jobIDS = new ArrayList<>();
        try (RestClusterClient<StandaloneClusterId> client = new
                RestClusterClient<>(config, StandaloneClusterId.getInstance())) {
            for (JarConfig jarFile : flinkProperties.getRemoteEnvJarFiles()) {
                try {
                    String[] args = jarFile.getArgs().toArray(new String[0]);
                    PackagedProgram packagedProgram = PackagedProgram.newBuilder()
                            .setArguments(args)
                            .setConfiguration(config)
                            .setEntryPointClassName("")
                            .setEntryPointClassName(jarFile.getMainClass())
                            .setJarFile(jarFile.isAbsolute() ? new File(jarFile.getJarFile()) : new ClassPathResource(jarFile.getJarFile()).getFile())
                            .build();


                    int parallelism = 1;

                    JobGraph jobGraph = PackagedProgramUtils.createJobGraph(packagedProgram, config, parallelism, false);
                    jobIDS.add(client.submitJob(jobGraph).get());
                } catch (Exception | OutOfMemoryError e) {
                    log.error(e.getMessage());
                }
            }

        }


        return jobIDS;
    }


    @Override
    public List<JobDetailsInfo> getAllJobs() throws Exception {
        String clusterHost = flinkProperties.getJobManagerUrl();
        int clusterPort = flinkProperties.getJobManagerPort();

        Configuration config = new Configuration();
        config.setString(JobManagerOptions.ADDRESS, clusterHost);
        config.setInteger(RestOptions.PORT, clusterPort);
        try (RestClusterClient<StandaloneClusterId> client = new
                RestClusterClient<>(config, StandaloneClusterId.getInstance())) {


            return client.listJobs().get().stream().map(jobStatusMessage -> {
                        try {
                            return client.getJobDetails(jobStatusMessage.getJobId()).get();
                        } catch (Exception | OutOfMemoryError e) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

    }

    @Override
    public void killJob(String jobID) throws Exception {
        String clusterHost = flinkProperties.getJobManagerUrl();
        int clusterPort = flinkProperties.getJobManagerPort();

        Configuration config = new Configuration();
        config.setString(JobManagerOptions.ADDRESS, clusterHost);
        config.setInteger(RestOptions.PORT, clusterPort);

        try (RestClusterClient<StandaloneClusterId> client = new
                RestClusterClient<>(config, StandaloneClusterId.getInstance())) {
            client.cancel(JobID.fromHexString(jobID)).get();
        }


    }

}
