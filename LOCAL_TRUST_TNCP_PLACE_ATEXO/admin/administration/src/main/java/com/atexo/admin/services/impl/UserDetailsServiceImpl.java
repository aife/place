package com.atexo.admin.services.impl;



import com.atexo.admin.services.IUserPort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("atexoUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	private final IUserPort iUserPort;

	public UserDetailsServiceImpl(IUserPort iUserPort) {
		this.iUserPort = iUserPort;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return iUserPort.findByUsername(username);
	}

}
