package com.atexo.admin.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContexteAgent {

    private String id;

    private String identifiant;

    private String nom;

    private String prenom;

    private String acheteurPublic;

    private Organisme organisme;

    private String photoUrl;

    private String sigleUrl;

    private String serveurUrl;

    private String execUrl;

    private String serveurLogin;

    private String serveurPassword;

    private int version = 2;

    private List<String> roles;
}
