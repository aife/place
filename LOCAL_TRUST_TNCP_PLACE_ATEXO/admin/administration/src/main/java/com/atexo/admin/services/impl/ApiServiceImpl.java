package com.atexo.admin.services.impl;

import com.atexo.admin.exception.AdminException;
import com.atexo.admin.model.LogDetailFichier;
import com.atexo.admin.services.ApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ApiServiceImpl implements ApiService {

    private static final long MB = 1024L * 1024L;
    private static final long KB = 1024L;
    @Value("${administration.log-folder}")
    private String logFolder;


    //permet de recuperer les infos sur les fichiers du repertoire logs
    @Override
    public List<LogDetailFichier> getLogsMonitoring() {
        //dossier logs
        File files = new File(logFolder);
        Collection<File> fichierLogs = Arrays.asList(Objects.requireNonNull(files.listFiles()));
        File[] fichiers = new File[fichierLogs.size()];
        fichiers = fichierLogs.toArray(fichiers);
        return Arrays.stream(fichiers).map(file -> {
            try {
                BasicFileAttributes attr =
                        Files.readAttributes(Paths.get(file.getAbsolutePath()), BasicFileAttributes.class);
                return new LogDetailFichier(file.getName(), getDateFromFileTime(attr.creationTime())
                        , getDateFromFileTime(attr.lastAccessTime()), getDateFromFileTime(attr.lastModifiedTime()), KBToMB(attr.size()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  LogDetailFichier.builder().nom(file.getName()).taille(KBToMB(file.length())).build();
        }).collect(Collectors.toList());
    }

    private String getDateFromFileTime(FileTime fileTime) {
        long cTime = fileTime.toMillis();
        ZonedDateTime t = Instant.ofEpochMilli(cTime).atZone(ZoneId.of("Europe/Berlin"));
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(t);

    }

    private String KBToMB(long octet) {
        float megaOctet = (float) octet / (float) MB;
        return megaOctet >= 1 ? (megaOctet + "MB") : ((float) octet / (float) KB + "KB");
    }


    @Override
    public FileInputStream getFile(String nom) {
        try {
            File file = new File(logFolder+ nom);
            return new FileInputStream(file);
        } catch (IOException e) {
            log.error("", e.fillInStackTrace());
            throw new AdminException("Fichier non existant");
        }
    }

}
