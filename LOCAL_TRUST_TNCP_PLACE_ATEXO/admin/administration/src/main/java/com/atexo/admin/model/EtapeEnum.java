package com.atexo.admin.model;

public enum EtapeEnum {
    AJOUT_CONSULTATION, AJOUT_LOT, AJOUT_DCE, VALIDATION_CONSULTATION, MODIFICATION_CONSULTATION, MODIFICATION_DCE, MODIFICATION_RC
}
