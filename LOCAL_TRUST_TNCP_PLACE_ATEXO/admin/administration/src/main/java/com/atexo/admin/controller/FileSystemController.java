package com.atexo.admin.controller;

import com.atexo.admin.model.LogDetailFichier;
import com.atexo.admin.services.impl.ApiServiceImpl;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/files-logs")
public class FileSystemController {

    private final ApiServiceImpl apiService;

    public FileSystemController(ApiServiceImpl apiService) {
        this.apiService = apiService;
    }


    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LogDetailFichier> getAllFiles() {
        return apiService.getLogsMonitoring();
    }

    @GetMapping()
    public ResponseEntity<InputStreamResource> downloadLog(@RequestParam("name") String name) {
        InputStreamResource inputStreamResource = new InputStreamResource(apiService.getFile(name));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + name)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(inputStreamResource);
    }

}
