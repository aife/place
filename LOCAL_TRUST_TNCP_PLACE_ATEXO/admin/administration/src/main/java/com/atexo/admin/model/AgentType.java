//




//


package com.atexo.admin.model;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour AgentType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="AgentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="identifiant" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="plateforme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="acronymeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="organisme" type="{http://www.atexo.com/epm/xml}OrganismeType"/&gt;
 *         &lt;element name="api" type="{http://www.atexo.com/epm/xml}ServeurApiType"/&gt;
 *         &lt;element name="nom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prenom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="service" type="{http://www.atexo.com/epm/xml}ServiceType" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.atexo.com/epm/xml}EmailType"/&gt;
 *         &lt;element name="telephone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nomCourantAcheteurPublic" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="photoUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sigleUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="habilitations"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="habilitation" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentType", propOrder = {

})
public class AgentType {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    @NotBlank(message = "L'identifiant est obligatoire")
    protected String identifiant;
    @XmlElement(required = true)
    @NotBlank(message = "La plateforme est obligatoire")
    protected String plateforme;
    @XmlElement(required = true)
    protected String acronymeOrganisme;
    @XmlElement(required = true)
    @NotBlank(message = "L'organisme est obligatoire")
    protected OrganismeType organisme;
    @XmlElement(required = true)
    @Valid
    protected ServeurApiType api;
    @XmlElement(required = true)
    @NotBlank(message = "Le nom est obligatoire")
    protected String nom;
    @XmlElement(required = true)
    @NotBlank(message = "Le prénom est obligatoire")
    protected String prenom;
    @XmlElement(required = true)
    @NotBlank(message = "L'acheteur public est obligatoire")
    protected String nomCourantAcheteurPublic;
    protected String photoUrl;
    protected String sigleUrl;
    @XmlElement(required = true)
    protected Habilitations habilitations;

    /**
     * Obtient la valeur de la propriété id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété identifiant.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifiant() {
        return identifiant;
    }

    /**
     * Définit la valeur de la propriété identifiant.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifiant(String value) {
        this.identifiant = value;
    }

    /**
     * Obtient la valeur de la propriété plateforme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateforme() {
        return plateforme;
    }

    /**
     * Définit la valeur de la propriété plateforme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateforme(String value) {
        this.plateforme = value;
    }

    /**
     * Obtient la valeur de la propriété acronymeOrganisme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcronymeOrganisme() {
        return acronymeOrganisme;
    }

    /**
     * Définit la valeur de la propriété acronymeOrganisme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcronymeOrganisme(String value) {
        this.acronymeOrganisme = value;
    }

    /**
     * Obtient la valeur de la propriété organisme.
     * 
     * @return
     *     possible object is
     *     {@link OrganismeType }
     *     
     */
    public OrganismeType getOrganisme() {
        return organisme;
    }

    /**
     * Définit la valeur de la propriété organisme.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganismeType }
     *     
     */
    public void setOrganisme(OrganismeType value) {
        this.organisme = value;
    }

    /**
     * Obtient la valeur de la propriété api.
     * 
     * @return
     *     possible object is
     *     {@link ServeurApiType }
     *     
     */
    public ServeurApiType getApi() {
        return api;
    }

    /**
     * Définit la valeur de la propriété api.
     * 
     * @param value
     *     allowed object is
     *     {@link ServeurApiType }
     *     
     */
    public void setApi(ServeurApiType value) {
        this.api = value;
    }

    /**
     * Obtient la valeur de la propriété nom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNom() {
        return nom;
    }

    /**
     * Définit la valeur de la propriété nom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNom(String value) {
        this.nom = value;
    }

    /**
     * Obtient la valeur de la propriété prenom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Définit la valeur de la propriété prenom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrenom(String value) {
        this.prenom = value;
    }


    /**
     * Obtient la valeur de la propriété nomCourantAcheteurPublic.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomCourantAcheteurPublic() {
        return nomCourantAcheteurPublic;
    }

    /**
     * Définit la valeur de la propriété nomCourantAcheteurPublic.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomCourantAcheteurPublic(String value) {
        this.nomCourantAcheteurPublic = value;
    }

    /**
     * Obtient la valeur de la propriété photoUrl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhotoUrl() {
        return photoUrl;
    }

    /**
     * Définit la valeur de la propriété photoUrl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhotoUrl(String value) {
        this.photoUrl = value;
    }

    /**
     * Obtient la valeur de la propriété sigleUrl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigleUrl() {
        return sigleUrl;
    }

    /**
     * Définit la valeur de la propriété sigleUrl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigleUrl(String value) {
        this.sigleUrl = value;
    }

    /**
     * Obtient la valeur de la propriété habilitations.
     * 
     * @return
     *     possible object is
     *     {@link Habilitations }
     *     
     */
    public Habilitations getHabilitations() {
        return habilitations;
    }

    /**
     * Définit la valeur de la propriété habilitations.
     * 
     * @param value
     *     allowed object is
     *     {@link Habilitations }
     *     
     */
    public void setHabilitations(Habilitations value) {
        this.habilitations = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="habilitation" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "habilitation"
    })
    public static class Habilitations {

        @XmlElement(required = true)
        @NotEmpty(message = "La liste des roles ne doit pas être vide")
        protected List<String> habilitation;

        /**
         * Gets the value of the habilitation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the habilitation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getHabilitation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getHabilitation() {
            if (habilitation == null) {
                habilitation = new ArrayList<String>();
            }
            return this.habilitation;
        }

    }

}
