package com.atexo.admin.services.impl;

import com.atexo.admin.model.AtexoUserDetails;
import com.atexo.admin.services.IJwtService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class JwtServiceImpl implements IJwtService {

	@Value("${jwt.expirationMs}")
	private int jwtExpirationMs;
	@Value("${jwt.secret}")
	private String jwtSecret;
	private final ObjectMapper objectMapper;

	public JwtServiceImpl(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}



	@Override
	public String generateJwtToken(Authentication authentication) {

		AtexoUserDetails userPrincipal = (AtexoUserDetails) authentication.getPrincipal();

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}



	@Override
	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			throw new JwtException("Invalid JWT signature: " + e.getMessage());
		} catch (MalformedJwtException e) {
			throw new JwtException("Invalid JWT token: " + e.getMessage());
		} catch (ExpiredJwtException e) {
			throw new JwtException("JWT token is expired: " + e.getMessage());
		} catch (UnsupportedJwtException e) {
			throw new JwtException("JWT token is unsupported: " + e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new JwtException("JWT claims string is empty: " + e.getMessage());
		}

	}

	@Override
	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

}
