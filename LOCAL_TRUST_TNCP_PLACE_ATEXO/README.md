# lt_flink

docker-compose run sql-client

- http://flink.apache.org/docs/latest/apis/cli.html
- https://citizix.com/how-to-install-and-set-up-kafdrop-kafka-web-ui/
- https://www.conduktor.io/kafka/how-to-install-apache-kafka-on-linux

- /home/iat-atx/Bureau/ATEXO/TOOLS/kafka_2.13-3.0.0/bin/zookeeper-server-start.sh \
  /home/iat-atx/Bureau/ATEXO/TOOLS/kafka_2.13-3.0.0/config/zookeeper.properties &
- java --add-opens=java.base/sun.nio.ch=ALL-UNNAMED -jar \
  /home/iat-atx/Bureau/ATEXO/TOOLS/kafdrop/kafdrop-3.30.0.jar --kafka.brokerConnect=localhost:9092 &
- /home/iat-atx/Bureau/ATEXO/TOOLS/flink-1.15.0-bin-scala_2.12/flink-1.15.0/bin/start-cluster.sh &
- /home/hmo-atx/Desktop/ATEXO/TOOLS/flink-1.15.0/bin/start-cluster.sh &
