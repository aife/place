package com.atexo.flink.batch.services;

import com.atexo.flink.config.argument.ArgsBatch;
import com.atexo.flink.batch.conf.ApplicationTechnicalException;

public interface SynchronisationService {

    void jobDispatcher(ArgsBatch main) throws ApplicationTechnicalException;

}
