package com.atexo.flink.batch.services;

import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.argument.ArgsBatch;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.InterfaceEnum;
import com.atexo.flink.domain.commun.TypeEnum;
import com.atexo.flink.domain.mpe.model.MpeEnveloppe;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SynchronisationServiceImpl implements SynchronisationService {

    static final Logger LOG = LoggerFactory.getLogger(SynchronisationServiceImpl.class);
    private final KafkaTemplate<String, EvenementAtexo> kafkaTemplate;
    private final KafkaTemplate<String, MpeEnveloppe> kafkaRecuperationTemplate;

    @Value("${tncp.kafka.topic.notification.envoi}")
    private String notificationTopicEnvoi;
    @Value("${tncp.kafka.topic.consultation.envoi}")
    private String consultationTopicEnvoi;

    @Override
    public void jobDispatcher(ArgsBatch main) {
        if (main.isNotifications()) {
            LOG.info("Début du job Notifications");
            runJobNotification();
            LOG.info("Fin du job Notifications");
        } else if (main.isRecuperation()) {
            LOG.info("Début du job Notifications");
            runJobRecuperationConsultation();
            LOG.info("Fin du job Notifications");
        } else {
            LOG.warn("Aucun job à lancer");
        }

    }

    public void runJobNotification() {
        EvenementAtexo evenementAtexo = new EvenementAtexo();
        evenementAtexo.setFlux(InterfaceEnum.TNCP_ENTRANTE);
        evenementAtexo.setEtape(EtapeEnum.RECUPERATION_NOTIFICATION);
        evenementAtexo.setType(TypeEnum.RECUPERATION_NOTIFICATION);
        evenementAtexo.setUuid(UUID.randomUUID().toString());
        LOG.info("Envoi via KAFKA du notification UID => {} sur le topic {}", evenementAtexo.getUuid(), notificationTopicEnvoi);

        var future = kafkaTemplate.send(notificationTopicEnvoi, evenementAtexo);
        future.addCallback(stringTNCPEnvoiSendResult -> {
            LOG.info("Envoi du notification UID => {} avec succès", evenementAtexo.getUuid());
        }, throwable -> {
            LOG.error("Erreur lors de l'envoi du notification UID => {}", evenementAtexo.getUuid(), throwable);

        });
    }

    public void runJobRecuperationConsultation() {
        EvenementAtexo evenementAtexo = new EvenementAtexo();
        evenementAtexo.setFlux(InterfaceEnum.TNCP_ENTRANTE);
        evenementAtexo.setEtape(EtapeEnum.RECUPERATION_CONSULTATIONS_TNCP);
        evenementAtexo.setType(TypeEnum.INTERGRATION_CONSULTATIONS_TNCP);
        evenementAtexo.setUuid(UUID.randomUUID().toString());
        LOG.info("Envoi via KAFKA du notification UID => {} sur le topic {}", evenementAtexo.getUuid(), consultationTopicEnvoi);

        MpeEnveloppe mpeEnveloppe = new MpeEnveloppe();
        mpeEnveloppe.setBody(AtexoUtils.writeValueAsString(evenementAtexo));
        var future = kafkaRecuperationTemplate.send(consultationTopicEnvoi, mpeEnveloppe);
        future.addCallback(stringTNCPEnvoiSendResult -> {
            LOG.info("Envoi du Demande de récupération UID => {} avec succès", evenementAtexo.getUuid());
        }, throwable -> {
            LOG.error("Erreur lors de l'envoi du Demande de récupération UID => {}", evenementAtexo.getUuid(), throwable);

        });
    }

}
