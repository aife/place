package com.atexo.flink;

import com.atexo.flink.config.argument.ArgsBatch;
import com.atexo.flink.batch.services.SynchronisationService;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@EnableTransactionManagement
@EnableConfigurationProperties
@Slf4j
@Profile({"dev", "tncp-batch"})
public class TncpBatchApplication implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public TncpBatchApplication(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    public static void main(String[] args) {
        SpringApplication.run(TncpBatchApplication.class, args).close();
    }

    @Override
    public void run(String... args) throws Exception {
        var main = new ArgsBatch();
        JCommander.newBuilder().addObject(main).build().parse(ArgsBatch.filterSpringBootArgs(args));
        var synchronisationService = applicationContext.getBean(SynchronisationService.class);
        synchronisationService.jobDispatcher(main);
        System.exit(0);
    }
}
