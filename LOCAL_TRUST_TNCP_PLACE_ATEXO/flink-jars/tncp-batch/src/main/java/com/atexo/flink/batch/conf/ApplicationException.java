package com.atexo.flink.batch.conf;

import java.text.MessageFormat;

/**
 * Created by MZO on 05/02/2016.
 */
public class ApplicationException extends Exception {


    public ApplicationException() {

    }

    public ApplicationException(String reason) {
        super(reason);
    }

	public ApplicationException(String reason, Object... args) {
		super(MessageFormat.format(reason, args));
	}

    public ApplicationException(Throwable t) {
        super(t);
    }

}
