package com.atexo.flink.batch.conf;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "tncp.kafka")
@Getter
@Setter
public class KafkaSecurityConf {
    private final Map<String, Object> security = new HashMap<>();
    public Map<String, Object> getKafkaSecurityProperties() {
        var properties = new HashMap<String, Object>();
        for (var entry : security.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof String && value.toString().startsWith("classpath:")) {
                try {
                    value = new ClassPathResource(value.toString().substring(10)).getFile().getAbsolutePath();
                } catch (IOException e) {
                    value = entry.getValue();
                }
            }
            properties.put(entry.getKey().replace("-", "."), value);
        }
        return properties;
    }
}
