package com.atexo.flink.batch.conf;

public class ApplicationTechnicalException extends ApplicationException {

    String messageKey;

    Object[] messageArgs;

    Throwable throwable;

    public ApplicationTechnicalException() {

    }

    public ApplicationTechnicalException(Throwable throwable) {
        super(throwable);
        this.throwable = throwable;
    }

    public String getMessageKey() {
        return messageKey;
    }

    private void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public Object[] getMessageArgs() {
        return messageArgs;
    }

    private void setMessageArgs(Object[] messageArgs) {
        this.messageArgs = messageArgs;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    private void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public static class ApplicationExceptionBuilder {

        ApplicationTechnicalException result = new ApplicationTechnicalException();

        private ApplicationExceptionBuilder setMessageKey(String messageKey) {
            result.setMessageKey(messageKey);
            return this;
        }

        private ApplicationExceptionBuilder setMessageArgs(Object[] messageArgs) {
            result.setMessageArgs(messageArgs);
            return this;
        }

        private ApplicationExceptionBuilder setThrowable(Throwable throwable) {
            result.setThrowable(throwable);
            return this;
        }

        public ApplicationTechnicalException build() {
            return result;
        }
    }
}
