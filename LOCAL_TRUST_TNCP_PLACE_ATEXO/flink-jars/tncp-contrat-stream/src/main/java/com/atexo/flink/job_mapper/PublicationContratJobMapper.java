package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.contrat.model.EvenementContrat;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.contrat.TNCPContratMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.springframework.util.Base64Utils;

import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;


@Slf4j
public class PublicationContratJobMapper extends RichMapFunction<Tuple3<String, EvenementContrat, AtexoJobException>, Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public PublicationContratJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementContrat, AtexoJobException> in) throws Exception {
        EvenementContrat evenementContrat = in.f1;
        FlinkJobUtils.setMDC(evenementContrat);
        try {
            log.info("Execution job de publication pour la contrat id={} et referenceUnique = {}", evenementContrat.getIdObjetSource(), evenementContrat.getIdObjetDestination());

            String src = new String(Base64Utils.decodeFromString(evenementContrat.getPayload()));

            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            TNCPContratMessage contratMessage = tncpWs.sendPublicationContrat(src, token);
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementContrat);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setIdObjetDestination(contratMessage.getReferenceUnique());

            tncpSuivi.setStatut(StatutEnum.FINI);

            return new Tuple4<>(in.f0, evenementContrat, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0, evenementContrat, e, getTncpSuiviErreur(evenementContrat, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de publication pour la contrat id=" + evenementContrat.getIdObjetSource(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementContrat, atexoJobException, getTncpSuiviErreur(evenementContrat, atexoJobException, log));
        }
    }
}
