package com.atexo.flink;


import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.*;
import com.atexo.flink.domain.contrat.TncpContratsEnAttenteSerializationSchema;
import com.atexo.flink.domain.contrat.TncpContratsErreurAbandonSerializationSchema;
import com.atexo.flink.domain.contrat.TncpContratsSuiviSerializationSchema;
import com.atexo.flink.domain.contrat.model.EvenementContrat;
import com.atexo.flink.domain.contrat.process.DelayEvenementContratFromRetryProcess;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.job_mapper.PublicationContratJobMapper;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.web.client.RestTemplate;

import static com.atexo.flink.config.FlinkJobUtils.*;

@Slf4j
public class MpeContratStream extends TncpStream<EvenementContrat> {


    /**
     * Creates a job using the source and sink provided.
     *
     * @param waitingSource
     * @param errorSink
     * @param abandonSink
     * @param tncpSuiviSink
     * @param waintingSink
     * @param tncpWs
     */
    public MpeContratStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> sucessSink, KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> waintingSink, TncpWs tncpWs, JarFlinkConfiguration configuration) {
        super(waitingSource, sucessSink, errorSink, abandonSink, tncpSuiviSink, waintingSink, null, tncpWs, configuration);
    }


    /**
     * Main method.
     */
    public static void main(String[] args) {
        try {
            String jobName = "contrat";
            var main = new JarFlinkConfiguration();
            JCommander.newBuilder().addObject(main).build().parse(JarFlinkConfiguration.log(args));

            MpeContratStream job = getTncpJob(main, jobName);
            job.execute(jobName);
        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", e.getMessage(), e);
        }
    }


    public JobExecutionResult execute(String... args) throws Exception {
        String jobName = args[0];
        StreamExecutionEnvironment env = getStreamExecutionEnvironment();
        // start the data generator and arrange for watermarking
        KeyedStream<Tuple3<String, EvenementContrat, AtexoJobException>, String> evenementStream = getEvenementContratEnAttente(env, jobName);

        log.info("Conversion des evenements");
        KeyedStream<String, Object> noParsedStream = evenementStream.filter(value -> value.f2 != null).name("Filtre sur les evenements contrats avec des donnees corrompues").map(k -> k.f0).name("Correspondance avec la source d'origine").keyBy(value -> value);
        KeyedStream<Tuple3<String, EvenementContrat, AtexoJobException>, Object> validEvenement = evenementStream.filter(value -> value.f1 != null && EtapeEnum.PUBLICATION_DE_CONTRAT.equals(value.f1.getEtape()) && TypeEnum.PUBLICATION_DE_CONTRAT.equals(value.f1.getType())).name("Filtre sur les evenements de contrat valides").keyBy(value -> value.f0);

        KeyedStream<Tuple3<String, EvenementContrat, AtexoJobException>, Object> tokenCheckedStream = validEvenement
                .keyBy(value -> value.f0)
                .process(new DelayEvenementContratFromRetryProcess())
                .name("Activer l'horloge pour les messages une future tentative")
                .keyBy(k -> k.f0);
        KeyedStream<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>, Object> publicationContratStream = tokenCheckedStream.keyBy(k -> k.f0).filter(stringEvenementContratAtexoJobExceptionTuple3 -> stringEvenementContratAtexoJobExceptionTuple3.f1.getPayload() != null).map(new PublicationContratJobMapper(configuration)).name("Publication la contrat").keyBy(k -> k.f0).keyBy(k -> k.f0);

       

        //Send to error source
        publicationContratStream.filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() <= 45).name("Filtre sur les evenements Contrat avec une tentative <= 2 heures").sinkTo(errorSink).name("Aquittement dans la source en erreur pour une future tentative");

        SingleOutputStreamOperator<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> abandonAfterRetryStream = publicationContratStream.filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() > 45).name("Filtre sur les evenements Contrat avec une tentative > 2 heures");
        //Send to abandon source

        noParsedStream.union(abandonAfterRetryStream.map(value -> value.f0)).sinkTo(abandonSink).name("Aquittement dans la source Contrat d'abandon");
        //Send to success source
        publicationContratStream.filter(value -> value.f3.getStatut().equals(StatutEnum.FINI)).name("Filtre sur les evenements Contrat avec succes").sinkTo(sucessSink).name("Aquittement dans la source Contrat en succes");
        publicationContratStream.filter(value -> value.f3.getStatut().equals(StatutEnum.EN_COURS)).name("Filtre sur les evenements Contrat futures").sinkTo(waitingSink).name("Aquittement dans la source Contrat pour traiter l'etape suivante");

        publicationContratStream.sinkTo(tncpSuiviSink).name("Aquittement dans la source Contrat de suivi TNCP");

        // execute the transformation pipeline
        JobConfig jobConfig = getJobConfig();
        return env.execute(jobConfig.getName() + " " + jobConfig.getVersion());
    }

    public static MpeContratStream getTncpJob(JarFlinkConfiguration configuration, String jobName) {
        log.info("Configuration du job : {} | config : {}", jobName, configuration);

        String waitingTopic = configuration.getKafkaWaitingTopic();
        String errorTopic = configuration.getKafkaErrorTopic();


        KafkaSource<String> source = getKafkaSource(getProperties(configuration, jobName, "waiting-source"), waitingTopic, errorTopic);

        TncpContratsSuiviSerializationSchema<EvenementContrat> valueSerializationSchema = new TncpContratsSuiviSerializationSchema<>(configuration.getPlateforme());
        TncpContratsEnAttenteSerializationSchema<EvenementContrat> valueSerializationSchemaAttente = new TncpContratsEnAttenteSerializationSchema<>();
        TncpContratsErreurAbandonSerializationSchema<EvenementContrat> valueSerializationSchemaErreur = new TncpContratsErreurAbandonSerializationSchema<>();
        SuccessSerializationSchema<EvenementContrat> valueSerializationSchemaSuccess = new SuccessSerializationSchema<>();

        FlinkService<EvenementContrat> flinkService = new FlinkService<>();

        KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> waitingSink = flinkService.getWaitingSinkEvenement(waitingTopic, getProperties(configuration, jobName, "waiting"), valueSerializationSchemaAttente);
        KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> topicErreur = flinkService.getErreurAbandonSinkEvenement(configuration.getKafkaErrorTopic(), getProperties(configuration, jobName, "erreur"), valueSerializationSchemaErreur);
        KafkaSink<String> topicAbandon = flinkService.getSink(configuration.getKafkaAbandonTopic(), getProperties(configuration, jobName, "abandon"));
        KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> topicSuivi = flinkService.getSinkTncpSuivi(configuration.getKafkaSuiviTopic(), getProperties(configuration, jobName, "suivi"), valueSerializationSchema);
        KafkaSink<Tuple4<String, EvenementContrat, AtexoJobException, TncpSuivi>> topicSucces = flinkService.getSuccessSinkEvenement(configuration.getKafkaSuccessTopic(), getProperties(configuration, jobName, "succes"), valueSerializationSchemaSuccess);

        RestTemplate restTemplate = new RestTemplateConfig().atexoRestTemplate();
        TncpWs tncpWs = new TncpWs(configuration, restTemplate);
        return new MpeContratStream(source, topicSucces, topicErreur, topicAbandon, topicSuivi, waitingSink, tncpWs, configuration);

    }

}
