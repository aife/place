package com.atexo.flink.ws.mpe;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.JarFlinkConfiguration;
import com.atexo.flink.config.RestTemplateConfig;
import com.atexo.flink.config.WsResource;
import com.atexo.flink.ws.mpe.model.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.ZonedDateTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MpeWsTest extends WsResource {

    private String idConsultationAllotie = "508127";
    private String idConsultationNonAllotie = "508128";
    JarFlinkConfiguration configuration = JarFlinkConfiguration.builder().mpeUrl("https://mpe-release.local-trust.com").mpeLogin("mpe_tncp_int").mpePassword("mpe_tncp_int").mpeUuid("MPE_atexo_release").build();
    private final MpeWs mpeWs = new MpeWs(configuration, new RestTemplateConfig().atexoRestTemplate());

    @Test
    void Given_IdConsultation_Then_ReturnConsultation() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        MpeConsultation result = mpeWs.getConsultation(idConsultationNonAllotie);
        assertJsonEqual("getConsultation", result);
    }

    @Test
    void Given_IdConsultationInvalid_Then_ThrowEception() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> mpeWs.getConsultation("0"));
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }

    @Test
    void Given_IdAgent_When_getAgentCreateur_Then_ReturnAgent() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        MpeAgent result = mpeWs.getAgent("24");
        assertJsonEqual("getAgent", result);
    }

    @Test
    void Given_IdAgentInvalid_When_getAgentCreateur_Then_ThrowEception() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            mpeWs.getAgent("9999999999999999999999999");
        });
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }

    @Test
    void Given_IdService_When_getService_Then_ReturnService() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        MpeService result = mpeWs.getService("/api/v2/referentiels/services/2921");
        assertNotNull(result);
        assertJsonEqual("getService", result);

    }

    @Test
    void Given_IdServiceInvalid_When_getService_Then_ThrowEception() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            mpeWs.getService("/api/v2/referentiels/services/9999999999999999999999999");
        });
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }

    @Test
    void Given_IdConsultation_When_getListeLots_Then_ReturnListe() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        //passer id d'une consultation allotie
        List<MpeLot> result = mpeWs.getListeLots(idConsultationAllotie);
        assertJsonEqual("getLots", result);

    }

    @Test
    void Given_IdConsultationInvalid_When_getLots_Then_ThrowEception() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        List<MpeLot> result = mpeWs.getListeLots("/api/v2/lots/9999999999999999999999999");
        assertThat(result).isEmpty();
    }

    @Test
    void Given_IdLot_When_getLot_Then_ReturnLot() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        List<MpeLot> lots = mpeWs.getListeLots(idConsultationAllotie);
        MpeLotDetails result = mpeWs.getLot(lots.get(0).getHydraId());
        assertNotNull(result);
        assertJsonEqual("getLot", result);
    }

    @Test
    void Given_IdLotInvalid_When_getLot_Then_ThrowEception() throws Exception {
        String token = mpeWs.getAuthentificationV2(null);
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            mpeWs.getLot("/api/v2/lots/9999999999999999999999999");
        });
        assertEquals("{\"type\":\"https:\\/\\/tools.ietf.org\\/html\\/rfc2616#section-10\",\"title\":\"An error occurred\",\"detail\":\"Not Found\"}", exception.getMessage());
    }


    @Test
    void Given_IdConsultation_When_getFichiers_Then_ReturnListe() throws Exception {
        //passer id d'une consultation avec fichiers
        List<MpeFichier> result = mpeWs.getFichiers(idConsultationNonAllotie);
        assertNotNull(result);
        assertThat(result.size()).isEqualTo(1);
        MpeFichier fichier = result.get(0);
        assertNotNull(fichier);
        assertThat(fichier.getHash()).isEqualTo("518137d403e82bec3f1d17168a0c1f9834f23a3f");
        assertThat(fichier.getType()).isEqualTo("AUTRE_PIECE");
        assertThat(fichier.getName()).isEqualTo("DCE NEW.zip");

    }

    @Test
    void Given_IdConsultation_When_getConsultationByReferenceUnique_Then_ReturnListe() throws Exception {
        String token = mpeWs.getAuthentificationV2("https://mpe-release.local-trust.com");
        //passer id d'une consultation avec fichiers
        List<MpeConsultation> result = mpeWs.getConsultationByReferenceUnique("508128");
        assertNotNull(result);

    }

    @Test
    @Disabled
    void Given_Consultation_When_saveConsultation_Then_Ok() throws Exception {
        String token = mpeWs.getAuthentificationV2("https://mpe-release.local-trust.com");
        //passer id d'une consultation avec fichiers

        File src = new File("src/test/resources/save_consultation.json");
        MpeConsultation consultation = objectMapper.readValue(src, MpeConsultation.class);
        consultation.setId(null);
        consultation.setReference("FLINK-TNCP-" + ZonedDateTime.now().toEpochSecond());
        MpeConsultation result = mpeWs.saveConsultation(consultation);
        assertNotNull(result);

    }

    @Test
    @Disabled
    void Given_Consultation_When_updateConsultation_Then_Ok() throws Exception {
        String token = mpeWs.getAuthentificationV2("https://mpe-release.local-trust.com");
        //passer id d'une consultation avec fichiers

        File src = new File("src/test/resources/save_consultation.json");
        MpeConsultation consultation = objectMapper.readValue(src, MpeConsultation.class);
        consultation.setId(null);
        consultation.setReference("FLINK-TNCP-" + ZonedDateTime.now().toEpochSecond());
        MpeConsultation result = mpeWs.updateConsultation(consultation.getId(), consultation);
        assertNotNull(result);

    }

    @Test
    @Disabled
    void Given_IdFichier_When_downloadFichier_Then_Download() throws Exception {
        List<MpeFichier> list = mpeWs.getFichiers(idConsultationNonAllotie);
        assertNotNull(list);
        assertThat(list.size()).isEqualTo(1);
        MpeFichier fichier = list.get(0);
        File result = mpeWs.downloadFichier(fichier.getId());
        assertNotNull(result);

    }

    @Test
    void Given_IdExterne_When_GetProcedure_Then_ReturnOneElement() throws Exception {
        MpeReferentiel procedure = mpeWs.getReferentielsByTypeAndIdExterne("procedures", "AOO");
        assertNotNull(procedure);
        assertNotNull(procedure.getId());
        assertThat(procedure.getIdExterne()).isEqualTo("AOO");
    }

    @Test
    void Given_IdExterne_When_GetContrats_Then_ReturnOneElement() throws Exception {
        MpeReferentiel procedure = mpeWs.getReferentielsByTypeAndIdExterne("contrats", "mar");
        assertNotNull(procedure);
        assertNotNull(procedure.getId());
        assertThat(procedure.getIdExterne()).isEqualTo("mar");
    }

  @Test
    void Given_IdExterne_When_GetNaturePrestation_Then_ReturnOneElement() throws Exception {
        MpeReferentiel procedure = mpeWs.getReferentielsByTypeAndIdExterne("nature-prestations", "TV");
        assertNotNull(procedure);
        assertNotNull(procedure.getId());
        assertThat(procedure.getIdExterne()).isEqualTo("TV");
    }



}
