package com.atexo.flink.ws.tcnp;

import com.atexo.flink.config.*;
import com.atexo.flink.ws.mpe.model.NaturePrestationEnum;
import com.atexo.flink.ws.tcnp.model.PisteToken;
import com.atexo.flink.ws.tcnp.model.consultation.*;
import com.atexo.flink.ws.tcnp.model.contrat.TNCPContratMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.atexo.flink.config.AtexoUtils.getFileNameUploadTncp;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled
class TncpWsTest {
    JarFlinkConfiguration configuration = JarFlinkConfiguration.builder().pisteUrl("https://api.rec.piste.gouv.fr")
            .pisteClientId("6f63a252-eb6c-4e62-ae77-c8fe2be79661")
            .pisteSecretId("50626b85-a893-4167-a9ab-5554ba97da19").pisteAuthUrl("https://oauth.rec.piste.gouv.fr/api/oauth/token")
            .build();
    private final TncpWs tncpWs = new TncpWs(configuration, new RestTemplateConfig().atexoRestTemplate());
    private String token;
    private String referenceUniqueTest;
    private TncpConsultation consultation;

    private ReponseUpload responseUpload;
    private ReponsePreUpload reponsePreUpload;

    public static <T> T getObjectFromFile(String fileName, Class<T> tClass) throws IOException {
        Path pathSource = Paths.get(fileName).toAbsolutePath();
        ObjectMapper objectMapper = AtexoUtils.getMapper();
        return objectMapper.readValue(pathSource.toFile(), tClass);
    }

    @Test
    @Order(1)
    void Given_Params_Then_ReturnToken() {
        PisteToken result = tncpWs.getToken();
        assertNotNull(result);
        assertNotNull(result.getToken());
        this.token = result.getToken();
    }

    @Test
    @Order(2)
    void Given_Consultation_When_addConsultation_Then_ReturnReferenceUnique() {
        //token
        TncpConsultation consultation = getTncpConsultation();
        if (this.token == null) {
            getToken();
        }
        String result = tncpWs.addConsultation(token, consultation);
        assertNotNull(result);
        this.referenceUniqueTest = result;
    }

    @Test
    @Order(3)
    void Given_InvalidConsultation_When_addConsultation_Then_ThrowErreur() {
        //token
        TncpConsultation consultationVide = new TncpConsultation();
        if (this.token == null) {
            getToken();
        }
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.addConsultation(token, consultationVide);
        });
        assertEquals(JobExeptionEnum.ENVOI_CONSULTATION_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(4)
    void Given_ReferenceUnique_When_getConsultation_Then_ReturnConsultation() {
        if (this.token == null) {
            getToken();
        }
        if (referenceUniqueTest == null) {
            this.referenceUniqueTest = tncpWs.addConsultation(token, getTncpConsultation());
        }
        TncpConsultation result = tncpWs.getConsultation(token, referenceUniqueTest);
        assertNotNull(result);
        this.consultation = result;
    }

    @Test
    @Order(5)
    void Given_InvalidReferenceUnique_When_getConsultation_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.getConsultation(token, "100100100");
        });
        assertEquals(JobExeptionEnum.RECUPERATION_CONSULTATION_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(6)
    @Disabled
    void Given_ReferenceUnique_When_deleteConsultation_Then_delete() {
        if (this.token == null) {
            getToken();
        }
        String url = "https://api.rec.piste.gouv.fr";
        if (referenceUniqueTest == null) {
            this.referenceUniqueTest = tncpWs.addConsultation(token, getTncpConsultation());
        }
        if (consultation == null) {
            this.consultation = tncpWs.getConsultation(token, referenceUniqueTest);
        }
        tncpWs.deleteConsultation(token, referenceUniqueTest, consultation.getStructureAcheteur().getCodeService(), consultation.getStructureAcheteur().getSiret());
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.getConsultation(token, referenceUniqueTest);
        });
        assertEquals("404 Not Found: [no body]", exception.getMessage());
    }

    @Test
    @Order(7)
    void Given_InvalidParameters_When_deleteConsultation_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.deleteConsultation(token, "100100100", "xxxx", "yyyy");
        });
        assertEquals(JobExeptionEnum.SUPPRESSION_CONSULTATION_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(8)
    void Given_Lot_When_addLots_Then_ReturnListe() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation2 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation2);
        LotTNCP lot = getLot();
        List<LotTNCP> lots = new ArrayList<>();
        lots.add(lot);
        List<LotTNCP> result = tncpWs.addLots(token, result1, consultation2.getStructureAcheteur().getCodeService(), consultation2.getStructureAcheteur().getSiret(), lots);
        assertNotNull(result);
        assertThat(result).hasSize(1);
    }

    @Test
    @Order(9)
    void Given_InvalidLot_When_addLots_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation2 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation2);
        LotTNCP lot = new LotTNCP();
        List<LotTNCP> lots = new ArrayList<>();
        lots.add(lot);
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.addLots(token, result1, consultation2.getStructureAcheteur().getCodeService(), consultation2.getStructureAcheteur().getSiret(), lots);
        });
        assertEquals(JobExeptionEnum.ENVOI_LOTS_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(10)
    void Given_Token_When_PreUpload_Then_ReturnDatas() {
        if (this.token == null) {
            getToken();
        }
        ReponsePreUpload result = tncpWs.preUpload(token);
        assertNotNull(result);
        assertNotNull(result.getAppKey());
        assertNotNull(result.getFolderName());
        assertNotNull(result.getAuthorizationForUpload());
        assertNotNull(result.getStorageURL());
        assertNotNull(result.getVirtualFolderId());
    }

    @Test
    @Order(11)
    void Given_FileMoins10Mo_When_UploadMoins10Mo_Then_Upload() {
        ReponseUpload result2 = getReponseUpload("src/test/resources/piece-jointe.pdf");
        assertNotNull(result2);
        assertNotNull(result2.getStatus());
    }

    @Test
    @Order(13)
    void Given_FilePlus10Mo_When_UploadPlus10Mo_Then_Upload() {
        ReponseUpload result2 = getReponseUpload("src/test/resources/newtest13Mo");
        assertNotNull(result2);
        assertNotNull(result2.getStatus());
    }

    @Test
    @Order(14)
    void Given_InvalidParameters_When_UploadPlus10Mo_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            String idTechnique = getFileNameUploadTncp(UUID.randomUUID().toString());
            tncpWs.uploadFile(ReponsePreUpload.builder().build(), new File("src/test/resources/newtest13Mo"), idTechnique);
        });
        assertEquals(JobExeptionEnum.UPLOAD_PLUS_10MO.toString(), exception.getType());
    }

    @Test
    @Order(15)
    void Given_EnvoiPieceJointe_When_addPieceJointeToConsultation_Then_Add() {
        if (this.token == null) {
            getToken();
        }
        String name = "piece-jointe.pdf";
        String pathname = "src/test/resources/" + name;
        if (this.responseUpload == null) {
            getReponseUpload(pathname);
        }
        TncpConsultation consultation3 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation3);
        consultation3.setReferenceUnique(result1);
        EnvoiPieceJointe envoi = getEnvoiPJ(consultation3, pathname);
        tncpWs.addPieceJointeToConsultation(token, envoi);
        TncpConsultation result2 = tncpWs.getConsultation(token, result1);
        assertNotNull(result2);
        assertThat(result2.getPieceJointeConsultations()).hasSize(1);
    }

    @Test
    @Order(16)
    void Given_EnvoiPieceJointeAndInvalidRferenceUnique_When_addPieceJointeToConsultation_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        PieceJointeConsultation pj = PieceJointeConsultation.builder()
                .fileId("235598629")
                .fileName("FAVORIS.odt")
                .folderId("200260959")
                .hashSha256("458b339a7c294b127e3f59d82a5ea0ba5d78a817b7a0a0085d2fd1afa5c4d2df")
                .build();
        EnvoiPieceJointe envoi2 = EnvoiPieceJointe.builder()
                .referenceUnique("eeeeeeeeeeeeeee")
                .codeService("vvvv")
                .piecesJointes(pj)
                .build();
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.addPieceJointeToConsultation(token, envoi2);
        });
        assertEquals(JobExeptionEnum.ASSOCIATION_CONSULTATION_PIECE_JOINTE_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(17)
    void Given_ValidParameters_When_validateConsultation_Then_Validate() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation4 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation4);
        var added = this.tncpWs.getConsultation(this.token, result1);
        ReponseValidation result2 = tncpWs.validateConsultation(token, result1, consultation4.getStructureAcheteur().getCodeService(), consultation4.getStructureAcheteur().getSiret(), ZonedDateTime.parse(added.getDateMiseEnLigne()));
        assertNotNull(result2);
        assertEquals("VALIDE", result2.getConsultationStatus());
        TncpConsultation result3 = tncpWs.getConsultation(token, result1);
        assertNotNull(result3);
        assertEquals(StatutConsultEnum.PUBLIE, result3.getStatutConsult());
    }

    @Test
    @Order(18)
    void Given_InvalidParameters_When_validateConsultation_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation4 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation4);
        consultation4.setReferenceUnique(result1);
        ZonedDateTime dateMiseEnLigne = ZonedDateTime.parse("2023-04-02T00:00:00+00:00");
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.validateConsultation(token, "aaaaaaaaaaaaaa", consultation4.getStructureAcheteur().getCodeService(), consultation4.getStructureAcheteur().getSiret(), dateMiseEnLigne);
        });
        assertEquals(JobExeptionEnum.VALIDATION_CONSULTATION_TNCP.toString(), exception.getType());
    }

    @Test
    @Order(19)
    void Given_Dates_When_updateConsultationPublie_Then_Update() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation7 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation7);
        consultation7.setReferenceUnique(result1);
        ZonedDateTime dateMiseEnLigne = ZonedDateTime.parse("2023-04-02T00:00:00+00:00");
        tncpWs.validateConsultation(token, result1, consultation7.getStructureAcheteur().getCodeService(), consultation7.getStructureAcheteur().getSiret(), dateMiseEnLigne);
        ZonedDateTime dateDLRO = ZonedDateTime.parse("2023-10-12T00:00:00+00:00");
        ZonedDateTime dateFinAffichage = ZonedDateTime.parse("2023-10-13T00:00:00+00:00");
        tncpWs.updateConsultation(token, consultation7.getReferenceUnique(), dateDLRO, dateFinAffichage);
        TncpConsultation result2 = tncpWs.getConsultation(token, result1);
        assertNotNull(result2);
        assertEquals("2023-10-12T00:00:00+00:00", result2.getSpecifiqueLot().getDateLimiteRemiseOffre());
        assertEquals("2023-10-13T00:00:00+00:00", result2.getSpecifiqueLot().getDateFinAffichage());
    }

    @Test
    @Order(20)
    void Given_InvalidDates_When_updateConsultationPublie_Then_ThrowErreur() {
        if (this.token == null) {
            getToken();
        }
        TncpConsultation consultation7 = getTncpConsultation();
        String result1 = tncpWs.addConsultation(token, consultation7);
        consultation7.setReferenceUnique(result1);
        ZonedDateTime dateMiseEnLigne = ZonedDateTime.parse("2023-04-02T00:00:00+00:00");
        tncpWs.validateConsultation(token, result1, consultation7.getStructureAcheteur().getCodeService(), consultation7.getStructureAcheteur().getSiret(), dateMiseEnLigne);
        ZonedDateTime dateDLRO = ZonedDateTime.parse("2023-10-12T00:00:00+00:00");
        ZonedDateTime dateFinAffichage = ZonedDateTime.parse("2023-10-17T00:00:00+00:00");
        AtexoJobException exception = assertThrows(AtexoJobException.class, () -> {
            tncpWs.updateConsultation(token, consultation7.getReferenceUnique(), dateDLRO, dateFinAffichage);
        });
        assertEquals(JobExeptionEnum.MODIFICATION_CONSULTATION_PUBLIE.toString(), exception.getType());
    }

    @Test
    @Order(21)
    void Given_Contrat_When_PublicationContrat_Then_ReturnOk() throws IOException {
        if (this.token == null) {
            getToken();
        }

        String tncpContrat = getObjectFromFile("src/test/resources/envoi-contrat.json", String.class);
        TNCPContratMessage result = tncpWs.sendPublicationContrat(tncpContrat, token);
        assertNotNull(result);
        assertNotNull(result.getReferenceUnique());
    }

    @Test
    @Order(22)
    void Given_Contrat_When_GetConsultations_Then_ReturnOk() throws IOException {
        if (this.token == null) {
            getToken();
        }

        ZonedDateTime now = ZonedDateTime.now();
        PageTncpConsultation result = tncpWs.getConsultations(now.minusDays(1), now, "PUBLIE", token, 1);
        assertNotNull(result);
    }

    private void getToken() {
        this.token = tncpWs.getToken().getToken();
    }

    private TncpConsultation getTncpConsultation() {
        LieuExecution lieu1 = LieuExecution.builder()
                .nomLieu("Paris")
                .build();
        LieuExecution lieu2 = LieuExecution.builder()
                .nomLieu("Dijon")
                .build();
        List<LieuExecution> lieuxExecutions = new ArrayList<>();
        lieuxExecutions.add(lieu1);
        lieuxExecutions.add(lieu2);
        CodeCpvSecondaire code1 = CodeCpvSecondaire.builder()
                .codeCpv("48111111")
                .build();
        CodeCpvSecondaire code2 = CodeCpvSecondaire.builder()
                .codeCpv("48222222")
                .build();
        List<CodeCpvSecondaire> cpvSecondaires = new ArrayList<>();
        cpvSecondaires.add(code1);
        cpvSecondaires.add(code2);
        ModaliteReponse modalite1 = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.SIGNATURE_ELECTRONIQUE)
                .valModaliteReponse(ValModaliteReponseEnum.AUTORISE)
                .build();
        ModaliteReponse modalite2 = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.REPONSE_ELECTRONIQUE)
                .valModaliteReponse(ValModaliteReponseEnum.AUTORISE)
                .build();
        List<ModaliteReponse> modaliteReponses = new ArrayList<>();
        modaliteReponses.add(modalite1);
        modaliteReponses.add(modalite2);
        SpecifiqueLot specifiqueLot = SpecifiqueLot.builder()
                .dateLimiteRemiseOffre("2023-10-02T00:00:00+00:00")
                .dateFinAffichage("2023-10-03T00:00:00+00:00")
                .accesRestreint(false)
                .categoriePrin(NaturePrestationEnum.FOURNITURES)
                .lieuExecutions(lieuxExecutions)
                .codeCpvPrincipal("48000000")
                .codeCpvSecondaires(cpvSecondaires)
                .modaliteReponses(modaliteReponses)
                .build();
        TncpConsultation consultation = TncpConsultation.builder()
                .specifiqueLot(specifiqueLot)
                .reference("ref332")
                .idConPlateforme("33388810102")
                .idConEurUnique("3334102")
                .intitule("intitule332")
                .natureContrat(NatureContratEnum.MARCHE.name())
                .objet("objet332")
                .typeProcedure(TypeProcedureEnum.DIALOGUE_COMPETITIF.name())
                .statutConsult(StatutConsultEnum.ELABORE)
                .dateMiseEnLigne("2023-05-11T00:31:00+02:00")
                .commentaireInterne("comm333")
                .urlConsultation("")
                .reponsePaTiers(true)
                .structureAcheteur(StructureAcheteur.builder()
                        .raisonSociale("denominationOrg")
                        .adresse1("adresse1")
                        .codePostal("75009")
                        .formeJuridique("formeJuridique")
                        .entiteAdjudicatrice(false)
                        .activiteStructurePublique("12345")
                        .siret("12345678911111")
                        .codeService("222")
                        .contactAcheteurs(List.of(ContactAcheteur.builder()
                                .nom("Test")
                                .prenom("Atexo")
                                .telecoms(List.of(Telecom.builder()
                                        .type(TypeTelecomEnum.TELEPHONE)
                                        .numero("+337556565")
                                        .build()))
                                .build()))
                        .build())
                .build();
        return consultation;
    }

    private LotTNCP getLot() {
        LotTNCP lot = LotTNCP.builder()
                .id(null)
                .description("unedescriptiontest")
                .intitule("intituletest")
                .numeroLot(4)
                .montantLot(13000.0)
                .statutLot(StatutConsultEnum.ELABORE)
                .specifiqueLot(SpecifiqueLot.builder()
                        .codeCpvPrincipal("340000")
                        .build())
                .build();
        return lot;
    }

    private EnvoiPieceJointe getEnvoiPJ(TncpConsultation consultation, String fileName) {
        File file = new File(fileName);


        //voir pour recuperer donnees apres un upload
        PieceJointeConsultation pj = null;
        try {
            String hashSha256 = DigestUtils.sha256Hex(new FileInputStream(file));
            pj = PieceJointeConsultation.builder()
                    .fileId(this.responseUpload.getDataFileId())
                    .fileName(AtexoUtils.getFileNameUploadTncp(hashSha256))
                    .nomFonctionnelMetier(file.getName())
                    .folderId(this.reponsePreUpload.getVirtualFolderId())
                    .hashSha256(hashSha256)
                    .latestVersionId(this.responseUpload.getDataFileVersionId())
                    .size(1L)
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return EnvoiPieceJointe.builder()
                .referenceUnique(consultation.getReferenceUnique())
                .codeService(consultation.getStructureAcheteur().getCodeService())
                .siret(consultation.getStructureAcheteur().getSiret())
                .piecesJointes(pj)
                .build();
    }

    private ReponseUpload getReponseUpload(String pathname) {
        if (this.token == null) {
            getToken();
        }

        ReponsePreUpload result = tncpWs.preUpload(token);
        this.reponsePreUpload = result;
        File path = new File(pathname);
        String idTechnique = getFileNameUploadTncp(UUID.randomUUID().toString());
        ReponseUpload reponseUpload = tncpWs.uploadFile(result, path, idTechnique);
        this.responseUpload = reponseUpload;
        return reponseUpload;
    }
}
