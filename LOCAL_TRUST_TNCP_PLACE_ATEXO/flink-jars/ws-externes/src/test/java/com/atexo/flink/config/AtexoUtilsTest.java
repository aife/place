package com.atexo.flink.config;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AtexoUtilsTest {

    @Test
    void Given_IdFichier_When_downloadFichier_Then_Download() throws Exception {
        String path = "src/test/resources/newtest13Mo";
        String result = AtexoUtils.getHashed(new File(path).toPath());
        assertEquals("d874d23e022cd2db859e6d6825af392f8f28d0996f8be2eb6e31e01561087d67", result);

    }
}
