package com.atexo.flink.ws.mpe.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClauseN1 {

    private String label;
    private String slug;
    private List<ClauseN2> clausesN2;
}
