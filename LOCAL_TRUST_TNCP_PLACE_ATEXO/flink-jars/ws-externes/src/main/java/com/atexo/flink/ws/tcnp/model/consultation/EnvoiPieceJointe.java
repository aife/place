package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EnvoiPieceJointe {

    private String codeService;
    private String referenceUnique;
    private String siret;
    private PieceJointeConsultation piecesJointes;
}
