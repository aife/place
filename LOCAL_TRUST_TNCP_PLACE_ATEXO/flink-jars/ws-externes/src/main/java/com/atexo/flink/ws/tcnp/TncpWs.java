package com.atexo.flink.ws.tcnp;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.JarFlinkConfiguration;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.ws.tcnp.model.PisteToken;
import com.atexo.flink.ws.tcnp.model.consultation.*;
import com.atexo.flink.ws.tcnp.model.contrat.TNCPContratMessage;
import com.atexo.flink.ws.tcnp.model.contrat.TNCPContratResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.flink.config.AtexoUtils.*;

@Slf4j
public class TncpWs {
    private final RestTemplate restTemplate;
    private static final String PATTERN_FORMAT = "yyyy-MM-dd'T'HH:mm:ssxxx";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_FORMAT);
    private static final String PATTERN_FORMAT_LOCAL = "dd/MM/yyyy";
    private static final DateTimeFormatter formatterLocal = DateTimeFormatter.ofPattern(PATTERN_FORMAT_LOCAL);
    private final JarFlinkConfiguration configuration;

    public TncpWs(JarFlinkConfiguration configuration, RestTemplate restTemplate) {
        this.configuration = configuration;
        this.restTemplate = restTemplate;
    }

    private static final String MESSAGE = "Url d'authentification | client id | secret id ne peuvent pas être null";

    public String addConsultation(String token, TncpConsultation tncpConsultation) {
        try {
            String url = configuration.getPisteUrl() + "/tncp/consultations/v1/consultations";
            log.info("Ajout de la consultation TNCP URL=[{}] avec le json {}", url, AtexoUtils.getMapper().writeValueAsString(tncpConsultation));
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            return restTemplate.postForObject(url, new HttpEntity<>(tncpConsultation,
                    headers), String.class);
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(tncpConsultation, e.getResponseBodyAsString(), JobExeptionEnum.ENVOI_CONSULTATION_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(tncpConsultation, e.getMessage(), JobExeptionEnum.ENVOI_CONSULTATION_TNCP, e);
        }
    }

    public ReponsePreUpload preUpload(String token) {
        String url = configuration.getPisteUrl() + "/tncp/consultations/v1/pj";
        log.info("Recuperation des donnees obligatoires pour le upload > 10 Mo : URL=[{}]", url);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            return restTemplate.exchange(url, HttpMethod.GET,
                    new HttpEntity<>(headers), ReponsePreUpload.class).getBody();

        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.PRE_UPLOAD, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.PRE_UPLOAD, e);
        }
    }

    public ReponseUpload uploadFile(ReponsePreUpload preUpload, File path, String idTechnique) {
        log.info("Upload d un fichier {}", path);
        try {
            Path pathToFile = path.toPath();
            File toFile = pathToFile.toAbsolutePath().toFile();
            FileSystemResource file = new FileSystemResource(toFile);
            String hashed = getHashed(pathToFile);
            MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
            bodyMap.add("fileData", file);
            bodyMap.add("virtualFolderId", preUpload.getVirtualFolderId());
            bodyMap.add("sha256", hashed);
            bodyMap.add("transfer-encoding", "binary");
            bodyMap.add("filename", idTechnique);
            bodyMap.add("sessionKey", preUpload.getAuthorizationForUpload());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", preUpload.getAuthorizationForUpload());
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.add("AppKey", preUpload.getAppKey());
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(preUpload.getStorageURL() + "/v2/mime/files");
            builder.queryParam("filepath", preUpload.getFolderName() + "/" + idTechnique);
            String uriBuilder = encodeUrl(builder.build().encode().toUriString());
            log.info("Ajout dans TNCP : URL[{}] avec le json {}", uriBuilder, bodyMap);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
            SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setBufferRequestBody(false);
            restTemplate.setRequestFactory(requestFactory);
            return restTemplate.exchange(uriBuilder, HttpMethod.POST,
                    requestEntity, ReponseUpload.class).getBody();

        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.UPLOAD_PLUS_10MO, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.UPLOAD_PLUS_10MO, e);
        }
    }


    public String addPieceJointeToConsultation(String token, EnvoiPieceJointe envoi) {
        String url = configuration.getPisteUrl() + "/tncp/consultations/v1/pj";
        log.info("Ajout de piece jointe pour la consultation de reference={} URL=[{}] avec le json {}", envoi.getReferenceUnique(), url,
                AtexoUtils.writeValueAsString(envoi));
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            return restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(envoi, headers), String.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(envoi, e.getResponseBodyAsString(), JobExeptionEnum.ASSOCIATION_CONSULTATION_PIECE_JOINTE_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(envoi, e.getMessage(), JobExeptionEnum.ASSOCIATION_CONSULTATION_PIECE_JOINTE_TNCP, e);
        }
    }

    public String deletePieceJointeConsultation(String token, String fileName, String nomFonctionnelMetier, String referenceUnique, String siret) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configuration.getPisteUrl() + "/tncp/consultations/v1/pj");
            builder.queryParam("fileName", fileName)
                    .queryParam("referenceUnique", referenceUnique)
                    .queryParam("nomFonctionnelMetier", nomFonctionnelMetier)
                    .queryParam("siret", siret);
            String uriBuilder = builder.build().toUriString();
            log.info("Supression de la pièce jointe : DELETE URL=[{}]", uriBuilder);

            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.setBearerAuth(token);
            return restTemplate.exchange(uriBuilder, HttpMethod.DELETE, new HttpEntity<>(headers), String.class).getBody();
        } catch (RestClientResponseException e) {
            log.error("Erreur lors de la supression de piece jointe {}", e.getResponseBodyAsString());
            throw new AtexoJobException(null, "Erreur lors de la suppression du fichier id=" + fileName, JobExeptionEnum.SUPPRESSION_PIECE_JOINTE, e);
        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors de la supression de piece jointe {}", e.getMessage());
            throw new AtexoJobException(null, "Erreur lors de la suppression du fichier id=" + fileName, JobExeptionEnum.SUPPRESSION_PIECE_JOINTE, e);
        }
    }

    public TncpConsultation getConsultation(String token, String referenceUnique) {
        String url = configuration.getPisteUrl() + "/tncp/consultations/v1/consultations/" + referenceUnique;
        log.info("Recuperation de la consultation de : GET URL=[{}]", url);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            return restTemplate.exchange(url, HttpMethod.GET,
                    new HttpEntity<>(headers), TncpConsultation.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_CONSULTATION_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.RECUPERATION_CONSULTATION_TNCP, e);
        }
    }

    public void deleteConsultation(String token, String reference, String codeService, String siret) {
        String uri = configuration.getPisteUrl() + "/tncp/consultations/v1/consultations";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        builder.queryParam("codeService", codeService)
                .queryParam("referenceUnique", reference)
                .queryParam("siret", siret);
        String uriBuilder = encodeUrl(builder.build().encode().toUriString());
        log.info("Supression de la consultation : DELETE URL=[{}]", uriBuilder);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            restTemplate.exchange(uriBuilder, HttpMethod.DELETE, new HttpEntity<>(headers), String.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.SUPPRESSION_CONSULTATION_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.SUPPRESSION_CONSULTATION_TNCP, e);
        }
    }

    public PisteToken getToken() {
        String oauthUrl = configuration.getPisteAuthUrl();
        String clientId = configuration.getPisteClientId();
        String clientSecret = configuration.getPisteSecretId();
        if (oauthUrl == null || clientId == null || clientSecret == null)
            throw new AtexoJobException(null, MESSAGE, JobExeptionEnum.CONFIGURATION_TNCP_MANQUANTE);
        //Cliend id and client secret
        var keys = clientId + ":" + clientSecret;

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("grant_type", "client_credentials");
        String form = parameters.keySet().stream()
                .map(key -> key + "=" + URLEncoder.encode(parameters.get(key), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));

        String encoding = Base64.getEncoder().encodeToString(keys.getBytes());
        //Creating a HttpClient object
        try (CloseableHttpClient httpclient = getNewHttpClient()) {
            HttpPost httpPost = new HttpPost(oauthUrl);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.addHeader("Authorization", "Basic " + encoding);
            httpPost.setEntity(new ByteArrayEntity(form.getBytes()));
            //Executing the Get request
            org.apache.http.HttpResponse httpresponse = httpclient.execute(httpPost);
            //Printing the status line
            int statusCode = httpresponse.getStatusLine().getStatusCode();
            String responseBody = EntityUtils.toString(httpresponse.getEntity(), StandardCharsets.UTF_8);
            if (statusCode == 200) {
                PisteToken pisteToken = AtexoUtils.getMapper().readValue(responseBody, PisteToken.class);
                if (pisteToken == null || pisteToken.getToken() == null) {
                    throw new AtexoJobException(null, "le token est null " + responseBody, JobExeptionEnum.RECUPERATION_TNCP_TOKEN);
                }
                return pisteToken;
            }

            throw new AtexoJobException(null, responseBody, JobExeptionEnum.RECUPERATION_TNCP_TOKEN);
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_TNCP_TOKEN, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.RECUPERATION_TNCP_TOKEN, e);
        }
    }

    public ReponseValidation validateConsultation(String token, String reference, String codeService, String siret, ZonedDateTime dateDeMiseEnLigne) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configuration.getPisteUrl() + "/tncp/consultations/v1/consultations/validation");
        builder.queryParam("codeService", codeService)
                .queryParam("referenceUnique", reference)
                .queryParam("siret", siret)
                .queryParam("dateDeMiseEnLigne", getFormat(dateDeMiseEnLigne, formatterLocal));
        String uriBuilder = encodeUrl(builder.build().encode().toUriString());
        try {
            log.info("Validation de la consultation avec : PUT URL=[{}]", uriBuilder);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            return restTemplate.exchange(uriBuilder, HttpMethod.PUT, new HttpEntity<>(headers), ReponseValidation.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.VALIDATION_CONSULTATION_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.VALIDATION_CONSULTATION_TNCP, e);
        }
    }

    public List<LotTNCP> addLots(String token, String reference, String codeService, String siret, List<LotTNCP> lots) {
        log.info("Ajout de lot pour la consultation de reference={}", reference);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configuration.getPisteUrl() + "/tncp/consultations/v1/lots");
        builder.queryParam("codeService", codeService)
                .queryParam("referenceUnique", reference)
                .queryParam("siret", siret);
        String uriBuilder = encodeUrl(builder.build().encode().toUriString());
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            log.info("Ajout de lot pour la consultation avec POST : URL[{}] avec le json {}", uriBuilder, AtexoUtils.writeValueAsString(lots));
            ResponseEntity<List<LotTNCP>> exchange = restTemplate.exchange(uriBuilder, HttpMethod.POST,
                    new HttpEntity<>(lots, headers), new org.springframework.core.ParameterizedTypeReference<>() {
                    });
            if (exchange.getStatusCode() != HttpStatus.OK) {
                throw new AtexoJobException(lots, "Erreur lors de l'ajout de lot", JobExeptionEnum.ENVOI_LOTS_TNCP);
            }
            return exchange.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(lots, e.getResponseBodyAsString(), JobExeptionEnum.ENVOI_LOTS_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(lots, e.getMessage(), JobExeptionEnum.ENVOI_LOTS_TNCP, e);
        }
    }

    public void updateConsultation(String token, String reference, ZonedDateTime dateDLRO, ZonedDateTime dateFinAffichage) {
        log.info("Modification de la consultation publiee avec : reference={}  dateDLRO={} dateFinAffichage={}", reference, dateDLRO, dateFinAffichage);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configuration.getPisteUrl() + "/tncp/consultations/v1/consultations/PUBLIE");
        builder.queryParam("referenceUnique", reference)
                .queryParam("dateFinAffichage", getFormat(dateFinAffichage, formatter))
                .queryParam("dateLimiteRemiseOffre", getFormat(dateDLRO, formatter));

        String uriBuilder = encodeUrl(builder.build().encode().toUriString());
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            log.info("Modification de la consultation publiee : PUT URL=[{}]", uriBuilder);
            restTemplate.exchange(uriBuilder, HttpMethod.PUT, new HttpEntity<>(headers), String.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.MODIFICATION_CONSULTATION_PUBLIE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.MODIFICATION_CONSULTATION_PUBLIE, e);
        }
    }


    private String encodeUrl(String url) {
        while (url.indexOf('+') > -1) {
            url = url.replace("+", "%2B");
        }
        while (url.indexOf(' ') > -1) {
            url = url.replace(" ", "%20");
        }
        return url;
    }

    public TNCPContratMessage sendPublicationContrat(String tncpContrat, String token) {
        String url = configuration.getPisteUrl() + "/dume/donneesEssentielles/";
        log.info("Envoi de la publication de contrat TNCP URL=[{}] avec le json {}", url, AtexoUtils.writeValueAsString(tncpContrat));
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("X-Api-Version", "2");
            headers.setAccept(List.of(MediaType.valueOf("application/json;charset=UTF-8")));
            headers.setContentType(MediaType.APPLICATION_JSON);
            ResponseEntity<TNCPContratResponse> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(tncpContrat, headers), TNCPContratResponse.class);
            if (exchange.getStatusCode() != HttpStatus.OK)
                throw new AtexoJobException(tncpContrat, AtexoUtils.writeValueAsString(exchange.getBody()), JobExeptionEnum.ENVOI_CONTRAT_TNCP);
            TNCPContratResponse response = exchange.getBody();
            if (response == null) {
                throw new AtexoJobException(tncpContrat, "La réponse est null", JobExeptionEnum.ENVOI_CONTRAT_TNCP);
            }
            TNCPContratMessage contratMessage = response.getResponse();
            if (contratMessage == null) {
                throw new AtexoJobException(tncpContrat, "La réponse est null", JobExeptionEnum.ENVOI_CONTRAT_TNCP);
            }
            if (!CollectionUtils.isEmpty(contratMessage.getMessageList()) && contratMessage.getMessageList().stream().anyMatch(message -> message.getType().equalsIgnoreCase("ERREUR")))
                throw new AtexoJobException(tncpContrat, AtexoUtils.writeValueAsString(response), JobExeptionEnum.ENVOI_CONTRAT_TNCP);
            return contratMessage;

        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.ENVOI_CONTRAT_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.ENVOI_CONTRAT_TNCP, e);
        }
    }

    public String lastNotification(final String token) {
        String lastNotification = configuration.getPisteUrl() + "/tncp/notifications/v1/last";
        log.info("Récupération de la dernière notification auprès de TNCP avec le token = {} de URL = {}", token, lastNotification);
        try {
            var headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(List.of(MediaType.APPLICATION_JSON));
            return restTemplate.exchange(lastNotification, HttpMethod.GET, new HttpEntity<>(headers), String.class).getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(null, e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_NOTIFICATIONS_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(null, e.getMessage(), JobExeptionEnum.RECUPERATION_NOTIFICATIONS_TNCP, e);
        }
    }

    public PageTncpConsultation getConsultations(ZonedDateTime dateMiseEnLigneMin, ZonedDateTime dateMiseEnLigneMax, String statutConsult, String token, int page) {
        try {

            List<String> criteres = new ArrayList<>();
            if (dateMiseEnLigneMin != null) {
                criteres.add("dateMiseEnLigne>=" + dateMiseEnLigneMin.format(formatter));
            }
            if (dateMiseEnLigneMax != null) {
                criteres.add("dateMiseEnLigne<=" + dateMiseEnLigneMax.format(formatter));
            }

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(configuration.getPisteUrl() + "/tncp/consultations/v1/consultations/");
            if (statutConsult != null) {
                builder.queryParam("statutConsult=" + statutConsult);
            }
            if (!criteres.isEmpty()) {
                builder.queryParam("criteres=" + String.join(" and ", criteres));
            }
            builder.queryParam("page", page);
            String uriBuilder = encodeUrl(builder.build().encode().toUriString());
            log.info("Recuperation des consultations TNCP URL=[{}]", uriBuilder);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            var response = restTemplate.exchange(uriBuilder, HttpMethod.GET, new HttpEntity<>(headers), PageTncpConsultation.class);
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_CONSULTATIONS_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_CONSULTATIONS_TNCP, e);
        }
    }

    public boolean abonnementConsultation(String referenceUnique, String token) {
        try {
            String url = configuration.getPisteUrl() + "/tncp/consultations/v1/abonnement/" + referenceUnique;
            log.info("Abonnement à la consultation TNCP URL=[{}] avec la referenceUnique=[{}]", url, referenceUnique);
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            var response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(headers), Object.class);
            if (!response.getStatusCode().is2xxSuccessful()) {
                throw new AtexoJobException(referenceUnique, "L'abonnement a retourné une erreur", JobExeptionEnum.ABONNEMENT_CONSULTATION_TNCP);
            }
            return true;
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.ABONNEMENT_CONSULTATION_TNCP, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.ABONNEMENT_CONSULTATION_TNCP, e);
        }
    }
}
