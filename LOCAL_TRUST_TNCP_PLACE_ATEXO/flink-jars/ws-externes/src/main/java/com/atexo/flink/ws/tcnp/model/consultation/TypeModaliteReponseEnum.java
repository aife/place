package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TypeModaliteReponseEnum {

    CHIFFREMENT_OFFRE,
    ENVELOPPE_ANONYMAT,
    ENVELOPPE_CANDIDATURE,
    ENVELOPPE_OFFRE,
    ENVELOPPE_OFFRE_TECHNIQUE,
    MODE_OUVERTURE_REPONSE,
    REPONSE_ELECTRONIQUE,
    SIGNATURE_ELECTRONIQUE
}
