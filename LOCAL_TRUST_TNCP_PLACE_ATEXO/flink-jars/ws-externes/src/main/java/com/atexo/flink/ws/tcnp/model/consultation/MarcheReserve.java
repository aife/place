package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MarcheReserve {

    private MarcheReserveEnum marcheReserveEnum;
}
