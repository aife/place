package com.atexo.flink.ws.mpe.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Clause {

    private String label;
    private String slug;
    private List<Clause> clausesN2;
}
