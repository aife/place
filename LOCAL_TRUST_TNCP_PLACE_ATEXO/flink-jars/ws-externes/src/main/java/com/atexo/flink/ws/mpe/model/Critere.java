package com.atexo.flink.ws.mpe.model;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Critere {

    private Map<String, String> criteres;

}
