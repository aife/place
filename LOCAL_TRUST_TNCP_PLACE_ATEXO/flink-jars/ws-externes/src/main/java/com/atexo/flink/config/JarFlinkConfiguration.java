package com.atexo.flink.config;

import com.beust.jcommander.Parameter;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Arrays;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@ToString
@Slf4j
public class JarFlinkConfiguration implements Serializable {

    @Parameter(names = {"--mpe-url", "-mpe-url"}, description = "Url de la MPE", required = true)
    private String mpeUrl;

    @Parameter(names = {"--mpe-login", "-mpe-login"}, description = "Login de la MPE", required = true)
    private String mpeLogin;

    @Parameter(names = {"--mpe-password", "-mpe-password"}, description = "Mot de passe de la MPE", required = true)
    private String mpePassword;

    @Parameter(names = {"--mpe-uuid", "-mpe-uuid"}, description = "UUID de la MPE", required = true)
    private String mpeUuid;


    @Parameter(names = {"--piste-url", "-piste-url"}, description = "Url de la piste", required = true)
    private String pisteUrl;

    @Parameter(names = {"--piste-client-id", "-piste-client-id"}, description = "Identifiant du client de la piste", required = true)
    private String pisteClientId;

    @Parameter(names = {"--piste-secret-id", "-piste-secret-id"}, description = "Identifiant secret du client de la piste", required = true)
    private String pisteSecretId;

    @Parameter(names = {"--piste-auth-url", "-piste-auth-url"}, description = "Url de l'authentification de la piste", required = true)
    private String pisteAuthUrl;

    @Parameter(names = {"--kafka-broker", "-kafka-broker"}, description = "Adresse du broker Kafka", required = true)
    private String kafkaBroker;

    @Parameter(names = {"--kafka-group-id-prefix", "-kafka-group-id-prefix"}, description = "Préfixe de l'identifiant du groupe Kafka", required = true)
    private String kafkaGroupIdPrefix;

    @Parameter(names = {"--kafka-client-id", "-kafka-client-id"}, description = "Identifiant du client Kafka", required = true)
    private String kafkaClientId;

    @Parameter(names = {"--kafka-notifications-topic", "-kafka-notifications-topic"}, description = "Liste des topic de notifications")
    private String kafkaNotificationsTopic;

    @Parameter(names = {"--kafka-waiting-topic", "-kafka-waiting-topic"}, description = "Liste des topic d'attente", required = true)
    private String kafkaWaitingTopic;

    @Parameter(names = {"--kafka-success-topic", "-kafka-success-topic"}, description = "Liste des topic de succès")
    private String kafkaSuccessTopic;

    @Parameter(names = {"--kafka-error-topic", "-kafka-error-topic"}, description = "Liste des topic d'erreur")
    private String kafkaErrorTopic;

    @Parameter(names = {"--kafka-abandon-topic", "-kafka-abandon-topic"}, description = "Liste des topic d'abandon")
    private String kafkaAbandonTopic;

    @Parameter(names = {"--kafka-suivi-topic", "-kafka-suivi-topic"}, description = "Liste des topic de suivi", required = true)
    private String kafkaSuiviTopic;


    @Parameter(names = {"--kafka-security-protocol", "-kafka-security-protocol"}, description = "Protocole de sécurité Kafka")
    private String kafkaSecurityProtocol;

    @Parameter(names = {"--kafka-sasl-mechanism", "-kafka-sasl-mechanism"}, description = "Mécanisme SASL Kafka")
    private String kafkaSaslMechanism;

    @Parameter(names = {"--kafka-sasl-jaas-config", "-kafka-sasl-jaas-config"}, description = "Configuration JAAS SASL Kafka")
    private String kafkaSaslJaasConfig;

    @Parameter(names = {"--kafka-cert-truststore-folder", "-kafka-cert-truststore-folder"}, description = "Dossier de confiance des certificats Kafka")
    private String kafkaCertTruststoreFolder;

    @Parameter(names = {"--kafka-cert-keystore-folder", "-kafka-cert-keystore-folder"}, description = "Dossier de stockage des certificats Kafka")
    private String kafkaCertKeystoreFolder;

    @Parameter(names = {"--kafka-cert-keystore-password", "-kafka-cert-keystore-password"}, description = "Mot de passe du stockage des certificats Kafka")
    private String kafkaCertKeystorePassword;

    @Parameter(names = {"--kafka-cert-truststore-password", "-kafka-cert-truststore-password"}, description = "Mot de passe de la clé des certificats Kafka")
    private String kafkaCertTruststorePassword;

    @Parameter(names = {"--mpe-dce-desactive", "-mpe-dce-desactive"}, description = "Désactivation de la DCE", required = true)
    private String dceDesactive;

    @Parameter(names = {"--configuration-checkpoints-folder", "-configuration-checkpoints-folder"}, description = "Dossier de sauvegarde des checkpoints", required = true)
    private String checkpointsFolder;

    @Parameter(names = {"--configuration-storage-folder", "-configuration-storage-folder"}, description = "Dossier de stockage des données", required = true)
    private String storageFolder;

    @Parameter(names = {"--configuration-plateforme", "-configuration-plateforme"}, description = "Plateforme de traitement", required = true)
    private String plateforme;


    @Parameter(names = {"--configuration-organisme", "-configuration-organisme"}, description = "Organisme de traitement")
    private String organisme = "i5o";

    public static String[] log(String[] args) {
        if (args == null || args.length == 0) {
            return new String[0];
        }
        String argsString = String.join(",", Arrays.asList(args));
        log.info("Starting avec les args : [{}]", argsString);

        // Filtrer les arguments
        return args;
    }

    public boolean isDceDesactive() {
        return "true".equals(dceDesactive);
    }

}
