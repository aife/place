package com.atexo.flink.ws.tcnp.model.consultation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReponseUpload {
    @JsonProperty("Status")
    private String status;
    @JsonProperty("data_file_id")
    private String dataFileId;
    @JsonProperty("data_file_version_id")
    private String dataFileVersionId;
    @JsonProperty("client_file_version_id")
    private String clientFileVersionId;
    @JsonProperty("date_added_utc")
    private String dateAddedUtc;
}
