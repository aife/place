package com.atexo.flink.ws.tcnp.model.contrat;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TNCPContratMessage {
    private String referenceUnique;
    private String statutDE;
    private List<TNCPMessage> messageList;
}
