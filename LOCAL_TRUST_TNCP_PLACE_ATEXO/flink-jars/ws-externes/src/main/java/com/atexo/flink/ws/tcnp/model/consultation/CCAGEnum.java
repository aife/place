package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CCAGEnum {

    AUCUN, CCAG_FCS, CCAG_MI, CCAG_MOE, CCAG_PI, CCAG_TIC, CCAG_TX;
}
