package com.atexo.flink.ws.tcnp.model.contrat;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TNCPMessage {
    private String message;
    private String type;
    private String code;
}
