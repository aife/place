package com.atexo.flink.ws.mpe.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SignatureElectroniqueEnum {

    NON_REQUISE, REQUISE, AUTORISEE;
}
