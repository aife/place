package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LotTNCP {

    private Long id;
    private DecisionLotEnum decisionLot;
    private String description;//obligatoire POST
    private String intitule;//obligatoire POST
    private Double montantLot;//0 apres POST lot
    private Integer numeroLot;//obligatoire POST
    private StatutConsultEnum statutLot;//ELABORE apres POST lot
    private SpecifiqueLot specifiqueLot;

}
