package com.atexo.flink.ws.tcnp.model.consultation;

public enum ValConsiderationEnum {

    CONDITION_EXECUTION,
    CRITERE_ATTRIBUTION,
    INSERTION,
    SPECIFICATION_TECHNIQUE;

}
