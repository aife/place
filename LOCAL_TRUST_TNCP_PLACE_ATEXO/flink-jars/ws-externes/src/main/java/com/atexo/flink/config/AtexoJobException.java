package com.atexo.flink.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Getter
@Setter
@Slf4j
public class AtexoJobException extends RuntimeException {
    protected final String message;
    protected String body;
    protected final String type;

    public AtexoJobException(String errorMessage, JobExeptionEnum type, Throwable err) {
        super(errorMessage, err);
        this.message = StringUtils.isEmpty(errorMessage) ? err.getMessage() : errorMessage;
        this.type = type.name();
        this.body = null;
    }

    public AtexoJobException(String errorMessage, JobExeptionEnum type) {
        super(errorMessage);
        this.message = errorMessage;
        this.type = type.name();
        this.body = null;
    }

    public AtexoJobException(Object body, String errorMessage, JobExeptionEnum type, Throwable err) {
        super(errorMessage, err);
        this.message = StringUtils.isEmpty(errorMessage) ? err.getMessage() : errorMessage;
        this.type = type.name();
        try {
            this.body = AtexoUtils.getMapper().writeValueAsString(body);
        } catch (JsonProcessingException e) {
            log.warn("Erreur lors de la serialisation : {}", e.getMessage());
            this.body = null;
        }
    }

    public AtexoJobException(Object body, String errorMessage, JobExeptionEnum type) {
        super(errorMessage);
        this.message = errorMessage;
        this.type = type.name();
        try {
            this.body = AtexoUtils.getMapper().writeValueAsString(body);
        } catch (JsonProcessingException e) {
            log.warn("Erreur lors de la serialisation : {}", e.getMessage());
            this.body = null;
        }
    }
}
