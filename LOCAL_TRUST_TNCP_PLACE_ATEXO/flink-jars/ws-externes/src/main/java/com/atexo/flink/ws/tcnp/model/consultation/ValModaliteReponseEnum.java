package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ValModaliteReponseEnum {
    AUTORISE, NON_AUTORISE, REQUIS, SANS_OBJET;
}
