package com.atexo.flink.ws.mpe.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClauseN2 {

    private String label;
    private String slug;
    private List<Clause> clausesN3;
}
