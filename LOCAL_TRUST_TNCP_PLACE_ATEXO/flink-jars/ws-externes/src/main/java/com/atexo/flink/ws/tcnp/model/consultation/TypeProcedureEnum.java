package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TypeProcedureEnum {

    AO_OUVERT, AO_RESTREINT, DIALOGUE_COMPETITIF, MARCHE_PUBLIC_SANS_PUB, PROC_ADAPTEE_OUVERTE,
    PROC_ADAPTEE_RESTREINTE, PRO_NEGOCIATION;
}
