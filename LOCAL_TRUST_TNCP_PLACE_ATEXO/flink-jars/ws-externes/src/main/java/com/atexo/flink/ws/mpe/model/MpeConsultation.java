package com.atexo.flink.ws.mpe.model;


import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeConsultation {
    private Long id;//idConPlateforme...
    private String codeExterne;//referenceUnique
    private String organisme;
    private Boolean organismeDecentralise;
    private Boolean entiteAdjudicatrice;
    private String directionService;
    private String naturePrestation;
    private String typeProcedure;
    private String typeContrat;
    private String reference;
    private String intitule;
    private String objet;
    private String commentaireInterne;
    private ZonedDateTime datefin;//pas dans tncp
    private ZonedDateTime datefinAffichage;
    private ZonedDateTime dateLimiteRemiseOffres;
    private ZonedDateTime dateMiseEnLigneCalcule;
    private String idCreateur;
    private String statutCalcule;
    private boolean dume;
    private List<String> lieuxExecution;
    private String codeCpvPrincipal;
    private String codeCpvSecondaire1;
    private String codeCpvSecondaire2;
    private String codeCpvSecondaire3;
    private String urlConsultation;
    private boolean bourseCotraitance;
    private String signatureElectronique;
    private String reponseElectronique;
    private boolean enveloppeCandidature;
    private boolean enveloppeAnonymat;
    private boolean enveloppeOffre;
    private boolean enveloppeOffreTechnique;
    private boolean chiffrement;
    private boolean alloti;
    private Integer nombreLots;
    private Integer poursuivreAffichage;
    private String poursuivreAffichageUnite;
    private List<ClauseN1> clauses;
    private ZonedDateTime dateLimiteRemisePlis;

}
