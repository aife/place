package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TypeModaliteParticipationEnum {

    BOURSE_COTRAITANCE,
    CRITERES_OBJECTIFS,
    DEVISE,
    DROIT_APPLICABLE_UE,
    GROUPEMENT_OE,
    JUSTIFICATIFS_A_PRODUIRE,
    LANGUE,
    TRIBUNAL_RECOURS,
    UNITE_MESURE_DUREE,
    VARIANTES,
    VISITE_OBLIGATOIRE
}
