package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StructureAcheteur {

    private Long id;
    private String raisonSociale;
    private String designation;
    private String activiteStructurePublique;
    private String adresse1;
    private String codePostal;
    private Boolean entiteAdjudicatrice;
    private String formeJuridique;
    private String siret;
    private String codeService;
    private List<ContactAcheteur> contactAcheteurs;
}
