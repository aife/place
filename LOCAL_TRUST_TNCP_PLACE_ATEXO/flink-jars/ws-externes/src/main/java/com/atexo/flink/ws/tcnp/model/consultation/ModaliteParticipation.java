package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModaliteParticipation {

    private TypeModaliteParticipationEnum typeModaliteParticipation;
    private ValModaliteReponseEnum valModaliteParticipation;
}
