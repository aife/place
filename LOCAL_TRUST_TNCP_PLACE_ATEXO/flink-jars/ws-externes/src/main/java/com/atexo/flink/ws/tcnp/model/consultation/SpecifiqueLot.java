package com.atexo.flink.ws.tcnp.model.consultation;

import com.atexo.flink.ws.mpe.model.NaturePrestationEnum;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SpecifiqueLot {

    private Long id;
    private String dateLimiteRemiseOffre;
    private String dateFinAffichage;//ne doit pas exceder 48h apres date DLRO
    private Boolean accesRestreint;
    private List<LieuExecution> lieuExecutions;
    private String codeCpvPrincipal;
    private NaturePrestationEnum categoriePrin;
    private List<CodeCpvSecondaire> codeCpvSecondaires;
    private List<ModaliteReponse> modaliteReponses;
    private List<ModaliteParticipation> modaliteParticipations;
    private List<Consideration> considerations;
    private List<MarcheReserve> marcheReserves;
}
