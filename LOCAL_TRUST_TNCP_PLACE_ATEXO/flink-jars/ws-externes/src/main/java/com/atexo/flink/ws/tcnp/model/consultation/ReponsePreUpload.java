package com.atexo.flink.ws.tcnp.model.consultation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReponsePreUpload {
    @JsonProperty("VirtualFolderId")
    private String virtualFolderId;
    @JsonProperty("Authorization_for_upload")
    private String authorizationForUpload;
    @JsonProperty("AppKey")
    private String appKey;
    @JsonProperty("Folder_Name")
    private String folderName;
    @JsonProperty("Storage_URL")
    private String storageURL;

}
