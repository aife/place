package com.atexo.flink.ws.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeService {

    private String sigle;
    private String siren;
    private String complement;
    private String siret;
    private String libelle;
    private String adresse;
    private String codePostal;
    private String ville;
    private String idParent;
    private String idExterne;

}
