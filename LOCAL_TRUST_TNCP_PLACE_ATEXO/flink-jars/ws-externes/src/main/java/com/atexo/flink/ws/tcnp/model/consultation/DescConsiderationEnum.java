package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DescConsiderationEnum {

    ACHATS_ETHIQUES, AUTRE_CLAUSE_SOCIALE, CLAUSE_SOCIALE_FORMATION, COMMERCE_EQUITABLE, INSERTION_ACTIVITE_ECONOMIQUE, LUTTE_CONTRE_DISCRIMINATIONS, TRACABILITE_SOCIALE;
}
