package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactAcheteur {

    private Long id;
    private String nom;
    private String prenom;
    private String mail;
    private List<Telecom> telecoms;
    private String libelleService;
}
