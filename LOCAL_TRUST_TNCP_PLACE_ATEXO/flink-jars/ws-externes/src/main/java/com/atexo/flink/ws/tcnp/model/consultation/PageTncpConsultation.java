package com.atexo.flink.ws.tcnp.model.consultation;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PageTncpConsultation {

    private List<TncpConsultation> consultations;
    private int currentPage;
    private long totalItems;
    private int totalPages;

}
