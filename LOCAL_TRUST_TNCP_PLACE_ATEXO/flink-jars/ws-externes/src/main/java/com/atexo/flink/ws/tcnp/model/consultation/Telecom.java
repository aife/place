package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Telecom {

    private Long id;
    private String numero;
    private TypeTelecomEnum type;
}
