package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SearchCriteria {

    private ZonedDateTime dateMiseEnLigneMin;
    private ZonedDateTime dateMiseEnLigneMax;
    private String statutConsult;
    private List<String> plateformesTncpToIgnore;
    private Integer page;

}
