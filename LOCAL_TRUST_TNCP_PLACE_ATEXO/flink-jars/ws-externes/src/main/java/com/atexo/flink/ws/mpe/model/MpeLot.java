package com.atexo.flink.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MpeLot {
    @JsonProperty("@id")
    private String hydraId;
    private String consultation; //ex: "/api/v2/consultations/500004"
    private String numero;
    private String intitule;
    private String descriptionSuccinte;
    private String naturePrestation;  //ex: "/api/v2/referentiels/nature-prestations/2"
    private String codeCpvPrincipal;
    private String codeCpvSecondaire1;
    private String codeCpvSecondaire2;
    private String codeCpvSecondaire3;

}
