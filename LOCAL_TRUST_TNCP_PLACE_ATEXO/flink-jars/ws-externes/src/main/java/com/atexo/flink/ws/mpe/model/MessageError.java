package com.atexo.flink.ws.mpe.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MessageError {
    private String errorCode;
    private String errorMessage;
}
