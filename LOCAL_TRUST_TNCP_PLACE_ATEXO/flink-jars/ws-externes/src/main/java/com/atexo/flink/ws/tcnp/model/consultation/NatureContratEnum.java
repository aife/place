package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum NatureContratEnum {

    CONCESSION_SECURITE, CONCESSION_SERVICE, CONCESSION_TRAVAUX, DELEGATION_SERVICE_PUBLIC, MARCHE, MARCHE_DEFENSE_SECURITE, PARTENARIAT;
}
