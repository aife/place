package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DecisionLotEnum {

    ATTRIBUTION, INFRUCTUOSITE, SANS_SUITE_AUTRE;
}
