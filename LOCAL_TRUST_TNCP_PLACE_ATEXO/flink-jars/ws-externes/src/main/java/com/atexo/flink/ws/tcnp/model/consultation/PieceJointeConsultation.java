package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PieceJointeConsultation {

    private String fileId;
    private String folderId;
    private String fileName;
    private String typeFichier;
    private String nomFonctionnelMetier;
    private String hashSha256;
    private String latestVersionId;  //utile?
    private Long size;  //utile?
}
