package com.atexo.flink.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HydraReferentielResponse {

    @JsonProperty("hydra:member")
    private List<MpeReferentiel> list;
}
