package com.atexo.flink.ws.mpe;


import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.JarFlinkConfiguration;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.ws.mpe.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class MpeWs {

    private final JarFlinkConfiguration configuration;
    private final RestTemplate restTemplate;

    public MpeWs(JarFlinkConfiguration configuration, RestTemplate restTemplate) {
        this.configuration = configuration;
        this.restTemplate = restTemplate;
    }


    private static final String MESSAGE = "Token ne peut pas être null";


    public String getAuthentificationV2(String urlServeurUpload) {
        if (StringUtils.hasText(configuration.getMpeUrl()) && !StringUtils.hasText(urlServeurUpload)) {
            urlServeurUpload = configuration.getMpeUrl();
        }
        String url = urlServeurUpload + "/api/v2/token";
        MpeToken ticket = restTemplate.postForObject(url,
                AuthenitificationRequest.builder().login(configuration.getMpeLogin()).password(configuration.getMpePassword()).build(), MpeToken.class);
        if (ticket == null)
            throw new AtexoJobException(MESSAGE, JobExeptionEnum.TOKEN_MPE_MANQUANT);
        return ticket.getToken();

    }

    public MpeConsultation getConsultation(String id) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/consultations/" + id;
        log.info("avec le token [{}]", token);
        log.info("Recuperation de la consultation [{}]", info);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeConsultation> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeConsultation.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_CONSULTATION_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_CONSULTATION_MPE, e);
        }
    }

    public MpeAgent getAgent(String id) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/agents/" + id;
        log.info("avec le token [{}]", token);
        log.info("Recuperation de l'agent [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeAgent> exchange = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeAgent.class);
            HttpHeaders headersResponse = exchange.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return exchange.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_AGENT_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_AGENT_MPE, e);
        }
    }


    public MpeService getService(String hydraId) {
        String url = configuration.getMpeUrl();
        if (StringUtils.isEmpty(hydraId))
            return null;
        String token = getAuthentificationV2(url);
        String info = url + hydraId;
        log.info("avec le token [{}]", token);
        log.info("Recuperation du service [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.setBearerAuth(token);
            ResponseEntity<MpeService> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeService.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_SERVICE_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_SERVICE_MPE, e);
        }
    }

    public List<MpeLot> getListeLots(String id) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/lots?pagination=false&consultation=" + id;
        log.info("avec le token [{}]", token);
        log.info("Recuperation de la liste de lots de consultation [{}]", info);
        //Creating a HttpClient object
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpget = new HttpGet(info);
            httpget.addHeader("accept", "*/*");
            httpget.addHeader("Authorization", "Bearer " + token);
            //Executing the Get request
            HttpResponse httpresponse = httpclient.execute(httpget);
            //Printing the status line
            int statusCode = httpresponse.getStatusLine().getStatusCode();
            String responseBody = EntityUtils.toString(httpresponse.getEntity(), StandardCharsets.UTF_8);
            if (statusCode == 200) {
                return AtexoUtils.getMapper().readValue(responseBody, HydraLotResponse.class).getList()
                        .stream()
                        .filter(mpeLot -> mpeLot.getConsultation().endsWith("/" + id))
                        .collect(Collectors.toList());
            }
            String errorMessage = AtexoUtils.getMapper().writeValueAsString(MessageError.builder().errorMessage(responseBody)
                    .errorCode(String.valueOf(statusCode))
                    .build());
            throw new AtexoJobException(errorMessage, JobExeptionEnum.RECUPERATION_LOTS_MPE);
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_LOTS_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_LOTS_MPE, e);
        }
    }


    public MpeLotDetails getLot(String hydraId) {
        String url = configuration.getMpeUrl();
        if (StringUtils.isEmpty(hydraId))
            return null;
        String token = getAuthentificationV2(url);
        String info = url + hydraId;
        log.info("avec le token [{}]", token);
        log.info("Recuperation du lot [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeLotDetails> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeLotDetails.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_LOT_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_LOT_MPE, e);
        }
    }

    public MpeOrganisme getOrganisme(String hydraId) {
        String url = configuration.getMpeUrl();
        if (StringUtils.isEmpty(hydraId))
            return null;
        String token = getAuthentificationV2(url);
        String info = url + hydraId;
        log.info("avec le token [{}]", token);
        log.info("Recuperation du organisme [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeOrganisme> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeOrganisme.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_ORGANISME_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_ORGANISME_MPE, e);
        }
    }

    public List<MpeFichier> getFichiers(String id) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/consultations/" + id + "/dce";
        log.info("avec le token [{}]", token);
        //Creating a HttpClient object
        log.info("Recuperation des fichiers de la consultation [{}]", info);
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpget = new HttpGet(info);
            httpget.addHeader("accept", "application/json");
            httpget.addHeader("Authorization", "Bearer " + token);
            //Executing the Get request
            HttpResponse httpresponse = httpclient.execute(httpget);
            //Printing the status line
            int statusCode = httpresponse.getStatusLine().getStatusCode();
            String responseBody = EntityUtils.toString(httpresponse.getEntity(), StandardCharsets.UTF_8);
            if (statusCode == 200) {
                return Arrays.asList(AtexoUtils.getMapper().readValue(responseBody, MpeFichier[].class));
            }
            String errorMessage = AtexoUtils.getMapper().writeValueAsString(MessageError.builder().errorMessage(responseBody)
                    .errorCode(String.valueOf(statusCode))
                    .build());
            throw new AtexoJobException(errorMessage, JobExeptionEnum.RECUPERATION_FICHIERS_MPE);
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_FICHIERS_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_FICHIERS_MPE, e);
        }
    }

    public File downloadFichier(String id) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + id + "/download";
        log.info("avec le token [{}]", token);
        log.info("Recuperation du fichier [{}]", info);
        try {
            File download = File.createTempFile("ressources-", "");
            log.info("Téléchargement dans le fichier {}", download.getAbsolutePath());
            String finalToken = token;
            return restTemplate.execute(info,
                    HttpMethod.GET,
                    clientHttpRequest -> {
                        clientHttpRequest.getHeaders().setBearerAuth(finalToken);
                        clientHttpRequest.getHeaders().setAccept(List.of(MediaType.ALL));
                    },
                    clientHttpResponse -> {
                        HttpStatus statusCode = clientHttpResponse.getStatusCode();
                        InputStream responseBody = clientHttpResponse.getBody();
                        HttpHeaders headersResponse = clientHttpResponse.getHeaders();
                        if (statusCode.is2xxSuccessful() && headersResponse.containsKey("content-disposition")) {
                            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
                            if (!CollectionUtils.isEmpty(first)) {
                                log.info("avec le cookies [{}]", String.join(", ", first));
                            }
                            StreamUtils.copy(responseBody, new FileOutputStream(download));
                            return download;
                        } else {
                            String errorMessage = AtexoUtils.getMapper().writeValueAsString(MessageError.builder().errorMessage(convertStreamToString(responseBody, "UTF-8"))
                                    .errorCode(String.valueOf(statusCode.value()))
                                    .build());
                            throw new AtexoJobException(errorMessage, JobExeptionEnum.DOWNLOAD_FICHIER_MPE);
                        }
                    });


        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.DOWNLOAD_FICHIER_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.DOWNLOAD_FICHIER_MPE, e);
        }
    }

    public MpeReferentiel getReferentiel(String hydraId) {
        String url = configuration.getMpeUrl();
        if (StringUtils.isEmpty(hydraId))
            return null;
        String token = getAuthentificationV2(url);
        String info = url + hydraId;
        log.info("avec le token [{}]", token);
        log.info("Recuperation du lot [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeReferentiel> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), MpeReferentiel.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_REFERENTIEL_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_REFERENTIEL_MPE, e);
        }
    }

    public MpeReferentiel getReferentielsByTypeAndIdExterne(String type, String idExterne) {
        String url = configuration.getMpeUrl();
        if (StringUtils.isEmpty(idExterne)) return null;
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/referentiels/" + type + "/?pagination=false&idExterne=" + idExterne;
        log.info("avec le token [{}]", token);
        log.info("Recuperation de la procédure [{}]", info);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            ResponseEntity<HydraReferentielResponse> response = restTemplate.exchange(info, HttpMethod.GET, new HttpEntity<>(headers), HydraReferentielResponse.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            if (response.getBody() == null) return null;
            List<MpeReferentiel> list = response.getBody().getList();
            return CollectionUtils.isEmpty(list) ? null : list.get(0);
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_REFERENTIEL_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_REFERENTIEL_MPE, e);
        }
    }



    private String convertStreamToString(InputStream is, String encoding) throws IOException {
        StringBuilder sb = new StringBuilder(Math.max(16, is.available()));
        char[] tmp = new char[4096];
        try {
            InputStreamReader reader = new InputStreamReader(is, encoding);
            for (int cnt; (cnt = reader.read(tmp)) > 0; )
                sb.append(tmp, 0, cnt);
        } finally {
            is.close();
        }
        return sb.toString();
    }

    public MpeConsultation saveConsultation(MpeConsultation consultation) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/consultations";
        log.info("avec le token [{}]", token);
        log.info("Insértion de la consultation [{}]", info);
        log.info("avec le json [{}]", AtexoUtils.writeValueAsString(consultation));
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeConsultation> response = restTemplate.exchange(info, HttpMethod.POST,
                    new HttpEntity<>(consultation, headers), MpeConsultation.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.ENVOI_CONSULTATION_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.ENVOI_CONSULTATION_MPE, e);
        }
    }

    public MpeLot saveLot(MpeLot mpeLot) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/lots/";
        log.info("avec le token [{}]", token);
        log.info("Insértion du lot [{}]", info);
        log.info("avec le json [{}]", AtexoUtils.writeValueAsString(mpeLot));
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeLot> response = restTemplate.exchange(info, HttpMethod.POST,
                    new HttpEntity<>(mpeLot, headers), MpeLot.class);
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.ENVOI_LOTS_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.ENVOI_LOTS_MPE, e);
        }
    }

    public List<MpeConsultation> getConsultationByReferenceUnique(String referenceUnique) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/consultations?idExterne=" + referenceUnique;
        log.info("avec le token [{}]", token);
        log.info("Recuperation de la consultation [{}]", info);
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<List<MpeConsultation>> response = restTemplate.exchange(info, HttpMethod.GET,
                    new HttpEntity<>(headers), new ParameterizedTypeReference<>() {
                    }
            );
            HttpHeaders headersResponse = response.getHeaders();
            List<String> first = headersResponse.get(HttpHeaders.SET_COOKIE);
            if (!CollectionUtils.isEmpty(first)) {
                log.info("avec le cookies [{}]", String.join(", ", first));
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.RECUPERATION_CONSULTATION_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.RECUPERATION_CONSULTATION_MPE, e);
        }
    }

    public MpeConsultation updateConsultation(Long id, MpeConsultation consultation) {
        String url = configuration.getMpeUrl();
        String token = getAuthentificationV2(url);
        String info = url + "/api/v2/consultations/" + id;
        log.info("avec le token [{}]", token);
        log.info("Mise à jour de la consultation [{}]", info);
        log.info("avec le json [{}]", AtexoUtils.writeValueAsString(consultation));
        //Creating a HttpClient object
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            headers.add("accept", "application/json");
            ResponseEntity<MpeConsultation> response = restTemplate.exchange(info, HttpMethod.PUT,
                    new HttpEntity<>(consultation, headers), MpeConsultation.class);
            if (!response.getStatusCode().is2xxSuccessful()) {
                throw new AtexoJobException(consultation.getReference(), "L'a mise à jour de la consultation MPE a retourné une erreur", JobExeptionEnum.ENVOI_CONTRAT_TNCP);
            }
            return response.getBody();
        } catch (RestClientResponseException e) {
            throw new AtexoJobException(e.getResponseBodyAsString(), JobExeptionEnum.MODIFICATION_CONSULTATION_MPE, e);
        } catch (Exception | OutOfMemoryError e) {
            throw new AtexoJobException(e.getMessage(), JobExeptionEnum.MODIFICATION_CONSULTATION_MPE, e);
        }
    }
}
