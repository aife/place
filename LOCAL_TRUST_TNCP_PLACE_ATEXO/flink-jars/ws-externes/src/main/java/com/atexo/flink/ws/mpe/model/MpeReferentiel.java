package com.atexo.flink.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeReferentiel {
    private String libelle;
    private String abreviation;
    private String idExterne;
    @JsonProperty("@id")
    private String id;

}

