package com.atexo.flink.ws.mpe.model;

import lombok.*;

import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeFichier {
    private String id;
    private String name;
    private String type;
    private ZonedDateTime dateModification;
    private String hash;
    private Long taille;
}
