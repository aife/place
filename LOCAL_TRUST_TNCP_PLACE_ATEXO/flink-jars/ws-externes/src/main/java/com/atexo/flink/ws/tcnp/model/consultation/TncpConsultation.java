package com.atexo.flink.ws.tcnp.model.consultation;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TncpConsultation {

    private Long id;
    private SpecifiqueLot specifiqueLot;
    private StructureAcheteur structureAcheteur;
    private String reference;
    private String referenceUnique;
    private String idConEurUnique;
    private String idConPlateforme;
    private String idPlateforme;
    private String intitule;
    private String natureContrat;
    private String typeProcedure;
    private StatutConsultEnum statutConsult;
    private String objet;
    private String derniereModification;
    private String dateMiseEnLigne;
    private String dateEmission;
    private String commentaireInterne;
    private String urlConsultation;
    private Boolean reponsePaTiers;
    private Integer nombreLots;
    private List<LotTNCP> lots;
    private List<PieceJointeConsultation> pieceJointeConsultations;

}
