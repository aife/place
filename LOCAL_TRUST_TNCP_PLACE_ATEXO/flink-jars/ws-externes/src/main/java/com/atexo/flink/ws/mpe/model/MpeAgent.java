package com.atexo.flink.ws.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeAgent {

    private String id;
    private String login;
    private String email;
    private String nom;
    private String prenom;
    private String telephone;
    private String fax;
    private String service;


}
