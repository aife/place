package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Consideration {

    private DescConsiderationEnum descConsideration;
    private TypeConsiderationEnum typeConsideration;
    private ValConsiderationEnum valConsideration;

}
