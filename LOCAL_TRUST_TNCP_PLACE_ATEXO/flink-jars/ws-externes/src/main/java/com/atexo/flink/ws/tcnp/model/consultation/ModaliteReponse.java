package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModaliteReponse {

    private TypeModaliteReponseEnum typeModaliteReponse;
    private ValModaliteReponseEnum valModaliteReponse;
}
