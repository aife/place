package com.atexo.flink.ws.tcnp.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PisteToken {
    @JsonProperty("access_token")
    private String token;
    @JsonProperty("token_type")
    private String bearer;

    private String scope;
    @JsonProperty("expires_in")
    private Integer expiresIn;
}
