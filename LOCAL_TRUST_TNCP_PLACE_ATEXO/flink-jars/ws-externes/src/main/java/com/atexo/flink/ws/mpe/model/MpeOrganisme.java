package com.atexo.flink.ws.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeOrganisme {
    private String sigle;//codeService ???
    private String denomination; //structureAcheteur.raisonSociale
    private String description;//activiteStructurePublique????
    private String siret;
    private String siren;
    private String complement;
    private String acronyme;//codeService ???
    private String adresse;
    private String codePostal;
    private String ville;
    private String idExterne;
}
