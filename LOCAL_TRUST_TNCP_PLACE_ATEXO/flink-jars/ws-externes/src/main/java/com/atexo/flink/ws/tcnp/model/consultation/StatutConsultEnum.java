package com.atexo.flink.ws.tcnp.model.consultation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatutConsultEnum {

    DECIDE, ELABORE, NOTIFIE, OUVERT, PUBLIE;
}
