package com.atexo.flink.config;

import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MySSLSocketFactory;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpVersion;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.security.KeyStore;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.ZoneOffset.UTC;


public final class AtexoUtils {
    private AtexoUtils() {

    }

    private static MpeWs mpeWs;
    private static TncpWs tncpWs;
    private static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        OBJECT_MAPPER.registerModule(new JavaTimeModule());
    }

    public static CloseableHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception | OutOfMemoryError e) {
            return new DefaultHttpClient();
        }
    }

    public static ObjectMapper getMapper() {
        return OBJECT_MAPPER;
    }

    public static MpeWs getMpeWs(JarFlinkConfiguration configuration) {
        if (mpeWs == null) {
            mpeWs = new MpeWs(configuration, new RestTemplateConfig().atexoRestTemplate());
        }
        return mpeWs;
    }

    public static void setMpeWs(MpeWs mpeWs) {
        AtexoUtils.mpeWs = mpeWs;
    }

    public static TncpWs getTncpWs(JarFlinkConfiguration configuration) {
        if (tncpWs == null) {
            tncpWs = new TncpWs(configuration, new RestTemplateConfig().atexoRestTemplate());
        }
        return tncpWs;
    }

    public static void setTncpWs(TncpWs tncpWs) {
        AtexoUtils.tncpWs = tncpWs;
    }

    public static String getHashed(Path pathToFile) throws IOException {
        return DigestUtils.sha256Hex(new FileInputStream(pathToFile.toFile()));
    }

    public static String getFormat(ZonedDateTime dateTime, DateTimeFormatter formatter) {
        if (dateTime == null) {
            return null;
        }
        return formatter.format(dateTime.withZoneSameInstant(UTC));
    }

    private static final String PATTERN_FORMAT = "yyyyMMdd'T'hhmmss";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_FORMAT);

    public static String getFileNameUploadTncp(String hash) {
        return hash + "_" + getFormat(ZonedDateTime.now(UTC), formatter);

    }

    public static String writeValueAsString(Object envoi) {
        try {
            return OBJECT_MAPPER.writeValueAsString(envoi);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
