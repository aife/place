package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.mapper.TncpConsultationMapper;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.tcnp.TncpWs;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.springframework.util.CollectionUtils;

import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;

@Slf4j
public class ModificationConsultationMpeJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public ModificationConsultationMpeJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de modification de la consultation MPE id={}, referenceUnique={}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());

            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            var tncpConsultation = tncpWs.getConsultation(token, evenementConsultation.getIdObjetDestination());

            MpeWs mpeWS = AtexoUtils.getMpeWs(configuration);
            var mpeConsultations = mpeWS.getConsultationByReferenceUnique(evenementConsultation.getIdObjetDestination());
            if (CollectionUtils.isEmpty(mpeConsultations)) {
                AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de modification de la consultation referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, new Exception("Consultation MPE non trouvée"));
                return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
            } else if (mpeConsultations.size() > 1) {
                AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de modification de la consultation referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, new Exception("Plusieurs consultations MPE trouvées"));
                return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
            }
            var mpeConsultation = mpeConsultations.get(0);

            var mpeConsultationUpdated = new TncpConsultationMapper().tncpConsultationToConsultationMpe(mpeConsultation, tncpConsultation, mpeWS, configuration.getOrganisme());

            evenementConsultation.setIdObjetSource(String.valueOf(mpeConsultation.getId()));
            var result = mpeWS.updateConsultation(mpeConsultationUpdated.getId(), mpeConsultationUpdated);
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setStatut(StatutEnum.FINI);
            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0, evenementConsultation, e, getTncpSuiviErreur(evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de modification de la consultation referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }


}
