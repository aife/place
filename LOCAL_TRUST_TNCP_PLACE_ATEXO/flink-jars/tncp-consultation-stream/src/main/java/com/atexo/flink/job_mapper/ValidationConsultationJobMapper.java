package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;

@Slf4j
public class ValidationConsultationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public ValidationConsultationJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de validation de la consultation id={}, referenceUnique={}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            TncpConsultation consultationTNCP = tncpWs.getConsultation(token, evenementConsultation.getIdObjetDestination());
            if (consultationTNCP.getDateMiseEnLigne() == null) {
                throw new AtexoJobException("Date de mise en ligne est obligatoire pour la validation", JobExeptionEnum.VALIDATION_CONSULTATION_TNCP);
            }
            if (consultationTNCP.getStructureAcheteur() == null || consultationTNCP.getStructureAcheteur().getSiret() == null) {
                throw new AtexoJobException("Le siret de la structure acheteur est obligatoire pour la validation", JobExeptionEnum.VALIDATION_CONSULTATION_TNCP);
            }
            if (consultationTNCP.getStructureAcheteur() == null || consultationTNCP.getStructureAcheteur().getCodeService() == null) {
                throw new AtexoJobException("Le code service de la structure acheteur est obligatoire pour la validation", JobExeptionEnum.VALIDATION_CONSULTATION_TNCP);
            }
            String date = consultationTNCP.getDateMiseEnLigne();
            tncpWs.validateConsultation(token, consultationTNCP.getReferenceUnique(), consultationTNCP.getStructureAcheteur().getCodeService(), consultationTNCP.getStructureAcheteur().getSiret(), ZonedDateTime.parse(date));

            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setStatut(StatutEnum.FINI);
            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de validation de la consultation id=" + evenementConsultation.getIdObjetSource() + ", referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }


}
