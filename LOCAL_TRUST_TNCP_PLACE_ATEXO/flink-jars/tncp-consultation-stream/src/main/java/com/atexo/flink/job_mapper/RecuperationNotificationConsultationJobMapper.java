package com.atexo.flink.job_mapper;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.mpe.model.Notification;
import com.atexo.flink.domain.mpe.model.NotificationTncp;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.springframework.util.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;
import static com.atexo.flink.domain.commun.EtapeEnum.MODIFICATION_CONSULTATION_MPE;

@Slf4j
public class RecuperationNotificationConsultationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi>> {


    public RecuperationNotificationConsultationJobMapper() {
    }

    @Override
    public Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de notification de la consultation base64 : [{}]", evenementConsultation.getObjetDestination());
            byte[] decodedBytes = Base64.getDecoder().decode(evenementConsultation.getObjetDestination());
            String decodedString = new String(decodedBytes);
            Notification response = AtexoUtils.getMapper().readValue(decodedString, Notification.class);
            List<EvenementAtexo> evenements = new ArrayList<>();
            TncpSuivi tncpSuivi;
            if (response != null && response.getResponse().getNotifications() != null && !response.getResponse().getNotifications().isEmpty()) {

                List<NotificationTncp> notifications = response.getResponse().getNotifications();
                evenements = notifications.stream().filter(notificationConsultation -> notificationConsultation.getBulleMetierInfos() != null && "CONSULTATION".equalsIgnoreCase(notificationConsultation.getBulleMetierInfos().getBulleMetier())).map(notificationConsultation -> {
                    EvenementAtexo eventConsultation = new EvenementAtexo();
                    eventConsultation.setIdObjetDestination(notificationConsultation.getDossierId());
                    eventConsultation.setEtape(MODIFICATION_CONSULTATION_MPE);
                    eventConsultation.setFlux(evenementConsultation.getFlux());
                    eventConsultation.setUuid(evenementConsultation.getUuid());
                    eventConsultation.setType(evenementConsultation.getType());
                    eventConsultation.setIdObjetSource(evenementConsultation.getIdObjetSource());
                    eventConsultation.setUuidPlateforme(evenementConsultation.getUuidPlateforme());
                    eventConsultation.setRetry(evenementConsultation.getRetry());
                    return eventConsultation;
                }).collect(Collectors.toList());
            }
            if (evenements.isEmpty()) {
                evenements.add(evenementConsultation);
                evenementConsultation.setMessage("Aucune consultation trouvée");
                if (response != null && response.getResponse() != null && !CollectionUtils.isEmpty(response.getResponse().getMessages())) {
                    evenementConsultation.setMessageDetails(response.getResponse().getMessages().stream().map(notificationConsultationMessage -> notificationConsultationMessage.getType() + " : " + notificationConsultationMessage.getContenu()).collect(Collectors.joining(", ")));
                }
                tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
                tncpSuivi.setStatut(StatutEnum.FINI);

            } else {
                evenementConsultation.setEtape(EtapeEnum.MODIFICATION_CONSULTATION_MPE);
                tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
                tncpSuivi.setDateEnvoi(ZonedDateTime.now());
                tncpSuivi.setStatut(StatutEnum.EN_COURS);
            }
            return new Tuple4<>(in.f0, evenements, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0, Arrays.asList(evenementConsultation), e, getTncpSuiviErreur(evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de notification de la consultation base64 : " + evenementConsultation.getObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, Arrays.asList(evenementConsultation), atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }


}
