package com.atexo.flink;


import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.*;
import com.atexo.flink.domain.mpe.TncpConsultationsEnAttenteSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsErreurAbandonSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsSuiviSerializationSchema;
import com.atexo.flink.domain.mpe.process.DelayEvenementAtexoFromRetryProcess;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.job_mapper.*;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.web.client.RestTemplate;

import static com.atexo.flink.config.FlinkJobUtils.*;


@Slf4j
public class MpeConsultationStream extends TncpStream<EvenementAtexo> {


    /**
     * Creates a job using the source and sink provided.
     *
     * @param waitingSource
     * @param sucessSink
     * @param errorSink
     * @param abandonSink
     * @param tncpSuiviSink
     * @param waintingSink
     * @param mpeWs
     * @param tncpWs
     */
    public MpeConsultationStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> sucessSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waintingSink, MpeWs mpeWs, TncpWs tncpWs, JarFlinkConfiguration configuration, KafkaSource<String> tncpNotificationsSuivi) {
        super(waitingSource, sucessSink, errorSink, abandonSink, tncpSuiviSink, waintingSink, mpeWs, tncpWs, configuration, tncpNotificationsSuivi);
    }

    /**
     * Main method.
     */
    public static void main(String[] args) {
        try {
            String jobName = "consultation";
            var main = new JarFlinkConfiguration();
            JCommander.newBuilder().addObject(main).build().parse(JarFlinkConfiguration.log(args));
            MpeConsultationStream job = getTncpJob(main, jobName);
            job.execute(jobName);
        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", args, e.getMessage(), e);
        }
    }


    public JobExecutionResult execute(String... args) throws Exception {
        String jobName = args[0];
        StreamExecutionEnvironment env = getStreamExecutionEnvironment();

        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> evenementStream = getEvenementEnAttente(env, jobName);
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> notificationConsultationValidTokenStream = getEvenementNotificationConsultation(env, jobName);

        log.info("Conversion des evenements");
        KeyedStream<String, Object> noParsedStream = evenementStream
                .filter(value -> value.f2 != null)
                .name("Filtre sur les evenements consultations avec des donnees corrompues")
                .map(k -> k.f0)
                .name("Correspondance avec la source d'origine")
                .keyBy(value -> value);
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, Object> validEvenement =
                evenementStream
                        .filter(value -> value.f1 != null &&
                                (TypeEnum.PUBLICATION_CONSULTATION.equals(value.f1.getType())
                                        || TypeEnum.MODIFICATION_DATE_LIMITE_REMISE_PLI.equals(value.f1.getType())
                                        || EtapeEnum.AJOUT_CONSULTATION.equals(value.f1.getEtape())
                                        || EtapeEnum.AJOUT_CONSULTATION_MPE.equals(value.f1.getEtape())
                                        || EtapeEnum.ABONNEMENT_CONSULTATION.equals(value.f1.getEtape())
                                        || EtapeEnum.MODIFICATION_CONSULTATION_MPE.equals(value.f1.getEtape())
                                        || EtapeEnum.VALIDATION_CONSULTATION.equals(value.f1.getEtape())
                                        || EtapeEnum.RECUPERATION_CONSULTATIONS_TNCP.equals(value.f1.getEtape())))
                        .name("Filtre sur les evenements de consultation valides")
                        .keyBy(value -> value.f0);


        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, Object> validStream = validEvenement
                .keyBy(value -> value.f0).process(new DelayEvenementAtexoFromRetryProcess())
                .name("Activer l'horloge pour les messages une future tentative")
                .keyBy(k -> k.f0);


        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> publicationConsultationStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.PUBLICATION_CONSULTATION.equals(value.f1.getType()) &&
                        EtapeEnum.AJOUT_CONSULTATION.equals(value.f1.getEtape()))
                .name("Filtre etape d'ajout de la consultation TNCP").map(new PublicationConsultationJobMapper(configuration))
                .name("Ajout la consultation a TNCP")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> modificationConsultationStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.MODIFICATION_DATE_LIMITE_REMISE_PLI.equals(value.f1.getType()) &&
                        value.f1.getEtape() == null
                )
                .name("Filtre etape de Modification de la consultation TNCP").map(new ModificationConsultationJobMapper(configuration))
                .name("Modification la consultation TNCP")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> validationConsultationStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.PUBLICATION_CONSULTATION.equals(value.f1.getType()) &&
                        EtapeEnum.VALIDATION_CONSULTATION.equals(value.f1.getEtape()))
                .name("Filtre etape de validation de la consultation TNCP").map(new ValidationConsultationJobMapper(configuration))
                .name("Validation de la consultation TNCP")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> recuperationConsultationStream = validStream
                .filter(value -> value.f1 != null && InterfaceEnum.TNCP_ENTRANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.INTERGRATION_CONSULTATIONS_TNCP.equals(value.f1.getType()) &&
                        (EtapeEnum.RECUPERATION_CONSULTATIONS_TNCP.equals(value.f1.getEtape()) || value.f1.getEtape() == null))
                .name("Filtre etape de recuperation de la consultation TNCP").map(new RecuperationConsultationJobMapper(configuration)).keyBy(k -> k.f0).flatMap(new TupleFlatMapFunction()).name("Recuperation de la consultation")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> ajoutConsultationMpeStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_ENTRANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.INTERGRATION_CONSULTATIONS_TNCP.equals(value.f1.getType()) &&
                        EtapeEnum.AJOUT_CONSULTATION_MPE.equals(value.f1.getEtape()))
                .name("Filtre etape d'ajout de la consultation TNCP").map(new AjoutConsultationMpeJobMapper(configuration))
                .name("Ajout la consultation a TNCP")
                .keyBy(k -> k.f0);


        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> abonnementConsultationTncpStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_ENTRANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.INTERGRATION_CONSULTATIONS_TNCP.equals(value.f1.getType()) &&
                        EtapeEnum.ABONNEMENT_CONSULTATION.equals(value.f1.getEtape()))
                .name("Filtre etape d'ajout de la consultation TNCP").map(new AbonnementConsultationTncpJobMapper(configuration))
                .name("Ajout la consultation a TNCP")
                .keyBy(k -> k.f0);


        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> notificationConsultationTncpStream = notificationConsultationValidTokenStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_ENTRANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.RECUPERATION_NOTIFICATION.equals(value.f1.getType()) &&
                        (EtapeEnum.RECUPERATION_NOTIFICATION.equals(value.f1.getEtape()) || value.f1.getEtape() == null))
                .name("Filtre etape de recuperation de la notification de la consultation TNCP")
                .map(new RecuperationNotificationConsultationJobMapper())
                .flatMap(new TupleFlatMapFunction()).name("Recuperation de la notification")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> modificationConsultationMpeStream = validStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_ENTRANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.RECUPERATION_NOTIFICATION.equals(value.f1.getType()) &&
                        EtapeEnum.MODIFICATION_CONSULTATION_MPE.equals(value.f1.getEtape()))
                .name("Filtre etape de modification de la consultation MPE").map(new ModificationConsultationMpeJobMapper(configuration))
                .name("Modification de la consultation MPE")
                .keyBy(k -> k.f0);

        DataStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> processTncpStream = publicationConsultationStream.union(validationConsultationStream).union(modificationConsultationStream).union(recuperationConsultationStream).union(ajoutConsultationMpeStream).union(abonnementConsultationTncpStream).union(notificationConsultationTncpStream).union(modificationConsultationMpeStream);


        //Send to error source
        processTncpStream.filter(value -> StatutEnum.ERREUR.equals(value.f3.getStatut()) && value.f1.getRetry() <= 45)
                .name("Filtre sur les evenements avec une tentative <= 2 heures")
                .sinkTo(errorSink).name("Aquittement dans la source en erreur pour une future tentative");

        SingleOutputStreamOperator<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> abandonAfterRetryStream = processTncpStream.filter(value -> StatutEnum.ERREUR.equals(value.f3.getStatut()) && value.f1.getRetry() > 45)
                .name("Filtre sur les evenements avec une tentative > 2 heures");
        //Send to abandon source

        noParsedStream
                .union(abandonAfterRetryStream.map(value -> value.f0))
                .sinkTo(abandonSink).name("Aquittement dans la source d'abandon");
        //Send to success source
        processTncpStream.filter(value -> value.f3.getStatut().equals(StatutEnum.FINI))
                .name("Filtre sur les evenements avec succes")
                .sinkTo(sucessSink).name("Aquittement dans la source en succes");
        processTncpStream.filter(value -> value.f3.getStatut().equals(StatutEnum.EN_COURS))
                .name("Filtre sur les evenements futures")
                .sinkTo(waitingSink).name("Aquittement dans la source pour traiter l'etape suivante");

        processTncpStream.sinkTo(tncpSuiviSink).name("Aquittement dans la source de suivi TNCP");

        // execute the transformation pipeline
        JobConfig jobConfig = getJobConfig();
        return env.execute(jobConfig.getName() + " " + jobConfig.getVersion());
    }


    public static MpeConsultationStream getTncpJob(JarFlinkConfiguration configuration, String jobName) {
        log.info("Configuration du job : {} | config : {}", jobName, configuration);
        String waitingTopic = configuration.getKafkaWaitingTopic();
        String errorTopic = configuration.getKafkaErrorTopic();
        KafkaSource<String> source = getKafkaSource(getProperties(configuration, jobName, "waiting-source"), waitingTopic, errorTopic);


        FlinkService<EvenementAtexo> flinkService = new FlinkService<>();
        TncpConsultationsSuiviSerializationSchema<EvenementAtexo> valueSerializationSchemaSuivi = new TncpConsultationsSuiviSerializationSchema<>(configuration.getPlateforme());
        TncpConsultationsEnAttenteSerializationSchema<EvenementAtexo> valueSerializationSchemaAttente = new TncpConsultationsEnAttenteSerializationSchema<>();
        SuccessSerializationSchema<EvenementAtexo> valueSerializationSchemaSuccess = new SuccessSerializationSchema<>();
        TncpConsultationsErreurAbandonSerializationSchema<EvenementAtexo> valueSerializationSchemaErreur = new TncpConsultationsErreurAbandonSerializationSchema<>();

        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waitingSink = flinkService.getWaitingSinkEvenement(waitingTopic, getProperties(configuration, jobName, "waiting"), valueSerializationSchemaAttente);
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSucces = flinkService.getSuccessSinkEvenement(configuration.getKafkaSuccessTopic(), getProperties(configuration, jobName, "succes"), valueSerializationSchemaSuccess);
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicErreur = flinkService.getErreurAbandonSinkEvenement(errorTopic, getProperties(configuration, jobName, "erreur"), valueSerializationSchemaErreur);
        KafkaSink<String> topicAbandon = flinkService.getSink(configuration.getKafkaAbandonTopic(), getProperties(configuration, jobName, "abandon"));
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSuivi = flinkService.getSinkTncpSuivi(configuration.getKafkaSuiviTopic(), getProperties(configuration, jobName, "suivi"), valueSerializationSchemaSuivi);

        RestTemplate restTemplate = new RestTemplateConfig().atexoRestTemplate();
        MpeWs mpeWs = new MpeWs(configuration, restTemplate);
        TncpWs tncpWs = new TncpWs(configuration, restTemplate);

        KafkaSource<String> sourceNotificationSuivi = null;
        if (configuration.getKafkaNotificationsTopic() != null) {
            sourceNotificationSuivi = getKafkaSource(getProperties(configuration, jobName, "notification-source"), configuration.getKafkaNotificationsTopic());
        }
        return new MpeConsultationStream(source, topicSucces, topicErreur, topicAbandon, topicSuivi, waitingSink, mpeWs, tncpWs, configuration, sourceNotificationSuivi);

    }


}
