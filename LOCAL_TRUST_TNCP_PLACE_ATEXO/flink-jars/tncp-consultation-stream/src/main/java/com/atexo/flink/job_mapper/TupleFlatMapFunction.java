package com.atexo.flink.job_mapper;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TupleFlatMapFunction implements FlatMapFunction<Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    @Override
    public void flatMap(Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi> input, Collector<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> out) {
        // Iterate over the list of EvenementAtexo
        if (input.f1 == null) {
            return;
        }
        List<EvenementAtexo> evenements = input.f1.stream().filter(Objects::nonNull).collect(Collectors.toList());
        for (EvenementAtexo evenement : evenements) {
            // Emit a new tuple for each EvenementAtexo in the list
            out.collect(new Tuple4<>(input.f0, evenement, input.f2, input.f3));
        }
    }
}
