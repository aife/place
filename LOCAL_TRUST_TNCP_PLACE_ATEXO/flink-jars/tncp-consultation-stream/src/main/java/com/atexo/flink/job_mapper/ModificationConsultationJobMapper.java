package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.TncpWs;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;


@Slf4j
public class ModificationConsultationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public ModificationConsultationJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de modification de la consultation id={}, referenceUnique={}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            MpeWs mpeWs = AtexoUtils.getMpeWs(configuration);
            String token = tncpWs.getToken().getToken();
            MpeConsultation consultationMPE = mpeWs.getConsultation(evenementConsultation.getIdObjetSource());
            ZonedDateTime dateDLRO = null;
            ZonedDateTime dateFinAffichage = null;
            ZonedDateTime dateLimiteRemiseOffres = consultationMPE.getDateLimiteRemiseOffres();
            if (dateLimiteRemiseOffres != null)
                dateDLRO = dateLimiteRemiseOffres;
            Integer poursuivreAffichage = consultationMPE.getPoursuivreAffichage();
            if (dateLimiteRemiseOffres != null && poursuivreAffichage != null && poursuivreAffichage > 0) {
                switch (consultationMPE.getPoursuivreAffichageUnite()) {
                    case "DAY":
                        dateFinAffichage = dateLimiteRemiseOffres.plusDays(poursuivreAffichage);
                        break;
                    case "MONTH":
                        dateFinAffichage = dateLimiteRemiseOffres.plusMonths(poursuivreAffichage);
                        break;
                    case "MINUTE":
                        dateFinAffichage = dateLimiteRemiseOffres.plusMinutes(poursuivreAffichage);
                        break;
                    case "HOUR":
                        dateFinAffichage = dateLimiteRemiseOffres.plusHours(poursuivreAffichage);
                        break;
                    case "SECONDE":
                        dateFinAffichage = dateLimiteRemiseOffres.plusSeconds(poursuivreAffichage);
                        break;
                    case "YEAR":
                        dateFinAffichage = dateLimiteRemiseOffres.plusYears(poursuivreAffichage);
                        break;
                    default:
                        dateFinAffichage = dateLimiteRemiseOffres;
                }
            } else {
                dateFinAffichage = consultationMPE.getDatefinAffichage();
            }
            tncpWs.updateConsultation(token, evenementConsultation.getIdObjetDestination(), dateDLRO, dateFinAffichage);

            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setStatut(StatutEnum.FINI);
            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de modification de la consultation id=" + evenementConsultation.getIdObjetSource() + ", referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }


}
