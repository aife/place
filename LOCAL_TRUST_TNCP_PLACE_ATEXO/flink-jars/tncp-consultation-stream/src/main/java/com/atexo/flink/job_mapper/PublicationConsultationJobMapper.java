package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.mpe.mapper.ConsultationMpeToConsultationTncpMapper;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;

@Slf4j
public class PublicationConsultationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {

    private final JarFlinkConfiguration configuration;

    public PublicationConsultationJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de publication de la consultation id={}", evenementConsultation.getIdObjetSource());
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            MpeConsultation consultation = null;
            MpeAgent agent;
            MpeService service = null;
            MpeWs mpeWS = AtexoUtils.getMpeWs(configuration);
            consultation = mpeWS.getConsultation(evenementConsultation.getIdObjetSource());
            if (consultation.getStatutCalcule() == null ||
                    !consultation.getStatutCalcule().contains(StatutConsultationEnum.STATUS_CONSULTATION.getCode())) {
                throw new AtexoJobException("La consultation doit avoir le statut \"Consultation\" : [statut calculé de la consultation est :null] : [statut calculé de la consultation est :" + consultation.getStatutCalcule() + "]", JobExeptionEnum.STATUT_CONSULTATION_MPE_NON_CONFORME);
            }
            agent = mpeWS.getAgent(consultation.getIdCreateur());//url ? token ?
            String refService = agent.getService();
            if (refService != null) {
                service = AtexoUtils.getMpeWs(configuration).getService(refService);
            }
            MpeOrganisme organisme = mpeWS.getOrganisme(consultation.getOrganisme());
            MpeReferentiel typeContrat = mpeWS.getReferentiel(consultation.getTypeContrat());
            MpeReferentiel typeProcedure = mpeWS.getReferentiel(consultation.getTypeProcedure());
            MpeService directionService = mpeWS.getService(consultation.getDirectionService());
            TncpConsultation tncpConsultation = new ConsultationMpeToConsultationTncpMapper().mapToTncp(consultation, agent, service, evenementConsultation.getUuidPlateforme(),
                    typeContrat, typeProcedure, organisme, directionService);
            String referenceConsultation = tncpWs.addConsultation(token, tncpConsultation);
            evenementConsultation.setIdObjetDestination(referenceConsultation);
            if (consultation.isAlloti()) {
                evenementConsultation.setEtape(EtapeEnum.AJOUT_LOT);
            } else if (configuration.isDceDesactive()) {
                log.info("ignore l'étape d'upload DCE");
                evenementConsultation.setEtape(EtapeEnum.VALIDATION_CONSULTATION);
            } else {
                evenementConsultation.setEtape(EtapeEnum.AJOUT_DCE);
            }
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setStatut(StatutEnum.EN_COURS);

            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de publication de la consultation id={}" + evenementConsultation.getIdObjetSource(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }

}
