package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.consultation.PageTncpConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.SearchCriteria;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.io.File;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;
import static com.atexo.flink.domain.commun.EtapeEnum.AJOUT_CONSULTATION_MPE;

@Slf4j
public class RecuperationConsultationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public RecuperationConsultationJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, List<EvenementAtexo>, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        try {
            log.info("Execution job de recuperation des consultations TNCP");
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            File searchCriteriaFile = new File(configuration.getStorageFolder(), "searchCriteria.json");
            SearchCriteria searchCriteria;
            if (!searchCriteriaFile.exists()) {
                searchCriteria = new SearchCriteria();
                searchCriteria.setStatutConsult("PUBLIE");
                searchCriteria.setPage(1);
            } else {
                searchCriteria = AtexoUtils.getMapper().readValue(searchCriteriaFile, SearchCriteria.class);
            }
            if (searchCriteria.getPage() == 1 && searchCriteria.getDateMiseEnLigneMax() == null) {
                searchCriteria.setDateMiseEnLigneMax(ZonedDateTime.now());
            }
            List<EvenementAtexo> evenements = new ArrayList<>();
            PageTncpConsultation pages = tncpWs.getConsultations(searchCriteria.getDateMiseEnLigneMin(), searchCriteria.getDateMiseEnLigneMax(), searchCriteria.getStatutConsult(), token, searchCriteria.getPage());
                List<TncpConsultation> tncpConsultations = pages.getConsultations().stream()
                        .filter(tncpConsultation -> !tncpConsultation.getIdConEurUnique().startsWith(configuration.getMpeUuid())
                                && (searchCriteria.getPlateformesTncpToIgnore() == null || !searchCriteria.getPlateformesTncpToIgnore().contains(tncpConsultation.getIdPlateforme()))).collect(Collectors.toList());
                for (TncpConsultation tncpConsultation : tncpConsultations) {
                    EvenementAtexo eventConsultation = new EvenementAtexo();
                    eventConsultation.setIdObjetDestination(tncpConsultation.getReferenceUnique());
                    String objetDestination = Base64.getEncoder().encodeToString(AtexoUtils.getMapper().writeValueAsString(tncpConsultation).getBytes());
                    eventConsultation.setObjetDestination(objetDestination);
                    eventConsultation.setEtape(AJOUT_CONSULTATION_MPE);
                    eventConsultation.setFlux(evenementConsultation.getFlux());
                    eventConsultation.setUuid(evenementConsultation.getUuid());
                    eventConsultation.setType(evenementConsultation.getType());
                    eventConsultation.setIdObjetSource(evenementConsultation.getIdObjetSource());
                    eventConsultation.setUuidPlateforme(evenementConsultation.getUuidPlateforme());
                    evenements.add(eventConsultation);
                }


            if (pages.getCurrentPage() < pages.getTotalPages()) {
                searchCriteria.setPage(pages.getCurrentPage() + 1);
            } else {
                searchCriteria.setDateMiseEnLigneMin(searchCriteria.getDateMiseEnLigneMax());
                searchCriteria.setDateMiseEnLigneMax(null);
                searchCriteria.setPage(1);
            }
            if (!searchCriteriaFile.getParentFile().exists())
                Files.createDirectories(searchCriteriaFile.getParentFile().toPath());
            if (searchCriteriaFile.exists()) {
                searchCriteriaFile.delete();
            }

            searchCriteriaFile.createNewFile();
            AtexoUtils.getMapper().writeValue(searchCriteriaFile, searchCriteria);
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            if (evenements.isEmpty()) {
                evenements.add(evenementConsultation);
                evenementConsultation.setMessage("Aucune consultation trouvée");
                tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
                tncpSuivi.setStatut(StatutEnum.FINI);

            } else {
                tncpSuivi.setStatut(StatutEnum.EN_COURS);
            }

            return new Tuple4<>(in.f0, evenements, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            TncpSuivi tncpSuiviErreur = getTncpSuiviErreur(evenementConsultation, e, log);
            tncpSuiviErreur.setRetry(120);
            evenementConsultation.setRetry(120);
            return new Tuple4<>(in.f0, Arrays.asList(evenementConsultation), e, tncpSuiviErreur);
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de recuperation des consultations TNCP", JobExeptionEnum.ERREUR_TECHNIQUE, e);
            TncpSuivi tncpSuiviErreur = getTncpSuiviErreur(evenementConsultation, atexoJobException, log);
            tncpSuiviErreur.setRetry(120);
            evenementConsultation.setRetry(120);
            return new Tuple4<>(in.f0, Arrays.asList(evenementConsultation), atexoJobException, tncpSuiviErreur);
        }
    }


}
