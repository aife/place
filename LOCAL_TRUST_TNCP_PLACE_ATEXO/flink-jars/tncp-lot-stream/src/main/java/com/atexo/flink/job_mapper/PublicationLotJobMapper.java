package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.mpe.mapper.LotMpeToLotTncpMapper;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeLot;
import com.atexo.flink.ws.mpe.model.MpeLotDetails;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.consultation.LotTNCP;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;


@Slf4j
public class PublicationLotJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public PublicationLotJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        TncpConsultation tncpConsultation;
        try {
            log.info("Execution job de publication de lot pour la consultation id={} et referenceUnique = {}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            tncpConsultation = tncpWs.getConsultation(token, evenementConsultation.getIdObjetDestination());
            MpeWs mpeWs = AtexoUtils.getMpeWs(configuration);

            List<MpeLotDetails> lots;
            log.info("Recuperation de la liste de lots de la consultation id={}", evenementConsultation.getIdObjetSource());
            List<MpeLot> lotIds = mpeWs.getListeLots(evenementConsultation.getIdObjetSource());
            if (CollectionUtils.isEmpty(lotIds)) {
                AtexoJobException atexoJobException = new AtexoJobException("La consultation n'a aucun lot defini. Veuillez verifier la consultation dans " + evenementConsultation.getUuidPlateforme(), JobExeptionEnum.CONSULTATION_ALLOTIE_SANS_LOT_DEFINI);
                return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
            }
            lots = lotIds
                    .stream()
                    .map(lot -> {
                        log.info("Recuperation du lot id={}", lot.getHydraId());
                        return mpeWs.getLot(lot.getHydraId());
                    })
                    .collect(Collectors.toList());
            List<LotTNCP> lotsTncp;
            lotsTncp = lots.stream().map(lot -> new LotMpeToLotTncpMapper().mapLotDetailToLotTncp(lot)).collect(Collectors.toList());
            log.info("Ajout des lots a la consultation id={}", evenementConsultation.getIdObjetSource());
            tncpWs.addLots(token, tncpConsultation.getReferenceUnique(),
                    tncpConsultation.getStructureAcheteur().getCodeService(), tncpConsultation.getStructureAcheteur().getSiret(), lotsTncp);

            if (configuration.isDceDesactive()) {
                log.info("ignore l'étape d'upload DCE");
                evenementConsultation.setEtape(EtapeEnum.VALIDATION_CONSULTATION);
            } else {
                evenementConsultation.setEtape(EtapeEnum.AJOUT_DCE);
            }
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());

            tncpSuivi.setStatut(StatutEnum.EN_COURS);

            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de publication de lot pour la consultation id=" + evenementConsultation.getIdObjetSource(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }
}
