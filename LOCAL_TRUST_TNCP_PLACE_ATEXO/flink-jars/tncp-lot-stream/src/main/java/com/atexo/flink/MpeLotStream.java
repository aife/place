package com.atexo.flink;


import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.FlinkService;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.mpe.TncpConsultationsEnAttenteSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsErreurAbandonSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsSuiviSerializationSchema;
import com.atexo.flink.domain.mpe.process.DelayEvenementAtexoFromRetryProcess;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.job_mapper.AjoutLotMpeJobMapper;
import com.atexo.flink.job_mapper.PublicationLotJobMapper;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.web.client.RestTemplate;

import static com.atexo.flink.config.FlinkJobUtils.*;

@Slf4j
public class MpeLotStream extends TncpStream<EvenementAtexo> {


    /**
     * Creates a job using the source and sink provided.
     *
     * @param waitingSource
     * @param errorSink
     * @param abandonSink
     * @param tncpSuiviSink
     * @param waintingSink
     * @param mpeWs
     * @param tncpWs
     */
    public MpeLotStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waintingSink, MpeWs mpeWs, TncpWs tncpWs, JarFlinkConfiguration configuration) {
        super(waitingSource, null, errorSink, abandonSink, tncpSuiviSink, waintingSink, mpeWs, tncpWs, configuration);
    }


    /**
     * Main method.
     */
    public static void main(String[] args) {
        try {
            String jobName = "lot";
            var main = new JarFlinkConfiguration();
            JCommander.newBuilder().addObject(main).build().parse(JarFlinkConfiguration.log(args));

            MpeLotStream job = getTncpJob(main, jobName);
            job.execute(jobName);
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }


    public JobExecutionResult execute(String... args) throws Exception {
        String jobName = args[0];
        StreamExecutionEnvironment env = getStreamExecutionEnvironment();
        // start the data generator and arrange for watermarking
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> evenementStream = getEvenementEnAttente(env, jobName);

        log.info("Conversion des evenements");
        KeyedStream<String, Object> noParsedStream = evenementStream
                .filter(value -> value.f2 != null)
                .name("Filtre sur les evenements consultations avec des donnees corrompues")
                .map(k -> k.f0)
                .name("Correspondance avec la source d'origine")
                .keyBy(value -> value);
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, Object> validEvenement =
                evenementStream
                        .filter(value -> value.f1 != null &&
                                (EtapeEnum.AJOUT_LOT.equals(value.f1.getEtape()) || EtapeEnum.AJOUT_LOT_MPE.equals(value.f1.getEtape()))
                        ).name("Filtre sur les evenements de lots valides")
                        .keyBy(value -> value.f0);


        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> validTokenStream = validEvenement
                .keyBy(value -> value.f0).process(new DelayEvenementAtexoFromRetryProcess())
                .name("Activer l'horloge pour les messages une future tentative")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> publicationLotStream = validTokenStream
                .filter(value -> value.f1 != null &&EtapeEnum.AJOUT_LOT.equals(value.f1.getEtape()))
                .map(new PublicationLotJobMapper(configuration))
                .name("Publication la consultation")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> publicationLotMpeStream = validTokenStream
                .filter(value -> value.f1 != null &&EtapeEnum.AJOUT_LOT_MPE.equals(value.f1.getEtape()))
                .map(new AjoutLotMpeJobMapper(configuration))
                .name("Publication la consultation")
                .keyBy(k -> k.f0);

        DataStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> processTncpStream = publicationLotStream.union(publicationLotMpeStream);


        //Send to error source
        processTncpStream
                .filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() <= 120)
                .name("Filtre sur les evenements avec une tentative <= 2 heures")
                .sinkTo(errorSink).name("Aquittement dans la source en erreur pour un futur retry");

        SingleOutputStreamOperator<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> abandonAfterRetryStream = processTncpStream
                .filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() > 120)
                .name("Filtre sur les evenements avec une tentative > 2 heures");
        //Send to abandon source

        noParsedStream
                .union(abandonAfterRetryStream.map(value -> value.f0))
                .sinkTo(abandonSink).name("Aquittement dans la source d'abandon");

//Send to success source
        processTncpStream.filter(value -> value.f3.getStatut().equals(StatutEnum.EN_COURS))
                .name("Filtre sur les evenements futures")
                .sinkTo(waitingSink).name("Aquittement dans la source pour traiter les lots");

        //Send to tncp
        processTncpStream.sinkTo(tncpSuiviSink).name("Aquittement dans la source de suivi TNCP");

        // execute the transformation pipeline
        JobConfig jobConfig = getJobConfig();
        return env.execute(jobConfig.getName() + " " + jobConfig.getVersion());
    }

    public static MpeLotStream getTncpJob(JarFlinkConfiguration configuration, String jobName) {
        log.info("Configuration du job : {} | config : {}", jobName, configuration);
        String waitingTopic = configuration.getKafkaWaitingTopic();
        String errorTopic = configuration.getKafkaErrorTopic();


        KafkaSource<String> source = getKafkaSource(getProperties(configuration, jobName, "waiting-source"), waitingTopic, errorTopic);

        FlinkService<EvenementAtexo> flinkService = new FlinkService<>();
        TncpConsultationsSuiviSerializationSchema<EvenementAtexo> valueSerializationSchema = new TncpConsultationsSuiviSerializationSchema<>(configuration.getPlateforme());
        TncpConsultationsEnAttenteSerializationSchema<EvenementAtexo> valueSerializationSchemaAttente = new TncpConsultationsEnAttenteSerializationSchema<>();
        TncpConsultationsErreurAbandonSerializationSchema<EvenementAtexo> valueSerializationSchemaErreur = new TncpConsultationsErreurAbandonSerializationSchema<>();

        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waitingSink = flinkService.getWaitingSinkEvenement(configuration.getKafkaWaitingTopic(), getProperties(configuration, jobName, "waiting"), valueSerializationSchemaAttente);
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicErreur = flinkService.getErreurAbandonSinkEvenement(configuration.getKafkaErrorTopic(), getProperties(configuration, jobName, "erreur"), valueSerializationSchemaErreur);
        KafkaSink<String> topicAbandon = flinkService.getSink(configuration.getKafkaAbandonTopic(), getProperties(configuration, jobName, "abandon"));
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSuivi = flinkService.getSinkTncpSuivi(configuration.getKafkaSuiviTopic(), getProperties(configuration, jobName, "suivi"), valueSerializationSchema);

        RestTemplate restTemplate = new RestTemplateConfig().atexoRestTemplate();
        MpeWs mpeWs = new MpeWs(configuration, restTemplate);
        TncpWs tncpWs = new TncpWs(configuration, restTemplate);
        return new MpeLotStream(source, topicErreur, topicAbandon, topicSuivi, waitingSink, mpeWs, tncpWs, configuration);

    }

}
