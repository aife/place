package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.mapper.TncpConsultationMapper;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.time.ZonedDateTime;
import java.util.Base64;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;

@Slf4j
public class AjoutLotMpeJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public AjoutLotMpeJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        String base64 = evenementConsultation.getObjetDestination();
        if (base64 == null) {
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job d'ajout des lots de la consultation id=" + evenementConsultation.getIdObjetSource() + ", referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, new Exception("Objet destination null"));
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
        byte[] decodedBytes = Base64.getDecoder().decode(evenementConsultation.getObjetDestination());
        String decodedString = new String(decodedBytes);
        TncpConsultation tncpConsultation = AtexoUtils.getMapper().readValue(decodedString, TncpConsultation.class);
        try {
            log.info("Execution job d'ajout des lots de la consultation id={}, referenceUnique={}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());
            MpeWs mpeWS = AtexoUtils.getMpeWs(configuration);
            MpeConsultation mpeConsultation = mpeWS.getConsultation(evenementConsultation.getIdObjetSource());
            tncpConsultation.getLots()
                    .stream()
                    .map(e -> new TncpConsultationMapper().tncoLotToMpeLot(e, evenementConsultation.getIdObjetSource(), mpeConsultation.getNaturePrestation(), mpeConsultation.getCodeCpvPrincipal()))
                    .forEach(e -> mpeWS.saveLot(e));

            evenementConsultation.setEtape(EtapeEnum.ABONNEMENT_CONSULTATION);
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            tncpSuivi.setStatut(StatutEnum.FINI);
            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job d'ajout des lots de la consultation id=" + evenementConsultation.getIdObjetSource() + ", referenceUnique=" + evenementConsultation.getIdObjetDestination(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }


}
