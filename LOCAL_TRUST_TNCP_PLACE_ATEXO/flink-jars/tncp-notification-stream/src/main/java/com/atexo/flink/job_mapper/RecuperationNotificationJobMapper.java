package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.tcnp.TncpWs;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.springframework.util.Base64Utils;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;

import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;


@Slf4j
public class RecuperationNotificationJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public RecuperationNotificationJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementAtexo = in.f1;
        FlinkJobUtils.setMDC(evenementAtexo);
        try {
            log.info("Execution job de recuperation de notification");

            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            String token = tncpWs.getToken().getToken();
            String notificationMessage = tncpWs.lastNotification(token);
            log.info("Notification récupérée ={}", notificationMessage);
            TncpSuivi tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementAtexo);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());
            String objetDestination = Base64Utils.encodeToString(notificationMessage.getBytes(StandardCharsets.UTF_8));
            tncpSuivi.setObjetDestination(objetDestination);

            tncpSuivi.setStatut(StatutEnum.FINI);
            return new Tuple4<>(in.f0, evenementAtexo, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error(e.getMessage());
            return new Tuple4<>(in.f0, evenementAtexo, e, getTncpSuiviErreur(evenementAtexo, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de la récupération des notifications", JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementAtexo, atexoJobException, getTncpSuiviErreur(evenementAtexo, atexoJobException, log));
        }
    }
}
