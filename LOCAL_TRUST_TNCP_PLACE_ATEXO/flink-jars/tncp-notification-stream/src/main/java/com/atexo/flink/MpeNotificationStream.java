package com.atexo.flink;


import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.FlinkService;
import com.atexo.flink.domain.commun.TypeEnum;
import com.atexo.flink.domain.contrat.TncpContratsSuiviSerializationSchema;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.job_mapper.RecuperationNotificationJobMapper;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.web.client.RestTemplate;

import static com.atexo.flink.config.FlinkJobUtils.*;

@Slf4j
public class MpeNotificationStream extends TncpStream<EvenementAtexo> {


    /**
     * Creates a job using the source and sink provided.
     *
     * @param waitingSource
     * @param tncpSuiviSink
     * @param tncpWs
     */
    public MpeNotificationStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> tncpSuiviSink, TncpWs tncpWs, JarFlinkConfiguration configuration) {
        super(waitingSource, null, null, null, tncpSuiviSink, null, null, tncpWs, configuration);
    }


    /**
     * Main method.
     */
    public static void main(String[] args) {
        try {
            String jobName = "notifications";
            var main = new JarFlinkConfiguration();
            JCommander.newBuilder().addObject(main).build().parse(JarFlinkConfiguration.log(args));
            MpeNotificationStream job = getTncpJob(main, jobName);
            job.execute(jobName);
        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", args, e.getMessage(), e);
        }
    }


    public JobExecutionResult execute(String... args) throws Exception {
        String jobName = args[0];
        StreamExecutionEnvironment env = getStreamExecutionEnvironment();
        // start the data generator and arrange for watermarking
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> evenementStream = getEvenementAtexoEnAttente(env, jobName);

        log.info("Conversion des evenements");
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, Object> validEvenement = evenementStream.filter(value -> value.f1 != null && EtapeEnum.RECUPERATION_NOTIFICATION.equals(value.f1.getEtape()) && TypeEnum.RECUPERATION_NOTIFICATION.equals(value.f1.getType())).name("Filtre sur les evenements de notification valides").keyBy(value -> value.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, Object> publicationContratStream = validEvenement.keyBy(k -> k.f0).map(new RecuperationNotificationJobMapper(configuration)).name("Récupération de la notification").keyBy(k -> k.f0).keyBy(k -> k.f0);

        publicationContratStream.sinkTo(tncpSuiviSink).name("Aquittement dans la source notification de suivi TNCP");

        // execute the transformation pipeline
        JobConfig jobConfig = getJobConfig();
        return env.execute(jobConfig.getName() + " " + jobConfig.getVersion());
    }

    public static MpeNotificationStream getTncpJob(JarFlinkConfiguration configuration, String jobName) {
        log.info("Configuration du job : {} | config : {}", jobName, configuration);

        String waitingTopic = configuration.getKafkaWaitingTopic();

        KafkaSource<String> source = getKafkaSource(getProperties(configuration, jobName, "waiting-source"), waitingTopic);

        TncpContratsSuiviSerializationSchema<EvenementAtexo> valueSerializationSchema = new TncpContratsSuiviSerializationSchema<>(configuration.getPlateforme());

        FlinkService<EvenementAtexo> flinkService = new FlinkService<>();

        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSuivi = flinkService.getSinkTncpSuivi(configuration.getKafkaSuiviTopic(), getProperties(configuration, jobName, "suivi"), valueSerializationSchema);

        RestTemplate restTemplate = new RestTemplateConfig().atexoRestTemplate();
        TncpWs tncpWs = new TncpWs(configuration, restTemplate);
        return new MpeNotificationStream(source, topicSuivi, tncpWs, configuration);
    }

}
