package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.ModaliteParticipation;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteParticipationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValModaliteReponseEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ModaliteParticipationMapperTest {

    private ModaliteParticipationMapper mapper = new ModaliteParticipationMapper();

    @Test
    void Given_MpeConsultation_When_mapToModaliteParticipation_Then_ReturnListe() {
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .bourseCotraitance(true)
                .build();
        List<ModaliteParticipation> modaliteParticipations = mapper.mapToModaliteParticipation(consultation);
        assertNotNull(modaliteParticipations);
        assertEquals(1, modaliteParticipations.size());
        assertEquals(TypeModaliteParticipationEnum.BOURSE_COTRAITANCE, modaliteParticipations.get(0).getTypeModaliteParticipation());
        assertEquals(ValModaliteReponseEnum.AUTORISE, modaliteParticipations.get(0).getValModaliteParticipation());


    }


}
