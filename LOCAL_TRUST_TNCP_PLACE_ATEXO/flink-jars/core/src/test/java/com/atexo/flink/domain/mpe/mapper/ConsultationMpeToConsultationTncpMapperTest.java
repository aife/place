package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.NatureContratEnum;
import com.atexo.flink.ws.tcnp.model.consultation.StatutConsultEnum;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.TypeProcedureEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ConsultationMpeToConsultationTncpMapperTest {

    private ConsultationMpeToConsultationTncpMapper mapper = new ConsultationMpeToConsultationTncpMapper();
    private MpeOrganisme organisme;
    private MpeReferentiel procedure;
    private MpeReferentiel contrat;
    private MpeService direction;
    private List<ClauseN1> clauses;

    @BeforeEach
    public void setUp() {
        organisme = MpeOrganisme.builder()
                .sigle("sigleOrg")
                .denomination("denominationOrg")
                .description("descriptionOrg")
                .siret("12345678911111")
                .acronyme("acronymeOrg")
                .adresse("adresseOrg")
                .codePostal("codePostalOrg")
                .ville("villeOrg")
                .idExterne("idExterneOrg")
                .build();
        procedure = MpeReferentiel.builder()
                .libelle("Appel d'offre ouvert")
                .abreviation("AOO")
                .idExterne("AOO")
                .build();
        contrat = MpeReferentiel.builder()
                .libelle("Marche")
                .abreviation("MAR")
                .idExterne("mar")
                .build();
        direction = MpeService.builder()
                .sigle("sigleDS")
                .siren("123456789")
                .libelle("libelleDS")
                .adresse("adresseDS")
                .codePostal("codePostalDS")
                .ville("villeDS")
                .idExterne("idExterneDS")
                .idParent("idParentDS")
                .build();
        clauses = List.of(ClauseN1.builder()
                        .slug("clausesSociales")
                        .clausesN2(List.of(ClauseN2.builder()
                                        .slug("conditionsExecutions")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("specificationTechnique")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("critereAttributionMarche")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("marcheReserve")
                                        .clausesN3(List.of(Clause.builder().slug("clauseSocialeReserveAtelierProtege").build(),
                                                Clause.builder().slug("clauseSocialeSIAE").build(),
                                                Clause.builder().slug("clauseSocialeEESS").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("insertion")
                                        .build())
                        ).build(),
                ClauseN1.builder().slug("clauseEnvironnementale")
                        .clausesN2(List.of(ClauseN2.builder().slug("conditionsExecutions").build(),
                                ClauseN2.builder().slug("criteresSelections").build(),
                                ClauseN2.builder().slug("specificationsTechniques").build())).build());
    }

    @Test
    void Given_MpeConsultationCentraliseAndMpeAgentAndMpeService_When_mapToTncp_Then_ReturnTncpConsultation() {
        List<String> lieux = new ArrayList<>();
        lieux.add("Paris");
        lieux.add("Lyon");
        lieux.add("Marseille");
        MpeAgent agent = MpeAgent.builder()
                .id("idAgent")
                .login("login")
                .email("email")
                .nom("Passet")
                .prenom("Marc")
                .telephone("0650000000")
                .fax("numfax")
                .service("service")
                .build();
        MpeService service = MpeService.builder()
                .sigle("sigleS")
                .siren("123456788")
                .libelle("libelleS")
                .adresse("adresseS")
                .codePostal("codePostalS")
                .ville("villeS")
                .idExterne("idExterneS")
                .idParent("idParentS")
                .build();
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .organisme("organisme")
                .naturePrestation("/api/v2/referentiels/nature-prestations/2")
                .typeProcedure("procedure")
                .typeContrat("contrat")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .commentaireInterne("commentaire")
                .datefinAffichage(ZonedDateTime.parse("2023-04-02T00:00:00+02:00"))
                .dateLimiteRemiseOffres(ZonedDateTime.parse("2023-04-03T00:00:00+02:00"))
                .dateMiseEnLigneCalcule(ZonedDateTime.parse("2023-01-03T00:00:00+02:00"))
                .idCreateur("idCreateur")
                .statutCalcule("brouillon")
                .dume(false)
                .lieuxExecution(lieux)
                .codeCpvPrincipal("12340000")
                .codeCpvSecondaire1("12340001")
                .codeCpvSecondaire2("12340002")
                .codeCpvSecondaire3("12340003")
                .bourseCotraitance(true)
                .signatureElectronique("REQUISE")
                .reponseElectronique("AUTORISEE")
                .enveloppeCandidature(false)
                .enveloppeAnonymat(true)
                .enveloppeOffre(false)
                .enveloppeOffreTechnique(true)
                .chiffrement(true)
                .alloti(false)
                .organismeDecentralise(true)
                .directionService("directionService")
                .nombreLots(0)
                .clauses(clauses)
                .build();
        TncpConsultation tncpConsultation = mapper.mapToTncp(consultation, agent, service, "uuidTest", contrat,
                procedure, organisme, null);
        assertNotNull(tncpConsultation);
        assertEquals("ref", tncpConsultation.getReference());
        assertEquals("uuidTest", tncpConsultation.getIdPlateforme());
        assertEquals("uuidTest-1", tncpConsultation.getIdConEurUnique());
        assertEquals("1", tncpConsultation.getIdConPlateforme());
        assertEquals(NatureContratEnum.MARCHE.name(), tncpConsultation.getNatureContrat());
        assertEquals(TypeProcedureEnum.AO_OUVERT.name(), tncpConsultation.getTypeProcedure());
        assertEquals(StatutConsultEnum.ELABORE, tncpConsultation.getStatutConsult());
        assertEquals("objet", tncpConsultation.getObjet());
        assertEquals("2023-01-02T22:00:00+00:00", tncpConsultation.getDateMiseEnLigne());
        assertEquals("commentaire", tncpConsultation.getCommentaireInterne());
        assertEquals(0, tncpConsultation.getNombreLots());
        assertEquals("2023-04-02T22:00:00+00:00", tncpConsultation.getSpecifiqueLot().getDateLimiteRemiseOffre());
        assertEquals("2023-04-01T22:00:00+00:00", tncpConsultation.getSpecifiqueLot().getDateFinAffichage());
        assertEquals(3, tncpConsultation.getSpecifiqueLot().getLieuExecutions().size());
        assertEquals("12340000", tncpConsultation.getSpecifiqueLot().getCodeCpvPrincipal());
        assertEquals(NaturePrestationEnum.FOURNITURES, tncpConsultation.getSpecifiqueLot().getCategoriePrin());
        assertEquals(3, tncpConsultation.getSpecifiqueLot().getCodeCpvSecondaires().size());
        assertEquals(7, tncpConsultation.getSpecifiqueLot().getModaliteReponses().size());
        assertEquals(1, tncpConsultation.getSpecifiqueLot().getModaliteParticipations().size());
        assertEquals(4, tncpConsultation.getSpecifiqueLot().getMarcheReserves().size());
        assertEquals(22, tncpConsultation.getSpecifiqueLot().getConsiderations().size());
        assertEquals("idExterneOrg", tncpConsultation.getStructureAcheteur().getCodeService());
        assertEquals("adresseOrg villeOrg", tncpConsultation.getStructureAcheteur().getAdresse1());
        assertEquals("codePostalOrg", tncpConsultation.getStructureAcheteur().getCodePostal());
        assertEquals("denominationOrg", tncpConsultation.getStructureAcheteur().getRaisonSociale());
        assertEquals("sigle", tncpConsultation.getStructureAcheteur().getActiviteStructurePublique());
        assertEquals("12345678911111", tncpConsultation.getStructureAcheteur().getSiret());
        assertEquals(1, tncpConsultation.getStructureAcheteur().getContactAcheteurs().size());
        assertEquals(2, tncpConsultation.getStructureAcheteur().getContactAcheteurs().get(0).getTelecoms().size());
        assertEquals("test", tncpConsultation.getIntitule());
    }

}
