package com.atexo.flink.domain.commun.service;


import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobConfig;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class JobUtilsTest {


    @Test
    void Given_Properties_Return_JobConfiguration() throws Exception {
        JobConfig configuration = FlinkJobUtils.getJobConfig();
        assertNotNull(configuration);
    }

    @Test
    void Given_file_Then_ReturnMappingConfiguration() throws Exception {
        Map<String, Map<String, String>> configuration = FlinkJobUtils.getMappingMpeTncp();
        assertThat(configuration).isNotEmpty();
    }

    @Test
    void Given_file_Then_ReturnExceptionTrace() {
        String configuration = FlinkJobUtils.getErreur(new AtexoJobException("test", JobExeptionEnum.ERREUR_TECHNIQUE), null);
        assertThat(configuration).startsWith("com.atexo.flink.config.AtexoJobException: test");
    }

    @Test
    void Given_EvenementWithException_Then_ReturnTncpSuivi() {
        EvenementAtexo evenementAtexo = new EvenementAtexo();
        TncpSuivi configuration = FlinkJobUtils.getTncpSuiviErreur(evenementAtexo, new AtexoJobException("test", JobExeptionEnum.ERREUR_TECHNIQUE), null);
        assertNotNull(configuration);
        assertThat(configuration.getMessage()).isEqualTo("ERREUR_TECHNIQUE : test");
        assertThat(configuration.getMessageDetails()).startsWith("com.atexo.flink.config.AtexoJobException: test");
    }

}
