package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.SpecifiqueLot;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteParticipationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteReponseEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValModaliteReponseEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class SpecifiqueLotMapperTest {

    private SpecifiqueLotMapper mapper = new SpecifiqueLotMapper();

    private MpeOrganisme organisme;
    private MpeReferentiel procedure;
    private MpeReferentiel contrat;
    private MpeService direction;
    private List<ClauseN1> clauses;

    @BeforeEach
    public void setUp() {
        organisme = MpeOrganisme.builder()
                .sigle("sigleOrg")
                .denomination("denominationOrg")
                .description("descriptionOrg")
                .siret("12345678911111")
                .acronyme("acronymeOrg")
                .adresse("adresseOrg")
                .codePostal("codePostalOrg")
                .ville("villeOrg")
                .idExterne("idExterneOrg")
                .build();
        procedure = MpeReferentiel.builder()
                .libelle("Appel d'offre ouvert")
                .abreviation("AOO")
                .idExterne("idProcedure")
                .build();
        contrat = MpeReferentiel.builder()
                .libelle("Marche")
                .abreviation("MAR")
                .idExterne("mar")
                .build();
        direction = MpeService.builder()
                .sigle("sigleDS")
                .siren("123456789")
                .libelle("libelleDS")
                .adresse("adresseDS")
                .codePostal("codePostalDS")
                .ville("villeDS")
                .idExterne("idExterneDS")
                .idParent("idParentDS")
                .build();
        clauses = List.of(ClauseN1.builder()
                        .slug("clausesSociales")
                        .clausesN2(List.of(ClauseN2.builder()
                                        .slug("conditionsExecutions")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("specificationTechnique")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("critereAttributionMarche")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("marcheReserve")
                                        .clausesN3(List.of(Clause.builder().slug("clauseSocialeReserveAtelierProtege").build(),
                                                Clause.builder().slug("clauseSocialeSIAE").build(),
                                                Clause.builder().slug("clauseSocialeEESS").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("insertion")
                                        .build())
                        ).build(),
                ClauseN1.builder().slug("clauseEnvironnementale")
                        .clausesN2(List.of(ClauseN2.builder().slug("conditionsExecutions").build(),
                                ClauseN2.builder().slug("criteresSelections").build(),
                                ClauseN2.builder().slug("specificationsTechniques").build())).build());
    }

    @Test
    void Given_MpeConsultation_When_mapToSpecifiqueLot_Then_ReturnSpecifiqueLot() {
        List<String> lieux = new ArrayList<>();
        lieux.add("Paris");
        lieux.add("Lyon");
        lieux.add("Marseille");
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .organisme("organisme")
                .directionService("directionService")
                .naturePrestation("/api/v2/referentiels/nature-prestations/2")
                .typeProcedure("procedure")
                .typeContrat("contrat")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .commentaireInterne("commentaire")
                .datefinAffichage(ZonedDateTime.parse("2023-04-02T00:00:00+02:00"))
                .dateLimiteRemiseOffres(ZonedDateTime.parse("2023-04-03T00:00:00+02:00"))
                .dateMiseEnLigneCalcule(ZonedDateTime.parse("2023-01-03T00:00:00+02:00"))
                .idCreateur("idCreateur")
                .statutCalcule("brouillon")
                .dume(false)
                .lieuxExecution(lieux)
                .codeCpvPrincipal("12340000")
                .codeCpvSecondaire1("12340001")
                .codeCpvSecondaire2("12340002")
                .codeCpvSecondaire3("12340003")
                .bourseCotraitance(true)
                .signatureElectronique("REQUISE")
                .reponseElectronique("AUTORISEE")
                .enveloppeCandidature(false)
                .enveloppeAnonymat(true)
                .enveloppeOffre(false)
                .enveloppeOffreTechnique(true)
                .chiffrement(true)
                .alloti(false)
                .nombreLots(0)
                .clauses(clauses)
                .build();
        SpecifiqueLot lot = mapper.mapToSpecifiqueLot(consultation);
        assertNotNull(lot);
        assertEquals(3, lot.getCodeCpvSecondaires().size());
        assertEquals("12340000", lot.getCodeCpvPrincipal());
        assertEquals(3, lot.getLieuExecutions().size());
        assertEquals("2023-04-02T22:00:00+00:00", lot.getDateLimiteRemiseOffre());
        assertEquals("2023-04-01T22:00:00+00:00", lot.getDateFinAffichage());
        assertEquals(NaturePrestationEnum.FOURNITURES, lot.getCategoriePrin());
        assertEquals(7, lot.getModaliteReponses().size());
        assertEquals(TypeModaliteReponseEnum.SIGNATURE_ELECTRONIQUE, lot.getModaliteReponses().get(0).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.REQUIS, lot.getModaliteReponses().get(0).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.REPONSE_ELECTRONIQUE, lot.getModaliteReponses().get(1).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, lot.getModaliteReponses().get(1).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.CHIFFREMENT_OFFRE, lot.getModaliteReponses().get(2).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, lot.getModaliteReponses().get(2).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_CANDIDATURE, lot.getModaliteReponses().get(3).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.NON_AUTORISE, lot.getModaliteReponses().get(3).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_ANONYMAT, lot.getModaliteReponses().get(4).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, lot.getModaliteReponses().get(4).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_OFFRE, lot.getModaliteReponses().get(5).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.NON_AUTORISE, lot.getModaliteReponses().get(5).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_OFFRE_TECHNIQUE, lot.getModaliteReponses().get(6).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, lot.getModaliteReponses().get(6).getValModaliteReponse());
        assertEquals(1, lot.getModaliteParticipations().size());
        assertEquals(TypeModaliteParticipationEnum.BOURSE_COTRAITANCE, lot.getModaliteParticipations().get(0).getTypeModaliteParticipation());
        assertEquals(ValModaliteReponseEnum.AUTORISE, lot.getModaliteParticipations().get(0).getValModaliteParticipation());
        assertEquals(22, lot.getConsiderations().size());
        assertEquals(4, lot.getMarcheReserves().size());
    }

    @Test
    void Given_MpeLotDetails_When_mapToSpecifiqueLot_Then_ReturnSpecifiqueLot() {
        MpeLotDetails lot = new MpeLotDetails();
        lot.setHydraId("/api/v2/lots/2");
        lot.setConsultation("/api/v2/consultations/500004");
        lot.setNumero("1");
        lot.setIntitule("intituleLot");
        lot.setDescriptionSuccinte("descriptionLot");
        lot.setNaturePrestation("/api/v2/referentiels/nature-prestations/2");
        lot.setCodeCpvPrincipal("12340000");
        lot.setCodeCpvSecondaire1("12341111");
        lot.setCodeCpvSecondaire2("12342222");
        lot.setCodeCpvSecondaire3("12343333");
        lot.setClauses(clauses);
        SpecifiqueLot specifiqueLot = mapper.mapLotToSpecifiqueLot(lot);
        assertNotNull(lot);
        assertEquals(3, specifiqueLot.getCodeCpvSecondaires().size());
        assertEquals(22, specifiqueLot.getConsiderations().size());
        assertEquals(4, specifiqueLot.getMarcheReserves().size());
    }

}
