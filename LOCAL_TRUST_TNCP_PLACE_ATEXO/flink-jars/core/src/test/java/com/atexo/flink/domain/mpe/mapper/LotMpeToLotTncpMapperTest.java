package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.LotTNCP;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class LotMpeToLotTncpMapperTest {

    private LotMpeToLotTncpMapper mapper = new LotMpeToLotTncpMapper();
    private MpeOrganisme organisme;
    private MpeReferentiel procedure;
    private MpeReferentiel contrat;
    private MpeService direction;
    private List<ClauseN1> clauses;

    @BeforeEach
    public void setUp() {
        organisme = MpeOrganisme.builder()
                .sigle("sigleOrg")
                .denomination("denominationOrg")
                .description("descriptionOrg")
                .siret("12345678911111")
                .acronyme("acronymeOrg")
                .adresse("adresseOrg")
                .codePostal("codePostalOrg")
                .ville("villeOrg")
                .idExterne("idExterneOrg")
                .build();
        procedure = MpeReferentiel.builder()
                .libelle("Appel d'offre ouvert")
                .abreviation("AOO")
                .idExterne("idProcedure")
                .build();
        contrat = MpeReferentiel.builder()
                .libelle("Marche")
                .abreviation("MAR")
                .idExterne("mar")
                .build();
        direction = MpeService.builder()
                .sigle("sigleDS")
                .siren("123456789")
                .libelle("libelleDS")
                .adresse("adresseDS")
                .codePostal("codePostalDS")
                .ville("villeDS")
                .idExterne("idExterneDS")
                .idParent("idParentDS")
                .build();
        clauses = List.of(ClauseN1.builder()
                        .slug("clausesSociales")
                        .clausesN2(List.of(ClauseN2.builder()
                                        .slug("conditionsExecutions")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("specificationTechnique")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("critereAttributionMarche")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("marcheReserve")
                                        .clausesN3(List.of(Clause.builder().slug("clauseSocialeReserveAtelierProtege").build(),
                                                Clause.builder().slug("clauseSocialeSIAE").build(),
                                                Clause.builder().slug("clauseSocialeEESS").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("insertion")
                                        .build())
                        ).build(),
                ClauseN1.builder().slug("clauseEnvironnementale")
                        .clausesN2(List.of(ClauseN2.builder().slug("conditionsExecutions").build(),
                                ClauseN2.builder().slug("criteresSelections").build(),
                                ClauseN2.builder().slug("specificationsTechniques").build())).build());

    }

    @Test
    void Given_MpeLotDetails_When_mapLotDetailToLotTncp_Then_ReturnLotTNCP() {
        MpeLotDetails mpeLotDetails = new MpeLotDetails();
        mpeLotDetails.setHydraId("/api/v2/lots/1");
        mpeLotDetails.setConsultation("/api/v2/consultations/500004");
        mpeLotDetails.setNumero("1");
        mpeLotDetails.setIntitule("intituleLot");
        mpeLotDetails.setDescriptionSuccinte("descriptionLot");
        mpeLotDetails.setNaturePrestation("/api/v2/referentiels/nature-prestations/2");
        mpeLotDetails.setCodeCpvPrincipal("12340000");
        mpeLotDetails.setCodeCpvSecondaire1("12341111");
        mpeLotDetails.setCodeCpvSecondaire2("12342222");
        mpeLotDetails.setCodeCpvSecondaire3("12343333");
        mpeLotDetails.setClauses(clauses);
        LotTNCP lotTNCP = mapper.mapLotDetailToLotTncp(mpeLotDetails);
        assertNotNull(lotTNCP);
        assertEquals("descriptionLot", lotTNCP.getDescription());
        assertEquals("intituleLot", lotTNCP.getIntitule());
        assertEquals(1, lotTNCP.getNumeroLot());
        assertEquals("12340000", lotTNCP.getSpecifiqueLot().getCodeCpvPrincipal());
        assertEquals(3, lotTNCP.getSpecifiqueLot().getCodeCpvSecondaires().size());
        assertEquals("12341111", lotTNCP.getSpecifiqueLot().getCodeCpvSecondaires().get(0).getCodeCpv());
        assertEquals("12342222", lotTNCP.getSpecifiqueLot().getCodeCpvSecondaires().get(1).getCodeCpv());
        assertEquals("12343333", lotTNCP.getSpecifiqueLot().getCodeCpvSecondaires().get(2).getCodeCpv());
        assertEquals(22, lotTNCP.getSpecifiqueLot().getConsiderations().size());
        assertEquals(4, lotTNCP.getSpecifiqueLot().getMarcheReserves().size());
    }

}
