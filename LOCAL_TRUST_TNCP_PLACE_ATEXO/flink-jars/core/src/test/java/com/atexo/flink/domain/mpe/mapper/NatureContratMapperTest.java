package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.MpeReferentiel;
import com.atexo.flink.ws.tcnp.model.consultation.NatureContratEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class NatureContratMapperTest {

    private NatureContratMapper mapper = new NatureContratMapper();


    @Test
    void Given_MpeConsultationPlateformeCentralise_When_mapToSpecifiqueLot_Then_ReturnSpecifiqueLot() {
        MpeReferentiel contrat = MpeReferentiel.builder()
                .libelle("Marche")
                .abreviation("MAR")
                .idExterne("mar")
                .build();
        String nature = mapper.mapToTncpNatureContrat(contrat);
        assertNotNull(nature);
        assertEquals(NatureContratEnum.MARCHE.name(), nature);
    }


}
