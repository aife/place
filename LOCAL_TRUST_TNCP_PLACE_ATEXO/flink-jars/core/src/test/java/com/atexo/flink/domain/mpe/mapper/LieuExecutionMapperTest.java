package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.LieuExecution;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class LieuExecutionMapperTest {

    private LieuExecutionMapper mapper = new LieuExecutionMapper();


    @Test
    void Given_MpeConsultation_When_mapToLieuExecutions_Then_ReturnListe() {
        List<String> lieux = new ArrayList<>();
        lieux.add("Paris");
        lieux.add("Lyon");
        lieux.add("Marseille");
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .lieuxExecution(lieux)
                .build();
        List<LieuExecution> lieuxTncp = mapper.mapToLieuExecutions(consultation);
        assertNotNull(lieuxTncp);
        assertEquals(3, lieuxTncp.size());
        assertEquals("Paris", lieuxTncp.get(0).getNomLieu());
        assertEquals("Lyon", lieuxTncp.get(1).getNomLieu());
        assertEquals("Marseille", lieuxTncp.get(2).getNomLieu());

    }

}
