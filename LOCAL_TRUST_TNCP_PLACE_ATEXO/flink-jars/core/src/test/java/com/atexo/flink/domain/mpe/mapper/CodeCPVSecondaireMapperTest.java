package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.tcnp.model.consultation.CodeCpvSecondaire;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class CodeCPVSecondaireMapperTest {

    private CodeCPVSecondaireMapper mapper = new CodeCPVSecondaireMapper();


    @Test
    void Given_StringList_When_mapToCodeCpvSecondaires_Then_ReturnListeCodeCPV() {
        List<String> codes = new ArrayList<>();
        codes.add("111");
        codes.add("222");
        codes.add("333");
        List<CodeCpvSecondaire> codesTNCP = mapper.mapToCodeCpvSecondaires(codes);
        assertNotNull(codesTNCP);
        assertEquals(3, codesTNCP.size());
        assertEquals("111", codesTNCP.get(0).getCodeCpv());
        assertEquals("222", codesTNCP.get(1).getCodeCpv());
        assertEquals("333", codesTNCP.get(2).getCodeCpv());

    }

}
