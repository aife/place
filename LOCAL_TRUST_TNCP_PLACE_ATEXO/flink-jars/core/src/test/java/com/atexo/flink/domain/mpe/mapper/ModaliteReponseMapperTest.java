package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.ModaliteReponse;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteReponseEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValModaliteReponseEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ModaliteReponseMapperTest {

    private ModaliteReponseMapper mapper = new ModaliteReponseMapper();

    @Test
    void Given_MpeConsultation_When_mapToModaliteReponse_Then_ReturnListe() {
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .signatureElectronique("REQUISE")
                .reponseElectronique("AUTORISEE")
                .enveloppeCandidature(false)
                .enveloppeAnonymat(true)
                .enveloppeOffre(false)
                .enveloppeOffreTechnique(true)
                .chiffrement(true)
                .build();
        List<ModaliteReponse> modaliteReponses = mapper.mapToModaliteReponse(consultation);
        assertNotNull(modaliteReponses);
        assertEquals(7, modaliteReponses.size());
        assertEquals(TypeModaliteReponseEnum.SIGNATURE_ELECTRONIQUE, modaliteReponses.get(0).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.REQUIS, modaliteReponses.get(0).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.REPONSE_ELECTRONIQUE, modaliteReponses.get(1).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, modaliteReponses.get(1).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.CHIFFREMENT_OFFRE, modaliteReponses.get(2).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, modaliteReponses.get(2).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_CANDIDATURE, modaliteReponses.get(3).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.NON_AUTORISE, modaliteReponses.get(3).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_ANONYMAT, modaliteReponses.get(4).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, modaliteReponses.get(4).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_OFFRE, modaliteReponses.get(5).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.NON_AUTORISE, modaliteReponses.get(5).getValModaliteReponse());
        assertEquals(TypeModaliteReponseEnum.ENVELOPPE_OFFRE_TECHNIQUE, modaliteReponses.get(6).getTypeModaliteReponse());
        assertEquals(ValModaliteReponseEnum.AUTORISE, modaliteReponses.get(6).getValModaliteReponse());

    }


}
