package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.Clause;
import com.atexo.flink.ws.mpe.model.ClauseN1;
import com.atexo.flink.ws.mpe.model.ClauseN2;
import com.atexo.flink.ws.tcnp.model.consultation.Consideration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class ConsiderationsMapperTest {

    private ConsiderationsMapper mapper = new ConsiderationsMapper();
    private List<ClauseN1> clauses;

    @Test
    void Given_MpeConsultationAndSpecifiqueLot_When_mapToConsiderations_Then_ReturnListe() {
        clauses = List.of(ClauseN1.builder()
                        .slug("clausesSociales")
                        .clausesN2(List.of(ClauseN2.builder()
                                        .slug("conditionsExecutions")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("specificationTechnique")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("critereAttributionMarche")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("marcheReserve")
                                        .clausesN3(List.of(Clause.builder().slug("clauseSocialeReserveAtelierProtege").build(),
                                                Clause.builder().slug("clauseSocialeSIAE").build(),
                                                Clause.builder().slug("clauseSocialeEESS").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("insertion")
                                        .build())
                        ).build(),
                ClauseN1.builder().slug("clauseEnvironnementale")
                        .clausesN2(List.of(ClauseN2.builder().slug("conditionsExecutions").build(),
                                ClauseN2.builder().slug("criteresSelections").build(),
                                ClauseN2.builder().slug("specificationsTechniques").build())).build());
        List<Consideration> considerations = mapper.mapToConsiderations(clauses);
        assertNotNull(considerations);
        assertThat(considerations).hasSize(22);
    }

}
