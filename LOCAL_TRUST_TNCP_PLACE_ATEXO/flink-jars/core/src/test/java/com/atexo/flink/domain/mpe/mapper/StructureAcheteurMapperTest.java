package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.StructureAcheteur;
import com.atexo.flink.ws.tcnp.model.consultation.TypeTelecomEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class StructureAcheteurMapperTest {

    private StructureAcheteurMapper mapper = new StructureAcheteurMapper();

    private MpeOrganisme organisme;
    private MpeReferentiel procedure;
    private MpeReferentiel contrat;
    private MpeService direction;
    private List<ClauseN1> clauses;
    private List<String> lieux;

    @BeforeEach
    public void setUp() {
        lieux = new ArrayList<>();
        lieux.add("Paris");
        lieux.add("Lyon");
        lieux.add("Marseille");
        organisme = MpeOrganisme.builder()
                .sigle("sigleOrg")
                .denomination("denominationOrg")
                .description("descriptionOrg")
                .siret("12345678911111")
                .acronyme("acronymeOrg")
                .adresse("adresseOrg")
                .codePostal("codePostalOrg")
                .ville("villeOrg")
                .idExterne("idExterneOrg")
                .build();
        procedure = MpeReferentiel.builder()
                .libelle("Appel d'offre ouvert")
                .abreviation("AOO")
                .idExterne("idProcedure")
                .build();
        contrat = MpeReferentiel.builder()
                .libelle("Marche")
                .abreviation("MAR")
                .idExterne("mar")
                .build();
        direction = MpeService.builder()
                .sigle("sigleDS")
                .siren("123456789")
                .libelle("libelleDS")
                .adresse("adresseDS")
                .codePostal("codePostalDS")
                .ville("villeDS")
                .idExterne("idExterneDS")
                .build();
        clauses = List.of(ClauseN1.builder()
                        .slug("clausesSociales")
                        .clausesN2(List.of(ClauseN2.builder()
                                        .slug("conditionsExecutions")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("specificationTechnique")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("critereAttributionMarche")
                                        .clausesN3(List.of(Clause.builder().slug("insertionActiviteEconomique").build(),
                                                Clause.builder().slug("clauseSocialeFormationScolaire").build(),
                                                Clause.builder().slug("lutteContreDiscriminations").build(),
                                                Clause.builder().slug("commerceEquitable").build(),
                                                Clause.builder().slug("achatsEthiquesTracabiliteSociale").build(),
                                                Clause.builder().slug("autreClauseSociale").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("marcheReserve")
                                        .clausesN3(List.of(Clause.builder().slug("clauseSocialeReserveAtelierProtege").build(),
                                                Clause.builder().slug("clauseSocialeSIAE").build(),
                                                Clause.builder().slug("clauseSocialeEESS").build()))
                                        .build(),
                                ClauseN2.builder()
                                        .slug("insertion")
                                        .build())
                        ).build(),
                ClauseN1.builder().slug("clauseEnvironnementale")
                        .clausesN2(List.of(ClauseN2.builder().slug("conditionsExecutions").build(),
                                ClauseN2.builder().slug("criteresSelections").build(),
                                ClauseN2.builder().slug("specificationsTechniques").build())).build());
    }

    @Test
    void Given_MpeConsultationPlateformeCentralise_When_mapToSpecifiqueLot_Then_ReturnSpecifiqueLot() {
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .organisme("organisme")
                .naturePrestation("/api/v2/referentiels/nature-prestations/2")
                .typeProcedure("procedure")
                .typeContrat("contrat")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .commentaireInterne("commentaire")
                .datefinAffichage(ZonedDateTime.parse("2023-04-02T00:00:00.000Z"))
                .dateLimiteRemiseOffres(ZonedDateTime.parse("2023-04-03T00:00:00.000Z"))
                .dateMiseEnLigneCalcule(ZonedDateTime.parse("2023-01-03T00:00:00.000Z"))
                .idCreateur("idCreateur")
                .statutCalcule("brouillon")
                .dume(false)
                .organismeDecentralise(false)
                .lieuxExecution(lieux)
                .codeCpvPrincipal("12340000")
                .codeCpvSecondaire1("12340001")
                .codeCpvSecondaire2("12340002")
                .codeCpvSecondaire3("12340003")
                .bourseCotraitance(true)
                .signatureElectronique("REQUISE")
                .reponseElectronique("AUTORISEE")
                .enveloppeCandidature(false)
                .enveloppeAnonymat(true)
                .enveloppeOffre(false)
                .enveloppeOffreTechnique(true)
                .chiffrement(true)
                .alloti(false)
                .nombreLots(0)
                .clauses(clauses)
                .build();
        MpeAgent agent = MpeAgent.builder()
                .id("idAgent")
                .login("login")
                .email("email")
                .nom("Passet")
                .prenom("Marc")
                .telephone("0650000000")
                .fax("numfax")
                .service("service")
                .build();
        MpeService service = MpeService.builder()
                .sigle("sigleS")
                .siren("123456788")
                .libelle("libelleS")
                .adresse("adresseS")
                .codePostal("codePostalS")
                .ville("villeS")
                .idExterne("idExterneS")
                .idParent("idParentS")
                .build();
        StructureAcheteur structureAcheteur = mapper.mapToStructureAcheteur(consultation, agent, service, organisme, direction);
        assertNotNull(structureAcheteur);
        assertEquals("denominationOrg", structureAcheteur.getRaisonSociale());
        assertEquals("acronymeOrg", structureAcheteur.getDesignation());
        assertEquals("sigle", structureAcheteur.getActiviteStructurePublique());
        assertEquals("adresseOrg villeOrg", structureAcheteur.getAdresse1());
        assertEquals("codePostalOrg", structureAcheteur.getCodePostal());
        assertEquals("12345678911111", structureAcheteur.getSiret());
        assertEquals("idExterneOrg", structureAcheteur.getCodeService());
        assertEquals(1, structureAcheteur.getContactAcheteurs().size());
        assertEquals("0650000000", structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(0).getNumero());
        assertEquals(TypeTelecomEnum.TELEPHONE, structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(0).getType());
        assertEquals("numfax", structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(1).getNumero());
        assertEquals(TypeTelecomEnum.TELECOPIEUR, structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(1).getType());
        assertEquals("Passet", structureAcheteur.getContactAcheteurs().get(0).getNom());
        assertEquals("Marc", structureAcheteur.getContactAcheteurs().get(0).getPrenom());
        assertEquals("email", structureAcheteur.getContactAcheteurs().get(0).getMail());
        assertEquals("libelleS", structureAcheteur.getContactAcheteurs().get(0).getLibelleService());
    }

    @Test
    void Given_MpeConsultationPlateformeDecentralise_When_mapToSpecifiqueLot_Then_ReturnSpecifiqueLot() {
        MpeConsultation consultation = MpeConsultation.builder()
                .id(1L)
                .codeExterne("123456")
                .organismeDecentralise(true)
                .naturePrestation("/api/v2/referentiels/nature-prestations/2")
                .typeProcedure("procedure")
                .typeContrat("contrat")
                .reference("ref")
                .intitule("test")
                .objet("objet")
                .directionService("directionService")
                .commentaireInterne("commentaire")
                .datefinAffichage(ZonedDateTime.parse("2023-04-02T00:00:00.000Z"))
                .dateLimiteRemiseOffres(ZonedDateTime.parse("2023-04-03T00:00:00.000Z"))
                .dateMiseEnLigneCalcule(ZonedDateTime.parse("2023-01-03T00:00:00.000Z"))
                .idCreateur("idCreateur")
                .statutCalcule("brouillon")
                .dume(false)
                .lieuxExecution(lieux)
                .codeCpvPrincipal("12340000")
                .codeCpvSecondaire1("12340001")
                .codeCpvSecondaire2("12340002")
                .codeCpvSecondaire3("12340003")
                .bourseCotraitance(true)
                .signatureElectronique("REQUISE")
                .reponseElectronique("AUTORISEE")
                .enveloppeCandidature(false)
                .enveloppeAnonymat(true)
                .enveloppeOffre(false)
                .enveloppeOffreTechnique(true)
                .chiffrement(true)
                .alloti(false)
                .nombreLots(0)
                .clauses(clauses)
                .build();
        MpeAgent agent = MpeAgent.builder()
                .id("idAgent")
                .login("login")
                .email("email")
                .nom("Passet")
                .prenom("Marc")
                .telephone("0650000000")
                .fax("numfax")
                .service("service")
                .build();
        MpeService service = MpeService.builder()
                .sigle("sigleS")
                .siren("123456788")
                .libelle("libelleS")
                .adresse("adresseS")
                .codePostal("codePostalS")
                .ville("villeS")
                .idExterne("idExterneS")
                .idParent("idParentS")
                .build();
        StructureAcheteur structureAcheteur = mapper.mapToStructureAcheteur(consultation, agent, service, organisme, direction);
        assertNotNull(structureAcheteur);
        assertEquals("libelleDS", structureAcheteur.getRaisonSociale());
        assertEquals("libelleDS", structureAcheteur.getDesignation());
        assertEquals("sigle", structureAcheteur.getActiviteStructurePublique());
        assertEquals("adresseDS villeDS", structureAcheteur.getAdresse1());
        assertEquals("codePostalDS", structureAcheteur.getCodePostal());
        assertEquals("12345678900000", structureAcheteur.getSiret());
        assertEquals("idExterneDS", structureAcheteur.getCodeService());
        assertEquals(1, structureAcheteur.getContactAcheteurs().size());
        assertEquals("0650000000", structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(0).getNumero());
        assertEquals(TypeTelecomEnum.TELEPHONE, structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(0).getType());
        assertEquals("numfax", structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(1).getNumero());
        assertEquals(TypeTelecomEnum.TELECOPIEUR, structureAcheteur.getContactAcheteurs().get(0).getTelecoms().get(1).getType());
        assertEquals("Passet", structureAcheteur.getContactAcheteurs().get(0).getNom());
        assertEquals("Marc", structureAcheteur.getContactAcheteurs().get(0).getPrenom());
        assertEquals("email", structureAcheteur.getContactAcheteurs().get(0).getMail());
        assertEquals("libelleS", structureAcheteur.getContactAcheteurs().get(0).getLibelleService());
    }
}
