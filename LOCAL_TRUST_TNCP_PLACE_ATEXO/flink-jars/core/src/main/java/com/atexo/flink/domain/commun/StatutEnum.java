package com.atexo.flink.domain.commun;

public enum StatutEnum {
    FINI, ERREUR, EN_COURS, EN_ATTENTE
}
