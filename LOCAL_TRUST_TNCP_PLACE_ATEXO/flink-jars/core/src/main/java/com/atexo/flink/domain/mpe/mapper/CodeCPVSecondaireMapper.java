package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.tcnp.model.consultation.CodeCpvSecondaire;

import java.util.ArrayList;
import java.util.List;

public class CodeCPVSecondaireMapper {

    List<CodeCpvSecondaire> mapToCodeCpvSecondaires(List<String> codes) {
        List<CodeCpvSecondaire> codeCpvSecondaires = new ArrayList<>();
        for (String code : codes) {
            codeCpvSecondaires.add(CodeCpvSecondaire.builder().codeCpv(code).build());
        }
        return codeCpvSecondaires;
    }
}
