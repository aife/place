package com.atexo.flink.domain.mpe.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@JsonSerialize
public class NotificationConsultationMessage {
    private String type;
    private String contenu;
}
