package com.atexo.flink.domain.mpe.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@JsonSerialize
public class Notification {
    private NotificationConsultationResponse response;
}
