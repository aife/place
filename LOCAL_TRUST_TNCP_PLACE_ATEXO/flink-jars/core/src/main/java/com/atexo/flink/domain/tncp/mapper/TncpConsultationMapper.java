package com.atexo.flink.domain.tncp.mapper;

import com.atexo.flink.domain.mpe.mapper.NatureContratMapper;
import com.atexo.flink.domain.mpe.mapper.NaturePrestationMapper;
import com.atexo.flink.domain.mpe.mapper.ReponseElectroniqueMapper;
import com.atexo.flink.domain.mpe.mapper.TypeProcedureMapper;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.*;
import org.apache.commons.collections.CollectionUtils;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TncpConsultationMapper {

    private final NaturePrestationMapper naturePrestationMapper = new NaturePrestationMapper();
    private final NatureContratMapper natureContratMapper = new NatureContratMapper();
    private final TypeProcedureMapper typeProcedureMapper = new TypeProcedureMapper();
    private final ReponseElectroniqueMapper reponseElectroniqueMapper = new ReponseElectroniqueMapper();

    public MpeConsultation tncpConsultationToConsultationMpe(MpeConsultation mpeConsultation, TncpConsultation tncpConsultation, MpeWs mpeWS, String organisme) {
        if (tncpConsultation == null) {
            return null;
        }
        if (mpeConsultation.getOrganisme() == null) {
            mpeConsultation.setOrganisme("/api/v2/referentiels/organismes/" + organisme);
        }
        mpeConsultation.setCodeExterne(tncpConsultation.getReferenceUnique());
        if (tncpConsultation.getCommentaireInterne() != null) {
            mpeConsultation.setCommentaireInterne(tncpConsultation.getCommentaireInterne());
        } else if (mpeConsultation.getCommentaireInterne() == null) {
            mpeConsultation.setCommentaireInterne("");
        }

        if (tncpConsultation.getSpecifiqueLot() != null && tncpConsultation.getSpecifiqueLot().getCategoriePrin() != null) {
            mpeConsultation.setNaturePrestation(naturePrestationMapper.mapToMpeNatureNaturePrestation(mpeWS, tncpConsultation.getSpecifiqueLot().getCategoriePrin().name()));
        }
        if (tncpConsultation.getSpecifiqueLot() != null && tncpConsultation.getSpecifiqueLot().getDateLimiteRemiseOffre() != null) {
            mpeConsultation.setDateLimiteRemisePlis(ZonedDateTime.parse(tncpConsultation.getSpecifiqueLot().getDateLimiteRemiseOffre()));
        }
        if (tncpConsultation.getSpecifiqueLot() != null && tncpConsultation.getSpecifiqueLot().getDateFinAffichage() != null) {
            mpeConsultation.setDatefinAffichage(ZonedDateTime.parse(tncpConsultation.getSpecifiqueLot().getDateFinAffichage()));
        }
        if (tncpConsultation.getSpecifiqueLot() != null && !CollectionUtils.isEmpty(tncpConsultation.getSpecifiqueLot().getLieuExecutions())) {
            mpeConsultation.setLieuxExecution(tncpConsultation.getSpecifiqueLot().getLieuExecutions().stream().map(LieuExecution::getNomLieu).collect(Collectors.toList()));
        }
        if (tncpConsultation.getSpecifiqueLot() != null) {
            mpeConsultation.setReponseElectronique(calculReponseElectronique(tncpConsultation.getSpecifiqueLot().getModaliteReponses()));
        }
        mpeConsultation.setReference(tncpConsultation.getReference());
        mpeConsultation.setIntitule(tncpConsultation.getIntitule());
        mpeConsultation.setObjet(tncpConsultation.getObjet());
        String typeProcedure = tncpConsultation.getTypeProcedure();
        if (typeProcedure != null) {
            mpeConsultation.setTypeProcedure(typeProcedureMapper.mapToMpeTypeProcedure(mpeWS, typeProcedure));
        }
        String natureContrat = tncpConsultation.getNatureContrat();
        if (natureContrat != null) {
            mpeConsultation.setTypeContrat(natureContratMapper.mapToMpeNatureContrat(mpeWS, natureContrat));
        }
        if (tncpConsultation.getSpecifiqueLot() != null) {
            mpeConsultation.setCodeCpvPrincipal(tncpConsultation.getSpecifiqueLot().getCodeCpvPrincipal());
        }
        if (tncpConsultation.getSpecifiqueLot() != null && !CollectionUtils.isEmpty(tncpConsultation.getSpecifiqueLot().getConsiderations())) {
            mpeConsultation.setClauses(mappingClause(tncpConsultation.getSpecifiqueLot().getConsiderations()));
        }
        return mpeConsultation;
    }


    public MpeLot tncoLotToMpeLot(LotTNCP lotTNCP, String consultation, String naturePrestation, String codeCpvPrincipal) {
        if (lotTNCP == null) {
            return null;
        }
        MpeLot mpeLot = new MpeLot();
        mpeLot.setConsultation(consultation);
        if (lotTNCP.getNumeroLot() != null) {
            mpeLot.setNumero(String.valueOf(lotTNCP.getNumeroLot()));
        }
        mpeLot.setIntitule(lotTNCP.getIntitule());
        mpeLot.setDescriptionSuccinte(lotTNCP.getDescription());
        mpeLot.setNaturePrestation(naturePrestation);
        mpeLot.setCodeCpvPrincipal(codeCpvPrincipal);
        return mpeLot;
    }

    private String calculReponseElectronique(List<ModaliteReponse> modaliteReponseList) {
        return Optional.ofNullable(modaliteReponseList)
                .orElse(Collections.emptyList())
                .stream()
                .filter(e -> TypeModaliteReponseEnum.REPONSE_ELECTRONIQUE.equals(e.getTypeModaliteReponse()))
                .filter(e -> e.getValModaliteReponse() != null).findFirst().map(e -> reponseElectroniqueMapper.mapToMpeReponseElectronique(e.getValModaliteReponse().name())).orElse(null);
    }

    private List<ClauseN1> mappingClause(List<Consideration> considerations) {
        var clauses = prepareClauseN1(considerations);
        prepareClauseN2(considerations, clauses);
        prepareClauseN3(considerations, clauses);
        return clauses;

    }

    private List<ClauseN1> prepareClauseN1(List<Consideration> considerations) {
        return considerations.stream().map(Consideration::getTypeConsideration)
                .distinct()
                .map(e -> {
                    ClauseN1 clauseN1 = new ClauseN1();
                    clauseN1.setSlug(e.name());
                    clauseN1.setClausesN2(new ArrayList<>());
                    return clauseN1;
                }).collect(Collectors.toList());
    }

    private void prepareClauseN2(List<Consideration> considerations, List<ClauseN1> clauses) {
        for (ClauseN1 clauseN1 : clauses) {
            clauseN1.setClausesN2(considerations.stream()
                    .filter(e -> clauseN1.getSlug().equals(e.getTypeConsideration().name()))
                    .map(Consideration::getValConsideration)
                    .map(e -> {
                        ClauseN2 clauseN2 = new ClauseN2();
                        clauseN2.setSlug(e.name());
                        clauseN2.setClausesN3(new ArrayList<>());
                        return clauseN2;
                    })
                    .collect(Collectors.toList()));
        }
    }

    private void prepareClauseN3(List<Consideration> considerations, List<ClauseN1> clauses) {
        for (ClauseN1 clauseN1 : clauses) {
            for (ClauseN2 clauseN2 : clauseN1.getClausesN2()) {
                clauseN2.setClausesN3(considerations.stream()
                        .filter(e -> clauseN1.getSlug().equals(e.getTypeConsideration().name()))
                        .filter(e -> clauseN2.getSlug().equals(e.getValConsideration().name()))
                        .map(Consideration::getDescConsideration)
                        .map(e -> {
                            Clause clauseN3 = new Clause();
                            clauseN3.setSlug(e.name());
                            return clauseN3;
                        })
                        .collect(Collectors.toList()));
            }

        }
    }
}

