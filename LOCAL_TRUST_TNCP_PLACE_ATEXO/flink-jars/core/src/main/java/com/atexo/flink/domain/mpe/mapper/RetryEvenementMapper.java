package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.domain.commun.EvenementAtexo;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

@Slf4j
public class RetryEvenementMapper<T> extends RichMapFunction<Tuple3<String, EvenementAtexo, T>, Tuple3<String, EvenementAtexo, T>> {


    @Override
    public Tuple3<String, EvenementAtexo, T> map(Tuple3<String, EvenementAtexo, T> value) throws Exception {
        EvenementAtexo k = value.f1;
        int retry = k.getRetry();
        if (retry == 0) {
            k.setRetry(retry + 15);
        } else {
            k.setRetry(retry * 2);
        }
        return new Tuple3<>(value.f0, k, value.f2);
    }
}
