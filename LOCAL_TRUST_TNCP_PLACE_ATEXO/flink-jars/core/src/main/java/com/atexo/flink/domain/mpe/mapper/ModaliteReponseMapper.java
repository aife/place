package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.ModaliteReponse;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteReponseEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValModaliteReponseEnum;

import java.util.ArrayList;
import java.util.List;

public class ModaliteReponseMapper {

    List<ModaliteReponse> mapToModaliteReponse(MpeConsultation consultation) {
        List<ModaliteReponse> modaliteReponses = new ArrayList<>();
        if (consultation.getSignatureElectronique() != null) {
            ModaliteReponse modaliteSignature = ModaliteReponse.builder()
                    .typeModaliteReponse(TypeModaliteReponseEnum.SIGNATURE_ELECTRONIQUE)
                    .build();
            switch (consultation.getSignatureElectronique()) {
                case "AUTORISEE":
                    modaliteSignature.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE);
                    break;
                case "REQUISE":
                    modaliteSignature.setValModaliteReponse(ValModaliteReponseEnum.REQUIS);
                    break;
                default:
                    modaliteSignature.setValModaliteReponse(ValModaliteReponseEnum.SANS_OBJET);//verif
            }
            modaliteReponses.add(modaliteSignature);
        }
        if (consultation.getReponseElectronique() != null) {
            ModaliteReponse modaliteReponseElectronique = ModaliteReponse.builder()
                    .typeModaliteReponse(TypeModaliteReponseEnum.REPONSE_ELECTRONIQUE)
                    .build();
            switch (consultation.getReponseElectronique()) {
                case "AUTORISEE":
                    modaliteReponseElectronique.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE);
                    break;
                case "REFUSE":
                    modaliteReponseElectronique.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE);
                    break;
                case "OBLIGATOIRE":
                    modaliteReponseElectronique.setValModaliteReponse(ValModaliteReponseEnum.REQUIS);
                    break;
                default:
                    modaliteReponseElectronique.setValModaliteReponse(ValModaliteReponseEnum.SANS_OBJET);//verif
            }
            modaliteReponses.add(modaliteReponseElectronique);
        }
        ModaliteReponse modaliteChiffrement = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.CHIFFREMENT_OFFRE)
                .build();
        if (consultation.isChiffrement()) {
            modaliteChiffrement.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteChiffrement.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteReponses.add(modaliteChiffrement);
        ModaliteReponse modaliteEnveloppeCandidature = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.ENVELOPPE_CANDIDATURE)
                .build();
        if (consultation.isEnveloppeCandidature()) {
            modaliteEnveloppeCandidature.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteEnveloppeCandidature.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteReponses.add(modaliteEnveloppeCandidature);
        ModaliteReponse modaliteEnveloppeAnonymat = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.ENVELOPPE_ANONYMAT)
                .build();
        if (consultation.isEnveloppeAnonymat()) {
            modaliteEnveloppeAnonymat.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteEnveloppeAnonymat.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteReponses.add(modaliteEnveloppeAnonymat);
        ModaliteReponse modaliteEnveloppeOffre = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.ENVELOPPE_OFFRE)
                .build();
        if (consultation.isEnveloppeOffre()) {
            modaliteEnveloppeOffre.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteEnveloppeOffre.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteReponses.add(modaliteEnveloppeOffre);
        ModaliteReponse modaliteEnveloppeOffreTechnique = ModaliteReponse.builder()
                .typeModaliteReponse(TypeModaliteReponseEnum.ENVELOPPE_OFFRE_TECHNIQUE)
                .build();
        if (consultation.isEnveloppeOffreTechnique()) {
            modaliteEnveloppeOffreTechnique.setValModaliteReponse(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteEnveloppeOffreTechnique.setValModaliteReponse(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteReponses.add(modaliteEnveloppeOffreTechnique);

        return modaliteReponses;
    }
}
