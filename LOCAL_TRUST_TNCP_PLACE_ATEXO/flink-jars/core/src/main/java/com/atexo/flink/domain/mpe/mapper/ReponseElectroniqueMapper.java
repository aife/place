package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.FlinkJobUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReponseElectroniqueMapper {
    private final Map<String, String> tncpMpeMappingValues;

    public ReponseElectroniqueMapper() {

        Map<String, String> tncpMpeMappingValues1;
        try {
            tncpMpeMappingValues1 = FlinkJobUtils.getMappingTncpMpe().get("reponse-electronique");
        } catch (IOException e) {
            tncpMpeMappingValues1 = new HashMap<>();
        }
        tncpMpeMappingValues = tncpMpeMappingValues1;
    }


    public String mapToMpeReponseElectronique(String idExterne) {
        if (idExterne == null) return null;
        return tncpMpeMappingValues.get(idExterne);

    }
}
