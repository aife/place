package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeReferentiel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NaturePrestationMapper {
    private final Map<String, String> tncpMpeMappingValues;

    public NaturePrestationMapper() {

        Map<String, String> tncpMpeMappingValues1;
        try {
            tncpMpeMappingValues1 = FlinkJobUtils.getMappingTncpMpe().get("nature-prestation");
        } catch (IOException e) {
            tncpMpeMappingValues1 = new HashMap<>();
        }
        tncpMpeMappingValues = tncpMpeMappingValues1;
    }


    public String mapToMpeNatureNaturePrestation(MpeWs mpeWs, String idExterne) {
        if (idExterne == null) return null;
        MpeReferentiel naturePrestation = mpeWs.getReferentielsByTypeAndIdExterne("nature-prestations", tncpMpeMappingValues.get(idExterne));
        return naturePrestation == null ? null : naturePrestation.getId();

    }
}
