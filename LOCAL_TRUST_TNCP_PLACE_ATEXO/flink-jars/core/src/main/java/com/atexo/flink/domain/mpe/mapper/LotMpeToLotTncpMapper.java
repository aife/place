package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.MpeLotDetails;
import com.atexo.flink.ws.tcnp.model.consultation.LotTNCP;
import com.atexo.flink.ws.tcnp.model.consultation.SpecifiqueLot;

public class LotMpeToLotTncpMapper {

    private SpecifiqueLotMapper specifiqueLotMapper = new SpecifiqueLotMapper();


    public LotTNCP mapLotDetailToLotTncp(MpeLotDetails lot) {
        LotTNCP lotTncp = new LotTNCP();
        if (lot.getIntitule() != null)
            lotTncp.setIntitule(lot.getIntitule());
        if (lot.getDescriptionSuccinte() != null)
            lotTncp.setDescription(lot.getDescriptionSuccinte());
        if (lot.getNumero() != null)
            lotTncp.setNumeroLot(Integer.parseInt(lot.getNumero()));
        SpecifiqueLot specifiqueLot = specifiqueLotMapper.mapLotToSpecifiqueLot(lot);
        lotTncp.setSpecifiqueLot(specifiqueLot);
        return lotTncp;
    }
}
