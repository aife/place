package com.atexo.flink.domain.commun;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.java.tuple.Tuple4;

import java.nio.charset.StandardCharsets;

import static com.atexo.flink.config.FlinkJobUtils.setMDC;

@Slf4j
public class SuccessSerializationSchema<T extends EvenementAtexo> implements SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> {

    @Override
    public byte[] serialize(Tuple4<String, T, AtexoJobException, TncpSuivi> element) {
        setMDC(element.f1);
        return element.f0.getBytes(StandardCharsets.UTF_8);
    }
}
