package com.atexo.flink.domain.mpe;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.mpe.model.MpeEnveloppe;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.java.tuple.Tuple4;

import java.util.ArrayList;
import java.util.HashMap;

import static com.atexo.flink.config.FlinkJobUtils.setMDC;

@Slf4j
public class TncpConsultationsEnAttenteSerializationSchema<T extends EvenementAtexo> implements SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> {

    @Override
    public byte[] serialize(Tuple4<String, T, AtexoJobException, TncpSuivi> element) {

        try {
            T f1 = element.f1;
            setMDC(f1);
            MpeEnveloppe mpeMessageRequest = MpeEnveloppe.builder()
                    .headers(new HashMap<>())
                    .properties(new ArrayList<>())
                    .body(AtexoUtils.getMapper().writeValueAsString(f1)).build();
            return AtexoUtils.getMapper().writeValueAsString(mpeMessageRequest).getBytes();
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            log.error("Failed to parse JSON", e);
        }
        return new byte[0];
    }
}
