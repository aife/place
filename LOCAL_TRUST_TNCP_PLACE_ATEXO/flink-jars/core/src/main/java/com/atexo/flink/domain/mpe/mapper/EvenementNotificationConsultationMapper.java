package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import java.time.Instant;
import java.time.ZonedDateTime;

@Slf4j
public class EvenementNotificationConsultationMapper extends RichMapFunction<String, Tuple3<String, EvenementAtexo, AtexoJobException>> {


    @Override
    public Tuple3<String, EvenementAtexo, AtexoJobException> map(String source) throws Exception {
        AtexoJobException atexoJobException = null;
        EvenementAtexo evenement = null;
        try {
            TncpSuivi tncpSuivi = AtexoUtils.getMapper().readValue(source, TncpSuivi.class);
            if (StatutEnum.FINI.equals(tncpSuivi.getStatut())) {
                evenement = new EvenementAtexo();
                evenement.setUuid(tncpSuivi.getUuid());
                evenement.setEtape(tncpSuivi.getEtape());
                evenement.setFlux(tncpSuivi.getFlux());
                evenement.setType(tncpSuivi.getType());
                evenement.setUuidPlateforme(tncpSuivi.getUuidPlateforme());
                evenement.setIdObjetSource(tncpSuivi.getIdObjetSource());
                evenement.setIdObjetDestination(tncpSuivi.getIdObjetDestination());
                evenement.setObjetDestination(tncpSuivi.getObjetDestination());

                FlinkJobUtils.setMDC(evenement);
                log.info("process de la evenement {} / {}", evenement.getUuid(), EvenementAtexo.class);
                evenement.setExecutionTime(Instant.now());
                evenement.setDateReception(ZonedDateTime.now());
                evenement.setMessage(null);
                evenement.setMessageDetails(null);
            }
        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", source, e.getMessage());
            atexoJobException = new AtexoJobException(e.getMessage(), JobExeptionEnum.CONVERSION_EVENEMENT);
        }
        return new Tuple3<>(source, evenement, atexoJobException);

    }
}
