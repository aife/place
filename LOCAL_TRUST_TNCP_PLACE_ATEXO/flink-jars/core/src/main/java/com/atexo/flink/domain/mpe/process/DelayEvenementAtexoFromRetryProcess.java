package com.atexo.flink.domain.mpe.process;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.domain.commun.EvenementAtexo;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Slf4j
public class DelayEvenementAtexoFromRetryProcess extends KeyedProcessFunction<String, Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple3<String, EvenementAtexo, AtexoJobException>> {


    private ValueState<CountWithTimestamp> state;


    @Override
    public void processElement(Tuple3<String, EvenementAtexo, AtexoJobException> input, Context context, Collector<Tuple3<String, EvenementAtexo, AtexoJobException>> output) throws Exception {
        CountWithTimestamp current = state.value();
        FlinkJobUtils.setMDC(input.f1);

        if (current == null) {
            current = new CountWithTimestamp();
        }


        int retry = input.f1.getRetry();
        current.key = input;
        Instant dateExecution = current.dateExecution = input.f1.getExecutionTime().plus(retry, ChronoUnit.MINUTES);
        Instant now = Instant.now();

        if (dateExecution.toEpochMilli() <= now.toEpochMilli()) {
            state.clear();
            output.collect(input);
        } else {
            state.update(current);

            log.info("reported from {} to {}", now, current.dateExecution);
            log.info("current process : {}", Instant.ofEpochMilli(context.timerService().currentProcessingTime()));
            context.timerService().registerProcessingTimeTimer(current.dateExecution.toEpochMilli());
        }
    }

    @Override
    public void onTimer(long timestamp, KeyedProcessFunction<String, Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple3<String, EvenementAtexo, AtexoJobException>>.OnTimerContext ctx, Collector<Tuple3<String, EvenementAtexo, AtexoJobException>> out) throws Exception {
        CountWithTimestamp like = state.value();
        Instant sendAt = like.dateExecution;
        log.info("onTimer {} : {}", Instant.ofEpochMilli(timestamp), sendAt);
        if (timestamp >= sendAt.toEpochMilli()) {
            log.info("onTimer {}", timestamp);
            out.collect(like.key);
            state.clear();
        }
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        state = getRuntimeContext().getState(new ValueStateDescriptor<>("myState", CountWithTimestamp.class));
    }

    /**
     * The data type stored in the state
     */
    public static class CountWithTimestamp {

        public Tuple3<String, EvenementAtexo, AtexoJobException> key;
        public Instant dateExecution;
    }
}
