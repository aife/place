package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.Clause;
import com.atexo.flink.ws.mpe.model.ClauseN1;
import com.atexo.flink.ws.tcnp.model.consultation.MarcheReserve;
import com.atexo.flink.ws.tcnp.model.consultation.MarcheReserveEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class MarcheReserveMapper {

    List<MarcheReserve> mapToMarcheReserve(List<ClauseN1> clauses) {
        List<MarcheReserve> marcheReserves = new ArrayList<>();
        if (!CollectionUtils.isEmpty(clauses)) {
            clauses.forEach(clauseN1 -> {
                if ("clausesSociales".equals(clauseN1.getSlug())) {
                    if (!CollectionUtils.isEmpty(clauseN1.getClausesN2())) {
                        clauseN1.getClausesN2().forEach(clauseN2 -> {
                            if ("marcheReserve".equals(clauseN2.getSlug())) {
                                if (!CollectionUtils.isEmpty(clauseN2.getClausesN3())) {
                                    for (Clause critere : clauseN2.getClausesN3()) {
                                        switch (critere.getSlug()) {//verif renvoi MPE
                                            case "clauseSocialeReserveAtelierProtege":
                                                marcheReserves.add(MarcheReserve.builder().marcheReserveEnum(MarcheReserveEnum.EA).build());
                                                marcheReserves.add(MarcheReserve.builder().marcheReserveEnum(MarcheReserveEnum.ESAT).build());
                                                break;
                                            case "clauseSocialeSIAE":
                                                marcheReserves.add(MarcheReserve.builder().marcheReserveEnum(MarcheReserveEnum.SIAE).build());
                                                break;
                                            case "clauseSocialeEESS":
                                                marcheReserves.add(MarcheReserve.builder().marcheReserveEnum(MarcheReserveEnum.EESS).build());
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        });
                    }

                }
            });
        }

        return marcheReserves;

    }
}
