package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.ModaliteParticipation;
import com.atexo.flink.ws.tcnp.model.consultation.TypeModaliteParticipationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValModaliteReponseEnum;

import java.util.ArrayList;
import java.util.List;

public class ModaliteParticipationMapper {

    public List<ModaliteParticipation> mapToModaliteParticipation(MpeConsultation consultation) {
        List<ModaliteParticipation> modaliteParticipations = new ArrayList<>();
        ModaliteParticipation modaliteBourse = ModaliteParticipation.builder()
                .typeModaliteParticipation(TypeModaliteParticipationEnum.BOURSE_COTRAITANCE)
                .build();
        if (consultation.isBourseCotraitance()) {
            modaliteBourse.setValModaliteParticipation(ValModaliteReponseEnum.AUTORISE); //autorise ou requis ?
        } else {
            modaliteBourse.setValModaliteParticipation(ValModaliteReponseEnum.NON_AUTORISE); //non-autorise ou sans-objet ?
        }
        modaliteParticipations.add(modaliteBourse);

        return modaliteParticipations;
    }


}
