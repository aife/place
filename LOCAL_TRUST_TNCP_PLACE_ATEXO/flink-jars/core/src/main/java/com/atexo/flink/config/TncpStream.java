package com.atexo.flink.config;

import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementAtexoMapper;
import com.atexo.flink.domain.contrat.mapper.EvenementContratMapper;
import com.atexo.flink.domain.contrat.model.EvenementContrat;
import com.atexo.flink.domain.mpe.mapper.EvenementConsultationMapper;
import com.atexo.flink.domain.mpe.mapper.EvenementNotificationConsultationMapper;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.tcnp.TncpWs;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.ExecutionOptions;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.concurrent.TimeUnit;

public class TncpStream<T extends EvenementAtexo> {
    protected final KafkaSource<String> waitingSource;
    protected final KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> errorSink;

    protected final KafkaSink<String> abandonSink;
    protected final KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> tncpSuiviSink;
    protected final KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> sucessSink;
    protected final KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> waitingSink;
    protected final JarFlinkConfiguration configuration;
    protected KafkaSource<String> tncpNotificationsSuivi;

    /**
     * Creates a job using the source and sink provided.
     */
    public TncpStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> sucessSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> waitingSink, MpeWs mpeWs, TncpWs tncpWs, JarFlinkConfiguration configuration, KafkaSource<String> tncpNotificationsSuivi) {

        this.waitingSource = waitingSource;
        this.sucessSink = sucessSink;
        this.errorSink = errorSink;
        this.abandonSink = abandonSink;
        this.tncpSuiviSink = tncpSuiviSink;
        this.waitingSink = waitingSink;
        AtexoUtils.setMpeWs(mpeWs);
        AtexoUtils.setTncpWs(tncpWs);
        this.configuration = configuration;
        this.tncpNotificationsSuivi = tncpNotificationsSuivi;
    }

    public TncpStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> sucessSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> waitingSink, MpeWs mpeWs, TncpWs tncpWs, JarFlinkConfiguration configuration) {

        this.waitingSource = waitingSource;
        this.sucessSink = sucessSink;
        this.errorSink = errorSink;
        this.abandonSink = abandonSink;
        this.tncpSuiviSink = tncpSuiviSink;
        this.waitingSink = waitingSink;
        AtexoUtils.setMpeWs(mpeWs);
        AtexoUtils.setTncpWs(tncpWs);
        this.configuration = configuration;
    }

    protected KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> getEvenementEnAttente(StreamExecutionEnvironment env, String jobName) {
        KeyedStream<String, String> src = env
                .fromSource(waitingSource, WatermarkStrategy.noWatermarks(), "Consultation en Attente + En erreur")
                .uid("sourceEnAttente-" + jobName)
                .name("Recuperation des messages en attente + en erreur")
                .keyBy(value -> value);

        return parseEvenementConsultation(src, jobName);
    }

    protected KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> getEvenementNotificationConsultation(StreamExecutionEnvironment env, String jobName) {
        KeyedStream<String, String> src = env
                .fromSource(tncpNotificationsSuivi, WatermarkStrategy.noWatermarks(), "Suivi de Notifications TNCP ")
                .uid("sourceNotificationSuivi-" + jobName)
                .name("Recuperation des notifications TNCP")
                .keyBy(value -> value);

        return parseEvenementNotificationConsultation(src, jobName);
    }

    protected KeyedStream<Tuple3<String, EvenementContrat, AtexoJobException>, String> getEvenementContratEnAttente(StreamExecutionEnvironment env, String jobName) {
        KeyedStream<String, String> src = env.fromSource(waitingSource, WatermarkStrategy.noWatermarks(), "Consultation en Attente + En erreur").uid("sourceEnAttente-" + jobName).name("Recuperation des messages en attente + en erreur").keyBy(value -> value);

        return parseEvenementContrat(src, jobName);
    }

    protected KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> getEvenementAtexoEnAttente(StreamExecutionEnvironment env, String jobName) {
        KeyedStream<String, String> src = env.fromSource(waitingSource, WatermarkStrategy.noWatermarks(), "Consultation en Attente + En erreur").uid("sourceEnAttente-" + jobName).name("Recuperation des messages en attente + en erreur").keyBy(value -> value);

        return parseEvenementAtexo(src, jobName);
    }


    protected StreamExecutionEnvironment getStreamExecutionEnvironment() {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        RuntimeExecutionMode executionMode = ExecutionOptions.RUNTIME_MODE.defaultValue();
        env.setRuntimeMode(executionMode);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(100000, Time.of(5, TimeUnit.MINUTES)));
        return env;
    }

    private KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> parseEvenementConsultation(KeyedStream<String, String> sourceRabbitMQ, String jobName) {
        return sourceRabbitMQ.map(new EvenementConsultationMapper())
                .uid("evenement-consultation-parse-" + jobName)
                .name("Recuperation des informations a partir de l'evenement")
                .keyBy(value -> value.f0);
    }

    private KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> parseEvenementNotificationConsultation(KeyedStream<String, String> sourceRabbitMQ, String jobName) {
        return sourceRabbitMQ.map(new EvenementNotificationConsultationMapper())
                .uid("evenement-notification-parse-" + jobName)
                .name("Recuperation des informations a partir de l'evenement Notification")
                .keyBy(value -> value.f0);
    }


    private KeyedStream<Tuple3<String, EvenementContrat, AtexoJobException>, String> parseEvenementContrat(KeyedStream<String, String> sourceRabbitMQ, String jobName) {
        return sourceRabbitMQ.map(new EvenementContratMapper()).uid("evenement-contrat-parse-" + jobName).name("Recuperation des informations a partir de l'evenement").keyBy(value -> value.f0);
    }

    private KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> parseEvenementAtexo(KeyedStream<String, String> sourceRabbitMQ, String jobName) {
        return sourceRabbitMQ.map(new EvenementAtexoMapper()).uid("evenement-atexo-parse-" + jobName).name("Recuperation des informations a partir de l'evenement").keyBy(value -> value.f0);
    }


}
