package com.atexo.flink.config;

import org.springframework.util.StringUtils;

public class TechnicalExeption extends RuntimeException {


    public TechnicalExeption(String message) {
        super(message);
    }

    public TechnicalExeption(String message, Throwable cause) {
        super(StringUtils.isEmpty(message) ? cause.getMessage() : message, cause);
    }
}
