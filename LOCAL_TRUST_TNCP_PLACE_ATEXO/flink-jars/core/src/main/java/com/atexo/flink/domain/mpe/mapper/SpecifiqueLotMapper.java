package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.mpe.model.MpeLotDetails;
import com.atexo.flink.ws.mpe.model.NaturePrestationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.*;
import org.apache.commons.collections.CollectionUtils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.time.ZoneOffset.UTC;


public class SpecifiqueLotMapper {

    private static final String PATTERN_FORMAT = "yyyy-MM-dd'T'HH:mm:ssxxx";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_FORMAT);

    private final LieuExecutionMapper lieuExecutionMapper = new LieuExecutionMapper();
    private final CodeCPVSecondaireMapper codeCPVSecondaireMapper = new CodeCPVSecondaireMapper();
    private final ModaliteParticipationMapper modaliteParticipationMapper = new ModaliteParticipationMapper();
    private final ModaliteReponseMapper modaliteReponseMapper = new ModaliteReponseMapper();
    private final ConsiderationsMapper considerationsMapper = new ConsiderationsMapper();
    private final MarcheReserveMapper marcheReserveMapper = new MarcheReserveMapper();


    public SpecifiqueLot mapToSpecifiqueLot(MpeConsultation consultation) {
        SpecifiqueLot specifiqueLot = new SpecifiqueLot();

        ZonedDateTime dateLimiteRemiseOffres = consultation.getDateLimiteRemiseOffres();
        ZonedDateTime dateFinAffichage;

        Integer poursuivreAffichage = consultation.getPoursuivreAffichage();
        if (dateLimiteRemiseOffres != null && poursuivreAffichage != null && poursuivreAffichage > 0) {
            switch (consultation.getPoursuivreAffichageUnite()) {
                case "DAY":
                    dateFinAffichage = dateLimiteRemiseOffres.plusDays(poursuivreAffichage);
                    break;
                case "MONTH":
                    dateFinAffichage = dateLimiteRemiseOffres.plusMonths(poursuivreAffichage);
                    break;
                case "MINUTE":
                    dateFinAffichage = dateLimiteRemiseOffres.plusMinutes(poursuivreAffichage);
                    break;
                case "HOUR":
                    dateFinAffichage = dateLimiteRemiseOffres.plusHours(poursuivreAffichage);
                    break;
                case "SECONDE":
                    dateFinAffichage = dateLimiteRemiseOffres.plusSeconds(poursuivreAffichage);
                    break;
                case "YEAR":
                    dateFinAffichage = dateLimiteRemiseOffres.plusYears(poursuivreAffichage);
                    break;
                default:
                    dateFinAffichage = dateLimiteRemiseOffres;
            }
        } else {
            dateFinAffichage = consultation.getDatefinAffichage();
        }
        specifiqueLot.setDateFinAffichage(getFormat(dateFinAffichage));
        specifiqueLot.setDateLimiteRemiseOffre(getFormat(dateLimiteRemiseOffres));
        specifiqueLot.setCodeCpvPrincipal(consultation.getCodeCpvPrincipal());
        specifiqueLot.setCategoriePrin(getNaturePrestation(consultation.getNaturePrestation()));
        specifiqueLot.setAccesRestreint(false);
        List<LieuExecution> lieux = lieuExecutionMapper.mapToLieuExecutions(consultation);
        if (!CollectionUtils.isEmpty(lieux))
            specifiqueLot.setLieuExecutions(lieux);

        List<Consideration> considerations = considerationsMapper.mapToConsiderations(consultation.getClauses());
        if (!CollectionUtils.isEmpty(considerations))
            specifiqueLot.setConsiderations(considerations);
        List<MarcheReserve> marcheReserves = marcheReserveMapper.mapToMarcheReserve(consultation.getClauses());
        if (!CollectionUtils.isEmpty(marcheReserves))
            specifiqueLot.setMarcheReserves(marcheReserves);

        List<ModaliteParticipation> modaliteParticipations = modaliteParticipationMapper.mapToModaliteParticipation(consultation);
        if (!CollectionUtils.isEmpty(modaliteParticipations))
            specifiqueLot.setModaliteParticipations(modaliteParticipations);
        List<ModaliteReponse> modaliteReponses = modaliteReponseMapper.mapToModaliteReponse(consultation);
        if (!CollectionUtils.isEmpty(modaliteReponses))
            specifiqueLot.setModaliteReponses(modaliteReponses);
        List<String> codes = new ArrayList<>();
        if (consultation.getCodeCpvSecondaire1() != null)
            codes.add(consultation.getCodeCpvSecondaire1());
        if (consultation.getCodeCpvSecondaire2() != null)
            codes.add(consultation.getCodeCpvSecondaire2());
        if (consultation.getCodeCpvSecondaire3() != null)
            codes.add(consultation.getCodeCpvSecondaire3());
        List<CodeCpvSecondaire> codeCpvSecondaires = new ArrayList<>();
        if (!CollectionUtils.isEmpty(codes)) {
            codeCpvSecondaires = codeCPVSecondaireMapper.mapToCodeCpvSecondaires(codes);
        }
        specifiqueLot.setCodeCpvSecondaires(codeCpvSecondaires);
        return specifiqueLot;
    }

    public SpecifiqueLot mapLotToSpecifiqueLot(MpeLotDetails lot) {
        SpecifiqueLot specifiqueLot = new SpecifiqueLot();
        List<String> codes = new ArrayList<>();
        if (lot.getCodeCpvSecondaire1() != null)
            codes.add(lot.getCodeCpvSecondaire1());
        if (lot.getCodeCpvSecondaire2() != null)
            codes.add(lot.getCodeCpvSecondaire2());
        if (lot.getCodeCpvSecondaire3() != null)
            codes.add(lot.getCodeCpvSecondaire3());
        List<CodeCpvSecondaire> codeCpvSecondaires = new ArrayList<>();
        if (!CollectionUtils.isEmpty(codes)) {
            codeCpvSecondaires = codeCPVSecondaireMapper.mapToCodeCpvSecondaires(codes);
        }
        specifiqueLot.setCodeCpvSecondaires(codeCpvSecondaires);

        List<MarcheReserve> marcheReserves = marcheReserveMapper.mapToMarcheReserve(lot.getClauses());
        if (!CollectionUtils.isEmpty(marcheReserves))
            specifiqueLot.setMarcheReserves(marcheReserves);

        List<Consideration> considerations = considerationsMapper.mapToConsiderations(lot.getClauses());
        if (!CollectionUtils.isEmpty(considerations))
            specifiqueLot.setConsiderations(considerations);

        specifiqueLot.setCodeCpvPrincipal(lot.getCodeCpvPrincipal());

        return specifiqueLot;
    }

    private String getFormat(ZonedDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return formatter.format(dateTime.withZoneSameInstant(UTC));
    }

    private NaturePrestationEnum getNaturePrestation(String nature) {
        switch (nature.toLowerCase()) {
            case "/api/v2/referentiels/nature-prestations/1":
                return NaturePrestationEnum.TRAVAUX;
            case "/api/v2/referentiels/nature-prestations/2":
                return NaturePrestationEnum.FOURNITURES;
            case "/api/v2/referentiels/nature-prestations/3":
                return NaturePrestationEnum.SERVICES;
            default:
                return null;

        }
    }
}
