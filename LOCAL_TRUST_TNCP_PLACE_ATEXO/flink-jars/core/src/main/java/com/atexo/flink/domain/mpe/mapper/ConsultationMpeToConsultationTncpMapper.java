package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.*;
import com.atexo.flink.ws.tcnp.model.consultation.StatutConsultEnum;
import com.atexo.flink.ws.tcnp.model.consultation.StructureAcheteur;
import com.atexo.flink.ws.tcnp.model.consultation.TncpConsultation;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.ZoneOffset.UTC;

public class ConsultationMpeToConsultationTncpMapper {
    private static final String PATTERN_FORMAT = "yyyy-MM-dd'T'HH:mm:ssxxx";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN_FORMAT);
    private final SpecifiqueLotMapper specifiqueLotMapper = new SpecifiqueLotMapper();
    private final StructureAcheteurMapper structureAcheteurMapper = new StructureAcheteurMapper();
    private final NatureContratMapper natureContratMapper = new NatureContratMapper();
    private final TypeProcedureMapper typeProcedureMapper = new TypeProcedureMapper();

    public TncpConsultation mapToTncp(MpeConsultation consultation, MpeAgent mpeAgent, MpeService service, String uuidPlateforme,
                                      MpeReferentiel typeContrat, MpeReferentiel typeProcedure, MpeOrganisme organisme, MpeService directionService) {

        final int MAX_OBJET = 1024;
        final int MAX_INTITULE = 256;
        TncpConsultation tncpConsultation = new TncpConsultation();
        String intitule = consultation.getIntitule();
        if (intitule != null && intitule.length() > MAX_INTITULE) {
            tncpConsultation.setObjet(intitule.substring(0, MAX_INTITULE));
        } else {
            tncpConsultation.setIntitule(intitule);

        }
        tncpConsultation.setReference(consultation.getReference());
        String objet = consultation.getObjet();
        if (objet != null && objet.length() > MAX_OBJET) {
            tncpConsultation.setObjet(objet.substring(0, MAX_OBJET));
        } else {
            tncpConsultation.setObjet(objet);

        }
        if (consultation.getCommentaireInterne() != null) {
            // copy max 1024 characters
            tncpConsultation.setCommentaireInterne(consultation.getCommentaireInterne().substring(0, Math.min(1024, consultation.getCommentaireInterne().length())));
        }
        tncpConsultation.setDateMiseEnLigne(getFormat(consultation.getDateMiseEnLigneCalcule()));
        tncpConsultation.setDerniereModification(getFormat(ZonedDateTime.now(UTC)));
        tncpConsultation.setNombreLots(consultation.getNombreLots());
        tncpConsultation.setNatureContrat(natureContratMapper.mapToTncpNatureContrat(typeContrat));
        tncpConsultation.setTypeProcedure(typeProcedureMapper.mapToTncpTypeProcedure(typeProcedure));
        tncpConsultation.setStatutConsult(StatutConsultEnum.ELABORE);
        tncpConsultation.setIdConPlateforme(String.valueOf(consultation.getId()));
        tncpConsultation.setIdPlateforme(uuidPlateforme);
        String idConEurUnique = uuidPlateforme + "-" + consultation.getId();
        // copy max 32 characters
        tncpConsultation.setIdConEurUnique(idConEurUnique.substring(0, Math.min(32, idConEurUnique.length())));
        tncpConsultation.setSpecifiqueLot(specifiqueLotMapper.mapToSpecifiqueLot(consultation));
        StructureAcheteur structureAcheteur = structureAcheteurMapper.mapToStructureAcheteur(consultation, mpeAgent, service, organisme, directionService);
        tncpConsultation.setStructureAcheteur(structureAcheteur);
        String urlConsultation = consultation.getUrlConsultation();
        tncpConsultation.setUrlConsultation(urlConsultation == null ? "" : urlConsultation);
        tncpConsultation.setReponsePaTiers(false);
        return tncpConsultation;

    }

    private String getFormat(ZonedDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return formatter.format(dateTime.withZoneSameInstant(UTC));
    }

}
