package com.atexo.flink.domain.commun;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = {"uuid", "flux", "type", "etape"})
@JsonSerialize
public class EvenementAtexo implements Serializable {
    private String uuid;
    private InterfaceEnum flux;
    private TypeEnum type;
    private EtapeEnum etape;
    private String idObjetSource;
    private String idObjetDestination;
    private String objetDestination;
    private String uuidPlateforme;
    private String message;
    private String messageDetails;
    private ZonedDateTime dateEnvoi;
    private int retry = 0;
    @JsonIgnore
    private Instant executionTime;
    private ZonedDateTime dateReception;

}
