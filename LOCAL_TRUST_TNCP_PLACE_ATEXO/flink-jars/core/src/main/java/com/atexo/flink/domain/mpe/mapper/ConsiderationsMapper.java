package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.Clause;
import com.atexo.flink.ws.mpe.model.ClauseN1;
import com.atexo.flink.ws.mpe.model.ClauseN2;
import com.atexo.flink.ws.tcnp.model.consultation.Consideration;
import com.atexo.flink.ws.tcnp.model.consultation.DescConsiderationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.TypeConsiderationEnum;
import com.atexo.flink.ws.tcnp.model.consultation.ValConsiderationEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ConsiderationsMapper {


    List<Consideration> mapToConsiderations(List<ClauseN1> clauses) {

        List<Consideration> considerations = new ArrayList<>();
        if (!CollectionUtils.isEmpty(clauses)) {
            clauses.forEach(clauseN1 -> {
                if ("clausesSociales".equals(clauseN1.getSlug())) {
                    if (!CollectionUtils.isEmpty(clauseN1.getClausesN2())) {
                        clauseN1.getClausesN2().forEach(clauseN2 -> {
                            if ("conditionsExecutions".equals(clauseN2.getSlug()) || "conditionExecution".equals(clauseN2.getSlug())) {
                                if (!CollectionUtils.isEmpty(clauseN2.getClausesN3())) {
                                    addConsideration(clauseN2.getClausesN3(), considerations, ValConsiderationEnum.CONDITION_EXECUTION);
                                } else {
                                    considerations.add(createConsideration(ValConsiderationEnum.CONDITION_EXECUTION, null));
                                }
                            }
                            if ("specificationTechnique".equals(clauseN2.getSlug())) {
                                if (!CollectionUtils.isEmpty(clauseN2.getClausesN3())) {
                                    addConsideration(clauseN2.getClausesN3(), considerations, ValConsiderationEnum.SPECIFICATION_TECHNIQUE);
                                } else {
                                    considerations.add(createConsideration(ValConsiderationEnum.SPECIFICATION_TECHNIQUE, null));
                                }
                            }

                            if ("critereAttributionMarche".equals(clauseN2.getSlug())) {
                                if (!CollectionUtils.isEmpty(clauseN2.getClausesN3())) {
                                    addConsideration(clauseN2.getClausesN3(), considerations, ValConsiderationEnum.CRITERE_ATTRIBUTION);
                                } else {
                                    considerations.add(createConsideration(ValConsiderationEnum.CRITERE_ATTRIBUTION, null));
                                }
                            }
                            if ("insertion".equals(clauseN2.getSlug())) {
                                if (!CollectionUtils.isEmpty(clauseN2.getClausesN3())) {
                                    addConsideration(clauseN2.getClausesN3(), considerations, ValConsiderationEnum.INSERTION);
                                } else {
                                    considerations.add(createConsideration(ValConsiderationEnum.INSERTION, null));
                                }
                            }

                        });
                    }

                } else {
                    getClauseEnvironnementaleTncp(considerations, clauseN1.getClausesN2());

                }
            });
        }

        return considerations;

    }

    private void getClauseEnvironnementaleTncp(List<Consideration> considerations, List<ClauseN2> clauses) {
        clauses.forEach(clauseEnvironnementaleEnum -> {
            switch (clauseEnvironnementaleEnum.getSlug()) {
                case "specificationsTechniques":
                    considerations.add(Consideration.builder()
                            .valConsideration(ValConsiderationEnum.SPECIFICATION_TECHNIQUE)
                            .typeConsideration(TypeConsiderationEnum.ENVIRONNEMENTALE)
                            .build());
                    break;
                case "conditionsExecutions":
                    considerations.add(Consideration.builder()
                            .valConsideration(ValConsiderationEnum.CONDITION_EXECUTION)
                            .typeConsideration(TypeConsiderationEnum.ENVIRONNEMENTALE)
                            .build());
                    break;
                case "criteresSelections":
                    considerations.add(Consideration.builder()
                            .valConsideration(ValConsiderationEnum.CRITERE_ATTRIBUTION)
                            .typeConsideration(TypeConsiderationEnum.ENVIRONNEMENTALE)
                            .build());
                    break;
            }
        });
    }

    private void addConsideration(List<Clause> criteres, List<Consideration> considerations, ValConsiderationEnum valeur) {
        for (Clause critere : criteres) {
            switch (critere.getSlug()) {
                case "achatsEthiquesTracabiliteSociale":
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.ACHATS_ETHIQUES));
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.TRACABILITE_SOCIALE));
                    break;
                case "insertionActiviteEconomique":
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.INSERTION_ACTIVITE_ECONOMIQUE));
                    break;
                case "clauseSocialeFormationScolaire":
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.CLAUSE_SOCIALE_FORMATION));
                    break;
                case "lutteContreDiscriminations":
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.LUTTE_CONTRE_DISCRIMINATIONS));
                    break;
                case "commerceEquitable":
                    considerations.add(createConsideration(valeur, DescConsiderationEnum.COMMERCE_EQUITABLE));
                    break;
                default:
                    log.warn("le critere {} n'est pas pris en consideration", critere);
            }
        }
    }

    private Consideration createConsideration(ValConsiderationEnum valeur, DescConsiderationEnum description) {
        Consideration consideration = Consideration.builder().build();
        consideration.setTypeConsideration(TypeConsiderationEnum.SOCIALE);
        consideration.setValConsideration(valeur);
        consideration.setDescConsideration(description);
        return consideration;
    }
}
