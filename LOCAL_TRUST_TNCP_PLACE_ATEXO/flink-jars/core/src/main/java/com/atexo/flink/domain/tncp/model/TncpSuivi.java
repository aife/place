package com.atexo.flink.domain.tncp.model;

import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.StatutEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TncpSuivi extends EvenementAtexo implements Serializable {
    private StatutEnum statut;
    private String body;
    private String traitementPlateforme;

}
