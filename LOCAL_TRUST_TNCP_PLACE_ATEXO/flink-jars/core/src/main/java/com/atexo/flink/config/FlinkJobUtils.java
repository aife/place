package com.atexo.flink.config;

import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.shaded.jackson2.org.yaml.snakeyaml.Yaml;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Properties;

@Slf4j
public final class FlinkJobUtils {
    private FlinkJobUtils() {

    }

    public static JobConfig getJobConfig() throws IOException {
        Yaml yaml = new Yaml();

        Map<String, Object> map = yaml.load(new ClassPathResource("application.yml").getInputStream());


        return AtexoUtils.getMapper().readValue(AtexoUtils.getMapper().writeValueAsString(map), JobConfig.class);
    }

    public static Map<String, Map<String, String>> getMappingMpeTncp() throws IOException {
        Yaml yaml = new Yaml();

        Map<String, Object> map = yaml.load(new ClassPathResource("mapping-mpe-tncp.yml").getInputStream());


        return AtexoUtils.getMapper().readValue(AtexoUtils.getMapper().writeValueAsString(map), Map.class);
    }

    public static Map<String, Map<String, String>> getMappingTncpMpe() throws IOException {
        Yaml yaml = new Yaml();

        Map<String, Object> map = yaml.load(new ClassPathResource("mapping-tncp-mpe.yml").getInputStream());


        return AtexoUtils.getMapper().readValue(AtexoUtils.getMapper().writeValueAsString(map), Map.class);
    }

    public static TncpSuivi getTncpSuiviErreur(EvenementAtexo evenementConsultation, AtexoJobException ex, Logger log) {
        int retry = evenementConsultation.getRetry();
        evenementConsultation.setRetry(retry + 15);
        TncpSuivi f1 = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
        f1.setDateEnvoi(ZonedDateTime.now());
        f1.setStatut(StatutEnum.ERREUR);
        f1.setBody(ex.getBody());
        f1.setMessageDetails(getErreur(ex, log));
        f1.setMessage(ex.getType() + " : " + ex.getMessage());
        return f1;
    }

    public static String getErreur(AtexoJobException ex, Logger log) {
        StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        ex.printStackTrace(pw);
        if (log != null) {
            log.error("Erreur : {}", writer);
        }
        return writer.toString();
    }

    public static void setMDC(EvenementAtexo evenementConsultation) {
        if (evenementConsultation == null) {
            return;
        }
        log.info("Setting MDC for {}", evenementConsultation);
        if (evenementConsultation.getUuidPlateforme() != null) {
            MDC.put("uuidPlateforme", evenementConsultation.getUuidPlateforme());
        }
        if (evenementConsultation.getUuid() != null) {
            MDC.put("idEvenementMpe", evenementConsultation.getUuid());
        }
        if (evenementConsultation.getIdObjetSource() != null) {
            MDC.put("idMpe", evenementConsultation.getIdObjetSource());
        }
        if (evenementConsultation.getEtape() != null) {
            MDC.put("action", evenementConsultation.getType().name() + "(" + evenementConsultation.getEtape().name() + ")");
        } else {
            MDC.put("action", evenementConsultation.getType().name());
        }
        if (evenementConsultation.getIdObjetDestination() != null) {
            MDC.put("referenceTncp", evenementConsultation.getIdObjetDestination());
        }
    }


    public static KafkaSource<String> getKafkaSource(Properties properties, String... topics) {
        KafkaSourceBuilder<String> stringKafkaSourceBuilder = KafkaSource.<String>builder()
                .setBootstrapServers(properties.getProperty("bootstrap.servers"))
                .setTopics(topics)
                .setGroupId(properties.getProperty("group.id"))
                .setClientIdPrefix(properties.getProperty("client.id"))
                .setStartingOffsets(OffsetsInitializer.committedOffsets(OffsetResetStrategy.LATEST))
                .setValueOnlyDeserializer(new SimpleStringSchema());
        properties.forEach((o, o2) -> stringKafkaSourceBuilder.setProperty((String) o, (String) o2));
        stringKafkaSourceBuilder.setProperty("partition.discovery.interval.ms", "10000");
        stringKafkaSourceBuilder.setProperty("register.consumer.metrics", "false");
        return stringKafkaSourceBuilder.build();
    }


    public static Properties getProperties(JarFlinkConfiguration configuration, String jobName, String topic) {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", configuration.getKafkaBroker());
        properties.setProperty("group.id", configuration.getKafkaGroupIdPrefix() + "-" + jobName);
        properties.setProperty("client.id", configuration.getKafkaClientId() + "-" + jobName + "-" + topic);
        properties.setProperty("transaction.timeout.ms", "60000");
        if (configuration.getKafkaSecurityProtocol() != null) {
            log.info("activating SASL_SSL");
            properties.setProperty("security.protocol", configuration.getKafkaSecurityProtocol());
        }
        if (configuration.getKafkaSaslMechanism() != null) {
            properties.setProperty("sasl.mechanism", configuration.getKafkaSaslMechanism());
            properties.setProperty("ssl.endpoint.identification.algorithm", "");
        }
        if (configuration.getKafkaSaslJaasConfig() != null) {
            properties.setProperty("sasl.jaas.config", configuration.getKafkaSaslJaasConfig());
        }
        if (configuration.getKafkaCertTruststoreFolder() != null) {
            properties.setProperty("ssl.truststore.location", configuration.getKafkaCertTruststoreFolder());
        }
        if (configuration.getKafkaCertKeystoreFolder() != null) {
            properties.setProperty("ssl.keystore.location", configuration.getKafkaCertKeystoreFolder());
        }
        if (configuration.getKafkaCertKeystorePassword() != null) {
            properties.setProperty("ssl.keystore.password", configuration.getKafkaCertKeystorePassword());
        }
        if (configuration.getKafkaCertTruststorePassword() != null) {
            properties.setProperty("ssl.truststore.password", configuration.getKafkaCertTruststorePassword());
        }

        return properties;
    }


}
