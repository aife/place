package com.atexo.flink.domain.contrat.mapper;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.TypeEnum;
import com.atexo.flink.domain.contrat.model.EvenementContrat;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import java.time.Instant;
import java.time.ZonedDateTime;

@Slf4j
public class EvenementContratMapper extends RichMapFunction<String, Tuple3<String, EvenementContrat, AtexoJobException>> {


    @Override
    public Tuple3<String, EvenementContrat, AtexoJobException> map(String source) throws Exception {
        AtexoJobException atexoJobException = null;
        EvenementContrat evenement = null;
        try {

            evenement = AtexoUtils.getMapper().readValue(source, EvenementContrat.class);
                FlinkJobUtils.setMDC(evenement);
                log.info("process de la evenement {} / {}", evenement.getUuid(), EvenementContrat.class);
                evenement.setExecutionTime(Instant.now());
                evenement.setDateReception(ZonedDateTime.now());
                evenement.setMessage(null);
                evenement.setMessageDetails(null);
                if (evenement.getEtape() == null && TypeEnum.PUBLICATION_DE_CONTRAT.equals(evenement.getType())) {
                    evenement.setEtape(EtapeEnum.PUBLICATION_DE_CONTRAT);
                }


        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", source, e.getMessage());
            atexoJobException = new AtexoJobException(e.getMessage(), JobExeptionEnum.CONVERSION_EVENEMENT);
        }
        return new Tuple3<>(source, evenement, atexoJobException);

    }
}
