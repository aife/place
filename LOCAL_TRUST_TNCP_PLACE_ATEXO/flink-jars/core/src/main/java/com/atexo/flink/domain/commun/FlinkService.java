package com.atexo.flink.domain.commun;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;

import java.util.Properties;

public class FlinkService<T extends EvenementAtexo> {
    public FlinkService() {
    }

    public KafkaSink<String> getSink(String topic, Properties properties) {
        return KafkaSink.<String>builder().setBootstrapServers(properties.getProperty("bootstrap.servers")).setRecordSerializer(KafkaRecordSerializationSchema.builder().setTopic(topic).setValueSerializationSchema(new SimpleStringSchema()).build()).setTransactionalIdPrefix(properties.getProperty("client.id") + "-" + topic).setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE).setKafkaProducerConfig(properties).build();
    }

    public KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> getSinkTncpSuivi(String topic, Properties properties, SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> valueSerializationSchema) {
        return KafkaSink.<Tuple4<String, T, AtexoJobException, TncpSuivi>>builder().setBootstrapServers(properties.getProperty("bootstrap.servers")).setRecordSerializer(KafkaRecordSerializationSchema.builder().setTopic(topic).setValueSerializationSchema(valueSerializationSchema).build()).setTransactionalIdPrefix(properties.getProperty("client.id") + "-" + topic).setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE).setKafkaProducerConfig(properties).build();
    }


    public KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> getWaitingSinkEvenement(String topic, Properties properties, SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> valueSerializationSchema) {
        return KafkaSink.<Tuple4<String, T, AtexoJobException, TncpSuivi>>builder().setBootstrapServers(properties.getProperty("bootstrap.servers")).setRecordSerializer(KafkaRecordSerializationSchema.builder().setTopic(topic).setValueSerializationSchema(valueSerializationSchema).build()).setTransactionalIdPrefix(properties.getProperty("client.id") + "-" + topic).setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE).setKafkaProducerConfig(properties).build();
    }

    public KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> getErreurAbandonSinkEvenement(String topic, Properties properties, SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> valueSerializationSchema) {

        return KafkaSink.<Tuple4<String, T, AtexoJobException, TncpSuivi>>builder().setBootstrapServers(properties.getProperty("bootstrap.servers")).setRecordSerializer(KafkaRecordSerializationSchema.builder().setTopic(topic).setValueSerializationSchema(valueSerializationSchema).build()).setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE).setTransactionalIdPrefix(properties.getProperty("client.id") + "-" + topic).setKafkaProducerConfig(properties).build();
    }

    public KafkaSink<Tuple4<String, T, AtexoJobException, TncpSuivi>> getSuccessSinkEvenement(String topic, Properties properties, SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> valueSerializationSchema) {
        return KafkaSink.<Tuple4<String, T, AtexoJobException, TncpSuivi>>builder().setBootstrapServers(properties.getProperty("bootstrap.servers")).setRecordSerializer(KafkaRecordSerializationSchema.builder().setTopic(topic).setValueSerializationSchema(valueSerializationSchema).build()).setDeliverGuarantee(DeliveryGuarantee.AT_LEAST_ONCE).setTransactionalIdPrefix(properties.getProperty("client.id") + "-" + topic).setKafkaProducerConfig(properties).build();
    }
}
