package com.atexo.flink.domain.commun;


import com.atexo.flink.domain.tncp.model.TncpSuivi;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EvenementToTncpSuiviMapper {
    EvenementToTncpSuiviMapper INSTANCE = Mappers.getMapper(EvenementToTncpSuiviMapper.class);

    TncpSuivi mapToSuiviTncp(EvenementAtexo consultation);
}
