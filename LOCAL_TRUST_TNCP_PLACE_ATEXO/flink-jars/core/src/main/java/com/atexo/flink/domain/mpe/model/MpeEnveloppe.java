package com.atexo.flink.domain.mpe.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@JsonSerialize
@Builder
public class MpeEnveloppe {
    private String body;
    private List<String> properties = new ArrayList<>();
    private Map<String, String> headers = new HashMap<>();
}
