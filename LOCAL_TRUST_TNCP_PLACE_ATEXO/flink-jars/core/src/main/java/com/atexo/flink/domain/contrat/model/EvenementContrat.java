package com.atexo.flink.domain.contrat.model;

import com.atexo.flink.domain.commun.EvenementAtexo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@JsonSerialize
public class EvenementContrat extends EvenementAtexo implements Serializable {

    private String payload;

}
