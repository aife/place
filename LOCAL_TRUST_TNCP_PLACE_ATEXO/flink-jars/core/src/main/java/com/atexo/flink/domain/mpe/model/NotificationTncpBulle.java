package com.atexo.flink.domain.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotificationTncpBulle {

    private String bulleMetier;
}
