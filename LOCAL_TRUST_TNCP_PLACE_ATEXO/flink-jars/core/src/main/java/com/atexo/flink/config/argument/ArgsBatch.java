package com.atexo.flink.config.argument;

import com.beust.jcommander.Parameter;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@Getter
public class ArgsBatch {


    @Parameter(names = {"--notifications", "-notifications"}, description = "Lancement du job notifications TNCP")
    private boolean notifications;

    @Parameter(names = {"--recuperation", "-recuperation"}, description = "Lancement du job notifications TNCP")
    private boolean recuperation;

    public static String[] filterSpringBootArgs(String[] args) {
        // Filtrer les arguments
        List<String> allowedPrefixes = List.of("--notifications", "-notifications","--recuperation", "-recuperation");
        return Arrays.stream(args).filter(arg -> allowedPrefixes.stream().anyMatch(arg::startsWith)).toArray(String[]::new);
    }


}
