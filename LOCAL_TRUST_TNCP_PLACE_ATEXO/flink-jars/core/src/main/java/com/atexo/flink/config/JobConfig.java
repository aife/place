package com.atexo.flink.config;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobConfig {
    private String name;
    private String version;
}
