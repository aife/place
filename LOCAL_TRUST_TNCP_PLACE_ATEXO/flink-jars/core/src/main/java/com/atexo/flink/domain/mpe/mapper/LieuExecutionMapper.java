package com.atexo.flink.domain.mpe.mapper;


import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.tcnp.model.consultation.LieuExecution;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class LieuExecutionMapper {

    public List<LieuExecution> mapToLieuExecutions(MpeConsultation consultation) {

        List<LieuExecution> lieux = new ArrayList<>();

        if (!CollectionUtils.isEmpty(consultation.getLieuxExecution())) {
            for (String lieu : consultation.getLieuxExecution()) {
                LieuExecution lieuExecution = LieuExecution.builder()
                        .nomLieu(lieu)
                        .build();
                lieux.add(lieuExecution);
            }
        }
        return lieux;

    }
}
