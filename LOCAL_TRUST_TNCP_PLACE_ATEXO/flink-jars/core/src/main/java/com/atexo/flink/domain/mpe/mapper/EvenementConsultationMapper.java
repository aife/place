package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobExeptionEnum;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.TypeEnum;
import com.atexo.flink.domain.mpe.model.MpeEnveloppe;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.ZonedDateTime;

@Slf4j
public class EvenementConsultationMapper extends RichMapFunction<String, Tuple3<String, EvenementAtexo, AtexoJobException>> {


    @Override
    public Tuple3<String, EvenementAtexo, AtexoJobException> map(String source) throws Exception {
        MpeEnveloppe mpeMessage;
        AtexoJobException atexoJobException = null;
        EvenementAtexo evenement = null;
        try {
            mpeMessage = AtexoUtils.getMapper().readValue(source, MpeEnveloppe.class);
            if (mpeMessage == null || !StringUtils.hasText(mpeMessage.getBody())) {
                atexoJobException = new AtexoJobException("valeur null envoyee", JobExeptionEnum.CONVERSION_EVENEMENT);
                log.error(atexoJobException.getMessage());
            } else {
                evenement = AtexoUtils.getMapper().readValue(mpeMessage.getBody(), EvenementAtexo.class);
                FlinkJobUtils.setMDC(evenement);
                log.info("process de la evenement {} / {}", evenement.getUuid(), EvenementAtexo.class);
                evenement.setExecutionTime(Instant.now());
                evenement.setDateReception(ZonedDateTime.now());
                evenement.setMessage(null);
                evenement.setMessageDetails(null);
                if (evenement.getEtape() == null && TypeEnum.PUBLICATION_CONSULTATION.equals(evenement.getType())) {
                    evenement.setEtape(EtapeEnum.AJOUT_CONSULTATION);
                }
            }

        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", source, e.getMessage());
            atexoJobException = new AtexoJobException(e.getMessage(), JobExeptionEnum.CONVERSION_EVENEMENT);
        }
        return new Tuple3<>(source, evenement, atexoJobException);

    }
}
