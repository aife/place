package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeReferentiel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TypeProcedureMapper {

    private final Map<String, String> mpeTncpMappingValues;
    private final Map<String, String> tncpMpeMappingValues;

    public TypeProcedureMapper() {
        Map<String, String> mpeTncpMappingValues1;
        try {
            mpeTncpMappingValues1 = FlinkJobUtils.getMappingMpeTncp().get("type-procedure");
        } catch (IOException e) {
            mpeTncpMappingValues1 = new HashMap<>();
        }
        mpeTncpMappingValues = mpeTncpMappingValues1;

        Map<String, String> tncpMpeMappingValues1;
        try {
            tncpMpeMappingValues1 = FlinkJobUtils.getMappingTncpMpe().get("type-procedure");
        } catch (IOException e) {
            tncpMpeMappingValues1 = new HashMap<>();
        }
        tncpMpeMappingValues = tncpMpeMappingValues1;
    }

    public String mapToTncpTypeProcedure(MpeReferentiel typeProcedure) {
        if (typeProcedure == null || typeProcedure.getIdExterne() == null)
            return null;
        return mpeTncpMappingValues.get(typeProcedure.getIdExterne());
    }

    public String mapToMpeTypeProcedure(MpeWs mpeWs, String idExterne) {
        if (idExterne == null) return null;
        MpeReferentiel natureContratMpe = mpeWs.getReferentielsByTypeAndIdExterne("contrats", tncpMpeMappingValues.get(idExterne));
        return natureContratMpe == null ? null : natureContratMpe.getId();

    }
}
