package com.atexo.flink.domain.commun;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.config.JobExeptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import java.time.Instant;
import java.time.ZonedDateTime;

@Slf4j
public class EvenementAtexoMapper extends RichMapFunction<String, Tuple3<String, EvenementAtexo, AtexoJobException>> {


    @Override
    public Tuple3<String, EvenementAtexo, AtexoJobException> map(String source) throws Exception {
        AtexoJobException atexoJobException = null;
        EvenementAtexo evenement = null;
        try {

            evenement = AtexoUtils.getMapper().readValue(source, EvenementAtexo.class);
            FlinkJobUtils.setMDC(evenement);
            log.info("process de la evenement {} / {}", evenement.getUuid(), EvenementAtexo.class);
            evenement.setExecutionTime(Instant.now());
            evenement.setDateReception(ZonedDateTime.now());
            evenement.setMessage(null);
            evenement.setMessageDetails(null);
            if (evenement.getEtape() == null && TypeEnum.RECUPERATION_NOTIFICATION.equals(evenement.getType())) {
                evenement.setEtape(EtapeEnum.RECUPERATION_NOTIFICATION);
            }


        } catch (Exception | OutOfMemoryError e) {
            log.error("Erreur lors du process de {} => {}", source, e.getMessage());
            atexoJobException = new AtexoJobException(e.getMessage(), JobExeptionEnum.CONVERSION_EVENEMENT);
        }
        return new Tuple3<>(source, evenement, atexoJobException);

    }
}
