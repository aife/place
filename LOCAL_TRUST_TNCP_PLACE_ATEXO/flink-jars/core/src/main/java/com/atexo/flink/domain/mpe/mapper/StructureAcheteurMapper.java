package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.ws.mpe.model.MpeAgent;
import com.atexo.flink.ws.mpe.model.MpeConsultation;
import com.atexo.flink.ws.mpe.model.MpeOrganisme;
import com.atexo.flink.ws.mpe.model.MpeService;
import com.atexo.flink.ws.tcnp.model.consultation.ContactAcheteur;
import com.atexo.flink.ws.tcnp.model.consultation.StructureAcheteur;
import com.atexo.flink.ws.tcnp.model.consultation.Telecom;
import com.atexo.flink.ws.tcnp.model.consultation.TypeTelecomEnum;

import java.util.ArrayList;
import java.util.List;

public class StructureAcheteurMapper {

    public StructureAcheteur mapToStructureAcheteur(MpeConsultation consultation, MpeAgent agent, MpeService service,
                                                    MpeOrganisme organisme, MpeService directionService) {

        StructureAcheteur structureAcheteur = new StructureAcheteur();
        //verif cas centralise

        if (Boolean.TRUE.equals(consultation.getOrganismeDecentralise()) && directionService != null) {
            structureAcheteur.setRaisonSociale(directionService.getLibelle());
            String sigle = directionService.getSigle();
            String activiteStructurePublique = sigle != null && sigle.length() > 5 ? sigle.substring(0, 5) : sigle;
            structureAcheteur.setActiviteStructurePublique(activiteStructurePublique);
            structureAcheteur.setDesignation(directionService.getLibelle());
            structureAcheteur.setAdresse1(directionService.getAdresse() + " " + directionService.getVille());
            structureAcheteur.setCodeService(directionService.getIdExterne());
            structureAcheteur.setCodePostal(directionService.getCodePostal());
            if (directionService.getSiret() != null) {
                structureAcheteur.setSiret(directionService.getSiret());
            } else {
                String complement = directionService.getComplement();
                structureAcheteur.setSiret(directionService.getSiren() + (complement == null ? "00000" : complement));
            }

        } else if (organisme != null) {
            structureAcheteur.setRaisonSociale(organisme.getDenomination());
            String sigle = organisme.getSigle();
            String activiteStructurePublique = sigle != null && sigle.length() > 5 ? sigle.substring(0, 5) : sigle;
            structureAcheteur.setActiviteStructurePublique(activiteStructurePublique);
            structureAcheteur.setDesignation(organisme.getAcronyme());
            structureAcheteur.setAdresse1(organisme.getAdresse() + " " + organisme.getVille());
            structureAcheteur.setCodePostal(organisme.getCodePostal());
            structureAcheteur.setCodeService(organisme.getIdExterne());
            if (organisme.getSiret() != null) {
                structureAcheteur.setSiret(organisme.getSiret());
            } else {
                String complement = organisme.getComplement();
                structureAcheteur.setSiret(organisme.getSiren() + (complement == null ? "00000" : complement));
            }

        }

        List<ContactAcheteur> acheteurs = new ArrayList<>();
        ContactAcheteur acheteur = new ContactAcheteur();
        acheteur.setNom(agent.getNom());
        acheteur.setPrenom(agent.getPrenom());
        acheteur.setMail(agent.getEmail());
        if (service != null) {
            acheteur.setLibelleService(service.getLibelle());
        }
        List<Telecom> telecoms = new ArrayList<>();
        if (agent.getTelephone() != null) {
            Telecom telecom1 = Telecom.builder()
                    .type(TypeTelecomEnum.TELEPHONE)
                    .numero(agent.getTelephone())
                    .build();
            telecoms.add(telecom1);
        }
        if (agent.getFax() != null) {
            Telecom telecom2 = Telecom.builder()
                    .type(TypeTelecomEnum.TELECOPIEUR)
                    .numero(agent.getFax())
                    .build();
            telecoms.add(telecom2);
        }

        acheteur.setTelecoms(telecoms);
        acheteurs.add(acheteur);
        structureAcheteur.setContactAcheteurs(acheteurs);
        structureAcheteur.setEntiteAdjudicatrice(Boolean.TRUE.equals(consultation.getEntiteAdjudicatrice()));
        structureAcheteur.setFormeJuridique("formeJuridique");
        return structureAcheteur;
    }
}
