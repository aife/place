package com.atexo.flink.domain.contrat;

import com.atexo.flink.config.AtexoJobException;
import com.atexo.flink.config.AtexoUtils;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.java.tuple.Tuple4;

import static com.atexo.flink.config.FlinkJobUtils.getErreur;
import static com.atexo.flink.config.FlinkJobUtils.setMDC;


@Slf4j
public class TncpContratsErreurAbandonSerializationSchema<T extends EvenementAtexo> implements SerializationSchema<Tuple4<String, T, AtexoJobException, TncpSuivi>> {

    @Override
    public byte[] serialize(Tuple4<String, T, AtexoJobException, TncpSuivi> element) {

        try {
            T f1 = element.f1;
            setMDC(f1);
            return AtexoUtils.getMapper().writeValueAsString(f1).getBytes();
        } catch (com.fasterxml.jackson.core.JsonProcessingException e) {
            log.error("Failed to parse JSON", e);
        }
        return new byte[0];
    }

    public T getMpeEnveloppe(Tuple4<String, T, AtexoJobException, TncpSuivi> element) {
        AtexoJobException ex = element.f2;
        T f1 = element.f1;
        if (ex != null) {
            f1.setMessageDetails(getErreur(ex, log));
            f1.setMessage(ex.getType() + " : " + ex.getMessage());
        }
        return f1;
    }
}
