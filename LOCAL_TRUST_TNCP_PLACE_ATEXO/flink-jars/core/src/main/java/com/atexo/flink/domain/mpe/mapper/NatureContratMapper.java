package com.atexo.flink.domain.mpe.mapper;

import com.atexo.flink.config.FlinkJobUtils;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeReferentiel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NatureContratMapper {
    private final Map<String, String> mpeTncpMappingValues;
    private final Map<String, String> tncpMpeMappingValues;

    public NatureContratMapper() {
        Map<String, String> mpeTncpMappingValues1;
        try {
            mpeTncpMappingValues1 = FlinkJobUtils.getMappingMpeTncp().get("type-contrat");
        } catch (IOException e) {
            mpeTncpMappingValues1 = new HashMap<>();
        }
        mpeTncpMappingValues = mpeTncpMappingValues1;
        Map<String, String> tncpMpeMappingValues1;
        try {
            tncpMpeMappingValues1 = FlinkJobUtils.getMappingTncpMpe().get("type-contrat");
        } catch (IOException e) {
            tncpMpeMappingValues1 = new HashMap<>();
        }
        tncpMpeMappingValues = tncpMpeMappingValues1;
    }

    public String mapToTncpNatureContrat(MpeReferentiel typeContrat) {
        if (typeContrat == null || typeContrat.getIdExterne() == null)
            return null;

        return mpeTncpMappingValues.get(typeContrat.getIdExterne());

    }

    public String mapToMpeNatureContrat(MpeWs mpeWs, String idExterne) {
        if (idExterne == null) return null;
        MpeReferentiel natureContratMpe = mpeWs.getReferentielsByTypeAndIdExterne("contrats", tncpMpeMappingValues.get(idExterne));
        return natureContratMpe == null ? null : natureContratMpe.getId();

    }
}
