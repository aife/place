package com.atexo.flink.job_mapper;

import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.EtapeEnum;
import com.atexo.flink.domain.commun.EvenementAtexo;
import com.atexo.flink.domain.commun.EvenementToTncpSuiviMapper;
import com.atexo.flink.domain.commun.StatutEnum;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.mpe.model.MpeFichier;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.atexo.flink.ws.tcnp.model.consultation.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

import java.io.File;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atexo.flink.config.AtexoUtils.getFileNameUploadTncp;
import static com.atexo.flink.config.AtexoUtils.getHashed;
import static com.atexo.flink.config.FlinkJobUtils.getTncpSuiviErreur;
import static java.time.ZoneOffset.UTC;

@Slf4j
public class ProcessTncpDceJobMapper extends RichMapFunction<Tuple3<String, EvenementAtexo, AtexoJobException>, Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> {
    private final JarFlinkConfiguration configuration;

    public ProcessTncpDceJobMapper(JarFlinkConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi> map(Tuple3<String, EvenementAtexo, AtexoJobException> in) throws Exception {
        EvenementAtexo evenementConsultation = in.f1;
        FlinkJobUtils.setMDC(evenementConsultation);
        TncpConsultation tncpConsultation;
        File resource = null;
        try {
            TncpSuivi tncpSuivi;
            if (configuration.isDceDesactive()) {
                evenementConsultation.setMessage("Evenement abandonné. L'upload des DCE est désactivé");
                tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
                tncpSuivi.setStatut(StatutEnum.FINI);
                return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
            }
            log.info("Execution job de modification de DCE pour la consultation id={}, referenceUnique={}", evenementConsultation.getIdObjetSource(), evenementConsultation.getIdObjetDestination());
            TncpWs tncpWs = AtexoUtils.getTncpWs(configuration);
            MpeWs mpeWs = AtexoUtils.getMpeWs(configuration);
            String token = tncpWs.getToken().getToken();
            tncpConsultation = tncpWs.getConsultation(token, evenementConsultation.getIdObjetDestination());
            if (tncpConsultation.getStructureAcheteur() == null || tncpConsultation.getStructureAcheteur().getSiret() == null) {
                throw new AtexoJobException("Le siret de la structure acheteur est obligatoire pour la validation", JobExeptionEnum.VALIDATION_CONSULTATION_TNCP);
            }
            List<PieceJointeConsultation> pieces = tncpConsultation.getPieceJointeConsultations();
            if (!CollectionUtils.isEmpty(pieces)) {
                for (PieceJointeConsultation piece : pieces) {
                    log.info("Suppression du fichier id={}", piece.getFileId());
                    tncpWs.deletePieceJointeConsultation(token, piece.getFileName(),
                            piece.getNomFonctionnelMetier(), tncpConsultation.getReferenceUnique(),
                            tncpConsultation.getStructureAcheteur().getSiret());
                }
            }

            //suite idem que dans le job publicationDCE = recuperation fichiers MPE + association a la consultation
            Map<String, List<MpeFichier>> fichiersMap = mpeWs.getFichiers(evenementConsultation.getIdObjetSource())
                    .stream().collect(Collectors.groupingBy(MpeFichier::getType));
            List<MpeFichier> fichiers = fichiersMap.values()
                    .stream().map(mpeFichiers -> mpeFichiers.stream()
                            .peek(mpeFichier -> {
                                if (mpeFichier.getDateModification() == null)
                                    mpeFichier.setDateModification(ZonedDateTime.now(UTC));
                            })
                            .sorted(Comparator.comparing(MpeFichier::getDateModification).reversed())
                            .collect(Collectors.toList()).get(0))
                    .collect(Collectors.toList());

            for (MpeFichier fichier : fichiers) {
                log.info("Download du fichier id={}", fichier.getId());
                resource = mpeWs.downloadFichier(fichier.getId());
                if (resource.exists()) {
                    if (fichier.getTaille() == null) {
                        long bytes = Files.size(resource.toPath());
                        fichier.setTaille(bytes);
                    }
                    String hashed = getHashed(resource.toPath());
                    log.info("Pre-upload du fichier id={}", fichier.getId());
                    String idTechnique = getFileNameUploadTncp(hashed);
                    ReponsePreUpload infoPreUpload = tncpWs.preUpload(token);
                    if (infoPreUpload != null) {
                        log.info("Upload du fichier id={}", fichier.getId());
                        ReponseUpload infoFichierTncp = tncpWs.uploadFile(infoPreUpload, resource, idTechnique);
                        if (infoFichierTncp == null) {
                            AtexoJobException atexoJobException = new AtexoJobException("Le upload du fichier id=" + fichier.getId() + " a echoue", JobExeptionEnum.UPLOAD_PLUS_10MO);
                            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
                        }
                        associerConsultationPieceJointe(infoFichierTncp.getDataFileId(), infoPreUpload.getVirtualFolderId(), fichier, tncpConsultation, evenementConsultation, tncpWs, token, hashed, infoFichierTncp.getDataFileVersionId(), idTechnique);

                    } else {
                        AtexoJobException atexoJobException = new AtexoJobException("Le pre-upload du fichier id=" + fichier.getId() + " a echoue", JobExeptionEnum.PRE_UPLOAD);
                        return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
                    }
                    log.info("Suppression du fichier temporaire");
                    Files.delete(resource.toPath());
                } else {
                    AtexoJobException atexoJobException = new AtexoJobException("Fichier d'id " + fichier.getId() + " non recupere", JobExeptionEnum.DOWNLOAD_FICHIER_MPE);
                    return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
                }

            }
            if (EtapeEnum.AJOUT_DCE.equals(evenementConsultation.getEtape())) {
                evenementConsultation.setEtape(EtapeEnum.VALIDATION_CONSULTATION);
            }

            tncpSuivi = EvenementToTncpSuiviMapper.INSTANCE.mapToSuiviTncp(evenementConsultation);
            tncpSuivi.setDateEnvoi(ZonedDateTime.now());

            tncpSuivi.setIdObjetDestination(evenementConsultation.getIdObjetDestination());
            if (EtapeEnum.VALIDATION_CONSULTATION.equals(evenementConsultation.getEtape())) {
                tncpSuivi.setStatut(StatutEnum.EN_COURS);
            } else {
                tncpSuivi.setStatut(StatutEnum.FINI);
            }


            return new Tuple4<>(in.f0, evenementConsultation, null, tncpSuivi);
        } catch (AtexoJobException e) {
            log.error("Erreur lors de l'execution job de publication de la consultation id={} => {}", evenementConsultation.getIdObjetSource(), e.getMessage());
            if (resource != null) {
                resource.deleteOnExit();
            }
            return new Tuple4<>(in.f0,  evenementConsultation, e, getTncpSuiviErreur( evenementConsultation, e, log));
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            AtexoJobException atexoJobException = new AtexoJobException("Erreur lors de l'execution job de publication de la consultation id=" + evenementConsultation.getIdObjetSource(), JobExeptionEnum.ERREUR_TECHNIQUE, e);
            return new Tuple4<>(in.f0, evenementConsultation, atexoJobException, getTncpSuiviErreur(evenementConsultation, atexoJobException, log));
        }
    }

    private void associerConsultationPieceJointe(String fileId, String folderId, MpeFichier mpeFichier, TncpConsultation tncpConsultation, EvenementAtexo evenementConsultation, TncpWs tncpWs, String token, String hash, String lasVersion, String fileNameUploadTncp) {
        PieceJointeConsultation pieceJointe = PieceJointeConsultation.builder()
                .fileId(fileId)
                .folderId(folderId)
                .fileName(fileNameUploadTncp)
                .nomFonctionnelMetier(mpeFichier.getName())
                .latestVersionId(lasVersion)
                .hashSha256(hash)
                .size(mpeFichier.getTaille())
                .build();
        switch (mpeFichier.getType()) {
            case "RC":
                pieceJointe.setTypeFichier("RC");
                break;
            case "DUME":
                pieceJointe.setTypeFichier("DUME");
                break;
            default:
                pieceJointe.setTypeFichier("AUTRE");
        }
        EnvoiPieceJointe envoi = EnvoiPieceJointe.builder()
                .piecesJointes(pieceJointe)
                .codeService(tncpConsultation.getStructureAcheteur().getCodeService())
                .referenceUnique(evenementConsultation.getIdObjetDestination())
                .siret(tncpConsultation.getStructureAcheteur().getSiret())
                .build();
        log.info("Ajout du fichier id={} a la consultation de referenceUnique={}", mpeFichier.getId(), evenementConsultation.getIdObjetDestination());
        tncpWs.addPieceJointeToConsultation(token, envoi);

    }

}
