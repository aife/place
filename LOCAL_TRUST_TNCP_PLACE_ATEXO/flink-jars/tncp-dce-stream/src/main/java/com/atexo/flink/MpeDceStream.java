package com.atexo.flink;


import com.atexo.flink.config.*;
import com.atexo.flink.domain.commun.*;
import com.atexo.flink.domain.mpe.TncpConsultationsEnAttenteSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsErreurAbandonSerializationSchema;
import com.atexo.flink.domain.mpe.TncpConsultationsSuiviSerializationSchema;
import com.atexo.flink.domain.mpe.process.DelayEvenementAtexoFromRetryProcess;
import com.atexo.flink.domain.tncp.model.TncpSuivi;
import com.atexo.flink.job_mapper.ProcessTncpDceJobMapper;
import com.atexo.flink.ws.mpe.MpeWs;
import com.atexo.flink.ws.tcnp.TncpWs;
import com.beust.jcommander.JCommander;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.springframework.web.client.RestTemplate;

import static com.atexo.flink.config.FlinkJobUtils.*;
import static com.atexo.flink.config.JobExeptionEnum.UPLOAD_DCE_DESACTIVE;


@Slf4j
public class MpeDceStream extends TncpStream<EvenementAtexo> {
    /**
     * Creates a job using the source and sink provided.
     *
     * @param waitingSource
     * @param errorSource
     * @param sucessSink
     * @param errorSink
     * @param abandonSink
     * @param tncpSuiviSink
     * @param waintingSink
     * @param mpeWs
     * @param tncpWs
     */
    public MpeDceStream(KafkaSource<String> waitingSource, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> sucessSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> errorSink, KafkaSink<String> abandonSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> tncpSuiviSink, KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waintingSink, MpeWs mpeWs, TncpWs tncpWs, JarFlinkConfiguration configuration) {
        super(waitingSource, sucessSink, errorSink, abandonSink, tncpSuiviSink, waintingSink, mpeWs, tncpWs, configuration);
    }

    /**
     * Main method.
     */
    public static void main(String[] args) {
        try {
            String jobName = "dce";
            var main = new JarFlinkConfiguration();
            JCommander.newBuilder().addObject(main).build().parse(JarFlinkConfiguration.log(args));

            MpeDceStream job = getTncpJob(main, jobName);
            job.execute(jobName);
        } catch (Exception | OutOfMemoryError e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }


    public JobExecutionResult execute(String... args) throws Exception {
        String jobName = args[0];
        StreamExecutionEnvironment env = getStreamExecutionEnvironment();

        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> evenementStream = getEvenementEnAttente(env, jobName);

        log.info("Conversion des evenements");
        KeyedStream<String, Object> noParsedStream = evenementStream
                .filter(value -> value.f2 != null)
                .name("Filtre sur les evenements consultations avec des donnees corrompues")
                .map(k -> k.f0)
                .name("Correspondance avec la source d'origine")
                .keyBy(value -> value);
        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, Object> validEvenement =
                evenementStream
                        .filter(value -> value.f1 != null
                                && InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux())
                                && (TypeEnum.MODIFICATION_DCE.equals(value.f1.getType()) || EtapeEnum.AJOUT_DCE.equals(value.f1.getEtape())))
                        .name("Filtre sur les evenements de consultation valides")
                        .keyBy(value -> value.f0);


        KeyedStream<Tuple3<String, EvenementAtexo, AtexoJobException>, String> tokenCheckedStream = validEvenement
                .keyBy(value -> value.f0)
                .process(new DelayEvenementAtexoFromRetryProcess())
                .name("Activer l'horloge pour les messages une future tentative")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, String> publicationDceStream = tokenCheckedStream
                .filter(value -> value.f1 != null &&
                        InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux()) &&
                        TypeEnum.PUBLICATION_CONSULTATION.equals(value.f1.getType()) &&
                        EtapeEnum.AJOUT_DCE.equals(value.f1.getEtape()))
                .map(new ProcessTncpDceJobMapper(configuration)).name("Ajout du DCE a la consultation TNCP")
                .keyBy(k -> k.f0);

        KeyedStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>, String> modificationDceStream = tokenCheckedStream
                .filter(value -> value.f1 != null
                        && InterfaceEnum.TNCP_SORTANTE.equals(value.f1.getFlux())
                        && TypeEnum.MODIFICATION_DCE.equals(value.f1.getType()))
                .map(new ProcessTncpDceJobMapper(configuration))
                .name("Modification du DCE")
                .keyBy(k -> k.f0);


        DataStream<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> union = publicationDceStream.union(modificationDceStream);


        //Send to error source
        union
                .filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() <= 120 && (value.f2 != null && (!UPLOAD_DCE_DESACTIVE.name().equals(value.f2.getType()))))
                .name("Filtre sur les evenements avec une tentative <= 2 heures")
                .sinkTo(errorSink).name("Aquittement dans la source en erreur pour un futur retry");

        SingleOutputStreamOperator<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> abandonAfterRetryStream = union
                .filter(value -> value.f3.getStatut().equals(StatutEnum.ERREUR) && value.f1.getRetry() > 120 || (value.f2 != null && (UPLOAD_DCE_DESACTIVE.name().equals(value.f2.getType()))))
                .name("Filtre sur les evenements avec une tentative > 2 heures");
        //Send to abandon source

        noParsedStream
                .union(abandonAfterRetryStream.map(value -> value.f0))
                .sinkTo(abandonSink).name("Aquittement dans la source d'abandon");
        //Send to success source
        union.filter(value -> value.f3.getStatut().equals(StatutEnum.FINI))
                .name("Filtre sur les evenements avec succes")
                .sinkTo(sucessSink).name("Aquittement dans la source en succes");
//Send to success source
        union.filter(value -> value.f3.getStatut().equals(StatutEnum.EN_COURS))
                .name("Filtre sur les evenements futures")
                .sinkTo(waitingSink).name("Aquittement dans la source pour traiter les lots");

        //Send to tncp
        union.sinkTo(tncpSuiviSink).name("Aquittement dans la source de suivi TNCP");


        JobConfig jobConfig = getJobConfig();
        return env.execute(jobConfig.getName() + " " + jobConfig.getVersion());
    }


    public static MpeDceStream getTncpJob(JarFlinkConfiguration configuration, String jobName) {
        log.info("Configuration du job : {} | config : {}", jobName, configuration);

        String waitingTopic = configuration.getKafkaWaitingTopic();
        String errorTopic = configuration.getKafkaErrorTopic();


        KafkaSource<String> source = getKafkaSource(getProperties(configuration, jobName, "waiting-source"), waitingTopic, errorTopic);

        TncpConsultationsSuiviSerializationSchema<EvenementAtexo> valueSerializationSchema = new TncpConsultationsSuiviSerializationSchema<>(configuration.getPlateforme());

        TncpConsultationsEnAttenteSerializationSchema<EvenementAtexo> valueSerializationSchemaAttente = new TncpConsultationsEnAttenteSerializationSchema<>();
        TncpConsultationsErreurAbandonSerializationSchema<EvenementAtexo> valueSerializationSchemaErreur = new TncpConsultationsErreurAbandonSerializationSchema<>();
        SuccessSerializationSchema<EvenementAtexo> valueSerializationSchemaSuccess = new SuccessSerializationSchema<>();

        FlinkService<EvenementAtexo> flinkService = new FlinkService<>();

        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> waitingSink = flinkService.getWaitingSinkEvenement(waitingTopic, getProperties(configuration, jobName, "waiting"), valueSerializationSchemaAttente);
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSucces = flinkService.getSuccessSinkEvenement(configuration.getKafkaSuccessTopic(), getProperties(configuration, jobName, "succes"), valueSerializationSchemaSuccess);
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicErreur = flinkService.getErreurAbandonSinkEvenement(configuration.getKafkaErrorTopic(), getProperties(configuration, jobName, "erreur"), valueSerializationSchemaErreur);
        KafkaSink<String> topicAbandon = flinkService.getSink(configuration.getKafkaAbandonTopic(), getProperties(configuration, jobName, "abandon"));
        KafkaSink<Tuple4<String, EvenementAtexo, AtexoJobException, TncpSuivi>> topicSuivi = flinkService.getSinkTncpSuivi(configuration.getKafkaSuiviTopic(), getProperties(configuration, jobName, "suivi"), valueSerializationSchema);


        RestTemplate restTemplate = new RestTemplateConfig().atexoRestTemplate();
        MpeWs mpeWs = new MpeWs(configuration, restTemplate);
        TncpWs tncpWs = new TncpWs(configuration, restTemplate);
        log.info("TNCP configuration {}", configuration);
        return new MpeDceStream(source, topicSucces, topicErreur, topicAbandon, topicSuivi, waitingSink, mpeWs, tncpWs, configuration);

    }

}
