#!/bin/sh
# The initial version
eval export $(cat .env)



# Fetch the list of jobs
JOBS=$(curl -s  -X GET "http://localhost:8081/jobs/overview")

# Check for jobs associated with the JAR artifact ID
echo $JOBS | jq -c '.jobs[]' | while IFS= read -r JOB; do
  JOB_NAME=$(echo $JOB | jq -r '.name')
  JOB_ID=$(echo $JOB | jq -r '.jid')

    echo "Stopping job $JOB_NAME with ID $JOB_ID..."

    STOP_RESPONSE=$(curl -s  -X PATCH "http://localhost:8081/jobs/$JOB_ID?mode=cancel")

    # Check response code
    if [ $? -eq 0 ]; then
      echo "Successfully stopped job $JOB_NAME with ID $JOB_ID"
    else
      echo "Failed to stop job $JOB_NAME with ID $JOB_ID"
      exit 1
    fi

done

echo 'Fetching all Flink configuration jars...'

# Perform the GET request using curl
JARS=$(curl -s -X GET "http://localhost:8081/jars")

# Check if the response contains valid data
if [ $? -eq 0 ]; then
  echo "Successfully fetched jars from Flink API server:"
  echo $JARS | jq '.'
else
  echo "Failed to fetch jars from Flink API server"
  exit 1
fi

 echo 'Deleting existing JARs...'

  # Loop through each jar and delete
  echo $JARS | jq -c '.files[]' | while IFS= read -r JAR; do
    JAR_ID=$(echo $JAR | jq -r '.id')
    JAR_NAME=$(echo $JAR | jq -r '.name')
    echo "Deleting JAR with ID $JAR_ID ($JAR_NAME)..."

    DELETE_RESPONSE=$(curl -s -X DELETE "http://localhost:8081/jars/$JAR_ID")

    # Check response code
    if [ $? -eq 0 ]; then
      echo "Successfully deleted JAR ID $JAR_ID"
    else
      echo "Failed to delete JAR ID $JAR_ID"
      exit 1
    fi
  done

echo 'Uploading tncp-consultation-stream.jar to Flink jobmanager...'
UPLOAD_RESPONSE=$(curl -s -X POST --location --form 'jarfile=@flink-jars/tncp-consultation-stream/target/tncp-consultation-stream.jar' http://localhost:8081/jars/upload)

if [ $? -eq 0 ]; then
  echo 'Upload Response Consultation: ' $UPLOAD_RESPONSE
else
  echo 'Failed to upload tncp-consultation-stream.jar'
  exit 1
fi

echo "Uploading tncp-lot-stream.jar to Flink jobmanager..."
UPLOAD_RESPONSE=$(curl -s -X POST --location --form 'jarfile=@flink-jars/tncp-lot-stream/target/tncp-lot-stream.jar' http://localhost:8081/jars/upload)

if [ $? -eq 0 ]; then
  echo 'Upload Response Lot: ' $UPLOAD_RESPONSE
else
  echo 'Failed to upload tncp-lot-stream.jar'
  exit 1
fi


echo "Uploading tncp-dce-stream.jar to Flink jobmanager..."
UPLOAD_RESPONSE=$(curl -s -X POST --location --form 'jarfile=@flink-jars/tncp-dce-stream/target/tncp-dce-stream.jar' http://localhost:8081/jars/upload)

if [ $? -eq 0 ]; then
  echo 'Upload Response DCE: ' $UPLOAD_RESPONSE
else
  echo 'Failed to upload tncp-dce-stream.jar'
  exit 1
fi

echo "Uploading tncp-contrat-stream.jar to Flink jobmanager..."
UPLOAD_RESPONSE=$(curl -s -X POST --location --form 'jarfile=@flink-jars/tncp-contrat-stream/target/tncp-contrat-stream.jar' http://localhost:8081/jars/upload)

if [ $? -eq 0 ]; then
  echo 'Upload Response Contrat: ' $UPLOAD_RESPONSE
else
  echo 'Failed to upload tncp-contrat-stream.jar'
  exit 1
fi

echo "Uploading tncp-notification-stream.jar to Flink jobmanager..."
UPLOAD_RESPONSE=$(curl -s -X POST --location --form 'jarfile=@flink-jars/tncp-notification-stream/target/tncp-notification-stream.jar' http://localhost:8081/jars/upload)

if [ $? -eq 0 ]; then
  echo 'Upload Response Notification: ' $UPLOAD_RESPONSE
else
  echo 'Failed to upload tncp-notification-stream.jar'
  exit 1
fi


# Perform the GET request using curl
JARS=$(curl -s -X GET "http://localhost:8081/jars")

# Check if the response contains valid data
if [ $? -eq 0 ]; then
  echo "Successfully fetched jars from Flink API server:"
  echo $JARS | jq '.'
else
  echo "Failed to fetch jars from Flink API server"
  exit 1
fi

# Loop through each jar and delete
  echo $JARS | jq -c '.files[]' | while IFS= read -r JAR; do
    JAR_ID=$(echo $JAR | jq -r '.id')
    JAR_NAME=$(echo $JAR | jq -r '.name')
    if echo "$JAR_NAME" | grep -q "tncp-consultation-stream"; then
                      NEW_JAR_ID=$JAR_ID

                      echo 'Starting job with JAR Consultation ID $NEW_JAR_ID...'
                      RUN_RESPONSE=$(curl -s -X POST http://localhost:8081/jars/$NEW_JAR_ID/run \
                           -H 'Content-Type: application/json' \
                           -d '{
                             "parallelism": "1",
                             "programArgsList": [
                               "--piste-url", "'"${TNCP_JOBS_PISTE_API}"'",
                               "--piste-client-id", "'"${TNCP_JOBS_PISTE_CLIENT_ID}"'",
                               "--piste-secret-id", "'"${TNCP_JOBS_PISTE_SECRET_ID}"'",
                               "--piste-auth-url", "'"${TNCP_JOBS_PISTE_AUTH_TOKEN}"'",
                               "--kafka-broker", "'"${TNCP_KAFKA_BROKER_BOOTSTRAP_SERVERS}"'",
                               "--kafka-waiting-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_WAITING}"'",
                               "--kafka-success-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUCCESS}"'",
                               "--kafka-error-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ERROR}"'",
                               "--kafka-abandon-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ABANDON}"'",
                               "--kafka-suivi-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUIVI}"'",
                               "--kafka-notifications-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_NOTIFICATIONS_SUIVI}"'",
                               "--kafka-group-id-prefix", "'"${TNCP_JOBS_KAFKA_GROUP_ID_PREFIX}"'",
                               "--kafka-client-id", "'"${ANSIBLE_HOSTNAME}-consultation"'",
                               "--mpe-url", "'"${TNCP_JOBS_MPE_URL}"'",
                               "--mpe-login", "'"${TNCP_JOBS_MPE_LOGIN}"'",
                               "--mpe-password", "'"${TNCP_JOBS_MPE_PASSWORD}"'",
                               "--mpe-uuid", "'"${TNCP_JOBS_MPE_UUID}"'",
                               "--configuration-checkpoints-folder", "file://'"${FLINK_CONFIGURATION_CHECKPOINT_STORAGE_DIR}"'/checkpoints",
                               "--configuration-storage-folder", "'"${FLINK_CONFIGURATION_STORAGE_DIR}"'",
                               "--configuration-plateforme", "'"${ANSIBLE_HOSTNAME}"'",
                               "--mpe-dce-desactive", "'"${TNCP_JOBS_DCE_DESACTIVE}"'"
                             ]
                           }')
                      echo 'Run Response: ' $RUN_RESPONSE
                      if([ $? -eq 0 ]); then
                        echo 'Job started for flink-jars/tncp-consultation-stream/target/tncp-consultation-stream.jar.'
                      else
                        echo 'Failed to start job for flink-jars/tncp-consultation-stream/target/tncp-consultation-stream.jar.'
                        exit 1
                      fi

    elif echo "$JAR_NAME" | grep -q "tncp-lot-stream"; then
                    NEW_JAR_ID=$JAR_ID

                    echo 'Starting job with JAR Lot ID $NEW_JAR_ID...'
                    RUN_RESPONSE=$(curl -s -X POST http://localhost:8081/jars/$NEW_JAR_ID/run \
                         -H 'Content-Type: application/json' \
                         -d '{
                           "parallelism": "1",
                           "programArgsList": [
                             "--piste-url", "'"${TNCP_JOBS_PISTE_API}"'",
                             "--piste-client-id", "'"${TNCP_JOBS_PISTE_CLIENT_ID}"'",
                             "--piste-secret-id", "'"${TNCP_JOBS_PISTE_SECRET_ID}"'",
                             "--piste-auth-url", "'"${TNCP_JOBS_PISTE_AUTH_TOKEN}"'",
                             "--kafka-broker", "'"${TNCP_KAFKA_BROKER_BOOTSTRAP_SERVERS}"'",
                             "--kafka-waiting-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_WAITING}"'",
                             "--kafka-success-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUCCESS}"'",
                             "--kafka-error-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ERROR}"'",
                             "--kafka-abandon-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ABANDON}"'",
                             "--kafka-suivi-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUIVI}"'",
                             "--kafka-notifications-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_NOTIFICATIONS_SUIVI}"'",
                             "--kafka-group-id-prefix", "'"${TNCP_JOBS_KAFKA_GROUP_ID_PREFIX}"'",
                             "--kafka-client-id", "'"${ANSIBLE_HOSTNAME}-consultation"'",
                             "--mpe-url", "'"${TNCP_JOBS_MPE_URL}"'",
                             "--mpe-login", "'"${TNCP_JOBS_MPE_LOGIN}"'",
                             "--mpe-password", "'"${TNCP_JOBS_MPE_PASSWORD}"'",
                             "--mpe-uuid", "'"${TNCP_JOBS_MPE_UUID}"'",
                             "--configuration-checkpoints-folder", "file://'"${FLINK_CONFIGURATION_CHECKPOINT_STORAGE_DIR}"'/checkpoints",
                             "--configuration-storage-folder", "'"${FLINK_CONFIGURATION_STORAGE_DIR}"'",
                             "--configuration-plateforme", "'"${ANSIBLE_HOSTNAME}"'",
                             "--mpe-dce-desactive", "'"${TNCP_JOBS_DCE_DESACTIVE}"'"
                           ]
                         }')
                    echo 'Run Response: ' $RUN_RESPONSE
                    if([ $? -eq 0 ]); then
                      echo 'Job started for flink-jars/tncp-lot-stream/target/tncp-lot-stream.jar.'
                    else
                      echo 'Failed to start job for flink-jars/tncp-lot-stream/target/tncp-lot-stream.jar.'
                      exit 1
                    fi
    elif echo "$JAR_NAME" | grep -q "tncp-dce-stream"; then
                NEW_JAR_ID=$JAR_ID

                echo 'Starting job with JAR DCE ID $NEW_JAR_ID...'
                RUN_RESPONSE=$(curl -s -X POST http://localhost:8081/jars/$NEW_JAR_ID/run \
                     -H 'Content-Type: application/json' \
                     -d '{
                       "parallelism": "1",
                       "programArgsList": [
                         "--piste-url", "'"${TNCP_JOBS_PISTE_API}"'",
                         "--piste-client-id", "'"${TNCP_JOBS_PISTE_CLIENT_ID}"'",
                         "--piste-secret-id", "'"${TNCP_JOBS_PISTE_SECRET_ID}"'",
                         "--piste-auth-url", "'"${TNCP_JOBS_PISTE_AUTH_TOKEN}"'",
                         "--kafka-broker", "'"${TNCP_KAFKA_BROKER_BOOTSTRAP_SERVERS}"'",
                         "--kafka-waiting-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_WAITING}"'",
                         "--kafka-success-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUCCESS}"'",
                         "--kafka-error-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ERROR}"'",
                         "--kafka-abandon-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_ABANDON}"'",
                         "--kafka-suivi-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONSULTATIONS_SUIVI}"'",
                         "--kafka-notifications-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_NOTIFICATIONS_SUIVI}"'",
                         "--kafka-group-id-prefix", "'"${TNCP_JOBS_KAFKA_GROUP_ID_PREFIX}"'",
                         "--kafka-client-id", "'"${ANSIBLE_HOSTNAME}-consultation"'",
                         "--mpe-url", "'"${TNCP_JOBS_MPE_URL}"'",
                         "--mpe-login", "'"${TNCP_JOBS_MPE_LOGIN}"'",
                         "--mpe-password", "'"${TNCP_JOBS_MPE_PASSWORD}"'",
                         "--mpe-uuid", "'"${TNCP_JOBS_MPE_UUID}"'",
                         "--configuration-checkpoints-folder", "file://'"${FLINK_CONFIGURATION_CHECKPOINT_STORAGE_DIR}"'/checkpoints",
                         "--configuration-storage-folder", "'"${FLINK_CONFIGURATION_STORAGE_DIR}"'",
                         "--configuration-plateforme", "'"${ANSIBLE_HOSTNAME}"'",
                         "--mpe-dce-desactive", "'"${TNCP_JOBS_DCE_DESACTIVE}"'"
                       ]
                     }')
                echo 'Run Response: ' $RUN_RESPONSE
                if([ $? -eq 0 ]); then
                  echo 'Job started for flink-jars/tncp-dce-stream/target/tncp-dce-stream.jar.'
                else
                  echo 'Failed to start job for flink-jars/tncp-dce-stream/target/tncp-dce-stream.jar.'
                  exit 1
                fi
    elif echo "$JAR_NAME" | grep -q "tncp-contrat-stream"; then
                      NEW_JAR_ID=$JAR_ID

                echo 'Starting job with JAR Contrat ID $NEW_JAR_ID...'
                RUN_RESPONSE=$(curl -s -X POST http://localhost:8081/jars/$NEW_JAR_ID/run \
                     -H 'Content-Type: application/json' \
                     -d '{
                       "parallelism": "1",
                       "programArgsList": [
                         "--piste-url", "'"${TNCP_JOBS_PISTE_API}"'",
                         "--piste-client-id", "'"${TNCP_JOBS_PISTE_CLIENT_ID}"'",
                         "--piste-secret-id", "'"${TNCP_JOBS_PISTE_SECRET_ID}"'",
                         "--piste-auth-url", "'"${TNCP_JOBS_PISTE_AUTH_TOKEN}"'",
                         "--kafka-broker", "'"${TNCP_KAFKA_BROKER_BOOTSTRAP_SERVERS}"'",
                         "--kafka-waiting-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONTRATS_WAITING}"'",
                         "--kafka-success-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONTRATS_SUCCESS}"'",
                         "--kafka-error-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONTRATS_ERROR}"'",
                         "--kafka-abandon-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONTRATS_ABANDON}"'",
                         "--kafka-suivi-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_CONTRATS_SUIVI}"'",
                         "--kafka-group-id-prefix", "'"${TNCP_JOBS_KAFKA_GROUP_ID_PREFIX}"'",
                         "--kafka-client-id", "'"${ANSIBLE_HOSTNAME}-contrat"'",
                         "--mpe-url", "'"${TNCP_JOBS_MPE_URL}"'",
                         "--mpe-login", "'"${TNCP_JOBS_MPE_LOGIN}"'",
                         "--mpe-password", "'"${TNCP_JOBS_MPE_PASSWORD}"'",
                         "--mpe-uuid", "'"${TNCP_JOBS_MPE_UUID}"'",
                         "--mpe-dce-desactive", "'"${TNCP_JOBS_DCE_DESACTIVE}"'",
                         "--configuration-checkpoints-folder", "file://'"${FLINK_CONFIGURATION_CHECKPOINT_STORAGE_DIR}"'/checkpoints",
                         "--configuration-storage-folder", "'"${FLINK_CONFIGURATION_STORAGE_DIR}"'",
                         "--configuration-plateforme", "'"${ANSIBLE_HOSTNAME}"'"
                       ]
                     }')

                        echo 'Run Response: ' $RUN_RESPONSE
                        if([ $? -eq 0 ]); then
                          echo 'Job started for flink-jars/tncp-contrat-stream/target/tncp-contrat-stream.jar.'
                        else
                          echo 'Failed to start job for flink-jars/tncp-contrat-stream/target/tncp-contrat-stream.jar.'
                          exit 1
                        fi

    elif echo "$JAR_NAME" | grep -q "tncp-notification-stream"; then
                        NEW_JAR_ID=$JAR_ID

                  echo 'Starting job with JAR Notification ID $NEW_JAR_ID...'
                  RUN_RESPONSE=$(curl -s -X POST http://localhost:8081/jars/$NEW_JAR_ID/run \
                       -H 'Content-Type: application/json' \
                       -d '{
                         "parallelism": "1",
                         "programArgsList": [
                           "--piste-url", "'"${TNCP_JOBS_PISTE_API}"'",
                           "--piste-client-id", "'"${TNCP_JOBS_PISTE_CLIENT_ID}"'",
                           "--piste-secret-id", "'"${TNCP_JOBS_PISTE_SECRET_ID}"'",
                           "--piste-auth-url", "'"${TNCP_JOBS_PISTE_AUTH_TOKEN}"'",
                           "--kafka-broker", "'"${TNCP_KAFKA_BROKER_BOOTSTRAP_SERVERS}"'",
                           "--kafka-waiting-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_NOTIFICATIONS_WAITING}"'",
                           "--kafka-suivi-topic", "'"${TNCP_JOBS_KAFKA_TOPIC_NOTIFICATIONS_SUIVI}"'",
                           "--kafka-group-id-prefix", "'"${TNCP_JOBS_KAFKA_GROUP_ID_PREFIX}"'",
                           "--kafka-client-id", "'"${ANSIBLE_HOSTNAME}-contrat"'",
                           "--mpe-url", "'"${TNCP_JOBS_MPE_URL}"'",
                           "--mpe-login", "'"${TNCP_JOBS_MPE_LOGIN}"'",
                           "--mpe-password", "'"${TNCP_JOBS_MPE_PASSWORD}"'",
                           "--mpe-uuid", "'"${TNCP_JOBS_MPE_UUID}"'",
                           "--mpe-dce-desactive", "'"${TNCP_JOBS_DCE_DESACTIVE}"'",
                           "--configuration-checkpoints-folder", "file://'"${FLINK_CONFIGURATION_CHECKPOINT_STORAGE_DIR}"'/checkpoints",
                           "--configuration-storage-folder", "'"${FLINK_CONFIGURATION_STORAGE_DIR}"'",
                           "--configuration-plateforme", "'"${ANSIBLE_HOSTNAME}"'"
                         ]
                       }')
                        echo 'Run Response: ' $RUN_RESPONSE
                        if([ $? -eq 0 ]); then
                          echo 'Job started for flink-jars/tncp-notification-stream/target/tncp-notification-stream.jar.'
                        else
                          echo 'Failed to start job for flink-jars/tncp-notification-stream/target/tncp-notification-stream.jar.'
                          exit 1
                          fi
    fi
  done

