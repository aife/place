package fr.paris.epm.noyau.metier.objetvaleur;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * Doublet (identifiant, libellé).
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class Doublet implements Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Libellé.
     */
    private String libelle;

    /**
     * Vide.
     */
    public Doublet() {
    }

    /**
     * @param idD identifiant
     * @param libelleD libellé
     */
    public Doublet(final int idD, final String libelleD) {
        id = idD;
        libelle = libelleD;
    }


    // Accesseurs
    /**
     * @return identifiant
     */
    public final int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return libellé
     */
    public final String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé
     */
    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        if (obj instanceof Doublet) {
            Doublet doublet = (Doublet)obj;
            if (this.id == doublet.getId() && this.libelle.equals(doublet.getLibelle())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).append(this.libelle).toHashCode();
    }

}
