package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine analyseDepouillementPlis .propositionClassementOffre.critereCandidat(ParLots).critereNoteOffre.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCritereNoteOffre implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    private String nomCritere;

    private String noteCandidatCritere;

    private Double ponderationCandidatCritere;

    private String notePondereeCandidatCritere;

    private String commentaireCandidatCritere = "";

    private List<DomaineCritereNoteOffre> sousCriteres = Collections.emptyList();

    public final String getNomCritere() {
        return nomCritere;
    }

    public final void setNomCritere(final String valeur) {
        this.nomCritere = valeur;
    }

    public final String getNoteCandidatCritere() {
        return noteCandidatCritere;
    }

    public final void setNoteCandidatCritere(final String valeur) {
        this.noteCandidatCritere = valeur;
    }

    public final String getNotePondereeCandidatCritere() {
        return notePondereeCandidatCritere;
    }

    public final void setNotePondereeCandidatCritere(final String valeur) {
        this.notePondereeCandidatCritere = valeur;
    }

    public final String getCommentaireCandidatCritere() {
        return commentaireCandidatCritere;
    }

    public final void setCommentaireCandidatCritere(final String valeur) {
        this.commentaireCandidatCritere = valeur;
    }

    public final Double getPonderationCandidatCritere() {
        return ponderationCandidatCritere;
    }

    public void setPonderationCandidatCritere(final Double valeur) {
        this.ponderationCandidatCritere = valeur;
    }

    public List<DomaineCritereNoteOffre> getSousCriteres() {
        return sousCriteres;
    }

    public void setSousCriteres(List<DomaineCritereNoteOffre> sousCriteres) {
        this.sousCriteres = sousCriteres;
    }
}
