package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeMarche de la table "epm__t_ref_type_marche"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeMarche extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private boolean simplifie;

    public boolean isSimplifie() {
        return simplifie;
    }

    public void setSimplifie(boolean simplifie) {
        this.simplifie = simplifie;
    }

}