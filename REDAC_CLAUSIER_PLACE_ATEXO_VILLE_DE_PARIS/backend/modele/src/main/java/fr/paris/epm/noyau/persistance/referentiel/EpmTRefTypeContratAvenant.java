package fr.paris.epm.noyau.persistance.referentiel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO EpmTRefTypeContrat de la table "epm__t_ref_type_contrat"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeContratAvenant extends BaseEpmTRefReferentiel {

    private static final Logger LOG = LoggerFactory.getLogger(EpmTRefTypeContratAvenant.class);
    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int MARCHE = 1;
    public static final int ACCORD_CADRE = 2;
    public static final int DSP = 3;

    /**
     * Type de procedure que l'on associe à l'avenant (utile pour l'avenant)
     */
    public EpmTRefProcedure epmTRefProcedure;

    @Override
    public void setCodeExterne(String codeExterne) {
        // ajouter "code_externe" dans la base de données s'il faut l'avoir
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"code_externe\" dans la base de données s'il faut l'avoir");
    }

    public EpmTRefProcedure getEpmTRefProcedure() {
        return epmTRefProcedure;
    }

    public void setEpmTRefProcedure(EpmTRefProcedure epmTRefProcedure) {
        this.epmTRefProcedure = epmTRefProcedure;
    }

}
