package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by qba on 11/09/17.
 */
@Entity
@Table(name = "epm__t_template_version", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTTemplateVersion extends EpmTAbstractObject {

    @Id
    @Column(name = "id_version")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_page_garde")
    private Integer idPageGarde;

    @Column(name = "id_type_document")
    private Integer idTypeDocument;

    @Column(name = "id_intervention", nullable = false)
    private Integer idIntervention;

    @Column(name = "id_representant")
    private Integer idRepresentant;

    @Column(name = "nom_fichier", nullable = false)
    private String nomFichier;

    @Column(name = "date_creation", nullable = false)
    private Timestamp dateCreation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getIdPageGarde() {
        return idPageGarde;
    }

    public void setIdPageGarde(Integer idPageGarde) {
        this.idPageGarde = idPageGarde;
    }

    public Integer getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(Integer idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public Integer getIdIntervention() {
        return idIntervention;
    }

    public void setIdIntervention(Integer idIntervention) {
        this.idIntervention = idIntervention;
    }

    public Integer getIdRepresentant() {
        return idRepresentant;
    }

    public void setIdRepresentant(Integer idRepresentant) {
        this.idRepresentant = idRepresentant;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public Timestamp getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Timestamp dateCreation) {
        this.dateCreation = dateCreation;
    }

}
