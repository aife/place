package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefBeneficiaireMarche de la table "epm__t_ref_beneficiaire_marche"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefBeneficiaireMarche extends EpmTReferentielSimpleAbstract {

    /**
     * Identifiant de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Indique si le beneficiaire du marche implique un numero de contrat.
     */
    private boolean numeroContrat;

    /**
     * Indique si le beneficiaire du marche fait partie de l'ensemble des
     * beneficiaire dont au moins un element doit etre coche.
     */
    private boolean obligatoire;

    /**
     * Code du contrat qui remplace le code de la direction service
     */
    private String codeContrat;

    private EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur;

    public boolean isNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(boolean numeroContrat) {
        this.numeroContrat = numeroContrat;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    public String getCodeContrat() {
        return codeContrat;
    }

    public void setCodeContrat(String codeContrat) {
        this.codeContrat = codeContrat;
    }

    public EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur() {
        return epmTRefPouvoirAdjudicateur;
    }

    public void setEpmTRefPouvoirAdjudicateur(EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur) {
        this.epmTRefPouvoirAdjudicateur = epmTRefPouvoirAdjudicateur;
    }

}