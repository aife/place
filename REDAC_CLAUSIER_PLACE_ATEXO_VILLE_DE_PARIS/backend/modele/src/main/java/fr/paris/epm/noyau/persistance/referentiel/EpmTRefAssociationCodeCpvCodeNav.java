package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;

/**
 * Pojo hibernate association entre code cpv et code nav (n:1).
 * @author Rémi Villé
 * @version 2010/04/06 12:08
 */
public class EpmTRefAssociationCodeCpvCodeNav implements EpmTObject {

    /**
	 * Identifiant de serialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'identifiant en base
	 */
	private int id;

	/**
	 * Le code CPV
	 */
	private String codeCpv;
	
	/**
	 * Le libelle du code CPV
	 */
	private String libelleCodeCpv;
	
	/**
	 * Le code NAV associe
	 */
    private EpmTRefCodeAchat codeAchat;

	/**
     * @return hash code
     */
    public final int hashCode() {
        int result = 1;

        if (codeCpv != null) {
            result += Constantes.PREMIER * result + codeCpv.hashCode();
        }
        if (libelleCodeCpv != null) {
            result += Constantes.PREMIER * result + libelleCodeCpv.hashCode();
        }
        return result;
    }

    /**
     * Comparaison sur le code CPV
     * 
     * @param obj objet à compararer
     * @return boolean
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EpmTRefAssociationCodeCpvCodeNav autre = (EpmTRefAssociationCodeCpvCodeNav) obj;

        if (codeCpv == null) {
            if (autre.codeCpv != null) {
                return false;
            }
        } else if (!codeCpv.equals(autre.codeCpv)) {
            return false;
        }
        return true;
    }

    /**
     * Cette méthode affiche le contenu de l'objet.
     * @return String
     */
    public final String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nIdentifiant: ");
        res.append(id);
        res.append("\ncode: ");
        res.append(codeCpv);
        res.append("\nLibelléCpv: ");
        res.append(libelleCodeCpv);
        return res.toString();
    }

    /**
     * Comparaison par rapport au libellé. Gère "TOUS" en identifiant 1.
     * @param obj objet à comparer
     * @return int
     */
    public final int compareTo(final EpmTRef obj) {
        // L'objet d'identifiant 1 est toujours en premier
        if (id == 0 && ((EpmTRefAssociationCodeCpvCodeNav) obj).id == 0) {
            return 0;
        }
        if (id == 0) {
            return -1;
        }
        if (((EpmTRefAssociationCodeCpvCodeNav) obj).id == 1) {
            return 1;
        }

        return libelleCodeCpv.compareTo(((EpmTRefAssociationCodeCpvCodeNav) obj).libelleCodeCpv);
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodeCpv() {
		return codeCpv;
	}

	public void setCodeCpv(String codeCpv) {
		this.codeCpv = codeCpv;
	}

	public String getLibelleCodeCpv() {
		return libelleCodeCpv;
	}

	public void setLibelleCodeCpv(String libelleCodeCpv) {
		this.libelleCodeCpv = libelleCodeCpv;
	}

	public EpmTRefCodeAchat getCodeAchat() {
		return codeAchat;
	}

	public void setCodeAchat(EpmTRefCodeAchat codeAchat) {
		this.codeAchat = codeAchat;
	}

}
