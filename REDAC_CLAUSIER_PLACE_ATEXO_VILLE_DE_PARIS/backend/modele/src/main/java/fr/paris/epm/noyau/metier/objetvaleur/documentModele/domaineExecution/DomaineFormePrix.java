package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine Forme de
 * prix dans un contrat ou dans une tranche
 * 
 * @author GAO Xuesong
 */
public class DomaineFormePrix implements Serializable {
	
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * La libellé selon le type de forme de prix 
     */
    private String formePrix;
    
    /**
     * Montant de la forme de prix
     */
    private Double montant;
    
    
	public String getFormePrix() {
		return formePrix;
	}

	public final void setFormePrix(final String valeur) {
		this.formePrix = valeur;
	}

	public final Double getMontant() {
		return montant;
	}

	public final void setMontant(final Double valeur) {
		this.montant = valeur;
	}
    
    
    
	
	
	
}
