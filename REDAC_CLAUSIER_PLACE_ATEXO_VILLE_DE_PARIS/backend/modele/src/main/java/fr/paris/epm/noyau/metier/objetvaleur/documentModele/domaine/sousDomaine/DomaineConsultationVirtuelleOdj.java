package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données "consultations virtuelles" à fusionner dans un document pour un element du sous
 * domaine commission.domaineOrdreDuJour.listeConsultationVirtuelleOdj.
 * @author Marouane Gazanayi
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineConsultationVirtuelleOdj implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Le numéro de la consultation
     */
    private String numConsultation;
    /**
     * La direction-service
     */
    private String libelleDirectionService;
    /**
     * Intitulé de la consultation
     */
    private String intituleConsultation;
    /**
     * Objet de la consultation
     */
    private String objetConsultation;
    /**
     * Intitulé du lot
     */
    private String intituleLot;
    /**
     * Tableau des parties forfaitaires ou montant nominal
     */
    private Double pfMn;
    /**
     * Tableau des détails Quantitatifs Dénominatifs
     */
    private Double dqe;
    /**
     * Tableau des bons de commande min
     */
    private Double bcMin;
    /**
     * Tableau des bon de commande max
     */
    private Double bcMax;
    /**
     * @return the numConsultation
     */
    public final String getNumConsultation() {
        return numConsultation;
    }
    /**
     * @param numConsultation the numConsultation to set
     */
    public final void setNumConsultation(final String valeur) {
        this.numConsultation = valeur;
    }
    /**
     * @return the libelleDirectionService
     */
    public final String getLibelleDirectionService() {
        return libelleDirectionService;
    }
    /**
     * @param libelleDirectionService the libelleDirectionService to set
     */
    public final void setLibelleDirectionService(final String valeur) {
        this.libelleDirectionService = valeur;
    }
    /**
     * @return the intituleConsultation
     */
    public final String getIntituleConsultation() {
        return intituleConsultation;
    }
    /**
     * @param intituleConsultation the intituleConsultation to set
     */
    public final void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }
    /**
     * @return the objetConsultation
     */
    public final String getObjetConsultation() {
        return objetConsultation;
    }
    /**
     * @param objetConsultation the objetConsultation to set
     */
    public final void setObjetConsultation(final String valeur) {
        this.objetConsultation = valeur;
    }
    /**
     * @return the intituleLot
     */
    public final String getIntituleLot() {
        return intituleLot;
    }
    /**
     * @param intituleLot the intituleLot to set
     */
    public final void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }
    /**
     * @return the pfMn
     */
    public final Double getPfMn() {
        return pfMn;
    }
    /**
     * @param pfMn the pfMn to set
     */
    public final void setPfMn(final Double valeur) {
        this.pfMn = valeur;
    }
    /**
     * @return the dqe
     */
    public final Double getDqe() {
        return dqe;
    }
    /**
     * @param dqe the dqe to set
     */
    public final void setDqe(final Double valeur) {
        this.dqe = valeur;
    }
    /**
     * @return the bcMin
     */
    public final Double getBcMin() {
        return bcMin;
    }
    /**
     * @param bcMin the bcMin to set
     */
    public final void setBcMin(final Double valeur) {
        this.bcMin = valeur;
    }
    /**
     * @return the bcMax
     */
    public final Double getBcMax() {
        return bcMax;
    }
    /**
     * @param bcMax the bcMax to set
     */
    public final void setBcMax(final Double valeur) {
        this.bcMax = valeur;
    }

}
