package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefEtatAttestation de la table "epm__t_ref_etat_attestation"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefEtatAttestation extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int COMPLET = 1; // statut Complet

}