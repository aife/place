package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefReservationLot de la table "epm__t_ref_reservation_lot"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefReservationLot extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_ATELIER_PROTEGE = 1; // Id en base de donnée de 'Atelier protégé'.
    public static final int ID_EMPLOI_PROTEGE = 2; // Id en base de donnée de 'Emploi protégé'.

}