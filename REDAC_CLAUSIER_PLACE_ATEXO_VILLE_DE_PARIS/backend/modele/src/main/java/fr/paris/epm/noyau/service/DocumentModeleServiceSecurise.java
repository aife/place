package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.persistance.EpmTConsultation;

import java.util.Map;

/**
 * Interface pour la gestion des modéles de documents offrant des méthodes
 * spécifiques à l'ajout / modification de documents par l'utilisateur.
 *
 * @author Léon Barsamian
 */
public interface DocumentModeleServiceSecurise extends GeneriqueServiceSecurise {


    String genererFreeMarker(final Map<String, Object> freeMarkerMap, final String xml, final EpmTConsultation idConsultation, final int idLot);


}
