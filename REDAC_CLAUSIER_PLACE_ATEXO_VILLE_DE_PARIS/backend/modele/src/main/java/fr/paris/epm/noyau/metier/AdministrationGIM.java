/**
 * $Id$
 */
package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;

/**
 * Gestionnaire d'information métier administration.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface AdministrationGIM extends BaseGIM {


    EpmTUtilisateur chargerUtilisateur(int identifiant);

    void supprimerAuthentificationToken(String username);

    EpmTAuthentificationToken modifierAuthentificationToken(EpmTAuthentificationToken authToken);


}
