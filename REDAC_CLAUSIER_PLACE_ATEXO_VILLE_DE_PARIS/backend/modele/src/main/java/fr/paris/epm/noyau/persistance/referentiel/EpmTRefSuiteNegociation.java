package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefSuiteNegociation de la table "epm__t_ref_suite_negociation"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefSuiteNegociation extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_ADMIS_A_POURSUIVRE_NEGO = 1;
    public static final int ID_NON_ADMIS_A_POURSUIVRE_NEGO = 2;
    public static final int ID_REDUCTION_NBRE_CANDIDAT = 3;
    public static final int ID_INAPPROPRIE = 4;
    public static final int ID_IRREGULIERE = 5;
    public static final int ID_INACCEPTABLE = 6;

    /**
     * True si c'est une appréciation (reduction, inacceptale, irrecevale, inaproprié)
     * false sinon (admis à poursuivre, non admis).
     */
    private boolean appreciation;

    public boolean isAppreciation() {
        return appreciation;
    }

    public void setAppreciation(boolean appreciation) {
        this.appreciation = appreciation;
    }

}