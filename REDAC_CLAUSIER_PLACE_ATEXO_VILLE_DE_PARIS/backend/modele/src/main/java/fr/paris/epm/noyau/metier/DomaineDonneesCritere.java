/**
 * 
 */
package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Léon Barsamian
 *
 */
public class DomaineDonneesCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Serialisation
     */
    private static final long serialVersionUID = -5597266105998354990L;

    
    /**
     * Identifiant du domaine de données recherché.
     */
    private Integer id;
    
    private String libelle;

    private String methode;
    
	private Boolean moduleExecution = null;
    
    /**
     * Construction du corps de la requette.
     */
    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;
        Map parametres = getParametres();
        
        sb.append("from EpmTRefDomaineDonnees d");
        if (id != null) {
            sb.append(" where d.id = :id");
            parametres.put("id", id);
            debut = false;
        }
        if (libelle != null) {
            if (debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" d.libelle = :libelle");
            parametres.put("libelle", libelle);
            debut = false;
        }
        if (methode != null) {
            if (debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" d.methode = :methode");
            parametres.put("methode", methode);
            debut = false;
        }
        
        if(moduleExecution != null) {
            if(moduleExecution) {
            	if (debut) {
                    sb.append(" where ");
                } else {
                    sb.append(" and ");
                }
                sb.append(" d.moduleExecution = TRUE");
                debut = false;
            } else {
            	if (debut) {
                    sb.append(" where ");
                } else {
                    sb.append(" and ");
                }
                sb.append(" d.moduleExecution = FALSE");
                debut = false;
            }        
        }
        
        return sb;
    }

    /**
     * Construction de la requette pour obtenir le nombre de résultat total.
     */
    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(*) ");
        return select.append(corpsRequete()).toString();
    }

    /**
     * Construction de la requette pour obtenir la liste des résultats.
     */
    public String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by d.");
            sb.append(proprieteTriee);
        }
        return sb.toString();
    }

    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    /**
     * @param methode the methode to set
     */
    public final void setMethode(final String valeur) {
        this.methode = valeur;
    }

	public final void setModuleExecution(final boolean valeur) {
		this.moduleExecution = valeur;
	}
}
