package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine contractant(s)
 * @author GAO Xuesong
 */
public class DomaineContractant implements Serializable {
	
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Raison sociale du contractant
	 */
	private String raisonSocialeContractant;
	
	/**
	 * Rôle juridique du contractant
	 */
	private String roleJuridique;
	
	/**
	 * Rôle opérationnel du contractant
	 */
	private String roleOperationnel;
	
	/**
	 * Le contractant est-il mandataire ou non 
	 */
	private Boolean contractantMandataire;
	
	/**
	 * Le contractant est-il mandataire solidaire ou non 
	 */
	private Boolean contractantSolidaire;
	
	/**
	 * Montant HT de prestation du contractant
	 */
	private Double montantPrestation;
	
	/**
	 * SIREN du contractant
	 */
	private String sirenContractant;
	
	/**
	 * Code établissement du contractant
	 */
	private String codeEtabContractant;
	
	/**
	 * Adresse du contractant
	 */
	private String adresseContractant;
	
	/**
	 * Code postal du contractant
	 */
	private String codePostalContractant;
	
	/**
	 * Ville du contractant
	 */
	private String villeContractant;
	
	/**
	 * Téléphone du contractant
	 */
	private String telephoneContractant;

	
	public final String getRaisonSocialeContractant() {
		return raisonSocialeContractant;
	}

	public final void setRaisonSocialeContractant(final String valeur) {
		this.raisonSocialeContractant = valeur;
	}

	public final String getRoleJuridique() {
		return roleJuridique;
	}

	public final void setRoleJuridique(final String valeur) {
		this.roleJuridique = valeur;
	}

	public final String getRoleOperationnel() {
		return roleOperationnel;
	}

	public final void setRoleOperationnel(final String valeur) {
		this.roleOperationnel = valeur;
	}

	public final Double getMontantPrestation() {
		return montantPrestation;
	}

	public final void setMontantPrestation(final Double valeur) {
		this.montantPrestation = valeur;
	}

	public final String getSirenContractant() {
		return sirenContractant;
	}

	public final void setSirenContractant(final String valeur) {
		this.sirenContractant = valeur;
	}

	public final String getCodeEtabContractant() {
		return codeEtabContractant;
	}

	public final void setCodeEtabContractant(final String valeur) {
		this.codeEtabContractant = valeur;
	}

	public final String getAdresseContractant() {
		return adresseContractant;
	}

	public final void setAdresseContractant(final String valeur) {
		this.adresseContractant = valeur;
	}

	public final String getCodePostalContractant() {
		return codePostalContractant;
	}

	public final void setCodePostalContractant(final String valeur) {
		this.codePostalContractant = valeur;
	}

	public final String getVilleContractant() {
		return villeContractant;
	}

	public final void setVilleContractant(final String valeur) {
		this.villeContractant = valeur;
	}

	public final String getTelephoneContractant() {
		return telephoneContractant;
	}

	public final void setTelephoneContractant(final String valeur) {
		this.telephoneContractant = valeur;
	}

	public Boolean getContractantMandataire() {
		return contractantMandataire;
	}

	public void setContractantMandataire(Boolean valeur) {
		this.contractantMandataire = valeur;
	}

	public Boolean getContractantSolidaire() {
		return contractantSolidaire;
	}

	public void setContractantSolidaire(Boolean valeur) {
		this.contractantSolidaire = valeur;
	}
	
}
