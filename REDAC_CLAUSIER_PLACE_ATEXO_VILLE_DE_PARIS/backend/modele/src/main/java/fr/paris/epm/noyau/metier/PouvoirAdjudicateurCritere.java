package fr.paris.epm.noyau.metier;

import java.io.Serializable;

/**
 * Recherche de pouvoir adjudicateur.
 * @author RVI
 */
public class PouvoirAdjudicateurCritere extends AbstractCritere implements Critere, Serializable {

    private static final long serialVersionUID = -4792409134564167399L;

    private Integer id;

    private String libelle;

    private String libelleCourt;

    private String codeExterne;
    
    private String codeGo;

    /**
     * true si pouvoir adjudicateur utilisé pour reunion et instance.
     */
    private Boolean commission;

    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        sb.append(" from EpmTRefPouvoirAdjudicateur p ");
        if (id != null) {
            sb.append(" where id = :id ");
            getParametres().put("id", id);
            debut = false;
        }
        if (libelle != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(libelle) LIKE lower(:libelle) ");
            getParametres().put("libelle", "%" + libelle.trim() + "%");
            debut = false;
        }
        if (libelleCourt != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(libelleCourt) LIKE lower(:libelleCourt) ");
            getParametres().put("libelleCourt", "%" + libelleCourt.trim() + "%");
            debut = false;
        }
        if (codeExterne != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" codeExterne = :codeExterne ");
            getParametres().put("codeExterne", codeExterne);
            debut = false;
        }
        if (codeGo != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" codeGo = :codeGo ");
            getParametres().put("codeGo", codeGo);
            debut = false;
        }
        if (commission != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" commission = :commission");
            getParametres().put("commission", commission);
            debut = false;
        }

        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by p.").append(proprieteTriee);
            if (triCroissant)
                sb.append(" ASC ");
            else
                sb.append(" DESC ");
        }

        return sb.toString();
    }

    public String toCountHQL() {
        return "select count(*) " + corpsRequete();
    }

    public final void setId(final Integer id) {
        this.id = id;
    }

    public final void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    public final void setLibelleCourt(final String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public final void setCodeExterne(final String codeExterne) {
        this.codeExterne = codeExterne;
    }

    public final void setCodeGo(final String codeGo) {
        this.codeGo = codeGo;
    }

    public final void setCommission(final Boolean commission) {
        this.commission = commission;
    }

}
