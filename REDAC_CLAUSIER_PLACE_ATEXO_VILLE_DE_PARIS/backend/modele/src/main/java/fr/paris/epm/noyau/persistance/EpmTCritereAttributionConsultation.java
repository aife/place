package fr.paris.epm.noyau.persistance;

import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.*;

/**
 * Critére d'attribution lié à la consultaiton. Ils sont renseignés dans le formulaire amont.
 * @author Léon Barsamian
 * @version
 */
public class EpmTCritereAttributionConsultation extends EpmTAbstractObject implements Comparable<EpmTCritereAttributionConsultation>, Cloneable {

    /**
     * Serialisaiton.
     */
    private static final long serialVersionUID = 1605676937074818724L;

    private int id;

    /**
     * Enoncé du critére.
     */
    private String enonce;

    /**
     * Ordre de priorité du critére.
     */
    private int ordre;

    /**
     * Pondération du critére.
     */
    private Double ponderation;

    /**
     * Liste de sous critère liés au critère d'attribution
     */
    private Set<EpmTSousCritereAttributionConsultation> sousCritere;

    public int getId() {
        return id;
    }

    public void setId(final int valeur) {
        this.id = valeur;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(final String valeur) {
        this.enonce = valeur;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(final int valeur) {
        this.ordre = valeur;
    }

    public Double getPonderation() {
        return ponderation;
    }

    public void setPonderation(final Double valeur) {
        this.ponderation = valeur;
    }

    /**
     *
     * @return Liste de sous critère liés au critère d'attribution
     */
    public Set<EpmTSousCritereAttributionConsultation> getSousCritere() {
		return sousCritere;
	}

    /**
     *
     * @param sousCritere Liste de sous critère liés au critère d'attribution
     */
    public void setSousCritere(final Set<EpmTSousCritereAttributionConsultation> valeur) {
		this.sousCritere = valeur;
	}


    @Override
    public boolean equals(Object obj) {

        EpmTCritereAttributionConsultation critereCmp = null;

        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EpmTCritereAttributionConsultation)) {
            return false;
        }

        critereCmp = (EpmTCritereAttributionConsultation) obj;

        if ((this.id == 0) || (critereCmp.getId() == 0)) {
            return false;
        }

        return (this.id == critereCmp.getId());
    }

    @Override
    public final Object clone() throws CloneNotSupportedException {
        EpmTCritereAttributionConsultation critereClone = (EpmTCritereAttributionConsultation)super.clone();
        critereClone.setId(0);
        return critereClone;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

	@Override
	public int compareTo(EpmTCritereAttributionConsultation object) {
        if (object == null) {
            return -1;
        } else {
        	EpmTCritereAttributionConsultation obj = (EpmTCritereAttributionConsultation) object;
            return (this.getOrdre() - obj.getId());

        }
	}

	public List<EpmTSousCritereAttributionConsultation> getListeSousCritereAttributionTriee() {
		List<EpmTSousCritereAttributionConsultation> listeTriee = new ArrayList<EpmTSousCritereAttributionConsultation>();
		if (getSousCritere() != null) {
			for (EpmTSousCritereAttributionConsultation sousCritere : getSousCritere()) {
				listeTriee.add(sousCritere);
			}

			Collections.sort(listeTriee,
					new Comparator<EpmTSousCritereAttributionConsultation>() {

						@Override
						public int compare(
								EpmTSousCritereAttributionConsultation arg0,
								EpmTSousCritereAttributionConsultation arg1) {
							return arg0.getOrdre() - arg1.getOrdre();
						}
					});
		}

		return listeTriee;
	}

}
