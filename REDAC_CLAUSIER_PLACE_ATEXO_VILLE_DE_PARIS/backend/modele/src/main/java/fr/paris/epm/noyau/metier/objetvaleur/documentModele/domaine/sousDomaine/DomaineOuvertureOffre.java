package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.ouvertureOffres.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineOuvertureOffre implements Serializable {
	
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Sous-domaine affecte si le document est genere pour une entreprise.
     */
    private DomaineOffre offre;
    /**
     * Sous-domaines affectes si le document est genere pour une consultation ou
     * lot.
     */
    private List<DomaineOffre> listeOffres;
    /**
     * Liste des candidats ayant déposé une offre (phase restreinte) ou
     * admissibles si phase ouverte.
     */
    private List<String> listeCandidatsOffres;
    
    /**
     * OffreLc classées par lot.
     */
    private Map<String, List<DomaineOffre>> offresParLot;

    public final DomaineOffre getOffre() {
        return offre;
    }

    public final void setOffre(final DomaineOffre valeur) {
        this.offre = valeur;
    }

    public final List<DomaineOffre> getListeOffres() {
        return listeOffres;
    }

    public final void setListeOffres(final List<DomaineOffre> valeur) {
        listeOffres = valeur;
    }

    /**
     * @return Liste des candidats ayant déposé une offre (phase restreinte) ou
     *         admissibles si phase ouverte.
     */
    public final List<String> getListeCandidatsOffres() {
        return listeCandidatsOffres;
    }

    /**
     * @param valeur Liste des candidats ayant déposé une offre (phase
     *            restreinte) ou admissibles si phase ouverte.
     */
    public final void setListeCandidatsOffres(final List<String> valeur) {
        this.listeCandidatsOffres = valeur;
    }

	/**
	 * @return OffreLc classées par lot.
	 */
	public final Map<String, List<DomaineOffre>> getOffresParLot() {
		return offresParLot;
	}

	/**
	 * @param valeur OffreLc classées par lot.
	 */
	public final void setOffresParLot(final Map<String, List<DomaineOffre>> valeur) {
		this.offresParLot = valeur;
	}
}
