package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.List;

/**
 * Contient tous les domaines et/ou données de niveau 2 à être fusionné dans un document du module exécution.
 * @author rebeca
 *
 */
public class DomaineContrat implements Serializable {
	
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Domaine contenant des données de l'écran Caracteristiques du Contrat du
	 * module exécution
	 */
	private DomaineCaracteristiques caracteristiques; 
	
	/**
	 * Domaine contenant des données de l'écran Informations Principales du
	 * Contrat du module exécution
	 */
	private DomaineInformationsPrincipales informationsPrincipales;
	
	/**
	 * Liste de tous les attributaires du contrat
	 */
	private List<DomaineAttributaire> attributaires;
	
	/**
	 *  Liste de suivi factures dans l'écran suivi des événements financiers du contrat
	 */
	private List<DomaineSuiviFinancierFactures> suiviFactures;
	
	/**
	 * Liste de suivi des événements opérationnels dans l'écran Suivi
	 * des événements opérationnels
	 */
	private List<DomaineSuiviEvenementOperationnel> suiviEvenementOperationnels;
	
	/**
	 * Liste de suivi des événements administratifs dans
	 * l'écran Suivi des événements administratifs
	 */
	private List<DomaineSuiviEvenementAdministratif> suiviEvenementAdministratifs;
	
	/**
	 * Liste de suivi des dates dans
	 * l'écran Suivi des événements administratifs
	 */
	private List<DomaineSuiviDate> suiviDates;

	/**
	 * Liste de tous les avenants du contrat
	 */
    private List<DomaineAvenantDuContrat> avenants;

	/**
	 * Liste des marchés subséquents à l’accord-cadre, rempli si le contrat est un Accord-cadre
	 */
    private List<DomaineContrat> listeMarchesSub;
    
	/**
	 * Accord-cadre dont découle le marché subséquent, non null si le contrat est issu d'un Accord-cadre
	 */
    private DomaineContrat accordCadreMarcheSub;
    
	/**
	 * Liste de traches lié au contrat. Il est vide si marcheAvecTranches est Non
	 */
	private List<DomaineTranche> tranches;
	
	/**
	 * Liste de poste techniques liés au contrat. Il est vide si marcheAvecLotsTechniques est Non
	 */
	private List<DomainePosteTechnique> posteTechniques;
    
	/**
	 * @return les données de du domaine correspondant à l'écran caractéristiques du contrat
	 */
	public final DomaineCaracteristiques getCaracteristiques() {
		return caracteristiques;
	}

	/**
	 * @param valeur : données du domaine correspondant à l'écran caracteristique du contrat du module exécution
	 */
	public final void setCaracteristiques(final DomaineCaracteristiques valeur) {
		this.caracteristiques = valeur;
	}

	/**
	 * @return les données de du domaine correspondant à l'écran informations principales du contrat
	 */
	public final DomaineInformationsPrincipales getInformationsPrincipales() {
		return informationsPrincipales;
	}

	/**
	 * @param valeur : données du domaine correspondant à l'écran informations principales du contrat du module exécution
	 */
	public final void setInformationsPrincipales(final DomaineInformationsPrincipales valeur) {
		this.informationsPrincipales = valeur;
	}

	public final List<DomaineSuiviFinancierFactures> getSuiviFactures() {
		return suiviFactures;
	}

	public final void setSuiviFactures(
			final List<DomaineSuiviFinancierFactures> valeur) {
		this.suiviFactures = valeur;
	}

	public final List<DomaineSuiviEvenementOperationnel> getSuiviEvenementOperationnels() {
		return suiviEvenementOperationnels;
	}

	public final void setSuiviEvenementOperationnels(
			final List<DomaineSuiviEvenementOperationnel> valeur) {
		this.suiviEvenementOperationnels = valeur;
	}

	public final List<DomaineSuiviEvenementAdministratif> getSuiviEvenementAdministratifs() {
		return suiviEvenementAdministratifs;
	}

	public final void setSuiviEvenementAdministratifs(
			final List<DomaineSuiviEvenementAdministratif> valeur) {
		this.suiviEvenementAdministratifs = valeur;
	}
	
	public final List<DomaineAvenantDuContrat> getAvenants() {
		return avenants;
	}

	public final void setAvenants(final List<DomaineAvenantDuContrat> valeur) {
		this.avenants = valeur;
	}

	public final List<DomaineContrat> getListeMarchesSub() {
		return listeMarchesSub;
	}

	public final void setListeMarchesSub(final List<DomaineContrat> valeur) {
		this.listeMarchesSub = valeur;
	}

	public final DomaineContrat getAccordCadreMarcheSub() {
		return accordCadreMarcheSub;
	}

	public final void setAccordCadreMarcheSub(final DomaineContrat valeur) {
		this.accordCadreMarcheSub = valeur;
	}

	public final List<DomaineSuiviDate> getSuiviDates() {
		return suiviDates;
	}

	public final void setSuiviDates(final List<DomaineSuiviDate> valeur) {
		this.suiviDates = valeur;
	}
	
	/**
	 * 
	 * @param Liste de tous les attributaires du contrat
	 */
	public final List<DomaineAttributaire> getAttributaires() { 
		return attributaires;
	}

	/**
	 * 
	 * @param Liste de tous les attributaires du contrat
	 */
	public final void setAttributaires(final List<DomaineAttributaire> valeur) {
		this.attributaires = valeur;
	}
	
	public final List<DomaineTranche> getTranches() {
		return tranches;
	}

	public final void setTranches(final List<DomaineTranche> valeur) {
		this.tranches = valeur;
	}

	public final List<DomainePosteTechnique> getPosteTechniques() {
		return posteTechniques;
	}

	public final void setPosteTechniques(final List<DomainePosteTechnique> valeur) {
		this.posteTechniques = valeur;
	}
}
