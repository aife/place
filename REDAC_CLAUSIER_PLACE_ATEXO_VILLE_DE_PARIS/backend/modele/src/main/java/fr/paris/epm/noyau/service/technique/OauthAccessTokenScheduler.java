package fr.paris.epm.noyau.service.technique;

import org.springframework.stereotype.Service;

@Service
public interface OauthAccessTokenScheduler {

	/**
	 * Permet de programmer le rafraichissement du jeton
	 */
	void schedule();
}
