package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;

import java.util.Objects;

/**
 * Classe abstraite EpmTReferentielAbstract est la racine d'héritation pour toutes les référentiels.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class BaseEpmTRefReferentiel implements EpmTRef, Comparable<EpmTRef> {

	private static final int ID_TOUS = 0;
    /**
     * Identifiant de l'enregistrement.
     */
    protected Integer id;

    /**
     * État actif ou non.
     */
    protected boolean actif;

    /**
     * Libellé de l'enregistrement.
     */
    protected String libelle;

    /**
     * Libellé court de l'enregistrement.
     */
    protected String libelleCourt;

    /**
     * code externe utilise dans le cadre d'alize
     */
    protected String codeExterne;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean isActif() {
        return actif;
    }

    @Override
    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @Override
    public String getLibelle() {
        return libelle;
    }

    @Override
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String getLibelleCourt() {
        return libelleCourt;
    }

    @Override
    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    @Override
    public String getCodeExterne() {
        return codeExterne;
    }

    @Override
    public void setCodeExterne(String codeExterne) {
        this.codeExterne = codeExterne;
    }

    @Override
    public String toString() {
        return super.toString() + " Identifiant: " + id + ", Libellé: " + libelleCourt + (actif ? ", actif" : ", inactif");
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        BaseEpmTRefReferentiel that = (BaseEpmTRefReferentiel) obj;
        return id == that.id &&
                actif == that.actif &&
                Objects.equals(libelle, that.libelle) &&
                Objects.equals(libelleCourt, that.libelleCourt) &&
                Objects.equals(codeExterne, that.codeExterne);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actif, libelle, libelleCourt, codeExterne);
    }

    /**
     * Comparaison par rapport au libellé. Gère "TOUS" en identifiant 0.
     * @param referentiel objet à comparer
     * @return int
     */
    @Override
    public int compareTo(final EpmTRef referentiel) {
		if (null == referentiel || null == referentiel.getId()) {
			return -1;
		}

        if (id == ID_TOUS ) {
	       return referentiel.getId() == ID_TOUS ? 0 : -1;
        } else {
	        if ( referentiel.getId() == ID_TOUS ) {
		        return 1;
	        }
	        else {
		        return Objects.toString(libelle, "").compareTo(Objects.toString(referentiel.getLibelle(), ""));
	        }
        }
	}
}
