package fr.paris.epm.noyau.metier;

import java.util.List;
import java.util.Map;

public class DocumentEmplacementCritere extends AbstractCritere implements
		Critere {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = 7251991938900979054L;

	/**
	 * Liste de référence
	 */
	private List<String> references;
	
	private Boolean moduleExecution = null;
	
	@Override
	public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();
        sb.append("from EpmTRefDocumentEmplacement e");
        
        if (references != null && !references.isEmpty()) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" ( e.reference like (:reference0) ");
            parametres.put("reference0", references.get(0));
            for (int i = 1; i < references.size(); i++) {
                sb.append(" OR e.reference like (:reference"+i+") ");
                parametres.put("reference"+i, references.get(i));    
            }
            sb.append(" ) ");
            debut = true;
        }
        
        if(moduleExecution != null) {
            if(moduleExecution) {
            	if (debut) {
                    sb.append(" and ");
                } else {
                    sb.append(" where ");
                }
                sb.append(" e.moduleExecution = TRUE");
                debut = true;
            } else {
            	if (debut) {
                    sb.append(" and ");
                } else {
                    sb.append(" where ");
                }
                sb.append(" e.moduleExecution = FALSE");
                debut = true;
            }
        }
        return sb;
	}

	@Override
	public String toCountHQL() {
		return "select count(*) " + corpsRequete();
	}

	@Override
	public String toHQL() {
		StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append("  order by e.");
            sb.append(proprieteTriee);
            if (triCroissant) {
                sb.append(" ASC ");
            } else {
                sb.append(" DESC ");
            }
        }
        return sb.toString();
	}

	/**
	 * Liste des références recherché
	 * @param references
	 */
	public void setReferences(List<String> references) {
		this.references = references;
	}

	public final void setModuleExecution(final boolean valeur) {
		this.moduleExecution = valeur;
	}

}
