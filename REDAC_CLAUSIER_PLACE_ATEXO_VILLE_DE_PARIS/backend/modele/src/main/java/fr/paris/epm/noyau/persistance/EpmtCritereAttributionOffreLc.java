package fr.paris.epm.noyau.persistance;

import java.util.*;

/**
 * Critere d'attribution d'une offre ou d'un projet lie a un lot ou une
 * consultation.
 * @author Léon Barsamian
 * @version
 */
public class EpmtCritereAttributionOffreLc extends EpmTAbstractObject {
    
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -4709138212246681065L;

    /**
     * Identifiant
     */
    private int id;
    
    /**
     * Note de l'offre saisie pour le critére {@link EpmTCritereAttributionConsultation} lié.
     */
    private Double note;

    /**
     * note pondéré calculé par rapport à la note saisie par l'utilisateur et la
     * pondération indiquée dans le critere
     * {@link EpmTCritereAttributionConsultation} lié.
     */
    private Double notePondere;
    
    /**
     * Commentaire de l'offre pour ce critére.
     */
    private String commentaire;
    
    /**
     * Critére de la consultation auquel est lié le critére de l'offre.
     */
    private EpmTCritereAttributionConsultation critereConsultation;
    
    /**
     * Identifiat de l' EpmTOffreLc à laquelle est rataché le critére.
     */
    private Integer idOffreLc;
    
    /**
     * Identifiat du projet d'un lot ou d'une consultation auquel est ratache le critere.
     */
    private Integer idProjetLc;
    
    private Set<EpmTSousCritereAttributionOffreLc> sousCriteresOffreLc;
    
    public int getId() {
        return id;
    }

    public void setId(final int valeur) {
        this.id = valeur;
    }

    public Double getNote() {
        return note;
    }

    public void setNote(final Double valeur) {
        this.note = valeur;
    }

    public Double getNotePondere() {
        return notePondere;
    }

    public void setNotePondere(final Double valeur) {
        this.notePondere = valeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    public EpmTCritereAttributionConsultation getCritereConsultation() {
        return critereConsultation;
    }

    public void setCritereConsultation(final EpmTCritereAttributionConsultation valeur) {
        this.critereConsultation = valeur;
    }

    public Integer getIdOffreLc() {
        return idOffreLc;
    }

    public void setIdOffreLc(final Integer valeur) {
        this.idOffreLc = valeur;
    }

    public Integer getIdProjetLc() {
        return idProjetLc;
    }

    public void setIdProjetLc(final Integer valeur) {
        this.idProjetLc = valeur;
    }

	public Set<EpmTSousCritereAttributionOffreLc> getSousCriteresOffreLc() {
		return sousCriteresOffreLc;
	}

	public void setSousCriteresOffreLc(final
			Set<EpmTSousCritereAttributionOffreLc> valeur) {
		this.sousCriteresOffreLc = valeur;
	}

	public List<EpmTSousCritereAttributionOffreLc> getSousCriteresOffreTriee() {
	    	List<EpmTSousCritereAttributionOffreLc> listeTriee = new ArrayList<EpmTSousCritereAttributionOffreLc>();
	    	if(getSousCriteresOffreLc() != null) {
		    	for(EpmTSousCritereAttributionOffreLc sousCritere : getSousCriteresOffreLc()) {
		    		listeTriee.add(sousCritere);
		    	}
		    	
		    	Collections.sort(listeTriee, new Comparator<EpmTSousCritereAttributionOffreLc>() {

					@Override
					public int compare(EpmTSousCritereAttributionOffreLc arg0,
							EpmTSousCritereAttributionOffreLc arg1) {
						return arg0.getOrdre() - arg1.getOrdre();
					}
				});	    		
	    	}
	    	
	    	return listeTriee;
	    }

    
}
