/**
 * 
 */
package fr.paris.epm.noyau.persistance;

/**
 * Opération lié à un organisme (Lycée pour crfc).
 * @author Léon Barsamian
 *
 */
public class EpmTReferentielExterneOperationOrganisme extends EpmTReferentielExterne {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 954255694146182693L;

}
