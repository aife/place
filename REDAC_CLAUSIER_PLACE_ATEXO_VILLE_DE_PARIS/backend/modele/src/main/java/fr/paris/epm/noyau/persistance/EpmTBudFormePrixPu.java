/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefBonQuantite;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefMinMax;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypePrix;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefVariation;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate du Bloc Unitaire de Données Forme de Prix, Prix Unitaire.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBudFormePrixPu extends EpmTBudFormePrix {

    // Propriétés

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * sélect Bon Quantité.
     */
    private EpmTRefBonQuantite epmTRefBonQuantite;

    /**
     * Ensemble de sélects type de prix.
     */
    private Set<EpmTRefTypePrix> epmTRefTypePrix = new HashSet<EpmTRefTypePrix>();

    /**
     * sélect min/max.
     */
    private EpmTRefMinMax epmTRefMinMax;

    /**
     * minimum HT.
     */
    private Double puMinHt;

    /**
     * maximum HT.
     */
    private Double puMaxHt;
    
    /**
     * minimum TTC.
     */
    private Double puMinTtc;

    /**
     * maximum TTC.
     */
    private Double puMaxTtc;

    /**
     * estimation HT.
     */
    private Double puEstimationHt;

    /**
     * estimation TTC.
     */
    private Double puEstimationTtc;

    /**
     * date de valeur.
     */
    private Calendar puDateValeur;

    /**
     * Collection d'objets {
     * @link EpmTRefVariations } associés.
     */
    private Set<EpmTRefVariation> puEpmTRefVariations = new HashSet(0);

    // Constructeurs

    /**
     * Constructeur vide pour hibernate.
     */
    public EpmTBudFormePrixPu() {
        super();
    }

    //
    // /**
    // * Constructeur complet.
    // * @param bonQuantite sélect Bon Quantité
    // * @param typePrix ensemble de sélects type de prix
    // * @param minMax sélect min/max
    // * @param minHt minimum HT
    // * @param maxHt maximum HT
    // * @param estimationHt estimation HT
    // * @param estimationTtc estimation TTC
    // * @param dateValeur date de valeur
    // * @param variations Ensemble de { @link EpmTRefVariations } associés
    // */
    // public EpmTBudFormePrixPu(final EpmTRefBonQuantite bonQuantite,
    // final Set typePrix,
    // final EpmTRefMinMax minMax, final Integer minHt,
    // final Integer maxHt, final Integer estimationHt,
    // final Integer estimationTtc, final GregorianCalendar dateValeur,
    // final Set variations) {
    // super();
    // this.epmTRefBonQuantite = bonQuantite;
    // this.epmTRefTypePrix = typePrix;
    // this.epmTRefMinMax = minMax;
    // this.puMinHt = minHt;
    // this.puMaxHt = maxHt;
    // this.puEstimationHt = estimationHt;
    // this.puEstimationTtc = estimationTtc;
    // this.puDateValeur = dateValeur;
    // this.puEpmTRefVariations = variations;
    // }

    // Accesseurs

    /**
     * @return sélect Bon Quantité
     */
    public EpmTRefBonQuantite getEpmTRefBonQuantite() {
        return epmTRefBonQuantite;
    }

    /**
     * @param bonQuantite sélect Bon Quantité
     */
    public void setEpmTRefBonQuantite(final EpmTRefBonQuantite bonQuantite) {
        this.epmTRefBonQuantite = bonQuantite;
    }

    /**
     * @return sélect min/max
     */
    public EpmTRefMinMax getEpmTRefMinMax() {
        return epmTRefMinMax;
    }

    /**
     * @param minMax sélect min/max
     */
    public void setEpmTRefMinMax(final EpmTRefMinMax minMax) {
        this.epmTRefMinMax = minMax;
    }

    /**
     * @return ensemble de sélects type de prix
     */
    public Set<EpmTRefTypePrix> getEpmTRefTypePrix() {
        return epmTRefTypePrix;
    }

    /**
     * @param typePrix ensemble de sélects type de prix
     */
    public void setEpmTRefTypePrix(final Set<EpmTRefTypePrix> typePrix) {
        this.epmTRefTypePrix = typePrix;
    }

    /**
     * @return date de valeur
     */
    public Calendar getPuDateValeur() {
        return puDateValeur;
    }

    /**
     * @param dateValeur date de valeur
     */
    public void setPuDateValeur(final Calendar dateValeur) {
        this.puDateValeur = dateValeur;
    }

    /**
     * @return estimation interne HT
     */
    public Double getPuEstimationHt() {
        return puEstimationHt;
    }

    /**
     * @param estimationHT estimation interne HT
     */
    public void setPuEstimationHt(final Double estimationHT) {
        this.puEstimationHt = estimationHT;
    }

    /**
     * @return estimation interne TTC
     */
    public Double getPuEstimationTtc() {
        return puEstimationTtc;
    }

    /**
     * @param estimationTtc estimation interne TTC
     */
    public void setPuEstimationTtc(final Double estimationTtc) {
        this.puEstimationTtc = estimationTtc;
    }

    /**
     * @return maximum HT
     */
    public Double getPuMaxHt() {
        return puMaxHt;
    }

    /**
     * @param maxHt maximum HT
     */
    public void setPuMaxHt(final Double maxHt) {
        this.puMaxHt = maxHt;
    }

    /**
     * @return minimum HT
     */
    public Double getPuMinHt() {
        return puMinHt;
    }

    /**
     * @param minHt minimum HT
     */
    public void setPuMinHt(final Double minHt) {
        this.puMinHt = minHt;
    }

    /**
     * @return collection de {
     * @link EpmTRefVariations } associés.
     */
    public Set<EpmTRefVariation> getPuEpmTRefVariations() {
        return puEpmTRefVariations;
    }

    /**
     * @param variations Collection de {
     * @link EpmTRefVariations } associés.
     */
    public void setPuEpmTRefVariations(final Set<EpmTRefVariation> variations) {
        this.puEpmTRefVariations = variations;
    }
    
    /**
     * @return
     */
    public Double getPuMinTtc() {
        return puMinTtc;
    }

    /**
     * @param valeur
     */
    public void setPuMinTtc(final Double valeur) {
        this.puMinTtc = valeur;
    }

    /**
     * @return
     */
    public Double getPuMaxTtc() {
        return puMaxTtc;
    }

    /**
     * @param valeur
     */
    public void setPuMaxTtc(final Double valeur) {
        this.puMaxTtc = valeur;
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTBudFormePrixPu formPrix = new EpmTBudFormePrixPu();
        formPrix.setEpmTRefBonQuantite(epmTRefBonQuantite);
        formPrix.setEpmTRefMinMax(epmTRefMinMax);
        formPrix.setPuMinHt(puMinHt);
        formPrix.setPuMaxHt(puMaxHt);
        formPrix.setPuMinTtc(puMinTtc);
        formPrix.setPuMaxTtc(puMaxTtc);
        formPrix.setPuEstimationHt(puEstimationHt);
        formPrix.setPuEstimationTtc(puEstimationTtc);
        formPrix.setPuDateValeur(puDateValeur);
        if (epmTRefTypePrix != null) {
            Set set = new HashSet();
            set.addAll(epmTRefTypePrix);
            formPrix.setEpmTRefTypePrix(set);
        }
        if (puEpmTRefVariations != null) {
            Set set = new HashSet();
            set.addAll(puEpmTRefVariations);
            formPrix.setPuEpmTRefVariations(set);
        }
        
        return formPrix;
    }
}
