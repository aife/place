package fr.paris.epm.noyau.persistance;

import java.util.Date;

/**
 * Pojo hibernate répresentant le mail à envoyer lors de la gestion d'alertes
 * 
 * @author Rebeca Dantas
 *
 */
public class EpmTMailAlerte extends EpmTAbstractObject {

	/**
	 * Marqueur
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identifiant de l'alerte
	 */
	private int id;
	
	/**
	 * Destinataire du mail
	 */
	private String destinataire;

	/**
	 * L'objet du mail
	 */
	private String objet;
	
	/**
	 * Le contenu du mail
	 */
	private String contenu;
	
	/**
	 * Date d'envoi de mail
	 */
	private Date dateEnvoi;
	
	/**
	 * Status de l'envoi du mail
	 */
	private boolean envoye;
	
	/**
	 * Message d'erreur si le mail n'était pas envoyé
	 */
	private String messageErreur;

	/**
	 * Dans le cas de plusieurs pièces jointes.
	 */
	private EpmTPieceJointe piecesJointes;

	/**
	 * @return l'identifiant du mail alerte
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param l'identifiant du mail alerte
	 */
	public void setId(final int valeur) {
		this.id = valeur;
	}

	/**
	 * @return le destinataire du mail d'alerte
	 */
	public String getDestinataire() {
		return destinataire;
	}

	/**
	 * @param le destinataire du mail d'alerte
	 */
	public void setDestinataire(final String valeur) {
		this.destinataire = valeur;
	}

	/**
	 * @return l'objet du mail d'alerte
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param l'objet du mail d'alerte
	 */
	public void setObjet(final String valeur) {
		this.objet = valeur;
	}

	/**
	 * @return the contenu
	 */
	public String getContenu() {
		return contenu;
	}

	/**
	 * @param le contenu du mail d'alerte
	 */
	public void setContenu(final String valeur) {
		this.contenu = valeur;
	}

	/**
	 * @return la date d'envoi du mail d'alerte
	 */
	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	/**
	 * @param la date d'envoi du mail d'alerte
	 */
	public void setDateEnvoi(final Date valeur) {
		this.dateEnvoi = valeur;
	}

	/**
	 * @return le status de l'envoi du mail
	 */
	public boolean isEnvoye() {
		return envoye;
	}

	/**
	 * @param le status de l'envoi du mail
	 */
	public void setEnvoye(final boolean valeur) {
		this.envoye = valeur;
	}

	/**
	 * @return le message d'erreur en cas d'echec d'envoi de mail d'alerte
	 */
	public String getMessageErreur() {
		return messageErreur;
	}

	/**
	 * @param le message d'erreur en cas d'echec d'envoi de mail d'alerte
	 */
	public void setMessageErreur(final String valeur) {
		this.messageErreur = valeur;
	}

	public EpmTPieceJointe getPiecesJointes() {
		return piecesJointes;
	}

	public void setPiecesJointes(EpmTPieceJointe valeur) {
		this.piecesJointes = valeur;
	}
}
