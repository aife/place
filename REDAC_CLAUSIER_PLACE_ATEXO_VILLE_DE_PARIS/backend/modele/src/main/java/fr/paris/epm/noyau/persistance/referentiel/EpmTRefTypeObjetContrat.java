package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeObjetContrat de la table "epm__t_ref_type_objet_contrat"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeObjetContrat extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int MARCHE = 1;
    public static final int ACCORD_CADRE = 2;
    public static final int DSP = 3;
    public static final int MARCHES_SUBSEQUENTS = 4;

    public static final String LIBELLE_MARCHE = "Marché";
    public static final String LIBELLE_AC = "Accord-cadre";

}