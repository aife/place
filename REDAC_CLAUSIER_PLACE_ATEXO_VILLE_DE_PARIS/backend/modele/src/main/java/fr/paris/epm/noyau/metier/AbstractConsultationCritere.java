package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;

import java.util.Iterator;
import java.util.List;

/**
 * @author Léon BARSAMIAN
 * Criteres commun au rechercher de consultation (perimetre vision etc)
 */
public abstract class AbstractConsultationCritere extends AbstractCritere {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    /**
     * Type de périmètre: intervention, vision ou les deux.
     */
    protected int typePerimetre;
    

    /**
     * Direction/service de l'utilisateur courant.
     */
    protected DirectionService dirServiceUtilisateur;
    
    /**
     * Identifiant de l'organisme auquel la direction service est ratachée
     */
    protected Integer idOrganisme;

    /**
     * @param type type de périmètre: intervention, vision ou les deux
     * @param dirService direction/service de l'utilisateur courant
     * @throws ApplicationNoyauException erreur applicative
     */
    public final void setPerimetre(final int type, final DirectionService dirService) {
        if (dirService == null) {
            throw new ApplicationNoyauException("dir/service est nul");
        }

        switch (type) {
        case DirectionService.P_INTERVENTION:
        case DirectionService.P_VISION:
        case DirectionService.P_TOUS:
            typePerimetre = type;
            dirServiceUtilisateur = dirService;
            break;
        default:
            throw new ApplicationNoyauException("type inconnu");
        }
    }

    /**
     * @param sb stringbuffer contenant la requête en construction
     * @param d vrai si le "where" n'a pas encore été utilisé
     * @return nouvelle valeur du bouléen d
     */
    protected boolean perimetres(final StringBuffer sb, final boolean d) {
        boolean debut = d;

        switch (typePerimetre) {
        case DirectionService.P_INTERVENTION:
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            perimetreIntervention(sb);
            debut = false;
            break;
        case DirectionService.P_VISION:
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            perimetreVision(sb);
            debut = false;
            break;
        case DirectionService.P_TOUS:
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            StringBuffer sbTous = new StringBuffer();
            perimetreIntervention(sbTous);
            debut = false;
            sbTous.insert(0, "((");
            sbTous.append(") OR (");
            perimetreVision(sbTous);
            sbTous.append("))");
            sb.append(sbTous);
            break;
        default:
            // Cas impossible
            break;
        }
        return debut;
    }

    /**
     * Si la consultation n'est pas transverse, renvoie une contrainte sur les
     * direction/service bénéficiaires des consultations cherchées.
     * @param sb requête
     */
    protected void perimetreVision(final StringBuffer sb) {
        sb.append(" (c.dirServiceVision IN ");
        StringBuffer ids = new StringBuffer("(");
        List idsA = dirServiceUtilisateur.listeIdsAyantVision();
        for (Iterator iter = idsA.iterator(); iter.hasNext();) {
            Integer element = (Integer) iter.next();
            ids.append(element.intValue());
            if (iter.hasNext()) {
                ids.append(",");
            }
        }
        ids.append(") or c.transverse ='oui')");
        sb.append(ids);
    }

    /**
     * Restreint les consultations trouvées à celles dont la direction/service
     * responsable est dans le périmètre d'intervention de l'utilisateur.
     * @param sb requête
     */
    protected void perimetreIntervention(final StringBuffer sb) {
        sb.append(" c.epmTRefDirectionService.id IN ");
        StringBuffer ids = new StringBuffer("(");
        List idsA = dirServiceUtilisateur.listeIdsAyantIntervention();
        for (Iterator iter = idsA.iterator(); iter.hasNext();) {
            Integer element = (Integer) iter.next();
            ids.append(element.intValue());
            if (iter.hasNext()) {
                ids.append(",");
            }
        }
        ids.append(")");
        sb.append(ids);
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public final void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }
}
