package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefInfoBulle de la table "epm__t_ref_info_bulle"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefInfoBulle extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private String champ;

    /**
     * lien vers la fiche pratique.
     */
    private String fichePratique;

    /**
     * référence de l'ecran associé à cette info-bulle.
     */
    private String ecran;

    /**
     * Texte de l'info-bulle.
     */
    private String contenu;

    public String getChamp() {
        return champ;
    }

    public void setChamp(String champ) {
        this.champ = champ;
    }

    public String getFichePratique() {
        return fichePratique;
    }

    public void setFichePratique(String fichePratique) {
        this.fichePratique = fichePratique;
    }

    public String getEcran() {
        return ecran;
    }

    public void setEcran(String ecran) {
        this.ecran = ecran;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

}