package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Pojo hibernate du canevas
 */
@Entity
@Table(name = "epm__t_canevas", schema = "redaction")
@Audited
public class EpmTCanevas extends EpmTCanevasAbstract {

    /**
     * Cet attribut mappe la valeur de la colonne de clé primaire.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Identifiant de la dernière publication pour chaque canevas
     */
    @Column(name = "id_last_publication")
    private Integer idLastPublication;

    @OneToMany(targetEntity = EpmTChapitre.class,
            mappedBy = "epmTCanevas", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    @NotAudited
    private List<EpmTChapitre> epmTChapitres;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_canevas_has_ref_type_contrat",
            joinColumns = @JoinColumn(name = "id_canevas", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_type_contrat", referencedColumnName = "id"))
    @NotAudited
    private Set<EpmTRefTypeContrat> epmTRefTypeContrats;

    /**
     * Liste des procedures d'editeur
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_canevas_has_ref_procedure_passation",
            joinColumns = @JoinColumn(name = "id_canevas"),
            inverseJoinColumns = @JoinColumn(name = "id_procedure"))
    @NotAudited
    private Set<EpmTRefProcedure> epmTRefProcedures;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdCanevas() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    public Integer getIdLastPublication() {
        return idLastPublication;
    }

    public void setIdLastPublication(Integer idLastPublication) {
        this.idLastPublication = idLastPublication;
    }

    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        return epmTRefTypeContrats;
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        this.epmTRefTypeContrats = epmTRefTypeContrats;
    }

    @Override
    public Set<EpmTRefProcedure> getEpmTRefProcedures() {
        return epmTRefProcedures;
    }

    @Override
    public void setEpmTRefProcedures(Set<EpmTRefProcedure> epmTRefProcedures) {
        this.epmTRefProcedures = epmTRefProcedures;
    }

    @Override
    public List<EpmTChapitre> getEpmTChapitres() {
        return epmTChapitres;
    }

    @Override
    public void setEpmTChapitres(List<? extends EpmTChapitreAbstract> epmTChapitres) {
        this.epmTChapitres = (List<EpmTChapitre>) epmTChapitres;
    }

    @Override
    public EpmTCanevas clone() throws CloneNotSupportedException {
        EpmTCanevas epmTCanevas = (EpmTCanevas) super.clone();
        epmTCanevas.id = 0;

        epmTCanevas.epmTRefProcedures = new HashSet<EpmTRefProcedure>(epmTRefProcedures);
        epmTCanevas.epmTRefTypeContrats = new HashSet<EpmTRefTypeContrat>(epmTRefTypeContrats);
        epmTCanevas.idLastPublication = null;

        return epmTCanevas;
    }

}
