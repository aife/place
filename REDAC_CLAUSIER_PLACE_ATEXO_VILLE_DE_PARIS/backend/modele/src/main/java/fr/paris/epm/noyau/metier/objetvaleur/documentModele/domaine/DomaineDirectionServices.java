package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine directionServices.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineDirectionServices implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	private String libelleDirectionService;
	
	private String adresseDirectionService = "";
	
	private String telephoneDirectionService = "";
	
	private String faxDirectionService = "";
	
	private String mailDirectionService = "";
	
	private String serviceRattachementDS = "";
	
	private String directionTransverseON = "";

	public final String getLibelleDirectionService() {
		return libelleDirectionService;
	}

	public final void setLibelleDirectionService(final String valeur) {
		this.libelleDirectionService = valeur;
	}

	public final String getAdresseDirectionService() {
		return adresseDirectionService;
	}

	public final void setAdresseDirectionService(final String valeur) {
		this.adresseDirectionService = valeur;
	}

	public final String getTelephoneDirectionService() {
		return telephoneDirectionService;
	}

	public final void setTelephoneDirectionService(final String valeur) {
		this.telephoneDirectionService = valeur;
	}

	public final String getFaxDirectionService() {
		return faxDirectionService;
	}

	public final void setFaxDirectionService(final String valeur) {
		this.faxDirectionService = valeur;
	}

    public final String getMailDirectionService() {
        return mailDirectionService;
    }

    public final void setMailDirectionService(final String valeur) {
        this.mailDirectionService = valeur;
    }

    public final String getServiceRattachementDS() {
        return serviceRattachementDS;
    }

    public final void setServiceRattachementDS(final String valeur) {
        this.serviceRattachementDS = valeur;
    }

    public final String getDirectionTransverseON() {
        return directionTransverseON;
    }

    public final void setDirectionTransverseON(final String valeur) {
        this.directionTransverseON = valeur;
    }
	
}
