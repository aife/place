package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import fr.paris.epm.noyau.persistance.EpmTAvenant;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * avenant.caracteristiquesPrincipales.
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCaracteristiquesPrincipales implements Serializable {

    private String numeroContratInitialAvenant;

    private String referenceAvenant;

    private String intituleConsultationInitialeAvenant;

    private String intituleLotConsultationInitialeAvenant;

    private String objetAvenant;

    private String pouvoirAdjudicateurAvenant;

    private String directionServiceResponsableAvenant;

    private String typeContratAvenant;

    private String typeAvenant;

    private String montantAvenant;

    private String dateSelectionCandidatureCao;

    private String dateDeliberationAmontReunion;

    private String dateControleDeLegalite;

    /**
     * titulaire initiale.
     */
    private String titulaireInitial;

    /**
     * Date AR de notification sur écran attribution du contrat.
     */
    private Date dateARNotification;

    public DomaineCaracteristiquesPrincipales() {

    }

    public DomaineCaracteristiquesPrincipales(EpmTAvenant avenant) {
        setNumeroContratInitialAvenant(avenant.getReferenceMarche());
        setReferenceAvenant(avenant.getReferenceConcatenee());
        setIntituleConsultationInitialeAvenant(avenant.getIntituleConsultationInitiale());
        setObjetAvenant(avenant.getObjet());
        setPouvoirAdjudicateurAvenant(avenant.getEpmTRefPouvoirAdjudicateur()!=null && avenant.getEpmTRefPouvoirAdjudicateur().getLibelle()!=null?avenant.getEpmTRefPouvoirAdjudicateur().getLibelle():"");
        setDirectionServiceResponsableAvenant(avenant.getEpmTRefDirectionService()!=null&&avenant.getEpmTRefDirectionService().getLibelle()!=null?avenant.getEpmTRefDirectionService().getLibelle():"");
        setTypeContratAvenant(avenant.getTypeContrat().getLibelle());
        setTypeAvenant(avenant.getTypeAvenant()!=null &&avenant.getTypeAvenant().getLibelle()!=null?avenant.getTypeAvenant().getLibelle():"");
        setMontantAvenant(String.valueOf(avenant.getPfMn()));
        setIntituleLotConsultationInitialeAvenant(avenant.getIntituleLot());
        setTitulaireInitial(avenant.getTitulaireInitial());
    }

    public final String getNumeroContratInitialAvenant() {
        return numeroContratInitialAvenant;
    }

    public final void setNumeroContratInitialAvenant(final String valeur) {
        this.numeroContratInitialAvenant = valeur;
    }

    public final String getReferenceAvenant() {
        return referenceAvenant;
    }

    public final void setReferenceAvenant(final String valeur) {
        this.referenceAvenant = valeur;
    }

    public final String getIntituleConsultationInitialeAvenant() {
        return intituleConsultationInitialeAvenant;
    }

    public final void setIntituleConsultationInitialeAvenant(final String valeur) {
        this.intituleConsultationInitialeAvenant = valeur;
    }

    public final String getIntituleLotConsultationInitialeAvenant() {
        return intituleLotConsultationInitialeAvenant;
    }

    public final void setIntituleLotConsultationInitialeAvenant(
            final String valeur) {
        this.intituleLotConsultationInitialeAvenant = valeur;
    }

    public final String getObjetAvenant() {
        return objetAvenant;
    }

    public final void setObjetAvenant(final String valeur) {
        this.objetAvenant = valeur;
    }

    public final String getPouvoirAdjudicateurAvenant() {
        return pouvoirAdjudicateurAvenant;
    }

    public final void setPouvoirAdjudicateurAvenant(final String valeur) {
        this.pouvoirAdjudicateurAvenant = valeur;
    }

    public final String getDirectionServiceResponsableAvenant() {
        return directionServiceResponsableAvenant;
    }

    public final void setDirectionServiceResponsableAvenant(final String valeur) {
        this.directionServiceResponsableAvenant = valeur;
    }

    public final String getTitulaireInitial() {
        return titulaireInitial;
    }

    public final void setTitulaireInitial(final String valeur) {
        this.titulaireInitial = valeur;
    }

    public final Date getDateARNotification() {
        return dateARNotification;
    }

    public final void setDateARNotification(final Date valeur) {
        this.dateARNotification = valeur;
    }

    /**
     * @return the typeContratAvenant
     */
    public String getTypeContratAvenant() {
        return typeContratAvenant;
    }

    /**
     * @param typeContratAvenant the typeContratAvenant to set
     */
    public void setTypeContratAvenant(String valeur) {
        this.typeContratAvenant = valeur;
    }

    /**
     * @return the typeAvenant
     */
    public String getTypeAvenant() {
        return typeAvenant;
    }

    /**
     * @param typeAvenant the typeAvenant to set
     */
    public void setTypeAvenant(String valeur) {
        this.typeAvenant = valeur;
    }

    public String getMontantAvenant() {
        return montantAvenant;
    }

    public void setMontantAvenant(String montantAvenant) {
        this.montantAvenant = montantAvenant;
    }

    public String getDateSelectionCandidatureCao() {
        return dateSelectionCandidatureCao;
    }

    public void setDateSelectionCandidatureCao(String dateSelectionCandidatureCao) {
        this.dateSelectionCandidatureCao = dateSelectionCandidatureCao;
    }

    public String getDateDeliberationAmontReunion() {
        return dateDeliberationAmontReunion;
    }

    public void setDateDeliberationAmontReunion(String dateDeliberationAmontReunion) {
        this.dateDeliberationAmontReunion = dateDeliberationAmontReunion;
    }

    public String getDateControleDeLegalite() {
        return dateControleDeLegalite;
    }

    public void setDateControleDeLegalite(String dateControleDeLegalite) {
        this.dateControleDeLegalite = dateControleDeLegalite;
    }
}
