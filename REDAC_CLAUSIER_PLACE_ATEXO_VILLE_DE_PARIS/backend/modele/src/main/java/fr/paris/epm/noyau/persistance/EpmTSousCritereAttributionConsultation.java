package fr.paris.epm.noyau.persistance;

/**
 * Sous critère d'attribution, utilisés dans le formulaire amont.
 * @author Rebeca Dantas
 *
 */
public class EpmTSousCritereAttributionConsultation extends EpmTAbstractObject implements Comparable<EpmTSousCritereAttributionConsultation>, Cloneable {

	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identifiant
	 */
    private int id;
	
	/**
	 * Enoncé du sous critère
	 */
	private String enonce;
	
	/**
	 * Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	private Double ponderation;
	
	/**
	 * Le critère d'attribution associé au sous critère
	 */
	private EpmTCritereAttributionConsultation critereAttribution;
	
	/**
	 * L'ordre d'ajout du sous critère
	 */
	private int ordre;
	
	/**
	 * 
	 * @return l'identifiant de sous scritere
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id L'identifiant du sous critère
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return Enoncé du sous critère
	 */
	public String getEnonce() {
		return enonce;
	}

	/**
	 * 
	 * @param enonce Enoncé du sous critère
	 */
	public void setEnonce(final String valeur) {
		this.enonce = valeur;
	}

	/**
	 * 
	 * @return Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	public Double getPonderation() {
		return ponderation;
	}

	/**
	 * 
	 * @param ponderation Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	public void setPonderation(final Double valeur) {
		this.ponderation = valeur;
	}

	/**
	 * 
	 * @return  le critère d'attribution associè au sous critère
	 */
	public EpmTCritereAttributionConsultation getCritereAttribution() {
		return critereAttribution;
	}

	/**
	 * 
	 * @param valeur le critère d'attribution associè au sous critère
	 */
	public void setCritereAttribution(
			EpmTCritereAttributionConsultation valeur) {
		this.critereAttribution = valeur;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(final int valeur) {
		this.ordre = valeur;
	}

	@Override
	public final Object clone() throws CloneNotSupportedException {
		EpmTSousCritereAttributionConsultation sousCritereClone = (EpmTSousCritereAttributionConsultation)super.clone();
		sousCritereClone.setId(0);
		sousCritereClone.setCritereAttribution(null);
		return sousCritereClone;
	}

	@Override
	public int compareTo(EpmTSousCritereAttributionConsultation object) {
        if (object == null) {
            return -1;
        } else {
        	EpmTSousCritereAttributionConsultation obj = (EpmTSousCritereAttributionConsultation) object;
            return (this.getOrdre() - obj.getId());

        }
	}	
}
