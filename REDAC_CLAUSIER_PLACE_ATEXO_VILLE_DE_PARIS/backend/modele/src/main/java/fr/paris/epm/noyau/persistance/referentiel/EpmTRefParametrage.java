package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefParametrage de la table "epm__t_ref_parametrage"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefParametrage extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final String LIEU_EXECUTION = "lieuExecution"; // le clef pour la paramétrage : le lieu d'exécution par défaut.

    /**
     * Identifie le parametrage.
     */
    private String clef;

    private String valeur;

    public String getClef() {
        return clef;
    }

    public void setClef(String clef) {
        this.clef = clef;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

}