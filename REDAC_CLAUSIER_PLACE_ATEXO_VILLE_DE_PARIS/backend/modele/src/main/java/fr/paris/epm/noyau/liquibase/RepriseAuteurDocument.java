package fr.paris.epm.noyau.liquibase;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

public class RepriseAuteurDocument implements CustomTaskChange {

    private ResourceAccessor resourceAccessor;

    private static final Logger logger = LoggerFactory.getLogger(RepriseAuteurDocument.class);

    @Override
    public void execute(Database database) throws CustomChangeException {
        logger.info("Liste utilisateurs : Reprise des données à cote de noyau START");

        try {
            // partial database export
            JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();
            Statement smt = databaseConnection.createStatement();
            ResultSet resultSet = smt.executeQuery("select prenom, nom, id_utilisateur from consultation.epm__t_utilisateur");

            logger.info("Liste utilisateurs : requete SQL - SUCCES");

            HashMap<Integer, String> hashMap = new HashMap<>();
            while (resultSet.next()) {
                String nom = resultSet.getString("nom");
                String prenom = resultSet.getString("prenom");
                int idUtilisateur = resultSet.getInt("id_utilisateur");
                String auteur = prenom + " " + nom;
                hashMap.put(idUtilisateur, auteur);
            }

            logger.info("Liste utilisateurs : sauvegarde des données dans le fichier START");

            FileOutputStream fos = new FileOutputStream("/tmp/mapUsers.tmp");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(hashMap);
            oos.close();

            logger.info("Liste utilisateurs : sauvegarde des données dans le fichier SUCCES");

        } catch (Exception e) {
            throw new CustomChangeException(e);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        this.resourceAccessor = resourceAccessor;
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

}
