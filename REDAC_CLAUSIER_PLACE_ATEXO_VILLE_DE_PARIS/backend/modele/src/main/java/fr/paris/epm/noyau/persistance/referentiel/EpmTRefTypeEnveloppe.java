package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeEnveloppe de la table "epm__t_ref_type_enveloppe"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeEnveloppe extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int TYPE_PAPIER = 1; // Enveloppe papier.
    public static final int TYPE_ELECTRONIQUE = 2; // Envelpope électronique.

}