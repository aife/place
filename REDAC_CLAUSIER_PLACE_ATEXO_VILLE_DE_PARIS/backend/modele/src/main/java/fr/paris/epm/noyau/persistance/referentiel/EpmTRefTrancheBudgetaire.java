package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTrancheBudgetaire de la table "epm__t_ref_tranche_budgetaire"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTrancheBudgetaire extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

}