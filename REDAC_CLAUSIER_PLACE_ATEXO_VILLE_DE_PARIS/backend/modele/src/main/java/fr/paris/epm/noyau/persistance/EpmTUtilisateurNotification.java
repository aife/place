package fr.paris.epm.noyau.persistance;

/**
 * Created by abdelmjid on 25/04/17.
 */
public class EpmTUtilisateurNotification extends EpmTAbstractObject  {

    /**
     * Identifiant
     */
    private int id;

    private EpmTUtilisateur utilisateur;
    private EpmTNotification notification;


    private boolean vue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EpmTUtilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(EpmTUtilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public EpmTNotification getNotification() {
        return notification;
    }

    public void setNotification(EpmTNotification notification) {
        this.notification = notification;
    }

    public boolean isVue() {
        return vue;
    }

    public void setVue(boolean vue) {
        this.vue = vue;
    }
}
