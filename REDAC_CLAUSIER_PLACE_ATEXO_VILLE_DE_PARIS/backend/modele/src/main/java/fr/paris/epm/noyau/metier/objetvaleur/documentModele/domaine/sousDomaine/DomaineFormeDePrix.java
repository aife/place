package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour les sous domaines
 * consultation.formeDePrix.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineFormeDePrix implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    private String txtFormePrixNonAllotieNonFractionnee;
    /**
     * "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    private String formePrix = "";

    private Integer idFormePrix;

    private Double estimationForfaitaireHT;
    
    private Double estimationForfaitaireTTC;
    
    private Date dateValeurForfaitaire;
    
    private String variationPrix;

    private String variationPrixPMPF;

    private String variationPrixPMPU;
    /**
     * A bon de commande / Autres prix unitairesA bon de commande / Autres prix unitaires
     */
    private String prixUnitaireBdCAutres;

    /**
     * A bon de commande ou A quantité définiee
     */
    private Integer idBonQuantite;
    /**
     * "Avec Min/max en " ou "Sans mini / maxi"
     */
    private String unitaireBDCMinMaxAvecSans;
    /**
     * En EUR HT.
     */
    private Double unitaireBDCMin;
    /**
     * En EUR HT.
     */
    private Double unitaireBDCMax;
    
    private Double estimationUnitaireHT;
    
    private Double estimationUnitaireTTC;
    
    private Date dateValeurUnitaire;
    
    private String typePrixUnitaire;

    /**
     * @return "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    public final String getTxtFormePrixNonAllotieNonFractionnee() {
        return txtFormePrixNonAllotieNonFractionnee;
    }

    /**
     * @param valeur "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    public final void setTxtFormePrixNonAllotieNonFractionnee(final String valeur) {
        this.txtFormePrixNonAllotieNonFractionnee = valeur;
    }

    /**
     * @return "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    public final String getFormePrix() {
        return formePrix;
    }

    public Integer getIdFormePrix() {
        return idFormePrix;
    }

    public void setIdFormePrix(Integer idFormePrix) {
        this.idFormePrix = idFormePrix;
    }

    /**
     * @param valeur "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    public final void setFormePrix(final String valeur) {
        this.formePrix = valeur;
    }

    public final Double getEstimationForfaitaireHT() {
        return estimationForfaitaireHT;
    }

    public final void setEstimationForfaitaireHT(final Double valeur) {
        this.estimationForfaitaireHT = valeur;
    }

    public final Double getEstimationForfaitaireTTC() {
        return estimationForfaitaireTTC;
    }

    public final void setEstimationForfaitaireTTC(final Double valeur) {
        this.estimationForfaitaireTTC = valeur;
    }

    public final Date getDateValeurForfaitaire() {
        return dateValeurForfaitaire;
    }

    public final void setDateValeurForfaitaire(final Date valeur) {
        this.dateValeurForfaitaire = valeur;
    }

    public final String getVariationPrix() {
        return variationPrix;
    }

    public final void setVariationPrix(final String valeur) {
        this.variationPrix = valeur;
    }
    /**
     * @return A bon de commande / Autres prix unitaires.
     */
    public final String getPrixUnitaireBdCAutres() {
        return prixUnitaireBdCAutres;
    }
    /**
     * @param valeur A bon de commande / Autres prix unitaires.
     */
    public final void setPrixUnitaireBdCAutres(final String valeur) {
        this.prixUnitaireBdCAutres = valeur;
    }

    public final String getUnitaireBDCMinMaxAvecSans() {
        return unitaireBDCMinMaxAvecSans;
    }

    public final void setUnitaireBDCMinMaxAvecSans(final String valeur) {
        unitaireBDCMinMaxAvecSans = valeur;
    }

    public final Double getUnitaireBDCMin() {
        return unitaireBDCMin;
    }

    public final void setUnitaireBDCMin(final Double valeur) {
        unitaireBDCMin = valeur;
    }

    public final Double getUnitaireBDCMax() {
        return unitaireBDCMax;
    }

    public final void setUnitaireBDCMax(final Double valeur) {
        this.unitaireBDCMax = valeur;
    }

    public final Double getEstimationUnitaireHT() {
        return estimationUnitaireHT;
    }

    public final void setEstimationUnitaireHT(final Double valeur) {
        this.estimationUnitaireHT = valeur;
    }

    public final Double getEstimationUnitaireTTC() {
        return estimationUnitaireTTC;
    }

    public final void setEstimationUnitaireTTC(final Double valeur) {
        this.estimationUnitaireTTC = valeur;
    }

    public final Date getDateValeurUnitaire() {
        return dateValeurUnitaire;
    }

    public final void setDateValeurUnitaire(final Date valeur) {
        this.dateValeurUnitaire = valeur;
    }

    public final String getTypePrixUnitaire() {
        return typePrixUnitaire;
    }

    public final void setTypePrixUnitaire(final String valeur) {
        this.typePrixUnitaire = valeur;
    }

    public String getVariationPrixPMPF() {
        return variationPrixPMPF;
    }

    public void setVariationPrixPMPF(String variationPrixPMPF) {
        this.variationPrixPMPF = variationPrixPMPF;
    }

    public String getVariationPrixPMPU() {
        return variationPrixPMPU;
    }

    public void setVariationPrixPMPU(String variationPrixPMPU) {
        this.variationPrixPMPU = variationPrixPMPU;
    }

    public Integer getIdBonQuantite() {
        return idBonQuantite;
    }

    public void setIdBonQuantite(Integer idBonQuantite) {
        this.idBonQuantite = idBonQuantite;
    }

}
