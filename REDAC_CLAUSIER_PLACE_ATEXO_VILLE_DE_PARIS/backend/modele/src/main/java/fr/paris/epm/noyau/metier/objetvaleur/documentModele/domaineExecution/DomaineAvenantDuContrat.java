package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine avenant du contrat.
 * @author GAO Xuesong
 */
public class DomaineAvenantDuContrat implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Référence de l’avenant
	 */
	private String referenceAvenant;
	
	/**
	 * Objet de l’avenant
	 */
	private String objetAvenant;
	
	/**
	 * Montant HT de l’avenant
	 */
	private Double montantAvenant;
	
	/**
	 * Date de notification de l’avenant
	 */
	private Date dateNotificationAvenant;

	
	public final String getReferenceAvenant() {
		return referenceAvenant;
	}

	public final void setReferenceAvenant(final String valeur) {
		this.referenceAvenant = valeur;
	}

	public final String getObjetAvenant() {
		return objetAvenant;
	}

	public final void setObjetAvenant(final String valeur) {
		this.objetAvenant = valeur;
	}

	public final Double getMontantAvenant() {
		return montantAvenant;
	}

	public final void setMontantAvenant(final Double valeur) {
		this.montantAvenant = valeur;
	}

	public final Date getDateNotificationAvenant() {
		return dateNotificationAvenant;
	}

	public final void setDateNotificationAvenant(final Date valeur) {
		dateNotificationAvenant = valeur;
	}
	
}
