/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele;

import fr.paris.epm.noyau.persistance.documents.EpmTDocumentModele;

import java.io.Serializable;

/**
 * Objet pour le suivi de la génération des {@link EpmTDocumentModele}
 * @author Léon Barsamian
 */
public class SuiviGenerationDocumentModele implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -5482235348531221695L;

    /**
     * Statut de la génération du document (OK / KO / vide si en cours).
     */
    private String statutGeneration;

    /**
     * Nom du fichier à envoyer à la servlet de récupération des fichiers.
     */
    private String nomFichierGenere;
    
    /**
     * Jeton de suivi de génération du document, utilisé pour le retour dans les modules.
     */
    private String token;
    
    /**
     * Nom du fichier affiché dans l'interface.
     */
    private String nomFichierRetour;

    /**
     * @return Statut de la génération du document (OK / KO / vide si en cours).
     */
    public final String getStatutGeneration() {
        return statutGeneration;
    }

    /**
     * @param valeur Statut de la génération du document (OK / KO / vide si en
     *            cours).
     */
    public final void setStatutGeneration(final String valeur) {
        this.statutGeneration = valeur;
    }

    /**
     * @return Nom du fichier à envoyer à la servlet de récupération des
     *         fichiers.
     */
    public final String getNomFichierGenere() {
        return nomFichierGenere;
    }

    /**
     * @param valeur Nom du fichier à envoyer à la servlet de récupération des
     *            fichiers.
     */
    public final void setNomFichierGenere(final String valeur) {
        this.nomFichierGenere = valeur;
    }

	/**
	 * @return Jeton de suivi de génération du document, utilisé pour le retour dans les modules.
	 */
	public final String getToken() {
		return token;
	}

	/**
	 * @param valeur Jeton de suivi de génération du document, utilisé pour le retour dans les modules.
	 */
	public final void setToken(final String valeur) {
		this.token = valeur;
	}

	/**
	 * @return Nom du fichier affiché dans l'interface.
	 */
	public String getNomFichierRetour() {
		return nomFichierRetour;
	}

	/**
	 * @param nomFichierRetour Nom du fichier affiché dans l'interface.
	 */
	public void setNomFichierRetour(String nomFichierRetour) {
		this.nomFichierRetour = nomFichierRetour;
	}
}
