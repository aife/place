package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient tous les domaines et/ou données de niveau 3 à être fusionné dans un
 * document du module exécution. Ce domaine contient des données de l'écran
 * Informations Principales du Contrat appartenant au module exécution
 * 
 * @author Rebeca Dantas
 */
public class DomaineInformationsPrincipales implements Serializable {

	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Oui si coché à contrat reconductible sur l'écran Informations
	 * Principales du contrat d'exécution
	 */
	private String reconductible;

	/**
	 * Nombre de reconductions du marché appartenant à l'écran Informations
	 * Principales du contrat du module exécution
	 */
	private int nbReconductions;

	/**
	 * Les modalités de reconduction du marché appartenant à l'écran
	 * Informations Principales du contrat du module exécution
	 */
	private String modalitesReconduction = "";

	/**
	 * Les options du marché appartenant à l'écran Informations Principales du
	 * contrat du module exécution
	 */
	private String options;

	/**
	 * Oui si coché la case de marchés à tranches de l'écran Informations
	 * Principales du contrat du module exécution
	 */
	private String marcheAvecTranches;

	/**
	 * Oui si coché la case de marchés contenant des lots techniques de l'écran
	 * Informations Principales du contrat du module exécution
	 */
	private String marcheAvecLotsTechniques;

	/**
	 * Mode gestion appartenant à l'écran Informations Principales du
	 * contrat du module exécution
	 */
	private String modeGestion;
	
	/**
	 * Forme de prix du marché. Il est vide si marcheAvecTranches est Oui
	 */
	private DomaineFormePrix formePrix;
	
	
	/**
	 * 
	 * @return true si coché à contrat reconductible sur l'écran Informations
	 *         Principales du contrat d'exécution
	 */
	public final String getReconductible() {
		return reconductible;
	}

	/**
	 * 
	 * @param valeur
	 *            : Oui si coché à contrat reconductible sur l'écran
	 *            Informations Principales du contrat d'exécution
	 */
	public final void setReconductible(final String valeur) {
		this.reconductible = valeur;
	}

	/**
	 * 
	 * @return Nombre de reconductions du marché appartenant à l'écran
	 *         Informations Principales du contrat du module exécution
	 */
	public final int getNbReconductions() {
		return nbReconductions;
	}

	/**
	 * 
	 * @param valeur
	 *            : Nombre de reconductions du marché appartenant à l'écran
	 *            Informations Principales du contrat du module exécution
	 */
	public final void setNbReconductions(final int valeur) {
		this.nbReconductions = valeur;
	}

	/**
	 * 
	 * @return Les modalités de reconduction du marché appartenant à l'écran
	 *         Informations Principales du contrat du module exécution
	 */
	public final String getModalitesReconduction() {
		return modalitesReconduction;
	}

	/**
	 * 
	 * @param valeur
	 *            : Les modalités de reconduction du marché appartenant à
	 *            l'écran Informations Principales du contrat du module
	 *            exécution
	 */
	public final void setModalitesReconduction(final String valeur) {
		this.modalitesReconduction = valeur;
	}

	/**
	 * 
	 * @return Les options du marché appartenant à l'écran Informations
	 *         Principales du contrat du module exécution
	 */
	public final String getOptions() {
		return options;
	}

	/**
	 * 
	 * @param valeur
	 *            : Les options du marché appartenant à l'écran Informations
	 *            Principales du contrat du module exécution
	 */
	public final void setOptions(final String valeur) {
		this.options = valeur;
	}

	/**
	 * 
	 * @return Oui si coché la case de marchés à tranches de l'écran
	 *         Informations Principales du contrat du module exécution
	 */
	public final String getMarcheAvecTranches() {
		return marcheAvecTranches;
	}

	/**
	 * 
	 * @param valeur
	 *            : Oui si coché la case de marchés à tranches de l'écran
	 *            Informations Principales du contrat du module exécution
	 */
	public final void setMarcheAvecTranches(final String valeur) {
		this.marcheAvecTranches = valeur;
	}

	/**
	 * 
	 * @return Oui si coché la case de marchés contenant des lots techniques de
	 *         l'écran Informations Principales du contrat du module exécution
	 */
	public final String getMarcheAvecLotsTechniques() {
		return marcheAvecLotsTechniques;
	}

	/**
	 * 
	 * @param valeur
	 *            : Oui si coché la case de marchés contenant des lots
	 *            techniques de l'écran Informations Principales du contrat du
	 *            module exécution
	 */
	public final void setMarcheAvecLotsTechniques(final String valeur) {
		this.marcheAvecLotsTechniques = valeur;
	}

	/**
	 * 
	 * @return appartenant à l'écran Informations Principales du
	 * contrat du module exécution
	 */
	public final String getModeGestion() {
		return modeGestion;
	}

	/**
	 * 
	 * @param modeGestion appartenant à l'écran Informations Principales du
	 * contrat du module exécution
	 */
	public final void setModeGestion(final String valeur) {
		this.modeGestion = valeur;
	}

	public final DomaineFormePrix getFormePrix() {
		return formePrix;
	}

	public final void setFormePrix(final DomaineFormePrix valeur) {
		this.formePrix = valeur;
	}


}
