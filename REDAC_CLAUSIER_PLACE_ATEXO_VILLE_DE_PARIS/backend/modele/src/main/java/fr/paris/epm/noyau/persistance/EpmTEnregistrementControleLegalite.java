package fr.paris.epm.noyau.persistance;

import java.util.Date;


/**
 * POJO hibernate - pour stocker l'informations sur l'enregistrement de
 * télétransmission au contrôle de la légalité.
 * @author GAO Xuesong
 */
public class EpmTEnregistrementControleLegalite extends EpmTAbstractObject {
    
    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808555L;
    
    /**
     * clé primaire
     */
    private int id;
    
    /**
     * date d'enregistrement du contrôle de l'égalité aval.
     */
    private Date dateEnrControleAval;
    
    /**
     * Numéro d'enregistrement du contrôle de l'égalité aval.
     */
    private String numEnrControleAval;
    
    /**
     * Date d'enregistrement du contrôle de légalité.
     */
    private Date dateEnrControleLegal; 

    /**
     * Numéro d'enregistrement du contrôle de l'égalité.
     */
    private String numEnrControleLegal;
    
    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id id to set
     */
    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    /**
     * @return dateEnrControleAval
     */
    public Date getDateEnrControleAval() {
        return dateEnrControleAval;
    }

    /**
     * @return numEnrControleAval
     */
    public String getNumEnrControleAval() {
        return numEnrControleAval;
    }

    /**
     * @return dateEnrControleLegal
     */
    public Date getDateEnrControleLegal() {
        return dateEnrControleLegal;
    }

    /**
     * @return numEnrControleLegal
     */
    public String getNumEnrControleLegal() {
        return numEnrControleLegal;
    }

    /**
     * @param dateEnrControleAval dateEnrControleAval to set
     */
    public void setDateEnrControleAval(final Date valeur) {
        this.dateEnrControleAval = valeur;
    }

    /**
     * @param numEnrControleAval numEnrControleAval to set
     */
    public void setNumEnrControleAval(final String valeur) {
        this.numEnrControleAval = valeur;
    }

    /**
     * @param dateEnrControleLegal dateEnrControleLegal to set
     */
    public void setDateEnrControleLegal(final Date valeur) {
        this.dateEnrControleLegal = valeur;
    }

    /**
     * @param numEnrControleLegal numEnrControleLegal to set
     */
    public void setNumEnrControleLegal(final String valeur) {
        this.numEnrControleLegal = valeur;
    }


    
}
