package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefActionConfirmationAttributionChgtAttributaire de la table "epm__t_ref_action_confirmation_attribution_chgt_attributaire"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefActionConfirmationAttributionChgtAttributaire extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

}