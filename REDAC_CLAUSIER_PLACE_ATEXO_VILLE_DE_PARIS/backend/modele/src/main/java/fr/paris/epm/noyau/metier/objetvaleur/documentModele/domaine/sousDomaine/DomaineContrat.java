package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import fr.paris.epm.noyau.persistance.EpmTAttributaire;
import fr.paris.epm.noyau.persistance.EpmTContrat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * attribution.contrat.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineContrat implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
    
    private List<DomaineAttributaire> attributaires;

    private String raisonSocialeAttributaire;

    private String SIRETAttributaire;
    
    private List<DomaineTrancheAttribution> listeTranches;
    
    /**
     * prix consolidé au moment de l'attribution
     */
    private Double bcMaxConsolidesHt;
    /**
     * prix consolidé au moment de l'attribution
     */
    private Double bcMaxConsolidesTtc;
    /**
     * prix consolidé au moment de l'attribution
     */
    private Double bcMinConsolidesHt;
    /**
     * prix consolidé au moment de l'attribution
     */
    private Double bcMinConsolidesTtc;
    /**
     * prix consolidé au moment de l'attribution.
     */
    private Double pfDqeConsolidesHt;

    /**
     * prix consolidé au moment de l'attribution
     */
    private Double pfDqeConsolidesTtc;
    
    private List<String> numerosContrat;
    private String numeroContrat;

    private String serviceBeneficiaire;
    private String montant;

    public DomaineContrat(){

    }

    public DomaineContrat(EpmTContrat contrat) {
        this.setNumeroContrat(contrat.getNumeroContrat());
        this.setServiceBeneficiaire(contrat.getDirectionServiceResponsable().getLibelle());

        List<DomaineAttributaire> attributaires = new ArrayList<DomaineAttributaire>();

        for (EpmTAttributaire attributaire : contrat.getAttributaires()) {
            attributaires.add(new DomaineAttributaire(attributaire));
        }
        this.setAttributaires(attributaires);
        this.setMontant(String.valueOf(contrat.getMontantAttribueHT()));
    }

    public final String getRaisonSocialeAttributaire() {
        return raisonSocialeAttributaire;
    }

    public final void setRaisonSocialeAttributaire(final String valeur) {
        raisonSocialeAttributaire = valeur;
    }

    public final String getsIRETAttributaire() {
        return SIRETAttributaire;
    }

    public final void setsIRETAttributaire(final String valeur) {
        this.SIRETAttributaire = valeur;
    }

    public final List<DomaineTrancheAttribution> getListeTranches() {
        return listeTranches;
    }

    public final void setListeTranches(
            List<DomaineTrancheAttribution> listeTranches) {
        this.listeTranches = listeTranches;
    }

    /**
     * @return the bcMaxConsolidesHt
     */
    public final Double getBcMaxConsolidesHt() {
        return bcMaxConsolidesHt;
    }

    /**
     * @param bcMaxConsolidesHt the bcMaxConsolidesHt to set
     */
    public final void setBcMaxConsolidesHt(final Double valeur) {
        this.bcMaxConsolidesHt = valeur;
    }

    /**
     * @return the bcMaxConsolidesTtc
     */
    public final Double getBcMaxConsolidesTtc() {
        return bcMaxConsolidesTtc;
    }

    /**
     * @param bcMaxConsolidesTtc the bcMaxConsolidesTtc to set
     */
    public final void setBcMaxConsolidesTtc(final Double valeur) {
        this.bcMaxConsolidesTtc = valeur;
    }

    /**
     * @return the bcMinConsolidesHt
     */
    public final Double getBcMinConsolidesHt() {
        return bcMinConsolidesHt;
    }

    /**
     * @param bcMinConsolidesHt the bcMinConsolidesHt to set
     */
    public final void setBcMinConsolidesHt(final Double valeur) {
        this.bcMinConsolidesHt = valeur;
    }

    /**
     * @return the bcMinConsolidesTtc
     */
    public final Double getBcMinConsolidesTtc() {
        return bcMinConsolidesTtc;
    }

    /**
     * @param bcMinConsolidesTtc the bcMinConsolidesTtc to set
     */
    public final void setBcMinConsolidesTtc(final Double valeur) {
        this.bcMinConsolidesTtc = valeur;
    }

    /**
     * @return the pfDqeConsolidesHt
     */
    public final Double getPfDqeConsolidesHt() {
        return pfDqeConsolidesHt;
    }

    /**
     * @param pfDqeConsolidesHt the pfDqeConsolidesHt to set
     */
    public final void setPfDqeConsolidesHt(final Double valeur) {
        this.pfDqeConsolidesHt = valeur;
    }

    /**
     * @return the pfDqeConsolidesTtc
     */
    public final Double getPfDqeConsolidesTtc() {
        return pfDqeConsolidesTtc;
    }

    /**
     * @param pfDqeConsolidesTtc the pfDqeConsolidesTtc to set
     */
    public final void setPfDqeConsolidesTtc(final Double valeur) {
        this.pfDqeConsolidesTtc = valeur;
    }

    /**
     * @return the numeroContrat
     */
    public final List<String> getNumerosContrat() {
        return numerosContrat;
    }

    /**
     * @param numeroContrat the numeroContrat to set
     */
    public final void setNumerosContrat(final List<String> valeur) {
        this.numerosContrat = valeur;
    }

    /**
     * @return the attributaires
     */
    public final List<DomaineAttributaire> getAttributaires() {
        return attributaires;
    }

    /**
     * @param attributaires the attributaires to set
     */
    public final void setAttributaires(final List<DomaineAttributaire> valeur) {
        this.attributaires = valeur;
    }

    public String getServiceBeneficiaire() {
        return serviceBeneficiaire;
    }

    public void setServiceBeneficiaire(String serviceBeneficiaire) {
        this.serviceBeneficiaire = serviceBeneficiaire;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(String numeroContrat) {
        this.numeroContrat = numeroContrat;
    }
}
