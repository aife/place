package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.ArrayList;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour une element du sous
 * domaine commission.domaineOrdreDuJour.listeConsultationOdj.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineConsultationOdj extends DomaineLotOuConsultationOdj {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private List<DomaineLotOdj> listeLotOdj = new ArrayList<DomaineLotOdj>();

    private String directionServiceResponsable;

    private String directionServiceResponsableNiv1;

    private String intituleConsultation;

    private String referenceConsultation;

    private String typeProcedureConsultation;

    private boolean lotDissocie = false;

    private boolean reunionOuvertureOffre = false;

    private boolean reunionAttribution = false;
    
    private boolean reunionSelection = false;

    private String heure;

    private int ordrePassage;

    private String directionServiceBeneficiaire;

    private String pouvoirAdjudicateur;

    private boolean directionAchat = false;

    private String procedureIntituleDetaillee;

    private String referenceDeliberationAmont;

    private String dateDeliberationAmont;

    private String numeroDossier;

    private int nbPlisPapier;

    private int nbPlisElectronique;
    
    /**
     * True si avenant.
     */
    private boolean avenant;
    
    /**
     * True si DSP.
     */
    private boolean dsp;
    
    /**
     * Le libellé de l'étape affiché dans le calendrier.
     */
    private String libelleEtape;
    
    private DomaineCaracteristiquesPrincipales caracteristiquesAvenant;

    public final List<DomaineLotOdj> getListeLotOdj() {
        return listeLotOdj;
    }

    public final void setListeLotOdj(final List<DomaineLotOdj> valeur) {
        this.listeLotOdj = valeur;
    }

    public final String getDirectionServiceResponsable() {
        return directionServiceResponsable;
    }

    public final void setDirectionServiceResponsable(final String valeur) {
        this.directionServiceResponsable = valeur;
    }

    public final String getIntituleConsultation() {
        return intituleConsultation;
    }

    public final void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }

    public final String getReferenceConsultation() {
        return referenceConsultation;
    }

    public final void setReferenceConsultation(final String valeur) {
        this.referenceConsultation = valeur;
    }

    public final String getTypeProcedureConsultation() {
        return typeProcedureConsultation;
    }

    public final void setTypeProcedureConsultation(final String valeur) {
        this.typeProcedureConsultation = valeur;
    }

    public final boolean isLotDissocie() {
        return lotDissocie;
    }

    public final void setLotDissocie(final boolean valeur) {
        this.lotDissocie = valeur;
    }

    public final boolean isReunionOuvertureOffre() {
        return reunionOuvertureOffre;
    }

    public final void setReunionOuvertureOffre(final boolean valeur) {
        this.reunionOuvertureOffre = valeur;
    }

    public final boolean isReunionAttribution() {
        return reunionAttribution;
    }

    public final void setReunionAttribution(final boolean valeur) {
        this.reunionAttribution = valeur;
    }

    public final String getHeure() {
        return heure;
    }

    public final void setHeure(final String valeur) {
        this.heure = valeur;
    }

    public final String getDirectionServiceResponsableNiv1() {
        return directionServiceResponsableNiv1;
    }

    public final void setDirectionServiceResponsableNiv1(final String valeur) {
        this.directionServiceResponsableNiv1 = valeur;
    }

    public final int getOrdrePassage() {
        return ordrePassage;
    }

    public final void setOrdrePassage(final int valeur) {
        this.ordrePassage = valeur;
    }

    public final String getDirectionServiceBeneficiaire() {
        return directionServiceBeneficiaire;
    }

    public final void setDirectionServiceBeneficiaire(final String valeur) {
        this.directionServiceBeneficiaire = valeur;
    }

    public final String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    public final void setPouvoirAdjudicateur(final String valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    public final boolean isDirectionAchat() {
        return directionAchat;
    }

    public final void setDirectionAchat(final boolean valeur) {
        this.directionAchat = valeur;
    }

    public final String getProcedureIntituleDetaillee() {
        return procedureIntituleDetaillee;
    }

    public final void setProcedureIntituleDetaillee(final String valeur) {
        this.procedureIntituleDetaillee = valeur;
    }

    public final String getReferenceDeliberationAmont() {
        return referenceDeliberationAmont;
    }

    public final void setReferenceDeliberationAmont(final String valeur) {
        this.referenceDeliberationAmont = valeur;
    }

    public final String getDateDeliberationAmont() {
        return dateDeliberationAmont;
    }

    public final void setDateDeliberationAmont(final String valeur) {
        this.dateDeliberationAmont = valeur;
    }

    public final String getNumeroDossier() {
        return numeroDossier;
    }

    public final void setNumeroDossier(final String valeur) {
        this.numeroDossier = valeur;
    }

    public final int getNbPlisPapier() {
        return nbPlisPapier;
    }

    public final void setNbPlisPapier(final int valeur) {
        this.nbPlisPapier = valeur;
    }

    public final int getNbPlisElectronique() {
        return nbPlisElectronique;
    }

    public final void setNbPlisElectronique(final int valeur) {
        this.nbPlisElectronique = valeur;
    }

    /**
     * @return Le libellé de l'étape affiché dans le calendrier.
     */
    public final String getLibelleEtape() {
        return libelleEtape;
    }

    /**
     * @param valeur Le libellé de l'étape affiché dans le calendrier.
     */
    public final void setLibelleEtape(final String valeur) {
        this.libelleEtape = valeur;
    }

    /**
     * @param valeur true si avenant.
     */
    public final void setAvenant(final boolean valeur) {
        this.avenant = valeur;
    }

    /**
     * @param valeur true si dsp
     */
    public final void setDsp(final boolean valeur) {
        this.dsp = valeur;
    }

    public final DomaineCaracteristiquesPrincipales getCaracteristiquesAvenant() {
        return caracteristiquesAvenant;
    }

    public final void setCaracteristiquesAvenant(final DomaineCaracteristiquesPrincipales valeur) {
        this.caracteristiquesAvenant = valeur;
    }

    public final boolean isAvenant() {
        return avenant;
    }

    public final boolean isDsp() {
        return dsp;
    }

    public final boolean isReunionSelection() {
        return reunionSelection;
    }

    public final void setReunionSelection(final boolean valeur) {
        this.reunionSelection = valeur;
    }


}
