/**
 * 
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

/**
 * @author Mounthei
 */
public class EpmTCpv extends EpmTAbstractObject implements Comparable<EpmTCpv>, Cloneable {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identifiant base de données du cpv.
     */
    private int id;

    /**
     * Code CPV renvoyé par le référentiel de CPV.
     */
    private String codeCpv;
    /**
     * Libelle CPV renvoyé par le référentiel de CPV.
     */
    private String libelleCpv;
    /**
     * Indique si le CPV est principal ou secondaire.
     */
    private boolean principal;

    /**
     * Comparaison de l'objet courant avec other par rapport à l'id en base
     * de données.
     * @param other l'autre objet à comparer, du type EpmTCpv.
     * @return 0 si les id sont égaux <br/> inférieur à 0 si l'id courant
     *         est inférieur à l'id de other <br/>. supérieur à 0 si l'id
     *         courant est supérieur à l'id de other <br/>.
     */
    public final int compareTo(final EpmTCpv autre) {
        Integer idCourant = this.id;
        Integer idAutre = autre.getId();
        return idCourant.compareTo(idAutre);
    }

    // Méthodes
    /**
     * Utilise uniquement le libelle.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (this.libelleCpv != null) {
            result += Constantes.PREMIER + libelleCpv.hashCode();
        }

        return result;
    }

    /**
     * Comparaison sur tous les champs.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTCpv autre = (EpmTCpv) obj;
        boolean boolId = (this.id == autre.getId());
        boolean boolCodeCpv = (this.codeCpv.equals(autre.getCodeCpv()));
        boolean boolLibelleCpv =
                (this.libelleCpv.equals(autre.getLibelleCpv()));

        boolean boolPrincipal =
                (this.isPrincipal() == autre.isPrincipal());
        return (boolId && boolCodeCpv && boolLibelleCpv && boolPrincipal);
    }

    public EpmTCpv clone() throws CloneNotSupportedException{
        EpmTCpv cpv = new EpmTCpv();
        cpv.setId(0);
        cpv.setCodeCpv(codeCpv);
        cpv.setLibelleCpv(libelleCpv);
        cpv.setPrincipal(principal);
        return cpv;
    }

    /**
     * @return le code du cpv tel qu'il est défini au niveau du référentiel
     *         des CPV.
     */
    public String getCodeCpv() {
        return codeCpv;
    }

    /**
     * @param valeur assigne le code CPV.
     */
    public void setCodeCpv(final String valeur) {
        this.codeCpv = valeur;
    }

    /**
     * @return l'id base de données associé à ce cpv.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur l'id base de données à associer à ce cpv.
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return le libelle du cpv tel qu'il est défini au niveau du
     *         référentiel de CPV.
     */
    public String getLibelleCpv() {
        return libelleCpv;
    }

    /**
     * @param valeur le libelle du CPV.
     */
    public void setLibelleCpv(final String valeur) {
        this.libelleCpv = valeur;
    }

    /**
     * @return true si le cpv est principal, false sinon.
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * @param valeur true si on doit indiquer que le cpv courant est
     *            principal, false sinon.
     */
    public void setPrincipal(final boolean valeur) {
        this.principal = valeur;
    }

}
