package fr.paris.epm.noyau.persistance.referentiel;

import java.util.Objects;

/**
 * Classe abstraite EpmTReferentielByOrganismeAbstract est la racine d'héritation pour toutes les référentiels par organisme.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class EpmTReferentielByOrganismeAbstract extends BaseEpmTRefReferentiel implements EpmTReferentielByOrganisme {

    /**
     * L'organisme auquel appartient la référentiels
     */
    private EpmTRefOrganisme epmTRefOrganisme;

    @Override
    public EpmTRefOrganisme getEpmTRefOrganisme() {
        return epmTRefOrganisme;
    }

    @Override
    public void setEpmTRefOrganisme(EpmTRefOrganisme epmTRefOrganisme) {
        this.epmTRefOrganisme = epmTRefOrganisme;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj))
            return false;

        EpmTReferentielByOrganismeAbstract that = (EpmTReferentielByOrganismeAbstract) obj;
        return Objects.equals(epmTRefOrganisme, that.epmTRefOrganisme);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), epmTRefOrganisme != null ? epmTRefOrganisme.getId() : null);
    }

}
