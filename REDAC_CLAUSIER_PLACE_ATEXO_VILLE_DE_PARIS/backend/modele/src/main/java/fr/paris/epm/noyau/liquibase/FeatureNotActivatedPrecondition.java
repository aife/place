package fr.paris.epm.noyau.liquibase;

import fr.paris.epm.noyau.feature.FeatureUtil;
import liquibase.database.Database;
import liquibase.exception.CustomPreconditionErrorException;
import liquibase.exception.CustomPreconditionFailedException;
import liquibase.precondition.CustomPrecondition;

/**
 * Permet de conditionner le lancement d'un script à l'activation d'une feature (profile-Spring)
 * Usage :
 * <preConditions onFail="MARK_RAN">
 *            <customPrecondition className="fr.paris.epm.noyau.liquibase.FeatureNotActivatedPrecondition">
 *                <param name="hasFeatureNotActivated" value="redac"/>
 *            </customPrecondition>
 *        </preConditions>
 */
public class FeatureNotActivatedPrecondition implements CustomPrecondition {

    private String hasFeatureNotActivated;

    @Override
    public void check(Database database) throws CustomPreconditionFailedException, CustomPreconditionErrorException {
        try {
            FeatureUtil.Feature featureNotActivated = FeatureUtil.Feature.getByName(hasFeatureNotActivated);
            if (featureNotActivated == null) {
                throw new IllegalArgumentException("Feature precondition failed : Le feature " + hasFeatureNotActivated + " n'existe pas");
            }
            if (FeatureUtil.check(featureNotActivated)) {
                throw new CustomPreconditionFailedException("Feature precondition failed : Le feature " + hasFeatureNotActivated + " est activé");
            }
        } catch (CustomPreconditionFailedException e) {
            throw e;
        } catch (Exception e) {
            throw new CustomPreconditionErrorException("Une erreur est survenue lors de la vérification de precondition liquibase feature", e);
        }
    }

    public void setHasFeatureNotActivated(String valeur) {
        this.hasFeatureNotActivated = valeur;
    }
}
