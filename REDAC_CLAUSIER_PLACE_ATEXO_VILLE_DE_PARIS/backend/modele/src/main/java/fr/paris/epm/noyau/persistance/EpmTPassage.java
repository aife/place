/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import java.util.HashMap;
import java.util.Map;

/**
 * Pojo hibernate composant passage.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTPassage extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Ordre de passage par id de bloc (budLotConsultation).
     */
    private Map ordrePassage = new HashMap(0);

    /**
     * Heure de passage par id de bloc (budLotConsultation).
     */
    private Map heurePassage = new HashMap(0);





    // Accesseurs
    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return heure de passage par id de bloc (budLotConsultation)
     */
    public Map getHeurePassage() {
        return heurePassage;
    }


    /**
     * @param valeur heure de passage par id de bloc (budLotConsultation)
     */
    public void setHeurePassage(final Map valeur) {
        this.heurePassage = valeur;
    }


    /**
     * @return ordre de passage par id de bloc (budLotConsultation)
     */
    public Map getOrdrePassage() {
        return ordrePassage;
    }


    /**
     * @param valeur ordre de passage par id de bloc (budLotConsultation)
     */
    public void setOrdrePassage(final Map valeur) {
        this.ordrePassage = valeur;
    }
}
