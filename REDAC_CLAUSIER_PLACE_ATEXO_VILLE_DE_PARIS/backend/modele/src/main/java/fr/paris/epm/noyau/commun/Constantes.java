/**
 * $Id$
 */
package fr.paris.epm.noyau.commun;

import fr.paris.epm.noyau.persistance.EpmTDocument;
import fr.paris.epm.noyau.persistance.EpmTDocumentConserve;
import fr.paris.epm.noyau.persistance.EpmTDocumentEchange;

/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class Constantes {

    /**
     * Constructeur vide.
     */
    private Constantes() {
    }

    public static final String PATH_OUVERTURE_OFFRE = "ouvertureOffreFusionne";
    public static final String PATH_OUVERTURE_CANDIDATURE = "ouvertureCandidatureFusionne";
    public static final String PATH_OUVERTURE_OFFRE_CANDIDATURE = "ouvertureOffreCandidatureFusionne";
    public static final String PATH_OUVERTURE_PROJET = "ouvertureProjetFusionne";
    public static final String PATH_OUVERTURE_REPONSE = "ouvertureReponseFusionne";
    public static final String PATH_OUVERTURE_OFFRE_ANONYMAT = "ouvertureOffreAnonymatFusionne";
    public final static String ID_CONSULTATION_PARAM = "idCons";

    /**
     * Valeur pour les vérifications
     */
    public static final String OUI = "oui";
    
    public static final String NON = "non";
    
    public static final String TRUE = "true";
    
    /**
     * Valeur pour le hashCode.
     */
    public static final int PREMIER = 31;

    /**
     * Valeur pour le hashCode.
     */
    public static final int PREMIER2 = 1231;

    /**
     * Valeur pour le hashCode.
     */
    public static final int PREMIER3 = 1237;

    /**
     * Valeur à retourner pour un entier nul.
     */
    public static final int ENTIER_NUL = -1;

    /**
     * Marqueur de phase 1 (phase initiale de procédure restreinte, ..).
     */
    public static final String PHASE_1 = "PHASE_1";

    /**
     * Marqueur la phase 2 uniquement (cas général). "Null" correspond également
     * à cette marque.
     */
    public static final String PHASE_2 = "PHASE_2";

    /**
     * Marqueur la phase 3 uniquement (cas général).
     */
    public static final String PHASE_3 = "PHASE_3";

    /**
     * Marqueur la phase attribution
     */
    public static final String PHASE_ATTRIBUTION = "ATTRIBUTION";

    /**
     * Marqueur pour les deux phases.
     */
    public static final String PHASE_1_ET_2 = "PHASE_1_ET_2";

    /**
     * Type de document utilisé lors du chargement des documents liés à une
     * consultation. Ici document normal {@link EpmTDocument}.
     */
    public static final int TYPE_DOC_NORMAL = 0;

    /**
     * Type de document utilisé lors du chargement des documents liés à une
     * consultation. Ici document conservé {@link EpmTDocumentConserve}.
     */
    public static final int TYPE_DOC_CONSERVE = 1;

    /**
     * Type de document utilisé lors du chargement des documents liés à une
     * consultation. Ici document échangé {@link EpmTDocumentEchange}.
     */
    public static final int TYPE_DOC_ECHANGE = 2;
    /**
     * Type d'annonce pour demat.
     */
    public static final int TYPE_ANNONCE_DEMAT = 1;
    /**
     * Type d'annonce pour boamp/joue.
     */
    public static final int TYPE_ANNONCE_BOAMP_JOUE = 2;
    /**
     * Type d'annonce pour le moniteur.
     */
    public static final int TYPE_ANNONCE_MONITEUR = 3;

    public static final String CONNECTEUR_ID_CONSULTATION = "idConsultation";

    public static final String CONNECTEUR_MESSAGE_TYPE = "messageType";

    public static final String CONNECTEUR_PHASE = "phase";
    public static final String CONNECTEUR_ID_PUBLICATION = "idPublication";
    public static final String CONNECTEUR_CLOS = "statusClos";

    public static final String CONNECTEUR_AAPC = "AAPC";
    public static final String CONNECTEUR_PDCE = "PDCE";
    public static final String CONNECTEUR_MDCE = "MDCE";

    public static final String CONNECTEUR_NUMERO_CONSULTATION = "numeroConsultation";
    /**
     * utilisé dans la session du workflow.
     */
    public static final String WORKFLOW_CONSULTATION = "CONSULTATION";

    /**
     * utilisé dans las session du workflow.
     */
    public static final String WORKFLOW_AVENANT = "AVENANT";
    /**
     * Id du lot dissocié.
     */
    public static final String WORKFLOW_ID_LOT_DISSOCIE = "idLotDissocie";
    
    /**
     * Id du organisme associé à la consultation
     */
    public static final String WORKFLOW_ID_ORGANISME = "idOrganisme";

    /**
     * nom du GIM workflow definie dans Spring.
     */
    public static final String WORKFLOW_SERVICE = "workflowService";

    /**
     * transition non definie.
     */
    public static final String WORKFLOW_TRANSITION_NON_DEFINIE = "nonDefini";

    /**
     * nom du GIM administration defini dans String.
     */
    public static final String ADMINISTRATION_BEAN_NAME = "administrationService";

    /**
     * nom du GIM consultation defini dans String.
     */
    public static final String CONSULTATION_BEAN_NAME = "consultationService";

    /**
     * nom du GIM alertes defini dans String.
     */
    public static final String GESTIONNAIRE_ALERTES_BEAN_NAME = "gestionnaireAlertes";

    /**
     * nom du GIM calendrier defini dans String.
     */
    public static final String CALENDRIER_BEAN_NAME = "calendrierGIMTarget";

    /**
     * nom du GIM marché go defini dans Spring.
     */
    public static final String ECHANGE_GO_RECU_TRANCHE_SERVICE = "recuTrancheGoService";

    /**
     * nom du GIM tranche go defini dans Spring.
     */
    public static final String ECHANGE_GO_RECU_MARCHE_SERVICE = "recuMarcheGoService";
    
    /**
     * nom du GIM tranche go defini dans Spring.
     */
    public static final String ECHANGE_GO_ENVOI_MARCHE_SERVICE = "envoiMarcheGoService";
    
    /**
     * Entier utilisé pour la génération du code de consultation pour la partie
     * pouvoir adjudicateur.
     */
    public static final int GEN_NUMERO_CONSULT_PA = 1;

    /**
     * Entier utilisé pour la génération du code consultation.
     */
    public static final int GEN_NUMERO_CONSULT_PARTIE_2 = 2;

    /**
     * Entier utilisé pour la génération du code consultation.
     */
    public static final int GEN_NUMERO_CONSULT_PARTIE_4 = 5;

    /**
     * Entier utilisé pour la génération du code consultation.
     */
    public static final int GEN_NUMERO_DSP_PARTIE_4 = 4;

    
    /**
     * Coorespond à l'acronyme du pouvoir adjudicateur de type marie d'arrondissement
     */
    public static final String MAIRIE_ARRONDISSEMENT = "M";
   
    /**
     * utilisé pour determiner si la consultation est allotie.
     */
    public static final String CONSULTATION_ALLOTIE = "oui";
    
    public static final String MODIFICATION_REFERENTIEL = "MODIFICATION_REFERENTIEL";

    /**
     * Pour LOG les transferts de depots.
     */
    public static final String LOG_TRANSFERT_DEPOT = "logTransfertDepot";
    
    /**
     * Pour logguer l'ouverture des plis electronique.
     */
    public static final String LOG_OUVERTURE_PLIS_ELECTRONIQUES = "Ouverture plis electroniques : ";
    
    /**
     * Definit les diferents types de templates lies a une reunion.
     */
    public enum ReferenceEmplacementEnum { ODJ, CONVOCATION};

    /**
     * Clef de parametrage indiquant la saisie libre du numero de consultation.
     */
    public static final String PARAMETRAGE_CONSULTATION_NUMERO_SAISIE_LIBRE = "consultation.numero.saisieLibre";
    
    /**
     * Clef de parametrage indiquant la saisie libre du numero de DSP.
     */
    public static final String PARAMETRAGE_DSP_NUMERO_SAISIE_LIBRE = "dsp.numero.saisieLibre";

    /**
     * Clef de parametrage correspondant a la regex validant le format de numero
     * de consultation.
     */
    public static final String PARAMETRAGE_CONSULTATION_NUMERO_SAISIE_LIBRE_REGEX = "consultation.numero.saisieLibre.regex";
    
    /**
     * Clef de parametrage correspondant a la regex validant le format de numero
     * de DSP.
     */
    public static final String PARAMETRAGE_DSP_NUMERO_SAISIE_LIBRE_REGEX = "dsp.numero.saisieLibre.regex";
    
    /**
     * Clef de parametrage correspondant au format de generation de numero de
     * consultation.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_CONSULTATION = "formatNumero.consultation";


    /**
     * Clef de parametrage correspondant à la taille de l'objet de consultation
     */
    public static final String PARAMETRAGE_TAILLE_OBJET_CONSULTATION = "redaction.taille.limite.consultation.objet";

    /**
     * Clef de parametrage correspondant à la taille de l'intitulé de la consultation
     */

    public static final String PARAMETRAGE_TAILLE_INTITULE_CONSULTATION = "redaction.taille.limite.consultation.intitule";

    public static final String TYPE_NUMEROTATION_CONTRAT = "type.numerotation.contrat";

    /**
     * Clef de parametrage correspondant au format de generation de numero de
     * DSP.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_DSP = "formatNumero.dsp";
    
    /**
     * Clef de parametrage correspondant au format de generation de numero de
     * marche.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_MARCHE = "formatNumero.marche";
    
    /**
     * Clef de parametrage correspondant au format de generation de numero. Il
     * indique si le chrono annuel est commun à tous les pouvoirs adjudicateurs
     * ou par pouvoir adjudicateur
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_MARCHE_PAR_PA = "formatNumero.marche.parPA";
    
    /**
     * Clef de parametrage correspondant au format de generation de numero de
     * marche d'un accord cadre.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_MARCHE_ACCORD_CADRE = "formatNumero.marche.accordCadre";
    
    /**
     * Clef de parametrage correspondant au format de generation de numero de
     * marche d'une suite a acoord cadre.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_MARCHE_SUITE_A_ACCORD_CADRE = "formatNumero.marche.suiteAAccordCadre";
    
    /**
     * Clef de parametrage correspondant a la chaine devant prefixee le numero
     * de relance dans le numero de consultation.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_CONSULTATION_RELANCE_PREFIX = "formatNumero.consultation.numeroRelance.prefix";
    
    /**
     * Clef de parametrage indiquant si le numero de relance doit apparaitre
     * dans le numero de consultation s'il vaut 0.
     */
    public static final String PARAMETRAGE_FORMAT_NUMERO_CONSULTATION_RELANCE_AFFICHER_SI_ZERO = "formatNumero.consultation.numeroRelance.afficherSiZero";
    
    /**
     * Clef de parametrage indiquant si l'activation du webservice GDA / CORIOLIS est activé ou pas
     */
    public static final String PARAMETRAGE_ACTIVATION_WEBSERVICE_ATTRIBUTION_HANDLER = "activation.webserviceAttribution";
    
    public static final String NUMERO_CONTRAT = "numeroContrat";
    
    public static final String CHRONO_ACCORD_CADRE = "chronoAccordCadre";

	public static final String ENVELOPPE_TYPE_CANDIDATURE = "CANDIDATURE";
	
	public static final String ENVELOPPE_TYPE_OFFRE = "OFFRE";
	
	public static final String ENVELOPPE_TYPE_ANONYMAT = "ANONYMAT";
    
	public static final String TYPE_PLI_PRINCIPALE = "PRINCIPALE";
	
	public static final String TYPE_PLI_ACTE_ENGAGEMENT = "ACTE_ENGAGEMENT";
	
	public static final String CREATEUR_RSEM = "RSEM";
	
    public static final String CERT_TYPE_CANDIDATURE = "CANDIDATURE";
    
    public static final String CERT_TYPE_OFFRE = "OFFRE";

    public static final String CERT_TYPE_REPONSE = "REPONSE";
    
    public static final String CERT_TYPE_ANONYMAT = "ANONYMAT";
    
	public static final String ACTIVATION_WEBSERVICE_CONNECTEUR_DEMAT = "activation.webserviceConnecteurDemat";

    public static final String LOGO_DEFAULT = "logo.default";

    /**
     * Expression régulière destinée la validation des champs de date au format
     * dd/MM/yyyy.
     */
    public static final String DATE_REGEX_dd_MM_yyyy = "(0[1-9]|[1-9]|[12][0-9]|3[01])/"
            + "(0[1-9]|1[012]|[1-9])/(19|20|21|22)\\d{2}";

    public static final String DATE_REGEX_dd_MM_yyyy_HH_mm = "(0[1-9]|[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012]|[1-9])/(19|20|21|22)\\d{2} (([0-1][0-9])|([2][0-3])):([0-5][0-9])";

    /**
     * Retour virgule.
     */
    public static final String COTE = "'";

    /**
     * Retour slache virgule.
     */
    public static final String ENCODE_COTE = "''";
    
    /**
     * Actions possibles correspondant a la colonne "action d'origine" dans
     * l'ecran d'historique des modifications du DCE.
     */
    public static enum HistoriqueDceActionOrigineEnum {

        MISE_EN_LIGNE_DCE("MISE_EN_LIGNE_DCE"), AJUSTEMENT("AJUSTEMENT");

        private String valeur;

        private HistoriqueDceActionOrigineEnum(String val) {
            this.valeur = val;
        }

        public String getValeur() {
            return valeur;
        }
        
        public boolean valeurEquals(String cmp) {
            return getValeur().equals(cmp);
        }
    }

    /**
     * Actions possibles consernant l'ajout ou la supression d'un fichier devant
     * etre sauvegarde dans l'historique de modification du DCE.
     */
    public static enum HistoriqueDceDocumentActionEnum {

        AJOUT("AJOUT"), SUPPRESSION("SUPPRESSION");

        private String valeur;

        private HistoriqueDceDocumentActionEnum(String val) {
            this.valeur = val;
        }

        public String getValeur() {
            return valeur;
        }

        public boolean valeurEquals(String cmp) {
            return getValeur().equals(cmp);
        }
    }
    
    /**
     * Type des documents joints dans l'ecran des questions/reponses.
     */
    public static String TYPE_DOCUMENT_QUESTION = "question";

    /**
     * Retour de test de dechiffrement possibles : ok, absent ou corrompu.
     */
    public static enum TestDechiffrementEnum {

        OK("ok"), PLIS_NON_CHIFFRE_ABSENT("plis non chiffre absent"), PLIS_DECHIFFRE_ABSENT(
                "plis dechiffre absent"), CORROMPU("corrompu"), ECHEC("echec");

        private String valeur;

        private TestDechiffrementEnum(String val) {
            this.valeur = val;
        }

        public String getValeur() {
            return valeur;
        }

        public boolean valeurEquals(String cmp) {
            return getValeur().equals(cmp);
        }
    }

    /**
     * Pour distinguer les enveloppes de candidature des offres, reponses et
     * projets.
     */
    public static enum EnveloppeEnum {

        CANDIDATURE(1), OFFRE(2), REPONSE(3), PROJET(4), ANONYMAT(5), ACTE_ENGAGEMENT(6);

        private int valeur;

        private String valeurStr;

        private EnveloppeEnum(final int val) {
            this.valeur = val;
            this.valeurStr = val + "";
        }

        public final int getValeur() {
            return valeur;
        }

        public final boolean valeurEquals(final String cmp) {
            return valeurStr.equals(cmp);
        }
        public final boolean valeurEquals(final int cmp) {
            return valeur == cmp;
        }

        public final static EnveloppeEnum toEnveloppeEnum(final int valeur) {
            for (EnveloppeEnum enveloppeEnum : EnveloppeEnum.values()) {
                if (enveloppeEnum.valeurEquals(valeur)) {
                    return enveloppeEnum;
                }
            }
            throw new IllegalArgumentException(valeur + "");
        }

        public final static EnveloppeEnum toEnveloppeEnum(final String valeur) {
            for (EnveloppeEnum enveloppeEnum : EnveloppeEnum.values()) {
                if (enveloppeEnum.valeurEquals(valeur)) {
                    return enveloppeEnum;
                }
            }
            throw new IllegalArgumentException(valeur);
        }
    }
    
    /**
     * Ensembles des identifiants des referentiels tableau pour le module
     * redaction.
     */
    public static enum RefValeurTableauIdEnum {

        TABLEAU_PRIX(8), TABLEAU_RIB(10), TABLEAU_CONTRACTANT_VIERGE(11), TABLEAU_BON_COMMANDE_VIERGE(12), TABLEAU_ENGAGEMENT_SIGNATURE_CANDIDAT(
                13), TABLEAU_ENGAGEMENT_SIGNATURE_POUVOIR_ADJUDICATEUR(14);

        private int valeur;

        private RefValeurTableauIdEnum(int val) {
            this.valeur = val;
        }

        public int getValeur() {
            return valeur;
        }

        public boolean valeurEquals(int cmp) {
            return valeur == cmp;
        }
    }

}
