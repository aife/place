/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author MGA
 */
public class DomainePairesTypes implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identifiant du lot
     */
    private int idLot;
    /**
     * 
     */
    private String clef;
    /**
     * La liste des paires types
     */
    private Map<String, DomainePaireType> pairesTypes;
    
    /**
     * 
     */
    public DomainePairesTypes() {
        pairesTypes = new HashMap<String, DomainePaireType>();
    }
    /**
     * @return the idLot
     */
    public final int getIdLot() {
        return idLot;
    }

    /**
     * @param idLot the idLot to set
     */
    public final void setIdLot(final int valeur) {
        this.idLot = valeur;
    }
    /**
     * @return the clef
     */
    public final String getClef() {
        return clef;
    }
    /**
     * @param clef the clef to set
     */
    public final void setClef(final String valeur) {
        this.clef = valeur;
    }
    /**
     * @return the pairesTypes
     */
    public final Map<String, DomainePaireType> getPairesTypes() {
        return pairesTypes;
    }
    /**
     * @param pairesTypes the pairesTypes to set
     */
    public final void setPairesTypes(final Map<String, DomainePaireType> valeur) {
        this.pairesTypes = valeur;
    }
}
