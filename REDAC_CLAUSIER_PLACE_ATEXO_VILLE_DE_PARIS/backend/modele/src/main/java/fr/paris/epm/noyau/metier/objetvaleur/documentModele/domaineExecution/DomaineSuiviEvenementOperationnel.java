package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine Suivi opérationnel
 * 
 * @author GAO Xuesong
 */
public class DomaineSuiviEvenementOperationnel implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contenu d’un événement opérationnel
	 */
	private String evenementOpe; 
	
	/**
	 * Date d’un événement opérationnel
	 */
	private Date dateEvenementOpe;
	
	/**
	 * Phase associée à un événement opérationnel
	 */
	private String phaseEvenementOpe; 
	
	/**
	 * Commentaire associé à un événement opérationnel
	 */
	private String commentaireEvenementOpe;

	
	public final String getEvenementOpe() {
		return evenementOpe;
	}

	public final void setEvenementOpe(final String valeur) {
		this.evenementOpe = valeur;
	}

	public final Date getDateEvenementOpe() {
		return dateEvenementOpe;
	}

	public final void setDateEvenementOpe(final Date valeur) {
		this.dateEvenementOpe = valeur;
	}

	public final String getPhaseEvenementOpe() {
		return phaseEvenementOpe;
	}

	public final void setPhaseEvenementOpe(final String valeur) {
		this.phaseEvenementOpe = valeur;
	}

	public final String getCommentaireEvenementOpe() {
		return commentaireEvenementOpe;
	}

	public final void setCommentaireEvenementOpe(final String valeur) {
		this.commentaireEvenementOpe = valeur;
	} 
	
	
}
