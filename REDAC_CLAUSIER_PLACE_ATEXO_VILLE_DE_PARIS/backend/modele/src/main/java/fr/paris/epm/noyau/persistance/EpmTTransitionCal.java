/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

/**
 * Pojo hibernate Transaction du calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTTransitionCal extends EpmTModeleTransitionCal implements
        Cloneable {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Étape source.
     */
    private EpmTEtapeCal source;

    /**
     * Étape cible.
     */
    private EpmTEtapeCal cible;

    // Constructeurs
    /**
     * @param eSource étape source
     * @param eCible étape cible
     */
    public EpmTTransitionCal(final EpmTEtapeCal eSource,
            final EpmTEtapeCal eCible) {
        this.source = eSource;
        this.cible = eCible;
    }

    /**
     * Constructeur vide.
     */
    public EpmTTransitionCal() {
    }

    // Méthodes
    /**
     * @return vrai si l'étape source ou cible est une délibération amont
     */
    public final boolean isDelibAmont() {
        if (source != null && source.isDelibAmont()) {
            return true;
        }
        if (cible != null && cible.isDelibAmont()) {
            return true;
        }
        return false;
    }

    /**
     * @return vrai si l'étape source ou cible est une délibération aval
     */
    public final boolean isDelibAval() {
        if (source != null && source.isDelibAval()) {
            return true;
        }
        if (cible != null && cible.isDelibAval()) {
            return true;
        }
        return false;
    }

    /**
     * Réinitialisation des valeurs fixes et variables.
     */
    public final void reinitialiser() {
        setValeurFixe(getValeurFixeInit());
        setValeurVariable(getValeurVariableInit());
    }

    /**
     * Modifie les valeurs fixes initiales et modifiées.
     * @param valeur valeur à positionner
     */
    public final void modifierValeurFixeInit(final int valeur) {
        setValeurFixeInit(valeur);
        setValeurFixe(valeur);
    }

    /**
     * Modifie les valeurs variables initiales et modifiées.
     * @param valeur valeur à positionner
     */
    public final void modifierValeurVariableInit(final int valeur) {
        setValeurVariableInit(valeur);
        setValeurVariable(valeur);
    }

    /**
     * Hormis l'identifiant, les valeurs primitives initiales sont conservées.
     * Les valeurs primitives modifiées sont réinitialisées. Les références vers
     * les étapes source et cible sont supprimées.
     * @return objet cloné
     */
    public final Object clone() throws CloneNotSupportedException {
        EpmTTransitionCal clone = (EpmTTransitionCal) super.clone();
        clone.setId(0);
        clone.reinitialiser();
        clone.setSource(null);
        clone.setCible(null);
        return clone;

    }

    /**
     * @return cible
     */
    public EpmTEtapeCal getCible() {
        return cible;
    }

    /**
     * @param valeur cible
     */
    public void setCible(final EpmTEtapeCal valeur) {
        this.cible = valeur;
    }

    /**
     * @return source
     */
    public EpmTEtapeCal getSource() {
        return source;
    }

    /**
     * @param valeur source
     */
    public void setSource(final EpmTEtapeCal valeur) {
        this.source = valeur;
    }

}
