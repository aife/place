/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;


/**
 * Classe abstraite dont devra hériter l'ensemble des documents qui devront
 * être stockés sur disque.
 * @author Mounthei
 * @version $Revision$, $Date$, $Author$
 */
public abstract class EpmTAbstractDocument extends EpmTAbstractObject {

    /**
     * serialisation.
     */
    private static final long serialVersionUID = -7546535814059919030L;
    /**
     * Identifiant du document.
     */
    private int id;
    /**
     * Nom du fichier.
     */
    private String nomFichier;

    /**
     * Fichier sur le disque.
     */
    private EpmTFichierSurDisque fichierSurDisque;

    /**
     * @return fichier sur disque
     */
    public EpmTFichierSurDisque getFichierSurDisque() {
        return fichierSurDisque;
    }

    /**
     * @param valeur fichier sur disque
     */
    public void setFichierSurDisque(final EpmTFichierSurDisque valeur) {
        this.fichierSurDisque = valeur;
    }

    /**
     * @return nom du fichier
     */
    public String getNomFichier() {
        return nomFichier;
    }

    /**
     * @param valeur nom du fichier
     */
    public void setNomFichier(final String valeur) {
        this.nomFichier = valeur;
    }

    /**
     * Retourne l'identifiant du document.
     * @return l'identifiant du document.
     */
    public int getId() {
        return id;
    }

    /**
     * Assigne l'identifiant du document.
     * @param valeur la valeur à assigner
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

}
