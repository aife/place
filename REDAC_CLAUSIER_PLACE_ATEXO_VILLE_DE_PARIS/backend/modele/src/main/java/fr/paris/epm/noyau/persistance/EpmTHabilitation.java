package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefThemeHabilitation;

/**
 * EpmTHabilitation pour la persistence des habilitations.
 * @author Cheikh Diop, Guillaume Béraudo
 * @version $Revision: 96 $, $Date: 2007-07-30 $, $Author: DIOP $
 */
public class EpmTHabilitation extends EpmTAbstractObject {

    public static final int HABILITATION_QUORUM_CIM = 48;

    public static final int HABILITATION_QUORUM_CAO = 54;

    public static final int HABILITATION_INSTANCE_CIM = 43;

    public static final int HABILITATION_INSTANCE_CAO = 49;

    public static final int HABILITATION_REUNION_CIM = 44;

    public static final int HABILITATION_REUNION_CAO = 50;

    public static final int HABILITATION_PRE_INSCRIRE_CONSULTATION_CIM = 45;

    public static final int HABILITATION_PRE_INSCRIRE_CONSULTATION_CAO = 51;

    public static final int HABILITATION_DEFINIR_ORDRE_DU_JOUR_CIM = 46;

    public static final int HABILITATION_DEFINIR_ORDRE_DU_JOUR_CAO = 52;

    public static final int HABILITATION_ENVOYER_DOSSIER_CIM = 47;

    public static final int HABILITATION_ENVOYER_DOSSIER_CAO = 53;

    /**
     * Habilitation permettant d'afficher toutes les consultations dans
     * l'inscriptions des consulations à l'ordre du jour.
     */
    public static final int HABILITATION_AFFICHER_TOUTES_CONSULTATIONS = 90;
    
    public static final int HABILITATION_RECEPTION_OFFRES = 31;
    
    public static final int HABILITATION_RECEPTION_CANDIDATURE = 25;
    
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * Rôle.
     */
    private String role;

    /**
     * libellé de l'habilitation.
     */
    private String libelle;

    /**
     * état de l'habilitation.
     */
    private String active;

    /**
     * thème de l'habilitation.
     */
    private EpmTRefThemeHabilitation epmTRefThemeHabilitation;

    /**
     * @return identifiant de l'enregistrement.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement.
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return role de l'habilitation
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return hash code
     */
    public final int hashCode() {
        int result = 1;
        if (role != null) {
            result += Constantes.PREMIER * result + role.hashCode();
        }
        if (role != null) {
            result += Constantes.PREMIER * result + role.hashCode();
        }
        if (libelle != null) {
            result += Constantes.PREMIER * result + libelle.hashCode();
        }
        if (epmTRefThemeHabilitation != null) {
            result += Constantes.PREMIER * result + epmTRefThemeHabilitation.hashCode();
        }
        return result;
    }

    /**
     * Comparaison uniquement sur le role de l'habilitation.
     * @param obj objet à compararer
     * @return boolean
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EpmTHabilitation other = (EpmTHabilitation) obj;
        if (role == null) {
            if (other.role != null) {
                return false;
            }
        } else if (!role.equals(other.role)) {
            return false;
        }

        if (id != other.id) {
            return false;
        }

        if (libelle == null) {
            if (other.libelle != null) {
                return false;
            }
        } else if (!libelle.equals(other.libelle)) {
            return false;
        }

        if (active == null) {
            if (other.active != null) {
                return false;
            }
        } else if (!active.equals(other.active)) {
            return false;
        }

        return true;
    }

    /**
     * @return permet de savoir si une habilitation est actif.
     */
    public String getActive() {
        return active;
    }

    /**
     * @param valeur permet d'activer ou de désactiver une habilitation.
     */
    public void setActive(final String valeur) {
        this.active = valeur;
    }

    /**
     * @return {@link EpmTRefThemeHabilitation} associé à l'habilitation
     */
    public EpmTRefThemeHabilitation getEpmTRefThemeHabilitation() {
        return epmTRefThemeHabilitation;
    }

    /**
     * @param valeur {@link EpmTRefThemeHabilitation} associé à
     *            l'habilitation
     */
    public void setEpmTRefThemeHabilitation(final EpmTRefThemeHabilitation valeur) {
        this.epmTRefThemeHabilitation = valeur;
    }

    /**
     * @return libellé de l'habilitation
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé de l'habilitation
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }
    
}