package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * avenant.attribution.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineAvenantAttribution implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Numéro du contrat.
     */
    private String numeroCompletAvenant;
    
    private String dateEnvoiNotificationAvenant;
    /**
     * Raison sociale du dernier attributaire de l'avenant.
     */
    private String raisonSocialeAttributaireAvenant;
    
    private String adresseAttributaireAvenant = "";
    
    private String SIRETAttributaireAvenant = "";

    private String SIRENAttributaireAvenant = "";



    /**
     * @return Numéro du contrat.
     */
    public final String getNumeroCompletAvenant() {
        return numeroCompletAvenant;
    }

    /**
     * @param valeur Numéro du contrat.
     */
    public final void setNumeroCompletAvenant(final String valeur) {
        this.numeroCompletAvenant = valeur;
    }

    public final String getDateEnvoiNotificationAvenant() {
        return dateEnvoiNotificationAvenant;
    }

    public final void setDateEnvoiNotificationAvenant(final String valeur) {
        this.dateEnvoiNotificationAvenant = valeur;
    }

    /**
     * @return Raison sociale du dernier attributaire de l'avenant.
     */
    public final String getRaisonSocialeAttributaireAvenant() {
        return raisonSocialeAttributaireAvenant;
    }

    /**
     * @param valeur Raison sociale du dernier attributaire de l'avenant.
     */
    public final void setRaisonSocialeAttributaireAvenant(final String valeur) {
        this.raisonSocialeAttributaireAvenant = valeur;
    }

    public final String getAdresseAttributaireAvenant() {
        return adresseAttributaireAvenant;
    }

    public final void setAdresseAttributaireAvenant(final String valeur) {
        this.adresseAttributaireAvenant = valeur;
    }


    public void setSIRETAttributaireAvenant(String SIRETAttributaireAvenant) {
        this.SIRETAttributaireAvenant = SIRETAttributaireAvenant;
    }

    public String getSIRETAttributaireAvenant() {
        return SIRETAttributaireAvenant;
    }

    public String getSIRENAttributaireAvenant() {
        return SIRENAttributaireAvenant;
    }

    public void setSIRENAttributaireAvenant(String SIRENAttributaireAvenant) {
        this.SIRENAttributaireAvenant = SIRENAttributaireAvenant;
    }
}
