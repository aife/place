package fr.paris.epm.noyau.configuration;

import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Created by sta on 18/02/16.
 */
public class UtilisateurResolver implements HandlerMethodArgumentResolver {

    private static final Logger logger = LoggerFactory.getLogger(UtilisateurResolver.class);

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        //logger.debug("UtilisateurResolver -> supportsParameter -> "/* + methodParameter.toString()*/);
        return methodParameter.getParameterType().equals(EpmTUtilisateur.class) && methodParameter.hasParameterAnnotation(UtilisateurHandler.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        //Récupère l'utilisateur en session (EpmTUtilisateur)
        Object utilisateur = nativeWebRequest.getAttribute("utilisateur", 1);
        //logger.debug("UtilisateurResolver -> resolveArgument -> "/* + utilisateur*/);
        return utilisateur;
    }
}
