package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;

/**
 * Objet consultation simplifié pour passation. Destiné notamment à l'écran de
 * recherche de consultations par critères.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class ConsultPassation implements Serializable, Cloneable, Comparable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant de la consultation.
     */
    private int id;

    /**
     * Consultation transverse si vrai.
     */
    private boolean transverse;

    /**
     * identifiant du lot.
     */
    private int idLot;

    /**
     * numéro du lot.
     */
    private String numeroLot;

    /**
     * Numéro de consultation.
     */
    private String numeroConsult;

    /**
     * Pouvoir adjudicateur.
     */
    private String pouvAdjudicateur;

    /**
     * Utilisateur.
     */
    private String utilisateur;

    /**
     * Intitulé.
     */
    private String intitule;

    /**
     * Objet.
     */
    private String objet;

    /**
     * Procédure.
     */
    private Doublet procedure;

    /**
     * Direction/service.
     */
    private String dirService;

    /**
     * Dates remarquables du calendrier.
     */
    private EtapesDatesCal etapesDatesCal;

    /**
     * Statut de la consultation.
     */
    private Doublet statut;

    /**
     * Vrai si possède deux phases, donc deux dates limites.
     */
    private boolean deuxPhases;

    /**
     * Vrai si l'utilisateur courant possède le droit de vision.
     */
    private boolean vision;

    /**
     * indique si la consultation a un ou plusieurs lots dissocié.
     */
    private boolean lotDissocie;

    /**
     * Vrai si l'utilisateur courant possède le droit d'intervention.
     */
    private boolean intervention;

    /**
     * Identifiant de la direction/service responsable.
     */
    private Integer idDirService;

    /**
     * Identifiant de la direction/service bénéficiaire.
     */
    private Integer idDirServiceVision;

    // Accesseurs
    /**
     * @return direction/service
     */
    public final String getDirService() {
        return dirService;
    }

    /**
     * @param valeur direction/service
     */
    public final void setDirService(final String valeur) {
        this.dirService = valeur;
    }

    /**
     * @return intitulé
     */
    public final String getIntitule() {
        return intitule;
    }

    /**
     * @param valeur intitulé
     */
    public final void setIntitule(final String valeur) {
        this.intitule = valeur;
    }

    /**
     * @return dates remarquables du calendrier
     */
    public final EtapesDatesCal getEtapesDatesCal() {
        return etapesDatesCal;
    }

    /**
     * @param valeur dates remarquables du calendrier
     */
    public final void setEtapesDatesCal(final EtapesDatesCal valeur) {
        this.etapesDatesCal = valeur;
    }

    /**
     * @return numéro de consultation
     */
    public final String getNumeroConsult() {
        return numeroConsult;
    }

    /**
     * @param valeur numéro de consultation
     */
    public final void setNumeroConsult(final String valeur) {
        this.numeroConsult = valeur;
    }

    /**
     * @return objet de la consultation
     */
    public final String getObjet() {
        return objet;
    }

    /**
     * @param valeur objet de la consultation
     */
    public final void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return pouvoir adjudicateur
     */
    public final String getPouvAdjudicateur() {
        return pouvAdjudicateur;
    }

    /**
     * @param valeur pouvoir adjudicateur
     */
    public final void setPouvAdjudicateur(final String valeur) {
        this.pouvAdjudicateur = valeur;
    }

    /**
     * @return procédure
     */
    public final Doublet getProcedure() {
        return procedure;
    }

    /**
     * @param valeur procédure
     */
    public final void setProcedure(final Doublet valeur) {
        this.procedure = valeur;
    }

    /**
     * @return statut
     */
    public final Doublet getStatut() {
        return statut;
    }

    /**
     * @param valeur statut
     */
    public final void setStatut(final Doublet valeur) {
        this.statut = valeur;
    }

    /**
     * @return utilisateur
     */
    public final String getUtilisateur() {
        return utilisateur;
    }

    /**
     * @param valeur utilisateur
     */
    public final void setUtilisateur(final String valeur) {
        this.utilisateur = valeur;
    }

    /**
     * @return identifiant de la consultation
     */
    public final int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de la consultation
     */
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return vrai si possède deux phases, donc deux dates limites
     */
    public final boolean isDeuxPhases() {
        return deuxPhases;
    }

    /**
     * @param valeur vrai si possède deux phases, donc deux dates limites
     */
    public final void setDeuxPhases(final boolean valeur) {
        this.deuxPhases = valeur;
    }

    /**
     * @return identifiant de la direction/service responsable
     */
    public final Integer getIdDirService() {
        return idDirService;
    }

    /**
     * @param valeur identifiant de la direction/service responsable
     */
    public final void setIdDirService(final Integer valeur) {
        this.idDirService = valeur;
    }

    /**
     * @return identifiant de la direction/service bénéficiaire
     */
    public final Integer getIdDirServiceVision() {
        return idDirServiceVision;
    }

    /**
     * @param valeur identifiant de la direction/service bénéficiaire
     */
    public final void setIdDirServiceVision(final Integer valeur) {
        this.idDirServiceVision = valeur;
    }

    /**
     * @return vrai si l'utilisateur courant a les droits en intervention
     */
    public final boolean isIntervention() {
        return intervention;
    }

    /**
     * @param valeur vrai si l'utilisateur courant a les droits en intervention
     */
    public final void setIntervention(final boolean valeur) {
        this.intervention = valeur;
    }

    /**
     * @return vrai si l'utilisateur courant a les droits en vision
     */
    public final boolean isVision() {
        return vision;
    }

    /**
     * @param valeur vrai si l'utilisateur courant a les droits en vision
     */
    public final void setVision(final boolean valeur) {
        this.vision = valeur;
    }

    /**
     * @return indique si la consultation a un ou plusieurs lots dissocié.
     */
    public final boolean isLotDissocie() {
        return lotDissocie;
    }

    /**
     * @param valeur indique si la consultation a un
     *  ou plusieurs lots dissocié.
     */
    public final void setLotDissocie(final boolean valeur) {
        this.lotDissocie = valeur;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Comparaison sur le numero de consultation.
     * @param arg0 l'autre objet du type ConsultPassation.
     */
    public final int compareTo(final Object arg0) {
        ConsultPassation other = (ConsultPassation) arg0;
        if (other != null) {
            String autreNumeroConsult = other.getNumeroConsult();
            if (this.numeroConsult != null && autreNumeroConsult != null) {
                return numeroConsult.compareTo(autreNumeroConsult);
            }
        }
        return 0;
    }

    /**
     * @return identifiant du lot
     */
    public final int getIdLot() {
        return idLot;
    }

    /**
     * @param valeur identifiant du lot
     */
    public final void setIdLot(final int valeur) {
        this.idLot = valeur;
    }

    /**
     * @return numero du lot
     */
    public final String getNumeroLot() {
        return numeroLot;
    }

    /**
     * @param valeur numero du lot
     */
    public final void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }

    /**
     * @return vrai si la consultation est transverse
     */
    public final boolean isTransverse() {
        return transverse;
    }

    /**
     * @param valeur vrai si la consultation est transverse
     */
    public final void setTransverse(final boolean valeur) {
        this.transverse = valeur;
    }
}
