package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;

/**
 * Classe abstraite EpmTReferentielOrdredAbstract est la racine d'héritation pour toutes les référentiels ordonées.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class EpmTReferentielOrdredAbstract extends BaseEpmTRefReferentiel implements EpmTReferentielOrdred {

    /**
     * Marqueur de sérialization.
     */
    static private final long serialVersionUID = 1L;

    /**
     * Ordre de la ligne
     */
    private int ordre;

    @Override
    public int getOrdre() {
        return ordre;
    }

    @Override
    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    /**
     * Comparaison par rapport au libellé. Gère "TOUS" en identifiant 0.
     * @param referentiel objet à comparer
     * @return int
     */
    @Override
    public int compareTo(final EpmTRef referentiel) {
        final int idTous = 0; // L'objet d'identifiant idTous est toujours en premier
        if (getId() == idTous)
            if (referentiel.getId() == idTous)
                return 0;
            else
                return -1;
        else
        if (referentiel.getId() == idTous)
            return 1;
        else
            return Integer.compare(ordre, ((EpmTReferentielOrdredAbstract) referentiel).ordre);
    }

}
