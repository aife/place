package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

/**
 * Bean contenant tous les informations concernant la reconduction d'un lot ou une consultation. Ce domaine à être remplacé
 * dans un document. 
 * @author Rebeca Dantas
 *
 */
public class DomaineReconduction {
	
	/**
	 * True si la reconduction est activé
	 */
	private boolean reconduction;
	
	/**
	 * Le nombre de reconductions
	 */
	private Integer nombre;
	
	/**
	 * Les modalités de la reconduction. Par ex : durée
	 */
	private String modalite;

	/**
	 * 
	 * @return true si reconductible
	 */
	public final boolean isReconduction() {
		return reconduction;
	}

	/**
	 * 
	 * @param valeur true si reconductible
	 */
	public final void setReconduction(final boolean valeur) {
		this.reconduction = valeur;
	}

	/**
	 * 
	 * @return le nombre de reconductions
	 */
	public final Integer getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param valeur le nombre de recodnuctions
	 */
	public final void setNombre(final Integer valeur) {
		this.nombre = valeur;
	}

	/**
	 * 
	 * @return la modalité de la reconduction, notamment, la durée
	 */
	public final String getModalite() {
		return modalite;
	}

	/**
	 * 
	 * @param valeur la modalité de la reconduction, notamment, la durée
	 */
	public final void setModalite(final String valeur) {
		this.modalite = valeur;
	}
	
}
