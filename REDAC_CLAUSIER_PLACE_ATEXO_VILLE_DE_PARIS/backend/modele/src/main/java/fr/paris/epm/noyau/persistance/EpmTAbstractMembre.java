/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefRole;



/**
 * Classe mère des membres dans commission.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class EpmTAbstractMembre extends EpmTAbstractObject{

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;


    /**
     *
     */
    private EpmTRefRole role;

//    /**
//     *
//     */
//    private EpmTReunion reunion;


    // Accesseurs
    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

//    /**
//     * Non nul pour les experts.
//     * @return réunion
//     */
//    public EpmTReunion getReunion() {
//        return reunion;
//    }
//
//    /**
//     * Non nul pour les experts.
//     * @param valeur réunion
//     */
//    public void setReunion(final EpmTReunion valeur) {
//        this.reunion = valeur;
//    }

    /**
     * Non nul pour les membres permanents.
     * @return rôle
     */
    public EpmTRefRole getRole() {
        return role;
    }

    /**
     * Non nul pour les membres permanents.
     * @param valeur rôle
     */
    public void setRole(final EpmTRefRole valeur) {
        this.role = valeur;
    }

}
