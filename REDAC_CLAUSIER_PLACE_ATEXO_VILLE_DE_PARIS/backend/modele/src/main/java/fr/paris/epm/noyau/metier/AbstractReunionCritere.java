package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Date;

/**
 * Classe critère utilisée pour la recherche de réunions de commission.
 * 
 * @author Guillaume Béraudo
 * @version $Revision: $, $Date: $, $Author: $
 */
public abstract class AbstractReunionCritere extends AbstractCritere implements
        Critere, Serializable {
    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant de la réunion.
     */
    protected int id;

    /**
     * Identifiant du pouvoir adjudicateur.
     */
    protected int idPouvoirAdjudicateur;

    /**
     * Identifiant de la direction/service.
     */
    protected int idDirectionService;

    /**
     * Date et heure à partir desquelles chercher.
     */
    protected Date aPartirDu;

    /**
     * Date et heure à partir jusqu'auxquelles chercher.
     */
    protected Date jusquAu;

    /**
     * texte apparaissant dans le libelle de la réunion.
     */
    protected String libelle;

    /**
     * texte apparaissant dans le libelle de la réunion.
     */
    protected String libelleMatchExact;

    protected boolean minimal;

    // Méthodes
    /**
     * Les espaces extrémaux sont supprimés et la chaîne est mise en minuscule.
     * 
     * @param valeur
     *            texte apparaissant dans le libelle de l'instance
     */
    public final void setLibelle(final String valeur) {
        if (valeur == null) {
            libelle = null;
            return;
        }
        final String transforme = valeur.trim();
        if ("".equals(transforme)) {
            libelle = null;
            return;
        }
        libelle = transforme.toLowerCase();
    }

    public final void setLibelleMatchExact(final String valeur) {
        if (valeur == null) {
            libelleMatchExact = null;
            return;
        }
        final String transforme = valeur.trim();
        if ("".equals(transforme)) {
            libelleMatchExact = null;
            return;
        }
        libelleMatchExact = transforme.toLowerCase();
    }

    /**
     * @param pa
     *            pouvoir adjudicateur
     */
    public final void setPouvoirAdjudicateur(final int pa) {
        idPouvoirAdjudicateur = pa;
    }

    /**
     * @param ds
     *            direction/service
     */
    public final void setDirectionService(final int ds) {
        idDirectionService = ds;
    }

    /**
     * @param date
     *            et heure à partir jusqu'auxquelles chercher
     */
    public final void setAPartirDu(final Date date) {
        aPartirDu = date;
    }

    /**
     * @param date
     *            date et heure à partir jusqu'auxquelles chercher
     */
    public final void setJusquAu(final Date date) {
        this.jusquAu = date;
    }

    // Accesseurs
    /**
     * @param valeur
     *            identifiant de la réunion
     */
    public final void setId(final int valeur) {
        id = valeur;
    }

    /**
     * 
     * @return requete minimal
     */
    public boolean isMinimal() {
        return minimal;
    }

    /**
     * 
     * @param valeur
     *            requete minimal
     */
    public void setMinimal(boolean valeur) {
        this.minimal = valeur;
    }

}
