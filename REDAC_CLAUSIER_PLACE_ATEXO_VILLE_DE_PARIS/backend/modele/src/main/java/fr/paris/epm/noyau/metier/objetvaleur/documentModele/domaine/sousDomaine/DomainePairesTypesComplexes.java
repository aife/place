/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author MGA
 */
public class DomainePairesTypesComplexes implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identifiant du lot
     */
    private int idLot;
    /**
     * La clef du type complexe
     */
    private String clef;
    /**
     * Liste de paires types complexes
     */
    private List<DomainePairesTypes> pairesTypesComplexes;

    /**
     * 
     */
    public DomainePairesTypesComplexes() {
        pairesTypesComplexes = new ArrayList<DomainePairesTypes>();
    }
    
    /**
     * @return the idLot
     */
    public final int getIdLot() {
        return idLot;
    }

    /**
     * @param idLot the idLot to set
     */
    public final void setIdLot(final int valeur) {
        this.idLot = valeur;
    }

    /**
     * @return the clef
     */
    public final String getClef() {
        return clef;
    }

    /**
     * @param clef the clef to set
     */
    public final void setClef(final String valeur) {
        this.clef = valeur;
    }

    /**
     * @return the pairesTypesComplexes
     */
    public final List<DomainePairesTypes> getPairesTypesComplexes() {
        if (pairesTypesComplexes == null) {
            return new ArrayList<DomainePairesTypes>();
        }
        return pairesTypesComplexes;
    }

    /**
     * @param pairesTypesComplexes the pairesTypesComplexes to set
     */
    public final void setPairesTypesComplexes(final List<DomainePairesTypes> valeur) {
        this.pairesTypesComplexes = valeur;
    }

}
