package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;

/**
 * Created by qba on 12/09/17.
 */
public class TemplateVersionCritere extends AbstractCritere {

    /**
     * Identifiant
     */
    private Integer id;

    /**
     * Identifiant du niveau d'intervention
     */
    private Integer idIntervention;

    /**
     * Identifiant represantant (l'utilisateur, direction/service, organisme)
     */
    private Integer idRepresantant;

    /**
     * true -> Type de Page de Garde; sinon: false -> Type de Document
     */
    private Boolean isPageDeGarde = false;

    /**
     * Identifiant du type de document
     */
    private Integer idTypeDocument;

    /**
     * Recherche le derneir template à jour
     */
    private Boolean dernier;

    @Override
    public StringBuffer corpsRequete() {

        boolean hasWhere = false;
        StringBuffer clauseAnd = new StringBuffer();

        if (id != null) {
            clauseAnd.append(hasWhere ? " AND " : " WHERE ").append("templateVersion.id = ").append(id).append(" ");
            hasWhere = true;
        }

        if (idIntervention != null) {
            clauseAnd.append(hasWhere ? " AND " : " WHERE ")
                    .append("templateVersion.idIntervention = ")
                    .append(idIntervention)
                    .append(" ");
            hasWhere = true;
        }

        if (idRepresantant != null) {
            clauseAnd.append(hasWhere ? " AND " : " WHERE ")
                    .append("templateVersion.idRepresentant = ")
                    .append(idRepresantant)
                    .append(" ");
            hasWhere = true;
        }

        if (isPageDeGarde != null && idTypeDocument != null) {
            clauseAnd.append(hasWhere ? " AND " : " WHERE ")
                    .append("templateVersion.")
                    .append(isPageDeGarde ? "idPageGarde = " : "idTypeDocument = ")
                    .append(idTypeDocument)
                    .append(" ");
            hasWhere = true;
        }

        return clauseAnd;
    }

    @Override
    public String toHQL() {
        if (dernier != null && dernier) {
            String listClauses = "templateVersion.id, templateVersion.idIntervention, templateVersion.idRepresentant," +
                    " templateVersion.idPageGarde, templateVersion.idTypeDocument, templateVersion.nomFichier";

            return "select " + listClauses + ", max(templateVersion.dateCreation)" +
                    " from EpmTTemplateVersion as templateVersion " + corpsRequete() +
                    " group by " + listClauses;
        } else {
            return "select templateVersion from EpmTTemplateVersion as templateVersion " + corpsRequete();
        }
    }

    @Override
    public String toCountHQL() {
        return "select count(*) from EpmTTemplateVersion as templateVersion " + corpsRequete();
    }

    public void setIdIntervention(Integer idIntervention) {
        this.idIntervention = idIntervention;
    }

    public void setIdRepresantant(Integer idRepresantant) {
        this.idRepresantant = idRepresantant;
    }

    public void setIsPageDeGarde(Boolean pageDeGarde) {
        isPageDeGarde = pageDeGarde;
    }

    public void setIdTypeDocument(Integer idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public Boolean getDernier() {
        return dernier;
    }

    public void setDernier(Boolean dernier) {
        this.dernier = dernier;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
