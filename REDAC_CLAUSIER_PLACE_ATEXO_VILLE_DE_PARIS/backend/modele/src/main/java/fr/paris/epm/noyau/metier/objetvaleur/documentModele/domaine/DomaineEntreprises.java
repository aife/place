package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.*;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le domaine
 * entreprises.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineEntreprises implements Serializable {
	
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Sous-domaine d'un retrait electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    private DomaineRegistreRetraitDemat registreRetraitDemat;
    /**
     * Sous-domaine des retraits electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    private List<DomaineRegistreRetraitDemat> listeRegistreRetraitDemat;
    /**
     * Sous-domaine d'un retrait papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    private DomaineRegistreRetraitPapier registreRetraitPapier;
    /**
     * Sous-domaine des retraits papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    private List<DomaineRegistreRetraitPapier> listeRegistreRetraitPapier;
    /**
     * Sous-domaine d'un depot electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    private DomaineRegistreDepotDemat registreDepotDemat;
    /**
     * Sous-domaine des depots electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    private List<DomaineRegistreDepotDemat> listeRegistreDepotDemat;
    /**
     * Sous-domaine d'un depot papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    private DomaineRegistreDepotPapier registreDepotPapier;
    /**
     * Sous-domaine des depots papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    private List<DomaineRegistreDepotPapier> listeRegistreDepotPapier;
    /**
     * Informations generales sur les depots (nombre de depots papier, electronique).
     */
    private DomaineRegistreDepot registreDepot;
    /**
     * Informations generales sur les retraits (nombre de retraits...).
     */
    private DomaineRegistreRetrait registreRetrait;
    /**
     * Informations sur les coordonnees du destinataire.
     */
    private DomaineCourrier courrier;

    /**
     * @return Sous-domaine d'un retrait electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final DomaineRegistreRetraitDemat getRegistreRetraitDemat() {
        return registreRetraitDemat;
    }
    /**
     * @param valeur Sous-domaine d'un retrait electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final void setRegistreRetraitDemat(
            final DomaineRegistreRetraitDemat valeur) {
        this.registreRetraitDemat = valeur;
    }
    /**
     * @return Sous-domaine des retraits electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final List<DomaineRegistreRetraitDemat> getListeRegistreRetraitDemat() {
        return listeRegistreRetraitDemat;
    }
    /**
     * @param valeur Sous-domaine des retraits electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final void setListeRegistreRetraitDemat(
            final List<DomaineRegistreRetraitDemat> valeur) {
        this.listeRegistreRetraitDemat = valeur;
    }
    /**
     * @return Sous-domaine d'un retrait papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final DomaineRegistreRetraitPapier getRegistreRetraitPapier() {
        return registreRetraitPapier;
    }
    /**
     * @param valeur Sous-domaine d'un retrait papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final void setRegistreRetraitPapier(
            final DomaineRegistreRetraitPapier valeur) {
        this.registreRetraitPapier = valeur;
    }
    /**
     * @return Sous-domaine des retraits papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final List<DomaineRegistreRetraitPapier> getListeRegistreRetraitPapier() {
        return listeRegistreRetraitPapier;
    }
    /**
     * @param valeur Sous-domaine des retraits papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final void setListeRegistreRetraitPapier(
            final List<DomaineRegistreRetraitPapier> valeur) {
        this.listeRegistreRetraitPapier = valeur;
    }
    /**
     * @return Sous-domaine d'un depot electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final DomaineRegistreDepotDemat getRegistreDepotDemat() {
        return registreDepotDemat;
    }
    /**
     * @param valeur Sous-domaine d'un depot electronique (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final void setRegistreDepotDemat(
            final DomaineRegistreDepotDemat valeur) {
        this.registreDepotDemat = valeur;
    }
    /**
     * @return Sous-domaine des depots electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final List<DomaineRegistreDepotDemat> getListeRegistreDepotDemat() {
        return listeRegistreDepotDemat;
    }
    /**
     * @param valeur Sous-domaine des depots electroniques (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final void setListeRegistreDepotDemat(
            final List<DomaineRegistreDepotDemat> valeur) {
        this.listeRegistreDepotDemat = valeur;
    }
    /**
     * @return Sous-domaine d'un depot papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final DomaineRegistreDepotPapier getRegistreDepotPapier() {
        return registreDepotPapier;
    }
    /**
     * @param valeur Sous-domaine d'un depot papier (affecte si le document est
     * generer a partir d'un retrait).
     */
    public final void setRegistreDepotPapier(
            final DomaineRegistreDepotPapier valeur) {
        this.registreDepotPapier = valeur;
    }
    /**
     * @return Sous-domaine des depots papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final List<DomaineRegistreDepotPapier> getListeRegistreDepotPapier() {
        return listeRegistreDepotPapier;
    }
    /**
     * @param valeur Sous-domaine des depots papiers (affecte si le document est
     * generer a partir d'une consultation ou d'une entreprise).
     */
    public final void setListeRegistreDepotPapier(
            final List<DomaineRegistreDepotPapier> valeur) {
        this.listeRegistreDepotPapier = valeur;
    }
    /**
     * @return Informations generales sur les depots (nombre de depots papier, electronique).
     */
    public final DomaineRegistreDepot getRegistreDepot() {
        return registreDepot;
    }
    /**
     * @param valeur Informations generales sur les depots (nombre de depots papier, electronique).
     */
    public final void setRegistreDepot(final DomaineRegistreDepot valeur) {
        this.registreDepot = valeur;
    }
    /**
     * @return Informations sur les coordonnees du destinataire.
     */
    public final DomaineCourrier getCourrier() {
        return courrier;
    }
    /**
     * @param valeur Informations sur les coordonnees du destinataire.
     */
    public final void setCourrier(final DomaineCourrier valeur) {
        this.courrier = valeur;
    }
    /**
     * @return Informations generales sur les retraits (nombre de retraits...).
     */
    public final DomaineRegistreRetrait getRegistreRetrait() {
        return registreRetrait;
    }
    /**
     * @param valeur Informations generales sur les retraits (nombre de retraits...).
     */
    public final void setRegistreRetrait(final DomaineRegistreRetrait valeur) {
        this.registreRetrait = valeur;
    }
}
