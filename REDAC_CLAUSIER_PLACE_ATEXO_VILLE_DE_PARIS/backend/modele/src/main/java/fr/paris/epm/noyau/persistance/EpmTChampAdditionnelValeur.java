package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefChampAdditionnel;

/**
 * Pojo des valeur des champs additionnels
 * @author Tyurin Nikolay
 */
public class EpmTChampAdditionnelValeur extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private int id;

    private EpmTRefChampAdditionnel epmTRefChampAdditionnel;

    private int idInstance; // id de DemandeAchat (ou de ?Consultation?) dépend de idForm dans la référence ChampAdditionne

    private String value;

    private String label;   // utilisé pour les demande achat validé sinon utilisé tel de RefChampAdditionnel

    private int idTypeField;    // utilisé pour les demande achat validé sinon utilisé tel de RefChampAdditionnel

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EpmTRefChampAdditionnel getEpmTRefChampAdditionnel() {
        return epmTRefChampAdditionnel;
    }

    public void setEpmTRefChampAdditionnel(EpmTRefChampAdditionnel epmTRefChampAdditionnel) {
        this.epmTRefChampAdditionnel = epmTRefChampAdditionnel;
    }

    public int getIdInstance() {
        return idInstance;
    }

    public void setIdInstance(int idInstance) {
        this.idInstance = idInstance;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIdTypeField() {
        return idTypeField;
    }

    public void setIdTypeField(int idTypeField) {
        this.idTypeField = idTypeField;
    }

}
