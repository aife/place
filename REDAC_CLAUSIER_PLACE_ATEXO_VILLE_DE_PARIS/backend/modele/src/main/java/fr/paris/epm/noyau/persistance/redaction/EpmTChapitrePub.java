package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Class POJO EpmTChapitre -> epm__t_chapitre_pub
 * Created by nty on 05/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Entity
@Table(name = "epm__t_chapitre_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_chapitre", "id_publication"}))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTChapitrePub extends EpmTChapitreAbstract {

    /**
     * Clé primaire et id_clause en meme temps
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_chapitre", nullable = false)
    private Integer idChapitre;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;

    @Column(name = "id_canevas")
    private Integer idCanevas;

    @ManyToOne(targetEntity = EpmTCanevasPub.class)
    @JoinColumns({
            @JoinColumn(name = "id_canevas", referencedColumnName = "id_canevas", insertable = false, updatable = false),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication", insertable = false, updatable = false)
    })
    private EpmTCanevasPub epmTCanevas;

    @Column(name = "id_parent_chapitre")
    private Integer idParentChapitre;

    @ManyToOne(targetEntity = EpmTChapitrePub.class)
    @JoinColumns({
            @JoinColumn(name = "id_parent_chapitre", referencedColumnName = "id_chapitre", insertable = false, updatable = false),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication", insertable = false, updatable = false)
    })
    private EpmTChapitrePub epmTParentChapitre;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(schema = "redaction", name = "epm__t_chapitre_has_clause_pub",
            joinColumns = {
                    @JoinColumn(name = "id_chapitre", referencedColumnName = "id_chapitre"),
                    @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
            })
    @MapKeyColumn(name = "num_order")
    @Column(name = "reference_clause")
    private Map<Integer, String> clauses;

    @OneToMany(targetEntity = EpmTChapitrePub.class,
            mappedBy = "epmTParentChapitre", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<EpmTChapitrePub> epmTSousChapitres;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdChapitre() {
        return idChapitre;
    }

    public void setIdChapitre(Integer idChapitre) {
        this.idChapitre = idChapitre;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(Integer idCanevas) {
        this.idCanevas = idCanevas;
    }

    @Override
    public EpmTCanevasPub getEpmTCanevas() {
        return epmTCanevas;
    }

    @Override
    public void setEpmTCanevas(EpmTCanevasAbstract epmTCanevas) {
        this.epmTCanevas = (EpmTCanevasPub) epmTCanevas;
    }

    public Integer getIdParentChapitre() {
        return idParentChapitre;
    }

    public void setIdParentChapitre(Integer idParentChapitre) {
        this.idParentChapitre = idParentChapitre;
    }

    @Override
    public EpmTChapitrePub getEpmTParentChapitre() {
        return epmTParentChapitre;
    }

    @Override
    public void setEpmTParentChapitre(EpmTChapitreAbstract epmTParentChapitre) {
        this.epmTParentChapitre = (EpmTChapitrePub) epmTParentChapitre;
    }

    @Override
    public Map<Integer, String> getClauses() {
        return clauses;
    }

    @Override
    public void setClauses(Map<Integer, String> clauses) {
        this.clauses = clauses;
    }

    @Override
    public List<EpmTChapitrePub> getEpmTSousChapitres() {
        return epmTSousChapitres;
    }

    @Override
    public void setEpmTSousChapitres(List<? extends EpmTChapitreAbstract> epmTSousChapitres) {
        this.epmTSousChapitres = (List<EpmTChapitrePub>) epmTSousChapitres;
    }

    @Override
    public EpmTChapitrePub clone() throws CloneNotSupportedException {
        EpmTChapitrePub epmTChapitre = (EpmTChapitrePub) super.clone();
        epmTChapitre.id = 0;
        return epmTChapitre;
    }

}
