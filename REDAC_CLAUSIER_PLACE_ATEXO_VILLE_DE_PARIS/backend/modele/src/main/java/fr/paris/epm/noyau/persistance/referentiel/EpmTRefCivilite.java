package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefCivilite de la table "epm__t_ref_civilite"
 *      - M.
 *      - Mme.
 *      - Mlle.
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCivilite extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

}