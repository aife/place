package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefSuiteADonner de la table "epm__t_ref_suite_a_donner"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefSuiteADonner extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_ADMIS_A_POURSUIVRE = 1; // Admis à poursuivre.
    public static final int ID_NON_ADMIS_A_POURSUIVRE = 2; // Non Admis à poursuivre.
    public static final int ID_INAPPROPRIE = 3; // Inapproprié.
    public static final int ID_IRREGULIERE = 4; // Irréguliére.
    public static final int ID_INACCEPTABLE = 5; // Inacceptable.
    public static final int ID_LOT_NON_PRESENTE = 6;

    /**
     * True si c'est une appréciation (inacceptale, irrecevale, inaproprié)
     * false sinon (admis à poursuivre, non admis).
     */
    private boolean appreciation;

    public boolean isAppreciation() {
        return appreciation;
    }

    public void setAppreciation(boolean appreciation) {
        this.appreciation = appreciation;
    }

}