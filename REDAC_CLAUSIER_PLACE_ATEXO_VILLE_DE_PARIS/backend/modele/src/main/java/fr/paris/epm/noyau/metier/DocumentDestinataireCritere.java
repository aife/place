package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.List;

/**
 * Pour rechercher les destinataires de modele de document.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DocumentDestinataireCritere extends AbstractCritere implements
        Critere, Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = -924415715643487277L;

    private List<Integer> id;
    
	private Boolean moduleExecution = null;

    public StringBuffer corpsRequete() {
        boolean debut = false;

        StringBuffer sb = new StringBuffer();
        sb.append(" from EpmTRefDocumentDestinataire d ");

        if (id != null && !id.isEmpty()) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            
            sb.append(" d.id in ( ");
            sb.append(id.get(0).intValue());
            for (int i = 1; i < id.size(); i++) {
                sb.append(", ");
                sb.append(id.get(i).intValue());
            }
            sb.append(" )");
            debut = true;
        }
        
        if(moduleExecution != null) {
        	if(moduleExecution) {
            	if (debut) {
                    sb.append(" and ");
                } else {
                    sb.append(" where ");
                }
                sb.append(" d.moduleExecution = TRUE");
                debut = true;
            } else {
            	if (debut) {
                    sb.append(" and ");
                } else {
                    sb.append(" where ");
                }
                sb.append(" d.moduleExecution = FALSE");
                debut = true;
            }
        }

        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(*) ");
        return select.append(corpsRequete()).toString();
    }

    public final void setId(final List<Integer> valeur) {
        this.id = valeur;
    }
    
	public final void setModuleExecution(final boolean valeur) {
		this.moduleExecution = valeur;
	}
}
