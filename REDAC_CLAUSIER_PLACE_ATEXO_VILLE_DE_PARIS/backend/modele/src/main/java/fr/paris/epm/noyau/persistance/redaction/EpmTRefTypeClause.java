package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate de EpmTRefTypeClause .
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_type_clause", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefTypeClause extends BaseEpmTRefRedaction implements EpmTRefImportExport {

    /**
     * Le type d'une clause Texte fixe.
     */
    public static final int TYPE_CLAUSE_TEXTE_FIXE = 2;

    /**
     * Le type d'une clause Texte libre.
     */
    public static final int TYPE_CLAUSE_TEXTE_LIBRE = 3;

    /**
     * Le type d'une clause Texte prévalorisé.
     */
    public static final int TYPE_CLAUSE_TEXTE_PREVALORISE = 4;

    /**
     * Le type d'une clause Texte à valeur héritée.
     */
    public static final int TYPE_CLAUSE_TEXTE_HERITEE = 5; // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}

    /**
     * Le type d'une clause à liste exclusif.
     */
    public static final int TYPE_CLAUSE_LISTE_EXCLUSIF = 6;

    /**
     * Le type d'une clause à liste cumulatif.
     */
    public static final int TYPE_CLAUSE_LISTE_CUMMULATIF = 7;

    /**
     * Le type d'une clause tableau.
     */
    public static final int TYPE_CLAUSE_TABLEAU = 8; // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}

    /**
     * Le type d'une clause valeur heritée - remplaçant TYPE_CLAUSE_TEXTE_HERITEE et TYPE_CLAUSE_TABLEAU .
     */
    public static final int TYPE_CLAUSE_VALEUR_HERITEE = 9;

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
