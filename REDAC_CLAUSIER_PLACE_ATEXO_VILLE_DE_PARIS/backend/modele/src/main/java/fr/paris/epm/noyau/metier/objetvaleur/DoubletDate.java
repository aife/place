/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;
import java.util.Date;

/**
 * Gestion d'un doublet de date pour le connecteur.
 * @author Mounthei
 * @version $Revision$, $Date$, $Author$
 */
public class DoubletDate implements Serializable {
    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Date de lancement de la consultation.
     */
    private Date dateLancement;
    /**
     * Date de limite de remise des plis.
     */
    private Date dateLimiteRemisePli;

    /**
     * @return Date de lancement de la consultation.
     */
    public final Date getDateLancement() {
        return dateLancement;
    }

    /**
     * @param valeur Date de lancement de la consultation.
     */
    public final void setDateLancement(final Date valeur) {
        this.dateLancement = valeur;
    }

    /**
     * @return Date de limite de remise des plis.
     */
    public final Date getDateLimiteRemisePli() {
        return dateLimiteRemisePli;
    }

    /**
     * @param valeur Date de limite de remise des plis.
     */
    public final void setDateLimiteRemisePli(final Date valeur) {
        this.dateLimiteRemisePli = valeur;
    }

}
