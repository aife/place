package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;

/**
 * POJO EpmTRefCcag de la table "epm__t_ref_ccag"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCcag extends BaseEpmTRefReferentiel implements EpmTRefImportExport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_CCAG_FCS = 1;
    public static final int ID_CCAG_MI = 2;
    public static final int ID_CCAG_TR = 3;
    public static final int ID_CCAG_PI = 4;
    public static final int ID_CCAG_TIC = 5;
    public static final int ID_CCAG_CAC = 6;
    public static final int ID_CCAG_NON_PRECISE = 7;

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
