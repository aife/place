package fr.paris.epm.noyau.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SignatureBean implements Serializable {
    Integer etat;
    String signatairePartiel;
    String signataireComplet;
    String emetteur;
    Date dateValiditeDu;
    Date dateValiditeAu;
    Boolean periodiciteValide;
    String chaineDeCertificationValide;
    String absenceRevocationCRL;
    Date dateSignatureValide;
    Boolean signatureValide;
    DetailsBean emetteurDetails;
    List<RepertoiresChaineCertificationBean> repertoiresChaineCertification;
    String repertoiresRevocation;
    String signatureXadesServeurEnBase64;
    DetailsBean signataireDetail;
    DetailsBean signataireDetails;
    Integer qualifieEIDAS;
    String formatSignature;
    Date dateIndicative;
    Integer jetonHorodatage;
    Date dateHorodatage;


    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getSignatairePartiel() {
        return signatairePartiel;
    }

    public void setSignatairePartiel(String signatairePartiel) {
        this.signatairePartiel = signatairePartiel;
    }

    public String getSignataireComplet() {
        return signataireComplet;
    }

    public void setSignataireComplet(String signataireComplet) {
        this.signataireComplet = signataireComplet;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public Date getDateValiditeDu() {
        return dateValiditeDu;
    }

    public void setDateValiditeDu(Date dateValiditeDu) {
        this.dateValiditeDu = dateValiditeDu;
    }

    public Date getDateValiditeAu() {
        return dateValiditeAu;
    }

    public void setDateValiditeAu(Date dateValiditeAu) {
        this.dateValiditeAu = dateValiditeAu;
    }

    public Boolean getPeriodiciteValide() {
        return periodiciteValide;
    }

    public void setPeriodiciteValide(Boolean periodiciteValide) {
        this.periodiciteValide = periodiciteValide;
    }

    public String getChaineDeCertificationValide() {
        return chaineDeCertificationValide;
    }

    public void setChaineDeCertificationValide(String chaineDeCertificationValide) {
        this.chaineDeCertificationValide = chaineDeCertificationValide;
    }

    public String getAbsenceRevocationCRL() {
        return absenceRevocationCRL;
    }

    public void setAbsenceRevocationCRL(String absenceRevocationCRL) {
        this.absenceRevocationCRL = absenceRevocationCRL;
    }

    public Date getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(Date dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }

    public Boolean getSignatureValide() {
        return signatureValide;
    }

    public void setSignatureValide(Boolean signatureValide) {
        this.signatureValide = signatureValide;
    }

    public DetailsBean getEmetteurDetails() {
        return emetteurDetails;
    }

    public void setEmetteurDetails(DetailsBean emetteurDetails) {
        this.emetteurDetails = emetteurDetails;
    }

    public List<RepertoiresChaineCertificationBean> getRepertoiresChaineCertification() {
        return repertoiresChaineCertification;
    }

    public void setRepertoiresChaineCertification(List<RepertoiresChaineCertificationBean> repertoiresChaineCertification) {
        this.repertoiresChaineCertification = repertoiresChaineCertification;
    }

    public DetailsBean getSignataireDetail() {
        return signataireDetail;
    }

    public String getRepertoiresRevocation() {
        return repertoiresRevocation;
    }

    public void setRepertoiresRevocation(String repertoiresRevocation) {
        this.repertoiresRevocation = repertoiresRevocation;
    }

    public String getSignatureXadesServeurEnBase64() {
        return signatureXadesServeurEnBase64;
    }

    public void setSignatureXadesServeurEnBase64(String signatureXadesServeurEnBase64) {
        this.signatureXadesServeurEnBase64 = signatureXadesServeurEnBase64;
    }

    public void setSignataireDetail(DetailsBean signataireDetail) {
        this.signataireDetail = signataireDetail;
    }

    public Integer getQualifieEIDAS() {
        return qualifieEIDAS;
    }

    public void setQualifieEIDAS(Integer qualifieEIDAS) {
        this.qualifieEIDAS = qualifieEIDAS;
    }

    public String getFormatSignature() {
        return formatSignature;
    }

    public void setFormatSignature(String formatSignature) {
        this.formatSignature = formatSignature;
    }

    public Date getDateIndicative() {
        return dateIndicative;
    }

    public void setDateIndicative(Date dateIndicative) {
        this.dateIndicative = dateIndicative;
    }

    public Integer getJetonHorodatage() {
        return jetonHorodatage;
    }

    public void setJetonHorodatage(Integer jetonHorodatage) {
        this.jetonHorodatage = jetonHorodatage;
    }

    public Date getDateHorodatage() {
        return dateHorodatage;
    }

    public void setDateHorodatage(Date dateHorodatage) {
        this.dateHorodatage = dateHorodatage;
    }

    public void setSignataireDetails(DetailsBean signataireDetails) {
        this.signataireDetails = signataireDetails;
    }

    public DetailsBean getSignataireDetails() {
        return signataireDetails;
    }
}
