package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefFormatEchange de la table "epm__t_ref_format_echange"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefFormatEchange extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

    public final static int ECHANGE_MAIL_EPM = 3;

}