package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Rassemble les critères de recherche d'une publication de clausier.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public class PublicationClausierCritere extends AbstractCritere implements Critere, Serializable {


    protected String plateformeUuid = null;

    public PublicationClausierCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }
    /**
     * sérialiseur.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant de la publication
     */
    private Integer id;

    /**
     * La version du clausier publié
     */
    private String version;

    /**
     * Identifiant de l'organisme auquel la clause est liée
     */
    private Integer idOrganisme;

    /**
     * Identifiant de la clause editeur que le clause client surcharge
     */
    private Integer idUtilisateur;

    /**
     * si version est actif ou pas (on ne peut avoir qu'une seul version actif chaque moment)
     */
    private Boolean actif;

    private Date datePublicationMin;

    private Date datePublicationMax;

    private Integer gestionLocal;



    /**
     * Génére la requête HQL correspondant à l'ensemble des critères.
     * @return query la requête HQL
     */
    public StringBuffer corpsRequete() {
        Map<String, Object> parameters = getParametres();
        StringBuffer clauseAnd = new StringBuffer();
        boolean startFlag = true;

        if (id != null) {
            clauseAnd.append(" WHERE ");
            clauseAnd.append("publication.id = ").append(id);
            startFlag = false;
        }

        if (idOrganisme != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.idOrganisme = ").append(idOrganisme);
            startFlag = false;
        }

        if (idUtilisateur != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.idUtilisateur = ").append(idUtilisateur);
            startFlag = false;
        }

        if (version != null && !version.isEmpty()) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("(publication.version LIKE '%").append(version).append("%' OR publication.commentaire LIKE '%").append(version).append("%')");
            startFlag = false;
        }

        if (actif != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.actif = ").append(actif);
            startFlag = false;
        }

        if (datePublicationMin != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.datePublication > :datePublicationMin");
            parameters.put("datePublicationMin", datePublicationMin);
            startFlag = false;
        }
        if (plateformeUuid != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("(epmTRefOrganisme.plateformeUuid = :plateformeUuid AND publication.idOrganisme = epmTRefOrganisme.id)");
            parameters.put("plateformeUuid", plateformeUuid);
            startFlag = false;
        }
        if (datePublicationMax != null) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.datePublication < :datePublicationMax");
            parameters.put("datePublicationMax", datePublicationMax);
            startFlag = false;
        }

        if (gestionLocal != null && gestionLocal != 0) {
            clauseAnd.append(startFlag ? " WHERE " : " AND ");
            clauseAnd.append("publication.gestionLocal = ").append(gestionLocal == 1);
            startFlag = false;
        }
        return clauseAnd;
    }

    public String toHQL() {
        StringBuilder queryString = new StringBuilder("select publication from EpmTPublicationClausier as publication ");
        if (plateformeUuid != null) {
            queryString.append(" , EpmTRefOrganisme epmTRefOrganisme ");
        }
        queryString.append(corpsRequete());
        if (getProprieteTriee() != null) {
            queryString.append(" ORDER BY publication.").append(getProprieteTriee());
            queryString.append(isTriCroissant() ? " ASC" : " DESC");
        }
        return queryString.toString();
    }

    public String toCountHQL() {
        StringBuilder queryString = new StringBuilder("select count(*) from EpmTPublicationClausier as publication ");
        if (plateformeUuid != null) {
            queryString.append(" , EpmTRefOrganisme epmTRefOrganisme ");
        }
        queryString.append(corpsRequete());
        return queryString.toString();
    }

    /**
     * @param id the id to set
     */
    public final void setId(final Integer id) {
        this.id = id;
    }

    /**
     * @param version the version to set
     */
    public final void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public final void setIdOrganisme(final Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    /**
     * @param idUtilisateur the idUtilisateur to set
     */
    public final void setIdUtilisateur(final Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public final void setActif(final Boolean actif) {
        this.actif = actif;
    }

    public Date getDatePublicationMin() {
        return datePublicationMin;
    }

    public void setDatePublicationMin(Date datePublicationMin) {
        this.datePublicationMin = datePublicationMin;
    }

    public Date getDatePublicationMax() {
        return datePublicationMax;
    }

    public void setDatePublicationMax(Date datePublicationMax) {
        this.datePublicationMax = datePublicationMax;
    }

    public Integer getGestionLocal() {
        return gestionLocal;
    }

    public void setGestionLocal(Integer gestionLocal) {
        this.gestionLocal = gestionLocal;
    }

}
