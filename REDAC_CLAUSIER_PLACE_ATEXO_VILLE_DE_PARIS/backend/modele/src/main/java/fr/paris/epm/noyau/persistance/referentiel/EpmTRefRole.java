package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefRole de la table "epm__t_ref_role"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefRole extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_PRESIDENT = 1;
    public static final int ID_TITULAIRE = 2;
    public static final int ID_REPRESENTANT_ETAT = 3;
    public static final int ID_SUPPLEANT_PRESIDENT = 4;
    public static final int ID_SUPPLEANT_TITULAIRE = 5;
    public static final int ID_AUTRE = 6;

}