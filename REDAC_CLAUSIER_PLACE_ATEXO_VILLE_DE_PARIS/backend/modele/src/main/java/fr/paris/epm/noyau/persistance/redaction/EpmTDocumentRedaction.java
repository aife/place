package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * Pojo hibernate de EpmTDocumentRedaction.
 */
@Entity
@Table(name = "epm__t_document", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTDocumentRedaction extends EpmTAbstractObject implements Comparable<EpmTDocumentRedaction>, Cloneable {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * attribut static statuts en cours.
     */
    public static final String STATUT_EN_COURS = "EN COURS";
    /**
     * attribut static Demande de validation du document.
     */
    public static final String STATUT_EN_ATTENTE_VALIDATION = "DOCUMENT REDIGE ATTENTE VALIDATION";
    /**
     * Le document est validé
     * attribut static statut eligible.
     */
    public static final String STATUT_ELIGIBLE = "ELIGIBLE";

    /**
     * Cet attribut mappe la valeur de la clé primaire.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_canevas", nullable = false)
    private int idCanevas;

    @Column(name = "id_publication", nullable = true)
    private Integer idPublication;

    /**
     * attribut consultation.
     */
    @Column(name="id_consultation", nullable=false)
    private int idConsultation;

    /**
     * réference du document.
     */
    @Column(nullable=false)
    private String reference;

    /**
     * attribut designation.
     */
    private String designation;

    /**
     * attribut lotJuridique.
     */
    @Column(name="lot_juridique")
    private int lotJuridique;

    /**
     * attribut nom du fichier.
     */
    @Column(name="nom_fichier", nullable=false)
    private String nomFichier;

    /**
     * attribut auteur.
     */
    @Column(nullable=false)
    private String auteur;

    /**
     * attribut auteur.
     */
    @Column(name = "id_utilisateur", nullable = false)
    private Integer idUtilisateur;

    /**
     * attribut qui englobe les données xml.
     */
    @Column(name="xml_data", nullable=false)
    private String xmlData;

    /**
     * attribut version.
     */
    @Column(nullable=false)
    private int version;

    /**
     * date de création du document.
     */
    @Column(name="date_creation", nullable=false)
    private Date dateCreation;

    /**
     * date de modification du document.
     */
    @Column(name="date_modification", nullable=false)
    private Date dateModification;

    /**
     * attribut statut.
     */
    @Column(nullable=false)
    private String statut;

    /**
     * attribut titre.
     */
    @Column(nullable=false)
    private String titre;

    /**
     * attribut invalider Eligibilite.
     */
    @Column(name="invalider_eligibilite", nullable=false)
    private boolean invaliderEligibilite;

    /**
     * Action liée au document
     */
    @ManyToOne
    @JoinColumn(name = "id_action", nullable = false)
    private EpmTRefActionDocument epmTRefActionDocument;

    /**
     * Commentaire lors d'un refus/validation d'un document
     */
    private String commentaire;

    /**
     * True : si le dernier document crée
     */
    @Column(name="dernier_document_cree", nullable=false)
    private boolean dernierDocumentCree;

    /**
     * Identifiant du document ayant servi à la duplication
     */
    @Column(name="id_document_initial", nullable=false)
    private int idDocumentInitial;

    /**
     * Taux de progression de rédaction du document
     */
    @Column(name="taux_progression")
    private int tauxProgression;

    // Property accessors

    public int getId() {
        return id;
    }

    public void setId(final int valeur) {
        id = valeur;
    }

    public int getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public int getIdConsultation() {
        return idConsultation;
    }

    public void setIdConsultation(int idConsultation) {
        this.idConsultation = idConsultation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(final String valeur) {
        reference = valeur;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(final String valeur) {
        designation = valeur;
    }

    public int getLotJuridique() {
        return lotJuridique;
    }

    public void setLotJuridique(final int valeur) {
        lotJuridique = valeur;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(final String valeur) {
        nomFichier = valeur;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(final String valeur) {
        auteur = valeur;
    }

    public String getXmlData() {
        return xmlData;
    }

    public void setXmlData(final String valeur) {
        xmlData = valeur;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int valeur) {
        version = valeur;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date valeur) {
        dateCreation = valeur;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(final Date valeur) {
        dateModification = valeur;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(final String valeur) {
        statut = valeur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String valeur) {
        titre = valeur;
    }

    public boolean isInvaliderEligibilite() {
        return invaliderEligibilite;
    }

    public void setInvaliderEligibilite(final boolean valeur) {
        invaliderEligibilite = valeur;
    }

    public EpmTRefActionDocument getEpmTRefActionDocument() {
        return epmTRefActionDocument;
    }

    public void setEpmTRefActionDocument(final EpmTRefActionDocument valeur) {
        this.epmTRefActionDocument = valeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    public boolean isDernierDocumentCree() {
        return dernierDocumentCree;
    }

    public void setDernierDocumentCree(final boolean valeur) {
        this.dernierDocumentCree = valeur;
    }

    public int getIdDocumentInitial() {
        return idDocumentInitial;
    }

    public void setIdDocumentInitial(final int valeur) {
        this.idDocumentInitial = valeur;
    }

    public int getTauxProgression() {
        return tauxProgression;
    }

    public void setTauxProgression(final int valeur) {
        this.tauxProgression = valeur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int compareTo(EpmTDocumentRedaction epmTdoc) {
        if (epmTdoc != null)
            return this.getNomFichier().trim().compareTo(epmTdoc.getNomFichier().trim());
        else
            return 0;
    }

    @Override
    public final EpmTDocumentRedaction clone() throws CloneNotSupportedException {
        EpmTDocumentRedaction epmTDocument = (EpmTDocumentRedaction) super.clone();
        epmTDocument.setAuteur(auteur);
        epmTDocument.setIdUtilisateur(idUtilisateur);
        epmTDocument.setCommentaire(commentaire);
        epmTDocument.setIdConsultation(idConsultation);
        epmTDocument.setDateCreation(dateCreation);
        epmTDocument.setDateModification(dateModification);
        epmTDocument.setDernierDocumentCree(dernierDocumentCree);
        epmTDocument.setDesignation(designation);
        epmTDocument.setIdCanevas(idCanevas);
        epmTDocument.setIdPublication(idPublication);
        epmTDocument.setEpmTRefActionDocument(epmTRefActionDocument);
        epmTDocument.setInvaliderEligibilite(invaliderEligibilite);
        epmTDocument.setLotJuridique(lotJuridique);
        epmTDocument.setNomFichier(nomFichier);
        epmTDocument.setReference(reference);
        epmTDocument.setStatut(statut);
        epmTDocument.setTitre(titre);
        epmTDocument.setVersion(version);
        epmTDocument.setXmlData(xmlData);
        epmTDocument.setIdDocumentInitial(idDocumentInitial);
        epmTDocument.setTauxProgression(tauxProgression);
        return epmTDocument;
    }

}
