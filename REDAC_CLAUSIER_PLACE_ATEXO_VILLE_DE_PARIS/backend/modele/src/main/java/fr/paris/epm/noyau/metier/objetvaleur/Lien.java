package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;


public class Lien implements Serializable {

    private static final long serialVersionUID = 1L;

    private String url;
    private String description;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
