package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Referentiel des status de la rédaction des clauses
 * Brouillon, à valider, validée
 * @author MGA
 *
 */
@Entity
@Table(name = "epm__t_ref_statut_redaction_clausier", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefStatutRedactionClausier extends BaseEpmTRefRedaction implements EpmTRefImportExport {
    
    /**
     * Statut lors de la création
     */
    public static final int BROUILLON = 1;
    /**
     * Statut après demande de validation
     */
    public static final int A_VALIDER = 2;
    /**
     * Statut après validation
     */
    public static final int VALIDEE = 3;

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
