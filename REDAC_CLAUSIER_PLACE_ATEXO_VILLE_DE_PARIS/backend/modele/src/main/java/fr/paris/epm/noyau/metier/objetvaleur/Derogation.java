package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;

/**
 * Classe Derogation representant une dérogation au  CCAG
 * @author MGA
 */
public class Derogation implements Serializable {

    /**
     * ID de serialisation
     */
    private static final long serialVersionUID = -4832383224318444144L;
    
    /**
     * L'article de dérogation
     */
    private String articleDerogationCCAG;
    /**
     * Les commentaires de dérogation
     */
    private String commentairesDerogationCCAG;
    /**
     * Le numéro de l'article auquel la dérogation est liée (sert à la génération du tableau de dérogations)
     */
    private String numArticle;
    
    /**
     * Détermine la couleur en prévisualisation
     */
    private String couleur;
    
    /**
     * @return the articleDerogationCCAG
     */
    public final String getArticleDerogationCCAG() {
        return articleDerogationCCAG;
    }
    /**
     * @param articleDerogationCCAG the articleDerogationCCAG to set
     */
    public final void setArticleDerogationCCAG(final String articleDerogationCCAG) {
        this.articleDerogationCCAG = articleDerogationCCAG;
    }
    /**
     * @return the commentairesDerogationCCAG
     */
    public final String getCommentairesDerogationCCAG() {
        return commentairesDerogationCCAG;
    }
    /**
     * @param commentairesDerogationCCAG the commentairesDerogationCCAG to set
     */
    public final void setCommentairesDerogationCCAG(final String commentairesDerogationCCAG) {
        this.commentairesDerogationCCAG = commentairesDerogationCCAG;
    }
    /**
     * @return the numArticle
     */
    public final String getNumArticle() {
        return numArticle;
    }
    /**
     * @param numArticle the numArticle to set
     */
    public final void setNumArticle(final String numArticle) {
        this.numArticle = numArticle;
    }
    /**
     * @return the couleur
     */
    public final String getCouleur() {
        return couleur;
    }
    /**
     * @param couleur the couleur to set
     */
    public final void setCouleur(final String couleur) {
        this.couleur = couleur;
    }
    
}
