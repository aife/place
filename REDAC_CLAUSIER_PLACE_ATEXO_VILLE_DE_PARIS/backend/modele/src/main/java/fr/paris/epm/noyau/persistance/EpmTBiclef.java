package fr.paris.epm.noyau.persistance;

import java.util.Date;

/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBiclef extends EpmTAbstractObject {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Désignation de cette bi-clef.
     */
    private String designation;

    /**
     * Nom de la personne associée.
     */
    private String nom;

    /**
     * Prénom de la personne associée.
     */
    private String prenom;

    /**
     * vrai si bi-clef de secours.
     */
    private boolean secours;

    /**
     * Clef publique.
     */
    private String clefPub;

    /**
     * "Common name" du certificat.
     */
    private String cn;

    /**
     * Émetteur du certificat.
     */
    private String emetteur;

    /**
     * certificat actif/inactif.
     */
    private boolean actif;

    /**
     * Date de debut de validité
     */
    private Date dateDebutValidite;
    /**
     * Date de fin de validité
     */
    private Date dateFinValidite;

    /**
     * @return clef publique
     */
    public String getClefPub() {
        return clefPub;
    }

    // Accesseurs
    /**
     * @param valeur Clef publique.
     */
    public void setClefPub(final String valeur) {
        this.clefPub = valeur;
    }

    /**
     * @return "Common name" du certificat.
     */
    public String getCn() {
        return cn;
    }

    /**
     * @param valeur "Common name" du certificat.
     */
    public void setCn(final String valeur) {
        this.cn = valeur;
    }

    /**
     * @return Désignation de cette bi-clef.
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param valeur Désignation de cette bi-clef.
     */
    public void setDesignation(final String valeur) {
        this.designation = valeur;
    }

    /**
     * @return Émetteur du certificat.
     */
    public String getEmetteur() {
        return emetteur;
    }

    /**
     * @param valeur Émetteur du certificat.
     */
    public void setEmetteur(final String valeur) {
        this.emetteur = valeur;
    }

    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return nom de la personne associée.
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur nom de la personne associée.
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return prénom de la personne associée.
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param valeur prénom de la personne associée.
     */
    public void setPrenom(final String valeur) {
        this.prenom = valeur;
    }

    /**
     * @return vrai si bi-clef de secours
     */
    public boolean isSecours() {
        return secours;
    }

    /**
     * @param valeur vrai si bi-clef de secours
     */
    public void setSecours(final boolean valeur) {
        this.secours = valeur;
    }

    public Date getDateDebutValidite() {
        return dateDebutValidite;
    }

    public void setDateDebutValidite(Date dateDebutValidite) {
        this.dateDebutValidite = dateDebutValidite;
    }

    public Date getDateFinValidite() {
        return dateFinValidite;
    }

    public void setDateFinValidite(Date dateFinValidite) {
        this.dateFinValidite = dateFinValidite;
    }

    public String getType() {
        return "bi clé " + (secours ? "de secours" : "permanent");
    }

    public String getTypeClef() {
        return "clé " + (secours ? "de secours" : "principale");
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }
}
