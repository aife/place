package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.EpmTModeleTransitionCal;

/**
 * @author Léon Barsamian.
 * Transition entre les étapes des modéles de calendrier.
 */
public class EpmTRefTransitionCalendrier extends EpmTModeleTransitionCal {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * Étape source.
     */
    private EpmTRefEtapeCalendrier source;

    /**
     * Étape cible.
     */
    private EpmTRefEtapeCalendrier cible;

    // Méthodes
    /**
     * Réinitialisation des valeurs fixes et variables.
     */
    public final void reinitialiser() {
        valeurFixe = valeurFixeInit;
        valeurVariable = valeurVariableInit;
    }

    /**
     * Modifie les valeurs fixes initiales et modifiées.
     * @param valeur valeur à positionner
     */
    public final void modifierValeurFixeInit(final int valeur) {
        valeurFixeInit = valeur;
        valeurFixe = valeur;
    }

    /**
     * Modifie les valeurs variables initiales et modifiées.
     * @param valeur valeur à positionner
     */
    public final void modifierValeurVariableInit(final int valeur) {
        valeurVariableInit = valeur;
        valeurVariable = valeur;
    }

    /**
     * @return cible
     */
    public EpmTRefEtapeCalendrier getCible() {
        return cible;
    }

    /**
     * @param valeur cible
     */
    public void setCible(final EpmTRefEtapeCalendrier valeur) {
        this.cible = valeur;
    }

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return source
     */
    public EpmTRefEtapeCalendrier getSource() {
        return source;
    }

    /**
     * @param valeur source
     */
    public void setSource(final EpmTRefEtapeCalendrier valeur) {
        this.source = valeur;
    }

}