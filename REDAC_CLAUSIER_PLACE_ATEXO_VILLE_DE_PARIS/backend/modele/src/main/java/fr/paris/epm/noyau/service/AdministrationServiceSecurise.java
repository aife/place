package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.metier.MessageActualiteCritere;
import fr.paris.epm.noyau.metier.ProfilCritere;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefThemeHabilitation;

import java.util.List;

/**
 * Interface sécurisée du service d'administration. Ce service est utilisé pour
 * accéder aux méthodes CRUD (création, chargement, modification, suppression)
 * de Profil et Utilisateur.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface AdministrationServiceSecurise extends GeneriqueServiceSecurise {


    /**
     * Cette méthode permet de recupérer un utilisateur grâce à son GUID. C'est
     * pourquoi cette méthode n'est pas sécurisé.
     * @param guid identifiant de l'utilisateur appelant cette méhode
     * @return utilisateur possédant cet identifiant en base
     */
    EpmTUtilisateur chargerUtilisateur(String guid);


    /**
     * @param id identifiant de l'utilisateur appelant cette méhode
     * @param directionId de la direction/service à charger
     * @return direction/service possédant cet identifiant en base
     */
    EpmTRefDirectionService chargerDirectionService(int id, int directionId);



    /**
     * Génère un token qui permettra d'authentifier l'utilisateur plus tard
     * @param utilisateur
     * @return
     */
    String genererTokenUnique(EpmTUtilisateur utilisateur);

    /**
     * Retourne l'utilisateur correspondant à ce token ou null
     * @param token
     * @return
     */
    EpmTUtilisateur authentifierUtilisateurParToken(String token);

    /**
     * supprimer les informations d'authentification
     * @param username
     */
    void supprimerAuthentificationToken(final int id, String username);
    
    /**
     * modifier les informations d'authentification en mettant les valeurs dans param authToken
     * @EpmTAuthentificationToken authToken
     */
    EpmTAuthentificationToken modifierAuthentificationToken(final int id, EpmTAuthentificationToken authToken);



}
