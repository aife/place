package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefNature de la table "epm__t_ref_nature"
 * Pojo des valeurs de nomenclature des champs additionnels
 * Created by nty on 26/06/17.
 * @author Tyurin Nikolay
 * @author nty
 */
public class EpmTRefNomenclatureValeur extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private int idNomenclature;

    private int ordre;

    public int getIdNomenclature() {
        return idNomenclature;
    }

    public void setIdNomenclature(int idNomenclature) {
        this.idNomenclature = idNomenclature;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

}