/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class IdMap extends EpmTAbstractObject {


    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private Integer idEtapeCal;

    /**
     * Identifiant.
     */
    private Integer idLotConsultation;


    // Méthodes

    /**
     * @return code de hachage
     */
    public final int hashCode() {
        int res = 1;
        if (idEtapeCal != null) {
            res = Constantes.PREMIER * res + idEtapeCal.hashCode();
        }
        if (idLotConsultation != null) {
            res = Constantes.PREMIER * res + idLotConsultation.hashCode();
        }
        return res;
    }

    /**
     * @param obj objet comparé
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IdMap autre = (IdMap) obj;
        if (idEtapeCal == null) {
            if (autre.idEtapeCal != null) {
                return false;
            }
        } else if (!idEtapeCal.equals(autre.idEtapeCal)) {
            return false;
        }
        if (idLotConsultation == null) {
            if (autre.idLotConsultation != null) {
                return false;
            }
        } else if (!idLotConsultation.equals(autre.idLotConsultation)) {
            return false;
        }
        return true;
    }

    // Accesseurs
    /**
     * @return identifiant
     */
    public Integer getIdEtapeCal() {
        return idEtapeCal;
    }

    /**
     * @param valeur identifiant
     */
    public void setIdEtapeCal(final Integer valeur) {
        this.idEtapeCal = valeur;
    }

    /**
     * @return identifiant
     */
    public Integer getIdLotConsultation() {
        return idLotConsultation;
    }

    /**
     * @param valeur identifiant
     */
    public void setIdLotConsultation(final Integer valeur) {
        this.idLotConsultation = valeur;
    }

}
