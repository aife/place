/**
 * 
 */
package fr.paris.epm.noyau.persistance.documents;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import fr.paris.epm.noyau.persistance.referentiel.*;

import java.util.Set;

/**
 * Cette classe contient les informations relatives aux modéles de documents openoffice (traitement de texte)
 * ajoutés dans l'application par l'utilisateur via le module administration.
 * 
 * Cet objet contient : 
 *  - des informations relatives aux champs de fusion présent dans le modéle openOffice que l'utilisateur souhaite hériter de l'application. 
 *  - des informations sur la disponibilité du document (état d'avancement de la consultation)
 *  - le format du document généré
 *   
 * @author Léon Barsamian
 */
public class EpmTDocumentModele extends EpmTAbstractObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 5821549487816401022L;

    /**
     * Ensemeble des nom de modele de mail
     */
    public enum nomMailEnum {
        ENVOI_CONVOCATION_MEMBRE("mailEnvoiConvocationMembre"),
        ENVOI_ODJ_DIRECTION("mailEnvoiOrdreDuJourDirection"),
        ENVOI_MISE_A_DISPOSITION("mailEnvoiMiseADisposition"),
        MAIL_ALERTE_VALIDATION_INTERMEDIAIRE_DCE("mailAlerteValidationIntermediaireDce"),
        MAIL_ALERTE_VALIDATION_DCE("mailAlerteValidationDce"),
        MAIL_ALERTE_RECEPTION_QUESTION("mailAlerteReceptionQuestion"),
        MAIL_ALERTE_REMISE_PLIS_DEPASSEE("mailAlerteRemisePlisDepassee"),
        MAIL_ALERTE_MAJ_CALENDRIER_REEL("mailAlerteMajCalendrierReel"),
        MAIL_ALERTE_VALIDATION_DEMANDE_ACHAT("mailAlerteValidationDemandeAchat"),
        MAIL_ALERTE_REFUS_DEMANDE_ACHAT("mailAlerteRefusDemandeAchat"),
        MAIL_ALERTE_RAPPEL_SUIVI_DATE("mailAlerteRappelSuiviDate"),
        MAIL_ALERTE_REMISE_PLIS_DEPASSEE_ET_DEPOTS_INCOMPLETS("mailAlerteRemisePlisDepasseeEtDepotsIncomplets"),
        MODELE_RESOLUTION_ECHANGE_DEMAT("modeleResolutionEchangeDemat"),
        MODELE_ERREUR_ECHANGE_DEMAT("modeleErreurEchangeDemat"),
        MAIL_ALERTE_ECHEANCE_VALIDE_CERTIFICATS("mailAlerteEcheanceValideCertificats");

        private final String valeur;

        nomMailEnum(String val) {
            valeur = val;
        }

        public String getValeur() {
            return valeur;
        }
    };
    
    /**
     * Identifiant du document.
     */
    private int id;
    
    /**
     * Nom donné par l'utilisateur au document.
     */
    private String nom;
    
    /**
     * Nom du fichier modele uploader par l'utilisateur.
     */
    private String nomFichierModele;
    
    /**
     * Description du document
     */
    private String description;
    
    /**
     * Le type de document généré (odt, pdf, doc)
     */
    private EpmTRefDocumentType type;
    
    /**
     * étape pour laquelle le document sera disponible à la génération.
     */
    private EpmTRefDocumentEtape etape;
    
    /**
     *  page de l'application sur laquelle sera généré le document.
     */
    private Set<EpmTRefDocumentEmplacement> emplacements;
    
    /**
     * Paramétrage des critéres de recherche des entreprises à afficher dans la 
     * liste des bénéficiaires du document dans le module passation
     */
    private Set<EpmTRefDocumentDestinataire> destinataires;
    
    /**
     * Liste des champs de fusion associés au document.
     */
    private Set<EpmTRefChampFusion> champsDeFusion;

    /**
     * True si le document se génére par lot.
     */
    private boolean associeLot;
    
    /**
     * Le template.
     */
    private byte[] fichierAssocie;
    
    
    /**
     * taille du fichier en byte
     */
    private long taille;
    
    /**
     * Corps du mail dans le cas d'un mail ou code xml dans le cas d'un excel.
     */
    private String contenu;
    
    /**
     * Objet du mail.
     */
    private String objet;
    
    /**
     * Defini si le concerne à un champ de fusion du module execution
     */
    private boolean moduleExecution;    
    
    /**
     * @return Identifiant du document.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur Identifiant du document.
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return Nom donné par l'utilisateur au document.
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur Nom donné par l'utilisateur au document.
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return Description du document
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param valeur Description du document
     */
    public void setDescription(final String valeur) {
        this.description = valeur;
    }

    /**
     * @return Le type de document généré (odt, pdf, doc)
     */
    public EpmTRefDocumentType getType() {
        return type;
    }

    /**
     * @param valeur Le type de document généré (odt, pdf, doc)
     */
    public void setType(final EpmTRefDocumentType valeur) {
        this.type = valeur;
    }

    /**
     * @return étape pour laquelle le document sera disponible à la génération.
     */
    public EpmTRefDocumentEtape getEtape() {
        return etape;
    }

    /**
     * @param valeur étape pour laquelle le document sera disponible à la génération.
     */
    public void setEtape(final EpmTRefDocumentEtape valeur) {
        this.etape = valeur;
    }

    /**
     * @return page de l'application sur laquelle sera généré le document.
     */
    public Set<EpmTRefDocumentEmplacement> getEmplacements() {
        return emplacements;
    }

    /**
     * @param valeur page de l'application sur laquelle sera généré le document.
     */
    public void setEmplacements(Set<EpmTRefDocumentEmplacement> valeur) {
        this.emplacements = valeur;
    }

    /**
     * @return True si le document se génére par lot.
     */
    public boolean isAssocieLot() {
        return associeLot;
    }

    /**
     * 
     * @param valeur True si le document se génére par lot.
     */
    public void setAssocieLot(boolean valeur) {
        this.associeLot = valeur;
    }

    /**
     * @return Paramétrage des critéres de recherche des entreprises à afficher dans la 
     * liste des bénéficiaires du document dans le module passation
     */
    public Set<EpmTRefDocumentDestinataire> getDestinataires() {
        return destinataires;
    }

    /**
     * @param valeur Paramétrage des critéres de recherche des entreprises à afficher dans la 
     * liste des bénéficiaires du document dans le module passation
     */
    public void setDestinataires(final Set<EpmTRefDocumentDestinataire> valeur) {
        this.destinataires = valeur;
    }

    /**
     * @return Liste des champs de fusion associés au document.
     */
    public Set<EpmTRefChampFusion> getChampsDeFusion() {
        return champsDeFusion;
    }

    /**
     * @param valeur Liste des champs de fusion associés au document.
     */
    public void setChampsDeFusion(Set<EpmTRefChampFusion> valeur) {
        this.champsDeFusion = valeur;
    }
    
    /**
     * @return Nom du fichier modele uploader par l'utilisateur.
     */
    public String getNomFichierModele() {
        return nomFichierModele;
    }

    /**
     * @param valeur Nom du fichier modele uploader par l'utilisateur.
     */
    public void setNomFichierModele(final String valeur) {
        this.nomFichierModele = valeur;
    }

    public byte[] getFichierAssocie() {
        return fichierAssocie;
    }

    public void setFichierAssocie(final byte[] valeur) {
        this.fichierAssocie = valeur;
    }
    
    /**
     * 
     * @return taille du fichier en byte
     */
    public long getTaille() {
        return taille;
    }

    /**
     * 
     * @param valeur taille du fichier en byte
     */
    public void setTaille(final long valeur) {
        this.taille = valeur;
    }

	/**
	 * @return Corps du mail dans le cas d'un mail ou code xml dans le cas d'un excel.
	 */
	public String getContenu() {
		return contenu;
	}

	/**
	 * @param valeur Corps du mail dans le cas d'un mail ou code xml dans le cas d'un excel.
	 */
	public void setContenu(final String valeur) {
		this.contenu = valeur;
	}

	/**
	 * @return Objet du mail.
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * @param valeur Objet du mail.
	 */
	public void setObjet(final String valeur) {
		this.objet = valeur;
	}
	
	/**
	 * 
	 * @return : Defini si le concerne à un champ de fusion du module execution
	 */
	public boolean isModuleExecution() {
		return moduleExecution;
	}

	/**
	 * 
	 * @param valeur : Defini si le concerne à un champ de fusion du module execution
	 */
	public void setModuleExecution(final boolean valeur) {
		this.moduleExecution = valeur;
	}
}
