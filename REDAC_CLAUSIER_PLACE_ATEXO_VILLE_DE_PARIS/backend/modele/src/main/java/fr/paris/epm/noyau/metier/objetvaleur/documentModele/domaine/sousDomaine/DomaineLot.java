package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * consultation.lot.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineLot implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	private String identifiant;
	
    /**
     * Numero du lot.
     */
    private String referenceLot;

    private String intituleLot;
    /**
     * Pour chaque tranche nature_de_la_tranche : intitule_de_la_tranche.
     */
    private String txtAllotie;
    /**
     * Pour ChaqueTranche nature_de_la_tranche : estimation_HT_detaillee.
     */
    private String txtEstimationAllotie;

    private DomaineFormeDePrix formeDePrix;

    private String descriptionLot;
    
    private DomainePairesTypesComplexes pairesTypesComplexes;
    
    private DomainePairesTypes pairesTypes;

    private DomaineClauseSociale clauseSociale;

    private DomaineClauseEnvironnementale clauseEnvironnementale;

    /**
     * Liste des tranches associées au lot.
     */
    private List<DomaineTranche> listeTranches;
    
    private String naturePrestationLot;
    
    /**
     * Liste de lots techniques associés au lot
     */
    private List<DomaineLotTechique> listeLotTechnique;
    
    /**
     * Informations concernant la reconduction au lot
     */
    private DomaineReconduction reconduction;

    /**
     * Information contenant la reference CCAG
     */
    private String ccagDeReference;
    
    /**
	 * Informations concernant les variantes autorisées et obligatoires du lot
	 */
    private DomaineVariantes variantes;

    /**
     * type du critere d'attribution.
     */
    private String typeCriteresAttribution;

    /**
     * libelle du critere d'attribution.
     */
    private String libelleCritereAttribution;

    /**
     * liste des critere d'attribution.
     */
    private List<DomaineCritereConsultation> listeCriteres;

    private DomaineDureeMarche dureeMarche;

    /**
	 * L'idéntifiant en base, utilisée pour les clauses tableau du type crière
	 * du prix plus bas pour afficher le document rédigé pour un seul lot
	 * 
	 * @return
	 */
    public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
     * @return Numero du lot.
     */
    public final String getReferenceLot() {
        return referenceLot;
    }

    /**
     * @param valeur Numero du lot.
     */
    public final void setReferenceLot(final String valeur) {
        this.referenceLot = valeur;
    }

    public final String getIntituleLot() {
        return intituleLot;
    }

    public final void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return Pour chaque tranche nature_de_la_tranche :
     *         intitule_de_la_tranche.
     */
    public final String getTxtAllotie() {
        return txtAllotie;
    }

    /**
     * @param valeur Pour chaque tranche nature_de_la_tranche :
     *            intitule_de_la_tranche.
     */
    public final void setTxtAllotie(final String valeur) {
        this.txtAllotie = valeur;
    }

    /**
     * @return Pour ChaqueTranche nature_de_la_tranche :
     *         estimation_HT_detaillee.
     */
    public final String getTxtEstimationAllotie() {
        return txtEstimationAllotie;
    }

    /**
     * @param valeur Pour ChaqueTranche nature_de_la_tranche :
     *            estimation_HT_detaillee.
     */
    public final void setTxtEstimationAllotie(final String valeur) {
        this.txtEstimationAllotie = valeur;
    }

    public final DomaineFormeDePrix getFormeDePrix() {
        return formeDePrix;
    }

    public final void setFormeDePrix(DomaineFormeDePrix formeDePrix) {
        this.formeDePrix = formeDePrix;
    }

    /**
     * @return Liste des tranches associées au lot.
     */
    public final List<DomaineTranche> getListeTranches() {
        return listeTranches;
    }

    /**
     * @param valeur Liste des tranches associées au lot.
     */
    public final void setListeTranches(final List<DomaineTranche> valeur) {
        this.listeTranches = valeur;
    }

    public final String getDescriptionLot() {
        return descriptionLot;
    }

    public final void setDescriptionLot(final String valeur) {
        this.descriptionLot = valeur;
    }

    /**
     * @return the pairesTypesComplexes
     */
    public final DomainePairesTypesComplexes getPairesTypesComplexes() {
        return pairesTypesComplexes;
    }

    /**
     * @param valeur the pairesTypesComplexes to set
     */
    public final void setPairesTypesComplexes(final DomainePairesTypesComplexes valeur) {
        this.pairesTypesComplexes = valeur;
    }

    /**
     * @return the pairesTypes
     */
    public final DomainePairesTypes getPairesTypes() {
        return pairesTypes;
    }

    /**
     * @param valeur the pairesTypes to set
     */
    public final void setPairesTypes(final DomainePairesTypes valeur) {
        this.pairesTypes = valeur;
    }

    /**
     * @return the naturePrestationLot
     */
    public final String getNaturePrestationLot() {
        return naturePrestationLot;
    }

    /**
     * @param valeur the naturePrestationLot to set
     */
    public final void setNaturePrestationLot(final String valeur) {
        this.naturePrestationLot = valeur;
    }
    
    /**
     * 
     * @return liste des lots techniques associés au lot
     */
	public final List<DomaineLotTechique> getListeLotTechnique() {
        Collections.sort(listeLotTechnique); // ??? - normalement elle est déjà triée !!!
        return listeLotTechnique;
    }

	/**
	 * 
	 * @param valeur la liste des lots techniques associés au lot
	 */
	public final void setListeLotTechnique(final List<DomaineLotTechique> valeur) {
		this.listeLotTechnique = valeur;
	}

	/**
	 * 
	 * @return les informations concernant la reconduction du lot
	 */
	public final DomaineReconduction getReconduction() {
		return reconduction;
	}

	/**
	 * 
	 * @param valeur les informations concernant la reconduction du lot
	 */
	public final void setReconduction(final DomaineReconduction valeur) {
		this.reconduction = valeur;
	}

	/**
	 * 
	 * @return les informations concernant les variantes obligatoires et/ou autorisées
	 */
	public final DomaineVariantes getVariantes() {
		return variantes;
	}

	/**
	 *  
	 * @param valeur les informations concernant les variantes obligatoires et/ou autorisées
	 */
	public final void setVariantes(final DomaineVariantes valeur) {
		this.variantes = valeur;
	}
	
    public final List<DomaineCritereConsultation> getListeCriteres() {
        return listeCriteres;
    }

    public final void setListeCriteres(
            final List<DomaineCritereConsultation> valeur) {
        this.listeCriteres = valeur;
    }


    public final String getTypeCriteresAttribution() {
        return typeCriteresAttribution;
    }

    public final void setTypeCriteresAttribution(final String valeur) {
        this.typeCriteresAttribution = valeur;
    }

    public DomaineDureeMarche getDureeMarche() {
        return dureeMarche;
    }

    public void setDureeMarche(DomaineDureeMarche dureeMarche) {
        this.dureeMarche = dureeMarche;
    }

    public DomaineClauseSociale getClauseSociale() {
        return clauseSociale;
    }

    public void setClauseSociale(DomaineClauseSociale clauseSociale) {
        this.clauseSociale = clauseSociale;
    }

    public DomaineClauseEnvironnementale getClauseEnvironnementale() {
        return clauseEnvironnementale;
    }

    public void setClauseEnvironnementale(DomaineClauseEnvironnementale clauseEnvironnementale) {
        this.clauseEnvironnementale = clauseEnvironnementale;
    }

    public String getLibelleCritereAttribution() {
        return libelleCritereAttribution;
    }

    public void setLibelleCritereAttribution(String libelleCritereAttribution) {
        this.libelleCritereAttribution = libelleCritereAttribution;
    }


    public String getCcagDeReference() {
        return ccagDeReference;
    }

    public void setCcagDeReference(String ccagDeReference) {
        this.ccagDeReference = ccagDeReference;
    }
}
