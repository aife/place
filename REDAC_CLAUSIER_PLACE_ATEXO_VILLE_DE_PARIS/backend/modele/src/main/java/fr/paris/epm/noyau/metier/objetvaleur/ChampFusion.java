package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;


public class ChampFusion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String chemin;
    private String libelle;

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    private InfoBulle infoBulle;
    protected String blocUrl;

    protected boolean simple;



    public String getBlocUrl() {
        return blocUrl;
    }

    public void setBlocUrl(String blocUrl) {
        this.blocUrl = blocUrl;
    }

    public boolean isSimple() {
        return simple;
    }

    public void setSimple(boolean simple) {
        this.simple = simple;
    }



    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public InfoBulle getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulle infoBulle) {
        this.infoBulle = infoBulle;
    }





}
