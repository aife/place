package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefModeGestionContrat de la table "epm__t_ref_modalite_retrait"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefModeGestionContrat extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int DECOMPTE = 1;
    public static final int FACTURE = 2;

}