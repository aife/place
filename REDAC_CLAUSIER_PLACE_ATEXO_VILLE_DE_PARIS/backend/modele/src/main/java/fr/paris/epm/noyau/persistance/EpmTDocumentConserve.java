package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Date;

/**
 * @author Mounthei
 */
public class EpmTDocumentConserve extends EpmTAbstractDocument implements Comparable<EpmTDocumentConserve> {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Lot.
     */
    private EpmTBudLot lot;

    /**
     * Objet.
     */
    private String objet; 

    /**
     * Date d'insertion.
     */
    private Date dateInsertion;

    /**
     * Commentaire.
     */
    private String commentaire;

    /**
     * Identifiant de la consultation associée.
     */
    private Integer idConsultation;

    /**
     * identifiant de l'avenant.
     */
    private Integer idAvenant;

    /**
     * Étape spécifique.
     */
    private EpmTEtapeSpec etape;
    
    /**
     * eligible au DCE ou non
     */
    private boolean eligibleDCE;
    
    /**
     * eligible au DCE ou non
     */
    private boolean docPreRempli;

    /**
     * elle est <b>vrai</b> si le document est administrable, <b>faux</b> si autres 
     */
    private boolean documentsAdministrables;
    
    
    /**
     * Id Document  
     */
    private EpmTDocument document;
    
    /**
     * @return commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @param valeur commentaire
     */
    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    /**
     * @return date d'insertion
     */
    public Date getDateInsertion() {
        return dateInsertion;
    }

    /**
     * @param valeur date d'insertion
     */
    public void setDateInsertion(final Date valeur) {
        this.dateInsertion = valeur;
    }

    /**
     * @return lot
     */
    public EpmTBudLot getLot() {
        return lot;
    }

    /**
     * @param valeur lot
     */
    public void setLot(final EpmTBudLot valeur) {
        this.lot = valeur;
    }

    /**
     * @return objet
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param valeur objet
     */
    public void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return étape spécifique correspondante
     */
    public EpmTEtapeSpec getEtape() {
        return etape;
    }

    /**
     * @param valeur étape spécifique correspondante
     */
    public void setEtape(final EpmTEtapeSpec valeur) {
        this.etape = valeur;
    }


    /**
     * @return idConsultation identifiant de la consultation
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param valeur identifiant de la consultation
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @param valeur identifiant de l'avenant
     */
    public void setIdAvenant(final Integer valeur) {
        this.idAvenant = valeur;
    }

    /**
     * @return idConsultation identifiant de la consultation
     */
    public Integer getIdAvenant() {
        return idAvenant;
    }
    
    /**
     * @return the eligibleDCE
     */
    public boolean isEligibleDCE() {
        return eligibleDCE;
    }

    /**
     * @param eligibleDCE the eligibleDCE to set
     */
    public void setEligibleDCE(boolean valeur) {
        this.eligibleDCE = valeur;
    }
    /**
     * @return the isPreRempli
     */
    public boolean getDocPreRempli() {
        return docPreRempli;
    }
    

    /**
     * @param isPreRempli the isPreRempli to set
     */
    public void setDocPreRempli(boolean valeur) {
        this.docPreRempli = valeur;
    }

    /**
     * @return the documentsAdministrables
     */
    public boolean isDocumentsAdministrables() {
        return documentsAdministrables;
    }

    /**
     * @param documentsAdministrables the documentsAdministrables to set
     */
    public void setDocumentsAdministrables(boolean valeur) {
        this.documentsAdministrables = valeur;
    }

    /**
     * @return the document
     */
    public EpmTDocument getDocument() {
        return document;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(EpmTDocument valeur) {
        this.document = valeur;
    }    

    /**
     * FIXME, attention, on pourrait avoir 2 noms de fichier identiques!
     * L'uri du fichier étant unique elle est utilisée pour le hashcode.
     * @see java.lang.Object#hashCode()
     * @return code de hacahge
     */
    public final int hashCode() {
        int result = 1;
        if (getNomFichier() != null) {
            result = Constantes.PREMIER * result + getNomFichier().hashCode();
        }
        return result;
    }

    /**
     * Test d'égalité sur le commentaire, nom de fichier.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj objet à comparer
     * @return résulat de la comparaison
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTDocumentConserve autre = (EpmTDocumentConserve) obj;

        Integer idInt = getId();
        Integer idIntOther = autre.getId();
        boolean res =
                (idInt.equals(idIntOther) && objet.equals(autre.objet)
                        && getNomFichier().equals(autre.getNomFichier()) && commentaire
                        .equals(autre.commentaire));
        res =
                res
                        && (getFichierSurDisque().equals(autre
                                .getFichierSurDisque()));
        return res;
    }

    /**
     * Retourne 0 si les 2 id des 2 objets du type EpmTDocumentConserve
     * sont égaux. Retourne -1 si l'id courant est inférieur à celui de
     * arg0; Retourne +1 dans le cas inverse
     * @param obj l'autre EpmTDocumentConserve à comparer.
     * @return un entier exprimant le résultat exprimé plus haut.
     */
    public final int compareTo(final EpmTDocumentConserve obj) {
        if (this.getId() > obj.getId()) {
            return 1;
        } else if (this.getId() < obj.getId()) {
            return -1;
        } else {
            return 0;
        }

    }



}
