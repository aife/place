package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate Lot technique de la Consultation.
 * 
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBudLotOuConsultation extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;


    /**
     * document pouvant etre associé à un lot ou à tous les lots
     */
    // private EpmTDocument documentPreinscription;
    private Calendar dateValeurPreinscription;

    /**
     * La forme de prix associée au bloc (PF/PU ou PM).
     */
    private EpmTBudFormePrix epmTBudFormePrix;

    /**
     * Référenciel CCAG.
     */
    private EpmTRefCcag epmTRefCcag;

    /**
     * Sélect Tranche type prix.
     */
    private EpmTRefTrancheTypePrix epmTRefTrancheTypePrix;

    /**
     * Caractère reconductible ou non de la consultation.
     */
    private String reconductible;

    /**
     * Nombre de reconductions.
     */
    private Integer nbReconductions;

    /**
     * Modalités de reconduction.
     */
    private String modalitesReconduction;

    /**
     * Variantes obligatoire : OUI ou NON.
     */
    private String variantesExigees;

    /**
     * Variantes autorisées : OUI ou NON.
     */
    private String variantesAutorisees;

    /**
     * Options techniques imposées.
     */
    private String optionsTechniquesImposees;

    /**
     * Description des options ipmosées.
     */
    private String descriptionOptionsImposees;

    /**
     * indique si une consultation à un ou plusieurs lots dissociés. Dans le cas
     * d'un lot indique si le lot est dissocié.
     */
    private boolean lotDissocie;

    /**
     * Décomposition en lots techniques.
     */
    private String decompositionLotsTechniques;

    /**
     * Ensemble des lots techniques attachés à une consultation non allotie.
     */
    private Set<EpmTLotTechnique> epmTLotTechniques = new HashSet<EpmTLotTechnique>();

    /**
     * Ensemble des tranches attachées à une consultation non allotie.
     */
    private Set<EpmTBudTranche> epmTBudTranches = new HashSet<EpmTBudTranche>();


    /**
     * Si !null contient le nombre de candidatures à retenir.
     */
    private Integer numerusClausus;

    /**
     * Ensemble des marchés subséquents qui dépendent de cette consultation ou
     * lot (accord cadre).
     */
    private Set marchesSubsequents = new HashSet(0);

    /**
     * Dans le cas d'un marché subséquent, accord cadre dont il dépend.
     */
    private EpmTBudLotOuConsultation accordCadre;

    /**
     * Écran Commission preinscription consultation.
     */
    private Double estimationPfPreinscription;

    /**
     * Écran Commission preinscription consultation.
     */
    private Double estimationBcMinPreinscription;

    /**
     * Écran Commission preinscription consultation.
     */
    private Double estimationBcMaxPreinscription;

    /**
     * Écran Attrib_Pressent-Recommandation-attributaires-pressentis.
     */
    private Double estimationPf;

    /**
     * Écran Attrib_Pressent-Recommandation-attributaires-pressentis.
     */
    private Double estimationBcMin;

    /**
     * Écran Attrib_Pressent-Recommandation-attributaires-pressentis.
     */
    private Double estimationBcMax;
    /**
     * Ecran de tableau d'ouverture des offres partie forfaitaire.
     */
    private Double estimationPfTabOuvOffre;
    /**
     * Ecran de tableau d'ouverture des offres devis estimé.
     */
    private Double estimationDqeTabOuvOffre;
    /**
     * Statut de l'écran ouverture des candidatures.
     */
    private int statutOuvertureCandidature;

    private boolean classementCandidatureEcarte;

    private boolean classementCandidatureEcartePA;

    private boolean classementRecommandationCandidature;

    private boolean classementRecommandationCandidaturePA;

    /**
     * lieu d'exectution du marché (provenant de go).
     */
    private String lieuExecution;

    /**
     * Montant estimé de la preinscription.
     */
    private Double estimationDqe;

    /**
     * Montant estimé commande type au moment du tableau de l'ouverture des offres.
     */
    private Double estimationCtTabOuvOffre;

    /**
     * Montant estimé DQE au moment de la preinscription.
     */
    private Double estimationDqePresinscription;

    /**
     * Montant estimé commande type au moment de la preinscription.
     */
    private Double estimationCtPresinscription;

    /**
     * true si preparation d'un formaulaire BOAMP.
     */
    private boolean avisAttributionPresent = false;

    /**
     * Date à laquelle GO transmet à EPM le marché GO (clic de l'utilisateur GO
     * sur le bouton "Envoyer à EPM")
     */
    private Date dateEnvoiMarcheGo;
    
    private Boolean choixSupportAttributionTousBeneficiaires;

    /**
     * Liste des beneficiaires du marche.
     */
    private Set<EpmTRefBeneficiaireMarche> beneficiaireMarches;
    // Hashcode inutile

    // equals inutile

    /**
     * Unité des valeurs min et max.
     */
    private EpmTRefUnite unite;
    
    /**
     * Indique si le marché est soumis à des clause environnementales.
     */
    private boolean clausesEnvironnementales;
    
    private Set<EpmTRefClausesEnvironnementales> clausesEnvironnementalesChoixUtilisateur;

    private Set<EpmTRefClausesSociales> clausesSocialesChoixUtilisateur;

    private boolean clausesSocialesPresentCriteresAttribution;

    /**
     * Le marché est réservé aux quelles structures.
     */
    private Set<EpmTRefTypeStructureSociale> listeStructureSocialeReserves;


    /**
     * Indique si le marché est soumis à des clause sociales.
     */
    private boolean clausesSociales;

    private Double estimationBcMinTabOuvOffre;
    
    private Double estimationBcMaxTabOuvOffre;
    
    private Double estimationPfValidationClassementOffre;
    
    private Double estimationDqeValidationClassementOffre;
    
    private Double estimationBcMinValidationClassementOffre;
    
    private Double estimationBcMaxValidationClassementOffre;
    
    /**
     * Liste des paires types
     */
    private Set<EpmTValeurConditionnementExterne> epmTValeurConditionnementExternes = new HashSet<>();
    /**
     * Liste des paires types complexes
     */
    private Set<EpmTValeurConditionnementExterneComplexe> epmTValeurConditionnementExternesComplexe = new HashSet<>();
    
    /**
     * contient l'url de l'espace collaboratif du lot dessocié
     */
    private String sousEspaceCollaboratifUrl;
    
    /**
     * Référenciel Droits de propriété intellectuelle
     */
    private EpmTRefDroitProprieteIntellectuelle droitProprieteIntellectuelle;
    
    /**
     * @see java.lang.Object#toString()
     * @return représentation texte de l'objet
     */
    public final String toString() {
        return super.toString() +
                "\nSélect Tranche type prix: " + epmTRefTrancheTypePrix +
                "\nBud Forme de prix: " + epmTBudFormePrix +
                "\nRéf. CCAG: " + epmTRefCcag + droitProprieteIntellectuelle +
                "\nReconductible: " + reconductible +
                "\nNombre de reconductions: " + nbReconductions +
                "\nModalités de reconduction: " + modalitesReconduction +
                "\nVariantes autorisées: " + variantesAutorisees +
                "\nOptions techniques imposées: " + optionsTechniquesImposees +
                "\nDescription des options imposées: " + descriptionOptionsImposees +
                "\nDécomposition en postes techniques: " + decompositionLotsTechniques +
                "\nTranches associées: " + epmTBudTranches +
                "\nPostes techniques associés: " + epmTLotTechniques;
    }

    /**
     * @return bud forme de prix
     */
    public EpmTBudFormePrix getEpmTBudFormePrix() {
        return this.epmTBudFormePrix;
    }

    /**
     * @param valeur
     *            bud forme de prix
     */
    public void setEpmTBudFormePrix(final EpmTBudFormePrix valeur) {
        this.epmTBudFormePrix = valeur;
    }

    /**
     * @return référentiel CCAG
     */
    public EpmTRefCcag getEpmTRefCcag() {
        return this.epmTRefCcag;
    }

    /**
     * @param valeur
     *            référentiel CCAG
     */
    public void setEpmTRefCcag(final EpmTRefCcag valeur) {
        this.epmTRefCcag = valeur;
    }

    /**
     * @return sélect tranche et type de prix
     */
    public EpmTRefTrancheTypePrix getEpmTRefTrancheTypePrix() {
        return this.epmTRefTrancheTypePrix;
    }

    /**
     * @param valeur
     *            sélect tranche et type de prix
     */
    public void setEpmTRefTrancheTypePrix(final EpmTRefTrancheTypePrix valeur) {
        this.epmTRefTrancheTypePrix = valeur;
    }

    /**
     * @return reconductibilité (oui/non)
     */
    public String getReconductible() {
        return this.reconductible;
    }

    /**
     * @param valeur
     *            reconductibilité (oui/non)
     */
    public void setReconductible(final String valeur) {
        this.reconductible = valeur;
    }

    /**
     * @return nombre de reconductions
     */
    public Integer getNbReconductions() {
        return this.nbReconductions;
    }

    /**
     * @param valeur
     *            nombre de reconductions
     */
    public void setNbReconductions(final Integer valeur) {
        this.nbReconductions = valeur;
    }

    /**
     * @return modalités de reconduction
     */
    public String getModalitesReconduction() {
        return this.modalitesReconduction;
    }

    /**
     * @param valeur
     *            modalités de reconduction
     */
    public void setModalitesReconduction(final String valeur) {
        this.modalitesReconduction = valeur;
    }

    public String getVariantesExigees() {
        return variantesExigees;
    }

    public void setVariantesExigees(String valeur) {
        this.variantesExigees = valeur;
    }

    /**
     * @return variantes autorisées
     */
    public String getVariantesAutorisees() {
        return this.variantesAutorisees;
    }

    /**
     * @param valeur
     *            variantes autorisées
     */
    public void setVariantesAutorisees(final String valeur) {
        this.variantesAutorisees = valeur;
    }

    /**
     * @return options techniques imposées (oui/non)
     */
    public String getOptionsTechniquesImposees() {
        return this.optionsTechniquesImposees;
    }

    /**
     * @param valeur
     *            options techniques imposées (oui/non)
     */
    public void setOptionsTechniquesImposees(final String valeur) {
        this.optionsTechniquesImposees = valeur;
    }

    /**
     * @return description des options imposées
     */
    public String getDescriptionOptionsImposees() {
        return this.descriptionOptionsImposees;
    }

    /**
     * @param valeur
     *            description des options imposées
     */
    public void setDescriptionOptionsImposees(final String valeur) {
        this.descriptionOptionsImposees = valeur;
    }

    /**
     * @return décomposition en lots techniques (oui/non)
     */
    public String getDecompositionLotsTechniques() {
        return this.decompositionLotsTechniques;
    }

    /**
     * @param valeur
     *            décomposition en lots techniques (oui/non)
     */
    public void setDecompositionLotsTechniques(final String valeur) {
        this.decompositionLotsTechniques = valeur;
    }

    /**
     * @return ensemble des lots techniques
     */
    public Set<EpmTLotTechnique> getEpmTLotTechniques() {
        return this.epmTLotTechniques;
    }

    /**
     * @param valeur
     *            ensemble des lots techniques
     */
    public void setEpmTLotTechniques(final Set<EpmTLotTechnique> valeur) {
        this.epmTLotTechniques = valeur;
    }

    /**
     * @return ensemble des tranches
     */
    public Set<EpmTBudTranche> getEpmTBudTranches() {
        return this.epmTBudTranches;
    }

    /**
     * @param valeur
     *            ensemble des tranches
     */
    public void setEpmTBudTranches(final Set<EpmTBudTranche>  valeur) {
        this.epmTBudTranches = valeur;
    }

    /**
     * @return valeur du numérus clausus ou null si aucun
     */
    public Integer getNumerusClausus() {
        return numerusClausus;
    }

    /**
     * @param valeur
     *            valeur du numérus clausus ou null si aucun
     */
    public void setNumerusClausus(final Integer valeur) {
        this.numerusClausus = valeur;
    }


    /**
     * @return accord cadre dans le cas d'un marché subséquent, ou null
     * @deprecated
     */
    public EpmTBudLotOuConsultation getAccordCadre() {
        return accordCadre;
    }

    /**
     * @param valeur accord cadre dans le cas d'un marché subséquent, ou null
     * @deprecated
     */
    public void setAccordCadre(final EpmTBudLotOuConsultation valeur) {
        this.accordCadre = valeur;
    }

    /**
     * @return si accord cadre, ensemble des marchés subséquents qui en dépendent
     */
    public Set getMarchesSubsequents() {
        return marchesSubsequents;
    }

    /**
     * @param valeur si accord cadre, ensemble des marchés subséquents qui en dépendent
     */
    public void setMarchesSubsequents(final Set valeur) {
        this.marchesSubsequents = valeur;
    }


    /**
     * @return estimation bon de commande maximum au moment de pa preinscription
     */
    public Double getEstimationBcMaxPreinscription() {
        return estimationBcMaxPreinscription;
    }

    /**
     * @param valeur estimation bon de commande maximum au moment de pa preinscription
     */
    public void setEstimationBcMaxPreinscription(final Double valeur) {
        this.estimationBcMaxPreinscription = valeur;
    }

    /**
     * @return estimation bon de commande minimum au moment de pa preinscription
     */
    public Double getEstimationBcMinPreinscription() {
        return estimationBcMinPreinscription;
    }

    /**
     * @param valeur estimation bon de commande minimum au moment de pa preinscription
     */
    public void setEstimationBcMinPreinscription(final Double valeur) {
        this.estimationBcMinPreinscription = valeur;
    }

    /**
     * @return estimation prix fixe au moment de pa preinscription
     */
    public Double getEstimationPfPreinscription() {
        return estimationPfPreinscription;
    }

    /**
     * @param valeur estimation prix fixe au moment de pa preinscription
     */
    public void setEstimationPfPreinscription(final Double valeur) {
        this.estimationPfPreinscription = valeur;
    }

    /**
     * @return estimation bon de commande maximum
     */
    public Double getEstimationBcMax() {
        return estimationBcMax;
    }

    /**
     * @param valeur
     *            estimation bon de commande maximum
     */
    public void setEstimationBcMax(final Double valeur) {
        this.estimationBcMax = valeur;
    }

    /**
     * @return estimation bon de commande minimum
     */
    public Double getEstimationBcMin() {
        return estimationBcMin;
    }

    /**
     * @param valeur
     *            estimation bon de commande minimum
     */
    public void setEstimationBcMin(final Double valeur) {
        this.estimationBcMin = valeur;
    }

    /**
     * @return estimation prix fixe
     */
    public Double getEstimationPf() {
        return estimationPf;
    }

    /**
     * @param valeur
     *            estimation prix fixe
     */
    public void setEstimationPf(final Double valeur) {
        this.estimationPf = valeur;
    }

    /**
     * @return l'estimation du devis estimé de l'écran de tableau d'ouverture
     *         des offres.
     */
    public Double getEstimationDqeTabOuvOffre() {
        return estimationDqeTabOuvOffre;
    }

    /**
     * @param valeur
     *            l'estimation du devis estimé de l'écran de tableau d'ouverture
     *            des offres.
     */
    public void setEstimationDqeTabOuvOffre(final Double valeur) {
        this.estimationDqeTabOuvOffre = valeur;
    }

    /**
     * @return l'estimation de la partie forfaitaire de l'écran de tableau
     *         d'ouverture des offres.
     */
    public Double getEstimationPfTabOuvOffre() {
        return estimationPfTabOuvOffre;
    }

    /**
     * @param valeur
     *            l'estimation de la partie forfaitaire de l'écran de tableau
     *            d'ouverture des offres.
     */
    public void setEstimationPfTabOuvOffre(final Double valeur) {
        this.estimationPfTabOuvOffre = valeur;
    }

    /**
     * @return indique si une consultation à un ou plusieurs lots dissociés.
     *         Dans le cas d'un lot indique si le lot est dissocié.
     */
    public boolean isLotDissocie() {
        return lotDissocie;
    }

    /**
     * @param valeur indique si une consultation à un ou plusieurs lots dissociés.
     *               Dans le cas d'un lot indique si le lot est dissocié.
     */
    public void setLotDissocie(final boolean valeur) {
        this.lotDissocie = valeur;
    }

    /**
     * @return statut de l'écran ouverture des candidatures.
     */
    public int getStatutOuvertureCandidature() {
        return statutOuvertureCandidature;
    }

    /**
     * @param valeur statut de l'écran ouverture des candidatures.
     */
    public void setStatutOuvertureCandidature(final int valeur) {
        this.statutOuvertureCandidature = valeur;
    }

    /**
     * @return true si checkbox est coché au niveau de l'écran candididature écarté.
     */
    public boolean isClassementCandidatureEcarte() {
        return classementCandidatureEcarte;
    }

    /**
     * @param valeur true si checkbox est coché au niveau de l'écran candididature écarté.
     */
    public void setClassementCandidatureEcarte(final boolean valeur) {
        this.classementCandidatureEcarte = valeur;
    }

    /**
     * @return true si checkbox est coché au niveau de l'écran CandidatureEcartePA.
     */
    public boolean isClassementCandidatureEcartePA() {
        return classementCandidatureEcartePA;
    }

    /**
     * @param valeur true si checkbox est coché au niveau de l'écran CandidatureEcartePA.
     */
    public void setClassementCandidatureEcartePA(final boolean valeur) {
        this.classementCandidatureEcartePA = valeur;
    }

    /**
     * @return true si checkbox est coché au niveau de l'écran RecommandationCandidature.
     */
    public boolean isClassementRecommandationCandidature() {
        return classementRecommandationCandidature;
    }

    /**
     * @param valeur true si checkbox est coché au niveau de l'écran RecommandationCandidature.
     */
    public void setClassementRecommandationCandidature(final boolean valeur) {
        this.classementRecommandationCandidature = valeur;
    }

    /**
     * @return true si checkbox est coché au niveau de l'écran RecommandationCandidaturePA.
     */
    public boolean isClassementRecommandationCandidaturePA() {
        return classementRecommandationCandidaturePA;
    }

    /**
     * @param valeur true si checkbox est coché au niveau de l'écran RecommandationCandidaturePA.
     */
    public void setClassementRecommandationCandidaturePA(final boolean valeur) {
        this.classementRecommandationCandidaturePA = valeur;
    }

    /**
     * @return true si formulaire avis d'attribution
     */
    public boolean isAvisAttributionPresent() {
        return avisAttributionPresent;
    }

    /**
     * @param valeur
     *            true si formulaire avis d'attribution
     */
    public void setAvisAttributionPresent(final boolean valeur) {
        this.avisAttributionPresent = valeur;
    }

    /**
     * @return date de la preinscription
     */
    public Calendar getDateValeurPreinscription() {
        return dateValeurPreinscription;
    }

    /**
     * @param valeur
     *            date de la preinscription
     */
    public void setDateValeurPreinscription(final Calendar valeur) {
        this.dateValeurPreinscription = valeur;
    }

    /**
     * @return lieu d'exectution du marché (provenant de go)
     */
    public String getLieuExecution() {
        return lieuExecution;
    }

    /**
     * @param valeur
     *            lieu d'exectution du marché (provenant de go)
     */
    public void setLieuExecution(final String valeur) {
        this.lieuExecution = valeur;
    }

    /**
     * @return Montant estimé de la preinscription.
     */
    public Double getEstimationDqe() {
        return estimationDqe;
    }

    /**
     * @param valeur
     *            Montant estimé de la preinscription.
     */
    public void setEstimationDqe(final Double valeur) {
        this.estimationDqe = valeur;
    }

    /**
     * @return Montant estimé au moment du tableau de l'ouverture des offres.
     */
    public Double getEstimationCtTabOuvOffre() {
        return estimationCtTabOuvOffre;
    }

    /**
     * @param valeur
     *            Montant estimé au moment du tableau de l'ouverture des offres.
     */
    public void setEstimationCtTabOuvOffre(final Double valeur) {
        this.estimationCtTabOuvOffre = valeur;
    }

    /**
     * @return Montant estimé de la preinscription.
     */
    public Double getEstimationDqePresinscription() {
        return estimationDqePresinscription;
    }

    /**
     * @param valeur
     *            Montant estimé de la preinscription.
     */
    public void setEstimationDqePresinscription(final Double valeur) {
        this.estimationDqePresinscription = valeur;
    }

    /**
     * @return Montant estimé de la preinscription.
     */
    public Double getEstimationCtPresinscription() {
        return estimationCtPresinscription;
    }

    /**
     * @param valeur
     *            Montant estimé de la preinscription.
     */
    public void setEstimationCtPresinscription(final Double valeur) {
        this.estimationCtPresinscription = valeur;
    }

    /**
     * @return Date à laquelle GO transmet à EPM le marché GO (clic de
     *         l'utilisateur GO sur le bouton "Envoyer à EPM")
     */
    public Date getDateEnvoiMarcheGo() {
        return dateEnvoiMarcheGo;
    }

    /**
     * @param valeur
     *            Date à laquelle GO transmet à EPM le marché GO (clic de
     *            l'utilisateur GO sur le bouton "Envoyer à EPM")
     */
    public void setDateEnvoiMarcheGo(Date valeur) {
        this.dateEnvoiMarcheGo = valeur;
    }

    public Boolean getChoixSupportAttributionTousBeneficiaires() {
        return choixSupportAttributionTousBeneficiaires;
    }

    public void setChoixSupportAttributionTousBeneficiaires(
    		Boolean valeur) {
        this.choixSupportAttributionTousBeneficiaires = valeur;
    }
    
    /**
     * @return Liste des beneficiaires du marche.
     */
    public Set<EpmTRefBeneficiaireMarche> getBeneficiaireMarches() {
        return beneficiaireMarches;
    }

    /**
     * @param valeur Liste des beneficiaires du marche.
     */
    public void setBeneficiaireMarches(final Set<EpmTRefBeneficiaireMarche> valeur) {
        this.beneficiaireMarches = valeur;
    }
    
    /**
     * @return Unité des valeurs min et max
     */
    public EpmTRefUnite getUnite() {
        return unite;
    }

    /**
     * @param valeur Unité des valeurs min et max
     */
    public void setUnite(final EpmTRefUnite valeur) {
        this.unite = valeur;
    }

    /**
     * @return Indique si le marché est soumis à des clause environnementales.
     */
    public boolean getClausesEnvironnementales() {
        return clausesEnvironnementales;
    }

    /**
     * @param valeur Indique si le marché est soumis à des clause environnementales.
     */
    public void setClausesEnvironnementales(final boolean valeur) {
        this.clausesEnvironnementales = valeur;
    }

    public boolean getClausesSociales() {
        return clausesSociales;
    }

    public void setClausesSociales(final boolean valeur) {
        this.clausesSociales = valeur;
    }

    public Double getEstimationBcMinTabOuvOffre() {
        return estimationBcMinTabOuvOffre;
    }

    public void setEstimationBcMinTabOuvOffre(Double valeur) {
        this.estimationBcMinTabOuvOffre = valeur;
    }

    public Double getEstimationBcMaxTabOuvOffre() {
        return estimationBcMaxTabOuvOffre;
    }

    public void setEstimationBcMaxTabOuvOffre(Double valeur) {
        this.estimationBcMaxTabOuvOffre = valeur;
    }

    public Double getEstimationPfValidationClassementOffre() {
        return estimationPfValidationClassementOffre;
    }

    public void setEstimationPfValidationClassementOffre(final Double valeur) {
        this.estimationPfValidationClassementOffre = valeur;
    }

    public Double getEstimationDqeValidationClassementOffre() {
        return estimationDqeValidationClassementOffre;
    }

    public void setEstimationDqeValidationClassementOffre(final Double valeur) {
        this.estimationDqeValidationClassementOffre = valeur;
    }

    public Double getEstimationBcMinValidationClassementOffre() {
        return estimationBcMinValidationClassementOffre;
    }

    public void setEstimationBcMinValidationClassementOffre(
            final Double valeur) {
        this.estimationBcMinValidationClassementOffre = valeur;
    }

    public Double getEstimationBcMaxValidationClassementOffre() {
        return estimationBcMaxValidationClassementOffre;
    }

    public void setEstimationBcMaxValidationClassementOffre(
            final Double valeur) {
        this.estimationBcMaxValidationClassementOffre = valeur;
    }

    public Set<EpmTValeurConditionnementExterne> getEpmTValeurConditionnementExternes() {
        return epmTValeurConditionnementExternes;
    }

    public void setEpmTValeurConditionnementExternes(final Set<EpmTValeurConditionnementExterne> valeur) {
        this.epmTValeurConditionnementExternes = valeur;
    }

    /**
     * @return the epmTValeurConditionnementExternesComplexe
     */
    public Set<EpmTValeurConditionnementExterneComplexe> getEpmTValeurConditionnementExternesComplexe() {
        return epmTValeurConditionnementExternesComplexe;
    }

    /**
     * @param valeur the epmTValeurConditionnementExternesComplexe to set
     */
    public void setEpmTValeurConditionnementExternesComplexe(final Set<EpmTValeurConditionnementExterneComplexe> valeur) {
        this.epmTValeurConditionnementExternesComplexe = valeur;
    }

	/**
	 * @return the sousEspaceCollaboratif
	 */
	public  String getSousEspaceCollaboratifUrl() {
		return sousEspaceCollaboratifUrl;
	}

	/**
	 * @param valeur the sousEspaceCollaboratif to set
	 */
	public void setSousEspaceCollaboratifUrl(final String valeur) {
		this.sousEspaceCollaboratifUrl = valeur;
	}

    public Set<EpmTRefClausesEnvironnementales> getClausesEnvironnementalesChoixUtilisateur() {
        return clausesEnvironnementalesChoixUtilisateur;
    }

    public void setClausesEnvironnementalesChoixUtilisateur(final Set<EpmTRefClausesEnvironnementales> valeur) {
        this.clausesEnvironnementalesChoixUtilisateur = valeur;
    }

    public Set<EpmTRefClausesSociales> getClausesSocialesChoixUtilisateur() {
        return clausesSocialesChoixUtilisateur;
    }

    public void setClausesSocialesChoixUtilisateur(final Set<EpmTRefClausesSociales> valeur) {
        this.clausesSocialesChoixUtilisateur = valeur;
    }

    public boolean isClausesSocialesPresentCriteresAttribution() {
        return clausesSocialesPresentCriteresAttribution;
    }

    public void setClausesSocialesPresentCriteresAttribution(final boolean valeur) {
        this.clausesSocialesPresentCriteresAttribution = valeur;
    }

    public Set<EpmTRefTypeStructureSociale> getListeStructureSocialeReserves() {
        return listeStructureSocialeReserves;
    }

    public void setListeStructureSocialeReserves(Set<EpmTRefTypeStructureSociale> valeur) {
        this.listeStructureSocialeReserves = valeur;
    }

    public EpmTRefDroitProprieteIntellectuelle getDroitProprieteIntellectuelle() {
        return droitProprieteIntellectuelle;
    }

    public void setDroitProprieteIntellectuelle(final EpmTRefDroitProprieteIntellectuelle valeur) {
        this.droitProprieteIntellectuelle = valeur;
    }

    public void modifierEstimationsLc(Double estimationPfValidationClassementOffre, Double estimationPfPreinscription,
                                      Double estimationDqeValidationClassementOffre, Double estimationDqePresinscription,
                                      Double estimationBcMinValidationClassementOffre, Double estimationBcMinPreinscription,
                                      Double estimationBcMaxValidationClassementOffre, Double estimationBcMaxPreinscription) {
        this.setEstimationPfValidationClassementOffre(estimationPfValidationClassementOffre);
        this.setEstimationPfPreinscription(estimationPfPreinscription);
        this.setEstimationDqeValidationClassementOffre(estimationDqeValidationClassementOffre);
        this.setEstimationDqePresinscription(estimationDqePresinscription);
        this.setEstimationBcMinValidationClassementOffre(estimationBcMinValidationClassementOffre);
        this.setEstimationBcMinPreinscription(estimationBcMinPreinscription);
        this.setEstimationBcMaxValidationClassementOffre(estimationBcMaxValidationClassementOffre);
        this.setEstimationBcMaxPreinscription(estimationBcMaxPreinscription);
    }
}
