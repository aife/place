package fr.paris.epm.noyau.persistance.referentiel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * POJO EpmTRefCodeAchat de la table "epm__t_ref_code_achat"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCodeAchat extends BaseEpmTRefReferentiel {

    private static final Logger LOG = LoggerFactory.getLogger(EpmTRefCodeAchat.class);
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Montants
     */
    private Set<EpmTRefCodeAchatMontants> epmTRefCodeAchatMontantsSet = new HashSet<>();

    @Override
    public void setCodeExterne(String codeExterne) {
        // ajouter "code_externe" dans la base de données s'il faut l'avoir
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"code_externe\" dans la base de données s'il faut l'avoir");
    }

    public Set<EpmTRefCodeAchatMontants> getEpmTRefCodeAchatMontantsSet() {
        return epmTRefCodeAchatMontantsSet;
    }

    public void setEpmTRefCodeAchatMontantsSet(Set<EpmTRefCodeAchatMontants> epmTRefCodeAchatMontantsSet) {
        this.epmTRefCodeAchatMontantsSet = epmTRefCodeAchatMontantsSet;
    }

}
