package fr.paris.epm.noyau.metier.jms;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Mounthei Kham
 * @version $Revision$, $Date$, $Author$
 */
public class MessageJMS implements Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * variable static de type de message.
     */
    public static final int COURRIER = 0;

    /**
     * type de message.
     */
    private int typeMessage;

    /**
     * Dans le cas d'un envoi de courrier, cette collection contient un
     * ensemble de EpmTMail.
     */
    private Collection contenuMessage;

    /**
     * @return le contenu du message.
     */
    public final Collection getContenuMessage() {
        return contenuMessage;
    }

    /**
     * @param valeur la valeur à assigner au contenu du message.
     */
    public final void setContenuMessage(final Collection valeur) {
        this.contenuMessage = valeur;
    }

    /**
     * @return le type de message.
     */
    public final int getTypeMessage() {
        return typeMessage;
    }

    /**
     * @param valeur le type de message à assigner.
     */
    public final void setTypeMessage(final int valeur) {
        this.typeMessage = valeur;
    }
}
