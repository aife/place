package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Persistance des associations clause/criteres potentiellement conditionnes.
 */
@Entity
@Table(name = "epm__t_clause_has_potentiellement_conditionnee", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTClausePotentiellementConditionnee extends EpmTClausePotentiellementConditionneeAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(targetEntity = EpmTClause.class)
    @JoinColumn(name = "id_clause", nullable = false)
    private EpmTClause epmTClause;

    @OneToMany(targetEntity = EpmTClauseValeurPotentiellementConditionnee.class,
            mappedBy = "epmTClausePotentiellementConditionnee", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<EpmTClauseValeurPotentiellementConditionnee> epmTClauseValeurPotentiellementConditionnees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdClausePotentiellementConditionnee() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    @Override
    public EpmTClause getEpmTClause() {
        return epmTClause;
    }

    @Override
    public void setEpmTClause(EpmTClauseAbstract epmTClause) {
        this.epmTClause = (EpmTClause) epmTClause;
    }

    @Override
    public Set<EpmTClauseValeurPotentiellementConditionnee> getEpmTClauseValeurPotentiellementConditionnees() {
        return epmTClauseValeurPotentiellementConditionnees;
    }

    @Override
    public void setEpmTClauseValeurPotentiellementConditionnees(Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotentiellementConditionnees) {
        this.epmTClauseValeurPotentiellementConditionnees = (Set<EpmTClauseValeurPotentiellementConditionnee>) epmTClauseValeurPotentiellementConditionnees;
    }

    @Override
    public EpmTClausePotentiellementConditionnee clone() throws CloneNotSupportedException {
        EpmTClausePotentiellementConditionnee cpc = (EpmTClausePotentiellementConditionnee) super.clone();
        cpc.epmTClause = null;

        cpc.epmTClauseValeurPotentiellementConditionnees = new HashSet<>();
        for (EpmTClauseValeurPotentiellementConditionnee cvpc : getEpmTClauseValeurPotentiellementConditionnees()) {
            EpmTClauseValeurPotentiellementConditionnee cvpcClone = cvpc.clone();
            cvpcClone.setEpmTClausePotentiellementConditionnee(cpc);
            cpc.epmTClauseValeurPotentiellementConditionnees.add(cvpcClone);
        }
        return cpc;
    }

}
