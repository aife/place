package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Referention des types de page de garde
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_type_page_de_garde", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefTypePageDeGarde extends BaseEpmTRefRedaction {

    /**
     * nom du template applicable au type de page de garde
     */
    @Column(nullable=false)
    private String template;
    
    /**
     * extension applicable au type de page de garde
     */
    @Column(nullable=false)
    private String extension;

	/**
	 * type
	 */
	@Column(nullable=false)
	private String type;

	/**
	 * Ordre
	 */
	@Column(nullable=true)
	private Integer ordre;

    /**
     * @return the template
     */
    public String getTemplate() {
        return template;
    }

    /**
     * @param valeur the template to set
     */
    public void setTemplate(final String valeur) {
        this.template = valeur;
    }

    /**
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * @param valeur the extension to set
     */
    public void setExtension(final String valeur) {
        this.extension = valeur;
    }

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public Integer getOrdre() {
		return ordre;
	}

	public void setOrdre( Integer ordre ) {
		this.ordre = ordre;
	}
}
