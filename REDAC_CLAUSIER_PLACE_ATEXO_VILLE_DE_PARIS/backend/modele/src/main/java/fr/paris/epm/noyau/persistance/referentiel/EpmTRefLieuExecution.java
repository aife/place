package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefLieuExecution de la table "epm__t_ref_lieu_execution"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefLieuExecution extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

}