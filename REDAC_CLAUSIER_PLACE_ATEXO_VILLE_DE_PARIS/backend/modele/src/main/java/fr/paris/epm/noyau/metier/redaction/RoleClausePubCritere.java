package fr.paris.epm.noyau.metier.redaction;

public class RoleClausePubCritere extends RoleClauseCritere {

    private Integer idPublication;

    @Override
    public StringBuffer corpsRequete() {
        // assemblage de la requête
        StringBuffer sb = super.corpsRequete();

        if (idPublication != null) {
            sb.append(" AND roleClause.idPublication = ");
            sb.append(idPublication);
        }

        return sb;
    }

    @Override
    protected String getTableDeRecherche() {
        return " EpmTRoleClausePub ";
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

}