package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;

/**
 * Pojo des montants des codes achat
 * Created by nty on 09/05/17.
 * @author Tyurin Nikolay
 * @author nty
 */
public class EpmTRefCodeAchatMontants extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant
     */
    private int id;

    /**
     * Identifiant du code achat en base
     */
    private int idCodeAchat;

	/**
     * Année
     */
    private int year;
    
    /**
     * Moyenne des montants reéls
     */
    private Double montantMoyen;
    
    /**
     * Montant reél
     */
    private Double montantReel;

    /**
     * Montant recensé
     */
    private Double montantRecense;

    /**
     * Cette méthode affiche le contenu de l'objet.
     * @return String
     */
    public final String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nIdentifiant: ");
        res.append(id);
        res.append("\nIdCodeAchat: ");
        res.append(idCodeAchat);
        res.append("\nAnnée: ");
        res.append(year);
        return res.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCodeAchat() {
        return idCodeAchat;
    }

    public void setIdCodeAchat(int idCodeAchat) {
        this.idCodeAchat = idCodeAchat;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Double getMontantMoyen() {
        return montantMoyen;
    }

    public void setMontantMoyen(Double montantMoyen) {
        this.montantMoyen = montantMoyen;
    }

    public Double getMontantReel() {
        return montantReel;
    }

    public void setMontantReel(Double montantReel) {
        this.montantReel = montantReel;
    }

    public Double getMontantRecense() {
        return montantRecense;
    }

    public void setMontantRecense(Double montantRecense) {
        this.montantRecense = montantRecense;
    }

}