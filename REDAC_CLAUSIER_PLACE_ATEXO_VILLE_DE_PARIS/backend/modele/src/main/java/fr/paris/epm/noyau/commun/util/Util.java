/**
 * $Id$
 */
package fr.paris.epm.noyau.commun.util;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;


/**
 * Classe utilitaire.
 *
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public final class Util {

    public static final String STYLE = "style";

    /**
     * Constructeur caché.
     */
    private Util() {
    }


    /**
     * décode les caractères spéciaux.
     *
     * @param chaine la chaine à encoder
     * @return la chaine encodée
     */
    public static String decodeCaractere(final String chaine) {
        String retour = sanitizeHtmlInput(chaine);
        if (retour != null) {
            retour = retour.replaceAll("<ul>(<br[^<]*>)*<li>", "<ul><li>");
            retour = retour.replaceAll("</li>(<br[^<]*>)*<li>", "</li><li>");
            retour = retour.replaceAll("</li>(<br[^<]*>)*</ul>", "</li></ul>");
            retour = retour.replace(Character.toString((char) 10), "");
            retour = retour.replace((char) 8217, '\'');
            retour = StringEscapeUtils.unescapeHtml(retour);
        }
        return retour;
    }


    public static String sanitizeHtmlInput(String untrustedHTML) {
        if (StringUtils.isBlank(untrustedHTML)) return untrustedHTML;
        PolicyFactory policy = new HtmlPolicyBuilder().allowElements("table", "tbody", "tr", "td", "a", "br", "div", "img", "ul", "li", "p", "span", "u", "em", "strong", "b", "i")
                .allowAttributes("href", STYLE).onElements("a")
                .allowAttributes(STYLE).onElements("div")
                .allowAttributes("width", "src", "height").onElements("img")
                .allowAttributes(STYLE).onElements("li")
                .allowAttributes(STYLE).onElements("p")
                .allowAttributes(STYLE).onElements("span")
                .allowAttributes(STYLE).onElements("u")
                .allowAttributes(STYLE).onElements("em")
                .allowAttributes(STYLE).onElements("strong")
                .allowAttributes(STYLE).onElements("b")
                .allowAttributes("class", "border", "cellpadding", "cellspacing").onElements("table")
                .allowAttributes("width", STYLE).onElements("td")
                .allowAttributes("class", STYLE).onElements("tr")
                .allowUrlProtocols("https", "http")
                .toFactory();
        return StringEscapeUtils.unescapeHtml(policy.sanitize(untrustedHTML));
    }

}
