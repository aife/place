package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefActionConfirmationAttribution de la table "epm__t_ref_action_confirmation_attribution"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefActionConfirmationAttribution extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final int ATTRIBUTION_CONFIRME = 1; // Valeur de la base
    public static final int CHANGEMENT_ATTRIBUTAIRE_DEMANDE = 2; // Valeur de la base

}