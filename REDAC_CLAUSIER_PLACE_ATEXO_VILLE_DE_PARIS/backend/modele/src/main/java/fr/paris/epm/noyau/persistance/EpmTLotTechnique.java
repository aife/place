package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Pojo hibernate Lot Technique associé à une consultation.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTLotTechnique extends EpmTAbstractObject implements Comparable<EpmTLotTechnique>,
        Cloneable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * numéro du lot.
     */
    private String numeroLot;

    /**
     * intitulé du lot.
     */
    private String intituleLot;

    /**
     * identifie le lot technique principal.
     */
    private String principal;

    /**
     * Ensemble des tranches associées à ce lot technique.
     */
    private Set<EpmTBudTranche> epmTBudTranches = new HashSet(0);

    // Méthodes
    /**
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (numeroLot != null)
            result += Constantes.PREMIER * result + numeroLot.hashCode();
        if (intituleLot != null)
            result += Constantes.PREMIER * result + intituleLot.hashCode();
        if (principal != null)
            result += Constantes.PREMIER * result + principal.hashCode();
        result += Constantes.PREMIER * result + id;
        return result;
    }

    /**
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EpmTLotTechnique autre = (EpmTLotTechnique) obj;
        if (intituleLot == null) {
            if (autre.intituleLot != null) {
                return false;
            }
        } else if (!intituleLot.equals(autre.intituleLot)) {
            return false;
        }
        if (epmTBudTranches == null) {
            if (autre.epmTBudTranches != null) {
                return false;
            } // FIXME
        } else if (!(new HashSet(epmTBudTranches)).equals(new HashSet(autre.epmTBudTranches))) {
            return false;
        }

        if (intituleLot == null) {
            if (autre.intituleLot != null) {
                return false;
            }
        } else if (!intituleLot.equals(autre.intituleLot)) {
            return false;
        }
        if (principal == null) {
            if (autre.principal != null) {
                return false;
            }
        } else if (!principal.equals(autre.principal)) {
            return false;
        }
        if (id != autre.id) {
            return false;
        }
        if (numeroLot != null && autre.numeroLot != null
                && !numeroLot.equals(autre.numeroLot)) {
            return false;
        }
        return true;
    }

    /**
     * @param object objet à comparer
     * @return résultat
     */
    public final int compareTo(final EpmTLotTechnique lotTechnique) {
        return numeroLot.compareTo(lotTechnique.getNumeroLot());
    }

    public Object clone(Collection newTranches) throws CloneNotSupportedException {
        EpmTLotTechnique lotTechnique = new EpmTLotTechnique();
        lotTechnique.setNumeroLot(numeroLot);
        lotTechnique.setIntituleLot(intituleLot);
        lotTechnique.setPrincipal(principal);
        Iterator it = epmTBudTranches.iterator();
        Set tranches = new HashSet();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            tranches.add(getTrancheByTranche(tranche, newTranches));
        }
        lotTechnique.setEpmTBudTranches(tranches);
        return lotTechnique;
    }

    private EpmTBudTranche getTrancheByTranche(EpmTBudTranche oldTranche,
            Collection newTranches) {
        for (Iterator iterator = newTranches.iterator(); iterator.hasNext();) {
            EpmTBudTranche newTranche = (EpmTBudTranche) iterator.next();
            if (newTranche.getIntituleTranche() != null
                    && oldTranche.getIntituleTranche() != null
                    && newTranche.getIntituleTranche().equals(oldTranche.getIntituleTranche())) {
                return newTranche;
            }
        }
        return null;

    }

    // Accesseurs
    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return this.id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return numéro de lot
     */
    public String getNumeroLot() {
        return this.numeroLot;
    }

    /**
     * @param valeur numéro de lot
     */
    public void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }

    /**
     * @return intitulé du lot
     */
    public String getIntituleLot() {
        return this.intituleLot;
    }

    /**
     * @param valeur intitulé du lot
     */
    public void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return identifie le lot technique principal
     */
    public String getPrincipal() {
        return this.principal;
    }

    /**
     * @param valeur identifie le lot technique principal
     */
    public void setPrincipal(final String valeur) {
        this.principal = valeur;
    }

    /**
     * @return Set d'élément {@link EpmTBudTranche}
     */
    public Set<EpmTBudTranche> getEpmTBudTranches() {
        return epmTBudTranches;
    }

    /**
     * @param valeur Set d'élément {@link EpmTBudTranche}
     */
    public void setEpmTBudTranches(final Set<EpmTBudTranche> valeur) {
        this.epmTBudTranches = valeur;
    }
}
