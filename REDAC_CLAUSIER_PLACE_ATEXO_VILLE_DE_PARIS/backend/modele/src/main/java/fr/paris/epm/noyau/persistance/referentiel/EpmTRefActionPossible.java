package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefActionPossible de la table "epm__t_ref_action_possible"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefActionPossible extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ACT_INDIQUER_OUVERTE = 1; // Action, indiquer que l'enveloppe est ouverte.
    public static final int ACT_INDIQUER_HORS_DELAI = 2; // Action, indiquer que l'enveloppe est hors délai.
    public static final int ACT_OUVRIR = 3; // Action, ouvrir l'enveloppe électronique.
    public static final int ACT_REJETER_HORS_DELAI = 4; // Action, rejeter l'enveloppe électronique car hors délai.
    public static final int ACT_IDENTIFIER_LOTS = 5; // Action, identifier les lots présents dans l'enveloppe.
    public static final int ACT_IDENTIFIER_LOTS_COMPL = 6; // Action, identifier les informations complémentaires pour les lots présents dans l'enveloppe.
    public static final int ACT_VERIFIER_SIGNATURES = 7; // Action, vérification des signatures.
    public static final int ACT_TELECHARGER = 8; // Action, télécharger l'enveloppe chiffrée.
    public static final int ACT_INDIQUER_COMPLETE = 9; // Action, indiquer que l'enveloppe est complète.
    public static final int ACT_INDIQUER_A_COMPLETER = 10; // Action, indiquer que l'enveloppe est complète.
    public static final int ACT_COMPLETE_A_OUVERTURE = 11; // Action, indiquer que l'enveloppe doit être complétée.
    public static final int ACT_INCOMPLETE_A_OUVERTURE_A_COMPLETER = 12; // Action, indiquer que l'enveloppe doit être complétée.
    public static final int ACT_LOT_NON_PRESENTE = 13; // Action, indiquer que le lot n'a pas été présenté (lot papier).

}