package fr.paris.epm.noyau.service.technique;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Service
public class LocalRestService implements RestService {

	private final Logger LOG = LoggerFactory.getLogger(LocalRestService.class);

	@Override
	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}

	private RestTemplate restTemplate;

	@PostConstruct
	private void build() {
		LOG.debug("Construction du template REST");

		restTemplate = new RestTemplate(creerHttpClientSansSecurite());
	}

	private static ClientHttpRequestFactory creerHttpClientSansSecurite() {
		// disable all security
		return new HttpComponentsClientHttpRequestFactory(HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build());
	}
}
