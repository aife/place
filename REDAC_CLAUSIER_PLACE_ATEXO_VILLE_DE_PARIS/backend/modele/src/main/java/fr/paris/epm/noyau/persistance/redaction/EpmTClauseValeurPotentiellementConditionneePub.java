package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_clause_has_valeur_potentiellement_conditionnee_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_clause_valeur_potentiellement_conditionnee", "id_publication"}))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTClauseValeurPotentiellementConditionneePub extends EpmTClauseValeurPotentiellementConditionneeAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_clause_valeur_potentiellement_conditionnee", nullable = false)
    private Integer idClauseValeurPotentiellementConditionnee;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;
    
    @Column(name = "id_clause_potentiellement_conditionnee")
    private Integer idClausePotentiellementConditionnee;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "id_clause_potentiellement_conditionnee", referencedColumnName = "id_clause_potentiellement_conditionnee", insertable = false, updatable = false),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication", insertable = false, updatable = false)
    })
    private EpmTClausePotentiellementConditionneePub epmTClausePotentiellementConditionnee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdClauseValeurPotentiellementConditionnee() {
        return idClauseValeurPotentiellementConditionnee;
    }

    public void setIdClauseValeurPotentiellementConditionnee(Integer idClauseValeurPotentiellementConditionnee) {
        this.idClauseValeurPotentiellementConditionnee = idClauseValeurPotentiellementConditionnee;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdClausePotentiellementConditionnee() {
        return idClausePotentiellementConditionnee;
    }

    public void setIdClausePotentiellementConditionnee(Integer idClausePotentiellementConditionnee) {
        this.idClausePotentiellementConditionnee = idClausePotentiellementConditionnee;
    }

    @Override
    public EpmTClausePotentiellementConditionneePub getEpmTClausePotentiellementConditionnee() {
        return epmTClausePotentiellementConditionnee;
    }

    @Override
    public void setEpmTClausePotentiellementConditionnee(EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee) {
        this.epmTClausePotentiellementConditionnee = (EpmTClausePotentiellementConditionneePub) epmTClausePotentiellementConditionnee;
    }

    @Override
    protected EpmTClauseValeurPotentiellementConditionneePub clone() throws CloneNotSupportedException {
        EpmTClauseValeurPotentiellementConditionneePub cvpc = (EpmTClauseValeurPotentiellementConditionneePub) super.clone();
        cvpc.idClauseValeurPotentiellementConditionnee = idClauseValeurPotentiellementConditionnee;
        cvpc.idPublication = idPublication;
        cvpc.epmTClausePotentiellementConditionnee = null;
        return cvpc;
    }

}
