/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;



/**
 * Stocke un biclef et le nombre d'instances de commission associées à lui.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class BiclefCompte extends EpmTAbstractObject {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Biclef.
     */
    private EpmTBiclef biclef;

    /**
     * Compte.
     */
    private int compte;

    // Accesseurs
    /**
     * @return biclef
     */
    public final EpmTBiclef getBiclef() {
        return biclef;
    }
    /**
     * @param valeur biclef
     */
    public final void setBiclef(final EpmTBiclef valeur) {
        this.biclef = valeur;
    }
    /**
     * @return compte
     */
    public final int getCompte() {
        return compte;
    }
    /**
     * @param valeur compte
     */
    public final void setCompte(final int valeur) {
        this.compte = valeur;
    }

}
