/**
 * $Id: ApplicationNoyauException.java 146 2007-07-30 17:00:07Z beraudo $
 */
package fr.paris.epm.noyau.commun.exception;

/**
 * @author Guillaume Béraudo
 * @version $Revision: 146 $,
 *          $Date: 2007-07-30 19:00:07 +0200 (lun., 30 juil. 2007) $,
 *          $Author: beraudo $
 */
public class ApplicationNoyauException extends RuntimeException {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Exception d'ordre fonctionnel du noyau.
     */
    public ApplicationNoyauException() {
        super();
    }

    /**
     * Exception d'ordre fonctionnel du noyau.
     * @param msg Message d'erreur
     * @param cause Exception ayant provoqué cette exception noyau
     */
    public ApplicationNoyauException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * Exception d'ordre fonctionnel du noyau.
     * @param msg Message d'erreur
     */
    public ApplicationNoyauException(final String msg) {
        super(msg);
    }

    /**
     * Exception d'ordre fonctionnel du noyau.
     * @param cause Exception ayant provoqué cette exception noyau
     */
    public ApplicationNoyauException(final Throwable cause) {
        super(cause);
    }
}
