package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefChampAdditionnel de la table "epm__t_ref_champ_additionnel"
 * Created by nty on 23/06/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefChampAdditionnel extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private int idForm; // 1 -> DemandeAchat, 2 -> ?Consultation?

    private int idTypeField;

    private String comment;

    private boolean required;

    private String defaultValue;

    private EpmTRefNomenclature epmTRefNomenclature;

    private EpmTRefNomenclatureValeur defaultEpmTRefNomenclatureValeur;

    public int getIdForm() {
        return idForm;
    }

    public void setIdForm(int idForm) {
        this.idForm = idForm;
    }

    public int getIdTypeField() {
        return idTypeField;
    }

    public void setIdTypeField(int idTypeField) {
        this.idTypeField = idTypeField;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public EpmTRefNomenclature getEpmTRefNomenclature() {
        return epmTRefNomenclature;
    }

    public void setEpmTRefNomenclature(EpmTRefNomenclature epmTRefNomenclature) {
        this.epmTRefNomenclature = epmTRefNomenclature;
    }

    public EpmTRefNomenclatureValeur getDefaultEpmTRefNomenclatureValeur() {
        return defaultEpmTRefNomenclatureValeur;
    }

    public void setDefaultEpmTRefNomenclatureValeur(EpmTRefNomenclatureValeur defaultEpmTRefNomenclatureValeur) {
        this.defaultEpmTRefNomenclatureValeur = defaultEpmTRefNomenclatureValeur;
    }

}
