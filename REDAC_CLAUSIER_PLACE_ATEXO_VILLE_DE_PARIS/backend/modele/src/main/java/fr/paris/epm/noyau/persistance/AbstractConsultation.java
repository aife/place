package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.*;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import java.util.Set;

public class AbstractConsultation extends EpmTAbstractObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 5554142632811214875L;


    /**
     * Numéro de consultation.
     */
    protected String numeroConsultation;

    /**
     * Justification du non allotissement
     */
    protected String justificationNonAllotissement;

    /**
     * Référentiel Pouvoir Adjudicateur.
     */
    protected EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur;

    /**
     * Intitulé de consultation.
     */
    @Field(index = Index.TOKENIZED, store = Store.YES)
    protected String intituleConsultation;

    /**
     * Référentiel Direction et service (bénéficiaire/vision).
     */
    protected int dirServiceVision;

    /**
     * Objet de la consultation.
     */
    @Field(index = Index.TOKENIZED, store = Store.YES)
    protected String objet;

    /**
     * Référentiel Procédure.
     */
    protected EpmTRefProcedure epmTRefProcedure;

    /**
     * Référentiel Nature.
     */
    protected EpmTRefNature epmTRefNature;

    /**
     * Référentiel Direction et service (intervention).
     */
    protected EpmTRefDirectionService epmTRefDirectionService;

    /**
     * "oui" si transverse, "non" sinon.
     */
    protected String transverse;

    /**
     * Utilisateur ayant créé la consultation.
     */
    protected EpmTUtilisateur epmTUtilisateur;

    /**
     * true si la consultation a un de ses lots dissociés.
     */
    protected boolean lotDissocie;

    /**
     * "oui" si alloti.
     */
    protected String allotissement;

    /**
     * L'identifiant de l'organisme auquel appartient la consultation
     */
    protected Integer idOrganisme;

    /**
     * Si true, la consultation utilise le Webservice Demat (nouveau connecteur) pour publier DCE,
     * envoyer des annonces, récupérer les registres du dépôt/retrait/question. Si false, la
     * consultation utilise l'ancien connecteur (basé sur jms).
     */
    protected boolean echangeWebserviceDemat;

    /**
     * Liste des lieux d'éxecution associés à la consultation.
     */
    protected Set<EpmTRefLieuExecution> lieuxExecution;

    /**
     * True aura pour effet de faire de cette consultation un marché public simplifié (MPS) donnant
     * ainsi la possibilité aux entreprises de saisir leur candidature en ligne simplement à partir
     * de leur SIRET
     */
    protected Boolean marchePublicSimplifie;

    /**
     * @return "oui" si alloti.
     */
    public String getAllotissement() {
        return allotissement;
    }

    /**
     * @param valeur "oui" si alloti
     */
    public void setAllotissement(final String valeur) {
        this.allotissement = valeur;
    }

    /**
     * @return référentiel Direction et service (vision)
     */
    public EpmTRefDirectionService getEpmTRefDirectionService() {
        return epmTRefDirectionService;
    }

    /**
     * @param valeur référentiel Direction et service (vision)
     */
    public void setEpmTRefDirectionService(final EpmTRefDirectionService valeur) {
        this.epmTRefDirectionService = valeur;
    }

    /**
     * @return référentiel nature
     */
    public EpmTRefNature getEpmTRefNature() {
        return epmTRefNature;
    }

    /**
     * @param valeur référentiel nature
     */
    public void setEpmTRefNature(final EpmTRefNature valeur) {
        this.epmTRefNature = valeur;
    }

    /**
     * @return référentiel pouvoir adjudicateur
     */
    public EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur() {
        return epmTRefPouvoirAdjudicateur;
    }

    /**
     * @param valeur référentiel pouvoir adjudicateur
     */
    public void setEpmTRefPouvoirAdjudicateur(final EpmTRefPouvoirAdjudicateur valeur) {
        this.epmTRefPouvoirAdjudicateur = valeur;
    }

    /**
     * @return référentiel procédure
     */
    public EpmTRefProcedure getEpmTRefProcedure() {
        return epmTRefProcedure;
    }

    /**
     * @param valeur référentiel procédure
     */
    public void setEpmTRefProcedure(final EpmTRefProcedure valeur) {
        this.epmTRefProcedure = valeur;
    }

    /**
     * @return utilisateur ayant créé cette consultation
     */
    public EpmTUtilisateur getEpmTUtilisateur() {
        return epmTUtilisateur;
    }

    /**
     * @param valeur utilisateur ayant créé cette consultation
     */
    public void setEpmTUtilisateur(final EpmTUtilisateur valeur) {
        this.epmTUtilisateur = valeur;
    }

    /**
     * @return intitulé de la consultation
     */
    public String getIntituleConsultation() {
        return intituleConsultation;
    }

    /**
     * @param valeur intitulé de la consultation
     */
    public void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }

    /**
     * @return numéro de la consultation
     */
    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    /**
     * @param valeur numéro de la consultation
     */
    public void setNumeroConsultation(final String valeur) {
        this.numeroConsultation = valeur;
    }

    /**
     * @return objet de la consultation
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param valeur objet de la consultation
     */
    public void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return "oui" si transverse
     */
    public String getTransverse() {
        return transverse;
    }

    /**
     * @param valeur "oui" si transverse
     */
    public void setTransverse(final String valeur) {
        this.transverse = valeur;
    }

    /**
     * @return true si un des lots de la consultation est dissocié.
     */
    public boolean isLotDissocie() {
        return lotDissocie;
    }

    /**
     * @param lotDissocie si un des lots de la consultation est dissocié.
     */
    public void setLotDissocie(boolean lotDissocie) {
        this.lotDissocie = lotDissocie;
    }

    /**
     * @return référentiel Direction et service (bénéficiaire)
     */
    public int getDirServiceVision() {
        return dirServiceVision;
    }

    /**
     * @param valeur référentiel Direction et service (bénéficiaire)
     */
    public void setDirServiceVision(int valeur) {
        this.dirServiceVision = valeur;
    }

    /**
     * @return the idOrganisme
     */
    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public void setIdOrganisme(final Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    /**
     * Si true, la consultation utilise le Webservice Demat (nouveau connecteur) pour publier DCE,
     * envoyer des annonces, récupérer les registres du dépôt/retrait/question. Si false, la
     * consultation utilise l'ancien connecteur.
     */
    public boolean isEchangeWebserviceDemat() {
        return echangeWebserviceDemat;
    }

    public void setEchangeWebserviceDemat(final boolean valeur) {
        this.echangeWebserviceDemat = valeur;
    }

    public Set<EpmTRefLieuExecution> getLieuxExecution() {
        return lieuxExecution;
    }

    public void setLieuxExecution(final Set<EpmTRefLieuExecution> valeur) {
        this.lieuxExecution = valeur;
    }

    public Boolean getMarchePublicSimplifie() {
        return marchePublicSimplifie;
    }

    public void setMarchePublicSimplifie(final Boolean valeur) {
        this.marchePublicSimplifie = valeur;
    }

    public String getJustificationNonAllotissement() {
        return justificationNonAllotissement;
    }

    public void setJustificationNonAllotissement(String valeur) {
        this.justificationNonAllotissement = valeur;
    }

}
