package fr.paris.epm.noyau.persistance;


/**
 * POJO hibernate gérant les différentes phases de la consultation.
 * @author Mounthei Kham
 * @version $Revision:$, $Date: $, $Author: $
 */
public class EpmTPhase extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * libelle de la phase.
     */
    private String libelle;

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return libellé de la phase
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé de la phase
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

}
