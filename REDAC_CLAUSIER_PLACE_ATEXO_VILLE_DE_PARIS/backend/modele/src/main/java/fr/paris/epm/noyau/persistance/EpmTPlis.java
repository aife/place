package fr.paris.epm.noyau.persistance;


/**
 * Tous les plis sauf actes engagements.
 * @author Régis Menet
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTPlis extends EpmTPlisGenerique {

    private static final long serialVersionUID = -2361723850238962873L;
}
