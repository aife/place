package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour les sous domaines
 * attribution.contrat.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineTrancheAttribution implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    private String details;

    private Double montantPfHt;

    private Double montantPfTtc;

    private Double montantPuHt;

    private Double montantPuTtc;

    public final String getDetails() {
        return details;
    }

    public final void setDetails(final String valeur) {
        this.details = valeur;
    }

    public final Double getMontantPfHt() {
        return montantPfHt;
    }

    public final void setMontantPfHt(final Double valeur) {
        this.montantPfHt = valeur;
    }

    public final Double getMontantPfTtc() {
        return montantPfTtc;
    }

    public final void setMontantPfTtc(final Double valeur) {
        this.montantPfTtc = valeur;
    }

    public final Double getMontantPuHt() {
        return montantPuHt;
    }

    public final void setMontantPuHt(final Double valeur) {
        this.montantPuHt = valeur;
    }

    public final Double getMontantPuTtc() {
        return montantPuTtc;
    }

    public final void setMontantPuTtc(final Double valeur) {
        this.montantPuTtc = valeur;
    }
}
