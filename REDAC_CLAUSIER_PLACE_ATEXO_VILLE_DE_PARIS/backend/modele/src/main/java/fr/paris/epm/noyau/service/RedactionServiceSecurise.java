package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.metier.redaction.TemplateVersionCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;

import java.util.List;

/**
 * Interface sécurisée du service d e redaction. Ce service est utilisé pour
 * accéder aux méthodes CRUD (création, chargement, modification, suppression)
 * de Profil et Utilisateur.
 */
public interface RedactionServiceSecurise extends GeneriqueServiceSecurise {

    /**
     * Recherche l'intitulé de la dernière version publié
     *
     * @param idLastversion
     * @return
     */
    String chercherLastVersion(Integer idLastversion);

    /**
     * Permet de retrouver la liste des canevas pour une clause
     *
     * @param referenceClause la reference de la clause
     * @param organisme       identifiant de l'organisme, null si pas de filtre
     * @return la liste des canevas
     */
    List<EpmTCanevas> chercherListeCanevasPourClauseMinisterielle(final String referenceClause, final Integer organisme);

    /**
     * Permet de retrouver la liste des canevas pour une clause
     *
     * @param referenceClause la reference de la clause
     * @return la liste des canevas
     */
    List<EpmTCanevas> chercherListeCanevasPourClauseInterministerielle(final String referenceClause);

    /**
     * Permet de retrouver la liste des clauses pour un canevas ministeriel
     *
     * @param referenceCanevas la reference du canevas
     * @param organisme        identifiant de l'organisme, null si pas de filtre
     * @return la liste des clauses
     */
    List<EpmTClause> chercherListeClausesPourCanevasMinisterielle(final String referenceCanevas, final Integer organisme);

    /**
     * Permet de retrouver la liste des clauses pour un canevas interministeriel
     *
     * @param referenceCanevas la reference du canevas
     * @return la liste des clauses
     */
    List<EpmTClause> chercherListeClausesPourCanevasInterministerielle(final String referenceCanevas);

    /**
     * chercher Derniere Reference d'un canevas
     */
    List<String> chercherDerniereReferenceCanevas(Integer idOrganisme, boolean canevasEditeur);

    List<String> chercherDerniereReferenceClause(Integer idOrganisme, boolean clauseEditeur);

    EpmTClauseAbstract chercherClauseMemeReferenceGrandIdentifiant(Integer idOrganisme, String reference, Integer idPublication,
                                                                   Boolean isFormeEditeur, boolean bricolage);

    /**
     * @param idPublication id publication à vérifier
     * @return true si cette publication à été déjà activée au moins une fois
     */
    boolean isAlreadyActivated(int idPublication);

    boolean canActivate(Integer idOrganisme);

    void desactivationAllPublication();
    void desactivationAllPublicationEnCours();

    void publicationClausierUpdateClause(int idPublication);

    void publicationClausierUpdateClause(int idPublication, List<Integer> idClauses);

    void publicationClausierUpdateClausePub(int idPublication);

    void updateClausePub(int id, int idClause);

    void updateRoleClausePub(int id, int idClause, int idRoleClause);

    void publicationClausierUpdateRoleClausePub(int idPublication);

    void publicationClausierUpdateClauseHasTypeContratPub(int idPublication);

    void publicationClausierUpdateClauseHasPotentiellementConditionneePub(int idPublication);

    void publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(int idPublication);

    void publicationClausierUpdateCanevas(int idPublication);

    void publicationClausierUpdateCanevas(int idPublication, List<Integer> idCanevas);

    void publicationClausierUpdateCanevasPub(int idPublication);

    void publicationClausierUpdateCanevasHasProcedurePub(int idPublication);

    void publicationClausierUpdateCanevasHasTypeContratPub(int idPublication);

    void publicationClausierUpdateChapitrePub(int idPublication);

    void publicationClausierUpdateChapitreHasClausePub(int idPublication);

    void rollbackPublicationClausier(int idPublication);

    @Deprecated
    public List<EpmTPublicationClausier> chercherPublicationsParClause(final int idClause);

    public String getReferenceDocumentSuivante();

    public EpmTDocumentRedaction modifierDocumentRedaction(final EpmTDocumentRedaction epmTDocumentInstance,
                                                           final boolean nouveauRegistreDocVersion);

    public EpmTDocumentRedaction chargerDernierParReference(String valeur);

    /**
     * Récupère une liste d'utilisateur
     *
     * @param critere le critere de recherche
     * @return la liste des utilisateurs
     */
    List<EpmTUtilisateur> chercher(UtilisateurCritere critere);

    /**
     * Permet de rechercher un template versionné par critere.
     *
     * @param critere objet rassemblant l'ensembles des critères de recherche
     * @return liste des template
     */
    EpmTTemplateVersion rechercherTemplateVersionUnique(TemplateVersionCritere critere);


    void deleteContratPub(int id, int idClause, int idPublication);


    void updateClientClauseAndCanevas();
}
