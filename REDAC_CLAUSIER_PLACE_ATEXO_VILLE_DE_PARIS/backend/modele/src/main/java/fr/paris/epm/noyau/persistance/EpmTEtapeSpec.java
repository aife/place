package fr.paris.epm.noyau.persistance;


import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

/**
 * POJO hibernate gérant les différentes phases de la consultation.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
@Audited
@AuditTable(value = "epm__t_etape_spec_aud", schema = "consultation")
public class EpmTEtapeSpec extends EpmTAbstractObject {

    public static final String DEFINITION = "Définition";
    public static final String PUBLICITE = "Publicité";
    
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * libellé de l'étape spécifique.
     */
    private String libelle;

    /**
     * Consultation.
     */
    private Integer idConsultation;

    /**
     * Consultation.
     */
    private Integer idAvenant;

    /*
    * Ordre
     */

    private Integer ordre;
    
    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return libellé de l'étape
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé de l'étape
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    /**
     * @return consultation rattachée
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param valeur consultation rattachée
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }
    
    /**
     * @return consultation rattachée
     */
    public Integer getIdAvenant() {
        return idAvenant;
    }

    /**
     * @param valeur consultation rattachée
     */
    public void setIdAvenant(final Integer valeur) {
        this.idAvenant = valeur;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }
}
