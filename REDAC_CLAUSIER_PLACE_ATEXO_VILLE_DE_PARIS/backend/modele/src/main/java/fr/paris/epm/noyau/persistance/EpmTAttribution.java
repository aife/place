/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefBeneficiaireMarche;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefMoyenNotification;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * Pojo hibernate Attribution.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTAttribution extends EpmTAbstractObject {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Mise au point du contrat.
     */
    private EpmTDocument documentMisePoint;

    /**
     * Offre finale de l'attributaire.
     */
    private EpmTDocument documentOffreFinale;

    /**
     * Utilisateur ayant le pouvoir adjudicateur pour signer.
     */
    private String utilisateur;

    /**
     * Moyen de notification.
     */
    private EpmTRefMoyenNotification moyenNotification;

    /**
     * Numéro d'avenant.
     */
    // FIXME L'avenant de EpmTAttribution est jamais setté en base. Il existe
    // une relation entre l'attribution et l'avenant dans EpmTAvenant. Cette
    // variable est inutile.
    private Integer avenant;

    /**
     * Mise au point, ou non.
     */
    private boolean miseAuPoint;

    /**
     * Date de mise au point.
     */
    private Date dateMiseAuPoint;
    
    /**
     * Date de fin de marché alimenté par ALIZE.
     */
    private Date dateFinMarche;

    /**
     * Commentaire.
     */
    private String commentaire;

    /**
     * Engagement comptable.
     */
    private boolean engagementComptable;

    /**
     * Date de l'engagement comptable.
     */
    private Date dateEngagementComptable;

    /**
     * Numéro de l'engagement comptable.
     */
    private String numEngagementComptable;

    /**
     * Date de la signature par le pouvoir adjudicateur.
     */
    private Date dateSignaturePA;

    /**
     * Référence du marché.
     */
    private String referenceMarche;

    /**
     * Date d'enregistrement du contrôle de légalité aval.
     */
    private Date dateEnrControleAval;

    /**
     * Numéro d'enregistrement du contrôle de l'égalité aval.
     */
    private String numEnrControleAval;

    /**
     * Date d'enregistrement du contrôle de légalité.
     */
    private Date dateEnrControleLegal;

    /**
     * Numéro d'enregistrement du contrôle de l'égalité.
     */
    private String numEnrControleLegal;

    /**
     * Date d'envoi ou de remise.
     */
    private Date dateEnvoiRemise;

    /**
     * Date AR.
     */
    private Date dateAR;

    /**
     * Date de publication de l'avis d'attribution.
     */
    private Date datePubAvisAttr;

    /** The beneficiaire marche. */
    private EpmTRefBeneficiaireMarche beneficiaireMarche;

    /**
     * Date d'envoi du courrier aux soummissionaires non retenu.
     */
    private Date dateEnvoiCourrierNonRetenus;
    
    /**
     * Date de validation de l'attribution.
     */
    private Date dateValidation;

    // Méthodes
    /**
     * Marché initial: référence du marché. Avenant: référence du marché "-"
     * numéro d'avenant.
     * @return référence complète
     */
    public final String getReference() {
        if (avenant != null) {
            return referenceMarche + avenant;
        }
        return referenceMarche;
    }

    // Accesseurs
    /**
     *  commentaire.
     *
     * @return commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *  commentaire.
     *
     * @param valeur commentaire
     */
    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    /**
     *  date AR.
     *
     * @return date AR
     */
    public Date getDateAR() {
        return dateAR;
    }

    /**
     *  date AR.
     *
     * @param valeur date AR
     */
    public void setDateAR(final Date valeur) {
        this.dateAR = valeur;
    }

    /**
     *  date de l'engagement comptable.
     *
     * @return date d'engagement comptable
     */
    public Date getDateEngagementComptable() {
        return dateEngagementComptable;
    }

    /**
     *  date de l'engagement comptable.
     *
     * @param valeur date d'engagement comptable
     */
    public void setDateEngagementComptable(final Date valeur) {
        this.dateEngagementComptable = valeur;
    }

    /**
     *  date d'enregistrement du contrôle de légalité.
     *
     * @return date d'enregistrement du contrôle de légalité
     */
    public Date getDateEnrControleLegal() {
        return dateEnrControleLegal;
    }

    /**
     *  date d'enregistrement du contrôle de légalité.
     *
     * @param valeur date d'enregistrement du contrôle de légalité
     */
    public void setDateEnrControleLegal(final Date valeur) {
        this.dateEnrControleLegal = valeur;
    }

    /**
     *  date d'envoi ou de remise.
     *
     * @return date d'envoi ou remise
     */
    public Date getDateEnvoiRemise() {
        return dateEnvoiRemise;
    }

    /**
     *  date d'envoi ou de remise.
     *
     * @param valeur date d'envoi ou remise
     */
    public void setDateEnvoiRemise(final Date valeur) {
        this.dateEnvoiRemise = valeur;
    }

    /**
     *  date de mise au point.
     *
     * @return date de mise au point
     */
    public Date getDateMiseAuPoint() {
        return dateMiseAuPoint;
    }

    /**
     *  date de mise au point.
     *
     * @param valeur date de misau point
     */
    public void setDateMiseAuPoint(final Date valeur) {
        this.dateMiseAuPoint = valeur;
    }

    /**
     *  date de publication de l'avis d'attribution.
     *
     * @return date de publication de l'avis d'attribution
     */
    public Date getDatePubAvisAttr() {
        return datePubAvisAttr;
    }

    /**
     *  date de publication de l'avis d'attribution.
     *
     * @param valeur date de publication de l'avis d'attribution
     */
    public void setDatePubAvisAttr(final Date valeur) {
        this.datePubAvisAttr = valeur;
    }

    /**
     *  date de la signature par le pouvoir adjudicateur.
     *
     * @return date de signature par le pouvoir adjudicateur
     */
    public Date getDateSignaturePA() {
        return dateSignaturePA;
    }

    /**
     *  date de la signature par le pouvoir adjudicateur.
     *
     * @param valeur date de signature par le pouvoir adjudicateur
     */
    public void setDateSignaturePA(final Date valeur) {
        this.dateSignaturePA = valeur;
    }

    /**
     * Checks if is engagement comptable.
     *
     * @return vrai si engagement comptable
     */
    public boolean isEngagementComptable() {
        return engagementComptable;
    }

    /**
     *  engagement comptable.
     *
     * @param valeur vrai si engagement comptable
     */
    public void setEngagementComptable(final boolean valeur) {
        this.engagementComptable = valeur;
    }

    /**
     * Checks if is mise au point, ou non.
     *
     * @return vrai si mise au point
     */
    public boolean isMiseAuPoint() {
        return miseAuPoint;
    }

    /**
     *  mise au point, ou non.
     *
     * @param valeur vrai si mise au point
     */
    public void setMiseAuPoint(final boolean valeur) {
        this.miseAuPoint = valeur;
    }

    /**
     *  identifiant.
     *
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     *  identifiant.
     *
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     *  moyen de notification.
     *
     * @return moyen de notification
     */
    public EpmTRefMoyenNotification getMoyenNotification() {
        return moyenNotification;
    }

    /**
     *  moyen de notification.
     *
     * @param valeur moyen de notification
     */
    public void setMoyenNotification(final EpmTRefMoyenNotification valeur) {
        this.moyenNotification = valeur;
    }

    /**
     *  numéro d'enregistrement du contrôle de l'égalité.
     *
     * @return numéro d'enregistrement du contrôle de légalité
     */
    public String getNumEnrControleLegal() {
        return numEnrControleLegal;
    }

    /**
     *  numéro d'enregistrement du contrôle de l'égalité.
     *
     * @param valeur numéro d'enregistrement du contrôle de légalité
     */
    public void setNumEnrControleLegal(final String valeur) {
        this.numEnrControleLegal = valeur;
    }


    /**
     *  référence du marché.
     *
     * @return référence du marché
     */
    public String getReferenceMarche() {
        return referenceMarche;
    }

    /**
     *  référence du marché.
     *
     * @param valeur référence du marché
     */
    public void setReferenceMarche(final String valeur) {
        this.referenceMarche = valeur;
    }

    /**
     *  numéro d'avenant.
     *
     * @return numéro d'avenant
     */
    // FIXME L'avenant de EpmTAttribution est jamais setté en base. Il existe
    // une relation entre l'attribution et l'avenant dans EpmTAvenant. Cette
    // variable est inutile.    
    public Integer getAvenant() {
        return avenant;
    }

    /**
     *  numéro d'avenant.
     *
     * @param valeur numéro d'avenant
     */
    // FIXME L'avenant de EpmTAttribution est jamais setté en base. Il existe
    // une relation entre l'attribution et l'avenant dans EpmTAvenant. Cette
    // variable est inutile.    
    public void setAvenant(final Integer valeur) {
        this.avenant = valeur;
    }

    /**
     *  date d'enregistrement du contrôle de légalité aval.
     *
     * @return date d'enregistrement au contrôle de la légalité avale
     */
    public Date getDateEnrControleAval() {
        return dateEnrControleAval;
    }

    /**
     *  date d'enregistrement du contrôle de légalité aval.
     *
     * @param valeur date d'enregistrement au contrôle de la légalité avale
     */
    public void setDateEnrControleAval(final Date valeur) {
        this.dateEnrControleAval = valeur;
    }

    /**
     *  mise au point du contrat.
     *
     * @return mise au point du contrat
     */
    public EpmTDocument getDocumentMisePoint() {
        return documentMisePoint;
    }

    /**
     *  mise au point du contrat.
     *
     * @param valeur mise au point du contrat
     */
    public void setDocumentMisePoint(final EpmTDocument valeur) {
        this.documentMisePoint = valeur;
    }

    /**
     *  offre finale de l'attributaire.
     *
     * @return offre finale de l'attributaire
     */
    public EpmTDocument getDocumentOffreFinale() {
        return documentOffreFinale;
    }

    /**
     *  offre finale de l'attributaire.
     *
     * @param valeur offre finale de l'attributaire
     */
    public void setDocumentOffreFinale(final EpmTDocument valeur) {
        this.documentOffreFinale = valeur;
    }

    /**
     *  numéro d'enregistrement du contrôle de l'égalité aval.
     *
     * @return numéro d'enregistrement au contrôle de la légalité avale
     */
    public String getNumEnrControleAval() {
        return numEnrControleAval;
    }

    /**
     *  numéro d'enregistrement du contrôle de l'égalité aval.
     *
     * @param valeur numéro d'enregistrement au contrôle de la légalité avale
     */
    public void setNumEnrControleAval(final String valeur) {
        this.numEnrControleAval = valeur;
    }

    /**
     *  utilisateur ayant le pouvoir adjudicateur pour signer.
     *
     * @return utilisateur ayant le pouvoir adjudicateur pour signer
     */
    public String getUtilisateur() {
        return utilisateur;
    }

    /**
     *  utilisateur ayant le pouvoir adjudicateur pour signer.
     *
     * @param valeur utilisateur ayant le pouvoir adjudicateur pour signer
     */
    public void setUtilisateur(final String valeur) {
        this.utilisateur = valeur;
    }

    /**
     *  numéro de l'engagement comptable.
     *
     * @return numéro de l'engagement comptable
     */
    public String getNumEngagementComptable() {
        return numEngagementComptable;
    }

    /**
     *  numéro de l'engagement comptable.
     *
     * @param valeur numéro de l'engagement comptable
     */
    public void setNumEngagementComptable(final String valeur) {
        this.numEngagementComptable = valeur;
    }

    /**
     *  date de fin de marché alimenté par ALIZE.
     *
     * @return Date de fin de marche alimenté par alize.
     */
	public Date getDateFinMarche() {
		return dateFinMarche;
	}

	/**
	 *  date de fin de marché alimenté par ALIZE.
	 *
	 * @param valeur Date de fin de marche alimenté par alize.
	 */
	public void setDateFinMarche(final Date valeur) {
		this.dateFinMarche = valeur;
	}

    /**
     * beneficiaire marche.
     *
     * @return beneficiaire marche
     */
    public EpmTRefBeneficiaireMarche getBeneficiaireMarche() {
        return beneficiaireMarche;
    }

    /**
     * beneficiaire marche.
     *
     * @param valeur beneficiaire marche
     */
    public void setBeneficiaireMarche(final EpmTRefBeneficiaireMarche valeur) {
        this.beneficiaireMarche = valeur;
    }

    /**
     *  date d'envoi du courrier aux soummissionaires non retenu.
     *
     * @return the date d'envoi du courrier aux soummissionaires non retenu
     */
    public Date getDateEnvoiCourrierNonRetenus() {
        return dateEnvoiCourrierNonRetenus;
    }

    /**
     *  date d'envoi du courrier aux soummissionaires non retenu.
     *
     * @param valeur the new date d'envoi du courrier aux soummissionaires non retenu
     */
    public void setDateEnvoiCourrierNonRetenus(Date valeur) {
        this.dateEnvoiCourrierNonRetenus = valeur;
    }

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(final Date valeur) {
		this.dateValidation = valeur;
	}
    
    
}
