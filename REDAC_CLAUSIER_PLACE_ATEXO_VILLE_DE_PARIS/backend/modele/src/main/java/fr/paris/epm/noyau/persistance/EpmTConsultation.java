package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeContrat;
import fr.paris.epm.noyau.persistance.referentiel.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.util.*;

public class EpmTConsultation extends AbstractConsultation implements Comparable<EpmTConsultation> {

    /**
     * Marqueur de sérialization.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Statut de la consultation lorsque ni l'ouverture initiale des
     * candidatures, ni le suivi des demandes de compléments n'ont été validés.
     */
    public static final int CANDIDATURES_NON_VALIDEES = 0;

    /**
     * Statut de la consultation lorsque les candidatures initiales ont été
     * validées.
     */
    public static final int CANDIDATURES_INITIALES_VALIDEES = 1;

    /**
     * Statut de la consultation lorsque les demandes de compléments ont été
     * validées.
     */
    public static final int CANDIDATURES_COMPLEMENTAIRES_VALIDEES = 2;

    /**
     * Statut de la consultation lorsque les candidatures initiales ainsi que
     * les demandes de compléments ont été validées.
     */
    public static final int CANDIDATURE_INITALES_COMPLEMENTAIRES = 3;

	/**
	 * DLRO
	 */
	private Calendar dateRemisePlis;

    /**
     * Indique la dernière date de modification. Cette date est mise à jour lors
     * de la création de la consultation & de la modification de la consultation
     * au niveau du formulaire amont.
     */
    private Calendar dateModification;

    private List<EpmTCalendrier> calendriers;
    List<EpmTValeurConditionnementExterneComplexe> epmTValeurConditionnementExterneComplexeList;
    List<EpmTValeurConditionnementExterne> epmTValeurConditionnementExterneList;
    /**
     * Objet contenant les attributs specifique à une DSP.
     */
    private EpmTDsp epmTDsp;

    /**
     * Opération de travaux (à terme hérité de GO).
     */
    private String operationTravaux;

    /**
     * Autre opération.
     */
    private String autreOperation = "";

    /**
     * Sélect "en vue d'attribution de".     */

    private EpmTRefTypeContrat epmTRefTypeContrat;


    /**
     * Référentiel Article.
     */
    private EpmTRefArticle epmTRefArticle;

    // Propriétés de la phase 2
    /**
     * Ensemble des lots associés à la consultation.
     */

    private Set<EpmTBudLot> epmTBudLots = new HashSet<EpmTBudLot>(0);

    private boolean rapportEstimationIdentique;

    /**
     * Bloc unitaire de donnée "lot ou consultation. Non nul si consultation non
     * allotie.
     */
    private EpmTBudLotOuConsultation epmTBudLotOuConsultation;

    /**
     * "oui" si des clauses sociales sont présentes, "non" sinon.
     */
    private String clausesSociales;

    /**
     * "oui" si des clauses environnementales sont présentes, "non" sinon.
     */
    private String clausesEnvironnementales;

    /**
     * Ensemble de {@link EpmTRefReservationLot}.
     */
    private Set<EpmTRefReservationLot> lotsReserves = new HashSet<EpmTRefReservationLot>();

    /**
     * Sélect Groupement attributaire.
     */
    private EpmTRefGroupementAttributaire epmTRefGroupementAttributaire;

    /**
     * Sélect Choix entre mois et jours.
     */
    private EpmTRefChoixMoisJour epmTRefChoixMoisJour;

    /**
     * Sélect Durée/délai/description.
     */
    private EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription;

    /**
     * Durée du marché.
     */
    private Integer dureeMarche;

    /**
     * Date de début de l'exécution des prestations.
     */
    private Calendar dateExecutionPrestationsDebut;

    /**
     * Date de fin de l'exécution des prestations.
     */
    private Calendar dateExecutionPrestationsFin;

    /**
     * descriptions de la durée.
     */
    private String descriptionDuree;

    /**
     * Sélect nombre de candidats.
     */
    private EpmTRefNbCandidatsAdmis epmTRefNbCandidatsAdmis;

    /**
     * Nombre de candidats fixe.
     */
    private Integer nombreCandidatsFixe;

    /**
     * Nombre de candidats minimum.
     */
    private Integer nombreCandidatsMin;

    /**
     * Nombre de candidats maximum.
     */
    private Integer nombreCandidatsMax;

    /**
     * "oui" si en phases successives.
     */
    private String enPhasesSuccessives;

    /**
     * Délai de validité des offres.
     */
    private Integer delaiValiditeOffres;

    /**
     * "oui" si le DCE doit être mis en ligne.
     */
    private String dceEnLigne;

    /**
     * "true" si la signature  est activé.
     */
    private Short signature;

    /**
     * "true" si le chiffrement est activé.
     */
    private Boolean chiffrement;

    /**
     * "oui" si la réponse doit se faire dans une enveloppe unique.
     */
    private String enveloppeUniqueReponse;

    /**
     * Code d'envoi à démat pour les procédures restreintes.
     */
    private String codeRestreint;

    /**
     * Code d'envoi à démat pour les procédures restreintes.
     */
    private String codeRestreintPhaseFinale;

    /**
     * Étapes de cette consultation {@link EpmTEtapeSpec}.
     */
    private List<EpmTEtapeSpec> etapes = new ArrayList();


    /**
     * Délibération amont, numéro de projet pour la délibération amont.
     */
    private String delamNumeroProj;

    /**
     * Délibération amont, date d'entrée du projet dans ALPACA.
     */
    private Date delamEntreeProjAlp;

    /**
     * Délibération amont, date du visa de la Direction des Finances.
     */
    private Date delamVisaDf;

    /**
     * Délibération amont, date du vote du Conseil d'arrondissement.
     */
    private Date delamVoteCa;

    /**
     * Délibération amont, date du vote du Conseil de Paris.
     */
    private Date delamVoteCp;

    /**
     * Délibération amont, date de l'AR du contrôle de légalité.
     */
    private Date delamContLeg;

    /*
    *   Date d'envoi du DCE dans le cadre d'un envoi differe
    * */

    private Date dateEnvoiDCEDiffere;

    private Calendar dateValeurPreinscription;

    /**
     * Délibération amont, commentaire du conseil d'Arrondissement.
     */
    private String delamCommentaireCa;

    /**
     * Délibération aval direction, numéro de projet pour la délibération aval
     * direction.
     */
    private String delavdirNumeroProj;

    /**
     * Délibération aval direction, date d'entrée du projet dans ALPACA.
     */
    private Date delavdirEntreeProjAlp;

    /**
     * Délibération aval direction, date du visa de la Direction des Finances.
     */
    private Date delavdirVisaDf;

    /**
     * Délibération aval direction, date du vote du Conseil d'arrondissement.
     */
    private Date delavdirVoteCa;

    /**
     * Délibération aval direction, date du vote du Conseil de Paris.
     */
    private Date delavdirVoteCp;

    /**
     * Délibération aval direction, date de l'AR du contrôle de légalité.
     */
    private Date delavdirContLeg;

    /**
     * Délibération aval direction, commentaire du conseil d'Arrondissement.
     */
    private String delavdirCommentaireCa;

    /**
     * Statut courante de la consultation.
     */
    private EpmTRefStatut epmTRefStatut;

    /**
     * Statut du lot précédent; éventuellement nul.
     */
    private Integer statutAncien;

    /**
     * Liste de code CPV lié à la consultation.
     */
    private Set<EpmTCpv> cpvs = new HashSet(0);

    /**
     * Code Achat
     */
    private EpmTRefCodeAchat epmTRefCodeAchat;

    /**
     * Operation - Unite Fonctionnelle
     */
    private EpmTOperationUniteFonctionnelle epmTOperationUniteFonctionnelle;

    /**
     * Date de dernière synchronisation des questions/réponses.
     */
    private Date dateDerniereSynchronisationDematQuestionReponse;
    /**
     * Date de dernière synchronisation des retraits.
     */
    private Date dateDerniereSynchronisationDematRetrait;
    /**
     * Date de dernière synchronisation des dépôts.
     */
    private Date dateDerniereSynchronisationDemateDepot;

    /**
     * Presence d'une consultation avec l'envoi d'un AAPC à DEMAT.
     */
    private boolean aapcPresent = true;
    /**
     * Presence d'un avis rectificatif (phase1).
     */
    private boolean avisRectif1Present = true;
    /**
     * Presence d'un avis rectificatif (phase2).
     */
    private boolean avisRectif2Present = true;
    /**
     * Presence d'un avis rectificatif (phase 3).
     */
    private boolean avisRectif3Present = true;
    /**
     * Structure de la consultation.
     */
    private String structure;
    /**
     * Cas des marchés faisant suite à accord cadre.
     */
    private Integer idConsultationLiee;

    /**
     * Phase courante.
     */
    private String phaseCourante;
    /**
     * Numero de séquence de depot.
     */
    private int sequenceDepot;
    /**
     * Si date modifiée dans l'ajustement dce (phase1).
     */
    private boolean ajustementDCE1Date;
    /**
     * Si mail modifié dans l'ajustement dce (phase1).
     */
    private boolean ajustementDCE1Mail;
    /**
     * Si avis rectif coché dans l'ajustement dce (phase1).
     */
    private String ajustementDCE1AvisRectificatif;
    /**
     * Si date modifiée dans l'ajustement dce (phase2).
     */
    private boolean ajustementDCE2Date;
    /**
     * Si mail modifié dans l'ajustement dce (phase2).
     */
    private boolean ajustementDCE2Mail;
    /**
     * Si avis rectif coché dans l'ajustement dce (phase2).
     */
    private String ajustementDCE2AvisRectificatif;

    /**
     * Si date modifiée dans l'ajustement dce (phase3).
     */
    private boolean ajustementDCE3Date;
    /**
     * Si mail modifié dans l'ajustement dce (phase3).
     */
    private boolean ajustementDCE3Mail;
    /**
     * Si avis rectif coché dans l'ajustement dce (phase3).
     */
    private String ajustementDCE3AvisRectificatif;

    /**
     * contient un nombre associé a la validation par une etape dans le
     * workflow.
     */
    private int validationEtape;

    /**
     * valeur de la validation etape maximal (consultation + lot en cas de
     * dissociation).
     */
    private int maximumValidationEtape;
    /**
     * S'il s'agit d'une attribution tous lot.
     */
    private String attributionTousLots;

    /**
     * Id de la consultation initiale dont est issu un marché subséquent à un
     * accord cadre ou un marché faisant suite à.
     */
    private Integer idConsultationInitiale;

    /**
     * Id du lot initial pour lequel à fait suite à un accord cadre avec la
     * consultation courante de type marché subséquent. Null dans le cas d'une
     * consultation ne faisant pas suite à un accord cadre, égal à 0 dans le cas
     * d'une consultation faisant suite à un accord cadre non allotie.
     */
    private Integer idLotAccordCadreInitial;

    private Set<EpmTReferentielExterne> referentielsExterne;

    /**
     * Indique s'il y a un acte d'engagement a signature propre.
     */
    private boolean signatureEngagement;

    /**
     * Numéro de consultation externe à RSEM
     */
    private String numeroConsultationExterne;

    /**
     * L'identifiant de l'organisme auquel la consultation est liée
     */
    private Integer idOrganisme;

    /**
     * Détermine si la consultation est compatible avec l'entité adjudicatrice
     */
    private boolean compatibleEntiteAdjudicatrice;

    /**
     * L'identifiant du contrat auquel la consultation est liée.
     * Quand faire suite à un accord cadre, une consultation est crée à partir d'un contrat
     * idContratInitial est identifiant de ce contrat
     */
    private Integer idContratInitial;

    /**
     * contient l'url de l'espace collaboratif de la consultation
     */
    private String sousEspaceCollaboratifUrl;

    /**
     * La valeur de l'option "Réponse électronique" (Refusée, Autorisée ou Obligatoire)
     */
    private EpmTRefReponseElectronique refReponseElectronique;

    private boolean critereAppliqueTousLots = true;


    /**
     * un lien donnée par mpe qui permet de consulter le dce publié
     */
    private String urlExterne;

    private Boolean mailResolutionEnvoye;
    private Boolean mailErreurEnvoye;

    /**
     * Liste des critéres d'attribution liés à la consultation.
     */
    private Set<EpmTCritereAttributionConsultation> listeCritereAttribution;

    /**
     * Type de critére d'attribution associé à la consultation.
     */
    private EpmTRefCritereAttribution critereAttribution;

    public Boolean getMailResolutionEnvoye() {
        return mailResolutionEnvoye;
    }

    public void setMailResolutionEnvoye(Boolean mailResolutionEnvoye) {
        this.mailResolutionEnvoye = mailResolutionEnvoye;
    }

    public Boolean getMailErreurEnvoye() {
        return mailErreurEnvoye;
    }

    public void setMailErreurEnvoye(Boolean mailErreurEnvoye) {
        this.mailErreurEnvoye = mailErreurEnvoye;
    }

    // Méthodes

    public List<EpmTBudLot> trieSetLots() {
        List<EpmTBudLot> empTBudLotsLots = new ArrayList<EpmTBudLot>();

        if (this.getEpmTBudLots() != null && !this.getEpmTBudLots().isEmpty()) {
            Set<EpmTBudLot> epmTBudLotsSet = this.getEpmTBudLots();

            // tri des lots
            for (Iterator<EpmTBudLot> iter = epmTBudLotsSet.iterator(); iter.hasNext(); ) {
                EpmTBudLot element = (EpmTBudLot) iter.next();
                empTBudLotsLots.add(element);
            }
            Collections.sort(empTBudLotsLots);
        }
        return empTBudLotsLots;
    }

    /**
     * Utilise uniquement le numero de consultation.
     *
     * @return code de hachage
     * @see Object#hashCode()
     */
    public final int hashCode() {
        int result = 1;
        if (numeroConsultation != null) {
            result = Constantes.PREMIER * result
                    + numeroConsultation.hashCode();
        }
        return result;
    }

    /**
     * @param obj objet à tester
     * @return vrai si les numéros de consultation sont égaux
     * @see Object#equals(Object)
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTConsultation autre = (EpmTConsultation) obj;

        if (numeroConsultation == null) {
            if (autre.numeroConsultation != null) {
                return false;
            }
        } else if (!numeroConsultation.equals(autre.numeroConsultation)) {
            return false;
        }
        return true;
    }

    /**
     * @return "oui" si autre opération
     */
    public String getAutreOperation() {
        return autreOperation;
    }

    /**
     * @param valeur "oui" si autre opération
     */
    public void setAutreOperation(final String valeur) {
        this.autreOperation = valeur;
    }

    /**
     * @return "oui" si des clauses environnementales sont présentes
     */
    public String getClausesEnvironnementales() {
        return clausesEnvironnementales;
    }

    /**
     * @param valeur "oui" si des clauses environnementales sont présentes
     */
    public void setClausesEnvironnementales(final String valeur) {
        this.clausesEnvironnementales = valeur;
    }

    /**
     * @return {@link EpmTRefReservationLot}
     */
    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTRefReservationLot> getLotsReserves() {
        return lotsReserves;
    }

    /**
     * @param valeur {@link EpmTRefReservationLot}
     */
    public void setLotsReserves(final Set<EpmTRefReservationLot> valeur) {
        this.lotsReserves = valeur;
    }

    /**
     * @return "oui" si des clauses sociales sont présentes
     */
    public String getClausesSociales() {
        return clausesSociales;
    }

    /**
     * @param valeur "oui" si des clauses sociales sont présentes
     */
    public void setClausesSociales(final String valeur) {
        this.clausesSociales = valeur;
    }

    /**
     * @return date de fin de l'exécution des prestations
     */
    public Calendar getDateExecutionPrestationsFin() {
        return dateExecutionPrestationsFin;
    }

    /**
     * @param valeur date de fin de l'exécution des prestations
     */
    public void setDateExecutionPrestationsFin(final Calendar valeur) {
        this.dateExecutionPrestationsFin = valeur;
    }

    /**
     * @return date de début de l'exécution des prestations
     */
    public Calendar getDateExecutionPrestationsDebut() {
        return dateExecutionPrestationsDebut;
    }

    /**
     * @param valeur date de début de l'exécution des prestations
     */
    public void setDateExecutionPrestationsDebut(final Calendar valeur) {
        this.dateExecutionPrestationsDebut = valeur;
    }

    /**
     * @return délai de validité des offres
     */
    public Integer getDelaiValiditeOffres() {
        return delaiValiditeOffres;
    }

    /**
     * @param valeur délai de validité des offres
     */
    public void setDelaiValiditeOffres(final Integer valeur) {
        this.delaiValiditeOffres = valeur;
    }

    /**
     * @return description de la durée
     */
    public String getDescriptionDuree() {
        return descriptionDuree;
    }

    /**
     * @param valeur description de la durée
     */
    public void setDescriptionDuree(final String valeur) {
        this.descriptionDuree = valeur;
    }

    /**
     * @return description de la durée et du délai
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefDureeDelaiDescription getEpmTRefDureeDelaiDescription() {
        return this.epmTRefDureeDelaiDescription;
    }

    /**
     * @param valeur description de la durée et du délai
     */
    public void setEpmTRefDureeDelaiDescription(final EpmTRefDureeDelaiDescription valeur) {
        this.epmTRefDureeDelaiDescription = valeur;
    }

    /**
     * @return durée du marché
     */
    public Integer getDureeMarche() {
        return dureeMarche;
    }

    /**
     * @param valeur durée du marché
     */
    public void setDureeMarche(final Integer valeur) {
        this.dureeMarche = valeur;
    }

    /**
     * @return "oui" si en phases successives
     */
    public String getEnPhasesSuccessives() {
        return enPhasesSuccessives;
    }

    /**
     * @param valeur "oui" si en phases successives
     */
    public void setEnPhasesSuccessives(final String valeur) {
        this.enPhasesSuccessives = valeur;
    }

    /**
     * @return référentiel article
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefArticle getEpmTRefArticle() {
        return epmTRefArticle;
    }

    /**
     * @param valeur référentiel article
     */
    public void setEpmTRefArticle(final EpmTRefArticle valeur) {
        this.epmTRefArticle = valeur;
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefTypeContrat getEpmTRefTypeContrat() {
        return epmTRefTypeContrat;
    }

    public void setEpmTRefTypeContrat(EpmTRefTypeContrat epmTRefTypeContrat) {
        this.epmTRefTypeContrat = epmTRefTypeContrat;
    }

    /**
     * @return sélect entre la valeur mois et jour
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefChoixMoisJour getEpmTRefChoixMoisJour() {
        return epmTRefChoixMoisJour;
    }

    /**
     * @param valeur sélect entre la valeur mois et jour
     */
    public void setEpmTRefChoixMoisJour(final EpmTRefChoixMoisJour valeur) {
        this.epmTRefChoixMoisJour = valeur;
    }

    /**
     * @return sélect Groupement Attributaire
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefGroupementAttributaire getEpmTRefGroupementAttributaire() {
        return epmTRefGroupementAttributaire;
    }

    /**
     * @param valeur sélect Groupement Attributaire
     */
    public void setEpmTRefGroupementAttributaire(
            final EpmTRefGroupementAttributaire valeur) {
        this.epmTRefGroupementAttributaire = valeur;
    }

    /**
     * @return nombre de candidats fixe
     */
    public Integer getNombreCandidatsFixe() {
        return nombreCandidatsFixe;
    }

    /**
     * @param valeur nombre de candidats fixe
     */
    public void setNombreCandidatsFixe(final Integer valeur) {
        this.nombreCandidatsFixe = valeur;
    }

    /**
     * @return nombre de candidats maximum
     */
    public Integer getNombreCandidatsMax() {
        return nombreCandidatsMax;
    }

    /**
     * @param valeur nombre de candidats maximum
     */
    public void setNombreCandidatsMax(final Integer valeur) {
        this.nombreCandidatsMax = valeur;
    }

    /**
     * @return nombre de candidats minimum
     */
    public Integer getNombreCandidatsMin() {
        return nombreCandidatsMin;
    }

    /**
     * @param valeur nombre de candidats minimum
     */
    public void setNombreCandidatsMin(final Integer valeur) {
        this.nombreCandidatsMin = valeur;
    }

    /**
     * @return opération de travaux (hérité de GO)
     */
    public String getOperationTravaux() {
        return operationTravaux;
    }

    /**
     * @param valeur opération de travaux (hérité de GO)
     */
    public void setOperationTravaux(final String valeur) {
        this.operationTravaux = valeur;
    }

    /**
     * @return ensemble des lots associés à la consultation/marché.
     * {@link EpmTBudLot}
     */
    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTBudLot> getEpmTBudLots() {
        return epmTBudLots;
    }

    /**
     * @param valeur ensemble des lots associés à la consultation/marché.
     *               {@link EpmTBudLot}
     */
    public void setEpmTBudLots(final Set<EpmTBudLot> valeur) {
        this.epmTBudLots = valeur;
    }

    /**
     * @return BUD lot ou consultation associé, null si aucun.
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTBudLotOuConsultation getEpmTBudLotOuConsultation() {
        return epmTBudLotOuConsultation;
    }

    /**
     * @param valeur BUD lot ou consultation associé, null si aucun.
     */
    public void setEpmTBudLotOuConsultation(
            final EpmTBudLotOuConsultation valeur) {
        this.epmTBudLotOuConsultation = valeur;
    }

    /**
     * @return Sélect nombre de candidats admis
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefNbCandidatsAdmis getEpmTRefNbCandidatsAdmis() {
        return epmTRefNbCandidatsAdmis;
    }

    /**
     * @param valeur sélect nombre de candidats admis
     */
    public void setEpmTRefNbCandidatsAdmis(final EpmTRefNbCandidatsAdmis valeur) {
        this.epmTRefNbCandidatsAdmis = valeur;
    }

    /**
     * @return "oui" si le DCE doit être mis en ligne
     */
    public String getDceEnLigne() {
        return dceEnLigne;
    }

    /**
     * @param valeur "oui" si le DCE doit être mis en ligne
     */
    public void setDceEnLigne(final String valeur) {
        this.dceEnLigne = valeur;
    }

    /**
     * @return "oui" si la réponse doit être dans une seule enveloppe
     */
    public String getEnveloppeUniqueReponse() {
        return enveloppeUniqueReponse;
    }

    /**
     * @param valeur "oui" si la réponse doit être dans une seule enveloppe
     */
    public void setEnveloppeUniqueReponse(final String valeur) {
        this.enveloppeUniqueReponse = valeur;
    }

    /**
     * @return "true" si signature  activé
     */
    public Short getSignature() {
        return signature;
    }

    /**
     * @param valeur "true" si signature et chiffrement activé
     */
    public void setSignature(final Short valeur) {
        this.signature = valeur;
    }

    /**
     * @return "true" si chiffrement activé
     */
    public Boolean getChiffrement() {
        return chiffrement;
    }

    /**
     * @param valeur "true" si chiffrement et chiffrement activé
     */
    public void setChiffrement(final Boolean valeur) {
        this.chiffrement = valeur;
    }

    /**
     * @return {@link EpmTEtapeSpec}
     */
    public List<EpmTEtapeSpec> getEtapes() {
        return etapes;
    }

    /**
     * @param etapes {@link EpmTEtapeSpec}
     */
    public void setEtapes(final List<EpmTEtapeSpec> etapes) {
	    this.etapes =etapes;
    }




    /**
     * @return délibération amont: commentaire du CA
     */
    public String getDelamCommentaireCa() {
        return delamCommentaireCa;
    }

    /**
     * @param valeur délibération amont: commentaire du CA
     */
    public void setDelamCommentaireCa(String valeur) {
        this.delamCommentaireCa = valeur;
    }

    /**
     * @return délibération amont: date contrôle légalité
     */
    public Date getDelamContLeg() {
        return delamContLeg;
    }

    /**
     * @param valeur délibération amont: date contrôle légalité
     */
    public void setDelamContLeg(final Date valeur) {
        this.delamContLeg = valeur;
    }

    /**
     * @return délibération amont: date entrée projet sur ALPACA
     */
    public Date getDelamEntreeProjAlp() {
        return delamEntreeProjAlp;
    }

    /**
     * @param valeur délibération amont: date entrée projet sur ALPACA
     */
    public void setDelamEntreeProjAlp(final Date valeur) {
        this.delamEntreeProjAlp = valeur;
    }

    /**
     * @return délibération amont: numéro du projet
     */
    public String getDelamNumeroProj() {
        return delamNumeroProj;
    }

    /**
     * @param valeur délibération amont: numéro du projet
     */
    public void setDelamNumeroProj(final String valeur) {
        this.delamNumeroProj = valeur;
    }

    /**
     * @return délibération amont: date visa de la Direction des Finances
     */
    public Date getDelamVisaDf() {
        return delamVisaDf;
    }

    /**
     * @param valeur délibération amont: date visa de la Direction des Finances
     */
    public void setDelamVisaDf(final Date valeur) {
        this.delamVisaDf = valeur;
    }

    /**
     * @return délibération amont: date du vote du CA
     */
    public Date getDelamVoteCa() {
        return delamVoteCa;
    }

    /**
     * @param valeur délibération amont: date du vote du CA
     */
    public void setDelamVoteCa(final Date valeur) {
        this.delamVoteCa = valeur;
    }

    /**
     * @return délibération amont: date du vote du CP
     */
    public Date getDelamVoteCp() {
        return delamVoteCp;
    }

    /**
     * @param valeur délibération amont: date du vote du CP
     */
    public void setDelamVoteCp(final Date valeur) {
        this.delamVoteCp = valeur;
    }

    /**
     * @return délibération avale Direction: commentaire CA
     */
    public String getDelavdirCommentaireCa() {
        return delavdirCommentaireCa;
    }

    /**
     * @param valeur délibération avale Direction: commentaire CA
     */
    public void setDelavdirCommentaireCa(final String valeur) {
        this.delavdirCommentaireCa = valeur;
    }

    /**
     * @return délibération avale Direction: date contrôle légalité
     */
    public Date getDelavdirContLeg() {
        return delavdirContLeg;
    }

    /**
     * @param valeur délibération avale Direction: date contrôle légalité
     */
    public void setDelavdirContLeg(final Date valeur) {
        this.delavdirContLeg = valeur;
    }

    /**
     * @return délibération avale Direction: date entrée projet ALPACA
     */
    public Date getDelavdirEntreeProjAlp() {
        return delavdirEntreeProjAlp;
    }

    /**
     * @param valeur délibération avale Direction: date entrée projet ALPACA
     */
    public void setDelavdirEntreeProjAlp(final Date valeur) {
        this.delavdirEntreeProjAlp = valeur;
    }

    /**
     * @return délibération avale Direction: numéro de projet
     */
    public String getDelavdirNumeroProj() {
        return delavdirNumeroProj;
    }

    /**
     * @param valeur délibération avale Direction: numéro de projet
     */
    public void setDelavdirNumeroProj(final String valeur) {
        this.delavdirNumeroProj = valeur;
    }

    /**
     * @return délibération avale Direction: date visa Direction et Finances
     */
    public Date getDelavdirVisaDf() {
        return delavdirVisaDf;
    }

    /**
     * @param valeur délibération avale Direction: date visa Direction et Finances
     */
    public void setDelavdirVisaDf(final Date valeur) {
        this.delavdirVisaDf = valeur;
    }

    /**
     * @return délibération avale Direction: date vote CA
     */
    public Date getDelavdirVoteCa() {
        return delavdirVoteCa;
    }

    /**
     * @param valeur délibération avale Direction: date vote CA
     */
    public void setDelavdirVoteCa(final Date valeur) {
        this.delavdirVoteCa = valeur;
    }

    /**
     * @return délibération avale Direction: date vote CP
     */
    public Date getDelavdirVoteCp() {
        return delavdirVoteCp;
    }

    /**
     * @param valeur délibération avale Direction: date vote CP
     */
    public void setDelavdirVoteCp(final Date valeur) {
        this.delavdirVoteCp = valeur;
    }

    /**
     * @return date de dernière modification du formulaire amont
     */
    public Calendar getDateModification() {
        return dateModification;
    }

    /**
     * @param valeur date de dernière modification du formulaire amont
     */
    public void setDateModification(final Calendar valeur) {
        this.dateModification = valeur;
    }

    /**
     * @return
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefStatut getEpmTRefStatut() {
        return epmTRefStatut;
    }

    /**
     * @param valeur
     */
    public void setEpmTRefStatut(final EpmTRefStatut valeur) {
        this.epmTRefStatut = valeur;
    }

    /**
     * @return les codes CPV liés à la consultation courante.
     */
    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTCpv> getCpvs() {
        return cpvs;
    }

    /**
     * @param valeur les codes CPV à associer à la consultation courante.
     */
    public void setCpvs(final Set<EpmTCpv> valeur) {
        this.cpvs = valeur;
    }

    /**
     * @return
     */
    public Date getDateDerniereSynchronisationDemateDepot() {
        return dateDerniereSynchronisationDemateDepot;
    }

    /**
     * @param valeur
     */
    public void setDateDerniereSynchronisationDemateDepot(final Date valeur) {
        this.dateDerniereSynchronisationDemateDepot = valeur;
    }

    /**
     * @return
     */
    public Date getDateDerniereSynchronisationDematQuestionReponse() {
        return dateDerniereSynchronisationDematQuestionReponse;
    }

    /**
     * @param valeur
     */
    public void setDateDerniereSynchronisationDematQuestionReponse(
            final Date valeur) {
        this.dateDerniereSynchronisationDematQuestionReponse = valeur;
    }

    /**
     * @return
     */
    public Date getDateDerniereSynchronisationDematRetrait() {
        return dateDerniereSynchronisationDematRetrait;
    }

    /**
     * @param valeur
     */
    public void setDateDerniereSynchronisationDematRetrait(final Date valeur) {
        this.dateDerniereSynchronisationDematRetrait = valeur;
    }

    /**
     * @return structure de la consultation. calculé à la validation du
     * formulaire amont.
     */
    public String getStructure() {
        return structure;
    }

    /**
     * @param valeur structure de la consultation. calculé à la validation du
     *               formulaire amont.
     */
    public void setStructure(final String valeur) {
        this.structure = valeur;
    }

    /**
     * @return codeRestreint code d'envoi à démat pour les procédures
     * restreintes
     */
    public String getCodeRestreint() {
        return codeRestreint;
    }

    /**
     * @param valeur code d'envoi à démat pour les procédures restreintes
     */
    public void setCodeRestreint(final String valeur) {
        this.codeRestreint = valeur;
    }

    /**
     * @return l'id de la consultation liée.
     */
    public Integer getIdConsultationLiee() {
        return idConsultationLiee;
    }

    /**
     * @param valeur l'id de la consultation liée.
     */
    public void setIdConsultationLiee(final Integer valeur) {
        this.idConsultationLiee = valeur;
    }

    /**
     * @return la phase courante. {@link Constantes}
     */
    public String getPhaseCourante() {
        return phaseCourante;
    }

    /**
     * @param valeur la phase courante.
     *               {@link Constantes}
     */
    public void setPhaseCourante(final String valeur) {
        this.phaseCourante = valeur;
    }

    /**
     * @return numéro de séquence de dépot papier.
     */
    public int getSequenceDepot() {
        return sequenceDepot;
    }

    /**
     * @param valeur numéro de séquence de dépot papier.
     */
    public void setSequenceDepot(final int valeur) {
        this.sequenceDepot = valeur;
    }

    /**
     * @return contient un nombre associé a la validation par une etape dans le
     * workflow
     */
    public int getValidationEtape() {
        return validationEtape;
    }

    /**
     * @param valeur contient un nombre associé a la validation par une etape dans
     *               le workflow
     */
    public void setValidationEtape(final int valeur) {
        this.validationEtape = valeur;
    }

    /**
     * @return objet contenant les attributs specifiques à une DSP.
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTDsp getEpmTDsp() {
        return epmTDsp;
    }

    /**
     * param objet contenant les attributs specifiques à une DSP.
     */
    public void setEpmTDsp(final EpmTDsp valeur) {
        this.epmTDsp = valeur;
    }

    /**
     * @return si date modifiée dans l'écran d'ajustement dce.
     */
    public boolean isAjustementDCE1Date() {
        return ajustementDCE1Date;
    }

    /**
     * @param valeur si date modifiée dans l'écran d'ajustement dce.
     */
    public void setAjustementDCE1Date(final boolean valeur) {
        this.ajustementDCE1Date = valeur;
    }

    /**
     * @return si mail modifié dans l'écran d'ajustement dce.
     */
    public boolean isAjustementDCE1Mail() {
        return ajustementDCE1Mail;
    }

    /**
     * @param valeur si mail modifié dans l'écran d'ajustement dce.
     */
    public void setAjustementDCE1Mail(final boolean valeur) {
        this.ajustementDCE1Mail = valeur;
    }

    /**
     * @return si date modifiée dans l'écran d'ajustement dce. (phase2)
     */
    public boolean isAjustementDCE2Date() {
        return ajustementDCE2Date;
    }

    /**
     * @param valeur si date modifiée dans l'écran d'ajustement dce. (phase2)
     */
    public void setAjustementDCE2Date(final boolean valeur) {
        this.ajustementDCE2Date = valeur;
    }

    /**
     * @return si mail modifié dans l'écran d'ajustement dce. (phase2)
     */
    public boolean isAjustementDCE2Mail() {
        return ajustementDCE2Mail;
    }

    /**
     * @param valeur si mail modifié dans l'écran d'ajustement dce. (phase2)
     */
    public void setAjustementDCE2Mail(final boolean valeur) {
        this.ajustementDCE2Mail = valeur;
    }

    /**
     * @return si avis rectif en phase 1.
     */
    public String getAjustementDCE1AvisRectificatif() {
        return ajustementDCE1AvisRectificatif;
    }

    /**
     * @param valeur si avis rectif en phase 1.
     */
    public void setAjustementDCE1AvisRectificatif(final String valeur) {
        this.ajustementDCE1AvisRectificatif = valeur;
    }

    /**
     * @return si avis rectif en phase 2.
     */
    public String getAjustementDCE2AvisRectificatif() {
        return ajustementDCE2AvisRectificatif;
    }

    /**
     * @param valeur si avis rectif en phase 2.
     */
    public void setAjustementDCE2AvisRectificatif(final String valeur) {
        this.ajustementDCE2AvisRectificatif = valeur;
    }

    /**
     * @return si attribution tous lots.
     */
    public String getAttributionTousLots() {
        return attributionTousLots;
    }

    /**
     * @param valeur si attribution tous lots.
     */
    public void setAttributionTousLots(final String valeur) {
        this.attributionTousLots = valeur;
    }

    /**
     * @return id de la consultation initiale dont est issu un marché subséquent
     * à un accord cadre ou un marché faisant suite à.
     */
    public Integer getIdConsultationInitiale() {
        return idConsultationInitiale;
    }

    /**
     * @param valeur id de la consultation initiale dont est issu un marché
     *               subséquent à un accord cadre ou un marché faisant suite à.
     */
    public void setIdConsultationInitiale(final Integer valeur) {
        this.idConsultationInitiale = valeur;
    }

    /**
     * @return id du lot initial pour lequel à fait suite à un accord cadre avec
     * la consultation courante de type marché subséquent.
     */
    public Integer getIdLotAccordCadreInitial() {
        return idLotAccordCadreInitial;
    }

    /**
     * @param valeur id du lot initial pour lequel à fait suite à un accord cadre
     *               avec la consultation courante de type marché subséquent.
     */
    public void setIdLotAccordCadreInitial(final Integer valeur) {
        this.idLotAccordCadreInitial = valeur;
    }

    /**
     * @return valeur de la validation etape maximal (consultation + lot en cas
     * de dissociation).
     */
    public int getMaximumValidationEtape() {
        return maximumValidationEtape;
    }

    /**
     * @param valeur valeur de la validation etape maximal (consultation + lot en
     *               cas de dissociation).
     */
    public void setMaximumValidationEtape(final int valeur) {
        this.maximumValidationEtape = valeur;
    }

    /**
     * @return true si l'AAPC present dans la consultation
     */
    public boolean isAapcPresent() {
        return aapcPresent;
    }

    /**
     * @param valeur true si l'AAPC present dans la consultation
     */
    public void setAapcPresent(final boolean valeur) {
        this.aapcPresent = valeur;
    }

    /**
     * @return l'avis rectif phase 1.
     */
    public boolean isAvisRectif1Present() {
        return avisRectif1Present;
    }

    /**
     * @param valeur l'avis rectif phase 1.
     */
    public void setAvisRectif1Present(final boolean valeur) {
        this.avisRectif1Present = valeur;
    }

    /**
     * @return l'avis rectif phase 2.
     */
    public boolean isAvisRectif2Present() {
        return avisRectif2Present;
    }

    /**
     * @param valeur l'avis rectif phase 2.
     */
    public void setAvisRectif2Present(final boolean valeur) {
        this.avisRectif2Present = valeur;
    }

    /**
     * @return si date modifié en phase 3.
     */
    public boolean isAjustementDCE3Date() {
        return ajustementDCE3Date;
    }

    /**
     * @param valeur si date modifié en phase 3.
     */
    public void setAjustementDCE3Date(final boolean valeur) {
        this.ajustementDCE3Date = valeur;
    }

    /**
     * @return si mail à envoyer modifié en phase 3.
     */
    public boolean isAjustementDCE3Mail() {
        return ajustementDCE3Mail;
    }

    /**
     * @param valeur si mail à envoyer modifié en phase 3.
     */
    public void setAjustementDCE3Mail(final boolean valeur) {
        this.ajustementDCE3Mail = valeur;
    }

    /**
     * @return si ajustement rectificatif à envoyer modifié en phase 3.
     */
    public String getAjustementDCE3AvisRectificatif() {
        return ajustementDCE3AvisRectificatif;
    }

    /**
     * @param valeur si ajustement rectificatif à envoyer modifié en phase 3.
     */
    public void setAjustementDCE3AvisRectificatif(final String valeur) {
        this.ajustementDCE3AvisRectificatif = valeur;
    }

    /**
     * @return si avis rectif en phase 3 présent.
     */
    public boolean isAvisRectif3Present() {
        return avisRectif3Present;
    }

    /**
     * @param valeur si avis rectif en phase 3 présent.
     */
    public void setAvisRectif3Present(final boolean valeur) {
        this.avisRectif3Present = valeur;
    }

    /**
     * @return code restreint en phase3.
     */
    public String getCodeRestreintPhaseFinale() {
        return codeRestreintPhaseFinale;
    }

    /**
     * @param valeur code restreint en phase3.
     */
    public void setCodeRestreintPhaseFinale(final String valeur) {
        this.codeRestreintPhaseFinale = valeur;
    }

    /**
     * @return statut précédent, éventuellement nul
     */
    public Integer getStatutAncien() {
        return statutAncien;
    }

    /**
     * @param valeur statut précédent, éventuellement nul
     */
    public void setStatutAncien(final Integer valeur) {
        this.statutAncien = valeur;
    }

    /**
     * @return code achat
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefCodeAchat getEpmTRefCodeAchat() {
        return epmTRefCodeAchat;
    }

    /**
     * @param valeur code achat
     */
    public void setEpmTRefCodeAchat(final EpmTRefCodeAchat valeur) {
        this.epmTRefCodeAchat = valeur;
    }

    /**
     * @return operation travaux / unité fonctionnelle
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTOperationUniteFonctionnelle getEpmTOperationUniteFonctionnelle() {
        return epmTOperationUniteFonctionnelle;
    }

    /**
     * @param valeur operation travaux / unité fonctionnelle
     */
    public void setEpmTOperationUniteFonctionnelle(EpmTOperationUniteFonctionnelle valeur) {
        this.epmTOperationUniteFonctionnelle = valeur;
    }

    /**
     * @return date valeur au moment de la preinscription des consultations
     */
    public Calendar getDateValeurPreinscription() {
        return dateValeurPreinscription;
    }

    /**
     * date valeur au moment de la preinscription des consultations
     */
    public void setDateValeurPreinscription(final Calendar valeur) {
        this.dateValeurPreinscription = valeur;
    }

    public boolean isRapportEstimationIdentique() {
        return rapportEstimationIdentique;
    }

    /**
     * @param valeur statut précédent, éventuellement nul
     */
    public void setRapportEstimationIdentique(final boolean valeur) {
        this.rapportEstimationIdentique = valeur;
    }

    /**
     * Permet le trie d'un ensemble de consultation par leur intitule.
     */
    public int compareTo(EpmTConsultation cmp) {
        return this.getIntituleConsultation().compareTo(cmp.getIntituleConsultation());
    }

    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTReferentielExterne> getReferentielsExterne() {
        return referentielsExterne;
    }

    public void setReferentielsExterne(final Set<EpmTReferentielExterne> valeur) {
        this.referentielsExterne = valeur;
    }

    /**
     * @return Indique s'il y a un acte d'engagement a signature propre.
     */
    public boolean isSignatureEngagement() {
        return signatureEngagement;
    }

    /**
     * @param valeur Indique s'il y a un acte d'engagement a signature propre.
     */
    public void setSignatureEngagement(final boolean valeur) {
        this.signatureEngagement = valeur;
    }

    /**
     * @return the numeroConsultationExterne
     */
    public String getNumeroConsultationExterne() {
        return numeroConsultationExterne;
    }

    public void setNumeroConsultationExterne(final String valeur) {
        this.numeroConsultationExterne = valeur;
    }

    /**
     * @return the compatibleEntiteAdjudicatrice
     */
    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(final boolean valeur) {
        this.compatibleEntiteAdjudicatrice = valeur;
    }

    /**
     * @return the idContratInitial
     */
    public Integer getIdContratInitial() {
        return idContratInitial;
    }

    public void setIdContratInitial(final Integer valeur) {
        this.idContratInitial = valeur;
    }

    /**
     * @return the sousEspaceCollaboratif
     */
    public String getSousEspaceCollaboratifUrl() {
        return sousEspaceCollaboratifUrl;
    }

    public void setSousEspaceCollaboratifUrl(final String valeur) {
        this.sousEspaceCollaboratifUrl = valeur;
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefReponseElectronique getRefReponseElectronique() {
        return refReponseElectronique;
    }

    public void setRefReponseElectronique(final EpmTRefReponseElectronique valeur) {
        this.refReponseElectronique = valeur;
    }

    public boolean isCritereAppliqueTousLots() {
        return critereAppliqueTousLots;
    }

    public void setCritereAppliqueTousLots(final boolean valeur) {
        this.critereAppliqueTousLots = valeur;
    }

    public String getUrlExterne() {
        return urlExterne;
    }

    public void setUrlExterne(String valeur) {
        this.urlExterne = valeur;
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Override
    public EpmTUtilisateur getEpmTUtilisateur() {
        return super.getEpmTUtilisateur();
    }


    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Override
    public EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur() {
        return super.getEpmTRefPouvoirAdjudicateur();
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Override
    public EpmTRefProcedure getEpmTRefProcedure() {
        return super.getEpmTRefProcedure();
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Override
    public EpmTRefNature getEpmTRefNature() {
        return super.getEpmTRefNature();
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @Override
    public EpmTRefDirectionService getEpmTRefDirectionService() {
        return super.getEpmTRefDirectionService();
    }

    public Date getDateEnvoiDCEDiffere() {
        return dateEnvoiDCEDiffere;
    }

    public void setDateEnvoiDCEDiffere(Date dateEnvoiDCEDiffere) {
        this.dateEnvoiDCEDiffere = dateEnvoiDCEDiffere;
    }

    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTCritereAttributionConsultation> getListeCritereAttribution() {
        return listeCritereAttribution;
    }

    public void setListeCritereAttribution(final Set<EpmTCritereAttributionConsultation> valeur) {
        this.listeCritereAttribution = valeur;
    }

    public List<EpmTCritereAttributionConsultation> getListeCritereAttributionTriee() {
        List<EpmTCritereAttributionConsultation> listeTriee = null;
        if (getListeCritereAttribution() != null) {
            listeTriee = new ArrayList<>(getListeCritereAttribution());
            Collections.sort(listeTriee, new Comparator<EpmTCritereAttributionConsultation>() {
                @Override
                public int compare(EpmTCritereAttributionConsultation arg0,
                                   EpmTCritereAttributionConsultation arg1) {
                    return arg0.getOrdre() - arg1.getOrdre();
                }
            });
        }
        return listeTriee;
    }

    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefCritereAttribution getCritereAttribution() {
        return critereAttribution;
    }

    public void setCritereAttribution(final EpmTRefCritereAttribution valeur) {
        this.critereAttribution = valeur;
    }

	public Calendar getDateRemisePlis() {
		return dateRemisePlis;
	}

	public void setDateRemisePlis( Calendar dateRemisePlis ) {
		this.dateRemisePlis = dateRemisePlis;
	}



    public void addCalendrier(EpmTCalendrier epmTCalendrier) {
        if (calendriers == null) {
            calendriers = new ArrayList<>();
        }
        calendriers.add(epmTCalendrier);
    }

    public List<EpmTCalendrier> getCalendriers() {
        return calendriers;
    }

    public void setCalendriers(List<EpmTCalendrier> calendriers) {
        this.calendriers = calendriers;
    }

    public List<EpmTValeurConditionnementExterneComplexe> getEpmTValeurConditionnementExterneComplexeList() {
        return epmTValeurConditionnementExterneComplexeList;
    }

    public void setEpmTValeurConditionnementExterneComplexeList(List<EpmTValeurConditionnementExterneComplexe> epmTValeurConditionnementExterneComplexeList) {
        this.epmTValeurConditionnementExterneComplexeList = epmTValeurConditionnementExterneComplexeList;
    }

    public List<EpmTValeurConditionnementExterne> getEpmTValeurConditionnementExterneList() {
        return epmTValeurConditionnementExterneList;
    }

    public void setEpmTValeurConditionnementExterneList(List<EpmTValeurConditionnementExterne> epmTValeurConditionnementExterneList) {
        this.epmTValeurConditionnementExterneList = epmTValeurConditionnementExterneList;
    }


}
