package fr.paris.epm.noyau.persistance.referentiel;

import java.util.Set;

/**
 * POJO EpmTRefTypeEvenementCalendrier de la table "epm__t_ref_type_evenement_calendrier"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeEvenementCalendrier extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    private Set<EpmTRefEvenementCalendrier> evenementAssocies;

    public Set<EpmTRefEvenementCalendrier> getEvenementAssocies() {
        return evenementAssocies;
    }

    public void setEvenementAssocies(final Set<EpmTRefEvenementCalendrier> valeur) {
        this.evenementAssocies = valeur;
    }

}