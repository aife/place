package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le domaine attributaire(s)
 * @author GAO Xuesong
 */
public class DomaineAttributaire implements Serializable {
	
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Date de notification d’un attributaire
	 */
	private Date dateNotification;
	
	/**
	 * Raison sociale de l’attributaire
	 */
	private String raisonSocialeAttributaire;
	
	/**
	 * Type d’attributaire
	 */
	private String typeAttributaire;
	
	/**
	 * Les contractants liés à cet attributaire
	 */
	private List<DomaineContractant> contractants;
	

	public final Date getDateNotification() {
		return dateNotification;
	}

	public final void setDateNotification(final Date valeur) {
		this.dateNotification = valeur;
	}

	public final String getRaisonSocialeAttributaire() {
		return raisonSocialeAttributaire;
	}

	public final void setRaisonSocialeAttributaire(final String valeur) {
		this.raisonSocialeAttributaire = valeur;
	}

	public final String getTypeAttributaire() {
		return typeAttributaire;
	}

	public final void setTypeAttributaire(final String valeur) {
		this.typeAttributaire = valeur;
	}

	public final List<DomaineContractant> getContractants() {
		return contractants;
	}

	public final void setContractants(final List<DomaineContractant> valeur) {
		this.contractants = valeur;
	}
	
}
