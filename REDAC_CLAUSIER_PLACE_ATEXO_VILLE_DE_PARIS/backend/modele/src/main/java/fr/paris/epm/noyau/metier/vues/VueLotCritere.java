package fr.paris.epm.noyau.metier.vues;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class VueLotCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Le LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(VueLotCritere.class);
    
    private Integer idLot;
    
    private List<Integer> idsLot;
    
    private Integer idConsultation;
    
    private String numeroConsultation;
    
    private String numeroLot;
    
    @Override
    public String toHQL() {
        StringBuffer requeteHQL = corpsRequete();
        if (proprieteTriee != null) {
            requeteHQL.append(" order by l."); 
            requeteHQL.append(proprieteTriee);
            if (triCroissant) {
                requeteHQL.append(" ASC ");
            } else {
                requeteHQL.append(" DESC ");
            }
        }
        return "Select l " + requeteHQL.toString();
    }

    @Override
    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(l) ");
        select.append(corpsRequete());
        LOG.debug("countHQL : " + select.toString());
        return select.toString();
    }

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map<String, Object> parametres = getParametres();

        sb.append("from EpmVLot l");
        if (idLot != null && idLot != 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            debut = true;
            parametres.put("idLot", idLot);
            sb
            .append(" l.id = :idLot ");
        }
        
        if(idsLot != null && !idsLot.isEmpty()) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" l.id IN (");
            sb.append(((Integer) idsLot.get(0)).intValue());
            for (int i = 1; i < idsLot.size(); i++) {
                sb.append(", ");
                sb.append(((Integer) idsLot.get(i)).intValue());
            }
            sb.append(") ");
            debut = true;
        }
        
        if (idConsultation != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            debut = true;
            parametres.put("idConsultation", idConsultation);
            sb
            .append(" l.idConsultation = :idConsultation ");
        }
        
        if (numeroLot != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            debut = true;
            parametres.put("numeroLot", numeroLot);
			sb.append(" l.numero = :numeroLot ");
        }

        return sb;
    }
    
    /**
     * @return the numeroConsultation
     */
    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    /**
     * @param numeroConsultation the numeroConsultation to set
     */
    public void setNumeroConsultation(String numeroConsultation) {
        this.numeroConsultation = numeroConsultation;
    }

    /**
     * @param idConsultation the idConsultation to set
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @param idLot the idLot to set
     */
    public void setIdLot(Integer idLot) {
        this.idLot = idLot;
    }
    
    /**
     * @param idsLot the idsLot to set
     */
    public void setIdsLot(List<Integer> idsLot) {
        this.idsLot = idsLot;
    }

    /**
     * @return the idConsultation
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

	public void setNumeroLot(String valeur) {
		this.numeroLot = valeur;
	}
    
    
}
