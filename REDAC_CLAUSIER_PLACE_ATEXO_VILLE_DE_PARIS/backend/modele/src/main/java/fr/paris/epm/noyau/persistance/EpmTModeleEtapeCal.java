package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefEvenementCalendrier;

import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * Classe utilisé lors du chargment des calendriers
 *         (modéle et instance) dans le calendrier GWT lors de son utilisation.
 *         Dans tous les autres cas on utilisera {@link EpmTEtapeCal}.
 * @author Léon Barsamian.
 */
public class EpmTModeleEtapeCal extends EpmTAbstractObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 5266606053987958100L;

    /**
     * identifiant de l'enregistrement.
     */
    private Integer id;

    /**
     * Code unique requis. Permet d'identifier les étapes. (E0, E1, ...)
     */
    private String code;

    /**
     * Libellé.
     */
    private String libelle;

    /**
     * Transitions sortantes.
     */
    private Set transitionsSortantes = new HashSet();

    /**
     * Transitions entrantes.
     */
    private Set transitionsEntrantes = new HashSet();
    
    /**
     * L'événement associé à l'étape qui déclenchera la mise à jour de la date réelle.
     */
    private EpmTRefEvenementCalendrier evenementAssocie;
    
    /**
     * Indique si le type d'événement associé est administrable.
     */
    private boolean administrable;

    

    /**
     * Vrai si la date réelle de l'étape n'est pas saisissable dans le
     * calendrier réel.
     */
    private boolean reelNonSaisissable;

    /**
     *  code unique requis.
     *
     * @return code de l'étape
     */
    public String getCode() {
        return code;
    }

    /**
     *  code unique requis.
     *
     * @param valeur code del'étape
     */
    public void setCode(final String valeur) {
        this.code = valeur;
    }

    /**
     *  identifiant de l'enregistrement.
     *
     * @return identifiant de l'enregistrement
     */
    public Integer getId() {
        return id;
    }

    /**
     *  identifiant de l'enregistrement.
     *
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    /**
     *  libellé.
     *
     * @return libellé
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *  libellé.
     *
     * @param valeur libellé
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }
    
    /**
     *  transitions sortantes.
     *
     * @return the transitionsSortantes
     */
    public Set<EpmTModeleTransitionCal> getTransitionsSortantes() {
        return transitionsSortantes;
    }

    /**
     *  transitions sortantes.
     *
     * @param valeur the transitionsSortantes to set
     */
    public void setTransitionsSortantes(final Set<EpmTModeleTransitionCal> valeur) {
        this.transitionsSortantes = valeur;
    }

    /**
     *  transitions entrantes.
     *
     * @return the transitionsEntrantes
     */
    public Set getTransitionsEntrantes() {
        return transitionsEntrantes;
    }

    /**
     *  transitions entrantes.
     *
     * @param valeur the transitionsEntrantes to set
     */
    public void setTransitionsEntrantes(final Set valeur) {
        this.transitionsEntrantes = valeur;
    }

    /**
     * Checks if is vrai si la date réelle de l'étape n'est pas saisissable dans le calendrier réel.
     *
     * @return vrai si la date réelle de l'étape n'est pas saisissable dans le
     * calendrier réel
     */
    public boolean isReelNonSaisissable() {
        return reelNonSaisissable;
    }

    /**
     *  vrai si la date réelle de l'étape n'est pas saisissable dans le calendrier réel.
     *
     * @param valeur vrai si la date réelle de l'étape n'est pas saisissable
     * dans le calendrier réel
     */
    public void setReelNonSaisissable(final boolean valeur) {
        this.reelNonSaisissable = valeur;
    }
    
	/**
	 *  l'événement associé à l'étape qui déclenchera la mise à jour de la date réelle.
	 *
	 * @return L'événement associé à l'étape qui déclenchera la mise à jour de la date réelle.
	 */
	public EpmTRefEvenementCalendrier getEvenementAssocie() {
		return evenementAssocie;
	}

	/**
	 *  l'événement associé à l'étape qui déclenchera la mise à jour de la date réelle.
	 *
	 * @param valeur the new l'événement associé à l'étape qui déclenchera la mise à jour de la date réelle
	 */
	public void setEvenementAssocie(EpmTRefEvenementCalendrier valeur) {
		this.evenementAssocie = valeur;
	}

    /**
     * Checks if is indique si le type d'événement associé est administrable.
     *
     * @return the indique si le type d'événement associé est administrable
     */
    public boolean isAdministrable() {
        return administrable;
    }

    /**
     *  indique si le type d'événement associé est administrable.
     *
     * @param valeur the new indique si le type d'événement associé est administrable
     */
    public void setAdministrable(boolean valeur) {
        this.administrable = valeur;
    }
}
