package fr.paris.epm.noyau.persistance.commun;

/**
 * Interface EpmTReferentiel est la racine d'héritation pour toutes les référentiels.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface EpmTRefImportExport extends EpmTRef {

    String getUid();

	default boolean estExportable() {
		return true;
	}
}
