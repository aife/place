package fr.paris.epm.noyau.persistance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Cette classe gere l'acces aux messages d'actualité.
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTMessageActualite extends EpmTAbstractObject implements Comparable<EpmTMessageActualite> {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    private String titre;

    /**
     * contenu du message.
     */
    private String texte;

    /**
     * Date du message.
     */
    private Date date;

    /**
     * Ordre d'affichage des actualités.
     */
    private int ordre;

    /**
     * @return identifiant du message
     */
    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @JsonProperty("content")
    public String getTexte() {
        return texte;
    }

    public void setTexte(final String texte) {
        this.texte = texte;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    @JsonProperty("title")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public final int compareTo(final EpmTMessageActualite messageActualite) {
        if (id == messageActualite.getId()) {
            return 1;
        } else {
            return -1;
        }
    }

    @JsonIgnore
    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(final int valeur) {
        this.ordre = valeur;
    }

}