package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

public class DomaineDureeMarche {

    private String dureeMarchee; // "Durée du marché" ou "Délai d'exécution des prestations" ou "Description libre du délai ou de la durée"

    private String uniteTemps; // remplis si "Durée du marché"

    private Integer nombreUnite; // remplis si "Durée du marché"

    private Date dateDebut; // remplis si "Délai d'exécution des prestations"

    private Date dateFin; // remplis si "Délai d'exécution des prestations"

    private String description; // remplis si "Description libre du délai ou de la durée"

    public String getDureeMarchee() {
        return dureeMarchee;
    }

    public void setDureeMarchee(String dureeMarchee) {
        this.dureeMarchee = dureeMarchee;
    }

    public String getUniteTemps() {
        return uniteTemps;
    }

    public void setUniteTemps(String uniteTemps) {
        this.uniteTemps = uniteTemps;
    }

    public Integer getNombreUnite() {
        return nombreUnite;
    }

    public void setNombreUnite(Integer nombreUnite) {
        this.nombreUnite = nombreUnite;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
