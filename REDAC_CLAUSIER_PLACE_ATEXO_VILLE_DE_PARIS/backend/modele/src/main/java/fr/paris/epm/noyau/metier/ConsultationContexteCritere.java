package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * Classe critère utilisé pour la recherche de parametrage.
 *
 * @author Rémi Villé
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ConsultationContexteCritere extends AbstractCritere implements Critere, Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 765599478321140537L;

    private String plateforme;

    private String reference;

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();

        sb.append("from EpmTConsultationContexte ");

        if (plateforme != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" plateforme = :plateforme");
            parametres.put("plateforme", plateforme);
            debut = true;
        }

        if (reference != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" reference = :reference");
            parametres.put("reference", reference);
            debut = true;
        }


        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by ").append(proprieteTriee);
            if (triCroissant) {
                sb.append(" ASC ");
            } else {
                sb.append(" DESC ");
            }
        }

        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer sb = new StringBuffer("select count(*) ");
        sb.append(corpsRequete());
        return sb.toString();
    }

    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
