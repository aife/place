package fr.paris.epm.noyau.metier;

import java.io.Serializable;

/**
 * Classe critère utilisé pour la recherche de procedure.
 * @author Rémi Villé
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ProcedureCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * libelle
     */
    private String libelle;

    /**
     * Acronyme de la procédure
     */
    private String libelleCourt;

    /**
     * Code externe (code GO, code EAI, code Editeur etc)
     */
    private String codeExterne;

    /**
     * Libellés des procédures dans les documents
     */
    private String documentLibelle;

    /**
     * Liste de tous les types de marchés
     */
    private String typeMarche;

    /**
     * Procédure accessible en écriture (ie création de consultation / modif organigramme.)
     */
    private Boolean actif;

    /**
     * id de la procedure
     */
    private Integer id;

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        sb.append("from EpmTRefProcedure p");

        if (id != null && id != 0) {
            sb.append(debut ? " where " : " and ")
                    .append(" id = ")
                    .append(id);
            debut = false;
        }

        if (libelle != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(p.libelle) LIKE lower(:libelle)");
            String libelleLike = ajouterPourcentage(libelle, true, true);
            getParametres().put("libelle", libelleLike);
            debut = false;
        }

        if (libelleCourt != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(p.libelleCourt) = lower(:libelleCourt)");
            getParametres().put("libelleCourt", libelleCourt);
            debut = false;
        }

        if (codeExterne != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(p.codeExterne) = lower(:codeExterne)");
            getParametres().put("codeExterne", codeExterne);
            debut = false;
        }

        if (documentLibelle != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(p.documentLibelle) LIKE lower(:documentLibelle)");
            String documentLibelleLike = ajouterPourcentage(documentLibelle, true, true);
            getParametres().put("documentLibelle", documentLibelleLike);
            debut = false;
        }

        if (typeMarche != null && Integer.parseInt(typeMarche) != 0) {
            sb.append(debut ? " where " : " and ");
            sb.append(" p.epmTRefTypeMarche.id = :typeMarche");
            getParametres().put("typeMarche", Integer.parseInt(typeMarche));
            debut = false;
        }

        if (actif != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" p.actif = :actif");
            getParametres().put("actif", actif);
            debut = false;
        }

        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by ").append(proprieteTriee);
            sb.append(triCroissant ? " ASC " : " DESC ");
        }
        return sb.toString();
    }

    public String toCountHQL() {
        return "select count(*) " + corpsRequete();
    }

    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    public final void setLibelleCourt(final String valeur) {
        this.libelleCourt = valeur;
    }

    public final void setCodeExterne(final String valeur) {
        this.codeExterne = valeur;
    }

    public final void setDocumentLibelle(final String valeur) {
        this.documentLibelle = valeur;
    }

    public final void setTypeMarche(final String valeur) {
        this.typeMarche = valeur;
    }

    public void setActif(Boolean valeur) {
        this.actif = valeur;
    }

    public void setId(Integer valeur) {
        this.id = valeur;
    }

}
