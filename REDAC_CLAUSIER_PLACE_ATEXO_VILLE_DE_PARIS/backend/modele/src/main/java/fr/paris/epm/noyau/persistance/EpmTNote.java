/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;


/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class EpmTNote extends EpmTAbstractObject {

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Identifiant de la consultation, dans le cas d'une instance de calendrier.
     */
    private Integer idConsultation;

    /**
     * Identifiant de l'avenant, dans le cas d'une instance de calendrier.
     */
    private Integer idAvenant;

    /**
     * Identifiant du lot {@link EpmTBudLot}).
     */
    private Integer idBudLot;

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Note.
     */
    private String note;


    /**
     * @return id identifiant (celui de la consultation)
     */
    public int getId() {
        return id;
    }


    /**
     * @param valeur identifiant (celui de la consultation)
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }


    /**
     * @return note note
     */
    public String getNote() {
        return note;
    }


    /**
     * @param valeur note
     */
    public void setNote(final String valeur) {
        this.note = valeur;
    }


    public Integer getIdConsultation() {
        return idConsultation;
    }


    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }


    public Integer getIdAvenant() {
        return idAvenant;
    }


    public void setIdAvenant(final Integer valeur) {
        this.idAvenant = valeur;
    }


    public Integer getIdBudLot() {
        return idBudLot;
    }


    public void setIdBudLot(Integer valeur) {
        this.idBudLot = valeur;
    }

}
