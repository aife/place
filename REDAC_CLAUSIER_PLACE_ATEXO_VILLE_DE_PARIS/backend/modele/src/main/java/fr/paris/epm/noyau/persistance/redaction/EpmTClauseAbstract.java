package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.commun.util.Util;
import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@MappedSuperclass
@Audited
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class EpmTClauseAbstract extends EpmTAbstractObject
	implements Comparable<EpmTClauseAbstract>, Cloneable, Serializable {

	/**
	 * Etat de la clause.
	 */
	public static final String ETAT_SUPPRIMER = "2";

	/**
	 * Etat de la clause.
	 */
	public static final String ETAT_MODIFIER = "1";

	/**
	 * Etat de la clause.
	 */
	public static final String ETAT_DISPONIBLE = "0";

	/**
	 * l'objet epmTRefTypeDocument.
	 */
	@ManyToOne
	@JoinColumn(name = "id_ref_type_document", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private EpmTRefTypeDocument epmTRefTypeDocument;

	/**
	 * l'objet epmTRefTypeClause.
	 */
	@ManyToOne
	@JoinColumn(name = "id_ref_type_clause", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private EpmTRefTypeClause epmTRefTypeClause;

	/**
	 * l'objet epmTRefThemeClause.
	 */
	@ManyToOne
	@JoinColumn(name = "id_theme", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private EpmTRefThemeClause epmTRefThemeClause;

	/**
	 * la réference de la clause.
	 */
	@Column(nullable = false)
	private String reference;

	/**
	 * attribut qui précise si la clause est parametrable par la direction.
	 */
	@Column(name = "parametrable_direction", nullable = false)
	private String parametrableDirection;

	/**
	 * attribut qui precise si la clause est parametrable par l'agent.
	 */
	@Column(name = "parametrable_agent", nullable = false)
	private String parametrableAgent;

	/**
	 * l'attribut texte fixe avant.
	 */
	@Column(name = "texte_fixe_avant", nullable = false)
	private String texteFixeAvant;

	/**
	 * l'attribut texte fixe apres.
	 */
	@Column(name = "texte_fixe_apres")
	private String texteFixeApres;

	/**
	 * l'attribut date de modification de la clause.
	 */
	@Column(name = "date_modification", nullable = false)
	private Date dateModification;

	/**
	 * l'attribut date de création de la clause.
	 */
	@Column(name = "date_creation", nullable = false)
	private Date dateCreation;

	/**
	 * la date de premiere validation de la clause.
	 */
	@Column(name = "date_premiere_validation")
	private Date datePremiereValidation;

	/**
	 * la date de derniere validation de la clause
	 */
	@Column(name = "date_derniere_validation")
	private Date dateDerniereValidation;

	/**
	 * l'attribut auteur.
	 */
	@Column(nullable = false)
	private String auteur;

	/**
	 * l'attribut actif.
	 */
	@Column(nullable = false)
	private boolean actif;

	/**
	 * idNaturePrestation permet de sauvgarder l'id de la nature de prestation
	 * choisie.
	 */
	@Column(name = "id_nature_prestation")
	private int idNaturePrestation;

	@ManyToOne
	@JoinColumn(name = "id_procedure")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private EpmTRefProcedure epmTRefProcedure;

	/**
	 * l'attribut etat.
	 */
	@Column(nullable = false)
	private String etat;

	/**
	 * Attribut qui montre si la formulation est modifiable ou non.
	 */
	@Column(name = "formulation_modifiable")
	private boolean formulationModifiable;

	/**
	 * l'attribut sautLigneTexteAvant.
	 */
	@Column(name = "saut_ligne_texte_avant")
	private boolean sautLigneTexteAvant;

	/**
	 * l'attribut sautLigneTexteApres.
	 */
	@Column(name = "saut_ligne_texte_apres")
	private boolean sautLigneTexteApres;

	/**
	 * Statut de redaction de la clause
	 */
	@ManyToOne
	@JoinColumn(name = "id_statut_redaction_clausier", nullable = false)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier;

	/**
	 * Attribut compatibilité entité adjudicatrice.
	 */
	@Column(name = "compatible_entite_adjudicatrice")
	private boolean compatibleEntiteAdjudicatrice = true;

	/**
	 * L'identifiant de l'organisme auquel la consultation est liée
	 */
	@Column(name = "id_organisme")
	private Integer idOrganisme;

	/**
	 * Détermine si la clause est de type éditeur ou client
	 */
	@Column(name = "clause_editeur")
	private boolean clauseEditeur;

	/**
	 * l'attribut contient la valeur des mots clés.
	 */
	@Column(name = "mots_cles")
	private String motsCles;

	/**
	 * texte contenu dans l'info-bull .
	 */
	@Column(name = "info_bulle_text")
	private String infoBulleText;

	/**
	 * Attribut Lien .
	 */
	@Column(name = "info_bulle_url")
	private String infoBulleUrl;

	public abstract Integer getIdClause();

	public abstract Integer getIdPublication();

	public abstract Integer getIdLastPublication(); // utilisé pour l'affichage de la liste des clauses

	public EpmTRefTypeDocument getEpmTRefTypeDocument() {
		return epmTRefTypeDocument;
	}

	public void setEpmTRefTypeDocument(EpmTRefTypeDocument epmTRefTypeDocument) {
		this.epmTRefTypeDocument = epmTRefTypeDocument;
	}

	public EpmTRefTypeClause getEpmTRefTypeClause() {
		return epmTRefTypeClause;
	}

	public void setEpmTRefTypeClause(EpmTRefTypeClause epmTRefTypeClause) {
		this.epmTRefTypeClause = epmTRefTypeClause;
	}

	public EpmTRefThemeClause getEpmTRefThemeClause() {
		return epmTRefThemeClause;
	}

	public void setEpmTRefThemeClause(EpmTRefThemeClause epmTRefThemeClause) {
		this.epmTRefThemeClause = epmTRefThemeClause;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getParametrableDirection() {
		return parametrableDirection;
	}

	public void setParametrableDirection(String parametrableDirection) {
		this.parametrableDirection = parametrableDirection;
	}

	public String getParametrableAgent() {
		return parametrableAgent;
	}

	public void setParametrableAgent(String parametrableAgent) {
		this.parametrableAgent = parametrableAgent;
	}

	public String getTexteFixeAvant() {
		return texteFixeAvant;
	}

	public void setTexteFixeAvant(String texteFixeAvant) {
		this.texteFixeAvant = texteFixeAvant;
	}

	public String getTexteFixeApres() {
		return texteFixeApres;
	}

	public void setTexteFixeApres(String texteFixeApres) {
		this.texteFixeApres = texteFixeApres;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(final java.util.Date valeur) {
		dateModification = valeur;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public int getIdNaturePrestation() {
		return idNaturePrestation;
	}

	public void setIdNaturePrestation(int idNaturePrestation) {
		this.idNaturePrestation = idNaturePrestation;
	}

	public abstract Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() ;

	public abstract void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) ;

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

	public EpmTRefProcedure getEpmTRefProcedure() {
		return epmTRefProcedure;
	}

	public void setEpmTRefProcedure(EpmTRefProcedure epmTRefProcedure) {
		this.epmTRefProcedure = epmTRefProcedure;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public boolean isFormulationModifiable() {
		return formulationModifiable;
	}

	public void setFormulationModifiable(boolean formulationModifiable) {
		this.formulationModifiable = formulationModifiable;
	}

	public boolean isSautLigneTexteAvant() {
		return sautLigneTexteAvant;
	}

	public void setSautLigneTexteAvant(boolean sautLigneTexteAvant) {
		this.sautLigneTexteAvant = sautLigneTexteAvant;
	}

	public boolean isSautLigneTexteApres() {
		return sautLigneTexteApres;
	}

	public void setSautLigneTexteApres(boolean sautLigneTexteApres) {
		this.sautLigneTexteApres = sautLigneTexteApres;
	}

	public EpmTRefStatutRedactionClausier getEpmTRefStatutRedactionClausier() {
		return epmTRefStatutRedactionClausier;
	}

	public void setEpmTRefStatutRedactionClausier(EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier) {
		this.epmTRefStatutRedactionClausier = epmTRefStatutRedactionClausier;
	}

	public boolean isCompatibleEntiteAdjudicatrice() {
		return compatibleEntiteAdjudicatrice;
	}

	public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
		this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
	}

	public Integer getIdOrganisme() {
		return idOrganisme;
	}

	public void setIdOrganisme(Integer idOrganisme) {
		this.idOrganisme = idOrganisme;
	}

	public boolean isClauseEditeur() {
		return clauseEditeur;
	}

	public void setClauseEditeur(boolean clauseEditeur) {
		this.clauseEditeur = clauseEditeur;
	}

	public abstract Set<? extends EpmTRoleClauseAbstract> getEpmTRoleClauses() ;

	public abstract void setEpmTRoleClauses(Set<? extends EpmTRoleClauseAbstract> epmTRoleClauses) ;

	public abstract Set<? extends EpmTClausePotentiellementConditionneeAbstract> getEpmTClausePotentiellementConditionnees() ;

	public abstract void setEpmTClausePotentiellementConditionnees(Set<? extends EpmTClausePotentiellementConditionneeAbstract> epmTClausePotentiellementConditionnees) ;

	public Date getDatePremiereValidation() {
		return datePremiereValidation;
	}

	public void setDatePremiereValidation(final Date valeur) {
		this.datePremiereValidation = valeur;
	}

	public Date getDateDerniereValidation() {
		return dateDerniereValidation;
	}

	public void setDateDerniereValidation(final Date valeur) {
		this.dateDerniereValidation = valeur;
	}

	public String getMotsCles() {
		return motsCles;
	}

	public void setMotsCles(String motsCles) {
		this.motsCles = motsCles;
	}

	public String getInfoBulleText() {
		return infoBulleText;
	}

	public void setInfoBulleText(String infoBulleText) {
		this.infoBulleText = infoBulleText;
	}

	public String getInfoBulleUrl() {
		return infoBulleUrl;
	}

	public void setInfoBulleUrl(String infoBulleUrl) {
		this.infoBulleUrl = infoBulleUrl;
	}

	public int compareTo(EpmTClauseAbstract epmTClauseAbstract) {
		if (epmTClauseAbstract.isActif() == isActif()) {
			if (getIdClause() > epmTClauseAbstract.getIdClause()) {
				return -1;
			} else {
				return 1;
			}
		} else {
			if (!epmTClauseAbstract.isActif()) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	@Override
	public EpmTClauseAbstract clone() throws CloneNotSupportedException {
		EpmTClauseAbstract epmTClause = (EpmTClauseAbstract) super.clone();
		epmTClause.dateModification = new Date(); // date courante

		epmTClause.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
		epmTClause.clauseEditeur = clauseEditeur;

		epmTClause.dateDerniereValidation = null;
		epmTClause.datePremiereValidation = null;

		return epmTClause;
	}

	/**
	 * cette methode trie la liste des roles Clauses.
	 * @return methode qui trie la liste des roles Clauses.
	 */
	@Deprecated
	public List<? extends EpmTRoleClauseAbstract> getEpmTRoleClausesTrie() {
		if (getEpmTRoleClauses() != null) {
			for (EpmTRoleClauseAbstract roleClause : getEpmTRoleClauses())
				roleClause.setValeurDefaut(Util.decodeCaractere(roleClause.getValeurDefaut()));
			List<EpmTRoleClauseAbstract> listRoleClause = new ArrayList<>(getEpmTRoleClauses());
			Collections.sort(listRoleClause);
			return listRoleClause;
		} else {
			return new ArrayList<>();
		}
	}

	/**
	 * cette methode trie la liste des roles Clauses.
	 * @return methode qui trie la liste des roles Clauses.
	 */
	@Deprecated
	public final List<? extends EpmTRoleClauseAbstract> getEpmTRoleClausesDefault() {
		if (getEpmTRoleClauses() != null)
			return filtreRoleDefault(getEpmTRoleClausesTrie());
		else
			return new ArrayList<>();
	}

	/**
	 * Permet de récuprer les propriètés externes défines par defaut d'une
	 * clauses sous forme de liste.
	 * @param roleNonFiltre liste des roles (propriètés externes) de la clause
	 * @return List Liste des propriètés externes par defaut
	 */
	@Deprecated
	private List<? extends EpmTRoleClauseAbstract> filtreRoleDefault(final List<? extends EpmTRoleClauseAbstract> roleNonFiltre) {
		List<EpmTRoleClauseAbstract> roleFiltre = new ArrayList<>();
		for (EpmTRoleClauseAbstract epmTRoleClauseInss : roleNonFiltre)
			if (epmTRoleClauseInss.getIdUtilisateur() == null && epmTRoleClauseInss.getIdDirectionService() == null)
				roleFiltre.add(epmTRoleClauseInss);
		return roleFiltre;
	}

	@Override
	public String toString() {
		return  reference;
	}
}
