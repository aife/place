package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeStructureSociale de la table "epm__t_ref_type_structure_sociale"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeStructureSociale extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

    public static final String CODE_ESAT_EA = "ESAT_EA";
    public static final String CODE_SIAE = "SIAE";
    public static final String CODE_EESS = "EESS";
    public static final String CODE_ACITIVITE_ECONOMIQUE = "insertionActiviteEconomique";
    public static final String CODE_COMMERCE_EQUITABLE = "commerceEquitable";
    public static final String CODE_AUTRE = "autreClauseSociale";
    public static final String CODE_ACHATS_ETHIQUES = "achatsEthiquesTracabiliteSociale";
    public static final String CODE_FORMATION_SCOLAIRE = "clauseSocialeFormationScolaire";
    public static final String CODE_LUTE_DISCRIMINATIONS = "lutteContreDiscriminations";

    public static final int ID_ESAT_EA = 1;
    public static final int ID_SIAE = 2;
    public static final int ID_EESS = 3;

    //condition d'exécution
    public static final int ID_ACITIVITE_ECONOMIQUE_CONDEXEC = 4;
    public static final int ID_COMMERCE_EQUITABLE_CONDEXEC = 5;
    public static final int ID_AUTRE_CONDEXEC = 6;
    public static final int ID_ACHATS_ETHIQUES_CONDEXEC = 7;
    public static final int ID_FORMATION_SCOLAIRE_CONDEXEC = 8;
    public static final int ID_LUTE_DISCRIMINATIONS_CONDEXEC = 9;

    //Specification
    public static final int ID_ACITIVITE_ECONOMIQUE_SPECIFICATION = 10;
    public static final int ID_COMMERCE_EQUITABLE_SPECIFICATION = 11;
    public static final int ID_AUTRE_SPECIFICATION = 12;
    public static final int ID_ACHATS_ETHIQUES_SPECIFICATION = 13;
    public static final int ID_FORMATION_SCOLAIRE_SPECIFICATION = 14;
    public static final int ID_LUTE_DISCRIMINATIONS_SPECIFICATION = 15;

    //Condition Attribution
    public static final int ID_ACITIVITE_ECONOMIQUE_CONDATTRIB = 16;
    public static final int ID_COMMERCE_EQUITABLE_CONDATTRIB = 17;
    public static final int ID_AUTRE_CONDATTRIB = 18;
    public static final int ID_ACHATS_ETHIQUES_CONDATTRIB = 19;
    public static final int ID_FORMATION_SCOLAIRE_CONDATTRIB = 20;
    public static final int ID_LUTE_DISCRIMINATIONS_CONDATTRIB = 21;

    private EpmTRefClausesSociales epmTRefClauseSociale;

    public EpmTRefClausesSociales getEpmTRefClauseSociale() {
        return epmTRefClauseSociale;
    }

    public void setEpmTRefClauseSociale(EpmTRefClausesSociales epmTRefClauseSociale) {
        this.epmTRefClauseSociale = epmTRefClauseSociale;
    }

}