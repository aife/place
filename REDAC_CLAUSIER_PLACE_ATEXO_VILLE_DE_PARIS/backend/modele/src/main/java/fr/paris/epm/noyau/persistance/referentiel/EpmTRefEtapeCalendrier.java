package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.EpmTModeleEtapeCal;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO EpmTRefEtapeCalendrier de la table "epm__t_ref_etape_calendrier"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefEtapeCalendrier extends EpmTModeleEtapeCal implements EpmTRef, Comparable {

    private static final Logger LOG = LoggerFactory.getLogger(EpmTRefEtapeCalendrier.class);
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -888936182514971149L;

    /**
     * Type d'étape.
     */
    private int deliberation;

    /**
     * code Go.
     */
    private String codeGo;

    /**
     * Position de l'étape dans le calendrier.
     */
    private int position;

    /**
     * type de commission.
     */
    private EpmTRefTypeCommission typeCommission;

    /**
     * Type de procédure.
     */
    private Integer idProcedure;

    private boolean actif;

    public int getDeliberation() {
        return deliberation;
    }

    public void setDeliberation(int deliberation) {
        this.deliberation = deliberation;
    }

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(String codeGo) {
        this.codeGo = codeGo;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public EpmTRefTypeCommission getTypeCommission() {
        return typeCommission;
    }

    public void setTypeCommission(EpmTRefTypeCommission typeCommission) {
        this.typeCommission = typeCommission;
    }

    public Integer getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(Integer idProcedure) {
        this.idProcedure = idProcedure;
    }

    @Override
    public boolean isActif() {
        return actif;
    }

    @Override
    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @Override
    public String getLibelleCourt() {
        return getLibelle();
    }

    @Override
    public void setLibelleCourt(String libelleCourt) {
        // ajouter "libelle_court" dans la base de données s'il faut différentier avec "libelle"
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"libelle_court\" dans la base de données s'il faut différentier avec \"libelle\"");
    }

    @Override
    public String getCodeExterne() {
        return null;
    }

    @Override
    public void setCodeExterne(String codeExterne) {
        // ajouter "code_externe" dans la base de données s'il faut l'avoir
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"code_externe\" dans la base de données s'il faut l'avoir");
    }

    public int compareTo(Object arg0) {
        EpmTRefEtapeCalendrier etape = (EpmTRefEtapeCalendrier) arg0;
        return Integer.compare(this.position, etape.position);
    }

}
