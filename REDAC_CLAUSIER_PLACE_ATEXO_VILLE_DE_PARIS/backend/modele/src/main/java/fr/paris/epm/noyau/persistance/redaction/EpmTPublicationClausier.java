package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * Classe représentant l'objet contenant les informations de publication du clausier.
 */
@Entity
@Table(name = "epm__t_publication_clausier", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTPublicationClausier extends EpmTAbstractObject implements Comparable<EpmTPublicationClausier> {
    
    /**
     * Identifiant unique.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "version")
    private String version;

    /**
     * Identifiant de l'utilisateur ayant publié le clausier.
     */
    @Column(name = "id_utilisateur")
    private Integer idUtilisateur;

    /**
     * Nom + prénom de l'utilisateur ayant publié le clausier utilisé pour le tri.
     */
    @Column(name = "nom_complet_utilisateur")
    private String nomCompletUtilisateur;

    /**
     * Identifiant de l'organisme
     */
    @Column(name = "id_organisme")
    private Integer idOrganisme;

    /**
     * Commentaire saisi sur l'écran d'hitorique des version
     */
    @Column(name = "commentaire")
    private String commentaire;

    /**
     * Date de publication du clausier.
     */
    @Column(name = "date_publication")
    private Date datePublication;

    /**
     * si version est actif ou pas (on ne peut avoir qu'une seul version actif chaque moment)
     */
    @Column(name = "actif")
    private boolean actif;

    @Column(name = "en_cours_activation")
    private boolean enCoursActivation;

    @Column(name = "editeur")
    private String editeur;

    @Column(name = "date_integration")
    private Date dateIntegration;

    @Column(name = "date_activation")
    private Date dateActivation;

    @Column(name = "gestion_local")
    private boolean gestionLocal;

    /**
     * fichier contenant les référentiels lors de cette publication (exel)
     */
    //@ManyToOne(targetEntity = EpmTPublicationClausierFile.class, cascade = CascadeType.ALL)
    //@JoinColumn(name = "id_file_referentiel")
    //private EpmTPublicationClausierFile fichierReferentiel;

    @Column(name = "id_file_referentiel")
    private Integer idFileReferentiel;

    /**
     * fichier contenant le clausier de cette publication (exel)
     */
    //@ManyToOne(targetEntity = EpmTPublicationClausierFile.class, cascade = CascadeType.ALL)
    //@JoinColumn(name = "id_file_clausier")
    //private EpmTPublicationClausierFile fichierClausier;

    @Column(name = "id_file_clausier")
    private Integer idFileClausier;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomCompletUtilisateur() {
        return nomCompletUtilisateur;
    }

    public void setNomCompletUtilisateur(String nomCompletUtilisateur) {
        this.nomCompletUtilisateur = nomCompletUtilisateur;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public Date getDateIntegration() {
        return dateIntegration;
    }

    public void setDateIntegration(Date dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public Date getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Date dateActivation) {
        this.dateActivation = dateActivation;
    }

    public boolean isGestionLocal() {
        return gestionLocal;
    }

    public void setGestionLocal(boolean gestionLocal) {
        this.gestionLocal = gestionLocal;
    }

    public Integer getIdFileReferentiel() {
        return idFileReferentiel;
    }

    public void setIdFileReferentiel(Integer idFileReferentiel) {
        this.idFileReferentiel = idFileReferentiel;
    }

    public Integer getIdFileClausier() {
        return idFileClausier;
    }

    public void setIdFileClausier(Integer idFileClausier) {
        this.idFileClausier = idFileClausier;
    }

    public boolean isEnCoursActivation() {
        return enCoursActivation;
    }

    public void setEnCoursActivation(boolean enCoursActivation) {
        this.enCoursActivation = enCoursActivation;
    }

    @Override
    public int compareTo(final EpmTPublicationClausier epmTPublicationClausier) {
        return this.version.compareTo(epmTPublicationClausier.version);
    }

}
