package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutEnveloppe de la table "epm__t_ref_statut_enveloppe"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutEnveloppe extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int STAT_FERMEE = 1; // Enveloppe fermée.
    public static final int STAT_OUVERTE = 2; // Enveloppe ouverte.
    public static final int STAT_HORS_DELAI = 3; // Enveloppe hors délai.
    public static final int STAT_COMPLETE = 4; // Enveloppe complète.
    public static final int STAT_A_COMPLETER = 5; // Enveloppe à compléter.
    public static final int STAT_LOT_NON_PRESENTE = 6; // Enveloppe hors délai.
    public static final int STAT_ECHEC_OUVERTURE = 7; // Echec ouverture.
    public static final int STAT_EN_COURS_DECHIFFREMENT = 9; // Echec ouverture.
    public static final int STAT_OUVERTURE_PARTIELLE = 10; // Ouverture partielle.

}