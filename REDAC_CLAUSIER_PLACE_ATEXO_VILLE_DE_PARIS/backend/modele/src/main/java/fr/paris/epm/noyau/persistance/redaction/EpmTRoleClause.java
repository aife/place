package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Pojo hibernate pour les Rôles des clauses.
 */
@Entity
@Table(name = "epm__t_role_clause", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTRoleClause extends  EpmTRoleClauseAbstract {

    /**
     * attribut qui mappe la valeur de la clé primaire .
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * l'objet epmTClause.
     */
    @ManyToOne(targetEntity = EpmTClause.class)
    @JoinColumn(name="id_clause")
    private EpmTClause epmTClause;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdRoleClause() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    @Override
    public EpmTClause getEpmTClause() {
        return epmTClause;
    }

    @Override
    public void setEpmTClause(EpmTClauseAbstract epmTClause) {
        this.epmTClause = (EpmTClause) epmTClause;
    }

    public EpmTRoleClause clone() throws CloneNotSupportedException {
        EpmTRoleClause nouveau = (EpmTRoleClause) super.clone();
        nouveau.id = 0;
        return nouveau;
    }

}
