package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "epm__t_preference", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTPreference extends EpmTAbstractObject implements Serializable {
    
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 133923131374998536L;

    /**
     * Identifiant technique de la préférence.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    /**
     * Identifiant tecnique de la clause.
     */
    @Column(name="id_clause")
    private Integer idClause;

    /**
     * révision Envers de la clause Dans le cas d'une clause client la version
     * de la clause au moment de la création de la préférence, dans le cas d'une
     * clause éditeur la version de la clause au moment de sa dernière modification avant qu'elle ne soit publiée.
     * Cela correspond a la version envers de la clause à la mise à jour de la propriété "VersionClausierDerniereModification" de epmtclause.
     */
    @Column(name="revision_clause")
    private int revisionClause;
    
    /**
     * attribut identificateur de direction.
     */
    @Column(name="id_direction_service")
    private Integer idDirectionService;
    
    /**
     * attribut identificateur d'agent.
     */
    @Column(name="id_utilisateur")
    private Integer idUtilisateur;


    public final int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public int getIdClause() {
        return idClause;
    }


    public void setIdClause(final Integer valeur) {
        this.idClause = valeur;
    }


    public int getRevisionClause() {
        return revisionClause;
    }


    public void setRevisionClause(final int valeur) {
        this.revisionClause = valeur;
    }

    public Integer getIdDirectionService() {
        return idDirectionService;
    }

    public void setIdDirectionService(Integer idDirectionService) {
        this.idDirectionService = idDirectionService;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

}
