package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;

/**
 * Interface EpmTReferentielByOrganisme est la racine d'héritation pour toutes les référentiels par organisme.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface EpmTReferentielByOrganisme extends EpmTRef {

    /**
     * @return organisme de la ligne
     */
    abstract public EpmTRefOrganisme getEpmTRefOrganisme();

    /**
     * @param epmTRefOrganisme organisme de la ligne
     */
    abstract public void setEpmTRefOrganisme(final EpmTRefOrganisme epmTRefOrganisme);

}
