package fr.paris.epm.noyau.metier.vues;

import fr.paris.epm.noyau.metier.AbstractConsultationCritere;
import fr.paris.epm.noyau.metier.Critere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Regis Menet
 * @version $Revision: $, $Date: $, $Author: $
 *
 */
public class VueConsultationCritere extends AbstractConsultationCritere
        implements Critere, Serializable {

    /**
     * Le LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(VueConsultationCritere.class);

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = -3921384059116029626L;

    /**
     * Valeur de la chaîne lorsque consultation non transverse.
     */
    public static final String NON_TRANSVERSE = "non";

    /**
     * procedure de type DSP.
     */
    private Boolean dsp = null;

    /**
     * statut de la consultation.
     */
    private Integer statut;

    /**
     * date de début de recherche de consultation.
     */
    private Date dateRemiseDebut;

    /**
     * date de fin de recherche de consultation.
     */
    private Date dateRemiseFin;

    /**
     * mots clés associé de la consultation.
     */
    private String motsCles;

    /**
     * consultation transverse.
     */
    private String consultationTransverse;

    /**
     * consultation MPS.
     */
    private Boolean consultationMPS;

    /**
     * référence de la consultation.
     */
    private String reference;

    /**
     * directionServices associé à la consultation.
     */
    private int directionServices;

    /**
     * procédure de passation.
     */
    private int procedureDePassation;

    private List procedureDePassationList;

    /**
     * nature de la prestation.
     */
    private int naturePrestation;

    /**
     * pouvoir adjudicateur.
     */
    private int pouvoirAdjudicateur;

    /**
     * pouvoir adjudicateur.
     */
    private int enVueAttribution;

    /**
     * objet de la consultation.
     */
    private String objetConsultation;

    /**
     * id de l'utilisateur.
     */
    private int utilisateurId;

    /**
     * Recherche par le nom de la personne suivant le dossier.
     */
    private String nomPersonneSuivantConsultation;

    /**
     * numero representant l'etape venant d'etre vaider.
     */
    private int etatValide;

    /**
     * une liste de statuts de consultation.
     */
    private List listeStatuts;

    /**
     * numero du marché GO.
     */
    private String numeroMarcheGo;

    /**
     * numero de l'operatin Go.
     */
    private String numeroOperationGo;

    /**
     * Identifiant de la consultation.
     */
    private Integer idConsultation;

    /**
     * id lot dissocie
     */
    private Integer idLotDissocie;

    /**
     * Reference externe de la consultation (ie envoyé par mpe dans le cadre de l'intégration de rédac).
     */
    private String numeroConsultationExterne;

    /**
     * Pour la recherche de consultations par contrat.
     */
    private String numeroContrat;

    private Integer autoriteDelegante;

    /**
     * @param valeur consultation transverse
     */
    public final void setConsultationTransverse(final String valeur) {
        this.consultationTransverse = valeur;
    }

    /**
     * @param consultationMPS consultation MPS
     */
    public final void setConsultationMPS(Boolean consultationMPS) { this.consultationMPS = consultationMPS; }

    /**
     * @param valeur date de début de recherche des consultations
     */
    public final void setDateRemiseDebut(final Date valeur) {
        this.dateRemiseDebut = valeur;
    }

    /**
     * @param valeur date de fin de recherche des consultations
     */
    public final void setDateRemiseFin(final Date valeur) {
        this.dateRemiseFin = valeur;
    }

    /**
     * @param valeur direction service de la consultation
     */
    public final void setDirectionServices(final int valeur) {
        this.directionServices = valeur;
    }

    /**
     * @param valeur mot clés de la consultation
     */
    public final void setMotsCles(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.motsCles = valeur.toLowerCase();
        }
    }

    /**
     * @param valeur référence de la consultation
     */
    public final void setReference(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.reference = valeur.toLowerCase().trim();
        }
    }

    /**
     * @param valeur pouvoir ajudicateur
     */
    public final void setPouvoirAdjudicateur(final int valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    /**
     * @param valeur procédure de passation
     */
    public final void setProcedureDePassation(final int valeur) {
        this.procedureDePassation = valeur;
    }

    /**
     * @param valeur statut de la consultation
     */
    public final void setStatut(final Integer valeur) {
        statut = valeur;
    }

    /**
     * @param valeur nature de la prestation
     */
    public final void setNaturePrestation(final int valeur) {
        this.naturePrestation = valeur;
    }

    /**
     * @param valeur objet de la consultation
     */
    public final void setObjetConsultation(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.objetConsultation = valeur.toLowerCase().trim();
        }
    }


    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();

        sb.append("from EpmVConsultation c");

        if (numeroContrat != null && !numeroContrat.isEmpty()) {
            sb.append(" , EpmTContrat contrat");
        }
        if (reference != null) {
            sb.append(" where lower(c.numeroConsultation)= lower(:reference)");
            parametres.put("reference", reference);
            debut = true;
        }
        if (numeroOperationGo != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (numeroOperationGo != null) {
                sb.append(" lower(c.goTRecuMarche.numeroOperation) = :numeroOperationGo");
                parametres.put("numeroOperationGo", numeroOperationGo);
            }
            debut = true;
        }
        if (numeroMarcheGo != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (numeroMarcheGo != null) {
                sb.append(" lower(c.goTRecuMarche.numeroContractuel) = :numeroMarcheGo");
                parametres.put("numeroMarcheGo", numeroMarcheGo);
            }
            debut = true;
        }

        if (directionServices >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefDirectionService.id =");
            sb.append(directionServices);
            debut = true;

        }
        if (idOrganisme != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.idOrganisme =");
            sb.append(idOrganisme);
            debut = true;

        }
        if (etatValide > 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.validationEtape >=");
            sb.append(etatValide);
            debut = true;

        }
        if (utilisateurId >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTUtilisateur.id=");
            sb.append(utilisateurId);
            debut = true;

        }

        if (motsCles != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            String[] motsClesTab = motsCles.split(",");
            StringBuffer intitule = new StringBuffer("");
            StringBuffer objet = new StringBuffer("");
            StringBuffer numeroConsultation = new StringBuffer("");
            for (int i = 0; i < motsClesTab.length; i++) {
                if (intitule.length() != 0) {
                    intitule.append(" OR ");
                }
                intitule.append(" lower(c.intituleConsultation) ").append(
                        "like lower(:motCle").append(i).append(")");
                parametres.put("motCle" + i, pourcentTrim(motsClesTab[i]));

                if (objet.length() != 0) {
                    objet.append(" OR ");
                }
                objet.append(" lower(c.objet) like lower(:motCle" + i + ")");

                if (numeroConsultation.length() != 0) {
                    numeroConsultation.append(" OR ");
                }
                numeroConsultation.append(" lower(c.numeroConsultation) ")
                        .append("like lower(:motCle" + i + ")");
            }
            if (objet.length() != 0) {
                sb.append("(");
                sb.append(intitule);
                sb.append(" OR ");
                sb.append(objet);
                sb.append(" OR ");
                sb.append(numeroConsultation);
                sb.append(")");
            }
            debut = true;
        }
        if (enVueAttribution >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefTypeContrat.id= ");
            sb.append(enVueAttribution);
            debut = true;
        }
        if (procedureDePassationList != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefProcedure.id IN (");
            for (int i = 0; i < procedureDePassationList.size(); i++) {
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append((Integer) procedureDePassationList.get(i));
            }
            sb.append(")");
            debut = true;
        } else if (procedureDePassation >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefProcedure.id= ");
            sb.append(procedureDePassation);
            debut = true;
        }
        if (naturePrestation >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefNature.id= ");
            sb.append(naturePrestation);
            debut = true;
        }

        if (objetConsultation != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" lower(c.objet)= lower(:objet)");
            parametres.put("objet", objetConsultation);
            debut = true;
        }

        if (pouvoirAdjudicateur >= 1) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefPouvoirAdjudicateur.id= ");
            sb.append(pouvoirAdjudicateur);
            debut = true;
        }

        if (nomPersonneSuivantConsultation != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" lower(c.epmTUtilisateur.nom) like lower(:nomPerson)");
            parametres.put("nomPerson",
                    pourcentTrim(nomPersonneSuivantConsultation));
            debut = true;
        }

        // statut
        if (statut != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (listeStatuts != null && listeStatuts.size() > 0) {
                sb.append(" ( ");
            }
            sb.append(" c.epmTRefStatut.id = ");
            sb.append(statut);
            debut = true;
        }

        if (listeStatuts != null && listeStatuts.size() > 0) {
            if (statut != null) {
                sb.append(" or ");
            } else if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.epmTRefStatut.id IN ( ");
            for (Iterator iter = listeStatuts.iterator(); iter.hasNext();) {
                Integer element = (Integer) iter.next();
                sb.append(element.intValue());
                if (iter.hasNext()) {
                    sb.append(",");
                }
            }
            sb.append(" ) ");
            if (statut != null) {
                sb.append(" ) ");
            }
            debut = true;
        }

        if (consultationTransverse != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" lower(c.transverse) = lower(:consTransverse)");
            parametres.put("consTransverse", consultationTransverse.trim());
            debut = true;
        }

        if (consultationMPS != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.marchePublicSimplifie = :consMPS");
            parametres.put("consMPS", consultationMPS);
            debut = true;
        }

        // transverse

        if (dsp != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (dsp.booleanValue()) {
                sb.append(" c.epmTDsp != null");
            } else {
                sb.append(" c.epmTDsp = null");
            }
            if(autoriteDelegante != null && autoriteDelegante >= 1) {
                sb.append(" and c.epmTDsp.epmTRefAutoriteDelegante.id = :autoriteDelegante");
                parametres.put("autoriteDelegante", autoriteDelegante);
            }
            debut = true;
        }

        if (dateRemiseDebut != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            parametres.put("dateRemiseDebut", dateRemiseDebut);

            sb.append("((c.dateReceptionOffreHerite != null and c.dateReceptionOffreHerite > :dateRemiseDebut)")
                    .append("OR (c.dateReceptionOffrePrevue != null AND c.dateReceptionOffrePrevue > :dateRemiseDebut AND c.dateReceptionOffreHerite = null)")
                    .append("OR (c.dateCandidatureHerite != null AND c.dateCandidatureHerite > :dateRemiseDebut AND c.dateReceptionOffreHerite = null AND c.dateReceptionOffrePrevue = null)")
                    .append("OR (c.dateCandidaturePrevue != null AND c.dateCandidaturePrevue > :dateRemiseDebut AND c.dateReceptionOffreHerite = null AND c.dateReceptionOffrePrevue = null AND c.dateCandidatureHerite = null))");
            debut = true;
        }

        if (dateRemiseFin != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            parametres.put("dateRemiseFin", dateRemiseFin);

            sb.append("((c.dateReceptionOffreHerite != null and c.dateReceptionOffreHerite < :dateRemiseFin)")
                    .append("OR (c.dateReceptionOffrePrevue != null AND c.dateReceptionOffrePrevue < :dateRemiseFin AND c.dateReceptionOffreHerite = null)")
                    .append("OR (c.dateCandidatureHerite != null AND c.dateCandidatureHerite < :dateRemiseFin AND c.dateReceptionOffreHerite = null AND c.dateReceptionOffrePrevue = null)")
                    .append("OR (c.dateCandidaturePrevue != null AND c.dateCandidaturePrevue < :dateRemiseFin AND c.dateReceptionOffreHerite = null AND c.dateReceptionOffrePrevue = null AND c.dateCandidatureHerite = null))");
            debut = true;
        }

        if (idConsultation != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            debut = true;
            parametres.put("idConsultation", idConsultation);
            sb.append(" c.idConsultation = :idConsultation ");
        }

        // Périmètres d'intervention et de vision
        if (dirServiceUtilisateur != null
                && !dirServiceUtilisateur.isTransverse()) {
            perimetres(sb, debut);
        }

        if (idLotDissocie != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (idLotDissocie != 0) {
                sb.append(" c.idLot = ");
                sb.append(idLotDissocie);
            } else {
                sb.append(" c.lotDissocie = false ");
            }
        }

        if (numeroConsultationExterne != null) {
            debut = gererDebut(debut, sb);
            sb.append(" c.numeroConsultationExterne = :numeroConsultationExterne");
            parametres.put("numeroConsultationExterne", numeroConsultationExterne);
        }

        if (numeroContrat != null && !numeroContrat.isEmpty()) {
            debut = gererDebut(debut, sb);
            sb.append(" contrat.numeroConsultation = c.numeroConsultation");
            sb.append(" and ");
            sb.append(" lower(contrat.numeroContrat) = :numeroContrat");
            parametres.put("numeroContrat", numeroContrat);
        }

        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer requeteHQL = corpsRequete();
        if (proprieteTriee != null) {
            requeteHQL.append(" order by c.");
            requeteHQL.append(proprieteTriee);
            if (triCroissant) {
                requeteHQL.append(" ASC ");
            } else {
                requeteHQL.append(" DESC ");
            }
        }
        return "Select c " + requeteHQL.toString();
    }

    /**
     * Pour compter le nombre de résultat.
     */
    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(c) ");
        select.append(corpsRequete());
        LOG.debug("countHQL : " + select.toString());
        return select.toString();
    }

    /**
     * @param valeur id de l'utilisateur
     */
    public final void setUtilisateurId(final int valeur) {
        this.utilisateurId = valeur;
    }

    /**
     * @param valeur le nom de la personne ayant créé la consultation à rechercher.
     */
    public final void setNomPersonneSuivantConsultation(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.nomPersonneSuivantConsultation = valeur.toLowerCase().trim();
        }
    }

    /**
     * @param valeur le nom de la personne ayant créé la consultation à rechercher.
     */
    public final void setEtatValide(final int valeur) {
        this.etatValide = valeur;
    }

    /**
     * @param valeur true si procedure de type DSP.
     */
    public final void setDsp(final Boolean valeur) {
        this.dsp = valeur;
    }

    /**
     * @return the dsp
     */
    public final Boolean isDsp() {
        return dsp;
    }

    /**
     * @param valeur true si procedure de type DSP.
     */
    public final void setEnVueAttribution(final int valeur) {
        this.enVueAttribution = valeur;
    }

    /**
     * @return the reference
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @return la date de début (limite de remise des plis).
     */
    public final Date getDateRemiseDebut() {
        return dateRemiseDebut;
    }

    /**
     * @return la date de fin (limite de remise des plis).
     */
    public final Date getDateRemiseFin() {
        return dateRemiseFin;
    }

    /**
     * @return nombre representant l'etape venant d'etre validé.
     */
    public final int getEtatValide() {
        return etatValide;
    }

    /**
     * @return the dsp
     */
    public final Boolean getDsp() {
        return dsp;
    }

    /**
     * @return the statut
     */
    public final Integer getStatut() {
        return statut;
    }

    /**
     * @return the motsCles
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * @return the consultationTransverse
     */
    public final String getConsultationTransverse() {
        return consultationTransverse;
    }

    /**
     * @return the directionServices
     */
    public final int getDirectionServices() {
        return directionServices;
    }

    /**
     * @return the procedureDePassation
     */
    public final int getProcedureDePassation() {
        return procedureDePassation;
    }

    /**
     * @return the procedureDePassationList
     */
    public final List getProcedureDePassationList() {
        return procedureDePassationList;
    }

    /**
     * @return the naturePrestation
     */
    public final int getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @return the pouvoirAdjudicateur
     */
    public final int getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @return the enVueAttribution
     */
    public final int getEnVueAttribution() {
        return enVueAttribution;
    }

    /**
     * @return the objetConsultation
     */
    public final String getObjetConsultation() {
        return objetConsultation;
    }

    /**
     * @return the utilisateurId
     */
    public final int getUtilisateurId() {
        return utilisateurId;
    }

    /**
     * @return the nomPersonneSuivantConsultation
     */
    public final String getNomPersonneSuivantConsultation() {
        return nomPersonneSuivantConsultation;
    }

    /**
     * @return the listeStatuts
     */
    public final List getListeStatuts() {
        return listeStatuts;
    }

    /**
     * @return the numeroMarcheGo
     */
    public final String getNumeroMarcheGo() {
        return numeroMarcheGo;
    }

    /**
     * @return the numeroOperationGo
     */
    public final String getNumeroOperationGo() {
        return numeroOperationGo;
    }

    public final void setProcedureDePassationList(final List valeur) {
        procedureDePassationList = valeur;
    }

    /**
     * @param valeur liste des status possible
     */
    public final void setListeStatuts(final List valeur) {
        this.listeStatuts = valeur;
    }

    /**
     * @param valeur numero de marche GO
     */
    public final void setNumeroMarcheGo(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.numeroMarcheGo = valeur.toLowerCase().trim();
        }
    }

    /**
     * @param valeur numero d'operation GO.
     */
    public final void setNumeroOperationGo(final String valeur) {
        if (valeur != null && !valeur.trim().equals("")) {
            this.numeroOperationGo = valeur.toLowerCase().trim();
        }
    }

    /**
     * @param valeur identifiant de la consultation recherchée.
     */
    public final void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     *
     * @param valeur id lot dissocie
     */
    public final void setIdLotDissocie(final Integer valeur) {
        this.idLotDissocie = valeur;
    }

    /**
     * @return the typePerimetre
     */
    public final int getTypePerimetre() {
        return typePerimetre;
    }

    /**
     * @param valeur the typePerimetre to set
     */
    public final void setTypePerimetre(int valeur) {
        this.typePerimetre = valeur;
    }

    public final void setNumeroConsultationExterne(final String valeur) {
        this.numeroConsultationExterne = valeur;
    }

    public String getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(String valeur) {
        this.numeroContrat = valeur;
        if (valeur != null)
            this.numeroContrat = valeur.toLowerCase().trim();
    }

    public Integer getAutoriteDelegante() {
        return autoriteDelegante;
    }

    public void setAutoriteDelegante(Integer autoriteDelegante) {
        this.autoriteDelegante = autoriteDelegante;
    }

}
