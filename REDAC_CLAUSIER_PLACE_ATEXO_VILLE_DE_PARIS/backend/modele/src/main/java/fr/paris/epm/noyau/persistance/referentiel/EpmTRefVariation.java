package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefVariation de la table "epm__t_ref_variation"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefVariation extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int PRIX_ACTUALISABLES = 1; // "Prix actualisables".
    public static final int PRIX_REVISABLES = 2; // "Prix révisables".
    public static final int PRIX_FERMES = 3; // "Prix fermes".

    /**
     * identifiant GO.
     */
    private String codeGo;

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(final String valeur) {
        this.codeGo = valeur;
    }

}