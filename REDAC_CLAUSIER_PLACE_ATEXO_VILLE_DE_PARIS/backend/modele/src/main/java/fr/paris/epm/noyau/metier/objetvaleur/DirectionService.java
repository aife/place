package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Classe deconnectée de la base de donnée utilisée par les modules.
 * Les directions/service peuvent être manipulées facilement via des méthodes
 * spéciales.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public final class DirectionService implements Serializable, Comparable<DirectionService> {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Propriétés à ignorer lors de la copie.
     */
    public static final String[] IGNORER = {"enfants"};

    /**
     * Identifiant de la racine (direction/service virtuelle).
     */
    public static final int RACINE = -1;

    /**
     * Code pour un périmètre d'intervention.
     */
    public static final int P_INTERVENTION = 1;

    /**
     * Code pour un périmètre de vision.
     */
    public static final int P_VISION = 2;

    /**
     * Code pour tous les périmètres (réunion des résultats).
     */
    public static final int P_TOUS = 3;

    // Propriétés
    /**
     * Identifiant de cette entité.
     */
    private int id;

    /**
     * Niveau de cette entité.
     */
    private String niveau;

    /**
     * Nom de l'entité.
     */
    private String libelle;

    /**
     * Courriel de cette entité.
     */
    private String courriel;

    /**
     * Parent de cette entité. Null si niveau le plus haut.
     */
    private DirectionService parent;

    /**
     * Code de la branche.
     */
    private String libelleCourt;

    /**
     * Possède les droits d'accès transverse en intervention.
     */
    private boolean transverse;

    /**
     * Liste des enfants attachés à cette entité.
     */
    private List<DirectionService> enfants = new ArrayList<>(0);

    /**
     * Identifiants des directions/services transverses.
     */
    private static List<Integer> idsTransverses = new Vector<>(0);

    // Méthodes
    /**
     * Recherche dans la hiérarchie des parents si la direction/service
     * possédant cet identifiant existe. Dont la direction/service courante.
     * Il ne doit pas y avoir de boucle dans l'arbre des direction/service
     * @param idDs identifiant de la direction/service à chercher
     * @return vrai si trouvé dans la hiérarchie des parents; faux sinon
     */
    public boolean contientAuDessus(final int idDs) {
        if (id == idDs) {
            // Trouvée
            return true;
        }

        if (parent == null) {
            // Non trouvée
            return false;
        } else {
            // Continue la recherche avec la direction/service au dessus
            return parent.contientAuDessus(idDs);
        }
    }

    /**
     * Ordre de base en haut.
     * Il s'agit de la liste des direction qui ont le droit d'intervenir sur la direction courrante.
     * @return liste des identifiants des parents (dont celle-ci)
     */
    public List<Integer> listeIdsAuDessus() {
        DirectionService courant = this;
        List<Integer> ids = new ArrayList<>();
        while (courant != null && courant.id != RACINE) {
            ids.add(courant.id);
            courant = courant.parent;
        }
        return ids;
    }

    /**
     * Parcours en profondeur pour renvoyer la liste des identifiants des
     * directions/service situées en dessous de celle-ci (comprise). Il s'agit
     * du périmétre d'intervention de la direction courrante. C'est à dire que
     * les utilisateurs de cette direction on le droit d'intervention sur les
     * directions retournées
     * @return liste des identifiants au dessous
     */
    public List<Integer> listeIdsAuDessous() {
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(id);

        enfants.stream()
                .map(DirectionService::listeIdsAuDessous)
                .forEach(ids::addAll);

        return ids;
    }

    /**
     * @return direction (niveau 1) de la DS courante.
     */
    public Integer getIdDirection() {
        // Récupère la liste des direction/services au dessus. Dont courant.
        final List<Integer> liste = listeIdsAuDessus();
        assert liste != null;
        assert liste.size() > 0;
        return liste.get(liste.size() - 1);
    }


    /**
     * @param idDsResponsable idnetifiant de la direction/service responsable
     * @return vrai si la direction/service courante est dans le périmètre
     * d'intervention de la direction/service possédant l'identifiant passé
     * en paramètre
     */
    public boolean aInterventionSur(final int idDsResponsable) {
        boolean intervention = transverse || contientAuDessous(idDsResponsable);
        return intervention;
    }

    /**
     * @param idDsResponsable identifiant de la direction/service responsable
     * @param idDsBeneficiaire identifiant de la direction/service bénéficiaire
     * @return vrai si la direction/service courante est dans le périmètre
     * de vision de la direction/service possédant l'identifiant passé en
     * paramètre
     */
    public boolean aVisionSur(final int idDsResponsable, final int idDsBeneficiaire) {
        boolean intervention = aInterventionSur(idDsResponsable);
        return aVisionSur(intervention, idDsBeneficiaire);
    }

    /**
     * @param intervention vrai si possède l'intervention
     * @param idDsBeneficiaire identifiant de la direction/service bénéficiaire
     * @return vrai si la direction/service courante est dans le périmètre
     * de vision de la direction/service possédant l'identifiant passé en
     * paramètre
     */
    public boolean aVisionSur(final boolean intervention, final int idDsBeneficiaire) {
        if (intervention)
            return true;
        return contientAuDessus(idDsBeneficiaire);
    }

    /**
     * Recherche en profondeur dans la hiérarchie des enfants si la
     * direction/service possédant cet identifiant existe. Dont la
     * direction/service courante. Il ne doit pas y avoir de boucle dans
     * l'arbre des direction/service.
     * @param idDs identifiant de la direction/service à chercher
     * @return vrai si trouvé dans la hiérarchie des enfants; faux sinon
     */
    public boolean contientAuDessous(final int idDs) {
        if (id == idDs)
            return true; // Trouvée

        // Parcours en profondeur sur chaque enfant.
        return enfants.stream()
                .anyMatch(enfantCour -> enfantCour.contientAuDessous(idDs));
    }

    /**
     * @return nom de la direction/service, précédée par les noms des parents
     */
    public String libelleComplet() {
        if (parent != null &&  parent.id != -1)
            return parent.libelleComplet() + " " + libelle;
        else
            return libelle;
    }



    /**
     * Les noms des direction/service doivent être non nuls.
     * @param autreDs comparé
     * @return résultat de la comparaison
     */
    @Override
    public int compareTo(final DirectionService autreDs) {
        return libelle.compareTo(autreDs.libelle);
    }

    // Accesseurs
    /**
     * @return courriel de cette entité
     */
    public String getCourriel() {
        return courriel;
    }

    /**
     * @param valeur courriel de cette entité
     */
    public void setCourriel(final String valeur) {
        this.courriel = valeur;
    }

    /**
     * @return liste des enfants attachés à cette entité
     */
    public List<DirectionService> getEnfants() {
        return enfants;
    }

    /**
     * @param valeur liste des enfants attachés à cette entité
     */
    public void setEnfants(final List<DirectionService> valeur) {
        this.enfants = valeur;
    }

    /**
     * @return identifiant de cette entité
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de cette entité
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return niveau de cette entité
     */
    public String getNiveau() {
        return niveau;
    }

    /**
     * @param valeur niveau de cette entité
     */
    public void setNiveau(final String valeur) {
        this.niveau = valeur;
    }

    /**
     * @return libelle de l'entité
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libelle de l'entité
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    /**
     * @return parent de cette entité. Null si niveau le plus haut
     */
    public DirectionService getParent() {
        return parent;
    }

    /**
     * @param valeur parent de cette entité. Null si niveau le plus haut
     */
    public void setParent(final DirectionService valeur) {
        this.parent = valeur;
    }


    /**
     * @return code de la branche
     */
    public String getLibelleCourt() {
        return libelleCourt;
    }

    /**
     * @param valeur code de la branche
     */
    public void setLibelleCourt(final String valeur) {
        this.libelleCourt = valeur;
    }

    /**
     * @return possède les droits d'accès transverse en intervention
     */
    public boolean isTransverse() {
        return transverse;
    }

    /**
     * @param valeur possède les droits d'accès transverse en intervention
     */
    public void setTransverse(final boolean valeur) {
        this.transverse = valeur;
    }

    /**
     * @return identifiants des directions/services transverses
     */
    public static List getIdsTransverses() {
        return idsTransverses;
    }

    /**
     * @param valeur identifiants des directions/services transverses
     */
    public static void setIdsTransverses(final List<Integer> valeur) {
        DirectionService.idsTransverses = valeur;
    }

    /**
     * @param identifiant identifiant de la dir/service à ajouter
     */
    public static void ajouterIdTransverse(final int identifiant) {
        idsTransverses.add(identifiant);
    }

    /**
     * @return liste des identifiants de DS sur lesquelles la direction courrante à le droit d'intervention (direction au dessous dans l'arbre).
     */
    public List<Integer> listeIdsAyantIntervention() {
        return new ArrayList<>(listeIdsAuDessous());
    }

    /**
     * @return liste des identifiants de DS sur lesquelles la direction courrante à le droit de vision (direction au dessus + au dessous dans l'arbre).
     */
    public List listeIdsAyantVision() {
        List<Integer> liste = new ArrayList<>();
        liste.addAll(listeIdsAyantIntervention());
        liste.addAll(listeIdsAuDessus());
        return liste;
    }

}
