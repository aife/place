package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * Classe critère utilisée pour la recherche des logo d'organisme
 */
public class LogoOrganismeCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * sérialiseur.
     */
    private static final long serialVersionUID = 1L;

    /**
     * L'identifiant de l'organisme auquel appartient le logo. Il est null pour la plateforme de
     * mono-organisme
     */
    private Integer idOrganisme;

    private Integer id;

    @Override
    public String toHQL() {
        StringBuffer requeteHQL = corpsRequete();

        if (proprieteTriee != null) {
            requeteHQL.append(" order by logoOrganisme.");
            requeteHQL.append(proprieteTriee);
            if (triCroissant) {
                requeteHQL.append(" ASC ");
            } else {
                requeteHQL.append(" DESC ");
            }
        }

        return "Select logoOrganisme " + requeteHQL.toString();
    }

    @Override
    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(logoOrganisme) ");
        select.append(corpsRequete());
        return select.toString();
    }

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        Map<String, Object> parameters = getParametres();

        boolean debut = false;

        sb.append(" from EpmTLogoOrganisme logoOrganisme");

        if (idOrganisme != null && idOrganisme != 0) {
            debut = gererDebut(debut, sb);
            sb.append(" logoOrganisme.idOrganisme = :idOrganisme");
            parameters.put("idOrganisme", idOrganisme);
        }

        if (id != null && id != 0) {
            debut = gererDebut(debut, sb);
            sb.append(" logoOrganisme.id = :id");
            parameters.put("id", id);
        }

        return sb;
    }

    public void setIdOrganisme(Integer valeur) {
        this.idOrganisme = valeur;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}