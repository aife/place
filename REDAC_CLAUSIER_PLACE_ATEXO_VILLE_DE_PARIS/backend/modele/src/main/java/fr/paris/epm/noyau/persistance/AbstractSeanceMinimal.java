package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefMatinAprem;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefPouvoirAdjudicateur;

import java.util.Date;

/**
 * Objet abstrait pour la sauvegarde minimal de seances.
 * 
 * @author Rémi Villé
 * @version 2010/04/12
 */
public abstract class AbstractSeanceMinimal extends EpmTAbstractObject {
    /**
	 * Identifiant de serialisation.
	 */
	private static final long serialVersionUID = -6142203162065657104L;

	/**
     * Identifiant.
     */
    private int id;

    /**
     * Date de la seance.
     */
    private Date dateSeance;    

    /**
     * Direction associée.
     */
    private EpmTRefDirectionService direction;

    /**
     * PA associé.
     */
    private EpmTRefPouvoirAdjudicateur pouvoirAdjudicateur;

    /**
     * Libellé.
     */
    private String libelle;
    
    /**
     * Émetteur du certificat.
     */
    private EpmTRefMatinAprem matinAprem;
    
    /**
     * Indique si la séance est visible dans les moteurs de recherche.
     * Utilisé à false pour les sécances cachée lié à la gestion des envois de dossier par réunion.
     * True dans tous les autres cas.
     */
    private boolean visibleDansRecherche = true;
   
    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }    

    /**
     * @return date de la séance
     */
    public Date getDateSeance() {
        return dateSeance;
    }

    /**
     * @param valeur date de la séance
     */
    public void setDateSeance(final Date valeur) {
        this.dateSeance = valeur;
    }

    /**
     * @return direction associée
     */
    public EpmTRefDirectionService getDirection() {
        return direction;
    }

    /**
     * @param valeur direction associée
     */
    public void setDirection(final EpmTRefDirectionService valeur) {
        this.direction = valeur;
    }

    /**
     * @return PA associé
     */
    public EpmTRefPouvoirAdjudicateur getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur PA associé
     */
    public void setPouvoirAdjudicateur(final
            EpmTRefPouvoirAdjudicateur valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    /**
     * @return libellé
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }
    
    /**
     * @return séance du matin ou après-midi ou ...
     */
    public EpmTRefMatinAprem getMatinAprem() {
        return matinAprem;
    }

    /**
     * @param valeur séance du matin ou après-midi ou ...
     */
    public void setMatinAprem(final EpmTRefMatinAprem valeur) {
        this.matinAprem = valeur;
    }
    
    /**
     * @return Indique si la séance est visible dans les moteurs de recherche.
     * Utilisé à false pour les sécances cachée lié à la gestion des envois de dossier par réunion.
     * True dans tous les autres cas.
     */
    public boolean isVisibleDansRecherche() {
        return visibleDansRecherche;
    }

    /**
     * @param valeur Indique si la séance est visible dans les moteurs de recherche.
     * Utilisé à false pour les sécances cachée lié à la gestion des envois de dossier par réunion.
     * True dans tous les autres cas.
     */
    public void setVisibleDansRecherche(final boolean valeur) {
        this.visibleDansRecherche = valeur;
    }
    
	/**
	 * Respecter la règle Sonar: on doit override equals() et hashCode() en même temps.
	 */
    @Override
    public boolean equals(Object obj) {
    	return super.equals(obj);
    }
    
    /**
     * Paire utilisée: courriel, reunions
     * @return code de hachage
     */
    public int hashCode() {
        int res = super.hashCode();
        if (libelle != null) {
            res = Constantes.PREMIER * res + libelle.hashCode();
        }

        return res;
    }
}
