/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefVariation;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate du Bloc Unitaire de Données Forme de Prix, Prix Fixe.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBudFormePrixPf extends EpmTBudFormePrix {

    // Propriétés

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Estimaton interne HT.
     */
    protected Double pfEstimationHt;

    /**
     * Estimation interne TTC.
     */
    protected Double pfEstimationTtc;

    /**
     * Date de valeur.
     */
    protected Calendar pfDateValeur;

    /**
     * Collection d'objets {
     * @link EpmTRefVariations } associés.
     */
    protected Set<EpmTRefVariation> pfEpmTRefVariations = new HashSet(0);

    // Constructeurs

    /**
     * Constructeur vide pour Hibernate.
     */
    public EpmTBudFormePrixPf() {
        super();
    }

    /**
     * @param estimHt estimaton interne HT
     * @param estimTtc estimaton interne TTC
     * @param dateValeur date de valeur
     * @param variations ensemble de {
     * @link EpmTRefVariations } associés
     */
    public EpmTBudFormePrixPf(final Double estimHt, final Double estimTtc,
            final GregorianCalendar dateValeur, final Set variations) {
        super();
        this.pfEstimationHt = estimHt;
        this.pfEstimationTtc = estimTtc;
        this.pfDateValeur = dateValeur;
        this.pfEpmTRefVariations = variations;
    }

    /**
     * @return date de valeur
     */
    public Calendar getPfDateValeur() {
        return pfDateValeur;
    }

    /**
     * @param pfdv date de valeur
     */
    public void setPfDateValeur(final Calendar pfdv) {
        this.pfDateValeur = pfdv;
    }

    /**
     * @return estimaton interne HT
     */
    public Double getPfEstimationHt() {
        return pfEstimationHt;
    }

    /**
     * @param pfeht estimaton interne HT
     */
    public void setPfEstimationHt(final Double pfeht) {
        this.pfEstimationHt = pfeht;
    }

    /**
     * @return estimaton interne TTC
     */
    public Double getPfEstimationTtc() {
        return pfEstimationTtc;
    }

    /**
     * @param pfettc estimaton interne TTC
     */
    public void setPfEstimationTtc(final Double pfettc) {
        this.pfEstimationTtc = pfettc;
    }

    /**
     * @return collection de {
     * @link EpmTRefVariations } associés.
     */
    public Set<EpmTRefVariation> getPfEpmTRefVariations() {
        return pfEpmTRefVariations;
    }

    /**
     * @param variations Collection de {
     * @link EpmTRefVariations } associés.
     */
    public void setPfEpmTRefVariations(final Set<EpmTRefVariation> variations) {
        this.pfEpmTRefVariations = variations;
    }

    /**
     * @return forme de prix
     */
    public final String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nestimation interne HT: ");
        res.append(pfEstimationHt);
        res.append("\nestimation interne TTC: ");
        res.append(pfEstimationTtc);
        res.append("\ndate de valeur: ");
        res.append(((null == pfDateValeur) ? "non définie" : pfDateValeur
                        .getTime().toString()));
        res.append("\nPF variations associées: ");
        res.append(pfEpmTRefVariations);
        return res.toString();
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTBudFormePrixPf formePrix = new EpmTBudFormePrixPf();
        formePrix.setPfEstimationHt(pfEstimationHt);
        formePrix.setPfEstimationTtc(pfEstimationTtc);
        formePrix.setPfDateValeur(pfDateValeur);
        if (pfEpmTRefVariations != null) {
            Set set = new HashSet();
            set.addAll(pfEpmTRefVariations);
            formePrix.setPfEpmTRefVariations(set);
        }        
        return formePrix;
    }
}
