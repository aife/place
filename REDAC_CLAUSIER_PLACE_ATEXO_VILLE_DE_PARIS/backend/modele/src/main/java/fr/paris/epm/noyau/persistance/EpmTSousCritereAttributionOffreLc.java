package fr.paris.epm.noyau.persistance;

/**
 * Sous critère d'attribution, utilisés dans le formulaire amont.
 * @author Rebeca Dantas
 *
 */
public class EpmTSousCritereAttributionOffreLc extends EpmTAbstractObject implements Comparable<EpmTSousCritereAttributionOffreLc>, Cloneable {

	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identifiant
	 */
    private int id;
	
	/**
	 * Enoncé du sous critère
	 */
	private String enonce;
	
	/**
	 * Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	private Double ponderation;
	
	/**
	 * Le critère d'attribution associé au sous critère
	 */
	private EpmtCritereAttributionOffreLc critereAttribution;
	
	/**
	 * Utilisée pour la partie analyse des offres : la note de l'offre saisi au niveau du sous critère
	 */
	private Double note;
	
	/**
	 * Utilisée pour la partie analyse des offres : note pondéré calculé par rapport à la note saisie par l'utilisateur et la
     * pondération indiquée dans le sous critere
	 */
	private Double notePonderee;

	/**
	 * Commentaire de l'offre du sous critère
	 */
	private String commentaire;
	
	/**
	 * L'ordre d'ajout du sous critère
	 */
	private int ordre;
	
	/**
	 * 
	 * @return l'identifiant de sous scritere
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id L'identifiant du sous critère
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return Enoncé du sous critère
	 */
	public String getEnonce() {
		return enonce;
	}

	/**
	 * 
	 * @param enonce Enoncé du sous critère
	 */
	public void setEnonce(final String valeur) {
		this.enonce = valeur;
	}

	/**
	 * 
	 * @return Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	public Double getPonderation() {
		return ponderation;
	}

	/**
	 * 
	 * @param ponderation Pondération du sous critère : somme de tous les ponderations des sous
	 * critères d'un critère doit êtrre inférieur ou égal à 100% ou à la
	 * pondération de son crtitère
	 */
	public void setPonderation(final Double valeur) {
		this.ponderation = valeur;
	}

	/**
	 * 
	 * @return  le critère d'attribution associè au sous critère
	 */
	public EpmtCritereAttributionOffreLc getCritereAttribution() {
		return critereAttribution;
	}

	/**
	 * 
	 * @param valeur le critère d'attribution associè au sous critère
	 */
	public void setCritereAttribution(
			EpmtCritereAttributionOffreLc valeur) {
		this.critereAttribution = valeur;
	}

	public Double getNote() {
		return note;
	}

	public void setNote(final Double valeur) {
		this.note = valeur;
	}

	public Double getNotePonderee() {
		return notePonderee;
	}

	public void setNotePonderee(final Double valeur) {
		this.notePonderee = valeur;
	}
	
	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(final int valeur) {
		this.ordre = valeur;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String valeur) {
		this.commentaire = valeur;
	}

	@Override
	public int compareTo(EpmTSousCritereAttributionOffreLc object) {
        if (object == null) {
            return -1;
        } else {
        	EpmTSousCritereAttributionOffreLc obj = (EpmTSousCritereAttributionOffreLc) object;
            return (this.getOrdre() - obj.getId());

        }
	}	
}
