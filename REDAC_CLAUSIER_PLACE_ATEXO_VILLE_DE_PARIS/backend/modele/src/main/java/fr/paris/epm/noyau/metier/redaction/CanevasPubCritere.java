package fr.paris.epm.noyau.metier.redaction;

public class CanevasPubCritere extends CanevasCritere {

    private Integer idPublication;

    public CanevasPubCritere(String plateformeUuid) {
        super(plateformeUuid);
    }

    public CanevasPubCritere() {
        super();
    }

    @Override
    public StringBuffer corpsRequete() {
        // assemblage de la requête
        StringBuffer sb = super.corpsRequete();

        if (idPublication != null) {
            sb.append(" AND ").append(findByIdPublication());
            getParametres().put("idPublication", idPublication);
        }

        return sb;
    }

    @Override
    protected String findByIdCanevas() {
        return " AND canevas.idCanevas in (:listIds)";
    }

    /*
        il faut redéfinir cette méthode dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVClause.epmTClausePub.idPublication
     */
    protected String findByIdPublication() {
        return "canevas.idPublication = :idPublication";
    }

    @Override
    protected String getTableDeRecherche() {
        return " EpmTCanevasPub ";
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

}
