package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeAttributaire de la table "epm__t_ref_type_attributaire"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeAttributaire extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_ENTREPRISE_GENERALE = 1;
    public static final int ID_CONJOINT = 2;
    public static final int ID_SOLIDAIRE = 3;

    public static final String ENTREPRISE_GENERALE = "Entreprise générale";
    public static final String CONJOINT = "Groupement conjoint";
    public static final String SOLIDAIRE = "Groupement solidaire";

}