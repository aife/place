package fr.paris.epm.noyau.persistance;


/**
 * POJO hibernate - utilisé pour enregistrer le nombre du marchés attribués par
 * chaque pouvoir adjudicateur dans le cas numéro de chrono annuel par pouvoir adjudicateur
 * @author GAO Xuesong
 */
public class EpmTCompteurMarchePouvoirAdjudicateur extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808544L;

    /**
     * clé primaire
     */
    private Integer id;
    
    /**
     * id de Pouvoir Adjudicateur qui possède le chrono de ses marchés
     */
    private Integer idPouvoirAdjudicateur;

    /**
     * le nombre du marché 
     */
    private long chronoMarche;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return the chronoMarche
     */
    public long getChronoMarche() {
        return chronoMarche;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    /**
     * @param chronoMarche the chronoMarche to set
     */
    public void setChronoMarche(final long valeur) {
        this.chronoMarche = valeur;
    }

    /**
     * @return the idPouvoirAdjudicateur
     */
    public Integer getIdPouvoirAdjudicateur() {
        return idPouvoirAdjudicateur;
    }

    /**
     * @param idPouvoirAdjudicateur the idPouvoirAdjudicateur to set
     */
    public void setIdPouvoirAdjudicateur(final Integer valeur) {
        this.idPouvoirAdjudicateur = valeur;
    }

    
    
    
}
