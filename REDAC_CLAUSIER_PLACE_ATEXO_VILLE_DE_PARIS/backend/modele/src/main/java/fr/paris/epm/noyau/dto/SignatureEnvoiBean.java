package fr.paris.epm.noyau.dto;

import java.io.Serializable;
import java.util.List;

public class SignatureEnvoiBean implements Serializable {
    String hash;
    List<String> hashes;
    String idFileSystem;
    String mpeFilePath;
    String pdfBase64;
    String SignatureBase64;
    String signatureType;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<String> getHashes() {
        return hashes;
    }

    public void setHashes(List<String> hashes) {
        this.hashes = hashes;
    }

    public String getIdFileSystem() {
        return idFileSystem;
    }

    public void setIdFileSystem(String idFileSystem) {
        this.idFileSystem = idFileSystem;
    }

    public String getMpeFilePath() {
        return mpeFilePath;
    }

    public void setMpeFilePath(String mpeFilePath) {
        this.mpeFilePath = mpeFilePath;
    }

    public String getPdfBase64() {
        return pdfBase64;
    }

    public void setPdfBase64(String pdfBase64) {
        this.pdfBase64 = pdfBase64;
    }

    public String getSignatureBase64() {
        return SignatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        SignatureBase64 = signatureBase64;
    }

    public String getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(String signatureType) {
        this.signatureType = signatureType;
    }
}
