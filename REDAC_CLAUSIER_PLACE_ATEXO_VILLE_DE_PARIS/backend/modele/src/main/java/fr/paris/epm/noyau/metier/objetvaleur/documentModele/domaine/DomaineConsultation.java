package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefChoixFormePrix;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le domaine
 * consultation.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineConsultation implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Sous-domaine caracteristiques (intitule, direction/service de la consultation).
     */
    private DomaineCaracteristiques caracteristiques;

    /**
     * Sous-domaine informations principales (numero de la consultation).
     */
    private DomaineInformationsPrincipales informationsPrincipales;

    /**
     * Sous-domaine lot (affecte si le document est genere a partir d'un lot).
     */
    private DomaineLot lot;

    /**
     * Liste de sous-domaine lot (affecte si le document est genere a partir d'une consultation).
     */
    private List<DomaineLot> listeLot;

    /**
     * Liste de lots techniques associés à la consultation
     */
    private List<DomaineLotTechique> listeLotTechnique;

    private DomaineDonneesComplementaires donneesComplementaires;

    private List<DomaineTranche> tranches;

    private DomaineFormeDePrix formeDePrix;

    private DomaineClauseSociale clauseSociale;

    private DomaineClauseEnvironnementale clauseEnvironnementale;

    /**
     * Informations concernant la reconduction de la consultation
     */
    private DomaineReconduction reconduction;

    /**
     * Informations concernant les variantes autorisées et obligatoires de la  consultation
     */
    private DomaineVariantes variantes;

    private DomainePairesTypesComplexes pairesTypesComplexes;

    private DomainePairesTypes pairesTypes;

    private String typeCriteresAttribution;

    private List<DomaineCritereConsultation> listeCriteres;

    private DomaineDureeMarche dureeMarche;

    private boolean critereParLot = false;

    private List<DomaineContrat> contrats;
    private List<DomaineAvenant> avenants;

    /**
     * Nombre de candidats fixe.
     */
    private Integer nombreCandidatsFixe;

    /**
     * Nombre de candidats minimum.
     */
    private Integer nombreCandidatsMin;

    /**
     * Nombre de candidats maximum.
     */
    private Integer nombreCandidatsMax;

    /**
     * @return Sous-domaine caracteristiques (intitule, direction/service de la consultation).
     */
    public final DomaineCaracteristiques getCaracteristiques() {
        return caracteristiques;
    }

    /**
     * @param valeur Sous-domaine caracteristiques (intitule, direction/service de la consultation).
     */
    public final void setCaracteristiques(final DomaineCaracteristiques valeur) {
        this.caracteristiques = valeur;
    }

    /**
     * @return Sous-domaine informations principales (numero de la consultation).
     */
    public final DomaineInformationsPrincipales getInformationsPrincipales() {
        return informationsPrincipales;
    }

    /**
     * @param valeur Sous-domaine informations principales (numero de la consultation).
     */
    public final void setInformationsPrincipales(final DomaineInformationsPrincipales valeur) {
        this.informationsPrincipales = valeur;
    }

    /**
     * @return Sous-domaine lot (affecte si le document est genere a partir d'un lot).
     */
    public final DomaineLot getLot() {
        return lot;
    }

    /**
     * @param valeur Sous-domaine lot (affecte si le document est genere a partir d'un lot).
     */
    public final void setLot(DomaineLot valeur) {
        this.lot = valeur;
    }

    /**
     * @return liste de sous-domaine lot (affecte si le document est genere a partir d'une consultation).
     */
    public final List<DomaineLot> getListeLot() {
        return listeLot;
    }

    /**
     * @param valeur liste de sous-domaine lot (affecte si le document est genere a partir d'une consultation).
     */
    public final void setListeLot(final List<DomaineLot> valeur) {
        this.listeLot = valeur;
    }


    public final DomaineDonneesComplementaires getDonneesComplementaires() {
        return donneesComplementaires;
    }

    public final void setDonneesComplementaires(final DomaineDonneesComplementaires valeur) {
        this.donneesComplementaires = valeur;
    }

    public final List<DomaineTranche> getTranches() {
        return tranches;
    }

    public final void setTranches(final List<DomaineTranche> valeur) {
        this.tranches = valeur;
    }

    public final DomaineFormeDePrix getFormeDePrix() {
        return formeDePrix;
    }

    public final void setFormeDePrix(final DomaineFormeDePrix valeur) {
        this.formeDePrix = valeur;
    }

    /**
     * @return the pairesTypesComplexes
     */
    public final DomainePairesTypesComplexes getPairesTypesComplexes() {
        return pairesTypesComplexes;
    }

    /**
     * @param valeur the pairesTypesComplexes to set
     */
    public final void setPairesTypesComplexes(final DomainePairesTypesComplexes valeur) {
        this.pairesTypesComplexes = valeur;
    }

    /**
     * @return the pairesTypes
     */
    public final DomainePairesTypes getPairesTypes() {
        return pairesTypes;
    }

    /**
     * @param valeur the pairesTypes to set
     */
    public final void setPairesTypes(final DomainePairesTypes valeur) {
        this.pairesTypes = valeur;
    }

    /**
     * @return Liste des lots techniques associés à la consultation
     */
    public final List<DomaineLotTechique> getListeLotTechnique() {
        Collections.sort(listeLotTechnique);
        return listeLotTechnique;
    }

    /**
     * @param valeur des lots techniques associés à la consultation
     */
    public final void setListeLotTechnique(final List<DomaineLotTechique> valeur) {
        this.listeLotTechnique = valeur;
    }

    /**
     * @return les informations concernant la reconduction de la consultation
     */
    public final DomaineReconduction getReconduction() {
        return reconduction;
    }

    /**
     * @param valeur les informations concernant la reconduction de la consultation
     */
    public final void setReconduction(final DomaineReconduction valeur) {
        this.reconduction = valeur;
    }

    /**
     * @return les informations concernant les variantes obligatoires et/ou autorisées
     */
    public final DomaineVariantes getVariantes() {
        return variantes;
    }

    /**
     * @param valeur les informations concernant les variantes obligatoires et/ou autorisées
     */
    public final void setVariantes(final DomaineVariantes valeur) {
        this.variantes = valeur;
    }

    public DomaineClauseSociale getClauseSociale() {
        return clauseSociale;
    }

    public void setClauseSociale(DomaineClauseSociale clauseSociale) {
        this.clauseSociale = clauseSociale;
    }

    public final boolean isVariantesAutorisees() {
        if (listeLot == null || listeLot.isEmpty())
            return getVariantes().isAutorisee();

        if (lot != null)
            return lot.getVariantes().isAutorisee();

        for (DomaineLot lot : listeLot)
            if (lot.getVariantes().isAutorisee())
                return true; // contient ou moins un lot avec variante autorisee
        return false;
    }

    public final boolean isVariantesObligatoires() {
        if (listeLot == null || listeLot.isEmpty())
            return getVariantes().isObligatoire();

        if (lot != null)
            return lot.getVariantes().isObligatoire();

        for (DomaineLot lot : listeLot)
            if (lot.getVariantes().isObligatoire())
                return true; // contient ou moins un lot avec variante obligatoire
        return false;
    }

    /**
     * contient ou moins un(e) consultation/lot/tranche avec Forme de prix forfaitaire
     */
    public final boolean isFormeDePrixPF() {
        return isFormeDePrix(EpmTRefChoixFormePrix.FORFAITAIRE);
    }

    /**
     * contient ou moins un(e) consultation/lot/tranche avec Forme de prix mixte
     */
    public final boolean isFormeDePrixPM() {
        return isFormeDePrix(EpmTRefChoixFormePrix.MIXTE);
    }

    /**
     * contient ou moins un(e) consultation/lot/tranche avec Forme de prix unitaire
     */
    public final boolean isFormeDePrixPU() {
        return isFormeDePrix(EpmTRefChoixFormePrix.UNITAIRE);
    }

    public final boolean isFormeDePrix(int fdp) {
        boolean tranchesPresent = isTranchesPresent();
        if (listeLot == null || listeLot.isEmpty()) {
            if (tranchesPresent) {
                for (DomaineTranche tranche : getTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() == fdp) {
                        return true;
                    }
                }
            } else {
                if (getFormeDePrix().getIdFormePrix()!= null &&  getFormeDePrix().getIdFormePrix() == fdp) {
                    return true;
                }
            }
            return false;
        }
        if (lot != null) {
            if (lot.getListeTranches() != null && lot.getListeTranches().size() > 0) {
                for (DomaineTranche tranche : lot.getListeTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() == fdp) {
                        return true;
                    }
                }
            } else {
                if (lot.getFormeDePrix().getIdFormePrix() == fdp) {
                    return true;
                }
            }
            return false;
        }
        for (DomaineLot lot : listeLot) {
            if (!(lot.getListeTranches() != null && lot.getListeTranches().size() > 0)) {
                if (lot.getFormeDePrix().getIdFormePrix() == fdp) {
                    return true; // contient au moins un lot avec Forme de prix forfaitaire
                }
            } else {
                for (DomaineTranche tranche : lot.getListeTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() == fdp) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * contient au moins une tranche dans un lot ou consultation
     */
    public final boolean isTranchesPresent() {
        if (listeLot == null || listeLot.isEmpty()) {
            return (getTranches() != null && getTranches().size() > 0);
        }

        if (lot != null)
            return (lot.getListeTranches() != null && lot.getListeTranches().size() > 0);

        for (DomaineLot lot : listeLot)
            if (lot.getListeTranches().size() > 0)
                return true;
        return false;
    }

    /**
     * contient au moins une tranche dans un lot ou consultation
     */
    public final boolean isTranchesLotTechniquePresent() {
        if (listeLot == null || listeLot.isEmpty()) {
            return (getTranches() != null && getTranches().size() > 0) && (getListeLotTechnique() != null && getListeLotTechnique().size() > 0);
        }

        if (lot != null) {
            return (lot.getListeTranches() != null && lot.getListeTranches().size() > 0) && (lot.getListeLotTechnique() != null && lot.getListeLotTechnique().size() > 0);
        }

        for (DomaineLot lot : listeLot) {
            if (lot.getListeTranches() != null && lot.getListeTranches().size() > 0 && lot.getListeLotTechnique() != null && lot.getListeLotTechnique().size() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * contient au moins une tranche dans un lot ou consultation ou tranche
     */
    public final boolean isBonCommandePresent() {
        boolean tranchesPresent = isTranchesPresent();
        if (listeLot == null || listeLot.isEmpty()) {
            if (tranchesPresent) {
                for (DomaineTranche tranche : getTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() != EpmTRefChoixFormePrix.FORFAITAIRE && tranche.getFormeDePrix().getIdBonQuantite() == 1) {
                        return true;
                    }
                }
            } else {
                return getFormeDePrix().getIdBonQuantite() == 1;
            }
            return false;
        }

        if (lot != null) {
            if (lot.getListeTranches() != null && lot.getListeTranches().size() > 0) {
                for (DomaineTranche tranche : lot.getListeTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() != EpmTRefChoixFormePrix.FORFAITAIRE && tranche.getFormeDePrix().getIdBonQuantite() == 1) {
                        return true;
                    }
                }
            } else {
                return lot.getFormeDePrix().getIdBonQuantite() == 1;
            }
            return false;
        }

        for (DomaineLot lot : listeLot) {
            if (lot.getListeTranches() != null && lot.getListeTranches().size() > 0) {
                for (DomaineTranche tranche : lot.getListeTranches()) {
                    if (tranche.getFormeDePrix().getIdFormePrix() != EpmTRefChoixFormePrix.FORFAITAIRE && tranche.getFormeDePrix().getIdBonQuantite() == 1) {
                        return true;
                    }
                }
            } else {
                if (lot.getFormeDePrix().getIdFormePrix() != EpmTRefChoixFormePrix.FORFAITAIRE && lot.getFormeDePrix().getIdBonQuantite() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * contient au moins un lot technique dans un lot ou consultation
     */
    public final boolean isLotTechniquePresent() {
        if (listeLot == null || listeLot.isEmpty()) {
            return (getListeLotTechnique() != null && getListeLotTechnique().size() > 0);
        }

        if (lot != null) {
            return (lot.getListeLotTechnique() != null && lot.getListeLotTechnique().size() > 0);
        }

        for (DomaineLot lot : listeLot) {
            if (lot.getListeLotTechnique() != null && lot.getListeLotTechnique().size() > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * contient au moins une clauseSociale dans un lot ou consultation
     */
    public final boolean isClauseSocialePresent() {
        if (listeLot == null || listeLot.isEmpty()) {
            return (getClauseSociale().getClauseSociale() != null && getClauseSociale().getClauseSociale());
        }

        if (lot != null) {
            return (lot.getClauseSociale().getClauseSociale() != null && lot.getClauseSociale().getClauseSociale());
        }

        for (DomaineLot lot : listeLot)
            if (lot.getClauseSociale().getClauseSociale() != null && lot.getClauseSociale().getClauseSociale())
                return true;

        return false;
    }

    /**
     * contient au moins une clauseSociale dans un lot ou consultation
     */
    public final boolean isClauseEnvironnementalePresent() {
        if (listeLot == null || listeLot.isEmpty()) {
            return (getClauseEnvironnementale().getClauseEnvironnementale() != null && getClauseEnvironnementale().getClauseEnvironnementale());
        }

        if (lot != null) {
            return (lot.getClauseEnvironnementale().getClauseEnvironnementale() != null && lot.getClauseEnvironnementale().getClauseEnvironnementale());
        }

        for (DomaineLot lot : listeLot) {
            if (lot.getClauseEnvironnementale().getClauseEnvironnementale() != null && lot.getClauseEnvironnementale().getClauseEnvironnementale()) {
                return true;
            }
        }

        return false;
    }

    public final boolean hasPrestationsSupplementairesEventuelles() {
        if (listeLot == null || listeLot.isEmpty()) {
            return getVariantes().getDescription() != null && !getVariantes().getDescription().isEmpty();
        }

        if (lot != null) {
            return lot.getVariantes().getDescription() != null && !lot.getVariantes().getDescription().isEmpty();
        }

        for (DomaineLot lot : listeLot) {
            if (lot.getVariantes().getDescription() != null && !lot.getVariantes().getDescription().isEmpty()) {
                return true; // contient ou moins un lot avec prestations supplémentaires éventuelles
            }
        }
        return false;
    }

    public final String getTypeCriteresAttribution() {
        return typeCriteresAttribution;
    }

    public final void setTypeCriteresAttribution(final String valeur) {
        this.typeCriteresAttribution = valeur;
    }

    public final List<DomaineCritereConsultation> getListeCriteres() {
        return listeCriteres;
    }

    public final void setListeCriteres(final List<DomaineCritereConsultation> valeur) {
        this.listeCriteres = valeur;
    }

    public final DomaineDureeMarche getDureeMarche() {
        return dureeMarche;
    }

    public void setDureeMarche(final DomaineDureeMarche dureeMarche) {
        this.dureeMarche = dureeMarche;
    }

    public final boolean isCritereParLot() {
        return critereParLot;
    }

    public final void setCritereParLot(final boolean valeur) {
        this.critereParLot = valeur;
    }

    public final DomaineClauseEnvironnementale getClauseEnvironnementale() {
        return clauseEnvironnementale;
    }

    public void setClauseEnvironnementale(final DomaineClauseEnvironnementale clauseEnvironnementale) {
        this.clauseEnvironnementale = clauseEnvironnementale;
    }

    public final Integer getNombreCandidatsFixe() {
        return nombreCandidatsFixe;
    }

    public DomaineConsultation setNombreCandidatsFixe(final Integer nombreCandidatsFixe) {
        this.nombreCandidatsFixe = nombreCandidatsFixe;
        return this;
    }

    public final Integer getNombreCandidatsMin() {
        return nombreCandidatsMin;
    }

    public DomaineConsultation setNombreCandidatsMin(final Integer nombreCandidatsMin) {
        this.nombreCandidatsMin = nombreCandidatsMin;
        return this;
    }

    public final Integer getNombreCandidatsMax() {
        return nombreCandidatsMax;
    }

    public DomaineConsultation setNombreCandidatsMax(final Integer nombreCandidatsMax) {
        this.nombreCandidatsMax = nombreCandidatsMax;
        return this;
    }

    public String getListeNumerosLots() {
        StringBuilder sb = new StringBuilder();
        if(listeLot!=null && !listeLot.isEmpty()){
        for (DomaineLot lot : listeLot)
            sb.append(sb.length() == 0 ? lot.getReferenceLot() : ", " + lot.getReferenceLot());

       }else if(lot!=null)
           sb.append(lot.getReferenceLot());
        return sb.toString();
    }

    public String getListeIntitulesLots() {
        StringBuilder sb = new StringBuilder();
        if(listeLot!=null && !listeLot.isEmpty()){
        for (DomaineLot lot : listeLot)
            sb.append(sb.length() == 0 ? lot.getIntituleLot() : ", " + lot.getIntituleLot());

       }else if(lot!=null)
           sb.append(lot.getIntituleLot());
        return sb.toString();
    }

    public List<DomaineContrat> getContrats() {
        return contrats;
    }

    public void setContrats(List<DomaineContrat> contrats) {
        this.contrats = contrats;
    }

    public List<DomaineAvenant> getAvenants() {
        return avenants;
    }

    public void setAvenants(List<DomaineAvenant> avenants) {
        this.avenants = avenants;
    }

}
