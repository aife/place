package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * POJO hibernate Consultation.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class EpmTAvenant extends EpmTAbstractObject {
    /**
     * Marqueur de sérialization.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * idenfiant du lot {@link EpmTBudLot}.
     */
    private Integer idLot;

    /**
     * avis de la CAO.
     */
    private Boolean avisCAO = true;

    /**
     * Reference du marche.
     */
    private String referenceMarche;

    /**
     * Ensembles des titulaires de l'avenant.
     */
    private Set titulaires;

    /**
     * titulaire initiale.
     */
    private String titulaireInitial;

    private Date delavdirContLeg;

    private Date delavdirEntreeProjAlp;

    private Date delavdirVisaDf;

    private Date delavdirVoteCa;

    private Date delavdirVoteCp;

    private String delavdirNumeroProj;

    private EpmTAttribution attribution;

    /**
     * numero de marché provenant de GO.
     */
    private String numeroContractuel;

    /**
     * reference marche + reference.
     */
    private String referenceConcatenee;

    /**
     *
     */
    private String contratInitial;

    /**
     * Référentiel Pouvoir Adjudicateur.
     */
    private EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur;

    /**
     * Intitulé consultation initiale.
     */
    private String intituleConsultationInitiale;

    /**
     * Intitulé du lot.
     */
    private String intituleLot;

    /**
     * Objet de l'avenant.
     */
    private String objet;

    /**
     * Autres informations.
     */
    private String autre;

    /**
     * Partie forfaitaire.
     */
    private Double pfMn;

    /**
     * Bon de commande min.
     */
    private Double bcMin;

    /**
     * Bon de commande max.
     */
    private Double bcMax;

    /**
     * Passage en CAO.
     */
    private boolean passageCAO;

    /**
     * passage a la commission sapin.
     */
    private boolean passageSapin;

    /**
     * passage au conseil de Paris.
     */
    private boolean passageConseilParis;

    /**
     * reference de l'avenant.
     */
    private String reference;

    /**
     * Indique la derniére date de modification. Cette date est mise é jour lors de la création de la consultation & de la modification de la consultation au niveau du formulaire amont.
     */
    private Calendar dateModification;

    /**
     * Opération de travaux (é terme hérité de GO).
     */
    private String operationTravaux;

    /**
     * Autre opération.
     */
    private String autreOperation;

    /**
     * Intitulé de consultation.
     */
    private String intituleConsultation;

    /**
     * Référentiel Direction et service.
     */
    private EpmTRefDirectionService epmTRefDirectionService;

    /**
     * responsable ayant créé la consultation.
     */
    private EpmTUtilisateur responsable;

    /**
     *
     */
    private EpmTRefTypeContratAvenant typeContrat;

    /**
     * éétapes de cette consultation {@link EpmTEtapeSpec}.
     */
    private Set<EpmTEtapeSpec> etapes = new HashSet<>();

    /**
     * Statut courante de la consultation.
     */
    private EpmTRefStatutAvenant epmTRefStatut;

    private EpmTRefProcedure epmTRefProcedure;

    /**
     * Id de la consultation pour laquelle on crée l'avenant.
     */
    private Integer idConsultation;

    /**
     * Identifiant de l'organisme auquel l'avenant est rataché
     */
    private Integer idOrganisme;

    /**
     * L'identifiant du contrat (Epmtcontrat) auquel l'avenant est liée. Quand créer un avenant à partir d'un contrat, idContratInitial est identifiant de ce contrat
     */
    private Integer idContratInitial;

    /**
     * Type de passage en Commission
     */
    private EpmTRefTypeCommission typeCommission;

    /**
     * le Type de l'avenant
     */
    private EpmTRefTypeAvenant typeAvenant;

    /**
     * Date de validation de l'attribution.
     */
    private Date dateValidation;

    /**
     * Utilise uniquement le numero de consultation.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (reference != null) {
            result = Constantes.PREMIER * result + reference.hashCode();
        }
        return result;
    }

    /**
     * @param obj objet é tester
     * @see java.lang.Object#equals(java.lang.Object)
     * @return vrai si les numéros de consultation sont égaux
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTAvenant autre = (EpmTAvenant) obj;

        if (reference == null) {
            if (autre.reference != null) {
                return false;
            }
        } else if (!reference.equals(autre.reference)) {
            return false;
        }
        return true;
    }

    /**
     * @return "oui" si autre opération
     */
    public String getAutreOperation() {
        return autreOperation;
    }

    /**
     * @param valeur "oui" si autre opération
     */
    public void setAutreOperation(final String valeur) {
        this.autreOperation = valeur;
    }

    /**
     * @return référentiel Direction et service
     */
    public EpmTRefDirectionService getEpmTRefDirectionService() {
        return epmTRefDirectionService;
    }

    /**
     * @param valeur référentiel Direction et service
     */
    public void setEpmTRefDirectionService(final EpmTRefDirectionService valeur) {
        this.epmTRefDirectionService = valeur;
    }

    /**
     * @return référentiel pouvoir adjudicateur
     */
    public EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur() {
        return epmTRefPouvoirAdjudicateur;
    }

    /**
     * @param valeur référentiel pouvoir adjudicateur
     */
    public void setEpmTRefPouvoirAdjudicateur(
            final EpmTRefPouvoirAdjudicateur valeur) {
        this.epmTRefPouvoirAdjudicateur = valeur;
    }

    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return intitulé de la consultation
     */
    public String getIntituleConsultation() {
        return intituleConsultation;
    }

    /**
     * @param valeur intitulé de la consultation
     */
    public void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }

    /**
     * @return objet de la consultation
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param valeur objet de la consultation
     */
    public void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return opération de travaux (hérité de GO)
     */
    public String getOperationTravaux() {
        return operationTravaux;
    }

    /**
     * @param valeur opération de travaux (hérité de GO)
     */
    public void setOperationTravaux(final String valeur) {
        this.operationTravaux = valeur;
    }

    /**
     * @return {@link EpmTEtapeSpec}
     */
    public Set<EpmTEtapeSpec> getEtapes() {
        return etapes;
    }

    /**
     * @param valeur {@link EpmTEtapeSpec}
     */
    public void setEtapes(final Set<EpmTEtapeSpec> valeur) {
        this.etapes = valeur;
    }

    /**
     * @return date de dernière modification du formulaire amont
     */
    public Calendar getDateModification() {
        return dateModification;
    }

    /**
     * @param valeur date de dernière modification du formulaire amont
     */
    public void setDateModification(final Calendar valeur) {
        this.dateModification = valeur;
    }

    /**
     * @return
     */
    public EpmTRefStatutAvenant getEpmTRefStatut() {
        return epmTRefStatut;
    }

    /**
     * @param valeur
     */
    public void setEpmTRefStatut(final EpmTRefStatutAvenant valeur) {
        this.epmTRefStatut = valeur;
    }

    /**
     * @return
     */
    public String getContratInitial() {
        return contratInitial;
    }

    /**
     * @param contratInitial
     */
    public void setContratInitial(final String valeur) {
        this.contratInitial = valeur;
    }

    /**
     * @return
     */
    public String getIntituleConsultationInitiale() {
        return intituleConsultationInitiale;
    }

    /**
     * @param intituleConsultationInitiale
     */
    public void setIntituleConsultationInitiale(final String valeur) {
        this.intituleConsultationInitiale = valeur;
    }

    /**
     * @return
     */
    public String getIntituleLot() {
        return intituleLot;
    }

    /**
     * @param intituleLot
     */
    public void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return
     */
    public String getAutre() {
        return autre;
    }

    /**
     * @param autre
     */
    public void setAutre(final String valeur) {
        this.autre = valeur;
    }

    /**
     * @return
     */
    public Double getPfMn() {
        return pfMn;
    }

    /**
     * @param pfMn
     */
    public void setPfMn(final Double valeur) {
        this.pfMn = valeur;
    }

    /**
     * @return
     */
    public Double getBcMin() {
        return bcMin;
    }

    /**
     * @param bcMin
     */
    public void setBcMin(final Double valeur) {
        this.bcMin = valeur;
    }

    /**
     * @return
     */
    public Double getBcMax() {
        return bcMax;
    }

    /**
     * @param bcMax
     */
    public void setBcMax(final Double valeur) {
        this.bcMax = valeur;
    }

    /**
     * @return
     */
    public boolean isPassageCAO() {
        return passageCAO;
    }

    /**
     * @param passageCAO
     */
    public void setPassageCAO(final boolean valeur) {
        this.passageCAO = valeur;
    }

    /**
     * @return
     */
    public boolean isPassageConseilParis() {
        return passageConseilParis;
    }

    /**
     * @param passageConseilParis
     */
    public void setPassageConseilParis(final boolean valeur) {
        this.passageConseilParis = valeur;
    }

    /**
     * @return reference de l'avenant.
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param valeur reference de l'avenant
     */
    public void setReference(final String valeur) {
        this.reference = valeur;
    }

    /**
     * @return responsable de l'avenant
     */
    public EpmTUtilisateur getResponsable() {
        return responsable;
    }

    /**
     * @param valeur responsable de l'avenant
     */
    public void setResponsable(final EpmTUtilisateur valeur) {
        this.responsable = valeur;
    }

    /**
     * @return
     */
    public EpmTRefTypeContratAvenant getTypeContrat() {
        return typeContrat;
    }

    /**
     * @param valeur
     */
    public void setTypeContrat(final EpmTRefTypeContratAvenant valeur) {
        this.typeContrat = valeur;
    }

    /**
     * @return reference du marche.
     */
    public String getReferenceMarche() {
        return referenceMarche;
    }

    /**
     * @param valeur reference du marche.
     */
    public void setReferenceMarche(final String valeur) {
        this.referenceMarche = valeur;
    }

    /**
     * @return passage en commission Sapin.
     */
    public boolean isPassageSapin() {
        return passageSapin;
    }

    /**
     * @param valeur passage en commission Sapin
     */
    public void setPassageSapin(final boolean valeur) {
        this.passageSapin = valeur;
    }

    /**
     * @return
     */
    public Set getTitulaires() {
        return titulaires;
    }

    /**
     * @param valeur
     */
    public void setTitulaires(final Set valeur) {
        this.titulaires = valeur;
    }

    /**
     * @return
     */
    public String getTitulaireInitial() {
        return titulaireInitial;
    }

    /**
     * @param valeur
     */
    public void setTitulaireInitial(String valeur) {
        this.titulaireInitial = valeur;
    }

    /**
     * @return
     */
    public Date getDelavdirContLeg() {
        return delavdirContLeg;
    }

    /**
     * @param valeur
     */
    public void setDelavdirContLeg(Date valeur) {
        this.delavdirContLeg = valeur;
    }

    /**
     * @return
     */
    public Date getDelavdirEntreeProjAlp() {
        return delavdirEntreeProjAlp;
    }

    /**
     * @param valeur
     */
    public void setDelavdirEntreeProjAlp(Date valeur) {
        this.delavdirEntreeProjAlp = valeur;
    }

    /**
     * @return
     */
    public Date getDelavdirVisaDf() {
        return delavdirVisaDf;
    }

    /**
     * @param valeur
     */
    public void setDelavdirVisaDf(Date valeur) {
        this.delavdirVisaDf = valeur;
    }

    /**
     * @return
     */
    public Date getDelavdirVoteCa() {
        return delavdirVoteCa;
    }

    /**
     * @param valeur
     */
    public void setDelavdirVoteCa(Date valeur) {
        this.delavdirVoteCa = valeur;
    }

    /**
     * @return
     */
    public Date getDelavdirVoteCp() {
        return delavdirVoteCp;
    }

    /**
     * @param valeur
     */
    public void setDelavdirVoteCp(Date valeur) {
        this.delavdirVoteCp = valeur;
    }

    /**
     * @return
     */
    public String getDelavdirNumeroProj() {
        return delavdirNumeroProj;
    }

    /**
     * @param valeur
     */
    public void setDelavdirNumeroProj(String valeur) {
        this.delavdirNumeroProj = valeur;
    }

    /**
     * @return
     */
    public EpmTAttribution getAttribution() {
        return attribution;
    }

    /**
     * @param valeur
     */
    public void setAttribution(EpmTAttribution valeur) {
        this.attribution = valeur;
    }

    /**
     * @return
     */
    public EpmTRefProcedure getEpmTRefProcedure() {
        return epmTRefProcedure;
    }

    /**
     * @param valeur
     */
    public void setEpmTRefProcedure(EpmTRefProcedure valeur) {
        this.epmTRefProcedure = valeur;
    }

    /**
     * @return avis de la CAO favorable = true defavorable = false.
     */
    public Boolean isAvisCAO() {
        return avisCAO;
    }

    /**
     * @param valeur
     */
    public void setAvisCAO(Boolean valeur) {
        this.avisCAO = valeur;
    }

    /**
     * @return identifiant du lot
     */
    public Integer getIdLot() {
        return idLot;
    }

    /**
     * @param valeur identifiant du lot
     */
    public void setIdLot(final Integer valeur) {
        this.idLot = valeur;
    }

    /**
     * @return rference marche + reference
     */
    public String getReferenceConcatenee() {
        return referenceConcatenee;
    }

    /**
     * @param valeur reference marche + reference
     */
    public void setReferenceConcatenee(final String valeur) {
        this.referenceConcatenee = valeur;
    }

    /**
     * @return l'id de la consultation.
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param valeur l'id de la consultation.
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    public String getNumeroContractuel() {
        return numeroContractuel;
    }

    public void setNumeroContractuel(String numeroContractuel) {
        this.numeroContractuel = numeroContractuel;
    }

    /**
     * @return the idOrganisme
     */
    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

    /**
     * @return the idContratInitial
     */
    public Integer getIdContratInitial() {
        return idContratInitial;
    }

    /**
     * @param idContratInitial the idContratInitial to set
     */
    public void setIdContratInitial(final Integer valeur) {
        this.idContratInitial = valeur;
    }

    public EpmTRefTypeCommission getTypeCommission() {
        return typeCommission;
    }

    public void setTypeCommission(final EpmTRefTypeCommission valeur) {
        this.typeCommission = valeur;
    }

    /**
     * @return the typeAvenant
     */
    public EpmTRefTypeAvenant getTypeAvenant() {
        return typeAvenant;
    }

    /**
     * @param typeAvenant the typeAvenant to set
     */
    public void setTypeAvenant(final EpmTRefTypeAvenant valeur) {
        this.typeAvenant = valeur;
    }

    /**
     * @return the dateValidation
     */
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * @param dateValidation the dateValidation to set
     */
    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

}
