package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient tous les domaines et/ou données de niveau 3 à être fusionné dans un document du module exécution.
 * Ce domaine contient des données de l'écran Caracteristiques du Contrat du module exécution
 * 
 * @author Rebeca Dantas
 */
public class DomaineCaracteristiques implements Serializable {
	
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Type de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String typeContrat;
	
	/**
	 * Numéro de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String numeroContrat;
	
	/**
	 * L'objet du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String objetContrat;
	
	/**
	 * Contractant public du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String contractantPublic;
	
	/**
	 * Direction / Service(s) bénéficiaire du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String directionBeneficiaire;
	
	/**
	 * Montant HT du contrat du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private Double montantContrat;
	
	/**
	 * Durée du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	private String dureeContrat;
    
    
	/**
	 * @return type de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final String getTypeContrat() {
		return typeContrat;
	}

	/**
	 * @param valeur : type de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setTypeContrat(final String valeur) {
		this.typeContrat = valeur;
	}

	/**
	 * @return numéro de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final String getNumeroContrat() {
		return numeroContrat;
	}

	/**
	 * @param valeur : numéro de contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setNumeroContrat(final String valeur) {
		this.numeroContrat = valeur;
	}

	/**
	 * @return contractant public du contrat provenant de l'écran Caracteristiques du contrat du module exécution

	 */
	public final String getContractantPublic() {
		return contractantPublic;
	}

	/**
	 * 
	 * @param valeur : contractant public du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setContractantPublic(final String valeur) {
		this.contractantPublic = valeur;
	}

	/**
	 * 
	 * @return direction / Service(s) bénéficiaire du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final String getDirectionBeneficiaire() {
		return directionBeneficiaire;
	}

	/**
	 * 
	 * @param valeur : direction / Service(s) bénéficiaire du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setDirectionBeneficiaire(final String valeur) {
		this.directionBeneficiaire = valeur;
	}

	/**
	 * 
	 * @return montant HT du contrat du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final Double getMontantContrat() {
		return montantContrat;
	}

	/**
	 * 
	 * @param valeur : Montant HT du contrat du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setMontantContrat(final Double valeur) {
		this.montantContrat = valeur;
	}

	/**
	 * 
	 * @return Durée du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final String getDureeContrat() {
		return dureeContrat;
	}

	/**
	 * 
	 * @param valeur : Durée du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setDureeContrat(final String valeur) {
		this.dureeContrat = valeur;
	}
	
	/**
	 * 
	 * @return l'objet du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final String getObjetContrat() {
		return objetContrat;
	}

	/**
	 * 
	 * @param l'objet du contrat provenant de l'écran Caracteristiques du contrat du module exécution
	 */
	public final void setObjetContrat(final String valeur) {
		this.objetContrat = valeur;
	}

}
