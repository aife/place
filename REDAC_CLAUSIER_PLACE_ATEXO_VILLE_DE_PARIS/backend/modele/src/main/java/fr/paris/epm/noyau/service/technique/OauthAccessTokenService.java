package fr.paris.epm.noyau.service.technique;

import org.springframework.stereotype.Service;

@Service
public interface OauthAccessTokenService {

	/**
	 * Permet de récuperer l'access token
	 * @return la réponse d'authentification
	 */
	Credentials retrieve();

}
