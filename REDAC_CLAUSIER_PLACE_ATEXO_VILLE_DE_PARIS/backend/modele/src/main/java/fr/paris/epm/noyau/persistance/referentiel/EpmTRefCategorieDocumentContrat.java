package fr.paris.epm.noyau.persistance.referentiel;

import java.util.Set;

/**
 * POJO EpmTRefCategorieDocumentContrat de la table "epm__t_ref_categorie_document_contrat"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCategorieDocumentContrat extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_CATEGORIE_DOCUMENT_CONTRAT_PIECE_DCE = 1;
    public static final int ID_CATEGORIE_DOCUMENT_CONTRAT_DEPOT_ATTRIBUTAIRE = 2;
    public static final int ID_CATEGORIE_DOCUMENT_CONTRAT_PIECE_PUBLICITE = 3;
    public static final int ID_CATEGORIE_DOCUMENT_CONTRAT_PIECE_AUTRE = 4;
    public static final int ID_CATEGORIE_DOCUMENT_REDACTION = 5;
    public static final int ID_CATEGORIE_DOCUMENT_LIBRE = 6;
    public static final int ID_CATEGORIE_DOCUMENT_APPLICATIONS_TIERCES = 7;

    /**
     * liste de EpmTRefTypeDocumentContrat associé à ce type
     */
    private Set<EpmTRefTypeDocumentContrat> listeTypeDocumentContrat;

    public Set<EpmTRefTypeDocumentContrat> getListeTypeDocumentContrat() {
        return listeTypeDocumentContrat;
    }

    public void setListeTypeDocumentContrat(Set<EpmTRefTypeDocumentContrat> listeTypeDocumentContrat) {
        this.listeTypeDocumentContrat = listeTypeDocumentContrat;
    }

}