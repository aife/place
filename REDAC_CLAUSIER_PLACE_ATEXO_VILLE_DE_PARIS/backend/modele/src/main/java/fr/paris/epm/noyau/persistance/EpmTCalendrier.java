/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import java.util.*;

/**
 * Pojo hibernate calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTCalendrier extends EpmTAbstractObject implements Cloneable {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Identifiant de l'avenant, dans le cas d'une instance de calendrier.
     */
    private Integer avenant;

    /**
     * Identifiant du lot.
     */
    private Integer lot;

    /**
     * Vrai si parcours dans le sens direct.
     */
    private boolean sensDirect;

    /**
     * Étapes du calendrier.
     */
    private List<EpmTEtapeCal> etapes = new ArrayList<EpmTEtapeCal>();

    /**
     * Ensemble des jalons libres attachés au calendrier.
     */
    private Set jalonsLibres;

    // Méthodes
    /**
     * Ajoute une étape au calendrier.
     * @param etape étape
     */
    public final void ajouterEtape(final EpmTEtapeCal etape) {
        etapes.add(etape);
    }

    /**
     * @return vrai si toutes les étapes ont des noms différents, faux sinon
     */
    public final boolean isValide() {
        int nbEtapes = etapes.size();
        HashSet codes = new HashSet(nbEtapes);
        for (Iterator iter = etapes.iterator(); iter.hasNext();) {
            EpmTEtapeCal element = (EpmTEtapeCal) iter.next();
            codes.add(element.getCode());
        }
        return nbEtapes == codes.size();
    }

    /**
     * Clone du calendrier à attacher et mettre en base avant utilisation.
     * Certaines propriétés sont perdues.
     * @return objet cloné
     * @throws CloneNotSupportedException erreur
     */
    public final Object clone() throws CloneNotSupportedException {
        EpmTCalendrier clone = (EpmTCalendrier)super.clone();
        clone.setAvenant(null);
        clone.setLot(null);
        clone.setEtapes(new ArrayList<EpmTEtapeCal>());
        Set jalons = new HashSet();
        if (jalonsLibres != null) {
            Iterator it = jalonsLibres.iterator();
            while (it.hasNext()) {
                jalons.add(((String) it.next()));
            }
            clone.setJalonsLibres(jalons);
        }
        return clone;
    }



    /**
     * @return étapes
     */
    public List<EpmTEtapeCal> getEtapes() {
        return etapes;
    }

    /**
     * @param valeur étapes
     */
    public void setEtapes(final List<EpmTEtapeCal> valeur) {
        this.etapes = valeur;
    }

    /**
     * @return ensemble des jalons libres {@link JalonLibreCal} du calendrier
     */
    public Set getJalonsLibres() {
        return jalonsLibres;
    }

    /**
     * @param valeur ensemble des jalons libres {@link JalonLibreCal} du
     *            calendrier
     */
    public void setJalonsLibres(final Set valeur) {
        this.jalonsLibres = valeur;
    }

    /**
     * @return vrai si parcours dans le sens direct
     */
    public boolean isSensDirect() {
        return sensDirect;
    }

    /**
     * @param valeur vrai si parcours dans le sens direct
     */
    public void setSensDirect(final boolean valeur) {
        this.sensDirect = valeur;
    }

    /**
     * @return identifiant de l'avenant
     */
    public Integer getAvenant() {
        return avenant;
    }

    /**
     * @param valeur identifiant de l'avenant
     */
    public void setAvenant(final Integer valeur) {
        this.avenant = valeur;
    }

    /**
     * @return identifiant du lot
     */
    public Integer getLot() {
        return lot;
    }

    /**
     * @param valeur identifiant du lot
     */
    public void setLot(final Integer valeur) {
        this.lot = valeur;
    }

}
