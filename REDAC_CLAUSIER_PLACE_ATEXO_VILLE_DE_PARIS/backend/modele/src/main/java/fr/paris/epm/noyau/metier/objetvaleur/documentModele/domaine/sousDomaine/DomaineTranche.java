package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour les sous domaines
 * consultation.tranche.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineTranche implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Decrit l'estimation (distinction ferme, conditionnelle), sous la forme
     * "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    private String txtFormePrixNonAllotieFractionnee;
    /**
     * nature_de_la_tranche : estimation_HT_detaillee.
     */
    private String txtEstimationNonAllotieFractionnee;
    /**
     * Estimation si consultation non allotie et sans tranche <estimation HT
     * detaillee>.
     */
    private String txtEstimationNonAllotieNonFractionnee;
    /**
     * Decrit une tranche (distinction ferme, conditionnelle) , avec l'intitulé
     * de la tranche.
     */
    private String txtNonAllotieFractionnee;
    
    private DomaineFormeDePrix formeDePrix;
    
    /**
     * Nature de la tranche.
     */
    private String natureTranche;
    
    /**
     * Intitulé de la tranche.
     */
    private String intituleTranche;
    
    /**
     * Code de la tranche
     */
    private String codeTranche;

    /**
     * @return Decrit l'estimation (distinction ferme, conditionnelle), sous la
     *         forme "Prix Forfaitaire", "Prix Mixte" ou "Prix Unitaire".
     */
    public final String getTxtFormePrixNonAllotieFractionnee() {
        return txtFormePrixNonAllotieFractionnee;
    }

    /**
     * @param valeur Decrit l'estimation (distinction ferme, conditionnelle),
     *            sous la forme "Prix Forfaitaire", "Prix Mixte" ou
     *            "Prix Unitaire".
     */
    public final void setTxtFormePrixNonAllotieFractionnee(final String valeur) {
        this.txtFormePrixNonAllotieFractionnee = valeur;
    }

    /**
     * @return nature_de_la_tranche : estimation_HT_detaillee.
     */
    public final String getTxtEstimationNonAllotieFractionnee() {
        return txtEstimationNonAllotieFractionnee;
    }

    /**
     * @param valeur nature_de_la_tranche : estimation_HT_detaillee.
     */
    public final void setTxtEstimationNonAllotieFractionnee(final String valeur) {
        this.txtEstimationNonAllotieFractionnee = valeur;
    }

    /**
     * @return Decrit une tranche (distinction ferme, conditionnelle) , avec
     *         l'intitulé de la tranche.
     */
    public final String getTxtNonAllotieFractionnee() {
        return txtNonAllotieFractionnee;
    }

    /**
     * @param valeur Decrit une tranche (distinction ferme, conditionnelle) ,
     *            avec l'intitulé de la tranche.
     */
    public final void setTxtNonAllotieFractionnee(final String valeur) {
        txtNonAllotieFractionnee = valeur;
    }

    public final DomaineFormeDePrix getFormeDePrix() {
        return formeDePrix;
    }

    public final void setFormeDePrix(final DomaineFormeDePrix valeur) {
        this.formeDePrix = valeur;
    }

    public final String getNatureTranche() {
        return natureTranche;
    }

    /**
     * @param valeur nature de la tranche
     */
    public final void setNatureTranche(String valeur) {
        this.natureTranche = valeur;
    }

    /**
     * @return Intitulé de la tranche
     */
    public final String getIntituleTranche() {
        return intituleTranche;
    }

    /**
     * @param valeur Intitulé de la tranche
     */
    public final void setIntituleTranche(final String valeur) {
        this.intituleTranche = valeur;
    }
    /**
     * @return {@link DomaineTranche#txtEstimationNonAllotieNonFractionnee}
     */
    public final String getTxtEstimationNonAllotieNonFractionnee() {
        return txtEstimationNonAllotieNonFractionnee;
    }
    /**
     * @param txtEstimationNonAllotieNonFractionnee
     *            {@link DomaineTranche#txtEstimationNonAllotieNonFractionnee}
     */
    public final void setTxtEstimationNonAllotieNonFractionnee(
            String txtEstimationNonAllotieNonFractionnee) {
        this.txtEstimationNonAllotieNonFractionnee = txtEstimationNonAllotieNonFractionnee;
    }

    /**
     * 
     * @return le code de la tranche
     */
	public final String getCodeTranche() {
		return codeTranche;
	}

	/**
	 * 
	 * @param codeTranche le code de la tranche
	 */
	public final void setCodeTranche(final String valeur) {
		this.codeTranche = valeur;
	}
    
}