/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNatureTranche;

/**
 * Pojo hibernate du Bloc Unitaire de Données des tranches d'une consultations.
 * @author Régis Menet
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBudTranche extends EpmTAbstractObject implements Comparable<EpmTBudTranche>,
        Cloneable {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Sélect Nature Tranche.
     */
    private EpmTRefNatureTranche epmTRefNatureTranche;

    /**
     * Bloc unitaire de données (pf/pu/pm).
     */
    private EpmTBudFormePrix epmTBudFormePrix;

    /**
     * Code de la tranche (ce qui est après le TF/TC).
     */
    private String codeTranche;

    /**
     * Intitulé de la tranche.
     */
    private String intituleTranche;
    
    /**
     * Indique la tranche est issue d'un marché go.
     */
    private boolean go;
    
    /***
     * Indique si la tranche est pour Go ET issue de Go
     */
    private String referenceGo;

    // Méthodes

    /**
     * Utilise le code et l'intitulé de la tranche.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {

        int result = 1;

        if (codeTranche != null) {
            result = Constantes.PREMIER * result + codeTranche.hashCode();
        }
        if (intituleTranche != null) {
            result = Constantes.PREMIER * result + intituleTranche.hashCode();
        }
        return result;
    }

    /**
     * Utilise le code et l'intitulé de la tranche.
     * @param obj objet à comparer
     * @see java.lang.Object#equals(java.lang.Object)
     * @return résultat de la comparaison
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTBudTranche autre = (EpmTBudTranche) obj;
        if (codeTranche == null) {
            if (autre.codeTranche != null) {
                return false;
            }
        } else if (!codeTranche.equals(autre.codeTranche)) {
            return false;
        }

        if (intituleTranche == null) {
            if (autre.intituleTranche != null) {
                return false;
            }
        } else if (!intituleTranche.equals(autre.intituleTranche)) {
            return false;
        }

        return true;
    }

    /**
     * @param object objet à comparer
     * @return résultat selon le code de tranche
     */
    public final int compareTo(final EpmTBudTranche object) {
        EpmTBudTranche tranche = (EpmTBudTranche) object;
        if (codeTranche != null) {
            if (tranche != null && tranche.getCodeTranche() != null) {
                return codeTranche.compareTo(tranche.getCodeTranche());
            }
        }
        return -1;
    }

    /**
     * @see java.lang.Object#toString()
     * @return représentation texte de l'objet
     */
    public final String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nIdentifiant: ");
        res.append(id);
        res.append("\nBud forme de prix: ");
        res.append(epmTBudFormePrix);
        res.append("\nSélect Nature Tranche: ");
        res.append(epmTRefNatureTranche);
        res.append("\nCode de la tranche: ");
        res.append(codeTranche);
        res.append("\nIntitulé de la tranche: ");
        res.append(intituleTranche);
        return res.toString();
    }


    // Accesseurs
    /**
     * @return identifiant
     */
    public int getId() {
        return this.id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return sélect nature tranche
     */
    public EpmTRefNatureTranche getEpmTRefNatureTranche() {
        return this.epmTRefNatureTranche;
    }

    /**
     * @param valeur sélect nature tranche
     */
    public void setEpmTRefNatureTranche(final EpmTRefNatureTranche valeur) {
        this.epmTRefNatureTranche = valeur;
    }

    /**
     * @return bud forme de prix
     */
    public EpmTBudFormePrix getEpmTBudFormePrix() {
        return this.epmTBudFormePrix;
    }

    /**
     * @param valeur bud forme de prix
     */
    public void setEpmTBudFormePrix(final EpmTBudFormePrix valeur) {
        this.epmTBudFormePrix = valeur;
    }

    /**
     * @return code de la tranche
     */
    public String getCodeTranche() {
        return this.codeTranche;
    }

    /**
     * @param valeur code de la tranche
     */
    public void setCodeTranche(final String valeur) {
        this.codeTranche = valeur;
    }

    /**
     * @return intitulé de la tranche
     */
    public String getIntituleTranche() {
        return this.intituleTranche;
    }

    /**
     * @param valeur intitulé de la tranche
     */
    public void setIntituleTranche(final String valeur) {
        this.intituleTranche = valeur;
    }

    /**
     * 
     * @return true si tranche provenant de GO
     */
    public boolean isGo() {
        return go;
    }

    /**
     * 
     * @param valeur true si tranche provenant de GO
     */
    public void setGo(final boolean valeur) {
        this.go = valeur;
    }

    /**
     * @return rference GO
     */
    public String getReferenceGo() {
        return referenceGo;
    }

    /**
     * @param valeur cette valeur est renseigné seulement si la tranche
     * proviens de Go
     */
    public void setReferenceGo(final String valeur) {
        this.referenceGo = valeur;
    }
    
    
}
