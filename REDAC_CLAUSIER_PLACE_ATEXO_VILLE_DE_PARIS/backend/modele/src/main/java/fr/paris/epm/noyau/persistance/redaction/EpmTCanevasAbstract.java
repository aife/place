package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@MappedSuperclass
@Audited
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class EpmTCanevasAbstract extends EpmTAbstractObject
        implements Comparable<EpmTCanevasAbstract>, Cloneable, Serializable {

    /**
     * L'état supprimé du canevas.
     */
    public static final String ETAT_SUPPRIMER = "2";

    /**
     * L'état modifié du canevas.
     */
    public static final String ETAT_MODIFIER = "1";

    /**
     * L'état disponible du canevas.
     */
    public static final String ETAT_DISPONIBLE = "0";

    /**
     * l'objet EpmTRefTypeDocument.
     */
    @ManyToOne
    @JoinColumn(name = "id_document_type", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private EpmTRefTypeDocument epmTRefTypeDocument;

    /**
     * titre du canevas.
     */
    @Column(nullable = false)
    private String titre;

    /**
     * Réference du canevas.
     */
    @Column(nullable = false)
    private String reference;

    /**
     * la date création canevas.
     */
    @Column(name = "date_creation", nullable = false)
    private Date dateCreation;

    /**
     * la date modification canevas.
     */
    @Column(name = "date_modification", nullable = false)
    private Date dateModification;
    
    /**
     * la date de premiere validation du canevas.
     */
    @Column(name = "date_premiere_validation")
    private Date datePremiereValidation;

    /**
     * la date de derniere validation du canevas.
     */
    @Column(name = "date_derniere_validation")
    private Date dateDerniereValidation;

    /**
     * l'attribut actif.
     */
    @Column(nullable = false)
    private boolean actif;

    /**
     * idNaturePrestation permet de sauvgarder l'id de la nature de prestation
     * choisie.
     */
    @Column(name = "id_nature_prestation", nullable = false)
    private int idNaturePrestation;

    /**
     * l'attribut auteur.
     */
    @Column(nullable = false)
    private String auteur;

    /**
     * l'attribut etat.
     */
    @Column(nullable = false)
    private String etat = "0";

    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    @Column(name = "compatible_entite_adjudicatrice")
    private boolean compatibleEntiteAdjudicatrice = true;
    
    /**
     * L'identifiant de l'organisme auquel la consultation est liée
     */
    @Column(name = "id_organisme")
    private Integer idOrganisme;
    
    /**
     * Détermine si le canevas est de type éditeur ou client
     */
    @Column(name = "canevas_editeur")
    private boolean canevasEditeur;

    @ManyToOne
    @JoinColumn(name = "id_statut_redaction_clausier", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier;
    
    /**
     * CCAG
     */
    @Column(name = "id_ref_ccag")
    private Integer idRefCCAG;

    public abstract Integer getIdCanevas();

    public abstract Integer getIdPublication();

    public abstract Integer getIdLastPublication(); // utilisé pour l'affichage de la liste des canevas

    public EpmTRefTypeDocument getEpmTRefTypeDocument() {
        return epmTRefTypeDocument;
    }

    public void setEpmTRefTypeDocument(EpmTRefTypeDocument epmTRefTypeDocument) {
        this.epmTRefTypeDocument = epmTRefTypeDocument;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Date getDatePremiereValidation() {
        return datePremiereValidation;
    }

    public void setDatePremiereValidation(Date datePremiereValidation) {
        this.datePremiereValidation = datePremiereValidation;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public int getIdNaturePrestation() {
        return idNaturePrestation;
    }

    public void setIdNaturePrestation(int idNaturePrestation) {
        this.idNaturePrestation = idNaturePrestation;
    }

    public abstract Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() ;

    public abstract void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) ;

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public boolean isCanevasEditeur() {
        return canevasEditeur;
    }

    public void setCanevasEditeur(boolean canevasEditeur) {
        this.canevasEditeur = canevasEditeur;
    }

    public EpmTRefStatutRedactionClausier getEpmTRefStatutRedactionClausier() {
        return epmTRefStatutRedactionClausier;
    }

    public void setEpmTRefStatutRedactionClausier(EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier) {
        this.epmTRefStatutRedactionClausier = epmTRefStatutRedactionClausier;
    }

    public Integer getIdRefCCAG() {
        return idRefCCAG;
    }

    public void setIdRefCCAG(Integer idRefCCAG) {
        this.idRefCCAG = idRefCCAG;
    }

    public abstract List<? extends EpmTChapitreAbstract> getEpmTChapitres() ;

    public abstract void setEpmTChapitres(List<? extends EpmTChapitreAbstract> epmTChapitres) ;

    public abstract Set<EpmTRefProcedure> getEpmTRefProcedures() ;

    public abstract void setEpmTRefProcedures(Set<EpmTRefProcedure> epmTRefProcedures) ;

    @Override
    public int compareTo(EpmTCanevasAbstract epmTCanevasAbstract) {
        if (epmTCanevasAbstract.isActif() == isActif())
            return getIdCanevas() > epmTCanevasAbstract.getIdCanevas() ? -1 : 1;
        else
            return !epmTCanevasAbstract.isActif() ? 1 : -1;
    }

    public boolean notEquals(EpmTCanevasAbstract canevas) {

        if (getTitre() != null && canevas.getTitre() != null && !getTitre().equals(canevas.getTitre()))
            return true;

        if (isActif() != canevas.isActif())
            return true;

        if (getIdNaturePrestation() != canevas.getIdNaturePrestation())
            return true;

        if ((getEpmTRefProcedures() == null && canevas.getEpmTRefProcedures() != null) ||
                (getEpmTRefProcedures() != null && canevas.getEpmTRefProcedures() == null)) {
            return true;
        } else if (getEpmTRefProcedures() != null && canevas.getEpmTRefProcedures() != null) {

            if (getEpmTRefProcedures().size() != canevas.getEpmTRefProcedures().size())
                return true;

            List<EpmTRefProcedure> l1Tmp = new ArrayList<EpmTRefProcedure>(getEpmTRefProcedures());
            List<EpmTRefProcedure> l2Tmp = new ArrayList<EpmTRefProcedure>(canevas.getEpmTRefProcedures());
            Collections.sort(l1Tmp);
            Collections.sort(l2Tmp);

            for (int i = 0; i < l1Tmp.size(); i++)
                if (l1Tmp.get(i).getId() != l2Tmp.get(i).getId())
                    return true;
        }

        if (getEpmTRefTypeDocument() != null && canevas.getEpmTRefTypeDocument() != null &&
                getEpmTRefTypeDocument().getId() != canevas.getEpmTRefTypeDocument().getId())
            return true;

        if (isCompatibleEntiteAdjudicatrice() != canevas.isCompatibleEntiteAdjudicatrice())
            return true;

        if (!getIdRefCCAG().equals(canevas.getIdRefCCAG()))
            return true;

        return false;
    }

    @Override
    public EpmTCanevasAbstract clone() throws CloneNotSupportedException {
        EpmTCanevasAbstract epmTCanevas = (EpmTCanevasAbstract) super.clone();
        epmTCanevas.dateModification = new Date(); // date courante
        epmTCanevas.idOrganisme = idOrganisme;
        epmTCanevas.canevasEditeur = canevasEditeur;

        if (getEpmTChapitres() != null) {
            List<EpmTChapitreAbstract> listChapitres = new ArrayList<>();
            for (EpmTChapitreAbstract chapitre : getEpmTChapitres()) {
                EpmTChapitreAbstract epmTChapitre = chapitre.clone();
                epmTChapitre.setEpmTCanevas(epmTCanevas);
                listChapitres.add(epmTChapitre);
            }
            epmTCanevas.setEpmTChapitres(listChapitres);
        }
        return epmTCanevas;
    }

}
