package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDureeDelaiDescription de la table "epm__t_ref_duree_delai_description"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDureeDelaiDescription extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int DUREE_MARCHE_EN_MOIS_EN_JOUR = 1; // Référence à la base de données. Durée du marché en mois, en jour.
    public static final int DELAI_EXECUTION_DU_AU = 2; // Référence à la base de données. Délai d'exécution du, au.
    public static final int DESCRIPTION_LIBRE = 3; // Référence à la base de données. Description libre.

}