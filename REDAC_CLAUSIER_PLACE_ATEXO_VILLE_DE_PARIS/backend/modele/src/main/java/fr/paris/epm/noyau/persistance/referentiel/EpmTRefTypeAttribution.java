package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeAttribution de la table "epm__t_ref_type_attribution"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeAttribution extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_MONO_ATTRIBUTAIRE = 1;
    public static final int ID_MULTI_ATTRIBUTAIRE = 2;

    public static final String MONO_ATTRIBUTAIRE = "Mono-attributaire";
    public static final String MULTI_ATTRIBUTAIRE = "Multi-attributaire";

}