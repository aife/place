package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefPerimetreAlerte de la table "epm__t_ref_perimetre_alerte"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefPerimetreAlerte extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int VISION = 1; // Identifiant de base utilisé pour répresenter un périmètre de vision
    public static final int INTERVENTION = 2; // Identifiant de base utilisé pour répresenter un périmètre d'intervention
    public static final int RESPONSABLE_CONSULTATION = 3; // Identifiant de base utilisé pour répresenter un périmètre de responsable de consultation

}