package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefGroupementAttributaire de la table "epm__t_ref_groupement_attributaire"
 * Created by nty on 31/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefGroupementAttributaire extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int SOLIDAIRE = 1; // Id solidaire.
    public static final int CONJOINT = 2; // Id conjoint.
    public static final int AU_CHOIX = 3; // Id en base de au choix.

}