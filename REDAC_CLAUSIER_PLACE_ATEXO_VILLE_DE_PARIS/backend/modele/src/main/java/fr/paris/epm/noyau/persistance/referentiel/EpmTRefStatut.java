package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatut de la table "epm__t_ref_statut"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatut extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int DEFINITION = 1; // Etape de définition.
    public static final int PUBLICITE = 2; // Etape de publicité.
    public static final int CONSULTATION = 3; // Etape de consultation.
    public static final int DEPOUILLEMENT = 4; // Etape de dépouillement.
    public static final int ATTRIBUTION = 5; // Etape de attribution.
    public static final int EXECUTION = 6; // Consultation en exécution.
    public static final int CLOS = 7; // Consultation close.
    public static final int SANS_SUITE = 8; // Consultation sans suite ou infructueuse.
    public static final int INFRUCTUEUX = 9; // Consultation infructueuse.
    public static final int ABANDON_TECHNIQUE = 10; // Consultation Abandon technique.

    /**
     * identifiant GO.
     */
    private String codeGo;

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(String codeGo) {
        this.codeGo = codeGo;
    }

}