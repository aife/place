package fr.paris.epm.noyau.persistance;


/**
 * POJO hibernate - utilisé pour enregistrer le chrono de la consultation pour
 * chaque direction service
 * @author GAO Xuesong
 */
public class EpmTCompteurConsultationDirectionService extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808544L;

    /**
     * clé primaire
     */
    private Integer id;
    
    /**
     * id de Direction Service qui possède le chrono
     */
    private Integer idDirectionService;

    /**
     * le nombre de consultation de la direction service
     */
    private long chronoConsultation;

    
    public Integer getId() {
        return id;
    }

    public void setId(final Integer valeur) {
        this.id = valeur;
    }

	public Integer getIdDirectionService() {
		return idDirectionService;
	}

	public void setIdDirectionService(final Integer valeur) {
		this.idDirectionService = valeur;
	}

	public long getChronoConsultation() {
		return chronoConsultation;
	}

	public void setChronoConsultation(final long valeur) {
		this.chronoConsultation = valeur;
	}

    
    
    
    
}
