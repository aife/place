package fr.paris.epm.noyau.metier.redaction;

/**
 * Rassemble les critères de recherche des canevas et des canevas publiés selon le besion.
 * Utilisé pour faire la UNION des EpmTCanevas et EpmTCanevasPub => EpmVCanevas
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class CanevasViewCritere extends CanevasPubCritere {

    private Boolean hasPublication;

    public CanevasViewCritere(String plateformeUuid) {
        super(plateformeUuid);
    }

    public CanevasViewCritere() {
        super();
    }

    @Override
    public StringBuffer corpsRequete() {

        StringBuffer sb = super.corpsRequete();

        if (hasPublication != null) {
            if (hasPublication)
                sb.append(" AND canevas.epmTCanevasPub IS NOT NULL");
            else
                sb.append(" AND canevas.epmTCanevasPub IS NULL");
        }
        return sb;
    }

    @Override
    protected String findByIdCanevas() {
        String s1 = "epmTCanevas.id in (:listIds)";
        String s2 = "epmTCanevasPub.idCanevas in (:listIds)";
        return "  AND ((canevas.epmTCanevasPub IS NULL AND " + s1 + ")" +
                " OR " +
                "(canevas.epmTCanevasPub IS NOT NULL AND " + s2 + "))";
    }

    /*
        il faut redéfinir cette méthode dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVClause.epmTClausePub.idPublication
     */
    protected String findByIdPublication() {
        return "(canevas.epmTCanevasPub IS NOT NULL AND epmTCanevasPub.idPublication = :idPublication)";
    }

    /*
        il ne faut redéfinir cette méthode que dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVCanveas.epmTCanevas ou EpmVCanveas.epmTCanevasPub
     */
    protected String findByProcedure() {
        String s1 = "(size(epmTCanevas.epmTRefProcedures) = 0 OR (from EpmTRefProcedure where id = :idProcedure) in elements(epmTCanevas.epmTRefProcedures))";
        String s2 = "(size(epmTCanevasPub.epmTRefProcedures) = 0 OR (from EpmTRefProcedure where id = :idProcedure) in elements(epmTCanevasPub.epmTRefProcedures))";
        return "(canevas.epmTCanevasPub IS NULL AND " + s1 +
                " OR " +
                "canevas.epmTCanevasPub IS NOT NULL AND " + s2 + ")";
    }

    protected String findByProcedureCodeExterne() {
        String s1 = "(size(epmTCanevas.epmTRefProcedures) = 0 OR (from EpmTRefProcedure where codeExterne = :procedureCodeExterne) in elements(epmTCanevas.epmTRefProcedures))";
        String s2 = "(size(epmTCanevasPub.epmTRefProcedures) = 0 OR (from EpmTRefProcedure where codeExterne = :procedureCodeExterne) in elements(epmTCanevasPub.epmTRefProcedures))";
        return "(canevas.epmTCanevasPub IS NULL AND " + s1 +
                " OR " +
                "canevas.epmTCanevasPub IS NOT NULL AND " + s2 + ")";
    }

    /*
        il ne faut redéfinir cette méthode que dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVCanveas.epmTCanevas ou EpmVCanveas.epmTCanevasPub
     */
    protected String findByTypeContrat() {
        String s1 = "(size(epmTCanevas.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(epmTCanevas.epmTRefTypeContrats))";
        String s2 = "(size(epmTCanevasPub.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(epmTCanevasPub.epmTRefTypeContrats))";
        return "(canevas.epmTCanevasPub IS NULL AND " + s1 +
                " OR " +
                "canevas.epmTCanevasPub IS NOT NULL AND " + s2 + ")";
    }

    @Override
    protected String getTableDeRecherche() {
        return " EpmVCanevas ";
    }

    @Override
    protected String getJoinsTables() {
        return " left join canevas.epmTCanevas epmTCanevas left join canevas.epmTCanevasPub epmTCanevasPub, EpmTRefOrganisme epmTRefOrganisme ";
    }

    public void setHasPublication(Boolean hasPublication) {
        this.hasPublication = hasPublication;
    }

}
