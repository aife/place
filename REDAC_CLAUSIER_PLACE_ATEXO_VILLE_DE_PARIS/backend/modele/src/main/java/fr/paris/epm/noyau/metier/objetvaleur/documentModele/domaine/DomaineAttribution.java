package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineConfirmation;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineContrat;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine
 * attribution.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineAttribution implements Serializable {
	
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private DomaineContrat contrat;

    private DomaineConfirmation confirmation;

    public final DomaineContrat getContrat() {
        return contrat;
    }

    public final void setContrat(final DomaineContrat valeur) {
        this.contrat = valeur;
    }

    public final DomaineConfirmation getConfirmation() {
        return confirmation;
    }

    public final void setConfirmation(final DomaineConfirmation valeur) {
        this.confirmation = valeur;
    }
}
