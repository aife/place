package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * consultation.informationsPrincipales.critere.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCritereConsultation implements Serializable {

	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    private String nomCritere;

    private Double ponderationCritere;
    
    private List<DomaineSousCritereConsultation> listeSousCriteres;

    public final String getNomCritere() {
        return nomCritere;
    }

    public final void setNomCritere(final String valeur) {
        this.nomCritere = valeur;
    }

    public final Double getPonderationCritere() {
        return ponderationCritere;
    }

    public final void setPonderationCritere(final Double valeur) {
        this.ponderationCritere = valeur;
    }

	public final List<DomaineSousCritereConsultation> getListeSousCriteres() {
		return listeSousCriteres;
	}

	public final void setListeSousCriteres(final
			List<DomaineSousCritereConsultation> valeur) {
		this.listeSousCriteres = valeur;
	}
    
}
