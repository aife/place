/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefBonQuantite;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefMinMax;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypePrix;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefVariation;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate du Bloc Unitaire de Données Forme de Prix, Prix Mixte.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTBudFormePrixPm extends EpmTBudFormePrixPf {

    // Propriétés

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * sélect Bon Quantité.
     */
    private EpmTRefBonQuantite puEpmTRefBonQuantite;

    /**
     * ensemble de sélects type de prix.
     */
    private Set<EpmTRefTypePrix> puEpmTRefTypePrix = new HashSet<EpmTRefTypePrix>();

    /**
     * sélect min/max.
     */
    private EpmTRefMinMax puEpmTRefMinMax;

    /**
     * minimum HT.
     */
    private Double puMinHt;

    /**
     * maximum HT.
     */
    private Double puMaxHt;
    
    /**
     * minimum TTC.
     */
    private Double puMinTtc;

    /**
     * maximum TTC.
     */
    private Double puMaxTtc;

    /**
     * estimation HT.
     */
    private Double puEstimationHt;

    /**
     * estimation TTC.
     */
    private Double puEstimationTtc;

    /**
     * date de valeur.
     */
    private Calendar puDateValeur;

    /**
     * Collection d'objets {
     * @link EpmTRefVariations } associés à PU.
     */
    private Set<EpmTRefVariation> puEpmTRefVariations = new HashSet<EpmTRefVariation>();

    // Constructeurs

    /**
     * Constructeur vide pour hibernate.
     */
    public EpmTBudFormePrixPm() {
        super();
    }

    // Accesseurs PU
    /**
     * @return sélect Bon Quantité
     */
    public EpmTRefBonQuantite getPuEpmTRefBonQuantite() {
        return puEpmTRefBonQuantite;
    }

    /**
     * @param bonQuantite sélect Bon Quantité
     */
    public void setPuEpmTRefBonQuantite(final EpmTRefBonQuantite valeur) {
        this.puEpmTRefBonQuantite = valeur;
    }

    /**
     * @return sélect min/max
     */
    public EpmTRefMinMax getPuEpmTRefMinMax() {
        return puEpmTRefMinMax;
    }

    /**
     * @param minMax sélect min/max
     */
    public void setPuEpmTRefMinMax(final EpmTRefMinMax valeur) {
        this.puEpmTRefMinMax = valeur;
    }

    /**
     * @return ensemble de sélects type de prix
     */
    public Set<EpmTRefTypePrix> getPuEpmTRefTypePrix() {
        return puEpmTRefTypePrix;
    }

    /**
     * @param typePrix ensemble de sélects type de prix
     */
    public void setPuEpmTRefTypePrix(final Set<EpmTRefTypePrix> valeur) {
        this.puEpmTRefTypePrix = valeur;
    }

    /**
     * @return date de valeur
     */
    public Calendar getPuDateValeur() {
        return puDateValeur;
    }

    /**
     * @param dateValeur date de valeur
     */
    public void setPuDateValeur(final Calendar valeur) {
        this.puDateValeur = valeur;
    }

    /**
     * @return estimation interne HT
     */
    public Double getPuEstimationHt() {
        return puEstimationHt;
    }

    /**
     * @param estimationHT estimation interne HT
     */
    public void setPuEstimationHt(final Double valeur) {
        this.puEstimationHt = valeur;
    }

    /**
     * @return estimation interne TTC
     */
    public Double getPuEstimationTtc() {
        return puEstimationTtc;
    }

    /**
     * @param estimationTtc estimation interne TTC
     */
    public void setPuEstimationTtc(final Double valeur) {
        this.puEstimationTtc = valeur;
    }

    /**
     * @return maximum HT
     */
    public Double getPuMaxHt() {
        return puMaxHt;
    }

    /**
     * @param maxHt maximum HT
     */
    public void setPuMaxHt(final Double valeur) {
        this.puMaxHt = valeur;
    }

    /**
     * @return minimum HT
     */
    public Double getPuMinHt() {
        return puMinHt;
    }

    /**
     * @param minHt minimum HT
     */
    public void setPuMinHt(final Double valeur) {
        this.puMinHt = valeur;
    }

    /**
     * @return collection de {
     * @link EpmTRefVariations } associés.
     */
    public Set<EpmTRefVariation> getPuEpmTRefVariations() {
        return puEpmTRefVariations;
    }

    /**
     * @param variations Collection de {
     * @link EpmTRefVariations } associés.
     */
    public void setPuEpmTRefVariations(final Set<EpmTRefVariation> valeur) {
        this.puEpmTRefVariations = valeur;
    }
    
    /**
     * @return
     */
    public Double getPuMinTtc() {
        return puMinTtc;
    }

    /**
     * @param valeur
     */
    public void setPuMinTtc(final Double valeur) {
        this.puMinTtc = valeur;
    }

    /**
     * @return
     */
    public Double getPuMaxTtc() {
        return puMaxTtc;
    }

    /**
     * @param valeur
     */
    public void setPuMaxTtc(final Double valeur) {
        this.puMaxTtc = valeur;
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTBudFormePrixPm formePrix = new EpmTBudFormePrixPm();
        formePrix.setPfEstimationHt(pfEstimationHt);
        formePrix.setPfEstimationTtc(pfEstimationTtc);
        formePrix.setPfDateValeur(pfDateValeur);
        
        formePrix.setPuEpmTRefBonQuantite(puEpmTRefBonQuantite);

        formePrix.setPuEpmTRefMinMax(puEpmTRefMinMax);
        formePrix.setPuMinHt(puMinHt);
        formePrix.setPuMaxHt(puMaxHt);
        formePrix.setPuMinTtc(puMinTtc);
        formePrix.setPuMaxTtc(puMaxTtc);
        formePrix.setPuEstimationHt(puEstimationHt);
        formePrix.setPuEstimationTtc(puEstimationTtc);
        formePrix.setPuDateValeur(puDateValeur);

        if (pfEpmTRefVariations != null) {
            Set set = new HashSet();
            set.addAll(pfEpmTRefVariations);
            formePrix.setPfEpmTRefVariations(set);
        }
        if (puEpmTRefVariations != null) {
            Set set = new HashSet();
            set.addAll(puEpmTRefVariations);
            formePrix.setPuEpmTRefVariations(set);
        }
        if (puEpmTRefTypePrix != null) {
            Set set = new HashSet();
            set.addAll(puEpmTRefTypePrix);
            formePrix.setPuEpmTRefTypePrix(set);
        }
        return formePrix;
    }
}
