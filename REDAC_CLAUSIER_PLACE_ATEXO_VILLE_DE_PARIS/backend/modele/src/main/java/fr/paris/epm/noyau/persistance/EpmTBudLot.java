package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.*;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import java.util.*;

/**
 * Pojo hibernate du Bloc Unitaire de Données des lots d'une consultations.
 * @author Régis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Audited
@AuditTable(value = "epm__t_bud_lot_aud", schema = "consultation")
public class EpmTBudLot extends EpmTAbstractObject implements Comparable<EpmTBudLot> {

    /**
     * Constante indiquant qu'un document conservé est attaché à tous les lots.
     * (notion de multi-lots).
     */
    public static final String MULTI_LOT = "Tous lots";
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identifiant.
     */
    private int id;

    /**
     * Référentiel nature.
     */
    private EpmTRefNature epmTRefNature;

    /**
     * Statut du lot s'il est dissocié.
     */
    private EpmTRefStatut epmTRefStatut;

    /**
     * precedent statut du lot s'il est dissocié.
     */
    private Integer statutAncien;

    /**
     * Sélect entre jour et mois.
     */
    private EpmTRefChoixMoisJour epmTRefChoixMoisJour;

    /**
     * Bloc unitaire de données "lot ou consultation".
     */
    private EpmTBudLotOuConsultation epmTBudLotOuConsultation;

    /**
     * Numéro du lot.
     */
    private String numeroLot;

    private Integer idConsultationLotDissocie;

    /**
     * contient un nombre associé a la validation par une etape dans le
     * workflow.
     */
    private int validationEtape;

    /**
     * valeur de la validation etape maximal (lot en cas de
     * dissociation).
     */
    private int validationEtapeMaximum;

    /**
     * Intitulé du lot.
     */
    private String intituleLot;

    /**
     * Description succinte du lot.
     */
    private String descriptionSuccinte;

    /**
     * Sélect Durée/délai/description.
     */
    private EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription;

    /**
     * Durée du marché, dans l'unité sélectionnée (mois/jour).
     */
    private Integer dureeMarche;

    /**
     * Date de début du marché.
     */
    private Calendar dateDebut;

    /**
     * Date de fin du marché.
     */
    private Calendar dateFin;

    /**
     * Description de la durée du marché.
     */
    private String descriptionDuree;

    /**
     * Liste de cpvs liés au lot courant.
     */
    private Set<EpmTCpv> cpvs = new HashSet<>(0);

    /**
     * Code Achat
     */
    private EpmTRefCodeAchat epmTRefCodeAchat;

    /**
     * Operation / Unite Fonctionnelle
     */
    private EpmTOperationUniteFonctionnelle epmTOperationUniteFonctionnelle;

    private Calendar dateValeurPreinscription;

    private Integer idConsultation;

    /**
     * Liste des critéres d'attribution liés à la consultation.
     */
    private Set<EpmTCritereAttributionConsultation> listeCritereAttribution;

    /**
     * Type de critére d'attribution associé à la consultation.
     */
    private EpmTRefCritereAttribution critereAttribution;


    /**
     * @return référentiel nature
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefNature getEpmTRefNature() {
        return this.epmTRefNature;
    }

    /**
     * @param valeur référentiel nature
     */
    public void setEpmTRefNature(final EpmTRefNature valeur) {
        this.epmTRefNature = valeur;
    }

    /**
     * @return sélect choix mois/jour
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefChoixMoisJour getEpmTRefChoixMoisJour() {
        return this.epmTRefChoixMoisJour;
    }

    /**
     * @param valeur sélect choix mois/jour
     */
    public void setEpmTRefChoixMoisJour(final EpmTRefChoixMoisJour valeur) {
        this.epmTRefChoixMoisJour = valeur;
    }

    /**
     * @return numéro du lot
     */
    public String getNumeroLot() {
        return this.numeroLot;
    }

    /**
     * @param valeur numéro du lot
     */
    public void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }

    /**
     * @return intitulé du lot
     */
    public String getIntituleLot() {
        return this.intituleLot;
    }

    /**
     * @param valeur intitulé du lot
     */
    public void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return description succinte du lot
     */
    public String getDescriptionSuccinte() {
        return this.descriptionSuccinte;
    }

    /**
     * @param valeur description succinte du lot
     */
    public void setDescriptionSuccinte(final String valeur) {
        this.descriptionSuccinte = valeur;
    }

    /**
     * @return description de la durée ou du délai
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefDureeDelaiDescription getEpmTRefDureeDelaiDescription() {
        return this.epmTRefDureeDelaiDescription;
    }

    /**
     * @param valeur description de la durée ou du délai
     */
    public void setEpmTRefDureeDelaiDescription(final EpmTRefDureeDelaiDescription valeur) {
        this.epmTRefDureeDelaiDescription = valeur;
    }

    /**
     * @return durée du marché
     */
    public Integer getDureeMarche() {
        return this.dureeMarche;
    }

    /**
     * @param valeur durée du marché
     */
    public void setDureeMarche(final Integer valeur) {
        this.dureeMarche = valeur;
    }

    /**
     * @return date de début
     */
    public Calendar getDateDebut() {
        return this.dateDebut;
    }

    /**
     * @param valeur date de début
     */
    public void setDateDebut(final Calendar valeur) {
        this.dateDebut = valeur;
    }

    /**
     * @return date de fin
     */
    public Calendar getDateFin() {
        return this.dateFin;
    }

    /**
     * @param valeur date de fin
     */
    public void setDateFin(final Calendar valeur) {
        this.dateFin = valeur;
    }

    /**
     * @return description de la durée
     */
    public String getDescriptionDuree() {
        return this.descriptionDuree;
    }

    /**
     * @param valeur description de la durée
     */
    public void setDescriptionDuree(final String valeur) {
        this.descriptionDuree = valeur;
    }

    /**
     * @return bloc unitaire de données "lot ou consultation"
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTBudLotOuConsultation getEpmTBudLotOuConsultation() {
        return epmTBudLotOuConsultation;
    }

    /**
     * @param valeur bloc unitaire de données "lot ou consultation"
     */
    public void setEpmTBudLotOuConsultation(final EpmTBudLotOuConsultation valeur) {
        this.epmTBudLotOuConsultation = valeur;
    }

    /**
     * Code basé sur le numéro et l'intitulé du lot.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {
        int code = 1;
        if (numeroLot != null)
            code = Constantes.PREMIER * code + numeroLot.hashCode();
        if (intituleLot != null)
            code = Constantes.PREMIER * code + intituleLot.hashCode();
        return code;
    }

    /**
     * Basé sur le numéro de lot. Chaque lot doit avoir un numéro de lot unique.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj objet à comparer
     * @return vrai si les objets sont égaux, faux sinon
     */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!getClass().equals(obj.getClass()))
            return false;

        final EpmTBudLot autre = (EpmTBudLot) obj;
        if (numeroLot == null)
            return autre.numeroLot == null;
        else
            return numeroLot.equals(autre.numeroLot);
    }

    private Integer parseToInt(final String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /**
     * @param lot lot à comparer
     * @return résultat de la comparaison
     */
    @Override
    public final int compareTo(final EpmTBudLot lot) {
        Integer num1 = parseToInt(this.numeroLot);
        Integer num2 = parseToInt(lot.numeroLot);

        if (num1 == null) {
            if (num2 == null)
                return this.numeroLot.compareTo(lot.numeroLot);
            else
                return 1;
        } else {
            if (num2 == null)
                return -1;
            else
                return num1.compareTo(num2);
        }
    }

    /**
     * @return la liste des cpvs lié au cpv.
     */
    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTCpv> getCpvs() {
        return cpvs;
    }

    /**
     * @param valeur la liste de cpvs à lier au lot.
     */
    public void setCpvs(final Set<EpmTCpv> valeur) {
        this.cpvs = valeur;
    }

    /**
     * @return date valeur au moment de la preinscription des consultations
     */
    public Calendar getDateValeurPreinscription() {
        return dateValeurPreinscription;
    }

    /**
     * date valeur au moment de la preinscription des consultations
     */
    public void setDateValeurPreinscription(final Calendar valeur) {
        this.dateValeurPreinscription = valeur;
    }

    /**
     * @return statut du lot dissocié sinon null
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefStatut getEpmTRefStatut() {
        return epmTRefStatut;
    }

    /**
     * @param valeur statut du lot dissocié sinon null
     */
    public void setEpmTRefStatut(final EpmTRefStatut valeur) {
        this.epmTRefStatut = valeur;
    }

    /**
     * @return Code Achat
     */
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefCodeAchat getEpmTRefCodeAchat() {
        return epmTRefCodeAchat;
    }

    /**
     *
     * @param valeur Code Achat
     */
    public void setEpmTRefCodeAchat(final EpmTRefCodeAchat valeur) {
        this.epmTRefCodeAchat = valeur;
    }

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTOperationUniteFonctionnelle getEpmTOperationUniteFonctionnelle() {
        return epmTOperationUniteFonctionnelle;
    }

    public void setEpmTOperationUniteFonctionnelle(EpmTOperationUniteFonctionnelle epmTOperationUniteFonctionnelle) {
        this.epmTOperationUniteFonctionnelle = epmTOperationUniteFonctionnelle;
    }

    /**
     *
     * @return statut du lot dissocié sinon null
     */
    public Integer getStatutAncien() {
        return statutAncien;
    }

    /**
     * @param valeur statut du lot dissocié sinon null
     */
    public void setStatutAncien(final Integer valeur) {
        this.statutAncien = valeur;
    }

    /**
     * @return contient un nombre associé a la validation par une etape dans le workflow
     */
    public int getValidationEtape() {
        return validationEtape;
    }

    /**
     * @param valeur contient un nombre associé a la validation par une etape dans le workflow
     */
    public void setValidationEtape(final int valeur) {
        this.validationEtape = valeur;
    }

    /**
     * @return valeur de la validation etape maximal (lot en cas de dissociation).
     */
    public int getValidationEtapeMaximum() {
        return validationEtapeMaximum;
    }

    /**
     * @param valeur valeur de la validation etape maximal (lot en cas de dissociation).
     */
    public void setValidationEtapeMaximum(final int valeur) {
        this.validationEtapeMaximum = valeur;
    }

    public Integer getIdConsultationLotDissocie() {
        return idConsultationLotDissocie;
    }

    public void setIdConsultationLotDissocie(Integer consultationParent) {
        this.idConsultationLotDissocie = consultationParent;
    }

    public Integer getIdConsultation() {
        return idConsultation;
    }

    public void setIdConsultation(Integer idConsultation) {
        this.idConsultation = idConsultation;
    }

    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public Set<EpmTCritereAttributionConsultation> getListeCritereAttribution() {
        return listeCritereAttribution;
    }

    public void setListeCritereAttribution(
            final Set<EpmTCritereAttributionConsultation> valeur) {
        this.listeCritereAttribution = valeur;
    }

    public List<EpmTCritereAttributionConsultation> getListeCritereAttributionTriee() {
        List<EpmTCritereAttributionConsultation> listeTriee = null;
        if (getListeCritereAttribution() != null) {
            listeTriee = new ArrayList<EpmTCritereAttributionConsultation>(getListeCritereAttribution());
            Collections.sort(listeTriee, new Comparator<EpmTCritereAttributionConsultation>() {
                @Override
                public int compare(EpmTCritereAttributionConsultation arg0,
                                   EpmTCritereAttributionConsultation arg1) {
                    return arg0.getOrdre() - arg1.getOrdre();
                }
            });
        }
        return listeTriee;
    }

    @NotAudited
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    public EpmTRefCritereAttribution getCritereAttribution() {
        return critereAttribution;
    }

    public void setCritereAttribution(final EpmTRefCritereAttribution valeur) {
        this.critereAttribution = valeur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
