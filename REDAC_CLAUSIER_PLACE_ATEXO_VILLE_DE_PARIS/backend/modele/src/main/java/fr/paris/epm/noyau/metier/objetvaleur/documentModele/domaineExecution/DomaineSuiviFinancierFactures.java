package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le domaine suivi des factures
 * @author GAO Xuesong
 */
public class DomaineSuiviFinancierFactures implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numéro de facture
	 */
	private String numeroFacture; 
	
	/**
	 * Référence de facture
	 */
	private String referenceFacture; 
	
	/**
	 * Date de réception de la facture
	 */
	private Date dateReceptionFacture; 

	/**
	 * Date de facturation
	 */
	private Date dateFacture; 
	
	/**
	 * Tiers de la facture
	 */
	private String tiersFacture; 
	
	/**
	 * Montant HT de la facture
	 */
	private Double montantHTFacture;
	
	/**
	 * Montant TTC de la facture
	 */
	private Double montantTTCFacture;
	
	/**
	 * Liste de mandats associés à la facture
	 */
	private List<DomaineSuiviFinancierMandats> mandats;
	
	
	public final String getNumeroFacture() {
		return numeroFacture;
	}

	public final void setNumeroFacture(final String valeur) {
		this.numeroFacture = valeur;
	}

	public final String getReferenceFacture() {
		return referenceFacture;
	}

	public final void setReferenceFacture(final String valeur) {
		this.referenceFacture = valeur;
	}

	public final Date getDateReceptionFacture() {
		return dateReceptionFacture;
	}

	public final void setDateReceptionFacture(final Date valeur) {
		this.dateReceptionFacture = valeur;
	}

	public final Date getDateFacture() {
		return dateFacture;
	}

	public final void setDateFacture(final Date valeur) {
		this.dateFacture = valeur;
	}

	public final String getTiersFacture() {
		return tiersFacture;
	}

	public final void setTiersFacture(final String valeur) {
		this.tiersFacture = valeur;
	}

	public final Double getMontantHTFacture() {
		return montantHTFacture;
	}

	public final void setMontantHTFacture(final Double valeur) {
		this.montantHTFacture = valeur;
	}

	public final Double getMontantTTCFacture() {
		return montantTTCFacture;
	}

	public final void setMontantTTCFacture(Double valeur) {
		this.montantTTCFacture = valeur;
	}

	public final List<DomaineSuiviFinancierMandats> getMandats() {
		return mandats;
	}

	public final void setMandats(final List<DomaineSuiviFinancierMandats> valeur) {
		this.mandats = valeur;
	}
	
	
	
}
