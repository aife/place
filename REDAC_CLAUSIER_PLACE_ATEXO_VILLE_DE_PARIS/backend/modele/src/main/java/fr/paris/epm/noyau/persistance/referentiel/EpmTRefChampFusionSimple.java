package fr.paris.epm.noyau.persistance.referentiel;

/**
 * Champ de fusion simple dont l'expression est le chemin java d'un objet.
 * La valeur sera récupérée par introspection.
 * @author Léon Barsamian
 */
public class EpmTRefChampFusionSimple extends EpmTRefChampFusion {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

}