package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.EtapesDatesCal;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineAvenantAttribution;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineCaracteristiquesPrincipales;
import fr.paris.epm.noyau.persistance.EpmTAvenant;
import fr.paris.epm.noyau.persistance.EpmTEntrepriseGroupe;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Set;

/**
 * Contient les données a fusionner dans un document pour le domaine avenant.
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineAvenant implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    private DomaineCaracteristiquesPrincipales caracteristiquesPrincipales;

    private DomaineAvenantAttribution attribution;


    public DomaineAvenant() {
    }

    public DomaineAvenant(EpmTAvenant avenant) {
        caracteristiquesPrincipales = new DomaineCaracteristiquesPrincipales(avenant);
        attribution = new DomaineAvenantAttribution();
        attribution.setNumeroCompletAvenant(avenant.getReferenceConcatenee());
        Set<EpmTEntrepriseGroupe> entrepriseSet = avenant.getTitulaires();
        EpmTEntrepriseGroupe[] entreprises = entrepriseSet.toArray(new EpmTEntrepriseGroupe[0]);
        if (entreprises.length != 0) {// TODO: MERDE MAKA et si on a plus qu'une entreprise
            attribution.setRaisonSocialeAttributaireAvenant(entreprises[0].getNom());
            attribution.setAdresseAttributaireAvenant(entreprises[0].getAdresse() != null ? entreprises[0].getAdresse() : "");
            attribution.setSIRENAttributaireAvenant(entreprises[0].getSiren());
        } else {
            attribution.setRaisonSocialeAttributaireAvenant("");
            attribution.setAdresseAttributaireAvenant("");
            attribution.setSIRENAttributaireAvenant("");
        }
    }

    public DomaineAvenant(EpmTAvenant avenant, EtapesDatesCal etapesDatesCal, DateFormat simpleDateFormat) {
        this(avenant);
        if (etapesDatesCal.getNotificationAttribution() != null)
        attribution.setDateEnvoiNotificationAvenant(simpleDateFormat.format(etapesDatesCal.getNotificationAttribution().getDate()));

        if (etapesDatesCal.getDateSelectionCandidatureCao() != null && etapesDatesCal.getDateSelectionCandidatureCao().getDate() != null)
            caracteristiquesPrincipales.setDateSelectionCandidatureCao(simpleDateFormat.format(etapesDatesCal.getDateSelectionCandidatureCao().getDate()));

        if (etapesDatesCal.getDateDeliberationAmont() != null && etapesDatesCal.getDateSelectionCandidatureCao() != null && etapesDatesCal.getDateSelectionCandidatureCao().getDate() != null)
            caracteristiquesPrincipales.setDateDeliberationAmontReunion(simpleDateFormat.format(etapesDatesCal.getDateSelectionCandidatureCao().getDate()));

        if (etapesDatesCal.getEnregistrementControleLegalite() != null && etapesDatesCal.getEnregistrementControleLegalite().getDate() != null)
            caracteristiquesPrincipales.setDateControleDeLegalite(simpleDateFormat.format(etapesDatesCal.getEnregistrementControleLegalite().getDate()));

    }

    public final DomaineCaracteristiquesPrincipales getCaracteristiquesPrincipales() {
        return caracteristiquesPrincipales;
    }

    public final void setCaracteristiquesPrincipales(
            final DomaineCaracteristiquesPrincipales valeur) {
        this.caracteristiquesPrincipales = valeur;
    }

    public final DomaineAvenantAttribution getAttribution() {
        return attribution;
    }

    public final void setAttribution(final DomaineAvenantAttribution valeur) {
        this.attribution = valeur;
    }
}
