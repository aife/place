package fr.paris.epm.noyau.persistance;

import java.text.MessageFormat;
import java.util.Date;

/**
 * Objet hibernate de l'authentification inter-module d'un utilisateur avec verification
 * de l'integrite du cookie d'authentification. 
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTAuthentificationToken extends EpmTAbstractObject {
    /**
	 * Marqueur de sérialization.
	 */
	private static final long serialVersionUID = 7906277449674603333L;
	/**
	 * Login utilisateur.
	 */
	private String identifiant;
	/**
	 * Pour associer le cookie a l'utilisateur.
	 */
	private String signature;
	/**
	 * Incremente a chaque requete de l'utilisateur, permet de detecter un vol de cookie.
	 */
	private Integer token;
	/**
	 * Pour detecter l'incativite de l'utilisateur et desactiver son cookie.
	 */
	private Date dateDerniereActivite;
	/**
	 * Pour etre sur de savoir si l'utilisateur vient juste de s'authentifier ou non.
	 */
	private boolean justeAuthentifier;
	
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String valeur) {
		this.identifiant = valeur;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String valeur) {
		this.signature = valeur;
	}
	public Integer getToken() {
		return token;
	}
	public void setToken(Integer valeur) {
		this.token = valeur;
	}
	public Date getDateDerniereActivite() {
		return dateDerniereActivite;
	}
	public void setDateDerniereActivite(Date valeur) {
		this.dateDerniereActivite = valeur;
	}
	public boolean isJusteAuthentifier() {
		return justeAuthentifier;
	}
	public void setJusteAuthentifier(boolean valeur) {
		this.justeAuthentifier = valeur;
	}

	@Override
	public String toString() {
		return MessageFormat.format("<ticket>{0}</ticket>", signature);
	}
}
