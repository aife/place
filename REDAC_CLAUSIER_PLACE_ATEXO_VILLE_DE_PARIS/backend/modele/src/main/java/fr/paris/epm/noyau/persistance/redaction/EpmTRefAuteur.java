package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate de EpmTRefTypeDocument .
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_auteur", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefAuteur extends BaseEpmTRefRedaction {

    /**
     * Défini si le referentiel est affichable pour les clauses
     */
    private Boolean clause;
    
    /**
     * Défini si le referentiel est affichable pour les canevas
     */
    private Boolean canevas;
    
    /**
     * @return the clause
     */
    public Boolean getClause() {
        return clause;
    }

    /**
     * @param valeur the clause to set
     */
    public void setClause(final Boolean valeur) {
        this.clause = valeur;
    }

    /**
     * @return the canevas
     */
    public Boolean getCanevas() {
        return canevas;
    }

    /**
     * @param valeur the canevas to set
     */
    public void setCanevas(final Boolean valeur) {
        this.canevas = valeur;
    }

}
