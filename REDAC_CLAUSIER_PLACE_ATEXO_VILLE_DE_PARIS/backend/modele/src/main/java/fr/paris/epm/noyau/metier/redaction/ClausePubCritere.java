package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.commun.Constantes;

/**
 * Rassemble les critères de recherche des clauses pibliées.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class ClausePubCritere extends ClauseCritere {

    private Integer idClause;

    private Integer idPublication;

    public ClausePubCritere(String plateformeUuid) {
        super(plateformeUuid);
    }

    public ClausePubCritere() {
        super();
    }

    @Override
    public StringBuffer corpsRequete() {
        // assemblage de la requête
        StringBuffer sb = super.corpsRequete();

        if (idClause != null && idClause > 0) {
            sb.append(findByIdClause());
            getParametres().put("idClause", idClause);
        }
        if (idPublication != null && idPublication > 0) {
            sb.append(" AND clause.idPublication = ");
            sb.append(idPublication);
        }

        return sb;
    }

    @Override
    protected String findByIdClause() {
        return " AND clause.idClause = :idClause";
    }

    @Override
    protected String findByIdClauses() {
        return " AND clause.idClause in (:listIds)";
    }


    /*
        il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
        et ClausePubCritere ne doit pas la supporter
     */
    @Override
    protected String findByIdClauseOrigine() {
        throw new UnsupportedOperationException("ClausePubCritere ne peut pas etre surcharge, alors la recherche par 'referenceClauseSurchargee' est interdite");
    }

    /*
            il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
            car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
            et ClausePubCritere ne doit pas la supporter
         */
    @Override
    protected String nonAfficherSurcharge() {
        throw new UnsupportedOperationException("ClausePubCritere ne peut pas etre surcharge, alors la recherche par 'nonAfficherSurcharge' est interdite");
    }

    /*
        il faut redéfinir cette méthode dans ClausePubCritere et ClauseViewCritere
        car ClausePubCritere cherche dans EpmTRoleClausePub
        et ClauseViewCritere cherche dans EpmTClause.EpmTRoleClause
     */
    @Override
    protected String findByMotsCles() {
        StringBuilder sb = new StringBuilder("(");
        boolean debut = true;

        for (String motclef : getMotscles()) {
            motclef = motclef.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE);

            sb.append(debut ? "" : " OR ");
            // Chercher Le mot clé dans le contenu (texte fixe avant) de la clause
            sb.append("LOWER(clause.texteFixeAvant) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (texte fixe après) de la clause
            sb.append("LOWER(clause.texteFixeApres) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (valeur par defaut) de la clause
            sb.append("exists (from EpmTRoleClausePub role where LOWER(role.valeurDefaut) like LOWER('%").append(motclef).append("%') AND role in elements(clause.epmTRoleClauses))");

            // Chercher Le mot clé dans la liste des mots clés
            sb.append(" OR ");
            sb.append("LOWER(clause.motsCles) like LOWER('%").append(motclef).append("%')");

            debut = false;
        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    protected String getTableDeRecherche() {
        return " EpmTClausePub ";
    }

    public void setIdClause(Integer idClause) {
        this.idClause = idClause;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

}
