package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Les données a fusionner dans un document pour la demande d'achat
 * Created by nty on 07/08/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class DomaineDemandeAchat implements Serializable {
    
	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Pouvoir adjudicateur
     */
    private String pouvoirAdjudicateur;

    /**
     * Intitulé de consultation projetée 
     */
    private String intituleConsultation;

    /**
     * Montant total initial
     */
    private Double prixUnitaire;

    /**
     * Montant total initial
     */
    private Double prixForfaitaire;

    /**
     * Montant total initial
     */
    private Double prixTotal;

    /**
     * Date prévisionnelle de notification
     */
    private Date datePrevisionnelleNotification;

    /**
     * Date prévisionnelle de lancement
     */
    private Date datePrevisionnelleLancement;

    /**
     * Nom de Direction/Service Responsable
     */
    private String directionServiceResponsable;

    /**
     * Nom du responsable
     */
    private String responsableNom;

    /**
     * Prénom du responsable
     */
    private String responsablePrenom;

    /**
     * Nature (en toutes lettres) de prestations
     */
    private String nature;

    /**
     * CCAG de référence
     */
    private String ccag;

    /**
     * Type de marche (marché/AC/SAD)
     */
    private String typeMarche;

    /**
     * Attribution (mono/multi)
     */
    private String attribution;

    /**
     * Tranche (oui/non)
     */
    private String tranche;

    /**
     * Allotissement (oui/non)
     */
    private String allotissement;

    /**
     * Type de besoin ("Besoin spécifique", "Besoin récurrent", "Besoin ponctuel", "Besoin exceptionnel")
     */
    private String typeBesoin;

    /**
     * Operation travaux / Unité fonctionnelle associée
     */
    private String operationUniteFonctionnelle;

    /**
     * Code achat associé
     */
    private String codeAchat;

    /**
     * Imputation budgétaire associée
     */
    private String imputationBudgetaire;

    /**
     * Montant associé à l'année de référence pour l'Operation travaux / Unité fonctionnelle
     */
    private double montantOperationUniteFonctionnelle;

    /**
     * Montant associé à l'année de référence pour le Code Achat
     */
    private double montantCodeAchat;

    /**
     * Montant associé à l'année de référence pour l'Imputation budgétaire
     */
    private double montantImputationBudgetaire;

    /**
     * Nombre des Demandes d'achat associées à l'Opération travaux / Unité fonctionnelle
     */
    private String nombreItemsOperationUniteFonctionnelle;

    /**
     * Nombre des Demandes d'achat associées au Code Achat
     */
    private String nombreItemsCodeAchat;

    /**
     * Nombre des Demandes d'achat associées à l'Imputation Budgétaire
     */
    private String nombreItemsImputationBudgetaire;

    /**
     * Montant cumulé des Demandes d'achaté associées à l'Opération travaux / Unité fonctionnelle
     */
    private double montantItemsOperationUniteFonctionnelle;

    /**
     * Montant cumulé des Demandes d'achat associées au Code Achat
     */
    private double montantItemsCodeAchat;

    /**
     * Montant cumulé des Demandes d'achat associées à l'Imputation Budgétaire
     */
    private double montantItemsImputationBudgetaire;

    /**
     * "oui" si procédure dérogatoire, "non" sinon
     */
    private String procedureDerogatoire;

    /**
     * Type de procédure de passation
     */
    private String procedureApplicable;

    /**
     * Commentaire de derogation en cas de procédure derogatoire
     */
    private String commentaireDerogation;

    /**
     * Libellé du 1-er champ additionnel
     */
    private String libelleChampAdditionnel1;

    /**
     * Objet du 1-er champ additionnel
     */
    private String champAdditionnel1;

    /**
     * Libellé du 2-e champ additionnel
     */
    private String libelleChampAdditionnel2;

    /**
     * Objet du 2-e champ additionnel
     */
    private String champAdditionnel2;

    /**
     * Libellé du 3-e champ additionnel
     */
    private String libelleChampAdditionnel3;

    /**
     * Objet du 3-e champ additionnel
     */
    private String champAdditionnel3;

    /**
     * Libellé du 4-e champ additionnel
     */
    private String libelleChampAdditionnel4;

    /**
     * Objet du 4-e champ additionnel
     */
    private String champAdditionnel4;

    /**
     * Libellé du 5-e champ additionnel
     */
    private String libelleChampAdditionnel5;

    /**
     * Objet du 5-e champ additionnel
     */
    private String champAdditionnel5;

    /**
     * Libellé du 6-e champ additionnel
     */
    private String libelleChampAdditionnel6;

    /**
     * Objet du 6-e champ additionnel
     */
    private String champAdditionnel6;

    /**
     * Libellé du 7-e champ additionnel
     */
    private String libelleChampAdditionnel7;

    /**
     * Objet du 7-e champ additionnel
     */
    private String champAdditionnel7;

    /**
     * Libellé du 8-e champ additionnel
     */
    private String libelleChampAdditionnel8;

    /**
     * Objet du 8-e champ additionnel
     */
    private String champAdditionnel8;

    /**
     * Libellé du 9-e champ additionnel
     */
    private String libelleChampAdditionnel9;

    /**
     * Objet du 9-e champ additionnel
     */
    private String champAdditionnel9;

    /**
     * Libellé du 10-e champ additionnel
     */
    private String libelleChampAdditionnel10;

    /**
     * Objet du 10-e champ additionnel
     */
    private String champAdditionnel10;

    /**
     * Listes des contrats
     */
    private List contrats;

    private List<DomaineDemandeAchat> listeDemandesAchat;

    public String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    public void setPouvoirAdjudicateur(String pouvoirAdjudicateur) {
        this.pouvoirAdjudicateur = pouvoirAdjudicateur;
    }

    public String getIntituleConsultation() {
        return intituleConsultation;
    }

    public void setIntituleConsultation(String intituleConsultation) {
        this.intituleConsultation = intituleConsultation;
    }

    public Double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(Double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Double getPrixForfaitaire() {
        return prixForfaitaire;
    }

    public void setPrixForfaitaire(Double prixForfaitaire) {
        this.prixForfaitaire = prixForfaitaire;
    }

    public Double getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Double prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Date getDatePrevisionnelleNotification() {
        return datePrevisionnelleNotification;
    }

    public void setDatePrevisionnelleNotification(Date datePrevisionnelleNotification) {
        this.datePrevisionnelleNotification = datePrevisionnelleNotification;
    }

    public int getAnneePrevisionnelleNotification() {
        Calendar calendar = Calendar.getInstance();
        if (datePrevisionnelleNotification != null)
            calendar.setTime(datePrevisionnelleNotification);
        return calendar.get(Calendar.YEAR);
    }

    public Date getDatePrevisionnelleLancement() {
        return datePrevisionnelleLancement;
    }

    public void setDatePrevisionnelleLancement(Date datePrevisionnelleLancement) {
        this.datePrevisionnelleLancement = datePrevisionnelleLancement;
    }

    public int getAnneePrevisionnelleLancement() {
        Calendar calendar = Calendar.getInstance();
        if (datePrevisionnelleLancement != null)
            calendar.setTime(datePrevisionnelleLancement);
        return calendar.get(Calendar.YEAR);
    }

    public String getDirectionServiceResponsable() {
        return directionServiceResponsable;
    }

    public void setDirectionServiceResponsable(String directionServiceResponsable) {
        this.directionServiceResponsable = directionServiceResponsable;
    }

    public String getResponsableNom() {
        return responsableNom;
    }

    public void setResponsableNom(String responsableNom) {
        this.responsableNom = responsableNom;
    }

    public String getResponsablePrenom() {
        return responsablePrenom;
    }

    public void setResponsablePrenom(String responsablePrenom) {
        this.responsablePrenom = responsablePrenom;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getCcag() {
        return ccag;
    }

    public void setCcag(String ccag) {
        this.ccag = ccag;
    }

    public String getTypeMarche() {
        return typeMarche;
    }

    public void setTypeMarche(String typeMarche) {
        this.typeMarche = typeMarche;
    }

    public String getAttribution() {
        return attribution;
    }

    public void setAttribution(String attribution) {
        this.attribution = attribution;
    }

    public String getTranche() {
        return tranche;
    }

    public void setTranche(String tranche) {
        this.tranche = tranche;
    }

    public String getAllotissement() {
        return allotissement;
    }

    public void setAllotissement(String allotissement) {
        this.allotissement = allotissement;
    }

    public String getTypeBesoin() {
        return typeBesoin;
    }

    public void setTypeBesoin(String typeBesoin) {
        this.typeBesoin = typeBesoin;
    }

    public String getOperationUniteFonctionnelle() {
        return operationUniteFonctionnelle;
    }

    public void setOperationUniteFonctionnelle(String operationUniteFonctionnelle) {
        this.operationUniteFonctionnelle = operationUniteFonctionnelle;
    }

    public String getCodeAchat() {
        return codeAchat;
    }

    public void setCodeAchat(String codeAchat) {
        this.codeAchat = codeAchat;
    }

    public String getImputationBudgetaire() {
        return imputationBudgetaire;
    }

    public void setImputationBudgetaire(String imputationBudgetaire) {
        this.imputationBudgetaire = imputationBudgetaire;
    }

    public double getMontantOperationUniteFonctionnelle() {
        return montantOperationUniteFonctionnelle;
    }

    public void setMontantOperationUniteFonctionnelle(double montantOperationUniteFonctionnelle) {
        this.montantOperationUniteFonctionnelle = montantOperationUniteFonctionnelle;
    }

    public double getMontantCodeAchat() {
        return montantCodeAchat;
    }

    public void setMontantCodeAchat(double montantCodeAchat) {
        this.montantCodeAchat = montantCodeAchat;
    }

    public double getMontantImputationBudgetaire() {
        return montantImputationBudgetaire;
    }

    public void setMontantImputationBudgetaire(double montantImputationBudgetaire) {
        this.montantImputationBudgetaire = montantImputationBudgetaire;
    }

    public String getNombreItemsOperationUniteFonctionnelle() {
        return nombreItemsOperationUniteFonctionnelle;
    }

    public void setNombreItemsOperationUniteFonctionnelle(String nombreItemsOperationUniteFonctionnelle) {
        this.nombreItemsOperationUniteFonctionnelle = nombreItemsOperationUniteFonctionnelle;
    }

    public String getNombreItemsCodeAchat() {
        return nombreItemsCodeAchat;
    }

    public void setNombreItemsCodeAchat(String nombreItemsCodeAchat) {
        this.nombreItemsCodeAchat = nombreItemsCodeAchat;
    }

    public String getNombreItemsImputationBudgetaire() {
        return nombreItemsImputationBudgetaire;
    }

    public void setNombreItemsImputationBudgetaire(String nombreItemsImputationBudgetaire) {
        this.nombreItemsImputationBudgetaire = nombreItemsImputationBudgetaire;
    }

    public double getMontantItemsOperationUniteFonctionnelle() {
        return montantItemsOperationUniteFonctionnelle;
    }

    public void setMontantItemsOperationUniteFonctionnelle(double montantItemsOperationUniteFonctionnelle) {
        this.montantItemsOperationUniteFonctionnelle = montantItemsOperationUniteFonctionnelle;
    }

    public double getMontantItemsCodeAchat() {
        return montantItemsCodeAchat;
    }

    public void setMontantItemsCodeAchat(double montantItemsCodeAchat) {
        this.montantItemsCodeAchat = montantItemsCodeAchat;
    }

    public double getMontantItemsImputationBudgetaire() {
        return montantItemsImputationBudgetaire;
    }

    public void setMontantItemsImputationBudgetaire(double montantItemsImputationBudgetaire) {
        this.montantItemsImputationBudgetaire = montantItemsImputationBudgetaire;
    }

    public String getProcedureDerogatoire() {
        return procedureDerogatoire;
    }

    public void setProcedureDerogatoire(String procedureDerogatoire) {
        this.procedureDerogatoire = procedureDerogatoire;
    }

    public String getProcedureApplicable() {
        return procedureApplicable;
    }

    public void setProcedureApplicable(String procedureApplicable) {
        this.procedureApplicable = procedureApplicable;
    }

    public String getCommentaireDerogation() {
        return commentaireDerogation;
    }

    public void setCommentaireDerogation(String commentaireDerogation) {
        this.commentaireDerogation = commentaireDerogation;
    }

    public String getLibelleChampAdditionnel1() {
        return libelleChampAdditionnel1;
    }

    public void setLibelleChampAdditionnel1(String libelleChampAdditionnel1) {
        this.libelleChampAdditionnel1 = libelleChampAdditionnel1;
    }

    public String getChampAdditionnel1() {
        return champAdditionnel1;
    }

    public void setChampAdditionnel1(String champAdditionnel1) {
        this.champAdditionnel1 = champAdditionnel1;
    }

    public String getLibelleChampAdditionnel2() {
        return libelleChampAdditionnel2;
    }

    public void setLibelleChampAdditionnel2(String libelleChampAdditionnel2) {
        this.libelleChampAdditionnel2 = libelleChampAdditionnel2;
    }

    public String getChampAdditionnel2() {
        return champAdditionnel2;
    }

    public void setChampAdditionnel2(String champAdditionnel2) {
        this.champAdditionnel2 = champAdditionnel2;
    }

    public String getLibelleChampAdditionnel3() {
        return libelleChampAdditionnel3;
    }

    public void setLibelleChampAdditionnel3(String libelleChampAdditionnel3) {
        this.libelleChampAdditionnel3 = libelleChampAdditionnel3;
    }

    public String getChampAdditionnel3() {
        return champAdditionnel3;
    }

    public void setChampAdditionnel3(String champAdditionnel3) {
        this.champAdditionnel3 = champAdditionnel3;
    }

    public String getLibelleChampAdditionnel4() {
        return libelleChampAdditionnel4;
    }

    public void setLibelleChampAdditionnel4(String libelleChampAdditionnel4) {
        this.libelleChampAdditionnel4 = libelleChampAdditionnel4;
    }

    public String getChampAdditionnel4() {
        return champAdditionnel4;
    }

    public void setChampAdditionnel4(String champAdditionnel4) {
        this.champAdditionnel4 = champAdditionnel4;
    }

    public String getLibelleChampAdditionnel5() {
        return libelleChampAdditionnel5;
    }

    public void setLibelleChampAdditionnel5(String libelleChampAdditionnel5) {
        this.libelleChampAdditionnel5 = libelleChampAdditionnel5;
    }

    public String getChampAdditionnel5() {
        return champAdditionnel5;
    }

    public void setChampAdditionnel5(String champAdditionnel5) {
        this.champAdditionnel5 = champAdditionnel5;
    }

    public String getLibelleChampAdditionnel6() {
        return libelleChampAdditionnel6;
    }

    public void setLibelleChampAdditionnel6(String libelleChampAdditionnel6) {
        this.libelleChampAdditionnel6 = libelleChampAdditionnel6;
    }

    public String getChampAdditionnel6() {
        return champAdditionnel6;
    }

    public void setChampAdditionnel6(String champAdditionnel6) {
        this.champAdditionnel6 = champAdditionnel6;
    }

    public String getLibelleChampAdditionnel7() {
        return libelleChampAdditionnel7;
    }

    public void setLibelleChampAdditionnel7(String libelleChampAdditionnel7) {
        this.libelleChampAdditionnel7 = libelleChampAdditionnel7;
    }

    public String getChampAdditionnel7() {
        return champAdditionnel7;
    }

    public void setChampAdditionnel7(String champAdditionnel7) {
        this.champAdditionnel7 = champAdditionnel7;
    }

    public String getLibelleChampAdditionnel8() {
        return libelleChampAdditionnel8;
    }

    public void setLibelleChampAdditionnel8(String libelleChampAdditionnel8) {
        this.libelleChampAdditionnel8 = libelleChampAdditionnel8;
    }

    public String getChampAdditionnel8() {
        return champAdditionnel8;
    }

    public void setChampAdditionnel8(String champAdditionnel8) {
        this.champAdditionnel8 = champAdditionnel8;
    }

    public String getLibelleChampAdditionnel9() {
        return libelleChampAdditionnel9;
    }

    public void setLibelleChampAdditionnel9(String libelleChampAdditionnel9) {
        this.libelleChampAdditionnel9 = libelleChampAdditionnel9;
    }

    public String getChampAdditionnel9() {
        return champAdditionnel9;
    }

    public void setChampAdditionnel9(String champAdditionnel9) {
        this.champAdditionnel9 = champAdditionnel9;
    }

    public String getLibelleChampAdditionnel10() {
        return libelleChampAdditionnel10;
    }

    public void setLibelleChampAdditionnel10(String libelleChampAdditionnel10) {
        this.libelleChampAdditionnel10 = libelleChampAdditionnel10;
    }

    public String getChampAdditionnel10() {
        return champAdditionnel10;
    }

    public void setChampAdditionnel10(String champAdditionnel10) {
        this.champAdditionnel10 = champAdditionnel10;
    }

    public List getContrats() {
        return contrats;
    }

    public void setContrats(List contrats) {
        this.contrats = contrats;
    }

    public List<DomaineDemandeAchat> getListeDemandesAchat() {
        return listeDemandesAchat;
    }

    public void setListeDemandesAchat(List<DomaineDemandeAchat> listeDemandesAchat) {
        this.listeDemandesAchat = listeDemandesAchat;
    }

    // Code de traitement de la liste des demandes d'achat à coté de EXCEL
    /*

    <documentXLS>
	<onglet>
		<nom>Export</nom>
		<#assign numeroLigne = 4>
		<#if (racine.demandeAchat.listeDemandesAchat)??>
			<#list racine.demandeAchat.listeDemandesAchat as note>
				<#if note??>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="0" ligne="${numeroLigne}"/>
						<valeur><#if note.pouvoirAdjudicateur??>${note.pouvoirAdjudicateur}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="1" ligne="${numeroLigne}"/>
						<valeur><#if note.intituleConsultation??>${note.intituleConsultation}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="2" ligne="${numeroLigne}"/>
						<valeur><#if note.prixUnitaire??>${note.prixUnitaire?string(",##0.00")}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="3" ligne="${numeroLigne}"/>
						<valeur><#if note.prixForfaitaire??>${note.prixForfaitaire?string(",##0.00")}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="4" ligne="${numeroLigne}"/>
						<valeur><#if note.prixTotal??>${note.prixTotal?string(",##0.00")}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="5" ligne="${numeroLigne}"/>
						<valeur><#if note.datePrevisionnelleNotification??>${note.datePrevisionnelleNotification?string("yyyy")}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="6" ligne="${numeroLigne}"/>
						<valeur><#if note.datePrevisionnelleLancement??>${note.datePrevisionnelleLancement?string("yyyy")}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="7" ligne="${numeroLigne}"/>
						<valeur><#if note.directionServiceResponsable??>${note.directionServiceResponsable}<#else>Tous</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="8" ligne="${numeroLigne}"/>
						<valeur><#if note.responsablePrenom??>${note.responsablePrenom}</#if> <#if note.responsableNom??>${note.responsableNom}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="9" ligne="${numeroLigne}"/>
						<valeur><#if note.nature??>${note.nature?substring(0,1)}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="10" ligne="${numeroLigne}"/>
						<valeur><#if note.ccag??>${note.ccag}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="11" ligne="${numeroLigne}"/>
						<valeur><#if note.typeMarche??>${note.typeMarche}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="12" ligne="${numeroLigne}"/>
						<valeur><#if note.allotissement??>${note.allotissement}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="13" ligne="${numeroLigne}"/>
						<valeur><#if note.tranche??>${note.tranche}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="14" ligne="${numeroLigne}"/>
						<valeur><#if note.typeBesoin??>${note.typeBesoin}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="15" ligne="${numeroLigne}"/>
						<valeur><#if note.operationUniteFonctionnelle??>${note.operationUniteFonctionnelle}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="16" ligne="${numeroLigne}"/>
						<valeur><#if note.codeAchat??>${note.codeAchat}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="17" ligne="${numeroLigne}"/>
						<valeur><#if note.anneePrevisionnelleNotification??>${note.anneePrevisionnelleNotification}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="18" ligne="${numeroLigne}"/>
						<valeur><#if note.montantOperationUniteFonctionnelle??>${note.montantOperationUniteFonctionnelle}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="19" ligne="${numeroLigne}"/>
						<valeur><#if note.nombreItemsOperationUniteFonctionnelle??>${note.nombreItemsOperationUniteFonctionnelle}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="20" ligne="${numeroLigne}"/>
						<valeur><#if note.montantItemsOperationUniteFonctionnelle??>${note.montantItemsOperationUniteFonctionnelle}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="21" ligne="${numeroLigne}"/>
						<valeur><#if note.procedureApplicable??>${note.procedureApplicable}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="22" ligne="${numeroLigne}"/>
						<valeur><#if note.montantImputationBudgetaire??>${note.montantImputationBudgetaire}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="23" ligne="${numeroLigne}"/>
						<valeur><#if note.nombreItemsImputationBudgetaire??>${note.nombreItemsImputationBudgetaire}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="24" ligne="${numeroLigne}"/>
						<valeur><#if note.montantItemsImputationBudgetaire??>${note.montantItemsImputationBudgetaire}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="25" ligne="${numeroLigne}"/>
						<valeur><#if note.procedureApplicable??>${note.procedureApplicable}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="26" ligne="${numeroLigne}"/>
						<valeur><#if  note.procedureDerogatoire??>${note.procedureDerogatoire}</#if></valeur>
					</cellule>
					<cellule bordureDroite="" bordureBas="">
						<coordonne colonne="27" ligne="${numeroLigne}"/>
						<valeur><#if note.commentaireDerogation??>${note.commentaireDerogation}</#if></valeur>
					</cellule>

					<#if note.contrats??>
						<#list note.contrats as contrat>
							<#if contrat??>
								<cellule bordureGauche="" bordureDroite="" bordureBas="">
									<coordonne colonne="28" ligne="${numeroLigne}"/>
									<valeur><#if contrat.numero??>${contrat.numero}</#if></valeur>
								</cellule>
								<cellule bordureDroite="" bordureBas="">
									<coordonne colonne="29" ligne="${numeroLigne}"/>
									<valeur><#if contrat.libelle??>${contrat.libelle}<#else>Tous</#if></valeur>
								</cellule>
								<cellule bordureDroite="" bordureBas="">
									<coordonne colonne="30" ligne="${numeroLigne}"/>
									<valeur><#if contrat.dateDebut??>${contrat.dateDebut?string("yyyy")}</#if></valeur>
								</cellule>
								<cellule bordureDroite="" bordureBas="">
									<coordonne colonne="31" ligne="${numeroLigne}"/>
									<valeur><#if contrat.dateFin??>${contrat.dateFin?string("yyyy")}</#if></valeur>
								</cellule>
								<cellule bordureDroite="" bordureBas="">
									<coordonne colonne="32" ligne="${numeroLigne}"/>
									<valeur><#if contrat.montant??>${contrat.montant?string(",##0.00")}</#if></valeur>
								</cellule>
							</#if>
							<#assign numeroLigne = numeroLigne + 1>
						</#list>
					</#if>
				</#if>
				<#assign numeroLigne = numeroLigne + 1>
			</#list>
		</#if>
	</onglet>
</documentXLS>
    */

}
