package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class EpmTRoleClauseAbstract extends EpmTAbstractObject implements Comparable<EpmTRoleClauseAbstract>, Cloneable, Serializable {

    /**
     * Rôle activé.
     */
    public static final String ROLE_ACTIVE = "1";

    /**
     * Rôle désactivé.
     */
    public static final String ROLE_INACTIVE = "0";
    
    /**
     * attribut identificateur de direction.
     */
    @Column(name="id_direction_service")
    private Integer idDirectionService;
    
    /**
     * attribut identificateur d'agent.
     */
    @Column(name="id_utilisateur")
    private Integer idUtilisateur;
    
    /**
     * attribut valeur par defaut.
     */
    @Column(name="valeur_defaut")
    private String valeurDefaut;

    public boolean isPrecochee() {
        return precochee;
    }

    public void setPrecochee(boolean precochee) {
        this.precochee = precochee;
    }

    /**
     * attribut precoche.
     */
    private boolean precochee;

    /**
     * attribut etat.
     */
    private String etat;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_preference", nullable=true)
    private EpmTPreference preference;

    /**
     * attribut nombre_carateres_max
     */
    @Column(name="nombre_carateres_max")
    private Integer nombreCarateresMax;

    /**
     * attribut champ Obligatoire .
     */
    @Column(name="champ_obligatoire")
    private String champObligatoire;

    /**
     * attribut valeur heritée modifiable.
     */
    @Column(name="valeur_heritee_modifiable")
    private String valeurHeriteeModifiable;

    /**
     * attribut numéro de formulation.
     */
    @Column(name="num_formulation")
    private Integer numFormulation;

    public abstract Integer getIdRoleClause();

    public abstract Integer getIdPublication();

    public abstract EpmTClauseAbstract getEpmTClause() ;

    public abstract void setEpmTClause(EpmTClauseAbstract epmTClause) ;

    public Integer getIdDirectionService() {
        return idDirectionService;
    }

    public void setIdDirectionService(Integer idDirectionService) {
        this.idDirectionService = idDirectionService;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }





    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public EpmTPreference getPreference() {
        return preference;
    }

    public void setPreference(EpmTPreference preference) {
        this.preference = preference;
    }

    public Integer getNombreCarateresMax() {
        return nombreCarateresMax;
    }

    public void setNombreCarateresMax(Integer nombreCarateresMax) {
        this.nombreCarateresMax = nombreCarateresMax;
    }

    public String getChampObligatoire() {
        return champObligatoire;
    }

    public void setChampObligatoire(String champObligatoire) {
        this.champObligatoire = champObligatoire;
    }

    public String getValeurHeriteeModifiable() {
        return valeurHeriteeModifiable;
    }

    public void setValeurHeriteeModifiable(String valeurHeriteeModifiable) {
        this.valeurHeriteeModifiable = valeurHeriteeModifiable;
    }

    public Integer getNumFormulation() {
        return numFormulation;
    }

    public void setNumFormulation(Integer numFormulation) {
        this.numFormulation = numFormulation;
    }

    public int compareTo(final EpmTRoleClauseAbstract epmTRoleClauseAbstract) {
        if (epmTRoleClauseAbstract.getNumFormulation() != null && this.getNumFormulation() != null) {
            return this.getNumFormulation().compareTo(epmTRoleClauseAbstract.getNumFormulation());
        } else {
            return -1;
        }
    }

    public EpmTRoleClauseAbstract clone() throws CloneNotSupportedException {
        EpmTRoleClauseAbstract nouveau = (EpmTRoleClauseAbstract) super.clone();
        nouveau.idUtilisateur = null;
        nouveau.idDirectionService = null;
        return nouveau;
    }

}
