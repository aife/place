package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;

public class ClauseValeurPotentiellementConditionneePubCritere extends AbstractCritere {

    private Integer idPublication;

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        if (idPublication != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" conditionValeur.idPublication = ");
            sb.append(idPublication);
            debut = true;
        }
        return sb;
    }

    @Override
    public String toHQL() {
        StringBuilder sb = new StringBuilder("select conditionValeur from EpmTClauseValeurPotentiellementConditionneePub as conditionValeur ");
        sb.append(corpsRequete());
        if (getProprieteTriee() != null) {
            sb.append(" ORDER BY conditionValeur.").append(getProprieteTriee());
            sb.append(isTriCroissant() ? " ASC" : " DESC");
        }
        return sb.toString();
    }

    @Override
    public String toCountHQL() {
        return "select count(conditionValeur.id) from EpmTClauseValeurPotentiellementConditionneePub as conditionValeur " + corpsRequete();
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

}