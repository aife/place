package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import fr.paris.epm.noyau.persistance.EpmTAttributaire;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour les sous domaines
 * attribution.confirmation.attributaire.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineAttributaire implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    private String raisonSocial;
    
    private String siret;

    private String nom;

    private Integer rang;

    public DomaineAttributaire() {
    }
    public DomaineAttributaire(EpmTAttributaire attributaire) {

        this.raisonSocial = attributaire.getRaisonSocial();
        this.siret = attributaire.getSiret();
        this.nom = attributaire.getRaisonSocial();
    }

    public final String getNom() {
        return nom;
    }

    public final void setNom(final String valeur) {
        this.nom = valeur;
    }

    public final Integer getRang() {
        return rang;
    }

    public final void setRang(final Integer valeur) {
        this.rang = valeur;
    }

    /**
     * @return the raisonSocial
     */
    public final String getRaisonSocial() {
        return raisonSocial;
    }

    /**
     * @param raisonSocial the raisonSocial to set
     */
    public final void setRaisonSocial(final String valeur) {
        this.raisonSocial = valeur;
    }

    /**
     * @return the siret
     */
    public final String getSiret() {
        return siret;
    }

    /**
     * @param siret the siret to set
     */
    public final void setSiret(final String valeur) {
        this.siret = valeur;
    }
    
    
}
