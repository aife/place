package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Set;

/**
 * Created by mca on 20/06/2017.
 */
@Entity
@Table(name = "epm__t_ref_type_contrat_editeur", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTRefTypeContrat extends BaseEpmTRefRedaction implements EpmTRefImportExport {

    @Column(name = "avenant_active")
    private boolean avenantActive;

    @Column(name = "abreviation_type_contrat")
    private String abreviation;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(schema = "redaction", name = "epm__t_type_contrat_has_procedure",
            joinColumns = @JoinColumn(name = "id_type_contrat", referencedColumnName = "id"))
    @Column(name = "id_procedure")
    private Set<Integer> idsProcedures;

    public boolean isAvenantActive() {
        return avenantActive;
    }

    public void setAvenantActive(boolean avenantActive) {
        this.avenantActive = avenantActive;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public Set<Integer> getIdsProcedures() {
        return idsProcedures;
    }

    public void setIdsProcedures(Set<Integer> idsProcedures) {
        this.idsProcedures = idsProcedures;
    }

	@Override
	public String toString() {
		return MessageFormat.format("Type de contrat #{0,number,#} (libellé = {1}, code externe = {2}, libellé court = {3})",
			this.getId(), this.getLibelle(), this.getCodeExterne(), this.getLibelleCourt());
	}

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
