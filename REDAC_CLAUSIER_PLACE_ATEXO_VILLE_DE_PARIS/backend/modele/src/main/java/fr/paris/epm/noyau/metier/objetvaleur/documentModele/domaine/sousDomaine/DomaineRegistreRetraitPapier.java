package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine entreprises.registreRetrait
 * des retraits papier.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreRetraitPapier extends AbstractDomaineRegistreRetrait {
	
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	private String adresseRetraitPapier;
	
	private String codePostalRetraitPapier;
	
	private String villeRetraitPapier;
	
	private String telephoneRetraitPapier;
	
	private String faxRetraitPapier;
	
	private String modalitesRetraitPapier;
	
	private String modalitesRetraitEchantillonPapier;
	
	private String nomContactRetraitPapier;
	
	private String prenomRetraitPapier;
	
	private Date dateRetrait;
	
	private String entrepriseRetraitPapier;

	private String mailRetraitPapier;
	
	private String sirenRetraitPapier;
	
	private Date dateRetraitEchantillonsPapier;
	
	public final String getAdresseRetraitPapier() {
		return adresseRetraitPapier;
	}

	public final void setAdresseRetraitPapier(final String valeur) {
		this.adresseRetraitPapier = valeur;
	}

	public final String getCodePostalRetraitPapier() {
		return codePostalRetraitPapier;
	}

	public final void setCodePostalRetraitPapier(final String valeur) {
		this.codePostalRetraitPapier = valeur;
	}

	public final String getVilleRetraitPapier() {
		return villeRetraitPapier;
	}

	public final void setVilleRetraitPapier(final String valeur) {
		this.villeRetraitPapier = valeur;
	}

	public final String getTelephoneRetraitPapier() {
		return telephoneRetraitPapier;
	}

	public final void setTelephoneRetraitPapier(final String valeur) {
		this.telephoneRetraitPapier = valeur;
	}

	public final String getFaxRetraitPapier() {
		return faxRetraitPapier;
	}

	public final void setFaxRetraitPapier(final String valeur) {
		this.faxRetraitPapier = valeur;
	}

	public final String getModalitesRetraitPapier() {
		return modalitesRetraitPapier;
	}

	public final void setModalitesRetraitPapier(final String valeur) {
		this.modalitesRetraitPapier = valeur;
	}

	public final String getModalitesRetraitEchantillonPapier() {
		return modalitesRetraitEchantillonPapier;
	}

	public final void setModalitesRetraitEchantillonPapier(final
			String valeur) {
		this.modalitesRetraitEchantillonPapier = valeur;
	}

    public final String getNomContactRetraitPapier() {
        return nomContactRetraitPapier;
    }

    public final void setNomContactRetraitPapier(final String valeur) {
        this.nomContactRetraitPapier = valeur;
    }

    public final String getPrenomRetraitPapier() {
        return prenomRetraitPapier;
    }

    public final void setPrenomRetraitPapier(final String valeur) {
        this.prenomRetraitPapier = valeur;
    }

	/**
	 * @return the dateRetrait
	 */
	public final Date getDateRetrait() {
		return dateRetrait;
	}

	/**
	 * @param valeur the dateRetrait to set
	 */
	public final void setDateRetrait(final Date valeur) {
		this.dateRetrait = valeur;
	}

	/**
	 * @return the entrepriseRetraitPapier
	 */
	public final String getEntrepriseRetraitPapier() {
		return entrepriseRetraitPapier;
	}

	/**
	 * @param valeur the entrepriseRetraitPapier to set
	 */
	public final void setEntrepriseRetraitPapier(final String valeur) {
		this.entrepriseRetraitPapier = valeur;
	}

	/**
	 * @return the mailRetraitPapier
	 */
	public final String getMailRetraitPapier() {
		return mailRetraitPapier;
	}

	/**
	 * @param valeur the mailRetraitPapier to set
	 */
	public final void setMailRetraitPapier(final String valeur) {
		this.mailRetraitPapier = valeur;
	}

	/**
	 * @return the sirenRetraitPapier
	 */
	public final String getSirenRetraitPapier() {
		return sirenRetraitPapier;
	}

	/**
	 * @param valeur the sirenRetraitPapier to set
	 */
	public final void setSirenRetraitPapier(final String valeur) {
		this.sirenRetraitPapier = valeur;
	}

	/**
	 * @return the dateRetraitEchantillonsPapier
	 */
	public final Date getDateRetraitEchantillonsPapier() {
		return dateRetraitEchantillonsPapier;
	}

	/**
	 * @param valeur the dateRetraitEchantillonsPapier to set
	 */
	public final void setDateRetraitEchantillonsPapier(
			final Date valeur) {
		this.dateRetraitEchantillonsPapier = valeur;
	}
}
