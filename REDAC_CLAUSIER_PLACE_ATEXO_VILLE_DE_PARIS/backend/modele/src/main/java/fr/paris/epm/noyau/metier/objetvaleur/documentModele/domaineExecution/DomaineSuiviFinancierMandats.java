package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine suivi des mandats
 * @author GAO Xuesong
 */
public class DomaineSuiviFinancierMandats implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Référence du mandat
	 */
	private String referenceMandat; 

	/**
	 * Date de mandatement
	 */
	private Date dateMandat; 
	
	/**
	 * Date de paiement du mandat
	 */
	private Date datePaiementMandat; 
	
	/**
	 * Montant TTC du mandat
	 */
	private Double montantTTCMandat;
	
	
	public final String getReferenceMandat() {
		return referenceMandat;
	}

	public final void setReferenceMandat(final String valeur) {
		this.referenceMandat = valeur;
	}

	public final Date getDateMandat() {
		return dateMandat;
	}

	public final void setDateMandat(final Date valeur) {
		this.dateMandat = valeur;
	}

	public final Date getDatePaiementMandat() {
		return datePaiementMandat;
	}

	public final void setDatePaiementMandat(final Date valeur) {
		this.datePaiementMandat = valeur;
	}

	public final Double getMontantTTCMandat() {
		return montantTTCMandat;
	}

	public final void setMontantTTCMandat(final Double valeur) {
		this.montantTTCMandat = valeur;
	}

}
