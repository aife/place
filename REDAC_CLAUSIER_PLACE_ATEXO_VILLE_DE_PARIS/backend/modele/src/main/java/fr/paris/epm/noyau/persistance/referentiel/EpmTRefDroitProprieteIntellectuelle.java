package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDroitProprieteIntellectuelle de la table "epm__t_ref_droit_propriete_intellectuelle"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDroitProprieteIntellectuelle extends BaseEpmTRefReferentiel {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_OPTION_A = 1;
    public static final int ID_OPTION_B = 2;
    public static final int ID_OPTION_DEROG = 3;

}
