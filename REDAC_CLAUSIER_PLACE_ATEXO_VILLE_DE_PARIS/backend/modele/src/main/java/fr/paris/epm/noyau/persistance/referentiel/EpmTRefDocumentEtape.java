package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDocumentEtape de la table "epm__t_ref_document_etape"
 * Referentiel permettant de paramétrer l'étape d'avancement de la consultation
 * pour laquelle le document sera disponible à la génération.
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDocumentEtape extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Desciption de l'étape pour laquelle le document sera disponible à la
     * génération.
     */
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}