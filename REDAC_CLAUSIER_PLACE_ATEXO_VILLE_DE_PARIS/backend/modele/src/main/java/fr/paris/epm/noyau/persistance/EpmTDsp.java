package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefArrondissement;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefAutoriteDelegante;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeConsultation;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeDsp;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Classe contenant l'ensemble des propriétés spécifique à une DSP.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class EpmTDsp extends EpmTAbstractObject implements Cloneable {
    /**
     * Marqueur de sérialization.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * commentaires internes.
     */
    private String commentairesInternes;

    /**
     * renouvellemenent existante.
     */
    private boolean renouvellementExistante;

    /**
     * renouvellement à l'identique.
     */
    private boolean renouvellementIdentique;

    /**
     * projet localisé.
     */
    private boolean projetLocalise;

    /**
     * equipement à proximité.
     */
    private boolean equipementProximite;

    /**
     * date valeur.
     */
    private Calendar dateValeur;

    /**
     * estimation.
     */
    private double estimation;

    /**
     * Autorité délégante.
     */
    private EpmTRefAutoriteDelegante epmTRefAutoriteDelegante;

    /**
     * type de DSP.
     */
    private EpmTRefTypeDsp epmTRefTypeDsp;

    /**
     * type de consultation.
     */
    private EpmTRefTypeConsultation epmTRefTypeConsultation;

    /**
     * ensemble des arrondissements associé à la DSP.
     */
    private Set arrondissements;
    
    /**
     * Identifiant de l'organisme auquel l'avenant est rataché
     */
    private Integer idOrganisme;

    /**
     * @return identifiant du bloc DSP
     */
    public int getId() {
        return id;
    }

    /**
     * @param id identifiant du bloc DSP.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return commentaires internes
     */
    public String getCommentairesInternes() {
        return commentairesInternes;
    }

    /**
     * @param valeur commentaires internes
     */
    public void setCommentairesInternes(final String valeur) {
        this.commentairesInternes = valeur;
    }

    /**
     * @return renouvellement existante
     */
    public boolean isRenouvellementExistante() {
        return renouvellementExistante;
    }

    /**
     * @param valeur renouvellement existante.
     */
    public void setRenouvellementExistante(final boolean valeur) {
        this.renouvellementExistante = valeur;
    }

    /**
     * @return renouvellement à l'identique
     */
    public boolean isRenouvellementIdentique() {
        return renouvellementIdentique;
    }

    /**
     * @param valeur renouvellement à l'identique
     */
    public void setRenouvellementIdentique(final boolean valeur) {
        this.renouvellementIdentique = valeur;
    }

    /**
     * @return projet localise
     */
    public boolean isProjetLocalise() {
        return projetLocalise;
    }

    /**
     * @param valeur projet loicalise
     */
    public void setProjetLocalise(final boolean valeur) {
        this.projetLocalise = valeur;
    }

    /**
     * @return equipement à proximité
     */
    public boolean isEquipementProximite() {
        return equipementProximite;
    }

    /**
     * @param valeur equipement à proximité
     */
    public void setEquipementProximite(final boolean valeur) {
        this.equipementProximite = valeur;
    }

    /**
     * @return estimation
     */
    public double getEstimation() {
        return estimation;
    }

    /**
     * @param valeur estimation
     */
    public void setEstimation(final double valeur) {
        this.estimation = valeur;
    }

    /**
     * @return autorité délégante
     */
    public EpmTRefAutoriteDelegante getEpmTRefAutoriteDelegante() {
        return epmTRefAutoriteDelegante;
    }

    /**
     * @param valeur autorité délégante
     */
    public void setEpmTRefAutoriteDelegante(
            final EpmTRefAutoriteDelegante valeur) {
        this.epmTRefAutoriteDelegante = valeur;
    }

    /**
     * @return type de DSP
     */
    public EpmTRefTypeDsp getEpmTRefTypeDsp() {
        return epmTRefTypeDsp;
    }

    /**
     * @param valeur type de DSP
     */
    public void setEpmTRefTypeDsp(final EpmTRefTypeDsp valeur) {
        this.epmTRefTypeDsp = valeur;
    }

    /**
     * @return type de consultation.
     */
    public EpmTRefTypeConsultation getEpmTRefTypeConsultation() {
        return epmTRefTypeConsultation;
    }

    /**
     * @param valeur type de consultation
     */
    public void setEpmTRefTypeConsultation(final EpmTRefTypeConsultation valeur) {
        this.epmTRefTypeConsultation = valeur;
    }

    /**
     * @return DSP cloné
     * @throws CloneNotSupportedException probleme lors du clonage de la DSP
     */
    public Object clone() throws CloneNotSupportedException {
        EpmTDsp dsp = new EpmTDsp();
        dsp.setCommentairesInternes(commentairesInternes);
        dsp.setRenouvellementExistante(renouvellementExistante);
        dsp.setRenouvellementIdentique(renouvellementIdentique);
        dsp.setEquipementProximite(equipementProximite);
        dsp.setDateValeur(dateValeur);
        dsp.setEstimation(estimation);

        if (arrondissements != null) {
            Iterator it = arrondissements.iterator();
            Set tmp = new HashSet();
            while (it.hasNext()) {
                EpmTRefArrondissement arrondissement =
                    (EpmTRefArrondissement) it.next();
                tmp.add(arrondissement);
            }
            dsp.setArrondissements(tmp);
        }
        dsp.setId(0);
        dsp.setEpmTRefAutoriteDelegante(epmTRefAutoriteDelegante);
        dsp.setEpmTRefTypeConsultation(epmTRefTypeConsultation);
        dsp.setEpmTRefTypeDsp(epmTRefTypeDsp);
        return dsp;
    }

    /**
     * @return date valeur
     */
    public Calendar getDateValeur() {
        return dateValeur;
    }

    /**
     * @param dateValeur date valeur
     */
    public void setDateValeur(final Calendar dateValeur) {
        this.dateValeur = dateValeur;
    }

    /**
     * @return arrondissment lié à la DSP
     */
    public Set getArrondissements() {
        return arrondissements;
    }

    /**
     * @param valeur arrondissement lié à la DSP
     */
    public void setArrondissements(final Set valeur) {
        this.arrondissements = valeur;
    }

    /**
     * @return the idOrganisme
     */
    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }
    
}
