package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutNotificationContrat de la table "epm__t_ref_statut_notification_contrat"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutNotificationContrat extends EpmTReferentielExterneAbstract {

    /**
     * Mqraueur de sérailisation
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_STATUT_CONTRAT_PROVISOIRE = 1;
    public static final int ID_STATUT_CONTRAT_NOTIFIE = 2;

}