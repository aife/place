package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Classe critère utilisé pour la recherche des profils des utilisateurs.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class UtilisateurCritere extends AbstractCritere implements Critere, Serializable {

    public UtilisateurCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * GUID renvoyé par le WWSO.
     */
    private Collection<String> guidsSso;

    private String guidSso;

    private boolean guidSsoStrict = false;

    /**
     * nom de l'utilisateur.
     */
    private String nom;

    /**
     * prénom de l'utilisateur.
     */
    private String prenom;

    /**
     * Indique la selection de tous les utilisateurs (inactif et inactif).
     */
    private Boolean actif;

    /**
     * Ensemble didentifiants des direction/service
     */
    private Collection<Integer> idDirServices;

    private List<Integer> idProfils;

    /**
     * Code alerte
     */
    private String codeAlerte;

    /**
     * Identifiant de l'organisme auquel l'utilisateur est lié
     */
    private Integer idOrganisme;

    /**
     * clé public codé en base 64 utilisée en cas d'autentification par certificat
     */
    private String clePub;
    private String plateformeUuid;

    private String identifiant;

    /**
     * @return chaine HQL utilisé par Hibernate
     */
    public final StringBuffer corpsRequete() {

        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        sb.append("from EpmTUtilisateur user");
        if (idProfils != null && !idProfils.isEmpty()) {
            sb.append(", EpmTProfilUtilisateur profilUtilisateur ");
        }
        if (codeAlerte != null && !codeAlerte.trim().isEmpty()) {
            sb.append(" JOIN FETCH user.alertes AS alerte WHERE ");
            sb.append(" alerte.codeExterne = '").append(codeAlerte).append("'");
            debut = false;
        }
        if (actif != null) {
            sb.append(debut ? " where " : " and ");
            sb.append("user.actif =").append(actif);
            debut = false;
        }
        if (nom != null && !nom.trim().equals("")) {
            sb.append(debut ? " where " : " and ");
            //sb.append(" lower(user.nom) = lower(:nom)");
            sb.append(" lower(user.nom) like lower(:nom)");
            getParametres().put("nom", "%" + nom.trim() + "%");
            debut = false;
        }
        if (prenom != null && !prenom.trim().equals("")) {
            sb.append(debut ? " where " : " and ");
            //sb.append(" lower(user.prenom) = lower(:prenom)");
            sb.append(" lower(user.prenom) like lower(:prenom)");
            getParametres().put("prenom", "%" + prenom.trim() + "%");
            debut = false;
        }
        if (guidsSso != null && !guidsSso.isEmpty()) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.guidSso in (:listGuidSso)");
            getParametres().put("listGuidSso", guidsSso);
            debut = false;
        } else if (guidSso != null && !guidSso.trim().equals("")) {
            sb.append(debut ? " where " : " and ");
            if (guidSsoStrict)
                sb.append(" lower(user.guidSso) = lower(:guidSso)");
            else
                sb.append(" lower(user.guidSso) like lower(:guidSso)");

            getParametres().put("guidSso",  guidSso.trim());
            debut = false;
        }
        if (id != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.id = :id ");
            getParametres().put("id", id);
            debut = false;
        }
        if (idDirServices != null && !idDirServices.isEmpty()) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.directionService IN (:idDirServices)");
            getParametres().put("idDirServices", idDirServices);
            debut = false;
        }
        if(idProfils != null && !idProfils.isEmpty()) {
            sb.append(debut ? " where " : " and ");
            sb.append("profilUtilisateur.idUtilisateur = user.id and profilUtilisateur.profil.id IN (:idProfils)");
            getParametres().put("idProfils", idProfils);
            debut = false;
        }
        if (idOrganisme != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.epmTRefOrganisme.id = :idOrganisme ");
            getParametres().put("idOrganisme", idOrganisme);
            debut = false;
        }
        if (plateformeUuid != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.epmTRefOrganisme.plateformeUuid = :plateformeUuid ");
            getParametres().put("plateformeUuid", plateformeUuid);
            debut = false;
        }
        if (clePub != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.clef = :clePub ");
            getParametres().put("clePub", clePub);
            debut = false;
        }
        if (identifiant != null && !identifiant.isEmpty()) {
            sb.append(debut ? " where " : " and ");
            sb.append(" user.identifiant = :identifiant ");
            getParametres().put("identifiant", identifiant);
            debut = false;
        }
        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = new StringBuffer("select distinct user ");
        sb.append(corpsRequete());
        if (proprieteTriee != null) {
            sb.append(" order by user.").append(proprieteTriee);
            if (triCroissant)
                sb.append(" ASC ");
            else
                sb.append(" DESC ");
        }

        return sb.toString();
    }

    public String toCountHQL() {
        return "select count(distinct user) " + corpsRequete();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setGuidsSso(String... guidsSso) {
        this.guidsSso = Arrays.asList(guidsSso);
    }

    public void setGuidsSso(Collection<String> listGuidSso) {
        this.guidsSso = listGuidSso;
    }

    /**
     * @param nom de l'utilisateur
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @param prenom prénom de l'utilisateur
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @param actif Indique la selection de tous les utilisateurs (inactif et inactif).
     */
    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    /**
     * @param idDirServices the idDirServices to set
     */
    public void setIdDirServices(Integer... idDirServices) {
        this.idDirServices = Arrays.asList(idDirServices);
    }

    /**
     * @param idDirServices the idDirServices to set
     */
    public void setIdDirServices(Collection<Integer> idDirServices) {
        this.idDirServices = idDirServices;
    }

    /**
     * @param idProfils the idProfils to set
     */
    public void setIdProfils(Integer... idProfils) {
        this.idProfils = Arrays.asList(idProfils);
    }

    /**
     * @param idProfils the idProfils to set
     */
    public void setIdProfils(List<Integer> idProfils) {
        this.idProfils = idProfils;
    }

    public void setCodeAlerte(String codeAlerte) {
        this.codeAlerte = codeAlerte;
    }

    /**
     * @param idOrganisme the idOrganisme to set
     */
    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    /**
     * @param clePub the clePub to set
     */
    public void setClePub(String clePub) {
        this.clePub = clePub;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public void setGuidSso(String guidSso) {
        this.guidSso = guidSso;
    }

    public void setGuidSsoStrict(boolean guidSsoStrict) {
        this.guidSsoStrict = guidSsoStrict;
    }
}
