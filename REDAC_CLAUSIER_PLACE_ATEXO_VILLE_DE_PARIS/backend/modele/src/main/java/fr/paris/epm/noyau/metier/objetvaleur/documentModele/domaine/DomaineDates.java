package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineCalendrierReelEnCours;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine dates.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineDates implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private DomaineCalendrierReelEnCours calendrierReelEnCours;

    public final DomaineCalendrierReelEnCours getCalendrierReelEnCours() {
        return calendrierReelEnCours;
    }

    public final void setCalendrierReelEnCours(final
            DomaineCalendrierReelEnCours valeur) {
        this.calendrierReelEnCours = valeur;
    }
}
