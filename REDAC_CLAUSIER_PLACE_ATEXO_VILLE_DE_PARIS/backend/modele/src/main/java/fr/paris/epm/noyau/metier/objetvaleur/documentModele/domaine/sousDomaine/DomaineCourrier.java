package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * entreprises.courrier (le document concerne une entreprise).
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCourrier implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    DomaineCoordonneesEntreprise coordonneesEntrepriseDestinataire;

    public final DomaineCoordonneesEntreprise getCoordonneesEntrepriseDestinataire() {
        return coordonneesEntrepriseDestinataire;
    }

    public final void setCoordonneesEntrepriseDestinataire(
            final DomaineCoordonneesEntreprise valeur) {
        this.coordonneesEntrepriseDestinataire = valeur;
    }
}
