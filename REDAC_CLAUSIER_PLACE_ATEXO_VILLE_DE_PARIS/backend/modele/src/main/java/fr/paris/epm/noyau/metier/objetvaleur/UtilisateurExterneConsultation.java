package fr.paris.epm.noyau.metier.objetvaleur;

import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Association d'un utilisateur externe avec une consultation.
 *
 * @author RVI
 */
public class UtilisateurExterneConsultation implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private EpmTUtilisateur utilisateur;

    private List<String> habilitations;

    private EpmTConsultationContexte consultation;

    private UUID contexte;

    private String idContexte;

    private String accessToken;
    private String plateformeUuid;

    private String client;

    private String erreur;

    private String documentUrl;

    private UUID agent;

    public final EpmTUtilisateur getUtilisateur() {
        return utilisateur;
    }

    public final void setUtilisateur(final EpmTUtilisateur valeur) {
        this.utilisateur = valeur;
    }

    public final EpmTConsultationContexte getConsultation() {
        return consultation;
    }

    public final void setConsultation(final EpmTConsultationContexte valeur) {
        this.consultation = valeur;
    }

    public UUID getContexte() {
        return contexte;
    }

    public void setContexte(UUID contexte) {
        this.contexte = contexte;
    }

    public UUID getAgent() {
        return agent;
    }

    public void setAgent(UUID agent) {
        this.agent = agent;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getErreur() {
        return erreur;
    }

    public void setErreur(String erreur) {
        this.erreur = erreur;
    }

    public List<String> getHabilitations() {
        return habilitations;
    }

    public void setHabilitations(List<String> habilitations) {
        this.habilitations = habilitations;
    }

    public String getIdContexte() {
        return idContexte;
    }

    public void setIdContexte(String idContexte) {
        this.idContexte = idContexte;
    }

    public String getPlateformeUuid() {
        return plateformeUuid;
    }

    public void setPlateformeUuid(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }
}
