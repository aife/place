package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate de EpmTRefThemeClause .
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_theme_clause", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefThemeClause extends BaseEpmTRefRedaction implements EpmTRefImportExport {

    /**
     * attribut ID_TOUS static .
     */
    public static final int ID_TOUS = 1;

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
