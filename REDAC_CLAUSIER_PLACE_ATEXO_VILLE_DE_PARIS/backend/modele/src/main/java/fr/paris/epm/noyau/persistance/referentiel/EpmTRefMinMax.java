package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefMinMax de la table "epm__t_ref_min_max"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefMinMax extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}