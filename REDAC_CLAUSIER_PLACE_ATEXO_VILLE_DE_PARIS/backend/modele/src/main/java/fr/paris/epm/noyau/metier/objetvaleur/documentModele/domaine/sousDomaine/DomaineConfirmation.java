package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Contient les données a fusionner dans un document pour les sous domaines
 * attribution.confirmation.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineConfirmation implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private DomaineAttributaire attributaire;
    
    /**
     * Liste des attributaires par lot.
     */
    private Map<String, List<DomaineAttributaire>> attributairesParLot;

    public final DomaineAttributaire getAttributaire() {
        return attributaire;
    }

    public final void setAttributaire(final DomaineAttributaire valeur) {
        this.attributaire = valeur;
    }

	/**
	 * @return the attributairesParLot
	 */
	public final Map<String, List<DomaineAttributaire>> getAttributairesParLot() {
		return attributairesParLot;
	}

	/**
	 * @param valeur the attributairesParLot to set
	 */
	public final void setAttributairesParLot(
			final Map<String, List<DomaineAttributaire>> valeur) {
		this.attributairesParLot = valeur;
	}
}
