package fr.paris.epm.noyau.metier.objetvaleur.documentModele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValidationChampFusionBean implements Serializable {

	private static final long serialVersionUID = -3207384848107625755L;

	private List<String> champsNonExistantDansTemplate = new ArrayList<String>();

	private List<String> champsNonExistantDansDTO = new ArrayList<String>();

	public List<String> getChampsNonExistantDansTemplate() {
		return champsNonExistantDansTemplate;
	}

	public void setChampsNonExistantDansTemplate(List<String> valeur) {
		this.champsNonExistantDansTemplate = valeur;
	}

	public List<String> getChampsNonExistantDansDTO() {
		return champsNonExistantDansDTO;
	}

	public void setChampsNonExistantDansDTO(List<String> valeur) {
		this.champsNonExistantDansDTO = valeur;
	}
}
