package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Mécanisme de sécurisation des requêtes pour les critères texte.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractCritere implements Serializable, Critere {

    protected String proprieteTriee = null;

    protected boolean triCroissant = true;
    
    private static final String POURCENTAGE = "%";
    
    public final static String AND = " and ";

    public final static String WHERE = " where ";

    /**
     * N° de la page à afficher.
     */
    private int numeroPage;
    /**
     * Taille des pages.
     */
    private int taillePage;
    /**
     * Indique s'il faut rechercher le nombre de résultat total.
     */
    private boolean chercherNombreResultatTotal;
    /**
     * Paramètres pour hibernate.
     */
    private Map<String, Object> parametres = new HashMap<>();

    /**
     * @param chaine chaîne à traiter
     * @return avec des pourcents
     */
    protected final String pourcentTrim(final String chaine) {
        return "%" + chaine.trim() + "%";
    }

    
    protected final boolean gererDebut(boolean debut, StringBuffer sb) {
        if (debut)
            sb.append(" and ");
        else
            sb.append(" where ");
        return true;
    }
    /**
     * @return map de paramètres hibernate
     */
    public final Map<String, Object> getParametres() {
        return parametres;
    }

    /**
     * @return le numero de page courant.
     */
    public final int getNumeroPage() {
        return numeroPage;
    }

    /**
     * @param valeur le numero de page courant.
     */
    public final void setNumeroPage(final int valeur) {
        this.numeroPage = valeur;
    }

    /**
     * @return la taille des pages à afficher.
     */
    public final int getTaillePage() {
        return taillePage;
    }

    /**
     * @param valeur la taille des pages à afficher.
     */
    public final void setTaillePage(final int valeur) {
        this.taillePage = valeur;
    }

    /**
     * @return s'il faut chercher le nombre de résultat total.
     */
    public final boolean isChercherNombreResultatTotal() {
        return chercherNombreResultatTotal;
    }

    /**
     * @param valeur s'il faut chercher le nombre de résultat total.
     */
    public final void setChercherNombreResultatTotal(final boolean valeur) {
        this.chercherNombreResultatTotal = valeur;
    }

    /**
     * @return propriété à trier
     */
    public final String getProprieteTriee() {
        return proprieteTriee;
    }

    /**
     * @param valeur propriété à trier
     */
    public final void setProprieteTriee(final String valeur) {
        if (valeur != null && valeur.trim().length() > 0) {
            this.proprieteTriee = valeur.trim();
        }
    }

    /**
     * @return tri croissant
     */
    public final boolean isTriCroissant() {
        return triCroissant;
    }

    /**
     * @param valeur tri croissant
     */
    public final void setTriCroissant(final boolean valeur) {
        triCroissant = valeur;
    }
    
    public abstract StringBuffer corpsRequete();
    
    /**
     * @return chaine HQL utilisée par Hibernate
     */
    public final String toHQLCompact() {
        String res = corpsRequete().toString();
        res = res.replaceFirst("where", "and");
        return res;
    }
    
    /**
     * Préfixe et suffixe une chaine de caractéres avec %
     * @param parametre le paramétre à modifier.
     * @param debut indique si un % doit être ajouté au début.
     * @param fin indique si un % doit être ajouté a la fin.
     * @return une nouvelle chaine modifiée.
     */
    protected final String ajouterPourcentage(final String parametre, final boolean debut, final boolean fin) {
        StringBuffer resultat = new StringBuffer();
        if (debut) {
            resultat.append(POURCENTAGE);    
        }
        resultat.append(parametre);
        if (fin) {
            resultat.append(POURCENTAGE);
        }
        return resultat.toString();
    }

//    @Override
//    public String toString() {
//        // TODO Auto-generated method stub
////        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE) + " " + this.toHQL();
//    }
}
