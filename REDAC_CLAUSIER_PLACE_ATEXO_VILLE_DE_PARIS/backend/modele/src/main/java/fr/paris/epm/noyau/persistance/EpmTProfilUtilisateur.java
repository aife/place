package fr.paris.epm.noyau.persistance;

import java.util.Set;


/**
 * Represente l'objet persistant de l'association entre un utilisateur et un profil.
 * 
 * @author Rémi Villé
 * @version 2010/09/10
 */
public class EpmTProfilUtilisateur extends EpmTAbstractObject {

	/**
	 * Identifiant de serialisation.
	 */
	private static final long serialVersionUID = -5623654744150013868L;

	/**
	 * Identifiant en base.
	 */
	private int id;
	
	/**
	 * Ensemble des marche associes au profil de l'utilisateur.
	 */
	private Set<EpmTTypeMarcheProfil> typeMarcheProfilSet;
	
	/**
	 * Profil de l'utilisateur.
	 */
	private EpmTProfil profil;

	private Integer idUtilisateur;
	
	/**
	 * @return Identifiant en base.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param valeur Identifiant en base.
	 */
	public void setId(int valeur) {
		this.id = valeur;
	}

	/**
	 * @return Ensemble des marche associes au profil de l'utilisateur.
	 */
	public Set<EpmTTypeMarcheProfil> getTypeMarcheProfilSet() {
		return typeMarcheProfilSet;
	}

	/**
	 * @param valeur Ensemble des marche associes au profil de l'utilisateur.
	 */
	public void setTypeMarcheProfilSet(
			Set<EpmTTypeMarcheProfil> valeur) {
		this.typeMarcheProfilSet = valeur;
	}

	/**
	 * @return Profil de l'utilisateur.
	 */
	public EpmTProfil getProfil() {
		return profil;
	}

	/**
	 * @param valeur Profil de l'utilisateur.
	 */
	public void setProfil(EpmTProfil valeur) {
		this.profil = valeur;
	}

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(final Integer valeur) {
        this.idUtilisateur = valeur;
    }
}
