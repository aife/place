package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.EpmTBiclef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;

import java.util.HashSet;
import java.util.Set;

/**
 * POJO EpmTRefDirectionService de la table "epm__t_ref_direction_service"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDirectionService extends EpmTReferentielByOrganismeAbstract implements EpmTRefImportExport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String NIVEAU_1 = "0"; // Niveau hiérarchique de la direction.
    public static final String NIVEAU_2 = "1"; // Niveau hiérarchique de la direction.

    /**
     * Type d'entité (direction, service, bureau...).
     */
    private String typeEntite;

    /**
     * Courriel de la direction/service.
     */
    private String courriel;

    /**
     * adresse du service.
     */
    private String adresse;

    private String codePostal;

    private String ville;

    /**
     * fax du service.
     */
    private String fax;

    /**
     * telephone du service.
     */
    private String telephone;

    /**
     * Possède les droits d'accès transverse en intervention.
     */
    private boolean transverse;

    private EpmTRefDirectionService parent;

    /**
     * Identifie si il sagit d'une DA
     */
    private boolean directionAchat;

    /**
     * Code externe de l'organisme
     */
    private String codeOrganisme; // TODO : supprimer à l'avenir car double id_organisme -> EpmTRefOrganisme(id_organisme)

    public String getTypeEntite() {
        return typeEntite;
    }

    public void setTypeEntite(String typeEntite) {
        this.typeEntite = typeEntite;
    }

    public String getCourriel() {
        return courriel;
    }

    public void setCourriel(String courriel) {
        this.courriel = courriel;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public boolean isTransverse() {
        return transverse;
    }

    public void setTransverse(boolean transverse) {
        this.transverse = transverse;
    }

    public EpmTRefDirectionService getParent() {
        return parent;
    }

    public void setParent(EpmTRefDirectionService parent) {
        this.parent = parent;
    }

    public boolean isDirectionAchat() {
        return directionAchat;
    }

    public void setDirectionAchat(boolean directionAchat) {
        this.directionAchat = directionAchat;
    }

    public String getCodeOrganisme() {
        return codeOrganisme;
    }

    public void setCodeOrganisme(String codeOrganisme) {
        this.codeOrganisme = codeOrganisme;
    }

	@Override
	public String getUid() {
		return null;
	}
}
