package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutEntreprise de la table "epm__t_ref_statut_entreprise"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutEntreprise extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}