package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * Pour rechercher les types de document. 
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DocumentTypeCritere extends AbstractCritere implements Critere,
        Serializable {
	/**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 2685442559603742467L;	
	
	private String type;
	
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        Map parametres = getParametres();
        sb.append("from EpmTRefDocumentType d");
        if (type != null) {
            sb.append(" where d.type= lower(:type)");
            parametres.put("type", type);
        }

        return sb;
    }
    
    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(*) ");
        return select.append(corpsRequete()).toString();
    }

    public final void setType(final String valeur) {
        this.type = valeur;
    }
}
