package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefNatureTranche de la table "epm__t_ref_nature_tranche"
 * Created by nty on 31/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefNatureTranche extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int TRANCHE_FIXE = 1; // id de la Tranche fixe
    public static final int TRANCHE_CONDITIONNELLE = 2; // id de la Tranche conditionnelle

}