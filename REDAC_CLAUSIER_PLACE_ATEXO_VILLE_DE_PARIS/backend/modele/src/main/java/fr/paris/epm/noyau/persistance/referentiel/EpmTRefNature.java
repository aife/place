package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;

/**
 * POJO EpmTRefNature de la table "epm__t_ref_nature"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefNature extends BaseEpmTRefReferentiel implements EpmTRefImportExport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String ACRONYME_TRAVAUX = "TRA"; // Acronyme en base pour la référence travaux.
    public static final String ACRONYME_SERVICE = "SER"; // Acronyme en base pour la référence service.
    public static final String ACRONYME_FOURNITURE = "FOU"; // Acronyme en base pour la référence fourniture.

    /**
     * Identifiant GO.
     */
    private String codeGo;

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(String codeGo) {
        this.codeGo = codeGo;
    }

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
