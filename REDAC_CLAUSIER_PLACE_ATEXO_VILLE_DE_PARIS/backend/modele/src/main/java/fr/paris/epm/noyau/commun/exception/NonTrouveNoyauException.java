/**
 * $Id: NonTrouveNoyauException.java 146 2007-07-30 17:00:07Z beraudo $
 */
/**
 * $Id: NonTrouveNoyauException.java 146 2007-07-30 17:00:07Z beraudo $
 */
package fr.paris.epm.noyau.commun.exception;

/**
 * Exception levée lorsque l'objet cherché n'a pas été trouvé.
 * @author Guillaume Béraudo
 * @version $Revision: 146 $,
 *          $Date: 2007-07-30 19:00:07 +0200 (lun., 30 juil. 2007) $,
 *          $Author: $
 */
public class NonTrouveNoyauException extends ApplicationNoyauException {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur vide.
     */
    public NonTrouveNoyauException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause Exception ayant provoqué cette exception noyau
     */
    public NonTrouveNoyauException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public NonTrouveNoyauException(final String msg) {
        super(msg);
    }

    /**
     * @param cause Exception ayant provoqué cette exception noyau
     */
    public NonTrouveNoyauException(final Throwable cause) {
        super(cause);
    }
}
