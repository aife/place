package fr.paris.epm.noyau.persistance.negociation;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefSuiteNegociation;

import java.util.Date;

/**
 * Representer une ligne d'un tour de negociation.
 */
public class EpmTNegociationTourLigne extends EpmTAbstractObject implements Comparable, Cloneable {

    private static final long serialVersionUID = -3248079651284287338L;

    private Integer id;
    private EpmTNegociationTour negociationTour;
    private EpmTNegociationTourLot negociationTourLot;
    private String candidat;
    private Integer rang;
    private String typeReception;
    private Double prixPF;
    private Double prixDQE;
    private String commentaire;
    private EpmTRefSuiteNegociation suiteADonner;
    private EpmTRefSuiteNegociation appreciation;
    private Integer idOffreLc;
    private Integer note;
    private Integer idCandidat;

    private String adresseDestinataire;
    private Date dateReponse;
    private Date dateRemisePapier;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EpmTNegociationTour getNegociationTour() {
        return negociationTour;
    }

    public void setNegociationTour(EpmTNegociationTour negociationTour) {
        this.negociationTour = negociationTour;
    }

    public String getCandidat() {
        return candidat;
    }

    public void setCandidat(String candidat) {
        this.candidat = candidat;
    }

    public Integer getRang() {
        return rang;
    }

    public void setRang(Integer rang) {
        this.rang = rang;
    }

    public String getTypeReception() {
        return typeReception;
    }

    public void setTypeReception(String typeReception) {
        this.typeReception = typeReception;
    }

    public Double getPrixPF() {
        return prixPF;
    }

    public void setPrixPF(Double prixPF) {
        this.prixPF = prixPF;
    }

    public Double getPrixDQE() {
        return prixDQE;
    }

    public void setPrixDQE(Double prixDQE) {
        this.prixDQE = prixDQE;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public EpmTRefSuiteNegociation getSuiteADonner() {
        return suiteADonner;
    }

    public void setSuiteADonner(EpmTRefSuiteNegociation suiteADonner) {
        this.suiteADonner = suiteADonner;
    }

    public EpmTRefSuiteNegociation getAppreciation() {
        return appreciation;
    }

    public void setAppreciation(EpmTRefSuiteNegociation appreciation) {
        this.appreciation = appreciation;
    }

    public Integer getIdOffreLc() {
        return idOffreLc;
    }

    public void setIdOffreLc(Integer idOffreLc) {
        this.idOffreLc = idOffreLc;
    }

    public EpmTNegociationTourLot getNegociationTourLot() {
        return negociationTourLot;
    }

    public void setNegociationTourLot(EpmTNegociationTourLot negociationTourLot) {
        this.negociationTourLot = negociationTourLot;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public Integer getIdCandidat() {
        return idCandidat;
    }

    public void setIdCandidat(Integer idCandidat) {
        this.idCandidat = idCandidat;
    }

    public String getAdresseDestinataire() {
        return adresseDestinataire;
    }

    public void setAdresseDestinataire(String adresseDestinataire) {
        this.adresseDestinataire = adresseDestinataire;
    }

    public Date getDateReponse() {
        return dateReponse;
    }

    public void setDateReponse(Date dateReponse) {
        this.dateReponse = dateReponse;
    }

    public Date getDateRemisePapier() {
        return dateRemisePapier;
    }

    public void setDateRemisePapier(Date dateRemisePapier) {
        this.dateRemisePapier = dateRemisePapier;
    }

    @Override
    public int compareTo(Object o) {
        return this.rang - ((EpmTNegociationTourLigne) o).getRang();
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTNegociationTourLigne epmTNegociationTourLigne = (EpmTNegociationTourLigne) super.clone();
        epmTNegociationTourLigne.setId(null);
        epmTNegociationTourLigne.setNegociationTour(null);
        epmTNegociationTourLigne.setNegociationTourLot(null);
        return epmTNegociationTourLigne;
    }
}
