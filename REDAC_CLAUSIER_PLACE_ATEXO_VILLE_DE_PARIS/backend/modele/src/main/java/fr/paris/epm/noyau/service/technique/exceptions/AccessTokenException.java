package fr.paris.epm.noyau.service.technique.exceptions;

import org.slf4j.helpers.MessageFormatter;

public class AccessTokenException extends RuntimeException {
	public AccessTokenException( final String message, final String client, final String plateforme ) {
		super(MessageFormatter.format(message, client, plateforme).getMessage());
	}
}
