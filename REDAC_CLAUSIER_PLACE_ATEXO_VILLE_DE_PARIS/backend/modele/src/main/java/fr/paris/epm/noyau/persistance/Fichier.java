/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class Fichier {

    /**
     * Chemin du fichier sur disque.
     */
    private String chemin;

    /**
     * Nom du fichier sur disque.
     */
    private String nom;

    /**
     * Nom du fichier attendu.
     */
    private String nomFinal;


    // Accesseurs
    /**
     * @return chemin du fichier sur disque
     */
    public final String getChemin() {
        return chemin;
    }

    /**
     * @return nom du fichier sur disque
     */
    public final String getNom() {
        return nom;
    }

    /**
     * @return nom du fichier attendu
     */
    public final String getNomFinal() {
        return nomFinal;
    }

    /**
     * @param valeur chemin du fichier sur disque
     */
    public final void setChemin(final String valeur) {
        this.chemin = valeur;
    }

    /**
     * @param valeur nom du fichier sur disque
     */
    public final void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @param valeur nom du fichier attendu
     */
    public final void setNomFinal(final String valeur) {
        this.nomFinal = valeur;
    }
}
