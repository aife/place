/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * @author Léon Barsamian
 */
public class DomaineValidationClassementOffre implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    private String rangValidationClassementOffre;
    private String attributaireValidationClassementOffre;
    private String pFPrixHTAttributaire;
    private String dQEPrixHTAttributaire;
    private String appreciationValidationClassementOffre;
    private String attestationValidationClassementOffre;
    private String commentairesValidationClassementOffre;

    /**
     * List des candidats admis aprés l'ouverture des offres.
     */
    List<DomaineCandidature> listeCandidatsAdmis;

    private Integer nombreCandidatsRetenus;

    /**
     * Liste des candidats admissibles dont l'offre n'est pas retenue
     */
    List<DomaineCandidature> listeCandidatsOffreNonRetenue;

    /**
     * @return List des candidats admis aprés l'ouverture des offres.
     */
    public final List<DomaineCandidature> getListeCandidatsAdmis() {
        return listeCandidatsAdmis;
    }

    /**
     * @param valeur List des candidats admis aprés l'ouverture des offres.
     */
    public final void setListeCandidatsAdmis(List<DomaineCandidature> valeur) {
        this.listeCandidatsAdmis = valeur;
    }

    public final String getRangValidationClassementOffre() {
        return rangValidationClassementOffre;
    }

    public final void setRangValidationClassementOffre(final String valeur) {
        this.rangValidationClassementOffre = valeur;
    }

    public final String getAttributaireValidationClassementOffre() {
        return attributaireValidationClassementOffre;
    }

    public final void setAttributaireValidationClassementOffre(String valeur) {
        this.attributaireValidationClassementOffre = valeur;
    }

    public final String getpFPrixHTAttributaire() {
        return pFPrixHTAttributaire;
    }

    public final void setpFPrixHTAttributaire(String valeur) {
        this.pFPrixHTAttributaire = valeur;
    }

    public final String getdQEPrixHTAttributaire() {
        return dQEPrixHTAttributaire;
    }

    public final void setdQEPrixHTAttributaire(final String valeur) {
        this.dQEPrixHTAttributaire = valeur;
    }

    public final String getAppreciationValidationClassementOffre() {
        return appreciationValidationClassementOffre;
    }

    public final void setAppreciationValidationClassementOffre(
            final String valeur) {
        this.appreciationValidationClassementOffre = valeur;
    }

    public final String getAttestationValidationClassementOffre() {
        return attestationValidationClassementOffre;
    }

    public final void setAttestationValidationClassementOffre(
            final String valeur) {
        this.attestationValidationClassementOffre = valeur;
    }

    public final String getCommentairesValidationClassementOffre() {
        return commentairesValidationClassementOffre;
    }

    public final void setCommentairesValidationClassementOffre(
            final String valeur) {
        this.commentairesValidationClassementOffre = valeur;
    }

    public final Integer getNombreCandidatsRetenus() {
        return nombreCandidatsRetenus;
    }

    public final void setNombreCandidatsRetenus(final Integer valeur) {
        this.nombreCandidatsRetenus = valeur;
    }

    public List<DomaineCandidature> getListeCandidatsOffreNonRetenue() {
        return listeCandidatsOffreNonRetenue;
    }

    public void setListeCandidatsOffreNonRetenue(List<DomaineCandidature> valeur) {
        this.listeCandidatsOffreNonRetenue = valeur;
    }
    
    
}
