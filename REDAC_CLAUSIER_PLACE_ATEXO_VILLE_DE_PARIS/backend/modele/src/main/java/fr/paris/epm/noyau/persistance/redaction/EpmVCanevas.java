package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "epm__v_canevas", schema = "redaction")
public class EpmVCanevas extends EpmTCanevasAbstract {

    @Id
    private int id;

    @ManyToOne(targetEntity = EpmTCanevas.class)
    @JoinColumn(name = "id_canevas", referencedColumnName = "id")
    private EpmTCanevas epmTCanevas;

    @ManyToOne(targetEntity = EpmTCanevasPub.class)
    @JoinColumns({
            @JoinColumn(name = "id_canevas_publication", referencedColumnName = "id_canevas"),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
    })
    private EpmTCanevasPub epmTCanevasPub;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EpmTCanevas getEpmTCanevas() {
        return epmTCanevas;
    }

    public void setEpmTCanevas(EpmTCanevas epmTCanevas) {
        this.epmTCanevas = epmTCanevas;
    }

    public EpmTCanevasPub getEpmTCanevasPub() {
        return epmTCanevasPub;
    }

    public void setEpmTCanevasPub(EpmTCanevasPub epmTCanevasPub) {
        this.epmTCanevasPub = epmTCanevasPub;
    }

    @Override
    public Integer getIdCanevas() {
        if(epmTCanevasPub == null && epmTCanevas == null)
            return null;
        return epmTCanevasPub != null ? epmTCanevasPub.getIdCanevas() : epmTCanevas.getIdCanevas();
    }

    @Override
    public Integer getIdPublication() {
        return epmTCanevasPub != null ? epmTCanevasPub.getIdPublication() : null;
    }

    @Override
    public Integer getIdLastPublication() {
        if(epmTCanevasPub == null && epmTCanevas == null)
            return null;
        return epmTCanevasPub != null ? epmTCanevasPub.getIdPublication() : epmTCanevas.getIdLastPublication();
    }

    @Override
    public List<? extends EpmTChapitreAbstract> getEpmTChapitres() {
        if(epmTCanevasPub == null && epmTCanevas == null)
            return new ArrayList<>();
        return epmTCanevasPub != null ? epmTCanevasPub.getEpmTChapitres() : epmTCanevas.getEpmTChapitres();
    }

    @Override
    public void setEpmTChapitres(List<? extends EpmTChapitreAbstract> epmTChapitres) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTChapitres' est non-modifiable");
    }
/*
    @Override
    public Set<? extends EpmTClauseAbstract> getEpmTClauses() {
        return epmTCanevasPub != null ? epmTCanevasPub.getEpmTClauses() : epmTCanevas.getEpmTClauses();
    }

    @Override
    public void setEpmTClauses(Set<? extends EpmTClauseAbstract> epmTClauses) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTClauses' est non-modifiable");
    }
*/
    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        if(epmTCanevasPub == null && epmTCanevas == null)
            return new HashSet<>();
        return epmTCanevasPub != null ? epmTCanevasPub.getEpmTRefTypeContrats() : epmTCanevas.getEpmTRefTypeContrats();
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTRefTypeContrats' est non-modifiable");
    }

    @Override
    public Set<EpmTRefProcedure> getEpmTRefProcedures() {
        if(epmTCanevasPub == null && epmTCanevas == null)
            return new HashSet<>();
        return epmTCanevasPub != null ? epmTCanevasPub.getEpmTRefProcedures() : epmTCanevas.getEpmTRefProcedures();
    }

    @Override
    public void setEpmTRefProcedures(Set<EpmTRefProcedure> epmTRefProcedures) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTRefProcedures' est non-modifiable");
    }

    @Override
    public EpmVCanevas clone() throws CloneNotSupportedException {
        EpmVCanevas epmVCanevas = (EpmVCanevas) super.clone();
        epmVCanevas.epmTCanevas = epmTCanevas.clone();
        epmVCanevas.epmTCanevasPub = epmTCanevasPub.clone();
        return epmVCanevas;
    }

}
