package fr.paris.epm.noyau.service.technique;

import fr.paris.epm.noyau.service.technique.exceptions.AccessTokenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
public class HttpOauthAccessTokenService implements OauthAccessTokenService {
	private final Logger LOG = LoggerFactory.getLogger(HttpOauthAccessTokenService.class);

	// Keycloak
	@Value("${oauth.type}")
	private String oauthType;

	@Value("${oauth.client}")
	private String oauthClient;

	@Value("${oauth.username}")
	private String oauthUsername;

	@Value("${oauth.password}")
	private String oauthPassword;

	@Value("${oauth.url}")
	private String oauthUrl;

	@Autowired
	private RestService restService;

	@Override
	public Credentials retrieve() {
		Credentials credentials;
		LOG.debug("Récupération du jeton technique depuis l'url '{}' pour l'utilisateur technique '{}'", oauthUrl, oauthUsername);

		try {
			// Headers
			final MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
			body.add("grant_type", oauthType);
			body.add("client_id", oauthClient);
			body.add("username", oauthUsername);
			body.add("password", oauthPassword);

			final ResponseEntity<Credentials> response = restService.getRestTemplate().postForEntity(oauthUrl, body, Credentials.class);

			if ( HttpStatus.OK != response.getStatusCode() || null == response.getBody() ) {
				LOG.error("Impossible de récuperer le jeton technique depuis l'URL '{}' pour l'utilisateur '{}'", oauthUrl, oauthUsername);

				throw new RuntimeException("Impossible de créer l'access token. Response = " + response);
			}

			credentials = response.getBody();
		} catch ( final Exception e ) {
			LOG.error("Erreur lors de l'obtention du jeton", e);

			throw new AccessTokenException("Impossible d'obtenir le jeton technique depuis l'url '{}' pour l'utilisateur technique '{}'", oauthUrl, oauthUsername);
		}

		LOG.info("Récupération du jeton technique OK depuis '{}' pour l'utilisateur '{}'. Jeton = {}", oauthUrl, oauthUsername, credentials);

		return credentials;
	}

	public void setOauthType( String oauthType ) {
		this.oauthType = oauthType;
	}

	public void setOauthClient( String oauthClient ) {
		this.oauthClient = oauthClient;
	}

	public void setOauthUsername( String oauthUsername ) {
		this.oauthUsername = oauthUsername;
	}

	public void setOauthPassword( String oauthPassword ) {
		this.oauthPassword = oauthPassword;
	}

	public void setOauthUrl( String oauthUrl ) {
		this.oauthUrl = oauthUrl;
	}

	public void setRestService( RestService restService ) {
		this.restService = restService;
	}
}
