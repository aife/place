package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

/**
 * Contient les données a fusionner dans un document pour une element du sous
 * domaine commission.domaineOrdreDuJour.listeConsultationOdj.listeLotOdj.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineLotOdj extends DomaineLotOuConsultationOdj {

    private String numeroLot;

    private String heure;

    private int ordrePassage;

    private String intituleLot;

    public final String getNumeroLot() {
        return numeroLot;
    }

    public final void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }

    public final String getHeure() {
        return heure;
    }

    public final void setHeure(final String valeur) {
        this.heure = valeur;
    }

    public final int getOrdrePassage() {
        return ordrePassage;
    }

    public final void setOrdrePassage(final int valeur) {
        this.ordrePassage = valeur;
    }

    public final String getIntituleLot() {
        return intituleLot;
    }

    public final void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }
}
