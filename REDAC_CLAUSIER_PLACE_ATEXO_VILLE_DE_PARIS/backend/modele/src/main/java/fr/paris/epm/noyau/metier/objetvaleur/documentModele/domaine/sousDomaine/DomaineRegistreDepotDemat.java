package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine entreprises.registreDepot
 * de la plateforme de dematerialisation.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreDepotDemat extends AbstractDomaineRegistreDepot {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Reference du Depot par la plate-forme de dematerialisation.
	 */
	private String refDepotDemat;
	
	private String entrepriseDepotDemat;
	
	private String sirenDepotDemat;
	
	private String adresseDepotDemat;
	
	private Date dateDepotDemat;
	/**
	 * Date du depot des elements physiques par la plate-forme de dematerialisation.
	 */
	private Date dateDepotElemPhyDemat;
	
	private String mailDepotDemat;
	
	private String nomContactDepotDemat;

    private String prenomContactDepotDemat;
    
    private String telephoneDepotDemat;

    private String faxDepotDemat;
    
    private String codePostalDepotDemat;

    private String villeDepotDemat;

	
	/**
	 * @return Reference du Depot par la plate-forme de dematerialisation.
	 */
	public final String getRefDepotDemat() {
		return refDepotDemat;
	}
	/**
	 * @param valeur Reference du Depot par la plate-forme de dematerialisation.
	 */
	public final void setRefDepotDemat(final String valeur) {
		this.refDepotDemat = valeur;
	}
	
	public final String getEntrepriseDepotDemat() {
		return entrepriseDepotDemat;
	}
	
	public final void setEntrepriseDepotDemat(final String valeur) {
		this.entrepriseDepotDemat = valeur;
	}
	
	public final String getSirenDepotDemat() {
		return sirenDepotDemat;
	}
	
	public final void setSirenDepotDemat(final String valeur) {
		this.sirenDepotDemat = valeur;
	}
	
	public final String getAdresseDepotDemat() {
		return adresseDepotDemat;
	}
	
	public final void setAdresseDepotDemat(final String valeur) {
		this.adresseDepotDemat = valeur;
	}
	
	public final Date getDateDepotDemat() {
		return dateDepotDemat;
	}
	
	public final void setDateDepotDemat(final Date valeur) {
		this.dateDepotDemat = valeur;
	}
	/**
	 * @return Date du depot des elements physiques par la plate-forme de dematerialisation.
	 */
	public final Date getDateDepotElemPhyDemat() {
		return dateDepotElemPhyDemat;
	}
	/**
	 * @param valeur Date du depot des elements physiques par la plate-forme de dematerialisation.
	 */
	public final void setDateDepotElemPhyDemat(final Date valeur) {
		this.dateDepotElemPhyDemat = valeur;
	}
	/**
	 * @return the mailDepotDemat
	 */
	public final String getMailDepotDemat() {
		return mailDepotDemat;
	}
	/**
	 * @param valeur the mailDepotDemat to set
	 */
	public final void setMailDepotDemat(final String valeur) {
		this.mailDepotDemat = valeur;
	}
	/**
	 * @return the nomContactDepotDemat
	 */
	public final String getNomContactDepotDemat() {
		return nomContactDepotDemat;
	}
	/**
	 * @param valeur the nomContactDepotDemat to set
	 */
	public final void setNomContactDepotDemat(final String valeur) {
		this.nomContactDepotDemat = valeur;
	}
	/**
	 * @return the prenomContactDepotDemat
	 */
	public final String getPrenomContactDepotDemat() {
		return prenomContactDepotDemat;
	}
	/**
	 * @param valeur the prenomContactDepotDemat to set
	 */
	public final void setPrenomContactDepotDemat(final String valeur) {
		this.prenomContactDepotDemat = valeur;
	}
	/**
	 * @return the telephoneDepotDemat
	 */
	public final String getTelephoneDepotDemat() {
		return telephoneDepotDemat;
	}
	/**
	 * @param valeur the telephoneDepotDemat to set
	 */
	public final void setTelephoneDepotDemat(final String valeur) {
		this.telephoneDepotDemat = valeur;
	}
	/**
	 * @return the faxDepotDemat
	 */
	public final String getFaxDepotDemat() {
		return faxDepotDemat;
	}
	/**
	 * @param valeur the faxDepotDemat to set
	 */
	public final void setFaxDepotDemat(final String valeur) {
		this.faxDepotDemat = valeur;
	}
	/**
	 * @return the codePostalDepotDemat
	 */
	public final String getCodePostalDepotDemat() {
		return codePostalDepotDemat;
	}
	/**
	 * @param valeur the codePostalDepotDemat to set
	 */
	public final void setCodePostalDepotDemat(final String valeur) {
		this.codePostalDepotDemat = valeur;
	}
	/**
	 * @return the villeDepotDemat
	 */
	public final String getVilleDepotDemat() {
		return villeDepotDemat;
	}
	/**
	 * @param valeur the villeDepotDemat to set
	 */
	public final void setVilleDepotDemat(final String valeur) {
		this.villeDepotDemat = valeur;
	}
}
