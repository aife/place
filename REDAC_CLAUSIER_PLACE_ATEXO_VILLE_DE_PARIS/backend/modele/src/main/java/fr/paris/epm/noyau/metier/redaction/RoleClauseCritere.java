package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;

public class RoleClauseCritere extends AbstractCritere {
    
    private int idClause;
    
    private Integer idUtilisateur;
    
    private Integer idDirectionService;

    @Override
    public StringBuffer corpsRequete() {

        StringBuffer requete = new StringBuffer();

        requete.append(" WHERE roleClause.preference.idClause = :idClause ");
        getParametres().put("idClause", idClause);
        /* TODO: NIKO REVOIR
        if (revisionClause != null) {
            requete.append(" AND roleClause.preference.revisionClause = :revisionClause ");
            getParametres().put("revisionClause", revisionClause);
        }
        */
        if (idUtilisateur != null) {
            requete.append(" AND roleClause.preference.idUtilisateur = :idUtilisateur ");
            getParametres().put("idUtilisateur", idUtilisateur);
        }
        
        if (idDirectionService != null) {
            requete.append(" AND roleClause.preference.idDirectionService = :idDirectionService ");
            getParametres().put("idDirectionService", idDirectionService);
        }
        
        return requete;
    }

    protected String getTableDeRecherche() {
        return " EpmTRoleClause ";
    }

    @Override
    public String toHQL() {
        StringBuilder sb = new StringBuilder("select roleClause from ");
        sb.append(getTableDeRecherche());
        sb.append(" as roleClause ");
        sb.append(corpsRequete());
        if (getProprieteTriee() != null) {
            sb.append(" ORDER BY roleClause.").append(getProprieteTriee());
            sb.append(isTriCroissant() ? " ASC" : " DESC");
        }
        return sb.toString();
    }

    @Override
    public String toCountHQL() {
        return "select count(roleClause.id) from " + getTableDeRecherche() + " as roleClause " + corpsRequete();
    }

    public final void setIdClause(final int valeur) {
        this.idClause = valeur;
    }

    public final void setIdUtilisateur(final Integer valeur) {
        this.idUtilisateur = valeur;
    }

    public void setIdDirectionService(Integer idDirectionService) {
        this.idDirectionService = idDirectionService;
    }

}