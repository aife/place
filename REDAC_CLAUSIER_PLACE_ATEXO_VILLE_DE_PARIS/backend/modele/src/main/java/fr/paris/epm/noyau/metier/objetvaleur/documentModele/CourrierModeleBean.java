package fr.paris.epm.noyau.metier.objetvaleur.documentModele;

import java.io.Serializable;

/**
 * Bean représentant un courrier electronique après génération via le module de document.
 *
 */
public class CourrierModeleBean implements Serializable {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = 8949008091236735402L;

	/**
	 * Objet du courrier elecronique.
	 */
	private String objet;
	
	/**
	 * Message du courrier electronique.
	 */
	private String message;

	/**
	 *  objet du courrier elecronique.
	 *
	 * @return the objet du courrier elecronique
	 */
	public final String getObjet() {
		return objet;
	}

	/**
	 * Sets the objet du courrier elecronique.
	 *
	 * @param objet the new objet du courrier elecronique
	 */
	public final void setObjet(String objet) {
		this.objet = objet;
	}

	/**
	 *  message du courrier electronique.
	 *
	 * @return the message du courrier electronique
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 *  message du courrier electronique.
	 *
	 * @param message the new message du courrier electronique
	 */
	public final void setMessage(String message) {
		this.message = message;
	}
	
	
}
