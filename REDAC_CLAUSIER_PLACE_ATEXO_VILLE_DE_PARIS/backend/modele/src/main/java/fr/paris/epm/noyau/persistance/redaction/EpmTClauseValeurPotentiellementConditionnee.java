package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Persistance des associations clause/valeurs des criteres potentiellement
 * conditionnes.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_clause_has_valeur_potentiellement_conditionnee", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTClauseValeurPotentiellementConditionnee extends EpmTClauseValeurPotentiellementConditionneeAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id ;

    @ManyToOne
    @JoinColumn(name = "id_clause_potentiellement_conditionnee", nullable = false)
    private EpmTClausePotentiellementConditionnee epmTClausePotentiellementConditionnee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdClauseValeurPotentiellementConditionnee() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    @Override
    public EpmTClausePotentiellementConditionnee getEpmTClausePotentiellementConditionnee() {
        return epmTClausePotentiellementConditionnee;
    }

    @Override
    public void setEpmTClausePotentiellementConditionnee(EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee) {
        this.epmTClausePotentiellementConditionnee = (EpmTClausePotentiellementConditionnee) epmTClausePotentiellementConditionnee;
    }

    @Override
    protected EpmTClauseValeurPotentiellementConditionnee clone() throws CloneNotSupportedException {
        EpmTClauseValeurPotentiellementConditionnee cvpc = (EpmTClauseValeurPotentiellementConditionnee) super.clone();
        cvpc.epmTClausePotentiellementConditionnee = null;
        return cvpc;
    }

}
