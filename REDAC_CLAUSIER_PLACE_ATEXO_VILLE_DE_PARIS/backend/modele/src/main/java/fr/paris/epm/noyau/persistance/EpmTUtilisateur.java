package fr.paris.epm.noyau.persistance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeMarche;
import fr.paris.epm.noyau.persistance.referentiel.EpmTReferentielSimpleByOrganismeAbstract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class EpmTUtilisateur extends EpmTReferentielSimpleByOrganismeAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EpmTUtilisateur.class);

    /**
     * GUID du WSSO.
     */
    private String guidSso;

    /**
     * nom de l'utilisateur.
     */
    private String nom;

    /**
     * prénom de l'utilisateur.
     */
    private String prenom;

    /**
     * email de l'utilisateur.
     */
    private String courriel;

    /**
     * identifiant de l'enregistrement.
     */
    private String identifiant;

    /**
     * Adresse de l'utilisateur.
     */
    private String adresse;

    /**
     * Mot de passe de l'utilisateur.
     */
    private String motDePasse;

    /**
     * Code postal de l'utilisateur.
     */
    private String codePostal;

    /**
     * Ville de l'utilisateur.
     */
    private String ville;

    /**
     * Telephone de l'utilisateur.
     */
    private String telephone;

    /**
     * Fax de l'utilisateur.
     */
    private String fax;

    /**
     * Acronyme organisme
     */
    private String codeOrganisme;

    private int directionService;

    /**
     * Ensemble des profil/type de marche lies a un utilisateur.
     */
    @JsonIgnore
    private Map<EpmTProfil, EpmTProfilUtilisateur> profilUtilisateurMap;


    /**
     * certificat codé en base 64 utilisée en cas d'autentification par certificat
     */
    private String clef;
    
    /**
     * L'identifiant n'est pas utilisé.
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (courriel != null) {
            result += Constantes.PREMIER * result + courriel.hashCode();
        }

        result = Constantes.PREMIER * result + directionService;

        if (guidSso != null) {
            result += Constantes.PREMIER * result + guidSso.hashCode();
        }
        if (nom != null) {
            result += Constantes.PREMIER * result + nom.hashCode();
        }
        if (prenom != null) {
            result += Constantes.PREMIER * result + prenom.hashCode();
        }
        return result;
    }

    /**
     * L'identifiant n'est pas utilisé.
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EpmTUtilisateur other = (EpmTUtilisateur) obj;

        if (courriel == null) {
            if (other.courriel != null) {
                return false;
            }
        } else if (!courriel.equals(other.courriel)) {
            return false;
        }
        if (directionService != other.directionService) {
                return false;
            }
        if (guidSso == null) {
            if (other.guidSso != null) {
                return false;
            }
        } else if (!guidSso.equals(other.guidSso)) {
            return false;
        }
        if (nom == null) {
            if (other.nom != null) {
                return false;
            }
        } else if (!nom.equals(other.nom)) {
            return false;
        }
        if (prenom == null) {
            if (other.prenom != null) {
                return false;
            }
        } else if (!prenom.equals(other.prenom)) {
            return false;
        }
        return true;
    }


    // Accesseurs
    /**
     * @return direction/service associé à l'utilisateur
     */
    public int getDirectionService() {
        return this.directionService;
    }

    /**
     * @param valeur direction/service associé à l'utilisateur
     */
    public void setDirectionService(final int valeur) {
        this.directionService = valeur;
    }

    /**
     * @return identifiant de l'utilisateur à travers le WSSO
     */
    public String getGuidSso() {
        return this.guidSso;
    }

    /**
     * @param valeur identifiant de l'utilisateur à travers le WSSO
     */
    public void setGuidSso(final String valeur) {
        this.guidSso = valeur;
    }

    /**
     * @return nom de l'utilisateur
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * @param valeur nom de l'utilisateur
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return prénom de l'utilisateur
     */
    public String getPrenom() {
        return this.prenom;
    }

    /**
     * @param valeur prénom de l'utilisateur
     */
    public void setPrenom(final String valeur) {
        this.prenom = valeur;
    }

    /**
     * @return email de l'utilisateur
     */
    public String getCourriel() {
        return this.courriel;
    }

    /**
     * @param valeur emial de l'utilisateur
     */
    public void setCourriel(final String valeur) {
        this.courriel = valeur;
    }

    /**
     * @return identifiant de l'utilisateur.
     */
    public String getIdentifiant() {
        return identifiant;
    }

    /**
     * @param valeur identifiant de l'utilisateur
     */
    public void setIdentifiant(final String valeur) {
        this.identifiant = valeur;
    }

    /**
     * @return adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param valeur adresse
     */
    public void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    /**
     * @return Mot de passe de l'utilisateur.
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * @param valeur Mot de passe de l'utilisateur.
     */
    public void setMotDePasse(String valeur) {
        this.motDePasse = valeur;
    }

    /**
     * @return Code postal de l'utilisateur.
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param valeur Code postal de l'utilisateur.
     */
    public void setCodePostal(String valeur) {
        this.codePostal = valeur;
    }

    /**
     * @return Ville de l'utilisateur.
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param valeur Ville de l'utilisateur.
     */
    public void setVille(String valeur) {
        this.ville = valeur;
    }

    /**
     * @return Telephone de l'utilisateur.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param valeur Telephone de l'utilisateur.
     */
    public void setTelephone(String valeur) {
        this.telephone = valeur;
    }

    /**
     * @return Fax de l'utilisateur.
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param valeur Fax de l'utilisateur.
     */
    public void setFax(String valeur) {
        this.fax = valeur;
    }

    /**
     * @return the acronymOrganism
     */
    public String getCodeOrganisme() {
        return codeOrganisme;
    }

    /**
     * @param valeur the acronymOrganism to set
     */
    public void setCodeOrganisme(String valeur) {
        this.codeOrganisme = valeur;
    }

    /**
     * @return Ensemble des profil/type de marche lies a un utilisateur.
     */
    public Map<EpmTProfil, EpmTProfilUtilisateur> getProfilUtilisateurMap() {
        return profilUtilisateurMap;
    }

    /**
     * @param valeur Ensemble des profil/type de marche lies a un utilisateur.
     */
    public void setProfilUtilisateurMap(Map<EpmTProfil, EpmTProfilUtilisateur> valeur) {
        this.profilUtilisateurMap = valeur;
    }


    /**
     * @return the clefPub
     */
    public String getClef() {
        return clef;
    }

    public void setClef(String valeur) {
        this.clef = valeur;
    }

    /**
     * Retourne l'identifiant de l'organisme
     */
    public Integer getIdOrganisme(){
        if (getEpmTRefOrganisme() != null)
            return getEpmTRefOrganisme().getId();
        else
            return null;
    }

    /**
     * Afficher les profils de cet utilisateur et ses habilitations
     */
    public String afficherListeHabilitation() {

        StringBuilder message = new StringBuilder();

        try {
            message.append("\n");
            message.append("La liste habilitation par profil de l'utilisateur :\n");
            message.append("Nom de l'utilisateur: ").append(nom).append("\n");
            message.append("Prenom de l'utilisateur: ").append(prenom).append("\n");

            if (this.getProfil() != null) {
                Iterator it = this.getProfil().iterator();
                while (it.hasNext()) {
                    EpmTProfil profil = (EpmTProfil) it.next();
                    message.append("  - Habilitations du profil ")
                            .append(profil.getNom()).append(" : \n");
                    if (profil.getHabilitationAssocies() != null) {
                        for (Object objet : profil.getHabilitationAssocies()) {
                            EpmTHabilitation habilitation = (EpmTHabilitation) objet;
                            message.append("      ")
                                    .append(habilitation.getLibelle())
                                    .append(" \n");
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("Erreur pendant la récupération de la liste habilitation de l'utilisateur ");
        }

        return message.toString();
    }

    /**
     * Renvoie la liste des type de marche correspondant a un profil.
     * @param valeur le profil
     * @return la liste des type de marche correspondant
     */
    public final List<EpmTRefTypeMarche> getListeTypeMarches(final EpmTProfil valeur) {
        if (profilUtilisateurMap != null) {
            EpmTProfilUtilisateur profilUtilisateur = profilUtilisateurMap.get(valeur);
            return profilUtilisateur.getTypeMarcheProfilSet().stream()
                    .map(EpmTTypeMarcheProfil::getTypeMarche)
                    .collect(Collectors.toList());
        }
        return null;
    }

    /**
     * Cette méthode associe à un profil des type de marchés.
     * @param key profil
     * @param valeur ensembled des type de marche lies au profil
     */
    public final void putProfil(final EpmTProfil key, final Set<EpmTTypeMarcheProfil> valeur) {
        if (profilUtilisateurMap == null)
            profilUtilisateurMap = new HashMap<EpmTProfil, EpmTProfilUtilisateur>();
        EpmTProfilUtilisateur profilUtilisateur = new EpmTProfilUtilisateur();
        profilUtilisateur.setProfil(key);
        profilUtilisateur.setTypeMarcheProfilSet(valeur);
        profilUtilisateurMap.put(key, profilUtilisateur);
    }


    /**
     * @return Collection de {@link EpmTProfil}
     */
    @JsonIgnore
    public final Collection<EpmTProfil> getProfil() {
        if (profilUtilisateurMap != null)
            return profilUtilisateurMap.keySet();
        return null;
    }

	@Override
	public String toString() {
		return identifiant == null ? guidSso : identifiant;
	}

	public String getNomComplet() {
		return this.prenom + " " + this.nom.toUpperCase();
	}
}
