package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefPresence de la table "epm__t_ref_presence"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefPresence extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_PRESENT = 1;
    public static final int ID_EXCUSE = 2;
    public static final int ID_ABSENT = 3;

}