package fr.paris.epm.noyau.persistance.referentiel;

import java.util.Set;

/**
 * POJO EpmTRefDomaineDonnees de la table "epm__t_ref_domaine_donnees"
 * Domaine de données d'un champ de fusion. Définie l'ensemble des données à
 * récuperer pour afficher les inforations d'un champs de fusion.
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDomaineDonnees extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Nom de la méthode à invoquer pour charger les données du domaine.
     */
    private String methode;

    /**
     * Domaine parent.
     */

    private EpmTRefDomaineDonnees parent;

    /**
     * Liste des sous domaines de données.
     */
    private Set<EpmTRefDomaineDonnees> sousDomaine;

    /**
     * Defini si le concerne à un champ de fusion du module execution
     */
    private boolean moduleExecution;

    public String getMethode() {
        return methode;
    }

    public void setMethode(String methode) {
        this.methode = methode;
    }

    public EpmTRefDomaineDonnees getParent() {
        return parent;
    }

    public void setParent(EpmTRefDomaineDonnees parent) {
        this.parent = parent;
    }

    public Set<EpmTRefDomaineDonnees> getSousDomaine() {
        return sousDomaine;
    }

    public void setSousDomaine(Set<EpmTRefDomaineDonnees> sousDomaine) {
        this.sousDomaine = sousDomaine;
    }

    public boolean isModuleExecution() {
        return moduleExecution;
    }

    public void setModuleExecution(boolean moduleExecution) {
        this.moduleExecution = moduleExecution;
    }

}