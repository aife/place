package fr.paris.epm.noyau.persistance;

public class EpmTDocumentMarchePublicSimplifie extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808544L;

    /**
     * clé primaire
     */
    private int id;

    /**
     * Les données communes du document
     */
    private EpmTDocument document;

    /**
     * L'id du fichier dans MPE
     */
    private Integer idFichierDemat;

    /**
     * Permet de vérifier l'intégrité d'un fichier
     */
    private String md5Checksum;

    /**
     * Le dépot associé
     */
    private Integer idDepot;

    public int getId() {
        return id;
    }

    public void setId(int valeur) {
        this.id = valeur;
    }

    public EpmTDocument getDocument() {
        return document;
    }

    public void setDocument(EpmTDocument valeur) {
        this.document = valeur;
    }

    public Integer getIdFichierDemat() {
        return idFichierDemat;
    }

    public void setIdFichierDemat(Integer valeur) {
        this.idFichierDemat = valeur;
    }

    public String getMd5Checksum() {
        return md5Checksum;
    }

    public void setMd5Checksum(String valeur) {
        this.md5Checksum = valeur;
    }

    public Integer getIdDepot() {
        return idDepot;
    }

    public void setIdDepot(Integer valeur) {
        this.idDepot = valeur;
    }

}
