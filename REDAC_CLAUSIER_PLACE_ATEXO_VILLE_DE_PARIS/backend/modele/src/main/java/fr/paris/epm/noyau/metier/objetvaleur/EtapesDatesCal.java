/**
 * $Id$
 */
package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;

/**
 * Objet contenant les étapes simplifiées d'un calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EtapesDatesCal implements Serializable, Cloneable {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Étape de lancement.
     */
    private EtapeSimple lancement;

    /**
     * Étape de remise des candidatures.
     */
    private EtapeSimple limiteRemiseCandidatures;

    /**
     * Étape de remise des offres (également remise plis si ouvert).
     */
    private EtapeSimple limiteRemiseOffres;
    /**
     * Etape limite de remise des maquettes.
     */
    private EtapeSimple limiteRemiseMaquettes;

    /**
     * Etape avis du conseil d'arrondissement (delib amont - dsp).
     */
    private EtapeSimple conseilArrondissement;

    /**
     * Etape avis du conseil de paris.
     */
    private EtapeSimple conseilParis;

    /**
     * Etape validation de la liste des candidats autorisés à présenter une
     * offre.
     */
    private EtapeSimple validationCandidatsOffre;

    /**
     * Etape Analyse, négociation et Analyse des offres pour les écrans
     * validation du classement des offres / validation du classement des
     * projets.
     */
    private EtapeSimple analyseDesOffres;

    /**
     * Etape proposition de classement, date du classement des offres par le PA.
     * (Recommandation attrib Present PA).
     */
    private EtapeSimple classementOffresPA;

    /**
     * Etape choix des loreat date de validation du classement des offres par le
     * PA. (attribution sous reserve PA).
     */
    private EtapeSimple validationClassementOffresPA;

    /**
     * Etape d'attribution.
     */
    private EtapeSimple attribution;

    /**
     * Etape de recommandation de délégataire pressentis dnas le cas de dsp o
     * /r.
     */
    private EtapeSimple recommandationDelegatairePressentis;

    /**
     * Etape vote du conseil de paris suivi délib aval.
     */
    private EtapeSimple conseilParisAval;

    /**
     * Etape enregistrement controle legalité écran attribution marché. Dans le
     * cas d'une consultaiton allotie on prend la derniére date.
     */
    private EtapeSimple enregistrementControleLegalite;

    /**
     * Etape notification attribution écran attribution marché. Dans le cas
     * d'une consultaiton allotie on prend la derniére date.
     */
    private EtapeSimple notificationAttribution;

    /**
     * Etape d'envoi d'avis d'attribution.
     */
    private EtapeSimple dateAvisAttribution;
    
    /**
     * Correspond a la date de la premiere commission.
     */
    private EtapeSimple dateOuvertureCandidature;
    /**
     * Date de l'ouverture des offres.
     */
    private EtapeSimple dateOuvertureOffre;
    /**
     * Date CAO de selection des candidatures.
     */
    private EtapeSimple dateSelectionCandidatureCao;

    private EtapeSimple dateDeliberationAmont;




    // Accesseurs
    /**
     * @return étape lancement
     */
    public final EtapeSimple getLancement() {
        return lancement;
    }

    /**
     * @param valeur étape lancement
     */
    public final void setLancement(final EtapeSimple valeur) {
        this.lancement = valeur;
    }

    /**
     * @return étape limite de remise des offres (également remise plis si
     *         ouvert)
     */
    public final EtapeSimple getLimiteRemiseOffres() {
        return limiteRemiseOffres;
    }

    /**
     * @param valeur étape limite de remise des offres (également remise plis si
     *            ouvert)
     */
    public final void setLimiteRemiseOffres(final EtapeSimple valeur) {
        this.limiteRemiseOffres = valeur;
    }

    /**
     * @return étape limite de remise des candidatures
     */
    public final EtapeSimple getLimiteRemiseCandidatures() {
        return limiteRemiseCandidatures;
    }

    /**
     * @param valeur étape limite de remise des candidatures
     */
    public final void setLimiteRemiseCandidatures(final EtapeSimple valeur) {
        this.limiteRemiseCandidatures = valeur;
    }

    /**
     * @return étape limite de remise des maquettes.
     */
    public final EtapeSimple getLimiteRemiseMaquettes() {
        return limiteRemiseMaquettes;
    }

    /**
     * @param valeur étape limite de remise des maquettes.
     */
    public final void setLimiteRemiseMaquettes(final EtapeSimple valeur) {
        this.limiteRemiseMaquettes = valeur;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * @return etape conseil d'arrondissment (lancement - delib amont - dsp)
     */
    public final EtapeSimple getConseilArrondissement() {
        return conseilArrondissement;
    }

    /**
     * @param valeur etape conseil d'arrondissment (lancement - delib amont dsp)
     */
    public final void setConseilArrondissement(final EtapeSimple valeur) {
        this.conseilArrondissement = valeur;
    }

    /**
     * @return etape conseil de paris (lancement - delib amont)
     */
    public final EtapeSimple getConseilParis() {
        return conseilParis;
    }

    /**
     * @param valeur etape conseil de paris (lancement - delib amont)
     */
    public final void setConseilParis(final EtapeSimple valeur) {
        this.conseilParis = valeur;
    }

    /**
     * @return Etape validation de la liste des candidats autorisés à présenter
     *         une offre.
     */
    public final EtapeSimple getValidationCandidatsOffre() {
        return validationCandidatsOffre;
    }

    /**
     * @param valeur Etape validation de la liste des candidats autorisés à
     *            présenter une offre.
     */
    public final void setValidationCandidatsOffre(final EtapeSimple valeur) {
        this.validationCandidatsOffre = valeur;
    }

    /**
     * @return Etape Analyse, négociation et Analyse des offres pour les écrans
     *         validation du classement des offres / validation du classement
     *         des projets.
     */
    public final EtapeSimple getAnalyseDesOffres() {
        return analyseDesOffres;
    }

    /**
     * @param valeur Etape Analyse, négociation et Analyse des offres pour les
     *            écrans validation du classement des offres / validation du
     *            classement des projets.
     */
    public final void setAnalyseDesOffres(final EtapeSimple valeur) {
        this.analyseDesOffres = valeur;
    }

    /**
     * @return Etape proposition de classement, date du classement des offres
     *         par le PA. (Recommandation attrib Present PA).
     */
    public final EtapeSimple getClassementOffresPA() {
        return classementOffresPA;
    }

    /**
     * @param valeur Etape proposition de classement, date du classement des
     *            offres par le PA. (Recommandation attrib Present PA).
     */
    public final void setClassementOffresPA(final EtapeSimple valeur) {
        this.classementOffresPA = valeur;
    }

    /**
     * @return Etape choix des loreat date de validation du classement des
     *         offres par le PA. (attribution sous reserve PA).
     */
    public final EtapeSimple getValidationClassementOffresPA() {
        return validationClassementOffresPA;
    }

    /**
     * @param valeur Etape choix des loreat date de validation du classement des
     *            offres par le PA. (attribution sous reserve PA).
     */
    public final void setValidationClassementOffresPA(
            final EtapeSimple valeur) {
        this.validationClassementOffresPA = valeur;
    }

    /**
     * @return the attribution
     */
    public final EtapeSimple getAttribution() {
        return attribution;
    }

    /**
     * @param valeur date de validation de l'écran de confirmation
     *            d'attribution.
     */
    public final void setAttribution(final EtapeSimple valeur) {
        this.attribution = valeur;
    }

    /**
     * @return Etape de recommandation de délégataire pressentis dnas le cas de
     *         dsp o /r.
     */
    public final EtapeSimple getRecommandationDelegatairePressentis() {
        return recommandationDelegatairePressentis;
    }

    /**
     * @param valeur Etape de recommandation de délégataire pressentis dnas le
     *            cas de dsp o /r.
     */
    public final void setRecommandationDelegatairePressentis(
            final EtapeSimple valeur) {
        this.recommandationDelegatairePressentis = valeur;
    }

    /**
     * @return Etape vote du conseil de paris suivi délib aval.
     */
    public final EtapeSimple getConseilParisAval() {
        return conseilParisAval;
    }

    /**
     * @param valeur Etape vote du conseil de paris suivi délib aval.
     */
    public final void setConseilParisAval(final EtapeSimple valeur) {
        this.conseilParisAval = valeur;
    }

    /**
     * @return Etape enregistrement controle legalité écran attribution marché.
     *         Dans le cas d'une consultaiton allotie on prend la derniére date.
     */
    public final EtapeSimple getEnregistrementControleLegalite() {
        return enregistrementControleLegalite;
    }

    /**
     * @param valeur Etape enregistrement controle legalité écran attribution
     *            marché. Dans le cas d'une consultaiton allotie on prend la
     *            derniére date.
     */
    public final void setEnregistrementControleLegalite(
            final EtapeSimple valeur) {
        this.enregistrementControleLegalite = valeur;
    }

    /**
     * @return Etape notification attribution écran attribution marché. Dans le
     *         cas d'une consultaiton allotie on prend la derniére date.
     */
    public final EtapeSimple getNotificationAttribution() {
        return notificationAttribution;
    }

    /**
     * @param valeur Etape notification attribution écran attribution marché.
     *            Dans le cas d'une consultaiton allotie on prend la derniére
     *            date.
     */
    public final void setNotificationAttribution(final EtapeSimple valeur) {
        this.notificationAttribution = valeur;
    }

    /**
     * @return Etape d'envoi d'avis d'attribution.
     */
    public final EtapeSimple getDateAvisAttribution() {
        return dateAvisAttribution;
    }

    /**
     * @param valeur Etape d'envoi d'avis d'attribution.
     */
    public final void setDateAvisAttribution(EtapeSimple valeur) {
        this.dateAvisAttribution = valeur;
    }

    /**
     * @return Correspond a la date de la premiere commission.
     */
	public final EtapeSimple getDateOuvertureCandidature() {
		return dateOuvertureCandidature;
	}

	/**
	 * @param valeur Correspond a la date de la premiere commission.
	 */
	public final void setDateOuvertureCandidature(
			final EtapeSimple valeur) {
		this.dateOuvertureCandidature = valeur;
	}
	/**
	 * @param valeur Date de l'ouverture des offres.
	 */
    public void setDateOuvertureOffre(EtapeSimple valeur) {
       this.dateOuvertureOffre = valeur;
        
    }
    /**
     * @return Date de l'ouverture des offres.
     */
    public final EtapeSimple getDateOuvertureOffre() {
        return dateOuvertureOffre;
    }
    /**
     * @param valeur date CAO de selection des candidatures.
     */
    public void setDateSelectionCandidatureCao(EtapeSimple valeur) {
        this.dateSelectionCandidatureCao = valeur;
    }
    /**
     * @return date CAO de selection des candidatures.
     */
    public final EtapeSimple getDateSelectionCandidatureCao() {
        return dateSelectionCandidatureCao;
    }


    public EtapeSimple getDateDeliberationAmont() {
        return dateDeliberationAmont;
    }

    public void setDateDeliberationAmont(EtapeSimple dateDeliberationAmont) {
        this.dateDeliberationAmont = dateDeliberationAmont;
    }


}
