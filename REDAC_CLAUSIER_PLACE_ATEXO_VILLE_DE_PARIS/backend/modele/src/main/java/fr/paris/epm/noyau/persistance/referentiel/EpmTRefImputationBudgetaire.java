package fr.paris.epm.noyau.persistance.referentiel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO EpmTRefImputationBudgetaire de la table "epm__t_ref_imputation_budgetaire"
 * Created by nty on 16/05/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefImputationBudgetaire extends BaseEpmTRefReferentiel {

    private static final Logger LOG = LoggerFactory.getLogger(EpmTRefImputationBudgetaire.class);
    private static final long serialVersionUID = 1L;

    /**
     * Type de budget
     */
	private EpmTRefTypeBudget epmTRefTypeBudget;

    /**
     * Montant de l'imputation budgetaire
     */
	private double montant;

    /**
     * Année d'application
     */
	private int anneeApplication;

    @Override
    public void setCodeExterne(String codeExterne) {
        // ajouter "code_externe" dans la base de données s'il faut l'avoir
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"code_externe\" dans la base de données s'il faut l'avoir");
    }

    public EpmTRefTypeBudget getEpmTRefTypeBudget() {
        return epmTRefTypeBudget;
    }

    public void setEpmTRefTypeBudget(final EpmTRefTypeBudget epmTRefTypeBudget) {
        this.epmTRefTypeBudget = epmTRefTypeBudget;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(final double montant) {
        this.montant = montant;
    }

    public int getAnneeApplication() {
        return anneeApplication;
    }

    public void setAnneeApplication(final int anneeApplication) {
        this.anneeApplication = anneeApplication;
    }

}
