package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineClauseSociale {

    private Boolean clauseSociale;
    private DomaineStructureSociale execution;
    private DomaineStructureSociale attribution;
    private DomaineStructureSociale reserver;
    private DomaineStructureSociale specification;
    private DomaineStructureSociale insertion;

    public Boolean getClauseSociale() {
        return clauseSociale;
    }

    public void setClauseSociale(Boolean clauseSociale) {
        this.clauseSociale = clauseSociale;
    }

    public DomaineStructureSociale getExecution() {
        return execution;
    }

    public void setExecution(DomaineStructureSociale execution) {
        this.execution = execution;
    }

    public DomaineStructureSociale getAttribution() {
        return attribution;
    }

    public void setAttribution(DomaineStructureSociale attribution) {
        this.attribution = attribution;
    }

    public DomaineStructureSociale getReserver() {
        return reserver;
    }

    public void setReserver(DomaineStructureSociale reserver) {
        this.reserver = reserver;
    }

    public DomaineStructureSociale getSpecification() {
        return specification;
    }

    public void setSpecification(DomaineStructureSociale specification) {
        this.specification = specification;
    }

    public DomaineStructureSociale getInsertion() {
        return insertion;
    }

    public void setInsertion(DomaineStructureSociale insertion) {
        this.insertion = insertion;
    }
}
