package fr.paris.epm.noyau.metier;

import java.util.Date;
import java.util.Map;

/**
 * Pour rechercher les informations d'authentification inter-module d'un utilisateur. 
 *
 * @author Rémi Villé
 * @version $Revision: $, $Date: $, $Author: $
 */
public class AuthentificationTokenCritere extends AbstractCritere {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1819959683365343992L;

    /**
     * Login utilisateur.
     */
    private String identifiant;

    /**
     * Delai maximal d'inactivite (en minute).
     */
    private Integer delaiInactiviteMax;

    private String signature;

    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;
        Map parametres = getParametres();

        sb.append("from EpmTAuthentificationToken a");
        if (identifiant != null) {
            sb.append(" where lower(a.identifiant) = lower(:identifiant)");
            parametres.put("identifiant", identifiant);
            debut = false;
        }

        if (delaiInactiviteMax != null) {
            if (debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            debut = false;
            Date dateDerniereActiviteMax = new Date();
            dateDerniereActiviteMax.setTime(dateDerniereActiviteMax.getTime() - (delaiInactiviteMax * 60 * 1000));
            sb.append(" a.dateDerniereActivite < :dateDerniereActiviteMax ");
            parametres.put("dateDerniereActiviteMax", dateDerniereActiviteMax);
        }

        if (signature != null) {
            if (debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            debut = false;
            sb.append(" a.signature = :signature ");
            parametres.put("signature", signature);
        }

        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        return corpsRequete().toString();
    }

    public String toCountHQL() {
        return "select count(identifiant) " + corpsRequete();
    }

    public final void setIdentifiant(final String valeur) {
        this.identifiant = valeur;
    }

    public final void setDelaiInactiviteMax(final Integer valeur) {
        this.delaiInactiviteMax = valeur;
    }

    public final void setSignature(final String valeur) {
        this.signature = valeur;
    }

}
