package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefStatutEntreprise;

/**
 * Une entreprise, appartenant à un groupement d'entreprises.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTEntrepriseGroupe extends EpmTAbstractObject {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * numéro siren.
     */
    private String siren;

    /**
     * nom du groupement.
     */
    private String nom;

    /**
     * email du groupement.
     */
    private String courriel;

    /**
     * numéro de téléphone.
     */
    private String telephone;

    /**
     * numéro du fax.
     */
    private String telecopie;

    /**
     * adresse du groupement.
     */
    private String adresse;

    /**
     * statut de l'entreprise.
     */
    private EpmTRefStatutEntreprise statutEntreprise;

    /**
     * @return adresse de l'entreprise.
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param valeur adresse de l'entreprise
     */
    public void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    /**
     * @return email de l'entreprise
     */
    public String getCourriel() {
        return courriel;
    }

    /**
     * @param valeur email de l'entreprise
     */
    public void setCourriel(final String valeur) {
        this.courriel = valeur;
    }

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return nom du groupement
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur nom du groupement
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return numéro de siren
     */
    public String getSiren() {
        return siren;
    }

    /**
     * @param valeur numero de siren
     */
    public void setSiren(final String valeur) {
        this.siren = valeur;
    }

    /**
     * @return numé de fax
     */
    public String getTelecopie() {
        return telecopie;
    }

    /**
     * @param valeur numéro de fax
     */
    public void setTelecopie(final String valeur) {
        this.telecopie = valeur;
    }

    /**
     * @return numéro de téléphone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param valeur numéro de téléphone
     */
    public void setTelephone(final String valeur) {
        this.telephone = valeur;
    }

    /**
     * @return statut de l'entreprise
     */
    public EpmTRefStatutEntreprise getStatutEntreprise() {
        return statutEntreprise;
    }

    /**
     * @param valeur statut de l'entreprise
     */
    public void setStatutEntreprise(final EpmTRefStatutEntreprise valeur) {
        this.statutEntreprise = valeur;
    }

    /**
     * @return hash code de l'objet
     */
    public final int hashCode() {
        int result = 1;
        if (adresse != null) {
            result += Constantes.PREMIER * result + adresse.hashCode();
        }
        if (nom != null) {
            result += Constantes.PREMIER * result + nom.hashCode();
        }
        if (siren != null) {
            result += Constantes.PREMIER * result + siren.hashCode();
        }
        return result;
    }

    /**
     * Comparaison de l'objet.
     * @param obj {@link Object}
     * @return boolean
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EpmTEntrepriseGroupe other = (EpmTEntrepriseGroupe) obj;
        if (adresse == null) {
            if (other.adresse != null) {
                return false;
            }
        } else if (!adresse.equals(other.adresse)) {
            return false;
        }
        if (nom == null) {
            if (other.nom != null) {
                return false;
            }
        } else if (!nom.equals(other.nom)) {
            return false;
        }
        if (siren == null) {
            if (other.siren != null) {
                return false;
            }
        } else if (!siren.equals(other.siren)) {
            return false;
        }
        return true;
    }

    /**
     * Cette méthode affiche le contenu de l'objet.
     * @return String
     */
    public final String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nIdentifiant: ");
        res.append(id);
        res.append("\nsiren: ");
        res.append(siren);
        res.append("\nom: ");
        res.append(nom);
        res.append("\ncourriel : ");
        res.append(courriel);
        res.append("\ntéléphone: ");
        res.append(telephone);
        res.append("\ntélécopie: ");
        res.append(telecopie);
        res.append("\nadresse: ");
        res.append(adresse);
        return res.toString();
    }

}
