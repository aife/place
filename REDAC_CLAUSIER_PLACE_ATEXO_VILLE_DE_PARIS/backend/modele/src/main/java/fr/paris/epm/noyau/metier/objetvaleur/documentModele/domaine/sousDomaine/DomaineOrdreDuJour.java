package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * commission.domaineOrdreDuJour.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineOrdreDuJour implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private List<DomaineConsultationOdj> listeConsultationOdj;
    
    private String dateReunionOdj;

    private String heureReunionOdj;

    private String lieuReunionOdj;

    private String etapeConsultationODJ;


    private List<DomaineConsultationVirtuelleOdj> listeConsultationVirtuelleOdj;


    private DomaineConsultationOdj consultationOdj ;
    /**
     * Nombre total de dossier.
     */
    private int nbDossier;

    private DomaineOrdreDuJourNombreDossier nbDossierOuverture = new DomaineOrdreDuJourNombreDossier();

    private DomaineOrdreDuJourNombreDossier nbDossierAttribution = new DomaineOrdreDuJourNombreDossier();
    
    private DomaineOrdreDuJourNombreDossier nbDossierSelection = new DomaineOrdreDuJourNombreDossier();
    
    private int nbDossierAvenantTotal;

    public final List<DomaineConsultationOdj> getListeConsultationOdj() {
        return listeConsultationOdj;
    }

    public final void setListeConsultationOdj(final List<DomaineConsultationOdj> valeur) {
        this.listeConsultationOdj = valeur;
    }

    public final String getDateReunionOdj() {
        return dateReunionOdj;
    }

    public final void setDateReunionOdj(final String valeur) {
        this.dateReunionOdj = valeur;
    }

    public final String getHeureReunionOdj() {
        return heureReunionOdj;
    }

    public final void setHeureReunionOdj(final String valeur) {
        this.heureReunionOdj = valeur;
    }

    public final String getLieuReunionOdj() {
        return lieuReunionOdj;
    }

    public final void setLieuReunionOdj(final String valeur) {
        this.lieuReunionOdj = valeur;
    }

    /**
     * @return Nombre total de dossier.
     */
    public final int getNbDossier() {
        return nbDossier;
    }

    /**
     * @param valeur Nombre total de dossier.
     */
    public final void setNbDossier(final int valeur) {
        this.nbDossier = valeur;
    }

    public final DomaineOrdreDuJourNombreDossier getNbDossierOuverture() {
        return nbDossierOuverture;
    }

    public final void setNbDossierOuverture(final DomaineOrdreDuJourNombreDossier valeur) {
        this.nbDossierOuverture = valeur;
    }

    public final DomaineOrdreDuJourNombreDossier getNbDossierAttribution() {
        return nbDossierAttribution;
    }

    public final void setNbDossierAttribution(final DomaineOrdreDuJourNombreDossier valeur) {
        this.nbDossierAttribution = valeur;
    }

    /**
     * @return the listeConsultationVirtuelleOdj
     */
    public final List<DomaineConsultationVirtuelleOdj> getListeConsultationVirtuelleOdj() {
        return listeConsultationVirtuelleOdj;
    }

    /**
     * @param listeConsultationVirtuelleOdj the listeConsultationVirtuelleOdj to set
     */
    public final void setListeConsultationVirtuelleOdj(final List<DomaineConsultationVirtuelleOdj> valeur) {
        this.listeConsultationVirtuelleOdj = valeur;
    }

    public final DomaineOrdreDuJourNombreDossier getNbDossierSelection() {
        return nbDossierSelection;
    }

    public final void setNbDossierSelection(final DomaineOrdreDuJourNombreDossier valeur) {
        this.nbDossierSelection = valeur;
    }

    public final int getNbDossierAvenantTotal() {
        return nbDossierAvenantTotal;
    }

    public final void setNbDossierAvenantTotal(final int valeur) {
        this.nbDossierAvenantTotal = valeur;
    }

    public DomaineConsultationOdj getConsultationOdj() {
        return consultationOdj;
    }

    public void setConsultationOdj(DomaineConsultationOdj consultationOdj) {
        this.consultationOdj = consultationOdj;
    }

    public String getEtapeConsultationODJ() {
      if(listeConsultationOdj==null || listeConsultationOdj.isEmpty())
          return "Pas de consultation à l'ordre du jour";
        return consultationOdj!=null && consultationOdj.getLibelleEtape()!=null?consultationOdj.getLibelleEtape():"";
    }

    public void setEtapeConsultationODJ(String etapeConsultationODJ) {
        this.etapeConsultationODJ = etapeConsultationODJ;
    }
}
