package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.metier.AbstractCritere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe critère utilisée pour la recherche des documents.
 *
 * @author Regis Menet
 */
public class CanevasCritere extends AbstractCritere {

    protected final String plateformeUuid;

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CanevasCritere.class);

    /**
     * sérialiseur.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant de l'organisme auquel la clause est liée
     */
    private Integer idOrganisme;

    /**
     * Détermine si le canevas est un canevas editeur ou client
     */
    private Boolean editeur;

    /**
     * reference du canevas.
     */
    private String reference;

    /**
     * La liste des ID canevas
     */
    private List<Integer> listIds;

    /**
     * titre du canevas.
     */
    private String titre;

    /**
     * procedure de passation du canevas.
     */
    private Integer procedure;

    /**
     * nature de prestation du canevas.
     */
    private int naturePrestation;

    /**
     * La liste de CCAG que le canevas doit correspondre
     */
    private List<Integer> listeIdCCAG;

    /**
     * le type de document pour laquelle la clause est construite.
     */
    private int idTypeDocument;

    private Boolean typeDocumentActif;

    private Integer idTypeContrat;

    private Boolean actif;

    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private boolean canevasCompatibleEntiteAdjudicatrice = true;

    /**
     * L'identifiant du statut de redaction de la clause
     */
    private Integer idStatutRedactionClausier;

    /**
     * Client ou Editeur
     */
    private int auteur = 0;

    private Date dateModificationDebut = null;

    private Date dateModificationFin = null;

    public CanevasCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }

    public CanevasCritere() {
        this.plateformeUuid = null;
    }


    public StringBuffer corpsRequete() {
        StringBuffer canevasAnd = new StringBuffer();

        canevasAnd.append(" WHERE canevas.etat != '2' ");

        if (reference != null && !reference.isEmpty()) {
            canevasAnd.append(" AND LOWER(canevas.reference) like LOWER(:reference)");
            getParametres().put("reference", "%" + reference.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE).trim() + "%");
        }

        if (listIds != null && !listIds.isEmpty()) {
            canevasAnd.append(findByIdCanevas());
            getParametres().put("listIds", listIds);
        }

        // recherche par titre
        if (titre != null) {
            canevasAnd.append(" AND LOWER(canevas.titre) like LOWER(:titre)");
            getParametres().put("titre", "%" + titre.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE).trim() + "%");
        }

        // recherche par procedure
        if (procedure != null && procedure != 0) {
            canevasAnd.append(" AND ").append(findByProcedure());
            getParametres().put("idProcedure", procedure);
        } /*else*/
        if (idTypeContrat != null && idTypeContrat != 0) { // pas de recherche par Type Contrat et par Procedure en meme temps
            canevasAnd.append(" AND ").append(findByTypeContrat());
            getParametres().put("idTypeContrat", idTypeContrat);
        }

        // recherche par nature de prestation
        if (naturePrestation > 0) {
            canevasAnd.append(" AND (canevas.idNaturePrestation = 0 OR canevas.idNaturePrestation = :idNature)");
            getParametres().put("idNature", naturePrestation);
        }

        if (null != listeIdCCAG && !listeIdCCAG.isEmpty()) {
            if (!listeIdCCAG.stream().allMatch(ccag -> ccag == 0)) {
                var clause = " AND (";

                var ccags = listeIdCCAG.stream().map(String::valueOf).collect(Collectors.joining(", "));

                clause += " ( canevas.idRefCCAG IN (" + ccags + ") ) ";

                if (listeIdCCAG.stream().anyMatch(ccag -> ccag == 0)) {
                    clause += "OR ( canevas.idRefCCAG IS NULL ) ";
                }

                clause += ")";
                canevasAnd.append(clause);
            }
        }

        if (idTypeDocument > 1) {
            canevasAnd.append(" AND (canevas.epmTRefTypeDocument.id = 1 OR canevas.epmTRefTypeDocument.id = :idTypeDocument)");
            getParametres().put("idTypeDocument", idTypeDocument);
        }

        if (typeDocumentActif != null) {
            canevasAnd.append(" AND canevas.epmTRefTypeDocument.actif = :typeDocumentActif");
            getParametres().put("typeDocumentActif", typeDocumentActif);
        }

        if (actif != null) {
            canevasAnd.append(" AND canevas.actif = ").append(actif);
        }

        if (!canevasCompatibleEntiteAdjudicatrice) {
            canevasAnd.append(" AND canevas.compatibleEntiteAdjudicatrice = :canevasCompatibleEntiteAdjudicatrice");
            getParametres().put("canevasCompatibleEntiteAdjudicatrice", canevasCompatibleEntiteAdjudicatrice);
        }

        if (idOrganisme != null && idOrganisme != 0) {
            canevasAnd.append(" AND (canevas.canevasEditeur = true OR canevas.idOrganisme = :idOrganisme)");
            getParametres().put("idOrganisme", idOrganisme);
        }
        if (plateformeUuid != null) {
            canevasAnd.append(" AND (epmTRefOrganisme.plateformeUuid = :plateformeUuid AND canevas.idOrganisme = epmTRefOrganisme.id)");
            getParametres().put("plateformeUuid", plateformeUuid);
        }

        if (editeur != null) {// le traitement pour la recherche dans Administration Editeur
            canevasAnd.append(" AND canevas.canevasEditeur = :editeur");
            getParametres().put("editeur", editeur);
        }

        if (idStatutRedactionClausier != null && idStatutRedactionClausier != 0) {
            canevasAnd.append(" AND canevas.epmTRefStatutRedactionClausier.id = :idStatutRedactionClausier");
            getParametres().put("idStatutRedactionClausier", idStatutRedactionClausier);
        }

        if (auteur != 0) {
            if (auteur == 2) canevasAnd.append(" AND canevas.canevasEditeur = true");
            else canevasAnd.append(" AND canevas.canevasEditeur = false");
        }

        if (dateModificationDebut != null) {
            canevasAnd.append(" AND canevas.dateDerniereValidation > :dateModificationDebut");
            getParametres().put("dateModificationDebut", dateModificationDebut);
        }

        if (dateModificationFin != null) {
            canevasAnd.append(" AND canevas.dateDerniereValidation < :dateModificationFin ");
            getParametres().put("dateModificationFin", dateModificationFin);
        }

        return canevasAnd;
    }

    protected String findByIdCanevas() {
        return " AND canevas.id in (:listIds)";
    }

    /*
        il ne faut redéfinir cette méthode que dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVCanveas.epmTCanevas ou EpmVCanveas.epmTCanevasPub
     */
    protected String findByProcedure() {
        return "(size(canevas.epmTRefProcedures) = 0 OR (from EpmTRefProcedure where id = :idProcedure) in elements(canevas.epmTRefProcedures))" + " AND " + "(size(canevas.epmTRefTypeContrats) = 0 OR exists(from EpmTRefTypeContrat c where :idProcedure in elements(c.idsProcedures) and c in elements(canevas.epmTRefTypeContrats)))";
    }

    /*
        il ne faut redéfinir cette méthode que dans CanevasViewCritere
        car CanevasViewCritere cherche dans EpmVCanveas.epmTCanevas ou EpmVCanveas.epmTCanevasPub
     */
    protected String findByTypeContrat() {
        return "(size(canevas.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(canevas.epmTRefTypeContrats))";
    }

    protected String getTableDeRecherche() {
        return " EpmTCanevas ";
    }

    protected String getJoinsTables() {
        if (plateformeUuid != null) {
            return (" , EpmTRefOrganisme epmTRefOrganisme ");
        }
        return "";
    }

    public String toHQL() {
        StringBuilder sb = new StringBuilder("select canevas from ");
        sb.append(getTableDeRecherche());
        sb.append(" as canevas ");
        sb.append(getJoinsTables());
        sb.append(corpsRequete());
        if (getProprieteTriee() != null) {
            if (proprieteTriee.equals("epmTRefProcedures.id")) sb.append(" ORDER BY canevas.epmTRefProcedures.");
            else sb.append(" ORDER BY canevas.");
            sb.append(proprieteTriee);
            sb.append(triCroissant ? " ASC" : " DESC");
        }
        return sb.toString();
    }

    public String toCountHQL() {
        StringBuilder sb = new StringBuilder("select count(canevas.id) from ");
        sb.append(getTableDeRecherche());
        sb.append(" as canevas ");
        sb.append(getJoinsTables());
        sb.append(corpsRequete());
        return sb.toString();
    }

    public final void setReference(final String valeur) {
        reference = valeur;
    }

    public final void setProcedure(Integer procedure) {
        this.procedure = procedure;
    }

    public final void setNaturePrestation(final int valeur) {
        naturePrestation = valeur;
    }

    public final void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public final void setTypeDocumentActif(Boolean typeDocumentActif) {
        this.typeDocumentActif = typeDocumentActif;
    }

    public final void setTitre(String titre) {
        this.titre = titre;
    }

    public final void setActif(final Boolean valeur) {
        this.actif = valeur;
    }

    public void setListIds(List<Integer> listIds) {
        this.listIds = listIds;
    }

    public final void setCanevasCompatibleEntiteAdjudicatrice(final boolean valeur) {
        this.canevasCompatibleEntiteAdjudicatrice = valeur;
    }

    public final void setEditeur(final Boolean valeur) {
        this.editeur = valeur;
    }

    public final void setIdStatutRedactionClausier(final Integer valeur) {
        this.idStatutRedactionClausier = valeur;
    }

    public final void setIdCanevas(final Integer valeur) {
        this.setListIds(Collections.singletonList(valeur));
    }

    public final Integer getIdOrganisme() {
        return idOrganisme;
    }

    public final void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

    public final void setAuteur(int auteur) {
        this.auteur = auteur;
    }

    public final void setDateModificationDebut(final Date valeur) {
        this.dateModificationDebut = valeur;
    }

    public final void setDateModificationFin(final Date valeur) {
        this.dateModificationFin = valeur;
    }

    public final void setListeIdCCAG(final List<Integer> valeur) {
        this.listeIdCCAG = valeur;
    }

    public final void setIdTypeContrat(Integer idTypeContrat) {
        this.idTypeContrat = idTypeContrat;
    }
}
