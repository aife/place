package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Persistance des associations clause/valeurs des criteres potentiellement conditionnes.
 */
@Entity
@Table(name = "epm__t_clause_has_potentiellement_conditionnee_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_clause_potentiellement_conditionnee", "id_publication"}))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTClausePotentiellementConditionneePub extends EpmTClausePotentiellementConditionneeAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_clause_potentiellement_conditionnee", nullable = false)
    private Integer idClausePotentiellementConditionnee;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;

    @Column(name = "id_clause")
    private Integer idClause;

    @ManyToOne(targetEntity = EpmTClausePub.class)
    @JoinColumns({
            @JoinColumn(name = "id_clause", referencedColumnName = "id_clause", insertable = false, updatable = false),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication", insertable = false, updatable = false)
    })
    private EpmTClausePub epmTClause;

    @OneToMany(targetEntity = EpmTClauseValeurPotentiellementConditionneePub.class,
            mappedBy = "epmTClausePotentiellementConditionnee", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<EpmTClauseValeurPotentiellementConditionneePub> epmTClauseValeurPotentiellementConditionnees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdClausePotentiellementConditionnee() {
        return idClausePotentiellementConditionnee;
    }

    public void setIdClausePotentiellementConditionnee(Integer idClausePotentiellementConditionnee) {
        this.idClausePotentiellementConditionnee = idClausePotentiellementConditionnee;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdClause() {
        return idClause;
    }

    public void setIdClause(Integer idClause) {
        this.idClause = idClause;
    }

    @Override
    public EpmTClausePub getEpmTClause() {
        return epmTClause;
    }

    @Override
    public void setEpmTClause(EpmTClauseAbstract epmTClause) {
        this.epmTClause = (EpmTClausePub) epmTClause;
    }

    @Override
    public Set<EpmTClauseValeurPotentiellementConditionneePub> getEpmTClauseValeurPotentiellementConditionnees() {
        return epmTClauseValeurPotentiellementConditionnees;
    }

    @Override
    public void setEpmTClauseValeurPotentiellementConditionnees(Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotentiellementConditionnees) {
        this.epmTClauseValeurPotentiellementConditionnees = (Set<EpmTClauseValeurPotentiellementConditionneePub>) epmTClauseValeurPotentiellementConditionnees;
    }

    @Override
    public EpmTClausePotentiellementConditionneePub clone() throws CloneNotSupportedException {
        EpmTClausePotentiellementConditionneePub cpc = (EpmTClausePotentiellementConditionneePub) super.clone();
        cpc.idClausePotentiellementConditionnee = idClausePotentiellementConditionnee;
        cpc.idPublication = idPublication;
        cpc.epmTClause = null;

        cpc.epmTClauseValeurPotentiellementConditionnees = new HashSet<>();
        for (EpmTClauseValeurPotentiellementConditionneePub cvpc : getEpmTClauseValeurPotentiellementConditionnees()) {
            EpmTClauseValeurPotentiellementConditionneePub cvpcClone = cvpc.clone();
            cvpcClone.setEpmTClausePotentiellementConditionnee(cpc);
            cpc.epmTClauseValeurPotentiellementConditionnees.add(cvpcClone);
        }
        return cpc;
    }

}
