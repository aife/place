package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Collection;

@MappedSuperclass
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class EpmTClauseValeurPotentiellementConditionneeAbstract extends EpmTAbstractObject implements Cloneable, Serializable {

    /**
     * Identifiant de la valeur du critere potentiellement conditionne. (si 0 concerne toutes les procedures).
     */
    @Column(name = "valeur")
    private Integer valeur;

    public abstract Integer getIdClauseValeurPotentiellementConditionnee();

    public abstract Integer getIdPublication();

    public abstract EpmTClausePotentiellementConditionneeAbstract getEpmTClausePotentiellementConditionnee() ;

    public abstract void setEpmTClausePotentiellementConditionnee(EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee) ;

    public Integer getValeur() {
        return valeur;
    }

    public void setValeur(Integer valeur) {
        this.valeur = valeur;
    }

    public static boolean containsValue(Collection<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> collection, Integer valeur) {
        for (EpmTClauseValeurPotentiellementConditionneeAbstract item : collection)
            if (item != null && item.getValeur() != null && item.getValeur().equals(valeur))
                return true;
        return false;
    }

    @Override
    protected EpmTClauseValeurPotentiellementConditionneeAbstract clone() throws CloneNotSupportedException {
        EpmTClauseValeurPotentiellementConditionneeAbstract cvpc = (EpmTClauseValeurPotentiellementConditionneeAbstract) super.clone();
        cvpc.valeur = valeur;
        return cvpc;
    }

}
