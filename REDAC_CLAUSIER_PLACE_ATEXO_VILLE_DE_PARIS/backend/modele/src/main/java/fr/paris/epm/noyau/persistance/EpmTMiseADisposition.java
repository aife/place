/**
 * 
 */
package fr.paris.epm.noyau.persistance;

import java.util.Date;
import java.util.Set;

/**
 * POJO hibernate pour la mise a disposition
 * @author khaled.benari
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTMiseADisposition extends EpmTAbstractObject {

    /**
     * 
     */
    private static final long serialVersionUID = 1228605899499674068L;

    /**
     * clé primaire
     */
    private int id;
    /**
     * id del la consultation de reference
     */
    private Integer idConsultation;

    /**
     * id del la consultation de reference
     */
    private Integer idContrat;

    /**
     * date de la mise à disposition elle est null par defaut
     */
    private Date dateMiseADisposition;

    /**
     * date de recuperation de la mise à disposition elle est null par defaut
     */
    private Date dateRecuperation;

    /**
     * le statut de la mise a disposition decrit les etape du transfert avec les valeurs suivantes : 
    * <li><ul> 0:NON_TRAITE </ul><ul> 1:REUSSIS</ul><ul> 2:ECHEC<ul></li>
     * <br/> le statut de recuêration permi l'affichage 
     * cette valeur est mis à jour par le webservices mais qui sera afficher dans l'ecran mise à disposition 
     */
    private Integer statutMiseAdisposition;
   
    /**
     * le statut de la recuperation avec les valeurs suivantes: 
    * <li><ul> 0:En attente (en attente de mise à disposition)</ul><ul> 1:RECUPERE (Recupere)</ul><ul> 2:ACQUITTE (Acquitte)<ul></li>
     * <br/> si le transfert echoue il retourne à 1 :en attente pour permettre à la mise à disposition d'etre envoyé lors du brochain crone du connecteur 
     *<br/>Cette valeur est utlisé par le webservices seulement 
     */
    private int statutRecuperation;

    /**
     * l'application tiers de destination
     */
    private Integer idApplicationTiers;
    
    /**
     * ensemble documents liés à la mise a disposition
     */
    private Set<EpmTDocumentMiseADispositionPassation> documentsMiseADisposition;
    
    /**
     * Lorsque l’application tierce échoue dans la récupération des documents,
     * cette propriété stocke le message renvoyé par l’application tierce pour
     * expliciter les raisons de l’échec de la récupération.
     */
    private String raisonsEchec;
    
    /**
     * Indique si il s'agit d'une mise à disposition pour le module exécution.
     */
    private boolean execution = false;
    
    /**
     * Utilisateur.
     */
    private EpmTUtilisateur utilisateur;

    /**
     * @return the idMiseADisposition
     */
    public  int getId() {
        return id;
    }

    /**
     * @param idMiseADisposition the idMiseADisposition to set
     */
    public  void setId(int valeur) {
        this.id = valeur;
    }

    /**
     * @return the idConsultation
     */
    public  Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param idConsultation the idConsultation to set
     */
    public  void setIdConsultation(Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @return the idContrat
     */
    public  Integer getIdContrat() {
        return idContrat;
    }

    /**
     * @param idContrat the idContrat to set
     */
    public  void setIdContrat(Integer valeur) {
        this.idContrat = valeur;
    }

    /**
     * @return the dateMiseADisposition
     */
    public  Date getDateMiseADisposition() {
        return dateMiseADisposition;
    }

    /**
     * @param dateMiseADisposition the dateMiseADisposition to set
     */
    public  void setDateMiseADisposition(Date valeur) {
        this.dateMiseADisposition = valeur;
    }

    /**
     * @return the dateRecuperation
     */
    public  Date getDateRecuperation() {
        return dateRecuperation;
    }

    /**
     * @param dateRecuperation the dateRecuperation to set
     */
    public  void setDateRecuperation(Date valeur) {
        this.dateRecuperation = valeur;
    }

   /**
     * @return the statutRecuperation
     */
    public int getStatutRecuperation() {
        return statutRecuperation;
    }

    /**
     * @param statutRecuperation the statutRecuperation to set
     */
    public void setStatutRecuperation(int valeur) {
        this.statutRecuperation = valeur;
    }

    /**
     * @return the idApplicationTiers
     */
    public Integer getIdApplicationTiers() {
        return idApplicationTiers;
    }

    /**
     * @param idApplicationTiers the idApplicationTiers to set
     */
    public void setIdApplicationTiers(Integer valeur) {
        this.idApplicationTiers = valeur;
    }

    /**
     * @return the documentsMiseADisposition
     */
    public  Set<EpmTDocumentMiseADispositionPassation> getDocumentsMiseADisposition() {
        return documentsMiseADisposition;
    }

    /**
     * @param documentsMiseADisposition the documentsMiseADisposition to set
     */
    public  void setDocumentsMiseADisposition(
            Set<EpmTDocumentMiseADispositionPassation> valeur) {
        this.documentsMiseADisposition = valeur;
    }

    /**
     * @return the statutMiseAdisposition
     */
    public Integer getStatutMiseAdisposition() {
        return statutMiseAdisposition;
    }

    /**
     * @param statutMiseAdisposition the statutMiseAdisposition to set
     */
    public void setStatutMiseAdisposition(Integer valeur) {
        this.statutMiseAdisposition = valeur;
    }

    /**
     * @return raisonsEchec
     */
    public String getRaisonsEchec() {
        return raisonsEchec;
    }

    /**
     * @param raisonsEchec raisonsEchec to set
     */
    public void setRaisonsEchec(String valeur) {
        this.raisonsEchec = valeur;
    }

    public boolean isExecution() {
        return execution;
    }

    public void setExecution(final boolean valeur) {
        this.execution = valeur;
    }

	public EpmTUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(final EpmTUtilisateur valeur) {
		this.utilisateur = valeur;
	}

    

}
