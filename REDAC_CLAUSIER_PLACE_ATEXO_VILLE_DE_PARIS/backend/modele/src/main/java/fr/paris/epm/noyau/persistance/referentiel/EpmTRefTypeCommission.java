package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeCommission de la table "epm__t_ref_type_commission"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeCommission extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_CIM = 1; // Identifiant en base pour les CIM.
    public static final int ID_CAO = 2; // Identifiant en base pour les CAO.
    public static final int ID_JURY = 3; // Identifiant en base pour les JURY.
    public static final int ID_SAPIN = 4; // Identifiant en base pour les SAPIN.
    public static final int ID_AUTRE = 5; // Identifiant en base pour les AUTRES.

    /**
     * Indique si le type de commission est transverse (true => pas de direction/service)
     */
    private boolean transverse;

    /**
     * Indique si le type de commission sera affiché dans l'écran de délibération.
     */
    private boolean afficherTableauDeliberation;

    /**
     * Référence de l'emplacement des documents modéles à rechercher pour
     * l'export de l'ordre du jour.
     */
    private EpmTRefDocumentEmplacement referenceEmplacementExportODJ;

    /**
     * Référence de l'emplacement des documents modéles à rechercher pour
     * l'export des convocations.
     */
    private EpmTRefDocumentEmplacement referenceEmplacementExportConvocation;

    /**
     * Indique si le type de commission peut afficher sur l'écran de la création de avenant
     */
    private boolean avenantActive;

    public boolean isTransverse() {
        return transverse;
    }

    public void setTransverse(boolean transverse) {
        this.transverse = transverse;
    }

    public boolean isAfficherTableauDeliberation() {
        return afficherTableauDeliberation;
    }

    public void setAfficherTableauDeliberation(boolean afficherTableauDeliberation) {
        this.afficherTableauDeliberation = afficherTableauDeliberation;
    }

    public EpmTRefDocumentEmplacement getReferenceEmplacementExportODJ() {
        return referenceEmplacementExportODJ;
    }

    public void setReferenceEmplacementExportODJ(EpmTRefDocumentEmplacement referenceEmplacementExportODJ) {
        this.referenceEmplacementExportODJ = referenceEmplacementExportODJ;
    }

    public EpmTRefDocumentEmplacement getReferenceEmplacementExportConvocation() {
        return referenceEmplacementExportConvocation;
    }

    public void setReferenceEmplacementExportConvocation(EpmTRefDocumentEmplacement referenceEmplacementExportConvocation) {
        this.referenceEmplacementExportConvocation = referenceEmplacementExportConvocation;
    }

    public boolean isAvenantActive() {
        return avenantActive;
    }

    public void setAvenantActive(boolean avenantActive) {
        this.avenantActive = avenantActive;
    }

}