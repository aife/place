package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "epm__v_clause", schema = "redaction")
public class EpmVClause extends EpmTClauseAbstract {

    @Id
    private int id;

    @ManyToOne(targetEntity = EpmTClause.class)
    @JoinColumn(name = "id_clause", referencedColumnName = "id")
    private EpmTClause epmTClause;

    @ManyToOne(targetEntity = EpmTClausePub.class)
    @JoinColumns({
            @JoinColumn(name = "id_clause_publication", referencedColumnName = "id_clause"),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
    })
    private EpmTClausePub epmTClausePub;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EpmTClause getEpmTClause() {
        return epmTClause;
    }

    public void setEpmTClause(EpmTClause epmTClause) {
        this.epmTClause = epmTClause;
    }

    public EpmTClausePub getEpmTClausePub() {
        return epmTClausePub;
    }

    public void setEpmTClausePub(EpmTClausePub epmTClausePub) {
        this.epmTClausePub = epmTClausePub;
    }

    @Override
    public Integer getIdClause() {
        return epmTClausePub != null ? epmTClausePub.getIdClause() : epmTClause.getIdClause();
    }

    @Override
    public Integer getIdPublication() {
        return epmTClausePub != null ? epmTClausePub.getIdPublication() : null;
    }

    @Override
    public Integer getIdLastPublication() {
        return epmTClausePub != null ? epmTClausePub.getIdPublication() : epmTClause.getIdLastPublication();
    }

    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        return epmTClausePub != null ? epmTClausePub.getEpmTRefTypeContrats() : epmTClause.getEpmTRefTypeContrats();
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTRefTypeContrats' est non-modifiable");
    }

    @Override
    public Set<? extends EpmTRoleClauseAbstract> getEpmTRoleClauses() {
        return epmTClausePub != null ? epmTClausePub.getEpmTRoleClauses() : epmTClause.getEpmTRoleClauses();
    }

    @Override
    public void setEpmTRoleClauses(Set<? extends EpmTRoleClauseAbstract> epmTRoleClauses) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTRoleClauses' est non-modifiable");
    }

    @Override
    public Set<? extends EpmTClausePotentiellementConditionneeAbstract> getEpmTClausePotentiellementConditionnees() {
        return epmTClausePub != null ? epmTClausePub.getEpmTClausePotentiellementConditionnees() : epmTClause.getEpmTClausePotentiellementConditionnees();
    }

    @Override
    public void setEpmTClausePotentiellementConditionnees(Set<? extends EpmTClausePotentiellementConditionneeAbstract> epmTClausePotentiellementConditionnees) {
        throw new UnsupportedOperationException("C'est une VIEW, 'EpmTClausePotentiellementConditionnees' est non-modifiable");
    }

    @Override
    public EpmVClause clone() throws CloneNotSupportedException {
        EpmVClause epmVClause = (EpmVClause) super.clone();
        if (epmTClause != null)
            epmVClause.epmTClause = epmTClause.clone();
        if (epmTClausePub != null)
            epmVClause.epmTClausePub = epmTClausePub.clone();
        return epmVClause;
    }

}
