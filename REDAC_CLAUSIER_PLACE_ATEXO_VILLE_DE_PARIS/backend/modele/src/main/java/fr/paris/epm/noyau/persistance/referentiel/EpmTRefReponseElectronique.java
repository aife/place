package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefReponseElectronique de la table "epm__t_ref_reponse_electronique"
 * Created by nty on 31/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefReponseElectronique extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int REFUSEE = 1; // Réponse par voie électronique n'est pas autorisée
    public static final int AUTORISEE = 2; // Réponse par voie électronique est autorisée
    public static final int OBLIGATOIRE = 3; // Réponse par voie électronique est obligatoire

    public static final String CODE_REFUSEE = "REFUSEE"; // Marquer le code externe quand Réponse par voie électronique n'est pas autorisée
    public static final String CODE_AUTORISEE = "AUTORISEE"; // Marquer le code externe quand Réponse par voie électronique est autorisée
    public static final String CODE_OBLIGATOIRE = "OBLIGATOIRE"; // Marquer le code externe Réponse par voie électronique est obligatoire

}