package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefPouvoirAdjudicateur;

/**
 * Pojo hibernate opération unité fonctionnelle
 * 
 * @author Rebeca Dantas
 * 
 */
public class EpmTOperationUniteFonctionnelle extends EpmTAbstractObject {

	/**
	 * Marqueur de sérialisation
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identifiant de l'unite fontionnelle
	 */
	private Integer id;

	/**
	 * Pouvoir adjuciateur
	 */
	private EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur;

	/**
	 * Direction service bénéficiaire
	 */
	private EpmTRefDirectionService epmTRefDirectionServiceBeneficiaire;

	/**
	 * Direction service responsable
	 */
	private EpmTRefDirectionService epmTRefDirectionServiceResponsable;

	/**
	 * Responsable
	 */
	private EpmTUtilisateur epmTUtilisateurResponsable;

	/**
	 * Nature des prestations
	 */
	private EpmTRefNature epmTRefNature;

	/**
	 * Type : 1 - Opération travaux; 2 - Unité fonctionnelle
	 */
	private Integer type;

	/**
	 * Code
	 */
	private String code;

	/**
	 * Libelle
	 */
	private String libelle;

	/**
	 * Le montant initial de l'operation fonctionnelle
	 */
	private Double estimatifInitialHT;

	/**
	 * Type de procédure applicable
	 */
	private String procedureApplicable;

	/**
	 * Année de début de l'exercice de l'opération fonctionnelle
	 */
	private Integer anneeDebut;

	/**
	 * Année de fin de l'exercice de l'opération fonctionnelle
	 */
	private Integer anneeFin;

    /**
     * Commentaire
     */
    private String commentaire;

    /**
     * Status : true - actif ; false - inactif
     */
    private boolean status;

	/**
	 * 
	 * @return l'identifiant de l'operation fonctionnelle
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param valeur l'identifiant de l'operation fonctionnelle
	 */
	public void setId(final Integer valeur) {
		this.id = valeur;
	}

	/**
	 * 
	 * @return le pouvoir adjudicateur
	 */
	public EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur() {
		return epmTRefPouvoirAdjudicateur;
	}

	/**
	 * 
	 * @param valeur le pouvoir adjudicateur
	 */
	public void setEpmTRefPouvoirAdjudicateur(final EpmTRefPouvoirAdjudicateur valeur) {
		this.epmTRefPouvoirAdjudicateur = valeur;
	}

	/**
	 *
	 * @return la valeur de la direction service bénéficiaire
	 */
	public EpmTRefDirectionService getEpmTRefDirectionServiceBeneficiaire() {
		return epmTRefDirectionServiceBeneficiaire;
	}

	/**
	 *
	 * @param valeur la direction service bénéficiaire
	 */
	public void setEpmTRefDirectionServiceBeneficiaire(final EpmTRefDirectionService valeur) {
		this.epmTRefDirectionServiceBeneficiaire = valeur;
	}

	/**
	 *
	 * @return la valeur de la direction service responsable
	 */
	public EpmTRefDirectionService getEpmTRefDirectionServiceResponsable() {
		return epmTRefDirectionServiceResponsable;
	}

	/**
	 *
	 * @param valeur la direction service responsable
	 */
	public void setEpmTRefDirectionServiceResponsable(final EpmTRefDirectionService valeur) {
		this.epmTRefDirectionServiceResponsable = valeur;
	}

	/**
	 *
	 * @return la valeur du responsable
	 */
	public EpmTUtilisateur getEpmTUtilisateurResponsable() {
		return epmTUtilisateurResponsable;
	}

	/**
	 *
	 * @param valeur le responsable
	 */
	public void setEpmTUtilisateurResponsable(final EpmTUtilisateur valeur) {
		this.epmTUtilisateurResponsable = valeur;
	}

	/**
	 *
	 * @return nature de prestations
	 */
	public EpmTRefNature getEpmTRefNature() {
		return epmTRefNature;
	}

	/**
	 *
	 * @param valeur nature de prestations
	 */
	public void setEpmTRefNature(final EpmTRefNature valeur) {
		this.epmTRefNature = valeur;
	}

	/**
	 *
	 * @return le type : 1 - Opération travaux; 2 - Unité fonctionnelle
	 */
	public Integer getType() {
		return type;
	}

	/**
	 *
	 * @param valeur le type : 1 - Opération travaux; 2 - Unité fonctionnelle
	 */
	public void setType(Integer valeur) {
		this.type = valeur;
	}

	/**
	 * 
	 * @return le code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param valeur code
	 */
	public void setCode(String valeur) {
		this.code = valeur;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param valeur libelle
	 */
	public void setLibelle(final String valeur) {
		this.libelle = valeur;
	}

	/**
	 * 
	 * @return l'estimation initial de l'unite fontionnelle
	 */
	public Double getEstimatifInitialHT() {
		return estimatifInitialHT;
	}

	/**
	 * 
	 * @param valeur l'estimation initial de l'unite fontionnelle
	 */
	public void setEstimatifInitialHT(Double valeur) {
		this.estimatifInitialHT = valeur;
	}

	/**
	 *
	 * @return le type de procédure applicable
	 */
	public String getProcedureApplicable() {
		return procedureApplicable;
	}

	/**
	 *
	 * @param valeur le type de procédure applicable
	 */
	public void setProcedureApplicable(final String valeur) {
		this.procedureApplicable = valeur;
	}

	/**
	 * 
	 * @return l'année de début de l'exercice de l'opération fonctionnelle
	 */
	public Integer getAnneeDebut() {
		return anneeDebut;
	}

	/**
	 * 
	 * @param valeur l'année de début de l'exercice de l'opération fonctionnelle
	 */
	public void setAnneeDebut(final Integer valeur) {
		this.anneeDebut = valeur;
	}

	/**
	 * 
	 * @return l'année de fin de l'exercice de l'opération fonctionnelle
	 */
	public Integer getAnneeFin() {
		return anneeFin;
	}

	/**
	 * 
	 * @param valeur l'année de fin de l'exercice de l'opération fonctionnelle
	 */
	public void setAnneeFin(final Integer valeur) {
		this.anneeFin = valeur;
	}

    /**
     *
     * @return commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param valeur commentaire
     */
    public void setCommentaire(String valeur) {
        this.commentaire = valeur;
    }

    /**
     *
     * @return status : true - actif ; false - inactif
     */
    public boolean isStatus() {
        return status;
    }

    /**
     *
     * @param valeur status : true - actif ; false - inactif
     */
    public void setStatus(boolean valeur) {
        this.status = valeur;
    }

}
