package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Class POJO EpmTChapitre -> epm__t_chapitre
 * Created by nty on 05/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Entity
@Table(name = "epm__t_chapitre", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTChapitre extends EpmTChapitreAbstract {

    /**
     * Clé primaire et id_clause en meme temps
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_canevas", nullable = true)
    private EpmTCanevas epmTCanevas;

    @ManyToOne
    @JoinColumn(name = "id_parent_chapitre", nullable = true)
    private EpmTChapitre epmTParentChapitre;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(schema = "redaction", name = "epm__t_chapitre_has_clause",
            joinColumns = @JoinColumn(name = "id_chapitre"))
    @MapKeyColumn(name = "num_order")
    @Column(name = "reference_clause")
    private Map<Integer, String> clauses;

    @OneToMany(targetEntity = EpmTChapitre.class,
            mappedBy = "epmTParentChapitre", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<EpmTChapitre> epmTSousChapitres;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdChapitre() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    @Override
    public EpmTCanevas getEpmTCanevas() {
        return epmTCanevas;
    }

    @Override
    public void setEpmTCanevas(EpmTCanevasAbstract epmTCanevas) {
        this.epmTCanevas = (EpmTCanevas) epmTCanevas;
    }

    @Override
    public EpmTChapitre getEpmTParentChapitre() {
        return epmTParentChapitre;
    }

    @Override
    public void setEpmTParentChapitre(EpmTChapitreAbstract epmTParentChapitre) {
        this.epmTParentChapitre = (EpmTChapitre) epmTParentChapitre;
    }

    @Override
    public Map<Integer, String> getClauses() {
        return clauses;
    }

    @Override
    public void setClauses(Map<Integer, String> clauses) {
        this.clauses = clauses;
    }

    @Override
    public List<EpmTChapitre> getEpmTSousChapitres() {
        return epmTSousChapitres;
    }

    @Override
    public void setEpmTSousChapitres(List<? extends EpmTChapitreAbstract> epmTSousChapitres) {
        this.epmTSousChapitres = (List<EpmTChapitre>) epmTSousChapitres;
    }

    @Override
    public EpmTChapitre clone() throws CloneNotSupportedException {
        EpmTChapitre epmTChapitre = (EpmTChapitre) super.clone();
        epmTChapitre.id = 0;
        return epmTChapitre;
    }

}
