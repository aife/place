package fr.paris.epm.noyau.persistance;


/**
 * POJO hibernate gérant les pièces jointes.
 * @author Mounthei Kham
 * @version $Revision:$, $Date: $, $Author: $
 */
public class EpmTPieceJointe extends EpmTAbstractDocument {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}
