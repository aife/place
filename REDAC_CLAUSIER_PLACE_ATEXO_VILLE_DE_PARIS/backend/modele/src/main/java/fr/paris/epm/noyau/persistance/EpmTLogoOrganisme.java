package fr.paris.epm.noyau.persistance;

import java.io.Serializable;

/**
 * Pojo hibernate pour la gestion de logo d'organisme
 */
public class EpmTLogoOrganisme implements Serializable {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Cet attribut mappe la valeur de la clé primaire.
     */
    private int id;

    /**
     * Le fichier logo
     */
    private byte[] fichierLogo;

    /**
     * L'identifiant de l'organisme auquel appartient le logo. Il est null pour la plateforme de
     * mono-organisme
     */
    private Integer idOrganisme;

    /**
     * Le type du fichier image logo : .gif, .jpeg, .jpg ou .png
     */
    private String typeFichierLogo;
    
    /**
     * Pour vérifier si le fichier logo a été changé
     */
    private String md5Checksum;

    public int getId() {
        return id;
    }

    public void setId(int valeur) {
        this.id = valeur;
    }

    public byte[] getFichierLogo() {
        return fichierLogo;
    }

    public void setFichierLogo(byte[] valeur) {
        this.fichierLogo = valeur;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer valeur) {
        this.idOrganisme = valeur;
    }

    public String getTypeFichierLogo() {
        return typeFichierLogo;
    }

    public void setTypeFichierLogo(String valeur) {
        this.typeFichierLogo = valeur;
    }

    public String getMd5Checksum() {
        return md5Checksum;
    }

    public void setMd5Checksum(String valeur) {
        this.md5Checksum = valeur;
    }

}
