package fr.paris.epm.noyau.metier;

import java.io.Serializable;

/**
 * Classe critère utilisé pour la recherche des info-bulle.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class InfoBulleCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Référence de l'info-bulle.
     */
    private String libelle;

    private boolean libelleStrict = true;

    private Boolean actif;

    @Override
    public final StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        sb.append("from EpmTRefInfoBulle infoBulle");
        if (libelle != null && !libelle.trim().equals("")) {
            sb.append(debut ? " where " : " and ");

            if (libelleStrict) {
                sb.append("lower(infoBulle.libelle) = lower(:libelle)");
                getParametres().put("libelle", libelle.trim());
            } else {
                sb.append("lower(infoBulle.libelle) like lower(:libelle)");
                getParametres().put("libelle", pourcentTrim(libelle));
            }
            debut = false;
        }

        if (actif != null) {
            sb.append(debut ? " where " : " and ");
            sb.append("actif = :actif");
            getParametres().put("actif", actif);
            debut = false;
        }
        return sb;
    }

    @Override
    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null)
            sb.append(" order by infoBulle.").append(proprieteTriee).append(triCroissant ? " ASC " : " DESC ");
        return sb.toString();
    }

    @Override
    public String toCountHQL() {
        return "select count(*) " + corpsRequete();
    }

    public final void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setLibelleStrict(boolean libelleStrict) {
        this.libelleStrict = libelleStrict;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

}