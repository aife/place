package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDocumentDestinataire de la table "epm__t_ref_document_destinataire"
 * Paramétrage des critéres de recherche des entreprises à afficher dans la
 * liste des bénéficiaires du document dans le module passation. fonction des
 * différents statut d'ouvertures et d'attribution
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDocumentDestinataire extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Suite à donner à l'offreLc (admis ou non à poursuivre)
     */
    private EpmTRefSuiteADonner suiteADonnerOffreLc;

    /**
     * Statut de l'ouverture de l'enveloppe candidature
     */
    private EpmTRefStatutEnveloppe statutEnveloppeCandidature;
    
    /**
     * Statut de l'ouverture de l'enveloppe offreLc
     */
    private EpmTRefStatutEnveloppe statutEnveloppeOffreLc;

    /**
     * Complétude de l'enveloppe candidature lc
     */
    private EpmTRefCompletudeEnveloppe completudeEnveloppeCandidatureLc;

    /**
     * Admissibilité d'une candidature lc.
     */
    private EpmTRefAdmissibilite admissibilite;

    /**
     * Confirmation ou non de l'attribution à une offre.
     */
    private EpmTRefActionConfirmationAttribution confirmationAttribution;
    
    /**
     * True si l'on doit rechercher toute les entreprises ayant effectué un dépot.
     */
    private Boolean entrepriseDepot;

    /**
     * True si l'on doit rechercher toute les entreprises ayant effectué un retrait.
     */
    private Boolean entrepriseRetrait;
    
    /**
     * True si l'on doit appliquer les critéres de recherche à la consultation initiale.
     */
    private Boolean consultationInitiale;
    
    /**
     * Defini si le concerne à un champ de fusion du module execution
     */
    private boolean moduleExecution;

    public EpmTRefSuiteADonner getSuiteADonnerOffreLc() {
        return suiteADonnerOffreLc;
    }

    public void setSuiteADonnerOffreLc(EpmTRefSuiteADonner suiteADonnerOffreLc) {
        this.suiteADonnerOffreLc = suiteADonnerOffreLc;
    }

    public EpmTRefStatutEnveloppe getStatutEnveloppeCandidature() {
        return statutEnveloppeCandidature;
    }

    public void setStatutEnveloppeCandidature(EpmTRefStatutEnveloppe statutEnveloppeCandidature) {
        this.statutEnveloppeCandidature = statutEnveloppeCandidature;
    }

    public EpmTRefStatutEnveloppe getStatutEnveloppeOffreLc() {
        return statutEnveloppeOffreLc;
    }

    public void setStatutEnveloppeOffreLc(EpmTRefStatutEnveloppe statutEnveloppeOffreLc) {
        this.statutEnveloppeOffreLc = statutEnveloppeOffreLc;
    }

    public EpmTRefCompletudeEnveloppe getCompletudeEnveloppeCandidatureLc() {
        return completudeEnveloppeCandidatureLc;
    }

    public void setCompletudeEnveloppeCandidatureLc(EpmTRefCompletudeEnveloppe completudeEnveloppeCandidatureLc) {
        this.completudeEnveloppeCandidatureLc = completudeEnveloppeCandidatureLc;
    }

    public EpmTRefAdmissibilite getAdmissibilite() {
        return admissibilite;
    }

    public void setAdmissibilite(EpmTRefAdmissibilite admissibilite) {
        this.admissibilite = admissibilite;
    }

    public EpmTRefActionConfirmationAttribution getConfirmationAttribution() {
        return confirmationAttribution;
    }

    public void setConfirmationAttribution(EpmTRefActionConfirmationAttribution confirmationAttribution) {
        this.confirmationAttribution = confirmationAttribution;
    }

    public Boolean getEntrepriseDepot() {
        return entrepriseDepot;
    }

    public void setEntrepriseDepot(Boolean entrepriseDepot) {
        this.entrepriseDepot = entrepriseDepot;
    }

    public Boolean getEntrepriseRetrait() {
        return entrepriseRetrait;
    }

    public void setEntrepriseRetrait(Boolean entrepriseRetrait) {
        this.entrepriseRetrait = entrepriseRetrait;
    }

    public Boolean getConsultationInitiale() {
        return consultationInitiale;
    }

    public void setConsultationInitiale(Boolean consultationInitiale) {
        this.consultationInitiale = consultationInitiale;
    }

    public boolean isModuleExecution() {
        return moduleExecution;
    }

    public void setModuleExecution(boolean moduleExecution) {
        this.moduleExecution = moduleExecution;
    }

}