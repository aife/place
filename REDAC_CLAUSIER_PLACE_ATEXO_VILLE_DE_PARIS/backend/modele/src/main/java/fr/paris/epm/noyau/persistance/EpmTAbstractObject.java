package fr.paris.epm.noyau.persistance;

public abstract class EpmTAbstractObject implements EpmTObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return this.getClass().toString();
    }

}
