package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefMatinAprem de la table "epm__t_ref_matin_aprem"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefMatinAprem extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

}