/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * @author MGA
 */
public class DomainePaireType implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * La clef de la paire
     */
    private String clef;
    /**
     * La valeur de la paire
     */
    private String valeur;

    /**
     * @return the clef
     */
    public final String getClef() {
        return clef;
    }

    /**
     * @param clef the clef to set
     */
    public final void setClef(final String valeur) {
        this.clef = valeur;
    }

    /**
     * @return the valeur
     */
    public final String getValeur() {
        return valeur;
    }

    /**
     * @param valeur the valeur to set
     */
    public final void setValeur(final String valeur) {
        this.valeur = valeur;
    }

}
