package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Surepclass EpmTChapitreAbstract des POJO EpmTChapitre et EpmTChapitrePub
 * Created by nty on 05/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@MappedSuperclass
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class EpmTChapitreAbstract extends EpmTAbstractObject implements Serializable, Cloneable {

    @Column(name = "titre", nullable = false)
    private String titre;

    @Column(name = "numero", nullable = false)
    private String numero;

    /**
     * texte contenu dans l'info-bull .
     */
    @Column(name = "info_bulle_text")
    private String infoBulleText;

    /**
     * Attribut Lien .
     */
    @Column(name = "info_bulle_url")
    private String infoBulleUrl;

    @Column(name = "style")
    private String style;

    @Column(name = "derogation_article")
    private String derogationArticle;

    @Column(name = "derogation_commentaires")
    private String derogationCommentaires;

    @Column(name = "derogation_active")
    private Boolean derogationActive;

    @Column(name = "afficher_derogation")
    private Boolean afficherMessageDerogation;

    public abstract Integer getIdChapitre();

    public abstract Integer getIdPublication();

    public abstract EpmTCanevasAbstract getEpmTCanevas() ;

    public abstract void setEpmTCanevas(EpmTCanevasAbstract epmTCanevas) ;

    public abstract EpmTChapitreAbstract getEpmTParentChapitre() ;

    public abstract void setEpmTParentChapitre(EpmTChapitreAbstract epmTParentChapitre) ;

    public abstract Map<Integer, String> getClauses() ;

    public abstract void setClauses(Map<Integer, String> clauses) ;

    public abstract List<? extends EpmTChapitreAbstract> getEpmTSousChapitres() ;

    public abstract void setEpmTSousChapitres(List<? extends EpmTChapitreAbstract> epmTSousChapitres) ;

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getInfoBulleText() {
        return infoBulleText;
    }

    public void setInfoBulleText(String infoBulleText) {
        this.infoBulleText = infoBulleText;
    }

    public String getInfoBulleUrl() {
        return infoBulleUrl;
    }

    public void setInfoBulleUrl(String infoBulleUrl) {
        this.infoBulleUrl = infoBulleUrl;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDerogationArticle() {
        return derogationArticle;
    }

    public void setDerogationArticle(String derogationArticle) {
        this.derogationArticle = derogationArticle;
    }

    public String getDerogationCommentaires() {
        return derogationCommentaires;
    }

    public void setDerogationCommentaires(String derogationCommentaires) {
        this.derogationCommentaires = derogationCommentaires;
    }

    public Boolean getDerogationActive() {
        return derogationActive;
    }

    public void setDerogationActive(Boolean derogationActive) {
        this.derogationActive = derogationActive;
    }

    public Boolean getAfficherMessageDerogation() {
        return afficherMessageDerogation;
    }

    public void setAfficherMessageDerogation(Boolean afficherMessageDerogation) {
        this.afficherMessageDerogation = afficherMessageDerogation;
    }

    @Override
    public EpmTChapitreAbstract clone() throws CloneNotSupportedException {
        EpmTChapitreAbstract epmTChapitre = (EpmTChapitreAbstract) super.clone();

        epmTChapitre.setEpmTCanevas(null);
        epmTChapitre.setEpmTParentChapitre(null);

        if (getEpmTSousChapitres() != null) {
            List<EpmTChapitreAbstract> listSousChapitres = new ArrayList<>();
            for (EpmTChapitreAbstract sousChapitre : getEpmTSousChapitres()) {
                EpmTChapitreAbstract epmTSousChapitre = sousChapitre.clone();
                epmTSousChapitre.setEpmTParentChapitre(epmTChapitre);
                listSousChapitres.add(epmTSousChapitre.clone());
            }
            epmTChapitre.setEpmTSousChapitres(listSousChapitres);
        }
        return epmTChapitre;
    }

}
