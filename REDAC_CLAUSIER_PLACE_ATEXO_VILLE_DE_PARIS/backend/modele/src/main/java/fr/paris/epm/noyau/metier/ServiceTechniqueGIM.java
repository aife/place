/**
 * $Id$
 */
package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;

public interface ServiceTechniqueGIM extends BaseGIM {


    UtilisateurExterneConsultation getUtilisateurExterneConsultation(final String identifiantExterne);


}
