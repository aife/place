package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefBonQuantite de la table "epm__t_ref_bon_quantite"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefBonQuantite extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_BON_COMMANDE = 1;  // Identifiant du bon de commande.
    public static final int ID_AUTRES_PRIX_UNITAIRES = 2;

}