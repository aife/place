package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;


/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.propositionClassementOffre.critereCandidat.critereCandidatLot.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCritereCandidatLot implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private String numeroLot;

    private String intituleLot;

    /**
     * type de critère en cas de allotie
     */
    private String typeCritere;

    /**
     * Liste de nom criteres, notes (par candidat).
     */
    private List<DomaineCritereCandidat> listeCriteresCandidats;

    private List<DomaineCritereCandidat> listeCriteresCandidatsNonRetenu;

    /**
     * Liste des critères de tous les candidats.
     */
    private List<DomaineCritereCandidat> tableauAnalyseOffres;

    public List<DomaineCritereCandidat> getTableauAnalyseOffres() {
        return tableauAnalyseOffres;
    }

    public void setTableauAnalyseOffres(List<DomaineCritereCandidat> tableauAnalyseOffres) {
        this.tableauAnalyseOffres = tableauAnalyseOffres;
    }

    public final String getNumeroLot() {
        return numeroLot;
    }
    
    public final void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }
    /**
     * @return the intituleLot
     */
    public final String getIntituleLot() {
        return intituleLot;
    }

    /**
     * @param intituleLot the intituleLot to set
     */
    public final void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }

    /**
     * @return Liste de nom criteres, notes (par candidat).
     */
    public final List<DomaineCritereCandidat> getListeCriteresCandidats() {
        return listeCriteresCandidats;
    }
    /**
     * @param valeur Liste de nom criteres, notes (par candidat).
     */
    public final void setListeCriteresCandidats(final
            List<DomaineCritereCandidat> valeur) {
        this.listeCriteresCandidats = valeur;
    }

    public List<DomaineCritereCandidat> getListeCriteresCandidatsNonRetenu() {
        return listeCriteresCandidatsNonRetenu;
    }

    public void setListeCriteresCandidatsNonRetenu(List<DomaineCritereCandidat> valeur) {
        this.listeCriteresCandidatsNonRetenu = valeur;
    }

    public String getTypeCritere() {
        return typeCritere;
    }

    public void setTypeCritere(String valeur) {
        this.typeCritere = valeur;
    }

    public boolean getPonderationCritere() {
        return typeCritere != null && typeCritere.equals("ponderation");
    }

    public boolean getPointCritere() {
        return typeCritere != null && typeCritere.equals("point");
    }
}
