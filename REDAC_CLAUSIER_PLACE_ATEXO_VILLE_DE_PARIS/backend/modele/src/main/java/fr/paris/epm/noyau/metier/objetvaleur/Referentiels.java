package fr.paris.epm.noyau.metier.objetvaleur;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.*;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Objet stockant tous les référentiels de la base.
 * @author guillaume Béraudo
 * @version $Revision: 157 $, $Date: 2007-07-31 14:26:48 +0200 (mar., 31 juil. 2007) $, $Author: beraudo $
 */
public class Referentiels implements Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private static final String TOUS = "Tous";
    private static final String TOUTES = "Toutes";

    // Propriétés
    /**
     * Référentiel Pouvoir Adjudicateur.
     */
    private Collection<EpmTRefPouvoirAdjudicateur> refPouvoirAdjudicateurs;

    /**
     * Référentiel Pouvoir Adjudicateur.
     */
    private Collection<EpmTRefPouvoirAdjudicateur> refPouvoirAdjudicateursCommission;

    /**
     * Référentiel Procédure accessible en lecture (ie moteur de recherche mais pas de nouvelle consultation).
     */
    private Collection<EpmTRefProcedure> refProcedureLecture;

    /**
     * Référentiel Procédure accessible en ecriture (ie nouvelle consultation).
     */
    private Collection<EpmTRefProcedure> refProcedureEcriture;

    /**
     * Référentiel Article.
     */
    private Collection<EpmTRefArticle> refArticles;

    /**
     * Référentiel Nature.
     */
    private Collection<EpmTRefNature> refNatures;

    /**
     * Référentiel Nature.
     */
    private Collection<EpmTRefAutoriteDelegante> refAutoriteDelegante;

    /**
     * Référentiel type de DSP.
     */
    private Collection<EpmTRefTypeDsp> refTypeDsp;

    /**
     * Référentiel Arrondissement.
     */
    private Collection<EpmTRefArrondissement> refArrondissement;
    /**
     * Référentiel Nature.
     */
    private Collection<EpmTRefTypeConsultation> refTypeConsultation;

    /**
     * Référentiel Direction Service accessible en lecture (ie moteur de recherche mais pas de nouvelle consultation).
     */
    private Collection<EpmTRefDirectionService> refDirectionServices;

    /**
     * Référentiel Direction Service accessible en ecriture (ie nouvelle consultation).
     */
    private Collection<EpmTRefDirectionService> refDirectionServicesActives;

    /**
     * Référentiel CCAG.
     */
    private Collection<EpmTRefCcag> refCcags;

	/**
	 * Signature électronique
	 */
	private Collection<EpmTRefSignatureElectronique> refSignatureElectronique;

    /**
     * Référentiel refTypeMarches.
     */
    private Collection<EpmTRefTypeMarche> refTypeMarches;

    /**
     * Référentiel refInfoBulleMap.
     */
    private Collection<EpmTRefInfoBulle> refInfoBulle;


    /**
     * Referentiel de documents utilises.
     */
    private List<EpmTRefDocumentUtilise> refDocumentUtilise;

    /**
     * Référentiels des fiches pratiques.
     */
    private Collection<EpmTRefFichePratique> refFichePratique;

    /**
     * Liste des étapes d'un calendrier.
     */
    private Collection<EpmTRefEtapeCalendrier> refModeleEtapesCalendrier;

    /**
     * Liste des beneficiare du marche.
     */
    private Collection<EpmTRefBeneficiaireMarche> refBeneficiaireMarcheList;

    /**
     * Liste des unités pour les formes de prix unitaires.
     */
    private Collection<EpmTRefUnite> refUnite;

    /**
     * Liste des critéres d'attribution pour le formulaire amont d'une consultation.
     */
    private Collection<EpmTRefCritereAttribution> refCritereAttribution;

    /**
     * Ensemble des parametrages propres a la version cliente de l'application.
     */
    private Collection<EpmTRefParametrage> refParametrage;

    /**
     * Liste des types d'événement du calendrier.
     */
    private Collection<EpmTRefTypeEvenementCalendrier> typesEvenementsCalendrier;

    /**
     * Liste des types d'événement du calendrier.
     */
    private Collection<EpmTRefEvenementCalendrier> evenementsCalendrier;


    /**
     * Map des direction/service {@link DirectionService}.
     */
    private Map<Integer, DirectionService> mapDirService;

    /**
     * Referentiel Code Achats (note d'opportunité)
     */
    private List<EpmTRefCodeAchat> refCodeAchat;

    /**
     * Rferentiel d'imputations budgetaires
     */
    private List<EpmTRefImputationBudgetaire> refImputationBudgetaire;

    /**
     * Referentiel des Champs Additionnels
     */
    private List<EpmTRefChampAdditionnel> refChampAdditionnel;

    /**
     * Referentiel de Nomenclature
     */
    private List<EpmTRefNomenclature> refNomenclature;

    /**
     * Referentiel type contrat
     */
    private List<EpmTRefTypeContratAvenant> refTypeContratAvenant;

    /**
     * Referentiel type objet contrat
     */
    private List<EpmTRefTypeObjetContrat> refTypeObjetContrat;


    private List<EpmTRefFormeJuridique> refFormesJuridique;

    private List<EpmTRefOrganisme> refOrganisme;

    /**
     * Referentiel catégorie du document contrat
     */
    private List<EpmTRefCategorieDocumentContrat> refCategorieDocumentContrat;

    /**
     * Referentiel type du document contrat
     */
    private List<EpmTRefTypeDocumentContrat> refTypeDocumentContrat;

    /**
     * Referentiel Moyen de Notification pour l'attributaire du contrat
     */
    private List<EpmTRefStatutNotificationContrat> refStatutNotificationContrat;

    /**
     * Referentiel pour le type d'établissement
     */
    private List<EpmTRefTypeEtablissement> refTypeEtablissement;

    /**
     * Referentiel pour le type d'attributaire (utilisé pour l'attribution du contrat)
     */
    private List<EpmTRefTypeAttributaire> refTypeAttributaire;

    /**
     * Referentiel pour le role juridique de l'établissement associé à un contrat
     */
    private List<EpmTRefRoleJuridique> refRoleJuridique;

    /**
     * Referentiel tranche budgetaire
     */
    private List<EpmTRefTrancheBudgetaire> refTrancheBudgetaire;

    /**
     * Referentiel type des avenants
     */
    private List<EpmTRefTypeAvenant> refTypeAvenant;

    /**
     * liste des options pour la réponse par voie électronique
     */
    private List<EpmTRefReponseElectronique> refReponseElectronique;

    /**
     * Liste des clauses sociales
     */
    private List<EpmTRefClausesSociales> refClausesSociales;

    private List<EpmTRefClausesEnvironnementales> refClausesEnvironnementales;

    private List<EpmTRefTypeStructureSociale> refStructureSocialeReserves;

    private List<EpmTRefDroitProprieteIntellectuelle> refDroitProprieteIntellectuelle;

    private List<EpmTRefLieuExecution> refLieuExecution;


    /**
     * Sélect Bon ou Quantité {@link EpmTRefBonQuantite}.
     */
    private Collection<EpmTRefBonQuantite> bonQuantites;

    /**
     * Sélect Choix de la Forme de Prix {@link EpmTRefChoixFormePrix}.
     */
    private Collection<EpmTRefChoixFormePrix> choixFormePrix;

    /**
     * Sélect Choix entre Jour et Mois {@link EpmTRefChoixMoisJour}.
     */
    private Collection<EpmTRefChoixMoisJour> choixMoisJours;

    /**
     * Sélect entre Durée, Délai et Description {@link EpmTRefDureeDelaiDescription}.
     */
    private Collection<EpmTRefDureeDelaiDescription> dureeDelaiDescriptions;

    /**
     * Sélect des groupements attributaires {@link EpmTRefGroupementAttributaire}.
     */
    private Collection<EpmTRefGroupementAttributaire> groupementAttributaires;

    /**
     * Sélect entre min et max {@link EpmTRefMinMax}.
     */
    private Collection<EpmTRefMinMax> minMax;

    /**
     * Sélect de la Nature des Tranches {@link EpmTRefNatureTranche}.
     */
    private Collection<EpmTRefNatureTranche> natureTranches;

    /**
     * Sélect du Nombre de Candidats Admis {@link EpmTRefNbCandidatsAdmis}.
     */
    private Collection<EpmTRefNbCandidatsAdmis> nbCandidatsAdmis;

    /**
     * Sélect de la Réservation par Lot {@link EpmTRefReservationLot}.
     */
    private Collection<EpmTRefReservationLot> reservationLots;

    /**
     * Sélect Tranches et Type de Prix {@link EpmTRefTrancheTypePrix}.
     */
    private Collection<EpmTRefTrancheTypePrix> trancheTypePrix;

    /**
     * Sélect Type de Prix {@link EpmTRefTypePrix}.
     */
    private Collection<EpmTRefTypePrix> typePrix;

    /**
     * Sélect Variaton {@link EpmTRefVariation}.
     */
    private Collection<EpmTRefVariation> variations;

    /**
     * Sélect Statut {@link EpmTRefStatut}.
     */
    private Collection<EpmTRefStatut> statuts;

    /**
     * Sélect Statut {@link EpmTRefStatutAvenant}.
     */
    private Collection<EpmTRefStatutAvenant> statutAvenants;

    /**
     * Sélect Vision {@link EpmTRefVision}.
     */
    private Collection<EpmTRefVision> visions;

    /**
     * Sélect modaliteRetrait {@link EpmTRefModaliteRetrait}.
     */
    private Collection<EpmTRefModaliteRetrait> modaliteDeRetrait;

    /**
     * Sélect modaliteRetrait {@link EpmTRefModaliteDepotPli}.
     */
    private Collection<EpmTRefModaliteDepotPli> modaliteDepotPli;

    /**
     * Sélect modaliteDepotElement {@link EpmTRefModaliteDepotElement}.
     */
    private Collection<EpmTRefModaliteDepotElement> modaliteDepotElement;

    /**
     * Select Suite à donner {@link EpmTRefSuiteADonner}.
     */
    private Collection<EpmTRefSuiteADonner> suiteADonner;

    private List<EpmTRefSuiteNegociation> suiteNegociations;

    /**
     * Sélect Moyen de Notifiaction {@link EpmTRefMoyenNotification}.
     */
    private List<EpmTRefMoyenNotification> moyenNotification;

    /**
     * Liste de base {@link EpmTRefAdmissibilite}.
     */
    private Collection<EpmTRefAdmissibilite> admissibilite;

    /**
     * Statut de l'entreprise {@link EpmTRefStatutEntreprise}.
     */
    private Collection<EpmTRefStatutEntreprise> statutEntreprise;

    /**
     * Liste de base {@link EpmTRefCompletudeEnveloppe}.
     */
    private Collection<EpmTRefCompletudeEnveloppe> completudeEnveloppe;

    /**
     * Liste de base {@link EpmTRefActionPossible}.
     */
    private Collection<EpmTRefActionPossible> actionsPossibles;

    /**
     * Statuts dees candidatures {@link EpmTRefStatutEnveloppe}.
     */
    private Collection<EpmTRefStatutEnveloppe> statutsCandidatures;

    /**
     * Types de candidatures {@link EpmTRefTypeEnveloppe}.
     */
    private Collection<EpmTRefTypeEnveloppe> typesCandidatures;


    /**
     * Ensemble des civilités.
     */
    private Collection<EpmTRefCivilite> listeCivilites;

    /**
     * Ensemble des formes juridiques.
     */
    private Collection<EpmTRefFormeJuridique> listeFormeJuridique;

    /**
     * Select de l'action sur l'écran de confirmation d'attribution.
     */
    private Collection<EpmTRefActionConfirmationAttribution> actionConfirmationAttribution;

    /**
     * Select de l'action sue l'écran de confirmation d'attribution s'il y a un changement d'attributaire.
     */
    private Collection<EpmTRefActionConfirmationAttributionChgtAttributaire> actionConfirmationAttributionChgtAttributaire;

    /**
     * Select de l'action sur l'ajout d'un échange.
     */
    private Collection<EpmTRefFormatEchange> formatEchange;

    /**
     * Sélect {@link EpmTRefEtatAttestation}.
     */
    private Collection<EpmTRefEtatAttestation> etatsAttestations;

    // Début Commissions
    /**
     * Rôles des membres permanents {@link EpmTRefRole}.
     */
    private Collection<EpmTRefRole> roles;

    /**
     * Présences des membres à une réunion {@link EpmTRefPresence}.
     */
    private Collection<EpmTRefPresence> presences;


    /**
     * Matins, après-midi ou les deux {@link EpmTRefMatinAprem}.
     */
    private Collection<EpmTRefMatinAprem> matinsAprems;



    /**
     * Statut de l'offre financière.
     */
    private Collection<EpmTRefStatutOffreFinanciere> statutOffreFinanciere;

    /**
     * Ensemble des départements.
     */
    private Collection<EpmTRefDepartement> departements;


    /**
     * Types de budget {@link EpmTRefTypeBudget}.
     */
    private Collection<EpmTRefTypeBudget> typesBudget;


    /**
     * Referentiel de Document Emplacement
     */
    private List<EpmTRefDocumentEmplacement> refDocumentEmplacements;

    /**
     * Referentiel de Document Destinataire
     */
    private List<EpmTRefDocumentDestinataire> refDocumentDestinataires;

    /**
     * Referentiel des Domaines
     */
    private List<EpmTRefDomaineDonnees> refDomainesDonnees;

    /**
     * Referentiel des Champs Fusion Simples
     */
    private List<EpmTRefChampFusionSimple> refChampsFusionSimples;

    /**
     * Referentiel des Champs Fusion Complexes
     */
    private List<EpmTRefChampFusionComplexe> refChampsFusionComplexes;

    /**
     * Referentiel des Types de Document (doc, odt, pdf)
     */
    private List<EpmTRefDocumentType> refDocumentTypesDocument;

    /**
     * Referentiel des Types de Document Excel
     */
    private List<EpmTRefDocumentType> refDocumentTypesExcel;


    // Référentiels de REDACTION

    private List<EpmTRefActionDocument> refActionDocuments;
    private List<EpmTRefAuteur> refAuteurs;
    private List<EpmTRefExtensionFichier> refExtensionFichiers;
    private List<EpmTRefIntervention> refInterventions;
    private List<EpmTRefPotentiellementConditionnee> refPotentiellementConditionnees;
    private List<EpmTRefStatutRedactionClausier> refStatutRedactionClausiers;
    private List<EpmTRefThemeClause> refThemeClauses;
    private List<EpmTRefTypeClause> refTypeClauses;
    private List<EpmTRefTypeContrat> refTypeContrats;
    private List<EpmTRefTypeDocument> refTypeDocuments;
    private List<EpmTRefTypePageDeGarde> refTypePageDeGardes;
    private List<EpmTRefValeurTypeClause> refValeurTypeClauses;

    // Accesseurs

    /**
     * @return collection de CCAG {@link EpmTRefCcag}
     */
    public final Collection<EpmTRefCcag> getRefCcag() {
        return refCcags;
    }

	public void setRefSignatureElectronique( List<EpmTRefSignatureElectronique> refSignatureElectronique ) {
		this.refSignatureElectronique = refSignatureElectronique;
	}


    public final void setRefCcag(final Collection<EpmTRefCcag> valeur) {
        this.refCcags = valeur;
    }

	/**
	 * @return collection de CCAG {@link EpmTRefCcag}
	 */
	public final Collection<EpmTRefSignatureElectronique> getRefSignatureElectronique() {
		return refSignatureElectronique;
	}

    /**
     * @return collection de CCAG {@link EpmTRefCcag}
     */
    public final Collection<EpmTRefCcag> getRefCcagTous() {
        EpmTRefCcag refCcag = new EpmTRefCcag();
        refCcag.setId(0);
        refCcag.setLibelleCourt(TOUS);
        refCcag.setLibelle(TOUS);
        refCcag.setActif(true);
        return ajouterTous(refCcags, refCcag);
    }


    /**
     * @return collection de référentiels Article {@link EpmTRefArticle}
     */
    public final Collection<EpmTRefArticle> getRefArticle() {
        return refArticles;
    }

    /**
     * @return collection de référentiels Article {@link EpmTRefArticle}
     */
    public final Collection<EpmTRefArticle> getRefArticleTous() {
        EpmTRefArticle refArticle = new EpmTRefArticle();
        refArticle.setLibelle(TOUS);
        refArticle.setLibelleCourt(TOUS);
        refArticle.setId(0);
        refArticles.add(refArticle);
        return ajouterTous(refArticles, refArticle);
    }

    /**
     * @return collection de référentiels Pouvoir Adjudicateur affiché dans
     * commissson {@link EpmTRefPouvoirAdjudicateur}
     */
    public final Collection<EpmTRefPouvoirAdjudicateur> getRefPouvoirAdjudicateurCommission() {
        return refPouvoirAdjudicateursCommission;
    }

    /**
     * @return collection de référentiels Pouvoir Adjudicateur affiché dans
     * commissson {@link EpmTRefPouvoirAdjudicateur}
     */
    public final Collection<EpmTRefPouvoirAdjudicateur> getRefPouvoirAdjudicateur() {
        return refPouvoirAdjudicateurs;
    }

    /**
     * @param rpa Collection<Referentiel>de référentiels Pouvoir Adjudicateur
     *            {@link EpmTRefPouvoirAdjudicateur}
     */
    public final void setRefPouvoirAdjudicateur(final Collection<EpmTRefPouvoirAdjudicateur> rpa) {
        this.refPouvoirAdjudicateurs = rpa.stream().sorted((pa1, pa2) -> pa1.getId() <= pa2.getId() ? -1 : 1).collect(Collectors.toList());
    }

    /**
     * @return Collection<Referentiel>de référentiels Pouvoir Adjudicateur
     * {@link EpmTRefPouvoirAdjudicateur}
     */
    public final Collection<EpmTRefPouvoirAdjudicateur> getRefPouvoirAdjudicateurCommissionTous() {
        EpmTRefPouvoirAdjudicateur refPouvoirAdjudicateur;
        refPouvoirAdjudicateur = new EpmTRefPouvoirAdjudicateur();
        refPouvoirAdjudicateur.setId(0);
        refPouvoirAdjudicateur.setLibelle(TOUS);
        return ajouterTous(refPouvoirAdjudicateursCommission, refPouvoirAdjudicateur);
    }

    /**
     * La méthode est gardé parce qu'elle est appelé via introspection par le module Rédaction. Utiliser getRefProcedureEcriture ou getRefProcedureLecture selon besoin.
     *
     * @return Collection<Referentiel> de référentiels procédure accessible en lecture (ie moteur de recherche mais pas de nouvelle consultation).
     */
    @Deprecated
    public final Collection<EpmTRefProcedure> getRefProcedure() {
        return refProcedureLecture;
    }

    /**
     * @return Collection<Referentiel> de référentiels procédure accessible en ecriture (ie nouvelle consultation).
     */
    public final Collection<EpmTRefProcedure> getRefProcedureEcriture() {
        return refProcedureEcriture;
    }

    public void setRefProcedureEcriture(Collection<EpmTRefProcedure> valeur) {
        this.refProcedureEcriture = valeur;
    }

    /**
     * @return Collection<Referentiel> de référentiels procédure accessible en lecture (ie moteur de recherche mais pas de nouvelle consultation).
     */
    public final Collection<EpmTRefProcedure> getRefProcedureLecture() {
        return refProcedureLecture;
    }

    public void setRefProcedureLecture(Collection<EpmTRefProcedure> valeur) {
        this.refProcedureLecture = valeur;
    }

    /**
     *
     * @return Collection<Referentiel>de référentiels Procédure {@link EpmTRefProcedure}
     */
    public final Collection<EpmTRefProcedure> getRefProcedureTous(boolean lecture) {
        EpmTRefProcedure refProcedure = new EpmTRefProcedure();
        refProcedure.setId(0);
        refProcedure.setLibelleCourt(TOUTES);
        refProcedure.setLibelle(TOUTES);
        refProcedure.setAttributFormalise(TOUTES);
        if (lecture)
            return ajouterTous(refProcedureLecture, refProcedure);
        else
            return ajouterTous(refProcedureEcriture, refProcedure);
    }

    /**
     * @return Collection<Referentiel>de référentiels Type de marché
     * {@link EpmTRefTypeMarche}
     */
    public final Collection<EpmTRefTypeMarche> getRefTypeMarche() {
        return refTypeMarches;
    }

    /**
     * @param rtp collection de type de Marché {@link EpmTRefTypeMarche}
     */
    public final void setRefTypeMarche(final Collection<EpmTRefTypeMarche> rtp) {
        this.refTypeMarches = rtp;
    }

    /**
     * @return collection de référentiels Pouvoir Adjudicateur
     * {@link EpmTRefPouvoirAdjudicateur}
     */
    public final Collection<EpmTRefPouvoirAdjudicateur> getRefPouvoirAdjudicateurTous() {
        EpmTRefPouvoirAdjudicateur refPouvoirAdjudicateur;
        refPouvoirAdjudicateur = new EpmTRefPouvoirAdjudicateur();
        refPouvoirAdjudicateur.setId(0);
        refPouvoirAdjudicateur.setLibelle(TOUS);
        return ajouterTous(refPouvoirAdjudicateurs, refPouvoirAdjudicateur);
    }

    /**
     * @param rpa Collection<Referentiel>de référentiels Pouvoir Adjudicateur
     *            {@link EpmTRefPouvoirAdjudicateur}
     */
    public final void setRefPouvoirAdjudicateurCommision(final Collection<EpmTRefPouvoirAdjudicateur> rpa) {
        this.refPouvoirAdjudicateursCommission = rpa;
    }


    /**
     * @param ra Collection<Referentiel>de référentiels Article {@link EpmTRefArticle}
     */
    public final void setRefArticle(final Collection<EpmTRefArticle> ra) {
        this.refArticles = ra;
    }

    /**
     * @return Collection de {@link EpmTRefNature}
     */
    public final Collection<EpmTRefNature> getRefNature() {
        return refNatures;
    }

    /**
     * @param rn Collection<Referentiel>de référentiels Nature {@link EpmTRefNature}
     */
    public final void setRefNature(final Collection<EpmTRefNature> rn) {
        this.refNatures = rn;
    }

    /**
     * @return Collection<Referentiel>de {@link EpmTRefAutoriteDelegante} utilisé dans les
     * DSP
     */
    public final Collection<EpmTRefAutoriteDelegante> getRefAutoriteDelegante() {
        return refAutoriteDelegante;
    }

    /**
     * @param rn collection de référentiels Nature {@link EpmTRefNature}
     */
    public final void setRefAutoriteDelegante(final Collection<EpmTRefAutoriteDelegante> rn) {
        this.refAutoriteDelegante = rn;
    }

    /**
     * @return COllection de {@link EpmTRefTypeDsp} utilisé dans les DSP
     */
    public final Collection<EpmTRefTypeDsp> getRefTypeDsp() {
        return refTypeDsp;
    }

    /**
     * @param rn collection de référentiels Nature {@link EpmTRefNature}
     */
    public final void setRefTypeDsp(final Collection<EpmTRefTypeDsp> rn) {
        this.refTypeDsp = rn;
    }

    /**
     * @return COllection de {@link EpmTRefArrondissement} utilisé dans les DSP
     */
    public final Collection<EpmTRefArrondissement> getRefArrondissement() {
        return refArrondissement;
    }

    /**
     * @param rn Collection<Referentiel>de référentiels Nature {@link EpmTRefNature}
     */
    public final void setRefArrondissement(final Collection<EpmTRefArrondissement> rn) {
        this.refArrondissement = rn;
    }

    /**
     * @return COllection de {@link EpmTRefTypeConsultation} utilisé dans les
     * DSP
     */
    public final Collection<EpmTRefTypeConsultation> getRefTypeConsultation() {
        return refTypeConsultation;
    }

    /**
     * @param rn Collection<Referentiel>de référentiels Nature {@link EpmTRefNature}
     */
    public final void setRefTypeConsultation(final Collection<EpmTRefTypeConsultation> rn) {
        this.refTypeConsultation = rn;
    }

    /**
     * @return collection de {@link EpmTRefNature} avec la valeur TOUTES en
     * première position
     */
    public final Collection<EpmTRefNature> getRefNatureTous() {
        EpmTRefNature refNature = new EpmTRefNature();
	    refNature.setId(0);
        refNature.setLibelleCourt(TOUTES);
        refNature.setLibelle(TOUTES);
        return ajouterTous(refNatures, refNature);
    }

    public Collection<EpmTRefDirectionService> getRefDirectionServices() {
        return refDirectionServices;
    }

    public void setRefDirectionServices(Collection<EpmTRefDirectionService> refDirectionServices) {
        this.refDirectionServices = refDirectionServices;
    }

    public Collection<EpmTRefDirectionService> getRefDirectionServicesActives() {
        return refDirectionServicesActives;
    }

    public void setRefDirectionServicesActives(Collection<EpmTRefDirectionService> refDirectionServicesActives) {
        this.refDirectionServicesActives = refDirectionServicesActives;
    }

    /**
     * @return collection de {@link EpmTRefDirectionService} avec la valeur
     * TOUS en première position.
     */
    public final Collection<EpmTRefDirectionService> getRefDirectionServiceTous(final boolean actif) {
        EpmTRefDirectionService refTous = new EpmTRefDirectionService();
        refTous.setTypeEntite(TOUS);
        refTous.setLibelle(TOUS);
        return ajouterTous(actif ? refDirectionServicesActives : refDirectionServices, refTous);
    }

    public Collection<EpmTRefInfoBulle> getRefInfoBulle() {
        return refInfoBulle;
    }

    public void setRefInfoBulle(Collection<EpmTRefInfoBulle> refInfoBulle) {
        this.refInfoBulle = refInfoBulle;
    }

    /**
     * @param referentiels Liste de {@link EpmTRef}
     * @param objet        objet prenant la première position de la liste renvoyé
     * @return Liste de {@link EpmTRef}
     */
    private <T extends EpmTRef> List<T> ajouterTous( final Collection<T> referentiels, final T objet) {
        List<T> list = new ArrayList<T>();
        list.add(objet);
        list.addAll(referentiels);
        return list;
    }


    /**
     * @return liste de {@link EpmTRefDocumentUtilise}
     */
    public final List<EpmTRefDocumentUtilise> getRefDocumentUtilise() {
        return refDocumentUtilise;
    }

    /**
     * @param valeur liste de {@link EpmTRefDocumentUtilise}
     */
    public final void setRefDocumentUtilise(final List<EpmTRefDocumentUtilise> valeur) {
        this.refDocumentUtilise = valeur;
    }

    public void setRefFichePratique(Collection<EpmTRefFichePratique> refFichePratique) {
        this.refFichePratique = refFichePratique;
    }

    /**
     * @return the refModeleEtapesCalendrier
     */
    public final Collection<EpmTRefEtapeCalendrier> getRefModeleEtapesCalendrier() {
        return refModeleEtapesCalendrier;
    }

    /**
     * @param valeur the refModeleEtapesCalendrier to set
     */
    public final void setRefModeleEtapesCalendrier(Collection<EpmTRefEtapeCalendrier> valeur) {
        this.refModeleEtapesCalendrier = valeur;
    }

    /**
     * @return Liste des beneficiare du marche {@link EpmTRefBeneficiaireMarche}.
     */
    public final Collection<EpmTRefBeneficiaireMarche> getRefBeneficiaireMarcheList() {
        return refBeneficiaireMarcheList;
    }

    /**
     * @param valeur collection des beneficiaires du marche {@link EpmTRefBeneficiaireMarche}
     */
    public final void setRefBeneficiaireMarcheList(final Collection<EpmTRefBeneficiaireMarche> valeur) {
        this.refBeneficiaireMarcheList = valeur;
    }

    /**
     * @return collection des beneficiaires du marche {@link EpmTRefBeneficiaireMarche}
     */
    public final Collection<EpmTRefBeneficiaireMarche> getRefBeneficiaireMarcheListTous() {
        EpmTRefBeneficiaireMarche refBM = new EpmTRefBeneficiaireMarche();
        refBM.setId(0);
        refBM.setLibelle(TOUS);
        refBeneficiaireMarcheList.add(refBM);
        return ajouterTous(refBeneficiaireMarcheList, refBM);
    }

    /**
     * @return Liste des unités pour les formes de prix unitaires.
     */
    public final Collection<EpmTRefUnite> getRefUnite() {
        return refUnite;
    }

    /**
     * @param valeur Liste des unités pour les formes de prix unitaires.
     */
    public final void setRefUnite(final Collection<EpmTRefUnite> valeur) {
        this.refUnite = valeur;
    }

    public final Collection<EpmTRefCritereAttribution> getRefCritereAttribution() {
        return refCritereAttribution;
    }

    public final void setRefCritereAttribution(final Collection<EpmTRefCritereAttribution> valeur) {
        this.refCritereAttribution = valeur;
    }


    public void setRefParametrage(Collection<EpmTRefParametrage> refParametrage) {
        this.refParametrage = refParametrage;
    }

    public Collection<EpmTRefTypeEvenementCalendrier> getTypesEvenementsCalendrier() {
        return typesEvenementsCalendrier;
    }

    public final void setTypesEvenementsCalendrier(Collection<EpmTRefTypeEvenementCalendrier> valeur) {
        this.typesEvenementsCalendrier = valeur;
        this.evenementsCalendrier = new ArrayList<EpmTRefEvenementCalendrier>();
        for (EpmTRefTypeEvenementCalendrier epmTRefTypeEvenementCalendrier : valeur) {
            if (epmTRefTypeEvenementCalendrier.getEvenementAssocies() != null) {
                this.evenementsCalendrier.addAll(epmTRefTypeEvenementCalendrier.getEvenementAssocies());
            }
        }
    }

    public Collection<EpmTRefEvenementCalendrier> getEvenementsCalendrier() {
        return evenementsCalendrier;
    }



    public Map<Integer, DirectionService> getMapDirService() {
        return mapDirService;
    }

    /**
     * @return collection de référentiels forme juridique
     * {@link EpmTRefFormeJuridique}
     */
    public final Collection<EpmTRefFormeJuridique> getRefFormesJuridique() {
        return refFormesJuridique;
    }

    /**
     * @return collection de référentiels forme juridique
     * {@link EpmTRefFormeJuridique}
     */
    public final Collection<EpmTRefFormeJuridique> getRefFormesJuridiqueTous() {
        EpmTRefFormeJuridique refFormeJuridique = new EpmTRefFormeJuridique();
        refFormeJuridique.setId(0);
        refFormeJuridique.setLibelle(TOUS);
        refFormesJuridique.add(refFormeJuridique);
        return ajouterTous(refFormesJuridique, refFormeJuridique);
    }

    public void setRefFormesJuridique(List<EpmTRefFormeJuridique> refFormesJuridique) {
        this.refFormesJuridique = refFormesJuridique;
    }


    public void setMapDirService(final Map<Integer, DirectionService> valeur) {
        this.mapDirService = valeur;
    }


    public final List<EpmTRefCodeAchat> getRefCodeAchat() {
        return refCodeAchat;
    }

    public final void setRefCodeAchat(final List<EpmTRefCodeAchat> valeur) {
        this.refCodeAchat = valeur;
    }

    public final Collection<EpmTRefImputationBudgetaire> getRefImputationBudgetaire() {
        return refImputationBudgetaire;
    }

    public final void setRefImputationBudgetaire(final List<EpmTRefImputationBudgetaire> valeur) {
        this.refImputationBudgetaire = valeur;
    }

    public List<EpmTRefChampAdditionnel> getRefChampAdditionnel() {
        return refChampAdditionnel;
    }

    public void setRefChampAdditionnel(List<EpmTRefChampAdditionnel> refChampAdditionnel) {
        this.refChampAdditionnel = refChampAdditionnel;
    }

    public List<EpmTRefNomenclature> getRefNomenclature() {
        return refNomenclature;
    }

    public void setRefNomenclature(List<EpmTRefNomenclature> refNomenclature) {
        this.refNomenclature = refNomenclature;
    }

    public final List<EpmTRefTypeContratAvenant> getRefTypeContratAvenant() {
        return refTypeContratAvenant;
    }

    public final void setRefTypeContratAvenant(List<EpmTRefTypeContratAvenant> valeur) {
        this.refTypeContratAvenant = valeur;
    }

    public final List<EpmTRefTypeObjetContrat> getRefTypeObjetContrat() {
        return refTypeObjetContrat;
    }

    public final void setRefTypeObjetContrat(List<EpmTRefTypeObjetContrat> valeur) {
        this.refTypeObjetContrat = valeur;
    }

    public final List<EpmTRefOrganisme> getRefOrganisme() {
        return refOrganisme;
    }

    public final void setRefOrganisme(final List<EpmTRefOrganisme> valeur) {
        this.refOrganisme = valeur;
    }

    /**
     * @return the refTypeDocumentContrat
     */
    public final List<EpmTRefTypeDocumentContrat> getRefTypeDocumentContrat() {
        return refTypeDocumentContrat;
    }

    /**
     * @param valeur the refTypeDocumentContrat to set
     */
    public final void setRefTypeDocumentContrat(final List<EpmTRefTypeDocumentContrat> valeur) {
        this.refTypeDocumentContrat = valeur;
    }

    /**
     * @return the refCategorieDocumentContrat
     */
    public final List<EpmTRefCategorieDocumentContrat> getRefCategorieDocumentContrat() {
        return refCategorieDocumentContrat;
    }

    /**
     * @param valeur the refCategorieDocumentContrat to set
     */
    public final void setRefCategorieDocumentContrat(final List<EpmTRefCategorieDocumentContrat> valeur) {
        this.refCategorieDocumentContrat = valeur;
    }

    /**
     * @return the refMoyenNotification
     */
    public final List<EpmTRefStatutNotificationContrat> getRefStatutNotificationContrat() {
        return refStatutNotificationContrat;
    }

    /**
     * @param valeur the refMoyenNotification to set
     */
    public final void setRefStatutNotificationContrat(final List<EpmTRefStatutNotificationContrat> valeur) {
        this.refStatutNotificationContrat = valeur;
    }

    /**
     * @return the refTypeEtablissement
     */
    public final List<EpmTRefTypeEtablissement> getRefTypeEtablissement() {
        return refTypeEtablissement;
    }

    /**
     * @param valeur the refTypeEtablissement to set
     */
    public final void setRefTypeEtablissement(final List<EpmTRefTypeEtablissement> valeur) {
        this.refTypeEtablissement = valeur;
    }

    /**
     * @return the refTypeAttributaire
     */
    public final List<EpmTRefTypeAttributaire> getRefTypeAttributaire() {
        return refTypeAttributaire;
    }

    /**
     * @param valeur the refTypeAttributaire to set
     */
    public final void setRefTypeAttributaire(final List<EpmTRefTypeAttributaire> valeur) {
        this.refTypeAttributaire = valeur;
    }

    /**
     * @return the refRoleJuridique
     */
    public List<EpmTRefRoleJuridique> getRefRoleJuridique() {
        return refRoleJuridique;
    }

    /**
     * @param valeur the refRoleJuridique to set
     */
    public final void setRefRoleJuridique(final List<EpmTRefRoleJuridique> valeur) {
        this.refRoleJuridique = valeur;
    }


    /**
     * @return the refTrancheBudgetaire
     */
    public final List<EpmTRefTrancheBudgetaire> getRefTrancheBudgetaire() {
        Collections.sort(refTrancheBudgetaire, (r1, r2) -> r1.getId() < r2.getId() ? -1 : r1.getId() == r2.getId() ? 0 : 1);
        return refTrancheBudgetaire;
    }

    /**
     * @param valeur the refTrancheBudgetaire to set
     */
    public final void setRefTrancheBudgetaire(final List<EpmTRefTrancheBudgetaire> valeur) {
        this.refTrancheBudgetaire = valeur;
    }

    /**
     * @return the refTypeAvenant
     */
    public final List<EpmTRefTypeAvenant> getRefTypeAvenant() {
        return refTypeAvenant;
    }

    /**
     * @param valeur the refTypeAvenant to set
     */
    public final void setRefTypeAvenant(final List<EpmTRefTypeAvenant> valeur) {
        this.refTypeAvenant = valeur;
    }

    public final List<EpmTRefReponseElectronique> getRefReponseElectronique() {
        return refReponseElectronique;
    }

    public final void setRefReponseElectronique(final List<EpmTRefReponseElectronique> valeur) {
        this.refReponseElectronique = valeur;
    }


    public List<EpmTRefClausesSociales> getRefClausesSociales() {
        return refClausesSociales;
    }

    public void setRefClausesSociales(final List<EpmTRefClausesSociales> valeur) {
        this.refClausesSociales = valeur;
    }

    public List<EpmTRefClausesEnvironnementales> getRefClausesEnvironnementales() {
        return refClausesEnvironnementales;
    }

    public void setRefClausesEnvironnementales(final List<EpmTRefClausesEnvironnementales> valeur) {
        this.refClausesEnvironnementales = valeur;
    }

    public List<EpmTRefTypeStructureSociale> getRefStructureSocialeReserves() {
        return refStructureSocialeReserves;
    }

    public void setRefStructureSocialeReserves(List<EpmTRefTypeStructureSociale> valeur) {
        this.refStructureSocialeReserves = valeur;
    }

    public List<EpmTRefDroitProprieteIntellectuelle> getRefDroitProprieteIntellectuelle() {
        return refDroitProprieteIntellectuelle;
    }

    public void setRefDroitProprieteIntellectuelle(final List<EpmTRefDroitProprieteIntellectuelle> valeur) {
        this.refDroitProprieteIntellectuelle = valeur;
    }

    public List<EpmTRefLieuExecution> getRefLieuExecution() {
        return refLieuExecution;
    }

    public void setRefLieuExecution(final List<EpmTRefLieuExecution> valeur) {
        this.refLieuExecution = valeur;
    }


    /**
     * @return Collection de {@link EpmTRefMoyenNotification}
     */
    public final List<EpmTRefMoyenNotification> getMoyenNotification() {
        return moyenNotification;
    }

    /**
     * @param valeur Collection de {@link EpmTRefMoyenNotification}
     */
    public final void setMoyenNotification(final List<EpmTRefMoyenNotification> valeur) {
        this.moyenNotification = valeur;
    }


    /**
     * @return collection de d'objets Sélect Bon ou Quantité {@link EpmTRefBonQuantite}
     */
    public final Collection<EpmTRefBonQuantite> getBonQuantite() {
        return bonQuantites;
    }



    /**
     * @param bq collection d'objets Sélect Bon ou Quantité {@link EpmTRefBonQuantite}
     */
    public final void setBonQuantite(final Collection<EpmTRefBonQuantite> bq) {
        this.bonQuantites = bq;
    }

    /**
     * @return collection d'objets Sélect Choix Forme Prix  {@link EpmTRefChoixFormePrix}
     */
    public final Collection<EpmTRefChoixFormePrix> getChoixFormePrix() {
        return choixFormePrix;
    }

    /**
     * @return collection d'objets Sélect Choix Forme Prix {@link EpmTRefChoixFormePrix}
     */
    public final Collection<EpmTRefChoixFormePrix> getChoixFormePrixTous() {
        EpmTRefChoixFormePrix epmTRefChoixFormePrix = new EpmTRefChoixFormePrix();
        epmTRefChoixFormePrix.setId(0);
        epmTRefChoixFormePrix.setLibelle(TOUTES);
        return ajouterTous(choixFormePrix, epmTRefChoixFormePrix);
    }

    /**
     * @param cfp collection d'objets Sélect Choix Forme Prix {@link EpmTRefChoixFormePrix}
     */
    public final void setChoixFormePrix(final Collection<EpmTRefChoixFormePrix> cfp) {
        this.choixFormePrix = cfp;
    }

    /**
     * @return collection d'objets Sélect Choix Mois Jour {@link EpmTRefChoixMoisJour}
     */
    public final Collection<EpmTRefChoixMoisJour> getChoixMoisJour() {
        return choixMoisJours;
    }

    /**
     * @return collection d'objets Sélect Choix Mois Jour {@link EpmTRefChoixMoisJour}
     */
    public final Collection<EpmTRefChoixMoisJour> getChoixMoisJourTous() {
        EpmTRefChoixMoisJour choixMoisJour = new EpmTRefChoixMoisJour();
        choixMoisJour.setId(0);
        choixMoisJour.setLibelle(TOUS);
        return ajouterTous(choixMoisJours, choixMoisJour);
    }

    /**
     * @param cmj collection d'objets Sélect Choix Mois Jour {@link EpmTRefChoixMoisJour}
     */
    public final void setChoixMoisJour(final Collection<EpmTRefChoixMoisJour> cmj) {
        this.choixMoisJours = cmj;
    }

    /**
     * @return collection d'objets Sélect Durée Délai Description {@link EpmTRefDureeDelaiDescription}
     */
    public final Collection<EpmTRefDureeDelaiDescription> getDureeDelaiDescription() {
        return dureeDelaiDescriptions;
    }

    /**
     * @return collection d'objets Sélect Durée Délai Description {@link EpmTRefDureeDelaiDescription}
     */
    public final Collection<EpmTRefDureeDelaiDescription> getDureeDelaiDescriptionTous() {
        EpmTRefDureeDelaiDescription dureeDelaiDescription = new EpmTRefDureeDelaiDescription();
        dureeDelaiDescription.setId(0);
        dureeDelaiDescription.setLibelle(TOUTES);
        return ajouterTous(dureeDelaiDescriptions, dureeDelaiDescription);
    }

    /**
     * @param ddd collection d'objets Sélect Durée Délai Description {@link EpmTRefDureeDelaiDescription}
     */
    public final void setDureeDelaiDescription(final Collection<EpmTRefDureeDelaiDescription> ddd) {
        this.dureeDelaiDescriptions = ddd;
    }

    /**
     * @return collection d'objets Sélect Groupement Attributaire {@link EpmTRefGroupementAttributaire}
     */
    public final Collection<EpmTRefGroupementAttributaire> getGroupementAttributaire() {
        return groupementAttributaires;
    }

    /**
     * @return collection d'objets Sélect Groupement Attributaire {@link EpmTRefGroupementAttributaire} methode utilisé dans le conditionnement
     */
    public final Collection<EpmTRefGroupementAttributaire> getGroupementAttributaireActif() {
        return groupementAttributaires.stream()
                .filter(EpmTRef::isActif)
                .collect(Collectors.toList());
    }

    /**
     * @return collection d'objets Sélect Groupement Attributaire {@link EpmTRefGroupementAttributaire}
     */
    public final Collection<EpmTRefGroupementAttributaire> getGroupementAttributaireTous() {
        EpmTRefGroupementAttributaire groupementAttributaire = new EpmTRefGroupementAttributaire();
        groupementAttributaire.setId(0);
        groupementAttributaire.setLibelle(TOUS);
        return ajouterTous(groupementAttributaires, groupementAttributaire);
    }

    /**
     * @param ga collection d'objets Sélect Groupement Attributaire {@link EpmTRefGroupementAttributaire}
     */
    public final void setGroupementAttributaire(final Collection<EpmTRefGroupementAttributaire> ga) {
        this.groupementAttributaires = ga;
    }

    /**
     * @return collection d'objets Sélect Min Max {@link EpmTRefMinMax}
     */
    public final Collection<EpmTRefMinMax> getMinMax() {
        return minMax;
    }

    /**
     * @return collection d'objets Sélect Min Max {@link EpmTRefMinMax}
     */
    public final Collection<EpmTRefMinMax> getMinMaxTous() {
        EpmTRefMinMax epmTRefMinMax = new EpmTRefMinMax();
        epmTRefMinMax.setId(0);
        epmTRefMinMax.setLibelle(TOUS);
        return ajouterTous(minMax, epmTRefMinMax);
    }

    /**
     * @param mm collection d'objets Sélect Min Max {@link EpmTRefMinMax}
     */
    public final void setMinMax(final Collection<EpmTRefMinMax> mm) {
        this.minMax = mm;
    }

    /**
     * @return collection d'objets Sélect Nature Tranche {@link EpmTRefNatureTranche}
     */
    public final Collection<EpmTRefNatureTranche> getNatureTranche() {
        return natureTranches;
    }

    /**
     * @return collection d'objets Sélect Nature Tranche {@link EpmTRefNatureTranche}
     */
    public final Collection<EpmTRefNatureTranche> getNatureTrancheTous() {
        EpmTRefNatureTranche natureTranche = new EpmTRefNatureTranche();
        natureTranche.setId(0);
        natureTranche.setLibelle(TOUTES);
        return ajouterTous(natureTranches, natureTranche);
    }

    /**
     * @param nt collection d'objets Sélect Nature Tranche {@link EpmTRefNatureTranche}
     */
    public final void setNatureTranche(final Collection<EpmTRefNatureTranche> nt) {
        this.natureTranches = nt;
    }

    /**
     * @return collection d'objets Sélect Nombre Candidats Admis {@link EpmTRefNbCandidatsAdmis}
     */
    public final Collection<EpmTRefNbCandidatsAdmis> getNbCandidatsAdmis() {
        return nbCandidatsAdmis;
    }

    /**
     * @return collection d'objets Sélect Nombre Candidats Admis {@link EpmTRefNbCandidatsAdmis}
     */
    public final Collection<EpmTRefNbCandidatsAdmis> getNbCandidatsAdmisTous() {
        EpmTRefNbCandidatsAdmis epmTRefNbCandidatsAdmis = new EpmTRefNbCandidatsAdmis();
        epmTRefNbCandidatsAdmis.setId(0);
        epmTRefNbCandidatsAdmis.setLibelle(TOUS);
        return ajouterTous(nbCandidatsAdmis, epmTRefNbCandidatsAdmis);
    }

    /**
     * @param nca collection d'objets Sélect Nombre Candidats Admis {@link EpmTRefNbCandidatsAdmis}
     */
    public final void setNbCandidatsAdmis(final Collection<EpmTRefNbCandidatsAdmis> nca) {
        this.nbCandidatsAdmis = nca;
    }



    /**
     * @param rl collection d'objets Sélect Réservation Lot {@link EpmTRefReservationLot}
     */
    public final void setReservationLot(final Collection<EpmTRefReservationLot> rl) {
        this.reservationLots = rl;
    }

    /**
     * @return collection d'objets Sélect Tranche Type Prix {@link EpmTRefTrancheTypePrix}
     */
    public final Collection<EpmTRefTrancheTypePrix> getTrancheTypePrix() {
        return trancheTypePrix;
    }

    /**
     * @return collection d'objets Sélect Tranche Type Prix {@link EpmTRefTrancheTypePrix}
     */
    public final Collection<EpmTRefTrancheTypePrix> getTrancheTypePrixTous() {
        EpmTRefTrancheTypePrix epmTRefTrancheTypePrix = new EpmTRefTrancheTypePrix();
        epmTRefTrancheTypePrix.setId(0);
        epmTRefTrancheTypePrix.setLibelle(TOUS);
        return ajouterTous(trancheTypePrix, epmTRefTrancheTypePrix);
    }

    /**
     * @param ttp collection d'objets Sélect Tranche Type Prix {@link EpmTRefTrancheTypePrix}
     */
    public final void setTrancheTypePrix(final Collection<EpmTRefTrancheTypePrix> ttp) {
        this.trancheTypePrix = ttp;
    }

    /**
     * @return collection d'objets Sélect Type Prix {@link EpmTRefTypePrix}
     */
    public final Collection<EpmTRefTypePrix> getTypePrix() {
        return typePrix;
    }

    /**
     * @return collection d'objets Sélect Type Prix {@link EpmTRefTypePrix}
     */
    public final Collection<EpmTRefTypePrix> getTypePrixTous() {
        EpmTRefTypePrix epmTRefTypePrix = new EpmTRefTypePrix();
        epmTRefTypePrix.setId(0);
        epmTRefTypePrix.setLibelle(TOUS);
        return ajouterTous(typePrix, epmTRefTypePrix);
    }

    /**
     * @param tp collection d'objets Sélect Type Prix {@link EpmTRefTypePrix}
     */
    public final void setTypePrix(final Collection<EpmTRefTypePrix> tp) {
        this.typePrix = tp;
    }

    /**
     * @return collection d'objets Sélect Variation {@link EpmTRefVariation}
     */
    public final Collection<EpmTRefVariation> getVariation() {
        return variations;
    }

    /**
     * @return collection d'objets Sélect Variation {@link EpmTRefVariation}
     */
    public final Collection<EpmTRefVariation> getVariationTous() {
        EpmTRefVariation variation = new EpmTRefVariation();
        variation.setId(0);
        variation.setLibelle(TOUTES);
        return ajouterTous(variations, variation);
    }

    /**
     * @param v collection d'objets Sélect Variation {@link EpmTRefVariation}
     */
    public final void setVariation(final Collection<EpmTRefVariation> v) {
        this.variations = v;
    }

    /**
     * @return collection d'objets Sélect Statut {@link EpmTRefStatut}
     */
    public final Collection<EpmTRefStatut> getStatut() {
        return statuts;
    }

    /**
     * @return collection d'objets Sélect Statut {@link EpmTRefStatut}
     */
    public final Collection<EpmTRefStatut> getStatutTous() {
        EpmTRefStatut statut = new EpmTRefStatut();
        statut.setId(0);
        statut.setLibelle(TOUS);
        return ajouterTous(statuts, statut);
    }

    /**
     * @param s collection d'objets Sélect Statut {@link EpmTRefStatut}
     */
    public final void setStatut(final Collection<EpmTRefStatut> s) {
        this.statuts = s;
    }

    /**
     * @return collection d'objets Sélect Statut {@link EpmTRefStatutAvenant}
     */
    public final Collection<EpmTRefStatutAvenant> getStatutAvenant() {
        return statutAvenants;
    }

    /**
     * @return collection d'objets Sélect Statut {@link EpmTRefStatutAvenant}
     */
    public final Collection<EpmTRefStatutAvenant> getStatutAvenantTous() {
        EpmTRefStatutAvenant statut = new EpmTRefStatutAvenant();
        statut.setId(0);
        statut.setLibelle(TOUS);
        return ajouterTous(statutAvenants, statut);
    }

    /**
     * @param valeur collection d'objets Sélect Statut {@link EpmTRefStatutAvenant}
     */
    public final void setStatutAvenant(final Collection<EpmTRefStatutAvenant> valeur) {
        this.statutAvenants = valeur;
    }

    /**
     * @return collection d'objets Sélect Vision {@link EpmTRefVision}
     */
    public final Collection<EpmTRefVision> getVision() {
        return visions;
    }

    /**
     * @return collection d'objets Sélect Vision {@link EpmTRefVision}
     */
    public final Collection<EpmTRefVision> getVisionTous() {
        EpmTRefVision vision = new EpmTRefVision();
        vision.setId(0);
        vision.setLibelle(TOUS);
        return ajouterTous(visions, vision);
    }

    /**
     * @param v collection d'objets Sélect Vision {@link EpmTRefVision}
     */
    public final void setVision(final Collection<EpmTRefVision> v) {
        this.visions = v;
    }

    /**
     * @return Collection de {@link EpmTRefModaliteRetrait}
     */
    public final Collection<EpmTRefModaliteRetrait> getModaliteDeRetrait() {
        return modaliteDeRetrait;
    }

    /**
     * @param valeur Collection de {@link EpmTRefModaliteRetrait}
     */
    public final void setModaliteDeRetrait(final Collection<EpmTRefModaliteRetrait> valeur) {
        this.modaliteDeRetrait = valeur;
    }

    /**
     * @return collection d'objets Sélect modalite de retrait {@link EpmTRefModaliteRetrait}
     */
    public final Collection<EpmTRefModaliteRetrait> getModaliteDeRetraitTous() {
        EpmTRefModaliteRetrait epmTRefModaliteRetrait = new EpmTRefModaliteRetrait();
        epmTRefModaliteRetrait.setId(0);
        epmTRefModaliteRetrait.setLibelle(TOUS);
        return ajouterTous(modaliteDeRetrait, epmTRefModaliteRetrait);

    }

    /**
     * @return Collection de {@link EpmTRefModaliteDepotElement}
     */
    public final Collection<EpmTRefModaliteDepotElement> getModaliteDepotElement() {
        return modaliteDepotElement;
    }

    /**
     * @return collection d'objets Sélect {@link EpmTRefModaliteDepotElement}
     */
    public final Collection<EpmTRefModaliteDepotElement> getModaliteDepotElementTous() {
        EpmTRefModaliteDepotElement epmtRefModaliteDepotElement = new EpmTRefModaliteDepotElement();
        epmtRefModaliteDepotElement.setId(0);
        epmtRefModaliteDepotElement.setLibelle(TOUS);
        return ajouterTous(modaliteDepotElement, epmtRefModaliteDepotElement);
    }

    /**
     * @param valeur Collection de {@link EpmTRefModaliteDepotElement}
     */
    public final void setModaliteDepotElement(final Collection<EpmTRefModaliteDepotElement> valeur) {
        this.modaliteDepotElement = valeur;
    }

    /**
     * @return Collection de {@link EpmTRefModaliteDepotPli}
     */
    public final Collection<EpmTRefModaliteDepotPli> getModaliteDepotPli() {
        return modaliteDepotPli;
    }

    /**
     * @return Collection de {@link EpmTRefModaliteDepotPli}
     */
    public final Collection<EpmTRefModaliteDepotPli> getModaliteDepotPliTous() {
        EpmTRefModaliteDepotPli epmtRefModaliteDepotPli = new EpmTRefModaliteDepotPli();
        epmtRefModaliteDepotPli.setId(0);
        epmtRefModaliteDepotPli.setLibelle(TOUS);
        return ajouterTous(modaliteDepotPli, epmtRefModaliteDepotPli);
    }

    /**
     * @param valeur Collection de {@link EpmTRefModaliteDepotPli}
     */
    public final void setModaliteDepotPli(final Collection<EpmTRefModaliteDepotPli> valeur) {
        this.modaliteDepotPli = valeur;
    }

    /**
     * @param appreciation true si c'est une appréciation, false sinon.
     * @return Collection de {@link EpmTRefSuiteADonner}
     */
    public final Collection<EpmTRefSuiteADonner> getSuiteADonner(final boolean appreciation) {
        return suiteADonner.stream()
                .filter(sad -> sad.isAppreciation() == appreciation)
                .collect(Collectors.toList());
    }

    /**
     * @param valeur Collection de {@link EpmTRefSuiteADonner}
     */
    public final void setSuiteADonner(final Collection<EpmTRefSuiteADonner> valeur) {
        this.suiteADonner = valeur;
    }

    /**
     * @param appreciation true si c'est une appréciation, false sinon.
     * @return Collection de {@link EpmTRefSuiteNegociation}
     */
    public final List<EpmTRefSuiteNegociation> getSuiteNegociation(final boolean appreciation) {
        return suiteNegociations.stream()
                .filter(sn -> sn.isAppreciation() == appreciation)
                .collect(Collectors.toList());
    }

    public void setSuiteNegociations(List<EpmTRefSuiteNegociation> suiteNegociations) {
        this.suiteNegociations = suiteNegociations;
    }

    /**
     * @return Collection de {@link EpmTRefStatutEntreprise}
     */
    public final Collection<EpmTRefStatutEntreprise> getStatutEntreprise() {
        return statutEntreprise;
    }

    /**
     * @param valeur Collection de {@link EpmTRefStatutEntreprise}
     */
    public final void setStatutEntreprise(final Collection<EpmTRefStatutEntreprise> valeur) {
        this.statutEntreprise = valeur;
    }

    /**
     * @return Collection de {@link EpmTRefAdmissibilite} pour l'écran
     *         d'informations complémentaires pour l'ouverture des candidatures
     */
    public final Collection<EpmTRefAdmissibilite> getAdmissibiliteInitCan() {
        List<EpmTRefAdmissibilite> liste = new ArrayList(admissibilite.size());
        for (EpmTRefAdmissibilite element : admissibilite) {
            switch (element.getId()) {
                case EpmTRefAdmissibilite.ADMISSIBLE:
                case EpmTRefAdmissibilite.NON_ADMISSIBLE:
                case EpmTRefAdmissibilite.EN_ATTENTE:
                    liste.add(element);
                    break; // fin de ce que l'on garde
                case EpmTRefAdmissibilite.HORS_DELAI:
                    break; // fin de ce que l'on ne garde pas
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * @return Collection de {@link EpmTRefAdmissibilite} pour l'écran
     *         d'informations complémentaires pour l'ouverture des candidatures
     */
    public final Collection<EpmTRefAdmissibilite> getAdmissibiliteComplCan() {
        List<EpmTRefAdmissibilite> liste = new ArrayList(admissibilite.size());
        for (EpmTRefAdmissibilite element : admissibilite) {
            switch (element.getId()) {
                case EpmTRefAdmissibilite.ADMISSIBLE:
                case EpmTRefAdmissibilite.NON_ADMISSIBLE:
                    liste.add(element);
                    break; // fin de ce que l'on garde
                case EpmTRefAdmissibilite.HORS_DELAI:
                case EpmTRefAdmissibilite.EN_ATTENTE:
                    break; // fin de ce que l'on ne garde pas
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * @return Collection de {@link EpmTRefCompletudeEnveloppe} pour l'écran des
     *         informations complémentaires de candidature.
     */
    public final Collection<EpmTRefCompletudeEnveloppe> getCompletudeEnveloppeComplCan() {
        List<EpmTRefCompletudeEnveloppe> liste = new ArrayList(completudeEnveloppe.size());
        for (EpmTRefCompletudeEnveloppe aCompletudeEnveloppe : completudeEnveloppe) {
            EpmTRefCompletudeEnveloppe element;
            element = aCompletudeEnveloppe;
            switch (element.getId()) {
                case EpmTRefCompletudeEnveloppe.INCOMPLET:
                case EpmTRefCompletudeEnveloppe.COMPLET:
                case EpmTRefCompletudeEnveloppe.IRRECEVABLE:
                    liste.add(element);
                    break; // fin de ce que l'on garde
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * @return Collection de {@link EpmTRefCompletudeEnveloppe} pour l'écran
     *         d'ouverture des candidatures initiale
     */
    public final Collection<EpmTRefCompletudeEnveloppe> getCompletudeEnveloppeInitCan() {
        List<EpmTRefCompletudeEnveloppe> liste = new ArrayList(completudeEnveloppe.size());
        for (EpmTRefCompletudeEnveloppe aCompletudeEnveloppe : completudeEnveloppe) {
            EpmTRefCompletudeEnveloppe element;
            element = aCompletudeEnveloppe;
            switch (element.getId()) {
                case EpmTRefCompletudeEnveloppe.COMPLET:
                case EpmTRefCompletudeEnveloppe.INCOMPLET:
                case EpmTRefCompletudeEnveloppe.IRRECEVABLE:
                    liste.add(element);
                    break; // fin de ce que l'on garde
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * @param valeur admissibilite collection <b>complète</b> de sélects
     */
    public final void setAdmissibilite(final Collection<EpmTRefAdmissibilite> valeur) {
        this.admissibilite = valeur;
    }

    /**
     * @param valeur completudeEnveloppe collection <b>complète</b> de sélects
     */
    public final void setCompletudeEnveloppe(final Collection<EpmTRefCompletudeEnveloppe> valeur) {
        this.completudeEnveloppe = valeur;
    }

    /**
     * @param valeur Collection de {@link EpmTRefActionPossible}
     */
    public final void setActionsPossibles(final Collection<EpmTRefActionPossible> valeur) {
        this.actionsPossibles = valeur;
    }

    /**
     * @param valeur Collection de {@link EpmTRefStatutEnveloppe}
     */
    public final void setStatutsCandidatures(final Collection<EpmTRefStatutEnveloppe> valeur) {
        this.statutsCandidatures = valeur;
    }

    /**
     * @param valeur Collection de {@link EpmTRefTypeEnveloppe}
     */
    public final void setTypesCandidatures(final Collection<EpmTRefTypeEnveloppe> valeur) {
        this.typesCandidatures = valeur;
    }


    /**
     * Écran candidatures initiales.
     *
     * @param recep type d'enveloppe
     * @param statut statut de l'enveloppe
     * @return liste des sélects à afficher pour cette configuration
     */
    public final List<EpmTRefActionPossible> getActionsPossiblesInitCanSi(final int recep, final int statut, final boolean signature) {
        List<EpmTRefActionPossible> liste = new ArrayList<EpmTRefActionPossible>(actionsPossibles.size());

        boolean init = statut == EpmTRefStatutEnveloppe.STAT_FERMEE || statut == EpmTRefStatutEnveloppe.STAT_HORS_DELAI;

        for (EpmTRefActionPossible actionsPossible : actionsPossibles) {
            switch (actionsPossible.getId()) {
                case EpmTRefActionPossible.ACT_INDIQUER_HORS_DELAI:
                    if (statut == EpmTRefStatutEnveloppe.STAT_OUVERTE && recep == EpmTRefTypeEnveloppe.TYPE_PAPIER)
                        liste.add(actionsPossible);
                    else if (recep == EpmTRefTypeEnveloppe.TYPE_PAPIER && init && statut != EpmTRefStatutEnveloppe.STAT_HORS_DELAI)
                        liste.add(actionsPossible);
                    break;

                case EpmTRefActionPossible.ACT_INDIQUER_OUVERTE:
                    // Défaut pour le papier
                    if (recep == EpmTRefTypeEnveloppe.TYPE_PAPIER && init)
                        liste.add(actionsPossible);
                    break;

                case EpmTRefActionPossible.ACT_OUVRIR:
                    // Défaut pour électronique
                    if (recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE && init)
                        liste.add(actionsPossible);
                    break;

                case EpmTRefActionPossible.ACT_REJETER_HORS_DELAI:
                    // Défaut pour électronique
                    if (statut == EpmTRefStatutEnveloppe.STAT_OUVERTE && recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE)
                        liste.add(actionsPossible);
                    else if (recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE && init && statut != EpmTRefStatutEnveloppe.STAT_HORS_DELAI)
                        liste.add(actionsPossible);
                    break;

                case EpmTRefActionPossible.ACT_IDENTIFIER_LOTS:
                    if (statut == EpmTRefStatutEnveloppe.STAT_OUVERTE) {
                        liste.add(actionsPossible);
                    }
                    break;
                case EpmTRefActionPossible.ACT_IDENTIFIER_LOTS_COMPL:
                    break; // pas dans cet écran
                case EpmTRefActionPossible.ACT_VERIFIER_SIGNATURES:
                    if (signature && recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE && statut == EpmTRefStatutEnveloppe.STAT_OUVERTE)
                        liste.add(actionsPossible);
                    break;

                case EpmTRefActionPossible.ACT_TELECHARGER:
                    if (recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE && statut == EpmTRefStatutEnveloppe.STAT_OUVERTE)
                        liste.add(actionsPossible);
                    break;
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * Écran demande de compléments pour la candidature.
     *
     * @param recep type d'enveloppe
     * @param statut statut de l'enveloppe
     * @return liste des sélects à afficher pour cette configuration
     */
    public final Collection getActionsPossiblesDemandeComplSi(final int recep, final int statut, final boolean signature) {
        // Adaptation de la méthode précédente, cf identifier_lots /
        // lots_compl
        List liste = new ArrayList(actionsPossibles.size());

        boolean init = statut == EpmTRefStatutEnveloppe.STAT_FERMEE || statut == EpmTRefStatutEnveloppe.STAT_HORS_DELAI;

        for (Iterator iter = actionsPossibles.iterator(); iter.hasNext();) {
            EpmTRefActionPossible element;
            element = (EpmTRefActionPossible) iter.next();
            switch (element.getId()) {
                case EpmTRefActionPossible.ACT_INDIQUER_OUVERTE:
                case EpmTRefActionPossible.ACT_INDIQUER_HORS_DELAI:
                    // Défaut pour le papier
                    if (recep == EpmTRefTypeEnveloppe.TYPE_PAPIER && init) {
                        liste.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_OUVRIR:
                case EpmTRefActionPossible.ACT_REJETER_HORS_DELAI:
                    // Défaut pour électronique
                    if (recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE && init) {
                        liste.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_IDENTIFIER_LOTS:
                    break; // pas dans cet écran
                case EpmTRefActionPossible.ACT_IDENTIFIER_LOTS_COMPL:
                    if (statut == EpmTRefStatutEnveloppe.STAT_OUVERTE) {
                        liste.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_VERIFIER_SIGNATURES:
                    if (signature
                            && recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && statut == EpmTRefStatutEnveloppe.STAT_OUVERTE) {
                        liste.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_TELECHARGER:
                    if (recep == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && statut == EpmTRefStatutEnveloppe.STAT_OUVERTE) {
                        liste.add(element);
                    }
                    break;
                default:
                    break; // TODO gestion des erreurs?
            }
        }
        return liste;
    }

    /**
     * Écran d'ouverture des offres.
     * @param typeEnveloppe identifiant {@link EpmTRefTypeEnveloppe}
     * @param statutEnveloppe identifiant {@link EpmTRefStatutEnveloppe}
     * @return sélects correspondants
     */
    public final List<EpmTRefActionPossible> getActionsPossiblesOuvertureOffre(
            final int typeEnveloppe, final int statutEnveloppe,
            final boolean isTableauOuvertureOffre, final boolean signature,
            final boolean ouvertureCandidature, final boolean alloti, final boolean plisPresent) {
        List<EpmTRefActionPossible> result = new ArrayList(actionsPossibles.size());
        for (Iterator iter = actionsPossibles.iterator(); iter.hasNext();) {
            EpmTRefActionPossible element;
            element = (EpmTRefActionPossible) iter.next();
            switch (element.getId()) {
                case EpmTRefActionPossible.ACT_OUVRIR:
                    if (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_FERMEE) {
                        result.add(element);
                    } else if (!ouvertureCandidature
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_ECHEC_OUVERTURE)) {
                        result.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_REJETER_HORS_DELAI:
                    if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_FERMEE || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_ECHEC_OUVERTURE)) {
                        result.add(element);
                    } else if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_PAPIER
                            && statutEnveloppe == EpmTRefStatutEnveloppe.STAT_FERMEE) {
                        result.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_VERIFIER_SIGNATURES:
                    if (signature
                            && typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_COMPLETE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_A_COMPLETER)
                            && plisPresent) {
                        result.add(element);
                    }
                    break;
                case EpmTRefActionPossible.ACT_TELECHARGER:
                    if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_COMPLETE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_A_COMPLETER)
                            && plisPresent) {
                        result.add(element);
                    }
                    break;

                case EpmTRefActionPossible.ACT_INDIQUER_COMPLETE:
                case EpmTRefActionPossible.ACT_INDIQUER_A_COMPLETER:
                    if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            && !isTableauOuvertureOffre) {
                        result.add(element);
                    } else if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_PAPIER
                            && statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            && !isTableauOuvertureOffre) {
                        result.add(element);
                    }
                    break;

                case EpmTRefActionPossible.ACT_LOT_NON_PRESENTE:
                    if ((statutEnveloppe != EpmTRefStatutEnveloppe.STAT_LOT_NON_PRESENTE)
                            && !isTableauOuvertureOffre
                            && ((typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_PAPIER)
                            || ((typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE) && !plisPresent))
                            && ((ouvertureCandidature && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_FERMEE))
                            || (alloti))) {
                        result.add(element);
                    }
                    break;

                case EpmTRefActionPossible.ACT_COMPLETE_A_OUVERTURE:
                case EpmTRefActionPossible.ACT_INCOMPLETE_A_OUVERTURE_A_COMPLETER:
                    if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_ELECTRONIQUE
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_COMPLETE || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_A_COMPLETER)
                            && isTableauOuvertureOffre) {
                        result.add(element);
                    } else if (typeEnveloppe == EpmTRefTypeEnveloppe.TYPE_PAPIER
                            && (statutEnveloppe == EpmTRefStatutEnveloppe.STAT_OUVERTE
                            || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_COMPLETE || statutEnveloppe == EpmTRefStatutEnveloppe.STAT_A_COMPLETER)
                            && isTableauOuvertureOffre) {
                        result.add(element);
                    }
                    break;

                default:
                    break;
            }
        }
        return result;
    }

    /**
     * @return Collection de {@link EpmTRefActionPossible}
     */
    public final Collection<EpmTRefActionPossible> getActionsPossibles() {
        return actionsPossibles;
    }

    /**
     * @return Collection de {@link EpmTRefStatutEnveloppe}
     */
    public final Collection<EpmTRefStatutEnveloppe> getStatutsCandidatures() {
        return statutsCandidatures;
    }

    /**
     * @return Collection de {@link EpmTRefTypeEnveloppe}
     */
    public final Collection<EpmTRefTypeEnveloppe> getTypesCandidatures() {
        return typesCandidatures;
    }


    /**
     * Retourne la liste des civilités.
     * @return la liste des civilités.
     */
    public final Collection<EpmTRefCivilite> getListeCivilites() {
        return listeCivilites;
    }

    /**
     * Assigne la liste des civilités.
     * @param valeur la valeur à assigner
     */
    public final void setListeCivilites(final Collection<EpmTRefCivilite> valeur) {
        this.listeCivilites = valeur;
    }

    /**
     * Assigne la liste des formes juridiques.
     * @param valeur la valeur à assigner
     */
    public final void setListeFormeJuridique(final Collection<EpmTRefFormeJuridique> valeur) {
        this.listeFormeJuridique = valeur;
    }

    /**
     * Retourne la liste des formes juridiques.
     * @return la liste des formes juridiques.
     */
    public final Collection<EpmTRefFormeJuridique> getListeFormeJuridique() {
        return listeFormeJuridique;
    }

    /**
     * @return le select des actions de l'écran de confirmation. d'attribution.
     */
    public final Collection<EpmTRefActionConfirmationAttribution> getActionConfirmationAttribution() {
        return actionConfirmationAttribution;
    }

    /**
     * Assigne le select des actions de l'écran de confirmation.
     * @param valeur la valeur à assigner
     */
    public final void setActionConfirmationAttribution(final Collection<EpmTRefActionConfirmationAttribution> valeur) {
        this.actionConfirmationAttribution = valeur;
    }

    /**
     * @return le select des actions de l'écran de confirmation des attributions
     *         des offres dans le cas d'un changement d'attributaire.
     */
    public final Collection<EpmTRefActionConfirmationAttributionChgtAttributaire> getActionConfirmationAttributionChgtAttributaire() {
        return actionConfirmationAttributionChgtAttributaire;
    }

    /**
     * Assigne le select des actions de l'écran de confirmation des attributions
     * des offres dans le cas d'un changement d'attributaire.
     * @param valeur la valeur à assigner
     */
    public final void setActionConfirmationAttributionChgtAttributaire(final Collection<EpmTRefActionConfirmationAttributionChgtAttributaire> valeur) {
        this.actionConfirmationAttributionChgtAttributaire = valeur;
    }

    /**
     * @param valeur les selects à assigner aux différents formats d'échanges.
     */
    public final void setFormatEchange(final Collection<EpmTRefFormatEchange> valeur) {
        this.formatEchange = valeur;
    }

    /**
     * @return la liste des formats d'échanges possible pour l'écran du suivi des échanges.
     */
    public final Collection<EpmTRefFormatEchange> getFormatEchange() {
        return formatEchange;
    }

    /**
     * @return matins, après-midi ou les deux {@link EpmTRefMatinAprem}
     */
    public final Collection<EpmTRefMatinAprem> getMatinsAprems() {
        return matinsAprems;
    }

    /**
     * @param valeur matins, après-midi ou les deux {@link EpmTRefMatinAprem}
     */
    public final void setMatinsAprems(final Collection<EpmTRefMatinAprem> valeur) {
        this.matinsAprems = valeur;
    }

    /**
     * @return présences des membres à une réunion {@link EpmTRefPresence}
     */
    public final Collection<EpmTRefPresence> getPresences() {
        return presences;
    }

    /**
     * @param valeur présences des membres à une réunion {@link EpmTRefPresence}
     */
    public final void setPresences(final Collection<EpmTRefPresence> valeur) {
        this.presences = valeur;
    }

    /**
     * @return rôles des membres permanents {@link EpmTRefRole}
     */
    public final Collection<EpmTRefRole> getRoles() {
        return roles;
    }

    /**
     * @param valeur rôles des membres permanents {@link EpmTRefRole}
     */
    public final void setRoles(final Collection<EpmTRefRole> valeur) {
        this.roles = valeur;
    }


    /**
     * @return sélects {@link EpmTRefEtatAttestation}
     */
    public final Collection<EpmTRefEtatAttestation> getEtatsAttestations() {
        return etatsAttestations;
    }

    /**
     * @param valeur sélects {@link EpmTRefEtatAttestation}
     */
    public final void setEtatsAttestations(final Collection<EpmTRefEtatAttestation> valeur) {
        this.etatsAttestations = valeur;
    }



    /**
     * @param valeur le statut de l'offre financière.
     */
    public final void setStatutOffreFinanciere(final Collection<EpmTRefStatutOffreFinanciere> valeur) {
        this.statutOffreFinanciere = valeur;
    }

    /**
     * @return valeur le statut de l'offre financière.
     */
    public final Collection<EpmTRefStatutOffreFinanciere> getStatutOffreFinanciere() {
        return statutOffreFinanciere;
    }

    /**
     * @return les départements.
     */
    public final Collection<EpmTRefDepartement> getDepartements() {
        return departements;
    }

    /**
     * @param valeur les départements.
     */
    public final void setDepartements(final Collection<EpmTRefDepartement> valeur) {
        this.departements = valeur;
    }



    /**
     * @return la liste de type de budget
     */
    public final Collection<EpmTRefTypeBudget> getTypesBudget() {
        return typesBudget;
    }

    /**
     * @param valeur la liste de type de budget
     */
    public final void setTypesBudget(final Collection<EpmTRefTypeBudget> valeur) {
        this.typesBudget = valeur;
    }



    public List<EpmTRefDocumentEmplacement> getRefDocumentEmplacements() {
        return refDocumentEmplacements;
    }

    public void setRefDocumentEmplacements(List<EpmTRefDocumentEmplacement> refDocumentEmplacements) {
        this.refDocumentEmplacements = refDocumentEmplacements;
    }

    public List<EpmTRefDocumentDestinataire> getRefDocumentDestinataires() {
        return refDocumentDestinataires;
    }

    public void setRefDocumentDestinataires(List<EpmTRefDocumentDestinataire> refDocumentDestinataires) {
        this.refDocumentDestinataires = refDocumentDestinataires;
    }

    public List<EpmTRefDomaineDonnees> getRefDomainesDonnees() {
        return refDomainesDonnees;
    }

    public void setRefDomainesDonnees(List<EpmTRefDomaineDonnees> refDomainesDonnees) {
        this.refDomainesDonnees = refDomainesDonnees;
    }

    public List<EpmTRefChampFusionSimple> getRefChampsFusionSimples() {
        return refChampsFusionSimples;
    }

    public void setRefChampsFusionSimples(List<EpmTRefChampFusionSimple> refChampsFusionSimples) {
        this.refChampsFusionSimples = refChampsFusionSimples;
    }

    public List<EpmTRefChampFusionComplexe> getRefChampsFusionComplexes() {
        return refChampsFusionComplexes;
    }

    public void setRefChampsFusionComplexes(List<EpmTRefChampFusionComplexe> refChampsFusionComplexes) {
        this.refChampsFusionComplexes = refChampsFusionComplexes;
    }

    public List<EpmTRefDocumentType> getRefDocumentTypesDocument() {
        return refDocumentTypesDocument;
    }

    public void setRefDocumentTypesDocument(List<EpmTRefDocumentType> refDocumentTypesDocument) {
        this.refDocumentTypesDocument = refDocumentTypesDocument;
    }

    public List<EpmTRefDocumentType> getRefDocumentTypesExcel() {
        return refDocumentTypesExcel;
    }

    public void setRefDocumentTypesExcel(List<EpmTRefDocumentType> refDocumentTypesExcel) {
        this.refDocumentTypesExcel = refDocumentTypesExcel;
    }

    // REDACTION //

    public List<EpmTRefActionDocument> getRefActionDocuments() {
        return refActionDocuments;
    }

    public void setRefActionDocuments(List<EpmTRefActionDocument> refActionDocuments) {
        this.refActionDocuments = refActionDocuments;
    }

    public List<EpmTRefAuteur> getRefAuteurs() {
        return refAuteurs;
    }

    public void setRefAuteurs(List<EpmTRefAuteur> refAuteurs) {
        this.refAuteurs = refAuteurs;
    }

    public List<EpmTRefExtensionFichier> getRefExtensionFichiers() {
        return refExtensionFichiers;
    }

    public void setRefExtensionFichiers(List<EpmTRefExtensionFichier> refExtensionFichiers) {
        this.refExtensionFichiers = refExtensionFichiers;
    }

    public List<EpmTRefIntervention> getRefInterventions() {
        return refInterventions;
    }

    public void setRefInterventions(List<EpmTRefIntervention> refInterventions) {
        this.refInterventions = refInterventions;
    }

    public List<EpmTRefPotentiellementConditionnee> getRefPotentiellementConditionnees() {
        return refPotentiellementConditionnees;
    }

    public void setRefPotentiellementConditionnees(List<EpmTRefPotentiellementConditionnee> refPotentiellementConditionnees) {
        this.refPotentiellementConditionnees = refPotentiellementConditionnees;
    }

    public List<EpmTRefStatutRedactionClausier> getRefStatutRedactionClausiers() {
        return refStatutRedactionClausiers;
    }

    public void setRefStatutRedactionClausiers(List<EpmTRefStatutRedactionClausier> refStatutRedactionClausiers) {
        this.refStatutRedactionClausiers = refStatutRedactionClausiers;
    }

    public List<EpmTRefThemeClause> getRefThemeClauses() {
        return refThemeClauses;
    }

    public void setRefThemeClauses(List<EpmTRefThemeClause> refThemeClauses) {
        this.refThemeClauses = refThemeClauses;
    }

    public List<EpmTRefTypeClause> getRefTypeClauses() {
        return refTypeClauses;
    }

    public void setRefTypeClauses(List<EpmTRefTypeClause> refTypeClauses) {
        this.refTypeClauses = refTypeClauses;
    }

    public List<EpmTRefTypeContrat> getRefTypeContrats() {
        return refTypeContrats;
    }

    public void setRefTypeContrats(List<EpmTRefTypeContrat> refTypeContrats) {
        this.refTypeContrats = refTypeContrats;
    }

    public List<EpmTRefTypeDocument> getRefTypeDocuments() {
        return refTypeDocuments;
    }

    public void setRefTypeDocuments(List<EpmTRefTypeDocument> refTypeDocuments) {
        this.refTypeDocuments = refTypeDocuments;
    }

    public List<EpmTRefTypePageDeGarde> getRefTypePageDeGardes() {
        return refTypePageDeGardes;
    }

    public void setRefTypePageDeGardes(List<EpmTRefTypePageDeGarde> refTypePageDeGardes) {
        this.refTypePageDeGardes = refTypePageDeGardes;
    }

    public List<EpmTRefValeurTypeClause> getRefValeurTypeClauses() {
        return refValeurTypeClauses;
    }

    public void setRefValeurTypeClauses(List<EpmTRefValeurTypeClause> refValeurTypeClauses) {
        this.refValeurTypeClauses = refValeurTypeClauses;
    }

	public Optional<EpmTRefTypeDocument> findTypeDocument( String uid) {
		return this.getRefTypeDocuments().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefStatutRedactionClausier> findStatutRedactionClausier( String uid) {
		return this.getRefStatutRedactionClausiers().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefTypeContrat> findTypeContrat( String uid) {
		return this.getRefTypeContrats().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefTypeClause> findTypeClause( String uid ) {
		return this.getRefTypeClauses().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefThemeClause> findThemeClause( String uid ) {
		return this.getRefThemeClauses().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefProcedure> findProcedure( String uid ) {
		return  this.getRefProcedureLecture().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

	public Optional<EpmTRefPotentiellementConditionnee> findRefPotentiellementConditionnee( String uid ) {
		return  this.getRefPotentiellementConditionnees().stream().filter(
			type -> type.getUid().equals(uid)
		).findFirst();
	}

    public Collection<EpmTRefProcedure> getRefProcedureEcritureTous() {
        EpmTRefProcedure epmTRefProcedure = new EpmTRefProcedure();
        epmTRefProcedure.setId(0);
        epmTRefProcedure.setLibelleCourt(TOUTES);
        epmTRefProcedure.setLibelle(TOUTES);
        return ajouterTous(refProcedureEcriture, epmTRefProcedure);
    }
}
