package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * Recherche des natures de prestations.
 * @author RVI
 */
public class NatureCritere extends AbstractCritere implements Critere, Serializable {

    private static final long serialVersionUID = -17468236648477910L;

    private Integer id;

    private String codeExterne;
    
    private String libelleCourt;
    private String libelle;

    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();

        sb.append(" from EpmTRefNature c ");
        if (id != null) {
            sb.append(" where id = :id ");
            parametres.put("id", id);
            debut = true;
        }
        if (codeExterne != null) {
            if (!debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" codeExterne = :codeExterne ");
            parametres.put("codeExterne", codeExterne);
            debut = true;
        }
        if (libelleCourt != null) {
            if (!debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" libelleCourt = :libelleCourt ");
            parametres.put("libelleCourt", libelleCourt);
            debut = true;
        }
        if (libelle != null) {
            if (!debut) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" libelle = :libelle ");
            parametres.put("libelle", libelle);
            debut = true;
        }

        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by c.");
            sb.append(proprieteTriee);
        }

        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(*) ");
        return select.append(corpsRequete()).toString();
    }

    public final Integer getId() {
        return id;
    }

    public final void setId(final Integer valeur) {
        this.id = valeur;
    }

    public final void setCodeExterne(final String valeur) {
        this.codeExterne = valeur;
    }

    public final void setLibelleCourt(final String valeur) {
        this.libelleCourt = valeur;
    }
    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }
}
