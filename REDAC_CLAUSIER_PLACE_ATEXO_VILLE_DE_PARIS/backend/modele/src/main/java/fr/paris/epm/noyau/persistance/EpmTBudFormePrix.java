/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;


/**
 * Pojo hibernate père du Bloc Unitaire de Données Forme de Prix.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 *
 */

public class EpmTBudFormePrix extends EpmTAbstractObject {

    // Propriétés

     /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant du bloc BUD Forme de Prix.
     */
    private int id;
    
    public static final int FORME_PRIX_UNITAIRE = 1;
    
    public static final int FORME_PRIX_MIXTE = 3;
    
    public static final int FORME_PRIX_FORFAITAIRE = 2;
    
    public static final String FORME_PRIX_FORFAITAIRE_STRING = "PF";

    public static final String FORME_PRIX_MIXTE_STRING = "PM";
    
    public static final String FORME_PRIX_UNITAIRE_STRING = "PU";
    
    
    // Constructeurs
    /**
     *
     */
    public EpmTBudFormePrix() {
        super();
    }


    /**
     * @param identifiant identifiant de base de donnée
     */
    public EpmTBudFormePrix(final int identifiant) {
        super();
        this.id = identifiant;
    }


    // Accesseurs
    /**
     * @return identifiant du bloc BUD Forme de Prix
     */
    public int getId() {
        return id;
    }


    /**
     * @param valeur identifiant du bloc BUD Forme de Prix
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

}
