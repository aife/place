package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeEtablissement de la table "epm__t_ref_type_etablissement"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeEtablissement extends EpmTReferentielExterneAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

}