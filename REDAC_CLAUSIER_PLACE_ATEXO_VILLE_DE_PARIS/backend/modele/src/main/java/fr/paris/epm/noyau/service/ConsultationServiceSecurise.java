package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.metier.objetvaleur.EtapeSimple;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTEtapeCal;
import fr.paris.epm.noyau.persistance.EpmTObject;

import java.util.Collection;
import java.util.List;

/**
 * Interface sécurisée du service de consultation. Ce service est utilisé pour accéder aux consultations.
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
public interface ConsultationServiceSecurise extends GeneriqueServiceSecurise {


    EtapeSimple chargerEtapeDateCal(EpmTEtapeCal etapeCal);


    <T> List<T> chercherParCritere(Critere critere);

    <T extends EpmTObject> Collection<T> modifier(final Collection<T> objets);

    <T extends EpmTObject> T modifier(T epmTObject);

    Collection<EpmTConsultation> chercherConsultations(ConsultationContexteCritere critere);

    EpmTConsultation chercherConsultation(ConsultationContexteCritere critereConstultation);
}

