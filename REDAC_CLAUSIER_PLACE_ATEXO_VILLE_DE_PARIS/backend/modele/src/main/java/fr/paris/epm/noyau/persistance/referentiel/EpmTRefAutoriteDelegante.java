package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefAutoriteDelegante de la table "epm__t_ref_autorite_delegante"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefAutoriteDelegante extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}