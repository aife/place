package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.ouvertureReponseEnregistrementCandidature.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineOuvertureReponseEnregistrementCandidature implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Sous-domaine de candidature (affecte si le document est genere pour une
     * candidature).
     */
    private DomaineCandidature candidature;
    /**
     * Sous-domaine des candidatures (affecte si le document est genere pour une
     * consultation/lot).
     */
    private List<DomaineCandidature> tableauCandidature;
    /**
     * Liste des candidats pour lesquels une demande de complément a été
     * effectuée (avec le commentaire de l'écran suivi des demandes de
     * compléments).
     */
    private List<DomaineCandidature> listeDemandeComplements;

    private Integer NbPlisHorsDelai;

    private List<String> listeCandidatsIrrecevables;

    private List<DomaineCandidature> listeCandidatsNonRetenusRejet;

    /**
     * Liste des candidats admissibles suite à ouverture candidature.
     */
    private List<DomaineCandidature> listeCandidatsAdmissibles;

    /**
     * Si phase ouverte : candidats admissibles. Si phase restreinte : candidats
     * ayant déposé offre (registre des dépôts phase 2).
     */
    private String candidatOffre;

    /**
     * Nombre de plis admis
     */
    private Integer nbPlisAdmis;
    /**
     * Liste des candidats dont la candidature est incomplète (Champ Complétude
     * "Incomplet" dans l'écran Ouverture des candidatures) avec le commentaire
     * de l'écran Ouverture des candidatures).
     */
    private List<DomaineCandidature> listeCandidaturesIncompletes;
    
    /**
     * Liste des candidature (epmtcandidature) de la consultation.
     */
    private List<DomaineCandidature> listeCandidatureDepuisDepot;
    
    /**
     * Liste des candidatures par lot.
     */
    private Map<String, List<DomaineCandidature>> candidatureParLot;

    /**
     * @return Sous-domaine de candidature (affecte si le document est genere
     *         pour une candidature).
     */
    public final DomaineCandidature getCandidature() {
        return candidature;
    }

    /**
     * @param valeur Sous-domaine de candidature (affecte si le document est
     *            genere pour une candidature).
     */
    public final void setCandidature(final DomaineCandidature valeur) {
        this.candidature = valeur;
    }

    /**
     * @return Sous-domaine des candidatures (affecte si le document est genere
     *         pour une consultation/lot).
     */
    public final List<DomaineCandidature> getTableauCandidature() {
        return tableauCandidature;
    }

    /**
     * @param valeur Sous-domaine des candidatures (affecte si le document est
     *            genere pour une consultation/lot).
     */
    public final void setTableauCandidature(
            final List<DomaineCandidature> valeur) {
        this.tableauCandidature = valeur;
    }

    public final Integer getNbPlisHorsDelai() {
        return NbPlisHorsDelai;
    }

    public final void setNbPlisHorsDelai(final Integer valeur) {
        NbPlisHorsDelai = valeur;
    }

    /**
     * @return Liste des candidats pour lesquels une demande de complément a été
     *         effectuée (avec le commentaire de l'écran suivi des demandes de
     *         compléments).
     */
    public final List<DomaineCandidature> getListeDemandeComplements() {
        return listeDemandeComplements;
    }

    /**
     * @param listeDemandeCompléments Liste des candidats pour lesquels une
     *            demande de complément a été effectuée (avec le commentaire de
     *            l'écran suivi des demandes de compléments).
     */
    public final void setListeDemandeComplements(
            final List<DomaineCandidature> valeur) {
        listeDemandeComplements = valeur;
    }

    public final List<String> getListeCandidatsIrrecevables() {
        return listeCandidatsIrrecevables;
    }

    public final void setListeCandidatsIrrecevables(final List<String> valeur) {
        this.listeCandidatsIrrecevables = valeur;
    }

    public final List<DomaineCandidature> getListeCandidatsNonRetenusRejet() {
        return listeCandidatsNonRetenusRejet;
    }

    public final void setListeCandidatsNonRetenusRejet(
            final List<DomaineCandidature> valeur) {
        this.listeCandidatsNonRetenusRejet = valeur;
    }

    /**
     * @return Si phase ouverte : candidats admissibles. Si phase restreinte :
     *         candidats ayant déposé offre (registre des dépôts phase 2).
     */
    public final String getCandidatOffre() {
        return candidatOffre;
    }

    /**
     * @param valeur Si phase ouverte : candidats admissibles. Si phase
     *            restreinte : candidats ayant déposé offre (registre des dépôts
     *            phase 2).
     */
    public final void setCandidatOffre(final String valeur) {
        this.candidatOffre = valeur;
    }

    /**
     * @return Liste des candidats admissibles suite à ouverture candidature.
     */
    public final List<DomaineCandidature> getListeCandidatsAdmissibles() {
        return listeCandidatsAdmissibles;
    }

    /**
     * @param valeur Liste des candidats admissibles suite à ouverture
     *            candidature.
     */
    public final void setListeCandidatsAdmissibles(
            List<DomaineCandidature> valeur) {
        listeCandidatsAdmissibles = valeur;
    }

    public final Integer getNbPlisAdmis() {
        return nbPlisAdmis;
    }

    public final void setNbPlisAdmis(final Integer valeur) {
        this.nbPlisAdmis = valeur;
    }

    public final List<DomaineCandidature> getListeCandidaturesIncompletes() {
        return listeCandidaturesIncompletes;
    }

    public final void setListeCandidaturesIncompletes(
            final List<DomaineCandidature> valeur) {
        this.listeCandidaturesIncompletes = valeur;
    }

	/**
	 * @return Liste des candidature (epmtcandidature) de la consultation.
	 */
	public final List<DomaineCandidature> getListeCandidatureDepuisDepot() {
		return listeCandidatureDepuisDepot;
	}

	/**
	 * @param valeur Liste des candidature (epmtcandidature) de la consultation.
	 */
	public final void setListeCandidatureDepuisDepot(
			final List<DomaineCandidature> valeur) {
		this.listeCandidatureDepuisDepot = valeur;
	}

	/**
	 * @return Liste des candidatures par lot.
	 */
	public final Map<String, List<DomaineCandidature>> getCandidatureParLot() {
		return candidatureParLot;
	}

	/**
	 * @param valeur
	 *            Liste des candidatures par lot.
	 */
	public final void setCandidatureParLot(
			final Map<String, List<DomaineCandidature>> valeur) {
		this.candidatureParLot = valeur;
	}
}
