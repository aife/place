package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefCompletudeEnveloppe de la table "epm__t_ref_completude_enveloppe"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCompletudeEnveloppe extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int COMPLET = 1; // Enveloppe complète.
    public static final int INCOMPLET = 2; // Enveloppe incomplète.
    public static final int IRRECEVABLE = 3; // Enveloppe irrecevable.
    public static final int LOT_NON_PRESENTE = 6;

}