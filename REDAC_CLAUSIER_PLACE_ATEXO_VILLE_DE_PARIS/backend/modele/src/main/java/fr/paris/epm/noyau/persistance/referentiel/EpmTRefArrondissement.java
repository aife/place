package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefArrondissement de la table "epm__t_ref_arrondissement"
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefArrondissement extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}