package fr.paris.epm.noyau.persistance.redaction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate de EpmTClauseActif.
 */
@Entity
@Table(name = "epm__t_clause_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_clause", "id_publication"}))
//@Audited
public class EpmTClausePub extends EpmTClauseAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_clause", nullable = false)
    private Integer idClause;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_clause_has_ref_type_contrat_pub",
            joinColumns = {
                    @JoinColumn(name = "id_clause", referencedColumnName = "id_clause"),
                    @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_type_contrat", referencedColumnName = "id")
            })
    private Set<EpmTRefTypeContrat> epmTRefTypeContrats;

    /**
     * la liste des roles clauses.
     */
    @OneToMany(targetEntity = EpmTRoleClausePub.class,
            mappedBy = "epmTClause", fetch = FetchType.EAGER, orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
    private Set<EpmTRoleClausePub> epmTRoleClauses = new HashSet<>();

    @OneToMany(targetEntity = EpmTClausePotentiellementConditionneePub.class,
            mappedBy = "epmTClause", fetch = FetchType.EAGER, orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
    private Set<EpmTClausePotentiellementConditionneePub> epmTClausePotentiellementConditionnees;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdClause() {
        return idClause;
    }

    public void setIdClause(Integer idClause) {
        this.idClause = idClause;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    @Override
    public Integer getIdLastPublication() {
        return idPublication;
    }

    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        return epmTRefTypeContrats;
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        this.epmTRefTypeContrats = epmTRefTypeContrats;
    }

    @Override
    public Set<EpmTRoleClausePub> getEpmTRoleClauses() {
        return epmTRoleClauses;
    }

    @Override
    public void setEpmTRoleClauses(Set<? extends EpmTRoleClauseAbstract> epmTRoleClauses) {
        this.epmTRoleClauses = (Set<EpmTRoleClausePub>) epmTRoleClauses;
    }

    @Override
    public Set<EpmTClausePotentiellementConditionneePub> getEpmTClausePotentiellementConditionnees() {
        return epmTClausePotentiellementConditionnees;
    }

    @Override
    public void setEpmTClausePotentiellementConditionnees(Set<? extends EpmTClausePotentiellementConditionneeAbstract> epmTClausePotentiellementConditionnees) {
        this.epmTClausePotentiellementConditionnees = (Set<EpmTClausePotentiellementConditionneePub>) epmTClausePotentiellementConditionnees;
    }

    @Override
    public EpmTClausePub clone() throws CloneNotSupportedException {
        EpmTClausePub epmTClause = (EpmTClausePub) super.clone();
        epmTClause.id = 0;

        if (epmTRoleClauses != null) {
            epmTClause.epmTRoleClauses = new HashSet<EpmTRoleClausePub>();
            for (EpmTRoleClausePub item : getEpmTRoleClauses()) {
                EpmTRoleClausePub epmTRoleClausesClone = item.clone();
                epmTRoleClausesClone.setEpmTClause(epmTClause);
                epmTClause.epmTRoleClauses.add(epmTRoleClausesClone);
            }
        }

        if (epmTClausePotentiellementConditionnees != null) {
            epmTClause.epmTClausePotentiellementConditionnees = new HashSet<>();
            for (EpmTClausePotentiellementConditionneePub item : getEpmTClausePotentiellementConditionnees()) {
                EpmTClausePotentiellementConditionneePub cpcClone = item.clone();
                cpcClone.setEpmTClause(epmTClause);
                epmTClause.epmTClausePotentiellementConditionnees.add(cpcClone);
            }
        }

        return epmTClause;
    }

}
