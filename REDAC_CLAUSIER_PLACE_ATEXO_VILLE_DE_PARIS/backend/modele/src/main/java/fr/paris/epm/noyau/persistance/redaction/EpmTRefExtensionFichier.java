package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Referention des extensions pour la génération de documents
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_extension_fichier", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefExtensionFichier extends BaseEpmTRefRedaction {

    public static final int ID_EXTENSION_ODT = 1;
    public static final int ID_EXTENSION_PDF = 2;

    /**
     * Extension du fichier à générer.
     */
    @Column(name="extension_fichier")
    private String extensionFichier;

    /**
     * @return the extension
     */
    public String getExtensionFichier() {
        return extensionFichier;
    }

    /**
     * @param valeur the extension to set
     */
    public void setExtensionFichier(final String valeur) {
        this.extensionFichier = valeur;
    }

}
