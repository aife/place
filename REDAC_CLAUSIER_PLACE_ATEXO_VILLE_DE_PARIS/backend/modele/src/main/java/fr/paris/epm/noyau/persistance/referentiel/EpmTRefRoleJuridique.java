package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefRoleJuridique de la table "EpmTRefRoleJuridique"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefRoleJuridique extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

    public static final int TITULAIRE = 1;
    public static final int CO_TRAITANT = 3;
    public static final int SOUS_TRAITANT = 4;

}