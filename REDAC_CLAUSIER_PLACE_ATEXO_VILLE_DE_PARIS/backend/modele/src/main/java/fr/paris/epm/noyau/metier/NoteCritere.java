package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.EpmTBudLot;

import java.io.Serializable;

public class NoteCritere extends AbstractCritere implements Critere,
        Serializable {
    
    
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 2253249494507720553L;

    /**
     * Identifiant de la consultation.
     */
    private int idConsultation;

    /**
     * identifiant de l'avenant
     */
    private int idAvenant;

    /**
     * identifiant du lot {@link EpmTBudLot}
     */
    private int idBudLot;
    
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        if (idConsultation != 0 && idBudLot == 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" note.idConsultation = ");
            sb.append(idConsultation);
            sb.append(" and ");
            sb.append(" note.idBudLot = 0 ");
            debut = true;
        }

        if (idAvenant != 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" note.idAvenant = ");
            sb.append(idAvenant);
            debut = true;
        }
        
        if (idBudLot != 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" note.idBudLot = ");
            sb.append(idBudLot);
            debut = true;
        }
        return sb;
    }

    /**
     * Non implementé
     */
    public String toCountHQL() {
        // TODO Auto-generated method stub
        return null;
    }

    public String toHQL() {
        final String req = "from EpmTNote as note " + corpsRequete();
        return req;
    }

    public final void setIdConsultation(final int valeur) {
        this.idConsultation = valeur;
    }

    public final void setIdAvenant(final int valeur) {
        this.idAvenant = valeur;
    }

    /**
     * @param valeur identifiant du lot {@link EpmTBudLot}
     */
    public final void setIdBudLot(final int valeur) {
        this.idBudLot = valeur;
    }

}
