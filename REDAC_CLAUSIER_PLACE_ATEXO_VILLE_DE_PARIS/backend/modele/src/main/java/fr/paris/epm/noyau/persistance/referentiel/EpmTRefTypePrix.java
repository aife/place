package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypePrix de la table "epm__t_ref_type_prix"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypePrix extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int PRIX_CATALOGUE = 1;
    public static final int BORDEREAU = 2;
    public static final int AUTRE = 3;

}