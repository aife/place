package fr.paris.epm.noyau.persistance.redaction;


import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Set;

@MappedSuperclass
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public abstract class EpmTClausePotentiellementConditionneeAbstract extends EpmTAbstractObject implements Cloneable, Serializable {

    /**
     * l'objet EpmTRefPotentiellementConditionnee.
     */
    @ManyToOne
    @JoinColumn(name = "id_ref_potentiellement_conditionnee")
    private EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee;

    public abstract Integer getIdClausePotentiellementConditionnee();

    public abstract Integer getIdPublication();

    public abstract EpmTClauseAbstract getEpmTClause() ;

    public abstract void setEpmTClause(EpmTClauseAbstract epmTClause) ;

    public EpmTRefPotentiellementConditionnee getEpmTRefPotentiellementConditionnee() {
        return epmTRefPotentiellementConditionnee;
    }

    public void setEpmTRefPotentiellementConditionnee(EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee) {
        this.epmTRefPotentiellementConditionnee = epmTRefPotentiellementConditionnee;
    }

    public abstract Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> getEpmTClauseValeurPotentiellementConditionnees() ;

    public abstract void setEpmTClauseValeurPotentiellementConditionnees(Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotentiellementConditionnees) ;

    @Override
    public EpmTClausePotentiellementConditionneeAbstract clone() throws CloneNotSupportedException {
        EpmTClausePotentiellementConditionneeAbstract cpc = (EpmTClausePotentiellementConditionneeAbstract) super.clone();
        cpc.epmTRefPotentiellementConditionnee = epmTRefPotentiellementConditionnee;
        return cpc;
    }

}
