package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutAttribution de la table "epm__t_ref_statut_attribution"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutAttribution extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_CONFIRMEE = 1; // Attribution confirmee
    public static final int ID_CHANGEMENT_DEMANDE = 2; // Demande de changement d'attributaire demandé.

}