package fr.paris.epm.noyau.persistance;

/**
 * Pojo Hibernate gérant les propriétés de la consultation non définie dans RSEM
 * @author MGA
 *
 */
public class EpmTValeurConditionnementExterne extends EpmTAbstractObject {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;


    /**
     * Clef de la valeur de conditionnement externe
     */
    private String clef;
    /**
     * La valeur de conditionnement externe
     */
    private String valeur;


    /**
     * @return the clef
     */
    public String getClef() {
        return clef;
    }
    /**
     * @param clef the clef to set
     */
    public void setClef(final String valeur) {
        this.clef = valeur;
    }
    /**
     * @return the valeur
     */
    public String getValeur() {
        return valeur;
    }
    /**
     * @param valeur the valeur to set
     */
    public void setValeur(final String valeur) {
        this.valeur = valeur;
    }

}
