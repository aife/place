package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.*;

import java.util.Date;
import java.util.Set;

/**
 * POJO hibernate - Contrat d'une consultation / lot issue d'un
 * marché/acccord-cadre attribué de la phase de passation
 * @author Rebeca Dantas
 */
public class EpmTContrat extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808544L;

    private Integer id;

    /**
     * Numéro du contrat
     */
    private String numeroContrat;

    /**
     * Numero de consultation initial en cas d'accord cadre/marché subséquent
     */
    private String numeroConsultationInitial;

    /**
     * Objet du contrat
     */
    private String objetContrat;

    /**
     * Referentiel exécution : Type de contrat
     */
    private EpmTRefTypeObjetContrat typeContrat;

    /**
     * Numéro de la consultation
     */
    private String numeroConsultation;

    /**
     * Objet de la consultation
     */
    private String objetConsultation;
    
    /**
     * L'intitulé de la consultation
     */
    private String intituleConsultation;
        
    /**
     * Le numéro du lot
     */
    private String numeroLot;
    
    /**
     * L'intitulé du lot
     */
    private String intituleLot;

    /**
     * Procédure de passation de la consultation
     */
    private EpmTRefProcedure procedureConsultation;

    /**
     * Le pouvoir adjudicateur associé : écran il s'appele contractant public
     */
    private EpmTRefPouvoirAdjudicateur pouvoirAdjudicateur;

    /**
     * La direction service responsable du contrat (alimenté par passation)
     */
    private EpmTRefDirectionService directionServiceResponsable;
    
    /**
     * La nature de prestation : alimenté par la consultation si marché non
     * alloti. Si marché alloti, ce champ est alimenté par la nature du lot.
     */
    private EpmTRefNature naturePrestation;

    /**
     * Referentiel exécution : type d'attribution (mono ou multi attributaire),
     * champ optionnel
     */
    private EpmTRefTypeAttribution typeAttribution;

    // Données écran : Informations principales du contrat
    /**
     * Le code externe de {EpmTRefChoixMoisJour} (alimenté par passation -
     * hérité du formulaire amont de la consultation)
     */
    private EpmTRefChoixMoisJour choixMoisJourAnnee;

    /**
     * Valeur de la durée du marché (alimenté par passation - hérité du
     * formulaire amont de la consultation)
     */
    private Integer valeurDureeMarche;

    /**
     * Nombre de reconduction (alimenté par passation - hérité du formulaire
     * amont de la consultation)
     */
    private Integer nombreReconduction;

    /**
     * Modalité de reconduction (alimenté par passation - hérité du formulaire
     * amont de la consultation)
     */
    private String modaliteReconduction;

    /**
     * Commantaire optionnel (alimenté par passation - hérité du formulaire
     * amont de la consultation)
     */
    private String options;

    /**
     * Tranches du module exécution dont les valeurs sont hérités du objet
     * {EpmTBudTranche} du formulaire amont de la consultation
     */
    private Set<EpmTBudTranche> tranches;

    /**
     * Forme de prix du contrat
     */
    private EpmTBudFormePrix formePrix;

    /**
     * Lots techniques du module éxécution dont les valeurs sont hérités du
     * objet {EpmTLotTechnique} du formulaire amont de la consultation
     */
    private Set<EpmTLotTechnique> lotsTechnique;

    /**
     * Mode de gestion possibles pour le contrat : décompte ou facture
     */
    private EpmTRefModeGestionContrat modeDeGestion;
    
    /**
     * Le montant attribué en EUR HT
     */
    private Double montantAttribueHT;
    /**
     * Montants PF DQE consolidés HT
     */
    private Double pfDQEConsolidesHT;
    
    /**
     * Montants PF DQE consolidés TTC
     */
    private Double pfDQEConsolidesTTC;
    
    /**
     * BC Max consolidés HT
     */
    private Double bcMaxConsolidesHT;
    
    /**
     * BC Max consolidés TTC
     */
    private Double bcMaxConsolidesTTC;
    
    /**
     * BC Min consolidés HT
     */
    private Double bcMinConsolidesHT;
    
    /**
     * BC Min consolidés TTC
     */
    private Double bcMinConsolidesTTC;

    private Set<EpmTDocument> documents;

    
    private Date dateEnvoiCourrierNonRetenu;
    
    private String numeroEngagementComptable;
    
    private String commentaireMiseAuPoint;
    
    private Date dateMiseAuPoint;
    
    private Date dateEngagementComptable;
    
    private boolean miseAuPoint;
    
    private Set<EpmTAttributaire> attributaires;
    
    /**
     * Indentifiant de l'organisme associé au contrat
     */
    private Integer idOrganisme;
    
    private EpmTDocument documentMiseAuPointDuContrat;
    
    private String nomSignataire;
    
    private Date dateSignature;
    
    private boolean saisieComplet;
    
    /**
     * Liste de code CPV lié à la consultation.
     */
    private Set<EpmTCpv> cpvs;
    
    /**
     * "true" si un contrat comporte une clause sociale. C'est à dire, l’écran
     * Données principales de la consultation est validé avec le champ Clauses
     * sociales coché.
     */
    private boolean clausesSociales;

    /**
     * "true" si un contrat comporte une clause environnementale. C'est à dire, l’écran
     * Données principales de la consultation est validé avec le champ Clauses
     * environnementale coché.
     */
    private boolean clausesEnvironnementales;

    
    private Integer nombreTotalProposition;
    
    
    private Integer nombrePropositionDemat;
    
    /**
     * id de la Responsable du contrat
     */
    private Integer idUtilisateur;
    
    /**
     * Date de validation de l'attribution.
     */
    private Date dateValidation;
    
    /**
     * Ils sont les 3 caractères qui fait une partie du numéro contrat d'un
     * accord-cadre, incrémenté à chaque nouvelle attribution d'un accord-cadre
     * et qui sera repris dans le numéro du marché subséquent. 
     * Par exemple, 2011 1 09 <b>185</b> 0000
     */
    private String numeroChronoAccordCadre;

    /**
     * Numero de l'avenant qui provient ce contrat
     */
    private Integer idAvenantLie;
    
    /**
     * Si l'application permet de transmettre au contrôle de la légalité, cette
     * propriété stocke des informations sur l'enregistrement de
     * télétransmission. 
     */
    private EpmTEnregistrementControleLegalite enregistrementControleLegalite;
    
    /**
     * marquer si tous les attributaires de ce contrat sont déjà notifiés. Cette propriété est
     * passée à 'true' lorsque l'écran Notification est validé. 
     */
    private boolean contratNotifie;
    
    /**
     * Referentiel contenant les tranches concernant le budget du contrat
     */
    private EpmTRefTrancheBudgetaire trancheBudgetaire;
    
    /**
     * Date de validation de l'attribution.
     */
    private Date dateFinMarche;

    /**
     * @return l'identifiant du contrat
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param valeur l'identifiant du contrat
     */
    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    /**
     * @return numeroContrat : retourne le numéro du contrat
     */
    public String getNumeroContrat() {
        return numeroContrat;
    }

    /**
     * @param valeur : défine le numéro du contrat
     */
    public void setNumeroContrat(final String valeur) {
        this.numeroContrat = valeur;
    }

    /**
     * @return objetContrat: retourne l'objet du contrat
     */
    public String getObjetContrat() {
        return objetContrat;
    }

    /**
     * @param valeur : défine l'objet du contrat
     */
    public void setObjetContrat(final String valeur) {
        this.objetContrat = valeur;
    }

    /**
     * @return typeContrat : retourne le type de contrat (appartient au
     *         referentiel du module exécution, mais cet objet du referentiel
     *         est alimenté par passation)
     */
    public EpmTRefTypeObjetContrat getTypeContrat() {
        return typeContrat;
    }

    /**
     * @param valeur : define le type de contrat (appartient au referentiel du
     *            module exécution, mais cet objet du referentiel est alimenté
     *            par passation)
     */
    public void setTypeContrat(final EpmTRefTypeObjetContrat valeur) {
        this.typeContrat = valeur;
    }

    /**
     * @return numeroConsultation : retourne le numéro de consultation
     */
    public String getNumeroConsultation() {
        return numeroConsultation;
    }

    /**
     * @param valeur : défine le numéro de consultation
     */
    public void setNumeroConsultation(final String valeur) {
        this.numeroConsultation = valeur;
    }

    /**
     * @return objetConsultation : retour l'objet de la consultation
     */
    public String getObjetConsultation() {
        return objetConsultation;
    }

    /**
     * @param valeur : défine l'objet de la consultation
     */
    public void setObjetConsultation(final String valeur) {
        this.objetConsultation = valeur;
    }

    /**
     * @return procedureConsultation : retourne la procédure
     *         de passation de la consultation
     */
    public EpmTRefProcedure getProcedureConsultation() {
        return procedureConsultation;
    }

    /**
     * @param valeur : défine l'identifiant de la procédure de passation de la
     *            consultation
     */
    public void setProcedureConsultation(final EpmTRefProcedure valeur) {
        this.procedureConsultation = valeur;
    }

    /**
     * @return idPouvoirAdjudicateur : retourne l'identifiant du pouvoir
     *         adjudicateur associé
     */
    public EpmTRefPouvoirAdjudicateur getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur : défine l'identifiant du pouvoir adjudicateur associé
     */
    public void setPouvoirAdjudicateur(final EpmTRefPouvoirAdjudicateur valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    /**
     * @return idDirectionServiceResponsable : retourne l'identifiant de la
     *         direction service responsable du contrat
     */
    public EpmTRefDirectionService getDirectionServiceResponsable() {
        return directionServiceResponsable;
    }

    /**
     * @param valeur : défine l'identifiant de la direction service responsable
     *            du contrat
     */
    public void setDirectionServiceResponsable(final EpmTRefDirectionService valeur) {
        this.directionServiceResponsable = valeur;
    }

    /**
     * @return typeAttribution : retourne le type d'attribution (mono ou multi
     *         attributaire) champ optionnel provenant du referentiel du module
     *         exécution
     */
    public EpmTRefTypeAttribution getTypeAttribution() {
        return typeAttribution;
    }

    /**
     * @param valeur : define le type d'attribution (mono ou multi attributaire)
     *            champ optionnel provenant du referentiel du module exécution
     */
    public void setTypeAttribution(final EpmTRefTypeAttribution valeur) {
        this.typeAttribution = valeur;
    }

    /**
     * @return uniteDureeDuMarche : retourne le code externe de
     *         {EpmTRefChoixMoisJour} (alimenté par passation - hérité du
     *         formulaire amont de la consultation)
     */
    public EpmTRefChoixMoisJour getChoixMoisJourAnnee() {
        return choixMoisJourAnnee;
    }

    /**
     * @param valeur : défine le code externe de {EpmTRefChoixMoisJour}
     *            (alimenté par passation - hérité du formulaire amont de la
     *            consultation)
     */
    public void setChoixMoisJourAnnee(final EpmTRefChoixMoisJour valeur) {
        this.choixMoisJourAnnee = valeur;
    }

    /**
     * @return valeurDureeMarche : retourne la valeur de la durée du marché
     *         (alimenté par passation - hérité du formulaire amont de la
     *         consultation)
     */
    public Integer getValeurDureeMarche() {
        return valeurDureeMarche;
    }

    /**
     * @param valeur : défine la valeur de la durée du marché (alimenté par
     *            passation - hérité du formulaire amont de la consultation)
     */
    public void setValeurDureeMarche(final Integer valeur) {
        this.valeurDureeMarche = valeur;
    }

    /**
     * @return nombreReconduction : retourne le nombre de reconduction (alimenté
     *         par passation - hérité du formulaire amont de la consultation)
     */
    public Integer getNombreReconduction() {
        return nombreReconduction;
    }

    /**
     * @param valeur : défine le nombre de reconduction (alimenté par passation
     *            - hérité du formulaire amont de la consultation)
     */
    public void setNombreReconduction(final Integer valeur) {
        this.nombreReconduction = valeur;
    }

    /**
     * @return modaliteReconduction : retourne la modalité de reconduction
     *         (alimenté par passation - hérité du formulaire amont de la
     *         consultation)
     */
    public String getModaliteReconduction() {
        return modaliteReconduction;
    }

    /**
     * @param valeur : défine la modalité de reconduction (alimenté par
     *            passation - hérité du formulaire amont de la consultation)
     */
    public void setModaliteReconduction(final String valeur) {
        this.modaliteReconduction = valeur;
    }

    /**
     * @return options : retourne l'option (alimenté par passation - hérité du
     *         formulaire amont de la consultation)
     */
    public String getOptions() {
        return options;
    }

    /**
     * @param valeur : défine l'option (alimenté par passation - hérité du
     *            formulaire amont de la consultation)
     */
    public void setOptions(final String valeur) {
        this.options = valeur;
    }

    /**
     * @return tranches : retourne l'ensemble de tranches du module exécution
     *         (hérités du objet {EpmTBudTranche} du formulaire amont de la
     *         consultation)
     */
    public Set<EpmTBudTranche> getTranches() {
        return tranches;
    }

    /**
     * @param valeur : défine l'ensemble de tranches du module exécution
     *            (hérités du objet {EpmTBudTranche} du formulaire amont de la
     *            consultation)
     */
    public void setTranches(final Set<EpmTBudTranche> valeur) {
        this.tranches = valeur;
    }

    /**
     * @return la forme de prix du contrat
     */
    public EpmTBudFormePrix getFormePrix() {
        return formePrix;
    }

    /**
     * @param valeur : défine la forme de prix du contrat
     */
    public void setFormePrix(final EpmTBudFormePrix valeur) {
        this.formePrix = valeur;
    }

    /**
     * @return lotsTechnique : retourne l'ensemble de lots techniques du module
     *         éxécution (hérités du objet {EpmTLotTechnique} du formulaire
     *         amont de la consultation)
     */
    public Set<EpmTLotTechnique> getLotsTechnique() {
        return lotsTechnique;
    }

    /**
     * @param valeur : défine l'ensemble de lots techniques du module éxécution
     *            (hérités du objet {EpmTLotTechnique} du formulaire amont de la
     *            consultation)
     */
    public void setLotsTechnique(final Set<EpmTLotTechnique> valeur) {
        this.lotsTechnique = valeur;
    }

    /**
     * @return modeDeGestion : retourne le mode de gestion pour le contrat
     *         (décompte ou facture)
     */
    public EpmTRefModeGestionContrat getModeDeGestion() {
        return modeDeGestion;
    }

    /**
     * @param valeur : défine un mode de gestion pour le contrat (décompte ou
     *            facture)
     */
    public void setModeDeGestion(final EpmTRefModeGestionContrat valeur) {
        this.modeDeGestion = valeur;
    }

    public String getNumeroConsultationInitial() {
        return numeroConsultationInitial;
    }

    public void setNumeroConsultationInitial(final String valeur) {
        this.numeroConsultationInitial = valeur;
    }

    /**
     * @return the documents
     */
    public Set<EpmTDocument> getDocuments() {
        return documents;
    }

    /**
     * @param valeur the documents to set
     */
    public void setDocuments(final Set<EpmTDocument> valeur) {
        this.documents = valeur;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

    /**
     * @return the intituleConsultation
     */
    public String getIntituleConsultation() {
        return intituleConsultation;
    }

    /**
     * @param intituleConsultation the intituleConsultation to set
     */
    public void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }

    /**
     * @return the numeroLot
     */
    public String getNumeroLot() {
        return numeroLot;
    }

    /**
     * @param numeroLot the numeroLot to set
     */
    public void setNumeroLot(final String valeur) {
        this.numeroLot = valeur;
    }

    /**
     * @return the intituleLot
     */
    public String getIntituleLot() {
        return intituleLot;
    }

    /**
     * @param intituleLot the intituleLot to set
     */
    public void setIntituleLot(final String valeur) {
        this.intituleLot = valeur;
    }



    /**
     * @return the dateEnvoiCourrierNonRetenu
     */
    public Date getDateEnvoiCourrierNonRetenu() {
        return dateEnvoiCourrierNonRetenu;
    }

    /**
     * @param dateEnvoiCourrierNonRetenu the dateEnvoiCourrierNonRetenu to set
     */
    public void setDateEnvoiCourrierNonRetenu(final Date valeur) {
        this.dateEnvoiCourrierNonRetenu = valeur;
    }

    /**
     * @return the numeroEngagementComptable
     */
    public String getNumeroEngagementComptable() {
        return numeroEngagementComptable;
    }

    /**
     * @param numeroEngagementComptable the numeroEngagementComptable to set
     */
    public void setNumeroEngagementComptable(final String valeur) {
        this.numeroEngagementComptable = valeur;
    }


    /**
     * @return the commentaire
     */
    public String getCommentaireMiseAuPoint() {
        return commentaireMiseAuPoint;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaireMiseAuPoint(final String valeur) {
        this.commentaireMiseAuPoint = valeur;
    }

    /**
     * @return the dateMiseAuPoint
     */
    public Date getDateMiseAuPoint() {
        return dateMiseAuPoint;
    }

    /**
     * @param dateMiseAuPoint the dateMiseAuPoint to set
     */
    public void setDateMiseAuPoint(final Date valeur) {
        this.dateMiseAuPoint = valeur;
    }

    /**
     * @return the dateEngagementComptable
     */
    public Date getDateEngagementComptable() {
        return dateEngagementComptable;
    }

    /**
     * @param dateEngagementComptable the dateEngagementComptable to set
     */
    public void setDateEngagementComptable(final Date valeur) {
        this.dateEngagementComptable = valeur;
    }

    /**
     * @return the miseAuPoint
     */
    public boolean isMiseAuPoint() {
        return miseAuPoint;
    }

    /**
     * @param miseAuPoint the miseAuPoint to set
     */
    public void setMiseAuPoint(final boolean valeur) {
        this.miseAuPoint = valeur;
    }



    /**
     * @return the attributaires
     */
    public Set<EpmTAttributaire> getAttributaires() {
        return attributaires;
    }

    /**
     * @param attributaires the attributaires to set
     */
    public void setAttributaires(final Set<EpmTAttributaire> valeur) {
        this.attributaires = valeur;
    }


    /**
     * @return the documentMiseAuPointDuContrat
     */
    public EpmTDocument getDocumentMiseAuPointDuContrat() {
        return documentMiseAuPointDuContrat;
    }

    /**
     * @return the nomSignature
     */
    public String getNomSignataire() {
        return nomSignataire;
    }

    /**
     * @return the dateSignature
     */
    public Date getDateSignature() {
        return dateSignature;
    }

    /**
     * @return the saisieComplet
     */
    public boolean isSaisieComplet() {
        return saisieComplet;
    }

    /**
     * @param documentMiseAuPointDuContrat the documentMiseAuPointDuContrat to set
     */
    public void setDocumentMiseAuPointDuContrat(final EpmTDocument valeur) {
        this.documentMiseAuPointDuContrat = valeur;
    }

    /**
     * @param nomSignature the nomSignature to set
     */
    public void setNomSignataire(final String valeur) {
        this.nomSignataire = valeur;
    }

    /**
     * @param dateSignature the dateSignature to set
     */
    public void setDateSignature(final Date valeur) {
        this.dateSignature = valeur;
    }

    /**
     * @param saisieComplet the saisieComplet to set
     */
    public void setSaisieComplet(final boolean valeur) {
        this.saisieComplet = valeur;
    }
    
    /**
     * @return montantAttribueHT : retourne le montant attribué en EUR HT
     */
    public Double getMontantAttribueHT() {
        return montantAttribueHT;
    }

    /**
     * @param valeur : défine le montant attribué en EUR HT
     */
    public void setMontantAttribueHT(final Double valeur) {
        this.montantAttribueHT = valeur;
    }
    
    /**
     * @return the pfDQEConsolidesHT
     */
    public Double getPfDQEConsolidesHT() {
        return pfDQEConsolidesHT;
    }

    /**
     * @param pfDQEConsolidesHT the pfDQEConsolidesHT to set
     */
    public void setPfDQEConsolidesHT(final Double valeur) {
        this.pfDQEConsolidesHT = valeur;
    }

    /**
     * @return the pfDQEConsolidesTTC
     */
    public Double getPfDQEConsolidesTTC() {
        return pfDQEConsolidesTTC;
    }

    /**
     * @param pfDQEConsolidesTTC the pfDQEConsolidesTTC to set
     */
    public void setPfDQEConsolidesTTC(final Double valeur) {
        this.pfDQEConsolidesTTC = valeur;
    }

    /**
     * @return the bcMaxConsolidesHT
     */
    public Double getBcMaxConsolidesHT() {
        return bcMaxConsolidesHT;
    }

    /**
     * @param bcMaxConsolidesHT the bcMaxConsolidesHT to set
     */
    public void setBcMaxConsolidesHT(final Double valeur) {
        this.bcMaxConsolidesHT = valeur;
    }

    /**
     * @return the bcMaxConsolidesTTC
     */
    public Double getBcMaxConsolidesTTC() {
        return bcMaxConsolidesTTC;
    }

    /**
     * @param bcMaxConsolidesTTC the bcMaxConsolidesTTC to set
     */
    public void setBcMaxConsolidesTTC(final Double valeur) {
        this.bcMaxConsolidesTTC = valeur;
    }

    /**
     * @return the bcMinConsolidesHT
     */
    public Double getBcMinConsolidesHT() {
        return bcMinConsolidesHT;
    }

    /**
     * @param bcMinConsolidesHT the bcMinConsolidesHT to set
     */
    public void setBcMinConsolidesHT(final Double valeur) {
        this.bcMinConsolidesHT = valeur;
    }

    /**
     * @return the bcMinConsolidesTTC
     */
    public Double getBcMinConsolidesTTC() {
        return bcMinConsolidesTTC;
    }

    /**
     * @param bcMinConsolidesTTC the bcMinConsolidesTTC to set
     */
    public void setBcMinConsolidesTTC(final Double valeur) {
        this.bcMinConsolidesTTC = valeur;
    }

    /**
     * @return the cpvs
     */
    public Set<EpmTCpv> getCpvs() {
        return cpvs;
    }

    /**
     * @return the clausesSociales
     */
    public boolean isClausesSociales() {
        return clausesSociales;
    }

    /**
     * @return the clausesEnvironnementales
     */
    public boolean isClausesEnvironnementales() {
        return clausesEnvironnementales;
    }

    /**
     * @return the nombreTotalProposition
     */
    public Integer getNombreTotalProposition() {
        return nombreTotalProposition;
    }

    /**
     * @return the nombrePropositionDemat
     */
    public Integer getNombrePropositionDemat() {
        return nombrePropositionDemat;
    }

    /**
     * @param cpvs the cpvs to set
     */
    public void setCpvs(final Set<EpmTCpv> valeur) {
        this.cpvs = valeur;
    }

    /**
     * @param clausesSociales the clausesSociales to set
     */
    public void setClausesSociales(final boolean valeur) {
        this.clausesSociales = valeur;
    }

    /**
     * @param clausesEnvironnementales the clausesEnvironnementales to set
     */
    public void setClausesEnvironnementales(final boolean valeur) {
        this.clausesEnvironnementales = valeur;
    }

    /**
     * @param nombreTotalProposition the nombreTotalProposition to set
     */
    public void setNombreTotalProposition(final Integer valeur) {
        this.nombreTotalProposition = valeur;
    }

    /**
     * @param nombrePropositionDemat the nombrePropositionDemat to set
     */
    public void setNombrePropositionDemat(final Integer valeur) {
        this.nombrePropositionDemat = valeur;
    }

    /**
     * @return the idUtilisateur
     */
    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    /**
     * @param idUtilisateur the idUtilisateur to set
     */
    public void setIdUtilisateur(final Integer valeur) {
        this.idUtilisateur = valeur;
    }

    /**
     * @return the dateValidation
     */
    public Date getDateValidation() {
        return dateValidation;
    }

    /**
     * @param dateValidation the dateValidation to set
     */
    public void setDateValidation(final Date valeur) {
        this.dateValidation = valeur;
    }


    public String getNumeroChronoAccordCadre() {
        return numeroChronoAccordCadre;
    }

    public void setNumeroChronoAccordCadre(final String valeur) {
        this.numeroChronoAccordCadre = valeur;
    }

    /**
     * @return the idAvenant
     */
    public Integer getIdAvenantLie() {
        return idAvenantLie;
    }

    /**
     * @param idAvenant the idAvenant to set
     */
    public void setIdAvenantLie(final Integer valeur) {
        this.idAvenantLie = valeur;
    }

    /**
     * @return contratNotifie
     */
    public boolean isContratNotifie() {
        return contratNotifie;
    }

    /**
     * @param contratNotifie contratNotifie to set
     */
    public void setContratNotifie(boolean valeur) {
        this.contratNotifie = valeur;
    }

    /**
     * @return enregistrementControleLegalite
     */
    public EpmTEnregistrementControleLegalite getEnregistrementControleLegalite() {
        return enregistrementControleLegalite;
    }

    /**
     * @param enregistrementControleLegalite enregistrementControleLegalite to
     *            set
     */
    public void setEnregistrementControleLegalite(EpmTEnregistrementControleLegalite valeur) {
        this.enregistrementControleLegalite = valeur;
    }

    /**
     * @return the trancheBudgetaire
     */
    public EpmTRefTrancheBudgetaire getTrancheBudgetaire() {
        return trancheBudgetaire;
    }

    /**
     * @param trancheBudgetaire the trancheBudgetaire to set
     */
    public void setTrancheBudgetaire(final EpmTRefTrancheBudgetaire valeur) {
        this.trancheBudgetaire = valeur;
    }

    /**
     * @return the naturePrestation
     */
    public EpmTRefNature getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @param valeur the naturePrestation to set
     */
    public void setNaturePrestation(final EpmTRefNature valeur) {
        this.naturePrestation = valeur;
    }

    /**
     * @return la date de fin du marché
     */
    public Date getDateFinMarche() {
        return dateFinMarche;
    }

    /**
     * @param la date de fin du marché
     */
    public void setDateFinMarche(final Date valeur) {
        this.dateFinMarche = valeur;
    }
    
}
