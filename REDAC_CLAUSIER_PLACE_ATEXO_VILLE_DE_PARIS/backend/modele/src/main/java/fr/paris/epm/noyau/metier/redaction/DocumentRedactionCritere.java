package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;

import java.io.Serializable;

/**
 * Classe critère utilisée pour la recherche des documents.
 *
 * @author Regis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class DocumentRedactionCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant du depot.
     */
    private int id;

    /**
     * consultation associée au document.
     */
    private int idConsultation;

    /**
     * reference du document.
     */
    private String reference;

    /**
     * True si dernier document crée
     */
    private boolean dernierDocumentCree = true;

    /**
     * @return corps de la requête HQL
     */
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        if (id != 0) {
            sb.append(debut ? " where " : " and ");
            sb.append(" c.id = ").append(id);
            debut = false;
        }

        if (idConsultation != 0) {
            sb.append(debut ? " where " : " and ");
            sb.append(" c.idConsultation = ").append(idConsultation);
            debut = false;
        }

        if (reference != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" lower(c.reference) = '").append(reference.toLowerCase()).append("'");
            debut = false;
        }

        if (dernierDocumentCree) {
            sb.append(debut ? " where " : " and ");
            sb.append(" c.dernierDocumentCree = ").append(dernierDocumentCree);
            debut = false;
        }

        return sb;
    }

    /**
     * @return chaine HQL utilisée par Hibernate
     */
    public final String toHQL() {
        final StringBuilder req = new StringBuilder("from EpmTDocumentRedaction as c ");
        req.append(corpsRequete());
        if (proprieteTriee != null)
            req.append(" order by c.").append(proprieteTriee).append(triCroissant ? " ASC" : " DESC");
        return req.toString();
    }

    // Accesseurs
    public final void setId(final int valeur) {
        id = valeur;
    }

    public final void setReference(final String valeur) {
        reference = valeur;
    }

    public final void setIdConsultation(final int valeur) {
        idConsultation = valeur;
    }

    public final void setDernierDocumentCree(final boolean valeur) {
        this.dernierDocumentCree = valeur;
    }

    @Override
    public String toCountHQL() {
        // TODO Auto-generated method stub
        return null;
    }

}
