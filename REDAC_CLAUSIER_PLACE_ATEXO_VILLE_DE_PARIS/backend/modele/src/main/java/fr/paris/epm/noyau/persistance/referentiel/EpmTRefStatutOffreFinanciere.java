package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutOffreFinanciere de la table "epm__t_ref_statut_offre_financiere"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutOffreFinanciere extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}