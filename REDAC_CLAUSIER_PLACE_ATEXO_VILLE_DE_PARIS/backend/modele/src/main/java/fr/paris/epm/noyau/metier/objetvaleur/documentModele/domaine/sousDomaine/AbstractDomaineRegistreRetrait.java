package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine entreprises.registreRetrait.
 * Herite par DomaineRegistreRetraitDemat et DomaineRegistreRetraitPapier.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractDomaineRegistreRetrait implements Serializable {
	
	/**
	 * Marqueur de sérialisation 
	 */
	protected static final long serialVersionUID = 1L;
	
	/**
	 * Phase du dépot.
	 */
	private String phase;

	/**
	 * @return Phase du dépot.
	 */
	public final String getPhase() {
		return phase;
	}

	/**
	 * @param valeur Phase du dépot.
	 */
	public final void setPhase(final String valeur) {
		this.phase = valeur;
	}
}
