package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * Critére de recherche des {@link EpmTRefOrganisme}. Utilisé dans noyau proxy.
 *
 * @author MGA
 */
public class OrganismeCritere extends AbstractCritere implements Critere, Serializable {

    public OrganismeCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;

    }

    /**
     * Serialisation
     */
    private static final long serialVersionUID = 766343928456812395L;

    /**
     * Identifiant de l'organisme.
     */
    private Integer idOrganisme;
    private final String plateformeUuid;
    private Boolean withoutPlateformeUuid;

    private Collection<String> codesExternes;

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;

        if (idOrganisme != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" organisme.id = :idOrganisme");
            getParametres().put("idOrganisme", idOrganisme);
            debut = true;
        }

        if (plateformeUuid != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("organisme.plateformeUuid = :plateformeUuid");
            getParametres().put("plateformeUuid", plateformeUuid);
            debut = true;
        }
        if (Boolean.TRUE.equals(withoutPlateformeUuid)) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("organisme.plateformeUuid is null");
            debut = true;
        }

        if (codesExternes != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" organisme.codeExterne in (:codesExternes)");
            getParametres().put("codesExternes", codesExternes);
            debut = true;
        }

        return sb;
    }


    public String toCountHQL() {
        return null;
    }

    public String toHQL() {
        final StringBuilder req = new StringBuilder();
        req.append("from EpmTRefOrganisme as organisme ");
        req.append(corpsRequete());
        if (proprieteTriee != null) {
            req.append(" order by organisme.").append(proprieteTriee);
            if (triCroissant) {
                req.append(" ASC ");
            } else {
                req.append(" DESC ");
            }
        }
        return req.toString();
    }

    public final void setIdOrganisme(final Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public final void setCodesExternes(String... codesExternes) {
        this.codesExternes = Arrays.asList(codesExternes);
    }

    public final void setCodesExternes(Collection<String> codesExternes) {
        this.codesExternes = codesExternes;
    }

    public void setWithoutPlateformeUuid(Boolean withoutPlateformeUuid) {
        this.withoutPlateformeUuid = withoutPlateformeUuid;
    }
}
