package fr.paris.epm.noyau.service.technique;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;

/**
 * Ce service gere la sauvegarde et la recuperation des documents auvegardés
 * dans le noyau.
 * @author Mounthei KHAM
 * @version $Revision$, $Date$, $Author$
 */
public interface ServiceTechniqueSecurise extends GeneriqueServiceSecurise {


    String ping();

    UtilisateurExterneConsultation getUtilisateurExterneConsultation(final String identifiantExterne);

    void modifierLogoOrganisme(byte[] fichierLogo, String typeFichierLogo, Integer idOrganisme);

    /**
     * Permet de rechercher le logo en base selon id d'organisme passé en paramétre.
     * @param idOrganisme organisme auquel appartient le logo
     * @return le fichier image du logo
     */
    EpmTLogoOrganisme chercherLogoOrganisme(Integer idOrganisme);

    /**
     * Permet de rechercher le logo par default.
     * @return le fichier image du logo
     */
    public EpmTLogoOrganisme chercherLogoDefault();

    /**
     * Supprimer un logo dans la base s'il existe
     * @param idOrganisme organisme auquel appartient le logo
     */
    void supprimerLogoOrganisme(Integer idOrganisme);

}
