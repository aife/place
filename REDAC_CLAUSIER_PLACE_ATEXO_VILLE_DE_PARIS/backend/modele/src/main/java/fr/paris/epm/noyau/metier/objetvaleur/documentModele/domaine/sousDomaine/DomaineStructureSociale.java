package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineStructureSociale {

    private Boolean esat;
    private Boolean siae;
    private Boolean eess;
    private Boolean activiteEconomique;
    private Boolean formation;
    private Boolean lutte;
    private Boolean commerce;
    private Boolean achat;
    private Boolean autres;

    private String libelleEsat;
    private String libelleSiae;
    private String libelleEess;
    private String libelleActiviteEconomique;
    private String libelleFormation;
    private String libelleLutte;
    private String libelleCommerce;
    private String libelleAchat;
    private String libelleAutres;

    public Boolean getEsat() {
        return esat;
    }

    public void setEsat(Boolean esat) {
        this.esat = esat;
    }

    public Boolean getSiae() {
        return siae;
    }

    public void setSiae(Boolean siae) {
        this.siae = siae;
    }

    public Boolean getEess() {
        return eess;
    }

    public void setEess(Boolean eess) {
        this.eess = eess;
    }

    public Boolean getActiviteEconomique() {
        return activiteEconomique;
    }

    public void setActiviteEconomique(Boolean activiteEconomique) {
        this.activiteEconomique = activiteEconomique;
    }

    public Boolean getFormation() {
        return formation;
    }

    public void setFormation(Boolean formation) {
        this.formation = formation;
    }

    public Boolean getLutte() {
        return lutte;
    }

    public void setLutte(Boolean lutte) {
        this.lutte = lutte;
    }

    public Boolean getCommerce() {
        return commerce;
    }

    public void setCommerce(Boolean commerce) {
        this.commerce = commerce;
    }

    public Boolean getAchat() {
        return achat;
    }

    public void setAchat(Boolean achat) {
        this.achat = achat;
    }

    public Boolean getAutres() {
        return autres;
    }

    public void setAutres(Boolean autres) {
        this.autres = autres;
    }

    public String getLibelleEsat() {
        return libelleEsat;
    }

    public void setLibelleEsat(String libelleEsat) {
        this.libelleEsat = libelleEsat;
    }

    public String getLibelleSiae() {
        return libelleSiae;
    }

    public void setLibelleSiae(String libelleSiae) {
        this.libelleSiae = libelleSiae;
    }

    public String getLibelleEess() {
        return libelleEess;
    }

    public void setLibelleEess(String libelleEess) {
        this.libelleEess = libelleEess;
    }

    public String getLibelleActiviteEconomique() {
        return libelleActiviteEconomique;
    }

    public void setLibelleActiviteEconomique(String libelleActiviteEconomique) {
        this.libelleActiviteEconomique = libelleActiviteEconomique;
    }

    public String getLibelleFormation() {
        return libelleFormation;
    }

    public void setLibelleFormation(String libelleFormation) {
        this.libelleFormation = libelleFormation;
    }

    public String getLibelleLutte() {
        return libelleLutte;
    }

    public void setLibelleLutte(String libelleLutte) {
        this.libelleLutte = libelleLutte;
    }

    public String getLibelleCommerce() {
        return libelleCommerce;
    }

    public void setLibelleCommerce(String libelleCommerce) {
        this.libelleCommerce = libelleCommerce;
    }

    public String getLibelleAchat() {
        return libelleAchat;
    }

    public void setLibelleAchat(String libelleAchat) {
        this.libelleAchat = libelleAchat;
    }

    public String getLibelleAutres() {
        return libelleAutres;
    }

    public void setLibelleAutres(String libelleAutres) {
        this.libelleAutres = libelleAutres;
    }
}
