package fr.paris.epm.noyau.persistance;

public class EpmTCompteurs extends EpmTAbstractObject {

    /**
     * identifiant unique des compteurs (une seule ligne tout le temps ! )
     */
    private int id;

    /**
     * increment pour les consultations
     */
    private int consultationSequence;

    /**
     * increment pour les contrats
     */
    private int contratSequence;

    private int contratChronoSequence;

    /**
     * increment pour les accords cadres (marchés subséquents)
     */
    private int marcheSubsequentSequence;

    public EpmTCompteurs() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConsultationSequence() {
        return consultationSequence;
    }

    public void setConsultationSequence(int consultationSequence) {
        this.consultationSequence = consultationSequence;
    }

    public int getContratSequence() {
        return contratSequence;
    }

    public void setContratSequence(int contratSequence) {
        this.contratSequence = contratSequence;
    }

    public int getContratChronoSequence() {
        return contratChronoSequence;
    }

    public void setContratChronoSequence(int contratChronoSequence) {
        this.contratChronoSequence = contratChronoSequence;
    }

    public int getMarcheSubsequentSequence() {
        return marcheSubsequentSequence;
    }

    public void setMarcheSubsequentSequence(int marcheSubsequentSequence) {
        this.marcheSubsequentSequence = marcheSubsequentSequence;
    }
}
