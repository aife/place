package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;

/**
 * POJO EpmTRefTypeDocumentContrat de la table "epm__t_ref_type_document_contrat"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeDocumentContrat extends EpmTReferentielExterneAbstract implements EpmTRefImportExport {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le catégorie du document associé
     */
    private int idCategorieDocumentContrat;

    private Integer idStatut;

    public int getIdCategorieDocumentContrat() {
        return idCategorieDocumentContrat;
    }

    public void setIdCategorieDocumentContrat(int idCategorieDocumentContrat) {
        this.idCategorieDocumentContrat = idCategorieDocumentContrat;
    }

    public Integer getIdStatut() {
        return idStatut;
    }

    public void setIdStatut(Integer idStatut) {
        this.idStatut = idStatut;
    }

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
