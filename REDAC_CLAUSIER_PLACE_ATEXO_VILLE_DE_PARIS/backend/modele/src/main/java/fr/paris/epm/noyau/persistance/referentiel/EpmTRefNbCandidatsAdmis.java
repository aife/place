package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefNbCandidatsAdmis de la table "epm__t_ref_nb_candidats_admis"
 * Created by nty on 31/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefNbCandidatsAdmis extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_FIXE = 1; // Id fixe.
    public static final int ID_FOURCHETTE = 2; // Id fourchette.

}