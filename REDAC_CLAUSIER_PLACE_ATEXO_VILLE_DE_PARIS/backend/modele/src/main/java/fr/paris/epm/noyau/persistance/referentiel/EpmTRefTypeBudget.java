package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeBudget de la table "epm__t_ref_type_budget"
 * Created by nty on 16/05/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeBudget extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

}
