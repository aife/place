/**
 * 
 */
package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine
 * note de recensement.
 * @author cyril
 *
 */
public class DomaineNoteRecensement {

    /**
     * Pouvoir adjudicateur
     */
    private String pouvoirAdjudicateur;

    /**
     * Description des pretations de la note de recensement
     */
    private String descriptionPrestations;

    /**
     * Montant initial de la note de recensement
     */
    private Double estimatifInitialHT;

    /**
     * Date prévisionnelle de notification de la note de recensement
     */
    private Date dateNotificationPrevisionnelle;

    /**
     * Nature de prestations
     */
    private String nature;

    /**
     * Operation/unité fonctionnelle
     */
    private int idOperationUniteFonctionnelle;

    /**
     * Type de procédure formalisé appliqué : AOO ou MAPA
     */
    private String procedureApplicable;

    /**
     * Si procédure dérogatoire : true
     */
    private boolean procedureDerogatoire;

    /**
     * Commentaire de derogation en cas de procédure derogatoire
     */
    private String commentaireDerogation;

    /**
     * Type de procédure de passation
     */
    private String typeProcedure;

    /**
     * @return the pouvoirAdjudicateur
     */
    public String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur the pouvoirAdjudicateur to set
     */
    public void setPouvoirAdjudicateur(String valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    /**
     * @return the descriptionPrestations
     */
    public String getDescriptionPrestations() {
        return descriptionPrestations;
    }

    /**
     * @param valeur the descriptionPrestations to set
     */
    public void setDescriptionPrestations(String valeur) {
        this.descriptionPrestations = valeur;
    }

    /**
     * @return the estimatifInitialHT
     */
    public Double getEstimatifInitialHT() {
        return estimatifInitialHT;
    }

    /**
     * @param valeur the estimatifInitialHT to set
     */
    public void setEstimatifInitialHT(Double valeur) {
        this.estimatifInitialHT = valeur;
    }

    /**
     * @return the dateNotificationPrevisionnelle
     */
    public Date getDateNotificationPrevisionnelle() {
        return dateNotificationPrevisionnelle;
    }

    /**
     * @param valeur the dateNotificationPrevisionnelle to set
     */
    public void setDateNotificationPrevisionnelle(Date valeur) {
        this.dateNotificationPrevisionnelle = valeur;
    }

    /**
     * @return the nature
     */
    public String getNature() {
        return nature;
    }

    /**
     * @param valeur the nature to set
     */
    public void setNature(String valeur) {
        this.nature = valeur;
    }

    /**
     * @return the idOperationUniteFonctionnelle
     */
    public int getIdOperationUniteFonctionnelle() {
        return idOperationUniteFonctionnelle;
    }

    /**
     * @param valeur the idOperationUniteFonctionnelle to set
     */
    public void setIdOperationUniteFonctionnelle(int valeur) {
        this.idOperationUniteFonctionnelle = valeur;
    }

    /**
     * @return the procedureApplicable
     */
    public String getProcedureApplicable() {
        return procedureApplicable;
    }

    /**
     * @param valeur the procedureApplicable to set
     */
    public void setProcedureApplicable(String valeur) {
        this.procedureApplicable = valeur;
    }
    
    /**
     * @return the procedureDerogatoire
     */
    public boolean isProcedureDerogatoire() {
        return procedureDerogatoire;
    }

    /**
     * @param valeur the procedureDerogatoire to set
     */
    public void setProcedureDerogatoire(boolean valeur) {
        this.procedureDerogatoire = valeur;
    }

    /**
     * @return the commentaireDerogation
     */
    public String getCommentaireDerogation() {
        return commentaireDerogation;
    }

    /**
     * @param valeur the commentaireDerogation to set
     */
    public void setCommentaireDerogation(String valeur) {
        this.commentaireDerogation = valeur;
    }

    /**
     * @return the typeProcedure
     */
    public String getTypeProcedure() {
        return typeProcedure;
    }

    /**
     * @param valeur the typeProcedure to set
     */
    public void setTypeProcedure(String valeur) {
        this.typeProcedure = valeur;
    }

}
