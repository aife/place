package fr.paris.epm.noyau.persistance.referentiel;

import java.util.Set;

/**
 * POJO EpmTRefNature de la table "epm__t_ref_nature"
 * Pojo de nomenclature des champs additionnels
 * Created by nty on 26/06/17.
 * @author Tyurin Nikolay
 * @author nty
 */
public class EpmTRefNomenclature extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private Set<EpmTRefNomenclatureValeur> epmTRefNomenclatureValeurs;

    public Set<EpmTRefNomenclatureValeur> getEpmTRefNomenclatureValeurs() {
        return epmTRefNomenclatureValeurs;
    }

    public void setEpmTRefNomenclatureValeurs(Set<EpmTRefNomenclatureValeur> epmTRefNomenclatureValeurs) {
        this.epmTRefNomenclatureValeurs = epmTRefNomenclatureValeurs;
    }

}