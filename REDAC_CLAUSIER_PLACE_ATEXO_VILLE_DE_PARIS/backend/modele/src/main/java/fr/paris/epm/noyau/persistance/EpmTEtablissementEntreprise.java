package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeEtablissement;

import java.util.Date;

/**
 * Pojo pour les établissements du référentiel entreprise associé à une
 * entreprise.
 * @author Rebeca Dantas
 */
public class EpmTEtablissementEntreprise extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;
    
    private Integer id;

    /**
     * numéro de son téléphone fixe.
     */
    private String telephoneFixe;

    /**
     * Numero voie, par exemple '12'.
     */
    private String numeroVoie;

    /**
     * Nom de la voie, par exemple 'rue royale'
     */
    private String nomVoie;

    /**
     * addresse de l'établissement.
     */
    private String complementAdresse;

    /**
     * code postal de l'établissement.
     */
    private String codePostal;

    /**
     * ville de l'établissement.
     */
    private String ville;

    /**
     * Cedex de l'établissement.
     */
    private String cedex;

    /**
     * Code du tier dans l'interface finance du client.
     */
    private String codeTierFinance;

    /**
     * date de création.
     */
    private Date dateCreationFiche;

    /**
     * date de modification.
     */
    private Date dateModificationFiche;

    /**
     * Code de l'établissement.
     */
    private String codeEtablissement;
    
    /**
     * Type de l'établissement dans l'entreprise (siége ou autre).
     */
    private EpmTRefTypeEtablissement typeEtablissement;

    /**
     * entreprise associé à cette personne.
     */
    private EpmTEntrepriseContrat entrepriseContrat;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer valeur) {
        this.id = valeur;
    }
    
    /**
     * @return téléphone fixe de la personne
     */
    public String getTelephoneFixe() {
        return telephoneFixe;
    }

    /**
     * @param valeur téléphone fixe de la personne
     */
    public void setTelephoneFixe(final String valeur) {
        this.telephoneFixe = valeur;
    }

    /**
     * @return code postal associé a l'adresse
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param valeur code postal associé à l'adresse
     */
    public void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    /**
     * @return date de la création de l'enregistrement
     */
    public Date getDateCreationFiche() {
        return dateCreationFiche;
    }

    /**
     * @param valeur date de la création de l'enregistrement
     */
    public void setDateCreationFiche(final Date valeur) {
        this.dateCreationFiche = valeur;
    }

    /**
     * @return dernière modification de l'enregistrement
     */
    public Date getDateModificationFiche() {
        return dateModificationFiche;
    }

    /**
     * @param valeur dernière modification de l'enregistrement
     */
    public void setDateModificationFiche(final Date valeur) {
        this.dateModificationFiche = valeur;
    }

    /**
     * @return ville associé à l'adresse
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param valeur ville associé à l'adresse
     */
    public void setVille(final String valeur) {
        this.ville = valeur;
    }

    /**
     * @return le code de l'établissement.
     */
    public String getCodeEtablissement() {
        return codeEtablissement;
    }

    /**
     * @param valeur code de l'établissement
     */
    public void setCodeEtablissement(String valeur) {
        this.codeEtablissement = valeur;
    }

    public String getNumeroVoie() {
        return numeroVoie;
    }

    public void setNumeroVoie(final String valeur) {
        this.numeroVoie = valeur;
    }

    public String getNomVoie() {
        return nomVoie;
    }

    public void setNomVoie(final String valeur) {
        this.nomVoie = valeur;
    }

    public String getComplementAdresse() {
        return complementAdresse;
    }

    public void setComplementAdresse(final String valeur) {
        this.complementAdresse = valeur;
    }

    public String getCedex() {
        return cedex;
    }

    public void setCedex(final String valeur) {
        this.cedex = valeur;
    }

    public String getCodeTierFinance() {
        return codeTierFinance;
    }

    public void setCodeTierFinance(final String valeur) {
        this.codeTierFinance = valeur;
    }

    public EpmTRefTypeEtablissement getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(final EpmTRefTypeEtablissement valeur) {
        this.typeEtablissement = valeur;
    }
    
    /**
     * @return entreprise associé à la personne
     */
    public EpmTEntrepriseContrat getEntrepriseContrat() {
        return entrepriseContrat;
    }

    /**
     * @param valeur entreprise associé à la personne
     */
    public void setEntrepriseContrat(final EpmTEntrepriseContrat valeur) {
        this.entrepriseContrat = valeur;
    }    
}
