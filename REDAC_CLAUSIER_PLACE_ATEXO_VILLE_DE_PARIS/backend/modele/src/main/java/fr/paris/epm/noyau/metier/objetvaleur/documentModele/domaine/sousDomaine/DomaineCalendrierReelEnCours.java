package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * dates.calendrierReelEnCours.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCalendrierReelEnCours implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private Date dateReelleLancement;

    private Date dateOuvertureCandidatures;

    private Date dateSelectionCandidatures;
    
    private Date dateCAOOuvertureOffre;
    
    private Date dateCAOAttribution;
    
    private String libelleDateOuvertureCandidatures;
    
    private String libelleDateCAOOuvertureOffre;
    
    private String libelleDateCAOAttribution;

	private Date dateDeliberationAmont;

    public final Date getDateReelleLancement() {
        return dateReelleLancement;
    }

    public final void setDateReelleLancement(final Date valeur) {
        dateReelleLancement = valeur;
    }

    public final Date getDateOuvertureCandidatures() {
        return dateOuvertureCandidatures;
    }

    public final void setDateOuvertureCandidatures(final Date valeur) {
        this.dateOuvertureCandidatures = valeur;
    }

    public final Date getDateSelectionCandidatures() {
        return dateSelectionCandidatures;
    }

    public final void setDateSelectionCandidatures(final Date valeur) {
        this.dateSelectionCandidatures = valeur;
    }

    public final Date getDateCAOOuvertureOffre() {
        return dateCAOOuvertureOffre;
    }

    public final void setDateCAOOuvertureOffre(final Date valeur) {
        this.dateCAOOuvertureOffre = valeur;
    }

    public final Date getDateCAOAttribution() {
        return dateCAOAttribution;
    }

    public final void setDateCAOAttribution(final Date valeur) {
        this.dateCAOAttribution = valeur;
    }

	/**
	 * @return the libelleDateOuvertureCandidatures
	 */
	public final String getLibelleDateOuvertureCandidatures() {
		return libelleDateOuvertureCandidatures;
	}

	/**
	 * @param valeur
	 *            the libelleDateOuvertureCandidatures to set
	 */
	public final void setLibelleDateOuvertureCandidatures(final String valeur) {
		this.libelleDateOuvertureCandidatures = valeur;
	}

	/**
	 * @return the libelleDateCAOOuvertureOffre
	 */
	public final String getLibelleDateCAOOuvertureOffre() {
		return libelleDateCAOOuvertureOffre;
	}

	/**
	 * @param valeur
	 *            the libelleDateCAOOuvertureOffre to set
	 */
	public final void setLibelleDateCAOOuvertureOffre(final String valeur) {
		this.libelleDateCAOOuvertureOffre = valeur;
	}

	/**
	 * @return the libelleDateCAOAttribution
	 */
	public final String getLibelleDateCAOAttribution() {
		return libelleDateCAOAttribution;
	}

	/**
	 * @param valeur the libelleDateCAOAttribution to set
	 */
	public final void setLibelleDateCAOAttribution(final String valeur) {
		this.libelleDateCAOAttribution = valeur;
	}

	public Date getDateDeliberationAmont() {
		return dateDeliberationAmont;
	}

	public void setDateDeliberationAmont(Date dateDeliberationAmont) {
		this.dateDeliberationAmont = dateDeliberationAmont;
	}
}
