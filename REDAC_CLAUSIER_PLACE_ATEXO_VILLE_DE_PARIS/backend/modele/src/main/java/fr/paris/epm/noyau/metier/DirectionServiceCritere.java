package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

/**
 * Critére de recherche des {@link EpmTRefDirectionService}. Utilisé dans noyau
 * proxy et administration.
 * @author Léon Barsamian
 */
public class DirectionServiceCritere extends AbstractCritere implements Critere, Serializable {

    public DirectionServiceCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }
    /**
     * Serialisation
     */

    private static final long serialVersionUID = 766343928456812395L;

    /**
     * Identifiant de la direction que l'on cherche.
     */
    private Integer idDirectionsService;

    /**
     * Niveau de la direction (niveau 0 == racine)
     */
    private String typeEntite;

    /**
     * Directions accessible en écriture (ie création de consultation / modif organigramme.)
     */
    private Boolean actif;

    private String libelle;
    private String plateformeUuid;

    /**
     * Code de la direction service
     */
    private String libelleCourt;

    /**
     * L'identifiant externe de la direction service
     */
    private Collection<String> codesExterne;

    /**
     *   ID de la bi-clé
     */
    private Integer idBiClef;

    private Integer idParent;

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        if (idBiClef != null) {
            sb.append(" JOIN FETCH direction.biclefs AS biClef WHERE biClef.id = :idBiClef");
            getParametres().put("idBiClef", idBiClef);
            debut = false;
        }

        if (idDirectionsService != null && idDirectionsService != 0) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.id = :idDirectionsService");
            getParametres().put("idDirectionsService", idDirectionsService);
            debut = false;
        }

        if (typeEntite != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.typeEntite = :typeEntite");
            getParametres().put("typeEntite", typeEntite);
            debut = false;
        }

        if (actif != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.actif = :actif");
            getParametres().put("actif", actif);
            debut = false;
        }

        if (libelle != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.libelle = :libelle");
            getParametres().put("libelle", libelle);
            debut = false;
        }

        if (libelleCourt != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.libelleCourt = :libelleCourt");
            getParametres().put("libelleCourt", libelleCourt);
            debut = false;
        }

        if (codesExterne != null && !codesExterne.isEmpty()) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.codeExterne in (:codesExterne)");
            getParametres().put("codesExterne", codesExterne);
            debut = false;
        }
        if (plateformeUuid != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.epmTRefOrganisme.plateformeUuid = :plateformeUuid ");
            getParametres().put("plateformeUuid", plateformeUuid);
            debut = false;
        }
        if (idParent != null && idParent != 0) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.parent.id = :idParent)");
            getParametres().put("idParent", idParent);
            debut = false;
        }

        if(idParent != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" direction.parent.id = :idParent");
            getParametres().put("idParent", idParent);
            debut = false;
        }

        return sb;
    }

    public String toCountHQL() {
        // TODO Auto-generated method stub
        return null;
    }

    public String toHQL() {
        final StringBuffer req = new StringBuffer();
        req.append("from EpmTRefDirectionService as direction ");
        req.append(corpsRequete());
        if (proprieteTriee != null) {
            req.append(" order by direction.").append(proprieteTriee);
            if (triCroissant) {
                req.append(" ASC ");
            } else {
                req.append(" DESC ");
            }
        }
        return req.toString();
    }

    public final void setIdDirectionsService(final Integer valeur) {
        this.idDirectionsService = valeur;
    }

    public final void setTypeEntite(final String valeur) {
        this.typeEntite = valeur;
    }

    public final void setActif(final Boolean valeur) {
        this.actif = valeur;
    }

    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public void setCodesExterne(String... codesExterne) {
        this.codesExterne = Arrays.asList(codesExterne);
    }

    public void setCodesExterne(Collection<String> codesExterne) {
        this.codesExterne = codesExterne;
    }

    public void setIdBiClef(Integer idBiClef) {
        this.idBiClef = idBiClef;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

}
