package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.metier.AbstractCritere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Rassemble les critères de recherche des clauses.
 *
 * @author RAMLI Tarik
 */
public class ClauseCritere extends AbstractCritere {
    public ClauseCritere() {
        this.plateformeUuid = null;
    }

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClauseCritere.class);

    /**
     * sérialiseur.
     */
    private static final long serialVersionUID = 1L;

    protected final String plateformeUuid;
    private Collection<Integer> listIds;

    /**
     * reference de la clause.
     */
    private String reference;

    /**
     * reference du canevas
     */
    private String referenceCanevas;

    /**
     * procedure de passation de la clause.
     */
    private Integer procedure;

    /**
     * les mots clés associé à la clause.
     */
    private String[] motscles;

    /**
     * thème de la clause.
     */
    private int theme;
    private Boolean disponible = true;
    /**
     * type de la clause.
     */
    private Integer idTypeClause;

    /**
     * nature de prestation de la clause.
     */
    private int naturePrestation;

    /**
     * le type de document pour laquelle la clause est construite.
     */
    private int idTypeDocument;

    private Boolean typeDocumentActif;

    private Integer idTypeContrat;
    private List<Integer> idsTypeContrat;

    /**
     * le statut de la clause actif ou non.
     */
    private Boolean actif;

    private Boolean parametrableDirection;

    private Boolean parametrableAgent;

    /**
     * L'identifiant du statut de redaction de la clause
     */
    private Integer idStatutRedactionClausier;

    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private boolean canevasCompatibleEntiteAdjudicatrice = true;

    /**
     * Identifiant de l'organisme auquel la clause est liée
     */
    private Integer idOrganisme;
    private Integer idClauseOrigine;

    /**
     * Détermine si la clause est une clause editeur ou client
     */
    private Boolean editeur;

    /**
     * Détermine si la surcharge est sélectionnée ou la clause éditeur est sélectionnée
     */
    private Boolean surchargeActif = null;

    /**
     * Rechrcher les clauses editeurs surchargées
     */
    private boolean nonAfficherSurcharge = false;

    /**
     * Client/Editeur/Editeur surchargée
     */
    private int auteur = 0;

    private Date dateModificationDebut = null;

    private Date dateModificationFin = null;

    public ClauseCritere(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }

    /**
     * Génére la requête HQL correspondant à l'ensemble des critères.
     *
     * @return query la requête HQL
     */
    @Override
    public StringBuffer corpsRequete() {

        StringBuffer clauseAnd = new StringBuffer();
        if (Boolean.TRUE.equals(disponible)) {
            clauseAnd.append(" WHERE clause.etat != '2'");
        } else if (Boolean.FALSE.equals(disponible)) {
            clauseAnd.append(" WHERE clause.etat = '2'");
        } else {
            clauseAnd.append(" WHERE 1 = 1");
        }

        if (theme > 1) {
            clauseAnd.append(" AND clause.epmTRefThemeClause.id = :idTheme");
            getParametres().put("idTheme", theme);
        }

        if (idTypeClause != null && idTypeClause > 0) {
            clauseAnd.append(" AND clause.epmTRefTypeClause.id = :idTypeClause");
            getParametres().put("idTypeClause", idTypeClause);
        }

        if (reference != null && !reference.isEmpty()) {
            clauseAnd.append(" AND LOWER(clause.reference) like LOWER(:reference)");
            getParametres().put("reference", "%" + reference.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE).trim() + "%");
        }

        if (procedure != null && procedure != 0) {
            clauseAnd.append(" AND (clause.epmTRefProcedure IS NULL OR clause.epmTRefProcedure.id = :idProcedure)");
            getParametres().put("idProcedure", procedure);
        }

        if (idTypeContrat != null && idTypeContrat != 0) {
            clauseAnd.append(" AND ").append(findByTypeContrat());
            getParametres().put("idTypeContrat", idTypeContrat);
        }
        if (!CollectionUtils.isEmpty(idsTypeContrat)) {
            clauseAnd.append(" AND ").append(findByTypesContrat());
            getParametres().put("idsTypeContrat", idsTypeContrat);
        }

        if (naturePrestation > 0) {
            clauseAnd.append(" AND (clause.idNaturePrestation = 0 OR clause.idNaturePrestation = :idNature)");
            getParametres().put("idNature", naturePrestation);
        }

        if (idTypeDocument > 1) {
            clauseAnd.append(" AND (clause.epmTRefTypeDocument.id = 1 OR clause.epmTRefTypeDocument.id = :idTypeDocument)");
            getParametres().put("idTypeDocument", idTypeDocument);
        }

        if (typeDocumentActif != null) {
            clauseAnd.append(" AND clause.epmTRefTypeDocument.actif = :typeDocumentActif");
            getParametres().put("typeDocumentActif", typeDocumentActif);
        }

        if (actif != null) {
            clauseAnd.append(" AND clause.actif = ").append(actif);
        }

        if (motscles != null && Arrays.stream(motscles).anyMatch(m -> m != null && !m.trim().isEmpty())) {
            clauseAnd.append(" AND ").append(findByMotsCles());
        }

        // type parametrage dans le cas du recherche parametrage par
        // direction ou agent
        String parametrable = null;
        if (parametrableDirection != null)
            parametrable = "clause.parametrableDirection = '" + (parametrableDirection ? "1" : "0") + "'";
        else if (parametrableAgent != null)
            parametrable = "clause.parametrableAgent = '" + (parametrableAgent ? "1" : "0") + "'";

        if (parametrable != null) clauseAnd.append(" AND ").append(parametrable);

        if (listIds != null && !listIds.isEmpty()) {
            LOG.info("Liste des ID clauses = {}", listIds);
            clauseAnd.append(findByIdClauses());
            getParametres().put("listIds", listIds);
        }

        if (idStatutRedactionClausier != null && idStatutRedactionClausier != 0) {
            clauseAnd.append(" AND clause.epmTRefStatutRedactionClausier.id = :idStatutRedactionClausier");
            getParametres().put("idStatutRedactionClausier", idStatutRedactionClausier);
        }

        if (!canevasCompatibleEntiteAdjudicatrice) {
            clauseAnd.append(" AND clause.compatibleEntiteAdjudicatrice = false");
        }

        if (idOrganisme != null && idOrganisme != 0) {
            clauseAnd.append(" AND (clause.clauseEditeur = true OR clause.idOrganisme = :idOrganisme)");
            getParametres().put("idOrganisme", idOrganisme);
        }
        if (idClauseOrigine != null && idClauseOrigine != 0) {
            clauseAnd.append(" AND ").append(findByIdClauseOrigine());
            getParametres().put("idClauseOrigine", idClauseOrigine);
        }

        if (editeur != null) {
            clauseAnd.append(" AND clause.clauseEditeur = :editeur");
            getParametres().put("editeur", editeur);
        }

        if (surchargeActif != null) {
            clauseAnd.append(" AND clause.surchargeActif = :surchargeActif");
            getParametres().put("surchargeActif", surchargeActif);
        }

        if (nonAfficherSurcharge) clauseAnd.append(" AND ").append(nonAfficherSurcharge());

        if (auteur != 0) {
            if (auteur == 2 || auteur == 3) {
                clauseAnd.append(" AND clause.clauseEditeur = true AND ");

                if (auteur != 3) clauseAnd.append("NOT ");
                clauseAnd.append("clause.epmTClausePub.idClause IN (" + "SELECT clauseSurcharge.idClauseOrigine " + "FROM EpmTClause AS clauseSurcharge " + "WHERE clauseSurcharge.idClauseOrigine IS NOT NULL AND clauseSurcharge.surchargeActif = TRUE");

                if (idOrganisme != null && idOrganisme > 0) {
                    clauseAnd.append(" AND clauseSurcharge.idOrganisme = :idOrganisme");
                    getParametres().put("idOrganisme", idOrganisme);
                }
                clauseAnd.append(")");
            } else {
                clauseAnd.append(" AND clause.clauseEditeur = false");
            }
        }

        if (dateModificationDebut != null) {
            clauseAnd.append(" AND clause.dateModification > :dateModificationDebut");
            getParametres().put("dateModificationDebut", dateModificationDebut);
        }

        if (dateModificationFin != null) {
            clauseAnd.append(" AND clause.dateModification < :dateModificationFin ");
            getParametres().put("dateModificationFin", dateModificationFin);
        }

        return clauseAnd;
    }

    protected String findByIdClauses() {
        return " AND clause.id in (:listIds)";
    }

    protected String findByIdClause() {
        return " AND clause.id = :idClause";
    }


    /*
        il ne faut redéfinir cette méthode que dans ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVCanevas.epmTClause ou EpmVCanevas.epmTClausePub
     */
    protected String findByTypeContrat() {
        return "(size(clause.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(clause.epmTRefTypeContrats))";
    }

    protected String findByTypesContrat() {
        return "(size(clause.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id in (:idsTypeContrat)) in elements(clause.epmTRefTypeContrats))";
    }

    /*
        il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
        et ClausePubCritere ne doit pas la supporter
     */
    protected String findByIdClauseOrigine() {
        return "clause.idClauseOrigine = :idClauseOrigine";
    }

    /*
            il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
            car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
            et ClausePubCritere ne doit pas la supporter
         */
    protected String nonAfficherSurcharge() {
        return "clause.idClauseOrigine IS NULL";
    }

    /*
        il faut redéfinir cette méthode dans ClausePubCritere et ClauseViewCritere
        car ClausePubCritere cherche dans EpmTRoleClausePub
        et ClauseViewCritere cherche dans EpmTClause.EpmTRoleClause
     */
    protected String findByMotsCles() {
        StringBuilder sb = new StringBuilder("(");
        boolean debut = true;

        for (String motclef : motscles) {
            motclef = motclef.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE);

            sb.append(debut ? "" : " OR ");
            // Chercher Le mot clé dans le contenu (texte fixe avant) de la clause
            sb.append("LOWER(clause.texteFixeAvant) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (texte fixe après) de la clause
            sb.append("LOWER(clause.texteFixeApres) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (valeur par defaut) de la clause
            sb.append("exists (from EpmTRoleClause role where LOWER(role.valeurDefaut) like LOWER('%").append(motclef).append("%') AND role in elements(clause.epmTRoleClauses))");

            // Chercher Le mot clé dans la liste des mots clés
            sb.append(" OR ");
            sb.append("LOWER(clause.motsCles) like LOWER('%").append(motclef).append("%')");

            debut = false;
        }
        sb.append(")");
        return sb.toString();
    }

    protected String getTableDeRecherche() {
        return " EpmTClause ";
    }

    protected String getJoinsTables() {
        return "";
    }

    public String toHQL() {
        StringBuilder sb = new StringBuilder("select clause from ");
        sb.append(getTableDeRecherche());
        sb.append(" as clause ");
        sb.append(getJoinsTables());
        sb.append(corpsRequete());
        if (getProprieteTriee() != null) {
            sb.append(" ORDER BY clause.").append(getProprieteTriee());
            sb.append(isTriCroissant() ? " ASC" : " DESC");
        }
        return sb.toString();
    }

    public String toCountHQL() {
        StringBuilder sb = new StringBuilder("select count(clause.id) from ");
        sb.append(getTableDeRecherche());
        sb.append(" as clause ");
        sb.append(getJoinsTables());
        sb.append(corpsRequete());
        return sb.toString();
    }

    public final void setId(Integer id) { // utilisé par SafelyFacade
        this.setIds(id);
    }

    public final void setIds(Integer... ids) {
        this.setListIds(Arrays.asList(ids));
    }

    public final void setListIds(Collection<Integer> listIds) {
        this.listIds = listIds;
    }

    public final void setReference(final String valeur) {
        reference = valeur;
    }

    public final void setReferenceCanevas(final String valeur) {
        this.referenceCanevas = valeur;
    }

    public final void setProcedure(Integer procedure) {
        this.procedure = procedure;
    }

    public final void setMotscles(final String... valeur) {
        motscles = valeur;
    }

    public final String[] getMotscles() {
        return motscles;
    }

    public final void setTheme(final int valeur) {
        theme = valeur;
    }

    public final void setNaturePrestation(final int valeur) {
        naturePrestation = valeur;
    }

    public final void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public final void setTypeDocumentActif(Boolean typeDocumentActif) {
        this.typeDocumentActif = typeDocumentActif;
    }

    public final void setActif(final Boolean valeur) {
        this.actif = valeur;
    }

    public final void setParametrableDirection(Boolean valeur) {
        this.parametrableDirection = valeur;
    }

    public final void setParametrableAgent(Boolean valeur) {
        this.parametrableAgent = valeur;
    }

    public final void setIdStatutRedactionClausier(final Integer valeur) {
        this.idStatutRedactionClausier = valeur;
    }

    public final void setCanevasCompatibleEntiteAdjudicatrice(final boolean valeur) {
        this.canevasCompatibleEntiteAdjudicatrice = valeur;
    }

    public final void setEditeur(final Boolean valeur) {
        this.editeur = valeur;
    }


    public final void setSurchargeActif(Boolean surchargeActif) {
        this.surchargeActif = surchargeActif;
    }

    public final void setNonAfficherSurcharge(boolean nonAfficherSurcharge) {
        this.nonAfficherSurcharge = nonAfficherSurcharge;
    }

    public final void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

    public final Boolean isEditeur() {
        return editeur;
    }

    public final void setAuteur(int auteur) {
        this.auteur = auteur;
    }

    public final void setDateModificationDebut(final Date valeur) {
        this.dateModificationDebut = valeur;
    }

    public final void setDateModificationFin(final Date valeur) {
        this.dateModificationFin = valeur;
    }

    public final void setIdTypeContrat(final Integer idTypeContrat) {
        this.idTypeContrat = idTypeContrat;
    }

    public final void setIdsTypeContrat(final List<Integer> idsTypeContrat) {
        this.idsTypeContrat = idsTypeContrat;
    }

    public final void setIdTypeClause(Integer type) {
        this.idTypeClause = type;
    }

    public final void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }

    public void setIdClauseOrigine(Integer idClauseOrigine) {
        this.idClauseOrigine = idClauseOrigine;
    }

    public String getPlateformeUuid() {
        return plateformeUuid;
    }
}
