package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.persistance.EpmTObject;

import java.util.List;

/**
 * Interface sécurisée du service generique. Ce service est utilisé pour
 * accéder aux consultations de facon generique.
 * 
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
public interface GeneriqueServiceSecurise {

    /**
     * L'identifiant de l'objet passé doit être renseigné et correspondre à
     * un objet de la consultation et implementé l'interface {@link EpmTObject}.
     * Merge
     */
    <T extends EpmTObject> T modifierEpmTObject(T objet);

    /**
     * L'identifiant de l'objet passé doit être renseigné et correspondre à
     * un objet de la consultation et implementé l'interface {@link EpmTObject}.
     * Persist
     */
    <T extends EpmTObject> void creerEpmTObject(T objet);
    
    /**
     * Recherche d'objet par criteres.
     * @param id id de l'utilisateur.
     * @param criteres les criteres de recherches.
     * @throws TechnicalNoyauException erreur technique.
     * @throws NonTrouveNoyauException dans le cas où les élements ne sont pas trouvés.
     * @return {@link List} retourne une liste de documents recherchés.
     */
    <T extends EpmTObject> List<T> chercherEpmTObject(final int id, final Critere criteres);
    
    /**
     * Recherche d'objet par criteres et retourne un objet que s'il est unique, retourne null si absent, une exception
     * si plusieurs objets trouvé
     * @param id id de l'utilisateur.
     * @param criteres les criteres de recherches.
     * @throws TechnicalNoyauException erreur technique.
     * @throws NonTrouveNoyauException dans le cas où les élements ne sont pas trouvés.
     * @return {@link List} retourne une liste de documents recherchés.
     */
    <T extends EpmTObject> T chercherUniqueEpmTObject(final int id, final Critere criteres);

    /**
     * Suppression d'une liste d'objet {@link EpmTObject}
     * @param valeur liste d'objet implementant l'interface {@link EpmTObject} à supprimer.
     * @param id id de l'utilisateur.
     * @throws TechnicalNoyauException
     */
    <T extends EpmTObject> void supprimerEpmTObject(final int id, final List<T> valeur);
    
    /**
     * Suppression d'un objet {@link EpmTObject}
     * @param valeur objet implement l'interface {@link EpmTObject} à supprimer.
     * @param id id de l'utilisateur.
     * @throws TechnicalNoyauException
     */
    <T extends EpmTObject> void supprimerEpmTObject(final int id, final T valeur);
    
    /**
     * modifier une liste de EpmTObject en base
     * 
     * @param id identifiant de l'utilisateur appelant cette méhode
     * @param objets liste de EpmTObject à modifier
     * @return liste de EpmTObject déjà modifié
     * @throws TechnicalNoyauException erreur technique
     */
    <T extends EpmTObject> List<T> modifierEpmTObject(final int id, final List<T> objets);

    /**
     * Charge un objet unique à partir de son ID
     *
     * @param id identifiant de l'objet recherché appelant cette méhode
     * @return clazz class concerné
     * @throws TechnicalNoyauException erreur technique
     */
    <T extends EpmTObject> T chercherObject(int id, Class<T> clazz);

}
