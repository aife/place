package fr.paris.epm.noyau.dto;

import java.io.Serializable;

public class RepertoiresChaineCertificationBean implements Serializable {

    String nom;
    String statut;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
}
