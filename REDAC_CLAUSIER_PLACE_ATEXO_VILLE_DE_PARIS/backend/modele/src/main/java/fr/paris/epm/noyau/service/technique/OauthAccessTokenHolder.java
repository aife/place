package fr.paris.epm.noyau.service.technique;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface OauthAccessTokenHolder {

	/**
	 * Permet de récuperer l'access token
	 * @return l'access token
	 */
	Optional<String> get();

	/**
	 * Permet de renouveller un jeton
	 *
	 * @return le nouveau jeton
	 */
	Credentials renew();

	/**
	 * Permet de verifier la validite du jeton
	 * @return vrai si le jeton est valide, faux sinon
	 */
	boolean check();
}
