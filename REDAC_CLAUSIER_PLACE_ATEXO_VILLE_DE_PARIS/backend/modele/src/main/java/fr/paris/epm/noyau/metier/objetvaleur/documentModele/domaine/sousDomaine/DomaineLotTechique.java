package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.List;

/**
 * Bean contenant tous les données nécessaires du lot technique à être remplacé
 * dans un document. Notamment, dans le cadre de la génération des tableaux
 * "Liste des postes/lots techniques"
 * 
 * @author Rebeca Dantas
 * 
 */
public class DomaineLotTechique implements Comparable<DomaineLotTechique> {
	
	/**
	 * Code / Numéro du lot
	 */
	private String code;
	
	/**
	 * L'intitulé du lot technique
	 */
	private String intitule;
	
	/**
	 * Défine si le lot technique est le lot principal (un seul lot principal)
	 */
	private boolean principal;
	
	/**
	 * La liste des tranches associés au lot technique
	 */
	private List<DomaineTranche> tranches;

	/**
	 * 
	 * @return le code du lot technique
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code le code/numéro du lot technique
	 */
	public final void setCode(final String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return l'intitule du lot technique
	 */
	public final String getIntitule() {
		return intitule;
	}

	/**
	 * 
	 * @param intitule l'intitule du lot technique
	 */
	public final void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	/**
	 * 
	 * @return true si le lot technique a été défini comme lot principal
	 */
	public final boolean isPrincipal() {
		return principal;
	}

	/**
	 * 
	 * @param principal : true si le lot technique est le lot principal
	 */
	public final void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	/**
	 * 
	 * @return liste des tranches associés au lot technique
	 */
	public final List<DomaineTranche> getTranches() {
		return tranches;
	}

	/**
	 * @param tranches liste des tranches qui sont associés au lot technique
	 */
	public final void setTranches(List<DomaineTranche> tranches) {
		this.tranches = tranches;
	}

    @Override
	public int compareTo(DomaineLotTechique lot) {
		Integer num1 = parseToInt(this.code);
		Integer num2 = parseToInt(lot.code);

		if (num1 == null) {
			if (num2 == null)
				return this.code.compareTo(lot.code);
			else
				return 1;
		} else {
			if (num2 == null)
				return -1;
			else
				return num1.compareTo(num2);
		}
	}

	private Integer parseToInt(final String s) {
        try {
			return Integer.parseInt(s);
		} catch (NumberFormatException ex) {
			return null;
        }
    }

}
