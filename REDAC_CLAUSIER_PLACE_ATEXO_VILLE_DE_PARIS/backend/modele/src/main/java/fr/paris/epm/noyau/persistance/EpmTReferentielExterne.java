/**
 * 
 */
package fr.paris.epm.noyau.persistance;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

/**
 * Classe représentant les référentiels externes à l'application remplis
 * dynamiquement par les clients dans le schémas echanges. Il s'agit de copie
 * des entrées du schémas échange saisi à un instant t par un utilisateur.
 * @author Léon Barsamian
 */
@Audited
@AuditTable(value = "epm__t_referentiel_externe_aud", schema = "consultation")
public class EpmTReferentielExterne extends EpmTAbstractObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -7517458801417350179L;

    /**
     * Identifiant unique.
     */
    private int id;

    /**
     * Libellé affiché à l'utilisateur.
     */
    private String libelle;

    /**
     * Code interne à l'application d'où proviennent les données. Ce code pourra
     * être affiché à l'utilisateur.
     */
    private String code;

    /**
     * @return Identifiant unique.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur Identifiant unique.
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return Libellé affiché à l'utilisateur.
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur Libellé affiché à l'utilisateur.
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    /**
     * @return Code interne à l'application d'où proviennent les données. Ce
     *         code pourra être affiché à l'utilisateur.
     */
    public String getCode() {
        return code;
    }

    /**
     * @param valeur Code interne à l'application d'où proviennent les données.
     *            Ce code pourra être affiché à l'utilisateur.
     */
    public void setCode(final String valeur) {
        this.code = valeur;
    }


}
