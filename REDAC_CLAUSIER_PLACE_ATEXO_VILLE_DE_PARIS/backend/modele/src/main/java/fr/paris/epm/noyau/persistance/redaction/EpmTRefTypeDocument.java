package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate de EpmTRefTypeDocument .
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_type_document", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefTypeDocument extends BaseEpmTRefRedaction implements EpmTRefImportExport {

    /**
     * attribut ID_TOUS static .
     */
    public static final int ID_TOUS = 1;

    /**
     * Type de fichier généré.
     */
    @Column(name="extension_fichier")
    private String extensionFichier;

    /**
     * template utilisé pour la génération du fichier.
     */
    private String template;
    
    /**
     * Attribut qui défini si le sommaire doit être généré ou pas
     */
    private boolean sommaire;

    @Column(name="template_tableau_derogation")
    private String templateTableauDerogation;

    /**
     * Détermine si la fonctionnalité de gestion de dérogations est activée
     */
    @Column(name="activer_derogation")
    private boolean activerDerogation;

    /*
    * l'id du type du document (EpmT_Document_Type_Contrat) du cote du module passation
    *
    * */
    @Column(name = "id_type_passation")
    private Integer idTypePassation;

    public String getExtensionFichier() {
        return extensionFichier;
    }

    public void setExtensionFichier(final String valeur) {
        this.extensionFichier = valeur;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(final String valeur) {
        this.template = valeur;
    }

    public boolean isSommaire() {
        return sommaire;
    }

    public void setSommaire(final boolean valeur) {
        this.sommaire = valeur;
    }

    public String getTemplateTableauDerogation() {
        return templateTableauDerogation;
    }

    public void setTemplateTableauDerogation(final String valeur) {
        this.templateTableauDerogation = valeur;
    }

    public boolean isActiverDerogation() {
        return activerDerogation;
    }

    public void setActiverDerogation(boolean valeur) {
        this.activerDerogation = valeur;
    }

    public Integer getIdTypePassation() {
        return idTypePassation;
    }

    public void setIdTypePassation(Integer idTypePassation) {
        this.idTypePassation = idTypePassation;
    }

	/**
     * comparer des objets.
     */
    public int compareTo(final BaseEpmTRefRedaction ref) {
        final int idTous = 1;

        if (this.getId() == ref.getId() || ref.getId() <= idTous)
            return 1;

        if (getId() <= idTous)
            return -1;

        return Integer.compare(getId(), ref.getId());
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    @Override
    public final int hashCode() {
        int result = super.hashCode();
        if (template != null)
            result = Constantes.PREMIER * result + template.hashCode();
        return result;
    }

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}
}
