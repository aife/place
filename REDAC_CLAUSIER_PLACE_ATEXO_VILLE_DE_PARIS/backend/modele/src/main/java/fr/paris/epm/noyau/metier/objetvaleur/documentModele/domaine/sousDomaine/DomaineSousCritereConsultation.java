package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineSousCritereConsultation {
	
    private String nomSousCritere;

    private Double ponderationSousCritere;
    
    public final String getNomSousCritere() {
        return nomSousCritere;
    }

    public final void setNomSousCritere(final String valeur) {
        this.nomSousCritere = valeur;
    }

    public final Double getPonderationSousCritere() {
        return ponderationSousCritere;
    }

    public final void setPonderationSousCritere(final Double valeur) {
        this.ponderationSousCritere = valeur;
    }
}
