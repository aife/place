package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Date;

/**
 * POJO hibernate Document.
 * @author Guillaume Béraudo
 * @version $Revision: 155 $, $Date: 2007-07-31 11:49:18 +0200 (mar., 31 juil. 2007) $, $Author: beraudo $
 */
public class EpmTDocument extends EpmTAbstractDocument {

    // Constantes
    /**
     * Constante, document du type pièce technique du DCE.
     */
    public static final String TYPE_DCE_TECHNIQUE = "pt";

    /**
     * Constante, document du type pièce administrative du DCE.
     */
    public static final String TYPE_DCE_ADMIN = "pa";

    /**
     * Constante, document du type pièce administrative validée dans rédaction.
     */
    public static final String TYPE_DCE_ADMIN_REDACTION = "par";
    /**
     * Constante pour une pièce provenant de rédaction et étant un règlement de
     * consultation.
     */
    public static final String TYPE_DCE_ADMIN_REDACTION_RC = "par_rc";
    
    /**
     * Constante pour une pièce etant le rapport d'estimation au moment de la 
     * preinscription d'une consultation.
     */
    public static final String TYPE_RAPPORT_ESTIMATION = "rapport_estimation";
    
    /**
     * Constante pour un document rapport disponible d'une consultation d'une reunion dans
     * l'ecran gestion des dossiers.
     */
    public static final String TYPE_RAPPORT_DISPONIBLE_SAUVEGARDE = "rapport_disponible_sauvegarde";
    
    /**
     * Constante pour un document rapport autre d'une consultation d'une reunion dans
     * l'ecran gestion des dossiers.
     */
    public static final String TYPE_RAPPORT_AUTRE = "rapport_autre";
    
    /**
     * Pièce incluse dans le projet de délibération.
     */
    public static final String TYPE_INCLUS_PROJET_DELIB = "projet_delib";

    /**
     * Visa de la direction financière pour la délibération.
     */
    public static final String TYPE_VISA_DIR_FINANC = "visa_dir_financ";

    /**
     * Projet de délibéré (délibération).
     */
    public static final String TYPE_PROJET_DELIB = "projetDelibere";

    /**
     * Type des motifs (délibération).
     */
    public static final String TYPE_EXPOSE_MOTIFS = "expose_motifs";

    /**
     * Type d'un document visa df dans suivi délibération.
     */
    public static final String TYPE_VISA_DF = "visaDF";

    /**
     * Type d'un document d'exposé des motifs dans suivi délibération.
     */
    public static final String TYPE_MOTIF = "motif";

    /**
     * Type d'un document ajouté lors de l'attribution du marché.
     */
    public static final String TYPE_MISE_AU_POINT = "miseAuPoint";

    /**
     * Type d'un document ajouté lors de l'attribution du marché.
     */
    public static final String TYPE_OFFRE_FINALE = "offreFinale";
    /**
     * Pli chiffré provenant de demat.
     */
    public static final String TYPE_PLI_CHIFFRE = "pliChiffre";

    /**
     * Pli deChiffré.
     */
    public static final String TYPE_PLI_DECHIFFRE = "pliDechiffre";
    
    /**
     * Pli chiffré provenant de demat.
     */
    public static final String TYPE_BLOC_PLI = "bloc_pli";
    
    /**
     * Bloc de plis d'acte d'engagement;
     */
    public static final String TYPE_BLOC_ACTE_ENGAGEMENT = "TYPE_BLOC_ACTE_ENGAGEMENT";
    
    /**
     * Le document MPS
     */
    public static final String TYPE_MPS = "MPS";
    
    /**
     * Statut d'une pièce du dce : excluse.
     */
    public static final String STATUT_EXCLUS_DCE = "exclus_dce";

    /**
     * Statut d'une piece du dce : incluse.
     */
    public static final String STATUT_PIECE_INCLUSE = "true";

    /**
     * Statut d'une piece du dce : non incluse.
     */
    public static final String STATUT_PIECE_REFUSEE = "false";

    /**
     * Suite à l'écran d'ajustement DCE.
     */
    public static final String STATUT_AJUSTEMENT_INCLUS_DCE = "inclus_dce";
    /**
     * Suite à l'écran d'ajustement DCE.
     */
    public static final String STATUT_AJUSTEMENT_EXCLUS_DCE = "exclus_dce";
    
    /**
     * Piece jointe aapc utilisé au niveau du choix de support de publicité.
     */
    public static final String AAPC_PIECE_JOINTE = "pice_jointe_aapc";
    
    /**
     * Type d'un document accompagné avec un fichier qui a besoin d'un certificat pour lire
     */
    public static final String TYPE_CERTIFICAT_SIGNATURE = "certificat_signature";
    
    /**
     * Type d'un document conservé dans l'objet contrat
     */
    public static final String TYPE_PIECE_OBJET_CONTRAT = "piece_objet_contrat";
    
    /**
     * Type d'un document administratif cloné attaché à un document conservé 
     */
    public static final String DOCUMENT_SUIVI_PA = "document_suivi_pa";
    
    /**
     * Type d'un document administratif cloné attaché à un document conservé 
     */
    public static final String DOCUMENT_SUIVI_PT = "document_suivi_pt";
    
    /**
     * Type d'un document de publcité envoyé cloné attaché à un document conservé 
     */
    public static final String DOCUMENT_SUIVI_PUB_ENVOYEE = "document_suivi_pub_envoyee";

    /*
    *  Type d'un document uploadé
    * */
    public static final String DOCUMENT_LIBRE = "document_libre";

    /*
    *  fichier xml reponseAnnonce
    * */
    public static final String REPONSE_ANNONCE = "reponse_annonce";

    /*
     *  Type d'un document uploadé depuis une application tierce
     * */
    public static final String APPLICATION_TIERCE = "application_tierce";

    // Prorpiétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Utilisateur.
     */
    private EpmTUtilisateur utilisateur;

    /**
     * Idejava.lang.Stringntifiant de la consultation.
     */
    private Integer idConsultation;

    /**
     * Idejava.lang.Stringntifiant du contrat
     */
    private Integer idContrat;

    /**
     * identifiant de l'avenant
     */
    private Integer idAvenant;
    
    /**
     * Référence.
     */
    private String reference;

    /**
     * Titre.
     */
    private String titre;

    /**
     * Type de document (RC, ...).
     */
    private String typeDocument;

    /**
     * Statut du document (parmis un ensemble de constantes).
     */
    private String statut;

    /**
     * Phase du document: phase1, phase2 ou phase 3.
     */
    private String phase;

    /**
     * Vrai si est inclus dans le projet de délibération amont.
     */
    private boolean delibAmont;

    /**
     * Vrai si est inclus dans le projet de délibération aval direction.
     */
    private boolean delibAvalDir;

    /**
     * Vrai si est inclus dans le projet de délibération aval DAJ.
     */
    private boolean delibAvalDaj;

    /**
     * Document de la consultation ou bien pour tous les lots; cf constantes.
     */
    private boolean multi;

    /**
     * Lot auquel est rattaché le document, éventuellement.
     */
    private EpmTBudLot lot;

    private Date dateLimiteRemisePliRC;
    /**
     * Statut suite à l'écran d'ajustement dce.
     */
    private String statutAjustementDCE;
    /**
     * Attribut modifié dès que le module redaction invalide une pièce provenant
     * du module redaction.
     */
    private boolean validiteRedaction;
    /**
     * id de type document point vers le referentiel : EpmTRefTypeDocumentContrat
     */
    private Integer idTypeDocument;

    /*
    *   Indique si le fichier provient de la base de donnée Redaction
    */
    private boolean issuRedaction;

    /*
    *  l'id du document source , au cas où le document est issu du module redaction
    */
    private Integer idDocumentRedaction;

    /**
     * Type du document selon l'application tierce
     */
    private String typeApplicationTiers;

    // Méthodes
    /**
     * Utilise uniquement la référence.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (reference != null) {
            result = Constantes.PREMIER + reference.hashCode();
        }
        return result;
    }

    /**
     * Deux objets de même référence sont égaux.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTDocument autre = (EpmTDocument) obj;
        if (reference == null) {
            if (autre.reference != null) {
                return false;
            }
        } else {
            if (!reference.equals(autre.reference)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @see java.lang.Object#toString()
     * @return référence du document
     */
    public final String toString() {
        return "Document de référence: " + reference;
    }

    // Accesseurs
    /**
     * @return consultation
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param valeur consultation
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @return référence
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param valeur référence
     */
    public void setReference(final String valeur) {
        this.reference = valeur;
    }

    /**
     * @return statut (parmi un ensemble de constantes)
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param valeur statut (parmi un ensemble de constantes)
     */
    public void setStatut(final String valeur) {
        this.statut = valeur;
    }

    /**
     * @return titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * @param valeur titre
     */
    public void setTitre(final String valeur) {
        this.titre = valeur;
    }

    /**
     * @return type de document (RC, ...)
     */
    public String getTypeDocument() {
        return typeDocument;
    }

    /**
     * @param valeur type de document (RC, ...)
     */
    public void setTypeDocument(final String valeur) {
        this.typeDocument = valeur;
    }

    /**
     * @return utilisateur
     */
    public EpmTUtilisateur getUtilisateur() {
        return utilisateur;
    }

    /**
     * @param valeur utilisateur
     */
    public void setUtilisateur(final EpmTUtilisateur valeur) {
        this.utilisateur = valeur;
    }

    /**
     * @return vrai si inclus dans le projet de délibération amont
     */
    public boolean isDelibAmont() {
        return delibAmont;
    }

    /**
     * @param valeur vrai si inclus dans le projet de délibération amont
     */
    public void setDelibAmont(final boolean valeur) {
        this.delibAmont = valeur;
    }

    /**
     * @return vrai si inclus dans le projet de délibération aval daj
     */
    public boolean isDelibAvalDaj() {
        return delibAvalDaj;
    }

    /**
     * @param valeur vrai si inclus dans le projet de délibération aval daj
     */
    public void setDelibAvalDaj(final boolean valeur) {
        this.delibAvalDaj = valeur;
    }

    /**
     * @return vrai si inclus dans le projet de délibération aval direction
     */
    public boolean isDelibAvalDir() {
        return delibAvalDir;
    }

    /**
     * @param valeur vrai si inclus dans le projet de délibération aval
     *            direction
     */
    public void setDelibAvalDir(final boolean valeur) {
        this.delibAvalDir = valeur;
    }

    /**
     * @return lot auquel est rattaché le document, éventuellement
     */
    public EpmTBudLot getLot() {
        return lot;
    }

    /**
     * @param valeur lot auquel est rattaché le document, éventuellement
     */
    public void setLot(final EpmTBudLot valeur) {
        this.lot = valeur;
    }

    /**
     * @return document de la consultation ou bien pour tous les lots; cf
     *         constantes
     */
    public boolean isMulti() {
        return multi;
    }

    /**
     * @param valeur document de la consultation ou bien pour tous les lots; cf
     *            constantes
     */
    public void setMulti(final boolean valeur) {
        this.multi = valeur;
    }

    /**
     * @return la date limite de remise des plis dans le cas d'une RC.
     */
    public Date getDateLimiteRemisePliRC() {
        return dateLimiteRemisePliRC;
    }

    /**
     * @param valeur la date limite de remise des plis dans le cas d'une RC.
     */
    public void setDateLimiteRemisePliRC(final Date valeur) {
        this.dateLimiteRemisePliRC = valeur;
    }

    /**
     * @return phase du document: phase1, phase2 ou les deux
     */
    public String getPhase() {
        return phase;
    }

    /**
     * @param valeur phase du document: phase1, phase2 ou les deux
     */
    public void setPhase(final String valeur) {
        this.phase = valeur;
    }

    public Integer getIdAvenant() {
        return idAvenant;
    }

    public void setIdAvenant(final Integer valeur) {
        this.idAvenant = valeur;
    }

    /**
     * @return le statut du document suite à l'ajustement du dce.
     */
    public String getStatutAjustementDCE() {
        return statutAjustementDCE;
    }

    /**
     * @param valeur le statut du document suite à l'ajustement du dce.
     */
    public void setStatutAjustementDCE(final String valeur) {
        this.statutAjustementDCE = valeur;
    }

    /**
     * @return Attribut modifié dès que le module redaction invalide une pièce
     *         provenant du module redaction.
     */
    public boolean isValiditeRedaction() {
        return validiteRedaction;
    }

    /**
     * @param valeur Attribut modifié dès que le module redaction invalide une
     *            pièce provenant du module redaction.
     */
    public void setValiditeRedaction(final boolean valeur) {
        this.validiteRedaction = valeur;
    }

    /**
     * @return the idTypeDocument
     */
    public Integer getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(Integer value) {
        this.idTypeDocument = value;
    }

    public boolean isIssuRedaction() {
        return issuRedaction;
    }

    public void setIssuRedaction(boolean issuRedaction) {
        this.issuRedaction = issuRedaction;
    }

    public Integer getIdDocumentRedaction() {
        return idDocumentRedaction;
    }

    public void setIdDocumentRedaction(Integer idDocumentRedaction) {
        this.idDocumentRedaction = idDocumentRedaction;
    }

    public String getTypeApplicationTiers() {
        return typeApplicationTiers;
    }

    public void setTypeApplicationTiers(String typeApplicationTiers) {
        this.typeApplicationTiers = typeApplicationTiers;
    }

    public Integer getIdContrat() {
        return idContrat;
    }

    public void setIdContrat(Integer idContrat) {
        this.idContrat = idContrat;
    }

}
