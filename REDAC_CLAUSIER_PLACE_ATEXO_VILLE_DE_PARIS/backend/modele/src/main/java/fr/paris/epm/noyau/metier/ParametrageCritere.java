package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.Map;

/**
 * Classe critère utilisé pour la recherche de parametrage.
 * @author Rémi Villé
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ParametrageCritere extends AbstractCritere implements Critere, Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 765599478321140537L;

    private String clef;

    private Boolean actif;

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();

        sb.append("from EpmTRefParametrage ");

        if (clef != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" clef = :clef");
            parametres.put("clef", clef);
            debut = true;
        }

        if (actif != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("actif = :actif");
            getParametres().put("actif", actif);
            debut = true;
        }

        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by ").append(proprieteTriee);
            if (triCroissant) {
                sb.append(" ASC ");
            } else {
                sb.append(" DESC ");
            }
        }

        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer sb = new StringBuffer("select count(*) ");
        sb.append(corpsRequete());
        return sb.toString();
    }

    public final void setClef(final String valeur) {
        this.clef = valeur;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }
}