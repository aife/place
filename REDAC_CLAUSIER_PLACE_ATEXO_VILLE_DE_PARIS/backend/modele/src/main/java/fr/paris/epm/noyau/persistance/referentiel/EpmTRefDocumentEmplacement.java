package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDocumentEmplacement de la table "epm__t_ref_document_emplacement"
 * Permet d'associer un modèle de document à un lien de l'application EPM.
 * Lors du clic sur un lien l'application recherchera tous les documents liés
 * à ce lien pour les générer.
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDocumentEmplacement extends EpmTReferentielSimpleAbstract {
    
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Description de la page de l'application sur laquelle sera généré le document.
     */
    private String description;
    
    
    /**
     * réference de l'emplacement. la recherche se fera sur cette référence.
     */
    private String reference;
    
    /**
     * Defini si le concerne à un champ de fusion du module execution
     */
    private boolean moduleExecution;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public boolean isModuleExecution() {
		return moduleExecution;
	}

	public void setModuleExecution(boolean moduleExecution) {
		this.moduleExecution = moduleExecution;
	}

}