package fr.paris.epm.noyau.persistance;

import java.util.Set;

/**
 * Pojo Hibernate gérant les propriétés de la consultation non définie dans RSEM
 * @author MGA
 *
 */
public class EpmTValeurConditionnementExterneComplexe extends EpmTAbstractObject {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant des paires type complexe
     */
    private String idObjetComplexe;

    private Set<EpmTValeurConditionnementExterne> epmTValeurConditionnementExternes;

    /**
     * @return the idObjetComplexe
     */
    public String getIdObjetComplexe() {
        return idObjetComplexe;
    }
    /**
     * @param idObjetComplexe the idObjetComplexe to set
     */
    public void setIdObjetComplexe(final String valeur) {
        this.idObjetComplexe = valeur;
    }

    /**
     * @return the epmTValeurConditionnementExternes
     */
    public Set<EpmTValeurConditionnementExterne> getEpmTValeurConditionnementExternes() {
        return epmTValeurConditionnementExternes;
    }
    /**
     * @param epmTValeurConditionnementExternes the epmTValeurConditionnementExternes to set
     */
    public void setEpmTValeurConditionnementExternes(final Set<EpmTValeurConditionnementExterne> valeur) {
        this.epmTValeurConditionnementExternes = valeur;
    }

}
