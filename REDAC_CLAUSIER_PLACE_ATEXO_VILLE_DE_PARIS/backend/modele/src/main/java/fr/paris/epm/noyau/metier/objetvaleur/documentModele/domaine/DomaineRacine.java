package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution.DomaineContrat;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document. Contient tous les domaines
 * de niveau 1.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRacine implements Serializable {

    /**
	 * Marqueur de serialisation
	 */
	private static final long serialVersionUID = 1L;

	private DomaineConsultation consultation;

    private DomaineDirectionServices directionService;

    private DomaineEntreprises entreprise;

    /**
     * Sous-domaine : analyse des plis / Dépouillement.
     */
    private DomaineAnalyseDepouillementPlis analyseDepouillementPlis;

    /**
     * Cas de mono-attributaire
     */
    private DomaineAttribution attribution;

    /**
     * Cas de multi-attributaires
     */
    private List<DomaineAttribution> attributions;

    private DomaineDates dates;

    private DomaineAvenant avenant;

	/**
	 * Domaine contenant des données du contrat du module exécution
	 */
	private DomaineContrat contrat;

    public final DomaineConsultation getConsultation() {
        return consultation;
    }

    public final void setConsultation(final DomaineConsultation consultation) {
        this.consultation = consultation;
    }

    public final DomaineDirectionServices getDirectionService() {
        return directionService;
    }

    public final void setDirectionService(final DomaineDirectionServices valeur) {
        this.directionService = valeur;
    }

    public final DomaineEntreprises getEntreprise() {
        return entreprise;
    }

    public final void setEntreprise(final DomaineEntreprises valeur) {
        this.entreprise = valeur;
    }

    /**
     * @return Sous-domaine : analyse des plis / Dépouillement.
     */
    public final DomaineAnalyseDepouillementPlis getAnalyseDepouillementPlis() {
        return analyseDepouillementPlis;
    }

    /**
     * @param valeur Sous-domaine : analyse des plis / Dépouillement.
     */
    public final void setAnalyseDepouillementPlis(final DomaineAnalyseDepouillementPlis valeur) {
        this.analyseDepouillementPlis = valeur;
    }

    public final DomaineAttribution getAttribution() {
        return attribution;
    }

    /**
     * @param valeur the attribution to set
     */
    public final void setAttribution(final DomaineAttribution valeur) {
        this.attribution = valeur;
    }

    /**
     * @return the attributions
     */
    public final List<DomaineAttribution> getAttributions() {
        return attributions;
    }

    /**
     * @param valeur the attributions to set
     */
    public final void setAttributions(final List<DomaineAttribution> valeur) {
        this.attributions = valeur;
    }

    public final DomaineDates getDates() {
        return dates;
    }

    public final void setDates(final DomaineDates valeur) {
        this.dates = valeur;
    }

    public final DomaineAvenant getAvenant() {
        return avenant;
    }

    public final void setAvenant(final DomaineAvenant valeur) {
        this.avenant = valeur;
    }

	/**
	 * @return les données du domaine du contrat du module exécution
	 */
	public final DomaineContrat getContrat() {
		return contrat;
	}

	/**
	 * @param valeur : les données du domaine du contrat du module exécution
	 */
	public final void setContrat(final DomaineContrat valeur) {
		this.contrat = valeur;
	}

}
