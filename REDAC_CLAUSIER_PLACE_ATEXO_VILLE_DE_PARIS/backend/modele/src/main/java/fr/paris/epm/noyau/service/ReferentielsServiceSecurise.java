package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Interface du service Référentiels. Ce service est utilisé pour récupérer
 * l'intégralité des référentiels en base.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface ReferentielsServiceSecurise extends GeneriqueServiceSecurise {

    /**
     * Agrège la totalité des référentiels en base de donnée. Une transaction
     * est effectuée pour chaque récupération de référentiel (une dizaine).
     * @return POJO Referentiels stockant chaque collection de référentiels
     */
    Referentiels getAllReferentiels();

	/**
	 * Recharge la totalité des référentiels.
	 *
	 * @throws TechnicalNoyauException erreur technique
	 * @throws NonTrouveNoyauException NonTrouveNoyauException
	 */
	Referentiels rechargerAllReferentiels();

    Collection<EpmTUtilisateur> getAllUtilisateurs();

    EpmTRefOrganisme getOrganismeById(Integer idOrganisme);

	List<EpmTRefProcedure> getProcedureByIdOrganisme(Integer idOrganisme);

	List<EpmTRefProcedure> getAllProcedures();

	Date getDateDerniereMiseAjour();

	Date getDateDerniereModification();
}
