package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefChampFusion de la table "epm__t_ref_champ_fusion"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefChampFusion extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Domaine de données associé au champ de fusion.
     */
    private EpmTRefDomaineDonnees domaineDonnees;

    /**
     * Discriminant permettant de savoir si il s'agit d'un champ de fusion simple ou complexe.
     */
    private String discriminant;

    /**
     * Valeur du champ de fusion. chemin d'un objet java dans le cas d'un champ
     * de fusion simple, expression freemarker dans le cas d'un champ de fusion
     * complexe.
     */
    private String expression;

    /**
     * Description du champ de fusion.
     */
    private String description;

    /**
     * Defini si le concerne à un champ de fusion du module execution
     */
    private boolean moduleExecution;

    /**
     * Defini le type du script
     */
    private String scriptType;

    public EpmTRefDomaineDonnees getDomaineDonnees() {
        return domaineDonnees;
    }

    public void setDomaineDonnees(EpmTRefDomaineDonnees domaineDonnees) {
        this.domaineDonnees = domaineDonnees;
    }

    public String getDiscriminant() {
        return discriminant;
    }

    public void setDiscriminant(String discriminant) {
        this.discriminant = discriminant;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isModuleExecution() {
        return moduleExecution;
    }

    public void setModuleExecution(boolean moduleExecution) {
        this.moduleExecution = moduleExecution;
    }

    public String getScriptType() {
        return scriptType;
    }

    public void setScriptType(String scriptType) {
        this.scriptType = scriptType;
    }

}