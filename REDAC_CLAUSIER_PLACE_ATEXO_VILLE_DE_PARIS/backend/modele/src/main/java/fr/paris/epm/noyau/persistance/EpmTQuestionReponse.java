package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Date;

/**
 * POJO hibernate représentant les questions/réponse posé concernant un
 * appel d'offre.
 * @author Régis Menet
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTQuestionReponse extends EpmTAbstractObject {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String TYPE_AUTRE = "Autre";

    public static final String TYPE_DEMAT = "Demat";

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * date du dépot.
     */
    private Date dateDepot;

    /**
     * date de la réponse.
     */
    private Date dateReponse;

    /**
     * type de la réponse.
     */
    private String typeReponse;

    /**
     * Contenu de la question.
     */
    private String question;

    /**
     * répondu par.
     */
    private String reponsePar;

    /**
     * entreprise ayant posé la question.
     */
    private String entreprise;

    /**
     * identité de la personne ayant posé la question.
     */
    private String identite;

    /**
     * email de la personne ou de l'entreprise.
     */
    private String email;

    /**
     * adresse de l'entreprise.
     */
    private String adresse;

    /**
     * code postal de l'entreprise.
     */
    private String codePostal;

    /**
     * ville de l'entreprise.
     */
    private String ville;

    /**
     * fax del'entreprise.
     */
    private String fax;
    /**
     * Commentaire lié à la question/reponse.
     */
    private String commentaire;
    /**
     * Consultation liée aux questions / réponses.
     */
    private int consultation;

    /**
     * Marqueur de phase {@link Constantes} PHASE_.
     */
    private String phase;
    
    /**
     * Fichier joint a la question.
     */
    private EpmTDocument epmTDocument;
    
    /**
     * identifiant de registre de question dans MPE associé
     */
    private Integer idRegistreDemat;
    
    /**
     * @return adresse de l'entreprise
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param valeur adresse de l'entreprise
     */
    public void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    /**
     * @return code postal de l'entreprise
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param valeur code postal de l'entreprise
     */
    public void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    /**
     * @return date du dépot de la question
     */
    public Date getDateDepot() {
        return dateDepot;
    }

    /**
     * @param valeur date du dépot de la question
     */
    public void setDateDepot(final Date valeur) {
        this.dateDepot = valeur;
    }

    public Date getDateReponse() {
        return dateReponse;
    }

    /**
     * @param valeur date dela réponse de la question
     */
    public void setDateReponse(final Date valeur) {
        this.dateReponse = valeur;
    }

    /**
     * @return email de la personne ayant posé la question
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param valeur email de la personne ayant posé la question
     */
    public void setEmail(final String valeur) {
        this.email = valeur;
    }

    /**
     * @return entreprise ayant posé la question
     */
    public String getEntreprise() {
        return entreprise;
    }

    /**
     * @param valeur entreprise ayant posé la question
     */
    public void setEntreprise(final String valeur) {
        this.entreprise = valeur;
    }

    /**
     * @return fax de l'entreprise
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param valeur fax de l'entreprise
     */
    public void setFax(final String valeur) {
        this.fax = valeur;
    }

    /**
     * @return identifiant de l'enregistrement.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement.
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return
     */
    public String getIdentite() {
        return identite;
    }

    /**
     * @param valeur
     */
    public void setIdentite(final String valeur) {
        this.identite = valeur;
    }

    /**
     * @return contenu de la question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * @param valeur contenu de la question
     */
    public void setQuestion(final String valeur) {
        this.question = valeur;
    }

    /**
     * @return répondu par
     */
    public String getReponsePar() {
        return reponsePar;
    }

    /**
     * @param valeur repondu par
     */
    public void setReponsePar(final String valeur) {
        this.reponsePar = valeur;
    }

    /**
     * @return type de reponse (papier ou electronique)
     */
    public String getTypeReponse() {
        return typeReponse;
    }

    /**
     * @param valeur type de réponse papier ou electronique
     */
    public void setTypeReponse(final String valeur) {
        this.typeReponse = valeur;
    }

    /**
     * @return ville de l'entrepise ayant posé la question
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param valeur ville de l'entreprise ayant posé la question
     */
    public void setVille(final String valeur) {
        this.ville = valeur;
    }

    /**
     * @return le commentaire lié à la question réponse.
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @param valeur le commentaire lié à la question réponse.
     */
    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    /**
     * @return la consultation liée aux questions / réponses.
     */
    public int getConsultation() {
        return consultation;
    }

    /**
     * @param valeur la consultation liée aux questions / réponses.
     */
    public void setConsultation(final int valeur) {
        this.consultation = valeur;
    }

    /**
     * @return marqueur de phase {@link Constantes} PHASE_
     */
    public String getPhase() {
        return phase;
    }

    /**
     * @param valeur marqueur de phase {@link Constantes} PHASE_
     */
    public void setPhase(final String valeur) {
        this.phase = valeur;
    }
    /**
     * @return {@link EpmTQuestionReponse#epmTDocument}
     */
    public EpmTDocument getEpmTDocument() {
        return epmTDocument;
    }
    /**
     * @param valeur {@link EpmTQuestionReponse#epmTDocument}
     */
    public void setEpmTDocument(final EpmTDocument valeur) {
        this.epmTDocument = valeur;
    }

	public Integer getIdRegistreDemat() {
		return idRegistreDemat;
	}

	public void setIdRegistreDemat(Integer valeur) {
		this.idRegistreDemat = valeur;
	}
    
    
}
