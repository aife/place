package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefCivilite;

import java.util.Date;

/**
 * Cette classe contient la description du personnel d'une entreprise répondant
 * à un appel d'offre.
 * 
 * @author Diop cheikh
 * @version $Revision:$, $Date: $, $Author: $
 */
public abstract class AbstractPersonnelEntreprise extends EpmTAbstractObject implements Comparable {
    
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * nom du personne.
     */
    private String nom;

    /**
     * prénom de la personne.
     */
    private String prenom;

    /**
     * email de la personne.
     */
    private String email;

    /**
     * numéro de son téléphone fixe.
     */
    private String telephoneFixe;

    /**
     * numero de son téléphone mobile.
     */
    private String telephoneMobile;

    /**
     * numéro de son fax.
     */
    private String fax;

    /**
     * addresse de la personne.
     */
    private String adresse;

    /**
     * fonction de la personne.
     */
    private String fonction;
    /**
     * code postal de la personne.
     */
    private String codePostal;
    /**
     * ville de la personne.
     */
    private String ville;

    /**
     * pays de la personne.
     */
    private String pays;
    /**
     * numéro de siret de la personne.
     */
    private String siret;

    /**
     * commentaires associé à la personne.
     */
    private String commentaires;

    /**
     * date de création.
     */
    private Date dateCreationFiche;

    /**
     * date de modification.
     */
    private Date dateModificationFiche;

    /**
     * Nom de l'établissement.
     */
    private String nomEtablissement;
    /**
     * Code de l'établissement.
     */
    private String codeEtablissement;
    /**
     * Civilite.
     */
    private EpmTRefCivilite civilite;

    // Accesseurs
    /**
     * @return adresse de la personne
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param valeur
     *            adresse de la personne
     */
    public void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    /**
     * @return email de la personne
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param valeur
     *            email de la personne
     */
    public void setEmail(final String valeur) {
        this.email = valeur;
    }

    /**
     * @return fax de la personne
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param valeur
     *            fax de la personne
     */
    public void setFax(final String valeur) {
        this.fax = valeur;
    }

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur
     *            identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return nom de la personne
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur
     *            nom de la personne
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return prénom de la personne
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param valeur
     *            prénom de la personne
     */
    public void setPrenom(final String valeur) {
        this.prenom = valeur;
    }

    /**
     * @return téléphone fixe de la personne
     */
    public String getTelephoneFixe() {
        return telephoneFixe;
    }

    /**
     * @param valeur
     *            téléphone fixe de la personne
     */
    public void setTelephoneFixe(final String valeur) {
        this.telephoneFixe = valeur;
    }

    /**
     * @return téléphone mobile de la personne
     */
    public String getTelephoneMobile() {
        return telephoneMobile;
    }

    /**
     * @param valeur
     *            téléphone mobile de la personne
     */
    public void setTelephoneMobile(final String valeur) {
        this.telephoneMobile = valeur;
    }

    /**
     * Cette méthode affiche le contenu de l'objet.
     * 
     * @return String
     */
    public String toString() {
        StringBuffer res = new StringBuffer(super.toString());
        res.append("\nIdentifiant: ");
        res.append(id);
        res.append("\n nom: ");
        res.append(nom);
        res.append("\nprenom: ");
        res.append(prenom);
        res.append("\nemail : ");
        res.append(email);
        res.append("\ntelephone fixe: ");
        res.append(telephoneFixe);
        res.append("\ntelephone mobile: ");
        res.append(telephoneMobile);
        res.append("\n fax : ");
        res.append(fax);
        res.append("\n adresse: ");
        res.append(adresse);
        return res.toString();
    }

    /**
     * @return code postal associé a l'adresse
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param valeur
     *            code postal associé à l'adresse
     */
    public void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    /**
     * @return date de la création de l'enregistrement
     */
    public Date getDateCreationFiche() {
        return dateCreationFiche;
    }

    /**
     * @param valeur
     *            date de la création de l'enregistrement
     */
    public void setDateCreationFiche(final Date valeur) {
        this.dateCreationFiche = valeur;
    }

    /**
     * @return dernière modification de l'enregistrement
     */
    public Date getDateModificationFiche() {
        return dateModificationFiche;
    }

    /**
     * @param valeur
     *            dernière modification de l'enregistrement
     */
    public void setDateModificationFiche(final Date valeur) {
        this.dateModificationFiche = valeur;
    }

    /**
     * @return fonction de la personne au sein de l'entreprise
     */
    public String getFonction() {
        return fonction;
    }

    /**
     * @param valeur
     *            fonction de la personne au sein de l'entreprise
     */
    public void setFonction(final String valeur) {
        this.fonction = valeur;
    }

    /**
     * @return pays associé à l'adresses
     */
    public String getPays() {
        return pays;
    }

    /**
     * @param valeur
     *            pays associé à l'adresse
     */
    public void setPays(final String valeur) {
        this.pays = valeur;
    }

    /**
     * @return SIRET de la personne
     */
    public String getSiret() {
        return siret;
    }

    /**
     * @param valeur
     *            SIRET de la personne
     */
    public void setSiret(final String valeur) {
        this.siret = valeur;
    }

    /**
     * @return ville associé à l'adresse
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param valeur
     *            ville associé à l'adresse
     */
    public void setVille(final String valeur) {
        this.ville = valeur;
    }

    /**
     * @return commentaire sur la personne
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     * @param valeur
     *            commentaire sur la personne
     */
    public void setCommentaires(final String valeur) {
        this.commentaires = valeur;
    }

    /**
     * @return le code de l'établissement.
     */
    public String getCodeEtablissement() {
        return codeEtablissement;
    }

    /**
     * @param valeur
     *            code de l'établissement
     */
    public void setCodeEtablissement(String valeur) {
        this.codeEtablissement = valeur;
    }

    /**
     * @return le nom de l'établissement.
     */
    public String getNomEtablissement() {
        return nomEtablissement;
    }

    /**
     * @param valeur
     *            nom de l'établissement
     */
    public void setNomEtablissement(String valeur) {
        this.nomEtablissement = valeur;
    }

    /**
     * @return la civilité du contact.
     */
    public EpmTRefCivilite getCivilite() {
        return civilite;
    }

    /**
     * @param valeur
     *            la civilité du contact entreprise.
     */
    public void setCivilite(EpmTRefCivilite valeur) {
        this.civilite = valeur;
    }

}
