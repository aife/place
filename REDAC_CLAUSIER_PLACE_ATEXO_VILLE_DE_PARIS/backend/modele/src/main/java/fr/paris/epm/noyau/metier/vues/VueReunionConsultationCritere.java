package fr.paris.epm.noyau.metier.vues;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;

import java.io.Serializable;
import java.util.List;

/**
 * Classe critère utilisée pour la recherche de réunions de commission.
 * @author Regis menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class VueReunionConsultationCritere extends AbstractCritere implements Critere, Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private Integer idReunionInscrit;
    
    private Boolean consultationInscrit;
    
    /**
     * Liste des identifiants de reunion.
     */
    private List<Integer> reunionIdList;

    /**
     * @return corps de la requête HQL
     */
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        if (idReunionInscrit != null) {    
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("(c.reunionHerite=");
            sb.append(idReunionInscrit);
            sb.append(" OR ");
            sb.append("c.reunionHerite = null and c.reunionPrevue = ");
            sb.append(idReunionInscrit);
            sb.append(")");
            debut = true;
        }
        if (consultationInscrit != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.consultationInscrite = ");
            sb.append(consultationInscrit);
            debut = true;
        }
        if(reunionIdList != null) {
        	if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
        	StringBuffer tmp = new StringBuffer("(");
        	boolean firstId = true;
        	for(int reunionId : reunionIdList) {
        		if(!firstId) {
        		    tmp.append(", ");
        		} else {
        			firstId = false;
        		}	
        		tmp.append(reunionId);
        	}
        	tmp.append(")");
            sb.append("(c.reunionHerite in " ).append(tmp);
            sb.append(" OR ");
            sb.append("c.reunionHerite = null and c.reunionPrevue in ");
            sb.append(tmp);
            sb.append(")");
        	debut = true;
        }
        return sb;
    }

    /**
     * @return chaine HQL utilisée par Hibernate
     */
    public final String toHQL() {
        StringBuffer req = new StringBuffer("from EpmVReunionConsultation as c");
        req.append(corpsRequete());
        if (proprieteTriee != null) {
            req.append(" order by c.");
            req.append(proprieteTriee);
            if (triCroissant) {
                req.append(" ASC ");
            } else {
                req.append(" DESC ");
            }
        }
        return req.toString();
    }

    public String toCountHQL() {
        return "select count(*) from EpmVReunionConsultation as c " + corpsRequete();
    }

    /**
     * 
     * @param valeur id reunion 
     */
    public final void setIdReunionInscrit(final Integer valeur) {
        this.idReunionInscrit = valeur;
    }

    public final void setConsultationInscrit(final Boolean valeur) {
        this.consultationInscrit = valeur;
    }

    /**
     * @param valeur la liste des identifiants de reunion.
     */
	public final void setReunionIdList(List<Integer> valeur) {
		this.reunionIdList = valeur;
	}
}
