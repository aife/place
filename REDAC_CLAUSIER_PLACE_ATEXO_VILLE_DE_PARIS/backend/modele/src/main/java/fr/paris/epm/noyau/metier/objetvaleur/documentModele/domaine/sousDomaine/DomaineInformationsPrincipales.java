package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * consultation.informationsPrincipales.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineInformationsPrincipales implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    private String numeroConsultation;

    private String dureeMarche;

    private Integer nbCandidatsAdmisPresenterOffre;

    private List<DomaineCritereConsultation> listeCriteres;
    /**
     * DCE mis en ligne.
     */
    private Boolean dCEenLigneON;

    private String codeAchatConsultation;
    /**
     * Delai de validite des offres (en jours).
     */
    private Integer delaiValiditeOffres;

    private String typeCriteresAttribution;

    private String codeRestreintDCE;

    private List<String> cPVConsultation;

    private String delaiExecution;

    private String libelleCritereAttribution;

    private String ponderationCritereAttribution;

    private String formeGroupement;

    private String ccagDeReference;
    private String varianteAutorise;
    private String varianteObligatoire;
    private String reconduction;
    private String modalitesReconduction;
    private String  formePrix ;
    private String   pfestimationHt ;
    private String  pfEstimationTtc ;
    private String  pfDateValeur ;
    private String  BonQuantite;
    private String   MinMax;
    private String   puMinHt ;
    private String  puMaxHt ;
    private String   puMinTtc;
    private String  puMaxTtc ;
    private String   puEstimationHt ;
    private String   puEstimationTtc ;
    private String  puDateValeur ;
    private Boolean clausesSocialesON;
    private Boolean clausesEnvironnementalesON;
    private String consultationAllotieON;
    private String reponseElectroniqueON;
    private String justificationNonAllotissement;
    private List<String> listreservationMarche;
    private String aapcPresence;

    /**
     * @return
     */
    public String getAapcPresence() {
        return aapcPresence;
    }

    /**
     * @param aapcPresence
     */
    public void setAapcPresence(String aapcPresence) {
        this.aapcPresence = aapcPresence;
    }

    /**
     * @return
     */
    public List<String> getListreservationMarche() {
        return listreservationMarche;
    }

    /**
     * @param listreservationMarche
     */
    public void setListreservationMarche(List<String> listreservationMarche) {
        this.listreservationMarche = listreservationMarche;
    }

    /**
     * justification de non allotissement.
     *
     * @return
     */
    public String getJustificationNonAllotissement() {
        return justificationNonAllotissement;
    }

    /**
     * @param justificationNonAllotissement : justification de non allotissement.
     */
    public void setJustificationNonAllotissement(String justificationNonAllotissement) {
        this.justificationNonAllotissement = justificationNonAllotissement;
    }

    /**
     * Réponse electronique.
     *
     * @return
     */
    public String getReponseElectroniqueON() {
        return reponseElectroniqueON;
    }

    /**
     * @param reponseElectroniqueON : Réponse electronique.
     */
    public void setReponseElectroniqueON(String reponseElectroniqueON) {
        this.reponseElectroniqueON = reponseElectroniqueON;
    }

    /**
     * Allotissement consultation.
     *
     * @return
     */
    public String getConsultationAllotieON() {
        return consultationAllotieON;
    }

    /**
     * @param consultationAllotieON : Allotissement consultation.
     */
    public void setConsultationAllotieON(String consultationAllotieON) {
        this.consultationAllotieON = consultationAllotieON;
    }

    public final String getNumeroConsultation() {
        return numeroConsultation;
    }

    public final void setNumeroConsultation(final String valeur) {
        this.numeroConsultation = valeur;
    }

    public final String getDureeMarche() {
        return dureeMarche;
    }

    public final void setDureeMarche(final String valeur) {
        this.dureeMarche = valeur;
    }

    public final Integer getNbCandidatsAdmisPresenterOffre() {
        return nbCandidatsAdmisPresenterOffre;
    }

    public final void setNbCandidatsAdmisPresenterOffre(final Integer valeur) {
        this.nbCandidatsAdmisPresenterOffre = valeur;
    }

    public final List<DomaineCritereConsultation> getListeCriteres() {
        return listeCriteres;
    }

    public final void setListeCriteres(
            final List<DomaineCritereConsultation> valeur) {
        this.listeCriteres = valeur;
    }

    /**
     * @return Delai de validite des offres (en jours).
     */
    public final Integer getDelaiValiditeOffres() {
        return delaiValiditeOffres;
    }

    /**
     * @param valeur Delai de validite des offres (en jours).
     */
    public final void setDelaiValiditeOffres(final Integer valeur) {
        this.delaiValiditeOffres = valeur;
    }

    /**
     * @return DCE mis en ligne.
     */
    public final Boolean getDCEenLigneON() {
        return dCEenLigneON;
    }

    /**
     * @param valeur DCE mis en ligne.
     */
    public final void setDCEenLigneON(final Boolean valeur) {
        this.dCEenLigneON = valeur;
    }

    public final String getCodeAchatConsultation() {
        return codeAchatConsultation;
    }

    public final void setCodeAchatConsultation(final String valeur) {
        this.codeAchatConsultation = valeur;
    }

    public final String getTypeCriteresAttribution() {
        return typeCriteresAttribution;
    }

    public final void setTypeCriteresAttribution(final String valeur) {
        this.typeCriteresAttribution = valeur;
    }

    public final String getCodeRestreintDCE() {
        return codeRestreintDCE;
    }

    public final void setCodeRestreintDCE(final String valeur) {
        this.codeRestreintDCE = valeur;
    }

    /**
     * @return the cPVConsultation
     */
    public final List<String> getcPVConsultation() {
        return cPVConsultation;
    }

    /**
     * @param cPVConsultation the cPVConsultation to set
     */
    public final void setcPVConsultation(final List<String> valeur) {
        this.cPVConsultation = valeur;
    }

    /**
     * @return the delaiExecution
     */
    public final String getDelaiExecution() {
        return delaiExecution;
    }

    /**
     * @param delaiExecution the delaiExecution to set
     */
    public final void setDelaiExecution(final String valeur) {
        this.delaiExecution = valeur;
    }

    /**
     * @return the libelleCritereAttribution
     */
    public final String getLibelleCritereAttribution() {
        return libelleCritereAttribution;
    }

    /**
     * @param libelleCritereAttribution the libelleCritereAttribution to set
     */
    public final void setLibelleCritereAttribution(final String valeur) {
        this.libelleCritereAttribution = valeur;
    }

    /**
     * @return the ponderationCritereAttribution
     */
    public final String getPonderationCritereAttribution() {
        return ponderationCritereAttribution;
    }

    /**
     * @param ponderationCritereAttribution the ponderationCritereAttribution to set
     */
    public final void setPonderationCritereAttribution(final String valeur) {
        this.ponderationCritereAttribution = valeur;
    }

    /**
     * @return the formeGroupement
     */
    public final String getFormeGroupement() {
        return formeGroupement;
    }

    /**
     * @param formeGroupement the formeGroupement to set
     */
    public final void setFormeGroupement(final String valeur) {
        this.formeGroupement = valeur;
    }


    public String getPuDateValeur() {
        return puDateValeur;
    }

    public void setPuDateValeur(String puDateValeur) {
        this.puDateValeur = puDateValeur;
    }

    public String getPuEstimationTtc() {
        return puEstimationTtc;
    }

    public void setPuEstimationTtc(String puEstimationTtc) {
        this.puEstimationTtc = puEstimationTtc;
    }

    public String getPuMaxTtc() {
        return puMaxTtc;
    }

    public void setPuMaxTtc(String puMaxTtc) {
        this.puMaxTtc = puMaxTtc;
    }

    public String getPuEstimationHt() {
        return puEstimationHt;
    }

    public void setPuEstimationHt(String puEstimationHt) {
        this.puEstimationHt = puEstimationHt;
    }

    public String getPuMinTtc() {
        return puMinTtc;
    }

    public void setPuMinTtc(String puMinTtc) {
        this.puMinTtc = puMinTtc;
    }

    public String getPuMaxHt() {
        return puMaxHt;
    }

    public void setPuMaxHt(String puMaxHt) {
        this.puMaxHt = puMaxHt;
    }

    public String getMinMax() {
        return MinMax;
    }

    public void setMinMax(String minMax) {
        MinMax = minMax;
    }

    public String getPuMinHt() {
        return puMinHt;
    }

    public void setPuMinHt(String puMinHt) {
        this.puMinHt = puMinHt;
    }

    public String getFormePrix() {
        return formePrix;
    }

    public void setFormePrix(String formePrix) {
        this.formePrix = formePrix;
    }

    public String getPfestimationHt() {
        return pfestimationHt;
    }

    public void setPfestimationHt(String pfestimationHt) {
        this.pfestimationHt = pfestimationHt;
    }

    public String getPfEstimationTtc() {
        return pfEstimationTtc;
    }

    public void setPfEstimationTtc(String pfEstimationTtc) {
        this.pfEstimationTtc = pfEstimationTtc;
    }

    public String getPfDateValeur() {
        return pfDateValeur;
    }

    public void setPfDateValeur(String pfDateValeur) {
        this.pfDateValeur = pfDateValeur;
    }

    public String getBonQuantite() {
        return BonQuantite;
    }

    public void setBonQuantite(String bonQuantite) {
        BonQuantite = bonQuantite;
    }

    public String getCcagDeReference() {
        return ccagDeReference;
    }

    public void setCcagDeReference(String ccagDeReference) {
        this.ccagDeReference = ccagDeReference;
    }

    public String getVarianteAutorise() {
        return varianteAutorise;
    }

    public void setVarianteAutorise(String varianteAutorise) {
        this.varianteAutorise = varianteAutorise;
    }

    public String getVarianteObligatoire() {
        return varianteObligatoire;
    }

    public void setVarianteObligatoire(String varianteObligatoire) {
        this.varianteObligatoire = varianteObligatoire;
    }

    public String getReconduction() {
        return reconduction;
    }

    public void setReconduction(String reconduction) {
        this.reconduction = reconduction;
    }

    public String getModalitesReconduction() {
        return modalitesReconduction;
    }

    public void setModalitesReconduction(String modalitesReconduction) {
        this.modalitesReconduction = modalitesReconduction;
    }

    public Boolean getClausesSocialesON() {
        return clausesSocialesON;
    }

    public void setClausesSocialesON(Boolean clausesSocialesON) {
        this.clausesSocialesON = clausesSocialesON;
    }

    public Boolean getClausesEnvironnementalesON() {
        return clausesEnvironnementalesON;
    }

    public void setClausesEnvironnementalesON(Boolean clausesEnvironnementalesON) {
        this.clausesEnvironnementalesON = clausesEnvironnementalesON;
    }

}
