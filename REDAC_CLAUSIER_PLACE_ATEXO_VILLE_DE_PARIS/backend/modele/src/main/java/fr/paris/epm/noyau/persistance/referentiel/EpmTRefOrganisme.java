package fr.paris.epm.noyau.persistance.referentiel;

import java.text.MessageFormat;

/**
 * POJO EpmTRefOrganisme de la table "epm__t_ref_organisme"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefOrganisme extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * logoActif
     */
    private boolean logoActif;

    private String plateformeUuid;

    public boolean isLogoActif() {
        return logoActif;
    }

    public void setLogoActif(boolean logoActif) {
        this.logoActif = logoActif;
    }

    public String getPlateformeUuid() {
        return plateformeUuid;
    }

    public void setPlateformeUuid(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }

    @Override
	public String toString() {
		return MessageFormat.format("''{0}'' (id = {1,number,#}, code externe = ''{2}'', libellé court = ''{3}'')",
			this.getLibelle(), this.getId(), this.getCodeExterne(), this.getLibelleCourt());
	}
}
