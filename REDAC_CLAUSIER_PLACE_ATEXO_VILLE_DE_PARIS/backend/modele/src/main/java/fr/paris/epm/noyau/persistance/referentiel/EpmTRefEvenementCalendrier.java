package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefEvenementCalendrier de la table "epm__t_ref_evenement_calendrier"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefEvenementCalendrier extends EpmTReferentielSimpleAbstract {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = 1L;
	
	private EpmTRefTypeEvenementCalendrier typeEvenement;
		
	/**
	 * Code de l'événement.
	 */
	private String codeEvenement;
	
	/**
	 * Indique si l'étape associée à cet évenement doit être mis à jour par le cron de mise à jour non.
	 */
	private Boolean miseAJourAutomatique;

	public EpmTRefTypeEvenementCalendrier getTypeEvenement() {
		return typeEvenement;
	}

	public void setTypeEvenement(EpmTRefTypeEvenementCalendrier typeEvenement) {
		this.typeEvenement = typeEvenement;
	}

	public String getCodeEvenement() {
		return codeEvenement;
	}

	public void setCodeEvenement(String codeEvenement) {
		this.codeEvenement = codeEvenement;
	}

	public Boolean getMiseAJourAutomatique() {
		return miseAJourAutomatique;
	}

	public void setMiseAJourAutomatique(Boolean miseAJourAutomatique) {
		this.miseAJourAutomatique = miseAJourAutomatique;
	}

}