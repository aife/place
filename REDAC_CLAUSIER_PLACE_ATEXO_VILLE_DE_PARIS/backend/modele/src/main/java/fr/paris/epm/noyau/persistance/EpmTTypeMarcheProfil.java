package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeMarche;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Represente l'objet persistant d'un type de marche lie a un profil.
 * 
 * @author Rémi Villé
 * @version 2010/09/10
 */
public class EpmTTypeMarcheProfil extends EpmTAbstractObject {

	/**
	 * Identifiant de serialisation.
	 */
	private static final long serialVersionUID = -6129211280478473569L;
	
	/**
	 * Identifiant en base.
	 */
	private int id;
	
	/**
	 * Type de marche referentiel.
	 */
	private EpmTRefTypeMarche typeMarche;

	/**
	 * @returnIdentifiant en base.
	 */
	public final int getId() {
		return id;
	}

	/**
	 * @param valeur Identifiant en base.
	 */
	public void setId(int valeur) {
		this.id = valeur;
	}

	/**
	 * @return Type de marche referentiel.
	 */
	public EpmTRefTypeMarche getTypeMarche() {
		return typeMarche;
	}

	/**
	 * @param valeur Type de marche referentiel.
	 */
	public void setTypeMarche(EpmTRefTypeMarche valeur) {
		this.typeMarche = valeur;
	}

	/**
	 * Comparaison de deux objets liant un type de marche lie a un profil par rapport a leur id.
	 * 
	 * @param objet a comparer
	 * @param true si egaux, false sinon
	 */
	public boolean equals(Object cmp) {
		
		if(!(cmp instanceof EpmTTypeMarcheProfil)) {
			return false;
		}
		EpmTTypeMarcheProfil typeMarcheProfilUtilisateur
											= (EpmTTypeMarcheProfil)cmp;
		return (typeMarche.getId() == typeMarcheProfilUtilisateur.getTypeMarche().getId());
	}
	
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(typeMarche.getId()).toHashCode();
    }
	
}
