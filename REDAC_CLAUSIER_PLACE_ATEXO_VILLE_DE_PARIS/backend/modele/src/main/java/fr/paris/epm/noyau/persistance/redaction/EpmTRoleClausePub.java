package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * Pojo hibernate pour les Rôles des clauses Actif
 */
@Entity
@Table(name = "epm__t_role_clause_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_role_clause", "id_publication"}))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTRoleClausePub extends  EpmTRoleClauseAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_role_clause", nullable = false)
    private Integer idRoleClause;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;

    @Column(name = "id_clause")
    private Integer idClause;

    /**
     * l'objet epmTClause.
     */
    @ManyToOne(targetEntity = EpmTClausePub.class)
    @JoinColumns({
            @JoinColumn(name = "id_clause", referencedColumnName = "id_clause", insertable = false, updatable = false),
            @JoinColumn(name = "id_publication", referencedColumnName = "id_publication", insertable = false, updatable = false)
    })
    private EpmTClausePub epmTClause;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdRoleClause() {
        return idRoleClause;
    }

    public void setIdRoleClause(Integer idRoleClause) {
        this.idRoleClause = idRoleClause;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdClause() {
        return idClause;
    }

    public void setIdClause(Integer idClause) {
        this.idClause = idClause;
    }

    @Override
    public EpmTClausePub getEpmTClause() {
        return epmTClause;
    }

    @Override
    public void setEpmTClause(EpmTClauseAbstract epmTClause) {
        this.epmTClause = (EpmTClausePub) epmTClause;
    }

    public EpmTRoleClausePub clone() throws CloneNotSupportedException {
        EpmTRoleClausePub nouveau = (EpmTRoleClausePub) super.clone();
        nouveau.id = 0;
        nouveau.idRoleClause = idRoleClause;
        nouveau.idPublication = idPublication;
        return nouveau;
    }

}
