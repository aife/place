package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefAdmissibilite de la table "epm__t_ref_admissibilite"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefAdmissibilite extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ADMISSIBLE = 1; // Lot/consultation admissible.
    public static final int NON_ADMISSIBLE = 2; // Lot/consultation non admissible.
    public static final int HORS_DELAI = 3; // Lot/consultation hors délai.
    public static final int EN_ATTENTE = 4; // En attente.

}
