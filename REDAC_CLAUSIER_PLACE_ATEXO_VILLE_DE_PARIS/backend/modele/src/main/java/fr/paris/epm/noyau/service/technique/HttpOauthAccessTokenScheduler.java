package fr.paris.epm.noyau.service.technique;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Timer;

@Service
public class HttpOauthAccessTokenScheduler implements OauthAccessTokenScheduler {
	private final Logger LOG = LoggerFactory.getLogger(HttpOauthAccessTokenScheduler.class);

	@Autowired
	private OauthAccessTokenHolder oauthAccessTokenHolder;

	@Value("${plateforme}")
	private String plateforme;

	@Value("${client}")
	private String client;

	@Value("${oauth.interval:300000}")
	private Long interval;

	private final Timer timer = new Timer();

	@PostConstruct
	@Override
	public void schedule() {
//		LOG.info("Initialisation du jeton technique pour le client = {} et la plateforme = {}", client, plateforme);
//
//		try {
//			timer.scheduleAtFixedRate(new RefreshAccessTokenTask(oauthAccessTokenHolder), 0, interval);
//		} catch ( IllegalStateException ise ) {
//			LOG.error("Echec de la tâche programmee de récupération du jeton", ise);
//		}
	}

	public void setOauthAccessTokenHolder( final OauthAccessTokenHolder oauthAccessTokenHolder ) {
		this.oauthAccessTokenHolder = oauthAccessTokenHolder;
	}

	public void setPlateforme( final String plateforme ) {
		this.plateforme = plateforme;
	}

	public void setClient( final String client ) {
		this.client = client;
	}

	public void setInterval( Long interval ) {
		this.interval = interval;
	}
}
