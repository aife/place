package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour les domaines tranches
 * définis dans le domaine Informations principales du contrat / Tranche.
 * 
 * @author GAO Xuesong
 */
public class DomaineTranche implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Identifiant de la tranche
     */
    private String identifiantTranche;
    
    /**
     * Intitulé de la tranche
     */
    private String intituleTranche;
    
    /**
     * Forme de prix de la tranche 
     */
    private DomaineFormePrix formePrixTranche;
    
    
	public final String getIdentifiantTranche() {
		return identifiantTranche;
	}
	
	public final void setIdentifiantTranche(final String valeur) {
		this.identifiantTranche = valeur;
	}
	
	public final String getIntituleTranche() {
		return intituleTranche;
	}
	
	public final void setIntituleTranche(final String valeur) {
		this.intituleTranche = valeur;
	}
	
	public final DomaineFormePrix getFormePrixTranche() {
		return formePrixTranche;
	}
	
	public final  void setFormePrixTranche(final DomaineFormePrix valeur) {
		this.formePrixTranche = valeur;
	}
    
}
