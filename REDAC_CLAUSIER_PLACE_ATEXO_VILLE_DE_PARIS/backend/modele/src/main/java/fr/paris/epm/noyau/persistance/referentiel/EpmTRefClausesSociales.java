package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefClausesEnvironnementales de la table "epm__t_ref_clauses_sociales"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefClausesSociales extends BaseEpmTRefReferentiel {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

    public static final String CODE_CLAUSE_EXECUTION = "execution";
    public static final String CODE_CLAUSE_RESERVE = "reserve";
    public static final String CODE_CLAUSE_INSERTION = "insertion";
    public static final String CODE_CLAUSE_ATTRIBUTION = "critere_attribution";
    public static final String CODE_CLAUSE_SPECIFICATION = "specification_technique";

}
