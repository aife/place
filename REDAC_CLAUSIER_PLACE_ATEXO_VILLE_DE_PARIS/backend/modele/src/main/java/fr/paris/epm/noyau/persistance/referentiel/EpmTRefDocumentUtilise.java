package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDocumentUtilise de la table "epm__t_ref_document_utilise"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDocumentUtilise extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String ETAPE_OUVERTURE_OFFRE = "ouverture_offre";
    public static final String ETAPE_OUVERTURE_CANDIDATURE = "ouverture_candidature";
    public static final String ETAPE_RECOMMANDATION_CANDIDATURE = "recommandation_candidature";
    public static final String ETAPE_CANDIDATURE_ECARTE = "candidature_ecarte";
    public static final String ETAPE_ATTRIBUTAIRE_PRESSENTI = "attributaire_pressenti";
    public static final String ETAPE_VALIDATION_ATTRIBUTAIRE_PRESSENTI = "validation_attributaire_pressenti";
    public static final String ETAPE_CONFIRMATION_ATTRIBUTION = "confirmation_attribution";
    public static final String ETAPE_GESTION_COURRIER = "gestion_courrier";

    public static final int ID_TABLEAU_DE_RECENSEMENT_OUV_CANDIDATURE = 1;
    public static final int ID_PV_OUV_CANDIDATURE = 2;
    public static final int ID_CADRE_DE_PV_DE_CAO_RECOMMANDATION_CANDIDATURE = 3;
    public static final int ID_PV_DE_LA_CAO_CANDIDATURE_ECARTE = 4;
    public static final int ID_ETAT_DE_SYNTHESE_OUVERTURE_OFFRE = 5;
    public static final int ID_TABLEAU_OUVERTURE_DES_OFFRES = 6;
    public static final int ID_PV_OUV_OFFRE = 7;
    public static final int ID_RAPPORT_DU_POUVOIR_ADJUDICATEUR_ATTRIBUTAIRE_PRESSENTI = 8;
    public static final int ID_RAPPORT_D_ANALYSE_VALIDATION_ATTRIBUTAIRE_PRESSENTI = 9;
    public static final int ID_RAPPORT_CONFIRMATION_ATTRIBUTION = 10;
    public static final int ID_CONSULTATION_LETTRE_CONSULTATION = 11;
    public static final int ID_DEPOUILLEMENT_DEMANDE_PIECE_CANDIDAT = 12;
    public static final int ID_DEPOUILLEMENT_LETTRE_SELECTION_CANDIDATURE = 13;
    public static final int ID_DEPOUILLEMENT_DEMANDE_PRECISION_OFFRE = 14;
    public static final int ID_DEPOUILLEMENT_LETTRE_CANDIDAT_NON_RETENU = 15;
    public static final int ID_DEPOUILLEMENT_LETTRE_INFRUCTUOSITE = 16;
    public static final int ID_ATTRIBUTION_LETTRE_OFFRE_NON_RETENU = 17;
    public static final int ID_ATTRIBUTION_LETTRE_OFFRE_RETENU = 18;
    public static final int ID_ATTRIBUTION_NOTIFICATION = 19;
    public static final int ID_TOUTES_COURRIER_GENERIQUE = 20;
    public static final int ID_TOUTES_COURRIER_SANS_SUITE = 21;

    /**
     * etape de passation associé au document.
     */
    private String etapePassation;

    private String libelleTemplate;

    private String libelleFichierGenere;

    public String getEtapePassation() {
        return etapePassation;
    }

    public void setEtapePassation(String etapePassation) {
        this.etapePassation = etapePassation;
    }

    public String getLibelleTemplate() {
        return libelleTemplate;
    }

    public void setLibelleTemplate(String libelleTemplate) {
        this.libelleTemplate = libelleTemplate;
    }

    public String getLibelleFichierGenere() {
        return libelleFichierGenere;
    }

    public void setLibelleFichierGenere(String libelleFichierGenere) {
        this.libelleFichierGenere = libelleFichierGenere;
    }

}