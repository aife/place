package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Classe représentant l'objet contenant les informations de publication du clausier.
 */
@Entity
@Table(name = "epm__t_publication_clausier_file", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTPublicationClausierFile extends EpmTAbstractObject {
    
    /**
     * Identifiant unique.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * nom du fichier referentiel ou du fichier contenant le clausier
     */
    @Column(name="file_name")
    private String fileName;

    /**
     * soit fichier contenant les référentiels lors de cette publication (json)
     * soit fichier contenant le clausier de cette publication (json)
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "file_clausier")
    @Type(type = "org.springframework.orm.hibernate3.support.BlobByteArrayType")
    private byte[] fileClausier;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileClausier() {
        return fileClausier;
    }

    public void setFileClausier(byte[] fileClausier) {
        this.fileClausier = fileClausier;
    }

}
