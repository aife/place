/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

/**
 * Pojo hibernate étape générale du calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTEtapeCal extends EpmTModeleEtapeCal implements Cloneable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Cette étape est une délibération amont.
     */
    public static final int DELIB_AMONT = 1;

    /**
     * Cette étape est une délibération avale.
     */
    public static final int DELIB_AVAL = 2;

    /**
     * Cette étape est une étape de lancement.
     */
    public static final int LANCEMENT = 3;

    /**
     * Cette étape est une étape de notification.
     */
    public static final int NOTIFICATION = 4;

    /**
     * Cette étape est la réception des candidatures. Limite de "remise des
     * candidatures" si restreint.
     */
    public static final int RECEPTION_CANDIDATURES = 5;

    /**
     * Cette étape est la réception des offres. Limite de "remise des plis" si
     * ouvert. Limite de "remise des offres" si restreint.
     */
    public static final int RECEPTION_OFFRES = 6;

    /**
     * Cette étape est la réception des offres. Limite de "remise des reponse"
     * si ouvert.
     */
    public static final int RECEPTION_REPONSES_OU_MAQUETTE = 7;

    /**
     * Cette étape de commission est l'ouverture des candidatures.
     */
    public static final int OUVERTURE_CANDIDATURES = 8;

    /**
     * Cette étape de commission est la selection des candidatures CAO.
     */
    public static final int SELECTION_CANDIDATURES_CAO = 13;

    /**
     * Cette étape de commission est la selection des candidatures CIM.
     */
    public static final int SELECTION_CANDIDATURES_CIM = 14;

    /**
     * Cette étape est la recommendation.
     */
    public static final int RECOMMENDATIONS = 9;

    /**
     * Cette étape de commission est l'ouverture des offres.
     */
    public static final int OUVERTURE_OFFRES = 10;

    /**
     * Cette étape de commission est la réception des offres.
     */
    public static final int OUVERTURE_OFFRES_FINALES = 26;

    /**
     * Cette étape est l'attribution.
     */
    public static final int ATTRIBUTION = 11;
    /**
     * Etape d'avis d'attribution.
     */
    public static final int AVIS_ATTRIBUTION = 12;

    /**
     * Étape SAPIN de sélection des candidats.
     */
    public static final int SAPIN_SELECTION_CANDIDATS = 15;

    /**
     * Étape SAPIN d'avis préalable de négociation.
     */
    public static final int SAPIN_AVIS_PREALABLE_NEGO = 16;

    /**
     * Étape de CAO d'avenant.
     */
    public static final int CAO_AVENANT = 17;

    /**
     * Étape de conseil de Paris pour un avenant.
     */
    public static final int CONSEIL_PARIS_AVENANT = 18;

    /**
     * Étape de SAPIN d'avenant.
     */
    public static final int SAPIN_AVENANT = 19;

    /**
     * L'étape est affichée.
     */
    public static final int ACTIF_AFFICHE = 0;

    /**
     * L'étape est cachée.
     */
    public static final int ACTIF_CACHE = 1;

    /**
     * L'étape est affichée mais l'écran du workflow n'est pas utilisé.
     */
    public static final int ACTIF_AFFICHE_SANS_ECRAN = 2;

    /**
     * Cette étape est une délibération amont.
     */
    public static final int DELIB_AMONT_ARRONDISSEMENT = 20;

    /**
     * Etape Validation de la liste des candidats autorisés à présenter une
     * offre.
     */
    public static final int VALITAION_CANDIDATS_OFFRE = 21;

    /**
     * Etape Analyse, négociation et Analyse des offres pour les écrans
     * validation du classement des offres / validation du classement des
     * projets.
     */
    public static final int ANALYSE_DES_OFFRES = 22;

    /**
     * Etape proposition de classement, date du classement des offres par le PA.
     * (Recommandation attrib Present PA).
     */
    public static final int CLASSEMENT_DES_OFFRES_PA = 23;

    /**
     * Etape choix des loreat date de validation du classement des offres par le
     * PA. (attribution sous reserve PA).
     */
    public static final int VALIDATION_CLASSEMENT_DES_OFFRES_PA = 24;

    /**
     * Etape date d'enregistrement au controle de légalité (écran attribution du
     * marché).
     */
    public static final int DATE_ENREGISTREMENT_CONTROLE_LEGALITE = 25;

    /**
     * Calendrier, attention chargement paresseux.
     */
    private Integer idCalendrier;

    /**
     * Date initiale. 
     * Saisi au moment de la creation du calendrier initial.
     */
    private Date dateInitiale;

    /**
     * Date réelle. date mise a jour au moment ou l'evenement est passé à partir de la date prevue.
     */
    private Date dateReelle;

    /**
     * Date prévue. date modifié par le calendrier une fois validé
     */
    private Date datePrevue;

    /**
     * Type d'étape.
     */
    private int typeEtape;

    /**
     * Explicite la façon de calculer la date de l'étape à partir des dates
     * générées par les transitions.
     */
    private String regleCalcul;

    /**
     * Vrai si phase initiale de remplissage du calendrier.
     */
    private boolean initial;

    /**
     * Indique si une date de l'étape peut avoir une date hérité.
     */
    private boolean dateHeritable;

    /**
     * La derniére date saisie dans un écran de l'application pour l'étape
     * courante.
     */
    private Date dateHerite;

    /**
     * Activité (affiché, caché, autre) si l'étape est affichée dans le
     * calendrier initial/réel.
     */
    private int actif;    
    
    /**
	 * Indique si l'etape est une ouverture de plis.
	 * 
	 * @param deliberation
	 *            l'etape
	 * @return
	 */
	public static final boolean isOuverturePlis(final int deliberation) {
		return ((OUVERTURE_CANDIDATURES == deliberation)
				|| (OUVERTURE_OFFRES == deliberation) || (OUVERTURE_OFFRES_FINALES == deliberation)
				|| (SELECTION_CANDIDATURES_CIM == deliberation) || (SELECTION_CANDIDATURES_CAO == deliberation));
	}

	/**
	 * Indique si l'etape est une attribution.
	 * 
	 * @param deliberation
	 *            l'etape
	 * @return
	 */
	public static final boolean isAttribution(final int deliberation) {
		return ((ATTRIBUTION == deliberation) || (RECEPTION_REPONSES_OU_MAQUETTE == deliberation));
	}
	
	/**
	 * Indique si l'etape est une selection.
	 */
	public static final boolean isSelection(final int deliberation) {
        return (SELECTION_CANDIDATURES_CAO == deliberation)
                || (SELECTION_CANDIDATURES_CIM == deliberation)
                || (SAPIN_SELECTION_CANDIDATS == deliberation);
	}
    
    
    /**
     * Ajoute une transition sortante.
     * @param transition transition
     */
    public final void ajouterTransitionSortante(
            final EpmTTransitionCal transition) {
        getTransitionsSortantes().add(transition);
    }

    /**
     * Ajoute une transition entrante.
     * @param transition transition
     */
    public final void ajouterTransitionEntrante(
            final EpmTTransitionCal transition) {
        getTransitionsEntrantes().add(transition);
    }


    /**
     * @return vrai si cette étape est une délibération amont.
     */
    public final boolean isDelibAmont() {
        return typeEtape == DELIB_AMONT;
    }

    /**
     * @return vrai si cette étape est une délibération aval.
     */
    public final boolean isDelibAval() {
        return typeEtape == DELIB_AVAL;
    }

    /**
     * @return vrai si cette étape est l'étape de lancement.
     */
    public final boolean isLancement() {
        return typeEtape == LANCEMENT;
    }

    /**
     * @return vrai si cette étape est l'étape de notification.
     */
    public final boolean isNotification() {
        return typeEtape == NOTIFICATION;
    }



    public  boolean isSelection() {
        return Arrays.asList(SELECTION_CANDIDATURES_CAO,SELECTION_CANDIDATURES_CIM,SAPIN_SELECTION_CANDIDATS,ATTRIBUTION).contains(typeEtape);


    }


    // Méthodes
    /**
     * Hormis l'identifiant, les valeurs primitives sont conservées. Les
     * transitions entrantes sont supprimées. Les références vers les
     * transitions sortantes sont conservées.
     * @return objet cloné
     */
    public Object clone() throws CloneNotSupportedException {
        // Clonage de l'étape
        EpmTEtapeCal clone;
        clone = (EpmTEtapeCal) super.clone();
        clone.setId(0);
        clone.setIdCalendrier(null);
        clone.setTransitionsEntrantes(new HashSet());
        clone.setTransitionsSortantes(new HashSet());
        return clone;
    }

    // Accesseurs


    /**
     * @return date initiale
     */
    public Date getDateInitiale() {
        return dateInitiale;
    }

    /**
     * @param valeur date initiale
     */
    public void setDateInitiale(final Date valeur) {
        this.dateInitiale = valeur;
    }

    /**
     * @return date prévue
     */
    public Date getDatePrevue() {
        return datePrevue;
    }

    /**
     * @param valeur date prévue
     */
    public void setDatePrevue(final Date valeur) {
        this.datePrevue = valeur;
    }

    /**
     * @return date réelle
     */
    public Date getDateReelle() {
        return dateReelle;
    }

    /**
     * @param valeur date réelle
     */
    public void setDateReelle(final Date valeur) {
        this.dateReelle = valeur;
    }

    /**
     * @return règle de calcul
     */
    public String getRegleCalcul() {
        return regleCalcul;
    }

    /**
     * @param valeur règle de calcul
     */
    public void setRegleCalcul(final String valeur) {
        this.regleCalcul = valeur;
    }

    /**
     * @return type d'étape (délibération, lancement, ...)
     */
    public int getTypeEtape() {
        return typeEtape;
    }

    /**
     * @param valeur type d'étape (délibération, lancement, ...)
     */
    public void setTypeEtape(final int valeur) {
        this.typeEtape = valeur;
    }

    /**
     * @return calendrier associé, attention chargement paresseux
     */
    public Integer getIdCalendrier() {
        return idCalendrier;
    }

    /**
     * @param valeur calendrier associé
     */
    public void setIdCalendrier(final Integer valeur) {
        this.idCalendrier = valeur;
    }

    /**
     * @return vrai si phase initiale de remplissage du calendrier
     */
    public boolean isInitial() {
        return initial;
    }

    /**
     * @param valeur vrai si phase initiale de remplissage du calendrier
     */
    public void setInitial(final boolean valeur) {
        this.initial = valeur;
    }

    /**
     * @return the dateHeritable
     */
    public boolean isDateHeritable() {
        return dateHeritable;
    }

    /**
     * @param valeur true si la date est héritable.
     */
    public void setDateHeritable(final boolean valeur) {
        this.dateHeritable = valeur;
    }

    /**
     * @return la date héritée.
     */
    public Date getDateHerite() {
        return dateHerite;
    }

    /**
     * @param valeur la date héritée.
     */
    public void setDateHerite(final Date valeur) {
        this.dateHerite = valeur;
    }

    /**
     * @return activité (affiché, caché, autre) si l'étape est affichée dans le
     *         calendrier initial/réel.
     */
    public int getActif() {
        return actif;
    }

    /**
     * @param valeur activité (affiché, caché, autre) si l'étape est affichée
     *            dans le calendrier initial/réel
     */
    public void setActif(final int valeur) {
        this.actif = valeur;
    }        
}
