package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Collection;

/**
 * EpmTProfil pojo pour la persistence.
 * @author Cheikh Diop, Guillaume Béraudo
 * @version $Revision: $, $Date:$, $Author: $
 */
public class EpmTProfil extends EpmTAbstractObject implements Comparable<EpmTProfil> {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * Code profil.
     */
    private String nom;

    /**
     * Collection des habilitations associées.
     */
    private Collection<EpmTHabilitation> habilitationAssocies;
    
    private boolean profilAdministrateur;
    
    /**
     * Identifiant de l'organisme auquel le profil est rataché
     */
    private Integer idOrganisme;

    /**
     * @return nom du profil
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur nom du profil
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return Collection des {@link EpmTHabilitation} associé à un profil
     */
    public Collection<EpmTHabilitation> getHabilitationAssocies() {
        return habilitationAssocies;
    }

    /**
     * @param valeur collection des {@link EpmTHabilitation} associé à un profil
     */
    public void setHabilitationAssocies(final Collection<EpmTHabilitation> valeur) {
        this.habilitationAssocies = valeur;
    }

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @deprecated
     * @return code du profil
     */
    public String getCodeProfil() {
        return nom;
    }

    /**
     * @deprecated
     */
    public void setCodeProfil(final String codeProfil) {
        this.nom = codeProfil;
    }
    
    /**
     * @return the profilAdministrateur
     */
    public boolean isProfilAdministrateur() {
        return this.profilAdministrateur;
    }

    /**
     * @param valeur the profilAdministrateur to set
     */
    public void setProfilAdministrateur(final boolean valeur) {
        this.profilAdministrateur = valeur;
    }

    /**
     * @return code de hachage
     */
    public final int hashCode() {
        int result = Constantes.PREMIER;
        // L'accesseur est utilisé car l'accès direct aux propriétés pose
        // problèmes: les propriétés du proxy sont nulles.
        if (getNom() != null) {
            result += getNom().hashCode();
        }
        return result;
    }

    /**
     * Comparaison uniquement sur le nom du profil.
     * @param obj objet à compararer
     * @return boolean
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!getClass().equals(obj.getClass())) {
            return false;
        }

        final EpmTProfil autre = (EpmTProfil) obj;
        if (nom == null) {
            if (autre.nom != null) {
                return false;
            }
        } else if (!nom.equals(autre.nom)) {
            return false;
        }
        return true;
    }

    /**
     * Comparaison des profils sur les noms.
     * @param profil le profil avec lequel on effectue la comparaison.
     * @return le resultat de la comparaison
     */
    public final int compareTo(final EpmTProfil profil) {
        return this.nom.toLowerCase().compareTo(profil.nom.toLowerCase());
    }

    /**
     * @return the idOrganisme
     */
    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    /**
     * @param valeur the idOrganisme to set
     */
    public void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }
    
}
