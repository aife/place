package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineSousCritereNoteOffre {

    private String nomCritere;

    private Double noteCandidatCritere;

    private Double notePondereeCandidatCritere;

	public String getNomCritere() {
		return nomCritere;
	}

	public void setNomCritere(String nomCritere) {
		this.nomCritere = nomCritere;
	}

	public Double getNoteCandidatCritere() {
		return noteCandidatCritere;
	}

	public void setNoteCandidatCritere(Double noteCandidatCritere) {
		this.noteCandidatCritere = noteCandidatCritere;
	}

	public Double getNotePondereeCandidatCritere() {
		return notePondereeCandidatCritere;
	}

	public void setNotePondereeCandidatCritere(Double notePondereeCandidatCritere) {
		this.notePondereeCandidatCritere = notePondereeCandidatCritere;
	}
    
}
