package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefRoleJuridique;

/**
 * Pojo hibernate - Contractant
 * 
 * @author Rebeca Dantas
 * 
 */
public class EpmTContractant extends EpmTAbstractObject {

	/**
	 * Serialisation
	 */
	private static final long serialVersionUID = 5179846896091018637L;
	
    private Integer id;

	/**
	 * Rôle juridique associé au contractant
	 */
	private EpmTRefRoleJuridique roleJuridique;

	/**
	  * Les informations détaillés de l'entreprise 
	 */
	private EpmTEtablissementEntreprise etablissementEntreprise;
	
	/**
	 * Montant de prestation HT
	 */
	private Double montantPrestationHT;

	/**
	 * Rôle opérationnel
	 */
	private String roleOperationnel;
	
	/**
	 * indique si le contractant est mandataire
	 */
	private boolean mandataire;
	/**
	 * indique si le contractant est solidaire
	 */
	private boolean solidaire;
	
	
    public Integer getId() {
        return id;
    }

    public void setId(Integer valeur) {
        this.id = valeur;
    }

	/**
	 * @return roleJuridique : retourne le rôle juridique associé au contractant
	 */
	public EpmTRefRoleJuridique getRoleJuridique() {
		return roleJuridique;
	}

	/**
	 * @param valeur : défine le rôle juridique associé au contractant
	 */
	public void setRoleJuridique(EpmTRefRoleJuridique valeur) {
		this.roleJuridique = valeur;
	}

	/**
	 * @return montantPrestationHT : retourne le montant de prestation HT
	 */
	public Double getMontantPrestationHT() {
		return montantPrestationHT;
	}

	/**
	 * @param valeur : défine le montant de prestation HT
	 */
	public void setMontantPrestationHT(Double valeur) {
		this.montantPrestationHT = valeur;
	}

	/**
	 * @return roleOperationnel : retourne le rôle opérationnel
	 */
	public String getRoleOperationnel() {
		return roleOperationnel;
	}

	/**
	 * @param valeur : défine le rôle opérationnel
	 */
	public void setRoleOperationnel(String valeur) {
		this.roleOperationnel = valeur;
	}

    /**
     * @return the entreprise
     */
    public EpmTEtablissementEntreprise getEtablissementEntreprise() {
        return etablissementEntreprise;
    }

    /**
     * @param entreprise the entreprise to set
     */
    public void setEtablissementEntreprise(EpmTEtablissementEntreprise valeur) {
        this.etablissementEntreprise = valeur;
    }
    
	/**
	 * @return is mandataire
	 */
	public boolean isMandataire() {
		return mandataire;
	}

	/**
	 * @param valeur mandataire to set
	 */
	public void setMandataire(boolean valeur) {
		this.mandataire = valeur;
	}

	/**
	 * @return
	 */
	public boolean isSolidaire() {
		return solidaire;
	}

	/** 
	 * @param valeur aleur solidaire to set
	 */
	public void setSolidaire(boolean valeur) {
		this.solidaire = valeur;
	}
	
}
