package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine entreprises.registreRetrait
 * de la plateforme de dematerialisation.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreRetraitDemat extends AbstractDomaineRegistreRetrait {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Mail du retrait par la plate-forme de dématérialisation.
	 */
	private String mailRetraitDemat;
	/**
	 * Adresse postale du retrait par la plate-forme de dématérialisation.
	 */
	private String adresseRetraitDemat;
	/**
	 * Date de retrait du DCE par la plate-forme de dématérialisation.
	 */
	private Date dateRetraitDemat;
	/**
	 * Date de retrait des echantillons par la plate-forme de dématérialisation.
	 */
	private Date dateRetraitEchantillonsDemat;
	
	/**
	 * Identite du demandeur (prenom).
	 */
	private String identitePrenomRetraitDemat;
	
	/**
	 * Identite du demandeur (nom).
	 */
	private String identiteNomRetraitDemat;
	
	/**
	 * Entreprise.
	 */
	private String entrepriseRetraitDemat;
	
	/**
	 * Siren.
	 */
	private String sirenRetraitDemat;
	
	private String telephoneRetraitDemat;
	
	private String faxRetraitDemat;
	
	private String villeRetraitDemat;
	
	private String codePostalRetraitDemat;
	
	/**
	 * @return Mail du retrait par la plate-forme de dématérialisation.
	 */
	public final String getMailRetraitDemat() {
		return mailRetraitDemat;
	}
	/**
	 * @param valeur Mail du retrait par la plate-forme de dématérialisation.
	 */
	public final void setMailRetraitDemat(final String valeur) {
		this.mailRetraitDemat = valeur;
	}
	/**
	 * @return Adresse postale du retrait par la plate-forme de dématérialisation.
	 */
	public final String getAdresseRetraitDemat() {
		return adresseRetraitDemat;
	}
	/**
	 * @param valeur Adresse postale du retrait par la plate-forme de dématérialisation.
	 */
	public final void setAdresseRetraitDemat(final
			String valeur) {
		this.adresseRetraitDemat = valeur;
	}
	/**
	 * @return retrait du DCE par la plate-forme de dématérialisation.
	 */
	public final Date getDateRetraitDemat() {
		return dateRetraitDemat;
	}
	/**
	 * @param valeur retrait du DCE par la plate-forme de dématérialisation.
	 */
	public final void setDateRetraitDemat(final Date valeur) {
		this.dateRetraitDemat = valeur;
	}
	/**
	 * @return retrait des echantillons par la plate-forme de dématérialisation.
	 */
	public final Date getDateRetraitEchantillonsDemat() {
		return dateRetraitEchantillonsDemat;
	}
	/**
	 * @param valeur retrait des echantillons par la plate-forme de dématérialisation.
	 */
	public final void setDateRetraitEchantillonsDemat(final
			Date valeur) {
		this.dateRetraitEchantillonsDemat = valeur;
	}
	/**
	 * @return Identite du demandeur.
	 */
    public final String getIdentitePrenomRetraitDemat() {
        return identitePrenomRetraitDemat;
    }
    /**
     * @param valeur Identite du demandeur.
     */
    public final void setIdentitePrenomRetraitDemat(final String valeur) {
        this.identitePrenomRetraitDemat = valeur;
    }
	/**
	 * @return  Entreprise.
	 */
	public final String getEntrepriseRetraitDemat() {
		return entrepriseRetraitDemat;
	}
	/**
	 * @param valeur  Entreprise.
	 */
	public final void setEntrepriseRetraitDemat(final String valeur) {
		this.entrepriseRetraitDemat = valeur;
	}
	/**
	 * @return Siren.
	 */
	public final String getSirenRetraitDemat() {
		return sirenRetraitDemat;
	}
	/**
	 * @param valeur Siren.
	 */
	public final void setSirenRetraitDemat(final String valeur) {
		this.sirenRetraitDemat = valeur;
	}
	/**
	 * @return Identite du demandeur (nom).
	 */
	public final String getIdentiteNomRetraitDemat() {
		return identiteNomRetraitDemat;
	}
	/**
	 * @param valeur Identite du demandeur (nom).
	 */
	public final void setIdentiteNomRetraitDemat(final String valeur) {
		this.identiteNomRetraitDemat = valeur;
	}
	/**
	 * @return the telephoneRetraitDemat
	 */
	public final String getTelephoneRetraitDemat() {
		return telephoneRetraitDemat;
	}
	/**
	 * @param valeur the telephoneRetraitDemat to set
	 */
	public final void setTelephoneRetraitDemat(final String valeur) {
		this.telephoneRetraitDemat = valeur;
	}
	/**
	 * @return the faxRetraitDemat
	 */
	public final String getFaxRetraitDemat() {
		return faxRetraitDemat;
	}
	/**
	 * @param valeur the faxRetraitDemat to set
	 */
	public final void setFaxRetraitDemat(final String valeur) {
		this.faxRetraitDemat = valeur;
	}
	/**
	 * @return the villeRetraitDemat
	 */
	public final String getVilleRetraitDemat() {
		return villeRetraitDemat;
	}
	/**
	 * @param valeur the villeRetraitDemat to set
	 */
	public final void setVilleRetraitDemat(final String valeur) {
		this.villeRetraitDemat = valeur;
	}
	/**
	 * @return the codePostalRetraitDemat
	 */
	public final String getCodePostalRetraitDemat() {
		return codePostalRetraitDemat;
	}
	/**
	 * @param valeur the codePostalRetraitDemat to set
	 */
	public final void setCodePostalRetraitDemat(final String valeur) {
		this.codePostalRetraitDemat = valeur;
	}
}
