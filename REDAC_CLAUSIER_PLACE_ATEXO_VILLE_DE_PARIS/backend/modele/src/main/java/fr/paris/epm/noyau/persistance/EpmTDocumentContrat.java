package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeDocumentContrat;

import java.util.Date;

/**
 * POJO hibernate pour le document du contrat
 * @author GAO Xuesong
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTDocumentContrat extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = -2985242668255808544L;

    private int id;

    /**
     * Marquer si le document est associé avec le contrat
     */
    private boolean selectionne;

    /**
     * Marquer le type du document
     */
    private EpmTRefTypeDocumentContrat typeDocumentContrat;

    /**
     * Marquer le catégorie du document
     */
    private int idCategorieDocumentContrat;

    /**
     * Document dans la passation
     */
    private EpmTDocument document;

    /**
     * Lot.
     */
    private EpmTBudLot lot;
    
    /**
     * Étape spécifique.
     */
    private EpmTEtapeSpec etape;
    
    /**
     * id du contrat qui possède ce document
     */
    private int idContrat;

    /**
     * date de: <li>
     * <ul>
     * la mise en ligne du document pour documet dce
     * </ul>
     * <ul>
     * envoie dela publcité
     * </ul>
     * <ul>
     * import de Mpe en "dd/MM/yyyy"
     * </ul>
     * </li>
     */
    private Date dateDocumentContrat;

    /**
     * elle est <b>vrai</b> si le document est administrable, <b>faux</b> si
     * autres
     */
    private boolean documentsAdministrables;
    
    
    /**
     * permet de commenter l'ajout d'un document contrat 
     */
    private  String commentaireDocumentContrat;
    
    /**
     * l'objet d'un document contrat 
     */
    
    private String objetDocumentContrat;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return the selectionne
     */
    public boolean isSelectionne() {
        return selectionne;
    }

    /**
     * @return the typeDocumentContrat
     */
    public EpmTRefTypeDocumentContrat getTypeDocumentContrat() {
        return typeDocumentContrat;
    }

    /**
     * @return the document
     */
    public EpmTDocument getDocument() {
        return document;
    }

    /**
     * @return the idContrat
     */
    public int getIdContrat() {
        return idContrat;
    }

    /**
     * @param selectionne the selectionne to set
     */
    public void setSelectionne(final boolean valeur) {
        this.selectionne = valeur;
    }

    /**
     * @param typeDocumentContrat the typeDocumentContrat to set
     */
    public void setTypeDocumentContrat(final EpmTRefTypeDocumentContrat valeur) {
        typeDocumentContrat = valeur;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(final EpmTDocument valeur) {
        this.document = valeur;
    }

    /**
     * @param idContrat the idContrat to set
     */
    public void setIdContrat(final int valeur) {
        this.idContrat = valeur;
    }

    /**
     * @return the categorieDocumentContrat
     */
    public int getIdCategorieDocumentContrat() {
        return idCategorieDocumentContrat;
    }

    /**
     * @param categorieDocumentContrat the categorieDocumentContrat to set
     */
    public void setIdCategorieDocumentContrat(final int valeur) {
        this.idCategorieDocumentContrat = valeur;
    }

    /**
     * @return the dateDocumentContrat
     */
    public Date getDateDocumentContrat() {
        return dateDocumentContrat;
    }

    /**
     * @param date the date to set
     */
    public void setDateDocumentContrat(final Date value) {
        this.dateDocumentContrat = value;
    }

    /**
     * @return the documentsAdministrables
     */
    public boolean isDocumentsAdministrables() {
        return documentsAdministrables;
    }

    /**
     * @param documentsAdministrables the documentsAdministrables to set
     */
    public void setDocumentsAdministrables(boolean valeur) {
        this.documentsAdministrables = valeur;
    }

    /**
     * @return the lot
     */
    public EpmTBudLot getLot() {
        return lot;
    }

    /**
     * @param lot the lot to set
     */
    public void setLot(EpmTBudLot valeur) {
        this.lot = valeur;
    }

    /**
     * @return the etape
     */
    public EpmTEtapeSpec getEtape() {
        return etape;
    }

    /**
     * @param etape the etape to set
     */
    public void setEtape(EpmTEtapeSpec valeur) {
        this.etape = valeur;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaireDocumentContrat() {
        return commentaireDocumentContrat;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaireDocumentContrat(String valeur) {
        this.commentaireDocumentContrat = valeur;
    }

    /**
     * @return the objetDocumentContrat
     */
    public  String getObjetDocumentContrat() {
        return objetDocumentContrat;
    }

    /**
     * @param objetDocumentContrat the objetDocumentContrat to set
     */
    public  void setObjetDocumentContrat(String valeur) {
        this.objetDocumentContrat = valeur;
    }
}
