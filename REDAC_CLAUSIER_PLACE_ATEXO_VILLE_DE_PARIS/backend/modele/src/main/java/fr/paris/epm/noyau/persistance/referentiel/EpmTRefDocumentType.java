package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDocumentType de la table "epm__t_ref_document_type"
 * Referentiel permettant d'afficher les différents format de documents supporté
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDocumentType extends EpmTReferentielSimpleAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le type de document généré (odt, pdf, doc)
     */
    private String type;

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

}