package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Classe critère utilisé pour la recherche de message d'actualité.
 * 
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class MessageActualiteCritere extends AbstractCritere implements
        Critere, Serializable {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * date .
     */
    private String date;

    /**
     * @param valeur
     *            date date
     */
    public final void setDate(final String valeur) {
        this.date = valeur;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final StringBuffer corpsRequete() {
        SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        sb.append("from EpmTMessageActualite ");

        if (id != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }

            sb.append(" id = ").append(id.intValue());
            debut = true;
        }

        if (date != null) {
            try {
                if (debut) {
                    sb.append(" and ");
                } else {
                    sb.append(" where ");
                }
                sb.append(" date >= '");
                sb.append(formatDate.parse(date));
                sb.append("'");

            } catch (ParseException e) {
            }
        }
        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee == null) {
            proprieteTriee = "date";
            triCroissant = false;
            sb.append(" order by ").append(" ordre ASC, ")
                    .append(proprieteTriee);
        } else {
            sb.append(" order by ").append(proprieteTriee);
        }
        if (triCroissant) {
            sb.append(" ASC ");
        } else {
            sb.append(" DESC ");
        }
        return sb.toString();
    }

    public String toCountHQL() {
        StringBuffer sb = new StringBuffer("select count(*) ");
        sb.append(corpsRequete());
        return sb.toString();
    }

}
