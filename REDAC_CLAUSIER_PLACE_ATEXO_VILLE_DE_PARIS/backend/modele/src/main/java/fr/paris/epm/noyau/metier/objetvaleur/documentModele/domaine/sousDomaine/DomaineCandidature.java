package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis
 * .ouvertureReponseEnregistrementCandidature.candidature.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCandidature implements Serializable {
	
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private String commentairesCandidatureInitiale;

    private String nomCandidat;

    private String completeAdmissibilite;
    
    private String referenceDepot;
    
    private String formeJuridique;
    
    private String ville;

    private String rang;

    /**
     * Nature des pieces recues.
     */
    private String commentairesSuiviComplementCandidature;

    public final String getCommentairesCandidatureInitiale() {
        return commentairesCandidatureInitiale;
    }

    public final void setCommentairesCandidatureInitiale(final String valeur) {
        this.commentairesCandidatureInitiale = valeur;
    }

    public final String getNomCandidat() {
        return nomCandidat;
    }

    public final void setNomCandidat(final String valeur) {
        this.nomCandidat = valeur;
    }

    /**
     * @return Nature des pieces recues.
     */
    public final String getCommentairesSuiviComplementCandidature() {
        return commentairesSuiviComplementCandidature;
    }

    /**
     * @param valeur Nature des pieces recues.
     */
    public final void setCommentairesSuiviComplementCandidature(
            final String valeur) {
        commentairesSuiviComplementCandidature = valeur;
    }

    public final String getCompleteAdmissibilite() {
        return completeAdmissibilite;
    }

    public final void setCompleteAdmissibilite(final String valeur) {
        this.completeAdmissibilite = valeur;
    }

	/**
	 * @return the referenceDepot
	 */
	public final String getReferenceDepot() {
		return referenceDepot;
	}

	/**
	 * @param valeur the referenceDepot to set
	 */
	public final void setReferenceDepot(final String valeur) {
		this.referenceDepot = valeur;
	}

	/**
	 * @return the formeJuridique
	 */
	public final String getFormeJuridique() {
		return formeJuridique;
	}

	/**
	 * @param valeur the formeJuridique to set
	 */
	public final void setFormeJuridique(final String valeur) {
		this.formeJuridique = valeur;
	}

	/**
	 * @return the ville
	 */
	public final String getVille() {
		return ville;
	}

	/**
	 * @param valeur the ville to set
	 */
	public final void setVille(final String valeur) {
		this.ville = valeur;
	}

    /**
     * @return the rang
     */
    public final String getRang() {
        return this.rang;
    }

    /**
     * @param valeur the rang to set
     */
    public void setRang(final String valeur) {
        this.rang = valeur;
    }
}
