package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefDepartement de la table "epm__t_ref_departement"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefDepartement extends EpmTReferentielExterneAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

}