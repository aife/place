package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * entreprises.registreRetrait.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreRetrait implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    private Integer nbTotalRetraits;
    
    private Integer nbRetraitsDemat;
    
    private Integer nbRetraitsPapier;

    public final Integer getNbTotalRetraits() {
        return nbTotalRetraits;
    }

    public final void setNbTotalRetraits(final Integer valeur) {
        this.nbTotalRetraits = valeur;
    }

    public final Integer getNbRetraitsDemat() {
        return nbRetraitsDemat;
    }

    public final void setNbRetraitsDemat(final Integer valeur) {
        this.nbRetraitsDemat = valeur;
    }

    public final Integer getNbRetraitsPapier() {
        return nbRetraitsPapier;
    }

    public final void setNbRetraitsPapier(final Integer valeur) {
        this.nbRetraitsPapier = valeur;
    }
}
