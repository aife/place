package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * commission.domaineOrdreDuJour.domaineOrdreDuJourNombreDossier.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineOrdreDuJourNombreDossier implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Nombre total de dossier.
     */
    private int nbDossier;

    private int nbDossierAoo;

    private int nbDossierAooMoe;

    private int nbDossierAor;

    private int nbDossierAorMoe;

    private int nbDossierArt30;

    private int nbDossierMarcheNegocie;

    private int nbDossierDc;
    
    private int nbDossierAvenant;

    /**
     * @return Nombre total de dossier.
     */
    public final int getNbDossier() {
        return nbDossier;
    }

    /**
     * @param valeur Nombre total de dossier.
     */
    public final void setNbDossier(final int valeur) {
        this.nbDossier = valeur;
    }

    public final int getNbDossierAoo() {
        return nbDossierAoo;
    }

    public final void setNbDossierAoo(final int valeur) {
        this.nbDossierAoo = valeur;
    }

    public final int getNbDossierAooMoe() {
        return nbDossierAooMoe;
    }

    public final void setNbDossierAooMoe(final int valeur) {
        this.nbDossierAooMoe = valeur;
    }

    public final int getNbDossierAor() {
        return nbDossierAor;
    }

    public final void setNbDossierAor(final int valeur) {
        this.nbDossierAor = valeur;
    }

    public final int getNbDossierAorMoe() {
        return nbDossierAorMoe;
    }

    public final void setNbDossierAorMoe(final int valeur) {
        this.nbDossierAorMoe = valeur;
    }

    public final int getNbDossierArt30() {
        return nbDossierArt30;
    }

    public final void setNbDossierArt30(final int valeur) {
        this.nbDossierArt30 = valeur;
    }

    public final int getNbDossierMarcheNegocie() {
        return nbDossierMarcheNegocie;
    }

    public final void setNbDossierMarcheNegocie(final int valeur) {
        this.nbDossierMarcheNegocie = valeur;
    }

    public final int getNbDossierDc() {
        return nbDossierDc;
    }

    public final void setNbDossierDc(final int valeur) {
        this.nbDossierDc = valeur;
    }

    public final int getNbDossierAvenant() {
        return nbDossierAvenant;
    }

    public final void setNbDossierAvenant(final int valeur) {
        this.nbDossierAvenant = valeur;
    }
}
