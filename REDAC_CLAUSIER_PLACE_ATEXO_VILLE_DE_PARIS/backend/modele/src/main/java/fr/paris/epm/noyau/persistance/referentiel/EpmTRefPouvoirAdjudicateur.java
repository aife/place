package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefPouvoirAdjudicateur de la table "epm__t_ref_pouvoir_adjudicateur"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefPouvoirAdjudicateur extends BaseEpmTRefReferentiel {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int ID_VILLE = 1; // Identifiant de l'enregistrement correspondant à la ville.
    public static final int ID_DPT = 2; // Identifiant de l'enregistrement correspondant au département.
    public static final int ID_GPMT = 3; // Identifiant de l'enregistrement correspondant au groupement.
    public static final int ID_VILLE_DPT_GPMT = 4; // Identifiant de l'enregistrement correspondant a l'ensemble des pouvoirs adjudicateurs.
    public static final int ID_VILLE_DPT = 5; // Identifiant de l'enregistrement correspondant a ville et departement.

    /**
     * true si pouvoir adjudicateur utilisé pour reunion et instance.
     */
    private boolean commission;

    /**
     * identifiant GO.
     */
    private String codeGo;
    
    /**
     * organisme de la plateforme DEMAT associé au pouvoir adjudicateur
     */
    private String organismeMPE;
    
    /**
     * code utilisé pour la generation des numero de contrat des offresLc.
     */
    private String codeContrat;
    
    /**
     * Indique si le pouvoir adjudicateur est un groupement.
     */
    private boolean groupement;

    /**
     * Indique si le pouvoir adjudicateur est uniquement utilisable en tant que membre de groupement
     */
    private boolean membre;
    
    /**
     * organisme de la plateforme DEMAT associé au pouvoir adjudicateur
     */
    private String acronymeOrganisme;

    /**
     * LOGO
     */
    private String logoType;
    private byte[] logo;

    public boolean isCommission() {
        return commission;
    }

    public void setCommission(boolean commission) {
        this.commission = commission;
    }

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(String codeGo) {
        this.codeGo = codeGo;
    }

    public String getOrganismeMPE() {
        return organismeMPE;
    }

    public void setOrganismeMPE(String organismeMPE) {
        this.organismeMPE = organismeMPE;
    }

    public String getCodeContrat() {
        return codeContrat;
    }

    public void setCodeContrat(String codeContrat) {
        this.codeContrat = codeContrat;
    }

    public boolean isGroupement() {
        return groupement;
    }

    public void setGroupement(boolean groupement) {
        this.groupement = groupement;
    }

    public boolean isMembre() {
        return membre;
    }

    public void setMembre(boolean membre) {
        this.membre = membre;
    }

    public String getAcronymeOrganisme() {
        return acronymeOrganisme;
    }

    public void setAcronymeOrganisme(String acronymeOrganisme) {
        this.acronymeOrganisme = acronymeOrganisme;
    }

    public String getLogoType() {
        return logoType;
    }

    public void setLogoType(String logoType) {
        this.logoType = logoType;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

}
