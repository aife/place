/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;


/**
 * Pojo hibernate étape de notification du calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class EpmTEtapeCalLancement extends EpmTEtapeCal {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}
