/**
 * 
 */
package fr.paris.epm.noyau.metier.vues;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @author Cyril DUBY
 *
 */
public class VueSeancesParMembreCritere extends AbstractCritere implements
		Critere, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Formateur de date au format SQL.
     */
    protected static final SimpleDateFormat DF
            = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/* CRITÈRES */
	
	private Integer idUtilisateur;

	private Integer idPouvoirAdjudicateur;
	
	private String libelle;
	
	private Date aPartirDu;
	
	private Date jusquAu;
	
	/* (non-Javadoc)
	 * @see fr.paris.epm.noyau.metier.AbstractCritere#corpsRequete()
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();

        if (idUtilisateur != null) {    
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("sm.utilisateur.id = :idUtilisateur");
            parametres.put("idUtilisateur", idUtilisateur);
            debut = true;
        }
        
        if (idPouvoirAdjudicateur != null && idPouvoirAdjudicateur > 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("sm.seance.pouvoirAdjudicateur.id = :idPouvoirAdjudicateur");
            parametres.put("idPouvoirAdjudicateur", idPouvoirAdjudicateur);
            debut = true;
        }
        
        if(libelle != null && libelle.trim().length() > 0) {
        	if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append("lower(sm.seance.libelle) like lower(:libelle)");
            StringBuffer sbLibelle = new StringBuffer();
            sbLibelle.append(pourcentTrim(libelle));
            parametres.put("libelle", sbLibelle.toString());
        }
        
        if (aPartirDu != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
			// On s'assure de ce que la première journée est prise entièrement
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(aPartirDu);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			sb.append(" sm.seance.dateSeance >= '");
            sb.append(DF.format(aPartirDu) + "'");
            debut = true;
        }

        if (jusquAu != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
			// On s'assure de ce que la dernière journée est prise entièrement
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(jusquAu);
			calendar.add(Calendar.DAY_OF_YEAR, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			calendar.add(Calendar.MILLISECOND, -1);
			sb.append(" sm.seance.dateSeance <= '");
			sb.append(DF.format(calendar.getTime()) + "'");
            debut = true;
        }

        return sb;
	}

	/* (non-Javadoc)
	 * @see fr.paris.epm.noyau.metier.Critere#toHQL()
	 */
	@Override
	public String toHQL() {
        StringBuffer req = new StringBuffer("from EpmVSeanceMembre as sm");
        req.append(corpsRequete());
        if (proprieteTriee != null) {
            req.append(" order by sm.");
            req.append(proprieteTriee);
            if (triCroissant) {
                req.append(" ASC ");
            } else {
                req.append(" DESC ");
            }
        }
        return req.toString();
	}

	/* (non-Javadoc)
	 * @see fr.paris.epm.noyau.metier.Critere#toCountHQL()
	 */
	@Override
	public String toCountHQL() {
        return "select count(*) from EpmVSeanceMembre as sm " + corpsRequete();
	}
	
	/* ACCESSEURS */

	public void setIdUtilisateur(Integer idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public void setIdPouvoirAdjudicateur(Integer idPouvoirAdjudicateur) {
		this.idPouvoirAdjudicateur = idPouvoirAdjudicateur;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public void setAPartirDu(Date aPartirDu) {
		this.aPartirDu = aPartirDu;
	}

	public void setJusquAu(Date jusquAu) {
		this.jusquAu = jusquAu;
	}

}
