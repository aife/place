package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefClausesEnvironnementales de la table "epm__t_ref_clauses_environnementales"
 * Created by nty on 29/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefClausesEnvironnementales extends BaseEpmTRefReferentiel {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;

    public static final String CODE_CLAUSE_SPECIFICATION = "CLAUSE_SPECIFICATION";
    public static final String CODE_CLAUSE_CONDITIONS = "CLAUSE_CONDITIONS";
    public static final String CODE_CLAUSE_ATTRIBUTION = "CLAUSE_ATTRIBUTION";

}
