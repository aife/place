package fr.paris.epm.noyau.feature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Classe utilitaire pour tester l'activation des fonctionnalités optionnelles de RSEM dans le code.
 * Created by sta on 15/12/15.
 */
public class FeatureUtil {

    final static Logger LOG = LoggerFactory.getLogger(FeatureUtil.class);

    /**
     * Enumération des fonctionnalités optionnelles disponibles dans RSEM.
     * En plus d'être ajoutées ici, les fonctionnalités et leur paramétrage doivent être détaillées dans feature.properties et faire l'objet d'un ticket d'exploitation.
     */
    public enum Feature {
        MPS("mps"),
        DEV("dev"),
        REDAC("redac"),
        BASE_FOURNISSEUR("base-fournisseur");

        private final String name;

        Feature(String name) {
            this.name = name;
        }

        public static Feature getByName(String name) {
            for (Feature feature : Feature.values()) {
                if (feature.getName().equals(name.trim())) {
                    return feature;
                }
            }
            return null;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * Objet JSON représentant les features et leur activation
     */
    private static String jsonActivatedFeatures;
    /**
     * EnumSet des features activées (Enumset est très performant)
     */
    private static EnumSet<Feature> activatedFeatures;

    /** Chargement statique des features activées via la propriété système 'spring.profiles.active' */
    static {
        parseActivatedFeatures();
        jsonifyfeatures();
    }

    private static void parseActivatedFeatures() {
        String profilesProperty = System.getProperty("spring.profiles.active");
        if (profilesProperty == null || profilesProperty.isEmpty()) {
            activatedFeatures = EnumSet.noneOf(Feature.class);
        } else {
            String[] profilesStrArray = profilesProperty.split(",");
            List<Feature> featuresArray = new ArrayList<>(profilesStrArray.length);
            for (String profileName : profilesStrArray) {
                Feature profileAsfeature = Feature.getByName(profileName);
                if (profileAsfeature == null) {
                    LOG.trace("Le profil Spring suivant a été activé via la propriété système 'spring.profiles.active', mais ne correspond à aucune fonctionnalité : " + profileName);
                } else {
                    featuresArray.add(profileAsfeature);
                }
            }

            if (featuresArray.isEmpty()) {
                activatedFeatures = EnumSet.noneOf(Feature.class);
            } else {
                activatedFeatures = EnumSet.copyOf(featuresArray);
            }
        }
    }

    private static void jsonifyfeatures() {
        StringBuilder jsonFeatures = new StringBuilder("{");
        for (Feature feature : activatedFeatures) {
            jsonFeatures.append(String.format("\"%s\":%b,", feature.getName(), true));
        }
        //remplace la dernière virgule par une accolade pour l'objet JSON
        int length = jsonFeatures.length();
        if (length > 1) { //si il n'y a rien d'activé, on n'a que "{"
            jsonFeatures.replace(length - 1, length, "}");
        } else {
            jsonFeatures.append("}");
        }
        jsonActivatedFeatures = jsonFeatures.toString();
    }

    /**
     * Permet de savoir si une fonctionnalité est activée ou non.
     *
     * @param feature fonctionnalité à tester
     * @return 'true' si la fonctionnalité est activée et 'false' dans le cas contraire
     */
    public static boolean check(Feature feature) {
        return activatedFeatures.contains(feature);
    }

    /**
     * Renvoie la liste des feature activée manipulable en Javascript.
     * Par exemple, pour tester l'activation de la feature "MPS", on pourra faire quelque chose comme 'if(features.mps){...}'
     *
     * @return
     */
    public static String getJsonActivatedFeatures() {
        return jsonActivatedFeatures;
    }
}
