package fr.paris.epm.noyau.metier;

import java.util.Map;

/**
 * Cette interface est utilisé par l'ensemble des classes critères.
 * 
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public interface Critere {

    /**
     * @return chaine HQL
     */
    String toHQL();

    /**
     * @return chaine HQL permetant de compter le nombre de resultat
     */
    String toCountHQL();

    /**
     * Création du corps de la requete sans le trie.
     * 
     * @return le corps de la requete
     */
    StringBuffer corpsRequete();

    /**
     * A appeler uniquement après avoir appeler toHQL().
     * 
     * @return map de paramètres hibernate
     */
    Map getParametres();

    void setTriCroissant(final boolean valeur);

    boolean isTriCroissant();

    void setProprieteTriee(final String valeur);

    String getProprieteTriee();

    void setChercherNombreResultatTotal(final boolean valeur);

    boolean isChercherNombreResultatTotal();

    void setTaillePage(final int valeur);

    int getTaillePage();

    void setNumeroPage(final int valeur);

    int getNumeroPage();

}
