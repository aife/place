package fr.paris.epm.noyau.persistance.referentiel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Set;

/**
 * POJO EpmTRefProcedure de la table "epm__t_ref_procedure"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefProcedure extends BaseEpmTRefReferentiel implements EpmTRefImportExport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private static final String COMPLEMENT_TYPE_MOE = "_MAITRISE_OEUVRE"; // Maitrise d'oeuvre (a utiliser en complement)
    public static final String TYPE_PROCEDURE_ADAPTEE = "PROCEDURE_ADAPTEE"; // Procédure adaptée.
    public static final String TYPE_AOO = "APPEL_OFFRES_OUVERT"; // Appel d'offres ouverts.
    public static final String TYPE_AOO_MOE = TYPE_AOO + COMPLEMENT_TYPE_MOE; // Appel d'offres ouverts.
    public static final String TYPE_AOR = "APPEL_OFFRES_RESTREINT"; // Appel d'offres retreints.
    public static final String TYPE_AOR_MOE = TYPE_AOR + COMPLEMENT_TYPE_MOE; // Appel d'offres retreints.
    public static final String TYPE_PROCEDURE_NEGOCIEE = "PROCEDURE_NEGOCIEE"; // Procedure negociee
    public static final String TYPE_DIALOGUE_COMPETITIF = "DIALOGUE_COMPETITIF"; // Dialogue compétitif.
    public static final String TYPE_CONCOURS_RESTREINT = "CONCOURS_RESTREINT"; // Concours restreint.
    public static final String TYPE_DSP = "DELEGATION_SERVICE_PUBLIQUE"; // DSP.
    public static final String TYPE_AVENANT = "AVENANT"; // Avenant
    public static final String TYPE_ACCORD_CADRE = "ACCORD_CADRE"; // Accord cadre
    public static final String TYPE_AUTRES = "NA"; // Autres.

    public static final String PROCEDURE_NON_FORMALISEE = "Procédure non formalisée"; // Procedure non formalisé.
    public static final String PROCEDURE_FORMALISEE = "Procédure formalisée"; // Procedure formalisée
    public static final String WORKFLOW_PROCEDURE_CONCOURS = "concours"; // concours

	public static final String CODE_MAPA_INF_PERSO = "PA-INF";

	public static final String CODE_MAPA_SUP_PERSO = "PA-SUP";

	/**
     * Oui si l'attribut est formalisé.
     */
    private String attributFormalise;

    /**
     * Collection d'articles associés.
     */
    private Collection articlesAssocies;

    /**
     * Collection d'articles associés.
     */
    private EpmTRefTypeMarche epmTRefTypeMarche;

    /**
     * Chemin vers le fichier de workflow.
     */
    private String workflow;

    /**
     * Vrai si possède deux phases, donc deux dates limites.
     */
    private boolean deuxPhases;

    /**
     * Indique s'il y a une étape de négociation pour le workflow.
     */
    private boolean negociable;

    private boolean enveloppeUnique;

    /**
     * Permet de savoir si la procedure est associée à l'option "DCE en ligne"
     */
    private boolean valeurDefautDceEnLigne;

    /**
     * Permet de savoir si la procedure est associée à l'option "Chiffrement"
     */
    private boolean valeurDefautChiffrement;

    /**
     * Permet de savoir si la procedure est associée à l'option "Signature"
     */
    private boolean valeurDefautSignature;

    /**
     * Permet de savoir si la procedure est associée à l'option "Enveloppe unique"
     */
    private boolean valeurDefautEnveloppeUnique;

    /**
     * Permet de savoir si la procedure est associée à l'option "Phase successiva"
     */
    private boolean valeurDefautPhaseSuccessive;

    /**
     * Permet de savoir si l'option "DCE en ligne" est modifiable ou non (grisée)
     */
    private boolean modifiableDceEnLigne;

    /**
     * Permet de savoir si l'option "Réponse électronique" est modifiable ou non (grisée)
     */
    private boolean modifiableReponseElectronique;

    /**
     * Permet de savoir si l'option "Chiffrement" est modifiable ou non (grisée)
     */
    private boolean modifiableChiffrement;

    /**
     * Permet de savoir si l'option "siganture" est modifiable ou non (grisée)
     */
    private boolean modifiableSignature;

    /**
     * Permet de savoir si l'option "Enveloppe unique" est modifiable ou non (grisée)
     */
    private boolean modifiableEnveloppeUnique;

    /**
     * Permet de savoir si l'option "Phase successive" est modifiable ou non (grisée)
     */
    private boolean modifiablePhaseSuccessive;

    /**
     * Permet de savoir si l'option "Nombre de candidat" est modifiable ou non (grisée)
     */
    private boolean modifiableNbCandidat;

    /**
     * Type de la procedure (procedure adaptée...)
     */
    private String typeProcedure;

    /**
     * Comlement boamp
     */
    private String procedureBOAMP;

    /**
     * Identifiant pour la fiche de rencensement
     */
    private String ficheRecensement;

    /**
     * Permet de generer un document de deliberation xls (le numero de ligne utilise)
     */
    private Integer documentDeliberation;

    /**
     * Libelle de la procedure dans un document ooffice.
     */
    private String documentLibelle;

    /**
     * Indique si l'acte d'engement a signature propre est active par defaut.
     */
    private boolean valeurDefautSignatureEngagement;

    /**
     * Indique si l'acte d'engement a signature propre est modifiable.
     */
    private boolean modifiableSignatureEngagement;

    /**
     * Indique si MPS est actif par defaut.
     */
    private boolean valeurDefautMPS;

    /**
     * Indique si MPS est modifiable.
     */
    private boolean modifiableMPS;

    private boolean procedurePersonnalise;

    /**
     * Permet de savoir la valeur par défaut pour "Réponse électronique"
     * (Refusée, Autorisée ou Obligatoire) de cette procédure
     */
    private EpmTRefReponseElectronique refReponseElectronique;


    private Set<EpmTRefParametrage> epmTRefParametrageSet;

    @JsonIgnore
    private Set<EpmTRefOrganisme> epmTRefOrganismeSet;

    /**
     * @return vrai si l'attribution est formalisés
     */
    public String getAttributFormalise() {
        return this.attributFormalise;
    }

    /**
     * @param valeur vrai si l'attribution est formalisés
     */
    public void setAttributFormalise(final String valeur) {
        this.attributFormalise = valeur;
    }

    /**
     * @return Collection ensemble de {@link EpmTRefArticle} associés à la procédure
     */
    public Collection getArticlesAssocies() {
        return articlesAssocies;
    }

    /**
     * @param valeur Collection de {@link EpmTRefArticle} associés à la procédure
     */
    public void setArticlesAssocies(final Collection valeur) {
        this.articlesAssocies = valeur;
    }

    /**
     * @return modèle de workflow
     */
    public String getWorkflow() {
        return workflow;
    }

    /**
     * @param valeur modèle de workflow
     */
    public void setWorkflow(final String valeur) {
        this.workflow = valeur;
    }

    /**
     * @return vrai si possède deux phases, donc deux dates limites
     */
    public boolean isDeuxPhases() {
        return deuxPhases;
    }

    /**
     * @param valeur vrai si possède deux phases, donc deux dates limites
     */
    public void setDeuxPhases(final boolean valeur) {
        this.deuxPhases = valeur;
    }

    /**
     * @return true si la procedure possède une phase de négociation, false sinon.
     */
    public boolean isNegociable() {
        return negociable;
    }

    /**
     * @param valeur true si la procedure possède une phase de négociation, false sinon.
     */
    public void setNegociable(final boolean valeur) {
        this.negociable = valeur;
    }

    /**
     * @return the epmTRefTypeMarche
     */
    public EpmTRefTypeMarche getEpmTRefTypeMarche() {
        return epmTRefTypeMarche;
    }

    public void setEpmTRefTypeMarche(final EpmTRefTypeMarche valeur) {
        this.epmTRefTypeMarche = valeur;
    }

    /**
     *
     * @return  true si enveloppe unique autorisé par defaut.
     *          false si enveloppe unique autorisé par defaut.
     *          null enveloppe unique non autorisé
     */
    public boolean isEnveloppeUnique() {
        return enveloppeUnique;
    }

    public void setEnveloppeUnique(final boolean valeur) {
        this.enveloppeUnique = valeur;
    }

    /**
     * @return true si l'option "DCE en ligne" est associée à la procédure
     */
    public boolean isValeurDefautDceEnLigne() {
        return valeurDefautDceEnLigne;
    }

    /**
     * @param valeurDefautDceEnLigne spécifie si l'option "DCE en ligne" est associée à la procédure
     */
    public void setValeurDefautDceEnLigne(boolean valeurDefautDceEnLigne) {
        this.valeurDefautDceEnLigne = valeurDefautDceEnLigne;
    }

    /**
     * @return true si l'option "Chiffrement" est associée à la procédure
     */
    public boolean isValeurDefautChiffrement() {
        return valeurDefautChiffrement;
    }

    /**
     * @param valeurDefautChiffrement spécifie si l'option "Chiffrement" est associée à la procédure
     */
    public void setValeurDefautChiffrement(boolean valeurDefautChiffrement) {
        this.valeurDefautChiffrement = valeurDefautChiffrement;
    }

    /**
     * @return true si l'option "Signature" est associée à la procédure
     */
    public boolean isValeurDefautSignature() {
        return valeurDefautSignature;
    }

    /**
     * @param valeurDefautSignature spécifie si l'option "Signature" est associée à la procédure
     */
    public void setValeurDefautSignature(boolean valeurDefautSignature) {
        this.valeurDefautSignature= valeurDefautSignature;
    }

    /**
     * @return true si l'option "Enveloppe unique" est associée à la procédure
     */
    public boolean isValeurDefautEnveloppeUnique() {
        return valeurDefautEnveloppeUnique;
    }

    /**
     * @param valeurDefautEnveloppeUnique spécifie si l'option "Enveloppe unique" est associée à la procédure
     */
    public void setValeurDefautEnveloppeUnique(boolean valeurDefautEnveloppeUnique) {
        this.valeurDefautEnveloppeUnique = valeurDefautEnveloppeUnique;
    }

    /**
     * @return true si l'option "Phase successive" est associée à la procédure
     */
    public boolean isValeurDefautPhaseSuccessive() {
        return valeurDefautPhaseSuccessive;
    }

    /**
     * @param valeurDefautPhaseSuccessive spécifie si l'option "Phase successive" est associée à la procédure
     */
    public void setValeurDefautPhaseSuccessive(boolean valeurDefautPhaseSuccessive) {
        this.valeurDefautPhaseSuccessive = valeurDefautPhaseSuccessive;
    }

    /**
     * @return true si l'option "DCE en ligne" est modifiable
     */
    public boolean isModifiableDceEnLigne() {
        return modifiableDceEnLigne;
    }

    /**
     * @param modifiableDceEnLigne spécifie si l'option "DCE en ligne" est modifiable
     */
    public void setModifiableDceEnLigne(boolean modifiableDceEnLigne) {
        this.modifiableDceEnLigne = modifiableDceEnLigne;
    }

    /**
     * @return true si l'option "Réponse électronique" est modifiable
     */
    public boolean isModifiableReponseElectronique() {
        return modifiableReponseElectronique;
    }

    /**
     * @param modifiableReponseElectronique spécifie si l'option "Réponse électronique" est modifiable
     */
    public void setModifiableReponseElectronique(boolean modifiableReponseElectronique) {
        this.modifiableReponseElectronique = modifiableReponseElectronique;
    }

    /**
     * @return true si l'option "Chiffrement" est modifiable
     */
    public boolean isModifiableChiffrement() {
        return modifiableChiffrement;
    }

    /**
     * @param modifiableChiffrement spécifie si l'option "Chiffrement" est modifiable
     */
    public void setModifiableChiffrement(boolean modifiableChiffrement) {
        this.modifiableChiffrement = modifiableChiffrement;
    }

    /**
     * @return true si l'option "Signature" est modifiable
     */
    public boolean isModifiableSignature() {
        return modifiableSignature;
    }

    /**
     * @param modifiableSignature spécifie si l'option "Signature" est modifiable
     */
    public void setModifiableSignature(boolean modifiableSignature) {
        this.modifiableSignature = modifiableSignature;
    }

    /**
     *
     * @return true si l'option "Enveloppe unique" est modifiable
     */
    public boolean isModifiableEnveloppeUnique() {
        return modifiableEnveloppeUnique;
    }

    /**
     *
     * @param modifiableEnveloppeUnique spécifie si l'option "Enveloppe unique" est modifiable
     */
    public void setModifiableEnveloppeUnique(boolean modifiableEnveloppeUnique) {
        this.modifiableEnveloppeUnique = modifiableEnveloppeUnique;
    }

    /**
     *
     * @return true si l'option "Phase successive" est modifiable
     */
    public boolean isModifiablePhaseSuccessive() {
        return modifiablePhaseSuccessive;
    }

    /**
     *
     * @param modifiablePhaseSuccessive spécifie si l'option "Phase successive" est modifiable
     */
    public void setModifiablePhaseSuccessive(boolean modifiablePhaseSuccessive) {
        this.modifiablePhaseSuccessive = modifiablePhaseSuccessive;
    }

    /**
     *
     * @return true si l'option "Nombre de candidat" est modifiable
     */
    public boolean isModifiableNbCandidat() {
        return modifiableNbCandidat;
    }

    /**
     *
     * @param modifiableNbCandidat spécifie si l'option "Nombre de candidat" est modifiable
     */
    public void setModifiableNbCandidat(boolean modifiableNbCandidat) {
        this.modifiableNbCandidat = modifiableNbCandidat;
    }

    /**
     *
     * @return le type de la procédure
     */
    public String getTypeProcedure() {
        return typeProcedure;
    }

    public void setTypeProcedure(String valeur) {
        this.typeProcedure = valeur;
    }

    /**
     *
     * @return le complement boamp tel qu'il sera vu dans le flux xml
     */
    public String getProcedureBOAMP() {
        return procedureBOAMP;
    }

    /**
     *
     * @param valeur le complement boamp tel qu'il sera vu dans le flux xml
     */
    public void setProcedureBOAMP(String valeur) {
        this.procedureBOAMP = valeur;
    }

    /**
     *
     * @return l'identifiant pour la fiche de recensement
     */
    public String getFicheRecensement() {
        return ficheRecensement;
    }

    /**
     *
     * @param valeur l'identifiant pour la fiche de recensement
     */
    public void setFicheRecensement(String valeur) {
        this.ficheRecensement = valeur;
    }

    /**
     *
     * @return le numero de ligne utilise pour generer un document de deliberation xls
     */
    public Integer getDocumentDeliberation() {
        return documentDeliberation;
    }

    /**
     *
     * @param valeur le numero de ligne utilise pour generer un document de deliberation xls
     */
    public void setDocumentDeliberation(Integer valeur) {
        this.documentDeliberation = valeur;
    }

    /**
     * @return Libelle de la procedure dans un document ooffice.
     */
    public String getDocumentLibelle() {
        return documentLibelle;
    }

    /**
     * @param valeur Libelle de la procedure dans un document ooffice.
     */
    public void setDocumentLibelle(String valeur) {
        this.documentLibelle = valeur;
    }
    /**
     * @return Indique si l'acte d'engement a signature propre est active par defaut.
     */
    public boolean isValeurDefautSignatureEngagement() {
        return valeurDefautSignatureEngagement;
    }
    /**
     * @param valeur Indique si l'acte d'engement a signature propre est active par defaut.
     */
    public void setValeurDefautSignatureEngagement(final boolean valeur) {
        this.valeurDefautSignatureEngagement = valeur;
    }
    /**
     * @return Indique si l'acte d'engement a signature propre est modifiable.
     */
    public boolean isModifiableSignatureEngagement() {
        return modifiableSignatureEngagement;
    }
    /**
     * @param valeur Indique si l'acte d'engement a signature propre est modifiable.
     */
    public void setModifiableSignatureEngagement(final boolean valeur) {
        this.modifiableSignatureEngagement = valeur;
    }


    /**
     * @return Indique si MPS est modifiable.
     */
    public boolean isModifiableMPS() {
        return modifiableMPS;
    }
    /**
     * @param valeur Indique si MPS est modifiable.
     */
    public void setModifiableMPS(boolean valeur) {
        this.modifiableMPS = valeur;
    }
    /**
     * @return Indique si MPS est actif par defaut.
     */
    public boolean isValeurDefautMPS() {
        return valeurDefautMPS;
    }
    /**
     * @param valeur Indique si MPS est actif par defaut.
     */
    public void setValeurDefautMPS(boolean valeur) {
        this.valeurDefautMPS = valeur;
    }

    public EpmTRefReponseElectronique getRefReponseElectronique() {
        return refReponseElectronique;
    }

    public void setRefReponseElectronique(
            final EpmTRefReponseElectronique valeur) {
        this.refReponseElectronique = valeur;
    }

    public Set<EpmTRefParametrage> getEpmTRefParametrageSet() {
        return epmTRefParametrageSet;
    }

    public void setEpmTRefParametrageSet(Set<EpmTRefParametrage> epmTRefParametrageSet) {
        this.epmTRefParametrageSet = epmTRefParametrageSet;
    }

    public Set<EpmTRefOrganisme> getEpmTRefOrganismeSet() {
        return epmTRefOrganismeSet;
    }

    public void setEpmTRefOrganismeSet(Set<EpmTRefOrganisme> epmTRefOrganismeSet) {
        this.epmTRefOrganismeSet = epmTRefOrganismeSet;
    }

    public boolean isProcedurePersonnalise() {
        return procedurePersonnalise;
    }

    public void setProcedurePersonnalise(boolean procedurePersonnalise) {
        this.procedurePersonnalise = procedurePersonnalise;
    }

	@Override
	public String toString() {
		return MessageFormat.format("''{0}'' (id = {1,number,#}, code externe = ''{2}'', libellé court = ''{3}'')",
			 this.getLibelle(), this.getId(), this.getCodeExterne(), this.getLibelleCourt());
	}

	@Override
	public String getUid() {
		return this.getCodeExterne();
	}

	public boolean estPAInf() {
		return CODE_MAPA_INF_PERSO.equals(this.codeExterne);
	}

	public boolean estPASup() {
		return CODE_MAPA_SUP_PERSO.equals(this.codeExterne);
	}

	public boolean estPurePAInf() {
		return CODE_MAPA_INF_PERSO.equals(this.codeExterne) && CODE_MAPA_INF_PERSO.equals(this.libelleCourt);
	}

	public boolean estPurePASup() {
		return CODE_MAPA_SUP_PERSO.equals(this.codeExterne) && CODE_MAPA_SUP_PERSO.equals(this.libelleCourt);
	}

	public boolean estPA() {
		return this.estPAInf() || this.estPASup();
	}

	public boolean estPurePA() {
		return this.estPurePAInf() || this.estPurePASup();
	}

	public boolean estProcedurePersonnalisee() {
		// Une PA non pure
		return this.estPA() && !this.estPurePA();
	}

	@Override
	public boolean estExportable(){
		// Tout sauf les procedures personnalisées
		return !this.estProcedurePersonnalisee();
	}
}
