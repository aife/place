package fr.paris.epm.noyau.commun.exception;

/**
 * Erreur lance lorsqu'une etape du workflow JBPM n'est pas validee.
 * @author RVI
 */
public class EtapeJbpmNonValideeNoyauException extends Exception {

    private static final long serialVersionUID = -8230500782213992177L;

    public EtapeJbpmNonValideeNoyauException() {
        super();
    }

    public EtapeJbpmNonValideeNoyauException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    public EtapeJbpmNonValideeNoyauException(final String msg) {
        super(msg);
    }

    public EtapeJbpmNonValideeNoyauException(final Throwable cause) {
        super(cause);
    }
}
