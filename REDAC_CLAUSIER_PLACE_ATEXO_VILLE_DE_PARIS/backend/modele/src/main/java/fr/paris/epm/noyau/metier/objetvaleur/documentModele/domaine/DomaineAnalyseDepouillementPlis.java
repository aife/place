package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine;

import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineOuvertureOffre;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineOuvertureReponseEnregistrementCandidature;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomainePropositionClassementOffre;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.DomaineValidationClassementOffre;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le domaine d' analyse
 * des plis / Dépouillement.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineAnalyseDepouillementPlis implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private DomaineOuvertureReponseEnregistrementCandidature ouvertureReponseEnregistrementCandidature;

    private DomaineOuvertureOffre ouvertureOffre;

    private DomainePropositionClassementOffre propositionClassementOffre;

    private DomaineValidationClassementOffre validationClassementOffre;


    public final DomaineOuvertureReponseEnregistrementCandidature getOuvertureReponseEnregistrementCandidature() {
        return ouvertureReponseEnregistrementCandidature;
    }

    public final void setOuvertureReponseEnregistrementCandidature(
            final DomaineOuvertureReponseEnregistrementCandidature ouvertureReponseEnregistrementCandidature) {
        this.ouvertureReponseEnregistrementCandidature = ouvertureReponseEnregistrementCandidature;
    }

    public final DomaineOuvertureOffre getOuvertureOffre() {
        return ouvertureOffre;
    }

    public final void setOuvertureOffre(final DomaineOuvertureOffre valeur) {
        this.ouvertureOffre = valeur;
    }

    public final DomainePropositionClassementOffre getPropositionClassementOffre() {
        return propositionClassementOffre;
    }

    public final void setPropositionClassementOffre(
            final DomainePropositionClassementOffre valeur) {
        this.propositionClassementOffre = valeur;
    }

    public final DomaineValidationClassementOffre getValidationClassementOffre() {
        return validationClassementOffre;
    }

    public final void setValidationClassementOffre(
            final DomaineValidationClassementOffre valeur) {
        this.validationClassementOffre = valeur;
    }
}
