package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefMoyenNotification;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefStatutNotificationContrat;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeAttributaire;

import java.util.Date;
import java.util.Set;

/**
 * Pojo hibernate - Attributaire du contrat
 * 
 * @author Rebeca Dantas
 *
 */
public class EpmTAttributaire extends EpmTAbstractObject {

	/**
	 * Serialisation
	 */
	private static final long serialVersionUID = 4343284265055034297L;
	
    private Integer id;

	/**
	 * Raison social du attributaire
	 */
	private String raisonSocial;

	/**
	 * Select - Le type d'attributaire : entreprise générale, group. conjoint ou
	 * solidaire
	 */
	private EpmTRefTypeAttributaire typeAttributaire;
		
	/**
	 * L'ensemble de contractants 
	 */
	private Set<EpmTContractant> contractants;
	
	private String siren;
	
	private String siret;

	/**
     * L'identifiant du moyen de notification - alimenté par passation 
     */
    private EpmTRefMoyenNotification moyenNotification;
    
    /**
     * Vaut 0 (contrat provisoire) tant que la notification n’a pas été saisie et enregistrée.
     * Vaut 1 sinon (contrat notifié).
     */
    private EpmTRefStatutNotificationContrat statutNotificationContrat;
    
    /**
     * la date d’envoi pour « Remise en main propre »
     */
    private Date dateEnvoiRemise;
    
    /**
     * la date pour « Courrier AR  »
     */
    private Date dateAR;
    
    /**
     * Le champ de saisie lorsque la confirmation d'attribution. Il s’agit de la date d’attribution. 
     */
    private Date dateAttribution;
	
	/**
	 * @return raisonSocial : retourne la raison social du attributaire
	 */
	public String getRaisonSocial() {
		return raisonSocial;
	}

	/**
	 * @param valeur : défine la raison social du attributaire

	 */
	public void setRaisonSocial(final String valeur) {
		this.raisonSocial = valeur;
	}

	/**
	 * @return typeAttributaire : retourne le type d'attributaire (entreprise générale, group. conjoint ou solidaire)
	 */
	public EpmTRefTypeAttributaire getTypeAttributaire() {
		return typeAttributaire;
	}

	/**
	 * @param valeur : défine le type d'attributaire (entreprise générale, group. conjoint ou solidaire)
	 */
	public void setTypeAttributaire(final EpmTRefTypeAttributaire valeur) {
		this.typeAttributaire = valeur;
	}

	/**
	 * @return moyenNotification : retourne l'identifiant du moyen de notification - alimenté par passation
	 */
	public EpmTRefMoyenNotification getMoyenNotification() {
		return moyenNotification;
	}

	/**
	 * @param valeur : défine l'identifiant du moyen de notification - alimenté par passation 

	 */
	public void setMoyenNotification(final EpmTRefMoyenNotification valeur) {
		this.moyenNotification = valeur;
	}
	


	/**
	 * @return contractants : retourne l'ensemble de contractants 
	 */
	public Set<EpmTContractant> getContractants() {
		return contractants;
	}

	/**
	 * @param valeur : define l'ensemble de contractants 
	 */
	public void setContractants(final Set<EpmTContractant> valeur) {
		this.contractants = valeur;
	}

    public Integer getId() {
        return id;
    }

    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    /**
     * @return the siren
     */
    public String getSiren() {
        return siren;
    }

    /**
     * @param siren the siren to set
     */
    public void setSiren(final String valeur) {
        this.siren = valeur;
    }

    /**
     * @return the statutContrat
     */
    public EpmTRefStatutNotificationContrat getStatutNotificationContrat() {
        return statutNotificationContrat;
    }

    /**
     * @return the dateEnvoiRemise
     */
    public Date getDateEnvoiRemise() {
        return dateEnvoiRemise;
    }

    /**
     * @return the dateAR
     */
    public Date getDateAR() {
        return dateAR;
    }

    /**
     * @param statutNotificationContrat the statutContrat to set
     */
    public void setStatutNotificationContrat(final EpmTRefStatutNotificationContrat valeur) {
        this.statutNotificationContrat = valeur;
    }

    /**
     * @param dateEnvoiRemise the dateEnvoiRemise to set
     */
    public void setDateEnvoiRemise(final Date valeur) {
        this.dateEnvoiRemise = valeur;
    }

    /**
     * @param dateAR the dateAR to set
     */
    public void setDateAR(final Date valeur) {
        this.dateAR = valeur;
    }

    /**
     * @return dateAttribution
     */
    public Date getDateAttribution() {
        return dateAttribution;
    }

    /**
     * @param dateAttribution dateAttribution to set
     */
    public void setDateAttribution(final Date valeur) {
        this.dateAttribution = valeur;
    }

    /**
     * @return siret
     */
    public String getSiret() {
        return siret;
    }

    /**
     * @param siret siret to set
     */
    public void setSiret(final String valeur) {
        this.siret = valeur;
    }	
    
    
}
