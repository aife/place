/**
 * 
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefFormatEchange;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Document échangé pour l'écran du suivi des échanges.
 * @author Mounthei
 * @version $Revision:$, $Date: $, $Author: $
 */
public class EpmTDocumentEchange implements Comparable<EpmTDocumentEchange>, Serializable {
    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Id du document échangé.
     */
    private int id;
    /**
     * L'identifiant de la consultation liée.
     */
    private Integer idConsultation;
    
    /**
     * identifiant de l'avenant.
     */
    private Integer idAvenant;
    
    /**
     * Le lot rattaché aux documents échangés.
     */
    private EpmTBudLot lot;
    /**
     * L'étape rattaché aux documents échangés.
     */
    private EpmTEtapeSpec etape;
    /**
     * L'objet des documents échangés.
     */
    private String objet = "";
    /**
     * La date de création de cette échange.
     */
    private Date date;
    /**
     * Le format d'échange utilisée pour ce type d'échange.
     */
    private EpmTRefFormatEchange formatEchange;
    /**
     * Expéditeur de l'échange.
     */
    private String expediteur = "";
    /**
     * Destinataire de l'échange.
     */
    private String destinataire = "";
    /**
     * Description de l'expéditeur dans le cas où ce n'est pas un pouvoir
     * adjudicateur ou un candidat.
     */
    private String expediteurAutre = "";
    /**
     * Description du destinataire dans le cas où ce n'est pas un pouvoir
     * adjudicateur ou un candidat.
     */
    private String destinataireAutre = "";
    /**
     * Commentaire associé à l'échange.
     */
    private String commentaire = "";
    /**
     * Ensemble des pièces jointes rattachés à cette échange.
     */
    private Set piecesJointes = new HashSet(0);

    // Méthodes
    /**
     * Utilise uniquement la référence.
     * @see java.lang.Object#hashCode()
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (id != 0) {
            result += Constantes.PREMIER + Integer.valueOf(id).hashCode();
        }
        if (objet != null) {
            result += Constantes.PREMIER + objet.hashCode();
        }
        if (commentaire != null) {
            result += Constantes.PREMIER + commentaire.hashCode();
        }
        if (destinataire != null) {
            result += Constantes.PREMIER + destinataire.hashCode();
        }
        if (expediteur != null) {
            result += Constantes.PREMIER + expediteur.hashCode();
        }
        return result;
    }

    /**
     * Deux objets de même référence sont égaux.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTDocumentEchange autre = (EpmTDocumentEchange) obj;

        if (!((id == autre.getId())
                && (commentaire.equals(autre.getCommentaire()))
                && (date.equals(autre.getDate()))
                && (destinataire.equals(autre.getDestinataire()))
                && (destinataireAutre.equals(autre.getDestinataireAutre())) && (expediteur
                .equals(autre.getExpediteur())
                && (expediteurAutre.equals(autre.getExpediteurAutre())) && (objet
                .equals(autre.getObjet()))))) {
            return false;
        }

        return true;
    }
    
    public Object clone() throws CloneNotSupportedException {
        EpmTDocumentEchange documentEchange = new EpmTDocumentEchange();
        documentEchange.setId(0);
        documentEchange.setCommentaire(commentaire);
        documentEchange.setDate(date);
        documentEchange.setDestinataire(destinataire);
        documentEchange.setDestinataireAutre(destinataireAutre);
        documentEchange.setEtape(etape);
        documentEchange.setExpediteur(expediteur);
        documentEchange.setExpediteurAutre(expediteurAutre);
        documentEchange.setFormatEchange(formatEchange);
        documentEchange.setIdAvenant(idAvenant);
        documentEchange.setIdConsultation(idConsultation);
        documentEchange.setLot(lot);
        documentEchange.setObjet(objet);
        return documentEchange;
    }

    /**
     * @return le destinataire.
     */
    public String getDestinataire() {
        return destinataire;
    }

    /**
     * @param valeur le destinataire de l'échange.
     */
    public void setDestinataire(final String valeur) {
        this.destinataire = valeur;
    }

    /**
     * @return la description du destinataire de l'échange dans le cas où
     *         celui-ci n'est pas un pouvoir adjudicateur ou un candidat.
     */
    public String getDestinataireAutre() {
        return destinataireAutre;
    }

    /**
     * @param valeur la description du destinataire de l'échange dans le
     *            cas où celui-ci n'est pas un pouvoir adjudicateur ou un
     *            candidat.
     */
    public void setDestinataireAutre(final String valeur) {
        destinataireAutre = valeur;
    }

    /**
     * @return le commentaire lié à cette échange.
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @param valeur le commentaire lié à cette échange.
     */
    public void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    /**
     * @return identifiant de la la consultation liée
     */
    public Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param valeur identifiant de la la consultation liée
     */
    public void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @return la date associé à cette échange.
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param valeur la date à associer à cette échange.
     */
    public void setDate(final Date valeur) {
        this.date = valeur;
    }

    /**
     * @return l'expéditeur associé à l'échange.
     */
    public String getExpediteur() {
        return expediteur;
    }

    /**
     * @param valeur l'expéditeur à associé à l'échange.
     */
    public void setExpediteur(final String valeur) {
        this.expediteur = valeur;
    }

    /**
     * @return la description de l'expéditeur si celui ci n'est pas un
     *         pouvoir adjudicateur ou un candidat.
     */
    public String getExpediteurAutre() {
        return expediteurAutre;
    }

    /**
     * @param valeur la description de l'expéditeur si celui ci n'est pas
     *            un pouvoir adjudicateur ou un candidat.
     */
    public void setExpediteurAutre(final String valeur) {
        this.expediteurAutre = valeur;
    }

    /**
     * @return l'étape associée à l'échange.
     */
    public EpmTEtapeSpec getEtape() {
        return etape;
    }

    /**
     * @param valeur l'étape à associer à l'échange.
     */
    public void setEtape(final EpmTEtapeSpec valeur) {
        this.etape = valeur;
    }

    /**
     * @return le format d'échange.
     */
    public EpmTRefFormatEchange getFormatEchange() {
        return formatEchange;
    }

    /**
     * @param valeur le format d'échange à associer.
     */
    public void setFormatEchange(final EpmTRefFormatEchange valeur) {
        this.formatEchange = valeur;
    }

    /**
     * @return le lot associé à l'échange.
     */
    public EpmTBudLot getLot() {
        return lot;
    }

    /**
     * @param valeur le lot à associer à l'échange.
     */
    public void setLot(final EpmTBudLot valeur) {
        this.lot = valeur;
    }

    /**
     * @return l'objet associé à l'échange.
     */
    public String getObjet() {
        return objet;
    }

    /**
     * @param valeur à associer à l'échange.
     */
    public void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return la liste des pièces jointes liées à l'échange.
     */
    public Set getPiecesJointes() {
        return piecesJointes;
    }

    /**
     * @param valeur la liste des pièces jointes à associer à l'échange.
     */
    public void setPiecesJointes(final Set valeur) {
        this.piecesJointes = valeur;
    }

    /**
     * @return l'id du document échangé courant.
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur la valeur de l'id à asisgner au document échangé
     *            courant.
     */
    public void setId(int valeur) {
        this.id = valeur;
    }
    
    /**
     * @param valeur identifiant de l'avenant
     */
    public void setIdAvenant(final Integer valeur) {
        this.idAvenant = valeur;
    }
    
    /**
     * @param valeur identifiant de l'avenant
     */
    public Integer getIdAvenant() {
        return idAvenant;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(final EpmTDocumentEchange echange) {
        return this.etape.getLibelle().compareTo(
                                                 echange.getEtape()
                                                         .getLibelle());
    }

}
