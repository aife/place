package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefUnite de la table "epm__t_ref_unite"
 *
 * Unité des bons de commande affiché dans le formulaire amont en cas de forme
 * de prix unitaire / mixte, ainsi que sur les écrans de recommandation des
 * candidature, pré inscription, inscription, tableau délibération et héritée
 * dans le module rédaction.
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefUnite extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Indique si le référetiel est le référentiel à afficher par defaut.
     */
    private boolean valeurParDefaut;

    public boolean isValeurParDefaut() {
        return valeurParDefaut;
    }

    public void setValeurParDefaut(final boolean valeur) {
        this.valeurParDefaut = valeur;
    }

}