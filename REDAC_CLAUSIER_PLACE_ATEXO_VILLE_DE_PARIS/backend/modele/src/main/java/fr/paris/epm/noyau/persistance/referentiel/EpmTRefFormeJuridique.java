package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefFormeJuridique de la table "epm__t_ref_forme_juridique"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefFormeJuridique extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

}