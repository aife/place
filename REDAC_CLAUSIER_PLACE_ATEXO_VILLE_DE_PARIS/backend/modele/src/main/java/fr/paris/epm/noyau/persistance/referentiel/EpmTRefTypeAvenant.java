package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeAvenant de la table "epm__t_ref_type_avenant"
 * Created by nty on 30/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeAvenant extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

}