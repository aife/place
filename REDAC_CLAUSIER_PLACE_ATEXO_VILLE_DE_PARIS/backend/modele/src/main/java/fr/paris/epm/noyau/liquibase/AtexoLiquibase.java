package fr.paris.epm.noyau.liquibase;

import liquibase.integration.spring.SpringLiquibase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Liquibase Adapter to work with file system ressources
 * Created by sta on 25/11/15.
 */
public class AtexoLiquibase extends SpringLiquibase {

    @Override
    protected SpringResourceOpener createResourceOpener() {
        return new AtexoResourceOpener(getChangeLog());
    }

    public class AtexoResourceOpener extends SpringResourceOpener {
        public AtexoResourceOpener(String parentFile) {
            super(parentFile);
        }

        @Override
        public Set<InputStream> getResourcesAsStream(String path) throws IOException {
            Set<InputStream> returnSet;
            try {
                returnSet = super.getResourcesAsStream(path);
            } catch (IOException e) {
                //allow working with external files (not only classpath resources)
                File ressource = new File(path);
                if (ressource.exists()) {
                    returnSet = new HashSet<>();
                    FileInputStream inputStream = new FileInputStream(path);
                    returnSet.add(inputStream);
                } else {
                    throw e;
                }
            }
            return returnSet;
        }
    }

}
