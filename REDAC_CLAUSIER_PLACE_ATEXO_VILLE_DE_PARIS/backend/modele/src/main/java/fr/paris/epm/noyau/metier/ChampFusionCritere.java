/**
 * 
 */
package fr.paris.epm.noyau.metier;

import java.util.Map;

/**
 * Critere de recherche pour les champs de fusion.
 * 
 * @author Léon Barsamian
 *
 */
public class ChampFusionCritere extends AbstractCritere {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -8184744750242244620L;

    /**
     * Identifiant du domaine de données pour lequel on recherche les champs de fusion.
     */
    private Integer idDomaineDonnee;

    /**
     * Identifiant du champ de fusion en base.
     */
    private Integer id; 
    
    private String libelle;
    
	private Boolean moduleExecution;

	private Boolean isSimple; // , NULL -> tous, True -> SIMPLE, False -> COMPLEXE
    
    /**
     * Construction du corps de la requette.
     */
    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map parametres = getParametres();
        
        sb.append("from EpmTRefChampFusion c");
        if (idDomaineDonnee != null) {
            debut = gererDebut(debut, sb);
            sb.append("c.domaineDonnees.id = :idDomaineDonnee");
            parametres.put("idDomaineDonnee", idDomaineDonnee);
        }
        if (id != null) {
            debut = gererDebut(debut, sb);
            sb.append("c.id = :id");
            parametres.put("id", id);
        }
        if (libelle != null) {
            debut = gererDebut(debut, sb);
            sb.append("c.libelle = :libelle");
            parametres.put("libelle", libelle);
        }
        if (moduleExecution != null) {
            debut = gererDebut(debut, sb);
            if(moduleExecution)
                sb.append("c.moduleExecution = TRUE");
            else
                sb.append("c.moduleExecution = FALSE");
        }
        if (isSimple != null) {
            debut = gererDebut(debut, sb);
            if(isSimple)
                sb.append("c.class = EpmTRefChampFusionSimple");
            else
                sb.append("c.class = EpmTRefChampFusionComplexe");
        }
        debut = gererDebut(debut, sb);
        sb.append("( c.actif = true OR c.class = EpmTRefChampFusionComplexe )");

        return sb;
    }

    /**
     * Construction de la requette pour obtenir le nombre de résultat total.
     */
    public String toCountHQL() {
        return "select count(*) " + corpsRequete();
    }

    /**
     * Construction de la requette pour obtenir la liste des résultats.
     */
    public String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null)
            sb.append(" order by c.").append(proprieteTriee);
        return sb.toString();
    }

    public void setIdDomaineDonnee(final Integer valeur) {
        this.idDomaineDonnee = valeur;
    }

    public final void setId(final Integer valeur) {
        this.id = valeur;
    }

    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

	public void setModuleExecution(final Boolean moduleExecution) {
        this.moduleExecution = moduleExecution;
    }

    public void setSimple(final Boolean simple) {
        isSimple = simple;
    }

}
