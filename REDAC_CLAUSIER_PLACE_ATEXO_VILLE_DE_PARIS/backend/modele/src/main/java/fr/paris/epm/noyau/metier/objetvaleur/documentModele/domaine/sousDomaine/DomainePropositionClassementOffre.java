package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;


/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.propositionClassementOffre.
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomainePropositionClassementOffre implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * type de critère en cas de non-allotie ou lot dissocie
     */
    private String typeCritere;

    /**
     * Liste de nom criteres, notes (par lot, affecte si consultation alloti).
     */
    private List<DomaineCritereCandidatLot> listeCriteresCandidatsParLots;

    /**
     * Liste de nom criteres, notes (par candidat, affecte si consultation non-alloti).
     */
    private List<DomaineCritereCandidat> listeCriteresCandidats;

    /**
     * Liste de nom criteres pour les candidatures non retenues, affecte si consultation non-alloti
     */
    private List<DomaineCritereCandidat> listeCriteresCandidatsNonRetenu;

    private String noteCritereOffre;

    /**
     * Liste des critères de tous les candidats.
     */
    private List<DomaineCritereCandidat> tableauAnalyseOffres;


    public List<DomaineCritereCandidat> getTableauAnalyseOffres() {
        return tableauAnalyseOffres;
    }

    public void setTableauAnalyseOffres(List<DomaineCritereCandidat> tableauAnalyseOffres) {
        this.tableauAnalyseOffres = tableauAnalyseOffres;
    }

    /**
     * @return Liste de nom criteres, notes (par lot, affecte si consultation alloti).
     */
    public final List<DomaineCritereCandidatLot> getListeCriteresCandidatsParLots() {
        return listeCriteresCandidatsParLots;
    }

    /**
     * @param valeur Liste de nom criteres, notes (par lot, affecte si consultation alloti).
     */
    public final void setListeCriteresCandidatsParLots(final
                                                       List<DomaineCritereCandidatLot> valeur) {
        this.listeCriteresCandidatsParLots = valeur;
    }

    /**
     * @return Liste de nom criteres, notes (par candidat, affecte si consultation non-alloti).
     */
    public final List<DomaineCritereCandidat> getListeCriteresCandidats() {
        return listeCriteresCandidats;
    }

    /**
     * @param valeur Liste de nom criteres, notes (par candidat, affecte si consultation non-alloti).
     */
    public final void setListeCriteresCandidats(final
                                                List<DomaineCritereCandidat> valeur) {
        this.listeCriteresCandidats = valeur;
    }

    /**
     * @return the noteCritereOffre
     */
    public final String getNoteCritereOffre() {
        return noteCritereOffre;
    }

    /**
     * @param noteCritereOffre the noteCritereOffre to set
     */
    public final void setNoteCritereOffre(final String valeur) {
        this.noteCritereOffre = valeur;
    }

    public final List<DomaineCritereCandidat> getListeCriteresCandidatsNonRetenu() {
        return listeCriteresCandidatsNonRetenu;
    }

    public final void setListeCriteresCandidatsNonRetenu(final List<DomaineCritereCandidat> valeur) {
        this.listeCriteresCandidatsNonRetenu = valeur;
    }

    public String getTypeCritere() {
        return typeCritere;
    }

    public void setTypeCritere(String valeur) {
        this.typeCritere = valeur;
    }

    public boolean getPonderationCritere() {
        return typeCritere != null && typeCritere.equals("ponderation");
    }

    public boolean getPointCritere() {
        return typeCritere != null && typeCritere.equals("point");
    }

    public DomaineCritereCandidat getCandidatsNonRetenuLot() {
        for (DomaineCritereCandidatLot lot : listeCriteresCandidatsParLots) {
            if (lot.getListeCriteresCandidatsNonRetenu() != null && !lot.getListeCriteresCandidatsNonRetenu().isEmpty())
                return lot.getListeCriteresCandidatsNonRetenu().get(0);
        }
        return null;
    }
}
