package fr.paris.epm.noyau.metier;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Classe critère utilisé pour la recherche des profils des utilisateurs.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ProfilCritere extends AbstractCritere implements Critere, Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * nom du profil.
     */
    private String nom;

    private boolean nomStrict = true;

    /**
     * id profil
     */
    private Integer idProfil;
    
    private List<Integer> idHabilitations;
    
    /**
     * Identifiant de l'organisme auquel le profil est rataché
     */
    private Integer idOrganisme;

    /**
     * @param valeur nom du profil à rechercher
     */
    public final void setNom(final String valeur) {
        this.nom = valeur;
    }

    public void setNomStrict(boolean nomStrict) {
        this.nomStrict = nomStrict;
    }

    /**
     * @param valeur id Profil
     */
    public final void setIdProfil(final Integer valeur) {
        idProfil = valeur;
    }

    /**
     * @param valeur l'id de la habilitations à définir
     */
    public void setIdHabilitations(final List<Integer> valeur) {
        this.idHabilitations = valeur;
    }

    /**
     * @param valeur the idOrganisme to set
     */
    public final void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

    /**
     * @return chaine HQL utilisé par Hibernate
     */
    public final StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        Map parametres = getParametres();
        boolean debut = false;
        sb.append("from EpmTProfil epmtprofil ");

        if (idProfil != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }

            sb.append("epmtprofil.id = :id");
            parametres.put("id", idProfil);
            debut = true;

        } else if (nom != null && !nom.trim().equals("")) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }

            if (nomStrict) {
                sb.append("lower(epmtprofil.nom) = lower(:nom)");
                parametres.put("nom", nom.trim());
            } else {
                sb.append("lower(epmtprofil.nom) like lower(:nom)");
                parametres.put("nom", pourcentTrim(nom));
            }
            debut = true;

        } else if (idHabilitations != null && !idHabilitations.isEmpty()) {

			sb.append(" JOIN FETCH epmtprofil.habilitationAssocies AS habilitation WHERE habilitation.id IN ( ");
            sb.append(idHabilitations.get(0));

            for (Integer h : idHabilitations)
            	sb.append(", ").append(h);
			
			sb.append(")");
			debut = true;
		}
        
        if (idOrganisme != null) {
            debut = gererDebut(debut, sb);
            sb.append(" epmtprofil.idOrganisme = ");
            sb.append(idOrganisme);
        }
        
        return sb;
    }

    public final String toHQL() {
        StringBuffer sb = corpsRequete();
        if (proprieteTriee != null) {
            sb.append(" order by epmtprofil.").append(proprieteTriee);
            if (triCroissant)
                sb.append(" ASC ");
            else
                sb.append(" DESC ");
        }

        return sb.toString();
    }


    public String toCountHQL() {
        return "select count(*) " + corpsRequete();
    }
	
}
