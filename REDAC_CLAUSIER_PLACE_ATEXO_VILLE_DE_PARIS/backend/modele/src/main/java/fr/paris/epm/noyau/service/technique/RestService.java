package fr.paris.epm.noyau.service.technique;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public interface RestService {

	/**
	 * Récupération du REST template
	 * @return le REST template
	 */
	RestTemplate getRestTemplate();
}
