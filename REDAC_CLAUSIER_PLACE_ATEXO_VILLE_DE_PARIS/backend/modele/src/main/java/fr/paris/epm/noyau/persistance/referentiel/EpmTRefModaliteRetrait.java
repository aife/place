package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefModaliteRetrait de la table "epm__t_ref_modalite_retrait"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefModaliteRetrait extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}