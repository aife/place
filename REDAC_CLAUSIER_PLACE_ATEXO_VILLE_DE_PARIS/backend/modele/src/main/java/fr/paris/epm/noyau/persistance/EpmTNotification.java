package fr.paris.epm.noyau.persistance;

import javax.persistence.Transient;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Offre d'une entreprise pour une consultation ou 1 ou plusieurs lots.
 *
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTNotification extends EpmTAbstractObject implements Cloneable {

    public final static String DECHIFFREMENT_OFFRE_TERMINE = "DECHIFFREMENT_OFFRE_TERMINE";
    public final static String DECHIFFREMENT_CANDIDATURE_TERMINE = "DECHIFFREMENT_CANDIDATURE_TERMINE";
    public static final String DECHIFFREMENT_PROJET_TERMINE = "DECHIFFREMENT_PROJET_TERMINE";
    public static final String DECHIFFREMENT_REPONSE_TERMINE = "DECHIFFREMENT_REPONSE_TERMINE";
    public static final String DECHIFFREMENT_ANONYMAT_TERMINE = "DECHIFFREMENT_ANONYMAT_TERMINE";
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;
    /**
     * id de l'objet concernant la notification.
     */
    private int idObjetNotifie;
    /**
     * le nom de la classe concerné de la notification pour l'utilisé dans les critéres de recherches.
     */
    private String entite;
    /**
     * le nom de la classe concerné de la notification pour l'utilisé dans les critéres de recherches.
     */
    @Transient
    private String messageNotification;

    /*
    *   Différentes valeurs (enum ) pour les notifications
    * */
    /**
     * Date de notification.
     */
    private Date dateNotification;
    /**
     * utilisateurs notifiés
     **/
    private Set<EpmTUtilisateurNotification> notificationsUtilisateur = new HashSet<>();

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EpmTNotification that = (EpmTNotification) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getIdObjetNotifie() {
        return idObjetNotifie;
    }

    public void setIdObjetNotifie(int idObjetNotifie) {
        this.idObjetNotifie = idObjetNotifie;
    }

    public Date getDateNotification() {
        return dateNotification;
    }

    public void setDateNotification(Date dateNotification) {
        this.dateNotification = dateNotification;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntite() {
        return entite;
    }

    public void setEntite(String entite) {
        this.entite = entite;
    }

    public String getMessageNotification() {
        return messageNotification;
    }

    public void setMessageNotification(String messageNotification) {
        this.messageNotification = messageNotification;
    }

    public Set<EpmTUtilisateurNotification> getNotificationsUtilisateur() {
        return notificationsUtilisateur;
    }

    public void setNotificationsUtilisateur(Set<EpmTUtilisateurNotification> notificationsUtilisateur) {
        this.notificationsUtilisateur = notificationsUtilisateur;
    }
}
