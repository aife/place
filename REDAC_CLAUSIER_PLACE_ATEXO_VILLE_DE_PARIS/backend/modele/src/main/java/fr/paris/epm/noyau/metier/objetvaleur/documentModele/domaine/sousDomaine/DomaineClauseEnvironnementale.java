package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineClauseEnvironnementale {

    private Boolean clauseEnvironnementale;
    private Boolean specification;
    private Boolean condition;
    private Boolean attribution;

    public Boolean getClauseEnvironnementale() {
        return clauseEnvironnementale;
    }

    public void setClauseEnvironnementale(Boolean clauseEnvironnementale) {
        this.clauseEnvironnementale = clauseEnvironnementale;
    }

    public Boolean getSpecification() {
        return specification;
    }

    public void setSpecification(Boolean specification) {
        this.specification = specification;
    }

    public Boolean getCondition() {
        return condition;
    }

    public void setCondition(Boolean condition) {
        this.condition = condition;
    }

    public Boolean getAttribution() {
        return attribution;
    }

    public void setAttribution(Boolean attribution) {
        this.attribution = attribution;
    }

}
