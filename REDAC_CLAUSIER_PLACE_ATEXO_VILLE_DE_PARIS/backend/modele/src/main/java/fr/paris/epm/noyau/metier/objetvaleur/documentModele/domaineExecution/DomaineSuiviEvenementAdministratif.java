package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine 
 * Suivi administratif / Evénements administratifs
 * 
 * @author GAO Xuesong
 */
public class DomaineSuiviEvenementAdministratif implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contenu d’un événement administratif
	 */
	private String evenementAdmin; 
	
	/**
	 * Date d’un événement administratif
	 */
	private Date dateEvenementAdmin;

	
	public final String getEvenementAdmin() {
		return evenementAdmin;
	}

	public final void setEvenementAdmin(final String valeur) {
		this.evenementAdmin = valeur;
	}

	public final Date getDateEvenementAdmin() {
		return dateEvenementAdmin;
	}

	public final void setDateEvenementAdmin(final Date valeur) {
		dateEvenementAdmin = valeur;
	}
	
}
