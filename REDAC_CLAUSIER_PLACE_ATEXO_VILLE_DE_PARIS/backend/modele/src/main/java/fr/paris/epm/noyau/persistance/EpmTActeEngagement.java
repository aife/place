package fr.paris.epm.noyau.persistance;


/**
 * Plis d'acte d'engagement.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTActeEngagement extends EpmTPlisGenerique {
    
    private static final long serialVersionUID = 1157407047276629030L;
}
