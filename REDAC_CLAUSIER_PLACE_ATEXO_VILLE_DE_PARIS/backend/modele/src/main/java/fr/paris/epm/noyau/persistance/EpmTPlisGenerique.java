package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Collection;
import java.util.Set;

/**
 * Tous les plis.
 * 
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTPlisGenerique extends EpmTAbstractObject {

    private static final long serialVersionUID = 7545912319721436455L;

    /**
     * Pli d'offre, projet ou reponse.
     */
    public static final String OFFRE = "OFFRE";
    /**
     * Pli d'acte d'engagement.
     */
    public static final String ACTE_ENGAGEMENT = "ACTE_ENGAGEMENT";
    /**
     * Pli d'acte d'engagement.
     */
    public static final String ACTE_ENGAGEMENT_TRANSFERE = "ACTE_ENGAGEMENT_TRANSFERE";
    /**
     * Pli non anonyme.
     */
    public static final String OFFRE_TRANSFERE = "TRANSFERT_OFFRE";
    /**
     * Pli non anonyme.
     */
    public static final String CANDIDATURE = "CANDIDATURE";
    /**
     * Pli non anonyme.
     */
    public static final String ANONYMAT = "ANONYMAT";
    /**
     * Pli non anonyme.
     */
    public static final String ANONYMAT_TRANSFERE = "TRANSFERT_ANONYMAT";
    /**
     * Pli non anonyme.
     */
    public static final String CANDIDATURE_TRANSFERE = "TRANSFERT_CANDIDATURE";
    /**
     * Format de signature XADES.
     */
    public static final String FORMAT_SIGNATURE_XML = "xml";
    /**
     * Format de signature p7s.
     */
    public static final String FORMAT_SIGNATURE_P7S = "p7s";
    /**
     * document representant le plis de l'entreprise. Il s'agit du message
     * complet envoyé par le connecteur demat (descripteur.xml + plis zippé).
     */
    private EpmTDocument plisCompletChiffre;

    /**
     * Le plis déchiffré dans epm par l'applet java.
     */
    private EpmTDocument plisDechiffre;

    /**
     * depot associé au plis
     */
    private int idDepot;

    /**
     * True si le plis est chiffré
     */
    private boolean chiffrement;

    /**
     * Les id des epmtDocument représentant les blocs du plis dans l'odre énoncé
     * dans le fichié ReponseAnnonce.xml. les id sont séparé par des espaces.
     * utilisé par l'applet de dechiffrement de EPM.
     */
    private String ordreBlocs;

    /**
     * Identifiant.
     */
    private int id;

    /**
     * Sélect modalité de dépôt des plis.
     */
    private String type;


    /**
     * Liste des epmtDocuments représentant les blocs du plis.
     */
    private Set<EpmTDocument> blocsPlis;
    
    /**
     * La taille en octet de l'enssemble des blocs du pli chiffré.
     */
    private int taillePliChiffre;
    
    private String sha1sum;
    
    private String signature;

    /**
     * Nom du plis dechiffre (potentiellement different du nom du pli global
     * chiffre si plusieurs plis par candidature, offre...)
     */
    private String nomPliDechiffre;
    
    /**
     * Le format de la signature utilisé lors de la réponse. 
     * Utilisé pour choisir la méthode de vérification de la signature ainsi que pour définir l'extension du fichier de signature.
     */
    private String formatSignature;
    
    /**
     * Tripet de vérification du certificat.
     */
    private String verificationCertificat;
    
    /**
     * identifiant de registre de depôt dans MPE associé
     */
    private Integer idRegistreDemat;

    /**
     * Validité générale de la signature
     */
    private Boolean signatureValide;

    /**
     * Indique si l'ensemble en parametre contient au moins un pli acte engagement.
     */
    public static boolean auMoinsUnPliActeEngagement(Collection<? extends EpmTPlisGenerique> pliCol) {

        if (pliCol == null)
            return false;

        for (EpmTPlisGenerique pliItem : pliCol)
            if (pliItem instanceof EpmTActeEngagement)
                return true;
        
        return false;
    }


    public String getType() {
        return type;
    }

    /**
     * @param valeur marqueur de phase {@link Constantes} PHASE_
     */
    public void setType(final String valeur) {
        this.type = valeur;
    }

    /**
     * @return document representant le plis de l'entreprise chiffre.
     */
    public EpmTDocument getPlisCompletChiffre() {
        return plisCompletChiffre;
    }

    /**
     * @param valeur representant le plis de l'entreprise chiffre.
     */
    public void setPlisCompletChiffre(final EpmTDocument valeur) {
        this.plisCompletChiffre = valeur;
    }

    /**
     * @return document representant le plis de l'entreprise.
     */
    public EpmTDocument getPlisDechiffre() {
        return plisDechiffre;
    }

    /**
     * @param valeur representant le plis de l'entreprise.
     */
    public void setPlisDechiffre(final EpmTDocument valeur) {
        this.plisDechiffre = valeur;
    }

    public int getId() {
        return id;
    }

    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return dépot associé au plis
     */
    public int getIdDepot() {
        return idDepot;
    }

    /**
     * @param valeur depot associé au plis
     */
    public void setIdDepot(final int valeur) {
        this.idDepot = valeur;
    }

    /**
     * @return ordre des blocs utilisés par EPM
     */
    public String getOrdreBlocs() {
        return ordreBlocs;
    }

    /**
     * @param valeur ordre des blocs utilisés par EPM
     */
    public void setOrdreBlocs(final String valeur) {
        this.ordreBlocs = valeur;
    }

    /**
     * @return true si plis chiffré
     */
    public boolean isChiffrement() {
        return chiffrement;
    }

    /**
     * @param valeur true si plis chiffre
     */
    public void setChiffrement(final boolean valeur) {
        this.chiffrement = valeur;
    }

    public Set<EpmTDocument> getBlocsPlis() {
        return blocsPlis;
    }

    public void setBlocsPlis(Set<EpmTDocument> valeur) {
        this.blocsPlis = valeur;
    }

    public int getTaillePliChiffre() {
        return taillePliChiffre;
    }

    public void setTaillePliChiffre(final int valeur) {
        this.taillePliChiffre = valeur;
    }

    public String getSha1sum() {
        return sha1sum;
    }

    public void setSha1sum(final String valeur) {
        this.sha1sum = valeur;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(final String valeur) {
        this.signature = valeur;
    }    

    public String getNomPliDechiffre() {
        return nomPliDechiffre;
    }

    public void setNomPliDechiffre(final String valeur) {
        this.nomPliDechiffre = valeur;
    }

    public String getFormatSignature() {
        return formatSignature;
    }

    public void setFormatSignature(final String valeur) {
        this.formatSignature = valeur;
    }

    public String getVerificationCertificat() {
        return verificationCertificat;
    }

    public void setVerificationCertificat(final String valeur) {
        this.verificationCertificat = valeur;
    }

	public Integer getIdRegistreDemat() {
		return idRegistreDemat;
	}

	public void setIdRegistreDemat(final Integer valeur) {
		this.idRegistreDemat = valeur;
	}

    public Boolean getSignatureValide() {
        return signatureValide;
    }

    public void setSignatureValide(Boolean signatureValide) {
        this.signatureValide = signatureValide;
    }

}
