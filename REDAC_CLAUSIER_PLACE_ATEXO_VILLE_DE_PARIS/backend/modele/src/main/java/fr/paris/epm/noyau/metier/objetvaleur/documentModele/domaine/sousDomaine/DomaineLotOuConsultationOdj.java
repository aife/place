package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour une element du sous
 * domaine commission.domaineOrdreDuJour.listeConsultationOdj ou
 * commission.domaineOrdreDuJour.listeConsultationOdj.lotOdj
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineLotOuConsultationOdj implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private String formeDePrix;

    private int nbReconduction;

    private String estimationPf;

    private String estimationDqe;

    private String estimationBcMin;

    private String estimationBcMax;

    private String uniteLibelle;
    
    private String dateLimiteRemisePlis;

    public final String getFormeDePrix() {
        return formeDePrix;
    }

    public final void setFormeDePrix(final String valeur) {
        this.formeDePrix = valeur;
    }

    public final int getNbReconduction() {
        return nbReconduction;
    }

    public final void setNbReconduction(final int valeur) {
        this.nbReconduction = valeur;
    }

    public final String getEstimationPf() {
        return estimationPf;
    }

    public final void setEstimationPf(final String valeur) {
        this.estimationPf = valeur;
    }

    public final String getEstimationDqe() {
        return estimationDqe;
    }

    public final void setEstimationDqe(final String valeur) {
        this.estimationDqe = valeur;
    }

    public final String getEstimationBcMin() {
        return estimationBcMin;
    }

    public final void setEstimationBcMin(final String valeur) {
        this.estimationBcMin = valeur;
    }

    public final String getEstimationBcMax() {
        return estimationBcMax;
    }

    public final void setEstimationBcMax(final String valeur) {
        this.estimationBcMax = valeur;
    }

    public final String getUniteLibelle() {
        return uniteLibelle;
    }

    public final void setUniteLibelle(final String valeur) {
        this.uniteLibelle = valeur;
    }

    public final String getDateLimiteRemisePlis() {
        return dateLimiteRemisePlis;
    }

    public final void setDateLimiteRemisePlis(final String valeur) {
        this.dateLimiteRemisePlis = valeur;
    }
}
