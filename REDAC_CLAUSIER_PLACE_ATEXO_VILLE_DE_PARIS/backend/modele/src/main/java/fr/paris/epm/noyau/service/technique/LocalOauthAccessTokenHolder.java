package fr.paris.epm.noyau.service.technique;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;

@Component
public class LocalOauthAccessTokenHolder implements OauthAccessTokenHolder {
	private final Logger LOG = LoggerFactory.getLogger(LocalOauthAccessTokenHolder.class);

	@Autowired
	private OauthAccessTokenService oauthAccessTokenService;

	private String accessToken;

	private Instant updatedAt;

	@Value("${jeton.periode.defaut:86395000}")
	private Long tokenValidityPeriodInMillis;

	@Override
	public Optional<String> get() {
		LOG.debug("Récupération du jeton. La valeur vaut = '{}'", accessToken);

		return Optional.ofNullable(this.check() ? accessToken : null);
	}

	private void store( final String accessToken ) {
	}

	@Override
	public Credentials renew() {
		if (this.check()) {
			LOG.warn("Demande de renouvellement du jeton, mais l'actuel '{}' est encore valide pendant {} secondes", accessToken, updatedAt.plusMillis(tokenValidityPeriodInMillis).getEpochSecond() -  Instant.now().getEpochSecond());
		}

		var credentials = oauthAccessTokenService.retrieve();
		this.accessToken = credentials.getAccessToken();
		this.updatedAt = Instant.now();
		this.tokenValidityPeriodInMillis = credentials.getExpiresIn() * 1000;

		LOG.info("Définition/remplacement du jeton. La nouvelle valeur vaut = '{}'", accessToken);

		return credentials;
	}

	@Override
	public boolean check() {
		boolean result;

		if (null == updatedAt || null == accessToken) {
			LOG.debug("Le jeton n'est pas encore défini.");

			result = false;
		} else if (updatedAt.plusMillis(tokenValidityPeriodInMillis).isBefore(Instant.now())) {
			LOG.debug("Le jeton a expiré (il a éte généré a {} et la durée de validité est de {} secondes, il a donc expiré depuis {} secondes)", updatedAt, tokenValidityPeriodInMillis / 1000,  Instant.now().getEpochSecond() - updatedAt.plusMillis(tokenValidityPeriodInMillis).getEpochSecond());

			result = false;
		} else {
			LOG.debug("Le jeton '{}' est toujours valide", accessToken);

			result = true;
		}

		return result;
	}
}
