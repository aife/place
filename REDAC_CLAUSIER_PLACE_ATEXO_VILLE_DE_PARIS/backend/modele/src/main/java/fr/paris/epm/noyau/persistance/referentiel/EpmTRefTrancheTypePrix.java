package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTrancheTypePrix de la table "epm__t_ref_tranche_type_prix"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTrancheTypePrix extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}