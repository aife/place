package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

public class DomaineVentilationMontantUniteFonctionnelle implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * année de la ventilation
     */
    private String annee;
    /**
     * montant associée à l'année de la ventilation
     */
    private Double montant;
    /**
     * procédure applicable pour cette année compte tenu du montant
     */
    private String procedureApplicable;
    
    /**
     * @return the annee
     */
    public String getAnnee() {
        return annee;
    }
    /**
     * @param annee the annee to set
     */
    public void setAnnee(String annee) {
        this.annee = annee;
    }
    /**
     * @return the montant
     */
    public Double getMontant() {
        return montant;
    }
    /**
     * @param montant the montant to set
     */
    public void setMontant(Double montant) {
        this.montant = montant;
    }
    /**
     * @return the procedureApplicable
     */
    public String getProcedureApplicable() {
        return procedureApplicable;
    }
    /**
     * @param procedureApplicable the procedureApplicable to set
     */
    public void setProcedureApplicable(String procedureApplicable) {
        this.procedureApplicable = procedureApplicable;
    }
    
    
}
