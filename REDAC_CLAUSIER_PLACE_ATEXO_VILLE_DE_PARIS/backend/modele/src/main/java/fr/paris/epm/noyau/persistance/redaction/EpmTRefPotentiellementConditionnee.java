package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate de EpmTRefPotentiellementConditionnee.
 *
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
@Entity
@Table(name = "epm__t_ref_potentiellement_conditionnee", schema = "redaction")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "cacheReferentielSelect")
public class EpmTRefPotentiellementConditionnee extends BaseEpmTRefRedaction implements EpmTRefImportExport {

    /**
     * attribut methode libelle.
     */
    @Column(name = "methode_libelle")
    private String methodeLibelle;

    /**
     * attribut methodeId.
     */
    @Column(name = "methode_id")
    private String methodeId;

    /**
     * attribut methodeConsultatin.
     */
    @Column(name = "methode_consultation")
    private String methodeConsultation;

    /**
     * attribut critereBooleen.
     */
    @Column(name = "critere_booleen", nullable = false)
    private boolean critereBooleen;

    @Column(name = "multi_choix")
    private boolean multiChoix;

    /**
     * cette méthode renvoie l'attribut methodeLibelle.
     *
     * @return renvoyer l'attribut methodeLibelle.
     */
    public String getMethodeLibelle() {
        return methodeLibelle;
    }

    /**
     * cette méthode initialise l'attribut methode.
     *
     * @param valeur pour initialiser l'attribut methode.
     */

    public void setMethodeLibelle(final String valeur) {
        methodeLibelle = valeur;
    }

    /**
     * cette méthode teste si le critère est de type boolean.
     *
     * @return test si le critère est de type boolean.
     */
    public boolean isCritereBooleen() {
        return critereBooleen;
    }

    /**
     * cette méthode initialise l'attribut critereBooleen.
     *
     * @param valeur pour initialiser l'attribut critereBooleen.
     */
    public void setCritereBooleen(final boolean valeur) {
        critereBooleen = valeur;
    }

    public boolean isProcedurePassation() {
        return getId() == 3;
    }

    /**
     * cette méthode renvoie l'attribut methodeId.
     *
     * @return renvoyer l'attribut methodeId.
     */
    public String getMethodeId() {
        return methodeId;
    }

    /**
     * cette méthode initialise l'attribut methodeId.
     *
     * @param valeur pour initialiser l'attribut methodeId.
     */
    public void setMethodeId(final String valeur) {
        methodeId = valeur;
    }

    /**
     * cette méthode renvoie l'attribut methodeConsultation.
     *
     * @return renvoyer l'attribut methodeConsultation.
     */
    public String getMethodeConsultation() {
        return methodeConsultation;
    }

    /**
     * cette méthode initialise l'attribut methodeConsultation.
     *
     * @param valeur pour initialiser methodeConsultation.
     */
    public void setMethodeConsultation(final String valeur) {
        methodeConsultation = valeur;
    }

    public boolean isMultiChoix() {
        return multiChoix;
    }

    public void setMultiChoix(final boolean valeur) {
        this.multiChoix = valeur;
    }

    @Override
    public String toString() {
        return "EpmTRefPotentiellementConditionnee = [" +
                "methodeLibelle='" + methodeLibelle + '\'' +
                ", methodeId='" + methodeId + '\'' +
                ", methodeConsultation='" + methodeConsultation + '\'' +
                ", critereBooleen=" + critereBooleen +
                ", multiChoix=" + multiChoix +
                ']';
    }

    /**
     * cette méthode permet la comparaison entre deux objets.
     *
     * @param objet est le paramètre à comparer.
     * @return resultat de la comparaison.
     */
    @Override
    public final int compareTo(final EpmTRef objet) {
        return getLibelle().compareTo(objet.getLibelle());
    }

    @Override
    public String getUid() {
        return this.getCodeExterne();
    }
}
