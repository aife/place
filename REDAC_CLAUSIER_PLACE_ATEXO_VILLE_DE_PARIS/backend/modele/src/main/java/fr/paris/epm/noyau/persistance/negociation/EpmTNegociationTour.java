package fr.paris.epm.noyau.persistance.negociation;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Representer un tour de negociation lié à une offre ou un lot d'une offre
 */
public class EpmTNegociationTour extends EpmTAbstractObject implements Comparable, Cloneable {

    private static final long serialVersionUID = -7265671935934167615L;

    private int numeroTour;
    private Integer idLotDissocie;
    private NegociationTourEtatEnnum etatTour;
    private Set<EpmTNegociationTourLigne> negociationTourLignes;
    private Set<EpmTNegociationTourLot> negociationTourLots;


    public int getNumeroTour() {
        return numeroTour;
    }

    public void setNumeroTour(int numeroTour) {
        this.numeroTour = numeroTour;
    }

    public NegociationTourEtatEnnum getEtatTour() {
        return etatTour;
    }

    public void setEtatTour(NegociationTourEtatEnnum etatTour) {
        this.etatTour = etatTour;
    }

    public Set<EpmTNegociationTourLigne> getNegociationTourLignes() {
        return negociationTourLignes;
    }

    public void setNegociationTourLignes(Set<EpmTNegociationTourLigne> negociationTourLignes) {
        this.negociationTourLignes = negociationTourLignes;
    }


    public Set<EpmTNegociationTourLot> getNegociationTourLots() {
        return negociationTourLots;
    }

    public void setNegociationTourLots(Set<EpmTNegociationTourLot> negociationTourLots) {
        this.negociationTourLots = negociationTourLots;
    }

    public Integer getIdLotDissocie() {
        return idLotDissocie;
    }

    public void setIdLotDissocie(Integer idLotDissocie) {
        this.idLotDissocie = idLotDissocie;
    }


    @Override
    public int compareTo(Object o) {
        return this.numeroTour - ((EpmTNegociationTour) o).getNumeroTour();
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTNegociationTour empEpmTNegociationTour = (EpmTNegociationTour) super.clone();

        if (!CollectionUtils.isEmpty(this.negociationTourLignes)) {
            Set<EpmTNegociationTourLigne> lignes = new HashSet<>();
            for (EpmTNegociationTourLigne epmTNegociationTourLigne : this.negociationTourLignes) {
                lignes.add((EpmTNegociationTourLigne) epmTNegociationTourLigne.clone());
            }
            empEpmTNegociationTour.setNegociationTourLignes(lignes);
        }
        if (!CollectionUtils.isEmpty(this.negociationTourLots)) {
            Set<EpmTNegociationTourLot> lots = new HashSet<>();
            for (EpmTNegociationTourLot epmTNegociationTourLot : this.negociationTourLots) {
                lots.add((EpmTNegociationTourLot) epmTNegociationTourLot.clone());
            }
            empEpmTNegociationTour.setNegociationTourLots(lots);
        }


        return empEpmTNegociationTour;
    }
}
