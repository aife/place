package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * consultation.donneesComplementaires.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineDonneesComplementaires {

    private Integer nbReconductions = 0;

    private String modalitesReconduction;

    private String varianteObligatoireON;

    private String varianteAutoriseeON;
    
    private String reconductibleON;

    public final Integer getNbReconductions() {
        return nbReconductions;
    }

    public final void setNbReconductions(final Integer valeur) {
        nbReconductions = valeur;
    }

    public final String getModalitesReconduction() {
        return modalitesReconduction;
    }

    public final void setModalitesReconduction(final String valeur) {
        this.modalitesReconduction = valeur;
    }

    /**
     * @return the varianteObligatoireON
     */
    public final String getVarianteObligatoireON() {
        return varianteObligatoireON;
    }

    /**
     * @param varianteObligatoireON the varianteObligatoireON to set
     */
    public final void setVarianteObligatoireON(final String valeur) {
        this.varianteObligatoireON = valeur;
    }

    /**
     * @return the varianteAutoriseeON
     */
    public final String getVarianteAutoriseeON() {
        return varianteAutoriseeON;
    }

    /**
     * @param varianteAutoriseeON the varianteAutoriseeON to set
     */
    public final void setVarianteAutoriseeON(final String valeur) {
        this.varianteAutoriseeON = valeur;
    }

    /**
     * @return the reconductibleON
     */
    public final String getReconductibleON() {
        return reconductibleON;
    }

    /**
     * @param reconductibleON the reconductibleON to set
     */
    public final void setReconductibleON(final String valeur) {
        this.reconductibleON = valeur;
    }
    
}
