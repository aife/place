package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.EpmTObject;

import java.util.Collection;
import java.util.List;

/**
 * Interface de GIM avec accès à la base de données.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface BaseGIM {

    /**
     * @param critere critère de recherche
     * @return liste des objets correspondants
     */
    <T> List<T> chercherParCritere(Critere critere);

    /**
     * @param valeur sauveagarde en base un objet implement l'interface.
     */
    <T extends EpmTObject> T modifier(T valeur);

    <T extends EpmTObject> void creer(T valeur);

    /**
     * Modification d'une liste d'objet
     * @param valeur liste d'objet implementant l'interface {@link T} à modifier.
     */
    <T extends EpmTObject> Collection<T> modifier(Collection<T> valeur);

    /**
     * Suppression d'un objet
     * @param valeur objet implement l'interface à supprimer.
     */
    <T extends EpmTObject> void supprimer(T valeur);

    /**
     * Suppression d'une liste d'objet
     * @param valeur liste d'objet implementant l'interface à supprimer.
     */
    <T extends EpmTObject> void supprimer(List<T> valeur);

    /**
     * Retourne un objet persistant s'il est unique, retourne null si absent, une exception
     * sinon.
     */
    <T extends EpmTObject> T chercherUniqueParCritere(final Critere critere);

    <T extends EpmTObject> T chercherObject(int id, Class<T> clazz);

}
