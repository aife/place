package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Pojo hibernate de EpmTPageDeGarde.
 * @author MGA
 */
@Entity
@Table(name = "epm__t_page_de_garde", schema = "redaction")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTPageDeGarde extends EpmTAbstractObject implements Serializable, Cloneable {

    /**
     * 
     */
    private static final long serialVersionUID = -4926822635999955752L;
    /**
     * Cet attribut mappe la valeur de la clé primaire.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_page_garde")
    private int id;
    
    /**
     * attribut auteur.
     */
    private String auteur;

    @Column(name = "id_utilisateur")
    private Integer idUtilisateur;

    /**
     * Le referentiel de type de page de garde.
     */
    @ManyToOne
    @JoinColumn(name="id_type_page_garde", nullable=false)
    private EpmTRefTypePageDeGarde epmTRefTypePageDeGarde;
    
    /**
     * Identifiant de la consultation
     */
    @Column(name="id_consultation")
    private int idConsultation;
    
    /**
     * Nom du fichier à la génération.
     */
    @Column(name="nom_fichier")
    private String nomFichier;
    
    /**
     * Titre du document
     */
    private String titre;
    
    /**
     * Date de création
     */
    @Column(name="date_creation", length=13)
    private Date dateCreation;

    /**
     * Date de modification
     */
    @Column(name="date_modification", length=13)
    private Date dateModification;

	/**
	 * Extension
	 */
	@Column(name="extension", length = 255)
	private String extension;

	/**
	 * Validation
	 */
	@Column(name="validation")
	private boolean validation;

	/**
	 * Validateur
	 */
	@Column(name="validateur")
	private String validateur;

	/**
	 * Date de validation
	 */
	@Column(name="date_validation")
	private Date dateValidation;


	/**
	 * Liste des versions
	 */
	@OneToMany(
		targetEntity = EpmTPageDeGardeVersion.class,
		mappedBy = "pageDeGarde",
		fetch = FetchType.EAGER,
		cascade = CascadeType.ALL
	)
	@OrderBy("dateCreation desc")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<EpmTPageDeGardeVersion> versions;

    public final int getId() {
        return id;
    }

    public void setId(final int valeur) {
        this.id = valeur;
    }

    public final String getAuteur() {
        return auteur;
    }

    public final void setAuteur(final String valeur) {
        this.auteur = valeur;
    }

    public EpmTRefTypePageDeGarde getEpmTRefTypePageDeGarde() {
        return epmTRefTypePageDeGarde;
    }

    public void setEpmTRefTypePageDeGarde(final EpmTRefTypePageDeGarde valeur) {
        this.epmTRefTypePageDeGarde = valeur;
    }

    public int getIdConsultation() {
        return idConsultation;
    }

    public void setIdConsultation(final int valeur) {
        this.idConsultation = valeur;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(final String valeur) {
        this.nomFichier = valeur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String valeur) {
        this.titre = valeur;
    }

    public final Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date valeur) {
        this.dateCreation = valeur;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(final Date valeur) {
        this.dateModification = valeur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

	public List<EpmTPageDeGardeVersion> getVersions() {
		return versions;
	}

	public boolean possedeVersions() {
		return null!=versions && !versions.isEmpty();
	}

	public void setVersions( List<EpmTPageDeGardeVersion> versions ) {
		this.versions = versions;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension( String extension ) {
		this.extension = extension;
	}

	public boolean isValidation() {
		return validation;
	}

	public void setValidation( boolean validation ) {
		this.validation = validation;
	}

	public String getValidateur() {
		return validateur;
	}

	public void setValidateur( String validateur ) {
		this.validateur = validateur;
	}

	public Date getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation( Date dateValidation ) {
		this.dateValidation = dateValidation;
	}

	public Optional<EpmTPageDeGardeVersion> recupererDerniereVersion() {
    	return this.getVersions().stream()
		    .filter(version -> null!=version.getFichier())
		    .max(Comparator.comparingInt(EpmTPageDeGardeVersion::getId));
	}

	public Optional<EpmTPageDeGardeVersion> recupererVersionCourante() {
		return this.getVersions().stream()
			.filter(version -> null==version.getFichier())
			.max(Comparator.comparingInt(EpmTPageDeGardeVersion::getId));
	}

	public Optional<EpmTPageDeGardeVersion> recupererVersion(String jeton, String statut) {
		return this.getVersions().stream()
			.filter(version -> null != version.getJeton() && version.getJeton().equalsIgnoreCase(jeton) && null!=version.getStatut() && version.getStatut().equalsIgnoreCase(statut))
			.max(Comparator.comparingInt(EpmTPageDeGardeVersion::getId));
	}

	public void ajouterVersion(final EpmTPageDeGardeVersion version) {
    	Date maintenant = new Date();

    	version.setDateCreation(maintenant);
    	version.setPageDeGarde(this);

    	if (null==this.versions) {
    		this.versions = new ArrayList<>();
	    }

    	this.versions.add(version);
	}

	/* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    public EpmTPageDeGarde clone() throws CloneNotSupportedException {
        EpmTPageDeGarde epmTPageDeGarde = new EpmTPageDeGarde();
        epmTPageDeGarde.setEpmTRefTypePageDeGarde(epmTRefTypePageDeGarde);
        epmTPageDeGarde.setIdConsultation(idConsultation);
        epmTPageDeGarde.setNomFichier(nomFichier);
        epmTPageDeGarde.setTitre(titre);
        epmTPageDeGarde.setDateCreation(new Date());
        epmTPageDeGarde.setDateModification(new Date());
        epmTPageDeGarde.setAuteur(auteur);
        epmTPageDeGarde.setIdUtilisateur(idUtilisateur);
        return epmTPageDeGarde;
    }
}
