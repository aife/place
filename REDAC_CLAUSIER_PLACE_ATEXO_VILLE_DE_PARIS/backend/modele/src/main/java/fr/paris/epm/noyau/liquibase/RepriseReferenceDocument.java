package fr.paris.epm.noyau.liquibase;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class RepriseReferenceDocument implements CustomTaskChange {

    private static final Logger logger = LoggerFactory.getLogger(RepriseReferenceDocument.class);

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {

    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }


    @Override
    public void execute(Database database) throws CustomChangeException {
        try {
            JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();
            Statement smt = databaseConnection.createStatement();
            ResultSet result = smt.executeQuery("SELECT id, reference, xml_id, xml_ref FROM redaction.epm__t_document");

            Map<String, String> correspondences = new HashMap<>();
            Set<String> referencesOK = new HashSet<>();
            Set<String> referencesNOK = new HashSet<>();
            Set<String> referencesNodes = new HashSet<>();
            Set<String> referencesLeafs = new HashSet<>();
            Map<String, List<String>> correspondencesResult = new HashMap<>();

            while (result.next()) {
                Integer idDocument = result.getInt(1);
                String reference = result.getString(2);
                Integer idDocumentXml = result.getInt(3);
                String referenceXml = result.getString(4);

                if (!correspondences.containsKey(reference)) {
                    if (referenceXml != null && !referenceXml.trim().isEmpty() && !referenceXml.equals(reference)) {
                        correspondences.put(reference, referenceXml);
                        referencesNOK.add(reference);
                    } else {
                        correspondences.put(reference, "");
                        referencesOK.add(reference);
                    }
                } else {
                    String ref = correspondences.get(reference);
                    if (referenceXml != null && !referenceXml.isEmpty() && !referenceXml.equals(reference)) {
                        if (ref.isEmpty()) {
                            correspondences.put(reference, referenceXml);
                            referencesOK.remove(reference);
                            referencesNOK.add(reference);
                        } else {
                            logger.error("Valeurs imprevues : reference {} point vers deux parents {} et {}", reference, referenceXml, ref);
                            throw new CustomChangeException("Valeurs imprevues : reference " + reference + " point vers deux parents " + referenceXml + " et " + ref);
                        }
                    }

                }
                referencesLeafs.add(reference);
                referencesNodes.add(referenceXml);
            }
            referencesLeafs.removeAll(referencesNodes);
            referencesLeafs.removeAll(referencesOK);

            for (String reference : referencesLeafs) {
                List<String> refs = new ArrayList<>();
                refs.add(reference);

                String referenceParent = correspondences.get(reference);
                while (referencesNOK.contains(referenceParent)) {
                    refs.add(referenceParent);
                    referenceParent = correspondences.get(referenceParent);
                }
                correspondencesResult.put(referenceParent, refs);
            }

            for (String referenceParent : correspondencesResult.keySet()) {
                for (String reference : correspondencesResult.get(referenceParent)) {
                    String query = "UPDATE redaction.epm__t_document SET reference = '" + referenceParent + "', " +
                            "xml_data = REPLACE(xml_data, xml_ref, '" + referenceParent + "') " +
                            "WHERE reference = '" + reference + "'";
                    smt.execute(query);
                    logger.info("QUERY : \"" + query + "\" -> OK");
                }
            }
        } catch (DatabaseException | SQLException ex) {
            logger.error("RepriseCanevasChapitreXML : " + ex.getMessage());
            throw new CustomChangeException(" Probleme d'execution du liquibase 'RED-35'", ex);
        }
    }

}
