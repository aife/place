package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.Map;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * consultation.caracteristiques.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCaracteristiques implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private String intituleConsultation;

    private String articleCmp;

    private String directionServiceResponsable;

    private String responsableConsultation;

    private String objetConsultation;

    private String typeProcedure;

    private String pouvoirAdjudicateur;
    
    private String directionServiceBeneficiaire;

    private String adresseDSResponsable;

    private String codePostalDSResponsable;

    private String villeDSResponsable;

    private String courrielDSResponsable;

    private String naturePrestation;

    private Map<String, String> codeLibelleLieuxExecution;

    private String libelleEtape;
    private String typeMarche;
    private String attributFormalise;

    public final String getIntituleConsultation() {
        return intituleConsultation;
    }

    public final void setIntituleConsultation(final String valeur) {
        this.intituleConsultation = valeur;
    }

    public final String getArticleCmp() {
        return articleCmp;
    }

    public final void setArticleCmp(final String articleCmp) {
        this.articleCmp = articleCmp;
    }

    public final String getDirectionServiceResponsable() {
        return directionServiceResponsable;
    }

    public final void setDirectionServiceResponsable(final String valeur) {
        this.directionServiceResponsable = valeur;
    }

    public final String getResponsableConsultation() {
        return responsableConsultation;
    }

    public final void setResponsableConsultation(final String valeur) {
        this.responsableConsultation = valeur;
    }

    public final String getObjetConsultation() {
        return objetConsultation;
    }

    public final void setObjetConsultation(final String valeur) {
        this.objetConsultation = valeur;
    }

    public final String getTypeProcedure() {
        return typeProcedure;
    }

    public final void setTypeProcedure(final String valeur) {
        this.typeProcedure = valeur;
    }

    public final String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    public final void setPouvoirAdjudicateur(final String valeur) {
        this.pouvoirAdjudicateur = valeur;
    }

    public String getDirectionServiceBeneficiaire() {
        return directionServiceBeneficiaire;
    }

    public void setDirectionServiceBeneficiaire(String directionServiceBeneficiaire) {
        this.directionServiceBeneficiaire = directionServiceBeneficiaire;
    }

    public final String getNaturePrestation() {
        return naturePrestation;
    }

    public final void setNaturePrestation(final String valeur) {
        this.naturePrestation = valeur;
    }

    public Map<String, String> getCodeLibelleLieuxExecution() {
        return codeLibelleLieuxExecution;
    }

    public void setCodeLibelleLieuxExecution(Map<String, String> codeLibelleLieuxExecution) {
        this.codeLibelleLieuxExecution = codeLibelleLieuxExecution;
    }

    public String getLieuxExecution() {
        StringBuilder sb = new StringBuilder();

        for (String code : codeLibelleLieuxExecution.keySet()) {
            sb.append(codeLibelleLieuxExecution.get(code))
                    .append(" (").append(code).append("),");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append(".");
        return sb.toString();
    }

    public String getAdresseDSResponsable() {
        return adresseDSResponsable;
    }

    public void setAdresseDSResponsable(String adresseDSResponsable) {
        this.adresseDSResponsable = adresseDSResponsable;
    }

    public String getCodePostalDSResponsable() {
        return codePostalDSResponsable;
    }

    public void setCodePostalDSResponsable(String codePostalDSResponsable) {
        this.codePostalDSResponsable = codePostalDSResponsable;
    }

    public String getVilleDSResponsable() {
        return villeDSResponsable;
    }

    public void setVilleDSResponsable(String villeDSResponsable) {
        this.villeDSResponsable = villeDSResponsable;
    }

    public String getCourrielDSResponsable() {
        return courrielDSResponsable;
    }

    public String getCourrielDirectionServiceResponsable() {
        return courrielDSResponsable;
    }

    public void setCourrielDSResponsable(String courrielDSResponsable) {
        this.courrielDSResponsable = courrielDSResponsable;
    }

   public String getAdresseDirectionServiceResponsable(){
      if(getCodePostalDSResponsable().isEmpty() && getVilleDSResponsable().isEmpty())
          return getAdresseDSResponsable();
        return  getAdresseDSResponsable()+"\n"+getCodePostalDSResponsable() +" - "+getVilleDSResponsable();
    }

    public String getLibelleEtape() {
        return libelleEtape;
    }

    public void setLibelleEtape(String libelleEtape) {
        this.libelleEtape = libelleEtape;
    }

    public String getTypeMarche() {
        return typeMarche;
    }

    public void setTypeMarche(String typeMarche) {
        this.typeMarche = typeMarche;
    }

    public String getAttributFormalise() {
        return attributFormalise;
    }

    public void setAttributFormalise(String attributFormalise) {
        this.attributFormalise = attributFormalise;
    }

}
