package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * entreprises.courrier.coordoneesEntrepriseDestinataire (le document concerne
 * une entreprise).
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCoordonneesEntreprise implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

    private String raisonSociale;

    private String adresse;

    private String codePostal;

    private String ville;

    public final String getRaisonSociale() {
        return raisonSociale;
    }

    public final void setRaisonSociale(final String valeur) {
        this.raisonSociale = valeur;
    }

    public final String getAdresse() {
        return adresse;
    }

    public final void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    public final String getCodePostal() {
        return codePostal;
    }

    public final void setCodePostal(final String valeur) {
        this.codePostal = valeur;
    }

    public final String getVille() {
        return ville;
    }

    public final void setVille(final String valeur) {
        this.ville = valeur;
    }
}
