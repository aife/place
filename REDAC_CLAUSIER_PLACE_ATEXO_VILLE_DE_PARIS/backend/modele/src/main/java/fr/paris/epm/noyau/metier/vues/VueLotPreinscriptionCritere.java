package fr.paris.epm.noyau.metier.vues;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Classe representant les criteres de recherche de la vue SQL du detail de la preinscription du module commission
 * 
 * @author Regis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class VueLotPreinscriptionCritere extends AbstractCritere implements
        Critere, Serializable {

    /**
     * Le LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(VueLotPreinscriptionCritere.class);

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = -3921384059116029626L;

    /**
     * id lot dissocie
     */
    private Integer idLotDissocie;

    /**
     * id de la consultation si consultation non alloti
     */
    private Integer idConsultation;

    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        
        sb.append("from EpmVLotPreinscription c ");
        if (idConsultation != null) {
        	if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" c.idConsultation = "); 
            sb.append(idConsultation);
            debut = true;
        }
        
        if (idLotDissocie != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            if (idLotDissocie != 0) {
                sb.append(" c.idLot = "); 
                sb.append(idLotDissocie);
            } else {
                sb.append(" c.lotDissocie = false ");
            }
        }
        return sb;
    }

    /**
     * @return transforme les critères en chaine HQL.
     */
    public final String toHQL() {
        StringBuffer requeteHQL = corpsRequete();
        if (proprieteTriee != null) {
            requeteHQL.append(" order by c."); 
            requeteHQL.append(proprieteTriee);
            if (triCroissant) {
                requeteHQL.append(" ASC ");
            } else {
                requeteHQL.append(" DESC ");
            }
        }
        return "Select distinct c " + requeteHQL.toString();
    }

    /**
     * Pour compter le nombre de résultat.
     */
    public String toCountHQL() {
        StringBuffer select = new StringBuffer("select count(distinct c) ");
        select.append(corpsRequete());
        LOG.debug("countHQL : " + select.toString());
        return select.toString();
    }

    /**
     * 
     * @return id lot dissocie
     */
    public final Integer getIdLotDissocie() {
        return idLotDissocie;
    }

    /**
     * 
     * @param valeur id lot dissocie
     */
    public final void setIdLotDissocie(final Integer valeur) {
        this.idLotDissocie = valeur;
    }

    /**
     * 
     * @return id de la consultation (consultation non alllotie).
     */
    public final Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * 
     * @param valeur id de la consultation (consultation non allotie).
     */
    public final void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }
}
