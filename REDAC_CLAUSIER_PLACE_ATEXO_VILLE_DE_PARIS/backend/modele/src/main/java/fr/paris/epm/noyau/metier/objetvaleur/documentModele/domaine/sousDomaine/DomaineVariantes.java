package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

public class DomaineVariantes {

	/**
	 * true si cas d'une variante autorisée
	 */
	private boolean autorisee;
	
	/**
	 * true si cas d'une variante obligatoire
	 */
	private boolean obligatoire;
	
	/**
	 * La description de la variante obligatoire
	 */
	private String description;

	/**
	 * 
	 * @return true si cas d'une variante autorisée
	 */
	public final boolean isAutorisee() {
		return autorisee;
	}

	/**
	 * 
	 * @param autorisee true si cas d'une variante autorisée
	 */
	public final void setAutorisee(final boolean autorisee) {
		this.autorisee = autorisee;
	}

	/**
	 * 
	 * @return true si cas d'une variante obligatoire
	 */
	public final boolean isObligatoire() {
		return obligatoire;
	}

	/**
	 * 
	 * @param obligatoire true si cas d'une variante obligatoire
	 */
	public final void setObligatoire(final boolean obligatoire) {
		this.obligatoire = obligatoire;
	}

	/**
	 * 
	 * @return la description de la variante obligatoire
	 */
	public final String getDescription() {
		return description;
	}

	/**
	 * 
	 * @param description la description de la variante obligatoire
	 */
	public final void setDescription(final String description) {
		this.description = description;
	}
	
}
