package fr.paris.epm.noyau.dto;

import java.io.Serializable;
import java.util.List;

public class SignaturesBean implements Serializable {

    List<SignatureBean> signatures;

    String typeSignature;

    Integer etat;

    public List<SignatureBean> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<SignatureBean> signatures) {
        this.signatures = signatures;
    }

    public String getTypeSignature() {
        return typeSignature;
    }

    public void setTypeSignature(String typeSignature) {
        this.typeSignature = typeSignature;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }
}
