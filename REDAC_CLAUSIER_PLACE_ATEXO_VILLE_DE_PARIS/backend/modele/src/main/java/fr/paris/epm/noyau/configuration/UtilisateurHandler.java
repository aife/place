package fr.paris.epm.noyau.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation portée par les arguments de méthode pour récupérer un utilisateur en session (à injecter par Spring)
 * cf. UtilisateurResolver
 * Created by sta on 18/02/16.
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface UtilisateurHandler {

}
