package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;

import javax.persistence.*;
import java.util.Objects;

/**
 * Classe abstraite pour le mapping des référentiels du module rédaction avec
 * les annotations.
 * @author lba
 */
@MappedSuperclass
public class BaseEpmTRefRedaction implements EpmTRef, Comparable<EpmTRef> {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "libelle", nullable = false)
    private String libelle;

	@Column(name = "libelle_court", nullable = false)
	private String libelleCourt;

	@Column(name = "code_externe", nullable = false)
	private String codeExterne;

    private boolean actif;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer valeur) {
        this.id = valeur;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

	@Override
	public String getLibelleCourt() {
		return libelleCourt;
	}

	@Override
	public void setLibelleCourt( String libelleCourt ) {
		this.libelleCourt = libelleCourt;
	}

	@Override
	public String getCodeExterne() {
		return codeExterne;
	}

	@Override
	public void setCodeExterne( String codeExterne ) {
		this.codeExterne = codeExterne;
	}

	/**
     * comparer des objets.
     */
    @Override
    public int compareTo(final EpmTRef referentiel) {
        final int idTous = 1;

        if ( Objects.equals(this.id, referentiel.getId()) || referentiel.getId() <= idTous)
            return 1;

        if (id <= idTous)
            return -1;

        return libelle.compareTo(referentiel.getLibelle());
    }

    /**
     * compare L’égalité des objets .
     */
    @Override
    @Deprecated
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        if (!(obj instanceof BaseEpmTRefRedaction))
            return false;

        final BaseEpmTRefRedaction autre = (BaseEpmTRefRedaction) obj;
        return this.id == autre.id;
    }

}
