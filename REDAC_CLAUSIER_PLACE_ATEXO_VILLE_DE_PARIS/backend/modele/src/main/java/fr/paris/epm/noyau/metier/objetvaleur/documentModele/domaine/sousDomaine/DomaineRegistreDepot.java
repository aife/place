package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les donnees a fusionner dans un document pour le sous domaine entreprises.registreDepot.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreDepot implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Nombre total de plis.
	 */
	private Integer nbTotalDepots;
	
	private Integer nbDepotsDemat;
	
	private Integer nbDepotsPapier;
	/**
	 * Nombre total de plis Phase 2 (procedure restreinte).
	 */
	private Integer nbTotalDepotsPhaseRestreinte;
    /**
     * Dans le Registre des depots en phase 1 - Nombre incremente si la date de
     * depot des elements physiques est renseignee.
     */
	private Integer nbTotalEchantillons;
	/**
	 * Liste de tous les depots en phase 1.
	 */
	private List<String> listeCandidats;
    /**
     * Dans le Registre des depots en phase 2 - Nombre incremente si la date de
     * depot des elements physiques est renseignee.
     */
	private Integer nbTotalEchantillonsPhaseRestreinte;
	
	/**
	 * @return Nombre total de plis.
	 */
	public final Integer getNbTotalDepots() {
		return nbTotalDepots;
	}
	/**
	 * @param valeur Nombre total de plis.
	 */
	public final void setNbTotalDepots(final Integer valeur) {
		this.nbTotalDepots = valeur;
	}
	
	public final Integer getNbDepotsDemat() {
		return nbDepotsDemat;
	}
	
	public final void setNbDepotsDemat(final Integer valeur) {
		this.nbDepotsDemat = valeur;
	}
	
	public final Integer getNbDepotsPapier() {
		return nbDepotsPapier;
	}
	
	public final void setNbDepotsPapier(final Integer valeur) {
		this.nbDepotsPapier = valeur;
	}
	/**
	 * @return Nombre total de plis Phase 2 (procedure restreinte).
	 */
	public final Integer getNbTotalDepotsPhaseRestreinte() {
		return nbTotalDepotsPhaseRestreinte;
	}
	/**
	 * @param valeur Nombre total de plis Phase 2 (procedure restreinte).
	 */
	public final void setNbTotalDepotsPhaseRestreinte(final
			Integer valeur) {
		this.nbTotalDepotsPhaseRestreinte = valeur;
	}
	/**
	 * @return {@link DomaineRegistreDepot#nbTotalEchantillons}
	 */
    public final Integer getNbTotalEchantillons() {
        return nbTotalEchantillons;
    }
    /**
     * @param {@link DomaineRegistreDepot#nbTotalEchantillons}
     */
    public final void setNbTotalEchantillons(final Integer valeur) {
        this.nbTotalEchantillons = valeur;
    }
    /**
     * @return Liste de tous les candidats (Registre des depots, phase 1).
     */
    public final List<String> getListeCandidats() {
        return listeCandidats;
    }
    /**
     * @param valeur Liste de tous les candidats (Registre des depots, phase 1).
     */
    public final void setListeCandidats(final List<String> valeur) {
        this.listeCandidats = valeur;
    }
    /**
     * @return {@link DomaineRegistreDepot#nbTotalEchantillonsPhaseRestreinte}
     */
    public final Integer getNbTotalEchantillonsPhaseRestreinte() {
        return this.nbTotalEchantillonsPhaseRestreinte;
    }
    /**
     * @param valeur {@link DomaineRegistreDepot#nbTotalEchantillonsPhaseRestreinte}
     */
    public final void setNbTotalEchantillonsPhaseRestreinte(final Integer valeur) {
        this.nbTotalEchantillonsPhaseRestreinte = valeur;
    }
}
