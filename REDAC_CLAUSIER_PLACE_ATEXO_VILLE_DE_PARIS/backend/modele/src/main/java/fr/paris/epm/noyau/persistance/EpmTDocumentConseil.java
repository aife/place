package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;


/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTDocumentConseil extends EpmTAbstractDocument implements
        Comparable<EpmTDocumentConseil> {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Marqueur unique pour ce fichier, utilisé dans hachage et equals.
     */
    private String reference;

    /**
     * La référence est unique et seule utilisée.
     * @return code de hachage
     */
    public final int hashCode() {
        int res = 1;
        if (reference != null) {
            res = Constantes.PREMIER * res + reference.hashCode();
        }
        return res;
    }

    /**
     * La référence est unique et seule utilisée.
     * @param obj comparé
     * @return résultat de la comparaison
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTDocumentConseil autre = (EpmTDocumentConseil) obj;
        if (reference == null) {
            if (autre.reference != null) {
                return false;
            }
        } else if (!reference.equals(autre.reference)) {
            return false;
        }
        return true;
    }

    /**
     * Retourne 0 si les 2 id des 2 objets du type EpmTDocumentConseil sont
     * égaux. Retourne -1 si l'id courant est inférieur à celui de arg0;
     * Retourne +1 dans le cas inverse
     * @param obj objet à comparer
     * @return un entier exprimant le résultat exprimé plus haut
     */
    public final int compareTo(final EpmTDocumentConseil obj) {
        if (this.getId() > obj.getId()) {
            return 1;
        } else if (this.getId() < obj.getId()) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * @return référence
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param valeur référence
     */
    public void setReference(final String valeur) {
        this.reference = valeur;
    }

}
