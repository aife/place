package fr.paris.epm.noyau.metier.objetvaleur;

import java.util.List;

/**
 * Objet-valeur pour le nombre d'étapes mises à jour lors du passage des dates
 * ou réunions de prévu à réel.
 * @author Guillaume BÉRAUDO
 */
public class EtapesMAJ {

    /**
     * Nombre d'étapes de commission mises à jour.
     */
    private int nbCommissions;

    /**
     * Nombre d'étapes normales mises à jour.
     */
    private int nbNormales;
    
    /**
     * Liste des indentifiants des étapes commissions mises à jour. 
     */
    private List<Integer> listeCommission;
    
    /**
     * Liste des indentifiants des etapes simples mises à jour.
     */
    private List<Integer> listeEtapeSimple;


    /**
     *
     */
    public EtapesMAJ() {
    }

    /**
     * @param nbComm nombre d'étapes de commission mises à jour
     * @param nbNorm nombre d'étapes normales mises à jour
     */
    public EtapesMAJ(final List<Integer> nbComm, final List<Integer> nbNorm) {
        nbCommissions = nbComm.size();
        nbNormales = nbNorm.size();
        listeCommission = nbComm;
        listeEtapeSimple = nbNorm;
    }


    /**
     * @return nombre d'étapes de commission mises à jour
     */
    public final int getNbCommissions() {
        return nbCommissions;
    }

    /**
     * @param valeur nombre d'étapes de commission mises à jour
     */
    public final void setNbCommissions(final int valeur) {
        this.nbCommissions = valeur;
    }

    /**
     * @return nombre d'étapes normales mises à jour
     */
    public final int getNbNormales() {
        return nbNormales;
    }

    /**
     * @param valeur nombre d'étapes normales mises à jour
     */
    public final void setNbNormales(final int valeur) {
        this.nbNormales = valeur;
    }

	public final List<Integer> getListeCommission() {
		return listeCommission;
	}


	public final List<Integer> getListeEtapeSimple() {
		return listeEtapeSimple;
	}

}
