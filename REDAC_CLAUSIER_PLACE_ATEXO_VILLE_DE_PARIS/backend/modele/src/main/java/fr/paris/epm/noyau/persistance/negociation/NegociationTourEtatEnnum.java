package fr.paris.epm.noyau.persistance.negociation;

/**
 * Created by ayr on 24/05/17.
 */
public enum NegociationTourEtatEnnum {

    CLOTURE, EN_COURS;
}
