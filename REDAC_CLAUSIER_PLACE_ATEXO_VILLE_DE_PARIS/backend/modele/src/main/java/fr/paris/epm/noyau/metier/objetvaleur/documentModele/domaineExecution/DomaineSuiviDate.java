package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;
import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le domaine Suivi
 * administratif / Suivi des dates
 * 
 * @author GAO Xuesong
 */
public class DomaineSuiviDate implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Date d’une étape du calendrier
	 */
	private Date dateCalendrier; 
	
	/**
	 * Etape du calendrier
	 */
	private String etapeDateCalendrier;
	
	/**
	 * Délai de préavis d’une date / étape du calendrier
	 */
	private Integer preavisDateCalendirer;
	
	/**
	 * Destinataire de l’alerte pour une date / étape du calendrier
	 */
	private String destinataireDateCalendrier;

	
	public final Date getDateCalendrier() {
		return dateCalendrier;
	}

	public final void setDateCalendrier(final Date valeur) {
		this.dateCalendrier = valeur;
	}

	public final String getEtapeDateCalendrier() {
		return etapeDateCalendrier;
	}

	public final void setEtapeDateCalendrier(final String valeur) {
		this.etapeDateCalendrier = valeur;
	}

	public final Integer getPreavisDateCalendirer() {
		return preavisDateCalendirer;
	}

	public final void setPreavisDateCalendirer(final Integer valeur) {
		this.preavisDateCalendirer = valeur;
	}

	public final String getDestinataireDateCalendrier() {
		return destinataireDateCalendrier;
	}

	public final void setDestinataireDateCalendrier(final String valeur) {
		this.destinataireDateCalendrier = valeur;
	}
	
	
}
