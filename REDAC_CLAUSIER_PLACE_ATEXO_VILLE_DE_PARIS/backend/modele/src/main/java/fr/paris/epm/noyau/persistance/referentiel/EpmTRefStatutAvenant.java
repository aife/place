package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefStatutAvenant de la table "epm__t_ref_statut_avenant"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefStatutAvenant extends EpmTRefStatut {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

    public static final int DEFINITION = 1; // Etape de définition.
    public static final int PUBLICITE = 2; // Etape de publicité.
    public static final int EXECUTION = 6; // Consultation en exécution.

}