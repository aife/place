package fr.paris.epm.noyau.persistance.referentiel;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;

/**
 * Interface EpmTReferentielOrdred est la racine d'héritation pour toutes les référentiels ordonées.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface EpmTReferentielOrdred extends EpmTRef {

    /**
     * @return ordre de la ligne
     */
    abstract public int getOrdre();

    /**
     * @param ordre ordre de la ligne
     */
    abstract public void setOrdre(final int ordre);

}
