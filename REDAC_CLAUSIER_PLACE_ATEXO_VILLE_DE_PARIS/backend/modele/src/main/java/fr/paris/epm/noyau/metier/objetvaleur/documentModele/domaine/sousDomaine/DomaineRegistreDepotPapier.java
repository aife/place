package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.util.Date;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * entreprises.registreDepot des depots papier.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineRegistreDepotPapier extends AbstractDomaineRegistreDepot {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * Entreprise ayant fait le depot.
     */
    private String entrepriseDepotPapier;

    private String sirenDepotPapier;

    private String numeroDepotPapier;

    private String nomContactDepotPapier;

    private String prenomContactDepotPapier;

    private String adresseDepotPapier;

    private String codePostalDepotPapier;

    private String villeDepotPapier;

    private String mailDepotPapier;

    private String telephoneDepotPapier;

    private String faxDepotPapier;

    private Date dateDepotPapier;

    private String modalitesDepotPapier;
    /**
     * Date et heure du depot de pli des elements physiques.
     */
    private Date dateDepot;
    /**
     * Modalite de depot depot de pli des elements physiques.
     */
    private String modalitesDepotElemPhyPapier;

    /**
     * @return Entreprise ayant fait le depot.
     */
    public final String getEntrepriseDepotPapier() {
        return entrepriseDepotPapier;
    }

    /**
     * @param valeur Entreprise ayant fait le depot.
     */
    public final void setEntrepriseDepotPapier(String valeur) {
        this.entrepriseDepotPapier = valeur;
    }

    public final String getSirenDepotPapier() {
        return sirenDepotPapier;
    }

    public final void setSirenDepotPapier(String valeur) {
        this.sirenDepotPapier = valeur;
    }

    public final String getNumeroDepotPapier() {
        return numeroDepotPapier;
    }

    public final void setNumeroDepotPapier(String valeur) {
        this.numeroDepotPapier = valeur;
    }

    public final String getNomContactDepotPapier() {
        return nomContactDepotPapier;
    }

    public final void setNomContactDepotPapier(String valeur) {
        this.nomContactDepotPapier = valeur;
    }

    public final String getPrenomContactDepotPapier() {
        return prenomContactDepotPapier;
    }

    public final void setPrenomContactDepotPapier(String valeur) {
        this.prenomContactDepotPapier = valeur;
    }

    public final String getAdresseDepotPapier() {
        return adresseDepotPapier;
    }

    public final void setAdresseDepotPapier(String valeur) {
        this.adresseDepotPapier = valeur;
    }

    public final String getCodePostalDepotPapier() {
        return codePostalDepotPapier;
    }

    public final void setCodePostalDepotPapier(String valeur) {
        this.codePostalDepotPapier = valeur;
    }

    public final String getVilleDepotPapier() {
        return villeDepotPapier;
    }

    public final void setVilleDepotPapier(String valeur) {
        this.villeDepotPapier = valeur;
    }

    public final String getMailDepotPapier() {
        return mailDepotPapier;
    }

    public final void setMailDepotPapier(String valeur) {
        this.mailDepotPapier = valeur;
    }

    public final String getTelephoneDepotPapier() {
        return telephoneDepotPapier;
    }

    public final void setTelephoneDepotPapier(String valeur) {
        this.telephoneDepotPapier = valeur;
    }

    public final String getFaxDepotPapier() {
        return faxDepotPapier;
    }

    public final void setFaxDepotPapier(String valeur) {
        this.faxDepotPapier = valeur;
    }

    public final Date getDateDepotPapier() {
        return dateDepotPapier;
    }

    public final void setDateDepotPapier(Date valeur) {
        this.dateDepotPapier = valeur;
    }

    public final String getModalitesDepotPapier() {
        return modalitesDepotPapier;
    }

    public final void setModalitesDepotPapier(String valeur) {
        this.modalitesDepotPapier = valeur;
    }

    /**
     * @return Date et heure du depot de pli des elements physiques.
     */
    public final Date getDateDepot() {
        return dateDepot;
    }

    /**
     * @param valeur Date et heure du depot de pli des elements physiques.
     */
    public final void setDateDepot(Date valeur) {
        this.dateDepot = valeur;
    }

    /**
     * @return Modalite de depot depot de pli des elements physiques.
     */
    public final String getModalitesDepotElemPhyPapier() {
        return modalitesDepotElemPhyPapier;
    }

    /**
     * @param valeur Modalite de depot depot de pli des elements physiques.
     */
    public final void setModalitesDepotElemPhyPapier(String valeur) {
        this.modalitesDepotElemPhyPapier = valeur;
    }
}
