package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;


public class InfoBulle implements Serializable {

    private static final long serialVersionUID = 1L;

    private String description;
    private Lien lien;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Lien getLien() {
        return lien;
    }

    public void setLien(Lien lien) {
        this.lien = lien;
    }
}
