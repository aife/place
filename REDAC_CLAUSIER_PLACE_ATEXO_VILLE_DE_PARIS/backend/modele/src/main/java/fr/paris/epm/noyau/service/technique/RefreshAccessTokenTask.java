package fr.paris.epm.noyau.service.technique;

import fr.paris.epm.noyau.service.technique.exceptions.AccessTokenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

public class RefreshAccessTokenTask extends TimerTask {
	private final Logger LOG = LoggerFactory.getLogger(RefreshAccessTokenTask.class);

	private final OauthAccessTokenHolder oauthAccessTokenHolder;

	public RefreshAccessTokenTask( OauthAccessTokenHolder oauthAccessTokenHolder ) {
		this.oauthAccessTokenHolder = oauthAccessTokenHolder;
	}

	@Override
	public void run() {
		LOG.debug("Lancement de la tâche de renouvellement du jeton d'authentification");

		try {
			if (!oauthAccessTokenHolder.check()) {
				oauthAccessTokenHolder.renew();
			}
		} catch ( AccessTokenException ato ) {
			LOG.error("Erreur lors de l'obtention du jeton dans la tache programmée", ato);
		}
	}
}
