package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaineExecution;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour les domaines « Poste technique »
 * définis dans le domaine Informations principales du contrat / Tranche.
 * 
 * @author GAO Xuesong
 */
public class DomainePosteTechnique implements Serializable {
    
	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
    /**
     * Identifiant du poste technique
     */
    private String identifiantPosteTechnique;
    
    /**
     * Le poste technique est-il le lot principal ou non
     */
    private String posteTechniquePrincipal;
    
    /**
     * Intitulé du poste technique
     */
    private String intitulePosteTechnique;
    
    
	public final String getIdentifiantPosteTechnique() {
		return identifiantPosteTechnique;
	}

	public final void setIdentifiantPosteTechnique(final String valeur) {
		this.identifiantPosteTechnique = valeur;
	}

	public final String getPosteTechniquePrincipal() {
		return posteTechniquePrincipal;
	}

	public final void setPosteTechniquePrincipal(final String valeur) {
		this.posteTechniquePrincipal = valeur;
	}

	public final String getIntitulePosteTechnique() {
		return intitulePosteTechnique;
	}

	public final void setIntitulePosteTechnique(final String valeur) {
		this.intitulePosteTechnique = valeur;
	}

} 
