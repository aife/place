package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefFichePratique de la table "epm__t_ref_fiche_pratique"
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefFichePratique extends EpmTReferentielSimpleAbstract {

    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * contenu du message.
     */
    private String url;

    /**
     * ecran associé à la fiche pratique.
     */
    private String ecran;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEcran() {
        return ecran;
    }

    public void setEcran(String ecran) {
        this.ecran = ecran;
    }

}