package fr.paris.epm.noyau.persistance.redaction;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Pojo hibernate du canevas_pub
 */
@Entity
@Table(name = "epm__t_consultation_contexte", schema = "redaction", uniqueConstraints = @UniqueConstraint(columnNames = {"PLATEFORME", "REFERENCE"}))
public class EpmTConsultationContexte implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Integer id;

    @Column(name = "PLATEFORME")
    private String plateforme;

    @Column(name = "REFERENCE")
    private String reference;

    @Lob
    @Column(name = "CONTEXTE", nullable = false)
    private String contexte;

    @Lob
    @Column(name = "CONSULTATION_JSON", nullable = false)
    private String consultationJson;


    @Column(name = "ID_UTILISATEUR", nullable = false)
    private Integer idUtilisateur;

    @Lob
    @Column(name = "XML_MPE", nullable = false)
    private String xmlMpe;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlateforme() {
        return plateforme;
    }

    public void setPlateforme(String plateforme) {
        this.plateforme = plateforme;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }


    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getConsultationJson() {
        return consultationJson;
    }

    public void setConsultationJson(String consultationJson) {
        this.consultationJson = consultationJson;
    }

    public String getXmlMpe() {
        return xmlMpe;
    }

    public void setXmlMpe(String xmlMpe) {
        this.xmlMpe = xmlMpe;
    }
}
