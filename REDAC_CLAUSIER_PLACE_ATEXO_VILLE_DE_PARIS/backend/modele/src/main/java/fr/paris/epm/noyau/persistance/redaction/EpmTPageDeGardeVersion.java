package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * Version d'une page de garde
 */
@Entity
@Table(name = "epm__t_page_de_garde_version", schema = "redaction")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EpmTPageDeGardeVersion extends EpmTAbstractObject {

	/**
	 * Id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_version")
	private int id;

    /**
     * Page de garde
     */
    @ManyToOne
    @JoinColumn(name="id_page_garde", nullable=false)
    private EpmTPageDeGarde pageDeGarde;

	/**
	 * Identifiant utilisateur
	 */
	@Column(name = "id_utilisateur")
    private Integer idUtilisateur;

	/**
	 * Utilisateur
	 */
	@Transient
	private String utilisateur;
    
    /**
     * Fichier (complet)
     */
    @Lob
    @Column(name="fichier")
    private String fichier;
    
    /**
     * Titre du document
     */
    @Column(name="titre")
    private String titre;
    
    /**
     * Date de création
     */
    @Column(name="date_creation", length=13)
    private Date dateCreation;

    /**
     * Date de modification
     */
    @Column(name="date_modification", length=13)
    private Date dateModification;

	/**
	 * Jeton
	 */
	@Lob
	@Column(name="jeton")
	private String jeton;

	/**
	 * Statut
	 */
	@Lob
	@Column(name="statut")
	private String statut;

	public int getId() {
		return id;
	}

	public void setId( int id ) {
		this.id = id;
	}

	public EpmTPageDeGarde getPageDeGarde() {
		return pageDeGarde;
	}

	public void setPageDeGarde( EpmTPageDeGarde pageDeGarde ) {
		this.pageDeGarde = pageDeGarde;
	}

	public Integer getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur( Integer idUtilisateur ) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getFichier() {
		return fichier;
	}

	public void setFichier( String fichier ) {
		this.fichier = fichier;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre( String titre ) {
		this.titre = titre;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation( Date dateCreation ) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification( Date dateModification ) {
		this.dateModification = dateModification;
	}

	public String getJeton() {
		return jeton;
	}

	public void setJeton( String jeton ) {
		this.jeton = jeton;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut( String etat ) {
		this.statut = etat;
	}

	public String getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur( String utilisateur ) {
		this.utilisateur = utilisateur;
	}
}
