package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Date;

/**
 * Pojo hibernate gérant la sauvegarde sur disque des fichiers.
 * @author Mounthei kham
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTFichierSurDisque extends EpmTAbstractObject {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * nom du document.
     */
    private String nomFichier;

    /**
     * chemin du document.
     */
    private String cheminFichier;


    /**
     * date de l'enregistrement.
     */
    private Date dateSauvegarde;
    
    /**
     * taille du fichier en byte
     */
    private long taille;


    /**
     * FIXME, attention, on pourrait avoir 2 noms de fichier identiques!
     * L'uri du fichier étant unique elle est utilisée pour le hashcode.
     * @see java.lang.Object#hashCode()
     * @return code de hacahge
     */
    public final int hashCode() {
        int result = 1;
        if (getNomFichier() != null) {
            result = Constantes.PREMIER * result + getNomFichier().hashCode();
        }
        return result;
    }

    /**
     * Sachant que le nom de fichier est généré à l'aide d'un timestamp, la
     * comparaison s'effectuera sur le nom du fichier.
     * @param obj l'autre objet à comparer.
     * @return "true" si les noms de fichiers sont identiques. <br/>"false"
     *         si les noms de fichiers sont différents.
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final EpmTFichierSurDisque autre = (EpmTFichierSurDisque) obj;

        return this.nomFichier.equals(autre.getNomFichier());
    }


    // Accesseurs
    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant de l'enregistrement
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return chemin du fichier
     */
    public String getCheminFichier() {
        return cheminFichier;
    }

    /**
     * @param valeur chemin du fichier
     */
    public void setCheminFichier(final String valeur) {
        this.cheminFichier = valeur;
    }

    /**
     * @return nom du fichier
     */
    public String getNomFichier() {
        return nomFichier;
    }

    /**
     * @param valeur nom du fichier
     */
    public void setNomFichier(final String valeur) {
        this.nomFichier = valeur;
    }

    /**
     * @return date de sauvegarde
     */
    public Date getDateSauvegarde() {
        return dateSauvegarde;
    }

    /**
     * @param valeur date de sauvegarde
     */
    public void setDateSauvegarde(final Date valeur) {
        this.dateSauvegarde = valeur;
    }

    /**
     * 
     * @return taille du fichier en byte
     */
    public long getTaille() {
        return taille;
    }

    /**
     * 
     * @param valeur taille du fichier en byte
     */
    public void setTaille(final long valeur) {
        this.taille = valeur;
    }
    
    
}
