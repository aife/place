package fr.paris.epm.noyau.metier;

import java.util.Map;

/**
 * Critere de recherche de l'objet @{EpmTOperationUniteFonctionnelle}
 * @author rebeca
 */
public class OperationUniteFonctionnelleCritere extends AbstractCritere {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer idPouvoirAdjudicateur;

    private Integer idDirectionServiceBeneficiaire;

    private Integer idDirectionServiceResponsable;

    private Integer idResponsable;

    private String code;

    private boolean codeStrict = false;

    private String libelle;

    private Integer idNature;

    private Double estimatifInitialHT;

    public OperationUniteFonctionnelleCritere() {
        // TODO Auto-generated constructor stub
    }
    
    public OperationUniteFonctionnelleCritere(OperationUniteFonctionnelleCritere critere) {
        this.id = critere.id;
        this.idPouvoirAdjudicateur = critere.idPouvoirAdjudicateur;
        this.idDirectionServiceBeneficiaire = critere.idDirectionServiceBeneficiaire;
        this.idDirectionServiceResponsable = critere.idDirectionServiceResponsable;
        this.idResponsable = critere.idResponsable;
        this.idNature = critere.idNature;
        this.code = critere.code;
        this.codeStrict = critere.codeStrict;
        this.libelle = critere.libelle;
        this.estimatifInitialHT = critere.estimatifInitialHT;
    }

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;
        Map<String, Object> parameters = getParametres();

        sb.append(" from EpmTOperationUniteFonctionnelle ouf");

        if (id != null) {
            debut = gererDebut(debut, sb);
            sb.append("ouf.id = :id");
            parameters.put("id", id);
            debut = true;
        }

        if (code != null && !code.trim().isEmpty()) {
            debut = gererDebut(debut, sb);

            if (codeStrict) {
                sb.append("lower(ouf.code) = lower(:code)");
                parameters.put("code", code.trim());
            } else {
                sb.append("lower(ouf.code) like lower(:code)");
                parameters.put("code", pourcentTrim(code));
            }
            debut = true;
        }

        if (libelle != null && !libelle.trim().isEmpty()) {
            debut = gererDebut(debut, sb);
            sb.append("lower(ouf.libelle) like lower(:libelle)");
            parameters.put("libelle", pourcentTrim(libelle));
            debut = true;
        }

        if (idPouvoirAdjudicateur != null) {
            debut = gererDebut(debut, sb);
            sb.append("ouf.epmTRefPouvoirAdjudicateur.id = :idPouvoirAdjudicateur");

            parameters.put("idPouvoirAdjudicateur", idPouvoirAdjudicateur);
            debut = true;
        }

        if (idDirectionServiceBeneficiaire != null) {
            debut = gererDebut(debut, sb);
            //sb.append("(ouf.epmTRefDirectionService IS NULL or ouf.epmTRefDirectionServiceBeneficiaire.id = :idDirectionServiceBeneficiaire)");
            sb.append("ouf.epmTRefDirectionServiceBeneficiaire.id = :idDirectionServiceBeneficiaire");

            parameters.put("idDirectionServiceBeneficiaire", idDirectionServiceBeneficiaire);
            debut = true;
        }

        if (idDirectionServiceResponsable != null) {
            debut = gererDebut(debut, sb);
            //sb.append("(ouf.epmTRefDirectionService IS NULL or ouf.epmTRefDirectionServiceResponsable.id = :idDirectionServiceResponsable)");
            sb.append("ouf.epmTRefDirectionServiceResponsable.id = :idDirectionServiceResponsable");

            parameters.put("idDirectionServiceResponsable", idDirectionServiceResponsable);
            debut = true;
        }

        if (idResponsable != null) {
            debut = gererDebut(debut, sb);
            //sb.append("(ouf.epmTRefDirectionService IS NULL or ouf.epmTRefDirectionServiceResponsable.id = :idDirectionServiceResponsable)");
            sb.append("ouf.epmTUtilisateurResponsable.id = :idResponsable");

            parameters.put("idResponsable", idResponsable);
            debut = true;
        }

        if (idNature != null) {
            debut = gererDebut(debut, sb);
            sb.append("ouf.epmTRefNature.id = :idNature");

            parameters.put("idNature", idNature);
            debut = true;
        }

        if (estimatifInitialHT != null) {
            debut = gererDebut(debut, sb);
            sb.append("ouf.estimatifInitialHT = :estimatifInitialHT");

            parameters.put("estimatifInitialHT", estimatifInitialHT);
            debut = true;
        }
        
        return sb;
    }

    @Override
    public String toHQL() {
        StringBuilder sb = new StringBuilder("select ouf ");
        sb.append(corpsRequete());
        if (proprieteTriee != null) {
            sb.append(" order by ouf.").append(proprieteTriee);
            if (triCroissant)
                sb.append(" ASC ");
            else
                sb.append(" DESC ");
        }

        return sb.toString();
    }

    @Override
    public String toCountHQL() {
        return "select count(distinct ouf) " + corpsRequete();
    }

    public void setId(Integer valeur) {
        this.id = valeur;
    }

    public void setCode(String valeur) {
        this.code = valeur;
    }

    public void setCodeStrict(boolean valeur) {
        this.codeStrict = valeur;
    }

    public void setLibelle(String valeur) {
        this.libelle = valeur;
    }

    /**
     * @param valeur the idPouvoirAdjudicateur to set
     */
    public void setIdPouvoirAdjudicateur(Integer valeur) {
        this.idPouvoirAdjudicateur = valeur;
    }

    /**
     * @param valeur l'identifiant de la direction service beneficiaire
     */
    public final void setIdDirectionServiceBeneficiaire(Integer valeur) {
        this.idDirectionServiceBeneficiaire = valeur;
    }

    /**
     * @param valeur l'identifiant de la direction service responsable
     */
    public final void setIdDirectionServiceResponsable(Integer valeur) {
        this.idDirectionServiceResponsable = valeur;
    }

    /**
     * @param valeur l'identifiant du responsable
     */
    public final void setIdResponsable(Integer valeur) {
        this.idResponsable = valeur;
    }

    /**
     * @param valeur the idNature to set
     */
    public void setIdNature(Integer valeur) {
        this.idNature = valeur;
    }

    public void setEstimatifInitialHT(Double valeur) {
        this.estimatifInitialHT = valeur;
    }

}
