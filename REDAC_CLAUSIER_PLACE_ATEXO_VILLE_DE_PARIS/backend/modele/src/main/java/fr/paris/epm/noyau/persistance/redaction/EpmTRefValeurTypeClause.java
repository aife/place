package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

/**
 * auteur nty
 */
@Entity
@Table(name = "epm__t_ref_valeur_type_clause", schema = "redaction")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "cacheReferentielSelect")
public class EpmTRefValeurTypeClause extends BaseEpmTRefRedaction {

    /**
     * attribut nom de consultation.
     */
    @ManyToOne
    @JoinColumn(name="id_ref_type_clause", nullable=false)
    private EpmTRefTypeClause epmTRefTypeClause;

    @Column(name = "simple")
    private boolean simple;

    @Column(name = "expression")
    private String expression;

    public EpmTRefTypeClause getEpmTRefTypeClause() {
        return epmTRefTypeClause;
    }

    public void setEpmTRefTypeClause(EpmTRefTypeClause epmTRefTypeClause) {
        this.epmTRefTypeClause = epmTRefTypeClause;
    }

    public boolean isSimple() {
        return simple;
    }

    public void setSimple(boolean simple) {
        this.simple = simple;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

	@Override
	public String toString() {
		var valeur = String.format("#%d - '%s'", this.getId(), this.getLibelle());

		if (null != this.epmTRefTypeClause) {
			valeur += String.format("(type de la clause = #%d - '%s')", this.epmTRefTypeClause.getId(), this.epmTRefTypeClause.getLibelle());
		}

		return valeur;
	}
}
