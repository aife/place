package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefChoixMoisJour de la table "epm__t_ref_choix_mois_jour"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefChoixMoisJour extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int EN_JOUR = 1;  // Identifiant de base utilisé pour le jour.
    public static final int EN_MOIS = 2;  // Identifiant de base utilisé pour le mois.
    public static final int EN_ANNEE = 3; // Identifiant de base utilisé pour l'année. (dsp)

}