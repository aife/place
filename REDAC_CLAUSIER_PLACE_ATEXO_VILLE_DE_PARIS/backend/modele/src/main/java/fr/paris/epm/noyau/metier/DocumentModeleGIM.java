package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.documents.EpmTDocumentModele;

import java.util.Map;

/**
 * Méthodes métiers pour les opérations liées au {@link EpmTDocumentModele}
 * @author Léon Barsamian
 */
public interface DocumentModeleGIM extends BaseGIM {

	String genererFreeMarker(final Map<String, Object> freeMarkerMap, final String xml, final EpmTConsultation idConsultation, final int idLot);


}
