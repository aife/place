package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Pojo hibernate de EpmTClause.
 */
@Entity
@Table(name = "epm__t_clause", schema = "redaction")
@Audited
public class EpmTClause extends EpmTClauseAbstract {

    /**
     * Cet attribut mappe la valeur de la colonne de clé primaire.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Identifiant de la dernière publication pour chaque clause
     */
    @Column(name = "id_last_publication")
    private Integer idLastPublication;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_clause_has_ref_type_contrat",
            joinColumns = @JoinColumn(name = "id_clause", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_type_contrat", referencedColumnName = "id"))
    @NotAudited
    private Set<EpmTRefTypeContrat> epmTRefTypeContrats;

    /**
     * la liste des roles clauses.
     */
    @OneToMany(targetEntity = EpmTRoleClause.class,
            mappedBy = "epmTClause", fetch = FetchType.EAGER, orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
    @NotAudited
    private Set<EpmTRoleClause> epmTRoleClauses = new HashSet<>();

    @OneToMany(targetEntity = EpmTClausePotentiellementConditionnee.class,
            mappedBy = "epmTClause", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL)
    @NotAudited
    private Set<EpmTClausePotentiellementConditionnee> epmTClausePotentiellementConditionnees;
    /*
     * id de la clause editeur origine
     */
    @Column(name = "id_clause_origine")
    private Integer idClauseOrigine;

    /**
     * Référence de la clause éditeur surchargée
     */
    @Column(name = "reference_clause_surchargee")
    private String referenceClauseSurchargee;

    /**
     * Détermine si la surcharge est sélectionnée ou la clause éditeur est sélectionnée
     */
    @Column(name = "surcharge_actif")
    private Boolean surchargeActif = false;

    /**
     * Identifiant de la clause éditeur surchargée
     */
    @Column(name = "nombre_surcharges")
    private Integer nombreSurcharges = 0;


    public Integer getId() {
        return id;
    }

    public void setId(final Integer valeur) {
        id = valeur;
    }

    @Override
    public Integer getIdClause() {
        return id;
    }

    @Override
    public Integer getIdPublication() {
        return null;
    }

    @Override
    public Integer getIdLastPublication() {
        return idLastPublication;
    }

    public void setIdLastPublication(Integer idLastPublication) {
        this.idLastPublication = idLastPublication;
    }

    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        return epmTRefTypeContrats;
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        this.epmTRefTypeContrats = epmTRefTypeContrats;
    }

    @Override
    public Set<EpmTRoleClause> getEpmTRoleClauses() {
        return epmTRoleClauses;
    }

    @Override
    public void setEpmTRoleClauses(Set<? extends EpmTRoleClauseAbstract> epmTRoleClauses) {
        this.epmTRoleClauses = (Set<EpmTRoleClause>) epmTRoleClauses;
    }

    @Override
    public Set<EpmTClausePotentiellementConditionnee> getEpmTClausePotentiellementConditionnees() {
        return epmTClausePotentiellementConditionnees;
    }

    @Override
    public void setEpmTClausePotentiellementConditionnees(Set<? extends EpmTClausePotentiellementConditionneeAbstract> epmTClausePotentiellementConditionnees) {
        this.epmTClausePotentiellementConditionnees = (Set<EpmTClausePotentiellementConditionnee>) epmTClausePotentiellementConditionnees;
    }

    public String getReferenceClauseSurchargee() {
        return referenceClauseSurchargee;
    }

    public void setReferenceClauseSurchargee(String referenceClauseSurchargee) {
        this.referenceClauseSurchargee = referenceClauseSurchargee;
    }

    public Boolean getSurchargeActif() {
        return surchargeActif;
    }

    public void setSurchargeActif(Boolean surchargeActif) {
        this.surchargeActif = surchargeActif;
    }

    public Integer getNombreSurcharges() {
        return nombreSurcharges;
    }

    public void setNombreSurcharges(Integer nombreSurcharges) {
        this.nombreSurcharges = nombreSurcharges;
    }

    @Override
    public EpmTClause clone() throws CloneNotSupportedException {
        EpmTClause epmTClause = (EpmTClause) super.clone();
        epmTClause.id = 0;

        if (epmTRoleClauses != null) {
            epmTClause.epmTRoleClauses = new HashSet<EpmTRoleClause>();
            for (EpmTRoleClause item : getEpmTRoleClauses()) {
                EpmTRoleClause epmTRoleClausesClone = item.clone();
                epmTRoleClausesClone.setEpmTClause(epmTClause);
                epmTClause.epmTRoleClauses.add(epmTRoleClausesClone);
            }
        }

        if (epmTClausePotentiellementConditionnees != null) {
            epmTClause.epmTClausePotentiellementConditionnees = new HashSet<>();
            for (EpmTClausePotentiellementConditionnee item : getEpmTClausePotentiellementConditionnees()) {
                EpmTClausePotentiellementConditionnee cpcClone = item.clone();
                cpcClone.setEpmTClause(epmTClause);
                epmTClause.epmTClausePotentiellementConditionnees.add(cpcClone);
            }
        }
        epmTClause.referenceClauseSurchargee = null;
        epmTClause.surchargeActif = null;
        epmTClause.nombreSurcharges = null;

        return epmTClause;
    }

    public Integer getIdClauseOrigine() {
        return idClauseOrigine;
    }

    public void setIdClauseOrigine(Integer idClauseOrigine) {
        this.idClauseOrigine = idClauseOrigine;
    }
}
