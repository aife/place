package fr.paris.epm.noyau.persistance;

import java.util.Set;

/**
 * Pojo hibernate - Entreprise du référentiel lié au contrat
 * @author Rebeca Dantas
 */
public class EpmTEntrepriseContrat extends EpmTAbstractObject {

    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1L;
    
    protected Integer id;

    /**
     * Numero de siren
     */
    private String siren;

    /**
     * Nom (raison sociale) de l'entreprise
     */
    private String nom;

    /**
     * Liste personnel de l'entreprise
     */
    private Set<EpmTEtablissementEntreprise> listeEtablissements;

    /**
     * L'identifiant de l'organisme de l'entreprise
     */
    private Integer idOrganisme;

    /**
     * @return le Nom (raison sociale) de l'entreprise
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param valeur : Nom (raison sociale) de l'entreprise
     */
    public void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @param valeur : défine le le siren de l'entreprise
     */
    public void setSiren(final String valeur) {
        this.siren = valeur;
    }

    /**
     * @return the siren
     */
    public String getSiren() {
        return siren;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer valeur) {
        this.idOrganisme = valeur;
    }

    public Set<EpmTEtablissementEntreprise> getListeEtablissements() {
        return listeEtablissements;
    }

    public void setListeEtablissements(final Set<EpmTEtablissementEntreprise> valeur) {
        this.listeEtablissements = valeur;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer valeur) {
        this.id = valeur;
    }

}
