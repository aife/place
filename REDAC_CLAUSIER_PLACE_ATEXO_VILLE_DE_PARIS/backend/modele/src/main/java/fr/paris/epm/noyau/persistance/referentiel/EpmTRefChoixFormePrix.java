package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefChoixFormePrix de la table "epm__t_ref_choix_forme_prix"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefChoixFormePrix extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final  String PRIX_UNITAIRE = "Prix unitaire";
    public static final  String PRIX_FORFAITAIRE = "Prix forfaitaire";
    public static final  String PRIX_MIXTE = "Prix mixte";

    public static final int UNITAIRE = 1;
    public static final int FORFAITAIRE = 2;
    public static final int MIXTE = 3;

    /**
     * identifiant GO.
     */
    private String codeGo;

    public String getCodeGo() {
        return codeGo;
    }

    public void setCodeGo(String codeGo) {
        this.codeGo = codeGo;
    }

}