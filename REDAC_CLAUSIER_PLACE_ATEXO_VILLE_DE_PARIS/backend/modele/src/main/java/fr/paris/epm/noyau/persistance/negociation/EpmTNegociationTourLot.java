package fr.paris.epm.noyau.persistance.negociation;

import fr.paris.epm.noyau.persistance.EpmTAbstractObject;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Representer une ligne d'un tour de negociation pour un lot.
 */
public class EpmTNegociationTourLot extends EpmTAbstractObject implements Cloneable {

    private static final long serialVersionUID = -3248079651284287338L;

    private Integer id;
    private NegociationTourEtatEnnum etatTourLot;
    private EpmTNegociationTour negociationTour;
    private Set<EpmTNegociationTourLigne> negociationTourLignes;
    private EpmTBudLot lot;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EpmTNegociationTour getNegociationTour() {
        return negociationTour;
    }

    public void setNegociationTour(EpmTNegociationTour negociationTour) {
        this.negociationTour = negociationTour;
    }

    public NegociationTourEtatEnnum getEtatTourLot() {
        return etatTourLot;
    }

    public void setEtatTourLot(NegociationTourEtatEnnum etatTourLot) {
        this.etatTourLot = etatTourLot;
    }

    public Set<EpmTNegociationTourLigne> getNegociationTourLignes() {
        return negociationTourLignes;
    }

    public void setNegociationTourLignes(Set<EpmTNegociationTourLigne> negociationTourLignes) {
        this.negociationTourLignes = negociationTourLignes;
    }

    public EpmTBudLot getLot() {
        return lot;
    }

    public void setLot(EpmTBudLot lot) {
        this.lot = lot;
    }

    public Object clone() throws CloneNotSupportedException {
        EpmTNegociationTourLot epmTNegociationTourLot = (EpmTNegociationTourLot) super.clone();
        epmTNegociationTourLot.setId(null);
        epmTNegociationTourLot.setNegociationTour(null);
        if (!CollectionUtils.isEmpty(this.negociationTourLignes)) {
            Set<EpmTNegociationTourLigne> lignes = new HashSet<>();
            for (EpmTNegociationTourLigne epmTNegociationTourLigne : this.negociationTourLignes) {
                lignes.add((EpmTNegociationTourLigne) epmTNegociationTourLigne.clone());
            }
            epmTNegociationTourLot.setNegociationTourLignes(lignes);
        }
        return epmTNegociationTourLot;
    }
}
