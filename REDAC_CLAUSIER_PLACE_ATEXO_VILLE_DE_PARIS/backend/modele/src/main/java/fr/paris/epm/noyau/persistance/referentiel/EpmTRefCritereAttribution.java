package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefCritereAttribution de la table "epm__t_ref_critere_attribution"
 * Created by nty on 31/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefCritereAttribution extends EpmTReferentielExterneAbstract {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

    public final static String TYPE_CRITERE_POINT = "point";
    public final static String TYPE_CRITERE_ORDRE = "ordre";
    public final static String TYPE_CRITERE_PRIX = "prix";
    public final static String TYPE_CRITERE_COUT = "coutglobal";
    public final static String TYPE_CRITERE_PONDERE = "ponderation";
    public final static String TYPE_CAHIER_CHARGES = "cahierCharges";

}