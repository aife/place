
package fr.paris.epm.noyau.persistance;


/**
 * POJO hibernate pour le document du passation pour la mise disposition
 * @author GAO Xuesong, Khaled.benari
 * @version $Revision$, $Date$, $Author$
 */
public class EpmTDocumentMiseADispositionPassation extends EpmTAbstractDocument {
    
    /**
     * Marqueur de serialisation
     */
    private static final long serialVersionUID = 1212114073232203345L;

    /**
     * clé primaire
     */
    private int id;

    /**
     * Marquer si le document est a mettre a disposition.
     */
    private boolean selectionne;

    /**
     * Marquer le type du document
     */
    private String typeDocumentPassation;

    /**
     * Marquer le catégorie du document
     */
    private int idCategorieDocument;
    
    /**
     * la mise à disposition effectué sur ce document
     */
    private Integer idMiseADisposition;
    /**
     * le document Conserve auquel apertient ce document
     */
    private Integer idDocumentConserve;
    /**
     * le document contrat auquel apertient ce document
     */
    private Integer idDocumentContrat;
    
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the selectionne
     */
    public boolean isSelectionne() {
        return selectionne;
    }

    /**
     * @param selectionne the selectionne to set
     */
    public void setSelectionne(boolean selectionne) {
        this.selectionne = selectionne;
    }

    /**
     * @return the idMiseADisposition
     */
    public Integer getIdMiseADisposition() {
        return idMiseADisposition;
    }

    /**
     * @param value the idMiseADisposition to set
     */
    public void setIdMiseADisposition(Integer value) {
        this.idMiseADisposition = value;
    }

    /**
     * @return the typeDocumentPassation
     */
    public String getTypeDocumentPassation() {
        return typeDocumentPassation;
    }

    /**
     * @param value the typeDocumentPassation to set
     */
    public void setTypeDocumentPassation(String value) {
        this.typeDocumentPassation = value;
    }

    /**
     * @return the idCategorieDocument
     */
    public int getIdCategorieDocument() {
        return idCategorieDocument;
    }

    /**
     * @param value the idCategorieDocument to set
     */
    public void setIdCategorieDocument(int value) {
        this.idCategorieDocument = value;
    }


    /**
     * @return the idDocumentConserve
     */
    public Integer getIdDocumentConserve() {
        return idDocumentConserve;
    }

    /**
     * @param idDocumentConserve the idDocumentConserve to set
     */
    public void setIdDocumentConserve(Integer idDocumentConserve) {
        this.idDocumentConserve = idDocumentConserve;
    }

    /**
     * @return the idDocumentContrat
     */
    public Integer getIdDocumentContrat() {
        return idDocumentContrat;
    }

    /**
     * @param idDocumentContrat the idDocumentContrat to set
     */
    public void setIdDocumentContrat(Integer idDocumentContrat) {
        this.idDocumentContrat = idDocumentContrat;
    }
    
}
