package fr.paris.epm.noyau.commun.exception;

/**
 * Exception levée lorsque d'un échec d'échange du message avec demat.
 * 
 * @author GAO Xuesong
 */
public class NoyauConnecteurException extends Exception {

	/**
	 * Marqueur de sérialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur vide.
	 */
	public NoyauConnecteurException() {
		super();
	}

	/**
	 * @param msg
	 *            message d'erreur
	 * @param cause
	 *            Exception ayant provoqué cette exception
	 */
	public NoyauConnecteurException(final String msg, final Throwable cause) {
		super(msg, cause);
	}

	/**
	 * @param msg
	 *            message d'erreur
	 */
	public NoyauConnecteurException(final String msg) {
		super(msg);
	}

	/**
	 * @param cause
	 *            Exception ayant provoqué cette exception
	 */
	public NoyauConnecteurException(final Throwable cause) {
		super(cause);
	}

}
