package fr.paris.epm.noyau.persistance.redaction;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Pojo hibernate pour les actions de l'application sur le document
 * @author Rebeca Dantas
 *
 */
@Entity
@Table(name = "epm__t_ref_action_document", schema = "redaction")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE, region="cacheReferentielSelect")
public class EpmTRefActionDocument extends BaseEpmTRefRedaction {

    /**
     * Version du serial
     */
    private static final long serialVersionUID = 1L;
    
    public static final int CREATION_DOCUMENT = 1;
    
    public static final int MODIFICATION_DOCUMENT = 2;
    
    public static final int DEMANDE_VALIDATION = 3;
    
    public static final int VALIDATION = 4;
    
    public static final int REFUS_VALIDATION = 5;
    
    public static final int DE_VALIDATION = 6;
    
}
