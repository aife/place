package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefThemeHabilitation de la table "epm__t_ref_theme_habilitation"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefThemeHabilitation extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}