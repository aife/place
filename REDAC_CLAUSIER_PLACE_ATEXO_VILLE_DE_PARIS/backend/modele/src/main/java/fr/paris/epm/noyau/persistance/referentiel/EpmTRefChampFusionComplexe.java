package fr.paris.epm.noyau.persistance.referentiel;

/**
 * Champ de fusion complexe dont l'expression est une expression freeMarker.
 * @author Léon Barsamian
 */
public class EpmTRefChampFusionComplexe extends EpmTRefChampFusion {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 1L;

}