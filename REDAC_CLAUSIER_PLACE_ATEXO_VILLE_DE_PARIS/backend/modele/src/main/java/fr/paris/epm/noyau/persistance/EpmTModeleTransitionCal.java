package fr.paris.epm.noyau.persistance;

/**
 * Classe utilisé lors du chargment des calendriers (modéle et instance) dans le
 * calendrier GWT lors de son utilisation. Dans tous les autres cas on utilisera
 * {@link EpmTTransitionCal}.
 * @author Léon Barsamian.
 */
public class EpmTModeleTransitionCal extends EpmTAbstractObject {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -3171582682985432442L;

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * Valeur fixe initiale.
     */
    protected int valeurFixeInit;

    /**
     * Valeur variable initiale.
     */
    protected int valeurVariableInit;

    /**
     * Valeur fixe.
     */
    protected int valeurFixe;

    /**
     * valeur variable.
     */
    protected int valeurVariable;

    /**
     * Modifiable, ou non miodifiable.
     */
    protected boolean modifiable;

    /**
     * @return identifiant de l'enregistrement
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return valeur fixe
     */
    public int getValeurFixe() {
        return valeurFixe;
    }

    /**
     * @param valeur valeur fixe
     */
    public void setValeurFixe(final int valeur) {
        this.valeurFixe = valeur;
    }

    /**
     * @return vrai si modifiable, faux sinon
     */
    public boolean isModifiable() {
        return modifiable;
    }

    /**
     * @param valeur vrai si modifiable, faux sinon
     */
    public void setModifiable(final boolean valeur) {
        this.modifiable = valeur;
    }

    /**
     * @return valeur variable
     */
    public int getValeurVariable() {
        return valeurVariable;
    }

    /**
     * @param valeur variable
     */
    public void setValeurVariable(final int valeur) {
        this.valeurVariable = valeur;
    }

    /**
     * @return valeur fixe initiale
     */
    public int getValeurFixeInit() {
        return valeurFixeInit;
    }

    /**
     * @return valeur variable initiale
     */
    public int getValeurVariableInit() {
        return valeurVariableInit;
    }

    /**
     * @param valeur valeur fixe initiale
     */
    public void setValeurFixeInit(final int valeur) {
        this.valeurFixeInit = valeur;
    }

    /**
     * @param valeur valeur variable initiale
     */
    public void setValeurVariableInit(final int valeur) {
        this.valeurVariableInit = valeur;
    }
}
