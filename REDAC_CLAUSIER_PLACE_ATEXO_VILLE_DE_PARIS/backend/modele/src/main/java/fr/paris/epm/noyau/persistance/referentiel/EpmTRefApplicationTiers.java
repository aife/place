package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefApplicationTiers de la table "epm__t_ref_application_tiers"
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefApplicationTiers extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Indiquer si l'application (service tiers) est une application de
     * télétransmission au contrôle de la légalité
     */
    private boolean transmissionControleLegal;

    public boolean isTransmissionControleLegal() {
        return transmissionControleLegal;
    }

    public void setTransmissionControleLegal(boolean transmissionControleLegal) {
        this.transmissionControleLegal = transmissionControleLegal;
    }

}