package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;

public class ClausePotentiellementConditionneePubCritere extends AbstractCritere {

    private Integer idPublication;

    @Override
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = true;

        if (idPublication != null) {
            sb.append(debut ? " where " : " and ");
            sb.append(" condition.idPublication = ");
            sb.append(idPublication);
            debut = true;
        }
        return sb;
    }

    @Override
    public String toHQL() {
        StringBuilder sb = new StringBuilder("select condition from EpmTClausePotentiellementConditionneePub as condition ");
        sb.append(corpsRequete());
        if (getProprieteTriee() != null) {
            sb.append(" ORDER BY condition.").append(getProprieteTriee());
            sb.append(isTriCroissant() ? " ASC" : " DESC");
        }
        return sb.toString();
    }

    @Override
    public String toCountHQL() {
        return "select count(condition.id) from EpmTClausePotentiellementConditionneePub as condition " + corpsRequete();
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

}