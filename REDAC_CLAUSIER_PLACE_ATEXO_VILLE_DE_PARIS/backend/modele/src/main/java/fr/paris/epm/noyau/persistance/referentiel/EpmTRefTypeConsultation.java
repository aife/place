package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefTypeConsultation de la table "epm__t_ref_type_consultation"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefTypeConsultation extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

}
