package fr.paris.epm.noyau.persistance.commun;

import fr.paris.epm.noyau.persistance.EpmTObject;

/**
 * Interface EpmTReferentiel est la racine d'héritation pour toutes les référentiels.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface EpmTRef extends EpmTObject {

	Integer getId();

    void setId(final Integer id);

    boolean isActif();

    void setActif(final boolean actif);

    String getLibelle();

    void setLibelle(final String libelle);

    String getLibelleCourt();

    void setLibelleCourt(final String libelleCourt);

    String getCodeExterne();

    void setCodeExterne(final String codeExterne);

	default boolean isInactif() {
		return !isActif();
	}
}
