/**
 * $Id$
 */
package fr.paris.epm.noyau.persistance;

import fr.paris.epm.noyau.commun.Constantes;

import java.util.Date;

/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class JalonLibreCal extends EpmTAbstractObject {

    // Propriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Libellé.
     */
    private String libelle;

    /**
     * Date.
     */
    private Date echeance;

    // Méthode



    /**
     * @return code de hachage
     */
    public final int hashCode() {
        int result = 1;
        if (echeance != null) {
            result = Constantes.PREMIER * result + echeance.hashCode();
        }
        if (libelle != null) {
            result = Constantes.PREMIER * result + libelle.hashCode();
        }
        return result;
    }

    /**
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JalonLibreCal autre = (JalonLibreCal) obj;
        if (echeance == null) {
            if (autre.echeance != null) {
                return false;
            }
        } else if (!echeance.equals(autre.echeance)) {
            return false;
        }
        if (libelle == null) {
            if (autre.libelle != null) {
                return false;
            }
        } else if (!libelle.equals(autre.libelle)) {
            return false;
        }
        return true;
    }

    // Accesseurs
    /**
     * @return date
     */
    public Date getEcheance() {
        return echeance;
    }

    /**
     * @param valeur date
     */
    public void setEcheance(final Date valeur) {
        this.echeance = valeur;
    }

    /**
     * @return libellé
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé
     */
    public void setLibelle(final String valeur) {
        this.libelle = valeur;
    }
}
