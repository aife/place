package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.commun.Constantes;

/**
 * Rassemble les critères de recherche des clauses et des clauses publiées selon le besion.
 * Utilisé pour faire la UNION des EpmTClause et EpmTClausePub => EpmVClause
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class ClauseViewCritere extends ClausePubCritere {

    private Integer idClausePublication;

    public ClauseViewCritere() {
        super();
    }

    public ClauseViewCritere(String plateformeUuid) {
        super(plateformeUuid);
    }

    @Override
    public StringBuffer corpsRequete() {
        // assemblage de la requête
        StringBuffer sb = super.corpsRequete();

        if (idClausePublication != null) {
            sb.append(" AND clause.epmTClausePub.idClause = ");
            sb.append(idClausePublication);
        }

        return sb;
    }

    @Override
    protected String findByIdClause() {
        return " AND ( ( epmTClausePub.idClause = (:idClause) ) OR ( epmTClause.id = (:idClause) ) )";
    }

    @Override
    protected String findByIdClauses() {
        return " AND ( ( epmTClausePub.idClause in (:listIds) ) OR ( epmTClause.id in (:listIds) ) )";
    }

    /*
        il ne faut redéfinir cette méthode que dans ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVClause.epmTClause ou EpmVClause.epmTClausePub
     */
    protected String findByTypeContrat() {
        String s1 = "(size(epmTClause.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(epmTClause.epmTRefTypeContrats))";
        String s2 = "(size(epmTClausePub.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id = :idTypeContrat) in elements(epmTClausePub.epmTRefTypeContrats))";
        return "(clause.epmTClausePub IS NULL AND " + s1 +
                " OR " +
                "clause.epmTClausePub IS NOT NULL AND " + s2 + ")";
    }

    protected String findByTypesContrat() {
        String s1 = "(size(epmTClause.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id in (:idsTypeContrat)) in elements(epmTClause.epmTRefTypeContrats))";
        String s2 = "(size(epmTClausePub.epmTRefTypeContrats) = 0 OR (from EpmTRefTypeContrat where id in (:idsTypeContrat)) in elements(epmTClausePub.epmTRefTypeContrats))";
        return "(clause.epmTClausePub IS NULL AND " + s1 +
                " OR " +
                "clause.epmTClausePub IS NOT NULL AND " + s2 + ")";
    }

    /*
        il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
        et ClausePubCritere ne doit pas la supporter
     */
    @Override
    protected String findByIdClauseOrigine() {
        return "(clause.epmTClausePub IS NULL AND epmTClause.idClauseOrigine = :idClauseOrigine)";
    }

    /*
        il ne faut redéfinir cette méthode que dans ClausePubCritere et ClauseViewCritere
        car ClauseViewCritere cherche dans EpmVCanevas.epmTClause
        et ClausePubCritere ne doit pas la supporter
     */
    protected String nonAfficherSurcharge() {
        return "(clause.epmTClausePub IS NOT NULL OR (clause.epmTClausePub IS NULL AND epmTClause.idClauseOrigine IS NULL))";
    }

    /*
        il faut redéfinir cette méthode dans ClausePubCritere et ClauseViewCritere
        car ClausePubCritere cherche dans EpmTRoleClausePub
        et ClauseViewCritere cherche dans EpmTClause.EpmTRoleClause
     */
    @Override
    protected String findByMotsCles() {
        StringBuilder sb = new StringBuilder("(");
        boolean debut = true;

        for (String motclef : getMotscles()) {
            motclef = motclef.replaceAll(Constantes.COTE, Constantes.ENCODE_COTE);

            sb.append(debut ? "" : " OR ");
            // Chercher Le mot clé dans le contenu (texte fixe avant) de la clause
            sb.append("LOWER(clause.texteFixeAvant) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (texte fixe après) de la clause
            sb.append("LOWER(clause.texteFixeApres) like LOWER('%").append(motclef).append("%')");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (valeur par defaut) de la clause
            sb.append("clause.epmTClausePub IS NULL AND " +
                    "exists (from EpmTRoleClause role where LOWER(role.valeurDefaut) like LOWER('%").append(motclef).append("%') AND role in elements(epmTClause.epmTRoleClauses))");

            sb.append(" OR ");
            // Chercher Le mot clé dans le contenu (valeur par defaut) de la clause
            sb.append("clause.epmTClausePub IS NOT NULL AND " +
                    "exists (from EpmTRoleClausePub role where LOWER(role.valeurDefaut) like LOWER('%").append(motclef).append("%') AND role in elements(epmTClausePub.epmTRoleClauses))");

            // Chercher Le mot clé dans la liste des mots clés
            sb.append(" OR ");
            sb.append("LOWER(clause.motsCles) like LOWER('%").append(motclef).append("%')");

            debut = false;
        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    protected String getTableDeRecherche() {
        return " EpmVClause ";
    }

    @Override
    protected String getJoinsTables() {
        return " left join clause.epmTClause epmTClause left join clause.epmTClausePub epmTClausePub ";
    }

    public void setIdClausePublication(Integer idClausePublication) {
        this.idClausePublication = idClausePublication;
    }

}
