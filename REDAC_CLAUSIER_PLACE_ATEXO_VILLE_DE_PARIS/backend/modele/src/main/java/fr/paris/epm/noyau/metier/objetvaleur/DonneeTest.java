package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;
import java.util.List;

public class DonneeTest implements Serializable {

    private String libelle;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<KeyValueRequest> getRequests() {
        return requests;
    }

    public void setRequests(List<KeyValueRequest> requests) {
        this.requests = requests;
    }

    private List<KeyValueRequest> requests;
}
