/**
 * $Id$
 */
package fr.paris.epm.noyau.metier.objetvaleur;

import java.io.Serializable;
import java.util.Date;

/**
 * Etape calculée simplifiée du calendrier.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class EtapeSimple implements Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Type d'étape.
     */
    private int type;

    /**
     * Libellé de l'étape.
     */
    private String libelle;

    /**
     * Date de l'étape.
     */
    private Date date;
    
    /**
     * Indique si la date de l'étape à été modifié.
     */
    private boolean dateModifiee = false;

    /**
     * Méthode appelé aprés le remplissage des champs lors du chargement depuis
     * la base.
     */
    public void initialisation() {
        dateModifiee = false;
    }

    // Accesseurs


    /**
     * @return libellé de l'étape
     */
    public final String getLibelle() {
        return libelle;
    }

    /**
     * @param valeur libellé de l'étape
     */
    public final void setLibelle(final String valeur) {
        this.libelle = valeur;
    }

    /**
     * @return type d'étape
     */
    public final int getType() {
        return type;
    }

    /**
     * @param valeur type d'étape
     */
    public final void setType(final int valeur) {
        this.type = valeur;
    }

    /**
     * @return date de l'étape
     */
    public final Date getDate() {
        return date;
    }

    /**
     * @param valeur date de l'étape
     */
    public final void setDate(final Date valeur) {
        this.dateModifiee = true;
        this.date = valeur;
    }

    /**
     * @return the dateModifiee
     */
    public final boolean isDateModifiee() {
        return dateModifiee;
    }
}
