package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;
import java.util.List;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.propositionClassementOffre.critereCandidat.
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineCritereCandidat implements Serializable {

    /**
     * Marqueur de sérialisation
     */
    private static final long serialVersionUID = 1L;

    private String nomCandidat;

    private String siretCandidat;

    private String adresseCandidat;

    private String codePostal;

    private String ville;

    private String mailCandidat;

    private String telephoneCandidat;

    private String faxCandidat;

    private Integer statutEnveloppe;

    private boolean pressenti;
    /**
     * Rang final, celui est rempli tout à la fin par l'instance de workflow.
     */
    private Integer rang;

    /**
     * Note par critere.
     */
    private List<DomaineCritereNoteOffre> listeNoteCritereOffre;

    private Double noteFinale;

    /**
     * Valeur maximal de la note d'une offre.
     */
    private Double maximumNoteCritere;

    /**
     * pf et dqe au moment du classement des offre ( si pas de validation aux classement )
     */
    private Double pfAttribPA;

    private Double dqeAttribPA;

    /**
     * prix consolidé au moment de l'attribution.
     */
    private Double pfDqeConsolidesHt;

    /**
     * prix consolidé au moment de l'attribution
     */
    private Double pfDqeConsolidesTtc;

    /**
     * Candidat retenu ou pas.
     */
    private boolean candidatRetenu;

    public boolean isCandidatRetenu() {
        return candidatRetenu;
    }

    public void setCandidatRetenu(boolean candidatRetenu) {
        this.candidatRetenu = candidatRetenu;
    }

    public final String getNomCandidat() {
        return nomCandidat;
    }

    public final void setNomCandidat(final String valeur) {
        this.nomCandidat = valeur;
    }

    /**
     * @return Note par critere.
     */
    public final List<DomaineCritereNoteOffre> getListeNoteCritereOffre() {
        return listeNoteCritereOffre;
    }

    /**
     * @param valeur Note par critere.
     */
    public final void setListeNoteCritereOffre(final List<DomaineCritereNoteOffre> valeur) {
        this.listeNoteCritereOffre = valeur;
    }

    public final Double getNoteFinale() {
        return noteFinale;
    }

    public final void setNoteFinale(final Double valeur) {
        this.noteFinale = valeur;
    }

    public Double getMaximumNoteCritere() {
        return maximumNoteCritere;
    }

    public void setMaximumNoteCritere(Double valeur) {
        this.maximumNoteCritere = valeur;
    }

    public Integer getRang() {
        return rang;
    }

    public void setRang(Integer valeur) {
        this.rang = valeur;
    }

    public String getSiretCandidat() {
        return siretCandidat;
    }

    public void setSiretCandidat(String siretCandidat) {
        this.siretCandidat = siretCandidat;
    }

    public String getAdresseCandidat() {
        return adresseCandidat;
    }

    public void setAdresseCandidat(String adresseCandidat) {
        this.adresseCandidat = adresseCandidat;
    }

    public String getMailCandidat() {
        return mailCandidat;
    }

    public void setMailCandidat(String mailCandidat) {
        this.mailCandidat = mailCandidat;
    }

    public String getTelephoneCandidat() {
        return telephoneCandidat;
    }

    public void setTelephoneCandidat(String telephoneCandidat) {
        this.telephoneCandidat = telephoneCandidat;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getFaxCandidat() {
        return faxCandidat;
    }

    public void setFaxCandidat(String faxCandidat) {
        this.faxCandidat = faxCandidat;
    }

    public String getNomCandidatUppercase() {
        return nomCandidat != null ? nomCandidat.toUpperCase() : null;
    }

    public Integer getStatutEnveloppe() {
        return statutEnveloppe;
    }

    public void setStatutEnveloppe(Integer statutEnveloppe) {
        this.statutEnveloppe = statutEnveloppe;
    }

    public final boolean isHorsDelai() {
        return (statutEnveloppe!=null && statutEnveloppe == 3);
    }

    public final boolean isNonCompleter() {
        return (statutEnveloppe != null && statutEnveloppe == 5);
    }

    public final boolean isPressenti() {
        return this.pressenti;
    }

    public void setPressenti(boolean pressenti) {
        this.pressenti = pressenti;
    }

    public final boolean isOffreAnalisee() {
        return isPressenti() && (!isHorsDelai() && !isNonCompleter());
    }

    public Double getPfDqeConsolidesTtc() {
        return pfDqeConsolidesTtc;
    }

    public void setPfDqeConsolidesTtc(Double pfDqeConsolidesTtc) {
        this.pfDqeConsolidesTtc = pfDqeConsolidesTtc;
    }

    public Double getPfDqeConsolidesHt() {
        return pfDqeConsolidesHt;
    }

    public void setPfDqeConsolidesHt(Double pfDqeConsolidesHt) {
        this.pfDqeConsolidesHt = pfDqeConsolidesHt;
    }

    public String getPfDqeConsolidesTtcString() {
        return pfDqeConsolidesTtc != null ? String.valueOf(pfDqeConsolidesTtc) + " € TTC" : "";
    }
    public String getPfDqeConsolidesHtString() {
        return pfDqeConsolidesHt != null ? String.valueOf(pfDqeConsolidesHt) + " € HT" : "";
    }

    public Double getPfAttribPA() {
        return pfAttribPA;
    }

    public void setPfAttribPA(Double pfAttribPA) {
        this.pfAttribPA = pfAttribPA;
    }

    public Double getDqeAttribPA() {
        return dqeAttribPA;
    }

    public void setDqeAttribPA(Double dqeAttribPA) {
        this.dqeAttribPA = dqeAttribPA;
    }
}

