package fr.paris.epm.noyau.persistance.referentiel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe abstraite EpmTReferentielSimpleAbstract est la racine d'héritation pour
 * toutes les référentiels sans LibelleCourt mais avec CodeExterne.
 * Created by nty on 27/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class EpmTReferentielExterneAbstract extends BaseEpmTRefReferentiel {

    private static final Logger LOG = LoggerFactory.getLogger(EpmTReferentielExterneAbstract.class);
    @Override
    public String getLibelleCourt() {
        return getLibelle();
    }

    @Override
    public void setLibelleCourt(String libelleCourt) {
        // ajouter "libelle_court" dans la base de données s'il faut différentier avec "libelle"
        // et repasser par EpmTReferentielAbstract
        LOG.debug("Ajouter la colonne \"code_externe\" dans la base de données s'il faut l'avoir");
    }

}
