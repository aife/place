package fr.paris.epm.noyau.persistance.redaction;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Pojo hibernate du canevas_pub
 */
@Entity
@Table(name = "epm__t_canevas_pub", schema = "redaction",
        uniqueConstraints = @UniqueConstraint(columnNames = {"id_canevas", "id_publication"}))
public class EpmTCanevasPub extends EpmTCanevasAbstract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "id_canevas", nullable = false)
    private Integer idCanevas;

    @Column(name = "id_publication", nullable = false)
    private Integer idPublication;

    @OneToMany(targetEntity = EpmTChapitrePub.class,
            mappedBy = "epmTCanevas", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<EpmTChapitrePub> epmTChapitres;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_canevas_has_ref_type_contrat_pub",
            joinColumns = {
                    @JoinColumn(name = "id_canevas", referencedColumnName = "id_canevas"),
                    @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_type_contrat", referencedColumnName = "id")
            })
    private Set<EpmTRefTypeContrat> epmTRefTypeContrats;

    /**
     * Liste des procedures d'editeur
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(schema = "redaction", name = "epm__t_canevas_has_ref_procedure_passation_pub",
            joinColumns = {
                    @JoinColumn(name = "id_canevas", referencedColumnName = "id_canevas"),
                    @JoinColumn(name = "id_publication", referencedColumnName = "id_publication")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "id_procedure", referencedColumnName = "id_procedure")
            })
    private Set<EpmTRefProcedure> epmTRefProcedures;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(Integer idCanevas) {
        this.idCanevas = idCanevas;
    }

    @Override
    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    @Override
    public Integer getIdLastPublication() {
        return idPublication;
    }

    @Override
    public List<EpmTChapitrePub> getEpmTChapitres() {
        return epmTChapitres;
    }

    @Override
    public void setEpmTChapitres(List<? extends EpmTChapitreAbstract> epmTChapitres) {
        this.epmTChapitres = (List<EpmTChapitrePub>) epmTChapitres;
    }

    @Override
    public Set<EpmTRefTypeContrat> getEpmTRefTypeContrats() {
        return epmTRefTypeContrats;
    }

    @Override
    public void setEpmTRefTypeContrats(Set<EpmTRefTypeContrat> epmTRefTypeContrats) {
        this.epmTRefTypeContrats = epmTRefTypeContrats;
    }

    @Override
    public Set<EpmTRefProcedure> getEpmTRefProcedures() {
        return epmTRefProcedures;
    }

    @Override
    public void setEpmTRefProcedures(Set<EpmTRefProcedure> epmTRefProcedures) {
        this.epmTRefProcedures = epmTRefProcedures;
    }

    @Override
    public EpmTCanevasPub clone() throws CloneNotSupportedException {
        EpmTCanevasPub epmTCanevas = (EpmTCanevasPub) super.clone();
        epmTCanevas.id = 0;

        epmTCanevas.epmTRefProcedures = new HashSet<EpmTRefProcedure>(epmTRefProcedures);
        epmTCanevas.epmTRefTypeContrats = new HashSet<EpmTRefTypeContrat>(epmTRefTypeContrats);

        return epmTCanevas;
    }

}
