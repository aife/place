package fr.paris.epm.noyau.commun;

/**
 * Constantes liee a la logique de workflow (introduites pas Jbpm)
 * 
 * @author Rémi Villé
 * @version 2010/03/26 18:29
 */
public class ConstantesWorkflow {

	/**
     * Dans le cas d'un appel d'offre ouvert.
     */
    public static final String WORKFLOW_AOO = "AOO";
    
    /**
     * Dans le cas d'un appel d'offre ouvert enveloppe unique.
     */
    public static final String WORKFLOW_AOO_U = "AOO_ENVELOPPE_UNIQUE"; 

    /**
     * Procedure dsp ouverte.
     */
    public static final String WORKFLOW_DSP_O = "DSP_O";

    /**
     * Procedure dsp restreinte.
     */
    public static final String WORKFLOW_DSP_R = "DSP_R";
    
    /**
     * Procedure dialogue competitif.
     */
    public static final String WORKFLOW_DC = "DIALOGUE_COMPETITIF";
    
    /**
     * Procedure dialogue concours.
     */
    public static final String WORKFLOW_CO = "CONCOURS";
    
    /**
     * Procedure MNAPAMC.
     */
    public static final String WORKFLOW_MNAPAMC = "MNAPMC";
    
    /**
     * rang utilisé dans le cadre des candidature ecarte.
     */
    public static final String RANG_CANDIDATURE_ECARTE = "rang";
}
