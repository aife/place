package fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine;

import java.io.Serializable;

/**
 * Contient les données a fusionner dans un document pour le sous domaine
 * analyseDepouillementPlis.ouvertureOffre.offre.
 * 
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DomaineOffre implements Serializable {

	/**
	 * Marqueur de sérialisation 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Candidat (Tableau d'ouverture des offres).
	 */
	private String candidatOffre;
	/**
	 * Complétude (Tableau d'ouverture des offres).
	 */
	private String completudeOffre;
	/**
	 * Prix EUR HT : PF (Tableau d'ouverture des offres).
	 */
	private Double pFPrixHTOffre;
	/**
	 * Prix EUR HT : DQE (Tableau d'ouverture des offres).
	 */
	private Double dQEPrixHTOffre;
	/**
	 * Prix EUR HT : Cde Type (Tableau d'ouverture des offres).
	 */
	private Double cdeTypePrixOffre;
	/**
	 * Variante technique obligatoire (Tableau d'ouverture des offres).
	 */
	private boolean varianteTechniqueObligOffre;
	/**
	 * Suite à donner (Tableau d'ouverture des offres).
	 */
	private String appreciationOffre;

	/**
	 * Reference du dépot.
	 */
	private String referenceDepot;
	/**
	 * Commentaire
	 */
	private String commentaireOffre;
	
	private String adresse;
	
	private String codePostal;
	
	private String ville;
	
	private String statut;

	private String appreciationComplementaire;

	public final String getCommentaireOffre() {
		return commentaireOffre;
	}

	public final void setCommentaireOffre(final String valeur) {
		this.commentaireOffre = valeur;
	}

	/**
	 * @return Candidat (Tableau d'ouverture des offres).
	 */
	public final String getCandidatOffre() {
		return candidatOffre;
	}

	/**
	 * @param Candidat
	 *            (Tableau d'ouverture des offres).
	 */
	public final void setCandidatOffre(final String valeur) {
		this.candidatOffre = valeur;
	}

	/**
	 * @return Complétude (Tableau d'ouverture des offres).
	 */
	public final String getCompletudeOffre() {
		return completudeOffre;
	}

	/**
	 * @param valeur
	 *            Complétude (Tableau d'ouverture des offres).
	 */
	public final void setCompletudeOffre(String valeur) {
		this.completudeOffre = valeur;
	}

	/**
	 * @return Prix EUR HT : PF (Tableau d'ouverture des offres).
	 */
	public final Double getpFPrixHTOffre() {
		return pFPrixHTOffre;
	}

	/**
	 * @param valeur
	 *            Prix EUR HT : PF (Tableau d'ouverture des offres).
	 */
	public final void setpFPrixHTOffre(final Double valeur) {
		this.pFPrixHTOffre = valeur;
	}

	/**
	 * @return the Prix EUR HT : DQE (Tableau d'ouverture des offres).
	 */
	public final Double getdQEPrixHTOffre() {
		return dQEPrixHTOffre;
	}

	/**
	 * @param valeur
	 *            Prix EUR HT : DQE (Tableau d'ouverture des offres).
	 */
	public final void setdQEPrixHTOffre(Double valeur) {
		this.dQEPrixHTOffre = valeur;
	}

	/**
	 * @return the cdeTypePrixOffre
	 */
	public final Double getCdeTypePrixOffre() {
		return cdeTypePrixOffre;
	}

	/**
	 * @param valeur
	 *            Prix EUR HT : Cde Type (Tableau d'ouverture des offres).
	 */
	public final void setCdeTypePrixOffre(final Double valeur) {
		this.cdeTypePrixOffre = valeur;
	}

	/**
	 * @return Variante technique obligatoire (Tableau d'ouverture des offres).
	 */
	public final boolean getVarianteTechniqueObligOffre() {
		return varianteTechniqueObligOffre;
	}

	/**
	 * @param valeur
	 *            Variante technique obligatoire (Tableau d'ouverture des
	 *            offres).
	 */
	public final void setVarianteTechniqueObligOffre(final boolean valeur) {
		this.varianteTechniqueObligOffre = valeur;
	}

	/**
	 * @return Suite à donner (Tableau d'ouverture des offres).
	 */
	public final String getAppreciationOffre() {
		return appreciationOffre;
	}

	/**
	 * @param valeur
	 *            Suite à donner (Tableau d'ouverture des offres).
	 */
	public final void setAppreciationOffre(final String valeur) {
		this.appreciationOffre = valeur;
	}

	/**
	 * @return Reference du dépot.
	 */
	public final String getReferenceDepot() {
		return referenceDepot;
	}

	/**
	 * @param valeur
	 *            Reference du dépot.
	 */
	public final void setReferenceDepot(final String valeur) {
		this.referenceDepot = valeur;
	}

	/**
	 * @return the adresse
	 */
	public final String getAdresse() {
		return adresse;
	}

	/**
	 * @param valeur the adresse to set
	 */
	public final void setAdresse(final String valeur) {
		this.adresse = valeur;
	}

	/**
	 * @return the codePostal
	 */
	public final String getCodePostal() {
		return codePostal;
	}

	/**
	 * @param valeur the codePostal to set
	 */
	public final void setCodePostal(final String valeur) {
		this.codePostal = valeur;
	}

	/**
	 * @return the ville
	 */
	public final String getVille() {
		return ville;
	}

	/**
	 * @param valeur the ville to set
	 */
	public final void setVille(final String valeur) {
		this.ville = valeur;
	}

    /**
     * @return the statut
     */
    public final String getStatut() {
        return statut;
    }

    /**
     * @param statut the statut to set
     */
    public final void setStatut(final String valeur) {
        this.statut = valeur;
    }

    /**
     * @return the appreciationComplementaire
     */
    public final String getAppreciationComplementaire() {
        return appreciationComplementaire;
    }

    /**
     * @param appreciationComplementaire the appreciationComplementaire to set
     */
    public final void setAppreciationComplementaire(final String valeur) {
        this.appreciationComplementaire = valeur;
    }

}
