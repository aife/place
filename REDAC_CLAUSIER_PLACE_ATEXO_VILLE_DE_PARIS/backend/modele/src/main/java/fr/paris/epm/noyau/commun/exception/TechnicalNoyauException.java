package fr.paris.epm.noyau.commun.exception;

/**
 * Classe mère pour les erreurs d'ordre technique. Conforme au chapitre 3.8 du
 * socle commun de la charte v2.0.
 * @author Guillaume Béraudo
 * @version $Revision: 146 $,
 *          $Date: 2007-07-30 19:00:07 +0200 (lun., 30 juil. 2007) $,
 *          $Author: $
 */
public class TechnicalNoyauException extends RuntimeException {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur vide.
     */
    public TechnicalNoyauException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause exception ayant provoqué cette exception noyau
     */
    public TechnicalNoyauException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public TechnicalNoyauException(final String msg) {
        super(msg);
    }

    /**
     * @param cause exception ayant provoqué cette exception noyau
     */
    public TechnicalNoyauException(final Throwable cause) {
        super(cause);
    }
}
