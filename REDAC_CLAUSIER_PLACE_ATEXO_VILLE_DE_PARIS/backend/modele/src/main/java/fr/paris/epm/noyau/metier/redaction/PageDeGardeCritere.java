package fr.paris.epm.noyau.metier.redaction;

import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.Critere;

import java.io.Serializable;

/**
 * Classe critère utilisée pour la recherche de pages de garde.
 * @author MGA
 * @version $Revision: $, $Date: $, $Author: $
 */
public class PageDeGardeCritere extends AbstractCritere implements Critere,
        Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant du depot.
     */
    private int id;

    /**
     * consultation associée à la page de garde.
     */
    private Integer idConsultation;

    /**
     * Titre de la page de garde.
     */
    private String titre;

    /**
     * Nom du fichier.
     */
    private String nomFichier;
    
    /**
     * @return corps de la requête HQL
     */
    public StringBuffer corpsRequete() {
        StringBuffer sb = new StringBuffer();
        boolean debut = false;

        if (id != 0) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" p.id = ");
            sb.append(id);
            debut = true;
        }

        if (idConsultation != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" p.idConsultation = ");
            sb.append(idConsultation);
            debut = true;
        }

        if (titre != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" lower(p.titre) = '");
            sb.append(titre.toLowerCase()).append("'");
            debut = true;
        }
        
        if (nomFichier != null) {
            if (debut) {
                sb.append(" and ");
            } else {
                sb.append(" where ");
            }
            sb.append(" lower(p.nomFichier) = '");
            sb.append(nomFichier.toLowerCase()).append("'");
            debut = true;
        }
        
        return sb;
    }

    /**
     * @return chaine HQL utilisée par Hibernate
     */
    public final String toHQL() {
        final StringBuffer req = new StringBuffer("from EpmTPageDeGarde as p ");
        req.append(corpsRequete());
        if (proprieteTriee != null) {
            req.append(" order by p.");
            req.append(proprieteTriee);
            if (triCroissant) {
                req.append(" ASC");
            } else {
                req.append(" DESC");
            }
        }        
        return req.toString();
    }

    /**
     * @return the id
     */
    public final int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return the idConsultation
     */
    public final Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param idConsultation the idConsultation to set
     */
    public final void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @return the titre
     */
    public final String getTitre() {
        return titre;
    }

    /**
     * @param titre the titre to set
     */
    public final void setTitre(final String valeur) {
        this.titre = valeur;
    }

    /**
     * @return the nomFichier
     */
    public final String getNomFichier() {
        return nomFichier;
    }

    /**
     * @param nomFichier the nomFichier to set
     */
    public final void setNomFichier(final String valeur) {
        this.nomFichier = valeur;
    }

    /* (non-Javadoc)
     * @see fr.paris.epm.noyau.metier.Critere#toCountHQL()
     */
    @Override
    public String toCountHQL() {
        return null;
    }
    
}
