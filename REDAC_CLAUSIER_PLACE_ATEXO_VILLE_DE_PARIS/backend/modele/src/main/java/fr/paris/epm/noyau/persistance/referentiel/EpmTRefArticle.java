package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefArticle de la table "epm__t_ref_article"
 * Created by nty on 28/12/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefArticle extends BaseEpmTRefReferentiel {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Nombre d'enregistrements actifs en base.
     */
    public static final int NB_ACTIFS = 29;
    public final static String NA = "NA";

}
