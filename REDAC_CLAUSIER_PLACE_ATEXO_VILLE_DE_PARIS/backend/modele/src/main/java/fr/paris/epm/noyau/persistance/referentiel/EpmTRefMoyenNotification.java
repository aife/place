package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefMoyenNotification de la table "epm__t_ref_moyen_notification"
 * Created by nty on 06/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefMoyenNotification extends EpmTReferentielExterneAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int REMISE_EN_MAIN_PROPRE = 1;
    public static final int COURRIER_AR = 2;

}