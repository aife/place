package fr.paris.epm.noyau.persistance.referentiel;

/**
 * POJO EpmTRefVision de la table "epm__t_ref_vision"
 * Created by nty on 07/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class EpmTRefVision extends EpmTReferentielSimpleAbstract {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final int P_AUCUN = 0; // Périmètre: aucun contrôle (id logique).
    public static final int P_INTERVENTION = 1; // Périmètre intervention (id en base).
    public static final int P_VISION = 2; // Périmètre vision (id en base).
    public static final int P_TOUS = 3; // Périmètre vision et intervention (id logique).
    public static final int P_INTERVENTION_BENEFICIAIRE = 4; // Perimetre de vision pour les directions beneficiaires.

}