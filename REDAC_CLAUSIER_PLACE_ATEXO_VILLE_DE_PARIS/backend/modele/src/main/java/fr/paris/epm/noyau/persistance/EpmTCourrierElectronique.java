package fr.paris.epm.noyau.persistance;

import java.util.Set;

/**
 * Classe contenant le contenu, l'expéditeur, les destinataires et la pièce.
 * jointe à envoyer
 * @author Mounthei
 */
public class EpmTCourrierElectronique extends EpmTAbstractObject {

    // Proriétés
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant en base.
     */
    private int id;

    /**
     * Expéditeur.
     */
    private String adresseElectroniqueDestinataireFrom;

    /**
     * Copie cachée.
     */
    private String adresseElectroniqueDestinataireBcc;

    /**
     * Copie non cachée.
     */
    private String adresseElectroniqueDestinataireCc;

    /**
     * Destinataire(s) principaux.
     */
    private String adresseElectroniqueDestinataireTo;

    /**
     * Objet du courrier électronique.
     */
    private String objetCourrierElectronique;

    /**
     * Contenu texte du courrier électronique.
     */
    private String contenuCourrierElectronique;
    
    /**
     * Dans le cas de plusieurs pièces jointes.
     */
    private Set<EpmTPieceJointe> piecesJointes;

    /**
     * @return adresse destinataire du courriel
     */
    public String getAdresseElectroniqueDestinataireTo() {
        return adresseElectroniqueDestinataireTo;
    }

    /**
     * @param valeur adresse destinataire du courriel
     */
    public void setAdresseElectroniqueDestinataireTo(final String valeur) {
        this.adresseElectroniqueDestinataireTo = valeur;
    }

    /**
     * @return contenu du courriel
     */
    public String getContenuCourrierElectronique() {
        return contenuCourrierElectronique;
    }

    /**
     * @param valeur contenu du courriel
     */
    public void setContenuCourrierElectronique(String valeur) {
        this.contenuCourrierElectronique = valeur;
    }

    /**
     * @return objet du courriel
     */
    public String getObjetCourrierElectronique() {
        return objetCourrierElectronique;
    }

    /**
     * @param valeur objet du courriel
     */
    public void setObjetCourrierElectronique(final String valeur) {
        this.objetCourrierElectronique = valeur;
    }

    /**
     * @return adresse destinataire caché en copie du courriel
     */
    public String getAdresseElectroniqueDestinataireBcc() {
        return adresseElectroniqueDestinataireBcc;
    }

    /**
     * @param valeur adresse destinataire caché en copie du courriel
     */
    public void setAdresseElectroniqueDestinataireBcc(final String valeur) {
        this.adresseElectroniqueDestinataireBcc = valeur;
    }

    /**
     * @return adresse destinataire en copie du courriel
     */
    public String getAdresseElectroniqueDestinataireCc() {
        return adresseElectroniqueDestinataireCc;
    }

    /**
     * @param valeur adresse destinataire en copie du courriel
     */
    public void setAdresseElectroniqueDestinataireCc(final String valeur) {
        this.adresseElectroniqueDestinataireCc = valeur;
    }

    /**
     * @return identifiant
     */
    public int getId() {
        return id;
    }

    /**
     * @param valeur identifiant
     */
    public void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return adresse expéditeur du courriel
     */
    public String getAdresseElectroniqueDestinataireFrom() {
        return adresseElectroniqueDestinataireFrom;
    }

    /**
     * @param valeur adresse expéditeur du courriel
     */
    public void setAdresseElectroniqueDestinataireFrom(final String valeur) {
        this.adresseElectroniqueDestinataireFrom = valeur;
    }
  
    /**
     * @return valeur ensemble des pieces jointes du mail
     */
    public Set<EpmTPieceJointe> getPiecesJointes() {
        return piecesJointes;
    }

    /**
     * @param valeur ensemble des pieces jointes du mail
     */
    public void setPiecesJointes(final Set<EpmTPieceJointe> valeur) {
        this.piecesJointes = valeur;
    }
}
