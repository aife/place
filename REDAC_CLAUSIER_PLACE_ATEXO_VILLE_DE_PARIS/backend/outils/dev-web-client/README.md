# ![Logo](../../.assets/logo.png "Logo") Atexo™ Redaction™ Dev Web Client
Client web pour l'application [Atexo™ Redaction™](./../../README.md)

[![node version](https://img.shields.io/badge/node-v14.8.0-green.svg?style=flat)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
[![npm version](https://img.shields.io/badge/npm-6.14.8-green.svg?style=flat)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
[![typescript version](https://img.shields.io/badge/typescript-3.9.5-blue.svg?style=flat)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 

Ce projet permet de s'interfacer facilement avec l'application REDAC (l'application de rédaction de document).

## Exigences minimales

- [node v14.8.0](https://nodejs.org/en/) (ou supérieur)
- [npm 6.14.8](https://www.npmjs.com/) (ou supérieur)

> Attention : 
> Dans le document suivant, vous pouvez utiliser soit : 
> - la version de npm installée localement (par maven) 
> - soit votre version globale/système
> (si elle est déjà installée et configurée, qui n'est pas documenté ici - faite attention à la version requise)
> 
> TL; DR; 
> Pour NPM, vous pouvez utiliser : `./node_modules/node/npm` ou juste `npm`
> Pour maven, vous pouvez utiliser : `./../.././mvnw` ou juste `mvn`

## Installation

```shell
./../.././mvnw install
```

alternativement, vous pouvez installer directement [npm](https://www.npmjs.com/) (local ou système)

```shell
./node_modules/node/npm install
```

## Clean

```shell
./../../mvnw clean
```

## Exécution

⚠️ Cette application doit etre installée [installée](#installation) en premier

```shell
./target/node/npm run local
```

L'URL par défaut est [http://localhost:4200/](http://localhost:4200/)

## Test

```shell
./target/node/npm test
```

## Configuration

### Environnements
Les environnements sont définis dans [src/assets/fixtures/environments.json](src/assets/fixtures/environments.json)

