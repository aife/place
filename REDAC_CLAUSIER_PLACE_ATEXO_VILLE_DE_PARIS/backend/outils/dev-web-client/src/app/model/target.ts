export class Target {
  constructor(
    public protocol : string,
    public host : string,
    public port : string,
    public path: string
  ) {}

  get uri(): string {
    return `${this.protocol}//${this.host}${this.port ? ':'+this.port : ''}`;
  }
}
