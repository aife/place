import { Target } from './target';
import { Account } from './account';

export class Environment {

  constructor(
    public id : string,
    public target : Target,
    public account : Account,
    public icon : string
  ) {}



}
