import { sha256 } from 'js-sha256';

export class Account {
  constructor(
    public host : string,
    public login : string,
    public password : string,
    public firstname : string,
    public lastname : string,
    public email: string,
    public mpeUrl: string,
    public plateformUuid: string,
    public organization : string,
    public encrypted: boolean,
    public salt: string,
    public service: string,
    public procedure: string
  ) {}


}
