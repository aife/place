import {Component, Inject} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent {
  constructor(@Inject(APP_BASE_HREF) public baseHref: string) {
  }
}
