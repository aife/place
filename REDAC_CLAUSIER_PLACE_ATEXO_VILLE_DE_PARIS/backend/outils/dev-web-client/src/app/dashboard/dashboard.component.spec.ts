import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HIGHLIGHT_OPTIONS, HighlightModule } from 'ngx-highlightjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountAdapter, EnvironmentAdapter, TargetAdapter } from '../adapters';
import { EnvironmentService, PayloadService, ToastService } from '../services';
import { ToastLocalService } from '../services/remote/toast-local.service';
import { EnvironmentLocalService } from '../services/remote/environment-local.service';
import { PayloadLocalService } from '../services/remote/payload-local.service';
import { AppComponent } from '../app.component';
import { SearchClauseComponent } from '../domains/clause/search-clause/search-clause.component';
import { CreateClauseComponent } from '../domains/clause/create-clause/create-clause.component';
import { ExportClauseComponent } from '../domains/clause/export-clause/export-clause.component';
import { ViewDocumentComponent } from '../domains/document/view-document/view-document.component';
import { CreateCanevasComponent } from '../domains/canevas/create-canevas/create-canevas.component';
import { SearchCanevasComponent } from '../domains/canevas/search-canevas/search-canevas.component';
import { InitContextComponent } from '../domains/authentication/init-context/init-context.component';
import { SafePipe, ToastsContainer } from '../shared';
import { UpdateTemplateComponent } from '../domains/template/update-template/update-template.component';
import { CreatePublicationComponent } from '../domains/publication/create-publication/create-publication.component';
import { HistoryPublicationComponent } from '../domains/publication/history-publication/history-publication.component';
import { SettingsClauseComponent } from '../domains/clause/settings-clause/settings-clause.component';
import { SettingsTemplateComponent } from '../domains/template/settings-template/settings-template.component';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        HighlightModule,
        NgbModule,
      ],
      providers: [
        EnvironmentAdapter,
        TargetAdapter,
        AccountAdapter,
        { provide: ToastService, useClass: ToastLocalService },
        { provide: EnvironmentService, useClass: EnvironmentLocalService },
        { provide: PayloadService, useClass: PayloadLocalService },
        {
          provide: HIGHLIGHT_OPTIONS,
          useValue: {
            coreLibraryLoader: () => import('highlight.js'),
            lineNumbersLoader: () => import('highlightjs-line-numbers.js'), // Optional, only if you want the line numbers
            languages: {
              typescript: () => import('highlight.js/lib/languages/typescript'),
              css: () => import('highlight.js/lib/languages/css'),
              xml: () => import('highlight.js/lib/languages/xml')
            }
          }
        }],
      declarations: [
        AppComponent,
        DashboardComponent,
        SearchClauseComponent,
        CreateClauseComponent,
        ExportClauseComponent,
        ViewDocumentComponent,
        CreateCanevasComponent,
        SearchCanevasComponent,
        InitContextComponent,
        SafePipe,
        UpdateTemplateComponent,
        CreatePublicationComponent,
        HistoryPublicationComponent,
        SettingsClauseComponent,
        SettingsTemplateComponent,
        ToastsContainer
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
