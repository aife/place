import {Component, Inject, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseComponent} from './shared';
import {Account, Environment} from './model';
import {EnvironmentService, ToastService} from './services';
import {debounceTime, distinctUntilChanged, filter, map, mergeMap, tap} from 'rxjs/operators';
import {merge, Observable, Subject} from 'rxjs';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {APP_BASE_HREF} from '@angular/common';
import {sha256} from 'js-sha256';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent extends BaseComponent {
  environments: Environment[];

  @ViewChild('instance', {static: true}) instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? this.environments
        : this.environments.filter(v => v.id.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  formatter = (x: {id: string}) => x.id;
  algorithm: string = 'SHA256';

  constructor(
    private http: HttpClient,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string

  ) {
    super(toastService, environmentService, activatedRoute, router);
    console.log('AppComponent constructor avec baseHref = ' + baseHref);

    if (this.baseHref === '/') {
      environmentService.findAll().subscribe( environnments => this.environments = environnments );
    } else {
      this.logout().subscribe(
        _ => {
          console.log(`Logout successfully`);
        },
        error => console.log(`Error during logout`, error)
      );
    }
  }

  public getSha(account: Account): string {
    console.log('password', account.password);
    return account.encrypted ? sha256(account.salt + account.password) : account.password;
  }
  logon(): void {
    this.toastService.clear();

    this.progress();

    console.log(`Login with key ${this.environment.account.login} and password ${this.environment.account.password}`);
    console.log(`Login with sha ${this.getSha(this.environment.account)}`);

    let uri = `/${this.environment.account.host}/authentification/connexion/${this.environment.account.login}/${this.getSha(this.environment.account)}`;

      this.http.get(uri, this.options)
        .pipe( tap( response => console.log(`Response before parsing = ${response} for url = ${uri}`) ) )
        .pipe( mergeMap( response => this.parser.parseStringPromise(response) ) )
        .pipe( map( xml => xml.ticket ) )
        .pipe( tap( ticket => console.log(`Successfully retrieve new ticket = ${ticket}`) ) )
        .pipe( tap( ticket => this.ticket = ticket ) )
        .pipe( tap( _ => this.connect() ))
        .subscribe(
          _ => {
            this.toastService.clear();
            this.toastService.show(`Vous êtes correctement connecté à ${this.environment.account.host}`, {classname: 'bg-success text-light'}, '✅ Connection OK');
            this.toastService.show(`${this.ticket}`, { delay: 1500000 }, 'ℹ️ Ticket');

            this.router.navigate(['dashboard']).then(
              _ => console.log('Go to dashboard')
            );
          },
          error=>{
            console.log('Error occured when try to login!', error);

            this.toastService.show('Impossible de se connecter au serveur, merci de réessayer', { classname: 'bg-danger text-light', delay: 1500000 }, '💥 Erreur HTTP');

            this.disconnect();
          });
    }


  onActivate( $event: any ) {
    $event?.contextEvent?.subscribe(data => this.identifiant = data)
  }

  update( proxy: string ) {
    if (proxy) {
      console.log(`Change env for ${proxy}`);
    }

    this.identifiant = this.ticket = undefined;

    this.environment.account = this.environment.account;
    this.saveContext();
    this.logon();

  }

  protected ready(): void {
    console.log(`Settings are proxy = ${this.environment.account?.host}, organization = ${this.environment.account?.organization}, login = ${this.environment.account?.login} and password = ${this.environment.account?.password}`);
  }

  clear() {
    this.environment.account.password = '';
  }
}
