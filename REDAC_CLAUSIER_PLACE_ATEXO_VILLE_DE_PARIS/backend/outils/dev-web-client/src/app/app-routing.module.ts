import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchClauseComponent} from './domains/clause/search-clause/search-clause.component';
import {CreateClauseComponent} from './domains/clause/create-clause/create-clause.component';
import {ExportClauseComponent} from './domains/clause/export-clause/export-clause.component';
import {ViewDocumentComponent} from './domains/document/view-document/view-document.component';
import {InitContextComponent} from './domains/authentication/init-context/init-context.component';
import {SearchCanevasComponent} from './domains/canevas/search-canevas/search-canevas.component';
import {CreateCanevasComponent} from './domains/canevas/create-canevas/create-canevas.component';
import {UpdateTemplateComponent} from './domains/template/update-template/update-template.component';
import {HistoryPublicationComponent} from './domains/publication/history-publication/history-publication.component';
import {CreatePublicationComponent} from './domains/publication/create-publication/create-publication.component';
import {SettingsClauseComponent} from './domains/clause/settings-clause/settings-clause.component';
import {SettingsTemplateComponent} from './domains/template/settings-template/settings-template.component';
import {DocumentModeleComponent} from './domains/administration/document-modele/document-modele.component';
import {
  ChampFusionComplexeComponent
} from './domains/administration/champ-fusion-complexe/champ-fusion-complexe.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'context/initialize', component: InitContextComponent },
  { path: 'clause/search', component: SearchClauseComponent },
  { path: 'clause/create', component: CreateClauseComponent },
  { path: 'clause/export', component: ExportClauseComponent },
  { path: 'clause/settings', component: SettingsClauseComponent },
  { path: 'canevas/search', component: SearchCanevasComponent },
  { path: 'canevas/create', component: CreateCanevasComponent },
  { path: 'template/update', component: UpdateTemplateComponent },
  { path: 'template/settings', component: SettingsTemplateComponent },
  { path: 'publication/create', component: CreatePublicationComponent },
  { path: 'publication/history', component: HistoryPublicationComponent },
  { path: 'document/view', component: ViewDocumentComponent },
  {path: 'administration/document-modele', component: DocumentModeleComponent},
  {path: 'administration/champ-fusion-complexe', component: ChampFusionComplexeComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
