import { Injectable, TemplateRef } from '@angular/core';

/**
 * Toast service
 */
@Injectable()
export abstract class ToastService {

  abstract show( textOrTpl: string | TemplateRef<any>, options: any, header?: string ): any;

  abstract clear();

  abstract remove( toast );

  abstract removeAll( predicate );

  abstract list(): any[];

  abstract filter(predicate): any[];
}

