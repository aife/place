import {Inject, Injectable} from '@angular/core';
import {EnvironmentService} from '../index';
import {Environment} from '../../model';
import {Environment as JSON} from '../../adapters/json';
import {Observable} from 'rxjs/internal/Observable';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {EnvironmentAdapter} from '../../adapters';
import {APP_BASE_HREF} from '@angular/common';

/**
 * Account Restful service
 */
@Injectable()
export class EnvironmentLocalService extends EnvironmentService {


  constructor(private httpClient: HttpClient, private environmentAdapter: EnvironmentAdapter, @Inject(APP_BASE_HREF) public baseHref: string) {
    super();
    console.log('EnvironmentLocalService constructor avec baseHref = ' + baseHref);
  }

  public findAll(): Observable<Environment[]> {
    return this.httpClient.get<any>(this.baseHref + 'assets/fixtures/environments.json')
      .pipe( map( environments => Object.values(environments) ))
      .pipe( map( (environments: JSON[]) => environments.map((env:JSON)=>this.environmentAdapter.convert(env)) ));
  }

  find( predicate ): Observable<Environment> {
    return this.findAll().pipe( map( environments => environments.find<Environment>(predicate)) );
  }

}
