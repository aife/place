import {Inject, Injectable} from '@angular/core';
import {PayloadService} from '../payload.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import {APP_BASE_HREF} from '@angular/common';

/**
 * Payload local service
 */
@Injectable()
export class PayloadLocalService extends PayloadService {

  constructor(private http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    super();
  }

  find( type: string ): Observable<string> {
    let options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/xml' })
        .set('Accept', 'text/xml,application/xhtml+xml,application/xml,application/vnd.sun.wadl+xml'),
      responseType: 'text' as 'text'
    };

    return this.http.get(this.baseHref + `assets/fixtures/put-payload-${type}.xml`, options);
  }


}
