import { Injectable, TemplateRef } from '@angular/core';
import { ToastService } from '../toast.service';

@Injectable({ providedIn: 'root' })
export class ToastLocalService implements ToastService {
  private toasts: any[] = [];

  show( content: string | TemplateRef<any>, options: any = {}, header?: string): any {
    let toast = { id: Math.random().toString(36).substr(2, 9), content: content, header, ...options };

    this.toasts.push(toast);

    return toast;
  }

  clear() {
    this.toasts.forEach(toast => this.remove(toast));
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t.id !== toast.id);
  }

  removeAll(predicate) {
    this.filter(predicate).forEach(
      toast => this.remove(toast)
    )
  }

  list(): any[] {
    return this.toasts;
  }

  filter( predicate ): any[] {
    return this.toasts.filter<any>(predicate);
  }
}
