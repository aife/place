import { Injectable } from '@angular/core';
import { Environment } from '../model/environment';
import { Observable } from 'rxjs/internal/Observable';

/**
 * Payload service
 */
@Injectable()
export abstract class PayloadService {

  /**
   * Get the payload
   */
  abstract find(type:string): Observable<string>;
}
