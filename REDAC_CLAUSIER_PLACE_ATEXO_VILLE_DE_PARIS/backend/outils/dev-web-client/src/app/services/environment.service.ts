import { Injectable } from '@angular/core';
import { Environment } from '../model';
import { Observable } from 'rxjs/internal/Observable';

/**
 * Environment service
 */
@Injectable()
export abstract class EnvironmentService {

  /**
   * Get the environments
   */
  abstract findAll(): Observable<Environment[]>;

  /**
   * Get the environment
   */
  abstract find( predicate ): Observable<Environment>;

}

