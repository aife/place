import {Component, Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseRequestableComponent} from '../../../shared';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {from, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-view-document',
  templateUrl: './view-document.component.html',
  styleUrls: ['./view-document.component.sass']
})
export class ViewDocumentComponent extends BaseRequestableComponent {

  consultation: Observable<any>;

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);

    let payload = window?.history?.state?.data;

    if (payload) {
      this.consultation = from(this.parser.parseStringPromise(payload))
        .pipe( tap(xml => console.log(`XML for payload is ${JSON.stringify(xml)}`)))
        .pipe( map( (xml: any) => xml.redaction) )
        .pipe( map( (redaction: any) => redaction.initialisation as Array<any>) )
        .pipe( map( (initalisations:Array<any>) => initalisations[0]))
        .pipe( map( (initialisation: any) => initialisation.consultation as Array<any>))
        .pipe( map( (consultations: Array<any>) => consultations[0]));
    }
  }

  protected getUrl(identifiant: string): string {
    return `/externeRechercheDocument.epm?identifiantSaaS=${identifiant}`;
  }
}
