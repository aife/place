import {Component} from '@angular/core';
import {BaseComponent} from '../../../shared';
import {ActivatedRoute, Router} from '@angular/router';
import {EnvironmentService, PayloadService, ToastService} from '../../../services';
import {map, tap} from 'rxjs/operators';

@Component({
  selector: 'app-init-context',
  templateUrl: './init-context.component.html',
  styleUrls: ['./init-context.component.sass']
})
export class InitContextComponent extends BaseComponent {
  payload: string;
  endpoint: string;

  constructor(
    private payloadService: PayloadService,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router
  ) {
    super(toastService, environmentService, activatedRoute, router);
  }

  ngOnInit(): void {
     super.ngOnInit();
  }

  protected ready(): void {
    this.activatedRoute.queryParams.subscribe( params => {
      let account= this.environment.account;
      console.log('Account = ', account);
        this.payloadService.find(params['payload'])
          .pipe( map( payload => payload.replace(/\${([a-z]*)}/gi, match =>
            account[match.substring(2, match.length-1)]
          )) )
          .pipe( tap( payload => `Transform payload is ${payload}`))
          .subscribe(
            next => this.payload = next,
            error => console.log('An error occured, sorry', error)
          );

        this.endpoint = params['forward'];
      }
    );
  }

  send( $event: HTMLElement ): boolean {
    this.payload = $event.innerText.replace(/^\s*[\r\n\t]/gm, '');

    console.log(`Payload after edition = '${this.payload}'`);

    let params: any = {
      ticket: this.ticket,
      proxy: this.environment?.target?.host,
      editor: this.editor,
      preferences: this.preferences,
      organization: this.environment?.account?.organization
    };

    let state: any = {
      data: this.payload
    };
    this.router.navigate( [this.endpoint], { queryParams: params, state: state } )
      .then( _ => console.log(`Redirect to ${this.endpoint}`));
    return false;
  }
}
