import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitContextComponent } from './init-context.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HIGHLIGHT_OPTIONS, HighlightModule } from 'ngx-highlightjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountAdapter, EnvironmentAdapter, TargetAdapter } from '../../../adapters';
import { EnvironmentService, PayloadService, ToastService } from '../../../services';
import { ToastLocalService } from '../../../services/remote/toast-local.service';
import { EnvironmentLocalService } from '../../../services/remote/environment-local.service';
import { PayloadLocalService } from '../../../services/remote/payload-local.service';
import { AppComponent } from '../../../app.component';
import { DashboardComponent } from '../../../dashboard/dashboard.component';
import { SearchClauseComponent } from '../../clause/search-clause/search-clause.component';
import { CreateClauseComponent } from '../../clause/create-clause/create-clause.component';
import { ExportClauseComponent } from '../../clause/export-clause/export-clause.component';
import { ViewDocumentComponent } from '../../document/view-document/view-document.component';
import { CreateCanevasComponent } from '../../canevas/create-canevas/create-canevas.component';
import { SearchCanevasComponent } from '../../canevas/search-canevas/search-canevas.component';
import { SafePipe, ToastsContainer } from '../../../shared';
import { UpdateTemplateComponent } from '../../template/update-template/update-template.component';
import { CreatePublicationComponent } from '../../publication/create-publication/create-publication.component';
import { HistoryPublicationComponent } from '../../publication/history-publication/history-publication.component';
import { SettingsClauseComponent } from '../../clause/settings-clause/settings-clause.component';
import { SettingsTemplateComponent } from '../../template/settings-template/settings-template.component';

describe('InitContextComponent', () => {
  let component: InitContextComponent;
  let fixture: ComponentFixture<InitContextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        HighlightModule,
        NgbModule,
      ],
      providers: [
        EnvironmentAdapter,
        TargetAdapter,
        AccountAdapter,
        { provide: ToastService, useClass: ToastLocalService },
        { provide: EnvironmentService, useClass: EnvironmentLocalService },
        { provide: PayloadService, useClass: PayloadLocalService },
        {
          provide: HIGHLIGHT_OPTIONS,
          useValue: {
            coreLibraryLoader: () => import('highlight.js'),
            lineNumbersLoader: () => import('highlightjs-line-numbers.js'), // Optional, only if you want the line numbers
            languages: {
              typescript: () => import('highlight.js/lib/languages/typescript'),
              css: () => import('highlight.js/lib/languages/css'),
              xml: () => import('highlight.js/lib/languages/xml')
            }
          }
        }],
      declarations: [
        AppComponent,
        DashboardComponent,
        SearchClauseComponent,
        CreateClauseComponent,
        ExportClauseComponent,
        ViewDocumentComponent,
        CreateCanevasComponent,
        SearchCanevasComponent,
        InitContextComponent,
        SafePipe,
        UpdateTemplateComponent,
        CreatePublicationComponent,
        HistoryPublicationComponent,
        SettingsClauseComponent,
        SettingsTemplateComponent,
        ToastsContainer
      ]
    })
      .compileComponents();
  }));  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        HighlightModule,
        NgbModule,
      ],
      providers: [
        EnvironmentAdapter,
        TargetAdapter,
        AccountAdapter,
        { provide: ToastService, useClass: ToastLocalService },
        { provide: EnvironmentService, useClass: EnvironmentLocalService },
        { provide: PayloadService, useClass: PayloadLocalService },
        {
          provide: HIGHLIGHT_OPTIONS,
          useValue: {
            coreLibraryLoader: () => import('highlight.js'),
            lineNumbersLoader: () => import('highlightjs-line-numbers.js'), // Optional, only if you want the line numbers
            languages: {
              typescript: () => import('highlight.js/lib/languages/typescript'),
              css: () => import('highlight.js/lib/languages/css'),
              xml: () => import('highlight.js/lib/languages/xml')
            }
          }
        }],
      declarations: [
        AppComponent,
        DashboardComponent,
        SearchClauseComponent,
        CreateClauseComponent,
        ExportClauseComponent,
        ViewDocumentComponent,
        CreateCanevasComponent,
        SearchCanevasComponent,
        InitContextComponent,
        SafePipe,
        UpdateTemplateComponent,
        CreatePublicationComponent,
        HistoryPublicationComponent,
        SettingsClauseComponent,
        SettingsTemplateComponent,
        ToastsContainer
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitContextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
