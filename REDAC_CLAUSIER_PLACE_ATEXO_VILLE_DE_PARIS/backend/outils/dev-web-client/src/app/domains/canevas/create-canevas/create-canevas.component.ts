import {Component, Inject} from '@angular/core';
import {BaseRequestableComponent} from '../../../shared/base.requestable.component';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-create-canevas',
  templateUrl: './create-canevas.component.html',
  styleUrls: ['./create-canevas.component.sass']
})
export class CreateCanevasComponent extends BaseRequestableComponent {

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);
  }

  protected getUrl(identifiant: string): string {
    return `/${this.editor ? 'CreationCanevas1EditeurInit.epm' : 'CreationCanevas1Init.epm'}?accesModeSaaS=yes&identifiantSaaS=${identifiant}`;
  }

}

