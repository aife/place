import {Component, Inject} from '@angular/core';
import {BaseRequestableComponent} from '../../../shared';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-search-canevas',
  templateUrl: './search-canevas.component.html',
  styleUrls: ['./search-canevas.component.sass']
})
export class SearchCanevasComponent extends BaseRequestableComponent {

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);
  }

  protected getUrl(identifiant: string): string {
    return `/listCanevas.htm?editeur=${this.editor ? 'yes': 'no'}&accesModeSaaS=yes&identifiantSaaS=${identifiant}`;
  }

}
