import {Component, Inject} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseRequestableComponent} from '../../../shared';
import {HttpClient} from '@angular/common/http';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-export-clause',
  templateUrl: './export-clause.component.html',
  styleUrls: ['./export-clause.component.sass']
})
export class ExportClauseComponent extends BaseRequestableComponent {

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);
  }

  protected getUrl(identifiant: string): string {
    return `/exporterClause.htm?editeur=${this.editor}&identifiantSaaS=${identifiant}`;
  }
}
