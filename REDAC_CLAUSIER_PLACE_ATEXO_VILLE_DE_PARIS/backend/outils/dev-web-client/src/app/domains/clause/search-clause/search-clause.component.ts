import {Component, Inject} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {BaseRequestableComponent} from '../../../shared';
import {HttpClient} from '@angular/common/http';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-search-clause',
  templateUrl: './search-clause.component.html',
  styleUrls: ['./search-clause.component.sass']
})
export class SearchClauseComponent extends BaseRequestableComponent {

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);
  }

  protected getUrl(identifiant: string): string {
    return `/listClauses.htm?editeur=${this.editor}&accesModeSaaS=yes&identifiantSaaS=${identifiant}`;
  }
}
