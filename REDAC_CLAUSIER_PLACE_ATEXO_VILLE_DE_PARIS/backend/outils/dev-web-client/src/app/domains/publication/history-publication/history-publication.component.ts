import {Component, Inject} from '@angular/core';
import {BaseRequestableComponent} from '../../../shared';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {EnvironmentService, ToastService} from '../../../services';
import {APP_BASE_HREF} from '@angular/common';

@Component({
  selector: 'app-history-publication',
  templateUrl: './history-publication.component.html',
  styleUrls: ['./history-publication.component.sass']
})
export class HistoryPublicationComponent extends BaseRequestableComponent {

  constructor(
    http: HttpClient,
    sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(http, sanitizer, toastService, environmentService, activatedRoute, router, baseHref);
  }

  protected getUrl(identifiant: string): string {
    return `/listPublicationsClausier.htm?initialisationSession=true&identifiantSaaS=${identifiant}`;
  }
}
