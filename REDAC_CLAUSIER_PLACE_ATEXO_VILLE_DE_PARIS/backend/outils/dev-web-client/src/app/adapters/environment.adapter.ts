import { Injectable } from '@angular/core';
import { Environment } from '../model';
import { Environment as Json } from './json';
import { Adapter } from './adapter';
import { TargetAdapter } from './target.adapter';
import { AccountAdapter } from './account.adapter';

@Injectable()
export class EnvironmentAdapter implements Adapter<Json, Environment> {
    constructor(
        private accountAdapter: AccountAdapter,
        private targetAdapter: TargetAdapter
    ) {
    }

    public convert( data: Json ): Environment {
        return new Environment(
          data.id,
          this.targetAdapter.convert(data),
          this.accountAdapter.convert(data.account),
          data.icon,
        );
    }
}
