export interface Account {
  host : string;
  login : string;
  password : string;
  firstname : string;
  lastname : string;
  email: string;
  mpeUrl: string;
  plateformUuid: string;
  organization : string;
  encrypted: boolean;
  salt : string;
  service: string;
  procedure: string;
}
