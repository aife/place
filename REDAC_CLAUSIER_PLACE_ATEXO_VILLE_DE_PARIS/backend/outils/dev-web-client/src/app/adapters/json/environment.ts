import { Account } from './account';

export interface Environment {
  id : string;
  target : string,
  contextPath: string,
  account : Account;
  context : string;
  icon : string;
}
