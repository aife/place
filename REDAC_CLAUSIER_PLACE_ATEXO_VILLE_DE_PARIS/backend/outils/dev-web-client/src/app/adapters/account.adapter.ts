import { Injectable } from '@angular/core';
import { Account } from '../model';
import { Account as Json } from './json';
import { Adapter } from './adapter';

@Injectable()
export class AccountAdapter implements Adapter<Json, Account> {

    public convert( data: Json ): Account {
        return new Account(
          data.host,
          data.login,
          data.password,
          data.firstname,
          data.lastname,
          data.email,
          data.mpeUrl,
          data.plateformUuid,
          data.organization,
          data.encrypted,
          data.salt,
          data.service,
          data.procedure
        );
    }
}
