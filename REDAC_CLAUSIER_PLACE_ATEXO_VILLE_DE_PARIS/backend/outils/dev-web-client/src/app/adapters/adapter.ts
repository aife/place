/**
 * Adaptateur
 *
 * @author <a href='hi@mikaelchobert.io'>Mikaël Chobert</a>
 */
export interface Adapter<U, T> {

    /**
     * Permet de convertir de U vers T
     * @param data les données
     */
    convert( data: U ): T;
}
