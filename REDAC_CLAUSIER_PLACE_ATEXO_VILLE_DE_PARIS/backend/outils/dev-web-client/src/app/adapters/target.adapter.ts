import { Injectable } from '@angular/core';
import { Target } from '../model';
import { Environment as Json } from './json';
import { Adapter } from './adapter';

@Injectable()
export class TargetAdapter implements Adapter<Json, Target> {

    public convert( data: Json ): Target {
        let url = new URL(data.target);

        let host = url.host;

        if (host.match('.*:(\\d*)')) {
          host = host.replace(`:${url.port}`,'');
        }

        return new Target(
          url.protocol,
          host,
          url.port,
          data.contextPath
        );
    }
}
