import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchClauseComponent} from './domains/clause/search-clause/search-clause.component';
import {CreateClauseComponent} from './domains/clause/create-clause/create-clause.component';
import {HttpClientModule} from '@angular/common/http';
import {ExportClauseComponent} from './domains/clause/export-clause/export-clause.component';
import {ViewDocumentComponent} from './domains/document/view-document/view-document.component';
import {CreateCanevasComponent} from './domains/canevas/create-canevas/create-canevas.component';
import {SearchCanevasComponent} from './domains/canevas/search-canevas/search-canevas.component';
import {FormsModule} from '@angular/forms';
import {HIGHLIGHT_OPTIONS, HighlightModule} from 'ngx-highlightjs';
import {InitContextComponent} from './domains/authentication/init-context/init-context.component';
import {SafePipe, ToastsContainer} from './shared';
import {UpdateTemplateComponent} from './domains/template/update-template/update-template.component';
import {CreatePublicationComponent} from './domains/publication/create-publication/create-publication.component';
import {HistoryPublicationComponent} from './domains/publication/history-publication/history-publication.component';
import {SettingsClauseComponent} from './domains/clause/settings-clause/settings-clause.component';
import {SettingsTemplateComponent} from './domains/template/settings-template/settings-template.component';
import {EnvironmentService, PayloadService, ToastService} from './services';
import {EnvironmentLocalService} from './services/remote/environment-local.service';
import {PayloadLocalService} from './services/remote/payload-local.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastLocalService} from './services/remote/toast-local.service';
import {AccountAdapter, EnvironmentAdapter, TargetAdapter} from './adapters';
import {CookieService} from 'ngx-cookie-service';
import {
  ChampFusionComplexeComponent
} from './domains/administration/champ-fusion-complexe/champ-fusion-complexe.component';
import {DocumentModeleComponent} from './domains/administration/document-modele/document-modele.component';
import {APP_BASE_HREF, PlatformLocation} from '@angular/common';
export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SearchClauseComponent,
    CreateClauseComponent,
    ExportClauseComponent,
    ViewDocumentComponent,
    CreateCanevasComponent,
    SearchCanevasComponent,
    InitContextComponent,
    ChampFusionComplexeComponent,
    DocumentModeleComponent,
    SafePipe,
    UpdateTemplateComponent,
    CreatePublicationComponent,
    HistoryPublicationComponent,
    SettingsClauseComponent,
    SettingsTemplateComponent,
    ToastsContainer
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HighlightModule,
    NgbModule,
  ],
  providers: [
    CookieService,
    EnvironmentAdapter,
    TargetAdapter,
    AccountAdapter,
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseHref,
      deps: [PlatformLocation]
    },
    {provide: ToastService, useClass: ToastLocalService},
    {provide: EnvironmentService, useClass: EnvironmentLocalService},
    {provide: PayloadService, useClass: PayloadLocalService},
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js'),
        lineNumbersLoader: () => import('highlightjs-line-numbers.js'), // Optional, only if you want the line numbers
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript'),
          css: () => import('highlight.js/lib/languages/css'),
          xml: () => import('highlight.js/lib/languages/xml')
        }
      }
    }],
  exports: [
    SafePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
