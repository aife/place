import {ActivatedRoute, Router} from '@angular/router';
import {Parser} from 'xml2js';
import {Directive, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {Account, Environment, State} from '../model';
import {EnvironmentService, ToastService} from '../services';
import {from, Observable} from 'rxjs';

/**
 * Base component
 */
@Directive()
export abstract class BaseComponent implements OnInit {

  protected options = {
    headers: new HttpHeaders({ 'Content-Type': 'application/xml' })
      .set('Accept', 'text/xml,application/xhtml+xml,application/xml,application/vnd.sun.wadl+xml'),
    responseType: 'text' as 'text'
  };

  account: Account = new Account('noyau/rest/', 'adminORME', null, 'Pierre', 'DURANT', 'pierre.durant@atexo.com', 'https://mpe-release.local-trust.com', 'MPE_atexo_release', 'pmi-min-1', false, null, null, null);

  public environment: Environment = new Environment('release', null, this.account, null);
  ticket: string;
  identifiant: string;
  editor: boolean;
  preferences: string;
  state: State = State.RELEASED;

  parser: Parser;

  @Output()
  contextEvent: EventEmitter<string> = new EventEmitter<string>();

  protected constructor(
    protected toastService: ToastService,
    protected environmentService: EnvironmentService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router) {
    this.getContext();

    this.parser = new Parser(
      {
        trim: true,
        explicitArray: true
      });

  }

  public connecting(): boolean {
    return this.state === 0;
  }

  public progress(): void {
    this.state = State.WIP;

    console.log(`Connection loading (state = ${this.state})`);
  }

  public connect(): void {
    this.state = State.CONNECTED;

    console.log(`Connection established (state = ${this.state})`);
  }

  public disconnect(): void {
    this.state = State.RELEASED;

    console.log(`Connection released (state = ${this.state})`);
  }

  public logout(): Observable<boolean> {
    this.toastService.clear();
    this.identifiant = null;
    this.ticket = null;

    return from(this.router.navigate(['dashboard']));
  }

  public isLogin(): boolean {
    return this.ticket != null;
  }

  ngOnInit(): void {
    window.addEventListener('message', (event) => {
      if ('REDAC' === event.data.application) {
        console.log(`Reception d'un nouvel évènement REDAC = ${JSON.stringify(event.data)}`);

        this.toastService.removeAll(toast=>'ℹ️ Contexte' == toast.header)

        this.toastService.show(JSON.stringify(event.data, null, 2)
            .replace(/\n/g, '<br/>').replace(/\s/g, '&nbsp;')
          , { delay: 1500000 }, 'ℹ️ Contexte');
      }
    }, false);

    this.activatedRoute.queryParams.subscribe( params => {
        if (params['ticket']) {
          this.ticket = params['ticket'];
        }

        if (!( this.ticket || this.router.url in ['/', '/dashboard'])) {
          this.toastService.clear();

          if (this.state == State.WIP) {
            this.toastService.show('Connection en cours, merci de patienter svp', {}, '⌛ Connection en cours');
          } else {
            this.toastService.show('Ticket non fourni, merci de vous connecter avant', { classname: 'bg-danger text-light' }, 'Pas de connection');
          }

          this.router.navigate(['/dashboard']).then(
            _ => console.log('Redirect to dashboard')
          );
        } else {
          if (params['editor']) {
            this.editor = params['editor'];
          }

          if (params['preferences']) {
            this.preferences = params['preferences'];
          }

          if (params['proxy']) {
            console.log(`Proxy from query param = ${params['proxy']}`);

            this.environmentService.find((env:Environment) => env.target.host == params['proxy']).subscribe(
              env => {
                console.log(`Environment found = ${JSON.stringify(env)}`);
                this.environment = env;
                this.saveContext();
              }
            )
          }
          this.ready();

      }
    } );
  }

  protected abstract ready(): void;

  saveContext() {
    sessionStorage.setItem('environment', JSON.stringify(this.environment));
  }

  getContext() {
    if (sessionStorage.getItem('environment') != null) {
      this.environment = JSON.parse(sessionStorage.getItem('environment')) as Environment;
    }
  }

}
