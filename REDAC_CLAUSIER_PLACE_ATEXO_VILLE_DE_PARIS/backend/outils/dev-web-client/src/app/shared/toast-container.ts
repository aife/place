import {Component, TemplateRef} from '@angular/core';
import { ToastService } from '../services';



@Component({
  selector: 'app-toasts',
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.list()"
      [header]="toast.header"
      [class]="toast.classname"
      [autohide]="true"
      [delay]="toast.delay || 5000"
      [dismissible]="toast.dismissible || true"
      (hide)="toastService.remove(toast)"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="toast.content"></ng-template>
      </ng-template>

      <ng-template #text><strong [innerHTML]="toast.content"></strong></ng-template>
    </ngb-toast>
  `,
  host: {'[class.ngb-toasts]': 'true'}
})
export class ToastsContainer {
  constructor(public toastService: ToastService) {}

  isTemplate(toast) { return toast.content instanceof TemplateRef; }
}
