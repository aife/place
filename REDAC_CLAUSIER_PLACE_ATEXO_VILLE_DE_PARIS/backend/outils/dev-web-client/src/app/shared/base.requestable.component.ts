import {Directive, Inject} from '@angular/core';
import {BaseComponent} from './base.component';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {map, mergeMap, tap} from 'rxjs/operators';
import {EnvironmentService, ToastService} from '../services';
import {HttpClient} from '@angular/common/http';
import {APP_BASE_HREF} from '@angular/common';
import {DomSanitizer} from '@angular/platform-browser';

/**
 * Base requestable component
 */
@Directive()
export abstract class BaseRequestableComponent extends BaseComponent {

  endpoint: Observable<string>;

  protected abstract getUrl(identifiant: string): string

  protected constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    toastService: ToastService,
    environmentService: EnvironmentService,
    activatedRoute: ActivatedRoute,
    router: Router,
    @Inject(APP_BASE_HREF) public baseHref: string
  ) {
    super(toastService, environmentService, activatedRoute, router);
  }

  protected getUri(): Observable<string> {
    let uri = `/${this.environment.account.host}/consultation/initialisation?ticket=${this.ticket}`;
    let payload = window.history.state.data;

    return this.http.put( uri, payload, this.options)
      .pipe( tap(response => console.log(`Response for url '${uri}' = ${response}`)) )
      .pipe( mergeMap( response => this.parser.parseStringPromise(response)) )
      .pipe( map(xml => xml.identifiant ))
      .pipe( tap(identifiant => console.log( `Successfully retrieve new identifiant = ${identifiant}` )))
      .pipe( tap(identifiant => this.contextEvent.emit( identifiant )))
      .pipe( tap(identifiant => {

        this.toastService.removeAll(toast => toast.header == 'ℹ️ Identifiant')

        this.toastService.show(`${identifiant}`, { delay: 1500000 }, 'ℹ️ Identifiant');
      }))
      .pipe( map(identifiant => this.getUrl(identifiant)) )
      .pipe( tap(identifiant => console.log( `URL to display in frame = ${identifiant}` )));
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected ready(): void {
    console.log(`BaseHref is = ${this.baseHref}`);
    if (this.baseHref === '/') {
      let uri = `${this.environment?.target?.protocol}//${this.environment?.target?.host}${this.environment?.target?.port ? ':' + this.environment?.target?.port : ''}`;
      console.log(`URI is = ${uri}`);
     this.getUri()
        .pipe(map(path => `${uri}${this.environment?.target?.path}${path}`))
        .pipe(tap(uri => console.log(`Return URL is = ${uri}`))).subscribe(value => {
       window.open( value, '_blank');
     });
    } else {
      this.getUri()
        .pipe(tap(uri => console.log(`Return URL is = ${uri}`))).subscribe(value => {
        window.open('http://localhost:8080/clausier' + value, '_blank');
      });
    }
  }
}
