let data = require('fs').readFileSync('src/assets/fixtures/environments.json', 'utf8');

console.log('Environments loaded are = ' + data);

const PROXY_CONFIG = JSON.parse(data);

module.exports = PROXY_CONFIG;
