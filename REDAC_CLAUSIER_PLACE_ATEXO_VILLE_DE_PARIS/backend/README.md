# ![Logo](.assets/logo.png "Logo") Atexo Redaction
Application de rédaction de documents

[![Build Status](https://gitlab.local-trust.com/module/lt_redac/badges/master/pipeline.svg)](https://gitlab.local-trust.com/module/lt_redac/badges/master/pipeline.svg)
[![java Version](https://img.shields.io/badge/Java%20Development%20Kit-11-blue.svg?style=flat)](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
[![maven](https://img.shields.io/badge/Maven-3.6.0%2B-blue.svg?style=flat)](https://maven.apache.org) 

![app snapshot](.assets/snapshot.png "app snapshot")

## Stack technique

|Logo                                                    |Framework  |
|--------------------------------------------------------|-----------|
|![Maven](.assets/maven.jpg "Maven")                     |[Maven]    |
|![Spring](.assets/spring.png "Spring")                  |[Spring]   |
|![Tomcat](.assets/tomcat.png "Tomcat")                  |[Tomcat]   |
|![Java](.assets/java.png "Java")                        |[Java 8]   |
|![Hibernate](.assets/hibernate.png "Hibernate")         |[hibernate]|

## Exigences minimales

- Java 11 (ou supérieur)

Ce projet necessite Tomcat (testé avec succès sur la version 9.0.38), maven (3.6.1), node (14.8.0), et npm (6.14.8).

*Toutes ces dépendences peuvent être téléchargées/configurées/installées automatiquement avec le wrapper maven embarqué.*

## Intégration continue

![Gitlab](.assets/gitlab.png "Gitlab") [gitlab]

![Sonarqube](.assets/sonarqube.png "Sonarqube") [sonarqube] 

## Architecture

> *TL; DR;* 
>
> Ce projet contient 3 applications distinctes : 
> - La partie **frontend**
>    - L'application [stub](./outils/dev-web-client), une application [angular], qui s'exécute sous [node], packagé avec [npm](https://www.npmjs.com/) et qui permet de simuler l'application hôte réelle (MPE)
> - La partie **backend**, une application par domaine :
>    - L'application [core](./domaines/noyau): les services noyau, non publics. Une application java packagée sous forme de war exécuté sous serveur [tomcat](./configuration/tomcat).
>    - L'application [writer](./domaines/clausier): les services de redaction de document, publics. Une application java packagée sous forme de war exécuté sous serveur [tomcat server](./configuration/tomcat).

Liste des modules : 

|Module                                                                                                                                            |Description                                                                 |
|--------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
|[outils](./outils)                                                                                                                                |contient les outils de développment                                         |
|[outils/dev-web-client](./outils/dev-web-client)                                                                                                  |contient l'appplication simulée utilisant REDAC en mode embarqué            |
|[configuration](./configuration)                                                                                                                  |contient la configuration des serveurs d'application                        |
|[configuration/tomcat](./configuration/tomcat)                                                                                                    |contient la configuration du serveur d'application backend (tomcat)         |
|[modele](./modele)                                                                                                                                |contient le modèle métier partagé                                           |
|[domaines](./domaines)                                                                                                                            |contient les domaines                                                       |
|[domaines/noyau](./domaines/noyau)                                                                                                                |contient le domaine noyau                                                   |
|[domaines/noyau/noyau-connecteur](./domaines/noyau/noyau-connecteurs)                                                                             |contient les connecteurs du domaine noyau                                   |
|[domaines/noyau/noyau-serveur](./domaines/noyau/noyau-www)                                                                                        |contient les elements server du domaine noyau                               |
|[domaines/noyau/noyau-webapp](./domaines/noyau/noyau-webapp)                                                                                      |contient l'application web (war) du domaine noyau webapp                    |
|[domaines/noyau/noyau-webservices](./domaines/noyau/noyau-webservices)                                                                            |contient les composants liés aux webservices du domaine core                |
|[domaines/noyau/noyau-fournisseurs](./domaines/noyau/noyau-fournisseurs)                                                                          |contient les interfaces du domaine core pour les fournisseurs de contenu    |
|[domaines/noyau/noyau-fournisseurs/noyau-fournisseurs-mpe](./domaines/noyau/noyau-fournisseurs/noyau-fournisseurs-mpe)                            |contient les classes/services pour le fournisseur MPE                       |
|[domaines/clausier](./domaines/clausier)                                                                                                          |contient le domaine clausier                                                |
|[domaines/clausier/clausier.webapp](./domaines/clausier/clausier-webapp)                                                                          |contient la webapp (war) du domaine clausier                                |
|[domaines/clausier/clausier.gwt](./domaines/clausier/clausier-gwt)                                                                                |contient les composants GWT du domaines clausier                            |
|[domaines/clausier/clausier.lib](./domaines/clausier/clausier-commun)                                                                             |contient les librairies partagées                                           |
|[domaines/clausier/clausier.www](./domaines/clausier/clausier-www)                                                                                |contient les composants partagés pour les applications web                  |

## Verification
Si vous voulez vérifier que le build de l'application est OK ✅, vous pouvez utiliser la commande suivante:

```shell
./mvnw clean verify
```

## Installation
Construire/build les artefacts - jar, war, pom - (et les déposer dans votre dépôt local)
```shell
./mvnw clean install -s .mvn/wrapper/settings.xml 
```

## Mise en place
⚠️ Cette application doit être [installée](#installation) au préalable 

Après l'installation
- un [tomcat embarqué] est disponible [dans le répertoire de téléchargement](./configuration/tomcat/target/downloads). Ce tomcat est prêt à l'emploi (déjà configuré).
- node et npm sont disponibles dans le [répertoire target](./outils/dev-web-client/target/node) du module [stub](./outils/dev-web-client)

Alternativement, vous pouvez aussi utilisez votre propre tomcat (système ou téléchargé), copiez simplement les fichiers de [ce répertoire](./configuration/tomcat/src/main/resources) directement dans le répertoire d'installation de votre tomcat.
 
## Exécution

### Backend
⚠️ Cette application doit être [installée](#installation) au préalable 

#### Tomcat système ou téléchargé manuellement
- copiez les wars
- démarrer votre tomcat

#### Tomcat embarqué
Utilisez simplement le [tomcat embarqué]

- copie des wars
``` shell 
cp ./domaines/noyau/noyau-webapp/target/epm.noyau.war ./configuration/tomcat/target/downloads/apache-tomcat-9.0.38/webapps/ 
cp ./domaines/clausier/clausier-webapp/target/epm.redaction.war ./configuration/tomcat/target/downloads/apache-tomcat-9.0.38/webapps/  
```

- démarrage
``` shell 
./configuration/tomcat/target/downloads/apache-tomcat-9.0.38/bin/./catalina.sh run
```

- si besoin, observation des logs
``` shell 
tail -f ./configuration/tomcat/target/downloads/apache-tomcat-9.0.38/logs/catalina.out  
```

- arrêt du tomcat

kill (`CTRL+C`) la commande ou exécutez:
```shell 
./configuration/tomcat/downloads/apache-tomcat-9.0.38/bin/./catalina.sh stop 
```
### Frontend
Voir le fichier [readme du module stub](./outils/dev-web-client/README.md) pour plus de détails

- Run
TL; DR; Executez le avec la commande suivante (depuis la racine, ou adaptez la):
```shell
./outils/dev-web-client/target/node/./npm --prefix=./outils/dev-web-client run local  
```

- Test
```shell
./outils/dev-web-client/target/node/./npm --prefix=./outils/dev-web-client test 
```

## Configuration

### Upgrade de la version maven
```shell
./mvnw -N io.takari:maven:0.7.7:wrapper -Dmaven=3.5.4
```

## License
Voir le fichier [license] pour plus de détails

## Contributions
Voir le fichier  [contributing] pour plus de détails

[license]: ./LICENSE.md
[contributing]: ./CONTRIBUTING.md
[angular]: https://angular.io/
[tomcat embarqué]: ./configuration/tomcat/target/downloads
[node]: https://nodejs.org/en/
[maven]: https://maven.apache.org/
[spring]: https://spring.io/
[tomcat]: http://tomcat.apache.org/
[java 8]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[hibernate]: http://hibernate.org/
[jenkins]: https://jenkins-ovh.local-trust.com/blue/organizations/jenkins/redaction/
[sonarqube]: http://172.16.1.15/sonarqube/dashboard?id=com.atexo.redaction.root
