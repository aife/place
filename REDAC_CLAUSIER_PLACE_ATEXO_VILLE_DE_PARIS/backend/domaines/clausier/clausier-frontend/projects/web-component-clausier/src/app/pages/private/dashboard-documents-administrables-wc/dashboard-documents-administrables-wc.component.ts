import {Component, OnInit} from '@angular/core';
import {BreadcrumbParamsConst} from '@core-clausier/constantes/breadcrumb-params.const';
import {HabilitationsEnum} from '@shared-global/core/enums/habilitations.enum';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {
  ModalAjoutDocumentAdministrableComponent
} from '@shared-global/components/modal-ajout-document-administrable/modal-ajout-document-administrable.component';
import {Agent, DocumentAdministrable, Produit} from '@shared-global/core/models/api/clausier-core.api';
import {DocumentService} from '@shared-global/core/services/document.service';
import {Store} from '@ngrx/store';
import {State} from '@shared-global/store';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import moment from 'moment/moment';
import {ModalConfirmationComponent} from '@shared-global/components/modal-confirmation/modal-confirmation.component';
import {showSuccessToast} from '@shared-global/store/toast/toast.action';

@Component({
  selector: 'app-dashboard-documents-administrables-wc',
  templateUrl: './dashboard-documents-administrables-wc.component.html',
  styleUrls: ['./dashboard-documents-administrables-wc.component.css'],
})
export class DashboardDocumentsAdministrablesWcComponent implements OnInit {
  param: any;
  agent: Agent;
  //loading = false;
  url: string;
  produits: Array<Produit> = [];
  nomsProduits: string[] = [];
  produitSelected: Produit;
  loadingEdit = false;
  loadingDesactivation = false;
  loadingTelechargement = false;
  loadingReinialiser = false;
  documents: Array<DocumentAdministrable> = []
  documentSelected: DocumentAdministrable;
  administrationRoles: string[] = [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR];
  bsModalRef!: BsModalRef;

  constructor(private readonly store: Store<State>, protected modalService: BsModalService, private documentService: DocumentService) {
  }

  ngOnInit(): void {
    this.param = BreadcrumbParamsConst.DOCUMENT_ADMINISTRABLE;
    this.store.select(state => state.userReducer.user)
      .subscribe(value => {
        this.agent = value;
        if (!!this.agent) {
          this.produits = this.agent.produits;
          this.produits?.forEach(produit => {
            this.nomsProduits.push(produit.nom)
          })
          this.getDocuments();
          setInterval(() => {
            this.getDocuments();
          }, 10000);
        }
      });

  }

  private getDocuments() {
    this.documentService.listDocumentsAdministrables(false, this.nomsProduits).subscribe(value => {
      this.documents = value;
      if (this.documentSelected) {
        this.documentSelected = value.find(item => item.code === this.documentSelected.code);
      }
    });
  }

  loadingExport: any;


  editer() {
    if (!!this.documentSelected) {
      this.produits.forEach(produit => {
        if (produit.nom.toLowerCase() === this.documentSelected.produit.toLowerCase()) {
          this.produitSelected = produit;
        }
      })
      if (!!this.produitSelected) {
        this.loadingEdit = true;
        this.documentService.editDocument(this.documentSelected.id, this.produitSelected).subscribe(value => {
          this.url = value.url;
          window.open(this.url, '_blank');
          this.getDocuments();

          this.loadingEdit = false;
        }, () => this.loadingEdit = false)
      }
    }

  }

  public grouper(document: DocumentAdministrable): string {
    return document?.produit;
  }


  openAjoutDocumentModal() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let i18n = '';
    this.bsModalRef = this.modalService.show(ModalAjoutDocumentAdministrableComponent, config);
    this.bsModalRef.content.titre = 'doc-administrable.add-modal';
    this.bsModalRef.content.isChampFusion = false;
    this.bsModalRef.content.products = this.nomsProduits;
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.documentService.listDocumentsAdministrables(false, this.nomsProduits).subscribe(value => {
            this.documents = value;
          })
        }
        this.bsModalRef = null;
      }
    );
  }

  desactiver() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'doc-administrable.delete-modal';
    this.bsModalRef.content.messages = ['doc-administrable.confirmation.delete'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.loadingDesactivation = true;
          this.documentService.disableDocument(this.documentSelected.id).subscribe(value1 => {
            this.documentSelected = null;
            this.loadingDesactivation = false;
            this.store.dispatch(showSuccessToast({
              header: 'Information',
              message: 'doc-administrable.confirmation.success.supprimer'
            }));
            this.documentService.listDocumentsAdministrables(false, this.nomsProduits).subscribe(value2 => {
              this.documents = value2;
            })
          },
            () => this.loadingDesactivation = false)
        }
        this.bsModalRef = null;
      }
    );
  }

  telecharger() {
    this.loadingTelechargement = true;
    this.documentService.downloadTemplate(this.documentSelected.id).subscribe(file => {
      this.loadingTelechargement = false;
      if (!!this.documentSelected.extension) {
        if (this.documentSelected.extension === "xlsx") {
          saveFile(file, "Template-" + this.documentSelected.produit + "-" + this.documentSelected.code + "-" + moment(new Date()).format("YYYYMMDD") + '.xlsx');
        } else {
          saveFile(file, "Template-" + this.documentSelected.produit + "-" + this.documentSelected.code + "-" + moment(new Date()).format("YYYYMMDD") + '.docx');
        }
      } else {
        console.log("Problème : L'extension du fichier est null");

      }

    }, () => this.loadingTelechargement = false)
  }

  getBadge(item: DocumentAdministrable) {
    if (!!item.plateformeUuid) {
      if (!!item.documentParentId) {
        return "ATEXO SURCHARGE"
      } else {
        return "PLATEFORME"
      }
    } else {
      return "ATEXO"
    }
  }

  getColor(item: any) {
    if (!!item.plateformeUuid) {
      if (!!item.documentParentId) {
        return 'badge-bleu'
      } else {
        return 'badge-rose'
      }
    } else {
      return 'badge-violet'
    }
  }

  searchByCode(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item?.code?.toLocaleLowerCase().indexOf(term) > -1 || item?.libelle?.toLocaleLowerCase().indexOf(term) > -1;

  }

  reinitialiser() {

    this.loadingReinialiser = true;
    this.documentService.reinitialiserDocument(this.documentSelected.id).subscribe(value1 => {
        this.loadingReinialiser = false;
        this.store.dispatch(showSuccessToast({
          header: 'Information',
          message: 'doc-administrable.confirmation.success.reinitialisation'
        }));
        this.documentService.listDocumentsAdministrables(false, this.nomsProduits).subscribe(value2 => {
          this.documents = value2;
          this.documentSelected = value2.find(item => item.id === value1.id);
        });
      },
      () => this.loadingReinialiser = false);

  }

  isOpen() {
    if (!this.documentSelected) {
      return false;
    }
    if (!this.documentSelected.statutEdition) {
      return false;
    }
    return this.documentSelected.statutEdition !== 'CLOSED_WITHOUT_EDITING' && this.documentSelected.statutEdition !== 'SAVED_AND_CLOSED';

  }
}
