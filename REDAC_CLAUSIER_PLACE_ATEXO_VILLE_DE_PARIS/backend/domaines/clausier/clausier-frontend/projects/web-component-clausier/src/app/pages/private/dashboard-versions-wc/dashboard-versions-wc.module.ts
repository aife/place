import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {NgxLoadingModule} from "ngx-loading";
import {ClrCommonFormsModule} from "@clr/angular";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {UiSwitchModule} from "ngx-ui-switch";
import {ComponentsModule} from "@shared-global/components/components.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {DashboardVersionsWcComponent} from "@pages-wc/private/dashboard-versions-wc/dashboard-versions-wc.component";
import {
  VersionsDetailsComponent
} from "@pages-wc/private/dashboard-versions-wc/components/version-details/versions-details.component";
import {
  VersionsFilterComponent
} from "@pages-wc/private/dashboard-versions-wc/components/versions-filter/versions-filter.component";
import {DashboardClausesWcModule} from "@pages-wc/private/dashboard-clauses-wc/dashboard-clauses-wc.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";

@NgModule({
  declarations: [VersionsDetailsComponent, DashboardVersionsWcComponent, VersionsFilterComponent],
    imports: [
        CommonModule,
        LayoutModule,
        RouterModule,
        NgbPaginationModule,
        TranslateModule,
        ChartsModule,
        FormsModule,
        NgbAlertModule,
        BsDatepickerModule,
        NgSelectModule,
        NgxLoadingModule,
        ClrCommonFormsModule,
        CommonModule,
        UiSwitchModule,
        ReactiveFormsModule,
        ComponentsModule,
        DirectivesModule,
        DashboardClausesWcModule,
        PipesModule,
        CommonModule,
    ], exports: [DashboardVersionsWcComponent],
  providers: [BsModalService, CanevasService]
})
export class DashboardVersionsWcModule {
}
