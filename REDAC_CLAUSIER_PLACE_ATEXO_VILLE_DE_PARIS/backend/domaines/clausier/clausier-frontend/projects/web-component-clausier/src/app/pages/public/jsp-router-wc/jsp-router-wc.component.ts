import {Component, OnInit} from '@angular/core';
import {RoutingInterneService} from '@shared-global/core/services/routing-interne.service';
import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';
import {Store} from '@ngrx/store';
import {State} from '@shared-global/store';
import {logoutUser} from '@shared-global/store/user/user.action';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'atx-accueil-page-wc',
  templateUrl: './jsp-router-wc.component.html',
  styleUrls: ['./jsp-router-wc.component.css']
})
export class JspRouterWcComponent implements OnInit {
  loading = true;

  constructor(
    private route: ActivatedRoute,
    private routingInterne: RoutingInterneService,
    private store: Store<State>) {
  }

  ngOnInit(): void {
    this.start();
  }

  private async start() {
    await this.store.dispatch(logoutUser());
    let pathname = window.location.pathname.replace(this.routingInterne.baseHref, '');
    this.route.queryParamMap.subscribe(item => {
      let editeur = item?.get('editeur');
      let preferences = item?.get('preferences');
      let identifiantSaaS = item?.get('identifiantSaaS');
      console.log(item, editeur, pathname, identifiantSaaS, preferences)
      let path = null;
      switch (pathname) {
        case  AppInternalPathEnum.DASHBOARD_CANEVAS_LEGACY:
        case  AppInternalPathEnum.DASHBOARD_CANEVAS_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS;
          if (editeur === 'yes' || editeur === 'true' || editeur === 'oui')
            path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS;
          break;

        case  AppInternalPathEnum.DASHBOARD_CLAUSES_LEGACY:
        case  AppInternalPathEnum.DASHBOARD_CLAUSES_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE;
          if (editeur === 'yes' || editeur === 'true' || editeur === 'oui')
            path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE;
          break;

        case  AppInternalPathEnum.CREATION_CANEVAS_EDITEUR_LEGACY:
        case  AppInternalPathEnum.CREATION_CANEVAS_EDITEUR_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CANEVAS.replace(":type", 'editeur');
          break;
        case  AppInternalPathEnum.CREATION_CANEVAS_CLIENT_LEGACY:
        case  AppInternalPathEnum.CREATION_CANEVAS_CLIENT_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CANEVAS.replace(":type", 'client');
          break;
        case  AppInternalPathEnum.CREATION_CLAUSE_EDITEUR_LEGACY:
        case  AppInternalPathEnum.CREATION_CLAUSE_EDITEUR_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CLAUSE.replace(":type", 'editeur');
          break;
        case  AppInternalPathEnum.CREATION_CLAUSE_CLIENT_LEGACY:
        case  AppInternalPathEnum.CREATION_CLAUSE_CLIENT_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CLAUSE.replace(":type", 'client');
          break;
        case  AppInternalPathEnum.CREATION_VERSION_LEGACY:
        case  AppInternalPathEnum.CREATION_VERSION_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CREATION_VERSION;
          break;
        case  AppInternalPathEnum.HISTORIQUES_VERSION_LEGACY:
        case  AppInternalPathEnum.HISTORIQUES_VERSION_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_VERSION;
          break;
        case  AppInternalPathEnum.EXPORT_CLAUSES_LEGACY:
        case  AppInternalPathEnum.EXPORT_CLAUSES_LEGACY_EPM:
        case  AppInternalPathEnum.EXPORT_CLAUSE_LEGACY:
        case  AppInternalPathEnum.EXPORT_CLAUSE_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_EXPORT_CLAUSE;
          if (editeur === 'yes' || editeur === 'true' || editeur === 'oui')
            path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_EXPORT_CLAUSE;
          break;

        case  AppInternalPathEnum.PARAMETRAGE_CLAUSE_LEGACY:
        case  AppInternalPathEnum.PARAMETRAGE_CLAUSE_LEGACY_EPM:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_DIRECTION_DASHBOARD_CLAUSE;
          if (preferences === 'agent')
            path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_AGENT_DASHBOARD_CLAUSE;
          break;
        case  AppInternalPathEnum.CHAMPS_FUSION_LEGACY_EPM:
        case  AppInternalPathEnum.CHAMPS_FUSION_LEGACY:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_CHAMP_FUSION_COMPLEXE;
          break;
        case  AppInternalPathEnum.DOCUMENT_MODELE_LEGACY_EPM:
        case  AppInternalPathEnum.DOCUMENT_MODELE_LEGACY:
          path = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_DOCUMENT_ADMINISTRABLE;
          break;
        default:
          console.log('default', pathname)
      }
      this.loading = false;
      this.routingInterne.navigateFromContexte(AppInternalPathEnum.LOGIN, {
        'identifiantSaaS': identifiantSaaS,
        isWebComponent: true,
        redirect: path
      });

    })
  }
}
