import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateLoader, TranslateModule, TranslateStore} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {LoginPageWCComponent} from './login-page-wc/login-page-wc.component';
import {ComponentsModule} from "@shared-global/components/components.module";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {FormsModule} from "@angular/forms";
import {NgxLoadingModule} from "ngx-loading";
import {ClrCommonFormsModule} from "@clr/angular";
import {JspRouterWcComponent} from "@pages-wc/public/jsp-router-wc/jsp-router-wc.component";

export function HttpPublicLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/public-', '.json');
}


@NgModule({
  declarations: [LoginPageWCComponent, JspRouterWcComponent],
  imports: [
    CommonModule, TranslateModule, TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpPublicLoaderFactory),
        deps: [HttpClient]
      }, isolate: true
    }), CommonModule, CommonModule, ComponentsModule, BsDatepickerModule, FormsModule, NgxLoadingModule, ClrCommonFormsModule
  ],
  providers: [TranslateStore],
  exports: [LoginPageWCComponent]

})
export class PublicWcModule {
}
