import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgbDatepickerModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from "@ngx-translate/core";
import {UiSwitchModule} from "ngx-ui-switch";
import {FormCanevasComponent} from "@pages-wc/private/form-canevas/form-canevas.component";
import {
  FormCanevasProprietesComponent
} from "@pages-wc/private/form-canevas/components/form-canevas-proprietes/form-canevas-proprietes.component";
import {ClrCommonFormsModule} from "@clr/angular";
import {
  FormCanevasContenuComponent
} from "@pages-wc/private/form-canevas/components/form-canevas-contenu/form-canevas-contenu.component";
import {ComponentsModule} from "@shared-global/components/components.module";
import {NgxLoadingModule} from "ngx-loading";
import {PipesModule} from "@shared-global/pipes/pipes.module";


@NgModule({
  declarations: [FormCanevasComponent, FormCanevasProprietesComponent, FormCanevasContenuComponent],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        NgSelectModule,
        BsDatepickerModule,
        BsDatepickerModule.forRoot(),
        NgbDatepickerModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule,
        UiSwitchModule,
        ClrCommonFormsModule,
        ComponentsModule,
        NgxLoadingModule,
        PipesModule
    ], exports: [FormCanevasComponent, FormCanevasProprietesComponent, FormCanevasContenuComponent]
})
export class FormCanevasModule {
}
