import {Component, Input, OnInit} from '@angular/core';
import {ClauseService} from "@shared-global/core/services/clause.service";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import moment from "moment/moment";
import {BaseDashboardComponent} from "@shared-global/components/base-dashboard/base-dashboard.component";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseFilterModel} from "@shared-global/core/models/clause-filter.model";
import {WorkerService} from "@shared-global/core/services/worker.service";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";


@Component({
  selector: 'atx-dashboard-clauses',
  templateUrl: './dashboard-clauses-wc.component.html',
  styleUrls: ['./dashboard-clauses-wc.component.css'],

})
export class DashboardClausesWcComponent extends BaseDashboardComponent<ClauseBean, ClauseFilterModel> implements OnInit {


  @Input() filterKey;
  @Input() pageableKey;
  @Input() sortFieldsList = [];
  @Input() param: any;
  @Input() editeur;
  @Input() loadingExport = false;
  @Input() linkCreation: string;
  @Input() administrationRoles: string[] = [];
  @Input() validationRoles: string[] = [];
  private checkWorkerInterval: any;

  constructor(protected readonly store: Store<State>,
              protected readonly clauseService: ClauseService, private workerService: WorkerService) {
    super(store, clauseService)
  }

  ngOnInit(): void {
    super.start(this.filterKey, this.pageableKey)
  }


  protected initFilter() {
    this.filter = new ClauseFilterModel(this.editeur);
    this.pageable.page = 0;
  }


  exporter() {
    this.loadingExport = true;
    this.clauseService.exporter(this.editeur).subscribe(idTask => {
      this.checkWorkerInterval = setInterval(() => {
        this.workerService.findTask(idTask).subscribe(value => {
          if (value.result === 'success' && value.data) {
            this.loadingExport = false;
            clearInterval(this.checkWorkerInterval);
            this.workerService.download(idTask).subscribe(file => {
              saveFile(file, "Export_clauses_" + moment(new Date()).format("YYYYMMDD") + '.xls');
              clearInterval(this.checkWorkerInterval);
            }, () => this.loadingExport = false)
          }
          if (value.result === 'error') {
            clearInterval(this.checkWorkerInterval);
            this.loadingExport = false;
          }
        })
      }, 10000);

    }, () => {
      this.loadingExport = false
      clearInterval(this.checkWorkerInterval);
    })
  }


  updateTotal() {
    super.start(this.filterKey, this.pageableKey);
  }
}


