import {Component, Input, OnInit} from '@angular/core';
import {CanevasBean} from "@shared-global/core/models/api/clausier.api";
import {CanevasFilterModel} from "@shared-global/core/models/canevas-filter.model";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {BaseDashboardComponent} from "@shared-global/components/base-dashboard/base-dashboard.component";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";


@Component({
  selector: 'atx-dashboard-canevas',
  templateUrl: './dashboard-canevas-wc.component.html',
  styleUrls: ['./dashboard-canevas-wc.component.css'],

})
export class DashboardCanevasWcComponent extends BaseDashboardComponent<CanevasBean, CanevasFilterModel> implements OnInit {
  @Input() filterKey;
  @Input() pageableKey;
  @Input() sortFieldsList: string[] = []
  @Input() param: any;
  @Input() editeur;
  @Input() linkCreation: string;
  @Input() administrationRoles: string[] = [];
  @Input() validationRoles: string[] = [];


  constructor(protected readonly store: Store<State>,
              private readonly canevasService: CanevasService) {
    super(store, canevasService);
  }

  ngOnInit(): void {
    super.start(this.filterKey, this.pageableKey)
  }


  protected initFilter() {
    this.filter = new CanevasFilterModel(this.editeur);
    this.pageable.page = 0;
  }

  updateTotal() {
    super.start(this.filterKey, this.pageableKey);
  }
}


