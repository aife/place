import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgbDatepickerModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from "@ngx-translate/core";
import {UiSwitchModule} from "ngx-ui-switch";
import {ClrCommonFormsModule} from "@clr/angular";
import {ComponentsModule} from "@shared-global/components/components.module";
import {FormClauseComponent} from "@pages-wc/private/form-clause/form-clause.component";
import {
  FormClauseProprietesComponent
} from "@pages-wc/private/form-clause/components/form-clause-proprietes/form-clause-proprietes.component";

import {NgxLoadingModule} from "ngx-loading";
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";


@NgModule({
  declarations: [FormClauseComponent, FormClauseProprietesComponent],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        NgSelectModule,
        BsDatepickerModule,
        BsDatepickerModule.forRoot(),
        NgbDatepickerModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule,
        UiSwitchModule,
        ClrCommonFormsModule,
        ComponentsModule,
        NgxLoadingModule,
        BibliotequeCommuneModule,
        DirectivesModule,
        PipesModule
    ], exports: [FormClauseComponent, FormClauseProprietesComponent]
})
export class FormClauseModule {
}
