import {Component} from '@angular/core';
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";


@Component({
  selector: 'app-dashboard-clauses-editeur',
  templateUrl: './dashboard-clauses-editeur-wc.component.html',
  styleUrls: ['./dashboard-clauses-editeur-wc.component.css'],

})
export class DashboardClausesEditeurWcComponent {

  param = BreadcrumbParamsConst.EDITEUR_CLAUSE_DASHBOARD;
  editeur = true;
  linkCreation = AppInternalPathEnum.PRIVATE_PATH + 'editeur' + AppInternalPathEnum.CREATION_CLAUSE;
  administrationRoles = [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR];
  validationRoles: string[] = [HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR];

  sortFieldsList = ['reference', 'clauseEditeur', 'epmTRefStatutRedactionClausier.id', 'epmTRefThemeClause.libelle',
    'dateCreation', 'dateModification', 'idLastPublication']

}


