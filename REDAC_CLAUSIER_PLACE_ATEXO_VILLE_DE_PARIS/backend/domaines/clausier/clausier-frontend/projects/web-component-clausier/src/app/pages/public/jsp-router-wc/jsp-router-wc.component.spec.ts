import {ComponentFixture, TestBed} from '@angular/core/testing';

import {JspRouterWcComponent} from './jsp-router-wc.component';

describe('AccueilPageWCComponent', () => {
  let component: JspRouterWcComponent;
  let fixture: ComponentFixture<JspRouterWcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JspRouterWcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JspRouterWcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
