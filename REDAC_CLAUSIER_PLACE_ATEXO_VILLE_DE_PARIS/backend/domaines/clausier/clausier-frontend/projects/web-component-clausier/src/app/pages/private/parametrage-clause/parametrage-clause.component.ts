import {Component, Input, OnInit} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {ClauseBean} from '@shared-global/core/models/api/clausier.api';
import {RoutingInterneService} from '@shared-global/core/services/routing-interne.service';
import {ClauseService} from '@shared-global/core/services/clause.service';
import {BreadcrumbParamsConst} from '@core-clausier/constantes/breadcrumb-params.const';
import {BaseFormQuillComponent} from '@shared-global/components/base-form-quill/base-form-quill.component';
import {Observable, zip} from 'rxjs';


@Component({
  selector: 'atx-parametrage-clause',
  templateUrl: './parametrage-clause.component.html',
  styleUrls: ['./parametrage-clause.component.css'],

})
export class ParametrageClauseComponent extends BaseFormQuillComponent implements OnInit {

  loading = true;
  surchargeActive = false;
  editeur = false;
  param: any;
  clauseParametree: ClauseBean;
  clauseSurcharge: ClauseBean;
  @Input() isParametrableAgent: boolean = false;
  @Input() isParametrableDirection: boolean = false;

  constructor(protected readonly clauseService: ClauseService,
              protected router: RoutingInterneService,
              protected route: ActivatedRoute) {
    super();

    this.route.params.subscribe(params => {
      if (params.type === 'agent') {
        this.isParametrableAgent = true;
        this.param = BreadcrumbParamsConst.PARAMETRAGE_AGENT_CLAUSE;
      } else if (params.type === 'direction') {
        this.isParametrableDirection = true;
        this.param = BreadcrumbParamsConst.PARAMETRAGE_DIRECTION_CLAUSE;
      }
      if (!!params.id && (this.isParametrableAgent || this.isParametrableDirection)) {
        this.loading = true;
        let observable: Observable<ClauseBean>;
        let clauseParametrable: Observable<ClauseBean>;
        if (this.isParametrableAgent) {
          observable = clauseService.getClauseDirection(params.id, params.idPublication, true);
          clauseParametrable = clauseService.getClauseAgent(params.id, params.idPublication, false);
        } else {
          observable = clauseService.getClauseSurcharge(params.id, params.idPublication);
          clauseParametrable = clauseService.getClauseDirection(params.id, params.idPublication, false);
        }


        zip(observable, clauseParametrable).subscribe(value => {
          this.loading = false;
          this.clauseSurcharge = value[0];
          this.clauseParametree = value[1];
        }, () => this.loading = false)


      }
    });
  }

  ngOnInit(): void {

  }


  back() {
    this.router.navigateFromContexte(this.param.links[0].link)
  }

  surcharge() {
    if (this.isParametrableAgent)
      this.clauseService.surchargeAgent(this.clauseParametree.idClause, this.clauseParametree.idPublication, this.clauseParametree)
        .subscribe(value => {
          this.clauseParametree = value;
          this.back();
        })
    else {
      this.clauseService.surchargeDirection(this.clauseParametree.idClause, this.clauseParametree.idPublication, this.clauseParametree)
        .subscribe(value => {
          this.clauseParametree = value;
          this.back();
        })
    }
  }
  getTexteAvant() {
    switch (this.clauseSurcharge.idTypeClause) {
      case 4:
        if (!!this.clauseSurcharge.clauseTextePrevalorise) {
          return this.clauseSurcharge.clauseTextePrevalorise.textFixeAvant;
        } else {
          return ""
        }
      case 6:
        if (!!this.clauseSurcharge.clauseListeChoixExclusif) {
          return this.clauseSurcharge.clauseListeChoixExclusif.textFixeAvant;
        } else {
          return ""
        }
      case 7:
        if (!!this.clauseSurcharge.clauseListeChoixCumulatif) {
          return this.clauseSurcharge.clauseListeChoixCumulatif.textFixeAvant;
        } else {
          return ""
        }
      default:
        console.log("incohérene de type clause");
    }
  }

  getTexteApres() {
    switch (this.clauseSurcharge.idTypeClause) {
      case 4:
        if (!!this.clauseSurcharge.clauseTextePrevalorise) {
          return this.clauseSurcharge.clauseTextePrevalorise.textFixeApres;
        } else {
          return ""
        }
      case 6:
        if (!!this.clauseSurcharge.clauseListeChoixExclusif) {
          return this.clauseSurcharge.clauseListeChoixExclusif.textFixeApres;
        } else {
          return ""
        }
      case 7:
        if (!!this.clauseSurcharge.clauseListeChoixCumulatif) {
          return this.clauseSurcharge.clauseListeChoixCumulatif.textFixeApres;
        } else {
          return ""
        }

      default:
        console.log("incohérene de type clause");
    }
  }
}


