import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {NgxLoadingModule} from "ngx-loading";
import {ClrCommonFormsModule} from "@clr/angular";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {UiSwitchModule} from "ngx-ui-switch";
import {ComponentsModule} from "@shared-global/components/components.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {
  DashboardDocumentsAdministrablesWcComponent
} from "@pages-wc/private/dashboard-documents-administrables-wc/dashboard-documents-administrables-wc.component";
import {
  DashboardClausesParametreAgentWcModule
} from "@pages-wc/private/dashboard-clauses-parametre-agent-wc/dashboard-clauses-parametre-agent-wc.module";

@NgModule({
  declarations: [DashboardDocumentsAdministrablesWcComponent],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    NgbPaginationModule,
    TranslateModule,
    ChartsModule,
    FormsModule,
    NgbAlertModule,
    BsDatepickerModule,
    NgSelectModule,
    NgxLoadingModule,
    ClrCommonFormsModule,
    CommonModule,
    UiSwitchModule,
    ReactiveFormsModule,
    ComponentsModule,
    DirectivesModule,
    PipesModule,
    CommonModule,
    DashboardClausesParametreAgentWcModule,
  ], exports: [DashboardDocumentsAdministrablesWcComponent],
  providers: [BsModalService]
})
export class DashboardDocumentsAdministrablesWcModule {
}
