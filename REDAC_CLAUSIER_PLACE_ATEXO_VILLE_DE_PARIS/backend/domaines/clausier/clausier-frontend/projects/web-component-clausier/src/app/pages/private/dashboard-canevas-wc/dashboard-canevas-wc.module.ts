import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {
  CanevasFilterComponent
} from '@pages-wc/private/dashboard-canevas-wc/components/canevas-filter/canevas-filter.component';
import {ChartsModule} from 'ng2-charts';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {NgxLoadingModule} from "ngx-loading";
import {ClrCommonFormsModule} from "@clr/angular";
import {
  CanevasDetailsComponent
} from "@pages-wc/private/dashboard-canevas-wc/components/canevas-details/canevas-details.component";
import {DashboardCanevasWcComponent} from "@pages-wc/private/dashboard-canevas-wc/dashboard-canevas-wc.component";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {UiSwitchModule} from "ngx-ui-switch";
import {ComponentsModule} from "@shared-global/components/components.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";

@NgModule({
  declarations: [CanevasDetailsComponent, DashboardCanevasWcComponent, CanevasFilterComponent],
    imports: [
        CommonModule,
        LayoutModule,
        RouterModule,
        NgbPaginationModule,
        TranslateModule,
        ChartsModule,
        FormsModule,
        NgbAlertModule,
        BsDatepickerModule,
        NgSelectModule,
        NgxLoadingModule,
        ClrCommonFormsModule,
        CommonModule,
        UiSwitchModule,
        ReactiveFormsModule,
        ComponentsModule,
        DirectivesModule,
    ], exports: [DashboardCanevasWcComponent],
  providers: [BsModalService, CanevasService]
})
export class DashboardCanevasWcModule {
}
