import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {ClauseSearch, Directory} from "@shared-global/core/models/api/clausier.api";
import moment from 'moment';
import {ClauseFilterModel} from '@shared-global/core/models/clause-filter.model';
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";


@Component({
  selector: 'atx-clauses-filter',
  templateUrl: './clauses-filter.component.html',
  styleUrls: ['./clauses-filter.component.css']
})
export class ClausesFilterComponent implements OnInit {

  @Input() editeur = true;

  @Input() filter: ClauseSearch;
  @Output() filterUsed = new EventEmitter();
  @Output() onChangeFilter = new EventEmitter<ClauseFilterModel>();
  @Output() reinitFilter = new EventEmitter<boolean>();
  refStatut$: Observable<Array<Directory>>;
  refNature$: Observable<Array<Directory>>;
  refProcedure$: Observable<Array<Directory>>;
  refTypeContrat$: Observable<Array<Directory>>;
  refTypeClause$: Observable<Array<Directory>>;
  refTypeDocument$: Observable<Array<Directory>>;
  refThemeClause$: Observable<Array<Directory>>;
  refTypeAuteur$: Observable<Array<Directory>>;
  @Input() showMore = false;
  minDate: Date;
  maxDate: Date;

  constructor(private bsLocaleService: BsLocaleService, private readonly store: Store<State>) {
    this.maxDate = new Date()
    this.minDate = moment(new Date("01/03/2000")).toDate();

  }

  ngOnInit(): void {
    this.refStatut$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.STATUT)?.content);
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content);
    this.refTypeContrat$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CONTRAT)?.content);
    this.refTypeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CLAUSE)?.content);
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.refProcedure$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content);
    this.refThemeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.THEME_CLAUSE)?.content);
    this.refTypeAuteur$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CLAUSE)?.content);
  }


  rechercher() {
    this.onChangeFilter.emit(this.filter);
  }

  changeStatutFilter(id: number) {
    this.filter.idStatutRedactionClausier = id;
  }

  changeNatureFilter(id: number) {
    this.filter.idNaturePrestation = id;
  }


  changeActivationFilter(activation: number) {
    if (activation === 0) {
      this.filter.actif = null;
    } else if (activation === 1) {
      this.filter.actif = true;
    } else if (activation === 2) {
      this.filter.actif = false;
    }
  }

  getStatutIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-edit";
    } else if (id === 2) {
      return "fa fa-hourglass";
    } else if (id === 3) {
      return "fa fa-check-circle";
    }
  }

  getNatureIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }

  reInitFilter() {
    this.filter = new ClauseFilterModel(this.editeur);
    if (this.editeur) {
      sessionStorage.removeItem('filter-dashboard-clause-editeur');
    } else {
      sessionStorage.removeItem('filter-dashboard-clause-client');
    }
    this.reinitFilter.emit(true);
    this.rechercher();
  }


  getBackgroundStatut(id: number) {
    if (id === this.filter.idStatutRedactionClausier) {
      switch (id) {
        case 1:
          return "active-error";
          break;
        case 2:
          return "active-warning";
          break;
        case 3:
          return "active-success";
          break;
      }
    }
  }
}
