import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Canevas, Directory} from "@shared-global/core/models/api/clausier.api";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";

@Component({
  selector: 'atx-form-canevas-proprietes',
  templateUrl: './form-canevas-proprietes.component.html',
  styleUrls: ['./form-canevas-proprietes.component.css']
})
export class FormCanevasProprietesComponent implements OnInit {


  @Input() canevas: Canevas;
  @Input() action: FormActionEnum;
  @Input() submitted: boolean;
  @Output() onTypeDocumentChanges = new EventEmitter<number>();
  refNature$: Observable<Array<Directory>>;
  refProcedure: Array<Directory>;
  refTypeContrat: Array<Directory>;
  refTypeDocument$: Observable<Array<Directory>>;
  refCCAG: Array<Directory>;


  constructor(private readonly store: Store<State>) {
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content);
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CONTRAT)?.content)
      .subscribe(value => {

        let directory: Directory = {
          id: 0,
          label: 'Ne dépend pas du type de contrat',
          uid: null,
          externalCode: null,
          actif: true,
          shortLabel: null
        };
        this.refTypeContrat = [directory, ...value];
      });
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content)
      .subscribe(value => {
        let directory: Directory = {
          id: 0,
          label: 'Toutes',
          uid: null,
          externalCode: null,
          actif: true,
          shortLabel: null
        };
        this.refProcedure = [directory, ...value];
      });
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.CCAG)?.content).subscribe(value => {
      let directory: Directory = {
        id: 0,
        label: 'Tous',
        uid: null,
        externalCode: null,
        actif: true,
        shortLabel: null
      };
      this.refCCAG = [directory, ...value];
    })

  }

  ngOnInit(): void {
    if (!!this.canevas) {
      if (this.canevas.typeContratList.length === 0) {
        this.canevas.typeContratList = [0];
      }
      if (!this.canevas.procedurePassationList || this.canevas.procedurePassationList.length === 0) {
        this.canevas.procedurePassationList = [0];
      }
    }
  }

  getNatureIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }

  changeNature(id: number) {
    this.canevas.naturePrestation = id;
  }

  updateContratList(event: any) {
    //si on a choisi "Ne dépend pas du type de contrat" on reset la liste
    if (event[event.length - 1] === 0) {
      this.canevas.typeContratList = [0];
    } else {
      //si la liste contient "Ne dépend pas du type de contrat" on le supprime
      if (this.canevas.typeContratList.length > 0 && this.canevas.typeContratList[0] === 0) {
        this.canevas.typeContratList = this.canevas.typeContratList.filter(value => value !== 0);
      }
    }
  }

  updateProcedureList(event: any) {
    //si on a choisi "Toutes" on reset la liste
    if (event[event.length - 1] === 0) {
      this.canevas.procedurePassationList = [0];
    } else {
      //si la liste contient "Toutes" on le supprime
      if (this.canevas.procedurePassationList.length > 0 && this.canevas.procedurePassationList[0] === 0) {
        this.canevas.procedurePassationList = this.canevas.procedurePassationList.filter(value => value !== 0);
      }
    }
  }

  updateTypeDocument($event: number) {
    //pour que la gestion de la dérogation soit proposée ou non en fonction du type de document il faut que l'info parvienne jusqu'au composant chapitre treeview
    this.onTypeDocumentChanges.emit($event);
  }
}
