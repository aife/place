import {Component} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {ClauseBean, ClauseDocument} from "@shared-global/core/models/api/clausier.api";
import {BaseFormComponent} from "@shared-global/components/base-form/base-form.component";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {
    ModalPreviewClauseComponent
} from "@shared-global/components/modal-preview-clause/modal-preview-clause.component";
import {BsModalService} from "ngx-bootstrap/modal";
import {ClauseBeanModel} from "@shared-global/core/models/clause-bean.model";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";


@Component({
    selector: 'atx-form-clause',
    templateUrl: './form-clause.component.html',
    styleUrls: ['./form-clause.component.css'],

})
export class FormClauseComponent extends BaseFormComponent<ClauseBean, ClauseBeanModel, ClauseDocument> {

    idTypeClause: number;
    submitted: boolean = false;
    isPotentiellementConditionnee1: boolean = false;
    isPotentiellementConditionnee2: boolean = false;


    constructor(protected modalService: BsModalService,
                protected readonly store: Store<State>,
                protected readonly clauseService: ClauseService,
                protected router: RoutingInterneService,
                protected route: ActivatedRoute) {
        super(clauseService, router, route, modalService, store, BreadcrumbParamsConst.CLIENT_CLAUSE_CREATION,
            BreadcrumbParamsConst.CLIENT_CLAUSE_MODIFICATION, BreadcrumbParamsConst.CLIENT_CLAUSE_DUPLICATION,
            BreadcrumbParamsConst.EDITEUR_CLAUSE_CREATION,
            BreadcrumbParamsConst.EDITEUR_CLAUSE_MODIFICATION, BreadcrumbParamsConst.EDITEUR_CLAUSE_DUPLICATION)
    }


    initModel() {
        this.model = new ClauseBeanModel();
        this.model.idNaturePrestation = 0;
        this.model.actif = false;
        this.model.idProcedure = 0;
        this.model.idTypeDocument = 1;
        this.model.idThemeClause = 1;
        this.model.idsTypeContrats = [0];
        this.model.potentiellementConditionnees = [];
        this.initContenuClause();


    }

    initContenuClause() {
        this.model.clauseTexteLibre = null;
        this.model.clauseListeChoixExclusif = null;
        this.model.clauseValeurHeritee = null;
        this.model.clauseTexteFixe = null;
        this.model.clauseListeChoixCumulatif = null;
        this.model.clauseTextePrevalorise = null;
    }


    updateForm($event: any) {
        this.idTypeClause = $event;
        if (!!this.model.idTypeClause) {
            this.initContenuClause();
        }

    }

    preview() {

      if (this.model.idTypeClause !== 9 || (this.model.idTypeClause === 9 && !!this.model.clauseValeurHeritee.idRefValeurTypeClause)) {
        this.clauseService.getClausePreview(this.model, this.editeur).subscribe(value => {
          let config = {
            backdrop: true,
            ignoreBackdropClick: true,
            class: "modal-lg"
          };
          let bsModal = this.modalService.show(ModalPreviewClauseComponent, config);
          bsModal.content.titre = 'redaction.clause.previsualiser.titre';
          bsModal.content.clauseDocument = value;
        })
      }
    }


    createOrUpdateOrCloneClause(statut: number, changePage: boolean, champs: string) {
      this.submitted = true;
      if (this.checkForm()) {
        this.submit(statut, 'clause', changePage, champs, this.model.idClause);
      }


    }

    checkForm() {

        //vérifier gestion potentiellement conditionnée 1
        if (this.isPotentiellementConditionnee1) {
            if (this.model.potentiellementConditionnees.length === 0) {
                return false;
            } else {
                if (!((!!this.model.potentiellementConditionnees[0] && !!this.model.potentiellementConditionnees[0].id
                        && !!this.model.potentiellementConditionnees[0].valeurId && !this.model.potentiellementConditionnees[0].multiChoix)
                    ||
                    (!!this.model.potentiellementConditionnees[0] && !!this.model.potentiellementConditionnees[0].id
                        && !!this.model.potentiellementConditionnees[0].valeurs && this.model.potentiellementConditionnees[0].valeurs.length > 0
                        && this.model.potentiellementConditionnees[0].multiChoix))) {
                    return false;
                }
            }
        }
        //vérifier gestion potentiellement conditionnée 2
        if (this.isPotentiellementConditionnee2) {
            if (this.model.potentiellementConditionnees.length < 2) {
                return false;
            } else {
                if (!(((!!this.model.potentiellementConditionnees[0] && !!this.model.potentiellementConditionnees[0].id
                            && !!this.model.potentiellementConditionnees[0].valeurId && !this.model.potentiellementConditionnees[0].multiChoix)
                        || (!!this.model.potentiellementConditionnees[0] && !!this.model.potentiellementConditionnees[0].id
                            && !!this.model.potentiellementConditionnees[0].valeurs && this.model.potentiellementConditionnees[0].valeurs.length >= 0
                            && this.model.potentiellementConditionnees[0].multiChoix))
                    &&
                    ((!!this.model.potentiellementConditionnees[1] && !!this.model.potentiellementConditionnees[1].id
                            && !!this.model.potentiellementConditionnees[1].valeurId && !this.model.potentiellementConditionnees[1].multiChoix)
                        || (!!this.model.potentiellementConditionnees[1] && !!this.model.potentiellementConditionnees[1].id
                            && !!this.model.potentiellementConditionnees[1].valeurs && this.model.potentiellementConditionnees[1].valeurs.length > 0
                            && this.model.potentiellementConditionnees[1].multiChoix)))) {
                    return false;
                }
            }
        }

        //modification
        if (!!this.model.idClause) {
            return (!!this.model.idThemeClause && this.checkFormType());

        } else {
            //creation
            return (!!this.model.idTypeClause &&
                !!this.model.idTypeDocument &&
                !!this.model.idThemeClause &&
                (!!this.model.idProcedure || this.model.idProcedure === 0) &&
                this.checkFormType());
        }

    }

    private checkFormType() {
        if (!!this.model.clauseTexteFixe) {
            return (!!this.model.clauseTexteFixe.texteFixe && this.model.clauseTexteFixe.texteFixe.length > 0);
        } else if (!!this.model.clauseTexteLibre) {
            return true;
        } else if (!!this.model.clauseTextePrevalorise) {
            return (!!this.model.clauseTextePrevalorise.defaultValue && this.model.clauseTextePrevalorise.defaultValue.length > 0);
        } else if (!!this.model.clauseValeurHeritee) {
            return (!!this.model.clauseValeurHeritee.idRefValeurTypeClause);
        } else if (!!this.model.clauseListeChoixCumulatif) {
            return (!!this.model.clauseListeChoixCumulatif.formulaires && this.model.clauseListeChoixCumulatif.formulaires.length >= 2);
        } else if (!!this.model.clauseListeChoixExclusif) {
            return (!!this.model.clauseListeChoixExclusif.formulaires && this.model.clauseListeChoixExclusif.formulaires.length >= 2);
        } else {
            return false;
        }
    }

    displaySuccessToast(action: string) {
        if (action === 'creer') {
            this.store.dispatch(showSuccessToast({
                header: 'Information', message: 'redaction.clause.succes.creer'
            }));
        } else {
            this.store.dispatch(showSuccessToast({
                header: 'Clause ' + this.model.referenceClause, message: 'redaction.clause.succes.' + action
            }));
        }
    }

    prepareModal(config: {
        backdrop: boolean;
        ignoreBackdropClick: boolean;
        class: string
    }, type: string, i18n: string, action: string) {
        let bsModal = this.modalService.show(ModalConfirmationComponent, config);
        bsModal.content.titre = 'redaction.' + type + '.confirmation.' + action;
        bsModal.content.messages = ['redaction.' + type + '.confirmation.' + i18n];
        bsModal.content.reference = this.model.referenceClause;
        bsModal.content.onSave.subscribe(value => this.updateAction(value))
        return bsModal;
    }

    updatePotentiellementConditionnee1($event: boolean) {
        this.isPotentiellementConditionnee1 = $event;
        this.checkForm();
    }

    updatePotentiellementConditionnee2($event: boolean) {
        this.isPotentiellementConditionnee2 = $event;
        this.checkForm();
    }

  private updateAction(value: boolean) {
    if(value) {
      this.action = FormActionEnum.MODIFICATION;
    }

  }

  navigateToModification() {
    let path =AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.FORM_CLAUSE.replace(':type', this.editeur ? 'editeur' : 'client')
      .replace(':id', this.model.idClause + '')
      .replace(':action', 'modification');
    if (this.model.idPublication) {
      path += '?idPublication' + this.model.idPublication
    }
    this.router.navigateFromContexte(path);
  }



}


