import {Component} from '@angular/core';
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";


@Component({
  selector: 'app-dashboard-clauses-client',
  templateUrl: './dashboard-clauses-client-wc.component.html',
  styleUrls: ['./dashboard-clauses-client-wc.component.css'],

})
export class DashboardClausesClientWcComponent {

  param = BreadcrumbParamsConst.CLIENT_CLAUSE_DASHBOARD;
  editeur = false;
  linkCreation = AppInternalPathEnum.PRIVATE_PATH + 'client' + AppInternalPathEnum.CREATION_CLAUSE;
  administrationRoles = [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT];
  validationRoles: string[] = [HabilitationsEnum.VALIDATION_CLAUSE_CLIENT];

  sortFieldsList = ['reference', 'clauseEditeur', 'epmTRefStatutRedactionClausier.id', 'epmTRefThemeClause.libelle',
    'dateCreation', 'dateModification', 'epmTClausePub.idPublication'];

  constructor() {
    console.log(this.editeur)
  }
}


