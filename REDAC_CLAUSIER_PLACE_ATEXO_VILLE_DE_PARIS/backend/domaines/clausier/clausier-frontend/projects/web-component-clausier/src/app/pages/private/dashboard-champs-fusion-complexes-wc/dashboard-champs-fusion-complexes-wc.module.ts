import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbAlertModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {NgxLoadingModule} from "ngx-loading";
import {ClrCommonFormsModule} from "@clr/angular";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {UiSwitchModule} from "ngx-ui-switch";
import {ComponentsModule} from "@shared-global/components/components.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {DashboardClausesWcModule} from "@pages-wc/private/dashboard-clauses-wc/dashboard-clauses-wc.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {
  DashboardChampsFusionComplexesWcComponent
} from "@pages-wc/private/dashboard-champs-fusion-complexes-wc/dashboard-champs-fusion-complexes-wc.component";

@NgModule({
  declarations: [DashboardChampsFusionComplexesWcComponent],
    imports: [
        CommonModule,
        LayoutModule,
        RouterModule,
        NgbPaginationModule,
        TranslateModule,
        ChartsModule,
        FormsModule,
        NgbAlertModule,
        BsDatepickerModule,
        NgSelectModule,
        NgxLoadingModule,
        ClrCommonFormsModule,
        CommonModule,
        UiSwitchModule,
        ReactiveFormsModule,
        ComponentsModule,
        DirectivesModule,
        DashboardClausesWcModule,
        PipesModule,
        CommonModule,
    ], exports: [DashboardChampsFusionComplexesWcComponent],
  providers: [BsModalService]
})
export class DashboardChampsFusionComplexesWcModule {
}
