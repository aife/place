import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {Canevas, CanevasBean, ChapitreDocument} from "@shared-global/core/models/api/clausier.api";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {BaseDetailsComponent} from "@shared-global/components/base-details/base-details.component";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {BsModalService} from "ngx-bootstrap/modal";
import {
  ModalPreviewCanevasComponent
} from "@shared-global/components/modal-preview-canevas/modal-preview-canevas.component";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";

@Component({
  selector: 'atx-canevas-details',
  templateUrl: './canevas-details.component.html',
  styleUrls: ['./canevas-details.component.css']

})
export class CanevasDetailsComponent extends BaseDetailsComponent<CanevasBean, Canevas, ChapitreDocument[]> {

  @Input() administrationRoles = [];
  @Input() validationRoles = [];
  @Input() showDetails = false;
  @Output() isDeleted = new EventEmitter<boolean>();
  @Input() set model(model: CanevasBean) {
    this._model = model;
  }

  get model() {
    return this._model;
  }

  @Input() set editeur(editeur: boolean) {
    this._editeur = editeur;
  }

  get editeur() {
    return this._editeur;
  }

  constructor(protected modalService: BsModalService, protected changeDetectorRef: ChangeDetectorRef, protected router: RoutingInterneService, protected readonly canevasService: CanevasService, protected readonly store: Store<State>) {
    super(modalService, changeDetectorRef, router, canevasService, store, AppInternalPathEnum.FORM_CANEVAS)
    this._type = "canevas";

  }


  preview() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-dialog-scrollable modal-full-width"
    };
    this.bsModalRef = this.modalService.show(ModalPreviewCanevasComponent, config);
    this.bsModalRef.content.titre = 'redaction.canevas.previsualiser.titre';
    this.bsModalRef.content.editeur = this.editeur;
    this.bsModalRef.content.canevas = this.model;

  }

  delete() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'redaction.' + this._type + '.confirmation.supprimer.titre';
    this.bsModalRef.content.messages = ['redaction.' + this._type + '.confirmation.supprimer.message'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.baseService.delete(this._model.id, this._model.idPublication)
            .subscribe(value => {
              this._model = value;
              this.isDeleted.emit(true);
              this.store.dispatch(showSuccessToast({
                header: 'Information',
                message: 'redaction.' + this._type + '.succes.supprimer'
              }));
            })
        }
        this.bsModalRef = null;
      }
    );
  }

}
