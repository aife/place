import {Component} from '@angular/core';

import {ActivatedRoute} from '@angular/router';

import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {BsModalService} from "ngx-bootstrap/modal";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";


@Component({
  selector: 'atx-surcharge-clause',
  templateUrl: './surcharge-clause.component.html',
  styleUrls: ['./surcharge-clause.component.css'],

})
export class SurchargeClauseComponent {

  loading = false;
  surchargeActive = false;
  editeur = false;
  param: any = BreadcrumbParamsConst.CLIENT_CLAUSE_SURCHARGE;
  clauseSurcharge: ClauseBean;
  clauseInitiale: ClauseBean;

  constructor(protected readonly clauseService: ClauseService,
              protected modalService: BsModalService,
              protected readonly store: Store<State>,
              protected router: RoutingInterneService,
              protected route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (!!params.id) {
        this.loading = true;
        clauseService.getById(params.id, params.idPublication, this.editeur, false,
          false).subscribe(value => {
          this.loading = false;
          this.clauseInitiale = value;

        }, () => this.loading = false)
        clauseService.getClauseSurcharge(params.id, params.idPublication).subscribe(value => {
          this.loading = false;
          this.clauseSurcharge = value
        }, () => this.loading = false)

      }
    });
  }


  back() {
    this.router.navigateFromContexte(this.param.links[0].link)
  }

  surcharge() {
    if (this.checkFormType()) {
      let config = {
        backdrop: true,
        ignoreBackdropClick: true,
        class: "modal-lg"
      };
      let bsModal = this.modalService.show(ModalConfirmationComponent, config);
      bsModal.content.titre = 'redaction.clause.confirmation.surcharger.titre';
      bsModal.content.messages = ['redaction.clause.confirmation.surcharger.message'];
      bsModal.content.reference = this.clauseInitiale.referenceClause;
      bsModal.content.onClose.subscribe(value => {
          if (value) {
            this.clauseService.surcharge(this.clauseInitiale.idClause, this.clauseInitiale.idPublication, this.editeur, this.clauseSurcharge, this.surchargeActive)
              .subscribe(value => {
                this.clauseSurcharge = value;
                this.router.navigateFromContexte(AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE)
                this.store.dispatch(showSuccessToast({
                  header: 'Clause ' + this.clauseInitiale.referenceClause, message: 'redaction.clause.succes.surcharger'
                }));
              })
          }
          bsModal = null;
        }
      );
    }
  }

  private checkFormType() {
    if (!!this.clauseSurcharge.clauseTexteFixe) {
      return (!!this.clauseSurcharge.clauseTexteFixe.texteFixe && this.clauseSurcharge.clauseTexteFixe.texteFixe.length > 0);
    } else if (!!this.clauseSurcharge.clauseTexteLibre) {
      return true;
    } else if (!!this.clauseSurcharge.clauseTextePrevalorise) {
      return (!!this.clauseSurcharge.clauseTextePrevalorise.defaultValue && this.clauseSurcharge.clauseTextePrevalorise.defaultValue.length > 0);
    } else if (!!this.clauseSurcharge.clauseValeurHeritee) {
      return (!!this.clauseSurcharge.clauseValeurHeritee.idRefValeurTypeClause);
    } else if (!!this.clauseSurcharge.clauseListeChoixCumulatif) {
      return (!!this.clauseSurcharge.clauseListeChoixCumulatif.formulaires && this.clauseSurcharge.clauseListeChoixCumulatif.formulaires.length >= 2);
    } else if (!!this.clauseSurcharge.clauseListeChoixExclusif) {
      return (!!this.clauseSurcharge.clauseListeChoixExclusif.formulaires && this.clauseSurcharge.clauseListeChoixExclusif.formulaires.length >= 2);
    } else {
      return false;
    }
  }
}


