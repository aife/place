import {Component} from '@angular/core';
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";


@Component({
  selector: 'app-dashboard-clauses-editeur',
  templateUrl: './dashboard-canevas-editeur-wc.component.html',
  styleUrls: ['./dashboard-canevas-editeur-wc.component.css'],

})
export class DashboardCanevasEditeurWcComponent {

  param = BreadcrumbParamsConst.EDITEUR_CANEVAS_DASHBOARD;
  editeur = true;
  linkCreation = AppInternalPathEnum.PRIVATE_PATH + 'editeur' + AppInternalPathEnum.CREATION_CANEVAS;
  administrationRoles = [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR];
  validationRoles: string[] = [HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR];
  sortFieldsList: string[] = ['reference', 'canevasEditeur', 'epmTRefStatutRedactionClausier.id', 'titre', 'epmTRefTypeDocument.libelle',
    'idNaturePrestation', 'dateCreation', 'dateModification', 'idLastPublication']

}


