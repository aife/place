import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgbDatepickerModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from "@ngx-translate/core";
import {UiSwitchModule} from "ngx-ui-switch";
import {ClrCommonFormsModule} from "@clr/angular";
import {ComponentsModule} from "@shared-global/components/components.module";

import {NgxLoadingModule} from "ngx-loading";
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {
  ParametrageClauseComponent
} from "@pages-wc/private/parametrage-clause/parametrage-clause.component";
import {QuillModule} from "ngx-quill";


@NgModule({
    declarations: [ParametrageClauseComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgSelectModule,
    BsDatepickerModule,
    BsDatepickerModule.forRoot(),
    NgbDatepickerModule,
    ReactiveFormsModule,
    NgbModule,
    TranslateModule,
    UiSwitchModule,
    ClrCommonFormsModule,
    ComponentsModule,
    NgxLoadingModule,
    BibliotequeCommuneModule,
    DirectivesModule,
    PipesModule,
    QuillModule
  ], exports: [ParametrageClauseComponent]
})
export class ParametrageClauseModule {
}
