import {Component, OnInit} from '@angular/core';
import {BreadcrumbParamsConst} from '@core-clausier/constantes/breadcrumb-params.const';
import {HabilitationsEnum} from '@shared-global/core/enums/habilitations.enum';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DocumentService} from '@shared-global/core/services/document.service';
import {
  ModalAjoutDocumentAdministrableComponent
} from '@shared-global/components/modal-ajout-document-administrable/modal-ajout-document-administrable.component';
import {Agent, DocumentAdministrable, Produit} from '@shared-global/core/models/api/clausier-core.api';
import {Store} from '@ngrx/store';
import {State} from '@shared-global/store';
import {saveFile} from '@shared-global/core/utils/download/doawnload.utils';
import moment from 'moment';
import {ModalConfirmationComponent} from '@shared-global/components/modal-confirmation/modal-confirmation.component';
import {showSuccessToast} from '@shared-global/store/toast/toast.action';


@Component({
  selector: 'app-dashboard-champs-fusion-complexes-wc',
  templateUrl: './dashboard-champs-fusion-complexes-wc.component.html',
  styleUrls: ['./dashboard-champs-fusion-complexes-wc.component.css'],

})
export class DashboardChampsFusionComplexesWcComponent implements OnInit {

  agent: Agent;
  param: any;
  loadingEdit = false;
  url: string;
  //loading = false;
  documents: Array<DocumentAdministrable> = []
  documentSelected: DocumentAdministrable;
  produits: Array<Produit> = [];
  nomsProduits: string[] = [];
  produitSelected: Produit;
  administrationRoles: string[] = [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR];
  loadingTelechargement = false;
  loadingReinialiser = false;
  bsModalRef!: BsModalRef;

  constructor(private readonly store: Store<State>, protected modalService: BsModalService, private documentService: DocumentService) {

  }


  ngOnInit(): void {
    this.param = BreadcrumbParamsConst.CHAMP_FUSION_COMPLEXE;
    this.store.select(state => state.userReducer.user)
      .subscribe(value => {
        this.agent = value;
        if (!!this.agent) {
          this.produits = this.agent.produits;
          this.produits.forEach(produit => {
            this.nomsProduits.push(produit.nom)
          })
          this.documentService.listDocumentsAdministrables(true, this.nomsProduits).subscribe(value => {
            this.documents = value;
          })
        }
      })


  }

  public grouper(document: DocumentAdministrable): string {
    return document?.produit;
  }


  editer() {
    this.produits.forEach(produit => {
      if (produit.nom.toLowerCase() === this.documentSelected.produit.toLowerCase()) {
        this.produitSelected = produit;
      }
    })
    if (!!this.produitSelected) {
      this.loadingEdit = true;
      this.documentService.editDocument(this.documentSelected.id, this.produitSelected).subscribe(value => {
        this.url = value.url;
        window.open(this.url, '_blank');
        this.loadingEdit = false;
      }, () => this.loadingEdit = false)
    }
  }

  openAjoutDocumentModal() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let i18n = '';
    this.bsModalRef = this.modalService.show(ModalAjoutDocumentAdministrableComponent, config);
    this.bsModalRef.content.titre = 'champ-fusion.add-modal';
    this.bsModalRef.content.isChampFusion = true;
    this.bsModalRef.content.products = this.nomsProduits;
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.documentService.listDocumentsAdministrables(true, this.nomsProduits).subscribe(value => {
            this.documents = value;
          })
        }
        this.bsModalRef = null;
      }
    );
  }

  desactiver() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'champ-fusion.delete-modal';
    this.bsModalRef.content.messages = ['champ-fusion.confirmation.delete'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.documentService.disableDocument(this.documentSelected.id).subscribe(value1 => {
            this.documentSelected = null;
            this.store.dispatch(showSuccessToast({
              header: 'Information',
              message: 'champ-fusion.confirmation.success.supprimer'
            }));
            this.documentService.listDocumentsAdministrables(true, this.nomsProduits).subscribe(value2 => {
              this.documents = value2;
            })
          })
        }
        this.bsModalRef = null;
      }
    );
  }

  searchByCode(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item?.code?.toLocaleLowerCase().indexOf(term) > -1 || item?.libelle?.toLocaleLowerCase().indexOf(term) > -1;

  }

  telecharger() {
    this.loadingTelechargement = true;
    this.documentService.downloadTemplate(this.documentSelected.id).subscribe(file => {
      this.loadingTelechargement = false;
      saveFile(file, "Template-" + this.documentSelected.produit + "-" + this.documentSelected.code + "-" + moment(new Date()).format("YYYYMMDD") + '.docx');

    }, () => this.loadingTelechargement = false)
  }
  reinitialiser() {

    this.loadingReinialiser = true;
    this.documentService.reinitialiserDocument(this.documentSelected.id).subscribe(value1 => {
        this.loadingReinialiser = false;
        this.store.dispatch(showSuccessToast({
          header: 'Information',
          message: 'doc-administrable.confirmation.success.reinitialisation'
        }));
        this.documentService.listDocumentsAdministrables(true, this.nomsProduits).subscribe(value2 => {
          this.documents = value2;
          this.documentSelected = value2.find(item => item.id === value1.id);
        });
      },
      () => this.loadingReinialiser = false);

  }

  isOpen() {
    if (!this.documentSelected) {
      return false;
    }
    return this.documentSelected.statutEdition !== 'CLOSED_WITHOUT_EDITING' && this.documentSelected.statutEdition !== 'SAVED_AND_CLOSED';

  }
}


