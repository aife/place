import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {
  ModalModificationPublicationComponent
} from "@shared-global/components/modal-modification-publication/modal-modification-publication.component";
import {PublicationService} from "@shared-global/core/services/publication.service";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import moment from "moment/moment";
import {WorkerService} from "@shared-global/core/services/worker.service";
import {showErrorToast, showSuccessToast} from "@shared-global/store/toast/toast.action";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";

@Component({
  selector: 'atx-versions-details',
  templateUrl: './versions-details.component.html',
  styleUrls: ['./versions-details.component.css']

})
export class VersionsDetailsComponent {

  @Input() administrationRoles = [];
  @Input() validationRoles = [];
  @Input() showDetails = false;

  private _loadingActivation: boolean;

  @Input()
  public  set loadingActivation(value: boolean){
    this._loadingActivation = value;
  }

  public get loadingActivation(){
    return this._loadingActivation;
  }


  bsModalRef!: BsModalRef;
  private _model: PublicationClausierBean;
  private checkWorkerInterval: any;
  loadingExport: boolean = false;
  loadingActivationVersion: boolean = false; //passe à true au clic sur activer
  data: ArrayBuffer = null;
  @Output() activationChange = new EventEmitter<boolean>();
  @Output() activationEnCours = new EventEmitter<boolean>();

  @Input() set model(model: PublicationClausierBean) {
    this._model = model;
  }

  get model() {
    return this._model;
  }


  constructor(protected modalService: BsModalService,
              protected readonly publicationService: PublicationService,
              protected readonly store: Store<State>,
              private workerService: WorkerService,
              protected router: RoutingInterneService) {

  }


  openModalModif() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let i18n = '';
    this.bsModalRef = this.modalService.show(ModalModificationPublicationComponent, config);
    this.bsModalRef.content.titre = 'publicationClausier.listPublicationClausier.commentaires';
    this.bsModalRef.content.publication = this.model;

    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.publicationService.editCommentPublicationClausier(this.model.id, this.model.commentaire).subscribe(value => {
            this.activationChange.emit(true);
          })

        }
        this.bsModalRef = null;
      }
    );
  }

  activate() {
    this.publicationService.canActivatePublication().subscribe(res => {
      if (res === "true") {
        this.loadingActivationVersion = true;
        this.loadingActivation = true;
        this.activationEnCours.emit(true);
        this.publicationService.activePublicationClausier(this.model.id).subscribe(idTask => {

          this.checkWorkerInterval = setInterval(() => {
            this.workerService.findTask(idTask).subscribe(value => {
              if (value.result === 'success' && value.data) {
                this.activationEnCours.emit(false);
                this.loadingActivation = false;
                this.loadingActivationVersion = false;
                this.store.dispatch(showSuccessToast({
                  header: 'Information', message: 'redaction.clause.succes.activation'
                }));
                clearInterval(this.checkWorkerInterval);
              }
              if (value.result === 'error' && value.message) {
                console.log("error", value.message)
                this.activationEnCours.emit(false);
                this.loadingActivation = false;
                this.loadingActivationVersion = false;
                this.store.dispatch(showErrorToast({
                  header: "error.header",
                  message: "error.activation"
                }))
                clearInterval(this.checkWorkerInterval);
              }
            })
          }, 10000);
        }, () => {
          this.activationEnCours.emit(false);
          this.loadingActivation = false;
          this.loadingActivationVersion = false;
          this.store.dispatch(showErrorToast({
            header: "error.header",
            message: "error.activation"
          }))
          clearInterval(this.checkWorkerInterval);
        })
      } else {
        console.log("une publication est déjà en cours d'activation")
      }
    })


  }

  download() {
    this.loadingExport = true;
    this.publicationService.downloadPublication(this.model.id).subscribe(value => {
      this.data = value;
      this.save(value);
      this.loadingExport = false;
    }, () => this.loadingExport = false)
  }


  save(value: ArrayBuffer) {
    saveFile(value, "001_" + moment(new Date()).format("YYYYMMDD") + '.zip');
  }
}
