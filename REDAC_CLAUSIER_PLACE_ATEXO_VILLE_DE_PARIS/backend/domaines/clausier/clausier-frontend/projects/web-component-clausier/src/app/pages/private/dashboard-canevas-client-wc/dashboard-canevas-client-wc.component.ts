import {Component} from '@angular/core';
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";


@Component({
  selector: 'app-dashboard-clauses-client',
  templateUrl: './dashboard-canevas-client-wc.component.html',
  styleUrls: ['./dashboard-canevas-client-wc.component.css'],

})
export class DashboardCanevasClientWcComponent {

  param = BreadcrumbParamsConst.CLIENT_CANEVAS_DASHBOARD;
  editeur = false;
  linkCreation = AppInternalPathEnum.PRIVATE_PATH + 'client' + AppInternalPathEnum.CREATION_CANEVAS;
  administrationRoles = [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT];
  validationRoles: string[] = [HabilitationsEnum.VALIDATION_CANEVAS_CLIENT];
  sortFieldsList: string[] = ['reference', 'canevasEditeur', 'epmTRefStatutRedactionClausier.id', 'titre', 'epmTRefTypeDocument.libelle',
    'idNaturePrestation', 'dateCreation', 'dateModification', 'epmTCanevasPub.idPublication']

}


