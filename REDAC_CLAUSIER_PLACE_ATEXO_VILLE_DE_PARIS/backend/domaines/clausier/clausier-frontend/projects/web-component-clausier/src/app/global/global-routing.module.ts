import {RouterTestingModule} from '@angular/router/testing';
import {NgModule} from '@angular/core';
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {LoginPageWCComponent} from "@pages-wc/public/login-page-wc/login-page-wc.component";

import {AccueilPageWCComponent} from "@pages-wc/private/accueil-page-wc/accueil-page-wc.component";
import {JspRouterWcComponent} from "@pages-wc/public/jsp-router-wc/jsp-router-wc.component";
import {FormCanevasComponent} from "@pages-wc/private/form-canevas/form-canevas.component";
import {FormClauseComponent} from "@pages-wc/private/form-clause/form-clause.component";
import {SurchargeClauseComponent} from "@pages-wc/private/surcharge-clause/surcharge-clause.component";
import {
  DashboardClausesEditeurWcComponent
} from "@pages-wc/private/dashboard-clauses-editeur-wc/dashboard-clauses-editeur-wc.component";
import {
  DashboardClausesClientWcComponent
} from "@pages-wc/private/dashboard-clauses-client-wc/dashboard-clauses-client-wc.component";
import {
  DashboardCanevasClientWcComponent
} from "@pages-wc/private/dashboard-canevas-client-wc/dashboard-canevas-client-wc.component";
import {
  DashboardCanevasEditeurWcComponent
} from "@pages-wc/private/dashboard-canevas-editeur-wc/dashboard-canevas-editeur-wc.component";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";
import {DashboardVersionsWcComponent} from "@pages-wc/private/dashboard-versions-wc/dashboard-versions-wc.component";
import {
  DashboardClausesParametreAgentWcComponent
} from "@pages-wc/private/dashboard-clauses-parametre-agent-wc/dashboard-clauses-parametre-agent-wc.component";
import {ParametrageClauseComponent} from "@pages-wc/private/parametrage-clause/parametrage-clause.component";
import {
  DashboardClausesParametreDirectionWcComponent
} from "@pages-wc/private/dashboard-clauses-parametre-direction-wc/dashboard-clauses-parametre-direction-wc.component";
import {FormVersionComponent} from "@pages-wc/private/form-version/form-version.component";
import {
  ExportClausesClientWcComponent
} from "@pages-wc/private/export-clauses-client-wc/export-clauses-client-wc.component";
import {
  ExportClausesEditeurWcComponent
} from "@pages-wc/private/export-clauses-editeur-wc/export-clauses-editeur-wc.component";
import {
  DashboardDocumentsAdministrablesWcComponent
} from "@pages-wc/private/dashboard-documents-administrables-wc/dashboard-documents-administrables-wc.component";
import {
  DashboardChampsFusionComplexesWcComponent
} from "@pages-wc/private/dashboard-champs-fusion-complexes-wc/dashboard-champs-fusion-complexes-wc.component";

const routes = [
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
    component: AccueilPageWCComponent,
    outlet: 'clausier',
    canActivate: [AuthGuard]
  },
  {
    path: AppInternalPathEnum.JSP_REGEX,
    outlet: 'clausier',
    component: JspRouterWcComponent,
  },
  {
    path: AppInternalPathEnum.LOGIN,
    outlet: 'clausier',
    component: LoginPageWCComponent,
    canActivate: [NotConnectedGuard]
  },

  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS,
    component: DashboardCanevasEditeurWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier',
    data: {
      roles: [HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR, HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR]
    }
  }, {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS,
    component: DashboardCanevasClientWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier',
    data: {
      roles: [HabilitationsEnum.VALIDATION_CANEVAS_CLIENT, HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_DOCUMENT_ADMINISTRABLE,
    component: DashboardDocumentsAdministrablesWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier'
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_CHAMP_FUSION_COMPLEXE,
    component: DashboardChampsFusionComplexesWcComponent,
    outlet: 'clausier',
    canActivate: [AuthGuard]
  }, {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_VERSION,
    component: DashboardVersionsWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier',
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR, HabilitationsEnum.ACTIVATION_VERSION_CLAUSIER_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS,
    component: DashboardClausesEditeurWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier',
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR, HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_AGENT_DASHBOARD_CLAUSE,
    component: DashboardClausesParametreAgentWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier'
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_DIRECTION_DASHBOARD_CLAUSE,
    component: DashboardClausesParametreDirectionWcComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier'
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRAGE_CLAUSE,
    component: ParametrageClauseComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier'
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE,
    component: DashboardClausesClientWcComponent,
    outlet: 'clausier',
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.VALIDATION_CLAUSE_CLIENT, HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CREATION_VERSION,
    component: FormVersionComponent,
    outlet: 'clausier',
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CANEVAS,
    component: FormCanevasComponent,
    canActivate: [AuthGuard],
    outlet: 'clausier',
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT, HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.TYPE_CREATION_CLAUSE,
    component: FormClauseComponent,
    outlet: 'clausier',
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT, HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR]
    }
  },
  {
    outlet: 'clausier',
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.FORM_CANEVAS,
    component: FormCanevasComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT, HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.FORM_CLAUSE,
    outlet: 'clausier',
    component: FormClauseComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT, HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR]
    }
  },
  {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.SURCHARGE_CLIENT_CLAUSE,
    outlet: 'clausier',
    component: SurchargeClauseComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
    }
  }, {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_EXPORT_CLAUSE,
    outlet: 'clausier',
    component: ExportClausesClientWcComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
    }
  }, {
    path: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_EXPORT_CLAUSE,
    outlet: 'clausier',
    component: ExportClausesEditeurWcComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR]
    }
  }];

@NgModule({
  imports: [
    RouterTestingModule.withRoutes(routes),
  ],
  exports: [RouterTestingModule],
})
export class GlobalRoutingModule {
}
