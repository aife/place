import {ApplicationRef, DoBootstrap, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GlobalModule} from "./global/global.module";
import {TranslateService, TranslateStore} from "@ngx-translate/core";
import {APP_BASE_HREF, registerLocaleData} from "@angular/common";
import {NgbCarouselConfig, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {BsModalService} from "ngx-bootstrap/modal";
import {BlockUIService} from "ng-block-ui";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {defineLocale} from "ngx-bootstrap/chronos";
import localeFr from '@angular/common/locales/fr';
import {frLocale} from 'ngx-bootstrap/locale';
defineLocale('fr', frLocale);
registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GlobalModule,
    AppRoutingModule
  ],
  providers: [TranslateStore,
    {
      provide: APP_BASE_HREF,
      useValue: '/redac/web-component-clausier'

    },
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerGestureConfig
    },
    NgbCarouselConfig,
    NgbModalConfig,
    BsModalService,
    BlockUIService],
})
export class AppModule implements DoBootstrap {

  constructor(private bsLocaleService: BsLocaleService, private translateService: TranslateService) {
    this.translateService.use('fr');
    this.bsLocaleService.use('fr');
  }

  ngDoBootstrap(appRef: ApplicationRef): void {
    if (document.querySelector('app-root')) {
      appRef.bootstrap(AppComponent);
    }
  }
}
