import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import moment from 'moment';
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {PublicationFilterModel} from "@shared-global/core/models/publication-filter.model";


@Component({
  selector: 'atx-versions-filter',
  templateUrl: './versions-filter.component.html',
  styleUrls: ['./versions-filter.component.css']
})
export class VersionsFilterComponent implements OnInit {

  @Input() filter: PublicationFilterModel;
  @Output() filterUsed = new EventEmitter();
  @Output() onChangeFilter = new EventEmitter<PublicationFilterModel>();

  showMore = false;
  minDate: Date;
  maxDate: Date;
  choixGestion: any[] = [
    {id: 0, label: 'Indifférent'},
    {id: 1, label: 'Oui'},
    {id: 2, label: 'Non'}];

  constructor(private bsLocaleService: BsLocaleService, private readonly store: Store<State>) {
    this.maxDate = new Date()
    this.minDate = moment(new Date("01/03/2000")).toDate();
  }

  ngOnInit(): void {
  }

  rechercher() {
    this.onChangeFilter.emit(this.filter);
  }

  reInitFilter() {
    this.filter = new PublicationFilterModel()
    this.rechercher();
  }


}
