import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
  ModalNouveauChapitreComponent
} from "@shared-global/components/modal-nouveau-chapitre/modal-nouveau-chapitre.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {CanevasModel} from "@shared-global/core/models/canevas.model";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Directory} from "@shared-global/core/models/api/clausier.api";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";


@Component({
  selector: 'atx-form-canevas-contenu',
  templateUrl: './form-canevas-contenu.component.html',
  styleUrls: ['./form-canevas-contenu.component.css']
})
export class FormCanevasContenuComponent implements OnInit {
  typeDocument: string;
  @Input() editeur: boolean;
  private _canevas: CanevasModel;
  @Input()
  public set canevas(value: CanevasModel) {
    this._canevas = value;
  }

  public get canevas() {
    return this._canevas;
  }

  @Input() action: FormActionEnum;
  displayMessageVersion: boolean;
  bsModalRef!: BsModalRef;

  constructor(private modalService: BsModalService) {

  }



  ngOnInit(): void {
    if (this.editeur && !!this.canevas.idCanevas && !!this.canevas.lastVersion && this.action===1) {
      this.displayMessageVersion = true;
    }
  }


  addChapitre() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-md"
    };
    this.bsModalRef = this.modalService.show(ModalNouveauChapitreComponent, config);
    this.bsModalRef.content.onClose.subscribe(chapitre => {
      if (chapitre) {
        if (!this.canevas.chapitres) {
          this.canevas.chapitres = [];
        }
        chapitre.numero = this.canevas.chapitres.length + 1;
        this.canevas.chapitres.push(chapitre)
      }
      this.bsModalRef = null;
    });
  }


  getMessage() {
    let message1 = "Pour déplacer les chapitres ou les clauses, vous pouvez glisser l'élément vers la position souhaitée.";
    let message2 = "Attention, vos modifications ne seront visibles qu'à la création d'une nouvelle version.";
    if (!this.displayMessageVersion && this.canevas?.chapitres?.length>0) {
      return message1;
    }
    else if (this.displayMessageVersion && this.canevas?.chapitres?.length>0) {
      return message1 + " " + message2;
    }
    else if (this.displayMessageVersion && !(this.canevas?.chapitres?.length>0)) {
      return message2;
    }
  }

  updateChapitre($event: ChapitreModel) {
    let chapitresUpdated: Array<ChapitreModel> = [];
    this.canevas.chapitres.forEach(chapitre => {
      if (chapitre.idChapitre === $event.idChapitre) {
        chapitre = $event;
      }
      chapitresUpdated.push(chapitre);
    })
    this.canevas.chapitres = chapitresUpdated;
  }
}
