import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccueilPageWCComponent} from './accueil-page-wc/accueil-page-wc.component';
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {ComponentsModule} from "@shared-global/components/components.module";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {TranslateModule} from "@ngx-translate/core";
import {NgbTabsetModule} from "@ng-bootstrap/ng-bootstrap";
import {ModalModule} from 'ngx-bootstrap/modal';
import {NgxLoadingModule} from "ngx-loading";
import {DashboardCanevasWcModule} from "@pages-wc/private/dashboard-canevas-wc/dashboard-canevas-wc.module";
import {UiSwitchModule} from "ngx-ui-switch";
import {FormCanevasModule} from "@pages-wc/private/form-canevas/form-canevas.module";
import {DashboardClausesWcModule} from "@pages-wc/private/dashboard-clauses-wc/dashboard-clauses-wc.module";
import {JspRouterWcComponent} from "@pages-wc/public/jsp-router-wc/jsp-router-wc.component";
import {FormClauseModule} from "@pages-wc/private/form-clause/form-clause.module";
import {SurchargeClauseModule} from "@pages-wc/private/surcharge-clause/surcharge-clause.module";
import {DashboardVersionsWcModule} from "@pages-wc/private/dashboard-versions-wc/dashboard-versions-wc.module";
import {FormVersionModule} from "@pages-wc/private/form-version/form-version.module";
import {
  DashboardClausesEditeurWcComponent
} from "@pages-wc/private/dashboard-clauses-editeur-wc/dashboard-clauses-editeur-wc.component";
import {
  DashboardClausesClientWcComponent
} from "@pages-wc/private/dashboard-clauses-client-wc/dashboard-clauses-client-wc.component";
import {
  DashboardCanevasEditeurWcComponent
} from "@pages-wc/private/dashboard-canevas-editeur-wc/dashboard-canevas-editeur-wc.component";
import {
  DashboardCanevasClientWcComponent
} from "@pages-wc/private/dashboard-canevas-client-wc/dashboard-canevas-client-wc.component";
import {
  ExportClausesClientWcComponent
} from "@pages-wc/private/export-clauses-client-wc/export-clauses-client-wc.component";
import {PipesModule} from "@shared-global/pipes/pipes.module";
import {
  ExportClausesEditeurWcComponent
} from "@pages-wc/private/export-clauses-editeur-wc/export-clauses-editeur-wc.component";
import {
  DashboardClausesParametreAgentWcModule
} from "@pages-wc/private/dashboard-clauses-parametre-agent-wc/dashboard-clauses-parametre-agent-wc.module";
import {ParametrageClauseModule} from "@pages-wc/private/parametrage-clause/parametrage-clause.module";
import {
  DashboardClausesParametreDirectionWcModule
} from "@pages-wc/private/dashboard-clauses-parametre-direction-wc/dashboard-clauses-parametre-direction-wc.module";
import {
  DashboardDocumentsAdministrablesWcModule
} from "@pages-wc/private/dashboard-documents-administrables-wc/dashboard-documents-administrables-wc.module";
import {
  DashboardChampsFusionComplexesWcModule
} from "@pages-wc/private/dashboard-champs-fusion-complexes-wc/dashboard-champs-fusion-complexes-wc.module";


@NgModule({
  declarations: [ExportClausesClientWcComponent, ExportClausesEditeurWcComponent, DashboardCanevasEditeurWcComponent, DashboardCanevasClientWcComponent, DashboardClausesEditeurWcComponent, DashboardClausesClientWcComponent, DashboardClausesClientWcComponent, AccueilPageWCComponent],
  exports: [
    AccueilPageWCComponent
  ],
  imports: [
    SurchargeClauseModule,
    DashboardCanevasWcModule,
    DashboardDocumentsAdministrablesWcModule,
    DashboardChampsFusionComplexesWcModule,
    DashboardClausesWcModule,
    DashboardClausesParametreAgentWcModule,
    DashboardClausesParametreDirectionWcModule,
    ParametrageClauseModule,
    DashboardVersionsWcModule,
        FormCanevasModule,
        FormClauseModule,
        FormVersionModule,
    CommonModule,
    BibliotequeCommuneModule,
    FormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule,
    NgbTabsetModule,
    ModalModule.forRoot(),
    NgxLoadingModule,
    UiSwitchModule,
    PipesModule
  ],
  providers: [BsLocaleService]
})
export class PrivateWcModule {

}
