import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  ClauseBean,
  Directory,
  InfoBulle,
  PotentiellementConditionnee,
  PotentiellementConditionneeValeur
} from "@shared-global/core/models/api/clausier.api";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";
import {NgForm} from "@angular/forms";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {PotentiellementConditionneeModel} from "@shared-global/core/models/potentiellement-conditionnee.model";

@Component({
  selector: 'atx-form-clause-proprietes',
  templateUrl: './form-clause-proprietes.component.html',
  styleUrls: ['./form-clause-proprietes.component.css']
})
export class FormClauseProprietesComponent implements OnInit {


  private _clause: ClauseBean;
  @Input()
  public  set clause(value: ClauseBean){
    this._clause = value;
    //modification
    if (!!this._clause.id) {
      if (this._clause.idsTypeContrats.length === 0) {
        this._clause.idsTypeContrats = [0];
      }
      if (!this._clause.idProcedure) {
        this._clause.idProcedure = 0;
      }
      this.infobulle = this._clause.infoBulle;
      this.potentiellementConditionnee1 = !!this._clause.potentiellementConditionnees && !!this._clause.potentiellementConditionnees[0];
      this.potentiellementConditionnee2 = !!this._clause.potentiellementConditionnees && !!this._clause.potentiellementConditionnees[1];
    }
  }

  public get clause(){
    return this._clause;
  }
  @Input() form: NgForm;
  @Input() action: FormActionEnum;
  @Input() submitted: boolean;
  @Input() editeur: boolean;

  potentiellementConditionnee1: boolean = false;
  potentiellementConditionnee2: boolean = false;

  potentiellementConditionneeList: Array<PotentiellementConditionnee> = []; //liste de criteres pour choix1 et choix2

  choix1: PotentiellementConditionnee = new PotentiellementConditionneeModel();
  valeurList1: Array<PotentiellementConditionneeValeur> = [];  //liste de valeurs pour critère 1
  valeur1: Array<number> = []; //liste ids de valeurs choisies pour critère 1 cas multichoix

  choix2: PotentiellementConditionnee = new PotentiellementConditionneeModel();
  valeurList2: Array<PotentiellementConditionneeValeur> = [];  //liste de valeurs pour critère 2
  valeur2: Array<number> = []; //liste ids de valeurs choisies pour critère 2

  @Output() changeTypeClause: EventEmitter<any> = new EventEmitter();
  @Output() isPotentiellementConditionnee1: EventEmitter<boolean> = new EventEmitter();
  @Output() isPotentiellementConditionnee2: EventEmitter<boolean> = new EventEmitter();
  infobulle: InfoBulle = new class implements InfoBulle {
    actif: boolean;
    description: string;
    descriptionLien: string;
    empty: boolean;
    lien: string;
  };

  idTypeDocument: any;

  refNature$: Observable<Array<Directory>>;
  refProcedure: Array<Directory>;
  refTypeContrat: Array<Directory>;
  refTypeClause$: Observable<Array<Directory>>;
  refTheme$: Observable<Array<Directory>>;
  refTypeDocument$: Observable<Array<Directory>>;

  constructor(private readonly store: Store<State>, protected readonly referentielsService: ReferentielsService) {
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels?.get(ReferentielEnum.NATURE)?.content);
    this.store.select(state => state.referentielReducer.referentiels?.get(ReferentielEnum.TYPE_CONTRAT)?.content)
      .subscribe(value => {
        if (!!value) {


          let directory: Directory = {
            id: 0,
            label: 'Ne dépend pas du type de contrat',
            uid: null,
            externalCode: null,
            actif: true,
            shortLabel: null
          };
          this.refTypeContrat = [directory, ...value];
        }
      });
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.refTypeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CLAUSE)?.content);
    this.refTheme$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.THEME_CLAUSE)?.content);
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content)
      .subscribe(value => {
        if (!!value) {
          let directory: Directory = {
            id: 0,
            label: 'Toutes',
            uid: null,
            externalCode: null,
            actif: true,
            shortLabel: null
          };
          this.refProcedure = [directory, ...value];
        }
      });
  }

  ngOnInit(): void {
    this.referentielsService.allCritereConditions().subscribe(value => {
      this.potentiellementConditionneeList = value;
      //modification
      if (!!this.clause.id) {
        if (this.potentiellementConditionnee1) {
          if (!!this.clause.potentiellementConditionnees[0]) {
            this.potentiellementConditionneeList.forEach(value => {
              if (value.id === this.clause.potentiellementConditionnees[0].id) {
                this.choix1 = value;
                this.valeurList1 = this.choix1.valeurs;
                this.clause.potentiellementConditionnees[0].valeurs.forEach(value => {
                  this.valeurList1.forEach(value2 => {
                    if (value === value2.id) {
                      this.valeur1.push(value2.id);
                    }
                  })
                })
              }
            })
          }

          if (!!this.clause.potentiellementConditionnees[1]) {
            this.potentiellementConditionneeList.forEach(value => {
              if (value.id === this.clause.potentiellementConditionnees[1].id) {
                this.choix2 = value;
                this.valeurList2 = this.choix2.valeurs;
                this.clause.potentiellementConditionnees[1].valeurs.forEach(value => {
                  this.valeurList2.forEach(value2 => {
                    if (value === value2.id) {
                      this.valeur2.push(value2.id);
                    }
                  })

                })
              }
            })
          }
        }
      } else {
        //creation
        this.clause.infoBulle = this.infobulle;
      }
    })
  }

  getNatureIcon(id: number) {
    if (id === 0) {
      return "";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }


  changeNature(id: number) {
    if (this.isCreation())
      this.clause.idNaturePrestation = id;
  }

  isModification() {
    return this.action === FormActionEnum.MODIFICATION;
  }

  isDuplication() {
    return this.action === FormActionEnum.DUPLICATION;
  }

  isCreation() {
    return this.action === FormActionEnum.CREATION;
  }

  getPrestationsIcons(id: number) {

    if (id === 1) {
      return 'fa fa-wrench'
    }
    if (id === 2) {
      return 'fa fa-shopping-cart'
    }
    if (id === 3) {
      return 'fa fa-briefcase'
    }
  }

  getPrestationClass(id: number) {
    if (this.clause.idNaturePrestation === 0 || this.clause.idNaturePrestation === id) {
      return 'badge-warning'
    }
    return 'badge-secondary'
  }


  updateForm() {
    this.changeTypeClause.emit(this.clause.idTypeClause);
  }

  updateChoixValeur1() {
    if (this.choix1.multiChoix) {
      this.clause.potentiellementConditionnees[0].valeurs = [];
      this.valeur1.forEach(value => {
        this.clause.potentiellementConditionnees[0].valeurs.push(value);
      })
    }
  }

  updateChoixValeur2() {
    if (this.choix2.multiChoix) {
      this.clause.potentiellementConditionnees[1].valeurs = [];
      this.valeur2.forEach(value => {
        this.clause.potentiellementConditionnees[1].valeurs.push(value);
      })
    }
  }


  updateChoixCritere1() {
    //reset toutes les données
    this.clause.potentiellementConditionnees = [];
    this.potentiellementConditionnee2 = false;
    this.valeurList1 = [];
    this.valeur1 = [];
    this.choix2 = null;
    this.valeur2 = [];
    if (!!this.choix1) {
      this.valeurList1 = this.choix1.valeurs;
      //update de la clause
      let newPC1 = new PotentiellementConditionneeModel();
      newPC1.id = this.choix1.id;
      newPC1.valeurs = [];
      newPC1.valeurId = null;
      newPC1.multiChoix = this.choix1.multiChoix;
      this.clause.potentiellementConditionnees.push(newPC1);

    } else {
      this.isPotentiellementConditionnee2.emit(false);
    }
  }

  updateChoixCritere2() {
    if (this.clause.potentiellementConditionnees.length === 2) {
      this.clause.potentiellementConditionnees.splice(1, 1);
    }

    //Remplissage du select valeur en fonction du critere choisi
    this.valeurList2 = [];
    if (!!this.choix2) {
      this.valeurList2 = this.choix2.valeurs;
      //update de la clause
      let newPC2 = new PotentiellementConditionneeModel();
      newPC2.id = this.choix2.id;
      newPC2.multiChoix = this.choix2.multiChoix;
      newPC2.valeurs = [];
      newPC2.valeurId = null;
      this.clause.potentiellementConditionnees[1] = newPC2;
    }

  }

  updateIdsContrat(event: any) {
    //si on a choisi "Ne dépend pas du type de contrat" on reset la liste
    if (event[event.length - 1] === 0) {
      this.clause.idsTypeContrats = [0];
    } else {
      //si la liste contient "Ne dépend pas du type de contrat" on le supprime
      if (this.clause.idsTypeContrats.length > 0 && this.clause.idsTypeContrats[0] === 0) {
        this.clause.idsTypeContrats = this.clause.idsTypeContrats.filter(value => value !== 0);
      }
    }
  }

  getValeur1(): string {
    let res: string[] = [];
    this.valeurList1.forEach(value => {
      this.clause.potentiellementConditionnees[0].valeurs.forEach(value2 => {
        if (value.id === value2) {
          res.push(value.libelle);
        }
      })
    })
    return res.toString();
  }

  getValeur2(): string {
    let res: string[] = [];
    this.valeurList2.forEach(value => {
      this.clause.potentiellementConditionnees[1].valeurs.forEach(value2 => {
        if (value.id === value2) {
          res.push(value.libelle);
        }
      })
    })
    return res.toString();
  }

  checkPotentiellementConditionnee(numero: number) {
    if (numero === 1) {
      if (this.potentiellementConditionnee1) {
        this.isPotentiellementConditionnee1.emit(true);
      } else {
        this.potentiellementConditionnee2 = false;
        this.isPotentiellementConditionnee1.emit(false);
        this.isPotentiellementConditionnee2.emit(false);
      }
    } else if (numero === 2) {
      if (this.potentiellementConditionnee2) {
        this.isPotentiellementConditionnee2.emit(true);
      } else {
        this.isPotentiellementConditionnee2.emit(false);
      }
    }
  }

}
