import {ApplicationRef, DoBootstrap, Injector, LOCALE_ID, NgModule} from '@angular/core';
import {APP_BASE_HREF, CommonModule, PlatformLocation, registerLocaleData} from '@angular/common';
import {GlobalComponent} from './global.component';
import {RouterModule} from "@angular/router";
import {ElementZoneStrategyFactory} from "elements-zone-strategy";
import {createCustomElement} from "@angular/elements";
import {TranslateLoader, TranslateModule, TranslateService, TranslateStore} from "@ngx-translate/core";
import {PublicWcModule} from "@pages-wc/public/public-wc.module";
import {CoreModule} from "@shared-global/core/core.module";
import {PrivateWcModule} from "@pages-wc/private/private-wc.module";
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {GlobalRoutingModule} from "@global-wc/global-routing.module";
import {defineLocale} from "ngx-bootstrap/chronos";
import {enGbLocale, frLocale} from "ngx-bootstrap/locale";
import localeFr from "@angular/common/locales/fr";
import localeEn from "@angular/common/locales/en";
import {ComponentsModule} from "@shared-global/components/components.module";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {NgxSpinnerModule} from "ngx-spinner";
import {LoadingBarModule} from "@ngx-loading-bar/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {MockPlatformLocation} from "@angular/common/testing";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {HAMMER_GESTURE_CONFIG, HammerGestureConfig} from "@angular/platform-browser";
import {NgbCarouselConfig, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {BlockUIService} from "ng-block-ui";
import {HttpClient} from "@angular/common/http";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {forkJoin, Observable} from "rxjs";
import {map} from "rxjs/operators";
import * as _ from "lodash";


defineLocale('fr', frLocale);
defineLocale('en', enGbLocale);
registerLocaleData(localeFr);
registerLocaleData(localeEn);


export class CustomLoader implements TranslateLoader {

  constructor(private http: HttpClient, private platformLocation: PlatformLocation,
              private readonly referentielsService: ReferentielsService) {
  }

  public getTranslation(lang: string): Observable<any> {
    let api = this.referentielsService.getApiI18n(lang).pipe();
    let app = this.referentielsService.getAppI18n('/redac/web-component-clausier', lang).pipe();
    return forkJoin(app, api).pipe(
      map(([a, b,]) => {
        // @ts-ignore
        return _.merge(a, b)
      }));

  }
}

@NgModule({
  declarations: [
    GlobalComponent
  ],
  imports: [
    PerfectScrollbarModule,
    NgxSpinnerModule,
    BibliotequeCommuneModule, CommonModule, PrivateWcModule,
    PublicWcModule, RouterModule, GlobalRoutingModule,
    NgSelectModule,
    CoreModule, ComponentsModule, LoadingBarModule, TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, PlatformLocation, ReferentielsService]
      }, isolate: true
    })
  ],
  providers: [TranslateStore,
    BsModalRef,
    {
      provide: PlatformLocation,
      useClass: MockPlatformLocation
    },
    {
      provide: APP_BASE_HREF,
      useValue: '/redac/web-component-clausier'
    },
    {provide: LOCALE_ID, useValue: 'fr-FR'}, {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerGestureConfig
    },
    NgbCarouselConfig,
    NgbModalConfig,
    BsModalService,
    BlockUIService],
  entryComponents: [GlobalComponent]

})
export class GlobalModule implements DoBootstrap {

  constructor(private injector: Injector, private bsLocaleService: BsLocaleService, private translateService: TranslateService) {
    let lang = localStorage.getItem("clausier-language");
    if (!lang) {
      lang = 'fr';
      localStorage.setItem("clausier-language", "fr")
    }
    this.translateService.use(lang);
    this.bsLocaleService.use(lang);
    const strategyFactory = new ElementZoneStrategyFactory(GlobalComponent, injector);
    const webComponent = createCustomElement(GlobalComponent, {injector, strategyFactory});
    customElements.define('atx-clausier', webComponent);
  }

  ngDoBootstrap(appRef: ApplicationRef): void {
  }
}
