import {Component} from '@angular/core';
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import moment from "moment";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {WorkerService} from "@shared-global/core/services/worker.service";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {Agent} from "@shared-global/core/models/api/clausier-core.api";
import {ImageUrlPipe} from "@shared-global/pipes/image-url/image-url.pipe";


@Component({
  selector: 'app-export-clauses-client',
  templateUrl: './export-clauses-client-wc.component.html',
  styleUrls: ['./export-clauses-client-wc.component.css'],

})
export class ExportClausesClientWcComponent {

  param: any = BreadcrumbParamsConst.CLIENT_CLAUSE_EXPORT;
  editeur = false;
  loadingExport = true;
  data: ArrayBuffer = null;
  administrationRoles = [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT];
  linkCreation = AppInternalPathEnum.PRIVATE_PATH + 'client' + AppInternalPathEnum.CREATION_CLAUSE;
  private checkWorkerInterval: any;
  agent: Agent;

  constructor(private readonly clauseService: ClauseService,
              private imageUrlPipe: ImageUrlPipe,
              private workerService: WorkerService,
              private readonly store: Store<State>) {
    this.store.select(state => state.userReducer.user)
      .subscribe(value => {
        this.agent = value;
      });
    this.exporter();
  }

  exporter() {
    if (this.data) {
      this.save(this.data)
    } else {
      this.loadingExport = true;
      this.clauseService.exporter(this.editeur).subscribe(idTask => {
        this.checkWorkerInterval = setInterval(() => {
          this.workerService.findTask(idTask).subscribe(value => {
            if (value.result === 'success' && value.data) {
              clearInterval(this.checkWorkerInterval);
              this.workerService.download(idTask).subscribe(file => {
                saveFile(file, "Export_clauses_" + moment(new Date()).format("YYYYMMDD") + '.xls');
                this.loadingExport = false;
                clearInterval(this.checkWorkerInterval);
              }, () => this.loadingExport = false)
            }
            if (value.result === 'error') {
              clearInterval(this.checkWorkerInterval);
              this.loadingExport = false;
            }
          })
        }, 10000);

      }, () => {
        clearInterval(this.checkWorkerInterval);
        this.loadingExport = false
      })
    }
  }

  save(value: ArrayBuffer) {
    saveFile(value, "Export_clauses_" + moment(new Date()).format("YYYYMMDD") + '.xls');
  }

  existingUrl() {
    return (!!this.agent && !!this.agent.mpeUrl && this.agent?.mpeUrl?.startsWith('http'));
  }
  getLogoUrl() {
      return this.agent.mpeUrl + "/themes/images/logo.gif";
  }
}


