import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CanevasFilterModel} from "@shared-global/core/models/canevas-filter.model";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {CanevasSearch, Directory} from "@shared-global/core/models/api/clausier.api";
import moment from 'moment';
import {Observable} from "rxjs";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";


@Component({
  selector: 'atx-canevas-filter',
  templateUrl: './canevas-filter.component.html',
  styleUrls: ['./canevas-filter.component.css']
})
export class CanevasFilterComponent implements OnInit {


  @Input() editeur = true;
  @Input() filter: CanevasSearch;
  @Output() reinitFilter = new EventEmitter<boolean>();
  @Output() filterUsed = new EventEmitter();
  @Output() onChangeFilter = new EventEmitter<CanevasFilterModel>();

  refTypeActeur$: Observable<Array<Directory>>;
  refStatut$: Observable<Array<Directory>>;
  refNature$: Observable<Array<Directory>>;
  refProcedure$: Observable<Array<Directory>>;
  refTypeContrat$: Observable<Array<Directory>>;
  refTypeDocument$: Observable<Array<Directory>>;
  refCCAG$: Observable<Array<Directory>>;
  showMore = false;
  minDate: Date;
  maxDate: Date;

  constructor(private bsLocaleService: BsLocaleService, private readonly store: Store<State>) {
    this.maxDate = new Date()
    this.minDate = moment(new Date("01/03/2000")).toDate();
  }

  ngOnInit(): void {
    this.refStatut$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.STATUT)?.content);
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content);
    this.refTypeContrat$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CONTRAT)?.content);
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.refProcedure$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content);
    this.refTypeActeur$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CANEVAS)?.content);
    this.refCCAG$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.CCAG)?.content);

  }


  rechercher() {
    this.onChangeFilter.emit(this.filter);
  }

  changeStatutFilter(id: number) {
    this.filter.idStatutRedactionClausier = id;
  }

  changeNatureFilter(id: number) {
    this.filter.idNaturePrestation = id;
  }


  changeActivationFilter(activation: number) {
    if (activation === 0) {
      this.filter.actif = null;
    } else if (activation === 1) {
      this.filter.actif = true;
    } else if (activation === 2) {
      this.filter.actif = false;
    }
  }

  getStatutIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-edit";
    } else if (id === 2) {
      return "fa fa-hourglass";
    } else if (id === 3) {
      return "fa fa-check-circle";
    }
  }

  getNatureIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }

  reInitFilter() {
    this.filter = new CanevasFilterModel(this.editeur);
    if (this.editeur) {
      sessionStorage.removeItem('filter-dashboard-canevas-editeur');
    } else {
      sessionStorage.removeItem('filter-dashboard-canevas-client');
    }
    this.reinitFilter.emit(true);
    this.rechercher();
  }


  getBackgroundStatut(id: number) {
    if (id === this.filter.idStatutRedactionClausier) {
      switch (id) {
        case 1:
          return "active-error";
          break;
        case 2:
          return "active-warning";
          break;
        case 3:
          return "active-success";
          break;
      }
    }
  }
}
