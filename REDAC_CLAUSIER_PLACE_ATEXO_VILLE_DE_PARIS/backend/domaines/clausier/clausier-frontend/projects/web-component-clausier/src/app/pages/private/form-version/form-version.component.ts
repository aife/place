import {Component, OnInit} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {PublicationService} from "@shared-global/core/services/publication.service";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";


@Component({
  selector: 'atx-form-version',
  templateUrl: './form-version.component.html',
  styleUrls: ['./form-version.component.css'],

})
export class FormVersionComponent implements OnInit {
  param: any;
  submitted: boolean = false;
  canCreatePublication: boolean = true;
  loadingPublication: boolean = false;
  linkCreation: string;
  administrationRoles: string[] = [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR];
  validationRoles: string[] = [];
  publication: PublicationClausierBean = new class implements PublicationClausierBean {
    actif: boolean;
    commentaire: string;
    enCoursActivation: boolean;
    dateActivation: Date;
    dateIntegration: Date;
    datePublication: Date;
    datePublicationMax: Date;
    datePublicationMin: Date;
    editeur: string;
    gestionLocal: boolean;
    gestionLocalRecherche: number;
    id: number;
    idClausierFile: number;
    idOrganisme: number;
    idReferentielFile: number;
    idUtilisateur: number;
    nomCompletUtilisateur: string;
    version: string;
    version1: string;
    version2: string;
    version3: string;
  }

  constructor(protected readonly publicationService: PublicationService,
              protected router: RoutingInterneService,
              protected route: ActivatedRoute) {


  }

  ngOnInit(): void {
    this.param = BreadcrumbParamsConst.VERSION_CREATION;
    this.linkCreation = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CREATION_VERSION;
    this.publicationService.canActivatePublication().subscribe(res => {
        this.canCreatePublication = res === "true";
      })
  }


  createPublication() {
    this.publicationService.canActivatePublication().subscribe(res => {
      if (res === "true") {
        this.submitted = true;
        if (!!this.publication.version1 && !!this.publication.version2 && !!this.publication.version3) {
          this.loadingPublication = true;
          this.publicationService.create(this.publication).subscribe(value => {
            this.loadingPublication = false;
            this.canCreatePublication = true;
            console.log("done " + value);
            this.router.navigateFromContexte(AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_VERSION);
          }, error => {
            this.loadingPublication = false;
            console.log("error " + error);
          })
        }
      } else {
        this.canCreatePublication = false;
      }
    })


  }


  exporter() {

  }
}


