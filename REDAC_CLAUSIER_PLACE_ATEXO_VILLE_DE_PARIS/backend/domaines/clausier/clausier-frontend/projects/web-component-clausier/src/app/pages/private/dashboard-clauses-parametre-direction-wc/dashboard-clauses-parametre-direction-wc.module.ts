import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClrCommonFormsModule} from "@clr/angular";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {ComponentsModule} from "@shared-global/components/components.module";
import {NgxLoadingModule} from "ngx-loading";
import {BsDatepickerConfig, BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {DirectivesModule} from "@shared-global/directives/directives.module";
import {
  DashboardClausesParametreDirectionWcComponent
} from "@pages-wc/private/dashboard-clauses-parametre-direction-wc/dashboard-clauses-parametre-direction-wc.component";
import {
  DashboardClausesParametreAgentWcModule
} from "@pages-wc/private/dashboard-clauses-parametre-agent-wc/dashboard-clauses-parametre-agent-wc.module";

@NgModule({
  declarations: [DashboardClausesParametreDirectionWcComponent],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    TranslateModule,
    ChartsModule,
    FormsModule,
    ClrCommonFormsModule,
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule,
    NgxLoadingModule,
    BsDatepickerModule.forRoot(),
    DirectivesModule,
    DashboardClausesParametreAgentWcModule,
  ],
  providers: [BsDatepickerConfig],
  exports: [DashboardClausesParametreDirectionWcComponent]
})
export class DashboardClausesParametreDirectionWcModule {
}
