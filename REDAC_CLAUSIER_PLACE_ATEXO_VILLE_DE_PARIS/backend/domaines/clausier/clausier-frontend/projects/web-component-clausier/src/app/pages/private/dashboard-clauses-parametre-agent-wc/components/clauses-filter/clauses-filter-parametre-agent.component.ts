import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {ClauseSearch, Directory} from "@shared-global/core/models/api/clausier.api";
import moment from 'moment';
import {ClauseFilterModel} from '@shared-global/core/models/clause-filter.model';
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";


@Component({
  selector: 'atx-clauses-filter-parametre-agent',
  templateUrl: './clauses-filter-parametre-agent.component.html',
  styleUrls: ['./clauses-filter-parametre-agent.component.css']
})
export class ClausesFilterParametreAgentComponent implements OnInit {

  @Input() filter: ClauseSearch;
  @Output() filterUsed = new EventEmitter();
  @Output() onChangeFilter = new EventEmitter<ClauseFilterModel>();

  refNature$: Observable<Array<Directory>>;
  refProcedure$: Observable<Array<Directory>>;
  refTypeContrat$: Observable<Array<Directory>>;

  refTypeDocument$: Observable<Array<Directory>>;
  refThemeClause$: Observable<Array<Directory>>;

  @Input() showMore = false;


  constructor(private bsLocaleService: BsLocaleService, private readonly store: Store<State>) {


  }

  ngOnInit(): void {
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content);
    this.refTypeContrat$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CONTRAT)?.content);
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.refProcedure$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content);
    this.refThemeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.THEME_CLAUSE)?.content);
  }


  rechercher() {
    this.onChangeFilter.emit(this.filter);
  }


  changeNatureFilter(id: number) {
    this.filter.idNaturePrestation = id;
  }


  changeActivationFilter(activation: number) {
    if (activation === 0) {
      this.filter.actif = null;
    } else if (activation === 1) {
      this.filter.actif = true;
    } else if (activation === 2) {
      this.filter.actif = false;
    }
  }



  getNatureIcon(id: number) {
    if (id === 0) {
      return "fa fa-list-alt";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }

  reInitFilter() {
    this.filter = new ClauseFilterModel(false);
    this.rechercher();
  }


}
