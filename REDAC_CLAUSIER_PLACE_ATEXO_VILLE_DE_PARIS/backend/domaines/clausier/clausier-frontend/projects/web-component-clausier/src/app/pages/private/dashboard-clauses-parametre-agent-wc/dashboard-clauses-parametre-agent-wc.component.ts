import {Component, OnInit} from '@angular/core';
import {ClauseService} from "@shared-global/core/services/clause.service";
import {ClauseBean, PageRepresentation} from "@shared-global/core/models/api/clausier.api";
import {ClauseFilterModel} from "@shared-global/core/models/clause-filter.model";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {PageableModel} from "@shared-global/core/models/pageable.model";


@Component({
  selector: 'atx-dashboard-clauses-parametre-agent',
  templateUrl: './dashboard-clauses-parametre-agent-wc.component.html',
  styleUrls: ['./dashboard-clauses-parametre-agent-wc.component.css'],

})
export class DashboardClausesParametreAgentWcComponent implements OnInit {


  param: any;
  pageable: PageableModel = {page: 0, size: 10, asc: false, sortField: 'reference'};
  loading = false;
  showDetails = false;
  total = 0;
  page: PageRepresentation<ClauseBean>;
  sortFieldsList = ['reference', 'epmTRefStatutRedactionClausier.id', 'epmTRefThemeClause.libelle',
    'dateCreation', 'dateModification']
  clauseList: Array<ClauseBean> = [];
  filter: ClauseFilterModel;
  linkCreation: string;
  editeur: boolean = false;

  constructor(protected readonly clauseService: ClauseService) {

  }

  ngOnInit(): void {
    this.param = BreadcrumbParamsConst.PARAMETRE_AGENT_CLAUSE_DASHBOARD;
    this.linkCreation = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CREATION_VERSION;
    if (!this.filter)
      this.filter = new ClauseFilterModel(false);
    this.filter.parametrableAgent = true;
    this.filter.editeur = false;
    this.filter.idStatutRedactionClausier = 3;
    this.searchPage(this.filter, this.pageable);
  }





  searchPage(filter: ClauseFilterModel, pageable) {
    this.loading = true;
    filter.parametrableAgent = true;
    filter.editeur = false;
    filter.idStatutRedactionClausier = 3;
    this.clauseService.list(filter, pageable).subscribe(value => {
      this.loading = false;
      this.page = value;
      this.total = value.totalElements;
      this.clauseList = this.page.content;
    }, () => {
      this.clauseList = [];
      this.page = null;
      this.total = null;
      this.loading = false;
    });

  }


}


