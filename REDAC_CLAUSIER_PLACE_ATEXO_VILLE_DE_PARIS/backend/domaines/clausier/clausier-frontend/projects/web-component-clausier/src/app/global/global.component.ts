import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {State} from "@shared-global/store"
import {Store} from "@ngrx/store";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})
export class GlobalComponent implements OnInit {
  private connected: boolean;


  @Input() identifiantSaaS: string;

  @Input() proxyPass = '/redac';

  @Input() set page(page: string) {
    if (!!page) {
      this._page = page;
    } else {
      this._page = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL;
    }

  };

  get page() {
    return this._page;
  }

  private _page = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL;

  constructor(
    private store: Store<State>,
    private router: RoutingInterneService, private translateService: TranslateService) {
    this.translateService.use('fr');
  }

  ngOnInit(): void {
    this.navigate()
  }


  private navigate() {
    this.router.navigateFromContexte('login', {
      'identifiantSaaS': this.identifiantSaaS,
      proxyPass: this.proxyPass,
      isWebComponent: true,
      redirect: this.page
    });

  }
}
