import {Component} from '@angular/core';

import {ActivatedRoute} from '@angular/router';
import {Canevas, CanevasBean, ChapitreDocument} from "@shared-global/core/models/api/clausier.api";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {BaseFormComponent} from "@shared-global/components/base-form/base-form.component";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {CanevasModel} from "@shared-global/core/models/canevas.model";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";
import {
  ModalPreviewCanevasComponent
} from "@shared-global/components/modal-preview-canevas/modal-preview-canevas.component";


@Component({
  selector: 'atx-form-canevas',
  templateUrl: './form-canevas.component.html',
  styleUrls: ['./form-canevas.component.css'],

})
export class FormCanevasComponent extends BaseFormComponent<CanevasBean, Canevas, ChapitreDocument[]> {

  submitted: boolean = false;
  bsModalRef: BsModalRef;

  constructor(protected modalService: BsModalService,
              protected readonly store: Store<State>,

              protected readonly canevasService: CanevasService,
              protected router: RoutingInterneService,
              protected route: ActivatedRoute) {
    super(canevasService, router, route, modalService, store, BreadcrumbParamsConst.CLIENT_CANEVAS_CREATION,
      BreadcrumbParamsConst.CLIENT_CANEVAS_MODIFICATION, BreadcrumbParamsConst.CLIENT_CANEVAS_DUPLICATION,
      BreadcrumbParamsConst.EDITEUR_CANEVAS_CREATION,
      BreadcrumbParamsConst.EDITEUR_CANEVAS_MODIFICATION, BreadcrumbParamsConst.EDITEUR_CANEVAS_DUPLICATION)
  }


    initModel() {
        this.model = new CanevasModel()
        this.model.statut = false;
        this.model.compatibleEntiteAdjudicatrice = false;
        this.model.naturePrestation = 0;
        this.model.typeDocument = 1;
        this.model.typeContratList = [0];
        this.model.idRefCCAG = 0;
        this.model.canevasEditeur = this.editeur;
        this.model.procedurePassationList = [0];
    }


    createOrUpdateOrCloneCanevas(statut: number, changePage: boolean, champs: string) {
        this.submitted = true;
        if (this.checkForm()) {
            this.submit(statut, 'canevas', changePage, champs, this.model.idCanevas);
        }
    }

    private checkForm() {
      return (
        !!this.model.typeDocument &&
        !!this.model.titre &&
        (!!this.model.idRefCCAG || this.model.idRefCCAG === 0) &&
        this.model.procedurePassationList.length > 0 &&
        this.model.typeContratList.length > 0);
    }

    displaySuccessToast(action: string) {
        if (action === 'creer') {
            this.store.dispatch(showSuccessToast({
                header: 'Information', message: 'redaction.canevas.succes.creer'
            }));
        } else {
            this.store.dispatch(showSuccessToast({
                header: 'Canevas ' + this.model.referenceCanevas, message: 'redaction.canevas.succes.' + action
            }));
        }
    }

    prepareModal(config: {
        backdrop: boolean;
        ignoreBackdropClick: boolean;
        class: string
    }, type: string, i18n: string, action: string) {
        let bsModal = this.modalService.show(ModalConfirmationComponent, config);
        bsModal.content.titre = 'redaction.' + type + '.confirmation.' + action;
        bsModal.content.messages = ['redaction.' + type + '.confirmation.' + i18n];
        bsModal.content.reference = this.model.referenceCanevas;
        bsModal.content.onSave.subscribe(value => this.updateAction(value))
        return bsModal;
    }


  private updateAction(value: boolean) {
    if(value) {
      this.action = FormActionEnum.MODIFICATION;
    }

  }

  navigateToModification() {
    let path =AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.FORM_CANEVAS.replace(':type', this.editeur ? 'editeur' : 'client')
      .replace(':id', this.model.idCanevas + '')
      .replace(':action', 'modification');
    if (this.model.idPublication) {
      path += '?idPublication' + this.model.idPublication
    }
    this.router.navigateFromContexte(path);
  }

  preview() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-dialog-scrollable modal-full-width"
    };
    this.bsModalRef = this.modalService.show(ModalPreviewCanevasComponent, config);
    this.bsModalRef.content.titre = 'redaction.canevas.previsualiser.titre';
    this.bsModalRef.content.editeur = this.editeur;
    this.bsModalRef.content.isEdition = true;
    this.bsModalRef.content.canevas = this.model;

  }

  canPreview() {
    if (!!this.model.chapitres) {
      return this.model.chapitres.length > 0;
    }
    else {
      return false;
    }
  }

  updateTypeDocument($event: number) {
    this.model.typeDocument = $event;
  }
}


