import {Component, OnInit} from '@angular/core';
import {PageableModel} from "@shared-global/core/models/pageable.model";
import {PageRepresentation, PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";
import {BreadcrumbParamsConst} from "@core-clausier/constantes/breadcrumb-params.const";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {PublicationFilterModel} from "@shared-global/core/models/publication-filter.model";
import {PublicationService} from "@shared-global/core/services/publication.service";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";
import moment from "moment";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalImportComponent} from "@shared-global/components/modal-import/modal-import.component";


@Component({
  selector: 'app-dashboard-versions-wc',
  templateUrl: './dashboard-versions-wc.component.html',
  styleUrls: ['./dashboard-versions-wc.component.css'],

})
export class DashboardVersionsWcComponent implements OnInit {

  param: any;
  pageable: PageableModel = {page: 0, size: 10, asc: false, sortField: 'actif'};
  loading = false;
  loadingActivation = false;
  loadingExport = false;
  showDetails = false;
  total = 0;
  page: PageRepresentation<PublicationClausierBean>;
  sortFieldsList = ['actif','datePublication', 'version']
  publicationList: Array<PublicationClausierBean> = [];
  filter: PublicationFilterModel;
  linkCreation: string;
  administrationRoles: string[] = [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR];
  validationRoles: string[] = [];
  data: ArrayBuffer = null;
  bsModalRef!: BsModalRef;

  constructor(private readonly publicationService: PublicationService, protected modalService: BsModalService) {
    let filtreDashboardPublication = sessionStorage.getItem('filtreDashboardPublication');
    if (!!filtreDashboardPublication && filtreDashboardPublication !== 'null') {
      this.filter = JSON.parse(filtreDashboardPublication)
    }
    let pageableDashboardPublication = sessionStorage.getItem('pageableDashboardPublication');
    if (!!pageableDashboardPublication && pageableDashboardPublication !== 'null') {
      this.pageable = JSON.parse(pageableDashboardPublication)
    }
  }



  ngOnInit(): void {
    this.param = BreadcrumbParamsConst.VERSION_DASHBOARD;
    this.linkCreation = AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CREATION_VERSION;
    if (!this.filter)
      this.filter = new PublicationFilterModel();
    this.searchPage(this.filter, this.pageable);
    this.publicationService.canActivatePublication().subscribe(res => {
      this.loadingActivation = res == "false";
    })
  }

  searchPage(filter: PublicationFilterModel, pageable) {
    if (filter)
      sessionStorage.setItem('filtreDashboardPublication', JSON.stringify(filter))
    if (pageable)
      sessionStorage.setItem('pageableDashboardPublication', JSON.stringify(pageable))
    this.loading = true;
    this.publicationService.list(filter, pageable).subscribe(value => {
      this.loading = false;
      this.page = value;
      this.total = value.totalElements;
      this.publicationList = this.page.content;
    }, () => {
      this.publicationList = [];
      this.page = null;
      this.total = null;
      this.loading = false;
    });

  }

  exporter() {
    this.loading = true;
    this.publicationService.exportPublicationClausier(this.filter).subscribe(value => {
      this.data = value;
      this.save(value);
      this.loading = false;
    }, () => this.loading = false)
  }


  save(value: ArrayBuffer) {
    saveFile(value, "Export_publication_" + moment(new Date()).format("YYYYMMDD") + '.zip');
  }

  refresh() {
    this.searchPage(this.filter, this.pageable);



  }

  importer() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalImportComponent, config);
    this.bsModalRef.content.onClose.subscribe(value => this.refresh());
  }

  refreshButtons($event: boolean) {
    this.loadingActivation = $event;
    if (!this.loadingActivation) {
      this.searchPage(this.filter, this.pageable);
    }

  }
}


