import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClrCommonFormsModule} from "@clr/angular";
import {LayoutModule} from "@core-clausier/layout/layout.module";
import {DashboardClausesWcComponent} from "@pages-wc/private/dashboard-clauses-wc/dashboard-clauses-wc.component";
import {ComponentsModule} from "@shared-global/components/components.module";
import {NgxLoadingModule} from "ngx-loading";
import {
  ClausesFilterComponent
} from "@pages-wc/private/dashboard-clauses-wc/components/clauses-filter/clauses-filter.component";
import {BsDatepickerConfig, BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {DirectivesModule} from "@shared-global/directives/directives.module";

@NgModule({
  declarations: [DashboardClausesWcComponent, ClausesFilterComponent],
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule,
    TranslateModule,
    ChartsModule,
    FormsModule,
    ClrCommonFormsModule,
    CommonModule,
    ReactiveFormsModule,
    ComponentsModule,
    NgxLoadingModule,
    BsDatepickerModule.forRoot(),
    DirectivesModule,
  ],
  providers: [BsDatepickerConfig],
  exports: [DashboardClausesWcComponent, ClausesFilterComponent]
})
export class DashboardClausesWcModule {
}
