import {Component, HostListener, Inject, OnInit, Renderer2} from '@angular/core';
import {ThemeSettingsService} from '../settings/theme-settings.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {NavbarService} from "@core-clausier/services/layout/navbar.service";
import {AppConstants} from "@core-clausier/constantes/app.constants";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {
  getCCAGReferentiel,
  getNaturePrestationReferentiel,
  getProcedureReferentiel,
  getStatutReferentiel,
  getThemeClauseReferentiel,
  getTypeAuteurCanevasReferentiel,
  getTypeAuteurClauseReferentiel,
  getTypeClauseReferentiel,
  getTypeContratReferentiel,
  getTypeDocumentReferentiel,
  getValeurHeriteeReferentiel
} from "@shared-global/store/referentiels/referentiels.action";
import {getUser} from "@shared-global/store/user/user.action";

@Component({
  selector: 'app-private-layout',
  templateUrl: './private-layout.component.html',
  styleUrls: ['./private-layout.component.css']
})
export class PrivateLayoutComponent implements OnInit {

  private _unsubscribeAll: Subject<any>;
  private _themeSettingsConfig: any;
  public layout: any;
  public customizer: any;
  deviceInfo = null;

  constructor(private renderer: Renderer2,
              @Inject(DOCUMENT) private document: Document,
              private router: Router,
              private store: Store<State>,
              private navbarService: NavbarService,
              private _themeSettingsService: ThemeSettingsService,
              private deviceService: DeviceDetectorService) {

    this._unsubscribeAll = new Subject();

  }

  ngOnInit() {
    // Subscribe to config changes
    this._themeSettingsService.config
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((config) => {
        this._themeSettingsConfig = config;
      });

    this.deviceInfo = this.deviceService.getDeviceInfo();
    const isMobile = this.deviceService.isMobile();
    this.handleBody(isMobile);
    this.handleCollapsibleMenu();
    this.onResize();

  }

  handleBody(isMobile: boolean) {
    const _self = this;


    _self.renderer.setAttribute(document.body, 'data-menu', 'horizontal-menu-modern');


    let currentBodyClassList = [];
    this.layout = this._themeSettingsConfig.layout.style;
    if (window.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH_HORIZONTAL) {
      const previosBodyClassList = [].slice.call(document.body.classList);
      previosBodyClassList.forEach(function (c) {
        _self.renderer.removeClass(document.body, c);
      });
      currentBodyClassList = ['horizontal-layout', 'horizontal-menu', '2-columns', 'pace-done',
        'fixed-navbar', 'menu-hide'];

      if (this._themeSettingsConfig.layout.pattern === 'fixed') {
        currentBodyClassList.push('fixed-navbar');
      }

      if (this._themeSettingsConfig.layout.pattern === '') {
        currentBodyClassList.push('fixed-navbar');
      }

      if (this._themeSettingsConfig.layout.pattern === 'boxed') {
        this.renderer.addClass(document.body, 'boxed-layout');
        this.renderer.addClass(document.body, 'container');
        this.renderer.addClass(document.body, 'fixed-navbar');
      }
      // Normal view
    } else {
      const previosBodyClassList = [].slice.call(document.body.classList);
      let callapseOrExpanded = '';
      previosBodyClassList.forEach(function (c) {
        if (c === 'menu-collapsed') {
          callapseOrExpanded = 'menu-collapsed';
        } else if (c === 'menu-expanded') {
          callapseOrExpanded = 'menu-expanded';
        }
        _self.renderer.removeClass(document.body, c);
      });

      currentBodyClassList = ['horizontal-layout', '2-columns', 'horizontal-menu'];
      if (window.innerWidth >= AppConstants.MOBILE_RESPONSIVE_WIDTH) {
        currentBodyClassList.push('menu-expanded');
      } else {
        currentBodyClassList.push('menu-collapsed');
      }

      if (this._themeSettingsConfig.layout.pattern === 'boxed') {
        this.renderer.addClass(document.body, 'boxed-layout');
        this.renderer.addClass(document.body, 'container');
      }

    }


    currentBodyClassList.forEach(function (c) {
      _self.renderer.addClass(document.body, c);
    });
  }

  handleCollapsibleMenu() {
    if (this._themeSettingsConfig.menu === 'collapse') {
      // show the left aside menu
      this.navbarService.setFixedMenu(false);
      this.document.body.classList.remove('menu-expanded');
      this.document.body.classList.add('menu-collapsed');
    } else {
      this.navbarService.setFixedMenu(true);
      this.document.body.classList.remove('menu-collapsed');
      this.document.body.classList.add('menu-expanded');
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    const menuClose = document.body.getElementsByClassName('menu-close');
    const sidenavOverlay = document.getElementsByClassName('sidenav-overlay');

    if (window.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH) {
      this.handleBody(true);
      if (menuClose) {
        this.renderer.removeClass(sidenavOverlay.item(0), 'd-block');
        this.renderer.addClass(sidenavOverlay.item(0), 'd-none');
      }
    } else {
      this.handleBody(false);
    }

  }

  rightbar(event) {
    const toggle = document.getElementById('sidenav-overlay');
    if (event.currentTarget.className === 'sidenav-overlay d-block') {
      this.renderer.removeClass(toggle, 'd-block');
      this.document.body.classList.remove('menu-open');
      this.document.body.classList.add('menu-close');
      this.renderer.addClass(toggle, 'd-none');
    } else if (event.currentTarget.className === 'sidenav-overlay d-none') {
      this.renderer.removeClass(toggle, 'd-none');
      this.document.body.classList.remove('menu-close');
      this.document.body.classList.add('menu-open');
      this.renderer.addClass(toggle, 'd-block');
    }
  }

}
