// Default menu settings configurations


export interface MenuItem {
  id?: string;
  title: string;
  object: object;
  icon: string;
  page: string;
  isOpen: boolean;
  hasError: boolean;
  isSelected: boolean;
  isExternalLink?: boolean;
  deletable?: boolean;
  internalModule?: string;
  internalModuleUrl?: string;
  issupportExternalLink?: boolean;
  badge: { type: string, value: string };
  submenu: {
    items: Partial<MenuItem>[];
  };
  roles: Array<string>;
  section: string;
  index: number
}

export interface MenuConfig {
  items: Partial<MenuItem>[]

}






