import {MenuConfig} from "@core-clausier/models/menu-settings.model";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";

export const MenuSettingsConfig: MenuConfig = {
  items: [
    {
      icon: 'la la-home',
      page: 'agent/accueil',
      roles: ['*'],
    },
    {
      title: 'Editeur',
      icon: 'la la-list',
      roles: [
        HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR,
        HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR,
        HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR,
        HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR
      ],
      submenu: {
        items: [
          {
            title: 'Clause',
            page: '/agent/editeur/clause/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR,
              HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR
            ],
            submenu: {
              items: [
                {
                  title: 'Liste des clauses',
                  icon: 'la la-list',
                  page: '/agent/editeur/clause/dashboard',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR,
                    HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR
                  ],
                },
                {
                  title: 'Création Clause',
                  icon: 'la la-pencil',
                  page: '/agent/editeur/clause/creation',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR
                  ]
                },
                {
                  title: 'Exporter',
                  icon: 'la la-list',
                  page: '/agent/editeur/clause/export',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR,
                    HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR
                  ],
                },
              ]
            }
          },
          {
            title: 'Canevas',
            page: '/agent/editeur/canevas/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR,
              HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR
            ],
            submenu: {
              items: [
                {
                  title: 'Liste des canevas',
                  icon: 'la la-list',
                  page: '/agent/editeur/canevas/dashboard',
                  roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR,
                    HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR],
                },
                {
                  title: 'Création Canevas',
                  icon: 'la la-pencil',
                  page: '/agent/editeur/canevas/creation',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR,
                  ]
                }
              ]
            }
          },
          {
            title: 'Version de clausier',
            page: '/agent/editeur/publication-clausier/dashboard',
            roles: ['*'],
            submenu: {
              items: [
                {
                  title: 'Historique des versions',
                  icon: 'la la-list',
                  page: '/agent/editeur/publication-clausier/dashboard',
                  roles: ['*']
                },
                {
                  title: 'Création Version',
                  icon: 'la la-pencil',
                  page: '/agent/editeur/publication-clausier/creation',
                  roles: [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR]
                }
              ]
            }
          }

        ]
      }
    },
    {
      title: 'Client',
      icon: 'la la-list',
      roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT,
        HabilitationsEnum.VALIDATION_CANEVAS_CLIENT,
        HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
        HabilitationsEnum.VALIDATION_CLAUSE_CLIENT],
      submenu: {
        items: [
          {
            title: 'Clause',
            page: '/agent/client/clause/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
              HabilitationsEnum.VALIDATION_CLAUSE_CLIENT
            ],
            submenu: {
              items: [
                {
                  title: 'Liste des clauses',
                  icon: 'la la-list',
                  page: '/agent/client/clause/dashboard',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
                    HabilitationsEnum.VALIDATION_CLAUSE_CLIENT
                  ],
                },
                {
                  title: 'Création Clause',
                  icon: 'la la-pencil',
                  page: '/agent/client/clause/creation',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
                },
                {
                  title: 'Exporter',
                  icon: 'la la-list',
                  page: '/agent/client/clause/export',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
                    HabilitationsEnum.VALIDATION_CLAUSE_CLIENT
                  ],
                },
              ]
            }
          },
          {
            title: 'Canevas',
            page: '/agent/client/canevas/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT,
              HabilitationsEnum.VALIDATION_CANEVAS_CLIENT
            ],
            submenu: {
              items: [
                {
                  title: 'Liste des canevas',
                  icon: 'la la-list',
                  page: '/agent/client/canevas/dashboard',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT,
                    HabilitationsEnum.VALIDATION_CANEVAS_CLIENT
                  ],
                },
                {
                  title: 'Création Canevas',
                  icon: 'la la-pencil',
                  page: '/agent/client/canevas/creation',
                  roles: [
                    HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT
                  ]
                }
              ]
            }
          },

        ]
      }
    },
    {
      title: 'Préparation|Rédaction',
      icon: 'la la-edit',
      roles: ['*'],
      submenu: {
        items: [
          {
            title: 'Clauses personnalisées',
            page: '/agent/preparation-redaction/clauses/dashboard',
            roles: ['*'],
            submenu: {
              items: [
                {
                  title: 'Mes clauses',
                  page: '/agent/preparation-redaction/clauses/dashboard',
                  roles: ['*']
                }
              ]
            }
          }
        ]
      }
    },
    {
      title: 'Clauses personnalisées',
      icon: 'la la-edit',
      roles: ['*'],
      submenu: {
        items: [
          {
            title: 'Clauses',
            page: '/agent/clauses-personnalisees/clauses/dashboard',
            roles: ['*']
          }
        ]
      }
    },
    {
      title: 'Documents administrables',
      icon: 'la la-file',
      roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT,
        HabilitationsEnum.VALIDATION_CANEVAS_CLIENT,
        HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
        HabilitationsEnum.VALIDATION_CLAUSE_CLIENT],
      submenu: {
        items: [
          {
            title: 'Documents modèles',
            page: '/agent/document-modele/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT,
              HabilitationsEnum.VALIDATION_CLAUSE_CLIENT
            ]
          },
          {
            title: 'Champs de fusion complexes',
            page: '/agent/champ-fusion-complexe/dashboard',
            roles: [
              HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT,
              HabilitationsEnum.VALIDATION_CANEVAS_CLIENT
            ]
          },

        ]
      }
    }
  ]
}


export const ThemeSettingsConfig = {
  colorTheme: 'semi-dark', // light, semi-light, semi-dark, dark
  layout: {
    style: 'horizontal', // style: 'vertical', horizontal,
    pattern: 'fixed' // fixed, boxed, static
  },
  menuColor: 'menu-dark', // Vertical: [menu-dark, menu-light] , Horizontal: [navbar-dark, navbar-light]
  navigation: 'menu-collapsible', // menu-collapsible, menu-accordation
  menu: 'expand', // collapse, expand
  header: 'static', // fix, static
  footer: 'static', // fix, static
  headerIcons: {
    maximize: 'off', // on, off
    search: 'off', // on, off
    internationalization: 'on', // on, off
    notification: 'off', // on, off
    email: 'off' // on, off
  },
  brand: {
    brand_name: 'Redac Clausier',
    router: 'http://www.atexo.com/',
    logo: {
      type: 'url',
      value: 'assets/images/logo/atexo.png'
    },
  },
  defaultTitleSuffix: 'Eforms'
};
