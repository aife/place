import {ApplicationRef, DoBootstrap, Injector, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbCarouselConfig, NgbModalConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// Routing
import {routing} from './app.routing';
// Components
import {AppComponent} from './app.component';
// perfect scroll bar
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
// spinner
import {NgxSpinnerModule} from 'ngx-spinner';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';

import {DeviceDetectorModule} from 'ngx-device-detector';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF, CommonModule, PlatformLocation} from '@angular/common';
import {enGbLocale, frLocale} from 'ngx-bootstrap/locale';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {TranslateLoader, TranslateModule, TranslateService, TranslateStore} from '@ngx-translate/core';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {BlockUIService} from "ng-block-ui";
import {createCustomElement} from "@angular/elements";
import {ElementZoneStrategyFactory} from "elements-zone-strategy";
import {BibliotequeCommuneModule} from "@shared-global/app.module";
import {LoadingBarModule} from "@ngx-loading-bar/core";
import {ComponentsModule} from "@shared-global/components/components.module";
import {PublicWcModule} from "@pages-wc/public/public-wc.module";
import {PrivateWcModule} from "@pages-wc/private/private-wc.module";
import {ClausierCoreModule} from "@core-clausier/clausier-core.module";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {forkJoin, Observable} from "rxjs";
import {map} from "rxjs/operators";
import * as _ from 'lodash';

defineLocale('en', enGbLocale);
defineLocale('fr', frLocale);

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

export class CustomLoader implements TranslateLoader {

  constructor(private http: HttpClient, private platformLocation: PlatformLocation,
              private readonly referentielsService: ReferentielsService) {
  }



  public getTranslation(lang: string): Observable<any> {
    let api = this.referentielsService.getApiI18n(lang).pipe();
    let app = this.referentielsService.getAppI18n(this.platformLocation.getBaseHrefFromDOM(), lang).pipe();
    return forkJoin(app, api).pipe(
      map(([a, b,]) => {

        // @ts-ignore
        return _.merge(a, b)
      }));

  }
}


@NgModule({
  imports: [
    ClausierCoreModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    routing,
    PerfectScrollbarModule,
    NgxSpinnerModule,
    DeviceDetectorModule.forRoot(),
    LoadingBarRouterModule,
    PublicWcModule,
    PrivateWcModule,
    CommonModule,
    BibliotequeCommuneModule,
    LoadingBarModule,
    ComponentsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, PlatformLocation, ReferentielsService]
      }, isolate: true
    })
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    BsModalRef,
    TranslateStore,
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseHref,
      deps: [PlatformLocation]
    },
    {provide: LOCALE_ID, useValue: 'en-EN'},
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerGestureConfig
    },
    NgbCarouselConfig,
    NgbModalConfig,
    BlockUIService,
    NgbCarouselConfig,
    NgbModalConfig,
    BsModalService
  ],
  entryComponents: [],
  exports: [RouterModule]
})

export class AppModule implements DoBootstrap {

  constructor(private injector: Injector, private bsLocaleService: BsLocaleService, private translateService: TranslateService) {
    let lang = localStorage.getItem("clausier-language");
    if (!lang) {
      lang = 'fr';
      localStorage.setItem("clausier-language", "fr")
    }
    this.translateService.use(lang);
    this.bsLocaleService.use(lang);
    const strategyFactory = new ElementZoneStrategyFactory(AppComponent, injector);
    const webComponent = createCustomElement(AppComponent, {injector, strategyFactory});
    customElements.define('clausier-app', webComponent);
  }

  ngDoBootstrap(appRef: ApplicationRef): void {
    if (document.querySelector('app-main')) {
      appRef.bootstrap(AppComponent);
    }

  }
}
