import {AppInternalPathEnum} from '@shared-global/core/enums/app-internal-path.enum';

export const BreadcrumbParamsConst = {
  EDITEUR_CANEVAS_DASHBOARD: {
    'mainlabel': 'editeur.canevas.administration',
    'links': [
      {
        'name': 'editeur.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS
      },
      {
        'name': 'editeur.canevas.dashboard',
        'isLink': false
      }
    ]
  },
  CLIENT_CANEVAS_DASHBOARD: {
    'mainlabel': 'client.canevas.administration',
    'links': [
      {
        'name': 'client.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS
      },
      {
        'name': 'client.canevas.dashboard',
        'isLink': false
      }
    ]
  },
  EDITEUR_CLAUSE_DASHBOARD: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'editeur.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE
      },
      {
        'name': 'editeur.clause.dashboard',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_DASHBOARD: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.dashboard',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_EXPORT: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.exporter',
        'isLink': false
      }
    ]
  },
  EDITEUR_CLAUSE_EXPORT: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'editeur.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE
      },
      {
        'name': 'editeur.clause.exporter',
        'isLink': false
      }
    ]
  },
  EDITEUR_CANEVAS_CREATION: {
    'mainlabel': 'editeur.canevas.administration',
    'links': [
      {
        'name': 'editeur.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS
      },
      {
        'name': 'editeur.canevas.creation',
        'isLink': false
      }
    ]
  },
  CLIENT_CANEVAS_CREATION: {
    'mainlabel': 'client.canevas.administration',
    'links': [
      {
        'name': 'client.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS
      },
      {
        'name': 'client.canevas.creation',
        'isLink': false
      }
    ]
  },
  EDITEUR_CLAUSE_CREATION: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'editeur.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE
      },
      {
        'name': 'editeur.clause.creation',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_CREATION: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.creation',
        'isLink': false
      }
    ]
  },
  EDITEUR_CANEVAS_MODIFICATION: {
    'mainlabel': 'editeur.canevas.administration',
    'links': [
      {
        'name': 'editeur.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS
      },
      {
        'name': 'editeur.canevas.modification',
        'isLink': false
      }
    ]
  },
  CLIENT_CANEVAS_MODIFICATION: {
    'mainlabel': 'client.canevas.administration',
    'links': [
      {
        'name': 'client.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS
      },
      {
        'name': 'client.canevas.modification',
        'isLink': false
      }
    ]
  },
  EDITEUR_CLAUSE_MODIFICATION: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'editeur.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE
      },
      {
        'name': 'editeur.clause.modification',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_MODIFICATION: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.modification',
        'isLink': false
      }
    ]
  },
  EDITEUR_CANEVAS_DUPLICATION: {
    'mainlabel': 'editeur.canevas.administration',
    'links': [
      {
        'name': 'editeur.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS
      },
      {
        'name': 'editeur.canevas.duplication',
        'isLink': false
      }
    ]
  },
  CLIENT_CANEVAS_DUPLICATION: {
    'mainlabel': 'client.canevas.administration',
    'links': [
      {
        'name': 'client.canevas.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS
      },
      {
        'name': 'client.canevas.duplication',
        'isLink': false
      }
    ]
  },
  EDITEUR_CLAUSE_DUPLICATION: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'editeur.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE
      },
      {
        'name': 'editeur.clause.duplication',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_DUPLICATION: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.duplication',
        'isLink': false
      }
    ]
  },
  CLIENT_CLAUSE_SURCHARGE: {
    'mainlabel': 'client.clause.administration',
    'links': [
      {
        'name': 'client.clause.name',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'client.clause.surcharge',
        'isLink': false
      }
    ]
  },
  VERSION_DASHBOARD: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'menuGauche.administration.publicationClausier2',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_VERSION
      },
      {
        'name': 'clause.txt.historiqueVersions',
        'isLink': false
      }
    ]
  },
  VERSION_CREATION: {
    'mainlabel': 'editeur.clause.administration',
    'links': [
      {
        'name': 'menuGauche.administration.publicationClausier2',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_VERSION
      },
      {
        'name': 'menuGauche.administration.publicationClausier.creerVersion',
        'isLink': false
      }
    ]
  },
  PARAMETRE_AGENT_CLAUSE_DASHBOARD: {
    'mainlabel': 'menuGauche.administrationTitre',
    'links': [
      {
        'name': 'menuGauche.administration.preferences.agent',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_AGENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'menuGauche.administration.personnaliseesAgent',
        'isLink': false
      }
    ]
  },
  PARAMETRE_DIRECTION_CLAUSE_DASHBOARD: {
    'mainlabel': 'menuGauche.administrationTitre',
    'links': [
      {
        'name': 'menuGauche.administration.preferences.direction',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_DIRECTION_DASHBOARD_CLAUSE
      },
      {
        'name': 'menuGauche.administration.personnaliseesDirection',
        'isLink': false
      }
    ]
  },
  PARAMETRAGE_AGENT_CLAUSE: {
    'mainlabel': 'menuGauche.administrationTitre',
    'links': [
      {
        'name': 'menuGauche.administration.preferences.agent',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_AGENT_DASHBOARD_CLAUSE
      },
      {
        'name': 'menuGauche.administration.preferences.parametrage',
        'isLink': false
      }
    ]
  },
  PARAMETRAGE_DIRECTION_CLAUSE: {
    'mainlabel': 'menuGauche.administrationTitre',
    'links': [
      {
        'name': 'menuGauche.administration.preferences.direction',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRE_DIRECTION_DASHBOARD_CLAUSE
      },
      {
        'name': 'menuGauche.administration.preferences.parametrage',
        'isLink': false
      }
    ]
  },
  DOCUMENT_ADMINISTRABLE: {
    'mainlabel': 'doc-administrable.mainlabel',
    'links': [
      {
        'name': 'doc-administrable.menuGauche',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_DOCUMENT_ADMINISTRABLE
      },
      {
        'name': 'doc-administrable.menuDroite',
        'isLink': false
      }
    ]
  },
  CHAMP_FUSION_COMPLEXE: {
    'mainlabel': 'champ-fusion.mainlabel',
    'links': [
      {
        'name': 'champ-fusion.menuGauche',
        'isLink': true,
        'link': AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.DASHBOARD_CHAMP_FUSION_COMPLEXE
      },
      {
        'name': 'champ-fusion.menuDroite',
        'isLink': false
      }
    ]
  },

};
