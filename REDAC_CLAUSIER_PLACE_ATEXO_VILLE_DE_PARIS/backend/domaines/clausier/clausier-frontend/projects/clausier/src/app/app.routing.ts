import {RouterModule, Routes} from '@angular/router';
import {PrivateLayoutComponent} from "@core-clausier/layout/private-layout/private-layout.component";
import {PublicLayoutComponent} from "@core-clausier/layout/public-layout/public-layout.component";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AccueilPageWCComponent} from "@pages-wc/private/accueil-page-wc/accueil-page-wc.component";
import {LoginPageWCComponent} from "@pages-wc/public/login-page-wc/login-page-wc.component";
import {FormCanevasComponent} from "@pages-wc/private/form-canevas/form-canevas.component";
import {JspRouterWcComponent} from "@pages-wc/public/jsp-router-wc/jsp-router-wc.component";
import {FormClauseComponent} from "@pages-wc/private/form-clause/form-clause.component";
import {SurchargeClauseComponent} from "@pages-wc/private/surcharge-clause/surcharge-clause.component";
import {DashboardVersionsWcComponent} from "@pages-wc/private/dashboard-versions-wc/dashboard-versions-wc.component";
import {FormVersionComponent} from "@pages-wc/private/form-version/form-version.component";
import {
  DashboardClausesClientWcComponent
} from "@pages-wc/private/dashboard-clauses-client-wc/dashboard-clauses-client-wc.component";
import {
  DashboardClausesEditeurWcComponent
} from "@pages-wc/private/dashboard-clauses-editeur-wc/dashboard-clauses-editeur-wc.component";
import {
  DashboardCanevasEditeurWcComponent
} from "@pages-wc/private/dashboard-canevas-editeur-wc/dashboard-canevas-editeur-wc.component";
import {
  DashboardCanevasClientWcComponent
} from "@pages-wc/private/dashboard-canevas-client-wc/dashboard-canevas-client-wc.component";
import {HabilitationsEnum} from "@shared-global/core/enums/habilitations.enum";
import {
  ExportClausesClientWcComponent
} from "@pages-wc/private/export-clauses-client-wc/export-clauses-client-wc.component";
import {
  ExportClausesEditeurWcComponent
} from "@pages-wc/private/export-clauses-editeur-wc/export-clauses-editeur-wc.component";
import {
  DashboardClausesParametreAgentWcComponent
} from "@pages-wc/private/dashboard-clauses-parametre-agent-wc/dashboard-clauses-parametre-agent-wc.component";
import {ParametrageClauseComponent} from "@pages-wc/private/parametrage-clause/parametrage-clause.component";
import {
  DashboardClausesParametreDirectionWcComponent
} from "@pages-wc/private/dashboard-clauses-parametre-direction-wc/dashboard-clauses-parametre-direction-wc.component";
import {
  DashboardDocumentsAdministrablesWcComponent
} from "@pages-wc/private/dashboard-documents-administrables-wc/dashboard-documents-administrables-wc.component";
import {
  DashboardChampsFusionComplexesWcComponent
} from "@pages-wc/private/dashboard-champs-fusion-complexes-wc/dashboard-champs-fusion-complexes-wc.component";


const appRoutes: Routes = [

  // Private layout
  {
    path: 'agent',
    component: PrivateLayoutComponent,
    children: [
      {
        path: AppInternalPathEnum.ACCUEIL,
        component: AccueilPageWCComponent,
        canActivate: [AuthGuard]
      },
      {
        path: AppInternalPathEnum.EDITEUR_DASHBOARD_CANEVAS,
        component: DashboardCanevasEditeurWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR, HabilitationsEnum.VALIDATION_CANEVAS_EDITEUR]
        }
      }, {
        path: AppInternalPathEnum.CLIENT_DASHBOARD_CANEVAS,
        component: DashboardCanevasClientWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT, HabilitationsEnum.VALIDATION_CANEVAS_CLIENT]
        }
      },
      {
        path: AppInternalPathEnum.DASHBOARD_VERSION,
        component: DashboardVersionsWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR, HabilitationsEnum.ACTIVATION_VERSION_CLAUSIER_EDITEUR]
        }
      },
      {
        path: AppInternalPathEnum.DASHBOARD_DOCUMENT_ADMINISTRABLE,
        component: DashboardDocumentsAdministrablesWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR] //à modifier
        }
      },
      {
        path: AppInternalPathEnum.DASHBOARD_CHAMP_FUSION_COMPLEXE,
        component: DashboardChampsFusionComplexesWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR] //à modifier
        }
      },
      {
        path: AppInternalPathEnum.EDITEUR_DASHBOARD_CLAUSE,
        component: DashboardClausesEditeurWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR, HabilitationsEnum.VALIDATION_CLAUSE_EDITEUR]
        }
      },
      {
        path: AppInternalPathEnum.PARAMETRE_AGENT_DASHBOARD_CLAUSE,
        component: DashboardClausesParametreAgentWcComponent,
        canActivate: [AuthGuard],
      },
      {
        path: AppInternalPathEnum.PARAMETRAGE_CLAUSE,
        component: ParametrageClauseComponent,
        canActivate: [AuthGuard]
      },
      {
        path: AppInternalPathEnum.PARAMETRE_DIRECTION_DASHBOARD_CLAUSE,
        component: DashboardClausesParametreDirectionWcComponent,
        canActivate: [AuthGuard]
      },
      {
        path: AppInternalPathEnum.CLIENT_DASHBOARD_CLAUSE,
        component: DashboardClausesClientWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT, HabilitationsEnum.VALIDATION_CLAUSE_CLIENT]
        }
      },
      {
        path: AppInternalPathEnum.CREATION_VERSION,
        component: FormVersionComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.PUBLICATION_VERSION_CLAUSE_EDITEUR, HabilitationsEnum.PUBLICATION_VERSION_CLAUSIER_EDITEUR]
        }
      },
      {
        path: AppInternalPathEnum.TYPE_CREATION_CANEVAS,
        component: FormCanevasComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT, HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR]
        }
      },
      {
        path: AppInternalPathEnum.TYPE_CREATION_CLAUSE,
        component: FormClauseComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR, HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
        }
      },
      {
        path: AppInternalPathEnum.FORM_CANEVAS,
        component: FormCanevasComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CANEVAS_CLIENT, HabilitationsEnum.ADMINISTRATION_CANEVAS_EDITEUR]
        }
      },
      {
        path: AppInternalPathEnum.FORM_CLAUSE,
        component: FormClauseComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR, HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
        }
      }, {
        path: AppInternalPathEnum.SURCHARGE_CLIENT_CLAUSE,
        component: SurchargeClauseComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
        }
      }, {
        path: AppInternalPathEnum.CLIENT_EXPORT_CLAUSE,
        component: ExportClausesClientWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_CLIENT]
        },
      },
      {
        path: AppInternalPathEnum.EDITEUR_EXPORT_CLAUSE,
        component: ExportClausesEditeurWcComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [HabilitationsEnum.ADMINISTRATION_CLAUSE_EDITEUR]
        }
      },
      {
        path: '',
        redirectTo: AppInternalPathEnum.ACCUEIL,
        pathMatch: 'prefix'
      }
    ],
  },// Public layout
  {
    path:  AppInternalPathEnum.LOGIN,
    component: PublicLayoutComponent,
    children: [
      {
        path: ':token',
        component: LoginPageWCComponent, canActivate: [NotConnectedGuard]
      },
      {
        path: ':token/:refeshToken',
        component: LoginPageWCComponent, canActivate: [NotConnectedGuard]
      }, {
        path: '',
        component: LoginPageWCComponent,
        canActivate: [NotConnectedGuard]
      },
    ]
  },
  {
    path: AppInternalPathEnum.JSP_REGEX,
    component: JspRouterWcComponent,
  },
  {path: '', redirectTo: '/' + AppInternalPathEnum.LOGIN, pathMatch: 'full'},
  {path: '**', redirectTo: '/' + AppInternalPathEnum.LOGIN, pathMatch: 'full'}

];

export const routing = RouterModule.forRoot(appRoutes, {
  scrollOffset: [0, 0],
  scrollPositionRestoration: 'top',
  relativeLinkResolution: 'legacy'
});
