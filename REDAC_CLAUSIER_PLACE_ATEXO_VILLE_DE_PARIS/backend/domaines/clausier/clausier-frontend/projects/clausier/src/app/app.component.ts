import {MenuSettingsService} from '@core-clausier/layout/settings/menu-settings.service';
import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {LoadingBarService} from '@ngx-loading-bar/core';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationStart,
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Router
} from '@angular/router';
import {DeviceDetectorService} from 'ngx-device-detector';
import {DOCUMENT} from '@angular/common';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AppConstants} from "@core-clausier/constantes/app.constants";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {ThemesService} from "@shared-global/core/services/themes.service";

@Component({
  selector: 'app-main',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable()
export class AppComponent implements OnInit {
  public _menuSettingsConfig: any;
  public _unsubscribeAll: Subject<any>;
  private _unsubscribeAllMenu: Subject<any>;
  public title;


  constructor(private spinner: NgxSpinnerService,
              private readonly store: Store<State>,
              @Inject(DOCUMENT) private document: Document,
              private router: Router,
              private themesService: ThemesService,
              public loader: LoadingBarService,
              private deviceService: DeviceDetectorService,
              public _menuSettingsService: MenuSettingsService
  ) {
    this._unsubscribeAll = new Subject();
    this._unsubscribeAllMenu = new Subject();

  }

  ngOnInit() {
    this.store.select(state => state.userReducer).subscribe(value => {
      if (value.connected) {
        this._menuSettingsService.config
          .pipe(takeUntil(this._unsubscribeAllMenu))
          .subscribe((config) => {
            this._menuSettingsConfig = config;
          });

        this.subscribePageProgressBar();
        //this.router.navigate([value.defaultPage]);

        this.applyRemoteCss()
      }
    })

  }

  async applyRemoteCss() {
    try {
      const cssContent = await this.themesService.getRemoteCss();
      if (!!cssContent && cssContent !== '') {
        const styleElement = document.createElement('style');
        styleElement.innerHTML = cssContent;
        document.head.appendChild(styleElement);
      }
    } catch (error) {
      console.error('Error fetching remote CSS:', error);
    }
  }

  private subscribePageProgressBar() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // set page progress bar loading to start on NavigationStart event router
        this.loader.start();
      }
      if (event instanceof RouteConfigLoadStart) {
        this.loader.increment(35);
      }
      if (event instanceof RouteConfigLoadEnd) {
        this.loader.increment(75);
      }
      if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        // set page progress bar loading to end on NavigationEnd event router
        this.loader.complete();
        if (this.deviceService.isMobile() || window.innerWidth < AppConstants.MOBILE_RESPONSIVE_WIDTH) {
          if (document.body.classList.contains('menu-open')) {
            document.body.classList.remove('menu-open');
            document.body.classList.add('menu-close');
          }
        }
      }
    });
  }
}
