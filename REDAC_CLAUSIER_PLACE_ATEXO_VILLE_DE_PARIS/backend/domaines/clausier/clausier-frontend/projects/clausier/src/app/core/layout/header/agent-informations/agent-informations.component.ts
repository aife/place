import {Component, Input} from '@angular/core';
import {Agent} from "@shared-global/core/models/api/clausier-core.api";

@Component({
  selector: 'atx-agent-informations',
  templateUrl: './agent-informations.component.html',
  styleUrls: ['./agent-informations.component.css']
})
export class AgentInformationsComponent {

  @Input()
  agent: Agent;


}
