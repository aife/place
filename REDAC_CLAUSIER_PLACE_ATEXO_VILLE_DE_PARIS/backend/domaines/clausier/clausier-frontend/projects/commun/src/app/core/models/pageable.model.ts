export class PageableModel {
  public size: number = 25;
  public page: number = 0;
  public sortField?: string;
  public asc?: boolean;
}
