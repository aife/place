import {CanevasBean, Directory} from "@shared-global/core/models/api/clausier.api";

export class CanevasDetailsModel implements CanevasBean {
  dateDerniereValidation: Date;
  datePremiereValidation: Date;
  codeTypeDocument: string;
  surchargeActif: boolean;
  referenceClauseSurchargee: string;
  idClauseOrigine: number;
  checked: boolean;
  actif: boolean;
  canevasEditeur: boolean;
  compatibleEntiteAdjudicatrice: boolean;
  dateCreation: Date;
  dateModification: Date;
  dateModificationMax: Date;
  dateModificationMin: Date;
  etat: number;
  id: number;
  idCanevas: number;
  idLastPublication: number;
  idNaturePrestation: number;
  idOrganisme: number;
  idProcedure: number;
  idPublication: number;
  idRefCCAG: number;
  idStatutRedactionClausier: number;
  idTypeContrat: number;
  idTypeDocument: number;
  idsProcedures: number[];
  idsTypeContrats: number[];
  labelNaturePrestation: string;
  labelTypeAuteur: string;
  labelTypeDocument: string;
  labelsProcedures: string[];
  labelsTypeContrats: string[];
  lastVersion: string;
  referenceCanevas: string;
  referenceClause: string;
  titre: string;
  typeAuteur: number;
  procedures: Array<Directory>;
  typesContrat: Array<Directory>;
  ccag: Directory;
}

