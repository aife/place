import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {DocumentAdministrable, Produit} from "@shared-global/core/models/api/clausier-core.api";
import {CreateDocumentAdministrableModel} from "@shared-global/core/models/create-document-administrable.model";
import {EditionRequestModel} from "@shared-global/core/models/edition-request.model";
import {APP_BASE_HREF} from "@angular/common";


@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  private RELATIVE_PATH = '/';
  private readonly BASE_PATH = 'clausier-api/documents-administrables/';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }

  listDocumentsAdministrables(champFusion: boolean, produits: string[]) {
    let params: any = {
      champFusion: champFusion,
      produits: produits
    };
    return this.http.get<Array<DocumentAdministrable>>(`${this.RELATIVE_PATH}${this.BASE_PATH}`, {
      params: params
    });
  }


  saveDocumentAdministrable(file: File, documentAdministrable: CreateDocumentAdministrableModel) {
    const formData = new FormData();
    console.log("FILE ", file)
    if (!!file) {
      formData.append('fichier', file);
    }
    formData.append("documentAdministrable", new Blob([JSON.stringify(documentAdministrable)], {
      type: "application/json"
    }))
    return this.http.post<CreateDocumentAdministrableModel>(this.RELATIVE_PATH+this.BASE_PATH, formData);
  }

  editDocument(documentId: number, produit: Produit): Observable<EditionRequestModel> {
    return this.http.post<EditionRequestModel>(`${this.RELATIVE_PATH}${this.BASE_PATH}${documentId}/document-edit`, produit);
  }

  disableDocument(documentId: number) {
    return this.http.patch<DocumentAdministrable>(`${this.RELATIVE_PATH}${this.BASE_PATH}${documentId}/desactiver`, null);
  }

  downloadTemplate(documentId: number) {
    return this.http.get<DocumentAdministrable>(`${this.RELATIVE_PATH}${this.BASE_PATH}${documentId}/download`, {
      // @ts-ignore
      responseType: 'blob'
    });
  }

  reinitialiserDocument(documentId: number) {
    return this.http.patch<DocumentAdministrable>(`${this.RELATIVE_PATH}${this.BASE_PATH}${documentId}/reinitialiser`, null);
  }
}
