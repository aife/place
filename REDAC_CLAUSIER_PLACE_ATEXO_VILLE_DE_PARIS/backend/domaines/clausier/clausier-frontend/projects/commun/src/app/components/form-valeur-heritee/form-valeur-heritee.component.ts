import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {Observable} from "rxjs";
import {ClauseBean, Directory} from "@shared-global/core/models/api/clausier.api";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {ClauseValeurHeriteeFormModel} from "@shared-global/core/models/ClauseValeurHeriteeForm.model";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-valeur-heritee',
  templateUrl: './form-valeur-heritee.component.html',
  styleUrls: ['./form-valeur-heritee.component.scss']
})
export class FormValeurHeriteeComponent extends BaseFormQuillComponent implements OnInit {

  @Input() clause: ClauseBean;
  @Input() disabled: boolean;
  refValeurHeritee$: Observable<Array<Directory>>;
  submitted: boolean;
  sautLigne1: boolean = false;
  sautLigne2: boolean = false;
  defaultTextColor = 'black';

  constructor(private readonly store: Store<State>) {
    super();
  }


  public ngOnInit(): void {
    this.refValeurHeritee$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.VALEUR_HERITEE)?.content);

    //modification
    if (!!this.clause.clauseValeurHeritee) {
      this.submitted = true;
      if (this.clause.clauseValeurHeritee.sautTextFixeAvant === "true") {
        this.sautLigne1 = true;
      }
      if (this.clause.clauseValeurHeritee.sautTextFixeApres === "true") {
        this.sautLigne2 = true;
      }

    } else { //creation
      this.clause.clauseValeurHeritee = new ClauseValeurHeriteeFormModel();
      this.clause.clauseValeurHeritee.textFixeAvant = "";
      this.clause.clauseValeurHeritee.textFixeApres = "";
      this.clause.clauseValeurHeritee.sautTextFixeAvant = String(false);
      this.clause.clauseValeurHeritee.sautTextFixeApres = String(false);

    }
  }

  updateSautLigne1() {
    this.clause.clauseValeurHeritee.sautTextFixeAvant = String(this.sautLigne1);
  }

  updateSautLigne2() {
    this.clause.clauseValeurHeritee.sautTextFixeApres = String(this.sautLigne2);
  }

  cleanTexteAvant() {
    this.clause.clauseValeurHeritee.textFixeAvant = this.removeTagForEmptyField(this.clause.clauseValeurHeritee.textFixeAvant);
  }

  cleanTexteApres() {
    this.clause.clauseValeurHeritee.textFixeApres = this.removeTagForEmptyField(this.clause.clauseValeurHeritee.textFixeApres);
  }

}
