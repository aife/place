import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {State} from "@shared-global/store"
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {Agent} from "@shared-global/core/models/api/clausier-core.api";
import {AuthenticationService} from "@shared-global/core/services/authentification.service";
import _ from "lodash";
import {BsModalService} from "ngx-bootstrap/modal";
import {ModalAccess403Component} from "@shared-global/components/modal-access-403/modal-access-403.component";

@Injectable()
export class AuthGuard implements CanActivate {
  user!: Agent;
  userRoles = []

  constructor(private modalService: BsModalService, private routingInterneService: RoutingInterneService, private readonly store: Store<State>, private readonly authenticationService: AuthenticationService) {
    this.store.select(state => state.userReducer.user).subscribe(value => {
      this.user = value
      if (!!value) {
        this.userRoles = value.roles;
      }
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.user) {
      let b = !(route.data.roles && _.intersection(route.data.roles, this.userRoles).length === 0);
      if (!b) {
        let config = {
          backdrop: true,
          ignoreBackdropClick: true,
          class: "modal-md"
        };
        this.modalService.show(ModalAccess403Component, config);
      }
      return b;
    }
    return false;
  }
}
