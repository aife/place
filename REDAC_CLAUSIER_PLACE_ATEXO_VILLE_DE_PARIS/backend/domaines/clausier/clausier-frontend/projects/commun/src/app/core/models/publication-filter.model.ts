import {PublicationClausierSearch} from "@shared-global/core/models/api/clausier.api";

export class PublicationFilterModel implements PublicationClausierSearch {
  asc: boolean;
  datePubicationMax: Date;
  datePublicationMin: Date;
  gestionLocale: number;
  page: number;
  size: number;
  sortField: string;
  version: string;

    constructor() {
        this.gestionLocale = 0;
        this.version = null;
    }


}
