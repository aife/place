import {createReducer, on} from "@ngrx/store";
import {clearAlert, showErrorAlert, showInfoAlert, showSuccessAlert} from "@shared-global/store/alert/alert.action";

export interface AlertState {
  alert: {
    messages: Array<string>,
    type: string,
    icon: string,
    keepAfterNavigationChange: boolean
  }
}

const initialState: AlertState = {
  alert: null
};


const _alertReducer = createReducer(
  initialState,
  on(showSuccessAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "success"
      }
    }
  }),
  on(showErrorAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "danger"
      }
    }
  }),
  on(showInfoAlert, (state, props) => {
    return {
      ...state,
      alert: {
        ...props,
        type: "info"
      }
    }
  }), on(clearAlert, (state) => {
    return {
      ...state,
      alert: null
    }
  })
);

export function alertReducer(state, action) {
  return _alertReducer(state, action);
}
