import {Canevas, CanevasChapitre, Chapitre} from "@shared-global/core/models/api/clausier.api";
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";

export class CanevasModel implements Canevas {
  auteur: string;
  canevasEditeur: boolean;
  chapitres: ChapitreModel[];
  compatibleEntiteAdjudicatrice: boolean;
  creeLe: string;
  derogationActive: boolean;
  derogationDevenirInactif: boolean;
  editeur: boolean;
  idCanevas: number;
  idOrganisme: number;
  idPublication: number;
  idRefCCAG: number;
  idStatutRedactionClausier: number;
  idTypePassation: number;
  lastVersion: string;
  message: string;
  modifieLe: string;
  naturePrestation: number;
  numero: string;
  parent: CanevasChapitre;
  procedurePassationList: number[];
  referenceCanevas: string;
  referenceInitialDuplication: string;
  statut: boolean;
  titre: string;
  typeContratList: number[];
  typeDocument: number;

}
