import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageableModel} from "@shared-global/core/models/pageable.model";
import {ClauseBean, ClauseSearch, PageRepresentation} from "@shared-global/core/models/api/clausier.api";
import {ClauseFilterModel} from "@shared-global/core/models/clause-filter.model";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {
  ModalPreviewClauseComponent
} from "@shared-global/components/modal-preview-clause/modal-preview-clause.component";
import {BsModalService} from "ngx-bootstrap/modal";


@Component({
  selector: 'atx-select-clauses',
  templateUrl: './select-clauses.component.html',
  styleUrls: ['./select-clauses.component.css'],

})
export class SelectClausesComponent implements OnInit {

  param: any;
  pageable: PageableModel = {page: 0, size: 10, asc: false, sortField: 'dateModification'};
  loading = false;
  total = 0;
  page: PageRepresentation<ClauseBean>;
  clauseList: Array<ClauseBean> = [];

  filter: ClauseSearch;

  @Output() onChangeSelectedList = new EventEmitter<ClauseBean[]>();

  private _selectedClauses: ClauseBean[];

  @Input()
  public set selectedClauses(value: ClauseBean[]) {
    this._selectedClauses = value;
  }

  public get selectedClauses() {
    return this._selectedClauses;
  }

  @Input() set editeur(editeur: boolean) {
    this._editeur = editeur;
    this.initEditeurOrClient()
  }

  @Input() set refresh(refresh: Date) {
    this.initEditeurOrClient();
  }

  get editeur() {
    return this._editeur;
  }

  private _editeur;

  constructor(protected modalService: BsModalService, private readonly clauseService: ClauseService) {

  }

  ngOnInit(): void {


  }

  private initEditeurOrClient() {

    this.filter = new ClauseFilterModel(this.editeur);
    this.filter.idStatutRedactionClausier = 3;
    this.filter.actif = true;
    this.filter.editeur = this.editeur;
    this.searchPage(this.filter, this.pageable);

  }

  searchPage(filter: ClauseFilterModel, pageable) {
    this.loading = true;
    this.clauseService.list(filter, pageable).subscribe(value => {
      this.loading = false;
      this.page = value;
      this.total = value.totalElements;
      this.clauseList = this.page.content;
      this.clauseList.forEach(item => {
        item.checked = this.selectedClauses?.findIndex(value => {
          return value.id === item.id
        }) > -1;
      })
    }, () => {
      this.clauseList = [];
      this.page = null;
      this.total = null;
      this.loading = false;
    });

  }

  getEtat(field: string) {
    if (this.pageable?.sortField !== field) {
      return '';
    }
    if (this.pageable.asc) {
      return '-asc'
    }
    return '-desc'


  }

  changeSort(field: string) {
    if (this.pageable?.sortField === field) {
      this.pageable.asc = !this.pageable?.asc;
    } else {
      this.pageable.sortField = field;
      this.pageable.asc = false;
    }
    this.searchPage(this.filter, this.pageable)
  }

  selectAll(checked: boolean) {
    this.clauseList.forEach(value => value.checked = checked)
    this.onChangeSelectedList.emit(this.clauseList.filter(value => value.checked))
  }

  updateAll(clause: ClauseBean) {
    if (clause.checked) {
      if (!this.clauseList.includes(clause)) {
        this.clauseList.push(clause)
      }
      if (!this.selectedClauses.includes(clause)) {
        this.selectedClauses.push(clause)
      }
    } else {
      this.deleteClause(clause);
    }
    this.onChangeSelectedList.emit(this.selectedClauses)
  }

  preview(clause: ClauseBean) {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let bsModal = this.modalService.show(ModalPreviewClauseComponent, config);
    bsModal.content.titre = 'redaction.clause.previsualiser.titre';
    bsModal.content.editeur = this.editeur;
    bsModal.content.surcharge = clause.typeAuteur === 3;
    bsModal.content.clauseBean = clause;
  }


  get checked() {
    return this.clauseList.findIndex(value => !value.checked) === -1;
  }

  deleteClause(clause: ClauseBean) {
    this.selectedClauses = this.selectedClauses.filter(item =>
      item.idClause !== clause.idClause
    );
    this.onChangeSelectedList.emit(this.selectedClauses)
    this.clauseList.forEach(item => {
      item.checked = this.selectedClauses?.findIndex(value => {
        return value.id === item.id
      }) > -1;
    })
  }
}


