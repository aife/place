import {BaseBean, Directory} from "@shared-global/core/models/api/clausier.api";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {AbstractService} from "@shared-global/core/services/abstract.service";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";
import {ChangeDetectorRef} from "@angular/core";


export class BaseDetailsComponent<T extends BaseBean, V, P> {
  protected _model: T;
  protected _editeur: boolean = true;
  protected _type: 'canevas' | 'clause'
  protected refStatut: Array<Directory>;
  protected refNature: Array<Directory>;
  bsModalRef!: BsModalRef;

  constructor(protected modalService: BsModalService, protected changeDetectorRef: ChangeDetectorRef, protected router: RoutingInterneService, protected readonly baseService: AbstractService<T, V, P>,
              protected readonly store: Store<State>,
              protected formPath: string) {

    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.STATUT)?.content)
      .subscribe(value => {
        this.refStatut = value;
      });
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content).subscribe(value => {
      this.refNature = value;
    });
  }

  getStatutBadge(statut: number) {
    if (this._model.idStatutRedactionClausier === statut) {
      switch (statut) {
        case 1:
          return "badge badge-pill badge-glow badge-danger badge-square margin-right-5px";
          break;
        case 2:
          return "badge badge-pill badge-glow badge-warning badge-square margin-right-5px";
          break;
        case 3:
          return "badge badge-pill badge-glow badge-success badge-square margin-right-5px";
          break;
      }

    } else {
      return "badge badge-pill badge-glow badge-secondary badge-square margin-right-5px";
    }
  }

  getStatutBadgeCode(statut: number) {
    if (statut === 1) {
      return "B";
    }
    if (statut === 2) {
      return "AV";
    }
    if (statut === 3) {
      return "V";
    }
  }

  isModifiable() {
    return ((this._editeur && this._model.typeAuteur === 2) || (!this._editeur && this._model.typeAuteur === 1));
  }


  getPrestationsIcons(id: number) {

    if (id === 1) {
      return 'fa fa-wrench'
    }
    if (id === 2) {
      return 'fa fa-shopping-cart'
    }
    if (id === 3) {
      return 'fa fa-briefcase'
    }
  }

  getPrestationClass(id: number) {
    if (this._model.idNaturePrestation === 0 || this._model.idNaturePrestation === id) {
      return 'badge-warning'
    }
    return 'badge-secondary'

  }

  updateStatut(idStatut: number) {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let i18n = '';
    switch (idStatut) {
      case 1:
        i18n = 'refuser';
        break;
      case 2:
        i18n = 'prevalider';
        break;
      case 3:
        i18n = 'valider';
        break;
    }
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'redaction.' + this._type + '.confirmation.' + i18n + '.titre';
    this.bsModalRef.content.messages = ['redaction.' + this._type + '.confirmation.' + i18n + '.message'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.baseService.changeStatus(this._model.id, this._model.idPublication, {idStatus: idStatut})
            .subscribe(value => {
              this._model = value;
              this.store.dispatch(showSuccessToast({
                header: 'Information', message: 'redaction.' + this._type + '.succes.' + i18n
              }));
              this.changeDetectorRef.detectChanges();
            })
        }
        this.bsModalRef = null;
      }
    );
  }

  modify() {
    let path =AppInternalPathEnum.PRIVATE_PATH + this.formPath.replace(':type', this._editeur ? 'editeur' : 'client')
      .replace(':id', this._model.id + '')
      .replace(':action', 'modification');
    if (this._model.idPublication) {
      path += '?idPublication' + this._model.idPublication
    }
    this.router.navigateFromContexte(path);
  }

  duplicate() {
    let path =AppInternalPathEnum.PRIVATE_PATH + this.formPath.replace(':type', this._editeur ? 'editeur' : 'client')
      .replace(':id', this._model.id + '')
      .replace(':action', 'duplication');
    let queryParams = {};
    if (this._model.idPublication) {
      queryParams = {...queryParams, idPublication: this._model.idPublication}
    }
    this.router.navigateFromContexte(path, queryParams);
  }

  delete() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'redaction.' + this._type + '.confirmation.supprimer.titre';
    this.bsModalRef.content.messages = ['redaction.' + this._type + '.confirmation.supprimer.message'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.baseService.delete(this._model.id, this._model.idPublication)
            .subscribe(value => {
              this._model = value;
              this.store.dispatch(showSuccessToast({
                header: 'Information',
                message: 'redaction.' + this._type + '.succes.supprimer'
              }));
            })
        }
        this.bsModalRef = null;
      }
    );
  }

}
