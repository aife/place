import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'valueFromBoolean',
  pure: false
})
export class ValueFromBooleanPipe implements PipeTransform {

  transform(isOk: boolean): string {
    return isOk?'Oui':'Non';
  }
}
