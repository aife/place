import {ClauseTextePrevaloriseForm} from "@shared-global/core/models/api/clausier.api";

export class ClauseTextePrevaloriseFormModel implements ClauseTextePrevaloriseForm {
  clauseSelectionnee: string;
  defaultValue: string;
  nbCaract: string;
  parametrableAgent: string;
  parametrableDirection: string;
  precochee: boolean;
  sautTextFixeApres: string;
  sautTextFixeAvant: string;
  tailleChamps: number;
  textFixeApres: string;
  textFixeAvant: string;
  valeurDefautCourt: string;
  valeurDefautMoyen: string;
  valeurDefautTresLong: string;



}
