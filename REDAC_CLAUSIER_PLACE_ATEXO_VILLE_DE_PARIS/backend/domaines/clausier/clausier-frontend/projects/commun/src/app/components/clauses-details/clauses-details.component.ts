import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {ClauseService} from "@shared-global/core/services/clause.service";
import {BaseDetailsComponent} from "@shared-global/components/base-details/base-details.component";
import {ClauseBean, ClauseDocument} from "@shared-global/core/models/api/clausier.api";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {BsModalService} from "ngx-bootstrap/modal";
import {
  ModalPreviewClauseComponent
} from "@shared-global/components/modal-preview-clause/modal-preview-clause.component";
import {
  ModalHistoriqueClauseComponent
} from "@shared-global/components/modal-historique-clause/modal-historique-clause.component";

import {ActivatedRoute} from "@angular/router";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";

@Component({
  selector: 'atx-clauses-details',
  templateUrl: './clauses-details.component.html',
  styleUrls: ['./clauses-details.component.css']

})
export class ClausesDetailsComponent extends BaseDetailsComponent<ClauseBean, ClauseBean, ClauseDocument> {

  @Input() administrationRoles = [];
  @Input() validationRoles = [];
  @Input() showDetails = false;
  @Input() readOnly = false;
  @Input() parametrableAgent: boolean;
  @Input() parametrableDirection: boolean;
  @Input() surcharge: boolean;
  @Output() isDeleted = new EventEmitter<boolean>();

  constructor(protected route: ActivatedRoute, protected modalService: BsModalService, protected changeDetectorRef: ChangeDetectorRef, protected router: RoutingInterneService, protected readonly clauseService: ClauseService, protected readonly store: Store<State>) {
    super(modalService, changeDetectorRef, router, clauseService, store, AppInternalPathEnum.FORM_CLAUSE);
    this._type = "clause";
  }

  get model() {
    return this._model;
  }

  @Input() set model(model: ClauseBean) {
    this._model = model;
    this._model.contextHtml = this.unescapeHtml(this._model.contextHtml);
    this._model.context = this.unescapeHtml(this._model.context);
  }

  get editeur() {
    return this._editeur;
  }

  @Input() set editeur(editeur: boolean) {
    this._editeur = editeur;
  }

  hasInfobulle() {
    return this.model.infoBulleText?.length > 0 || this.model.infoBulleUrl?.length > 0
  }


  preview() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    let bsModal = this.modalService.show(ModalPreviewClauseComponent, config);
    bsModal.content.titre = 'redaction.clause.previsualiser.titre';
    bsModal.content.editeur = this.editeur;
    if (this.parametrableAgent || this.parametrableDirection) {
      bsModal.content.editeur = false;
    }
    bsModal.content.agent = this.parametrableAgent;
    bsModal.content.direction = this.parametrableDirection;
    bsModal.content.surcharge = this.surcharge;
    bsModal.content.clauseBean = this.model;

  }

  isModifiable() {
    return ((!this.editeur && !this._model.clauseEditeur) || (this.editeur && this._model.clauseEditeur)) &&
      !this.parametrableAgent && !this.parametrableDirection;
  }

  isOverloadeable() {
    return !this.isModifiable();
  }

  overload() {
    let path =AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.SURCHARGE_CLIENT_CLAUSE
      .replace(':id', this._model.id + '')
      .replace(':idPublication', this._model.idPublication + '');
    this.router.navigateFromContexte(path);
  }

  configureClause() {
    let path =AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.PARAMETRAGE_CLAUSE
      .replace(':id', this._model.id + '')
      .replace(":type", this.parametrableAgent ? 'agent' : 'direction')
      .replace(':idPublication', this._model.idPublication + '');

    if (this.parametrableAgent) {
      this.router.navigateFromContexte(path);
    } else {
      this.router.navigateFromContexte(path);
    }

  }


  showHistoric() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-full-width modal-dialog-scrollable"
    };
    let bsModal = this.modalService.show(ModalHistoriqueClauseComponent, config);
    bsModal.content.titre = 'clause.txt.historiqueVersions';
    bsModal.content.clauseId = this.model.id;


  }


  isParametrable() {
    return this._model.lastVersion !== 'ND' && (this.parametrableAgent || this.parametrableDirection)
      && (this._model.idTypeClause === 4 || this._model.idTypeClause === 6 || this._model.idTypeClause === 7)
  }

  unescapeHtml(input) {
    const htmlEntities = {
      "&amp;": "&",
      "&lt;": "<",
      "&gt;": ">",
      "&#39;": "'",
      "&#61;": "=",
      "&#34;": "\""
    };

    return input.replace(/&[a-zA-Z0-9]+;|&#\d+;/g, match => {
      return htmlEntities[match] || match;
    });
  }

  delete() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalConfirmationComponent, config);
    this.bsModalRef.content.titre = 'redaction.' + this._type + '.confirmation.supprimer.titre';
    this.bsModalRef.content.messages = ['redaction.' + this._type + '.confirmation.supprimer.message'];
    this.bsModalRef.content.onClose.subscribe(value => {
        if (value) {
          this.baseService.delete(this._model.id, this._model.idPublication)
            .subscribe(value => {
              this._model = value;
              this.isDeleted.emit(true);
              this.store.dispatch(showSuccessToast({
                header: 'Information',
                message: 'redaction.' + this._type + '.succes.supprimer'
              }));
            })
        }
        this.bsModalRef = null;
      }
    );
  }
}

