import {NgModule} from '@angular/core';
import {CommonModule, CurrencyPipe} from '@angular/common';
import {NumberFormatPipe} from "@shared-global/pipes/number-format/number-format.pipe";
import {ImageUrlPipe} from "@shared-global/pipes/image-url/image-url.pipe";
import {LabelFromNumberPipe} from "@shared-global/pipes/label-from-number/label-from-number.pipe";
import {TailleFromNbCaracteresPipe} from "@shared-global/pipes/taille-from-nbCaracteres/taille-from-nbCaracteres.pipe";
import {DirectoryFilterPipe} from "@shared-global/pipes/directory-filter/directory-filter.pipe";
import {SafeUrlPipe} from "@shared-global/pipes/safe-url/safe-url.pipe";
import {NumberFromTaillePipe} from "@shared-global/pipes/number-from-taille/number-from-taille.pipe";
import {ValueFromBooleanPipe} from "@shared-global/pipes/value-from-boolean/value-from-boolean.pipe";
import {SafeHtmlPipe} from "@shared-global/pipes/safe-html/safe-html.pipe";


@NgModule({
  declarations: [
    SafeUrlPipe, SafeHtmlPipe, DirectoryFilterPipe, ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, NumberFromTaillePipe, TailleFromNbCaracteresPipe, ValueFromBooleanPipe
  ],
  imports: [
    CommonModule
  ],
  providers: [SafeUrlPipe, SafeHtmlPipe, DirectoryFilterPipe, ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, CurrencyPipe, NumberFromTaillePipe, TailleFromNbCaracteresPipe, ValueFromBooleanPipe],
  exports: [SafeUrlPipe, SafeHtmlPipe, DirectoryFilterPipe, ImageUrlPipe, NumberFormatPipe, LabelFromNumberPipe, NumberFromTaillePipe, TailleFromNbCaracteresPipe, ValueFromBooleanPipe]
})
export class PipesModule {
}
