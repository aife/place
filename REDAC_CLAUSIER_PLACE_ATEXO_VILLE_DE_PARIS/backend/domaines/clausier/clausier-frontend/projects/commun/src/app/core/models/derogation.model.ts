import {Derogation} from "@shared-global/core/models/api/clausier.api";

export class DerogationModel implements Derogation {
  articleDerogationCCAG: string;
  commentairesDerogationCCAG: string;
  empty: boolean;
  numArticle: string;
  active: boolean;


}
