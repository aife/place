import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BasePreviewQuillComponent} from "@shared-global/components/base-preview-quill/base-preview-quill.component";
import {ClauseDocument, ClauseListe} from "@shared-global/core/models/api/clausier.api";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-choix-multiple',
  templateUrl: './preview-choix-multiple.component.html',
  styleUrls: ['./preview-choix-multiple.component.scss']
})
export class PreviewChoixMultipleComponent extends BasePreviewQuillComponent {

  clauseDocument: ClauseListe;

}
