import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'atx-access-403',
  templateUrl: './modal-access-403.component.html',
  styleUrls: ['./modal-access-403.component.scss']
})
export class ModalAccess403Component {
  message = "Accès non autorisé";

  constructor(public bsModalRef: BsModalRef) {

  }


  closeModal() {
    this.bsModalRef.hide();
  }

}
