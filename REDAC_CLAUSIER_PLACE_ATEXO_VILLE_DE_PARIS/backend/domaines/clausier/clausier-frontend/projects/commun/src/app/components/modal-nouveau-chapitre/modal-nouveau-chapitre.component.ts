import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";
import {Subject} from "rxjs";

@Component({
  selector: 'atx-nouveau-chapitre',
  templateUrl: './modal-nouveau-chapitre.component.html',
  styleUrls: ['./modal-nouveau-chapitre.component.scss']
})
export class ModalNouveauChapitreComponent {
  protected _chapitre = new ChapitreModel()
  modification = false;

  @Input() set chapitre(chapitre: ChapitreModel) {
    this._chapitre = chapitre;
    this.modification = chapitre && (!!chapitre.idChapitre || !!chapitre.titre);
  }

  get chapitre() {
    return this._chapitre;
  }

  onClose = new Subject<ChapitreModel>();

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit(close) {
    this.onClose.next(this.chapitre);
    if (close) {
      this.bsModalRef.hide();
    } else {
      this.chapitre = new ChapitreModel();
    }

  }

}
