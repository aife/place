import {Component, ComponentFactoryResolver, Input, ViewChild} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {PreviewClauseDirective} from "@shared-global/directives/preview-clause/preview-clause.directive";
import {PreviewTexteFixeComponent} from "@shared-global/components/preview-texte-fixe/preview-texte-fixe.component";
import {PreviewChampLibreComponent} from "@shared-global/components/preview-champ-libre/preview-champ-libre.component";
import {
  PreviewValeurHeriteeComponent
} from "@shared-global/components/preview-valeur-heritee/preview-valeur-heritee.component";
import {
  PreviewChoixMultipleComponent
} from "@shared-global/components/preview-choix-multiple/preview-choix-multiple.component";
import {
  PreviewChoixExclusifComponent
} from "@shared-global/components/preview-choix-exclusif/preview-choix-exclusif.component";
import {
  PreviewChampPrevaloriseComponent
} from "@shared-global/components/preview-champ-prevalorise/preview-champ-prevalorise.component";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {ClauseBean, ClauseDocument} from "@shared-global/core/models/api/clausier.api";

@Component({
  selector: 'atx-preview-clause',
  templateUrl: './preview-clause.component.html',
  styleUrls: ['./preview-clause.component.scss']
})
export class PreviewClauseComponent {

  @ViewChild(PreviewClauseDirective, {static: true}) formulaireHost!: PreviewClauseDirective;
  @Input() titre = 'confirmation'
  @Input() editeur;
  @Input() direction;
  @Input() agent;

  @Input() surcharge;
  @Input() isCanevasPreview: boolean;
  @Input() set clauseBean(clause: ClauseBean) {
    if (clause) {
      this.loading = true;
      this.clauserService.preview(clause.id, this.editeur, this.agent, this.direction, this.surcharge, clause.idPublication)
        .subscribe(value => {
          this.clauseDocument = value;
          this.loading = false;
        }, () => {
          this.loading = false;
        })
    }
  }


  private _clauseDocuemnt: ClauseDocument;

  @Input() set clauseDocument(clauseDocument: ClauseDocument) {
    if (!!clauseDocument) {
      this._clauseDocuemnt = clauseDocument;
      this.chargerFormulaire(this._clauseDocuemnt.type);
    }
  }

  get clauseDocument() {
    return this._clauseDocuemnt;
  }

  loading = false;


  constructor(private readonly clauserService: ClauseService, public bsModalRef: BsModalRef, private componentFactoryResolver: ComponentFactoryResolver) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  chargerFormulaire(idTypeClause: number) {
    switch (idTypeClause) {
      case 2:
        this.afficherFormulaire(PreviewTexteFixeComponent);
        break;
      case 3:
        this.afficherFormulaire(PreviewChampLibreComponent);
        break;
      case 4:
        this.afficherFormulaire(PreviewChampPrevaloriseComponent);
        break;
      case 6:
        this.afficherFormulaire(PreviewChoixExclusifComponent);
        break;
      case 7:
        this.afficherFormulaire(PreviewChoixMultipleComponent);
        break;
      case 9:
        this.afficherFormulaire(PreviewValeurHeriteeComponent);
        break;
      default:
        console.log("Id type clause invalide");
    }
  }

  afficherFormulaire(nomComposant: any) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(nomComposant);
    const viewContainerRef = this.formulaireHost.viewContainerRef;
    viewContainerRef.clear();
    const component = viewContainerRef.createComponent<any>(componentFactory);
    console.log(this.clauseDocument)
    component.instance.clauseDocument = this.clauseDocument;
  }

  getMessage() {
    let condition: string = this.clauseDocument.texteConditions;
    let tab: string[] = condition.split(" ET ");
    return "Conditionnement(s) : " + tab.join("<strong> ET </strong>");

  }


}
