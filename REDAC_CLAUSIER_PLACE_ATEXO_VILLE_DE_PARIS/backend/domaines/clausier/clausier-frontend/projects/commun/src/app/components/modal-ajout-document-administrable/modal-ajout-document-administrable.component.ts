import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {NgxDropzoneChangeEvent} from "ngx-dropzone";
import {CreateDocumentAdministrableModel} from "@shared-global/core/models/create-document-administrable.model";
import {DocumentService} from "@shared-global/core/services/document.service";
import {Subject} from "rxjs";
import {showSuccessToast} from "@shared-global/store/toast/toast.action";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";

@Component({
  selector: 'atx-modal-ajout-document-administrable',
  templateUrl: './modal-ajout-document-administrable.component.html',
  styleUrls: ['./modal-ajout-document-administrable.component.scss']
})
export class ModalAjoutDocumentAdministrableComponent {


  @Input() titre = '';
  @Input() isChampFusion: boolean;
  onClose = new Subject<boolean>();
  isSubmitted: boolean = false;
  documentAdministrable: CreateDocumentAdministrableModel = new CreateDocumentAdministrableModel();
  file: any;
  files: File[] = [];
  @Input() products: string[];

  constructor(public bsModalRef: BsModalRef, public documentService: DocumentService, protected readonly store: Store<State>) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }


  createDocAdministrable() {
    this.isSubmitted = true;
    if (!!this.documentAdministrable.libelle && !!this.documentAdministrable.produit && !!this.documentAdministrable.code) {
      if (!!this.file) {
        console.log("avec fichier")
        this.documentAdministrable.champFusion = this.isChampFusion;
        this.documentService.saveDocumentAdministrable(this.file[0], this.documentAdministrable).subscribe(value => {
          this.documentAdministrable = value;
          this.afficheSuccesCreationToast();
          this.onClose.next(true)
          this.closeModal();
        })
      } else {
        console.log("sans fichier")
        this.documentAdministrable.champFusion = this.isChampFusion;
        this.documentService.saveDocumentAdministrable(null, this.documentAdministrable).subscribe(value => {
          this.documentAdministrable = value;
          this.afficheSuccesCreationToast();
          this.onClose.next(true)
          this.closeModal();
        })
      }

    }

  }

  selectSingleFile($event: NgxDropzoneChangeEvent) {
    this.file = $event.addedFiles;
  }

  singlefileonRemove(f: any) {
    this.file.splice(this.files.indexOf(f), 1);
  }

  afficheSuccesCreationToast() {
    if (this.isChampFusion) {
      this.store.dispatch(showSuccessToast({
        header: 'Information', message: 'champ-fusion.confirmation.success.creer'
      }));
    } else {
      this.store.dispatch(showSuccessToast({
        header: 'Information', message: 'doc-administrable.confirmation.success.creer'
      }));
    }
  }
}
