import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {ClauseTexte} from "@shared-global/core/models/api/clausier.api";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-texte-fixe',
  templateUrl: './preview-texte-fixe.component.html',
  styleUrls: ['./preview-texte-fixe.component.scss']

})
export class PreviewTexteFixeComponent {

  clauseDocument: ClauseTexte;

}
