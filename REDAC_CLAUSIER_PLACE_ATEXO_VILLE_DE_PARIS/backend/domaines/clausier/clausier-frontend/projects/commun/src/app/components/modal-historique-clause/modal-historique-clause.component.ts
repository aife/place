import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseService} from "@shared-global/core/services/clause.service";
import {PublicationClausierModel} from "@shared-global/core/models/publication-clausier.model";


@Component({
  selector: 'atx-modal-historique-clause',
  templateUrl: './modal-historique-clause.component.html',
  styleUrls: ['./modal-historique-clause.component.scss']
})
export class ModalHistoriqueClauseComponent {

  titre = 'confirmation'
  clauseBean: ClauseBean;

  public _clauseId: number;
  public historiques: Array<PublicationClausierModel>;

  @Input() set clauseId(clauseId: number) {
    if (clauseId) {
      this._clauseId = clauseId;
      this.clauseService.getHistoriqueVersion(clauseId).subscribe(value => {
        this.historiques = value.reverse();

      })
    }

  }


  constructor(public bsModalRef: BsModalRef, private readonly clauseService: ClauseService) {
  }

  closeModal() {
    this.bsModalRef.hide();
  }

}

