import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {FormulationModel} from "@shared-global/core/models/formulation.model";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseListeChoixCumulatifFormModel} from "@shared-global/core/models/ClauseListeChoixCumulatifForm.model";
import {moveArray} from "@shared-global/core/utils/technical/technical.utils";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-choix-multiple',
  templateUrl: './form-choix-multiple.component.html',
  styleUrls: ['./form-choix-multiple.component.scss']
})
export class FormChoixMultipleComponent extends BaseFormQuillComponent implements OnInit {

  formulation: FormulationModel = new FormulationModel(); //formulation à ajouter
  submitted: boolean;
  @Input() disabled: boolean;
  @Input() editeur: boolean;
  @Input() parametrageAgent: boolean;
  @Input() fromEcranSurcharge: boolean;
  @Input() clause: ClauseBean;
  sautLigne1: boolean = false;
  sautLigne2: boolean = false;
  paramDirection: boolean = false;
  paramAgent: boolean = false;
  draggedIndexFormulation: number;
  draggingIndexFormulation: number;
  defaultTextColor = 'black';

  constructor() {
    super();
  }

  public ngOnInit(): void {
    this.formulation.tailleChamps = "1";
    this.formulation.precochee = false;
    //modification
    if (!!this.clause.clauseListeChoixCumulatif) {
      this.submitted = true;
      if (this.clause.clauseListeChoixCumulatif.sautTextFixeAvant === "true") {
        this.sautLigne1 = true;
      }
      if (this.clause.clauseListeChoixCumulatif.sautTextFixeApres === "true") {
        this.sautLigne2 = true;
      }
      if (this.clause.clauseListeChoixCumulatif.parametrableDirection === "1") {
        this.paramDirection = true;
      }
      if (this.clause.clauseListeChoixCumulatif.parametrableAgent === "1") {
        this.paramAgent = true;
      }
    } else { //creation
      this.clause.clauseListeChoixCumulatif = new ClauseListeChoixCumulatifFormModel();
      this.clause.clauseListeChoixCumulatif.textFixeAvant = "";
      this.clause.clauseListeChoixCumulatif.textFixeApres = "";
      this.clause.clauseListeChoixCumulatif.sautTextFixeAvant = String(false);
      this.clause.clauseListeChoixCumulatif.sautTextFixeApres = String(false);
      this.clause.clauseListeChoixCumulatif.parametrableDirection = "0";
      this.clause.clauseListeChoixCumulatif.parametrableAgent = "0";
      this.clause.clauseListeChoixCumulatif.modifiable = false;
    }
  }

  addEtape() {

    if (!!this.formulation.defaultValeur) {
      this.formulation.numFormulation = this.clause.clauseListeChoixCumulatif.formulaires.length + 1;
      this.clause.clauseListeChoixCumulatif.formulaires.push(this.formulation);
      this.formulation = new FormulationModel();
      this.formulation.tailleChamps = "1";
      this.formulation.precochee = false;
    }
  }

  removeFormulation(index: number) {
    this.clause.clauseListeChoixCumulatif.formulaires.splice(index, 1);
    this.clause.clauseListeChoixCumulatif.formulaires.forEach((value, index) => {
      value.numFormulation = index + 1;
    })
  }


  updateSautLigne1() {
    this.clause.clauseListeChoixCumulatif.sautTextFixeAvant = String(this.sautLigne1);
  }

  updateSautLigne2() {
    this.clause.clauseListeChoixCumulatif.sautTextFixeApres = String(this.sautLigne2);
  }

  updateParamDirection() {
    if (this.paramDirection) {
      this.clause.clauseListeChoixCumulatif.parametrableDirection = "1";
    } else {
      this.clause.clauseListeChoixCumulatif.parametrableDirection = "0";
    }
  }

  updateParamAgent() {
    if (this.paramAgent) {
      this.clause.clauseListeChoixCumulatif.parametrableAgent = "1";
    } else {
      this.clause.clauseListeChoixCumulatif.parametrableAgent = "0";
    }
  }

  onDragEnd(list, draggingIndex, draggedIndex): void {
    if (draggingIndex !== draggedIndex && draggedIndex !== undefined) {
      moveArray(list, draggingIndex, draggedIndex);
      list.forEach((value, index) => {
        value.numFormulation = index + 1;
      })
    }
  }

  cleanTexteAvant() {
    this.clause.clauseListeChoixCumulatif.textFixeAvant = this.removeTagForEmptyField(this.clause.clauseListeChoixCumulatif.textFixeAvant);
  }

  cleanTexteApres() {
    this.clause.clauseListeChoixCumulatif.textFixeApres = this.removeTagForEmptyField(this.clause.clauseListeChoixCumulatif.textFixeApres);
  }


}
