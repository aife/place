import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Canevas, CanevasBean, ChapitreDocument, Clause} from "@shared-global/core/models/api/clausier.api";
import {AbstractService} from "@shared-global/core/services/abstract.service";
import {Observable} from "rxjs";
import {APP_BASE_HREF} from "@angular/common";


@Injectable({
  providedIn: 'root'
})
export class CanevasService extends AbstractService<CanevasBean, Canevas, ChapitreDocument[]> {


  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    super(http, baseHref === '/redac/web-component-clausier' ? '/redac/clausier/v2/canevas' : '/clausier/v2/canevas')

  }


  getByClause(idClause: number, editeur: boolean, idPublication: number) {
    let params: any = {editeur};
    if (idPublication) {
      params = {...params, idPublication}
    }
    return this.http.get<Clause>(`${this.BASE_PATH}/clauses/${idClause}.htm`,
      {params})
  }


  edit(id: number, editeur: boolean, idPublication?: number): Observable<string> {
    let params: any = {editeur};
    if (idPublication) {
      params = {...params, idPublication}
    }
    return this.http.get<string>(`${this.BASE_PATH}/${id}/edit.htm`,
      {params})
  }


  export(idCanevas: number, editeur: boolean, idPublication: number, outputFormat: string) {
    let params: any = {editeur};
    if (idPublication) {
      params = {...params, idPublication}
    }
    if (outputFormat) {
      params = {...params, outputFormat}
    }
    // @ts-ignore
    return this.http.get<any>(`${this.BASE_PATH}/${idCanevas}/export.htm`, {params: params, responseType: 'blob'})
  }

  previewCanevasEnCoursCreation(canevas): Observable<Array<ChapitreDocument>> {

    return this.http.post<Array<ChapitreDocument>>(`${this.BASE_PATH}/preview.htm`,
      canevas)
  }
}
