import {Chapitre, Clause, InfoBulle} from "@shared-global/core/models/api/clausier.api";

export class ClauseModel implements Clause {
  logiqueAssociationEt: boolean;
  actif: boolean;
  auteur: string;
  clauseEditeur: boolean;
  contenu: string;
  dateCreation: string;
  dateModification: string;
  etat: string;
  idClause: number;
  idPublication: number;
  idStatutRedactionClausier: number;
  infoBulle: InfoBulle;
  lastVersion: string;
  nombreSurcharges: number;
  parent: Chapitre;
  potentiellementApplicable: boolean;
  potentiellementConditionnee: boolean;
  reference: string;
  texteFixeApres: string;
  texteFixeApresAlaLigne: boolean;
  texteFixeAvant: string;
  texteFixeAvantAlaLigne: boolean;
  theme: string;
  idsTypeContrats: number[];
  idProcedure: number;
}
