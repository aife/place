import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '@shared-global/store';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private userRoles: string[];

  constructor(private readonly store: Store<State>) {
    this.store.select(state => state.userReducer?.user?.roles).subscribe((rolesTab) => {
      this.userRoles = rolesTab;
    });
  }

  isAuthorized(authorizedRoles: string[]): boolean {
    return authorizedRoles?.includes('*') || _.intersection(authorizedRoles, this.userRoles).length > 0;
  }

  async logout() {
    return true;
  }
}
