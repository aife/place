import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotConnectedGuard} from "@shared-global/core/guards/not-connected.guard";
import {AuthGuard} from "@shared-global/core/guards/auth.guard";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ModalModule} from "ngx-bootstrap/modal";
import {TokenInterceptor} from "@shared-global/core/interceptors/token.interceptor";
import {ErrorInterceptor} from "@shared-global/core/interceptors/error.interceptor";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {ThemesService} from "@shared-global/core/services/themes.service";
import {ActivatedRouteSnapshot} from "@angular/router";


@NgModule({
  declarations: [],
  imports: [
    CommonModule, HttpClientModule, ModalModule.forRoot(),

  ],
  providers: [NotConnectedGuard, AuthGuard, RoutingInterneService,
    CanevasService,
    ThemesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }]
})
export class CoreModule {
}
