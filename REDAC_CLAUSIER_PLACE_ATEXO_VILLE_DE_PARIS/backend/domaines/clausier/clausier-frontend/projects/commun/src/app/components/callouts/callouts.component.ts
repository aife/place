import {Component, Input} from '@angular/core';

@Component({
    selector: 'atx-callouts',
    templateUrl: './callouts.component.html',
    styleUrls: ['./callouts.component.css']
})
export class CalloutsComponent {
    @Input() message: string;
    @Input() header: string;
    @Input() icone: string;

}
