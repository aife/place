export class FetchStatusModel<T> {
    public content: T;
    public loading: boolean;
    public error: string
}
