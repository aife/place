import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";
import {
  ModalNouveauChapitreComponent
} from "@shared-global/components/modal-nouveau-chapitre/modal-nouveau-chapitre.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ClauseModel} from "@shared-global/core/models/clause.model";
import {ModalInfoBulleComponent} from "@shared-global/components/modal-info-bulle/modal-info-bulle.component";
import {ModalSelectClauseComponent} from "@shared-global/components/modal-select-clause/modal-select-clause.component";
import {ModalDerogationComponent} from "@shared-global/components/modal-derogation/modal-derogation.component";
import {ClauseBean, Derogation, Directory, InfoBulle} from "@shared-global/core/models/api/clausier.api";
import {DerogationModel} from "@shared-global/core/models/derogation.model";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import {moveArray} from "@shared-global/core/utils/technical/technical.utils";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";

@Component({
  selector: 'atx-chapitre-treeview',
  templateUrl: './chapitre-treeview.component.html',
  styleUrls: ['./chapitre-treeview.component.scss']
})
export class ChapitreTreeviewComponent implements OnInit, OnChanges {

  @Input() chapitres: ChapitreModel[];
  @Input() prefix = "";
  @Input() editeur = true;
  @Input() niveau = 0;
  typeDocument: string;
  private _idDocument: number;
  @Input()
  public  set idDocument(value: number){
    this._idDocument = value;
    this.refTypeDocument.forEach(type => {
      if (type.id === this._idDocument) {
        this.typeDocument = type.externalCode;
      }
    })

  }

  public get idDocument(){
    return this._idDocument;
  }
  @Output() onCancelIndentation = new EventEmitter<ChapitreModel>();
  @Output() onDerogationChanges = new EventEmitter<ChapitreModel>();

  refTypeDocument: Array<Directory> = [];
  bsModalRef!: BsModalRef;
  shouldShowChildren: boolean[] = [];
  draggingIndexChapitre: number;
  draggedIndexChapitre: number;
  draggingIndexClause: number;
  draggedIndexClause: number;

  constructor(private modalService: BsModalService, private readonly canevasService: CanevasService, private readonly store: Store<State>) {

  }

  ngOnInit(): void {
    if (!!this.chapitres) {
      this.chapitres.forEach(chapitre => {this.shouldShowChildren.push(false)})
    }
    this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content).subscribe(value => {
      this.refTypeDocument = value;
    });
    if (!!this.idDocument) {
      this.refTypeDocument.forEach(type => {
        if (type.id === this._idDocument) {
          this.typeDocument = type.externalCode;
        }
      })
    } else {
        this.typeDocument = "tous";
      }
  }

  hasChildren(chapitre) {
    return chapitre.chapitres?.length > 0 || chapitre.clauses?.length > 0;
  }

  get clauseMarginLeft() {
    return 20 * (this.niveau + 1);
  }

  get chapitreMarginLeft() {
    return 20 * this.niveau;
  }

  openUrl(lien: string) {
    if (lien?.length > 0) {
      window.open(lien, '_blank')
    }
  }

  addChapitre(chapitre: ChapitreModel, index: number) {
    if (!chapitre.chapitres) {
      chapitre.chapitres = [];
    }
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalNouveauChapitreComponent, config);
    this.bsModalRef.content.onClose.subscribe(value => {
      if (value) {
        value.numero = chapitre.numero + "." + (chapitre.chapitres.length + 1);
        chapitre.chapitres.push(value)
        this.updateIndex();
      }
      this.bsModalRef = null;
    });
  }

  modifyChapitre(chapitres: ChapitreModel[], index: number) {

    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalNouveauChapitreComponent, config);
    this.bsModalRef.content.chapitre = JSON.parse(JSON.stringify(chapitres[index]));
    this.bsModalRef.content.onClose.subscribe(value => {
      if (value) {
        if (value.infoBulle) {
          value.infoBulle.empty = !value.infoBulle.actif || !this.isNotEmptyInfoBulle(value.infoBulle);
        }
        chapitres[index] = value
      }
      this.bsModalRef = null;
    });
  }

  gererInfoBulleChapitre(chapitre: ChapitreModel) {

    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalInfoBulleComponent, config);
    this.bsModalRef.content.infoBulle = chapitre.infoBulle ? JSON.parse(JSON.stringify(chapitre.infoBulle)) : null;
    this.bsModalRef.content.onClose.subscribe(value => {
      if (value) {
        value.empty = !value.actif || !this.isNotEmptyInfoBulle(value)
      }
      chapitre.infoBulle = value
      this.bsModalRef = null;
    });
  }

  gererClauses(chapitre: ChapitreModel) {

    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-full-width modal-dialog-scrollable"
    };
    this.bsModalRef = this.modalService.show(ModalSelectClauseComponent, config);
    this.bsModalRef.content.editeur = this.editeur;
    this.bsModalRef.content.onClose.subscribe((value: ClauseBean[]) => {
      if (!chapitre.clauses) {
        chapitre.clauses = []
      }
      value.forEach(value1 => {
        this.canevasService.getByClause(value1.idClause, this.editeur, value1.idPublication)
          .subscribe(value2 => chapitre.clauses.push(value2))
      })
      this.bsModalRef = null;
    });
  }


  gererInfoBulleClause(clause: ClauseModel) {

    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    this.bsModalRef = this.modalService.show(ModalInfoBulleComponent, config);
    this.bsModalRef.content.infoBulle = clause.infoBulle ? JSON.parse(JSON.stringify(clause.infoBulle)) : null;
    this.bsModalRef.content.onClose.subscribe(value => {
      if (value) {
        value.empty = !value.actif || !this.isNotEmptyInfoBulle(value)
      }
      clause.infoBulle = value
      this.bsModalRef = null;
    });
  }

  gererDerogation(chapitre: ChapitreModel) {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: "modal-lg"
    };
    if (!chapitre.derogation) {
      let derogation = new DerogationModel();
      derogation.active = false;
      chapitre.derogation = derogation;
    }
    this.bsModalRef = this.modalService.show(ModalDerogationComponent, config);
    this.bsModalRef.content.chapitre = JSON.parse(JSON.stringify(chapitre));
    this.bsModalRef.content.onClose.subscribe((value: ChapitreModel) => {
      if (value) {
        chapitre = value
        this.onDerogationChanges.emit(chapitre);
      }
      this.bsModalRef = null;
    });
  }


  removeChapitre(chapitres: ChapitreModel[], index: number) {
    chapitres.splice(index, 1);
    this.updateIndex();
  }

  removeClause(clauses: ClauseModel[], index: number) {
    clauses.splice(index, 1)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes treeview", changes)
    this.updateIndex();
  }

  updateIndex() {
    this.chapitres.forEach((value, index) => {
      value.numero = this.prefix + (index + 1)
    })
  }

  isNotEmptyInfoBulle(infoBulle: InfoBulle) {
    if (infoBulle) {
      return infoBulle.lien?.length > 0 || infoBulle.description?.length > 0;
    }
    return false;
  }


  indenterChapitre(arr: ChapitreModel[], index: number) {
    //passer le chapitre en sous chapitre du chapitre précédent
    let eltPrecedent = arr[index - 1];
    if (!eltPrecedent.chapitres) {
      eltPrecedent.chapitres = [];
    }
    let chapIndente = arr[index];
    chapIndente.numero = eltPrecedent.numero + "." + (eltPrecedent.chapitres.length + 1);
    eltPrecedent.chapitres.push(chapIndente);
    arr.splice(index, 1);
    this.updateIndex();

  }

  cancelIndentationChapitre(arr: ChapitreModel[], index: number) {
    //enlever l'élément du tableau de sous chapitres
    let chapitreDesindente = arr[index];
    //mise à jour du numéro de ce chapitre + tous les autres chapitres de même niveau
    let debutNum = chapitreDesindente.numero.split(".")[0];
    chapitreDesindente.numero = parseInt(debutNum) + 1 + "";
    arr.splice(index, 1);
    this.onCancelIndentation.emit(chapitreDesindente); //va appeler la méthode insertChapitre
    this.updateIndex();
  }

  insertChapitre(arr: ChapitreModel[], chapitre: ChapitreModel, index: number) {
    //appelée quand on a désindenté un chapitre, pour le remonter d'un  niveau
    arr.splice(index + 1, 0, chapitre);
    this.updateIndex();
  }


  onDragEnd(list: any[], draggingIndex, draggedIndex): void {
    if (draggingIndex !== draggedIndex && draggedIndex !== undefined) {
      moveArray(list, draggingIndex, draggedIndex);
      this.updateIndex()
    }
  }


  showChildren(index: number) {
    this.shouldShowChildren[index] = !this.shouldShowChildren[index]
  }


  getInfoBulleTitle(infoBulle: InfoBulle) {
    if (!!infoBulle.description) {
      return infoBulle.description;
    } else {
      return "";
    }
  }

  isDerogeable() {
    return this.typeDocument === "CCAP" || this.typeDocument === "CCAPAE"
      || this.typeDocument === "CCP" || this.typeDocument === "CCPAE"
      || this.typeDocument === "tous";
  }
}
