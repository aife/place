import {PotentiellementConditionnee} from "@shared-global/core/models/api/clausier.api";

export class PotentiellementConditionneeModel implements PotentiellementConditionnee {
  id: number;
  libelle: string;
  multiChoix: boolean;
  valeurs: any[];
  valeurId: number;

}
