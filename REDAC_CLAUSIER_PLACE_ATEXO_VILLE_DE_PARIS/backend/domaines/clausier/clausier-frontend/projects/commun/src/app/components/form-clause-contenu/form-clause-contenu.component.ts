import {Component, ComponentFactoryResolver, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ClauseBean, Directory} from "@shared-global/core/models/api/clausier.api";
import {FormulaireClauseDirective} from "@shared-global/directives/formulaire-clause/formulaire-clause.directive";
import {Observable} from "rxjs";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {FormTexteFixeComponent} from "@shared-global/components/form-texte-fixe/form-texte-fixe.component";
import {FormChoixExclusifComponent} from '@shared-global/components/form-choix-exclusif/form-choix-exclusif.component';
import {FormValeurHeriteeComponent} from "@shared-global/components/form-valeur-heritee/form-valeur-heritee.component";
import {FormChoixMultipleComponent} from "@shared-global/components/form-choix-multiple/form-choix-multiple.component";
import {
  FormChampPrevaloriseComponent
} from "@shared-global/components/form-champ-prevalorise/form-champ-prevalorise.component";
import {FormChampLibreComponent} from "@shared-global/components/form-champ-libre/form-champ-libre.component";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";


@Component({
  selector: 'atx-form-clause-contenu',
  templateUrl: './form-clause-contenu.component.html',
  styleUrls: ['./form-clause-contenu.component.css']
})
export class FormClauseContenuComponent implements OnInit, OnChanges {
  private _clause: ClauseBean;
  @Input()
  public  set clause(value: ClauseBean){
    this._clause = value;
    if (!!value) {
      console.log("Chargement des formulaires selon le type de clause");
      if (!!value.id && !!value.idTypeClause) {
        this.chargerFormulaire(value.idTypeClause);
      }
    }
  }

  public get clause(){
    return this._clause;
  }
  @Input() idTypeClause: number;
  @Input() parametrage: boolean;
  @Input() fromEcranSurcharge: boolean;
  @Input() action: FormActionEnum;
  displayMessageVersion: boolean;
  @ViewChild(FormulaireClauseDirective, {static: true}) formulaireHost!: FormulaireClauseDirective;
  @Input() type: string;
  @Input() editeur: boolean;
  @Input() disabled = false;
  @Input() submitted : boolean;
  refTypeClause$: Observable<Array<Directory>>;

  constructor(private readonly store: Store<State>, private componentFactoryResolver: ComponentFactoryResolver) {
    this.refTypeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CLAUSE)?.content);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!!this.idTypeClause) {
      this.chargerFormulaire(this.idTypeClause);
    }

  }

  ngOnInit(): void {
    if (this.editeur && !!this.clause.idClause && !!this.clause.idLastPublication && this.action===1) {
      this.displayMessageVersion = true;
    }

  }

  chargerFormulaire(idTypeClause: number) {
    switch (idTypeClause) {
      case 2:
        this.afficherFormulaire(FormTexteFixeComponent);
        break;
      case 3:
        this.afficherFormulaire(FormChampLibreComponent);
        break;
      case 4:
        this.afficherFormulaire(FormChampPrevaloriseComponent);
        break;
      case 6:
        this.afficherFormulaire(FormChoixExclusifComponent);
        break;
      case 7:
        this.afficherFormulaire(FormChoixMultipleComponent);
        break;
      case 9:
        this.afficherFormulaire(FormValeurHeriteeComponent);
        break;
      default:
        console.log("Id type clause invalide");
    }
  }

  afficherFormulaire(nomComposant: any) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(nomComposant);

    const viewContainerRef = this.formulaireHost.viewContainerRef;
    viewContainerRef.clear();
    const component = viewContainerRef.createComponent<any>(componentFactory);
    component.instance.clause = this.clause;
    component.instance.disabled = this.disabled;
    component.instance.submitted = this.submitted;
    component.instance.editeur = this.editeur;
    component.instance.parametrageAgent = this.parametrage;
    component.instance.fromEcranSurcharge = this.fromEcranSurcharge;
  }


}
