import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Store} from "@ngrx/store";
import {catchError} from "rxjs/operators";
import {Injectable} from "@angular/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {State} from "@shared-global/store"
import {logoutUser} from "@shared-global/store/user/user.action";
import {SessionExpiredComponent} from "@shared-global/components/session-expired/session-expired.component";
import {showErrorToast} from "@shared-global/store/toast/toast.action";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  bsModalRef!: BsModalRef;

  constructor(private modalService: BsModalService, private readonly store: Store<State>) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if ([401].indexOf(err.status) !== -1) {
        this.store.dispatch(logoutUser());
        let config = {
          backdrop: true,
          ignoreBackdropClick: true,
          class: "modal-sm"
        };
        this.bsModalRef = this.modalService.show(SessionExpiredComponent, config);
      }

      const error = err.error.message || err.statusText;
      this.store.dispatch(showErrorToast({
        header: "error.header",
        message: "Une erreur est survenue, veuillez contacter l'administrateur."
      }))
      return throwError(error);
    }))
  }
}
