import {Produit} from "@shared-global/core/models/api/clausier-core.api";

export class AgentConfigurationModel {
  token: string;
  produits: Array<Produit>;
  plateformeUuid: string;



}
