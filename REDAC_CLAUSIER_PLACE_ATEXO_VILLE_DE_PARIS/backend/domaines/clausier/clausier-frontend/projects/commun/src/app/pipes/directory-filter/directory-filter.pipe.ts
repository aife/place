import {Inject, Pipe, PipeTransform} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";
import {Directory} from "@shared-global/core/models/api/clausier.api";

@Pipe({
    name: 'directoryFilter'
})
export class DirectoryFilterPipe implements PipeTransform {


    constructor(@Inject(APP_BASE_HREF) private baseHref: string) {
    }

    transform(list: Array<Directory>, filter: number[]): string {
        let tous = list.find(value => value.id === 0);
        if (!list || !filter) {
            return tous ? tous.label : null;
        }
        let directory = list.filter(value => filter.includes(value.id)).map(value => value.label).join(", ");
        if (directory?.length > 0)
            return directory;
        return tous ? tous.label : null;
    }

}
