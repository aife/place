import {CanevasSearch} from "@shared-global/core/models/api/clausier.api";

export class CanevasFilterModel implements CanevasSearch {
    actif: boolean;
    dateModificationMax: Date;
    dateModificationMin: Date;
    idNaturePrestation: number;
    idProcedure: number;
    idStatutRedactionClausier: number;
    idTypeContrat: number;
    idTypeDocument: number;
    page: number;
    referenceCanevas: string;
    referenceClause: string;
    size: number;
    typeAuteur: number;
    editeur: boolean;
    asc: boolean;
    sortField: string;
    titreCanevas: string;
    idCCAG: number;

    constructor(editeur: boolean) {
        this.editeur = editeur;
        this.idTypeDocument = 1;
        this.idNaturePrestation = 0;
        this.idStatutRedactionClausier = 0;
        this.typeAuteur = 0;
        this.actif = null;
        this.referenceCanevas = null;
        this.referenceClause = null;
        this.idTypeContrat = 0;
        this.idProcedure = 0;
        this.dateModificationMin = null;
        this.dateModificationMax = null;
        this.titreCanevas = null;
        this.idCCAG = 0;
    }
}
