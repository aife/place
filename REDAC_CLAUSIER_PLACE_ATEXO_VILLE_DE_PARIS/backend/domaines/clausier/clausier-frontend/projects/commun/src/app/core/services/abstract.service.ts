import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PageRepresentation} from '@shared-global/core/models/api/clausier.api';
import {PageableModel} from '@shared-global/core/models/pageable.model';


export abstract class AbstractService<T, V, P> {

  protected readonly BASE_PATH: string;

  protected constructor(public readonly http: HttpClient, path: string) {
    this.BASE_PATH = path;
  }

  list(filter: any, pageable: PageableModel): Observable<PageRepresentation<T>> {
    let params: any = {
      ...pageable,
      ...this.getParams(filter)
    };
    return this.http.get<PageRepresentation<T>>(this.BASE_PATH + '.htm', {
      params: params
    });
  }

  getById(id: number, idPublication: number, editeur: boolean, duplication: boolean, surcharge: boolean): Observable<V> {
    let params: any = {editeur, duplication, surcharge};
    if (idPublication) {
      params = {...params, idPublication};
    }
    return this.http.get<V>(`${this.BASE_PATH}/${id}.htm`,
      {params: params});

  }

  create(model: V, editeur: boolean): Observable<V> {
    return this.http.post<V>(`${this.BASE_PATH}.htm`, model, {params: {editeur}});
  }

  modify(id, idPublication: number, model: V): Observable<V> {
    let url = `${this.BASE_PATH}/${id}.htm`;
    if (idPublication) {
      url += `?idPublication=${idPublication}`;
    }
    return this.http.put<V>(url, model);
  }

  clone(id, idPublication: number, model: V, editeur: boolean): Observable<V> {
    let params: any = {editeur};
    if (idPublication) {
      params = {...params, idPublication};
    }
    return this.http.put<V>(`${this.BASE_PATH}/${id}/clone.htm`, model, {params});
  }

  surcharge(id: number, idPublication: number, editeur: boolean, model: V, surchargeActif: boolean): Observable<V> {
    let params: any = {editeur};
    if (surchargeActif !== undefined) {
      params = {...params, surchargeActif};
    }
    if (idPublication) {
      params = {...params, idPublication};
    }
    return this.http.put<V>(`${this.BASE_PATH}/${id}/surcharge.htm`, model, {params});

  }

  changeStatus(id: number, idPublication: number, idStatus: any): Observable<T> {
    let url = `${this.BASE_PATH}/${id}/statut.htm`;
    if (idPublication) {
      url += `?idPublication=${idPublication}`;
    }
    return this.http.patch<T>(url, idStatus);
  }

  delete(id: number, idPublication: number): Observable<T> {
    let url = `${this.BASE_PATH}/${id}.htm`;
    if (idPublication) {
      url += `?idPublication=${idPublication}`;
    }
    return this.http.delete<T>(url);
  }

  preview(id: number, editeur: boolean, agent?: boolean, direction?: boolean, surcharge?: boolean, idPublication?: number): Observable<P> {
    let params: any = {editeur};
    if (idPublication) {
      params = {...params, idPublication};
    }
    if (direction) {
      params = {...params, direction};
    }
    if (surcharge) {
      params = {...params, surcharge};
    }
    if (agent) {
      params = {...params, agent};
    }
    return this.http.get<P>(`${this.BASE_PATH}/${id}/preview.htm`,
      {params});
  }


  getParams(filter: any) {
    let params: any = {};
    if (!filter)
      return params;

    Object.keys(filter).filter(key => filter[key] !== null && filter[key] !== undefined)
      .forEach(key => {
        params[key] = filter[key];
      });
    return params;
  }

}
