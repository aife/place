import * as Quill from 'quill';

export class IndentAttributorComponent extends Quill.import('parchment').Attributor.Style {
  multiplier = 2;

  constructor(name: string, style: string, params: any) {
    super(name, style, params);
  }

  add(node, value) {
    return super.add(node, `${value * this.multiplier}rem`);
  }

  value(node) {
    return parseFloat(super.value(node)) / this.multiplier || undefined;
  }
}
