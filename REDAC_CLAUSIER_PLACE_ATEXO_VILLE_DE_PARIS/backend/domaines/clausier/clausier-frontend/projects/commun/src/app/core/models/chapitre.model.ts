import {CanevasChapitre, Chapitre, Clause, InfoBulle} from "@shared-global/core/models/api/clausier.api";
import {DerogationModel} from "@shared-global/core/models/derogation.model";
import {InfoBulleModel} from "@shared-global/core/models/info-bulle.model";

export class ChapitreModel implements Chapitre {
  afficherMessageDerogation: boolean;
  chapitres: Chapitre[];
  clauses: Clause[];
  derogation: DerogationModel;
  idChapitre: number;
  idPublication: number;
  infoBulle: InfoBulle;
  numero: string;
  parent: CanevasChapitre;
  styleChapitre: string;
  titre: string;

  constructor() {
    this.infoBulle = new InfoBulleModel();
  }

}
