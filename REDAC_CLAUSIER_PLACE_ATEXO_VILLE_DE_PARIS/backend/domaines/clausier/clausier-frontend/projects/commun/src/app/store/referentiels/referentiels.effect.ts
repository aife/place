import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  errorGetReferentiel,
  getCCAGReferentiel,
  getNaturePrestationReferentiel,
  getProcedureReferentiel,
  getStatutReferentiel,
  getThemeClauseReferentiel,
  getTypeAuteurCanevasReferentiel,
  getTypeClauseReferentiel,
  getTypeContratReferentiel,
  getTypeDocumentReferentiel,
  getValeurHeriteeReferentiel,
  successGetReferentiel
} from './referentiels.action';
import {catchError, mergeMap} from 'rxjs/operators';
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";


@Injectable()
export class ReferentielsEffect {
  constructor(
    private readonly actions$: Actions,
    private readonly referentielService: ReferentielsService
  ) {
  }


  loadReferentialProcedureListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getProcedureReferentiel),
    mergeMap((action) =>
      this.referentielService.allProcedures().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.PROCEDURE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.PROCEDURE
      })]))
    )));

  loadReferentialValeurHeriteeListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getValeurHeriteeReferentiel),
    mergeMap((action) =>
      this.referentielService.allValeursHeritees().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.VALEUR_HERITEE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.VALEUR_HERITEE
      })]))
    )));



  loadReferentialTypeContratListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getTypeContratReferentiel),
    mergeMap((action) =>
      this.referentielService.allTypesContrat().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.TYPE_CONTRAT
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.TYPE_CONTRAT
      })]))
    )));

  loadReferentialTypeClauseListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getTypeClauseReferentiel),
    mergeMap((action) =>
      this.referentielService.allTypesClause().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.TYPE_CLAUSE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.TYPE_CLAUSE
      })]))
    )));

  loadReferentialTypeDocumentListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getTypeDocumentReferentiel),
    mergeMap((action) =>
      this.referentielService.allTypesDocument().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.TYPE_DOCUMENT
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.TYPE_DOCUMENT
      })]))
    )));

  loadReferentialTypeAuteurCanevasListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getTypeAuteurCanevasReferentiel),
    mergeMap((action) =>
      this.referentielService.allTypeAuteursCanevas().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.TYPE_AUTEUR_CANEVAS
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.TYPE_AUTEUR_CANEVAS
      })]))
    )));
  loadReferentialTypeAuteurClausesListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getTypeAuteurCanevasReferentiel),
    mergeMap((action) =>
      this.referentielService.allTypeAuteursClauses().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.TYPE_AUTEUR_CLAUSE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.TYPE_AUTEUR_CLAUSE
      })]))
    )));

  loadReferentialNatureListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getNaturePrestationReferentiel),
    mergeMap((action) =>
      this.referentielService.allNatures().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.NATURE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.NATURE
      })]))
    )));

  loadReferentialCCAGListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getCCAGReferentiel),
    mergeMap((action) =>
      this.referentielService.allRefCCAG().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.CCAG
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.CCAG
      })]))
    )));

  loadReferentialThemeClauseListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getThemeClauseReferentiel),
    mergeMap((action) =>
      this.referentielService.allThemesClause().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.THEME_CLAUSE
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.THEME_CLAUSE
      })]))
    )));

  loadReferentialStatutListFromType$ = createEffect(() => this.actions$.pipe(
    ofType(getStatutReferentiel),
    mergeMap((action) =>
      this.referentielService.allStatut().pipe(mergeMap(data => {
        return [successGetReferentiel({
          list: data.filter(value => value.actif),
          refType: ReferentielEnum.STATUT
        })];
      }), catchError((error) => [errorGetReferentiel({
        error,
        refType: ReferentielEnum.STATUT
      })]))
    )));
}
