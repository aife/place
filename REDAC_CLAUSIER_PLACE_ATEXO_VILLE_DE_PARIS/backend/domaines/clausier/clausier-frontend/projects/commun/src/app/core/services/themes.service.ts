import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APP_BASE_HREF} from "@angular/common";


@Injectable({
  providedIn: 'root'
})
export class ThemesService {


  private RELATIVE_PATH = '/';
  private readonly BASE_PATH = 'clausier-api/themes';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }

  getRemoteCss(): Promise<string> {
    const apiUrl = this.RELATIVE_PATH + this.BASE_PATH + '/css/mpe-new-client.css';
    return this.http.get(apiUrl, {responseType: 'text'}).toPromise();
  }
}
