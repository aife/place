import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseTextePrevaloriseFormModel} from "@shared-global/core/models/ClauseTextePrevaloriseForm.model";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-champ-prevalorise',
  templateUrl: './form-champ-prevalorise.component.html',
  styleUrls: ['./form-champ-prevalorise.component.scss']
})
export class FormChampPrevaloriseComponent extends BaseFormQuillComponent implements OnInit {

  submitted: boolean;
  @Input() disabled: boolean;
  @Input() editeur: boolean;
  @Input() parametrageAgent: boolean;
  @Input() fromEcranSurcharge: boolean;
  @Input() clause: ClauseBean;
  sautLigne1: boolean = false;
  sautLigne2: boolean = false;
  paramDirection: boolean = false;
  paramAgent: boolean = false;
  defaultTextColor = 'black';

  constructor() {
    super();
  }

  public ngOnInit(): void {
    //modification
    if (!!this.clause.clauseTextePrevalorise) {
      this.submitted = true;
      if (this.clause.clauseTextePrevalorise.sautTextFixeAvant === "true") {
        this.sautLigne1 = true;
      }
      if (this.clause.clauseTextePrevalorise.sautTextFixeApres === "true") {
        this.sautLigne2 = true;
      }
      if (this.clause.clauseTextePrevalorise.parametrableDirection === "1") {
        this.paramDirection = true;
      }
      if (this.clause.clauseTextePrevalorise.parametrableAgent === "1") {
        this.paramAgent = true;
      }

    } else { //creation
      this.clause.clauseTextePrevalorise = new ClauseTextePrevaloriseFormModel();
      this.clause.clauseTextePrevalorise.textFixeAvant = "";
      this.clause.clauseTextePrevalorise.textFixeApres = "";
      this.clause.clauseTextePrevalorise.sautTextFixeAvant = String(false);
      this.clause.clauseTextePrevalorise.sautTextFixeApres = String(false);
      this.clause.clauseTextePrevalorise.tailleChamps = 1;
      this.clause.clauseTextePrevalorise.parametrableDirection = "0";
      this.clause.clauseTextePrevalorise.parametrableAgent = "0";
      this.clause.clauseTextePrevalorise.tailleChamps = 1;
    }
  }

  updateSautLigne1() {
    this.clause.clauseTextePrevalorise.sautTextFixeAvant = String(this.sautLigne1);
  }

  updateSautLigne2() {
    this.clause.clauseTextePrevalorise.sautTextFixeApres = String(this.sautLigne2);
  }

  updateParamDirection() {
    if (this.paramDirection) {
      this.clause.clauseTextePrevalorise.parametrableDirection = "1";
    } else {
      this.clause.clauseTextePrevalorise.parametrableDirection = "0";
    }
  }

  updateParamAgent() {
    if (this.paramAgent) {
      this.clause.clauseTextePrevalorise.parametrableAgent = "1";
    } else {
      this.clause.clauseTextePrevalorise.parametrableAgent = "0";
    }
  }

  cleanTexteAvant() {
    this.clause.clauseTextePrevalorise.textFixeAvant = this.removeTagForEmptyField(this.clause.clauseTextePrevalorise.textFixeAvant);
  }

  cleanTexteApres() {
    this.clause.clauseTextePrevalorise.textFixeApres = this.removeTagForEmptyField(this.clause.clauseTextePrevalorise.textFixeApres);
  }

}
