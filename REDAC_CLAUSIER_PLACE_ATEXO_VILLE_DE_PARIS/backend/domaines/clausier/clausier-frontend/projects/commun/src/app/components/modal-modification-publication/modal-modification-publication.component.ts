import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";

@Component({
  selector: 'atx-modal-derogation',
  templateUrl: './modal-modification-publication.component.html',
  styleUrls: ['./modal-modification-publication.component.scss']
})
export class ModalModificationPublicationComponent {

  @Input() titre = '';
  @Input() publication: PublicationClausierBean;


  onClose = new Subject<boolean>();

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit() {
    this.onClose.next(true)
    this.bsModalRef.hide();
  }

}
