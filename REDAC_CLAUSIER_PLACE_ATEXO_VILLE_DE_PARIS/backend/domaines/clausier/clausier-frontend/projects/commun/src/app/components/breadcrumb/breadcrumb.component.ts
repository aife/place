import {Component, EventEmitter, Inject, Input, Output} from '@angular/core';
import {BreadcrumbModel} from "@shared-global/core/models/breadcrumb.model";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {APP_BASE_HREF} from "@angular/common";

@Component({
  selector: 'atx-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent {

  @Input() breadcrumb: BreadcrumbModel;
  @Input() information = false;
  @Input() loadingExport = false;
  @Input() loadingActivation = false;
  @Input() create = false;
  @Input() createLabel: string;
  @Input() export = false;
  @Input() import = false;
  @Input() creationRoles: string[] = [];
  @Input() creationLink = '';
  @Output() onClickInformation = new EventEmitter<boolean>();
  @Output() onClickExport = new EventEmitter<Date>();
  @Output() onClickImport = new EventEmitter<Date>();
  isWebComponent = false;

  constructor(private readonly routingInterneService: RoutingInterneService,
              private store: Store<State>, @Inject(APP_BASE_HREF) public baseHref: string) {
    this.isWebComponent = this.baseHref.includes('web-component-clausier')
    this.store.select(state => state.userReducer.isWebComponent)
      .subscribe(value => {
        this.isWebComponent = value || this.baseHref.includes('web-component-clausier');
      })
  }

  navigateTo(link: string) {
    this.routingInterneService.navigateFromContexte(link);

  }


  exporter() {
    this.onClickExport.emit(new Date());
  }

  importer() {
    this.onClickImport.emit(new Date());
  }
}
