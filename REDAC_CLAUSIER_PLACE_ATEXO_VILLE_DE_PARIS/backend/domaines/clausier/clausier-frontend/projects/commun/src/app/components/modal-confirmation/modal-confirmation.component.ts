import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";

@Component({
  selector: 'atx-modal-derogation',
  templateUrl: './modal-confirmation.component.html',
  styleUrls: ['./modal-confirmation.component.scss']
})
export class ModalConfirmationComponent {

  @Input() titre = 'confirmation';
  @Input() reference;
  @Input() messages: string[] = [];
  @Output() onSave = new EventEmitter<boolean>();

  onClose = new Subject<boolean>();

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit() {
    this.onClose.next(true)
    this.bsModalRef.hide();
    this.onSave.emit(true)
  }

}
