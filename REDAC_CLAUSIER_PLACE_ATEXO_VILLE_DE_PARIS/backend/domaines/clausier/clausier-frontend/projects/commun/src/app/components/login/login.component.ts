import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {State} from '@shared-global/store';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {setDefaultParams} from '@shared-global/store/user/user.action';

@Component({
  selector: 'atx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  message = "Vous n'êtes pas connectés";
  connecting$ = of(false)

  constructor(
    private route: ActivatedRoute,
    private readonly store: Store<State>,
  ) {
    console.log('login component')
    this.route.queryParams.subscribe(params => {
      console.log("queryParams", params)
      if (params.token != null) {
        this.login(params.token, params);
      } else if (params.identifiantSaaS != null) {
        this.login(params.identifiantSaaS, params);

      }
    });
    this.connecting$ = this.store.select(state => state.userReducer.connecting);
  }


  login(token: string, query: any, refeshToken?: string) {
    this.store.dispatch(setDefaultParams({
      isWebComponent: query["isWebComponent"] === 'true',
      token,
      refeshToken,
      defaultPage: query["redirect"]
    }))

  }


}
