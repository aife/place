import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[formulaire-clause]',
})
export class FormulaireClauseDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
