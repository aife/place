import {BaseBean, CommunSearch, PageRepresentation} from "@shared-global/core/models/api/clausier.api";
import {PageableModel} from "@shared-global/core/models/pageable.model";
import {AbstractService} from "@shared-global/core/services/abstract.service";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";

export abstract class BaseDashboardComponent<M extends BaseBean, F extends CommunSearch> {

  public pageable: PageableModel = {page: 0, size: 10, asc: false, sortField: 'dateModification'};
  public loading = false;
  public showDetails = false;
  public total = 0;
  public page: PageRepresentation<M>;
  public list: Array<M> = [];

  public filter: F;
  protected _filterKey: string;
  protected _pageableKey: string;


  constructor(protected readonly store: Store<State>,
              protected abstractService: AbstractService<M, any, any>) {

  }

  start(filterKey, pageableKey): void {
    this._pageableKey = pageableKey;
    this._filterKey = filterKey;
    if (this._filterKey) {
      let filter = sessionStorage.getItem(this._filterKey);
      if (!!filter && filter !== 'null') {
        this.filter = JSON.parse(filter)
      }
    }
    if (this._pageableKey) {
      let pageable = sessionStorage.getItem(this._pageableKey);
      if (!!pageable && pageable !== 'null') {
        this.pageable = JSON.parse(pageable)
      }
    }

    if (!this.filter) {
      this.initFilter();
    }
    this.searchPage(this.filter, this.pageable);
  }


  protected abstract initFilter();

  public searchPage(filter: F, pageable: PageableModel) {
    if (filter && this._filterKey)
      sessionStorage.setItem(this._filterKey, JSON.stringify(filter))
    if (pageable && this._pageableKey)
      sessionStorage.setItem(this._pageableKey, JSON.stringify(pageable))
    this.loading = true;
    this.abstractService.list(filter, pageable).subscribe(value => {
      this.loading = false;
      this.page = value;
      this.total = value.totalElements;
      this.list = this.page.content;
    }, () => {
      this.list = [];
      this.page = null;
      this.total = null;
      this.loading = false;
    });

  }

}


