import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'tailleFromNbCaracteres',
  pure: false
})
export class NumberFromTaillePipe implements PipeTransform {

  transform(taille?: string): string {
    switch (taille) {
      case "Long":
        return "1";
      case "Moyen":
        return "2";
      case "Court":
        return "3";
      case "Très long":
        return "4";
      default:
        return "3";
    }
  }
}






