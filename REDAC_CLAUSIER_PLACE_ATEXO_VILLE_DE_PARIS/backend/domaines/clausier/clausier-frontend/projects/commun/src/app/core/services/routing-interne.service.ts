import {Router} from "@angular/router";
import {Inject, Injectable} from "@angular/core";
import {APP_BASE_HREF, Location} from "@angular/common";


@Injectable()
export class RoutingInterneService {
  constructor(@Inject(APP_BASE_HREF) public baseHref: string, public readonly router: Router, private location: Location) {

  }

  navigateFromContexte(path: string, queryParams?: any) {
    console.log('routing vers ', path, queryParams, this.baseHref)
    switch (this.baseHref) {
      case '/redac/web-component-clausier':
        this.router.navigate([{outlets: {clausier: path}}], {
          queryParams,
          skipLocationChange: true
        });
        window.dispatchEvent(new CustomEvent("change-route-interne", {
          detail: {path},
          composed: true // Like this
        }))

        return;
      default:
        this.router.navigate([path], {queryParams});
    }
  }

  back() {
    this.location.back();
  }
}
