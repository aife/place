import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {frLocale} from "ngx-bootstrap/locale";
import {defineLocale} from "ngx-bootstrap/chronos";
import {BsDatepickerConfig, BsLocaleService} from "ngx-bootstrap/datepicker";
import {ClauseSearch, Directory} from "@shared-global/core/models/api/clausier.api";
import moment from 'moment';
import {ClauseFilterModel} from '@shared-global/core/models/clause-filter.model';
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {getProcedureReferentiel} from "@shared-global/store/referentiels/referentiels.action";


@Component({
  selector: 'atx-select-clauses-filter',
  templateUrl: './select-clauses-filter.component.html',
  styleUrls: ['./select-clauses-filter.component.css']
})
export class SelectClausesFilterComponent implements OnInit {

  @Input() editeur = true;

  @Input() filter: ClauseSearch;
  @Output() filterUsed = new EventEmitter();
  @Output() onChangeFilter = new EventEmitter<ClauseFilterModel>();
  refStatut$: Observable<Array<Directory>>;
  refNature$: Observable<Array<Directory>>;
  refProcedure$: Observable<Array<Directory>>;
  refTypeContrat$: Observable<Array<Directory>>;
  refTypeDocument$: Observable<Array<Directory>>;
  refThemeClause$: Observable<Array<Directory>>;
  refTypeClause$: Observable<Array<Directory>>;
  refTypeActeur$: Observable<Array<Directory>>;
  bsConfig: Partial<BsDatepickerConfig>
  @Input() showMore = false;

  constructor(private bsLocaleService: BsLocaleService, private readonly store: Store<State>) {
    frLocale.invalidDate = 'Date invalide';
    defineLocale('fr', frLocale);
    this.bsLocaleService.use('fr');
    this.bsConfig = {
      containerClass: 'theme-dark-blue',
      adaptivePosition: true,
      dateInputFormat: 'dd/MM/YYYY',
      maxDate: moment().subtract(1, "day").toDate(),
      minDate: moment(new Date("01/03/2000")).toDate(),
    }
  }

  ngOnInit(): void {
    this.refStatut$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.STATUT)?.content);
    this.refNature$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.NATURE)?.content);
    this.refTypeContrat$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CONTRAT)?.content);
    this.refTypeDocument$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_DOCUMENT)?.content);
    this.refProcedure$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.PROCEDURE)?.content);
    this.refThemeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.THEME_CLAUSE)?.content);
    this.refTypeClause$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_CLAUSE)?.content);
    if (!this.editeur) {
      this.refTypeActeur$ = this.store.select(state => state.referentielReducer.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CLAUSE)?.content);
    }
  }


  rechercher() {
    this.onChangeFilter.emit(this.filter);
  }

  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('day');
  }

  changeStatutFilter(id: number) {
    this.filter.idStatutRedactionClausier = id;
  }

  changeNatureFilter(id: number) {
    this.filter.idNaturePrestation = id;
  }


  changeActivationFilter(activation: number) {
    if (activation === 0) {
      this.filter.actif = null;
    } else if (activation === 1) {
      this.filter.actif = true;
    } else if (activation === 2) {
      this.filter.actif = false;
    }
  }

  getStatutIcon(id: number) {
    if (id === 0) {
      return "";
    } else if (id === 1) {
      return "ft-edit-1";
    } else if (id === 2) {
      return "fa fa-hourglass";
    } else if (id === 3) {
      return "ft-check-circle";
    }
  }

  getNatureIcon(id: number) {
    if (id === 0) {
      return "";
    } else if (id === 1) {
      return "fa fa-wrench";
    } else if (id === 2) {
      return "fa fa-shopping-cart";
    } else if (id === 3) {
      return "fa fa-briefcase";
    }
  }

  reInitFilter() {
    this.filter = new ClauseFilterModel(this.editeur)
    this.rechercher();
  }


}
