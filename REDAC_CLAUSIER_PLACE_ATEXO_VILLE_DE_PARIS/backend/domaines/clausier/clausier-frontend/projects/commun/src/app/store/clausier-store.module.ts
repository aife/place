import {NgModule} from '@angular/core';
import {reducers} from './index';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {UserEffect} from '@shared-global/store/user/user.effect';
import {ToastEffect} from "@shared-global/store/toast/toast.effect";
import {ReferentielsEffect} from "@shared-global/store/referentiels/referentiels.effect";
import {ReferentielsService} from "@shared-global/core/services/referentiels.service";
import {AgentService} from "@shared-global/core/services/agent.service";


@NgModule({
  imports: [
    StoreModule.forRoot(reducers),

    StoreRouterConnectingModule.forRoot({
      stateKey: 'router'
    }),

    EffectsModule.forRoot([
      UserEffect,
      ToastEffect,
      ReferentielsEffect

    ]),
    StoreDevtoolsModule.instrument({
      name: '[CLAUSIER-APP]',
      maxAge: 100
    })
  ],
  providers: [
    ReferentielsService, AgentService
  ]
})
export class ClausierStoreModule {
}
