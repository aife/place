import {PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";

export class PublicationClausierModel implements PublicationClausierBean {
  actif: boolean;
  commentaire: string;
  dateActivation: Date;
  dateIntegration: Date;
  datePublication: Date;
  datePublicationMax: Date;
  datePublicationMin: Date;
  editeur: string;
  gestionLocal: boolean;
  gestionLocalRecherche: number;
  id: number;
  idClausierFile: number;
  idOrganisme: number;
  idReferentielFile: number;
  idUtilisateur: number;
  nomCompletUtilisateur: string;
  version: string;
  version1: string;
  version2: string;
  version3: string;
  enCoursActivation: boolean;
  selected: boolean;
}
