import {NumberFormatPipe} from './number-format.pipe';
import {async, inject, TestBed} from "@angular/core/testing";
import {CurrencyPipe} from "@angular/common";

describe('NumberFormatPipe', () => {

  const spyCurrencyPipe = jasmine.createSpyObj('spyCurrencyPipe',['transform']);
  spyCurrencyPipe.transform.and.returnValue()
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [NumberFormatPipe,
        {provide: CurrencyPipe,
        useValue: spyCurrencyPipe
        }]
    });
  }));
  let pipe : NumberFormatPipe;
  beforeEach(inject([NumberFormatPipe], (n: NumberFormatPipe) => {
    pipe = n;
  }));
  it('should transform "1260" to "1260 €"', () => {
    let nb = 1260;
    spyCurrencyPipe.transform.and.returnValue('1260 €');
    let result = pipe.transform(nb,true,'HT');
    expect(spyCurrencyPipe.transform).toHaveBeenCalledWith('1260','EUR','symbol','1.0-0');
    expect(result).toBe('1260 € HT');
  });
});
