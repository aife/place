import {Component, ViewChild} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {PreviewClauseDirective} from "@shared-global/directives/preview-clause/preview-clause.directive";
import {Clause, ClauseBean, ClauseDocument} from "@shared-global/core/models/api/clausier.api";

@Component({
    selector: 'atx-modal-preview-clause',
    templateUrl: './modal-preview-clause.component.html',
    styleUrls: ['./modal-preview-clause.component.scss']
})
export class ModalPreviewClauseComponent {

    @ViewChild(PreviewClauseDirective, {static: true}) formulaireHost!: PreviewClauseDirective;
    titre = 'confirmation'
    editeur;
    agent;
    surcharge;
    direction;
    clauseBean: ClauseBean;
    clauseDocument: ClauseDocument
    loading = false;

    constructor(public bsModalRef: BsModalRef) {

    }

    closeModal() {
        this.bsModalRef.hide();
    }

}
