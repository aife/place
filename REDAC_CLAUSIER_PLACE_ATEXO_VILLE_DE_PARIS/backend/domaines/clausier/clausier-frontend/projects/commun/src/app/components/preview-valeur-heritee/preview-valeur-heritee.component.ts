import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {ClauseDocument, ClauseTexte, Directory} from "@shared-global/core/models/api/clausier.api";
import {BasePreviewQuillComponent} from "@shared-global/components/base-preview-quill/base-preview-quill.component";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {Observable} from "rxjs";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-valeur-heritee',
  templateUrl: './preview-valeur-heritee.component.html',
  styleUrls: ['./preview-valeur-heritee.component.scss']
})
export class PreviewValeurHeriteeComponent extends BasePreviewQuillComponent {

  clauseDocument: ClauseTexte;

  constructor(private readonly store: Store<State>) {
    super();
  }

}
