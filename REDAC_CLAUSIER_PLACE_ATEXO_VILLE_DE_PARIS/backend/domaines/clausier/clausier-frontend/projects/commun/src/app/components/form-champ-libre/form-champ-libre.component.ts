import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseTexteLibreFormModel} from "@shared-global/core/models/ClauseTexteLibreForm.model";
import {TailleFromNbCaracteresPipe} from "@shared-global/pipes/taille-from-nbCaracteres/taille-from-nbCaracteres.pipe";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-champ-libre',
  templateUrl: './form-champ-libre.component.html',
  styleUrls: ['./form-champ-libre.component.scss']
})
export class FormChampLibreComponent extends BaseFormQuillComponent implements OnInit {
  submitted: boolean;
  tailles: string[] = ['Très long', 'Long', 'Moyen', 'Court'];
  @Input() clause: ClauseBean;
  @Input() disabled: boolean;
  tailleChamp: string = "Long";
  sautLigne1: boolean = false;
  sautLigne2: boolean = false;
  defaultTextColor = 'black';

  constructor(private readonly tailleFromNbCaracteresPipe: TailleFromNbCaracteresPipe) {
    super();
  }


  public ngOnInit(): void {
    //modification
    if (!!this.clause.clauseTexteLibre) {
      if (!!this.clause.clauseTexteLibre.tailleChamps) {
        this.tailleChamp = this.tailleFromNbCaracteresPipe.transform(this.clause.clauseTexteLibre.tailleChamps);
      } else {
        this.tailleChamp = this.tailleFromNbCaracteresPipe.transform(1);
      }
      if (this.clause.clauseTexteLibre.sautTextFixeAvant === "true") {
        this.sautLigne1 = true;
      }
      if (this.clause.clauseTexteLibre.sautTextFixeApres === "true") {
        this.sautLigne2 = true;
      }
    } else { //creation
      this.clause.clauseTexteLibre = new ClauseTexteLibreFormModel();
      this.clause.clauseTexteLibre.textFixeAvant = "";
      this.clause.clauseTexteLibre.textFixeApres = "";
      this.clause.clauseTexteLibre.sautTextFixeAvant = String(false);
      this.clause.clauseTexteLibre.sautTextFixeApres = String(false);
      this.clause.clauseTexteLibre.obligatoire = true;
      this.clause.clauseTexteLibre.tailleChamps = 1;
      this.tailleChamp = this.tailleFromNbCaracteresPipe.transform(1);
    }
  }

  updateTaille() {
    switch (this.tailleChamp) {
      case "Long":
        this.clause.clauseTexteLibre.tailleChamps = 1;
        break;
      case "Moyen":
        this.clause.clauseTexteLibre.tailleChamps = 2;
        break;
      case "Court":
        this.clause.clauseTexteLibre.tailleChamps = 3;
        break;
      case "Très long":
        this.clause.clauseTexteLibre.tailleChamps = 4;
        break;
      default:
        this.clause.clauseTexteLibre.tailleChamps = 1;
    }
  }

  updateSautLigne1() {
    this.clause.clauseTexteLibre.sautTextFixeAvant = String(this.sautLigne1);
  }

  updateSautLigne2() {
    this.clause.clauseTexteLibre.sautTextFixeApres = String(this.sautLigne2);
  }

  updateChampObligatoire() {
    if (this.clause.clauseTexteLibre.obligatoire) {
      this.clause.clauseTexteLibre.champLibreObligatoire="1"
    } else {
      this.clause.clauseTexteLibre.champLibreObligatoire="0"
    }
  }

  cleanTexteAvant() {
    this.clause.clauseTexteLibre.textFixeAvant = this.removeTagForEmptyField(this.clause.clauseTexteLibre.textFixeAvant);
  }

  cleanTexteApres() {
    this.clause.clauseTexteLibre.textFixeApres = this.removeTagForEmptyField(this.clause.clauseTexteLibre.textFixeApres);
  }
}
