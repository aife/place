import {createAction, props} from '@ngrx/store';
import {Agent} from "@shared-global/core/models/api/clausier-core.api";

export const setDefaultParams = createAction('[USER] Set setDefaultParams', props<{
  defaultPage: string,
  token: string,
  refeshToken: string,
  isWebComponent: boolean
}>());
export const setDefaultPage = createAction('[USER] Set DefaultPage', props<{
  defaultPage: string,
  queryParams: any
}>());
export const setIsWebComponent = createAction('[USER] Set WebComponent', props<{
  isWebComponent: boolean
}>());
export const getUser = createAction('[USER] Get User', props<{ redirect: string }>());

export const logoutUser = createAction('[USER] Logout User');
export const successLogout = createAction('[USER] Logout');

export const successGetUser = createAction('[USER] Success Get User', props<{ user: Agent }>());
export const errorGetUser = createAction('[USER] Error Get User', props<{ error: string }>());
