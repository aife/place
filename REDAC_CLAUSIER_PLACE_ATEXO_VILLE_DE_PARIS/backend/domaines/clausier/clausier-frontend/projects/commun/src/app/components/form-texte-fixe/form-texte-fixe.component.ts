import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseTexteFixeFormModel} from "@shared-global/core/models/ClauseTexteFixeForm.model";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-texte-fixe',
  templateUrl: './form-texte-fixe.component.html',
  styleUrls: ['./form-texte-fixe.component.scss']
})
export class FormTexteFixeComponent extends BaseFormQuillComponent implements OnInit {

  @Input() clause: ClauseBean;
  @Input() disabled: boolean;
  @Input() submitted: boolean;
  defaultTextColor = 'black';

  constructor() {
    super();
  }
  public ngOnInit(): void {

    if (!this.clause.clauseTexteFixe) {
      this.clause.clauseTexteFixe = new ClauseTexteFixeFormModel();
    } else {
      //modification
      this.submitted = true;
    }

  }


}
