import {createAction, props} from '@ngrx/store';
import {Directory} from "@shared-global/core/models/api/clausier.api";


export const getNaturePrestationReferentiel = createAction('[REFERENTIEL] Get Nature Prestation Referentiel List');
export const getProcedureReferentiel = createAction('[REFERENTIEL] Get Procedure Referentiel List');
export const getTypeContratReferentiel = createAction('[REFERENTIEL] Get Type Contrat Referentiel List');
export const getTypeClauseReferentiel = createAction('[REFERENTIEL] Get Type Clause Referentiel List');
export const getTypeDocumentReferentiel = createAction('[REFERENTIEL] Get Type Document Referentiel List');
export const getTypeAuteurCanevasReferentiel = createAction('[REFERENTIEL] Get Type Auteur Canevas Referentiel List');
export const getTypeAuteurClauseReferentiel = createAction('[REFERENTIEL] Get Type Auteur Clause Referentiel List');
export const getStatutReferentiel = createAction('[REFERENTIEL] Get Statut Referentiel List');
export const getCCAGReferentiel = createAction('[REFERENTIEL] Get Ref CCAG Referentiel List');
export const getThemeClauseReferentiel = createAction('[REFERENTIEL] Get Ref Theme Clause Referentiel List');
export const getValeurHeriteeReferentiel = createAction('[REFERENTIEL] Get Ref Valeur Héritée Referentiel List');

export const successGetReferentiel = createAction('[REFERNTIEL] Success Get Referentiel List', props<{ list: Array<Directory>, refType: string }>());
export const errorGetReferentiel = createAction('[REFERNTIEL] Error Get Referentiel List', props<{ error: string, refType: string }>());






