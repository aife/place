import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BasePreviewQuillComponent} from "@shared-global/components/base-preview-quill/base-preview-quill.component";
import {ClauseListe} from "@shared-global/core/models/api/clausier.api";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-choix-exclusif',
  templateUrl: './preview-choix-exclusif.component.html',
  styleUrls: ['./preview-choix-exclusif.component.scss']
})
export class PreviewChoixExclusifComponent extends BasePreviewQuillComponent {

  clauseDocument: ClauseListe;


}
