import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[preview-clause]',
})
export class PreviewClauseDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
