import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Agent} from "@shared-global/core/models/api/clausier-core.api";
import {APP_BASE_HREF} from "@angular/common";


@Injectable()
export class AgentService {

  private RELATIVE_PATH = '/';
  private BASE_PATH = 'clausier-api/agents/';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }

  whoami(): Observable<Agent> {
    return this.http.get<Agent>(`${this.RELATIVE_PATH}${this.BASE_PATH}whoami`);
  }
}
