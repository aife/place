/*=========================================================================================
    File Name: gauge.js
    Description: Chartist gauge chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Gauge chart
// ------------------------------
$(window).on("load", function(){

    new Chartist.Pie('#gauge-chart', {
		labels: ['E', 'EV', 'C', 'OA', 'D'],
        series: [2, 5, 7, 18, 12],
		
    }, {
        donut: true,
        donutWidth: 60,
        startAngle: 270,
        total: 88,
        showLabel: true
    });
});

$(window).on("load", function(){

    new Chartist.Pie('#accueilConsult', {
		labels: ['E', 'EV', 'C', 'OA', 'D'],
        series: [2, 5, 7, 18, 12],
		
    }, {
        donut: true,
        donutWidth: 60,

        showLabel: true
    });
});

$(window).on("load", function(){

    new Chartist.Pie('#accueilContrats', {
		labels: ['AS', 'AN', 'N'],
        series: [15, 25, 36],
		
    }, {
        donut: true,
        donutWidth: 60,

        showLabel: true
    });
});

$(window).on("load", function(){

    new Chartist.Pie('#accueilDemAchat', {
		labels: ['AV', 'VEC', 'V', 'E'],
        series: [5, 7, 3, 20],
		
    }, {
        donut: true,
        donutWidth: 60,

        showLabel: true
    });
});


$(window).on("load", function(){

    new Chartist.Pie('#accueilBesoinUnitaire', {
		labels: ['AV', 'VEC', 'V', 'E'],
        series: [13, 7, 14, 6],
		
    }, {
        donut: true,
        donutWidth: 60,

        showLabel: true
    });
});