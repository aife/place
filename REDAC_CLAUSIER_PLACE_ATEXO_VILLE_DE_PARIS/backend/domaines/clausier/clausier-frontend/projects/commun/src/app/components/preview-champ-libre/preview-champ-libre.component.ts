import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BasePreviewQuillComponent} from "@shared-global/components/base-preview-quill/base-preview-quill.component";
import {ClauseTexte} from "@shared-global/core/models/api/clausier.api";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-champ-libre',
  templateUrl: './preview-champ-libre.component.html',
  styleUrls: ['./preview-champ-libre.component.scss']
})
export class PreviewChampLibreComponent extends BasePreviewQuillComponent {
  clauseDocument: ClauseTexte;


}
