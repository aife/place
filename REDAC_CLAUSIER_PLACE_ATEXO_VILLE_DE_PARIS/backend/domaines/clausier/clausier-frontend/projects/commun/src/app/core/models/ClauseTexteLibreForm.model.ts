import {ClauseTexteLibreForm} from "@shared-global/core/models/api/clausier.api";

export class ClauseTexteLibreFormModel implements ClauseTexteLibreForm {
  champLibreObligatoire: string;
  clauseSelectionnee: string;
  nbCaract: string;
  obligatoire: boolean;
  precochee: string;
  sautTextFixeApres: string;
  sautTextFixeAvant: string;
  tailleChamps: number;
  textFixeApres: string;
  textFixeAvant: string;


}
