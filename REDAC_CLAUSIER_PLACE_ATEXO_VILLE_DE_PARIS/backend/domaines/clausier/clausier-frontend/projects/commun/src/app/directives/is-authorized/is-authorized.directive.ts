import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {AuthenticationService} from "@shared-global/core/services/authentification.service";

@Directive({
  selector: '[authorizedRoles]'
})
export class IsAuthorizedDirective implements OnInit {
  @Input('authorizedRoles') authorizedRoles: Array<string>;

  constructor(private el: ElementRef, private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.el.nativeElement.hidden = !this.authenticationService.isAuthorized(this.authorizedRoles);
  }

}
