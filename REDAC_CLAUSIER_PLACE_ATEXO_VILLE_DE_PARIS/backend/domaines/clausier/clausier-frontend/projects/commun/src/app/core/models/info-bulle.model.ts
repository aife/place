import {InfoBulle} from "@shared-global/core/models/api/clausier.api";

export class InfoBulleModel implements InfoBulle {
  actif: boolean;
  description: string;
  descriptionLien: string;
  empty: boolean;
  lien: string;

  constructor() {
    this.actif = false;
    this.empty = true;
  }

}
