import {ClauseSearch} from "@shared-global/core/models/api/clausier.api";

export class ClauseFilterModel implements ClauseSearch {
  page: number;
  size: number;
  actif: boolean;
  dateModificationMax: Date;
  dateModificationMin: Date;
  editeur: boolean;
  idNaturePrestation: number;
  idProcedure: number;
  idStatutRedactionClausier: number;
  idThemeClause: number;
  idTypeContrat: number;
  idTypeDocument: number;
  motsCles: string;
  referenceCanevas: string;
  referenceClause: string;
  typeAuteur: number;
  idTypeClause: number;
  asc: boolean;
  sortField: string;
  parametrableDirection: boolean;
  parametrableAgent: boolean;
  public constructor(editeur: boolean) {
    this.idTypeDocument = 1;
    this.idTypeClause = 0;
    this.idNaturePrestation = 0;
    this.idStatutRedactionClausier = 0;
    this.idThemeClause = 1;
    this.editeur = editeur;
    this.typeAuteur = 0;
    this.actif = null;
    this.referenceCanevas = null;
    this.referenceClause = null;
    this.idTypeContrat = 0;
    this.idProcedure = 0;
    this.dateModificationMin = null;
    this.dateModificationMax = null;
  }





}
