import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertComponent} from './alert/alert.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgSelectModule} from '@ng-select/ng-select';
import {CalloutsComponent} from './callouts/callouts.component';
import {PipesModule} from '../pipes/pipes.module';
import {DirectivesModule} from '../directives/directives.module';
import {TranslateModule, TranslatePipe} from "@ngx-translate/core";
import {
  NgbCarouselConfig,
  NgbDatepickerModule,
  NgbModalConfig,
  NgbModule,
  NgbPaginationModule,
  NgbTabsetModule,
  NgbTimepickerModule
} from "@ng-bootstrap/ng-bootstrap";
import {AtexoToastComponent} from './atexo-toast/atexo-toast.component';
import {RouterModule} from "@angular/router";
import {BreadcrumbComponent} from "@shared-global/components/breadcrumb/breadcrumb.component";
import {TimepickerModule} from "ngx-bootstrap/timepicker";
import {SessionExpiredComponent} from "@shared-global/components/session-expired/session-expired.component";
import {ChapitreTreeviewComponent} from "@shared-global/components/chapitre-treeview/chapitre-treeview.component";
import {ClausierStoreModule} from "@shared-global/store/clausier-store.module";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {BsModalService} from 'ngx-bootstrap/modal';
import {AccueilComponent} from "@shared-global/components/accueil/accueil.component";
import {BrowserModule} from "@angular/platform-browser";
import {PaginationComponent} from "@shared-global/components/pagination/pagination.component";
import {
  ModalNouveauChapitreComponent
} from "@shared-global/components/modal-nouveau-chapitre/modal-nouveau-chapitre.component";
import {ClrCommonFormsModule} from "@clr/angular";
import {FormInfoBulleComponent} from "@shared-global/components/form-info-bulle/form-info-bulle.component";
import {UiSwitchModule} from "ngx-ui-switch";
import {ModalInfoBulleComponent} from "@shared-global/components/modal-info-bulle/modal-info-bulle.component";
import {ModalSelectClauseComponent} from "@shared-global/components/modal-select-clause/modal-select-clause.component";
import {NgxLoadingModule} from "ngx-loading";

import {LoginComponent} from "@shared-global/components/login/login.component";
import {ModalDerogationComponent} from "@shared-global/components/modal-derogation/modal-derogation.component";
import {
  SelectClausesFilterComponent
} from "@shared-global/components/select-clauses-filter/select-clauses-filter.component";
import {SelectClausesComponent} from "@shared-global/components/select-clauses/select-clauses.component";
import {FormTexteFixeComponent} from "@shared-global/components/form-texte-fixe/form-texte-fixe.component";
import {FormChoixExclusifComponent} from "@shared-global/components/form-choix-exclusif/form-choix-exclusif.component";
import {FormChoixMultipleComponent} from "@shared-global/components/form-choix-multiple/form-choix-multiple.component";
import {FormChampLibreComponent} from "@shared-global/components/form-champ-libre/form-champ-libre.component";
import {FormValeurHeriteeComponent} from "@shared-global/components/form-valeur-heritee/form-valeur-heritee.component";
import {
  FormChampPrevaloriseComponent
} from "@shared-global/components/form-champ-prevalorise/form-champ-prevalorise.component";
import {QuillModule} from "ngx-quill";
import {ModalConfirmationComponent} from "@shared-global/components/modal-confirmation/modal-confirmation.component";
import {ModalAccess403Component} from "@shared-global/components/modal-access-403/modal-access-403.component";
import {
  ModalPreviewClauseComponent
} from "@shared-global/components/modal-preview-clause/modal-preview-clause.component";
import {
  PreviewChampPrevaloriseComponent
} from "@shared-global/components/preview-champ-prevalorise/preview-champ-prevalorise.component";
import {PreviewChampLibreComponent} from "@shared-global/components/preview-champ-libre/preview-champ-libre.component";
import {
  PreviewValeurHeriteeComponent
} from "@shared-global/components/preview-valeur-heritee/preview-valeur-heritee.component";
import {PreviewTexteFixeComponent} from "@shared-global/components/preview-texte-fixe/preview-texte-fixe.component";
import {
  PreviewChoixMultipleComponent
} from "@shared-global/components/preview-choix-multiple/preview-choix-multiple.component";
import {
  PreviewChoixExclusifComponent
} from "@shared-global/components/preview-choix-exclusif/preview-choix-exclusif.component";
import {FormClauseContenuComponent} from "@shared-global/components/form-clause-contenu/form-clause-contenu.component";
import {
  ModalPreviewCanevasComponent
} from "@shared-global/components/modal-preview-canevas/modal-preview-canevas.component";
import {
  PreviewChapitreTreeviewComponent
} from "@shared-global/components/preview-chapitre-treeview/preview-chapitre-treeview.component";
import {PreviewClauseComponent} from "@shared-global/components/preview-clause/preview-clause.component";
import {
  StatistiqueRechercheComponent
} from "@shared-global/components/statistique-recherche/statistique-recherche.component";
import {ClausesDetailsComponent} from "@shared-global/components/clauses-details/clauses-details.component";
import {
  ModalModificationPublicationComponent
} from "@shared-global/components/modal-modification-publication/modal-modification-publication.component";
import {ModalImportComponent} from "@shared-global/components/modal-import/modal-import.component";
import {
  ModalHistoriqueClauseComponent
} from "@shared-global/components/modal-historique-clause/modal-historique-clause.component";
import {
  HistoriquesVersionClauseComponent
} from "@shared-global/components/historiques-version-clause/historiques-version-clause.component";
import {
  ModalAjoutDocumentAdministrableComponent
} from "@shared-global/components/modal-ajout-document-administrable/modal-ajout-document-administrable.component";
import {NgxDropzoneModule} from "ngx-dropzone";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PipesModule,
    DirectivesModule,
    TranslateModule,
    NgbPaginationModule,
    ReactiveFormsModule,
    NgSelectModule,
    BrowserModule,
    BsDatepickerModule.forRoot(),
    NgbDatepickerModule,
    NgbTimepickerModule,
    RouterModule,
    TimepickerModule.forRoot(),
    CommonModule,
    TranslateModule,
    ClausierStoreModule,
    NgbTabsetModule,
    PerfectScrollbarModule,
    NgbModule,
    ClrCommonFormsModule,
    UiSwitchModule,
    NgxLoadingModule,
    QuillModule,
    NgxDropzoneModule,

  ],
  providers: [NgbCarouselConfig,
    NgbModalConfig,
    TranslatePipe,
    BsModalService,],
  declarations:
    [ClausesDetailsComponent, StatistiqueRechercheComponent, PreviewClauseComponent, PreviewChapitreTreeviewComponent, ModalPreviewCanevasComponent, FormClauseContenuComponent, ModalPreviewClauseComponent, ModalAccess403Component, ModalConfirmationComponent, SelectClausesFilterComponent,
      SelectClausesComponent,
      ModalDerogationComponent,
      ModalImportComponent,
      ModalAjoutDocumentAdministrableComponent,
      ModalModificationPublicationComponent,
      HistoriquesVersionClauseComponent,
      LoginComponent,
      ModalInfoBulleComponent,
      ModalSelectClauseComponent,
      ModalHistoriqueClauseComponent,
      FormInfoBulleComponent,
      ChapitreTreeviewComponent,
      PaginationComponent,
      AccueilComponent,
      ChapitreTreeviewComponent,
      ModalNouveauChapitreComponent,
      SessionExpiredComponent,
      BreadcrumbComponent,
      AlertComponent,
      CalloutsComponent,
      AtexoToastComponent,
      FormChoixExclusifComponent,
      FormChoixMultipleComponent,
      FormValeurHeriteeComponent,
      FormChampPrevaloriseComponent,
      FormChampLibreComponent,
      FormTexteFixeComponent,
      PreviewChoixExclusifComponent,
      PreviewChoixMultipleComponent,
      PreviewTexteFixeComponent,
      PreviewValeurHeriteeComponent,
      PreviewChampLibreComponent,
      PreviewChampPrevaloriseComponent],
  exports:
    [StatistiqueRechercheComponent, PreviewClauseComponent, PreviewChapitreTreeviewComponent, ModalPreviewCanevasComponent, FormClauseContenuComponent, ModalPreviewClauseComponent, ModalAccess403Component, ModalConfirmationComponent, LoginComponent,
      ChapitreTreeviewComponent,
      PaginationComponent,
      AccueilComponent,
      ChapitreTreeviewComponent,
      ModalModificationPublicationComponent,
      ModalAjoutDocumentAdministrableComponent,
      ModalNouveauChapitreComponent,
      ModalImportComponent,
      ModalHistoriqueClauseComponent,
      HistoriquesVersionClauseComponent,
      SessionExpiredComponent,
      BreadcrumbComponent,
      AlertComponent,
      CalloutsComponent,
      AtexoToastComponent,
      FormChoixExclusifComponent,
      FormChoixMultipleComponent,
      FormTexteFixeComponent,
      FormValeurHeriteeComponent,
      FormChampLibreComponent,
      FormChampPrevaloriseComponent,
      PreviewChoixExclusifComponent,
      PreviewChoixMultipleComponent,
      PreviewTexteFixeComponent,
      PreviewValeurHeriteeComponent,
      PreviewChampLibreComponent,
      PreviewChampPrevaloriseComponent,

      FormInfoBulleComponent, ClausesDetailsComponent]

})
export class ComponentsModule {
}
