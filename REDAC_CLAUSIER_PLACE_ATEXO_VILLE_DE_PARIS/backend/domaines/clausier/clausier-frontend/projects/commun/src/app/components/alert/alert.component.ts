import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Store} from "@ngrx/store";
import {NavigationStart, Router} from "@angular/router";
import {State} from "@shared-global/store";
import {clearAlert} from "@shared-global/store/alert/alert.action";


@Component({
  selector: 'atx-alert',
  templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit, OnDestroy {
  private state: Subscription;
  alert: { messages: Array<string>; type: string; keepAfterNavigationChange: boolean, icon: string };

  constructor(private store: Store<State>, private router: Router) {
    // clear alert message on route change
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.alert?.keepAfterNavigationChange) {
          // only keep for a single location change
          this.alert.keepAfterNavigationChange = false;
        } else {
          // clear alert
          this.alert = null;
        }
      }
    });
  }

  ngOnInit() {
    this.state = this.store.select(state => state.alertReducer).subscribe(alert => {
      this.alert = alert.alert;
    });
  }

  ngOnDestroy() {
    this.state.unsubscribe();
  }

  close() {
    this.store.dispatch(clearAlert())
  }
}
