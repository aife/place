import {createAction, props} from '@ngrx/store';

export const showSuccessToast = createAction('[Toast] Show Success Toast', props<{ header: string, message: string }>());
export const showErrorToast = createAction('[Toast] Show Error Toast', props<{ header: string, message: string }>());
