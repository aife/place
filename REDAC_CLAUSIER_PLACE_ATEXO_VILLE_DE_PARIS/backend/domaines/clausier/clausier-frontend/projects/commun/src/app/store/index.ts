import {ActionReducerMap} from '@ngrx/store';
import * as fromUserReducer from '@shared-global/store/user/user.reducer';
import * as fromAlertReducer from '@shared-global/store/alert/alert.reducer';
import * as fromReferentielReducer from '@shared-global/store/referentiels/referentiels.reducer';

export interface State {
  referentielReducer: fromReferentielReducer.ReferentielState;
  userReducer: fromUserReducer.UserState;
  alertReducer: fromAlertReducer.AlertState;
}

export const reducers: ActionReducerMap<State> = {
  referentielReducer: fromReferentielReducer.referentielsReducer,
  userReducer: fromUserReducer.userReducer,
  alertReducer: fromAlertReducer.alertReducer
};
