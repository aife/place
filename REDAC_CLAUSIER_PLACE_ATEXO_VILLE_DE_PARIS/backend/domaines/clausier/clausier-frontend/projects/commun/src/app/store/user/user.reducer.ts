import {createReducer, on} from "@ngrx/store";
import {
  errorGetUser,
  getUser,
  setDefaultPage,
  setDefaultParams,
  setIsWebComponent,
  successGetUser,
  successLogout
} from "./user.action";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {Agent} from "@shared-global/core/models/api/clausier-core.api";

export interface UserState {
  connecting: boolean,
  connected: boolean,
  user: Agent,
  defaultPage: string,
  queryParams: any,
  token: string,
  refeshToken: string,
  isWebComponent: boolean,
  error: string
}


function getInitialState(): UserState {

  let config = {
    user: null,
    connecting: false,
    connected: false,
    token: null,
    refeshToken: null,
    defaultPage: AppInternalPathEnum.PRIVATE_PATH + AppInternalPathEnum.ACCUEIL,
    queryParams: {},
    isWebComponent: JSON.parse(sessionStorage.getItem("isWebComponent")),
    error: null
  };
  console.log('getInitialState', config)
  return config;
}


const initialState: UserState = getInitialState();

const _userReducer = createReducer(
  initialState,
  on(setDefaultParams, (state, props) => {
    if (props.isWebComponent) {
      sessionStorage.setItem("isWebComponent", props.isWebComponent + '');
    }
    return {
      ...state,
      ...props,
    }
  }), on(setDefaultPage, (state, props) => {
    return {
      ...state,
      defaultPage: props.defaultPage,
      queryParams: props.queryParams
    }
  }), on(setIsWebComponent, (state, props) => {
    sessionStorage.setItem("isWebComponent", props.isWebComponent + '');
    return {
      ...state,
      isWebComponent: props.isWebComponent,
    }
  }),
  on(getUser, (state) => {
    return {
      ...state,
      connecting: true,
      error: null
    }
  }),
  on(successGetUser, (state, props) => {
    return {
      ...state,
      connecting: false,
      connected: true,
      user: props.user,
      error: null
    }
  }),
  on(errorGetUser, (state, props) => {
    return {
      ...state,
      connecting: false,
      connected: false,
      user: null,
      error: props.error
    }
  }),
  on(successLogout, (state, props) => {
    return initialState;

  })
);

export function userReducer(state, action) {
  return _userReducer(state, action);
}
