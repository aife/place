/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 2.18.565 on 2024-03-20 17:50:45.

export interface ResultList<B, F, K> extends Iterable<B> {
    listResults: B[];
    count: number;
    countPages: number;
    pageCurrent: number;
    interval: number;
}

export interface Directory extends AbstractBean {
    uid: string;
    label: string;
    actif: boolean;
    shortLabel: string;
    externalCode: string;
}

export interface CanevasBean extends BaseBean {
    idCanevas: number;
    idLastPublication: number;
    idOrganisme: number;
    referenceCanevas: string;
    referenceClause: string;
    titre: string;
    lastVersion: string;
    dateDerniereValidation: Date;
    datePremiereValidation: Date;
    idTypeDocument: number;
    labelTypeDocument: string;
    codeTypeDocument: string;
    idsTypeContrats: number[];
    labelsTypeContrats: string[];
    idTypeContrat: number;
    idsProcedures: number[];
    labelsProcedures: string[];
    idProcedure: number;
    labelNaturePrestation: string;
    dateCreation: Date;
    dateModificationMin: Date;
    dateModificationMax: Date;
    idRefCCAG: number;
    labelTypeAuteur: string;
    canevasEditeur: boolean;
    compatibleEntiteAdjudicatrice: boolean;
    procedures: Directory[];
    typesContrat: Directory[];
    ccag: Directory;
}

export interface ClauseBean extends BaseBean {
    idClause: number;
    idLastPublication: number;
    idOrganisme: number;
    referenceClause: string;
    referenceCanevas: string;
    idTypeClause: number;
    labelTypeClause: string;
    idTypeDocument: number;
    idsTypeContrats: number[];
    idTypeContrat: number;
    idProcedure: number;
    procedure: Directory;
    typesContrat: Directory[];
    idThemeClause: number;
    labelThemeClause: string;
    idsRolesClauses: number[];
    idsClausePotentiellementConditionnees: number[];
    motsCles: string;
    infoBulleText: string;
    infoBulleUrl: string;
    context: string;
    contextHtml: string;
    dateCreation: Date;
    dateModificationMin: Date;
    dateModificationMax: Date;
    lastVersion: string;
    clauseEditeur: boolean;
    parametreActif: boolean;
    compatibleEntiteAdjudicatrice: boolean;
    dateDerniereValidation: Date;
    datePremiereValidation: Date;
    clauseListeChoixExclusif: ClauseListeChoixExclusifForm;
    clauseListeChoixCumulatif: ClauseListeChoixCumulatifForm;
    clauseValeurHeritee: ClauseValeurHeriteeForm;
    clauseTexteFixe: ClauseTexteFixeForm;
    clauseTexteLibre: ClauseTexteLibreForm;
    clauseTextePrevalorise: ClauseTextePrevaloriseForm;
    infoBulle: InfoBulle;
    potentiellementConditionnees: PotentiellementConditionnee[];
    labelTypeAuteur: string;
}

export interface PublicationClausierBean extends AbstractBean {
    version: string;
    idUtilisateur: number;
    nomCompletUtilisateur: string;
    idOrganisme: number;
    commentaire: string;
    datePublication: Date;
    idReferentielFile: number;
    idClausierFile: number;
    editeur: string;
    dateIntegration: Date;
    dateActivation: Date;
    gestionLocal: boolean;
    datePublicationMin: Date;
    datePublicationMax: Date;
    gestionLocalRecherche: number;
    version1: string;
    version2: string;
    version3: string;
    actif: boolean;
    enCoursActivation: boolean;
}

export interface CanevasSearch extends CommunSearch {
    titreCanevas: string;
    idCCAG: number;
}

export interface ClauseSearch extends CommunSearch {
    motsCles: string;
    idThemeClause: number;
    idTypeClause: number;
    parametrableDirection: boolean;
    parametrableAgent: boolean;
}

export interface PublicationClausierSearch {
    page: number;
    size: number;
    sortField: string;
    asc: boolean;
    version: string;
    gestionLocale: number;
    datePublicationMin: Date;
    datePubicationMax: Date;
}

export interface PageRepresentation<T> {
    first: boolean;
    last: boolean;
    totalPages: number;
    totalElements: number;
    numberOfElements: number;
    size: number;
    number: number;
    content: T[];
}

export interface Canevas extends AbstractCanevasChapitre {
    idCanevas: number;
    idPublication: number;
    lastVersion: string;
    referenceCanevas: string;
    titre: string;
    procedurePassationList: number[];
    typeContratList: number[];
    typeDocument: number;
    naturePrestation: number;
    statut: boolean;
    auteur: string;
    message: string;
    compatibleEntiteAdjudicatrice: boolean;
    idOrganisme: number;
    canevasEditeur: boolean;
    editeur: boolean;
    idStatutRedactionClausier: number;
    creeLe: string;
    modifieLe: string;
    referenceInitialDuplication: string;
    idRefCCAG: number;
    derogationActive: boolean;
    derogationDevenirInactif: boolean;
    idTypePassation: number;
}

export interface PotentiellementConditionnee extends Comparable<PotentiellementConditionnee>, Serializable {
    libelle: string;
    id: number;
    valeurs: any[];
    valeurId: number;
    multiChoix: boolean;
}

export interface PotentiellementConditionneeValeur extends Comparable<any> {
    libelle: string;
    id: number;
}

export interface Constantes extends ConstantesGlobales {
}

export interface Document extends AbstractChapitreDocument {
    intituleConsultation: string;
    numConsultation: string;
    pouvoirAdjudicateur: string;
    directionServic: string;
    procedurePassation: string;
    naturePrestation: string;
    repIntegralement: boolean;
    autoriseSupprimerArticles: boolean;
    docType: string;
    lot: string;
    idLot: number;
    titre: string;
    nomFichier: string;
    reference: string;
    id: number;
    chapitres: ChapitreDocument[];
    idCanevas: number;
    idPublication: number;
    auteur: string;
    idUtilisateur: number;
    version: number;
    actif: string;
    dateCreation: Date;
    dateModification: Date;
    idConsultation: EpmTConsultation;
    extension: string;
    refCanevas: string;
    canevasModifie: boolean;
    messageModification: string;
    typeDocument: number;
    externe: boolean;
    profilRedacteur: boolean;
    profilValidateur: boolean;
    identifiant: string;
    url: string;
    commentaire: string;
    idActionDocument: number;
    sommaire: boolean;
    canevasActif: boolean;
    idDocumentInitial: number;
    premierAccesInitialisationDocument: boolean;
    derogationActive: boolean;
    choixExtensionAutorise: boolean;
    template: string;
    pageDeGarde: boolean;
    dateLimiteRemise: Date;
    idOrganisme: number;
    editeur: boolean;
    statut: string;
}

export interface ClauseListe extends ClauseDocument {
    formulations: Formulation[];
    formulationsModifiable: boolean;
}

export interface ClauseTexte extends ClauseDocument {
    texteVariable: string;
    texteObligatoire: boolean;
    tailleTexteVariable: string;
    dateModification: Date;
    idRefValeurTableau: number;
    invisibleCoordMap: { [index: string]: boolean };
    desactiverStyleTitre: boolean;
}

export interface AbstractBean extends Bean, Serializable {
}

export interface BaseBean extends AbstractBean {
    surchargeActif: boolean;
    typeAuteur: number;
    idPublication: number;
    referenceClauseSurchargee: string;
    idClauseOrigine: number;
    actif: boolean;
    checked: boolean;
    etat: number;
    dateModification: Date;
    idStatutRedactionClausier: number;
    idNaturePrestation: number;
}

export interface ClauseListeChoixExclusifForm extends AbstractClauseListeChoixForm {
    formulaires: ClauseFormulaire[];
    precochee: string;
    clauseSelectionnee: string;
}

export interface ClauseListeChoixCumulatifForm extends AbstractClauseListeChoixForm {
    precochee: string[];
    clauseSelectionnee: string;
    formulaires: ClauseFormulaire[];
}

export interface ClauseValeurHeriteeForm {
    textFixeAvant: string;
    refValeurTypeClause: string;
    idRefValeurTypeClause: number;
    textFixeApres: string;
    texteVariable: string;
    tableauRedaction: TableauRedaction;
    sautTextFixeAvant: string;
    sautTextFixeApres: string;
    listRefValeursTypeClause: EpmTRefValeurTypeClause[];
    clauseSelectionnee: string;
}

export interface ClauseTexteFixeForm {
    texteFixe: string;
    clauseSelectionnee: string;
}

export interface ClauseTexteLibreForm {
    textFixeAvant: string;
    nbCaract: string;
    precochee: string;
    textFixeApres: string;
    tailleChamps: number;
    champLibreObligatoire: string;
    obligatoire: boolean;
    sautTextFixeAvant: string;
    sautTextFixeApres: string;
    clauseSelectionnee: string;
}

export interface ClauseTextePrevaloriseForm {
    tailleChamps: number;
    textFixeAvant: string;
    nbCaract: string;
    precochee: boolean;
    textFixeApres: string;
    defaultValue: string;
    valeurDefautMoyen: string;
    valeurDefautCourt: string;
    valeurDefautTresLong: string;
    parametrableDirection: string;
    parametrableAgent: string;
    sautTextFixeAvant: string;
    sautTextFixeApres: string;
    clauseSelectionnee: string;
}

export interface InfoBulle extends Clausier {
    description: string;
    lien: string;
    descriptionLien: string;
    actif: boolean;
    empty: boolean;
}

export interface CommunSearch {
    page: number;
    size: number;
    sortField: string;
    asc: boolean;
    actif: boolean;
    editeur: boolean;
    idTypeContrat: number;
    idProcedure: number;
    idNaturePrestation: number;
    typeAuteur: number;
    idStatutRedactionClausier: number;
    idTypeDocument: number;
    referenceCanevas: string;
    referenceClause: string;
    dateModificationMin: Date;
    dateModificationMax: Date;
}

export interface Chapitre extends AbstractCanevasChapitre {
    idChapitre: number;
    idPublication: number;
    titre: string;
    infoBulle: InfoBulle;
    derogation: Derogation;
    afficherMessageDerogation: boolean;
    styleChapitre: string;
    clauses: Clause[];
}

export interface AbstractCanevasChapitre extends CanevasChapitre {
    numero: string;
}

export interface Serializable {
}

export interface Comparable<T> {
}

export interface ConstantesGlobales {
}

export interface ChapitreDocument extends AbstractChapitreDocument, Cloneable {
    id: number;
    ref: string;
    valide: boolean;
    titre: string;
    infoBulle: InfoBulle;
    styleChapitre: string;
    clausesDocument: ClauseDocument[];
    paragrapheDocument: ParagrapheDocument;
    sousChapitres: ChapitreDocument[];
    numero: string;
    numeroProvisoire: number;
    nombreSautLignes: string;
    duplique: boolean;
    supprime: boolean;
    derogation: Derogation;
    afficherNBMessageDerogation: boolean;
    reprisIntegralementAvantDuplication: boolean;
    terminer: boolean;
    numeroProvisoireAsString: string;
    niveau: number;
    grey: boolean;
    orange: boolean;
}

export interface EpmTConsultation extends AbstractConsultation, Comparable<EpmTConsultation> {
    dateRemisePlis: Calendar;
    dateModification: Calendar;
    calendriers: EpmTCalendrier[];
    epmTValeurConditionnementExterneComplexeList: EpmTValeurConditionnementExterneComplexe[];
    epmTValeurConditionnementExterneList: EpmTValeurConditionnementExterne[];
    epmTDsp: EpmTDsp;
    operationTravaux: string;
    autreOperation: string;
    epmTRefTypeContrat: EpmTRefTypeContrat;
    epmTRefArticle: EpmTRefArticle;
    epmTBudLots: EpmTBudLot[];
    rapportEstimationIdentique: boolean;
    epmTBudLotOuConsultation: EpmTBudLotOuConsultation;
    clausesSociales: string;
    clausesEnvironnementales: string;
    lotsReserves: EpmTRefReservationLot[];
    epmTRefGroupementAttributaire: EpmTRefGroupementAttributaire;
    epmTRefChoixMoisJour: EpmTRefChoixMoisJour;
    epmTRefDureeDelaiDescription: EpmTRefDureeDelaiDescription;
    dureeMarche: number;
    dateExecutionPrestationsDebut: Calendar;
    dateExecutionPrestationsFin: Calendar;
    descriptionDuree: string;
    epmTRefNbCandidatsAdmis: EpmTRefNbCandidatsAdmis;
    nombreCandidatsFixe: number;
    nombreCandidatsMin: number;
    nombreCandidatsMax: number;
    enPhasesSuccessives: string;
    delaiValiditeOffres: number;
    dceEnLigne: string;
    signature: number;
    chiffrement: boolean;
    enveloppeUniqueReponse: string;
    codeRestreint: string;
    codeRestreintPhaseFinale: string;
    etapes: EpmTEtapeSpec[];
    delamNumeroProj: string;
    delamEntreeProjAlp: Date;
    delamVisaDf: Date;
    delamVoteCa: Date;
    delamVoteCp: Date;
    delamContLeg: Date;
    dateEnvoiDCEDiffere: Date;
    dateValeurPreinscription: Calendar;
    delamCommentaireCa: string;
    delavdirNumeroProj: string;
    delavdirEntreeProjAlp: Date;
    delavdirVisaDf: Date;
    delavdirVoteCa: Date;
    delavdirVoteCp: Date;
    delavdirContLeg: Date;
    delavdirCommentaireCa: string;
    epmTRefStatut: EpmTRefStatut;
    statutAncien: number;
    cpvs: EpmTCpv[];
    epmTRefCodeAchat: EpmTRefCodeAchat;
    epmTOperationUniteFonctionnelle: EpmTOperationUniteFonctionnelle;
    dateDerniereSynchronisationDematQuestionReponse: Date;
    dateDerniereSynchronisationDematRetrait: Date;
    dateDerniereSynchronisationDemateDepot: Date;
    aapcPresent: boolean;
    avisRectif1Present: boolean;
    avisRectif2Present: boolean;
    avisRectif3Present: boolean;
    structure: string;
    idConsultationLiee: number;
    phaseCourante: string;
    sequenceDepot: number;
    ajustementDCE1Date: boolean;
    ajustementDCE1Mail: boolean;
    ajustementDCE1AvisRectificatif: string;
    ajustementDCE2Date: boolean;
    ajustementDCE2Mail: boolean;
    ajustementDCE2AvisRectificatif: string;
    ajustementDCE3Date: boolean;
    ajustementDCE3Mail: boolean;
    ajustementDCE3AvisRectificatif: string;
    validationEtape: number;
    maximumValidationEtape: number;
    attributionTousLots: string;
    idConsultationInitiale: number;
    idLotAccordCadreInitial: number;
    referentielsExterne: EpmTReferentielExterne[];
    signatureEngagement: boolean;
    numeroConsultationExterne: string;
    compatibleEntiteAdjudicatrice: boolean;
    idContratInitial: number;
    sousEspaceCollaboratifUrl: string;
    refReponseElectronique: EpmTRefReponseElectronique;
    critereAppliqueTousLots: boolean;
    urlExterne: string;
    mailResolutionEnvoye: boolean;
    mailErreurEnvoye: boolean;
    listeCritereAttribution: EpmTCritereAttributionConsultation[];
    critereAttribution: EpmTRefCritereAttribution;
    listeCritereAttributionTriee: EpmTCritereAttributionConsultation[];
}

export interface AbstractChapitreDocument extends Serializable {
}

export interface TableauRedaction extends Serializable {
    documentXLS: DocumentXLS[];
    textAvantTableau: string;
}

export interface Formulation extends Serializable {
    libelle: string;
    numFormulation: number;
    precochee: boolean;
    nbrCaraMax: number;
}

export interface ClauseDocument extends Serializable, IClauseDocument {
    id: number;
    ref: string;
    modifier: boolean;
    infoBulle: InfoBulle;
    type: number;
    texteFixeAvant: string;
    texteFixeApresAlaLigne: boolean;
    texteFixeAvantAlaLigne: boolean;
    texteFixeApres: string;
    terminer: boolean;
    texteModifiable: boolean;
    conditionee: boolean;
    texteConditions: string;
    valeurConditionnementList: string[][];
    typeConditionnementList: string[];
    etat: string;
    actif: string;
    tableauRedaction: TableauRedaction;
    duplique: boolean;
}

export interface Iterable<T> {
}

export interface Bean {
    id: number;
}

export interface EpmTRoleClauseAbstract extends EpmTAbstractObject, Comparable<EpmTRoleClauseAbstract>, Cloneable, Serializable {
    idDirectionService: number;
    idUtilisateur: number;
    valeurDefaut: string;
    precochee: boolean;
    etat: string;
    preference: EpmTPreference;
    nombreCarateresMax: number;
    champObligatoire: string;
    valeurHeriteeModifiable: string;
    numFormulation: number;
    idRoleClause: number;
    epmTClause: EpmTClauseAbstract;
    idPublication: number;
}

export interface ClauseFormulaire {
    id: number;
    precochee: boolean;
    defaultValeur: string;
    tailleChamps: string;
    numFormulation: number;
}

export interface AbstractClauseListeChoixForm {
    textFixeAvant: string;
    textFixeApres: string;
    sautTextFixeAvant: string;
    sautTextFixeApres: string;
    formulationModifiable: string;
    modifiable: boolean;
    parametrableDirection: string;
    parametrableAgent: string;
    numFormulation: string[];
    tailleChamp: string[];
    valeurDefaut: string[];
    valeurDefautMoyen: string[];
    valeurDefautCourt: string[];
    valeurDefautTresLong: string[];
    formulationCollection: EpmTRoleClauseAbstract[];
}

export interface EpmTRefValeurTypeClause extends BaseEpmTRefRedaction {
    epmTRefTypeClause: EpmTRefTypeClause;
    simple: boolean;
    expression: string;
}

export interface Clausier extends Serializable, Cloneable {
}

export interface Derogation extends Serializable {
    articleDerogationCCAG: string;
    commentairesDerogationCCAG: string;
    numArticle: string;
    active: boolean;
    empty: boolean;
}

export interface Clause extends Clausier {
    idClause: number;
    idPublication: number;
    lastVersion: string;
    reference: string;
    contenu: string;
    texteFixeApres: string;
    texteFixeApresAlaLigne: boolean;
    texteFixeAvantAlaLigne: boolean;
    texteFixeAvant: string;
    dateModification: string;
    dateCreation: string;
    theme: string;
    infoBulle: InfoBulle;
    parent: Chapitre;
    actif: boolean;
    etat: string;
    potentiellementConditionnee: boolean;
    potentiellementApplicable: boolean;
    idStatutRedactionClausier: number;
    clauseEditeur: boolean;
    nombreSurcharges: number;
    auteur: string;
    idsTypeContrats: number[];
    idProcedure: number;
}

export interface CanevasChapitre extends Clausier {
    chapitres: Chapitre[];
    parent: CanevasChapitre;
}

export interface ParagrapheDocument extends Serializable {
    id: number;
    texte: string;
}

export interface Cloneable {
}

export interface EpmTRefPouvoirAdjudicateur extends BaseEpmTRefReferentiel {
    commission: boolean;
    codeGo: string;
    organismeMPE: string;
    codeContrat: string;
    groupement: boolean;
    membre: boolean;
    acronymeOrganisme: string;
    logoType: string;
    logo: any;
}

export interface EpmTRefProcedure extends BaseEpmTRefReferentiel, EpmTRefImportExport {
    attributFormalise: string;
    articlesAssocies: any[];
    epmTRefTypeMarche: EpmTRefTypeMarche;
    workflow: string;
    deuxPhases: boolean;
    negociable: boolean;
    enveloppeUnique: boolean;
    valeurDefautDceEnLigne: boolean;
    valeurDefautChiffrement: boolean;
    valeurDefautSignature: boolean;
    valeurDefautEnveloppeUnique: boolean;
    valeurDefautPhaseSuccessive: boolean;
    modifiableDceEnLigne: boolean;
    modifiableReponseElectronique: boolean;
    modifiableChiffrement: boolean;
    modifiableSignature: boolean;
    modifiableEnveloppeUnique: boolean;
    modifiablePhaseSuccessive: boolean;
    modifiableNbCandidat: boolean;
    typeProcedure: string;
    procedureBOAMP: string;
    ficheRecensement: string;
    documentDeliberation: number;
    documentLibelle: string;
    valeurDefautSignatureEngagement: boolean;
    modifiableSignatureEngagement: boolean;
    valeurDefautMPS: boolean;
    modifiableMPS: boolean;
    procedurePersonnalise: boolean;
    refReponseElectronique: EpmTRefReponseElectronique;
    epmTRefParametrageSet: EpmTRefParametrage[];
}

export interface EpmTRefNature extends BaseEpmTRefReferentiel, EpmTRefImportExport {
    codeGo: string;
}

export interface EpmTRefDirectionService extends EpmTReferentielByOrganismeAbstract, EpmTRefImportExport {
    typeEntite: string;
    courriel: string;
    adresse: string;
    codePostal: string;
    ville: string;
    fax: string;
    telephone: string;
    transverse: boolean;
    parent: EpmTRefDirectionService;
    directionAchat: boolean;
    codeOrganisme: string;
}

export interface EpmTUtilisateur extends EpmTReferentielSimpleByOrganismeAbstract {
    guidSso: string;
    nom: string;
    prenom: string;
    courriel: string;
    identifiant: string;
    adresse: string;
    motDePasse: string;
    codePostal: string;
    ville: string;
    telephone: string;
    fax: string;
    codeOrganisme: string;
    directionService: number;
    clef: string;
    nomComplet: string;
    idOrganisme: number;
}

export interface EpmTRefLieuExecution extends EpmTReferentielExterneAbstract {
}

export interface Calendar extends Serializable, Cloneable, Comparable<Calendar> {
}

export interface EpmTCalendrier extends EpmTAbstractObject, Cloneable {
    avenant: number;
    lot: number;
    sensDirect: boolean;
    etapes: EpmTEtapeCal[];
    jalonsLibres: any[];
    valide: boolean;
}

export interface EpmTValeurConditionnementExterneComplexe extends EpmTAbstractObject {
    idObjetComplexe: string;
    epmTValeurConditionnementExternes: EpmTValeurConditionnementExterne[];
}

export interface EpmTValeurConditionnementExterne extends EpmTAbstractObject {
    clef: string;
    valeur: string;
}

export interface EpmTDsp extends EpmTAbstractObject, Cloneable {
    id: number;
    commentairesInternes: string;
    renouvellementExistante: boolean;
    renouvellementIdentique: boolean;
    projetLocalise: boolean;
    equipementProximite: boolean;
    dateValeur: Calendar;
    estimation: number;
    epmTRefAutoriteDelegante: EpmTRefAutoriteDelegante;
    epmTRefTypeDsp: EpmTRefTypeDsp;
    epmTRefTypeConsultation: EpmTRefTypeConsultation;
    arrondissements: any[];
    idOrganisme: number;
}

export interface EpmTRefTypeContrat extends BaseEpmTRefRedaction, EpmTRefImportExport {
    avenantActive: boolean;
    abreviation: string;
    idsProcedures: number[];
}

export interface EpmTRefArticle extends BaseEpmTRefReferentiel {
}

export interface EpmTBudLot extends EpmTAbstractObject, Comparable<EpmTBudLot> {
    id: number;
    epmTRefNature: EpmTRefNature;
    epmTRefStatut: EpmTRefStatut;
    statutAncien: number;
    epmTRefChoixMoisJour: EpmTRefChoixMoisJour;
    epmTBudLotOuConsultation: EpmTBudLotOuConsultation;
    numeroLot: string;
    idConsultationLotDissocie: number;
    validationEtape: number;
    validationEtapeMaximum: number;
    intituleLot: string;
    descriptionSuccinte: string;
    epmTRefDureeDelaiDescription: EpmTRefDureeDelaiDescription;
    dureeMarche: number;
    dateDebut: Calendar;
    dateFin: Calendar;
    descriptionDuree: string;
    cpvs: EpmTCpv[];
    epmTRefCodeAchat: EpmTRefCodeAchat;
    epmTOperationUniteFonctionnelle: EpmTOperationUniteFonctionnelle;
    dateValeurPreinscription: Calendar;
    idConsultation: number;
    listeCritereAttribution: EpmTCritereAttributionConsultation[];
    critereAttribution: EpmTRefCritereAttribution;
    listeCritereAttributionTriee: EpmTCritereAttributionConsultation[];
}

export interface EpmTBudLotOuConsultation extends EpmTAbstractObject {
    dateValeurPreinscription: Calendar;
    epmTBudFormePrix: EpmTBudFormePrix;
    epmTRefCcag: EpmTRefCcag;
    epmTRefTrancheTypePrix: EpmTRefTrancheTypePrix;
    reconductible: string;
    nbReconductions: number;
    modalitesReconduction: string;
    variantesExigees: string;
    variantesAutorisees: string;
    optionsTechniquesImposees: string;
    descriptionOptionsImposees: string;
    lotDissocie: boolean;
    decompositionLotsTechniques: string;
    epmTLotTechniques: EpmTLotTechnique[];
    epmTBudTranches: EpmTBudTranche[];
    numerusClausus: number;
    marchesSubsequents: any[];
    accordCadre: EpmTBudLotOuConsultation;
    estimationPfPreinscription: number;
    estimationBcMinPreinscription: number;
    estimationBcMaxPreinscription: number;
    estimationPf: number;
    estimationBcMin: number;
    estimationBcMax: number;
    estimationPfTabOuvOffre: number;
    estimationDqeTabOuvOffre: number;
    statutOuvertureCandidature: number;
    classementCandidatureEcarte: boolean;
    classementCandidatureEcartePA: boolean;
    classementRecommandationCandidature: boolean;
    classementRecommandationCandidaturePA: boolean;
    lieuExecution: string;
    estimationDqe: number;
    estimationCtTabOuvOffre: number;
    estimationDqePresinscription: number;
    estimationCtPresinscription: number;
    avisAttributionPresent: boolean;
    dateEnvoiMarcheGo: Date;
    choixSupportAttributionTousBeneficiaires: boolean;
    beneficiaireMarches: EpmTRefBeneficiaireMarche[];
    unite: EpmTRefUnite;
    clausesEnvironnementales: boolean;
    clausesEnvironnementalesChoixUtilisateur: EpmTRefClausesEnvironnementales[];
    clausesSocialesChoixUtilisateur: EpmTRefClausesSociales[];
    clausesSocialesPresentCriteresAttribution: boolean;
    listeStructureSocialeReserves: EpmTRefTypeStructureSociale[];
    clausesSociales: boolean;
    estimationBcMinTabOuvOffre: number;
    estimationBcMaxTabOuvOffre: number;
    estimationPfValidationClassementOffre: number;
    estimationDqeValidationClassementOffre: number;
    estimationBcMinValidationClassementOffre: number;
    estimationBcMaxValidationClassementOffre: number;
    epmTValeurConditionnementExternes: EpmTValeurConditionnementExterne[];
    epmTValeurConditionnementExternesComplexe: EpmTValeurConditionnementExterneComplexe[];
    sousEspaceCollaboratifUrl: string;
    droitProprieteIntellectuelle: EpmTRefDroitProprieteIntellectuelle;
}

export interface EpmTRefReservationLot extends EpmTReferentielSimpleAbstract {
}

export interface EpmTRefGroupementAttributaire extends EpmTReferentielExterneAbstract {
}

export interface EpmTRefChoixMoisJour extends EpmTReferentielExterneAbstract {
}

export interface EpmTRefDureeDelaiDescription extends EpmTReferentielSimpleAbstract {
}

export interface EpmTRefNbCandidatsAdmis extends EpmTReferentielSimpleAbstract {
}

export interface EpmTEtapeSpec extends EpmTAbstractObject {
    id: number;
    libelle: string;
    idConsultation: number;
    idAvenant: number;
    ordre: number;
}

export interface EpmTRefStatut extends EpmTReferentielSimpleAbstract {
    codeGo: string;
}

export interface EpmTCpv extends EpmTAbstractObject, Comparable<EpmTCpv>, Cloneable {
    id: number;
    codeCpv: string;
    libelleCpv: string;
    principal: boolean;
}

export interface EpmTRefCodeAchat extends BaseEpmTRefReferentiel {
    epmTRefCodeAchatMontantsSet: EpmTRefCodeAchatMontants[];
}

export interface EpmTOperationUniteFonctionnelle extends EpmTAbstractObject {
    id: number;
    epmTRefPouvoirAdjudicateur: EpmTRefPouvoirAdjudicateur;
    epmTRefDirectionServiceBeneficiaire: EpmTRefDirectionService;
    epmTRefDirectionServiceResponsable: EpmTRefDirectionService;
    epmTUtilisateurResponsable: EpmTUtilisateur;
    epmTRefNature: EpmTRefNature;
    type: number;
    code: string;
    libelle: string;
    estimatifInitialHT: number;
    procedureApplicable: string;
    anneeDebut: number;
    anneeFin: number;
    commentaire: string;
    status: boolean;
}

export interface EpmTReferentielExterne extends EpmTAbstractObject {
    id: number;
    libelle: string;
    code: string;
}

export interface EpmTRefReponseElectronique extends EpmTReferentielExterneAbstract {
}

export interface EpmTCritereAttributionConsultation extends EpmTAbstractObject, Comparable<EpmTCritereAttributionConsultation>, Cloneable {
    id: number;
    enonce: string;
    ordre: number;
    ponderation: number;
    sousCritere: EpmTSousCritereAttributionConsultation[];
    listeSousCritereAttributionTriee: EpmTSousCritereAttributionConsultation[];
}

export interface EpmTRefCritereAttribution extends EpmTReferentielExterneAbstract {
}

export interface AbstractConsultation extends EpmTAbstractObject {
    numeroConsultation: string;
    justificationNonAllotissement: string;
    epmTRefPouvoirAdjudicateur: EpmTRefPouvoirAdjudicateur;
    intituleConsultation: string;
    dirServiceVision: number;
    objet: string;
    epmTRefProcedure: EpmTRefProcedure;
    epmTRefNature: EpmTRefNature;
    epmTRefDirectionService: EpmTRefDirectionService;
    transverse: string;
    epmTUtilisateur: EpmTUtilisateur;
    lotDissocie: boolean;
    allotissement: string;
    idOrganisme: number;
    echangeWebserviceDemat: boolean;
    lieuxExecution: EpmTRefLieuExecution[];
    marchePublicSimplifie: boolean;
}

export interface DocumentXLS extends Serializable {
    onglet: Onglet[];
    versionMoteur: number;
}

export interface IClauseDocument {
    clauseVide: boolean;
}

export interface EpmTPreference extends EpmTAbstractObject, Serializable {
    id: number;
    idClause: number;
    revisionClause: number;
    idDirectionService: number;
    idUtilisateur: number;
}

export interface EpmTClauseAbstract extends EpmTAbstractObject, Comparable<EpmTClauseAbstract>, Cloneable, Serializable {
    epmTRefTypeDocument: EpmTRefTypeDocument;
    epmTRefTypeClause: EpmTRefTypeClause;
    epmTRefThemeClause: EpmTRefThemeClause;
    reference: string;
    parametrableDirection: string;
    parametrableAgent: string;
    texteFixeAvant: string;
    texteFixeApres: string;
    dateModification: Date;
    dateCreation: Date;
    datePremiereValidation: Date;
    dateDerniereValidation: Date;
    auteur: string;
    actif: boolean;
    idNaturePrestation: number;
    epmTRefProcedure: EpmTRefProcedure;
    etat: string;
    formulationModifiable: boolean;
    sautLigneTexteAvant: boolean;
    sautLigneTexteApres: boolean;
    epmTRefStatutRedactionClausier: EpmTRefStatutRedactionClausier;
    compatibleEntiteAdjudicatrice: boolean;
    idOrganisme: number;
    clauseEditeur: boolean;
    motsCles: string;
    infoBulleText: string;
    infoBulleUrl: string;
    idClause: number;
    epmTRoleClauses: EpmTRoleClauseAbstract[];
    epmTRoleClausesTrie: EpmTRoleClauseAbstract[];
    epmTClausePotentiellementConditionnees: EpmTClausePotentiellementConditionneeAbstract[];
    epmTRefTypeContrats: EpmTRefTypeContrat[];
    epmTRoleClausesDefault: EpmTRoleClauseAbstract[];
    idPublication: number;
    idLastPublication: number;
}

export interface EpmTAbstractObject extends EpmTObject {
}

export interface EpmTRefTypeClause extends BaseEpmTRefRedaction, EpmTRefImportExport {
}

export interface BaseEpmTRefRedaction extends EpmTRef, Comparable<EpmTRef> {
}

export interface BaseEpmTRefReferentiel extends EpmTRef, Comparable<EpmTRef> {
}

export interface EpmTRefTypeMarche extends EpmTReferentielExterneAbstract {
    simplifie: boolean;
}

export interface EpmTRefParametrage extends EpmTReferentielSimpleAbstract {
    clef: string;
    valeur: string;
}

export interface EpmTRefImportExport extends EpmTRef {
    uid: string;
}

export interface EpmTRefOrganisme extends EpmTReferentielExterneAbstract {
    logoActif: boolean;
    plateformeUuid: string;
}

export interface EpmTReferentielByOrganismeAbstract extends BaseEpmTRefReferentiel, EpmTReferentielByOrganisme {
}

export interface EpmTReferentielSimpleByOrganismeAbstract extends EpmTReferentielByOrganismeAbstract {
}

export interface EpmTReferentielExterneAbstract extends BaseEpmTRefReferentiel {
}

export interface EpmTEtapeCal extends EpmTModeleEtapeCal, Cloneable {
    idCalendrier: number;
    dateInitiale: Date;
    dateReelle: Date;
    datePrevue: Date;
    typeEtape: number;
    regleCalcul: string;
    initial: boolean;
    dateHeritable: boolean;
    dateHerite: Date;
    actif: number;
    selection: boolean;
    delibAmont: boolean;
    delibAval: boolean;
    lancement: boolean;
    notification: boolean;
}

export interface EpmTRefAutoriteDelegante extends EpmTReferentielSimpleAbstract {
}

export interface EpmTRefTypeDsp extends EpmTReferentielSimpleAbstract {
}

export interface EpmTRefTypeConsultation extends EpmTReferentielSimpleAbstract {
}

export interface EpmTBudFormePrix extends EpmTAbstractObject {
    id: number;
}

export interface EpmTRefCcag extends BaseEpmTRefReferentiel, EpmTRefImportExport {
}

export interface EpmTRefTrancheTypePrix extends EpmTReferentielSimpleAbstract {
}

export interface EpmTLotTechnique extends EpmTAbstractObject, Comparable<EpmTLotTechnique>, Cloneable {
    id: number;
    numeroLot: string;
    intituleLot: string;
    principal: string;
    epmTBudTranches: EpmTBudTranche[];
}

export interface EpmTBudTranche extends EpmTAbstractObject, Comparable<EpmTBudTranche>, Cloneable {
    id: number;
    epmTRefNatureTranche: EpmTRefNatureTranche;
    epmTBudFormePrix: EpmTBudFormePrix;
    codeTranche: string;
    intituleTranche: string;
    go: boolean;
    referenceGo: string;
}

export interface EpmTRefBeneficiaireMarche extends EpmTReferentielSimpleAbstract {
    numeroContrat: boolean;
    obligatoire: boolean;
    codeContrat: string;
    epmTRefPouvoirAdjudicateur: EpmTRefPouvoirAdjudicateur;
}

export interface EpmTRefUnite extends EpmTReferentielSimpleAbstract {
    valeurParDefaut: boolean;
}

export interface EpmTRefClausesEnvironnementales extends BaseEpmTRefReferentiel {
}

export interface EpmTRefClausesSociales extends BaseEpmTRefReferentiel {
}

export interface EpmTRefTypeStructureSociale extends EpmTReferentielExterneAbstract {
    epmTRefClauseSociale: EpmTRefClausesSociales;
}

export interface EpmTRefDroitProprieteIntellectuelle extends BaseEpmTRefReferentiel {
}

export interface EpmTReferentielSimpleAbstract extends BaseEpmTRefReferentiel {
}

export interface EpmTRefCodeAchatMontants extends EpmTAbstractObject {
    id: number;
    idCodeAchat: number;
    year: number;
    montantMoyen: number;
    montantReel: number;
    montantRecense: number;
}

export interface EpmTSousCritereAttributionConsultation extends EpmTAbstractObject, Comparable<EpmTSousCritereAttributionConsultation>, Cloneable {
    id: number;
    enonce: string;
    ponderation: number;
    critereAttribution: EpmTCritereAttributionConsultation;
    ordre: number;
}

export interface Onglet extends Serializable {
    nom: string;
    attributs: TableauAttributs;
    cellule: Cellule[];
    ligne: Ligne[];
    regionFusionnee: RegionFusionnee[];
    tableau: Tableau;
    numeroOnglet: number;
}

export interface EpmTRefTypeDocument extends BaseEpmTRefRedaction, EpmTRefImportExport {
    extensionFichier: string;
    template: string;
    sommaire: boolean;
    templateTableauDerogation: string;
    activerDerogation: boolean;
    idTypePassation: number;
}

export interface EpmTRefThemeClause extends BaseEpmTRefRedaction, EpmTRefImportExport {
}

export interface EpmTRefStatutRedactionClausier extends BaseEpmTRefRedaction, EpmTRefImportExport {
}

export interface EpmTClausePotentiellementConditionneeAbstract extends EpmTAbstractObject, Cloneable, Serializable {
    epmTRefPotentiellementConditionnee: EpmTRefPotentiellementConditionnee;
    epmTClause: EpmTClauseAbstract;
    idClausePotentiellementConditionnee: number;
    epmTClauseValeurPotentiellementConditionnees: EpmTClauseValeurPotentiellementConditionneeAbstract[];
    idPublication: number;
}

export interface EpmTObject extends Serializable {
}

export interface EpmTRef extends EpmTObject {
    libelleCourt: string;
    inactif: boolean;
    id: number;
    libelle: string;
    actif: boolean;
    codeExterne: string;
}

export interface EpmTReferentielByOrganisme extends EpmTRef {
    epmTRefOrganisme: EpmTRefOrganisme;
}

export interface EpmTModeleTransitionCal extends EpmTAbstractObject {
    id: number;
    valeurFixeInit: number;
    valeurVariableInit: number;
    valeurFixe: number;
    valeurVariable: number;
    modifiable: boolean;
}

export interface EpmTRefEvenementCalendrier extends EpmTReferentielSimpleAbstract {
    typeEvenement: EpmTRefTypeEvenementCalendrier;
    codeEvenement: string;
    miseAJourAutomatique: boolean;
}

export interface EpmTModeleEtapeCal extends EpmTAbstractObject {
    id: number;
    code: string;
    libelle: string;
    transitionsSortantes: EpmTModeleTransitionCal[];
    transitionsEntrantes: any[];
    evenementAssocie: EpmTRefEvenementCalendrier;
    administrable: boolean;
    reelNonSaisissable: boolean;
}

export interface EpmTRefNatureTranche extends EpmTReferentielSimpleAbstract {
}

export interface TableauAttributs extends Serializable {
    largeurColonne: AttributLargeurColonne[];
}

export interface Cellule extends Serializable {
    coordonne: CelluleCoordonnee;
    coordonnee: CelluleCoordonnee;
    valeur: string;
    valeurDate: number;
    valeurNumerique: string;
    valeurBooleenne: boolean;
    valeurFormule: string;
    valeurUrlImage: string;
    style: CelluleStyle;
    type: EnumerationCelluleType;
    hauteur: number;
    format: string;
    bordureHaut: string;
    bordureBas: string;
    bordureGauche: string;
    bordureDroite: string;
    couleurPolice: number;
    taillePolice: number;
    couleurFond: number;
    gras: string;
    retourLigne: string;
}

export interface Ligne extends Serializable {
    cellule: Cellule[];
    regionFusionnee: RegionFusionnee[];
    numeroLigneInsertion: number;
    duplicationStyleLigne: number;
}

export interface RegionFusionnee extends Serializable {
    ligneDebut: number;
    colonneDebut: number;
    ligneFin: number;
    colonneFin: number;
}

export interface Tableau extends Serializable {
    styleEntete: CelluleStyle;
    styleCorp: CelluleStyle;
    reference: string;
    numeroLigneInsertion: number;
    numeroColonneInsertion: number;
    masquerRepetition: boolean;
}

export interface EpmTRefPotentiellementConditionnee extends BaseEpmTRefRedaction, EpmTRefImportExport {
    methodeLibelle: string;
    methodeId: string;
    methodeConsultation: string;
    critereBooleen: boolean;
    multiChoix: boolean;
    procedurePassation: boolean;
}

export interface EpmTClauseValeurPotentiellementConditionneeAbstract extends EpmTAbstractObject, Cloneable, Serializable {
    valeur: number;
    idClauseValeurPotentiellementConditionnee: number;
    epmTClausePotentiellementConditionnee: EpmTClausePotentiellementConditionneeAbstract;
    idPublication: number;
}

export interface EpmTRefTypeEvenementCalendrier extends EpmTReferentielSimpleAbstract {
    evenementAssocies: EpmTRefEvenementCalendrier[];
}

export interface AttributLargeurColonne extends Serializable {
    colonne: number;
    largeur: number;
}

export interface CelluleCoordonnee extends Serializable {
    ligne: number;
    colonne: number;
}

export interface CelluleStyle extends Serializable {
    font: CelluleStyleFont;
    bordure: CelluleStyleBordure;
    alignement: CelluleStyleAlignement;
    remplissage: CelluleStyleRemplissage;
    retourLigne: boolean;
}

export interface CelluleStyleFont extends Serializable {
    police: string;
    gras: boolean;
    italic: boolean;
    souligne: EnumerationFontSousligne;
    couleur: EnumerationCouleur;
    taille: number;
}

export interface CelluleStyleBordure extends Serializable {
    haut: EnumerationBordure;
    bas: EnumerationBordure;
    gauche: EnumerationBordure;
    droite: EnumerationBordure;
}

export interface CelluleStyleAlignement extends Serializable {
    rotation: number;
    horizontal: EnumerationAlignementHorizontal;
    vertical: EnumerationAlignementVertical;
}

export interface CelluleStyleRemplissage extends Serializable {
    type: EnumerationRemplissage;
    premierPlan: EnumerationCouleur;
    arrierePlan: EnumerationCouleur;
}

export type EnumerationCelluleType = "CELL_TYPE_NUMERIC" | "CELL_TYPE_STRING" | "CELL_TYPE_FORMULA" | "CELL_TYPE_BLANK" | "CELL_TYPE_BOOLEAN" | "CELL_TYPE_ERROR";

export type EnumerationFontSousligne = "U_NONE" | "U_SINGLE" | "U_DOUBLE" | "U_SINGLE_ACCOUNTING" | "U_DOUBLE_ACCOUNTING";

export type EnumerationCouleur = "BLACK" | "BROWN" | "OLIVE_GREEN" | "DARK_GREEN" | "DARK_TEAL" | "DARK_BLUE" | "INDIGO" | "GREY_80_PERCENT" | "ORANGE" | "DARK_YELLOW" | "GREEN" | "TEAL" | "BLUE" | "BLUE_GREY" | "GREY_50_PERCENT" | "RED" | "LIGHT_ORANGE" | "LIME" | "SEA_GREEN" | "AQUA" | "LIGHT_BLUE" | "VIOLET" | "GREY_40_PERCENT" | "PINK" | "GOLD" | "YELLOW" | "BRIGHT_GREEN" | "TURQUOISE" | "DARK_RED" | "SKY_BLUE" | "PLUM" | "GREY_25_PERCENT" | "ROSE" | "LIGHT_YELLOW" | "LIGHT_GREEN" | "LIGHT_TURQUOISE" | "PALE_BLUE" | "LAVENDER" | "WHITE" | "CORNFLOWER_BLUE" | "LEMON_CHIFFON" | "MAROON" | "ORCHID" | "CORAL" | "ROYAL_BLUE" | "LIGHT_CORNFLOWER_BLUE" | "TAN";

export type EnumerationBordure = "BORDER_NONE" | "BORDER_THIN" | "BORDER_MEDIUM" | "BORDER_DASHED" | "BORDER_HAIR" | "BORDER_THICK" | "BORDER_DOUBLE" | "BORDER_DOTTED" | "BORDER_MEDIUM_DASHED" | "BORDER_DASH_DOT" | "BORDER_MEDIUM_DASH_DOT" | "BORDER_DASH_DOT_DOT" | "BORDER_MEDIUM_DASH_DOT_DOT" | "BORDER_SLANTED_DASH_DOT";

export type EnumerationAlignementHorizontal = "ALIGN_GENERAL" | "ALIGN_LEFT" | "ALIGN_CENTER" | "ALIGN_RIGHT" | "ALIGN_FILL" | "ALIGN_JUSTIFY" | "ALIGN_CENTER_SELECTION";

export type EnumerationAlignementVertical = "VERTICAL_TOP" | "VERTICAL_CENTER" | "VERTICAL_BOTTOM" | "VERTICAL_JUSTIFY";

export type EnumerationRemplissage = "NO_FILL" | "SOLID_FOREGROUND" | "FINE_DOTS" | "ALT_BARS" | "SPARSE_DOTS" | "THICK_HORZ_BANDS" | "THICK_VERT_BANDS" | "THICK_BACKWARD_DIAG" | "THICK_FORWARD_DIAG" | "BIG_SPOTS" | "BRICKS" | "THIN_HORZ_BANDS" | "THIN_VERT_BANDS" | "THIN_BACKWARD_DIAG" | "THIN_FORWARD_DIAG" | "SQUARES" | "DIAMONDS";
