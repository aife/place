import {createReducer, on} from '@ngrx/store';
import {Directory} from "@shared-global/core/models/api/clausier.api";
import {ReferentielEnum} from "@shared-global/core/enums/referentiel.enum";
import {FetchStatusModel} from '@shared-global/core/models/fetch-status.model';
import {
  errorGetReferentiel, getCCAGReferentiel,
  getNaturePrestationReferentiel,
  getProcedureReferentiel,
  getStatutReferentiel, getThemeClauseReferentiel,
  getTypeAuteurCanevasReferentiel, getTypeAuteurClauseReferentiel, getTypeClauseReferentiel,
  getTypeContratReferentiel,
  getTypeDocumentReferentiel, getValeurHeriteeReferentiel,
  successGetReferentiel
} from "@shared-global/store/referentiels/referentiels.action";


export interface ReferentielState {
  referentiels: Map<string, FetchStatusModel<Array<Directory>>>,

}

const initialState: ReferentielState = {
  referentiels: new Map(),
};


const _referentielReducer = createReducer(
  initialState,

  on(getProcedureReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.PROCEDURE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.PROCEDURE, ref);
    return {
      ...state
    };
  }),
  on(getNaturePrestationReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.NATURE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.NATURE, ref);
    return {
      ...state
    };
  }),
  on(getStatutReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.STATUT);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.STATUT, ref);
    return {
      ...state
    };
  }),
  on(getTypeContratReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_CONTRAT);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_CONTRAT, ref);
    return {
      ...state
    };
  }),
  on(getTypeClauseReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_CLAUSE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_CLAUSE, ref);
    return {
      ...state
    };
  }),
  on(getTypeDocumentReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_DOCUMENT);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_DOCUMENT, ref);
    return {
      ...state
    };
  }),
  on(getTypeAuteurCanevasReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CANEVAS);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_AUTEUR_CANEVAS, ref);
    return {
      ...state
    };
  }),
    on(getTypeAuteurCanevasReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CANEVAS);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_AUTEUR_CANEVAS, ref);
    return {
      ...state
    };
  }),

  on(getTypeAuteurClauseReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.TYPE_AUTEUR_CLAUSE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.TYPE_AUTEUR_CLAUSE, ref);
    return {
      ...state
    };
  }),
  on(getCCAGReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.CCAG);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.CCAG, ref);
    return {
      ...state
    };
  }),
  on(getThemeClauseReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.THEME_CLAUSE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.THEME_CLAUSE, ref);
    return {
      ...state
    };
  }),
  on(getValeurHeriteeReferentiel, (state) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(ReferentielEnum.VALEUR_HERITEE);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = null;
    ref.loading = true;
    state.referentiels.set(ReferentielEnum.VALEUR_HERITEE, ref);
    return {
      ...state
    };
  }),
  on(successGetReferentiel, (state, props) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(props.refType);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = props.list;
    ref.error = null;
    ref.loading = false;
    state.referentiels.set(props.refType, ref);
    return {
      ...state
    };
  }),
  on(errorGetReferentiel, (state, props) => {
    let ref: FetchStatusModel<Array<Directory>> = state.referentiels.get(props.refType);
    if (!ref) {
      ref = new FetchStatusModel();
    }
    ref.content = [];
    ref.error = props.error;
    ref.loading = false;
    state.referentiels.set(props.refType, ref);
    return {
      ...state
    };
  })
);

export function referentielsReducer(state, action) {
  return _referentielReducer(state, action);
}
