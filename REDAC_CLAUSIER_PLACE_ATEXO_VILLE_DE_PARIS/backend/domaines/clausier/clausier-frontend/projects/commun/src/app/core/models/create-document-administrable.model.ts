export class CreateDocumentAdministrableModel {

  libelle: string;
  code: string;
  produit: string;
  champFusion: boolean;
  plateformeUuid: string;

  constructor() {

  }

}
