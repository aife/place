import {Component, OnInit} from '@angular/core';
import {State} from "@shared-global/store";
import {Store} from "@ngrx/store";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {Agent} from "@shared-global/core/models/api/clausier-core.api";

@Component({
  selector: 'atx-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  private _unsubscribeAllMenu: Subject<any>;
  agent: Agent;

  constructor(private readonly store: Store<State>) {
    this._unsubscribeAllMenu = new Subject();
  }

  ngOnInit(): void {
    this.store.select(state => state.userReducer.user)
      .pipe(takeUntil(this._unsubscribeAllMenu))
      .subscribe(value => {
        this.agent = value;
      });
  }


}
