import {
    ClauseFormulaire,
    ClauseListeChoixCumulatifForm,
    EpmTRoleClauseAbstract
} from "@shared-global/core/models/api/clausier.api";

export class ClauseListeChoixCumulatifFormModel implements ClauseListeChoixCumulatifForm {
    clauseSelectionnee: string;
    formulaires: ClauseFormulaire[];
    formulationCollection: EpmTRoleClauseAbstract[];
    formulationModifiable: string;
    modifiable: boolean;
    numFormulation: string[];
    parametrableAgent: string;
    parametrableDirection: string;
    precochee: string[];
    sautTextFixeApres: string;
    sautTextFixeAvant: string;
    tailleChamp: string[];
    textFixeApres: string;
    textFixeAvant: string;
    valeurDefaut: string[];
    valeurDefautCourt: string[];
    valeurDefautMoyen: string[];
    valeurDefautTresLong: string[];

    constructor() {
        this.formulaires = []
    }

}
