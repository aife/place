import {Component, Input} from '@angular/core';
import {
  ChapitreDocument, ClauseDocument,
  InfoBulle
} from "@shared-global/core/models/api/clausier.api";

@Component({
  selector: 'atx-preview-chapitre-treeview',
  templateUrl: './preview-chapitre-treeview.component.html',
  styleUrls: ['./preview-chapitre-treeview.component.scss']
})
export class PreviewChapitreTreeviewComponent {

  @Input() chapitres: ChapitreDocument[];
  @Input() prefix = "";
  @Input() editeur = true;
  @Input() niveau = 0;


  hasChildren(chapitre) {
    return chapitre.chapitres?.length > 0 || chapitre.clauses?.length > 0;
  }

  get clauseMarginLeft() {
    return 20 * (this.niveau + 1);
  }

  get chapitreMarginLeft() {
    return 20 * this.niveau;
  }

  openUrl(lien: string) {
    if (lien?.length > 0) {
      window.open(lien, '_blank')
    }
  }


  isNotEmptyInfoBulle(infoBulle: InfoBulle) {
    if (infoBulle) {
      return infoBulle?.lien?.length > 0 || infoBulle?.description?.length > 0;
    }
    return false;
  }

  getInfoBulleTitle(infoBulle: InfoBulle) {
    if (!!infoBulle.description) {
      return infoBulle.description;
    } else {
      return "";
    }
  }

  getTexteConditions(clause: ClauseDocument) {
    let condition: string = clause.texteConditions;
    let tab: string[] = condition.split(" ET ");
    return "- Conditionnement(s) : " + tab.join("<strong> ET </strong>");

  }


}
