import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {InfoBulleModel} from "@shared-global/core/models/info-bulle.model";
import {InfoBulle} from "@shared-global/core/models/api/clausier.api";

@Component({
  selector: 'atx-modal-info-bulle',
  templateUrl: './modal-info-bulle.component.html',
  styleUrls: ['./modal-info-bulle.component.scss']
})
export class ModalInfoBulleComponent {
  private _infoBulle: InfoBulle = new InfoBulleModel();

  @Input() set infoBulle(infoBulle: InfoBulle) {
    this._infoBulle = !!infoBulle ? infoBulle : new InfoBulleModel();
  }

  get infoBulle() {
    return this._infoBulle;
  }


  onClose = new Subject<InfoBulle>();

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit() {
    this.onClose.next(this.infoBulle)
    this.bsModalRef.hide();

  }

  changeInfoBulle(actif: boolean) {
    this.infoBulle.actif = actif;
  }
}
