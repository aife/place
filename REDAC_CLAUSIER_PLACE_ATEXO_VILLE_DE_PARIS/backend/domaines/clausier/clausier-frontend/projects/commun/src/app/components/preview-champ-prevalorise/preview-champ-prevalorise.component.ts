import {Component} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BasePreviewQuillComponent} from "@shared-global/components/base-preview-quill/base-preview-quill.component";
import {ClauseDocument, ClauseTexte} from "@shared-global/core/models/api/clausier.api";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'atx-preview-champ-prevalorise',
  templateUrl: './preview-champ-prevalorise.component.html',
  styleUrls: ['./preview-champ-prevalorise.component.scss']
})
export class PreviewChampPrevaloriseComponent extends BasePreviewQuillComponent {
  clauseDocument: ClauseTexte;


}
