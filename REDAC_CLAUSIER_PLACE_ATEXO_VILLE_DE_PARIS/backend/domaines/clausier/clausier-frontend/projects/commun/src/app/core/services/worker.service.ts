import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {APP_BASE_HREF} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class WorkerService {


  private RELATIVE_PATH = '/';
  private readonly BASE_PATH = 'clausier/v2/worker';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }

  findTask(idTask: string): Observable<any> {
    return this.http.get<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/${idTask}.htm`)
  }

  download(idTask: string) {
    return this.http.get<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/${idTask}/download.htm`, {
      params: null,
      // @ts-ignore
      responseType: 'blob'
    })
  }

}
