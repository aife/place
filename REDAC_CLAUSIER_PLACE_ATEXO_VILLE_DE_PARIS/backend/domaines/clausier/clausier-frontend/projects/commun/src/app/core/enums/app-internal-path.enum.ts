export enum AppInternalPathEnum {
  PRIVATE_PATH = 'agent/',

  ACCUEIL = 'accueil',

  EDITEUR_DASHBOARD_CANEVAS = 'editeur/canevas/dashboard',
  CLIENT_DASHBOARD_CANEVAS = 'client/canevas/dashboard',
  FORM_CANEVAS = ':type/canevas/:id/:action',
  TYPE_CREATION_CANEVAS = ':type/canevas/creation',
  CREATION_CANEVAS = '/canevas/creation',

  EDITEUR_DASHBOARD_CLAUSE = 'editeur/clause/dashboard',
  CLIENT_DASHBOARD_CLAUSE = 'client/clause/dashboard',
  PARAMETRE_AGENT_DASHBOARD_CLAUSE = 'preparation-redaction/clauses/dashboard',
  PARAMETRE_DIRECTION_DASHBOARD_CLAUSE = 'clauses-personnalisees/clauses/dashboard',
  FORM_CLAUSE = ':type/clause/:id/:action',
  SURCHARGE_CLIENT_CLAUSE = 'clause/:id/surcharge/:idPublication',
  PARAMETRAGE_CLAUSE = 'clause/:id/parametrage/:type/:idPublication',
  TYPE_CREATION_CLAUSE = ':type/clause/creation',
  EDITEUR_EXPORT_CLAUSE = 'editeur/clause/export',
  CLIENT_EXPORT_CLAUSE = 'client/clause/export',
  CREATION_CLAUSE = '/clause/creation',

  DASHBOARD_VERSION = 'editeur/publication-clausier/dashboard',
  CREATION_VERSION = 'editeur/publication-clausier/creation',

  DASHBOARD_DOCUMENT_ADMINISTRABLE = 'document-modele/dashboard',
  DASHBOARD_CHAMP_FUSION_COMPLEXE = 'champ-fusion-complexe/dashboard',

  LOGIN = 'login',

  EXPORT_CANEVAS = ':type/canevas/export',
  CREATION_CANEVAS_EDITEUR_LEGACY = 'clausier/CreationCanevas1EditeurInit.epm',
  CREATION_CANEVAS_EDITEUR_LEGACY_EPM = 'epm.redaction/CreationCanevas1EditeurInit.epm',
  CREATION_CANEVAS_CLIENT_LEGACY = 'clausier/CreationCanevas1Init.epm',
  CREATION_CANEVAS_CLIENT_LEGACY_EPM = 'epm.redaction/CreationCanevas1Init.epm',
  CREATION_CLAUSE_EDITEUR_LEGACY = 'clausier/CreationClauseEditeurInit.epm',
  CREATION_CLAUSE_EDITEUR_LEGACY_EPM = 'epm.redaction/CreationClauseEditeurInit.epm',
  CREATION_CLAUSE_CLIENT_LEGACY = 'clausier/CreationClauseInit.epm',
  CREATION_CLAUSE_CLIENT_LEGACY_EPM = 'epm.redaction/CreationClauseInit.epm',
  DASHBOARD_CANEVAS_LEGACY = 'clausier/listCanevas.htm',
  DASHBOARD_CANEVAS_LEGACY_EPM = 'epm.redaction/listCanevas.htm',
  PARAMETRAGE_CLAUSE_LEGACY = 'clausier/ParametrageClauseInit.epm',
  PARAMETRAGE_CLAUSE_LEGACY_EPM = 'epm.redaction/ParametrageClauseInit.epm',

  CREATION_VERSION_LEGACY = 'clausier/createPublicationClausier.htm',
  CREATION_VERSION_LEGACY_EPM = 'epm.redaction/createPublicationClausier.htm',

  HISTORIQUES_VERSION_LEGACY = 'clausier/listPublicationsClausier.htm',
  HISTORIQUES_VERSION_LEGACY_EPM = 'epm.redaction/listPublicationsClausier.htm',

  DASHBOARD_CLAUSES_LEGACY = 'clausier/listClauses.htm',
  DASHBOARD_CLAUSES_LEGACY_EPM = 'epm.redaction/listClauses.htm',

  EXPORT_CLAUSES_LEGACY = 'clausier/exporterClauses.htm',
  EXPORT_CLAUSES_LEGACY_EPM = 'epm.redaction/exporterClauses.htm',
  EXPORT_CLAUSE_LEGACY = 'clausier/exporterClause.htm',
  EXPORT_CLAUSE_LEGACY_EPM = 'epm.redaction/exporterClause.htm',

  DOCUMENT_MODELE_LEGACY = 'clausier/document-modele.htm',
  DOCUMENT_MODELE_LEGACY_EPM = 'epm.redaction/document-modele.htm',

  CHAMPS_FUSION_LEGACY = 'clausier/champ-fusion-complexe.htm',
  CHAMPS_FUSION_LEGACY_EPM = 'epm.redaction/champ-fusion-complexe.htm',


  JSP_REGEX = ':prefix/:page.htm',
}

