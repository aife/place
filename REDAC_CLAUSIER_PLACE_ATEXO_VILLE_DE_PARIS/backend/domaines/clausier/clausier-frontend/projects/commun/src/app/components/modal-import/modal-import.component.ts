import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {PublicationService} from "@shared-global/core/services/publication.service";

@Component({
  selector: 'atx-modal-import',
  templateUrl: './modal-import.component.html',
  styleUrls: ['./modal-import.component.scss']
})
export class ModalImportComponent {

  @Input() titre = 'publicationClausier.importPublicationClausier.upload'
  @Input() messages: string[] = []


  onClose = new Subject<boolean>();
  file: any;

  constructor(private readonly publicationService: PublicationService, public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }


  handleFileInput(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.file = file;
    }
  }


  submit() {
    this.publicationService.importPublicationsClausier(this.file).subscribe(value => {
      this.onClose.next(true)
      this.bsModalRef.hide();
    })

  }

}
