import {
  ClauseBean,
  ClauseListeChoixCumulatifForm,
  ClauseListeChoixExclusifForm,
  ClauseTexteFixeForm,
  ClauseTexteLibreForm,
  ClauseTextePrevaloriseForm,
  ClauseValeurHeriteeForm,
  Directory,
  InfoBulle,
  PotentiellementConditionnee
} from "@shared-global/core/models/api/clausier.api";

export class ClauseBeanModel implements ClauseBean {
  contextHtml: string;
  idClauseOrigine: number;
  clauseListeChoixExclusif: ClauseListeChoixExclusifForm;
  clauseListeChoixCumulatif: ClauseListeChoixCumulatifForm;
  clauseValeurHeritee: ClauseValeurHeriteeForm;
  clauseTexteFixe: ClauseTexteFixeForm;
  clauseTexteLibre: ClauseTexteLibreForm;
  clauseTextePrevalorise: ClauseTextePrevaloriseForm;
  infoBulle: InfoBulle;
  potentiellementConditionnees: PotentiellementConditionnee[];
  surchargeActif: boolean;
  referenceClauseSurchargee: string;
  labelTypeClause: string;
  actif: boolean;
  clauseEditeur: boolean;
  context: string;
  dateCreation: Date;
  dateModification: Date;
  dateModificationMax: Date;
  dateModificationMin: Date;
  etat: number;
  id: number;
  idClause: number;
  idLastPublication: number;
  idNaturePrestation: number;
  idOrganisme: number;
  idProcedure: number;
  idPublication: number;
  idStatutRedactionClausier: number;
  idThemeClause: number;
  idTypeClause: number;
  idTypeContrat: number;
  idTypeDocument: number;
  idsClausePotentiellementConditionnees: number[];
  idsRolesClauses: number[];
  idsTypeContrats: number[];
  infoBulleText: string;
  infoBulleUrl: string;
  labelThemeClause: string;
  labelTypeAuteur: string;
  lastVersion: string;
  motsCles: string;
  referenceCanevas: string;
  referenceClause: string;
  typeAuteur: number;
  checked:boolean;
  compatibleEntiteAdjudicatrice:boolean;
  dateDerniereValidation: Date;
  parametreActif: boolean;
  datePremiereValidation: Date;
  procedure: Directory;
  typesContrat: Directory[];
}

