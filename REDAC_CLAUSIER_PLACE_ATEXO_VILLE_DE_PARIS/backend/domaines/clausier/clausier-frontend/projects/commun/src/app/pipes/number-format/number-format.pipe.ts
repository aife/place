import {Pipe, PipeTransform} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

@Pipe({
    name: 'numberFormat'
})
export class NumberFormatPipe implements PipeTransform {

    constructor(private readonly currencyPipe: CurrencyPipe) {
    }

    transform(value: any, withSymbol?: boolean, suffix?: string): string {

        if (!!value) {
            value = String(value).split(' ').join('');
        }else{
            return value
        }
        let result = this.currencyPipe.transform(value, 'EUR', 'symbol', '1.0-0');
        if (!!result && !withSymbol) {
            result = result.replace('€', '');
        }
        return suffix ? result + ' ' + suffix : result;
    }

}
