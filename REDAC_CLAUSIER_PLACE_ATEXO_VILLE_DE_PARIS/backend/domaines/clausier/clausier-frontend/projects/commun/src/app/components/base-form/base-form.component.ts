import {ActivatedRoute} from '@angular/router';
import {AbstractService} from "@shared-global/core/services/abstract.service";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {FormActionEnum} from "@shared-global/core/enums/form-action.enum";
import {BaseBean} from "@shared-global/core/models/api/clausier.api";
import {BsModalService} from "ngx-bootstrap/modal";
import {Store} from "@ngrx/store";
import {State} from "@shared-global/store";
import {AppInternalPathEnum} from "@shared-global/core/enums/app-internal-path.enum";
import {
  getCCAGReferentiel,
  getNaturePrestationReferentiel, getProcedureReferentiel,
  getStatutReferentiel, getThemeClauseReferentiel,
  getTypeAuteurCanevasReferentiel, getTypeAuteurClauseReferentiel,
  getTypeClauseReferentiel,
  getTypeContratReferentiel,
  getTypeDocumentReferentiel, getValeurHeriteeReferentiel
} from "@shared-global/store/referentiels/referentiels.action";


export abstract class BaseFormComponent<T extends BaseBean, V, P> {
    loading = false;
    editeur: boolean;
    param: any;
    action: FormActionEnum;
    formActionEnum = FormActionEnum;
    model: V;

    protected loadingSaveAndQuit = false;
    protected loadingSave = false;
    protected loadingSaveValidationRequest = false;
    protected loadingSaveValidation = false;
    protected id;
    protected idPublication;

    constructor(protected readonly abstractService: AbstractService<T, V, P>,
                protected router: RoutingInterneService,
                protected route: ActivatedRoute,
                protected modalService: BsModalService,
                protected readonly store: Store<State>,
                protected creationClientParam: any,
                protected modificationClientParam: any,
                protected duplicationClientParam: any,
                protected creationEditeurParam: any,
                protected modificationEditeurParam: any,
                protected duplicationEditeurParam: any) {


        this.route.params.subscribe(params => {
            this.id = params.id;
            if (this.route.snapshot.queryParams.idPublication)
                this.idPublication = this.route.snapshot.queryParams.idPublication;
            const action = params.action;
            this.editeur = params.type === 'editeur';
            this.setBreadcrumb(this.id, action, this.editeur)
            if (!!this.id) {
                this.loading = true;
                abstractService.getById(this.id, this.idPublication, this.editeur, this.action === FormActionEnum.DUPLICATION,
                    false).subscribe(value => {
                    this.loading = false;
                    this.model = value;
                }, () => this.loading = false)

            } else {
                this.initModel();
            }
        });
    }

    public abstract initModel();

    public abstract displaySuccessToast(action: string);

    public abstract prepareModal(config: {
        backdrop: boolean;
        ignoreBackdropClick: boolean;
        class: string
    }, type: string, i18n: string, action: string);

    back() {
        this.router.navigateFromContexte(this.param.links[0].link)
    }

    submit(idStatut: number, type: string, changePage: boolean, champs: string, idModel: number) {
        this.model['idStatutRedactionClausier'] = idStatut;
        let config = {
            backdrop: true,
            ignoreBackdropClick: true,
            class: "modal-lg"
        };
        let i18n = '';
        switch (idStatut) {
            case 1:
                i18n = 'brouillon';
                break;
            case 2:
                i18n = 'aValider';
                break;
            case 3:
                i18n = 'valide';
                break;
        }
        if (this.action === FormActionEnum.CREATION) {
            let bsModal = this.prepareModal(config, type, i18n, "creer");
            bsModal.content.onClose.subscribe(value => {
                    if (value) {
                        this[champs] = true;
                        this.abstractService.create(this.model, this.editeur)
                            .subscribe(value => {
                                this[champs] = false;
                                this.model = value;
                                if (changePage) {
                                    this.router.navigateFromContexte(this.param.links[0].link);
                                    this.displaySuccessToast("creer");
                                } else {
                                  this.navigateToModification();
                                }
                            }, () => this[champs] = false)
                    }
                    bsModal = null;
                }
            );

        } else if (this.action === FormActionEnum.MODIFICATION) {
            let bsModal = this.prepareModal(config, type, i18n, "modifier");
            bsModal.content.onClose.subscribe(value => {
                    if (value) {
                        this[champs] = true;
                      if (!this.id) {
                        this.id = idModel;
                      }
                        this.abstractService.modify(this.id, this.idPublication, this.model)
                            .subscribe(value => {
                                if (changePage) {
                                    this.router.navigateFromContexte(this.param.links[0].link);
                                    this.displaySuccessToast("modifier");
                                }
                                this[champs] = false;
                            }, () => this[champs] = false)
                    }
                    bsModal = null;
                }
            );

        } else if (this.action === FormActionEnum.DUPLICATION) {
            let bsModal = this.prepareModal(config, type, i18n, "cloner");
            bsModal.content.onClose.subscribe(value => {
                    if (value) {
                        this[champs] = true;
                        this.abstractService.clone(this.id, this.idPublication, this.model, this.editeur)
                            .subscribe(value => {
                              this.model = value;
                                this[champs] = false;
                                if (changePage) {
                                    this.router.navigateFromContexte(this.param.links[0].link);
                                    this.displaySuccessToast("cloner");

                                } else {
                                  this.navigateToModification();
                                }

                            }, () => this[champs] = false)
                    }
                    bsModal = null;
                }
            );
        }

    }


    private setBreadcrumb(id, action: string, editeur: boolean) {
        if (editeur) {
            if (!id) {
                this.action = FormActionEnum.CREATION;
                this.param = this.creationEditeurParam;
            }
            if (action === 'modification') {
                this.action = FormActionEnum.MODIFICATION;
                this.param = this.modificationEditeurParam;
            }

            if (action === 'duplication') {

                this.action = FormActionEnum.DUPLICATION;
                this.param = this.duplicationEditeurParam;
            }

        } else {
            if (!id) {
                this.action = FormActionEnum.CREATION;
                this.param = this.creationClientParam;
            }
            if (action === 'modification') {
                this.action = FormActionEnum.MODIFICATION;
                this.param = this.modificationClientParam;
            }
            if (action === 'duplication') {
                this.action = FormActionEnum.DUPLICATION;
                this.param = this.duplicationClientParam;
            }


        }
    }

  abstract navigateToModification();
}


