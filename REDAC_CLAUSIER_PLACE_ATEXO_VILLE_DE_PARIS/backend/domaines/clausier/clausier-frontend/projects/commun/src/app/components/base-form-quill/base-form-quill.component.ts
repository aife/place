import Quill from "quill";
import {IndentAttributorComponent} from "@shared-global/components/base-form-quill/indent-attributor.component";


export class BaseFormQuillComponent {
  sizeList = ['8pt', '10pt', '12pt', '14pt', '18pt', '24pt', '36pt'];
  quillConfigReadOnly = {
    toolbar: false
  }

  quillConfig = {
    toolbar: {
      container: [
        ['bold', 'italic', 'underline'],
        ['link'],
        [{size: this.sizeList}],
        [{'color': []}],
        [{'list': 'bullet'}],
        [{'indent': '-1'}, {'indent': '+1'}],
        [{'align': []}],
        ['undo', 'redo'],
      ], handlers: {
        redo() {
          this.quill.history.redo();
        }, undo() {
          this.quill.history.undo();
        }
      }
    }, history: {
      delay: 2000, maxStack: 500, userOnly: true

    }
  }

  constructor() {
    const icons = Quill.import('ui/icons');
    icons['undo'] = '<svg viewbox="0 0 18 18"><polygon class="ql-fill ql-stroke" points="6 10 4 12 2 10 6 10"></polygon>' +
      '<path class="ql-stroke" d="M8.09,13.91A4.6,4.6,0,0,0,9,14,5,5,0,1,0,4,9"></path></svg>';
    icons['redo'] = '<svg viewbox="0 0 18 18"><polygon class="ql-fill ql-stroke" points="12 10 14 12 16 10 12 10"></polygon>' +
      '<path class="ql-stroke" d="M9.91,13.91A4.6,4.6,0,0,1,9,14a5,5,0,1,1,5-5"></path></svg>';
    var Size = Quill.import('attributors/style/size');
    Size.whitelist = this.sizeList;
    Quill.register(Size, true);
    Quill.register(Quill.import('attributors/style/align'), true);
    Quill.register(Quill.import('attributors/style/direction'), true);
    this.createCustomIndent();
  }

  createCustomIndent(): void {
    const levels = [1, 2, 3, 4, 5];
    const multiplier = 2;
    const indentStyle = new IndentAttributorComponent('indent', 'margin-left', {
      scope: Quill.import('parchment').Scope.BLOCK,
      whitelist: levels.map(value => `${value * multiplier}rem`),
    });

    Quill.register(indentStyle);
  }

  removeTagForEmptyField(str: string) {
    if (str.length > 0) {
      //vérifie si la chaine contient du texte entre des balises html
      let contenuSansBalise = str.replace(/<[^>]*>/g, '');
      console.log("here ", contenuSansBalise)
      if (contenuSansBalise === '') {
        return str.replace(/<p><br[\/]?><[\/]?p>/g, '');
      } else {
        return str;
      }
    }
    else {
      return '';
    }

  }
}
