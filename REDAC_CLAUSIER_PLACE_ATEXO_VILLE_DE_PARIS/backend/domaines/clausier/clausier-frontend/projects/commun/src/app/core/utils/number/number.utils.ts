export function currencyInputChanged(value: string) {
    if (!value) {
        return null;
    }
    let isCharDigit = function (obj) {
        return obj === '0' || obj === '1' || obj === '2' || obj === '3' || obj === '4' || obj === '5' || obj === '6' || obj === '7' || obj === '8' || obj === '9';
    };
    let number = '0';
    for (let i = 0; i < value.length; i++) {
        let char: string = value.charAt(i);
        if (isCharDigit(char)) {
            number += char;
        }
    }
    return Number(number);
}
