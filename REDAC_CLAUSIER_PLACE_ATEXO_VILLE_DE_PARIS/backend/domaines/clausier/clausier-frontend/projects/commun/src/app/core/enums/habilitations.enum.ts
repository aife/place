export enum HabilitationsEnum {
  ADMINISTRATION_CLAUSE_CLIENT = "ROLE_clause",
  ADMINISTRATION_CANEVAS_CLIENT = "ROLE_canevas",
  VALIDATION_CLAUSE_CLIENT = "ROLE_validerClauses",
  VALIDATION_CANEVAS_CLIENT = "ROLE_validerCanevas",
  ADMINISTRATION_CLAUSE_EDITEUR = "ROLE_administrerClausesEditeur",
  ADMINISTRATION_CANEVAS_EDITEUR = "ROLE_administrerCanevasEditeur",
  VALIDATION_CLAUSE_EDITEUR = "ROLE_validerClausesEditeur",
  VALIDATION_CANEVAS_EDITEUR = "ROLE_validerCanevasEditeur",
  PUBLICATION_VERSION_CLAUSIER_EDITEUR = "ROLE_publierClausierEditeur",//verifier différence MPE/BDD
  PUBLICATION_VERSION_CLAUSE_EDITEUR = "ROLE_publicationVersionClauseEditeur",//verifier différence MPE/BDD
  ACTIVATION_VERSION_CLAUSIER_EDITEUR = "ROLE_activerVersionClausier",
  ADMINISTRATION_GABARIT_INTERMINISTERIEL = "ROLE_gabaritInterministeriel",
  ADMINISTRATION_GABARIT_MINISTERIEL = "ROLE_gabaritMinisteriel",
  ADMINISTRATION_GABARIT_DIRECTION = "ROLE_gabaritDirection",
  ADMINISTRATION_DOCUMENTS_MODELES = "administration_documents_modeles"

}
