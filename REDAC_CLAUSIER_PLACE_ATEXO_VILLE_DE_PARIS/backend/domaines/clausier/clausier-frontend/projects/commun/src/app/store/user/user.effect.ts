import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
    errorGetUser,
    getUser,
    logoutUser,
    setDefaultParams,
    successGetUser,
    successLogout
} from "@shared-global/store/user/user.action";
import {catchError, switchMap} from "rxjs/operators";
import {RoutingInterneService} from "@shared-global/core/services/routing-interne.service";
import {AgentService} from "@shared-global/core/services/agent.service";
import {AuthenticationService} from "@shared-global/core/services/authentification.service";
import {
    getCCAGReferentiel,
    getNaturePrestationReferentiel,
    getProcedureReferentiel,
    getStatutReferentiel,
    getThemeClauseReferentiel,
    getTypeAuteurCanevasReferentiel,
    getTypeAuteurClauseReferentiel,
    getTypeClauseReferentiel,
    getTypeContratReferentiel,
    getTypeDocumentReferentiel,
    getValeurHeriteeReferentiel
} from "@shared-global/store/referentiels/referentiels.action";

@Injectable()
export class UserEffect {
    constructor(
        private router: RoutingInterneService,
        private authenticationService: AuthenticationService,
        private agentService: AgentService,
        private readonly actions$: Actions,
    ) {
    }


    loadConnected$ = createEffect(() => this.actions$.pipe(
        ofType(getUser),
        switchMap((action) => {
                return this.agentService.whoami().pipe(switchMap(user => {
                    console.log('redirection vers -> ' + action.redirect)
                    this.router.navigateFromContexte(action.redirect ? action.redirect : '');

                    return [successGetUser({user}), getStatutReferentiel(), getNaturePrestationReferentiel(), getTypeContratReferentiel(),
                        getTypeClauseReferentiel(),getTypeDocumentReferentiel(),getTypeAuteurCanevasReferentiel(),getTypeAuteurClauseReferentiel(),
                        getProcedureReferentiel(),getCCAGReferentiel(),getThemeClauseReferentiel(),getValeurHeriteeReferentiel()];
                }), catchError((error) => [errorGetUser({error})]))
            }
        )))

    defaultParmas$ = createEffect(() => this.actions$.pipe(
        ofType(setDefaultParams),
        switchMap((action) => {
                return [getUser({redirect: action.defaultPage})];
            }
        )))


    logoutUser$ = createEffect(() => this.actions$.pipe(
        ofType(logoutUser),
        switchMap(() => {
            this.authenticationService.logout();
            //location.reload();
            return [successLogout()]
        }))
    )

}
