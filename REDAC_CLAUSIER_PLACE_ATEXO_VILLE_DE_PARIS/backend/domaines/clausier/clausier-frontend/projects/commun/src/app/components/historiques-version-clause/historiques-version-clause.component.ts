import {Component, Input} from '@angular/core';
import {ClauseBeanModel} from "@shared-global/core/models/clause-bean.model";
import {PublicationClausierModel} from "@shared-global/core/models/publication-clausier.model";

@Component({
  selector: 'atx-historiques-version-clause',
  templateUrl: './historiques-version-clause.component.html',
  styleUrls: ['./historiques-version-clause.component.scss']
})

export class HistoriquesVersionClauseComponent {


  selectedIndex = 0;
  selectedVersion: PublicationClausierModel;

  private _clauseId: number;


  @Input()
  set clauseId(value: number) {
    this._clauseId = value;
  }

  private _historiques: PublicationClausierModel[];
  clause: ClauseBeanModel;

  get historiques(): PublicationClausierModel[] {
    return this._historiques;
  }

  @Input()
  set historiques(value: PublicationClausierModel[]) {
    this._historiques = value;
    if (this._historiques?.length && this._historiques.length > 0) {
      this.selectedIndex = 0;
      this.selectedVersion = this._historiques[this.selectedIndex]
      this.setClauseBean();
      console.log(this.clause)
    }
  }

  setClauseBean() {
    this.clause = new ClauseBeanModel();
    this.clause.idPublication = this.selectedVersion.id;
    this.clause.id = this._clauseId;

  }


  selectVersion(index: number) {
    this.selectedIndex = index;
    this.selectedVersion = this._historiques[this.selectedIndex];
    this.setClauseBean();
  }
}
