import {TypeMessageEnum} from "@shared-global/core/enums/type-message.enum";

export class MessageModel {
    public constructor(
      public type?: TypeMessageEnum,
      public title?: string,
      public lines?: Array<string>

    ) {}
}
