import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'tailleFromNbCaracteres',
  pure: false
})
export class TailleFromNbCaracteresPipe implements PipeTransform {

  transform(nbCaracteres?: number): string {
    switch (nbCaracteres) {
      case 1:
        return "Long";
      case 2:
        return "Moyen";
      case 3:
        return "Court";
      case 4:
        return "Très long";
      default:
        return "Court";
    }
  }
}
