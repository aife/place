import {
  ClauseValeurHeriteeForm,
  EpmTRefValeurTypeClause,
  TableauRedaction
} from "@shared-global/core/models/api/clausier.api";

export class ClauseValeurHeriteeFormModel implements ClauseValeurHeriteeForm {
  clauseSelectionnee: string;
  listRefValeursTypeClause: EpmTRefValeurTypeClause[];
  refValeurTypeClause: string;
  sautTextFixeApres: string;
  sautTextFixeAvant: string;
  textFixeApres: string;
  textFixeAvant: string;
  idRefValeurTypeClause: number;
  tableauRedaction: TableauRedaction;
  texteVariable: string;



}
