import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {Derogation} from "@shared-global/core/models/api/clausier.api";
import {DerogationModel} from "@shared-global/core/models/derogation.model";
import {ChapitreModel} from "@shared-global/core/models/chapitre.model";

@Component({
  selector: 'atx-modal-derogation',
  templateUrl: './modal-derogation.component.html',
  styleUrls: ['./modal-derogation.component.scss']
})
export class ModalDerogationComponent {

  private _chapitre: ChapitreModel;

  @Input()
  set chapitre(value: ChapitreModel) {
    this._chapitre = value;
  }

  public get chapitre(){
    return this._chapitre;
  }

  onClose = new Subject<ChapitreModel>();

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit() {
    this.onClose.next(this.chapitre);
    this.bsModalRef.hide();
  }


}
