import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Subject} from "rxjs";
import {ClauseBean, ClauseSearch} from "@shared-global/core/models/api/clausier.api";
import {ClauseFilterModel} from "@shared-global/core/models/clause-filter.model";

@Component({
  selector: 'atx-modal-canevas-clause',
  templateUrl: './modal-select-clause.component.html',
  styleUrls: ['./modal-select-clause.component.scss']
})
export class ModalSelectClauseComponent {

  selectedClauses: ClauseBean[] = []
  onClose = new Subject<ClauseBean[]>();

  private _editeur: boolean;
  refresh: Date;

  @Input() set editeur(editeur: boolean) {
    this.filter = new ClauseFilterModel(editeur);
    this.filter.idStatutRedactionClausier = 3;
    this.filter.actif = true;
    this.filter.editeur = editeur;
    this._editeur = editeur;
  }

  get editeur() {
    return this._editeur;
  }

  filter: ClauseSearch;


  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  submit(close) {
    this.onClose.next(this.selectedClauses);
    if (close) {
      this.bsModalRef.hide();
    } else {
      this.selectedClauses = [];
      this.refresh = new Date()
    }

  }


}
