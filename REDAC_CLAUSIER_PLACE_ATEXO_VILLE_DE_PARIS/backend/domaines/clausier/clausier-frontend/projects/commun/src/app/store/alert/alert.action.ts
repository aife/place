import {createAction, props} from '@ngrx/store';

export const showSuccessAlert = createAction('[ALERT] Show Success Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean }>());

export const showErrorAlert = createAction('[ALERT] Show Error Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean }>());
export const showInfoAlert = createAction('[ALERT] Show Info Alert',
  props<{ messages: Array<string>, icon: string, keepAfterNavigationChange: boolean }>());
export const clearAlert = createAction('[ALERT] Clear Alert');
