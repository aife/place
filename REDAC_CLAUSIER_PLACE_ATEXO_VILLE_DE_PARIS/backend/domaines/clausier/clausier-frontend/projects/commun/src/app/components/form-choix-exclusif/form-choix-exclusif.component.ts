import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {BaseFormQuillComponent} from "@shared-global/components/base-form-quill/base-form-quill.component";
import {FormulationModel} from "@shared-global/core/models/formulation.model";
import {ClauseBean} from "@shared-global/core/models/api/clausier.api";
import {ClauseListeChoixExclusifFormModel} from "@shared-global/core/models/ClauseListeChoixExclusifForm.model";
import {moveArray} from "@shared-global/core/utils/technical/technical.utils";


@Component({
  viewProviders: [FormBuilder, NgForm],
  selector: 'form-choix-exclusif',
  templateUrl: './form-choix-exclusif.component.html',
  styleUrls: ['./form-choix-exclusif.component.scss']
})
export class FormChoixExclusifComponent extends BaseFormQuillComponent implements OnInit {

  formulation: FormulationModel = new FormulationModel(); //formulation à ajouter
  submitted: boolean;
  @Input() readOnly: boolean;
  @Input() editeur: boolean;
  @Input() clause: ClauseBean;
  @Input() disabled: boolean;
  @Input() parametrageAgent: boolean;
  @Input() fromEcranSurcharge: boolean;
  sautLigne1: boolean = false;
  sautLigne2: boolean = false;
  paramDirection: boolean = false;
  paramAgent: boolean = false;
  draggedIndexFormulation: number;
  draggingIndexFormulation: number;
  defaultTextColor = 'black';

  constructor() {
    super();
  }


  public ngOnInit(): void {
    this.formulation.tailleChamps = "1";
    this.formulation.precochee = false;
    //modification
    if (!!this.clause.clauseListeChoixExclusif) {
      this.submitted = true;
      if (this.clause.clauseListeChoixExclusif.sautTextFixeAvant === "true") {
        this.sautLigne1 = true;
      }
      if (this.clause.clauseListeChoixExclusif.sautTextFixeApres === "true") {
        this.sautLigne2 = true;
      }
      if (this.clause.clauseListeChoixExclusif.parametrableDirection === "1") {
        this.paramDirection = true;
      }
      if (this.clause.clauseListeChoixExclusif.parametrableAgent === "1") {
        this.paramAgent = true;
      }
    } else { //creation
      this.clause.clauseListeChoixExclusif = new ClauseListeChoixExclusifFormModel();
      this.clause.clauseListeChoixExclusif.textFixeAvant = "";
      this.clause.clauseListeChoixExclusif.textFixeApres = "";
      this.clause.clauseListeChoixExclusif.sautTextFixeAvant = String(false);
      this.clause.clauseListeChoixExclusif.sautTextFixeApres = String(false);
      this.clause.clauseListeChoixExclusif.parametrableDirection = "0";
      this.clause.clauseListeChoixExclusif.parametrableAgent = "0";
      this.clause.clauseListeChoixExclusif.modifiable = false;
    }
  }

  addEtape() {
    if (!!this.formulation.defaultValeur) {
      /*if (this.formulation.precochee) {
        this.clause.clauseListeChoixExclusif.formulaires.forEach(value => {
          value.precochee = false;
        })
      }*/
      this.formulation.numFormulation = this.clause.clauseListeChoixExclusif.formulaires.length + 1;
      this.clause.clauseListeChoixExclusif.formulaires.push(this.formulation);
      this.formulation = new FormulationModel();
      this.formulation.tailleChamps = "1";
      //this.formulation.precochee = false;
    }
  }

  removeFormulation(index: number) {
    this.clause.clauseListeChoixExclusif.formulaires.splice(index, 1);
    this.clause.clauseListeChoixExclusif.formulaires.forEach((value, index1) => {
      value.numFormulation = index1 + 1;
    })
  }


  updateSautLigne1() {
    this.clause.clauseListeChoixExclusif.sautTextFixeAvant = String(this.sautLigne1);
  }

  updateSautLigne2() {
    this.clause.clauseListeChoixExclusif.sautTextFixeApres = String(this.sautLigne2);
  }

  updateParamDirection() {
    if (this.paramDirection) {
      this.clause.clauseListeChoixExclusif.parametrableDirection = "1";
    } else {
      this.clause.clauseListeChoixExclusif.parametrableDirection = "0";
    }
  }

  updateParamAgent() {
    if (this.paramAgent) {
      this.clause.clauseListeChoixExclusif.parametrableAgent = "1";
    } else {
      this.clause.clauseListeChoixExclusif.parametrableAgent = "0";
    }
  }

  onDragEnd(list, draggingIndex, draggedIndex): void {
    if (draggingIndex !== draggedIndex && draggedIndex !== undefined) {
      moveArray(list, draggingIndex, draggedIndex);
      list.forEach((value, index) => {
        value.numFormulation = index + 1;
      })
    }
  }

  changePrecochee(index: number, precoche: any) {
    if (!precoche) {
      return;
    }
    this.clause.clauseListeChoixExclusif.formulaires.forEach((value, i) => {
      value.precochee = i === index;
    })
  }

  cleanTexteAvant() {
    this.clause.clauseListeChoixExclusif.textFixeAvant = this.removeTagForEmptyField(this.clause.clauseListeChoixExclusif.textFixeAvant);
  }

  cleanTexteApres() {
    this.clause.clauseListeChoixExclusif.textFixeApres = this.removeTagForEmptyField(this.clause.clauseListeChoixExclusif.textFixeApres);
  }
}
