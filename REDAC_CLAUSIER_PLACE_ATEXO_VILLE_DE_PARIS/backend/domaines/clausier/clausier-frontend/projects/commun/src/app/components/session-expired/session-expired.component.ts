import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'session-expired-errors',
  templateUrl: './session-expired.component.html',
  styleUrls: ['./session-expired.component.scss']
})
export class SessionExpiredComponent {

  constructor(public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }
}
