import {Component, Input} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";
import {Canevas} from "@shared-global/core/models/api/clausier.api";
import {CanevasService} from "@shared-global/core/services/canevas.service";
import moment from 'moment';
import {saveFile} from "@shared-global/core/utils/download/doawnload.utils";

@Component({
  selector: 'atx-modal-preview-canevas',
  templateUrl: './modal-preview-canevas.component.html',
  styleUrls: ['./modal-preview-canevas.component.scss']
})
export class ModalPreviewCanevasComponent {


  @Input() titre = 'confirmation'
  @Input() editeur;
  @Input() idPublication;
  @Input() isEdition: boolean;
  chapitres = []
  isCanevasVide: boolean = false;
  isCanevasEnCoursCreation: boolean = false;

  @Input() set canevas(canevas: Canevas) {
    this._canevas = canevas;
    if (!!canevas && !! canevas.idCanevas && !this.isEdition) {
      //preview depuis dashboard
      this.loading = true;
      this.canevasService.preview(canevas.idCanevas, this.editeur, false, false, true, canevas.idPublication)
        .subscribe(value => {
          this.chapitres = value;
          if (this.chapitres.length === 0) {
            this.isCanevasVide = true;
          }
          this.loading = false;
        }, () => {
          this.loading = false
        })
    } else if (!!canevas && !! canevas.idCanevas && this.isEdition) {
      //preview depuis modification
      this.loading = true;
      this.canevasService.previewCanevasEnCoursCreation(canevas).subscribe(value => {
        this.chapitres = value;
        if (this.chapitres.length === 0) {
          this.isCanevasVide = true;
        }
        this.loading = false;
      }, () => {
        this.loading = false
      })
    }
    else {
      //preview depuis creation
      this.isCanevasEnCoursCreation = true;
      this.loading = true;
      this.canevasService.previewCanevasEnCoursCreation(canevas).subscribe(value => {
        this.chapitres = value;
        if (this.chapitres.length === 0) {
          this.isCanevasVide = true;
        }
        this.loading = false;
      }, () => {
        this.loading = false
      })
    }
  }

  private _canevas: Canevas;
  get canevas() {
    return this._canevas;
  }

  url: string;
  loading = true;
  loadingPreview = false;
  loadingExport = false;
  loadingExportPdf = false;


  constructor(private readonly canevasService: CanevasService, public bsModalRef: BsModalRef) {

  }

  closeModal() {
    this.bsModalRef.hide();
  }

  protected readonly window = window;

  open() {
    if (!this.url) {
      this.loadingPreview = true;
      this.canevasService.edit(this.canevas.idCanevas, this.editeur, this.canevas.idPublication).subscribe(value => {
        this.url = value;
        window.open(this.url, '_blank');
        this.loadingPreview = false;
      }, () => this.loadingPreview = false)
    } else {
      window.open(this.url, '_blank');

    }
  }

  export(type) {
    if (type === 'docx') {
      this.loadingExport = true;
    } else if (type === 'pdf') {
      this.loadingExportPdf = true;
    }
    this.canevasService.export(this.canevas.idCanevas, this.editeur, this.canevas.idPublication, type).subscribe(value => {
      saveFile(value, this.canevas.referenceCanevas + '_' + this.canevas.titre + "_" + moment(new Date()).format("YYYYMMDD") + '.' + type);
      if (type === 'docx') {
        this.loadingExport = false;
      } else if (type === 'pdf') {
        this.loadingExportPdf = false;
      }
    }, () => {
      if (type === 'docx') {
        this.loadingExport = false;
      } else if (type === 'pdf') {
        this.loadingExportPdf = false;
      }
    })
  }
}
