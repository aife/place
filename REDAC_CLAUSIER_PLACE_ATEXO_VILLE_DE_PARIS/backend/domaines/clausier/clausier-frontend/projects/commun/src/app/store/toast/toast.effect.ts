import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {switchMap} from "rxjs/operators";
import {ToastrService} from 'ngx-toastr';
import {showErrorToast, showSuccessToast} from "@shared-global/store/toast/toast.action";

@Injectable()
export class ToastEffect {
    constructor(
        private readonly actions$: Actions,
        private toastService: ToastrService
    ) {
    }


    showSuccess$ = createEffect(() => this.actions$.pipe(
        ofType(showSuccessToast),
        switchMap((action) => {
                this.toastService.success(action.message, action.header);
                return [];

            }
        )))

    showError$ = createEffect(() => this.actions$.pipe(
        ofType(showErrorToast),
        switchMap((action) => {
                this.toastService.error(action.message, action.header);
                return [];

            }
        )))


}
