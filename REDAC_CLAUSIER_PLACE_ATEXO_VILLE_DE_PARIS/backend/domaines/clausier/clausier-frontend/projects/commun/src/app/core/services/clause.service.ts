import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AbstractService} from '@shared-global/core/services/abstract.service';
import {ClauseBean, ClauseDocument} from '@shared-global/core/models/api/clausier.api';
import {Observable} from 'rxjs';
import {PublicationClausierModel} from '@shared-global/core/models/publication-clausier.model';
import {APP_BASE_HREF} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ClauseService extends AbstractService<ClauseBean, ClauseBean, ClauseDocument> {

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    super(http, baseHref === '/redac/web-component-clausier' ? '/redac/clausier/v2/clauses' : '/clausier/v2/clauses')
  }

  exporter(editeur: boolean) {
    // @ts-ignore
    return this.http.get<string>(`${this.BASE_PATH}/export.htm`, {params: {editeur}})
  }

  getClauseSurcharge(idClause: number, idPublication: number): Observable<ClauseBean> {
    let params = {};
    if (idPublication) {
      params = {idPublication}
    }
    return this.http.get<ClauseBean>(`${this.BASE_PATH}/${idClause}/surcharge.htm`, {params: params})
  }

  getClauseAgent(idClause: number, idPublication: any, rolesActifs: boolean): Observable<ClauseBean> {
    let params = {};
    if (idPublication === "null") {
      idPublication = null
    }
    if (!!idPublication) {
      params = {idPublication}
    }
    if (rolesActifs !== null && rolesActifs !== undefined) {
      params = {rolesActifs};
    }

    return this.http.get<ClauseBean>(`${this.BASE_PATH}/${idClause}/preference-agent.htm`, {params: params})
  }


  getClauseDirection(idClause: number, idPublication: any, rolesActifs: boolean): Observable<ClauseBean> {
    let params = {};
    if (idPublication === "null") {
      idPublication = null
    }
    if (rolesActifs !== null && rolesActifs !== undefined) {
      params = {rolesActifs};
    }

    return this.http.get<ClauseBean>(`${this.BASE_PATH}/${idClause}/preference-direction.htm`, {params: params})
  }

  findClauseParametrageDirectionDefaut(idClause: number, idPublication: any): Observable<ClauseBean> {
    let params = {};
    if (idPublication === "null") {
      idPublication = null
    }
    if (!!idPublication) {
      params = {idPublication}
    }

    return this.http.get<ClauseBean>(`${this.BASE_PATH}/${idClause}/parametrage-direction-defaut.htm`, {params: params})
  }


  getClausePreview(clause: ClauseBean, editeur: boolean): Observable<ClauseDocument> {
    let params = {editeur};
    return this.http.post<ClauseDocument>(`${this.BASE_PATH}/preview.htm`, clause, {params: params})
  }

  getHistoriqueVersion(idClause: number): Observable<Array<PublicationClausierModel>> {
    return this.http.get<Array<PublicationClausierModel>>(`${this.BASE_PATH}/${idClause}/historique-version.htm`)
  }

  surchargeAgent(idClause: number, idPublication: number, clauseParametree: ClauseBean): Observable<ClauseBean> {
    let params = {};
    if (idPublication) {
      params = {idPublication}
    }
    return this.http.patch<ClauseBean>(`${this.BASE_PATH}/${idClause}/preference-agent.htm`, clauseParametree, {params: params})
  }

  surchargeDirection(idClause: number, idPublication: number, clauseParametree: ClauseBean): Observable<ClauseBean> {
    let params = {};
    if (idPublication) {
      params = {idPublication}
    }
    return this.http.patch<ClauseBean>(`${this.BASE_PATH}/${idClause}/preference-direction.htm`, clauseParametree, {params: params})
  }


}
