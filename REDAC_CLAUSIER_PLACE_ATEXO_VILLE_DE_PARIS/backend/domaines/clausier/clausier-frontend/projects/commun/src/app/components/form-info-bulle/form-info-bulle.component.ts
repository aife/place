import {Component, Input} from '@angular/core';
import {InfoBulle} from "@shared-global/core/models/api/clausier.api";
import {NgForm} from "@angular/forms";
import {InfoBulleModel} from "@shared-global/core/models/info-bulle.model";

@Component({
  selector: 'atx-form-info-bulle',
  templateUrl: './form-info-bulle.component.html',
  styleUrls: ['./form-info-bulle.component.scss']
})
export class FormInfoBulleComponent {

  private _infoBulle: InfoBulle = new InfoBulleModel();

  @Input() set infoBulle(infoBulle: InfoBulle) {
    this._infoBulle = !!infoBulle ? infoBulle : new InfoBulleModel();
  }

  get infoBulle() {
    return this._infoBulle;
  }

  @Input() form: NgForm;

  isNotEmpty(infoBulle) {
    if (infoBulle) {
      return infoBulle?.lien?.length > 0 || infoBulle?.description?.length > 0;
    }
    return false;
  }

  onChangeActif(actif: any) {
    this._infoBulle.actif = actif
    if (!actif) {
      this._infoBulle.lien = null;
      this._infoBulle.description = null;
      this._infoBulle.descriptionLien = null;
    }
  }
}
