import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PageRepresentation, PublicationClausierBean} from "@shared-global/core/models/api/clausier.api";
import {PageableModel} from "@shared-global/core/models/pageable.model";
import {Observable} from "rxjs";
import {PublicationFilterModel} from "@shared-global/core/models/publication-filter.model";
import {APP_BASE_HREF} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class PublicationService {


  private RELATIVE_PATH = '/';
  private readonly BASE_PATH = 'clausier/v2/publications-clausier';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }

  create(model: PublicationClausierBean): Observable<string> {
    return this.http.post<string>(`${this.RELATIVE_PATH}${this.BASE_PATH}.htm`, model, {})
  }

  editCommentPublicationClausier(idPublication: number, commentaire: string): Observable<PublicationClausierBean> {
    return this.http.patch<PublicationClausierBean>(`${this.RELATIVE_PATH}${this.BASE_PATH}/${idPublication}/commentaire.htm`, commentaire)
  }

  activePublicationClausier(idPublication: number): Observable<string> {
    return this.http.patch<string>(`${this.RELATIVE_PATH}${this.BASE_PATH}/${idPublication}/activation.htm`, null)
  }

  canActivatePublication(): Observable<string> {
    return this.http.get<string>(`${this.RELATIVE_PATH}${this.BASE_PATH}/activation-possible.htm`)
  }

  list(filter: any, pageable: PageableModel): Observable<PageRepresentation<PublicationClausierBean>> {
    let params: any = {
      ...pageable,
      ...this.getParams(filter)
    };
    return this.http.get<PageRepresentation<PublicationClausierBean>>(`${this.RELATIVE_PATH}${this.BASE_PATH}.htm`, {
      params: params
    });
  }

  exportPublicationClausier(filter: PublicationFilterModel) {
    let params: any = {
      ...this.getParams(filter)
    };
    // @ts-ignore
    return this.http.get<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/export.htm`, {params: params, responseType: 'blob'})
  }

  downloadPublication(idPublication: number) {
    // @ts-ignore
    return this.http.get<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/${idPublication}/download.htm`, {params: null, responseType: 'blob'})
  }

  importPublicationsClausier(file: File): any {
    const formData = new FormData();
    formData.append("file", file)
    return this.http.post<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/import.htm`, formData);
  }

  getParams(filter: any) {
    let params: any = {};
    if (!filter)
      return params;

    Object.keys(filter).filter(key => filter[key] !== null && filter[key] !== undefined)
      .forEach(key => {
        params[key] = filter[key]
      })
    return params;
  }

}
