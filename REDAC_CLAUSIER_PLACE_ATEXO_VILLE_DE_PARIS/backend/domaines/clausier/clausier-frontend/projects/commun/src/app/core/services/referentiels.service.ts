import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  Directory,
  PotentiellementConditionnee,
  PotentiellementConditionneeValeur
} from "@shared-global/core/models/api/clausier.api";
import * as _ from "lodash";
import {map} from "rxjs/operators";
import {APP_BASE_HREF} from "@angular/common";


@Injectable()
export class ReferentielsService {

  private RELATIVE_PATH = '/';
  private readonly BASE_PATH = 'clausier-api/referentiels';
  private readonly BASE_PATH2 = 'clausier/v2/referentiels';

  constructor(public readonly http: HttpClient, @Inject(APP_BASE_HREF) public baseHref: string) {
    if (this.baseHref === '/redac/web-component-clausier') {
      this.RELATIVE_PATH = '/redac/';
    }
  }


  allTypeAuteursCanevas(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/type-auteurs-canevas`);
  }

  allTypeAuteursClauses(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/type-auteurs-clause`);
  }

  allStatut(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/statuts-redaction-clausier`);
  }

  allTypesDocument(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/types-document`);
  }

  allTypesContrat(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/types-contrat`);
  }

  allRefCCAG(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/ccag`);
  }

  allNatures(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/natures`);
  }

  allProceduresMPE(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/ref-procedures`);
  }

  allProcedures(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/procedures`);
  }

  allDirectionService(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/ref-directions-services`);
  }

  allTypesClause(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/types-clause`);
  }


  allThemesClause(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/theme-clause`);
  }

  allValeursHeritees(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH}/valeur-heritee`);
  }

  /*allCritereConditions(): Observable<Array<Directory>> {
    return this.http.get<Array<Directory>>(`${this.RELATIVE_PATH}${this.BASE_PATH2}/criteres-condition.htm`);
  }*/

  allCritereConditions(): Observable<Array<PotentiellementConditionnee>> {
    return this.http.get<Array<PotentiellementConditionnee>>(`${this.RELATIVE_PATH}${this.BASE_PATH2}/criteres-condition.htm`);
  }

  allCritereConditionsValeur(id: number): Observable<Array<PotentiellementConditionneeValeur>> {
    return this.http.get<Array<PotentiellementConditionneeValeur>>(`${this.RELATIVE_PATH}${this.BASE_PATH2}/criteres-condition/${id}/valeurs.htm`);
  }


  getAppI18n(baseHrefFromDOM: any, lang: string): Observable<any> {
    console.log(baseHrefFromDOM)
    if (baseHrefFromDOM === '/')
      return this.http.get<any>(`/assets/i18n/public-${lang}.json`);
    return this.http.get<any>(`${baseHrefFromDOM}/assets/i18n/public-${lang}.json`);
  }

  getApiI18n(lang: string): Observable<any> {
    return this.http.get<any>(`${this.RELATIVE_PATH}${this.BASE_PATH}/ref-surcharge-libelle`)
      .pipe(
        map(value1 => {
          let result = {};
          Object.keys(value1).forEach(key => {
            console.log(key)
            const object = this.stringToNestedJSON(key, value1[key])
            result = _.merge(result, object);
          });
          console.log(result)
          return result;
        })
      );
  }

  stringToNestedJSON(inputString: string, value: any): any {
    const keys = inputString.split('.');
    const result: any = {};

    let currentLevel = result;
    for (let i = 0; i < keys.length - 1; i++) {
      currentLevel[keys[i]] = {};
      currentLevel = currentLevel[keys[i]];
    }

    currentLevel[keys[keys.length - 1]] = value;

    return result;
  }
}



