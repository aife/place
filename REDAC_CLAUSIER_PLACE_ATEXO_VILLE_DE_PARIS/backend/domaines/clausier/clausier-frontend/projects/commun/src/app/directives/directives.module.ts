import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatchHeightDirective} from './match-height/match-height.directive';
import {DigitOnlyDirective} from './digit-only/digit-only.directive';
import {IsAuthorizedDirective} from "@shared-global/directives/is-authorized/is-authorized.directive";
import {FormulaireClauseDirective} from "@shared-global/directives/formulaire-clause/formulaire-clause.directive";
import {PreviewClauseDirective} from "@shared-global/directives/preview-clause/preview-clause.directive";

@NgModule({
  declarations: [MatchHeightDirective, DigitOnlyDirective, IsAuthorizedDirective, FormulaireClauseDirective, PreviewClauseDirective],
  imports: [
    CommonModule
  ],
  exports: [MatchHeightDirective, DigitOnlyDirective, IsAuthorizedDirective, FormulaireClauseDirective, PreviewClauseDirective]
})
export class DirectivesModule {
}
