<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript"  language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"  language="JavaScript" src="js/editeurTexteRedaction.js"></script>

<c:set var="actionEditeur" value="" />
<c:if test="${editeur != null && editeur == true }">
    <c:set var="actionEditeur" value="Editeur" />
</c:if>

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs">
        <logic:equal name="typeAction" value="C">
            <bean:message key="CreationClause${actionEditeur}.titre.cree" />
        </logic:equal>
        <logic:equal name="typeAction" value="M">
            <bean:message key="CreationClause${actionEditeur}.titre.modifier" />
        </logic:equal>
        <logic:equal name="typeAction" value="D">
            <bean:message key="CreationClause${actionEditeur}.titre.dupliquer" />
        </logic:equal>
    </div>
    <atexo:fichePratique reference="" key="common.fichePratique"/>
    <div class="breaker"></div>

    <logic:messagesPresent>
        <div class="form-bloc-erreur msg-erreur">
            <div class="top">
                <span class="left"></span><span class="right"></span>
            </div>
            <div class="content">
                <div class="title"><bean:message key="erreur.texte.generique"/></div>
                <div class="message-left">
                    <bean:message key="redaction.msg.completer.erreur" />
                </div>
                <ul>
                    <logic:messagesPresent property="textFixeAvant">
                        <html:messages id="idMessage" property="textFixeAvant">
                            <li>${idMessage}</li>
                        </html:messages>
                    </logic:messagesPresent>
                    <logic:messagesPresent property="numFormulation">
                        <html:messages id="idMessage" property="numFormulation">
                            <li>${idMessage}</li>
                        </html:messages>
                    </logic:messagesPresent>
                    <logic:messagesPresent property="numFormuleExclusif">
                        <html:messages id="idMessage" property="numFormuleExclusif">
                            <li>${idMessage}</li>
                        </html:messages>
                    </logic:messagesPresent>
                    <logic:messagesPresent property="numFormule">
                        <html:messages id="idMessage" property="numFormule">
                            <li>${idMessage}</li>
                        </html:messages>
                    </logic:messagesPresent>
                    <logic:messagesPresent property="valeurDefaut">
                        <html:messages id="idMessage" property="valeurDefaut">
                            <li>${idMessage}</li>
                        </html:messages>
                    </logic:messagesPresent>
                </ul>
                <div class="breaker"></div>
            </div>
            <div class="bottom">
                <span class="left"></span><span class="right"></span>
            </div>
        </div>
    </logic:messagesPresent>

    <html:form method="POST" action="/Clause${actionEditeur}ListeChoixExclusifProcess.epm">
        <%--Debut bloc prévisualisation--%>
        <input type="hidden" name="nomMethode" id="nomMethode" />
        <input type="hidden" name="declencheurAction" id="declencheurAction" />

        <logic:equal parameter="nomMethode" scope="request" value="previsualiser" >
            <script type="text/javascript">
                popUp('PrevisualiserClause.epm','yes');
            </script>
        </logic:equal>

        <logic:equal name="typeAction" value="M">
            <%@ include file="tableauRecapitulatifClause.jsp"%>
        </logic:equal>
        <%--Fin bloc prévisualisation--%>

        <!--Debut bloc recap Type de clause-->
        <%@ include file="blocRecapClause.jsp"%>
        <!--Fin bloc recap Type de clause-->

        <!--Debut bloc Infos clause-->
        <div class="form-saisie">
            <c:choose>
                <c:when test="${clauseEditeur != null}">
                    <div class="surcharge-clause">
                        <div class="form-saisie">
                            <%@ include file="clauses/clauseListeChoixExclusifEditeur.jsp"%>
                        </div>
                        <div class="form-saisie float-right">
                            <%@ include file="clauses/clauseListeChoixExclusifEditeurSurchargee.jsp"%>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <%@ include file="clauses/clauseListeChoixExclusif.jsp"%>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="spacer"></div>
        <!--Fin bloc Infos clause-->

        <!--Debut boutons-->
        <div class="layer">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">

                <div class="float-left">
                    <c:choose>
                        <c:when test="${editeur != null && editeur == false && clauseEditeur == null}">
                            <a href="Annuler.epm"><img title="Annuler et quitter sans enregistrer" alt="Annuler et quitter sans enregistrer" src="<atexo:href href='images/bouton-small-annuler-quitter.gif'/>"></a>
                        </c:when>
                        <c:when test="${editeur != null && editeur == true}">
                            <a href="Annuler${actionEditeur}.epm"><img title="Annuler et quitter sans enregistrer" alt="Annuler et quitter sans enregistrer" src="<atexo:href href='images/bouton-small-annuler-quitter.gif'/>"></a>
                        </c:when>
                    </c:choose>
                </div>
                <div class="float-right">
                    <c:if test="${editeur != null && editeur == true }">
                        <atexo:filtreSecurite role="ROLE_validerClausesEditeur" egal="true">
                            <a href="javascript:validerClause();"><img title="Valider" alt="Valider" src="<atexo:href href='images/bouton-valider.gif'/>"></a>
                            <a href="javascript:demanderValidationClause();"><img title="Demander validation" alt="Demander validation" src="<atexo:href href='images/bouton-demande-validation.gif'/>"></a>
                        </atexo:filtreSecurite>
                    </c:if>
                    <c:if test="${editeur != null && editeur == false && clauseEditeur == null}">
                        <atexo:filtreSecurite role="ROLE_validerClauses" egal="true">
                            <a href="javascript:validerClause();"><img title="Valider" alt="Valider" src="<atexo:href href='images/bouton-valider.gif'/>"></a>
                            <a href="javascript:demanderValidationClause();"><img title="Demander validation" alt="Demander validation" src="<atexo:href href='images/bouton-demande-validation.gif'/>"></a>
                        </atexo:filtreSecurite>
                    </c:if>
                    <a href="javascript:save();"><img title="Enregistrer" alt="Enregistrer" src="<atexo:href href='images/bouton-save.gif'/>"></a>
                    <a href="javascript:document.forms[0].submit();"><img title="Enregistrer et quitter" alt="Enregistrer et quitter" src="<atexo:href href='images/bouton-save-quitter.gif'/>"></a>
                </div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>
        <!--Fin boutons-->
    </html:form>
</div>
<!--Fin main-part-->
