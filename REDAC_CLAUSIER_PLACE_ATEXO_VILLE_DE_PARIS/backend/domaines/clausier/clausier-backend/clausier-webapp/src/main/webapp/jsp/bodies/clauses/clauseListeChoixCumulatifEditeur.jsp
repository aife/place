<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="redactionTag" prefix="redaction" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="form-bloc">
    <div class="top">
        <span class="left"></span><span class="right"></span>
    </div>
    <div class="content">
        <h2 class="float-left">
            <html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee"
                        value="clauseEditeur"></html:radio>
            <label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeur"/></label>
        </h2>
        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <%-- <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
                <a title="<bean:message key="clause.txt.historiqueVersions"/>"
                   href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img
                        src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                <%-- </c:if> --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img
                    src="<atexo:href href='images/bouton-previsualiser.gif'/>"
                    alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>

        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixCumulatif.txt.texteFixeAvant"/>
            </span>

            <textarea class="texte-long mceEditorSansToolbar" rows="6" cols=""
                      title="Texte fixe avant" id="textFixeAvantEditeur"
                      name="textFixeAvantEditeur"
                      disabled="disabled">${clauseListeChoixCumulatifFormEditeur.textFixeAvant}</textarea>
        </div>

        <div class="line">
            <div class="retour-ligne">
                <input type="checkbox" id="sautTextFixeAvantEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.sautTextFixeAvant == 'true'}">checked="checked"</c:if>
                       disabled="disabled" title="Saut de ligne"/>

                <bean:message key="redaction.txt.sautLigne"/>
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne"/>
            </div>
        </div>
        <div class="separator"></div>

        <!--Fin Text fixe avant-->
        <!--Debut tableau formulation-->
        <div class="clause-choix-list">
            <table id="table-formulation-editeur">
                <thead>
                <tr>
                    <th class="num-formulation">
                        <bean:message key="ClauseListeChoixCumulatif.txt.numFormulation"/>
                    </th>
                    <th class="nb-carac">
                        <bean:message key="ClauseListeChoixCumulatif.txt.tailleChamp"/>
                    </th>
                    <th class="check-col">
                        <bean:message key="ClauseListeChoixCumulatif.txt.precochee"/>
                    </th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <tbody>
                <logic:notEmpty name="clauseListeChoixCumulatifFormEditeur" property="formulationCollection">
                    <script type="text/javascript">
                        <bean:size id="sizeListEditeur" name="clauseListeChoixCumulatifFormEditeur" property="formulationCollection"/>
                        var compteur = ${sizeListEditeur} +1;
                    </script>
                    <logic:iterate name="clauseListeChoixCumulatifFormEditeur" property="formulationCollection" id="clauseListeRoleEditeur" indexId="indexEditeur">
                        <bean:define id="lignePaire">${indexEditeur % 2}</bean:define>
                        <tr <logic:notEqual name="lignePaire" value="0">class="on"</logic:notEqual> id="editeur_tr_${indexEditeur + 1}">
                            <td class="num-formulation">
                                <input type="text" name="numFormulationEditeur"
                                       value="<nested:write name="clauseListeRoleEditeur" property="numFormulation"/>"
                                       title="Numéro de formulation" disabled="disabled">
                            </td>
                            <td class="nb-carac">
                                <div class="radio-choice-small">
                                    <select disabled="disabled" id="tailleChampEditeur" name="tailleChamp" class="auto"
                                            title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'editeur_valeurDefaut${indexEditeur}_', 'mceEditorSansToolbar');">
                                        <option value="4"
                                                <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="4">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.tresLong"/>
                                        </option>
                                        <option value="1"
                                                <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="1">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.long"/>
                                        </option>
                                        <option value="2"
                                                <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="2">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.moyen"/>
                                        </option>
                                        <option value="3"
                                                <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="3">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.court"/>
                                        </option>
                                    </select>
                                </div>
                            </td>

                            <td class="check-col" style="text-align: center">
                                <input type="checkbox" name="precochee" value="${indexEditeur}"
                                       <c:if test="${clauseListeRoleEditeur.precochee == '1'}">checked="checked"</c:if>
                                       disabled="disabled"/>
                            </td>
                            <td class="actions">
                                <a class="disabled-link" href="#">
                                    <img title="Supprimer la formulation (inactif)" alt="Supprimer la formulation (inactif)"
                                         src="<atexo:href href='images/picto-poubelle.gif'/>">
                                </a>
                            </td>
                        </tr>

                        <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="0">
                            <script>
                                mettreTailleChampValeurDefaut('tailleChampEditeur');
                            </script>
                        </nested:equal>

                        <tr
                                <logic:notEqual name="lignePaire" value="0">class="on"</logic:notEqual>
                                id="editeur_tr_${indexEditeur + 1}">
                            <td class="col-champ" colspan="4">
                                <div class="intitule">
                                    <bean:message key="ClauseListeChoixCumulatif.txt.valeurParDefaut" />
                                </div>
                                <c:set var="styleDisplayTresLongEditeur" value="none"/>
                                <c:set var="styleDisplayLongEditeur" value="none"/>
                                <c:set var="styleDisplayMoyenEditeur" value="none"/>
                                <c:set var="styleDisplayCourtEditeur" value="none"/>
                                <c:choose>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 4}">
                                        <c:set var="styleDisplayTresLongEditeur" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 3}">
                                        <c:set var="styleDisplayMoyenEditeur" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 2}">
                                        <c:set var="styleDisplayCourtEditeur" value="block"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="styleDisplayLongEditeur" value="block"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="valTmp"><c:out value="${clauseListeRoleEditeur.valeurDefaut}"/></c:set>
                                <textarea name="valeurDefautTresLong" title="Valeur par defaut"
                                          class="texte-tres-long mceEditorSansToolbar" id="editeur_valeurDefaut${indexEditeur}_4" cols="" rows="6"
                                          style="display:${styleDisplayTresLongEditeur}" disabled="disabled">${valTmp}</textarea>
                                <textarea name="valeurDefaut" title="Valeur par defaut" class="texte-long mceEditorSansToolbar"
                                          id="editeur_valeurDefaut${indexEditeur}_1" cols="" rows="6" style="display:${styleDisplayLongEditeur}"
                                          disabled="disabled">${valTmp}</textarea>
                                <input type="text" value="${valTmp}" name="valeurDefautCourt" title="Valeur par defaut"
                                       class="texte-court" id="editeur_valeurDefaut${indexEditeur}_3"
                                       style="display:${styleDisplayMoyenEditeur}" disabled="disabled"/>
                                <input type="text" value="${valTmp}" name="valeurDefautMoyen" title="Valeur par defaut"
                                       class="texte-long" id="editeur_valeurDefaut${indexEditeur}_2"
                                       style="display:${styleDisplayCourtEditeur}" disabled="disabled"/>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
                </tbody>
            </table>
            <a class="ajout-choix ajout-choix-inactive disabled-link"
               title="<bean:message key="ClauseListeChoixCumulatif.btn.ajouterFormulation" />"">
            <bean:message key="ClauseListeChoixCumulatif.btn.ajouterFormulation"/>
            </a>
        </div>
        <div class="breaker"></div>
        <div class="column">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiable"/>
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="formulationModifiableEditeur1"
                       name="formulationModifiableEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.formulationModifiable == '1'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiableOui"/>
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Non" id="formulationModifiableEditeur2"
                       name="formulationModifiableEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.formulationModifiable == '0'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiableNon"/>
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirection"/>
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="parametrableDirectionEditeur1"
                       name="parametrableDirectionEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.parametrableDirection == '1'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirectionOui"/>
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Non" id="parametrableDirectionEditeur2"
                       name="parametrableDirectionEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.parametrableDirection == '0'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirectionNon"/>
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgent"/>
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="parametrableAgentEditeur1" name="parametrableAgentEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.parametrableAgent == '1'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgentOui"/>
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Non" id="parametrableAgentEditeur2" name="parametrableAgentEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.parametrableAgent == '0'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgentNon"/>
            </div>
        </div>
        <div class="line">
            <div class="retour-ligne">
                <input type="checkbox" id="sautTextFixeApresEditeur"
                       <c:if test="${clauseListeChoixCumulatifFormEditeur.sautTextFixeApres == 'true'}">checked="checked"</c:if>
                       disabled="disabled" title="Saut de ligne"/>

                <bean:message key="redaction.txt.sautLigne"/>
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne"/>
            </div>
        </div>
        <div class="separator"></div>
        <!--Debut Bloc texte fixe apres-->
        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixCumulatif.txt.texteFixeApres"/>
            </span>

            <textarea class="texte-long mceEditorSansToolbar" rows="6" cols=""
                      title="Texte fixe après" id="textFixeApresEditeur"
                      name="texteFixeApresEditeur">${clauseListeChoixCumulatifFormEditeur.textFixeApres}</textarea>
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <%-- <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">		 --%>
                <a title="<bean:message key="clause.txt.historiqueVersions"/>"
                   href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img
                        src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                <%-- </c:if> --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img
                    src="<atexo:href href='images/bouton-previsualiser.gif'/>"
                    alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>
        <!--Fin Bloc texte fixe apres-->
        <div class="breaker"></div>
    </div>
    <div class="bottom">
        <span class="left"></span><span class="right"></span>
    </div>
</div>

<script type="text/javascript">
    initEditeursTexteSansToolbarRedaction();
</script>
