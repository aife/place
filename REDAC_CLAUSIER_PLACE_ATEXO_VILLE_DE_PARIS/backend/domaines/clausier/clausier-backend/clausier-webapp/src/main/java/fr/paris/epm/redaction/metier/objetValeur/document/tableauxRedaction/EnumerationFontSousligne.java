//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.05 at 05:36:49 PM CEST 
//


package fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumeration-font-sousligne.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumeration-font-sousligne">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="U_NONE"/>
 *     &lt;enumeration value="U_SINGLE"/>
 *     &lt;enumeration value="U_DOUBLE"/>
 *     &lt;enumeration value="U_SINGLE_ACCOUNTING"/>
 *     &lt;enumeration value="U_DOUBLE_ACCOUNTING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumeration-font-sousligne")
@XmlEnum
public enum EnumerationFontSousligne {

    U_NONE,
    U_SINGLE,
    U_DOUBLE,
    U_SINGLE_ACCOUNTING,
    U_DOUBLE_ACCOUNTING;

    public String value() {
        return name();
    }

    public static EnumerationFontSousligne fromValue(String v) {
        return valueOf(v);
    }

}
