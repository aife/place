package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class GeneriqueService implements GeneriqueServiceSecurise {

    private static RedactionServiceSecurise redactionService;

    @Override
    public <T extends EpmTObject> T modifierEpmTObject(T objet) {
        return redactionService.modifierEpmTObject(objet);
    }

    @Override
    public <T extends EpmTObject> void creerEpmTObject(T objet) {
        redactionService.creerEpmTObject(objet);
    }

    @Override
    public <T extends EpmTObject> List<T> chercherEpmTObject(int id, Critere criteres) {
        return redactionService.chercherEpmTObject(id, criteres);
    }

    @Override
    public <T extends EpmTObject> T chercherUniqueEpmTObject(int id, Critere criteres) {
        return redactionService.chercherUniqueEpmTObject(id, criteres);
    }

    @Override
    public <T extends EpmTObject> void supprimerEpmTObject(int id, List<T> valeur) {
        redactionService.supprimerEpmTObject(id, valeur);
    }

    @Override
    public <T extends EpmTObject> void supprimerEpmTObject(int id, T valeur) {
        redactionService.supprimerEpmTObject(id, valeur);
    }

    @Override
    public <T extends EpmTObject> List<T> modifierEpmTObject(int id, List<T> objets) {
        return new ArrayList<T>(redactionService.modifierEpmTObject(id, objets));
    }

    @Override
    public <T extends EpmTObject> T chercherObject(int id, Class<T> clazz) {
        return redactionService.chercherObject(id, clazz);
    }

    protected RedactionServiceSecurise getRedactionService() {
        return redactionService;
    }

    @Autowired
    public void setRedactionService(RedactionServiceSecurise redactionService) {
        GeneriqueService.redactionService = redactionService;
    }

}