package fr.paris.epm.redaction.presentation.views.mappers;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.redaction.presentation.views.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ServiceViewMapper implements Mapper<EpmTRefDirectionService, Service> {

	@Resource
	private OrganismeViewMapper organismeViewMapper;

	@Override
	public Service map( EpmTRefDirectionService service ) {
		final var result = new Service();

		result.setCodeExterne(service.getCodeExterne());
		result.setLibelle(service.getCodeOrganisme());
		result.setOrganisme(this.organismeViewMapper.map(service.getEpmTRefOrganisme()));
		result.setIdentifiantExterne(service.getId());
		if (null!= service.getParent()) {
			result.setParent(this.map(service.getParent()));
		}
		result.setLibelle(service.getLibelle());

		return result;
	}
}
