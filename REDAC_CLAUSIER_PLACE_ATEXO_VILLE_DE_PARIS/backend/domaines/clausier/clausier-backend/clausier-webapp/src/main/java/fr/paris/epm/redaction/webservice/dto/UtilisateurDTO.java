package fr.paris.epm.redaction.webservice.dto;

public class UtilisateurDTO {
	private OrganismeDTO organisme;

	private ServiceDTO service;

    private String nom;

    private String prenom;

    private String email;

	public UtilisateurDTO() {
	}

	public OrganismeDTO getOrganisme() {
		return organisme;
	}

	public void setOrganisme( OrganismeDTO organisme ) {
		this.organisme = organisme;
	}

	public ServiceDTO getService() {
		return service;
	}

	public void setService( ServiceDTO service ) {
		this.service = service;
	}

	public String getNom() {
		return nom;
	}

	public void setNom( String nom ) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom( String prenom ) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}
}
