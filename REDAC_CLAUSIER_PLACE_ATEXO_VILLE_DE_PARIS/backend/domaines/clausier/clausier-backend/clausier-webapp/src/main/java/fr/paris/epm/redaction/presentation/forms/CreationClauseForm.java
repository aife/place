package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.redaction.presentation.bean.ValeurPotentiellementConditionne;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Formulaire CreationClause.
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class CreationClauseForm extends ActionForm {

    /**
     * logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CreationClauseForm.class);

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut type de clause.
     */
    private String typeClause;
    /**
     * Attribut theme de clause.
     */
    private String themeClause;
    /**
     * Attribut type du document.
     */
    private String typeDocument;
    /**
     * Attribut procedure passation.
     */
    private Integer procedurePassation;
    /**
     * Attribut nature Prestasion.
     */
    private String naturePrestas;
    /**
     * Attribut mots Cles.
     */
    private String motsCles;
    /**
     * Attribut statut.
     */
    @Deprecated
    private String statut;
    /**
     * Attribut critere et ses/sa valeur(s).
     */
    private List<ValeurPotentiellementConditionne> valeurPotentiellementConditionneList;
    /**
     * Attribut Collection de critères .
     */
    private Collection critereCollection;
    /**
     * Attribut info Bulle.
     */
    private String infoBulle;
    /**
     * Attribut texte Info Bulle.
     */
    private String texteInfoBulle;
    /**
     * Attribut référence.
     */
    private String reference;
    /**
     * Attribut lien Hypertexte.
     */
    private String lienHypertext;
    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private String compatibleEntiteAdjudicatrice;
    /**
     * Attribut Collection de types de clause.
     */

    private Collection typeClauseCollection;
    /**
     * Attribut Collection de thème Clause.
     */
    private Collection themeClauseCollection;
    /**
     * Attribut Collection de types de document.
     */
    private Collection typeDocumentCollection;
    /**
     * Attribut Collection de nature préstation.
     */
    private Collection naturePrestasCollection;
    /**
     * Attribut Collection de procedure passation.
     */
    private Collection procedurePassationCollection;

    private List<Integer> listTypesContratsSelectionnes;

    private Collection typeContratCollection;
    /**
     * permet d'initialiser le formulaire de création de la clause.
     */
    public void reset() {
        typeClause = null;
        themeClause = null;
        typeDocument = null;
        procedurePassation = null;
        naturePrestas = null;
        motsCles = null;
        statut = "0";
        valeurPotentiellementConditionneList = null;
        infoBulle = "0";
        texteInfoBulle = null;
        reference = null;
        lienHypertext = null;
        typeClauseCollection = null;
        themeClauseCollection = null;
        typeDocumentCollection = null;
        compatibleEntiteAdjudicatrice = null;
        typeContratCollection = null;
        critereCollection = null;
        listTypesContratsSelectionnes = new ArrayList<>();
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        compatibleEntiteAdjudicatrice = null;
    }

    /**
     * @return Collection de natures de préstation.
     */
    public final Collection getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur pour initialiser une collection de natures de préstation.
     */
    public final void setNaturePrestasCollection(final Collection valeur) {
        naturePrestasCollection = valeur;
    }

    /**
     * @return Collection de types de document.
     */
    public final Collection getTypeDocumentCollection() {
        return typeDocumentCollection;
    }

    /**
     * @param valeur pour initialiser une collection de types de document.
     */
    public final void setTypeDocumentCollection(final Collection valeur) {
        typeDocumentCollection = valeur;
    }

    /**
     * @return info Bulle.
     */
    public final String getInfoBulle() {
        return infoBulle;
    }

    /**
     * @param valeur initialiser l'info-bulle.
     */
    public final void setInfoBulle(final String valeur) {
        infoBulle = valeur;
    }

    /**
     * @return mots Clés.
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * @param valeur initialise les mots clés.
     */
    public final void setMotsCles(final String valeur) {
        motsCles = valeur;
    }

    /**
     * @return nature de préstation.
     */
    public final String getNaturePrestas() {
        return naturePrestas;
    }

    /**
     * @param valeur positione la nature de préstation.
     */
    public final void setNaturePrestas(final String valeur) {
        naturePrestas = valeur;
    }

    /**
     * @return procédure de passation.
     */
    public final Integer getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * @param valeur positionne la procédure de passation.
     */
    public final void setProcedurePassation(final Integer valeur) {
        procedurePassation = valeur;
    }

    /**
     * @return statut de la clause.
     */
    @Deprecated
    public final String getStatut() {
        return statut;
    }

    /**
     * @param valeur initialise le statut de la clause.
     */
    @Deprecated
    public final void setStatut(final String valeur) {
        statut = valeur;
    }

    /**
     * @return Collection de thèmes de la clause.
     */
    public final Collection getThemeClauseCollection() {
        return themeClauseCollection;
    }

    /**
     * @param valeur positionne une collection de thèmes de la clause .
     */
    public final void setThemeClauseCollection(final Collection valeur) {
        themeClauseCollection = valeur;
    }

    /**
     * @return thème clause
     */
    public String getThemeClause() {
        return themeClause;
    }

    /**
     * @param valeur pour renvoyer l'attribut thème de clause.
     */
    public void setThemeClause(final String valeur) {
        themeClause = valeur;
    }

    /**
     * @return type de clause.
     */
    public final String getTypeClause() {
        return typeClause;
    }

    /**
     * @param valeur initialise le type de la clause.
     */
    public final void setTypeClause(final String valeur) {
        typeClause = valeur;
    }

    /**
     * @return type de document.
     */
    public final String getTypeDocument() {
        return typeDocument;
    }

    /**
     * @param valeur initialise type de document.
     */
    public final void setTypeDocument(final String valeur) {
        typeDocument = valeur;
    }

    /**
     * @return Collection de types de clause.
     */
    public final Collection getTypeClauseCollection() {
        return typeClauseCollection;
    }

    /**
     * @param valeur positionne une collection de types de clause..
     */
    public final void setTypeClauseCollection(final Collection valeur) {
        typeClauseCollection = valeur;
    }

    /**
     * @return texte Info Bulle.
     */
    public final String getTexteInfoBulle() {
        return texteInfoBulle;
    }

    /**
     * @param valeur positionne texte de l'info-bulle .
     */
    public final void setTexteInfoBulle(final String valeur) {
        texteInfoBulle = valeur;
    }

    /**
     * @return lien Hypertexte.
     */
    public final String getLienHypertext() {
        return lienHypertext;
    }

    /**
     * @param valeur positionne le lien Hypertexte.
     */
    public final void setLienHypertext(final String valeur) {
        lienHypertext = valeur;
    }

    /**
     * @return référence de la clause
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @param valeur initialise la référence de la clause.
     */
    public final void setReference(final String valeur) {
        reference = valeur;
    }

    /**
     * @return Collection de procedures de passation.
     */
    public final Collection getProcedurePassationCollection() {
        return procedurePassationCollection;
    }

    /**
     * @param valeur initialise une collection de procedures de passation.
     */
    public final void setProcedurePassationCollection(final Collection valeur) {
        procedurePassationCollection = valeur;
    }

    /**
     * @return the compatibleEntiteAdjudicatrice
     */
    public final String getCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    /**
     * @param valeur the compatibleEntiteAdjudicatrice to set
     */
    public final void setCompatibleEntiteAdjudicatrice(final String valeur) {
        this.compatibleEntiteAdjudicatrice = valeur;
    }

    public Collection getTypeContratCollection() {
        return typeContratCollection;
    }

    public void setTypeContratCollection(Collection typeContratCollection) {
        this.typeContratCollection = typeContratCollection;
    }

    public List<Integer> getListTypesContratsSelectionnes() {
        return listTypesContratsSelectionnes;
    }

    public void setListTypesContratsSelectionnes(List<Integer> listTypesContratsSelectionnes) {
        this.listTypesContratsSelectionnes = listTypesContratsSelectionnes;
    }

    public List<ValeurPotentiellementConditionne> getValeurPotentiellementConditionneList() {
        return valeurPotentiellementConditionneList;
    }

    public void setValeurPotentiellementConditionneList(List<ValeurPotentiellementConditionne> valeurPotentiellementConditionneList) {
        this.valeurPotentiellementConditionneList = valeurPotentiellementConditionneList;
    }

    public Collection getCritereCollection() {
        return critereCollection;
    }

    public void setCritereCollection(Collection critereCollection) {
        this.critereCollection = critereCollection;
    }

    /* methode validate () pour valider le formulaire.
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        ActionErrors erreurs = super.validate(mapping, request);
        SessionManagerGlobal sessionManager = SessionManagerGlobal.getInstance();
        HttpSession session = null;
        try {
            session = sessionManager.getSession(request);
        } catch (SessionExpireeException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        if (erreurs == null)
            erreurs = new ActionErrors();

        erreurs.clear();

        if (typeClause == null || typeClause.trim().equals("-1"))
            erreurs.add("typeClause", new ActionMessage("creationClause.valider.typeClause"));

        if (themeClause == null || themeClause.trim().equals("-1"))
            erreurs.add("themeClause", new ActionMessage("creationClause.valider.themeClause"));

        if (procedurePassation == null) {
            erreurs.add("procedurePassation", new ActionMessage("creationClause.valider.procedurePassation"));
            erreurs.add("procedurePassation", new ActionMessage("creationClause.valider.procedurePassationActive"));
        }

        /* ajout des valeur sélectionnées dans la CanevasForm */
        Iterable<String> typesContratsSelectionnes = request.getParameterValues("typesContratsSelectionnes") != null ?
                Arrays.asList(request.getParameterValues("typesContratsSelectionnes")) :
                (Iterable<String>) request.getSession().getAttribute("typesContratsSelectionnes");
        if (typesContratsSelectionnes != null) {
            if (typesContratsSelectionnes != (Iterable) listTypesContratsSelectionnes) { // c'est la meme liste ? (meme address)
                listTypesContratsSelectionnes.clear();
                for (String tcs : typesContratsSelectionnes)
                    listTypesContratsSelectionnes.add(Integer.parseInt(tcs));
            }
        } else {
            listTypesContratsSelectionnes.add(0);
        }

        /* vérification que l'utilisateur a fait son choix */
        /*if (listTypesContratsSelectionnes == null || listTypesContratsSelectionnes.size() == 0) {
            LOG.debug("typeContrat error ");
            erreurs.add("typeContrat", new ActionMessage("creationClause.valider.typeContrat"));
        }*/

        /* si l'utilisateur a chois "TOUS TYPES CONTRATS" -> NULL dans la base de PopulateActionForm.java:50) org.données */
        if (listTypesContratsSelectionnes.contains(Integer.valueOf(0)))
            listTypesContratsSelectionnes.clear();

        if (naturePrestas == null || naturePrestas.trim().equals("-1"))
            erreurs.add("naturePrestas", new ActionMessage("creationClause.valider.naturePrestas"));

        if (typeDocument == null || typeDocument.trim().equals("-1"))
            erreurs.add("typeDocument", new ActionMessage("creationClause.valider.typeDocument"));

        int i = 0;
        for (ValeurPotentiellementConditionne valeurPotentiellementConditionne : valeurPotentiellementConditionneList) {
            if (valeurPotentiellementConditionne.getPotCdt() != null && valeurPotentiellementConditionne.getPotCdt()) {

                if (valeurPotentiellementConditionne.getCritere() == null || valeurPotentiellementConditionne.getCritere() == 0)
                    erreurs.add("valeurPotentiellementConditionneList[" + i + "].critere", new ActionMessage("creationClause.valider.critereConditionnement", i+1));

                if (request.getParameter("valeurPotentiellementConditionneList" + i + "valeurListSelectionnees") != null) {
                    valeurPotentiellementConditionne.getValeurList().clear();
                    for (String valeur : request.getParameterValues("valeurPotentiellementConditionneList" + i + "valeurListSelectionnees")) {
                        if (Integer.parseInt(valeur) != 0)
                            valeurPotentiellementConditionne.getValeurList().add(Integer.parseInt(valeur));
                    }
                }

                if (valeurPotentiellementConditionne.getValeurList() == null || valeurPotentiellementConditionne.getValeurList().isEmpty())
                    erreurs.add("valeurPotentiellementConditionneList" + i + "valeurListSelectionnees", new ActionMessage("creationClause.valider.valeurConditionnement", i+1));
            }
            i++;
        }

        if (infoBulle.equals("1") && texteInfoBulle.trim().equals(""))
            erreurs.add("texteInfoBulle", new ActionMessage("creationClause.valider.texteInfoBulle"));

        return erreurs;
    }

}
