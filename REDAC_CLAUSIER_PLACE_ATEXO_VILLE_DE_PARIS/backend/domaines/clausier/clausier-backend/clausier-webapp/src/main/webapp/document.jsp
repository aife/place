<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="fr.paris.epm.redaction.document.nocache.js"></script>
<script type="text/javascript" src='js/tiny_mce/tiny_mce.js'></script>
<script type="text/javascript" src='js/editeurTexteRedaction.js'></script>
<script type="text/javascript" src="js/recap.js"></script>
<script type="text/javascript" src="js/apercuDynamique.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        setInterval(function () {
            textChanged()
        }, 200);
    });
</script>

<script type="text/javascript">
    initEditeursTexteRedaction();
</script>

<iframe id="__gwt_historyFrame" style="width: 0; height: 0; border: 0"></iframe>
<div class="main-part">
    <div class='breadcrumbs'>
        Administration > Document > Gestion
    </div>

    <div class='breaker'></div>
    <div id="notificationClauses"></div>
    <div id="main-part"></div>
    <div id="boutons-top"></div>
</div>

<script>
    function detaillerConsultation(id) {
        popUp("${requestScope.passationURL}/ficheSynthetique.epm?idConsultation=" + id, 'yes', 'details');
    }
</script>
