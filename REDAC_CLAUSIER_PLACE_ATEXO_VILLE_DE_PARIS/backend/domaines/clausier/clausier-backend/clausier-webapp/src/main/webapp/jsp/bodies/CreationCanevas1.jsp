<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--@elvariable id="frmCreationCanevas" type="fr.paris.epm.redaction.presentation.forms.CreationCanevasForm"--%>

<script type="text/javascript">
    var mapContratsProceduresIds = {};
    var toutesProcedures = [];
</script>

<c:choose>
    <c:when test="${editeur != null && editeur }">
        <c:set var="actionEditeur" value="Editeur" />
    </c:when>
    <c:otherwise>
        <c:set var="actionEditeur" value="" />
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test='${typeAction.equals("M") || typeAction.equals("D")}'>
        <c:set var="desactiverChamps" value="true" />
    </c:when>
    <c:otherwise>
        <c:set var="desactiverChamps" value="false" />
    </c:otherwise>
</c:choose>

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs m-b-2">
        <strong>
            <logic:equal name="typeAction" value="C">
                <bean:message key="creationCanevas1${actionEditeur}.txt.cree" />
            </logic:equal>
            <logic:equal name="typeAction" value="M">
                <bean:message key="creationCanevas1${actionEditeur}.txt.modifier" />
            </logic:equal>
            <logic:equal name="typeAction" value="D">
                <bean:message key="creationCanevas1${actionEditeur}.txt.dupliquer" />
            </logic:equal>
        </strong>
    </div>

    <atexo:fichePratique reference="" key="common.fichePratique" />
    <div class="breaker"></div>

    <logic:messagesPresent>
        <div class="form-bloc-erreur msg-erreur">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <div class="title"><bean:message key="erreur.texte.generique"/></div>
                <ul><li><bean:message  key="redaction.msg.global.erreur"/></li></ul>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
    </logic:messagesPresent>

    <!--Debut bloc Infos clause-->
    <div class="form-saisie">
        <html:form action="/CreationCanevas1${actionEditeur}Process.epm">
            <input type="hidden" name="nomMethode" id="nomMethode" />
            <div class="form-bloc">
                <div class="top"><span class="left"></span><span class="right"></span></div>

                <div class="content">
                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.canevasTitle"/></span>
                        <html:text property="canevasTitle" title="Titre du canevas" maxlength="200"
                                   styleId="canevasTitle" errorStyleClass="error-border" styleClass="width-200"
                                   disabled="${desactiverChamps && !typeAction.equals('D')}"/>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.docType"/></span>
                        <html:select property="typeDocument" title="Type de document"
                                     styleId="typeDocument" errorStyleClass="error-border" styleClass="width-200"
                                     disabled="${desactiverChamps}">
                            <html:option value="0"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="typeDocumentCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.typeContrat"/></span>
                        <div class="checklist-box liste-procedures" title="Type de contrat" id="typeContratDivId">

                            <c:set var="typesContratsSelectionnes" value="${frmCreationCanevas.listTypesContratsSelectionnes}" scope="request" />
                            <c:set var="typesContratsSelectionnes" value="${frmCreationCanevas.listTypesContratsSelectionnes}" scope="session" />

                            <label for="typesContratsSelectionnes_0">
                                <input type="checkbox" id="typesContratsSelectionnes_0" name="typesContratsSelectionnes" value="0"
                                       onclick="javascript:typeContratCheckAll();"
                                       <c:if test="${desactiverChamps eq true}">disabled="disabled"</c:if>
                                       <c:if test="${frmCreationCanevas.listTypesContratsSelectionnes.size() == 0 ||
                                        frmCreationCanevas.listTypesContratsSelectionnes.contains(Integer.valueOf(0))}">checked</c:if> />
                                <bean:message key="redaction.txt.neDependPasDeTypeDeContrat" />
                            </label>

                            <hr>

                            <c:forEach items="${frmCreationCanevas.typeContratCollection}" var="typeContratItem" varStatus="status">
                                <label for="typesContratsSelectionnes_${typeContratItem.id}">
                                    <input type="checkbox" id="typesContratsSelectionnes_${typeContratItem.id}" class="typesContratsSelectionnes"
                                           name="typesContratsSelectionnes" value="${typeContratItem.id}"
                                           onclick="javascript:typeContratCheck();"
                                           <c:if test="${desactiverChamps eq true}">disabled="disabled"</c:if>
                                           <c:if test="${frmCreationCanevas.listTypesContratsSelectionnes.contains(typeContratItem.id)}">checked</c:if> />
                                        ${typeContratItem.libelle}
                                </label>

                                <script type="text/javascript">
                                    mapContratsProceduresIds[${typeContratItem.id}] = ${typeContratItem.idsProcedures};
                                </script>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.naturePrestas"/></span>
                        <html:select property="naturePrestas" title="Nature des prestations" disabled="${desactiverChamps}"
                                     styleId="naturePrestas" styleClass="width-200" errorStyleClass="error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="naturePrestasCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.ccag"/></span>
                        <html:select property="refCCAG" title="CCAG de référence" disabled="${desactiverChamps}"
                                     styleId="refCCAG" styleClass="width-200" errorStyleClass="error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="listeCCAG" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.procedure"/></span>
                        <div class="checklist-box liste-procedures" title="Procédure de passation" id="procPassaDivId">
                            <logic:messagesPresent message="creationCanevas.valider.procedurePassation">
                                <script language="JavaScript">errorDivCanevasProcedurePassation(document.getElementById('procPassaDivId'));</script>
                            </logic:messagesPresent>

                            <c:set var="proceduresPassationSelectionnees" value="${frmCreationCanevas.listProceduresPassationSelectionnees}" scope="request" />
                            <c:set var="proceduresPassationSelectionnees" value="${frmCreationCanevas.listProceduresPassationSelectionnees}" scope="session" />

                            <label id="label_procedure_0}" for="proceduresPassationSelectionnees_0">
                                <input type="checkbox" id="proceduresPassationSelectionnees_0" name="proceduresPassationSelectionnees" value="0"
                                       onclick="javascript:procedureCheckAll();"
                                       <c:if test="${desactiverChamps eq true}">disabled="disabled"</c:if>
                                       <c:if test="${frmCreationCanevas.listProceduresPassationSelectionnees.size() == 0 ||
                                        frmCreationCanevas.listProceduresPassationSelectionnees.contains(Integer.valueOf(0))}">checked</c:if> />
                                <bean:message key="redaction.txt.Toutes" />
                            </label>

                            <hr>

                            <c:forEach items="${frmCreationCanevas.procedureCollection}" var="procedureItem" varStatus="status">
                                <label id="label_procedure_${procedureItem.id}" for="proceduresPassationSelectionnees_${procedureItem.id}">
                                    <input type="checkbox" id="proceduresPassationSelectionnees_${procedureItem.id}"
                                           name="proceduresPassationSelectionnees" class="proceduresPassationSelectionnees" value="${procedureItem.id}"
                                           onclick="javascript:procedureCheck();"
                                           <c:if test="${desactiverChamps eq true}">disabled="disabled"</c:if>
                                           <c:if test="${frmCreationCanevas.listProceduresPassationSelectionnees.contains(procedureItem.id)}">checked</c:if> />
                                        ${procedureItem.libelle}
                                </label>

                                <script type="text/javascript">
                                    toutesProcedures.push(${procedureItem.id});
                                </script>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="line">
                        <span class="intitule"><bean:message key="creationCanevas1.txt.statutCanevas"/></span>
                        <div class="content-bloc-long">
                            <div class="radio-choice">
                                <html:radio property="statut" title="Actif" value="1" styleId="statut"/>
                                <bean:message key="creationCanevas1.txt.oui"/>
                            </div>
                            <div class="radio-choice">
                                <html:radio property="statut" title="Inactif" value="0" styleId="statut"/>
                                <bean:message key="creationCanevas1.txt.non"/>
                            </div>
                            <div class="breaker"></div>
                            <c:if test="${entiteAdjudicatrice != null}">
                                <div class="radio-choice">
                                    <html:checkbox property="compatibleEntiteAdjudicatrice" disabled="${desactiverChamps}">
                                        <bean:message key="CreationClause.txt.compatibleEntiteAdjudicatrice"/>
                                    </html:checkbox>
                                </div>
                            </c:if>
                        </div>
                    </div>

                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>
            <!--Fin bloc Infos clause-->

            <!--Debut boutons-->
            <div class="spacer"></div>
            <div class="boutons">
                <html:hidden property="nameAction"/>
                <c:choose>
                    <c:when test="${typeAction eq 'C'}">
                        <a href="CreationCanevas1${actionEditeur}Init.epm" class="annuler"><bean:message key="creationCanevas1.txt.annuler"/></a>
                    </c:when>
                    <c:when test="${empty accesModeSaaS && (editeur != null && editeur)}">
                        <a href="listCanevas.htm?editeur=yes" class="annuler"><bean:message key="creationCanevas1.txt.annuler"/></a>
                    </c:when>
                    <c:when test="${empty accesModeSaaS && (editeur == null || !editeur)}">
                        <a href="listCanevas.htm?editeur=no" class="annuler"><bean:message key="creationCanevas1.txt.annuler"/></a>
                    </c:when>
                    <c:when test="${not empty accesModeSaaS && (editeur != null && editeur)}"> <!-- ORME recherche -->
                        <a href="listCanevas.htm?editeur=yes&accesModeSaaS=yes" class="annuler"><bean:message key="creationCanevas1.txt.annuler"/></a>
                    </c:when>
                    <c:when test="${not empty accesModeSaaS && (editeur == null || !editeur)}"> <!-- ORME recherche -->
                        <a href="listCanevas.htm?editeur=no&accesModeSaaS=yes" class="annuler"><bean:message key="creationCanevas1.txt.annuler"/></a>
                    </c:when>
                </c:choose>

                <a href="javascript:submitAction('next', document.forms[0].nameAction)" class="valider"><bean:message key="creationCanevas1.txt.valider"/></a>
                <logic:equal name="typeAction" value="M">
                    <a href="javascript:submitAction('valider', document.forms[0].nameAction)" class="enregistrer"><bean:message key="creationCanevas1.txt.enregistrer"/></a>
                </logic:equal>
            </div>
            <!--Fin boutons-->
        </html:form>
    </div>

    <script type="text/javascript">

        filtrerProceduresCanevas();

        jQuery(document).ready(function () {
            filtrerProceduresCanevas();
        });

        function typeContratCheckAll() {
            if (jQuery("#typesContratsSelectionnes_0").is(":checked"))
                jQuery(".typesContratsSelectionnes").prop("checked", false);

            filtrerProceduresCanevas();
        }

        function typeContratCheck() {
            if (jQuery(".typesContratsSelectionnes:checked").length > 0)
                jQuery("#typesContratsSelectionnes_0").prop("checked", false);

            filtrerProceduresCanevas();
        }

        function procedureCheckAll() {
            if (jQuery("#proceduresPassationSelectionnees_0").is(":checked"))
                jQuery(".proceduresPassationSelectionnees").prop("checked", false);
        }

        function procedureCheck() {
            if (jQuery(".proceduresPassationSelectionnees:checked").length > 0)
                jQuery("#proceduresPassationSelectionnees_0").prop("checked", false);
        }

        function filtrerProceduresCanevas() {

            var idsContrats = [];
            if (jQuery("#typesContratsSelectionnes_0").is(":checked")) {
                idsContrats.push(0);
            } else {
                var typesContratsSelectionnes = jQuery(".typesContratsSelectionnes:checked");

                for (var i = 0; i < typesContratsSelectionnes.length; i++) {
                    var typeContratId = jQuery(typesContratsSelectionnes[i]).attr("id").substring("typesContratsSelectionnes_".length);
                    idsContrats.push(parseInt(typeContratId));
                }
            }

            var proceduresCommunes = toutesProcedures;
            if (idsContrats[0] !== 0) {
                proceduresCommunes = getProceduresCommunes(idsContrats);
            }

            for (var i = 0; i < toutesProcedures.length; i++) {
                var proceduresPassationSelectionnees = jQuery("#proceduresPassationSelectionnees_" + toutesProcedures[i]);

                if (proceduresCommunes.indexOf(toutesProcedures[i]) === -1) {
                    proceduresPassationSelectionnees.closest("label").hide();
                    proceduresPassationSelectionnees.prop("checked", false);
                } else {
                    proceduresPassationSelectionnees.closest("label").show();
                }
            }
        }

        function getProceduresCommunes(idsContrats) {
            var proceduresCommunes = toutesProcedures;

            for (var i = 0; i < idsContrats.length; i++) {
                var intersection = [];
                for (var j = 0; j < proceduresCommunes.length; j++) {
                    if (mapContratsProceduresIds[idsContrats[i]].indexOf(proceduresCommunes[j]) !== -1)
                        intersection.push(proceduresCommunes[j]);
                }

                proceduresCommunes = intersection;
                if (proceduresCommunes.length === 0)
                    break;
            }
            return proceduresCommunes;
        }

    </script>

</div>
<!--Fin main-part-->
