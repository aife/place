/**
 * 
 */
package fr.paris.epm.redaction.metier.objetValeur.document;

import java.io.Serializable;

/**
 * Objet contenant la clause qui a permis la création du document, la clause du document modifié, 
 * la clause qui a servi à la création du document dupliqué et la clause en cours de rédaction
 * @author MGA
 *
 */
public class ComparaisonClauseDuplique implements Serializable{

    /**
     * ID de serialisation
     */
    private static final long serialVersionUID = 1826543825892503652L;
    /**
     * La clause du clausier du document initial
     */
    private ClauseDocument documentInitialClauseClausier;
    /**
     * La clause redacteur du document initial
     */
    private ClauseDocument documentInitialClauseRedacteur;
    /**
     * La clause du clausier du document en cours
     */
    private ClauseDocument documentEnCoursClauseClausier;
    /**
     * La clause du redacteur du document en cours
     */
    private ClauseDocument documentEnCoursClauseRedacteur;
    
    /**
     * @return the documentInitialClauseClausier
     */
    public final ClauseDocument getDocumentInitialClauseClausier() {
        return documentInitialClauseClausier;
    }
    /**
     * @param documentInitialClauseClausier the documentInitialClauseClausier to set
     */
    public final void setDocumentInitialClauseClausier(final ClauseDocument valeur) {
        this.documentInitialClauseClausier = valeur;
    }
    /**
     * @return the documentInitialClauseRedacteur
     */
    public final ClauseDocument getDocumentInitialClauseRedacteur() {
        return documentInitialClauseRedacteur;
    }
    /**
     * @param documentInitialClauseRedacteur the documentInitialClauseRedacteur to set
     */
    public final void setDocumentInitialClauseRedacteur(final ClauseDocument valeur) {
        this.documentInitialClauseRedacteur = valeur;
    }
    /**
     * @return the documentEnCoursClauseClausier
     */
    public final ClauseDocument getDocumentEnCoursClauseClausier() {
        return documentEnCoursClauseClausier;
    }
    /**
     * @param documentEnCoursClauseClausier the documentEnCoursClauseClausier to set
     */
    public final void setDocumentEnCoursClauseClausier(final ClauseDocument valeur) {
        this.documentEnCoursClauseClausier = valeur;
    }
    /**
     * @return the documentEnCoursClauseRedacteur
     */
    public final ClauseDocument getDocumentEnCoursClauseRedacteur() {
        return documentEnCoursClauseRedacteur;
    }
    /**
     * @param documentEnCoursClauseRedacteur the documentEnCoursClauseRedacteur to set
     */
    public final void setDocumentEnCoursClauseRedacteur(final ClauseDocument valeur) {
        this.documentEnCoursClauseRedacteur = valeur;
    }
    
}
