package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.commun.ResultList;
import fr.paris.epm.global.coordination.facade.AbstractMapperFacade;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.global.coordination.mapper.AbstractEpmObjectToBeanMapper;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.metier.redaction.ClauseViewCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTClausePub;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.redaction.EpmVClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.coordination.service.ClauseService;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.bean.ClauseSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade de gestion des Clause Niveau Ministeriel
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Service
public class ClauseViewFacadeImpl extends AbstractMapperFacade<ClauseBean, EpmVClause> implements ClauseViewFacade {

    private static final Logger LOG = LoggerFactory.getLogger(ClauseViewFacadeImpl.class);

    private ClauseMapper clauseMapper;

    private ClauseService clauseService;

    public ClauseViewFacadeImpl() {
        setCritereContructor(ClauseViewCritere::new);
        setDefaultSortField("dateModification");
        setDefaultIdField("idClause");
    }

    @Override
    public ClauseService getService() {
        return clauseService;
    }

    @Override
    protected boolean defaultRemoveTest(int id) {
        return false;
    }

    @Override
    public AbstractBeanToEpmObjectMapper<ClauseBean, EpmVClause> getToEpmObjectMapper() {
        return null; // TODO: refaire comme dans les module Administration et Passation après la fusion NOYAU-REDAC
    }

    @Override
    public EpmVClause findEntityById(final int id) {
        return safelyUniqueFind("id", id);
    }

    @Override
    public EpmVClause findByIdClausePublicationAndIdPublication(int idClausePublication, int idPublication) {
        return null;
    }


    @Override
    public ClauseBean map(EpmVClause epmVClause, EpmTRefOrganisme epmTRefOrganisme) {
        ClauseBean clauseBean = clauseMapper.toClauseBean(epmVClause);
        EpmTClause epmTClauseEditeurInitiale = null;
        EpmTClausePub epmTClausePub = epmVClause.getEpmTClausePub();
        if (epmTClausePub == null) {
            clauseBean.setTypeAuteur(1);
            clauseBean.setLabelTypeAuteur("Client");
        } else {
            Integer idClause = epmTClausePub.getIdClause();
            epmTClauseEditeurInitiale = clauseService.chercherObject(idClause, EpmTClause.class);

            EpmTClause clauseSurcharge = clauseService.chercherClauseSurcharge(idClause, epmTRefOrganisme);
            if (clauseSurcharge != null && Boolean.TRUE.equals(clauseSurcharge.getSurchargeActif())) {
                clauseBean.setTypeAuteur(3);
                clauseBean.setLabelTypeAuteur("Editeur Surchargé");
            } else {
                clauseBean.setTypeAuteur(2);
                clauseBean.setLabelTypeAuteur("Editeur");
            }
        }

        if (epmVClause.getIdLastPublication() != null && epmVClause.getIdLastPublication() != 0) {
            EpmTPublicationClausier publication = clauseService.chercherObject(epmVClause.getIdLastPublication(), EpmTPublicationClausier.class);
            clauseBean.setLastVersion(publication.getVersion());
        } else {
            clauseBean.setLastVersion("NA");
        }

        if (epmTClauseEditeurInitiale != null) {
            clauseBean.setReferenceClauseSurchargee(epmTClauseEditeurInitiale.getReferenceClauseSurchargee());
            clauseBean.setSurchargeActif(epmTClauseEditeurInitiale.getSurchargeActif());
        }
        clauseBean.setContext(makeContext(epmVClause));
        clauseBean.setContextHtml(makeContextHtml(epmVClause));
        return clauseBean;
    }

    @Override
    public EpmVClause map(ClauseBean clauseBean, EpmTRefOrganisme epmTRefOrganisme) {
        ClauseViewCritere ClauseViewCritere = new ClauseViewCritere(epmTRefOrganisme.getPlateformeUuid());
        ClauseViewCritere.setIdClause(clauseBean.getIdClause());
        ClauseViewCritere.setIdPublication(clauseBean.getIdPublication());
        EpmVClause epmVClause = safelyUniqueFind(ClauseViewCritere);
        clauseMapper.toEpmVClause(clauseBean, epmVClause);
        return epmVClause;
    }

    @Override
    public PageRepresentation<ClauseBean> findClauses(ClauseSearch search, EpmTRefOrganisme epmTRefOrganisme) {
        ResultList<ClauseBean, ? extends ClauseViewFacade, ClauseViewCritere> resultList = new ResultList<>(this, new ClauseViewCritere(epmTRefOrganisme.getPlateformeUuid()));


        resultList.critere().setReference(search.getReferenceClause());
        resultList.critere().setIdOrganisme(epmTRefOrganisme.getId());
        resultList.critere().setNonAfficherSurcharge(true);
        String referenceCanevas = search.getReferenceCanevas();

        if (null != referenceCanevas && !referenceCanevas.isEmpty()) {
            List<Integer> clausesId = clauseService.chercherListeClausesPourCanevasMinisterielle(referenceCanevas, epmTRefOrganisme.getId())
                    .stream().map(EpmTClause::getId).distinct().collect(Collectors.toList());

            LOG.info("Id des clauses pour le canevas {} = {}", referenceCanevas, clausesId);

            resultList.critere().setListIds(clausesId.isEmpty() ? Collections.singletonList(0) : clausesId);
        } else {
            resultList.critere().setListIds(null);
        }
        resultList.critere().setNaturePrestation(search.getIdNaturePrestation());
        resultList.critere().setMotscles(search.getMotsCles());
        resultList.critere().setAuteur(search.getTypeAuteur());
        resultList.critere().setIdStatutRedactionClausier(search.getIdStatutRedactionClausier());
        resultList.critere().setIdTypeClause(search.getIdTypeClause());
        resultList.critere().setIdTypeDocument(search.getIdTypeDocument());
        resultList.critere().setIdTypeContrat(search.getIdTypeContrat());
        resultList.critere().setProcedure(search.getIdProcedure());
        resultList.critere().setTheme(search.getIdThemeClause());
        resultList.critere().setActif(search.getActif());
        resultList.critere().setDateModificationDebut(search.getDateModificationMin());
        resultList.critere().setDateModificationFin(search.getDateModificationMax());

        resultList.critere().setParametrableDirection(search.getParametrableDirection());
        resultList.critere().setParametrableAgent(search.getParametrableAgent());
        resultList.critere().setProprieteTriee(search.getSortField());
        resultList.critere().setTriCroissant(search.isAsc());
        resultList.critere().setNumeroPage(search.getPage());
        resultList.critere().setTaillePage(search.getSize());
        resultList.critere().setChercherNombreResultatTotal(true);

        ResultList<ClauseBean, ? extends ClauseViewFacade, ClauseViewCritere> clauseBeans = resultList.find(epmTRefOrganisme);
        //pour chaque clause si surcharge active on recupere la clause surchargee pour afficher le contexte surchargé
        clauseBeans.getListResults().forEach(clauseBean -> {
            if (clauseBean.getSurchargeActif()) {
                EpmTClause clauseSurcharge = clauseService.chercherClauseSurcharge(clauseBean.getIdClause(), epmTRefOrganisme);
                if (clauseSurcharge != null) {
                    clauseBean.setContext((makeContext(clauseSurcharge)));
                }
            }
        });

        PageRepresentation<ClauseBean> representation = new PageRepresentation<>();
        representation.setContent(clauseBeans.getListResults());
        representation.setFirst(clauseBeans.pageCurrent() == 1);
        representation.setLast(clauseBeans.pageCurrent() == clauseBeans.getCountPages());
        representation.setNumberOfElements(clauseBeans.listResults().size());
        representation.setTotalPages(clauseBeans.countPages());
        representation.setTotalElements(clauseBeans.getCount());
        representation.setSize(search.getSize());
        representation.setNumber(search.getPage());
        return representation;

    }

    @Autowired
    public void setClauseService(ClauseService clauseService) {
        this.clauseService = clauseService;
    }

    @Autowired
    public void setClauseMapper(ClauseMapper clauseMapper) {
        this.clauseMapper = clauseMapper;
    }

}
