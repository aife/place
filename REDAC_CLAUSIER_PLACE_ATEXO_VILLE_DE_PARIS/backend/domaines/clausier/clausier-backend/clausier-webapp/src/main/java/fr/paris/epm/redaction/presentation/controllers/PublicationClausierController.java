package fr.paris.epm.redaction.presentation.controllers;

import fr.paris.epm.global.coordination.facade.DirectoryFacade;
import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.coordination.facade.PublicationClausierFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
@SessionAttributes({"searchInstance", "resultList", "listLayout", /* sont utilise dans {@link fr.paris.epm.global.presentation.controllers.NavigateController} */
        "listPublicationClausier", "publication", "allGestionLocale", "file"})
public class PublicationClausierController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(PublicationClausierController.class);

    @Resource
    private PublicationClausierFacade publicationClausierFacade;

    @Resource
    private ReferentielsServiceSecurise referentielsServiceSecurise;

    @Resource
    private DirectoryFacade directoryFacade;

    @RequestMapping(value = "/importPublicationsClausier")
    public final String importPublicationsClausier(@RequestParam("fileImport") final MultipartFile multipartFile, final RedirectAttributes redirectAttributes, HttpServletRequest request) {
        log.info("/importPublicationsClausier.json");
        try {
            EpmTUtilisateur utilisateur = (EpmTUtilisateur) request.getAttribute("utilisateur");
            List<String> errors = publicationClausierFacade.importPublicationClausier(multipartFile.getInputStream(), utilisateur);
            if (errors.size() > 0) {
                Message msg = new Message(TypeMessage.ERROR, "Erreurs");
                msg.addErrors(errors);
                redirectAttributes.addFlashAttribute("message", msg);
            } else {
                redirectAttributes.addFlashAttribute("message", messageSucces("publicationClausier.importPublicationClausier.succes"));
            }
        } catch (IOException e) {
            log.error("Erreur lors de la recuperation du fichier clausier depuis le front:" + e.getMessage(), e);
            redirectAttributes.addFlashAttribute("message", messageError("publicationClausier.importPublicationClausier.erreur.upload"));
        } catch (Exception e) {
            log.error("Erreur technique dans le process de l'import de clausier :" + e.getMessage(), e);
            redirectAttributes.addFlashAttribute("message", messageError());
        }
        return "redirect:/listPublicationsClausier.htm";
    }

}
