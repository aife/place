package fr.paris.epm.redaction.presentation.bean;

import fr.paris.epm.global.coordination.bean.AbstractBean;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class BaseBean extends AbstractBean {

    private Boolean surchargeActif = false;
    private int typeAuteur;

    private Integer idPublication;

    private String referenceClauseSurchargee;
    private Integer idClauseOrigine;

    /**
     * le statut du canevas.
     */
    private Boolean actif;
    private Boolean checked;

    /**
     * l'attribut etat.
     */
    private int etat;

    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModification;

    private int idStatutRedactionClausier;

    private int idNaturePrestation;

    public int getIdNaturePrestation() {
        return idNaturePrestation;
    }

    public void setIdNaturePrestation(int idNaturePrestation) {
        this.idNaturePrestation = idNaturePrestation;
    }

    public int getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }

    public void setIdStatutRedactionClausier(int idStatutRedactionClausier) {
        this.idStatutRedactionClausier = idStatutRedactionClausier;
    }


    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }
    public int getTypeAuteur() {
        return typeAuteur;
    }

    public void setTypeAuteur(int typeAuteur) {
        this.typeAuteur = typeAuteur;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Boolean getSurchargeActif() {
        return surchargeActif;
    }

    public void setSurchargeActif(Boolean surchargeActif) {
        this.surchargeActif = surchargeActif;
    }

    public String getReferenceClauseSurchargee() {
        return referenceClauseSurchargee;
    }

    public void setReferenceClauseSurchargee(String referenceClauseSurchargee) {
        this.referenceClauseSurchargee = referenceClauseSurchargee;
    }

    public Integer getIdClauseOrigine() {
        return idClauseOrigine;
    }

    public void setIdClauseOrigine(Integer idClauseOrigine) {
        this.idClauseOrigine = idClauseOrigine;
    }
}
