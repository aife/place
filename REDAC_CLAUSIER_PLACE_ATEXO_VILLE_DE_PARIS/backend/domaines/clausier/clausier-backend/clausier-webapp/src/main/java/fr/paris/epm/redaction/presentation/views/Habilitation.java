package fr.paris.epm.redaction.presentation.views;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefThemeHabilitation;

import java.util.List;

/**
 * EpmTHabilitation pour la persistence des habilitations.
 *
 * @author Cheikh Diop, Guillaume Béraudo
 * @version $Revision: 96 $, $Date: 2007-07-30 $, $Author: DIOP $
 */
public class Habilitation {

    private int id;

    private List<String> roles;


    private String libelle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
