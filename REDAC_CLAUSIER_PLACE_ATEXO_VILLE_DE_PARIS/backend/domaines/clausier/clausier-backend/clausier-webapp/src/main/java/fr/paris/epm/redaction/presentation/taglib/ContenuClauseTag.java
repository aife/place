/**
 * 
 */
package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag affichant le contenu d'une clause sous forme de : [Texte fixe
 * avant][formulations/texte prévaliorisé ou hérié][Texte fixe après].
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ContenuClauseTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * emplacement de la variable (request, session, parameter).
     */
    private String scope;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ContenuClauseTag.class);

    /**
     * injecté par spring.
     */
    private static ClauseFacadeGWT clauseFacadeGWT = null;

    /**
     * @return int
     */
    public int doStartTag() throws JspException {
        StringBuffer resultat = new StringBuffer("");
        // Object objetClause = tagUtils.lookup(pageContext, name, scope);
        // if (objetClause != null) {
        // try {
        // // String contenu = clauseFacade
        // // .getContenuClause(epmTClauseInstance);
        // // if (contenu != null) {
        // // resultat.append(contenu.replaceAll("\n", "<br/>"));
        // // }
        // } catch (NonTrouveException e) {
        // LOG.fatal(e, e.fillInStackTrace());
        // throw new JspException("Erreur d'E/S", e);
        // } catch (TechnicalException e) {
        // LOG.fatal(e, e.fillInStackTrace());
        // throw new JspException("Erreur d'E/S", e);
        // }
        // }
        try {
            pageContext.getOut().print(resultat.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        name = null;
        scope = null;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }

    /**
     * @param valeur emplacement de la variable (request, session, parameter)
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }

    /**
     * @param valeur injecté par spring
     */
    public final void setClauseFacadeGWT(final ClauseFacadeGWT valeur) {
        ContenuClauseTag.clauseFacadeGWT = valeur;
    }

}
