package fr.paris.epm.redaction.presentation.controllers;

import org.springframework.stereotype.Controller;

/**
 * Controlleur Abstrait de gestion des taches longues
 * Created by nty on 28/06/19.
 * @author Nikolay Tyurin
 * @author nty
 */
@Controller
public class WorkerController extends fr.paris.epm.global.presentation.controllers.WorkerController {

    /*
    TODO: c'est une copie de WorkerController de fr.paris.epm.global.presentation.controllers qui n'est pas visible d'ici
    */

}
