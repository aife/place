package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.redaction.commun.Constante;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Formulaire clause texte libre .
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseTexteLibreForm {
    /**
     * Marqueur de sérialisation.
     */

    private static final long serialVersionUID = 1L;
    /**
     * Attribut texte Fixe Avant.
     */

    private String textFixeAvant;
    /**
     * Attribut taille champ.
     */

    private String nbCaract;

    /**
     * Attribut precochee.
     */

    private String precochee;
    /**
     * Attribut texte Fixe Apres.
     */

    private String textFixeApres;
    /**
     * Attribut champ Libre Obligatoire.
     */

    private Integer tailleChamps;
    private String champLibreObligatoire;
    private boolean obligatoire;
    /**
     * Attribut saut Texte Fixe Avant.
     */

    private String sautTextFixeAvant;
    /**
     * Attribut saut Texte Fixe Apres.
     */

    private String sautTextFixeApres;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    /**
     * Constructeur de la classe ClauseTexteLibreForm.
     */
    public ClauseTexteLibreForm() {
        reset();
    }

    public Integer getTailleChamps() {
        return tailleChamps;
    }

    public void setTailleChamps(Integer tailleChamps) {
        this.tailleChamps = tailleChamps;
    }

    /**
     * Cette méthode permet d'initialiser le formulaire.
     */
    public void reset() {
        champLibreObligatoire = "1";
        obligatoire = true;
        textFixeAvant = null;
        nbCaract = null;
        textFixeApres = null;
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
        precochee = "";
    }

    /**
     * @return texte Fixe Avant
     */
    public final String getTextFixeAvant() {
        return textFixeAvant;
    }

    /**
     * @param valeur initialise l'attribut textFixeAvant.
     */
    public final void setTextFixeAvant(final String valeur) {
        textFixeAvant = valeur;
    }

    /**
     * @return taille champ
     */
    public final String getNbCaract() {
        return nbCaract;
    }

    /**
     * @param valeur initialise l'attribut taille du champ.
     */
    public final void setNbCaract(final String valeur) {
        nbCaract = valeur;
    }

    /**
     * @return texte Fixe Apres
     */
    public final String getTextFixeApres() {
        return textFixeApres;
    }

    /**
     * @param valeur initialise l'attribut textFixeApres.
     */
    public final void setTextFixeApres(final String valeur) {
        textFixeApres = valeur;
    }

    /**
     * @return champ Libre Obligatoire
     */
    public final String getChampLibreObligatoire() {
        return champLibreObligatoire;
    }

    /**
     * @param valeur initialise l'attribut champ Libre Obligatoire.
     */
    public final void setChampLibreObligatoire(final String valeur) {
        champLibreObligatoire = valeur;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(boolean obligatoire) {
        this.obligatoire = obligatoire;
    }

    /**
     * @return precochee
     */
    public final String getPrecochee() {
        return precochee;
    }

    /**
     * @param valeur initialise l'attribut precochee.
     */
    public final void setPrecochee(final String valeur) {
        precochee = valeur;
    }

    /* methode validate() pour valider les données du  formulaire.
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(final ActionMapping mapping,
                                 final HttpServletRequest request) {

        ActionErrors erreurs = new ActionErrors();

        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    /**
     * @return saut Texte Fixe Avant
     */
    public final String getSautTextFixeAvant() {
        return sautTextFixeAvant;
    }

    /**
     * @param valeur positionne l'attribut sautTextFixeAvant.
     */
    public final void setSautTextFixeAvant(final String valeur) {
        sautTextFixeAvant = valeur;
    }

    /**
     * @return saut Texte Fixe Apres
     */
    public final String getSautTextFixeApres() {
        return sautTextFixeApres;
    }

    /**
     * @param valeur initialise sautTextFixeApres.
     */
    public final void setSautTextFixeApres(final String valeur) {
        sautTextFixeApres = valeur;
    }

    /**
     * @return the clauseSelectionnee
     */
    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    /**
     * @param clauseSelectionnee the clauseSelectionnee to set
     */
    public final void setClauseSelectionnee(final String valeur) {
        this.clauseSelectionnee = valeur;
    }

}
