package fr.paris.epm.redaction.presentation.views;

public class Service {

	private Service parent;

	private Organisme organisme;

	private Number identifiantExterne;

	private String libelle;

	private String codeExterne;

	public Service getParent() {
		return parent;
	}

	public void setParent( Service parent ) {
		this.parent = parent;
	}

	public Organisme getOrganisme() {
		return organisme;
	}

	public void setOrganisme( Organisme organisme ) {
		this.organisme = organisme;
	}

	public Number getIdentifiantExterne() {
		return identifiantExterne;
	}

	public void setIdentifiantExterne( Number identifiantExterne ) {
		this.identifiantExterne = identifiantExterne;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}

	public String getCodeExterne() {
		return codeExterne;
	}

	public void setCodeExterne( String codeExterne ) {
		this.codeExterne = codeExterne;
	}
}
