CREATE TABLE epm__t_association_canevas_clause
( 
   id_canevas integer, 
   id_clause integer,
   CONSTRAINT epm__fk_canevas FOREIGN KEY (id_canevas) REFERENCES epm__t_canevas (id),
   CONSTRAINT epm__fk_clause FOREIGN KEY (id_clause) REFERENCES epm__t_clause (id)
) 
WITH (
  OIDS = FALSE
)
;