<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 23/11/17
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="atexo" uri="AtexoTag" %>

<div class="form-bloc">
    <div class="top"><span class="left"></span><span class="right"></span></div>
    <div class="content">
        <h2 class="float-left">
            <html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee" value="clauseEditeur"></html:radio>
            <label for="clauseEditeurSelectionnee"><bean:message key="clause.surcharge.clauseEditeur"/></label>
        </h2>

        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')">
                    <img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique">
                </a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>

        <div class="line">
            <span class="intitule-bloc"><bean:message key="ClauseValeurHeritee.txt.texteFixeAvant"/></span>
            <textarea id="textFixeAvantEditeur" class="texte-long mceEditorSansToolbar" cols="" rows="6">
                ${ClauseValeurHeriteeFormEditeur.textFixeAvant}
            </textarea>
        </div>

        <div class="line">
            <div class="retour-ligne">
                <input type="checkbox" id="sautTextFixeAvant" checked="${ClauseValeurHeriteeFormEditeur.sautTextFixeAvant}" disabled="disabled" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>

        <div class="line">
            <span class="intitule"><bean:message key="ClauseValeurHeritee.txt.referenceValeurHeritee"/></span>

            <select id="refValHeriteeEditeur" class="auto" title="Référence de la valeur héritée" disabled="disabled">
                <c:forEach items="${ClauseValeurHeriteeFormEditeur.listRefValeursTypeClause}" var="refValHeriteeOption">
                    <c:choose>
                        <c:when test="${ClauseValeurHeriteeFormEditeur.refValeurTypeClause == refValHeriteeOption.id}">
                            <option selected="selected">${refValHeriteeOption.libelle}</option>
                        </c:when>
                        <c:otherwise>
                            <option>${refValHeriteeOption.libelle}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
        </div>

        <div class="line">
            <div class="retour-ligne">
                <input type="checkbox" id="sautTextFixeApres" checked="${ClauseValeurHeriteeFormEditeur.sautTextFixeApres}" disabled="disabled" title="Saut de ligne"/>
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>

        <div class="line">
            <span class="intitule-bloc"><bean:message key="ClauseValeurHeritee.txt.texteFixeApres"/></span>
            <textarea id="textFixeApresEditeur" class="texte-long mceEditorSansToolbar" cols="" rows="6">
                ${ClauseValeurHeriteeFormEditeur.textFixeApres}
            </textarea>
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')">
                    <img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique">
                </a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>
<script type="text/javascript">
    initEditeursTexteSansToolbarRedaction();
</script>