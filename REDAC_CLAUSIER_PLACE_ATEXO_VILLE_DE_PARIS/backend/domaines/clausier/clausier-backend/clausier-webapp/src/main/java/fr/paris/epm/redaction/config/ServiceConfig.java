package fr.paris.epm.redaction.config;

import fr.paris.epm.redaction.coordination.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration-class contient tous les Services à injecter
 * Created by nty on 24/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Configuration
//@EnableTransactionManagement
public class ServiceConfig {

    @Bean(name = "generiqueService")
    public GeneriqueService generiqueService() {
        return new GeneriqueService();
    }

    @Bean(name = "clauseService")
    public ClauseService clauseService() {
        return new ClauseServiceImpl();
    }

    @Bean(name = "canevasService")
    public CanevasService canevasService() {
        return new CanevasServiceImpl();
    }

}