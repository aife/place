package fr.paris.epm.redaction.importExport.bean;

public class ReferentielImportExport {

	private int id;

	private String codeExterne;

    private String libelle;

    private boolean actif;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

	public String getCodeExterne() {
		return codeExterne;
	}

	public void setCodeExterne( String codeExterne ) {
		this.codeExterne = codeExterne;
	}
}
