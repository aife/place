package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.redaction.commun.Constante;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;

/**
 * formulaire de ClauseTexteFixe.
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseTexteFixeForm {
    /**
     * Marqueur de sérialisation.
     */

    private static final long serialVersionUID = 1L;
    /**
     * Attribut texte Fixe.
     */
    private String texteFixe;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    /**
     * Constructeur de la classe.
     */
    public ClauseTexteFixeForm() {
        reset();
    }

    /**
     * Initialiser le formulaire ClauseTexteFixe.
     */
    public void reset() {
        texteFixe = "";
    }

    /**
     * @return texte fixe
     */
    public final String getTexteFixe() {
        return texteFixe;
    }

    /**
     * @param valeur positionne l'attribut texteFixe.
     */
    public final void setTexteFixe(final String valeur) {
        texteFixe = valeur;
    }

    /**
     * methode validate () pour valider le formulaire.
     *
     * @see org.apache.struts.action.ActionForm#validate(
     *org.apache.struts.action.ActionMapping,
     * javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(final ActionMapping mapping,
                                 final HttpServletRequest request) {

        ActionErrors erreurs = new ActionErrors();

        if (texteFixe == null || texteFixe.trim().equals("")) {
            erreurs.add("texteFixe", new ActionMessage(
                    "creationClause.valider.texteFixe"));
        }
        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    /**
     * @return the clauseSelectionnee
     */
    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    /**
     * @param clauseSelectionnee the clauseSelectionnee to set
     */
    public final void setClauseSelectionnee(final String valeur) {
        this.clauseSelectionnee = valeur;
    }
}
