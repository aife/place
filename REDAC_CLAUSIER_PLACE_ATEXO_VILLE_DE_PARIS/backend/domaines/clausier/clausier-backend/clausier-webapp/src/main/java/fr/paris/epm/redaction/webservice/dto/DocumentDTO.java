package fr.paris.epm.redaction.webservice.dto;

import java.util.UUID;

public class DocumentDTO {

	private Long id;

	private AuditDTO creation;

	private AuditDTO modification;

	private Long taille;

	private ModeleDTO modele;

	private UtilisateurDTO creePar;

	private UtilisateurDTO modifiePar;

	private ContexteDTO contexte;

	private String nom;

	private String extension;

	private UUID uuid;

	private boolean actif;

	private Integer version;

	private Double progression = 0d;

	private boolean ouvert;

	private String commentaire;

	public DocumentDTO() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public AuditDTO getCreation() {
		return this.creation;
	}

	public void setCreation(final AuditDTO creation) {
		this.creation = creation;
	}

	public AuditDTO getModification() {
		return this.modification;
	}

	public void setModification(final AuditDTO modification) {
		this.modification = modification;
	}

	public Long getTaille() {
		return this.taille;
	}

	public void setTaille(final Long taille) {
		this.taille = taille;
	}

	public ModeleDTO getModele() {
		return this.modele;
	}

	public void setModele(final ModeleDTO modele) {
		this.modele = modele;
	}

	public UtilisateurDTO getCreePar() {
		return this.creePar;
	}

	public void setCreePar(final UtilisateurDTO creePar) {
		this.creePar = creePar;
	}

	public UtilisateurDTO getModifiePar() {
		return this.modifiePar;
	}

	public void setModifiePar(final UtilisateurDTO modifiePar) {
		this.modifiePar = modifiePar;
	}

	public ContexteDTO getContexte() {
		return this.contexte;
	}

	public void setContexte(final ContexteDTO contexte) {
		this.contexte = contexte;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(final String nom) {
		this.nom = nom;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(final String extension) {
		this.extension = extension;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public void setUuid(final UUID uuid) {
		this.uuid = uuid;
	}

	public boolean isActif() {
		return this.actif;
	}

	public void setActif(final boolean actif) {
		this.actif = actif;
	}

	public Integer getVersion() {
		return this.version;
	}

	public void setVersion(final Integer version) {
		this.version = version;
	}

	public Double getProgression() {
		return this.progression;
	}

	public void setProgression(final Double progression) {
		this.progression = progression;
	}

	public boolean isOuvert() {
		return this.ouvert;
	}

	public void setOuvert(final boolean ouvert) {
		this.ouvert = ouvert;
	}

	public String getCommentaire() {
		return this.commentaire;
	}

	public void setCommentaire(final String commentaire) {
		this.commentaire = commentaire;
	}

}
