package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.redaction.BaseEpmTRefRedaction;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefPotentiellementConditionnee;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeStructureSociale;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PotentiellementConditionneeServiceImpl implements PotentiellementConditionneeService {

    private final String LABEL_SOUCATEG_PREFIX = "Considération sociale:";
    private final String LABEL_RESERVE_A_PREFIX = "Le marché ou lot est réservé à";

    enum CATEGORIE {
        EXECUTIION("Considération sociale:clause sociale comme condition d'exécution", 1), SPECIFICATION_TECHNIQUE("Considération sociale:clause sociale comme spécification technique (ou exigences fonctionnelles)", 3),
        CRITERE_ATTRIBUTION("Considération sociale:critère social comme critère d'attribution du marché", 4), RESERVE_A("Considération sociale:marché public réservé", 2), UNKOWN("inconnu", 0), MARCHE_RESERVE_A("Le marché ou lot est réservé à", 2);
        private int id;
        private String Label;

        CATEGORIE(String label, int id) {
            this.id = id;
            Label = label;
        }

        public int getId() {
            return id;
        }

        public static CATEGORIE fromLabel(String label) {
            for (CATEGORIE c : CATEGORIE.values()) {
                if (c.Label.equals(label))
                    return c;
            }
            return UNKOWN;
        }
    }

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PotentiellementConditionneeServiceImpl.class);

    private ReferentielsServiceSecurise referentielsService;

    /**
     * @param id l'idetifiant de l'objet dans la base
     * @return
     * @throws TechnicalException
     */
    public final EpmTRefPotentiellementConditionnee charger(final int id) {
        return referentielsService.chercherObject(id, EpmTRefPotentiellementConditionnee.class);
    }

    @Override
    public final List<PotentiellementConditionneeValeur> getListValeurById(final int id, final Integer idOrganisme) {
        return this.getPotentiellementConditionneeValeurs(idOrganisme, this.charger(id));
    }

    public final List lister(Integer idOrganisme) {

        List<EpmTRefPotentiellementConditionnee> epmtPotConditionnees =
                referentielsService.getAllReferentiels().getRefPotentiellementConditionnees().stream()
                        .filter(BaseEpmTRefRedaction::isActif)
                        .collect(Collectors.toList());

        LOG.debug("{} criteres potentiellement conditionnés. Liste = {}", epmtPotConditionnees.size(), epmtPotConditionnees);

        List resultats = new ArrayList();

        for (EpmTRefPotentiellementConditionnee epmtPotConditionnee : epmtPotConditionnees) {
            PotentiellementConditionnee potConditionnee = new PotentiellementConditionnee();
            potConditionnee.setLibelle(epmtPotConditionnee.getLibelle());
            potConditionnee.setId(epmtPotConditionnee.getId());
            potConditionnee.setMultiChoix(epmtPotConditionnee.isMultiChoix());
            List<PotentiellementConditionneeValeur> list = getPotentiellementConditionneeValeurs(idOrganisme, epmtPotConditionnee);
            potConditionnee.setValeurs(list);
            resultats.add(potConditionnee);
        }
        Collections.sort(resultats);

        return resultats;
    }

    private List<PotentiellementConditionneeValeur> getPotentiellementConditionneeValeurs(Integer idOrganisme, EpmTRefPotentiellementConditionnee epmtPotConditionnee) {
        List<PotentiellementConditionneeValeur> list = new ArrayList<>();
        if (epmtPotConditionnee.isCritereBooleen()) {
            PotentiellementConditionneeValeur oui = new PotentiellementConditionneeValeur();
            oui.setLibelle("Non");
            oui.setId(1);
            list.add(oui);
            PotentiellementConditionneeValeur non = new PotentiellementConditionneeValeur();
            non.setLibelle("Oui");
            non.setId(2);
            list.add(non);
        } else {

            if (epmtPotConditionnee.isProcedurePassation()) {
                list = getProceduresPassationParOrganisme(idOrganisme);
            } else {
                list = getListValeurs(epmtPotConditionnee);
            }

            if (!CollectionUtils.isEmpty(list)) {
                if (epmtPotConditionnee.getLibelle().startsWith(LABEL_SOUCATEG_PREFIX) || epmtPotConditionnee.getLibelle().equals(LABEL_RESERVE_A_PREFIX)) {
                    //tri speciale pour les potentiellemnts conditionnées adapté pour le dev achat responsable
                    Collections.sort(list, new StructureSocialesComparator());
                } else {
                    Collections.sort(list);
                }
            }
        }
        return list;
    }

    public List<PotentiellementConditionneeValeur> getListValeurs(EpmTRefPotentiellementConditionnee epmtPotConditionnee) throws TechnicalException {
        List<PotentiellementConditionneeValeur> resultList =
                (List) getReferentielsSelectCollection(referentielsService.getAllReferentiels(), epmtPotConditionnee);
        return resultList;
    }

    /*
     * (non-Javadoc)
     * @see fr.paris.epm.redaction.metier.RefPotentiellementConditionneeGIM#lister()
     */
    public List<PotentiellementConditionneeValeur> getProceduresPassationParOrganisme(Integer idOrganisme) throws TechnicalException {
        List<PotentiellementConditionneeValeur> resultList = new ArrayList<>();
        List<EpmTRefProcedure> listProcedures;
        if (idOrganisme != null) {
            listProcedures = referentielsService.getProcedureByIdOrganisme(idOrganisme);
        } else {
            listProcedures = referentielsService.getAllProcedures();
        }
        // ---------
        for (EpmTRefProcedure refProcedure : listProcedures) {
            PotentiellementConditionneeValeur potentiellementConditionee = new PotentiellementConditionneeValeur();
            potentiellementConditionee.setId(refProcedure.getId());
            potentiellementConditionee.setLibelle(refProcedure.getLibelle());
            resultList.add(potentiellementConditionee);
        }
        return resultList;
    }

    /**
     * Récupérer la valeur d'un référentiel distant.
     *
     * @param objet               l'objet appelant la méthode
     * @param epmtPotConditionnee le pottentiellement conditionnée
     * @return liste
     * @throws TechnicalException erreur technique
     */
    private final Object getReferentielsSelectCollection(final Object objet,
                                                         final EpmTRefPotentiellementConditionnee epmtPotConditionnee)
            throws TechnicalNoyauException {
        if (epmtPotConditionnee.getMethodeLibelle() != null) {
            String[] methodesLibelle = epmtPotConditionnee.getMethodeLibelle().split("\\.");
            String[] methodesId = epmtPotConditionnee.getMethodeId().split("\\.");
            Object resultat = objet;
            for (int i = 0; i < methodesLibelle.length; i++) {
                String libelle = Util.creationMethode(methodesLibelle[i]);
                resultat = Util.lancerMethode(resultat, null, libelle);
                if (resultat != null) {
                    if (resultat instanceof List) {
                        if (epmtPotConditionnee.getLibelle().startsWith(LABEL_SOUCATEG_PREFIX) || epmtPotConditionnee.getLibelle().equals(LABEL_RESERVE_A_PREFIX)) {
                            resultat = filtrerValeusSouCategSociales((List<EpmTRefTypeStructureSociale>) resultat, epmtPotConditionnee.getLibelle());
                        }
                        List list = (List) resultat;
                        return creerListPotentiellementConditionneValeur(list, methodesLibelle[i + 1], methodesId[i + 1]);
                    }
                }
            }
        }
        return null;
    }

    private List filtrerValeusSouCategSociales(List<EpmTRefTypeStructureSociale> resultat, String potcdtLabel) {
        int idCategorieSociale = CATEGORIE.fromLabel(potcdtLabel).getId();
        return resultat.stream().filter(e -> e.getEpmTRefClauseSociale().getId() == idCategorieSociale).collect(Collectors.toList());
    }

    /**
     * @param list
     * @param methodeLibelle
     * @param methodId
     * @return
     * @throws TechnicalException
     */
    private List<PotentiellementConditionneeValeur> creerListPotentiellementConditionneValeur(List list, String methodeLibelle, String methodId) {
        List<PotentiellementConditionneeValeur> resultList = new ArrayList<PotentiellementConditionneeValeur>();
        String libelle = "";

        for (int j = 0; j < list.size(); j++) {
            libelle = Util.creationMethode(methodeLibelle);
            Object libelleObjet = Util.lancerMethode(list.get(j), null, libelle);
            if (libelle != null && libelle instanceof String) {
                PotentiellementConditionneeValeur valeur = new PotentiellementConditionneeValeur();
                valeur.setLibelle((String) libelleObjet);
                String id = Util.creationMethode(methodId);
                Object idObjet = Util.lancerMethode(list.get(j), null, id);

                if (idObjet != null && idObjet instanceof Integer) {
                    valeur.setId(((Integer) idObjet).intValue());
                }
                resultList.add(valeur);
            }
        }
        return resultList;
    }

    public class StructureSocialesComparator implements Comparator<PotentiellementConditionneeValeur> {
        static final String ESAT_EA = "Entreprise addaptées (EA), établissements et services d'aide par le travail (ESAT) ou structures équivalentes";
        static final String SIAE = "Structures d'insertion par l'activité économique (SIAE) ou structures équivalentes";
        static final String EESS = "Entreprises de l'économie socilae et solidaire (EESS) ou structures équivalentes";
        static final String IAE = "Insertion par l’activité économique";
        static final String CFS = "Clause sociale de formation sous statut scolaire au bénéfice de jeunes en situation de décrochage scolaire";
        static final String LCD = "Lutte contre les discriminations : Egalité femmes/hommes, etc";
        static final String CE = "Commerce équitable";
        static final String AETS = "Achats éthiques, traçabilité sociale des services/fournitures, etc";
        static final String ACS = "Autre(s) clause(s) sociale(s)";

        @Override
        public int compare(PotentiellementConditionneeValeur o1, PotentiellementConditionneeValeur o2) {
            return getOrder(o1).compareTo(getOrder(o2));
        }

        private Integer getOrder(PotentiellementConditionneeValeur structureSociale) {
            switch (structureSociale.getLibelle()) {
                case ESAT_EA:
                    return 1;
                case SIAE:
                    return 2;
                case EESS:
                    return 3;
                case IAE:
                    return 4;
                case CFS:
                    return 5;
                case LCD:
                    return 6;
                case CE:
                    return 7;
                case AETS:
                    return 8;
                case ACS:
                    return 9;
                default:
                    return 0;
            }
        }

    }

    public final void setReferentielsService(final ReferentielsServiceSecurise referentielsService) {
        this.referentielsService = referentielsService;
    }

}
