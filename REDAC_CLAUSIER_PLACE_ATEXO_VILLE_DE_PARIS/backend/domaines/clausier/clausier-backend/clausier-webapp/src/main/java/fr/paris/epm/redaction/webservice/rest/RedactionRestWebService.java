package fr.paris.epm.redaction.webservice.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.technique.OauthAccessTokenHolder;
import fr.paris.epm.noyau.service.technique.RestService;
import fr.paris.epm.redaction.coordination.DocumentRedactionFacade;
import fr.paris.epm.redaction.util.ResponseError;
import fr.paris.epm.redaction.webservice.beans.DocumentBean;
import fr.paris.epm.redaction.webservice.beans.DocumentWSBean;
import fr.paris.epm.redaction.webservice.dto.ContexteDTO;
import fr.paris.epm.redaction.webservice.dto.DocumentDTO;
import fr.paris.epm.redaction.webservice.dto.EnvironnementDTO;
import fr.paris.epm.redaction.webservice.rest.constantes.ConstantesRestWebService.ErreurEnum;
import fr.paris.epm.redaction.webservice.rest.constantes.ConstantesRestWebService.StatutDocumentEnum;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import javax.xml.ws.http.HTTPException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Web Service REST pour la récupération des documents
 *
 * @author MGA
 */
@Path("/redaction")
@Consumes("application/xml")
public class RedactionRestWebService {

	private static final Logger LOG = LoggerFactory.getLogger(RedactionRestWebService.class);

	private static DocumentRedactionFacade documentRedactionFacade;

	private static RedactionServiceSecurise redactionService;

	private static String plateforme;

	private static String client;

	private static String rechercheContexteUrl;

	private static String rechercheEnvironnementUrl;

	private static String listeDocumentsUrl;

	private static String telechargementDocumentUrl;

	private static String telechargementFichierUrl;

	private static OauthAccessTokenHolder oauthAccessTokenHolder;

	private static RestService restService;

	/**
	 * Méthode qui permet de télécharger un document depuis le module Redac
	 *
	 * @param id l'identifiant du document à télécharger
	 */
	@GET
	@Path("/telechargement/fichier")
	public Response recuperer(@QueryParam("identifiantDocument") final Long id) {

		Response result;
		String jeton;

		DocumentDTO documentDTO;
		try {
			jeton = oauthAccessTokenHolder.get().orElseGet(()->oauthAccessTokenHolder.renew().getAccessToken());

			documentDTO = telechargerDocument(jeton, id).orElseThrow(() -> new HTTPException(HttpStatus.NOT_FOUND.value()));
		} catch ( final HttpClientErrorException e ) {
			if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				LOG.info("Erreur HTTP 401 - unauthorized - lors de l'obtention du document. Nouvelle tentative");

				jeton = oauthAccessTokenHolder.renew().getAccessToken();

			    documentDTO = telechargerDocument(jeton, id).orElseThrow(() -> new HTTPException(HttpStatus.NOT_FOUND.value()));
			} else {
				throw new HTTPException(e.getStatusCode().value());
			}
		}

		final var resource = telechargerFichier(jeton, documentDTO).orElseThrow(()-> new HTTPException(HttpStatus.NOT_FOUND.value()));

		try {
			result = Response.ok(resource.getInputStream(), MediaType.APPLICATION_OCTET_STREAM)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename = \"" + documentDTO.getNom() + "\"").build();
		} catch ( final IOException e ) {
			LOG.error("Impossible d'obtenir le fichier", e);

			throw new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}

		return result;
	}

	/**
	 * Méthode qui permet de récupérer la liste des documents d'une consultation en XML
	 * <documents>
	 * <document>
	 * <identifiant>id</identifiant>
	 * <nom>nom</nom>
	 * <statut>statut</statut>
	 * <titre>titre</titre>
	 * <taille>taille</taille>
	 * </document>
	 * </documents>
	 *
	 * @param identifiantConsultation l'id de la consultation
	 * @param ticket le ticket d'authentification
	 */
	@GET
	@Path("/document")
	@Produces(MediaType.APPLICATION_XML)
	public Response getListeDocuments( @QueryParam("identifiantConsultation") final String identifiantConsultation, @QueryParam("ticket") final String ticket, @Context UriInfo uriInfo) {
		LOG.info("Recherche de document VALIDES pour la consultation '{}' et le ticket '{}' a l'url '{}'", identifiantConsultation, ticket,  uriInfo.getRequestUri().getQuery());

		String jeton = oauthAccessTokenHolder.get().orElseGet(()->oauthAccessTokenHolder.renew().getAccessToken());

		if ( null == jeton ) {
			throw new HTTPException(HttpStatus.UNAUTHORIZED.value());
		}

		final DocumentWSBean documentWSBean = new DocumentWSBean();
		List<DocumentDTO> documentsDTO;

		try {
			var environnement = this.rechercherEnvironnement(jeton, client, plateforme);

			var contexte = this.rechercherContexte(jeton, environnement, identifiantConsultation);

			if (null == contexte) {
				throw new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value());
			}

			documentsDTO = recupererDocumentsValides(jeton, environnement, contexte);
		} catch ( HttpClientErrorException exception ) {
			throw new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}

		documentWSBean.setDocuments(documentsDTO.stream().map(
			documentDTO -> {
				final DocumentBean documentBean = new DocumentBean();

				documentBean.setNom(documentDTO.getNom() + "." + documentDTO.getExtension());
				documentBean.setTitre(documentDTO.getNom());
				documentBean.setIdentifiant(documentDTO.getId());
				documentBean.setTaille(documentDTO.getTaille());
				documentBean.setStatut(StatutDocumentEnum.VALIDER);
				documentBean.setType(documentDTO.getModele().getType().getCode());

				return documentBean;
			}
		).collect(Collectors.toList()));

		return ecrireReponse(documentWSBean);
	}

	private Response ecrireReponse( DocumentWSBean documentWSBean ) {
		Response result;
		try {
			String output = new XmlMapper()
			    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
				//.writerWithDefaultPrettyPrinter()
				.writeValueAsString(documentWSBean);

			result = Response.ok(MessageFormat.format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>{0}", output)).build();
		} catch ( JsonProcessingException e ) {
			throw new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return result;
	}

	private ContexteDTO rechercherContexte( final String jeton, final EnvironnementDTO environnement, final String reference ) {
		var url = MessageFormat.format(rechercheContexteUrl, environnement.getUuid(), reference);

		LOG.info("Recherche du contexte pour l'environnement {} et la consultation {} depuis l'URL = {}", environnement, reference, url);

		// header keycloak
		final var headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
		headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");

		final var response = restService.getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(headers), ContexteDTO.class);

		return response.getBody();
	}

	private List<DocumentDTO> recupererDocumentsValides( final String jeton, final EnvironnementDTO environnementDTO, final ContexteDTO contexteDTO ) {
		var url = MessageFormat.format(listeDocumentsUrl, environnementDTO.getUuid(), contexteDTO.getUuid());

		LOG.info("Recuperation des documents pour le contexte {} depuis l'URL = {}", contexteDTO.getUuid(), url);

		// header keycloak
		final var headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
		headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");

		final var response = restService.getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<List<DocumentDTO>>() {
		});

		var result = response.getBody();

		if (null!=result) {
			result = result.stream().sorted(Comparator.comparing(d -> d.getModification().getDate())).collect(Collectors.toList());
		}

		return result;
	}

	private Optional<DocumentDTO> telechargerDocument( final String jeton, final Long id ) {
		String url = MessageFormat.format(telechargementDocumentUrl, id);

		LOG.info("Téléchargement du document {} depuis l'URL = {}", id, url);

		DocumentDTO result = null;
		// header keycloak
		final var headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
		headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");

		try {
			final var response = restService.getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(headers), DocumentDTO.class);

			result = response.getBody();
		} catch ( final HttpClientErrorException exception ) {
			LOG.error("Impossible de récupérer le document {}", id);
		}

		return Optional.ofNullable(result);
	}

	private Optional<Resource> telechargerFichier( final String jeton, final DocumentDTO documentDTO ) {
		String url = MessageFormat.format(telechargementFichierUrl, documentDTO.getId());

		LOG.info("Téléchargement du fichier pour l'id {} depuis l'URL = {}", documentDTO.getId(), url);

		Resource result = null;
		// header keycloak
		final var headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
		headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");

		try {
			final var response = restService.getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(headers), Resource.class);

			result = response.getBody();
		} catch ( final HttpClientErrorException exception ) {
			LOG.error("Impossible de recuperer le fichier pour l'UUID {}", documentDTO.getUuid());
		}

		return Optional.ofNullable(result);
	}

	private EnvironnementDTO rechercherEnvironnement( final String jeton, final String client, final String plateforme ) {
		var url = MessageFormat.format(rechercheEnvironnementUrl, client, plateforme);

		LOG.info("Recherche de l'environnement pour le client {} et la plateforme {} depuis l'URL = {}", client, plateforme, url);

		// header keycloak
		final var headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
		headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");

		final var response = restService.getRestTemplate().exchange(url, HttpMethod.GET, new HttpEntity<>(headers), EnvironnementDTO.class);

		return response.getBody();
	}

	/**
	 * Défini une nouvelle erreur
	 *
	 * @param error le type erreurM
	 * @param message le message
	 *
	 * @return les elements XML
	 */
	public static Response error( final ErreurEnum error, final String message ) {
		final ResponseError bean = new ResponseError();
		bean.setCodeMessage(error, message);
		final GenericEntity<String> genericEntity = new GenericEntity<>(bean.toString()) {
		};
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
			.entity(genericEntity)
			.type(MediaType.APPLICATION_XML)
			.build();
	}

	public void setDocumentRedactionFacade( final DocumentRedactionFacade valeur ) {
		RedactionRestWebService.documentRedactionFacade = valeur;
	}

	public void setRedactionService( final RedactionServiceSecurise redactionService ) {
		RedactionRestWebService.redactionService = redactionService;
	}

	public String getPlateforme() {
		return plateforme;
	}

	public void setPlateforme( String plateforme ) {
		RedactionRestWebService.plateforme = plateforme;
	}

	public String getClient() {
		return client;
	}

	public void setClient( String client ) {
		RedactionRestWebService.client = client;
	}

	public String getListeDocumentsUrl() {
		return listeDocumentsUrl;
	}

	public void setListeDocumentsUrl( String listeDocumentsUrl ) {
		RedactionRestWebService.listeDocumentsUrl = listeDocumentsUrl;
	}

	public String getRechercheContexteUrl() {
		return rechercheContexteUrl;
	}

	public void setRechercheContexteUrl( String rechercheContexteUrl ) {
		RedactionRestWebService.rechercheContexteUrl = rechercheContexteUrl;
	}

	public String getRechercheEnvironnementUrl() {
		return rechercheEnvironnementUrl;
	}

	public void setRechercheEnvironnementUrl( String rechercheEnvironnementUrl ) {
		RedactionRestWebService.rechercheEnvironnementUrl = rechercheEnvironnementUrl;
	}

	public String getTelechargementDocumentUrl() {
		return telechargementDocumentUrl;
	}

	public void setTelechargementDocumentUrl( String telechargementDocumentUrl ) {
		RedactionRestWebService.telechargementDocumentUrl = telechargementDocumentUrl;
	}

	public String getTelechargementFichierUrl() {
		return telechargementFichierUrl;
	}

	public void setTelechargementFichierUrl( String telechargementFichierUrl ) {
		RedactionRestWebService.telechargementFichierUrl = telechargementFichierUrl;
	}

	public OauthAccessTokenHolder getOauthAccessTokenHolder() {
		return oauthAccessTokenHolder;
	}

	public void setOauthAccessTokenHolder( OauthAccessTokenHolder oauthAccessTokenHolder ) {
		RedactionRestWebService.oauthAccessTokenHolder = oauthAccessTokenHolder;
	}

	public void setRestService( RestService restService ) {
		RedactionRestWebService.restService = restService;
	}


}
