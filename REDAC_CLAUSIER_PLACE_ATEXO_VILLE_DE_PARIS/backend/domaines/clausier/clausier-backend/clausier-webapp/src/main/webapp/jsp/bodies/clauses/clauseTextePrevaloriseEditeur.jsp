<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--Debut bloc Infos clause-->
<div class="form-saisie">
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>
		<div class="content">
			<h2 class="float-left">
				<html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee" value="clauseEditeur"></html:radio>
				<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeur"/></label>
			</h2>
			<div class="actions-clause no-padding">
				<logic:equal name="typeAction" value="M">
<%-- 					<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">				 --%>
						<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 					</c:if> --%>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
<!-- 		<div class="info-maj"> -->
<%-- 		<bean:message key="clause.surcharge.modifieeLe"/> --%>
<%-- 			<c:out value="TODO" /> --%>
<!-- 		</div> -->
			<div class="line">
				<span class="intitule-bloc"><bean:message
						key="ClauseTextePrevaloriser.txt.texteFixeAvant" /> </span>
					<textarea disabled="disabled" class="texte-long mceEditorSansToolbar" rows="4" cols=""
						title="Texte fixe avant" id="textFixeAvantEditeur"
						name="textFixeAvantEditeur">${clauseTextePrevaloriseFormEditeur.textFixeAvant}</textarea>
					
			</div>
			<div class="line">
			<div class="retour-ligne">
					
				<input type="checkbox" id="sautTextFixeAvantEditeur" <c:if test="${clauseTextePrevaloriseFormEditeur.sautTextFixeAvant == 'true'}">checked="checked"</c:if> 
					 disabled="disabled" title="Saut de ligne"/>	
					
				<bean:message key="redaction.txt.sautLigne" />
				<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
					title="Saut de ligne" />
			</div>
		</div>
		<div class="separator"></div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.tailleChamp" /> 
				</span>
			 <c:set var="nbCaractValueEditeur" value="1"/>
			 <c:if test="${not empty(clauseTextePrevaloriseFormEditeur.nbCaract)}">
				<c:set var="nbCaractValueEditeur" value="${clauseTextePrevaloriseFormEditeur.nbCaract}"/>
			 </c:if>
			 	<select disabled="disabled" name="tailleChamp" id="nbCaractEditeur" class="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'editeur_valeurDefaut_', 'mceEditorSansToolbar');">
					<option value="4" <c:if test="${nbCaractValueEditeur == '4'}">selected="selected"</c:if>>
		               <bean:message key="redaction.taille.champ.tresLong" />
		            </option>
		            <option value="1" <c:if test="${nbCaractValueEditeur == '1'}">selected="selected"</c:if>>
		               <bean:message key="redaction.taille.champ.long" />
		            </option>
		            <option value="2" <c:if test="${nbCaractValueEditeur == '2'}">selected="selected"</c:if>>
		               <bean:message key="redaction.taille.champ.moyen" />
		            </option>
		            <option value="3" <c:if test="${nbCaractValueEditeur == '3'}">selected="selected"</c:if>>
		               <bean:message key="redaction.taille.champ.court" />
		            </option>
         		</select>
         		
			</div>
			
			<c:if test="${nbCaractValueEditeur == '0'}">
				<script>
					mettreTailleChampValeurDefaut('nbCaractEditeur');
				</script>
		    </c:if>
			
			
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTextePrevaloriser.txt.valeurParDefaut" /></span>
					<c:set var="styleDisplayTresLongEditeur" value="none"/>
					<c:set var="styleDisplayLongEditeur" value="none"/>
					<c:set var="styleDisplayMoyenEditeur" value="none"/>
					<c:set var="styleDisplayCourtEditeur" value="none"/>
					<c:choose>
						<c:when test="${clauseTextePrevaloriseFormEditeur.nbCaract == 4}">
							<c:set var="styleDisplayTresLongEditeur" value="block"/>
						</c:when>
						<c:when test="${clauseTextePrevaloriseFormEditeur.nbCaract == 2}">
							<c:set var="styleDisplayMoyenEditeur" value="block"/>
						</c:when>
						<c:when test="${clauseTextePrevaloriseFormEditeur.nbCaract == 3}"> 
							<c:set var="styleDisplayCourtEditeur" value="block"/>
						</c:when>
						<c:otherwise>
							<c:set var="styleDisplayLongEditeur" value="block"/>
						</c:otherwise>
					</c:choose>
				    <textarea title="Valeur par defaut" class="texte-tres-long mceEditorSansToolbar" id="editeur_valeurDefaut_4"  cols="" rows="6" style="display:${styleDisplayTresLongEditeur}">${clauseTextePrevaloriseFormEditeur.valeurDefautTresLong}</textarea>
				    <textarea title="Valeur par defaut" class="texte-long mceEditorSansToolbar" id="editeur_valeurDefaut_1"  cols="" rows="6" style="display:${styleDisplayLongEditeur}">${clauseTextePrevaloriseFormEditeur.defaultValue}</textarea>
                    <input type="text" disabled="disabled" title="Valeur par defaut" class="texte-long" id="editeur_valeurDefaut_2"  style="display:${styleDisplayMoyenEditeur}" value="<c:out value="${clauseTextePrevaloriseFormEditeur.valeurDefautMoyen}"/>"  />
                    <input type="text" disabled="disabled" title="Valeur par defaut" class="texte-court" id="editeur_valeurDefaut_3" style="display:${styleDisplayCourtEditeur}" value="<c:out value="${clauseTextePrevaloriseFormEditeur.valeurDefautCourt}"/>"  />
                    
			</div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirection" /> </span>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" title="Oui" id="parametrableDirectionEditeur" name="parametrableDirectionEditeur" 
						<c:if test="${clauseTextePrevaloriseFormEditeur.parametrableDirection == '1'}">checked="checked"</c:if>/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirectionOui" />
				</div>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" title="Oui" id="parametrableDirectionEditeur" name="parametrableDirectionEditeur" 
						<c:if test="${clauseTextePrevaloriseFormEditeur.parametrableDirection == '0'}">checked="checked"</c:if>/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirectionNon" />
				</div>
			</div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgent" /> </span>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" title="Oui" id="parametrableAgentEditeur" name="parametrableAgentEditeur" 
						<c:if test="${clauseTextePrevaloriseFormEditeur.parametrableAgent == '1'}">checked="checked"</c:if>/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgentOui" />
				</div>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" title="Non" id="parametrableAgentEditeur" name="parametrableAgentEditeur" 
						<c:if test="${clauseTextePrevaloriseFormEditeur.parametrableAgent == '0'}">checked="checked"</c:if>/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgentNon" />
				</div>
			</div>
			<div class="line">
			<div class="retour-ligne">
					
				<input type="checkbox" id="sautTextFixeApresEditeur" 
					<c:if test="${clauseTextePrevaloriseFormEditeur.sautTextFixeAvant == 'true'}">checked="checked"</c:if> disabled="disabled" title="Saut de ligne"/>
					
				<bean:message key="redaction.txt.sautLigne" />
				<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
					title="Saut de ligne" />
			</div>
		</div>
		<div class="separator"></div>
			<div class="line">
				<span class="intitule-bloc"><bean:message
						key="ClauseTextePrevaloriser.txt.texteFixeApres" /> </span>

					<textarea disabled="disabled" class="texte-long mceEditorSansToolbar" rows="4" cols=""
						title="Texte fixe avant" id="textFixeApresEditeur"
						name="textFixeApresEditeur">${clauseTextePrevaloriseFormEditeur.textFixeApres}</textarea>
			</div>
			
			<div class="actions-clause">
				<logic:equal name="typeAction" value="M">
<%-- 					<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
						<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 					</c:if> --%>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
			
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span><span class="right"></span>
		</div>
	</div>
</div>
<div class="spacer"></div>
<script type="text/javascript">
	initEditeursTexteSansToolbarRedaction();
</script>