package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.persistance.redaction.*;

import java.util.List;

/**
 * Interface qui expose les méthodes pour récupérer les valeurs d'un
 * référentiel.
 *
 * @author Ramli Tarik
 */
public interface ReferentielFacade {

    EpmTRefTypeDocument getTypeDocumentParId(final int id) throws TechnicalException;


    EpmTRefValeurTypeClause getValeurTypeClauseById(final int id) throws TechnicalException;

    List chargerTousPotentiellementConditionne(Integer idOrganisme) throws TechnicalException;

    List<EpmTRefTypeDocument> chargerTousTypesDocument(final boolean avecTous, Boolean actif) throws TechnicalException;


    Integer getIdProcedurePassationCorrespondantDansAutreOrganisme(Integer idProcedurePassation, Integer idOrganisme) throws TechnicalException;

    EpmTRefPotentiellementConditionnee getPotentiellementConditionneeParId(final int id) throws TechnicalException;

}
