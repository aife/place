package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.ResultList;
import fr.paris.epm.global.commun.tags.HrefTag;
import fr.paris.epm.global.coordination.facade.AbstractMapperFacade;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.noyau.metier.redaction.PublicationClausierCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausierFile;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;
import fr.paris.epm.redaction.coordination.exception.ActivationPublicationException;
import fr.paris.epm.redaction.coordination.service.ImportExportService;
import fr.paris.epm.redaction.coordination.service.PublicationClausierService;
import fr.paris.epm.redaction.importExport.bean.CanevasImportExport;
import fr.paris.epm.redaction.importExport.bean.ClauseImportExport;
import fr.paris.epm.redaction.importExport.bean.ClausierImportExport;
import fr.paris.epm.redaction.importExport.mapper.PublicationClausierMapper;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierSearch;
import org.apache.commons.io.FileUtils;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@Service
public class PublicationClausierFacadeImpl extends AbstractMapperFacade<PublicationClausierBean, EpmTPublicationClausier> implements PublicationClausierFacade {

    private static final Logger LOG = LoggerFactory.getLogger(PublicationClausierFacadeImpl.class);

    private PublicationClausierMapper mapperPublication = Mappers.getMapper(PublicationClausierMapper.class);

    private PublicationClausierService publicationClausierService;

    private ImportExportService importExportService;

    private String editeurName;

    private String editeurUrl;

    @Override
    public GeneriqueServiceSecurise getService() {
        return publicationClausierService;
    }

    @Override
    protected boolean defaultRemoveTest(int id) {
        return true;
    }

    @Override
    public AbstractBeanToEpmObjectMapper<PublicationClausierBean, EpmTPublicationClausier> getToEpmObjectMapper() {
        return null; // TODO: refaire comme dans les module Administration et Passation après la fusion NOYAU-REDAC
    }

    @Override
    public PublicationClausierBean map(EpmTPublicationClausier epmTPublicationClausier, EpmTRefOrganisme epmTRefOrganisme) {
        return mapperPublication.fromPublicationBean(epmTPublicationClausier);
    }

    @Override
    public EpmTPublicationClausier map(PublicationClausierBean publicationClausierBean, EpmTRefOrganisme epmTRefOrganisme) {
        return mapperPublication.toEpmTPublication(publicationClausierBean);
    }


    @Override
    public PageRepresentation<PublicationClausierBean> findPublications(PublicationClausierSearch search, EpmTRefOrganisme epmTRefOrganisme) {
        ResultList<PublicationClausierBean, ? extends PublicationClausierFacade, PublicationClausierCritere> resultList = new ResultList<>(this, new PublicationClausierCritere(epmTRefOrganisme.getPlateformeUuid()));
        resultList.critere().setVersion(search.getVersion());
        resultList.critere().setDatePublicationMin(search.getDatePublicationMin());
        resultList.critere().setDatePublicationMax(search.getDatePubicationMax());
        resultList.critere().setGestionLocal(search.getGestionLocale());

        resultList.critere().setProprieteTriee(search.getSortField());
        resultList.critere().setTriCroissant(search.isAsc());
        resultList.critere().setNumeroPage(search.getPage());
        resultList.critere().setTaillePage(search.getSize());
        resultList.critere().setChercherNombreResultatTotal(true);
        ResultList<PublicationClausierBean, ? extends PublicationClausierFacade, PublicationClausierCritere> publicationsBean = resultList.find(epmTRefOrganisme);


        PageRepresentation<PublicationClausierBean> representation = new PageRepresentation<>();
        representation.setContent(publicationsBean.getListResults());
        representation.setFirst(publicationsBean.pageCurrent() == 1);
        representation.setLast(publicationsBean.pageCurrent() == publicationsBean.getCountPages());
        representation.setNumberOfElements(publicationsBean.listResults().size());
        representation.setTotalPages(publicationsBean.countPages());
        representation.setTotalElements(publicationsBean.getCount());
        representation.setSize(search.getSize());
        representation.setNumber(search.getPage());
        return representation;
    }


    @Override
    public EpmTPublicationClausier editCommentPublicationClausier(int idPublication, String commentaire) {
        EpmTPublicationClausier epmTPublicationClausier = publicationClausierService.chercherObject(idPublication, EpmTPublicationClausier.class);
        epmTPublicationClausier.setCommentaire(commentaire);
        return publicationClausierService.modifierEpmTObject(epmTPublicationClausier);
    }

    @Override
    public List<String> activePublication(int idPublication, EpmTUtilisateur utilisateur) {

        //si publication jamais activé, verification réferentiel suivi de l'ajout du clausier (canevas,clause,infoBulle...) après parsing des fichiers.
        if (!publicationClausierService.isAlreadyActivated(idPublication)) {
            //verifier  si le referentiel exporté est compatible avec la db destination
            List<String> errors = new ArrayList<>();
            ClausierImportExport clausierImportExport = importExportService.isValidReferentielFile(idPublication, errors);
            if (clausierImportExport == null) return errors;
            else importExportService.constituerClausierPub(clausierImportExport, idPublication, utilisateur);
        }

        //désactiver la publication actif
        publicationClausierService.desactivationAllPublication();

        //activer la publication désiré
        EpmTPublicationClausier epmTPublicationClausier = publicationClausierService.chercherObject(idPublication, EpmTPublicationClausier.class);
        epmTPublicationClausier.setActif(true);
        epmTPublicationClausier.setDateActivation(new Date());
        publicationClausierService.modifierEpmTObject(epmTPublicationClausier);
        return new ArrayList<>();
    }

    @Override
    public EpmTPublicationClausier activeClausierPublication(int idPublication, EpmTUtilisateur utilisateur) {
        if (publicationClausierService.canActivate(utilisateur.getIdOrganisme())) {
            try {


                ClausierImportExport clausierImportExport = null;
                //si publication jamais activé, verification réferentiel suivi de l'ajout du clausier (canevas,clause,infoBulle...) après parsing des fichiers.
                if (!publicationClausierService.isAlreadyActivated(idPublication)) {
                    //verifier  si le referentiel exporté est compatible avec la db destination
                    List<String> errors = new ArrayList<>();
                    clausierImportExport = importExportService.isValidReferentielFile(idPublication, errors);
                    if (clausierImportExport == null) throw new TechnicalException(String.join(",", errors));
                    else importExportService.constituerClausierPub(clausierImportExport, idPublication, utilisateur);
                }

                //désactiver la publication actif
                publicationClausierService.desactivationAllPublication();

                //activer la publication désiré
                EpmTPublicationClausier epmTPublicationClausier = publicationClausierService.chercherObject(idPublication, EpmTPublicationClausier.class);
                epmTPublicationClausier.setEnCoursActivation(true);
                epmTPublicationClausier = publicationClausierService.modifierEpmTObject(epmTPublicationClausier);
                epmTPublicationClausier.setDateActivation(new Date());
                epmTPublicationClausier.setEnCoursActivation(false);
                epmTPublicationClausier.setActif(true);
                if (epmTPublicationClausier.isGestionLocal()) {
                    try {
                        clausierImportExport = importExportService.isValidReferentielFile(idPublication, new ArrayList<>());
                        publicationClausierService.publicationClausierActivateTClause(idPublication, clausierImportExport.getClauses().stream().map(ClauseImportExport::getIdClause).collect(Collectors.toList()));
                        publicationClausierService.publicationClausierActivateTCanevas(idPublication, clausierImportExport.getCanevas().stream().map(CanevasImportExport::getIdCanevas).collect(Collectors.toList()));
                    } catch (Exception ex) {
                        epmTPublicationClausier.setActif(false);
                        publicationClausierService.modifierEpmTObject(epmTPublicationClausier);
                        throw new TechnicalException("Erreur pendant l'activation de la publication " + idPublication);
                    }

                }
                return publicationClausierService.modifierEpmTObject(epmTPublicationClausier);
            } catch (Exception e) {
                log.error("Erreur pendant l'activation de la publication " + idPublication, e);
                throw new ActivationPublicationException("Erreur pendant l'activation de la publication " + idPublication);
            }
        } else {
            throw new ActivationPublicationException("Impossible d'activer la publication, une publication est déjà en cours d'activation");
        }

    }

    @Override
    public long downloadPublicationClausier(List<Integer> idsPublications, OutputStream outputStream) {
        try {
            ZipOutputStream outputZip = new ZipOutputStream(outputStream);
            List<String> nomFichierEnregistre = new ArrayList<>();

            long length = 0;
            for (int idPublication : idsPublications) {
                EpmTPublicationClausier epmTPublicationClausier = publicationClausierService.chercherObject(idPublication, EpmTPublicationClausier.class);
                if (epmTPublicationClausier.getIdFileClausier() != null) {
                    EpmTPublicationClausierFile epmTClausierFile = publicationClausierService.chercherObject(epmTPublicationClausier.getIdFileClausier(), EpmTPublicationClausierFile.class);
                    File fichierClausierJson = new File("/tmp/" + epmTClausierFile.getFileName());
                    FileUtils.writeByteArrayToFile(fichierClausierJson, epmTClausierFile.getFileClausier());
                    putFileToZip(nomFichierEnregistre, fichierClausierJson, outputZip);
                    length += fichierClausierJson.length();
                }

                if (epmTPublicationClausier.getIdFileReferentiel() != null) {
                    EpmTPublicationClausierFile epmTReferentielFile = publicationClausierService.chercherObject(epmTPublicationClausier.getIdFileReferentiel(), EpmTPublicationClausierFile.class);
                    File fichierReferentielJson = new File("/tmp/" + epmTReferentielFile.getFileName());
                    FileUtils.writeByteArrayToFile(fichierReferentielJson, epmTReferentielFile.getFileClausier());
                    putFileToZip(nomFichierEnregistre, fichierReferentielJson, outputZip);
                    length += fichierReferentielJson.length();
                }
            }

            outputZip.finish();
            outputZip.close();
            return length;
        } catch (IOException e) {
            e.printStackTrace();
            throw new TechnicalException(e);
        }
    }

    private void putFileToZip(List<String> nomFichierEnregistre, File file, ZipOutputStream outputZip) throws IOException {
        nomFichierEnregistre.add(file.getName());
        int countFichierPresent = Collections.frequency(nomFichierEnregistre, file.getName());

        ZipEntry ze = null;
        if (countFichierPresent == 1) {
            ze = new ZipEntry(file.getName());
        } else {
            countFichierPresent = countFichierPresent - 1;
            String nomFichier = file.getName();
            String extention = file.getName();
            nomFichier = nomFichier.substring(0, nomFichier.lastIndexOf('.'));
            extention = extention.substring(extention.lastIndexOf('.') + 1);
            ze = new ZipEntry(nomFichier + "(" + countFichierPresent + ")" + extention);
        }

        FileInputStream input = new FileInputStream(file.getPath());

        outputZip.putNextEntry(ze);

        // buffer size
        byte[] b = new byte[1024];
        int count;
        while ((count = input.read(b)) > 0) outputZip.write(b, 0, count);
        outputZip.closeEntry();
        input.close();
    }

    /**
     * Créer une nouvelle version de la publication clausier
     *
     * @param publicationClausierBean publication clausier bean
     * @return null si elle est bien publiée sinon code d'erreur
     */
    @Override
    public String savePublicationClausier(PublicationClausierBean publicationClausierBean, EpmTUtilisateur utilisateur) {
        //check si pas de publication en cours d'activation
        if (publicationClausierService.canActivate(utilisateur.getIdOrganisme())) {
            LOG.debug("Publication du clausier");

            String version = publicationClausierBean.getVersion1() + "." + publicationClausierBean.getVersion2() + "." + publicationClausierBean.getVersion3();

            if (!publicationClausierService.isVersionNameUnique(version, utilisateur.getEpmTRefOrganisme().getPlateformeUuid()))
                return "publicationClausier.editPublicationClausier.erreur.version";
            publicationClausierService.desactivationAllPublication();

            publicationClausierBean.setVersion(version);
            publicationClausierBean.setActif(true);
            publicationClausierBean.setEnCoursActivation(true);
            publicationClausierBean.setDatePublication(new Date());
            publicationClausierBean.setDateActivation(new Date());
            publicationClausierBean.setDateIntegration(new Date());
            publicationClausierBean.setIdUtilisateur(utilisateur.getId());
            publicationClausierBean.setIdOrganisme(utilisateur.getIdOrganisme());
            publicationClausierBean.setNomCompletUtilisateur(utilisateur.getNomComplet());
            publicationClausierBean.setGestionLocal(true);

            if (editeurName.isEmpty() && editeurUrl.isEmpty()) {
                int dot = HrefTag.getHrefRacine().lastIndexOf("charte_graphique");
                publicationClausierBean.setEditeur(HrefTag.getHrefRacine().substring(0, dot));
            } else {
                publicationClausierBean.setEditeur(editeurName + "_" + editeurUrl);
            }
            publicationClausierBean = save(publicationClausierBean, utilisateur.getEpmTRefOrganisme());

            try {
                publicationClausierService.publicationClausierUpdateClausePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateRoleClausePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateClauseHasTypeContratPub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateClauseHasPotentiellementConditionneePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateCanevasPub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateChapitrePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateChapitreHasClausePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateCanevasHasProcedurePub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateCanevasHasTypeContratPub(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateClause(publicationClausierBean.getId());
                publicationClausierService.publicationClausierUpdateCanevas(publicationClausierBean.getId());
            } catch (Exception ex) {
                publicationClausierService.rollbackPublicationClausier(publicationClausierBean.getId());
                throw new ActivationPublicationException("Erreur pendant la publication du clausier " + publicationClausierBean.getId());
            }


            LOG.info("Debut du task de la generation des fichiers du clausier :" + publicationClausierBean.getVersion());

            //generation des fichiers de publication
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY_MM_dd_hh.mm.ss");
            String standardFileName = publicationClausierBean.getVersion() + "_" + sdf.format(new Date());
            File fileCla = importExportService.exporterClausier(publicationClausierBean.getId(), utilisateur.getEpmTRefOrganisme());
            File fileRef = importExportService.exporterReferentiel(publicationClausierBean.getId());

            EpmTPublicationClausierFile fichierClausier = new EpmTPublicationClausierFile();
            EpmTPublicationClausierFile fichierReferentiel = new EpmTPublicationClausierFile();
            fichierClausier.setFileName("CLA_" + standardFileName);
            fichierReferentiel.setFileName("REF_" + standardFileName);
            try {
                fichierClausier.setFileClausier(FileUtils.readFileToByteArray(fileCla));
                fichierReferentiel.setFileClausier(FileUtils.readFileToByteArray(fileRef));
            } catch (IOException e) {
                LOG.error("Pour la version : [" + publicationClausierBean.getVersion() + "], une erreur est survenue lors de l'enregistrement de fichier de publication du clausier en db : " + e.getMessage());
                throw new ActivationPublicationException("Pour la version : [" + publicationClausierBean.getVersion() + "], une erreur est survenue lors de l'enregistrement de fichier de publication du clausier en db : " + e.getMessage());
            }
            fichierClausier = publicationClausierService.modifierEpmTObject(fichierClausier);
            fichierReferentiel = publicationClausierService.modifierEpmTObject(fichierReferentiel);

            publicationClausierBean.setIdClausierFile(fichierClausier.getId());
            publicationClausierBean.setIdReferentielFile(fichierReferentiel.getId());
            publicationClausierBean.setEnCoursActivation(false);
            save(publicationClausierBean, utilisateur.getEpmTRefOrganisme());

            LOG.info("Fin avec succes du task de la generation des fichiers du clausier :" + publicationClausierBean.getVersion());
            return null;
        } else {
            throw new ActivationPublicationException("Impossible d'enregistrer la publication, une publication est déjà en cours d'activation");
        }

    }

    @Override
    public List<String> importPublicationClausier(InputStream file, EpmTUtilisateur utilisateur) {
        //premiere etape on fait l'extraction des fichiers de publication de l'archive
        List<File> publicationFichiers = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        try {
            File fichier = new File("/tmp/archivePublicationClausier");
            FileUtils.copyInputStreamToFile(file, fichier);
            ZipFile zipFile = new ZipFile(fichier);

            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry entry = entries.nextElement();
                File f = new File("/tmp/" + entry.getName());
                FileUtils.copyInputStreamToFile(zipFile.getInputStream(entry), f);
                publicationFichiers.add(f);
            }

        } catch (Exception e) {
            LOG.error("Une erreur est survenu lors de l'extraction des fichiers de l'archive de publication à importer:" + e.getMessage(), e);
            errors.add("Une erreur est survenue lors de l'extraction des fichiers de l'archive de publication à importer");
            return errors;
        }

        return importExportService.importerClausier(publicationFichiers, utilisateur);

    }

    @Override
    public String canActivatePublication(EpmTUtilisateur epmTUtilisateur) {
        boolean result = publicationClausierService.canActivate(epmTUtilisateur.getIdOrganisme());
        if (result) {
            return "true";
        } else {
            return "false";
        }

    }

    public void setPublicationClausierService(PublicationClausierService publicationClausierService) {
        this.publicationClausierService = publicationClausierService;
    }

    public void setImportExportService(ImportExportService importExportService) {
        this.importExportService = importExportService;
    }

    public void setEditeurName(String editeurName) {
        this.editeurName = editeurName;
    }

    public void setEditeurUrl(String editeurUrl) {
        this.editeurUrl = editeurUrl;
    }

}
