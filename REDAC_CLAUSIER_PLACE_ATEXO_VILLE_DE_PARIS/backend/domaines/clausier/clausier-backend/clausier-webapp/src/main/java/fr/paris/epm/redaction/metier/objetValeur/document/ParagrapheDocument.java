/**
 * 
 */
package fr.paris.epm.redaction.metier.objetValeur.document;

import java.io.Serializable;

/**
 * la classe ParagrapheDocument manipule les paragraphe d'un chapitre.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ParagrapheDocument implements Serializable {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * l'identifiant du paragraphe.
     */
    private int id;
    /**
     * le contenu du paragraphe.
     */
    private String texte;

    /**
     * cette méthode renvoie l'identifiant du paragraphe.
     * @return renvoyer l'identifiant du paragraphe.
     */
    public final int getId() {
        return id;
    }

    /**
     * cette méthode positionne l'identifiant du paragraphe.
     * @param valeur pour initialiser l'identifiant du paragraphe.
     */
    public final void setId(final int valeur) {
        id = valeur;
    }

    /**
     * cette méthode renvoie le contenu du paragraphe.
     * @return renvoyer le contenu du paragraphe.
     */
    public final String getTexte() {
        return texte;
    }

    /**
     * cette méthode positionne le contenu du paragraphe.
     * @param valeur pour initialiser le contenu du paragraphe.
     */
    public final void setTexte(final String valeur) {
        texte = valeur;
    }

}
