package fr.paris.epm.redaction.commun;

import fr.paris.epm.global.commons.exception.TechnicalException;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.StringTokenizer;

import static fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput;

/**
 * Classe des utilitaires commun à tout le projet redaction.
 * @author Ramli Tarik
 * @version $Revision$, $Date$, $Author$
 */
public class Util {

    /**
     * Logger de la classe.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Util.class);

    /**
     * La valeur de OUI.
     */
    public static final String OUI = "Oui";

    /**
     * La valeur de NON.
     */
    public static final String NON = "Non";

    /**
     * L'identifiant de la valeur tous dans le menu liste des lots.
     */
    public static final int ID_TOUS = 0;

    /**
     * L'intitulé de la valeur tous dans le menu liste des lots.
     */
    public static final String INTITULE_TOUS = "Tous";

    /**
     * L'identifiant de la valeur "Non applicable" dans le menu liste des lots.
     */
    public static final int ID_NON_APPLICABLE = -1;

    /**
     * Le libelle de la valeur "Non applicable" dans le menu liste des lots.
     */
    public static final String INTITULE_NON_APPLICABLE = "Non applicable";

    /**
     * Vérifie si la chaine "search" existe dans le tableau array.
     * @param search la chaine recherchée
     * @param array le tableau sur lequel on fait la recherche
     * @return boolean true ou false
     */
    public static boolean isInArray(final String search, final String[] array) {
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                if (array[i].compareTo(search) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Complète la chaine str par des '0' à gauche jusqu'à arriver à un maximum
     * définie dans maxChar.
     * @param str la chaine à complèter
     * @param maxChar le nombre maximal de caractère
     * @return la chaine complètée
     */
    public static String strPadding(final String str, final int maxChar) {
        String strPad = "";
        if (str.length() < maxChar) {
            for (int i = 0; i < (maxChar - str.length()); i++) {
                strPad = strPad.concat("0");
            }
        }
        return strPad.concat(str);
    }

    /**
     * Vérifie si value est entier ou pas .
     * @param value valeur à vérifier
     * @return boolean selon que value est entier ou non
     */
    public final boolean isInt(final String value) {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException pe) {
            return false;
        }
        return true;
    }

    /**
     * Permet de lancer la méthode "nomMethode" de l'objet "invoqueur" avec les
     * paramètres "args".
     * @param invoqueur l'objet sur lequel s'execute la méthode
     * @param args les argument de la méthode
     * @param nomMethode le nom de la méthode à invoquer
     * @return le resultat de l'execution de la méthode
     * @throws TechnicalException Erreur technique
     */
    public static Object lancerMethode(final Object invoqueur, final Object[] args, final String nomMethode) {
	    LOG.debug("Lancement de la méthode avec objet = {}, methode = {}, args = {}", invoqueur, nomMethode, args);
        if (invoqueur == null) {
            return null;
        }
        Class<?>[] paramTypes = null;
        if (args != null) {
            paramTypes = new Class[args.length];
            for (int i = 0; i < args.length; ++i) {
                if(args[i].getClass().equals(Integer.class)){
                    paramTypes[i] = int.class;
                }
                else{
                    paramTypes[i] = args[i].getClass();
                }
            }
        }
        Method methode;
        try {
	        LOG.debug("Récupération de la méthode depuis son nom {} et param {}", nomMethode, paramTypes);

	        methode = invoqueur.getClass().getMethod(nomMethode, paramTypes);
        } catch (SecurityException | NoSuchMethodException e) {
            throw new TechnicalException(e);
        }
        try {
	        LOG.trace("Invocation de la méthode depuis son nom {} et param {}", nomMethode, paramTypes);

            return methode.invoke(invoqueur, args);
        } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException e) {
            throw new TechnicalException(e);
        }
    }

    /**
     * Encode les caractères spéciaux.
     * @param chaine la chaine à encoder
     * @return la chaine encodée
     */
    public static String encodeCaractere(final String chaine) {
        String retour = chaine;
        if (chaine != null) {
            retour = retour.replace(Character.toString((char) 10), "");
            retour = retour.replace(Character.toString((char) 8217), "'");
            retour = retour
                    .replace(Character.toString((char) 8230), "...");
            retour = retour.replace(Character.toString((char) 164), "¤");
            retour = retour.replace(Character.toString((char) 339), "oe");
            retour = retour.replaceAll("ÂÂÂÂÂÂÂ¤", "¤");

        }
        return retour;
    }

    /**
     * décode les caractères spéciaux.
     * @param chaine la chaine à encoder
     * @return la chaine encodée
     */
    public static final String decodeCaractere(final String chaine) {
        String retour = sanitizeHtmlInput(chaine);
        if (chaine != null) {
            retour = retour.replaceAll("<ul>(<br[^<]*>)*<li>", "<ul><li>");
            retour = retour.replaceAll("</li>(<br[^<]*>)*<li>", "</li><li>");
            retour = retour.replaceAll("</li>(<br[^<]*>)*</ul>", "</li></ul>");
            retour = retour.replace(Character.toString((char) 10), "");
            retour = retour.replace((char) 8217, '\'');
            retour = StringEscapeUtils.unescapeHtml(retour);
        }
        return retour;
    }

    /**
     * Encode les caractères spéciaux.
     * @param chaine la chaine à encoder
     * @return la chaine encodée
     */
    public static String encodeXML(final String chaine) {
        String retour = chaine;
        if (chaine != null) {
            retour = retour.replace(Character.toString((char) 10), "");
            retour = retour.replace(Character.toString((char) 8217), "'");
            retour = retour.replace(Character.toString((char) 339), "oe");
        }
        return retour;
    }

    /**
     * Créer les méthodes getter à partir de l'attribut.
     * @param attribut l'attribut pour lequel on veut créer la méthode
     * @return le nom de la méthode
     */
    public static String creationMethode(final String attribut) {
        StringBuffer methode = new StringBuffer();
        methode.append("get");
        methode.append(attribut.substring(0, 1).toUpperCase());
        methode.append(attribut.substring(1));
        return methode.toString();
    }

    /**
     * Permet de tronquer une chaine de caractère.
     * @param chaine la chaine de caractère à tronquer
     * @param nombreCaractereMax nombre de caractère maximale
     * @return chaine tronquée
     */
    public static String tronquer(final String chaine, final int nombreCaractereMax) {
        if (chaine.trim().length() > nombreCaractereMax) {
            StringTokenizer contenu = new StringTokenizer(chaine.trim(),
                " ");
            StringBuffer contenuTronque = new StringBuffer();
            while (contenu.hasMoreElements()) {
                String morceau = (String) contenu.nextElement();
                if (contenuTronque.toString().trim().length() < nombreCaractereMax) {
                    if (contenuTronque.toString().trim().length()
                            + morceau.length() > nombreCaractereMax) {
                        break;
                    }
                    contenuTronque.append(" " + morceau);
                } else {
                    break;
                }
            }
            if (contenuTronque.toString().trim().length() < chaine.trim()
                    .length()) {
                contenuTronque.append("...");
            }
            return contenuTronque.toString().trim();
        } else {
            return chaine.trim();
        }
    }

    public static String remplaceTableauPourAffichage(String chaine) {
        if (chaine != null) {
            chaine = chaine.replaceAll(Constante.TABLEAU_VIRGULE, ",");
            chaine = chaine.replaceAll(Constante.TABLEAU_CROCHET_OUVERT,
                "[");
            chaine = chaine.replaceAll(Constante.TABLEAU_CROCHET_FERME,
                "]");
            return chaine.trim();
        } else {
            return null;
        }
    }

    public static String remplaceTableauPourSauvegarde(String chaine) {
        if (chaine != null) {
            chaine = chaine.replaceAll(",", Constante.TABLEAU_VIRGULE);
            chaine = chaine.replaceAll("\\[",
                Constante.TABLEAU_CROCHET_OUVERT);
            chaine = chaine.replaceAll("\\]",
                Constante.TABLEAU_CROCHET_FERME);
            return chaine;
        } else {
            return null;
        }
    }
    
    public static String getExtensionFichier(String nomFichier) {
        String ext = "";
        if (nomFichier != null && nomFichier.lastIndexOf('.') != -1) {
            ext = nomFichier.substring(nomFichier.lastIndexOf('.') + 1).toLowerCase();
        }
        return ext;
    }

    /**
     * @param strFichier le nom de fichier contenant l'extension qui permettra
     *            de retourner un résultat.
     * @return une chaine de caractères mise au niveau de setContentType pour la
     *         response.
     */
    public static String getTypeContenu(final String strFichier) {
        String extension = getExtension(strFichier).toLowerCase();
        switch (extension) {
            case "pdf": return "application/pdf";
            case "csv": return "application/vnd.ms-excel";
            case "xls": return "application/vnd.ms-excel";
            case "doc": return "application/msword";
            case "pps": return "application/vnd.ms-powerpoint";
            case "zip": return "multipart/zip";
            default: return "application/octet-stream";
        }
    }

    /**
     * @param strFichier fichier d'où il faut extraire l'extension
     * @return l'extension
     */
    @Deprecated
    public static String getExtension(final String strFichier) {
        if (strFichier.lastIndexOf(".") != -1) {
            if (strFichier.lastIndexOf(".") + 1 <= strFichier.length()) {
                if (LOG.isDebugEnabled())
                    LOG.debug(strFichier.substring(strFichier.lastIndexOf(".") + 1));
                return strFichier.substring(strFichier.lastIndexOf(".") + 1);
            }
        }
        return "";
    }

}
