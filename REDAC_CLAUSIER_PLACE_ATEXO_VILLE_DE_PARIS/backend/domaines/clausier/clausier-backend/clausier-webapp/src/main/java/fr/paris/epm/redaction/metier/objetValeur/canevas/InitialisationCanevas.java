package fr.paris.epm.redaction.metier.objetValeur.canevas;

import java.io.Serializable;
import java.util.List;

/**
 * Bean d'initialisation de l'écran de création/modification de canevas
 * @author MGA
 */
public class InitialisationCanevas implements Serializable {

    private static final long serialVersionUID = -4987265003989367805L;
    
    /**
     * L'objet canevas à traiter
     */
    private Canevas canevas;
    
    /**
     * La liste d'habilitations de l'utilisateur
     */
    private List<String> habilitationsUtilisateur;
    
    /**
     * Détermine si on est en mode editeur ou en mode client
     */
    private boolean modeEditeur;

    private boolean modeSaas;
    
    /**
     * Détermine si le module Entité Adjudicatrice est activé ou pas
     */
    private boolean compatibleEntiteAdjudicatriceActive;

    public Canevas getCanevas() {
        return canevas;
    }

    public void setCanevas(Canevas canevas) {
        this.canevas = canevas;
    }

    public List<String> getHabilitationsUtilisateur() {
        return habilitationsUtilisateur;
    }

    public void setHabilitationsUtilisateur(List<String> habilitationsUtilisateur) {
        this.habilitationsUtilisateur = habilitationsUtilisateur;
    }

    public boolean isModeEditeur() {
        return modeEditeur;
    }

    public void setModeEditeur(boolean modeEditeur) {
        this.modeEditeur = modeEditeur;
    }

    public boolean isModeSaas() {
        return modeSaas;
    }

    public void setModeSaas(boolean modeSaas) {
        this.modeSaas = modeSaas;
    }

    public boolean isCompatibleEntiteAdjudicatriceActive() {
        return compatibleEntiteAdjudicatriceActive;
    }

    public void setCompatibleEntiteAdjudicatriceActive(boolean compatibleEntiteAdjudicatriceActive) {
        this.compatibleEntiteAdjudicatriceActive = compatibleEntiteAdjudicatriceActive;
    }

}