package fr.paris.epm.redaction.presentation.controllers;

import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.presentation.views.Agent;
import fr.paris.epm.redaction.presentation.views.mappers.AgentViewMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/agents")
public class AgentRestController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(AgentRestController.class);

	@Resource
	private RedactionServiceSecurise redactionService;
	@Resource
	private ConsultationServiceSecurise consultationServiceSecurise;

	@Resource
	private AgentViewMapper agentViewMapper;

	@Value("${mpe.client}")
	private String mpePlateformeUuid;

	@Value("${mpe.client}")
	private String plateformeUuid;
	@GetMapping(value = "/rechercher")
	public final ResponseEntity<Set<Agent>> rechercher(
			@RequestParam(value = "consultation") final String reference,
			@RequestParam(value = "plateformeUuid", required = false) final String plateformeUuid
	) {
		final var critereConstultation = new ConsultationContexteCritere();
		critereConstultation.setReference(reference);
		if (null != plateformeUuid) {
			critereConstultation.setPlateforme(plateformeUuid);
		} else {
			critereConstultation.setPlateforme(mpePlateformeUuid);
		}
		final EpmTConsultation consultation = this.consultationServiceSecurise.chercherConsultation(critereConstultation);
		log.info("Consultation = {} ", consultation);

		final var organisme = this.redactionService.chercherObject(consultation.getIdOrganisme(), EpmTRefOrganisme.class);
		log.info("Organisme = {} ", organisme);

		final var services = this.recupererTousServices(consultation).stream().map(EpmTRefDirectionService::getId).collect(Collectors.toList());
		log.info("Liste des services = {} ", services);

		final var utilisateurCritere = new UtilisateurCritere(plateformeUuid);
		utilisateurCritere.setIdOrganisme(organisme.getId());
		utilisateurCritere.setIdDirServices(services);

		final var agents = this.redactionService.chercher(utilisateurCritere).stream().map(this.agentViewMapper::map).collect(Collectors.toSet());

		log.info("Liste des agents = {} ", agents);

		return ResponseEntity.ok(agents);
	}

	private List<EpmTRefDirectionService> recupererTousServices( final EpmTConsultation consultation ) {
		final List<EpmTRefDirectionService> result = new ArrayList<>();

		var directionService = consultation.getEpmTRefDirectionService();

		do {
			result.add(directionService);

			directionService = directionService.getParent();
		} while ( null != directionService );

		return result;
	}
}
