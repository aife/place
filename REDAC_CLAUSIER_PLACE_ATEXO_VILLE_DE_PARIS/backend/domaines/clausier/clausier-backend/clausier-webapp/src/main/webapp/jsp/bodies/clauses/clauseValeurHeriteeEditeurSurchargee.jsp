<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 23/11/17
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="atexo" uri="AtexoTag" %>

<div class="form-bloc">
    <div class="top"><span class="left"></span><span class="right"></span></div>
    <div class="content">
        <h2 class="float-left">
            <html:radio property="clauseSelectionnee" styleId="clauseEditeurSurchargeeSelectionnee" value="clauseEditeurSurcharge"></html:radio>
            <label for="clauseEditeurSurchargeeSelectionnee"><bean:message key="clause.surcharge.clauseEditeurSurchargee"/></label>
        </h2>

        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();">
                    <img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>">
                </a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>

        <div class="line">
            <span class="intitule-bloc"><bean:message key="ClauseValeurHeritee.txt.texteFixeAvant"/></span>
            <html:textarea property="textFixeAvant" title="Texte fixe avant" cols="" rows="6"
                           styleId="textFixeAvant" styleClass="texte-long mceEditor" errorStyleClass="error-border" />
        </div>
        
        <div class="line">
            <div class="retour-ligne">
                <html:checkbox property="sautTextFixeAvant" styleId="sautTextFixeAvant" value="true" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>

        <div class="line">
            <span class="intitule"><bean:message key="ClauseValeurHeritee.txt.referenceValeurHeritee"/></span>
            <html:select property="refValeurTypeClause" styleId="refValHeritee" styleClass="auto" title="Référence de la valeur héritée" errorStyleClass="error-border-auto">
                <html:option value="0"><bean:message key="redaction.txt.selectionner" /></html:option>
                <html:optionsCollection  property="listRefValeursTypeClause" label="libelle" value="id" />
            </html:select>
        </div>

        <div class="line">
            <div class="retour-ligne">
                <html:checkbox property="sautTextFixeApres" styleId="sautTextFixeApres" value="true" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>

        <div class="line">
            <span class="intitule-bloc"><bean:message key="ClauseValeurHeritee.txt.texteFixeApres"/></span>
            <html:textarea property="textFixeApres" title="Texte fixe après" cols="" rows="6"
                           styleId="textFixeApres" styleClass="texte-long mceEditor" errorStyleClass="error-border" />
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();">
                    <img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>">
                </a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>
<script type="text/javascript">
    initEditeursTexteRedactionSurchargeClauses();
</script>