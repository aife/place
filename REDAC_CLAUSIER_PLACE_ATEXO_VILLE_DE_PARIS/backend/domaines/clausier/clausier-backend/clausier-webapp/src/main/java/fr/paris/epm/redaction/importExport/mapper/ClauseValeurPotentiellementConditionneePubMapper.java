package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionneeAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionneePub;
import fr.paris.epm.redaction.importExport.bean.ClauseValeurPotentiellementConditionneeImportExport;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryNamedMapper;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {DirectoryMapper.class, DirectoryNamedMapper.class})
public abstract class ClauseValeurPotentiellementConditionneePubMapper {

    public abstract ClauseValeurPotentiellementConditionneeImportExport toClauseValeurPotentiellementConditionneeImportExport(
            EpmTClauseValeurPotentiellementConditionneePub epmTClauseValeurPotentiellementConditionneePub);

    public abstract EpmTClauseValeurPotentiellementConditionneePub toEpmTClauseValeurPotentiellementConditionneePub(
            ClauseValeurPotentiellementConditionneeImportExport clauseValeurPotentiellementConditionneeImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTClauseValeurPotentiellementConditionneePub setIdPublication(
            @MappingTarget EpmTClauseValeurPotentiellementConditionneePub epmTClauseValeurPotentiellementConditionneePub, @Context Integer idPublication) {
        epmTClauseValeurPotentiellementConditionneePub.setIdPublication(idPublication);
        return epmTClauseValeurPotentiellementConditionneePub;
    }

    protected Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> toEpmTClauseValeurPotentiellementConditionneeAbstracts(
            Set<ClauseValeurPotentiellementConditionneeImportExport> clauseValeurPotentiellementConditionneeImportExports, @Context Integer idPublication) {
        if (clauseValeurPotentiellementConditionneeImportExports == null)
            return null;

        Set<EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotentiellementConditionneeAbstracts = new HashSet<EpmTClauseValeurPotentiellementConditionneeAbstract>();
        for (ClauseValeurPotentiellementConditionneeImportExport clauseValeurPotentiellementConditionneeImportExport : clauseValeurPotentiellementConditionneeImportExports)
            epmTClauseValeurPotentiellementConditionneeAbstracts.add(toEpmTClauseValeurPotentiellementConditionneePub(clauseValeurPotentiellementConditionneeImportExport, idPublication));

        return epmTClauseValeurPotentiellementConditionneeAbstracts;
    }

}
