// JavaScript Document

/* Indique ou sont localiser les ressources (images, css) d'apres le fichier de configuration.
 * A affecter en debut de jsp avec le taglib <atexo:href. */

var hrefRacine = '<atexo:href href=""/>';

// Supprime les espaces inutiles en d�but et fin de la cha�ne pass�e en param�tre.
function trim(aString) {
    var regExpBeginning = /^\s+/;
    var regExpEnd = /\s+$/;
    return aString.replace(regExpBeginning, "").replace(regExpEnd, "");
}

function submitForm() {
    document.forms[0].submit();
}

function getIndexRow(elem) {
    var m = elem.rowIndex;
    return m;
}

function removeRowFromTable(row, idTable) {
    var tbl = document.getElementById(idTable);
    tbl.deleteRow(row);
}

function removeRowFromTableSurcharge(row, idTable) {
    var tbl = document.getElementById(idTable);
    tbl.deleteRow(row);
    tbl.deleteRow(row);
}

function vider_liste(id_liste) {
    var liste = document.getElementById(id_liste);
    var longueur = liste.options.length;
    for (var k = longueur - 1; k >= 0; k = k - 1) {
        liste.options[k] = null;
    }
}

function submitAction(action, input) {
    input.value = action;
    document.forms[0].submit();
}

function closeWin() {
    if (newWin != null) {
        if (!newWin.closed) {
            newWin.close();
        }
    }
}

function pagePrecedente() {
    var input = document.getElementById("nomMethode");
    input.value = "";
    history.back();
}

function initResultsTable() {
    var docType = document.getElementsByName("motsCles")[0].value;
    docType = replaceChars(docType, " et ", "+");
    docType = replaceChars(docType, "++", "");
    //document.getElementsByName("motsCles")[0].value = docType;
    if (docType.indexOf("++") != -1) {
        window.alert("Erreur de syntaxe dans les mots cles.");
        exit(0);
    }
    submitForm();
}

function AddRowTableCumulatif(idTable) {
    var nbrRows = compteur++; //document.getElementById(idTable).rows.length;
    var newRow = document.getElementById(idTable).insertRow(-1);
    if ((nbrRows % 2) == 0) {
        newRow.className = "on";
    }
    newRow.id = "tr_" + nbrRows;
    var newCell1 = newRow.insertCell(0);
    newCell1.className = "num-formulation";
    var input = "<input type=\"text\" name=\"numFormulation\" value=\"" + nbrRows + "\" title=\"Num\xe9ro de formulation\">";
    newCell1.innerHTML = input;
    var newCell2 = newRow.insertCell(1);
    newCell2.className = "nb-carac";
    var combo = "<select name=\"tailleChamp\" class=\"auto\" title=\"Taille du Champ\" onchange=\"javascript:displayOptionChoiceWithTinyMCE(this, 'valeurParDefaut" + nbrRows + "_', 'mceEditor');\"><option  value='4'>Tr\xe8s long</option><option  value='1' selected=\"true\">Long</option><option value='2'>Moyen</option><option value='3'>Court</option></select>";
    newCell2.innerHTML = combo;
    var newCell3 = newRow.insertCell(2);
    newCell3.className = "long-col";
    text = "<textarea   name=\"valeurDefautTresLong\"  title=\"Valeur par defaut\" class=\"texte-tres-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_4\"  cols=\"\" rows=\"6\" style=\"display:none\"></textarea>"
        + "<textarea   name=\"valeurDefaut\"  title=\"Valeur par defaut\" class=\"texte-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_1\"  cols=\"\" rows=\"6\" style=\"display:block\"></textarea>"
        + "<input type='text' name=\"valeurDefautCourt\"  title=\"Valeur par defaut\" class=\"texte-court\" id=\"valeurParDefaut" + nbrRows + "_3\" style=\"display:none\" />"
        + "<input type='text' name=\"valeurDefautMoyen\"  title=\"Valeur par defaut\" class=\"texte-moyen\" id=\"valeurParDefaut" + nbrRows + "_2\"  style=\"display:none\" />" + "</td>";
    newCell3.innerHTML = text;
    var newCell4 = newRow.insertCell(3);
    newCell4.className = "check-col";
    checkbox = "<input type=\"checkbox\" name=\"precochee\" value=\"" + (nbrRows - 1) + "\">";
    newCell4.innerHTML = checkbox;
    var newCell5 = newRow.insertCell(4);
    newCell5.className = "actions";
    var lien = "<a href=\"javascript:removeRowFromTable( document.getElementById('tr_" + nbrRows + "').rowIndex ,'table-formulation')\"><img src=\"" + hrefRacine + "images/picto-poubelle.gif\" alt=\"Supprimer la formulation\" title=\"Supprimer la formulation\" /></a>";
    newCell5.innerHTML = lien;

    tinyMCE.execCommand('mceAddControl', false, 'valeurParDefaut' + nbrRows + '_1');
    miseAJourTailleIFrame();
}

//Dans le cas de surcharge de clauses
function AddRowTableCumulatifSurcharge(idTable) {
    var nbrRows = compteur++; //document.getElementById(idTable).rows.length;
    var newRow = document.getElementById(idTable).insertRow(-1);
    if ((nbrRows % 2) == 0) {
        newRow.className = "on";
    }
    newRow.id = "tr_" + nbrRows;
    var newCell1 = newRow.insertCell(0);
    newCell1.className = "num-formulation";
    var input = "<input type=\"text\" name=\"numFormulation\" value=\"" + nbrRows + "\" title=\"Num\xe9ro de formulation\">";
    newCell1.innerHTML = input;
    var newCell2 = newRow.insertCell(1);
    newCell2.className = "nb-carac";
    var combo = "<select name=\"tailleChamp\" class=\"auto\" title=\"Taille du Champ\" onchange=\"javascript:displayOptionChoiceWithTinyMCE(this, 'valeurParDefaut" + nbrRows + "_', 'mceEditor');\"><option  value='4'>Tr\xe8s long</option><option  value='1' selected=\"true\">Long</option><option value='2'>Moyen</option><option value='3'>Court</option></select>";
    newCell2.innerHTML = combo;
    var newCell3 = newRow.insertCell(2);
    newCell3.className = "check-col";
    checkbox = "<input type=\"checkbox\" name=\"precochee\" value=\"" + (nbrRows - 1) + "\">";
    newCell3.innerHTML = checkbox;
    var newCell4 = newRow.insertCell(3);
    newCell4.className = "actions";
    var lien = "<a href=\"javascript:removeRowFromTableSurcharge( document.getElementById('tr_" + nbrRows + "').rowIndex ,'table-formulation')\"><img src=\"" + hrefRacine + "images/picto-poubelle.gif\" alt=\"Supprimer la formulation\" title=\"Supprimer la formulation\" /></a>";
    newCell4.innerHTML = lien;

    var newRow2 = document.getElementById(idTable).insertRow(-1);
    newRow2.id = "tr_" + nbrRows;
    if ((nbrRows % 2) == 0) {
        newRow2.className = "on";
    }
    var newCell5 = newRow2.insertCell(0);
    newCell5.className = "col-champ";
    newCell5.colSpan = "4";
    text = "<textarea   name=\"valeurDefautTresLong\"  title=\"Valeur par defaut\" class=\"texte-tres-long mceEditorSansToolbar\" id=\"valeurParDefaut" + nbrRows + "_4\"  cols=\"\" rows=\"6\" style=\"display:none\"></textarea>"
        + "<textarea   name=\"valeurDefaut\"  title=\"Valeur par defaut\" class=\"texte-long mceEditorSansToolbar\" id=\"valeurParDefaut" + nbrRows + "_1\"  cols=\"\" rows=\"6\" style=\"display:block\"></textarea>"
        + "<input type='text' name=\"valeurDefautCourt\"  title=\"Valeur par defaut\" class=\"texte-court\" id=\"valeurParDefaut" + nbrRows + "_3\" style=\"display:none\" />"
        + "<input type='text' name=\"valeurDefautMoyen\"  title=\"Valeur par defaut\" class=\"texte-long\" id=\"valeurParDefaut" + nbrRows + "_2\"  style=\"display:none\" />" + "</td>";
    newCell5.innerHTML = text;

    tinyMCE.execCommand('mceAddControl', false, 'valeurParDefaut' + nbrRows + '_1');
    miseAJourTailleIFrame();
}

function AddRowTableExclusif(idTable) {
    var nbrRows = compteur++; //document.getElementById(idTable).rows.length;
    var newRow = document.getElementById(idTable).insertRow(-1);
    if ((nbrRows % 2) == 0) {
        newRow.className = "on";
    }
    newRow.id = "tr_" + nbrRows;
    var newCell1 = newRow.insertCell(0);
    newCell1.className = "num-formulation";
    var input = "<input type=\"text\" name=\"numFormulation\" value=\"" + nbrRows + "\" title=\"Num\xe9ro de formulation\">";
    newCell1.innerHTML = input;
    var newCell2 = newRow.insertCell(1);
    newCell2.className = "nb-carac";
    var combo = "<select name=\"tailleChamp\" class=\"auto\" title=\"Taille du Champ\" onchange=\"javascript:displayOptionChoiceWithTinyMCE(this, 'valeurParDefaut" + nbrRows + "_', 'mceEditor');\"><option  value='4'>Tr\xe8s long</option><option  value='1' selected=\"true\">Long</option><option value='2'>Moyen</option><option value='3'>Court</option></select>";
    newCell2.innerHTML = combo;
    var newCell3 = newRow.insertCell(2);
    newCell3.className = "long-col";
    text = "<textarea   name=\"valeurDefautTresLong\"  title=\"Valeur par defaut\" class=\"texte-tres-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_4\"  cols=\"\" rows=\"6\" style=\"display:none\"></textarea>"
        + "<textarea   name=\"valeurDefaut\"  title=\"Valeur par defaut\" class=\"texte-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_1\"  cols=\"\" rows=\"6\" style=\"display:block\"></textarea>"
        + "<input type='text' name=\"valeurDefautCourt\"  title=\"Valeur par defaut\" class=\"texte-court\" id=\"valeurParDefaut" + nbrRows + "_3\" style=\"display:none\" />"
        + "<input type='text' name=\"valeurDefautMoyen\"  title=\"Valeur par defaut\" class=\"texte-moyen\" id=\"valeurParDefaut" + nbrRows + "_2\"  style=\"display:none\" />" + "</td>";
    newCell3.innerHTML = text;
    var newCell4 = newRow.insertCell(3);
    newCell4.className = "check-col";
    checkbox = "<input type=\"radio\" name=\"precochee\" value=\"" + (nbrRows - 1) + "\">";
    newCell4.innerHTML = checkbox;
    var newCell5 = newRow.insertCell(4);
    newCell5.className = "actions";
    var lien = "<a href=\"javascript:removeRowFromTable( document.getElementById('tr_" + nbrRows + "').rowIndex ,'table-formulation')\"><img src=\"" + hrefRacine + "images/picto-poubelle.gif\" alt=\"Supprimer la formulation\" title=\"Supprimer la formulation\" /></a>";
    newCell5.innerHTML = lien;

    tinyMCE.execCommand('mceAddControl', false, 'valeurParDefaut' + nbrRows + '_1');
    miseAJourTailleIFrame();
}

// Dans le cas de surcharge de clauses
function AddRowTableExclusifSurcharge(idTable) {
    var nbrRows = compteur++; //document.getElementById(idTable).rows.length;
    var newRow = document.getElementById(idTable).insertRow(-1);
    if ((nbrRows % 2) == 0) {
        newRow.className = "on";
    }
    newRow.id = "tr_" + nbrRows;
    var newCell1 = newRow.insertCell(0);
    newCell1.className = "num-formulation";
    var input = "<input type=\"text\" name=\"numFormulation\" value=\"" + nbrRows + "\" title=\"Num\xe9ro de formulation\">";
    newCell1.innerHTML = input;
    var newCell2 = newRow.insertCell(1);
    newCell2.className = "nb-carac";
    var combo = "<select name=\"tailleChamp\" class=\"auto\" title=\"Taille du Champ\" onchange=\"javascript:displayOptionChoiceWithTinyMCE(this, 'valeurParDefaut" + nbrRows + "_', 'mceEditor');\"><option  value='4'>Tr\xe8s long</option><option  value='1' selected=\"true\">Long</option><option value='2'>Moyen</option><option value='3'>Court</option></select>";
    newCell2.innerHTML = combo;

    var newCell3 = newRow.insertCell(2);
    newCell3.className = "check-col";
    checkbox = "<input type=\"radio\" name=\"precochee\" value=\"" + (nbrRows - 1) + "\">";
    newCell3.innerHTML = checkbox;
    var newCell4 = newRow.insertCell(3);
    newCell4.className = "actions";
    var lien = "<a href=\"javascript:removeRowFromTableSurcharge( document.getElementById('tr_" + nbrRows + "').rowIndex ,'table-formulation')\"><img src=\"" + hrefRacine + "images/picto-poubelle.gif\" alt=\"Supprimer la formulation\" title=\"Supprimer la formulation\" /></a>";
    newCell4.innerHTML = lien;

    var newRow2 = document.getElementById(idTable).insertRow(-1);
    newRow2.id = "tr_" + nbrRows;
    if ((nbrRows % 2) == 0) {
        newRow2.className = "on";
    }

    var newCell5 = newRow2.insertCell(0);
    newCell5.colSpan = "4";
    newCell5.className = "col-champ";
    text = "<textarea   name=\"valeurDefautTresLong\"  title=\"Valeur par defaut\" class=\"texte-tres-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_4\"  cols=\"\" rows=\"6\" style=\"display:none\"></textarea>"
        + "<textarea   name=\"valeurDefaut\"  title=\"Valeur par defaut\" class=\"texte-long mceEditor\" id=\"valeurParDefaut" + nbrRows + "_1\"  cols=\"\" rows=\"6\" style=\"display:block\"></textarea>"
        + "<input type='text' name=\"valeurDefautCourt\"  title=\"Valeur par defaut\" class=\"texte-court\" id=\"valeurParDefaut" + nbrRows + "_3\" style=\"display:none\" />"
        + "<input type='text' name=\"valeurDefautMoyen\"  title=\"Valeur par defaut\" class=\"texte-long\" id=\"valeurParDefaut" + nbrRows + "_2\"  style=\"display:none\" />" + "</td>";
    newCell5.innerHTML = text;

    tinyMCE.execCommand('mceAddControl', false, 'valeurParDefaut' + nbrRows + '_1');
    miseAJourTailleIFrame();
}

// valider les parrametre direction. Renvoie true si ok, false sinon
function validerNombre(element, checkBox, choixParam, numForm) {
    var myChoixParam = document.getElementsByName(choixParam)[1];
    if (myChoixParam.checked) {
        for (var i = 0; ; i++) {
            var myElement = document.getElementsByName(element)[i];
            var myCheck = document.getElementsByName(checkBox)[i];
            var myNumForm = document.getElementsByName(numForm)[i];
            if (myElement.length == 0) {
                break; // On a pars� tous les champs
            } else {
                //le n� de formulation est vide;
                if (myNumForm.value == "") {
                    alert("Le champs 'Numero Formulation' ne doit pas \xeatre vide.");
                    myNumForm.focus();
                    return false;
                }
                //le nb de caract�re est vide;
                if (!myCheck.checked && myElement.value == "") {
                    alert("Le champs 'Nb de caract\xe8res' ne doit pas \xeatre vide.");
                    myElement.focus();
                    return false;
                }
                //valeur non numeric
                if (myElement.value != "" && !is_numeric(myElement.value)) {
                    alert("'" + myElement.value + "' n'est pas un nombre");
                    myElement.focus();
                    return false;
                }
            }
        }
    }
    return true;
}

var initMultiCritereTab = new Array();
var initMultiCritereFlag = true;

function ajouterCritereMultiSelect(id, val, libelle, critereMultiSelectDivElement, checkAllFlag) {

    var critereMultiSelectLabelElement = document.getElementById('critereMultiSelectLabelId').cloneNode(true);
    var critereMultiSelectCheckBoxElement = critereMultiSelectLabelElement.childNodes[0];
    critereMultiSelectLabelElement.id = critereMultiSelectLabelElement.id + '_' + val;
    critereMultiSelectCheckBoxElement.id = critereMultiSelectCheckBoxElement.id + '_' + val;
    critereMultiSelectCheckBoxElement.title = libelle;
    critereMultiSelectCheckBoxElement.name = 'check_list';
    critereMultiSelectLabelElement.htmlFor = critereMultiSelectCheckBoxElement.id;
    var critereMultiSelectHiddenElement = critereMultiSelectLabelElement.childNodes[1];
    if (checkAllFlag) {
        critereMultiSelectHiddenElement.id = 'procedurePassationTouteId';
    } else {
        critereMultiSelectHiddenElement.id = critereMultiSelectHiddenElement.id + '_' + val;
    }
    var critereMultiSelectHiddenSaveElement = critereMultiSelectLabelElement.childNodes[2];
    if (checkAllFlag) {
        var myFunction = function () {
            canevasProcedureCheckAll();
        };
        associerEvenement(critereMultiSelectCheckBoxElement, 'click', myFunction);
    } else {
        var myFunction = function () {
            majCanevasProcedurePassation(val);
        };
        associerEvenement(critereMultiSelectCheckBoxElement, 'click', myFunction);
    }
    critereMultiSelectHiddenSaveElement.id = critereMultiSelectHiddenSaveElement.id + '_' + id;
    critereMultiSelectHiddenSaveElement.value = val;

    critereMultiSelectLabelElement.appendChild(critereMultiSelectCheckBoxElement);
    critereMultiSelectLabelElement.appendChild(critereMultiSelectHiddenElement);
    critereMultiSelectLabelElement.appendChild(critereMultiSelectHiddenSaveElement);

    var txtElement = document.createTextNode(libelle);
    critereMultiSelectLabelElement.appendChild(txtElement);

    critereMultiSelectDivElement.appendChild(critereMultiSelectLabelElement);
}

// Associe une evenement a un element DOM compatible firefox ie
function associerEvenement(element, eventName, functionParam) {
    if (document.addEventListener) {
        element.addEventListener(eventName, functionParam, false);
    } else {
        element.attachEvent('on' + eventName, functionParam);
    }
}

function vider_liste(id_liste) {
    var liste = document.getElementById(id_liste);
    var longueur = liste.options.length;
    for (var k = longueur - 1; k >= 0; k = k - 1) {
        liste.options[k] = null;
    }
}

/*permet de distinguer le bouton click */
function submitAction(action) {
    document.getElementsByName("nameAction")[0].value = action;
    submitForm();
}

var newWin = null;

function closeWin() {
    if (newWin != null) {
        if (!newWin.closed) {
            newWin.close();
        }
    }
}

/*permet de distinguer le bouton click*/
function submitAction(action) {
    document.getElementsByName("nameAction")[0].value = action;
    submitForm();
}

var newWin = null;

function closeWin() {
    if (newWin != null) {
        if (!newWin.closed) {
            newWin.close();
        }
    }
}

function previsualiser() {
    var input = document.getElementById("nomMethode");
    input.value = "previsualiser";
    submitForm();
}

function previsualiserEditeur() {
    var input = document.getElementById("nomMethode");
    var declencheurAction = document.getElementById("declencheurAction");
    input.value = "previsualiser";
    declencheurAction.value = "clauseEditeur";
    submitForm();
}

function previsualiserEditeurSurchargee() {
    var input = document.getElementById("nomMethode");
    var declencheurAction = document.getElementById("declencheurAction");
    input.value = "previsualiser";
    declencheurAction.value = "clauseEditeurSurchargee";
    submitForm();
}

function reinitialiser() {
    var input = document.getElementById("nomMethode");
    input.value = "reinitialiser";
    submitForm();
}

function save() {
    var input = document.getElementById("nomMethode");
    input.value = "enregistrer";
    submitForm();
}

function validerClause() {
    var input = document.getElementById("nomMethode");
    input.value = "valider";
    submitForm();
}

function demanderValidationClause() {
    var input = document.getElementById("nomMethode");
    input.value = "demanderValidation";
    submitForm();
}

function clausierNextStep() {
    submitForm();
}

/*display panel according to option choice*/
function displayOptionChoice(mySelect, prefix_texte) {
    var totalOptions = mySelect.options;
    var valeurParDefaut = "";
    for (var i = 0; i < totalOptions.length; i++) {
        var totalLayers = (totalOptions[i].getAttribute("value"));
        totalLayers = prefix_texte + totalLayers;
        //On prend la valeur du texte visible
        if (document.getElementById(totalLayers).style.display != "none") {
            valeurParDefaut = document.getElementById(totalLayers).value;
        }
        document.getElementById(totalLayers).style.display = "none";
    }
    document.getElementById(prefix_texte + mySelect.options[mySelect.selectedIndex].value).style.display = "";
    document.getElementById(prefix_texte + mySelect.options[mySelect.selectedIndex].value).value = valeurParDefaut;
}

function initFormRecherche() {
    var listeSelect = document.getElementsByTagName("select");
    for (var i = 0; i < listeSelect.length; i++) {
        listeSelect[i].options[0].selected = true;
    }
    var liste = document.getElementsByTagName("input");
    for (var i = 0; i < liste.length; i++) {
        liste[i].value = "";
    }
    var liste = document.getElementsByTagName("textarea");
    for (var i = 0; i < liste.length; i++) {
        liste[i].value = "";
    }
}

// v�rifie que le nombre maxi n'a pas �t� atteint pendant que l'utilisateur saisie de texte.
function limite(zone, max) {
    if (zone.value.length >= max) {
        zone.value = zone.value.substring(0, max);
    }
}

/**
 * Met a jour la valeur du formulaire pour la procedure associee au canevas.
 * @param checkBoxElement input checkbox
 * @param hiddenElement input valeur du formulaire
 * @param procedureId identifiant de la procedure
 */
function majCanevasProcedurePassation(procedureId) {
    var checkBoxElement = document.getElementById('procedure_' + procedureId);
    var hiddenElement = document.getElementById('procedurePassationTabId_' + procedureId);
    if (checkBoxElement.checked) {
        hiddenElement.value = procedureId;
    } else {
        hiddenElement.value = '';
        var checkBoxAllElement = document.getElementById('procedure_1');
        if (checkBoxAllElement.checked) {
            var checkAllHiddenElement = document.getElementById('procedurePassationTouteId');
            checkBoxAllElement.checked = false;
            checkAllHiddenElement.value = false;
        }
    }
}

var test = 0;

/**
 * Met a jour la valeur du formulaire de la coche 'toutes' les procedures pour la creation d'un canevas
 * @param checkBoxElement input checkbox
 * @param hiddenElement input valeur du formulaire
 */
function canevasProcedureCheckAll() {
    var checkBoxElement = document.getElementById('procedure_1');
    var hiddenElement = document.getElementById('procedurePassationTouteId');
    var checks = document.getElementsByName("check_list");
    var hiddenList = getElementsByName('procedurePassationTab');
    var procedurePassationTouteElementList = getElementsByName('procedurePassationToute');
    var debutHiddenList = 0;
    if (procedurePassationTouteElementList.length == 0) {
        debutHiddenList = 1;
    }
    if (checkBoxElement.checked) {
        hiddenElement.value = true;
        for (var i = debutHiddenList; i < hiddenList.length; i++) {
            var procIdSavElement = document.getElementById('procedureIdSave_' + (i - debutHiddenList));
            hiddenList[i].value = procIdSavElement.value;
        }
    } else {
        hiddenElement.value = false;
        for (var i = debutHiddenList; i < hiddenList.length; i++) {
            hiddenList[i].value = '';
        }
    }
    for (var i = 0; i < checks.length; i++) {
        var ch = checks[i];
        ch.checked = checkBoxElement.checked;
    }
}

function checkUncheckMultiSelectCheckBox(obj, idx, check) {
    if (isNaN(idx))
        return '';
    var checkBoxItem = document.getElementById(obj + idx);
    checkBoxItem.checked = check;
}

/**
 * Entoure en rouge la selection des procedures liees a un canevas.
 */
function errorDivCanevasProcedurePassation(element) {
    element.style.borderColor = '#FF0000';
}

/*Function toggle table row detail*/
function toggleTableRow(myPosition, myRow) {
    var myRowDisplay = jQuery('.' + myRow).css('display');
    jQuery(myPosition).toggleClass('toggle-recap');
    jQuery('.' + myRow).toggleClass('row-visible');
}

/*
 * la méthode pour valider un document et assurer que le bouton peut être cliqué une fois
 */
var boutonValidationDocumentDejaClique = false;
function validerDocument(idDocument, categorie) {
    if (!boutonValidationDocumentDejaClique) {
        document.location.href = "validationDocumentProcess.epm?categorie="+categorie+"&idDoc=" + idDocument;
        boutonValidationDocumentDejaClique = true;
    }
}

function mettreTailleChampValeurDefaut(idTailleChamp) {
    jQuery('#' + idTailleChamp + ' option').removeAttr('selected').filter('[value="1"]').attr('selected', "selected");
}

function rechercheClauseProceduresParContrat() {
    var idContrat = document.getElementById('typeContrat').value;
    try {
        idContrat = parseInt(idContrat);
    }
    catch (e) {
        console.log("Impossible de parser l'id Contrat");
    }
    var proceduresCommunes = mapContratsProceduresIds[idContrat];
    var proceduresGardees = [];
    var selectionnez = {id: -1, libelle: "Sélectionnez"};
    proceduresGardees.push(selectionnez);
    for (var i = 0; i < toutesProcedures.length; i++) {
        var procedure = toutesProcedures[i];
        if (idContrat === 1)
            proceduresGardees.push(procedure);
        else
            for (var j = 0; j < proceduresCommunes.length; j++) {
                var id = proceduresCommunes[j];
                if (id === procedure.id) {
                    proceduresGardees.push(procedure);
                    break;
                }
            }
    }

    jQuery('#procedurePassation').html('');
    var options = ""
    for (var k = 0; k < proceduresGardees.length; k++) {
        var p = proceduresGardees[k];
        options += "<option value=\"" + p.id + "\">" + p.libelle + "</option>\n"
    }
    jQuery('#procedurePassation').html(options)
}
