package fr.paris.epm.redaction.webservice.dto;

public class CPVDTO {
	private String code;

	private String libelle;

	public CPVDTO() {
	}

	public String getCode() {
		return code;
	}

	public void setCode( final String code ) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( final String libelle ) {
		this.libelle = libelle;
	}
}
