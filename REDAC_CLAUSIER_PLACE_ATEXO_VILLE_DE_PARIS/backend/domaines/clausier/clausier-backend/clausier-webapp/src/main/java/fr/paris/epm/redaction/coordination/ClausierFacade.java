package fr.paris.epm.redaction.coordination;

import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;

import java.util.List;

/**
 * Interface qui expose les méthodes offertes par le clausier.
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public interface ClausierFacade {
    
    /**
     * Recherche les informations de toutes les publications de la clause dont l'identifiant est passé en paramétre.
     * utilisé pour afficher la liste des versions dans la popup d'historique des clauses.
     * @param idClause identifiant de la clause éditeur pour laquelle on souhaite avoir les version de publication
     * @return une liste de EpmTPublicationClausier contenant les informations de publication.
     */
    @Deprecated
    List<EpmTPublicationClausier> recupererPublicationsParClause(final int idClause);
}
