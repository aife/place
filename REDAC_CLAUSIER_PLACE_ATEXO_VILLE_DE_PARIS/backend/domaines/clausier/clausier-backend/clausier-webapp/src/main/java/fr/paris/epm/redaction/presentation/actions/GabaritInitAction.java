package fr.paris.epm.redaction.presentation.actions;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.redaction.presentation.bean.Credentials;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class GabaritInitAction extends GabaritAbstractAction {

	private String level;

	// Keycloak
	private String oauthType;
	private String oauthClient;
	private String oauthUsername;
	private String oauthPassword;
	private String oauthUrl;

	// Document
	private String documentAgentUrl;
	private String client;


	private static Map<String, String> NIVEAUX = new HashMap<>() {{
		put(levelInterministeriel, "PLATEFORME");
		put(levelMinisteriel, "ORGANISME");
		put(levelDirectionService, "ENTITE_ACHAT");
		put(levelAgent, "AGENT");
	}};

	/**
	 * Permet de récupérer le jeton
	 * @return le jeton
	 */
	private String recupererAccessToken() {
		// Headers
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED);

		final MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("grant_type", oauthType);
		body.add("client_id", oauthClient);
		body.add("username", oauthUsername);
		body.add("password", oauthPassword);

		ResponseEntity<Credentials> response = new RestTemplate().postForEntity(oauthUrl, body, Credentials.class);

		if ( HttpStatus.OK != response.getStatusCode() || null == response.getBody() ) {
			LOG.error("Impossible de créer l'access token");

			throw new RuntimeException("Impossible de créer l'access token. Response = " + response);
		}

		return response.getBody().getAccessToken();
	}

	@Override
	public ActionForward execute(SessionManagerGlobal sessionManager, ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOG.info("Redirection vers le nouveau module des documents");

		try {
			String token = this.recupererAccessToken();
			sessionManager.getSession(request).setAttribute(ConstantesGlobales.ACCESS_TOKEN, token);

			LOG.info("Récupération de l'access token = {}", token);
		} catch ( Exception e ) {
			LOG.info("Erreur lors de la récupération de l'access token", e);
		}

		sessionManager.getSession(request).setAttribute(ConstantesGlobales.NIVEAU_INTERVENTION, NIVEAUX.get(level));
		sessionManager.getSession(request).setAttribute(ConstantesGlobales.CLIENT, client);


		return mapping.findForward("success");
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public void setOauthType( String oauthType ) {
		this.oauthType = oauthType;
	}

	public void setOauthClient( String oauthClient ) {
		this.oauthClient = oauthClient;
	}

	public void setOauthUsername( String oauthUsername ) {
		this.oauthUsername = oauthUsername;
	}

	public void setOauthPassword( String oauthPassword ) {
		this.oauthPassword = oauthPassword;
	}

	public void setOauthUrl( String oauthUrl ) {
		this.oauthUrl = oauthUrl;
	}

	public String getClient() {
		return client;
	}

	public void setClient( String client ) {
		this.client = client;
	}

	public String getDocumentAgentUrl() {
		return documentAgentUrl;
	}

	public void setDocumentAgentUrl( String documentAgentUrl ) {
		this.documentAgentUrl = documentAgentUrl;
	}
}
