package fr.paris.epm.redaction.coordination.bean;

public enum RechercheClauseEnum {

    /**
     * clause associé à un canevas
     */
    CLAUSE_CANEVAS,

    /**
     * clause associé a un document
     */
    CLAUSE_DOCUMENT,

    /**
     * derniere version de la clause (clause en cours), utilisée notamment pour
     * effectuer la comparaison des versions dans la popine lors de l'édition d'un document
     */
    DERNIERE_CLAUSE;

}