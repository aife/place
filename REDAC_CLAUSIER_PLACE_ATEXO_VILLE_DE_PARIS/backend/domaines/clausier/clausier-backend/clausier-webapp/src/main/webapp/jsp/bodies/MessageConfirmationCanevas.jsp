<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">
    hrefRacine = '<atexo:href href=""/>';
</script>

<c:set var="actionEditeur" value="" />
<c:if test="${editeur != null && editeur == true }">
    <c:set var="actionEditeur" value="Editeur" />
</c:if>

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs">
        <logic:equal name="typeAction" value="C">
            <bean:message key="previsualiserCanevas${actionEditeur}.titre.cree"/>
        </logic:equal>
        <logic:equal name="typeAction" value="M">
            <bean:message key="previsualiserCanevas${actionEditeur}.titre.modifier"/>
        </logic:equal>
        <logic:equal name="typeAction" value="D">
            <bean:message key="previsualiserCanevas${actionEditeur}.titre.dupliquer"/>
        </logic:equal>
    </div>
    <atexo:fichePratique reference="" key="common.fichePratique"/>
    <div class="breaker"></div>
    <!--Debut message confirmation-->
    <div class="form-bloc-message msg-conf">
        <div class="top"><span class="left"></span><span class="right"></span></div>
        <div class="content">
            <div class="message-left">
                <bean:message key="previsualiserCanevas.txt.canevas"/> <c:out value="${reference}"/> <bean:message key="previsualiserCanevas.txt.bien"/>

                <logic:equal name="typeAction" value="C">
                    <bean:message key="previsualiserCanevas.txt.cree" />
                </logic:equal>
                <logic:equal name="typeAction" value="M">
                    <bean:message key="previsualiserCanevas.txt.modifie" />
                </logic:equal>
                <logic:equal name="typeAction" value="D">
                    <bean:message key="previsualiserCanevas.txt.duplique" />
                </logic:equal>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left"></span><span class="right"></span></div>
    </div>
    <!--Fin message confirmation-->

    <!--Debut boutons-->
    <div class="spacer"></div>
    <div class="boutons">
        <a href="CreationCanevas1${actionEditeur}Init.epm" class="nouvelle-clause"><bean:message key="previsualiserCanevas.txt.nouveau"/></a>
    </div>
    <!--Fin boutons-->
</div>
<!--Fin main-part-->
