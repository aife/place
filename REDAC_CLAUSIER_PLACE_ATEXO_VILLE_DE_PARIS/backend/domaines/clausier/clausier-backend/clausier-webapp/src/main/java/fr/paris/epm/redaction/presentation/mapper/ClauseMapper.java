package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.metier.objetValeur.ClauseExport;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.forms.*;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * Clause Mapper
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Mapper(componentModel = "spring", uses = {HtmlSanitizerMapper.class, ClausePotentiellementConditionneeMapper.class, RoleClauseMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class}, collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE)
// CollectionMappingStrategy.TARGET_IMMUTABLE évite les list.clear(); list.addAll();
public interface ClauseMapper {

    @Mapping(source = "idClause", target = "id")
    @Mapping(source = "idClause", target = "idClause")
    @Mapping(source = "reference", target = "referenceClause")
    @Mapping(source = "epmTRefStatutRedactionClausier", target = "idStatutRedactionClausier")
    @Mapping(source = "epmTRefTypeClause", target = "idTypeClause")
    @Mapping(source = "epmTRefTypeClause", target = "labelTypeClause")
    @Mapping(source = "epmTRefTypeDocument", target = "idTypeDocument")
    @Mapping(source = "epmTRefThemeClause", target = "idThemeClause")
    @Mapping(source = "epmTRefThemeClause", target = "labelThemeClause")
    @Mapping(source = "epmTRefTypeContrats", target = "idsTypeContrats")
    @Mapping(source = "epmTRefTypeContrats", target = "typesContrat", qualifiedByName = {"DirectoryNamedMapper", "directoryListOfTypeContratSet"})
    @Mapping(source = "epmTRefProcedure", target = "idProcedure")
    @Mapping(source = "epmTRefProcedure", target = "procedure", qualifiedByName = {"DirectoryNamedMapper", "toDirectoryProcedureBean"})
    ClauseBean toClauseBean(EpmTClauseAbstract epmTClause);

    @Mapping(source = "idClause", target = "id")
    @Mapping(source = "referenceClause", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeClause", target = "epmTRefTypeClause")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idThemeClause", target = "epmTRefThemeClause")
    @Mapping(source = "idProcedure", target = "epmTRefProcedure")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    EpmTClause toEpmTClause(ClauseBean ClauseBean);

    @Mapping(source = "idClause", target = "id")
    @Mapping(source = "referenceClause", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeClause", target = "epmTRefTypeClause")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idThemeClause", target = "epmTRefThemeClause")
    @Mapping(source = "idProcedure", target = "epmTRefProcedure")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    void toEpmTClause(ClauseBean ClauseBean, @MappingTarget EpmTClause epmTClause);

    @Mapping(source = "referenceClause", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeClause", target = "epmTRefTypeClause")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idThemeClause", target = "epmTRefThemeClause")
    @Mapping(source = "idProcedure", target = "epmTRefProcedure")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    void toEpmVClause(ClauseBean ClauseBean, @MappingTarget EpmVClause epmVClause);


    @Mapping(source = "epmTRefThemeClause.libelle", target = "theme")
    @Mapping(source = "epmTRefStatutRedactionClausier.id", target = "idStatutRedactionClausier")
    @Mapping(target = "actif", ignore = true)
    Clause toClause(EpmTClauseAbstract epmTClauseAbstract);

    @Mapping(source = "epmTRefStatutRedactionClausier.libelle", target = "statut")
    @Mapping(source = "dateModification", target = "date")
    @Mapping(source = "epmTRefThemeClause.libelle", target = "theme")
    @Mapping(source = "epmTRefTypeClause.libelle", target = "libelleTypeClause")
    @Mapping(source = "epmTRefTypeClause.id", target = "idTypeClause")
    @Mapping(source = "epmTRefTypeDocument.libelle", target = "libelleTypeDocument")
    @Mapping(source = "infoBulleText", target = "infoBulle.description")
    @Mapping(source = "infoBulleUrl", target = "infoBulle.lien")
    ClauseExport toClauseExport(EpmTClauseAbstract epmTClauseAbstract);

    @Mapping(target = "reference", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "datePremiereValidation", ignore = true)
    @Mapping(target = "dateDerniereValidation", ignore = true)
    @Mapping(target = "epmTRefStatutRedactionClausier", ignore = true)
    @Mapping(target = "idOrganisme", ignore = true)
    @Mapping(target = "clauseEditeur", ignore = true)
    @Mapping(target = "epmTRoleClausesTrie", ignore = true)
    @Mapping(target = "epmTRoleClausesDefault", ignore = true)
    EpmTClause toEpmTClauseDuplication(EpmTClauseAbstract epmTClauseSource);

    @Mapping(target = "reference", ignore = true)
    @Mapping(target = "dateModification", ignore = true)
    @Mapping(target = "dateCreation", ignore = true)
    @Mapping(target = "datePremiereValidation", ignore = true)
    @Mapping(target = "dateDerniereValidation", ignore = true)
    @Mapping(target = "epmTRefTypeDocument", ignore = true)
    @Mapping(target = "epmTRefTypeClause", ignore = true)
    @Mapping(target = "parametrableDirection", ignore = true)
    @Mapping(target = "idNaturePrestation", ignore = true)
    @Mapping(target = "epmTRefProcedure", ignore = true)
    @Mapping(target = "etat", ignore = true)
    @Mapping(target = "formulationModifiable", ignore = true)
    @Mapping(target = "epmTRefStatutRedactionClausier", ignore = true)
    @Mapping(target = "compatibleEntiteAdjudicatrice", ignore = true)
    @Mapping(target = "idOrganisme", ignore = true)
    @Mapping(target = "clauseEditeur", ignore = true)
    @Mapping(target = "epmTRoleClausesTrie", ignore = true)
    @Mapping(target = "epmTRoleClausesDefault", ignore = true)
    void toEpmTClauseSurchargeDuplication(EpmTClause epmTClauseSource, @MappingTarget EpmTClauseAbstract epmTClauseTarget);


    @Mapping(target = "epmTRoleClausesTrie", ignore = true)
    @Mapping(target = "epmTRoleClausesDefault", ignore = true)
    EpmTClause toEpmTClause(EpmTClausePub epmTClausePub);


    @Mapping(target = "epmTRoleClausesTrie", ignore = true)
    @Mapping(target = "epmTRoleClausesDefault", ignore = true)
    EpmTClause toEpmTClause(EpmTClauseAbstract epmTClauseAbstract);

    default ClauseListeChoixCumulatifForm toClauseListeChoixCommulatif(EpmTClauseAbstract clauseAbstract) {
        if (CollectionUtils.isEmpty(clauseAbstract.getEpmTRoleClauses())) {
            return null;
        }

        ClauseListeChoixCumulatifForm clauseListeChoixCumulatifForm = new ClauseListeChoixCumulatifForm();
        clauseListeChoixCumulatifForm.setTextFixeAvant(Util.decodeCaractere(clauseAbstract.getTexteFixeAvant()));
        clauseListeChoixCumulatifForm.setTextFixeApres(Util.decodeCaractere(clauseAbstract.getTexteFixeApres()));
        clauseListeChoixCumulatifForm.setParametrableAgent(clauseAbstract.getParametrableAgent());
        clauseListeChoixCumulatifForm.setParametrableDirection(clauseAbstract.getParametrableDirection());
        clauseListeChoixCumulatifForm.setModifiable(clauseAbstract.isFormulationModifiable());
        clauseListeChoixCumulatifForm.setSautTextFixeApres(String.valueOf(clauseAbstract.isSautLigneTexteApres()));
        clauseListeChoixCumulatifForm.setSautTextFixeAvant(String.valueOf(clauseAbstract.isSautLigneTexteAvant()));

        clauseListeChoixCumulatifForm.setFormulaires(clauseAbstract.getEpmTRoleClauses().stream().map(epmTRoleClauseAbstract -> {
            ClauseFormulaire formulaire = new ClauseFormulaire();
            formulaire.setId(formulaire.getId());
            formulaire.setNumFormulation(epmTRoleClauseAbstract.getNumFormulation());
            formulaire.setTailleChamps(String.valueOf(epmTRoleClauseAbstract.getNombreCarateresMax()));
            formulaire.setDefaultValeur(Util.encodeCaractere(epmTRoleClauseAbstract.getValeurDefaut()));
            formulaire.setPrecochee(epmTRoleClauseAbstract.isPrecochee());
            return formulaire;
        }).sorted(Comparator.comparing(ClauseFormulaire::getNumFormulation)).collect(Collectors.toList()));


        return clauseListeChoixCumulatifForm;
    }

    default ClauseListeChoixExclusifForm toClauseListeChoixExclusif(EpmTClauseAbstract clauseAbstract) {
        if (CollectionUtils.isEmpty(clauseAbstract.getEpmTRoleClauses())) {
            return null;
        }

        ClauseListeChoixExclusifForm clauseListeChoixExclusifForm = new ClauseListeChoixExclusifForm();
        clauseListeChoixExclusifForm.setTextFixeAvant(Util.decodeCaractere(clauseAbstract.getTexteFixeAvant()));
        clauseListeChoixExclusifForm.setTextFixeApres(Util.decodeCaractere(clauseAbstract.getTexteFixeApres()));
        clauseListeChoixExclusifForm.setParametrableAgent(clauseAbstract.getParametrableAgent());
        clauseListeChoixExclusifForm.setParametrableDirection(clauseAbstract.getParametrableDirection());
        clauseListeChoixExclusifForm.setModifiable(clauseAbstract.isFormulationModifiable());
        clauseListeChoixExclusifForm.setSautTextFixeApres(String.valueOf(clauseAbstract.isSautLigneTexteApres()));
        clauseListeChoixExclusifForm.setSautTextFixeAvant(String.valueOf(clauseAbstract.isSautLigneTexteAvant()));

        clauseListeChoixExclusifForm.setFormulaires(clauseAbstract.getEpmTRoleClauses().stream().map(epmTRoleClauseAbstract -> {
            ClauseFormulaire formulaire = new ClauseFormulaire();
            formulaire.setId(formulaire.getId());
            formulaire.setNumFormulation(epmTRoleClauseAbstract.getNumFormulation());
            formulaire.setTailleChamps(String.valueOf(epmTRoleClauseAbstract.getNombreCarateresMax()));
            formulaire.setDefaultValeur(Util.encodeCaractere(epmTRoleClauseAbstract.getValeurDefaut()));
            formulaire.setPrecochee(epmTRoleClauseAbstract.isPrecochee());
            return formulaire;
        }).sorted(Comparator.comparing(ClauseFormulaire::getNumFormulation)).collect(Collectors.toList()));


        return clauseListeChoixExclusifForm;
    }

    default ClauseTexteLibreForm toClauseTexteLibreForm(EpmTClauseAbstract clauseAbstract) {
        if (CollectionUtils.isEmpty(clauseAbstract.getEpmTRoleClauses())) {
            return null;
        }
        EpmTRoleClauseAbstract roleClauseAbstract = clauseAbstract.getEpmTRoleClauses().stream().findFirst().orElse(null);
        if (roleClauseAbstract == null) {
            return null;
        }
        ClauseTexteLibreForm clauseTexteLibreForm = new ClauseTexteLibreForm();
        clauseTexteLibreForm.setTextFixeAvant(Util.decodeCaractere(clauseAbstract.getTexteFixeAvant()));
        clauseTexteLibreForm.setTextFixeApres(Util.decodeCaractere(clauseAbstract.getTexteFixeApres()));
        clauseTexteLibreForm.setSautTextFixeApres(String.valueOf(clauseAbstract.isSautLigneTexteApres()));
        clauseTexteLibreForm.setSautTextFixeAvant(String.valueOf(clauseAbstract.isSautLigneTexteAvant()));
        clauseTexteLibreForm.setObligatoire("1".equals(roleClauseAbstract.getChampObligatoire()));
        clauseTexteLibreForm.setTailleChamps(roleClauseAbstract.getNombreCarateresMax());

        return clauseTexteLibreForm;
    }

    default ClauseTexteFixeForm toClauseTexteFixeForm(EpmTClauseAbstract clauseAbstract) {
        ClauseTexteFixeForm clauseListeChoixExclusifForm = new ClauseTexteFixeForm();
        clauseListeChoixExclusifForm.setTexteFixe(clauseAbstract.getTexteFixeAvant());
        return clauseListeChoixExclusifForm;
    }

    default ClauseTextePrevaloriseForm toClauseTextePrevalorise(EpmTClauseAbstract clauseAbstract) {
        if (CollectionUtils.isEmpty(clauseAbstract.getEpmTRoleClauses())) {
            return null;
        }
        EpmTRoleClauseAbstract roleClauseAbstract = clauseAbstract.getEpmTRoleClauses().stream().findFirst().orElse(null);
        if (roleClauseAbstract == null) {
            return null;
        }
        ClauseTextePrevaloriseForm clauseTextePrevaloriseForm = new ClauseTextePrevaloriseForm();
        clauseTextePrevaloriseForm.setTextFixeAvant(Util.decodeCaractere(clauseAbstract.getTexteFixeAvant()));
        clauseTextePrevaloriseForm.setTextFixeApres(Util.decodeCaractere(clauseAbstract.getTexteFixeApres()));
        clauseTextePrevaloriseForm.setSautTextFixeApres(String.valueOf(clauseAbstract.isSautLigneTexteApres()));
        clauseTextePrevaloriseForm.setSautTextFixeAvant(String.valueOf(clauseAbstract.isSautLigneTexteAvant()));
        clauseTextePrevaloriseForm.setDefaultValue(roleClauseAbstract.getValeurDefaut());
        clauseTextePrevaloriseForm.setTailleChamps(roleClauseAbstract.getNombreCarateresMax());
        clauseTextePrevaloriseForm.setParametrableAgent(clauseAbstract.getParametrableAgent());
        clauseTextePrevaloriseForm.setParametrableDirection(clauseAbstract.getParametrableDirection());


        return clauseTextePrevaloriseForm;
    }

    default ClauseValeurHeriteeForm toClauseValeurHeritee(EpmTClauseAbstract clauseAbstract) {
        if (CollectionUtils.isEmpty(clauseAbstract.getEpmTRoleClauses())) {
            return null;
        }
        EpmTRoleClauseAbstract roleClauseAbstract = clauseAbstract.getEpmTRoleClauses().stream().findFirst().orElse(null);
        if (roleClauseAbstract == null) {
            return null;
        }
        ClauseValeurHeriteeForm clauseValeurHeriteeForm = new ClauseValeurHeriteeForm();
        clauseValeurHeriteeForm.setTextFixeAvant(Util.decodeCaractere(clauseAbstract.getTexteFixeAvant()));
        clauseValeurHeriteeForm.setTextFixeApres(Util.decodeCaractere(clauseAbstract.getTexteFixeApres()));
        clauseValeurHeriteeForm.setSautTextFixeApres(String.valueOf(clauseAbstract.isSautLigneTexteApres()));
        clauseValeurHeriteeForm.setSautTextFixeAvant(String.valueOf(clauseAbstract.isSautLigneTexteAvant()));
        clauseValeurHeriteeForm.setIdRefValeurTypeClause(Integer.parseInt(roleClauseAbstract.getValeurDefaut()));


        return clauseValeurHeriteeForm;
    }
}
