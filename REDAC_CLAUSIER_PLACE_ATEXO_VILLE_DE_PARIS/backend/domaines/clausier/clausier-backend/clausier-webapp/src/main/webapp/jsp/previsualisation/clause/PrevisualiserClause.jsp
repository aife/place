<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="redactionTag" prefix="redaction"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><bean:message key="previsualiserClause.txt.elaborationPassationMarche"/></title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css"/>
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
    <script type="text/javascript" language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/editeurTexteRedaction.js"></script>
    <script language="javascript">
        window.onload = function () {
            popupResize();
            initEditeursTexteSansToolbarRedaction();
        }
    </script>
</head>

<%--@elvariable id="clause" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>

<body id="layoutPopup">
<div class="previsu-doc previsu-clause" id="container">
    <div class="content">
        <center>
            <h5>
                <c:if test="${clause == null || clause.reference == null}">
                    <bean:message key="previsualiserClause.txt.previsualisation" />
                </c:if>
                <c:if test="${clause != null && clause.reference != null}">
                    <c:out value="${clause.reference}" />
                </c:if>
            </h5>
        </center>
        <logic:empty name="clause">
            <center>
                <bean:message key="redaction.msg.global.erreur" />
            </center>
        </logic:empty>
        <logic:notEmpty name="clause">
            <ul>
                <li>
                    <div class="${(clause.epmTRefTypeClause.id == 6 || clause.epmTRefTypeClause.id == 7) ? 'line' : 'clause'}">
                        ${clause.texteFixeAvant}
                        <c:if test="${clause.sautLigneTexteAvant == true}"><br/></c:if>
                        <redaction:previsualiserPptExtClause name="clause" user="utilisateur"/>
                        <c:if test="${clause.sautLigneTexteApres == true}"><br/></c:if>
                        ${clause.texteFixeApres}
                    </div>
                </li>
            </ul>
        </logic:notEmpty>
    </div>
    <a href="javascript:window.close();" class="fermer"> Fermer </a>
    <div class="breaker"></div>
</div>

</body>
</html>
