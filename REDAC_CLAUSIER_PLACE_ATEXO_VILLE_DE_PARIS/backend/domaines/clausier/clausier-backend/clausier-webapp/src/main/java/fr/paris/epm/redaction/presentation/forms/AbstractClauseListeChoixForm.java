package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.redaction.commun.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * Formulaire de Clause Liste Choix Cumulatif.
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */

public class AbstractClauseListeChoixForm {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut Text Fixe Avant.
     */

    protected String textFixeAvant;
    /**
     * Attribut Text Fixe Apres.
     */

    protected String textFixeApres;
    /**
     * Attribut saut Text Fixe Avant.
     */

    protected String sautTextFixeAvant;
    /**
     * Attribut saut Text Fixe Apres.
     */

    protected String sautTextFixeApres;
    /**
     * Attribut formulation Modifiable.
     */
    @Deprecated
    protected String formulationModifiable;

    protected boolean modifiable;
    /**
     * Attribut parametrable Direction.
     */

    protected String parametrableDirection;
    /**
     * Attribut parametrable Agent.
     */

    protected String parametrableAgent;
    /**
     * Attribut numéro de Formulation.
     */

    protected String[] numFormulation;
    /**
     * Attribut taille du Champ (long, court, moyen).
     */

    protected String[] tailleChamp;
    /**
     * Attribut valeur Defaut (champ de saisie long ou cas ou la valeur du
     * taille champ est : long).
     */

    protected String[] valeurDefaut;
    /**
     * Attribut valeur Defaut Moyen (champ de saisie moyen ou cas ou la valeur
     * du taille champ est : moyen).
     */

    protected String[] valeurDefautMoyen;
    /**
     * Attribut valeur Defaut Court (champ de saisie court ou cas ou la valeur
     * du taille champ est : court).
     */

    protected String[] valeurDefautCourt;

    protected String[] valeurDefautTresLong;

    /**
     * Attribut Collection de formulations.
     */

    protected Collection<? extends EpmTRoleClauseAbstract> formulationCollection;
    /**
     * marqueur de fichier journal.
     */

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractClauseListeChoixForm.class);

    /**
     * cette méthode permet de renvoyer une collection de formulations.
     *
     * @return renvoyer une collection de formulations.
     */
    public final Collection<? extends EpmTRoleClauseAbstract> getFormulationCollection() {
        return formulationCollection;
    }

    /**
     * cette méthode initialise une collection de formulations par une
     * collection donnée.
     *
     * @param valeur initialise une collection de formulations.
     */
    public final void setFormulationCollection(final Collection<? extends EpmTRoleClauseAbstract> valeur) {
        this.formulationCollection = valeur;
    }

    /**
     * cette méthode renvoie l'attribut textFixeAvant.
     *
     * @return renvoyer l'attribut textFixeAvant.
     */
    public final String getTextFixeAvant() {
        return textFixeAvant;
    }

    /**
     * cette méthode initialise l'attribut textFixeAvant par une valeur donnée.
     *
     * @param valeur pour initialiser l'attribut textFixeAvant.
     */
    public final void setTextFixeAvant(final String valeur) {
        textFixeAvant = valeur;
    }

    /**
     * cette méthode renvoie l'attribut textFixeApres.
     *
     * @return texte Fixe Apres .
     */
    public final String getTextFixeApres() {
        return textFixeApres;
    }

    /**
     * cette méthode initialise l'attribut textFixeApres par une valeur donnée.
     *
     * @param valeur pour initialiser l'attribut textFixeApres.
     */
    public final void setTextFixeApres(final String valeur) {
        textFixeApres = valeur;
    }

    /**
     * cette méthode renvoie l'attribut formulationModifiable.
     *
     * @return renvoyer l'attribut formulationModifiable..
     */
    @Deprecated
    public final String getFormulationModifiable() {
        return formulationModifiable;
    }

    /**
     * cette méthode initialise l'attribut formulationModifiable par une valeur
     * donnée.
     *
     * @param valeur pour initialiser l'attribut formulationModifiable.
     */
    @Deprecated
    public final void setFormulationModifiable(final String valeur) {
        formulationModifiable = valeur;
    }

    public boolean isModifiable() {
        return modifiable;
    }

    public void setModifiable(boolean modifiable) {
        this.modifiable = modifiable;
    }

    /**
     * cette méthode renvoie l'attribut parametrableDirection qui indique si la
     * clause est parametrable par la direction.
     *
     * @return renvoyer l'attribut parametrableDirection.
     */
    public final String getParametrableDirection() {
        return parametrableDirection;
    }

    /**
     * cette méthode initialise l'attribut parametrableDirection par une valeur
     * donnée.
     *
     * @param valeur positionne l'attribut parametrableDirection.
     */
    public final void setParametrableDirection(final String valeur) {
        parametrableDirection = valeur;
    }

    /**
     * cette méthode renvoie l'attribut parametrableAgent qui indique si la
     * clause est parametrable par l'agent.
     *
     * @return renvoyer l'attribut parametrableAgent.
     */
    public final String getParametrableAgent() {
        return parametrableAgent;
    }

    /**
     * cette méthode initialise l'attribut parametrableAgent par une valeur
     * donnée.
     *
     * @param valeur positionne l'attribut parametrableAgent.
     */
    public final void setParametrableAgent(final String valeur) {
        parametrableAgent = valeur;
    }

    /**
     * cette méthode renvoie le numéro de formulation.
     *
     * @return renvoyer le numéro de formulation.
     */
    public final String[] getNumFormulation() {
        return numFormulation;
    }

    /**
     * cette méthode initialise le numéro de formulation par une valeur donnée.
     *
     * @param valeur positionne le numéro de formulation.
     */
    public final void setNumFormulation(final String[] valeur) {
        numFormulation = valeur;
    }

    /**
     * cette méthode renvoie les valeurs par défaut.
     *
     * @return renvoyer les valeurs par défaut.
     */
    public final String[] getValeurDefaut() {
        if (valeurDefaut != null) {
            for (int i = 0; i < valeurDefaut.length; i++) {
                valeurDefaut[i] = Util.decodeCaractere(valeurDefaut[i]);
            }
        }
        return valeurDefaut;
    }

    /**
     * cette méthode initialise les valeurs par défaut par un tableau de
     * valeurs.
     *
     * @param valeur initialise les valeurs par défaut.
     */
    public final void setValeurDefaut(final String[] valeur) {
        valeurDefaut = valeur;
    }

    /**
     * cette méthode renvoie l'attribut sautTextFixeAvant.
     *
     * @return renvoyer l'attribut sautTextFixeAvant.
     */
    public final String getSautTextFixeAvant() {
        return sautTextFixeAvant;
    }

    /**
     * cette méthode initialise l'attribut sautTextFixeAvant par une valeur
     * donnée.
     *
     * @param valeur initialise l'attribut sautTextFixeAvant.
     */
    public final void setSautTextFixeAvant(final String valeur) {
        sautTextFixeAvant = valeur;
    }

    /**
     * cette méthode renvoie l'attribut sautTextFixeApres.
     *
     * @return renvoyer l'attribut sautTextFixeApres.
     */
    public final String getSautTextFixeApres() {
        return sautTextFixeApres;
    }

    /**
     * cette méthode initialise l'attribut sautTextFixeApres par une valeur
     * donnée.
     *
     * @param valeur initialise l'attribut sautTextFixeApres.
     */
    public final void setSautTextFixeApres(final String valeur) {
        sautTextFixeApres = valeur;
    }

    /**
     * cette méthode renvoie la taille du champ.
     *
     * @return renvoyer la taille du champ.
     */
    public final String[] getTailleChamp() {
        return tailleChamp;
    }

    /**
     * cette méthode initialise la taille du champ par une valeur donnée.
     *
     * @param valeur initialise la taille du champ.
     */
    public final void setTailleChamp(final String[] valeur) {
        tailleChamp = valeur;
    }

    /**
     * cette méthode renvoie la valeur du champ de saisie Court.
     *
     * @return valeur du champ de saisie Court .
     */
    public final String[] getValeurDefautCourt() {
        return valeurDefautCourt;
    }

    /**
     * cette méthode positionne la valeur du champ de saisie Court par une
     * valeur donnée.
     *
     * @param valeur pour positionner la valeur du champ de saisie Court.
     */
    public final void setValeurDefautCourt(final String[] valeur) {
        valeurDefautCourt = valeur;
    }

    /**
     * cette méthode renvoie la valeur du champ de saisie Moyen.
     *
     * @return valeur du champ de saisie Moyen .
     */
    public final String[] getValeurDefautMoyen() {
        return valeurDefautMoyen;
    }

    /**
     * cette méthode positionne la valeur du champ de saisie Moyen par une
     * valeur donnée.
     *
     * @param valeur pour initialiser la valeur du champ de saisie Moyen
     */
    public final void setValeurDefautMoyen(final String[] valeur) {
        valeurDefautMoyen = valeur;
    }

    public final String[] getValeurDefautTresLong() {
        return valeurDefautTresLong;
    }

    public final void setValeurDefautTresLong(final String[] valeur) {
        valeurDefautTresLong = valeur;
    }

}
