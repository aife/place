package fr.paris.epm.redaction.coordination;

import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Classe implementant les méthodes exposées par l'interface ClausierFacade.
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */

public class ClausierFacadeImpl implements ClausierFacade {

    /**
     * Loggeur.
     */
    private static final Logger logger = LoggerFactory.getLogger(ClausierFacadeImpl.class);

    /**
     * Manipulateur d'information métier du clausier.
     */
    private RedactionServiceSecurise redactionService;

    /**
     * Recherche les informations de toutes les publications de la clause dont l'identifiant est passé en paramétre.
     * utilisé pour afficher la liste des versions dans la popup d'historique des clauses.
     * @param idClause identifiant de la clause éditeur pour laquelle on souhaite avoir les version de publication
     * @return une liste de EpmTPublicationClausier contenant les informations de publication.
     */
    @Deprecated
    public List<EpmTPublicationClausier> recupererPublicationsParClause(final int idClause) {
        return redactionService.chercherPublicationsParClause(idClause);
    }

    public final void setRedactionService(final RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

}