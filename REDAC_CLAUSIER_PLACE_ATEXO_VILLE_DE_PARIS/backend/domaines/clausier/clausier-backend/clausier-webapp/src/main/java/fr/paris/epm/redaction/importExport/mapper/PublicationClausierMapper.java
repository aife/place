package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface PublicationClausierMapper {

    @Mapping(source = "idReferentielFile", target = "idFileReferentiel")
    @Mapping(source = "idClausierFile", target = "idFileClausier")
    EpmTPublicationClausier toEpmTPublication(PublicationClausierBean publicationClausierBean);

    @Mapping(source = "idFileReferentiel", target = "idReferentielFile")
    @Mapping(source = "idFileClausier", target = "idClausierFile")
    PublicationClausierBean fromPublicationBean(EpmTPublicationClausier epmTPublicationClausier);

}