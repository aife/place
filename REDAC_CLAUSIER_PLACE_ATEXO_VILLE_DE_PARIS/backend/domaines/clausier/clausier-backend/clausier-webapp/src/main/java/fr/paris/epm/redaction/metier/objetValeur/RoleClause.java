package fr.paris.epm.redaction.metier.objetValeur;

import fr.paris.epm.redaction.commun.Util;

import java.io.Serializable;

/**
 * Bean de présentation de EpmTRoleClause utilisé pour la génération de l'export des clause.
 * @author Léon Barsamian
 *
 */
public class RoleClause implements Serializable {
    
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -741690516001892916L;

    /**
     * attribut valeur par defaut.
     */
    private String valeurDefaut;
    
    /**
     * attribut precoche.
     */
    private boolean precochee;
    
    /**
     * attribut numéro de formulation.
     */
    private Integer numFormulation;
    
    /**
     * attribut nombre de caractère maximal .
     */
    private int nombreCarateresMax;
    
    private String nomTableau;


    /**
     * attribut champ Obligatoire .
     */
    private String champObligatoire;
    
    private String libelleTailleChamp;

    private String valeurHeriteeModifiable;
    
    private String libelleValeurHeritee;
    
    /**
     * cette méthode renvoie le nombre de caractère maximal.
     * @return renvoyer le nombre de caractère maximal.
     */
    public int getNombreCarateresMax() {
        return nombreCarateresMax;
    }
    
    /**
     * cette méthode renvoie le numéro de formulation.
     * @return renvoyer le numéro de formulation.
     */
    public Integer getNumFormulation() {
        return numFormulation;
    }
    
    /**
     * cette méthode renvoie la valeur par defaut.
     * @return renvoyer la valeur par defaut.
     */
    public String getValeurDefaut() {
        return valeurDefaut;
    }

    /**
     * cette méthode initialise la valeur par defaut.
     */
    public void setValeurDefaut(final String valeur) {
        valeurDefaut = Util.encodeCaractere(valeur);
    }



    public final void setNumFormulation(Integer valeur) {
        this.numFormulation = valeur;
    }

    public final void setNombreCarateresMax(int valeur) {
        this.nombreCarateresMax = valeur;
    }

    public final String getNomTableau() {
        return nomTableau;
    }

    public final void setNomTableau(final String valeur) {
        this.nomTableau = valeur;
    }
    
    /**
     * cette méthode renvoie l'attribut champObligatoire.
     * @return renvoyer l'attribut champObligatoire.
     */
    public String getChampObligatoire() {
        return champObligatoire;
    }

    /**
     * cette méthode initialise l'attribut champObligatoire.
     * @param valeur pour initialiser champObligatoire.
     */
    public void setChampObligatoire(final String valeur) {
        champObligatoire = valeur;
    }

    public final String getLibelleTailleChamp() {
        return libelleTailleChamp;
    }

    public final void setLibelleTailleChamp(final String valeur) {
        this.libelleTailleChamp = valeur;
    }
    
    /**
     * cette méthode renvoie la valeur heritée modifiable.
     * @return renvoyer la valeur heritée modifiable.
     */
    public String getValeurHeriteeModifiable() {
        return valeurHeriteeModifiable;
    }

    /**
     * cette méthode initialise l'attribut valeurHeriteeModifiable.
     * @param valeur pour positionner l'attribut valeurHeriteeModifiable.
     */
    public void setValeurHeriteeModifiable(final String valeur) {
        valeurHeriteeModifiable = valeur;
    }

    public final String getLibelleValeurHeritee() {
        return libelleValeurHeritee;
    }

    public final void setLibelleValeurHeritee(final String valeur) {
        this.libelleValeurHeritee = valeur;
    }

    public boolean isPrecochee() {
        return precochee;
    }

    public void setPrecochee(boolean precochee) {
        this.precochee = precochee;
    }
}
