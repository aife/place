package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmVCanevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Canevas Mapper
 * Created by nty on 04/07/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Mapper(componentModel = "spring", uses = {HtmlSanitizerMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class})
public interface CanevasMapper {

    @Mapping(source = "idCanevas", target = "id")
    @Mapping(source = "idCanevas", target = "idCanevas")
    @Mapping(source = "reference", target = "referenceCanevas")
    @Mapping(source = "idNaturePrestation", target = "idNaturePrestation")
    @Mapping(source = "idNaturePrestation", target = "labelNaturePrestation", qualifiedByName = {"DirectoryNamedMapper", "labelNaturePrestationById"})
    @Mapping(source = "epmTRefStatutRedactionClausier", target = "idStatutRedactionClausier")
    @Mapping(source = "epmTRefTypeDocument", target = "idTypeDocument")
    @Mapping(source = "epmTRefTypeDocument", target = "labelTypeDocument")
    @Mapping(source = "epmTRefTypeDocument.codeExterne", target = "codeTypeDocument")
    @Mapping(source = "epmTRefTypeContrats", target = "idsTypeContrats")
    @Mapping(source = "epmTRefTypeContrats", target = "labelsTypeContrats", qualifiedByName = {"DirectoryNamedMapper", "shortLabelOfTypeContrat"})
    @Mapping(source = "epmTRefTypeContrats", target = "typesContrat", qualifiedByName = {"DirectoryNamedMapper", "directoryListOfTypeContratSet"})
    @Mapping(source = "epmTRefProcedures", target = "idsProcedures")
    @Mapping(source = "epmTRefProcedures", target = "labelsProcedures", qualifiedByName = {"DirectoryNamedMapper", "shortLabelOfProcedureEditeur"})
    @Mapping(source = "epmTRefProcedures", target = "procedures", qualifiedByName = {"DirectoryNamedMapper", "directoryListOfProcedureSet"})
    @Mapping(source = "idRefCCAG", target = "ccag", qualifiedByName = {"DirectoryNamedMapper", "ccagDirectoryBeanById"})
    @Mapping(source = "idRefCCAG", target = "idRefCCAG")
    public abstract CanevasBean toCanevasBean(EpmTCanevasAbstract epmTCanevas);

    @Mapping(source = "idCanevas", target = "id")
    @Mapping(source = "referenceCanevas", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    @Mapping(source = "idsProcedures", target = "epmTRefProcedures")
    public abstract EpmTCanevas toEpmTCanevas(CanevasBean canevasBean);

    @Mapping(source = "idCanevas", target = "id")
    @Mapping(source = "referenceCanevas", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    @Mapping(source = "idsProcedures", target = "epmTRefProcedures")
    public abstract void toEpmTCanevas(CanevasBean canevasBean, @MappingTarget EpmTCanevas epmTCanevas);

    @Mapping(source = "referenceCanevas", target = "reference")
    @Mapping(source = "idStatutRedactionClausier", target = "epmTRefStatutRedactionClausier")
    @Mapping(source = "idTypeDocument", target = "epmTRefTypeDocument")
    @Mapping(source = "idsTypeContrats", target = "epmTRefTypeContrats")
    @Mapping(source = "idsProcedures", target = "epmTRefProcedures")
    public abstract void toEpmVCanevas(CanevasBean canevasBean, @MappingTarget EpmVCanevas epmVCanevs);


    @Mapping(target = "idNaturePrestation", source = "naturePrestation")
    @Mapping(target = "reference", source = "referenceCanevas")
    @Mapping(target = "actif", source = "statut")
    @Mapping(target = "epmTRefStatutRedactionClausier.id", source = "idStatutRedactionClausier")
    @Mapping(target = "epmTRefTypeDocument.idTypePassation", source = "idTypePassation")
    void toEpmTCanevas(Canevas canevas, @MappingTarget EpmTCanevas epmTCanevas);

    @Mapping(source = "idNaturePrestation", target = "naturePrestation")
    @Mapping(source = "reference", target = "referenceCanevas")
    @Mapping(source = "actif", target = "statut")
    @Mapping(source = "epmTRefStatutRedactionClausier.id", target = "idStatutRedactionClausier")
    @Mapping(source = "epmTRefTypeDocument.idTypePassation", target = "idTypePassation")
    Canevas toCanevas(EpmTCanevasAbstract epmTCanevasAbstract);



}
