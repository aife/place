<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--@elvariable id="epmTClauseInstance" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>
<%--@elvariable id="frmClauseListeChoixCumulatif" type="fr.paris.epm.redaction.presentation.forms.ClauseListeChoixCumulatifForm"--%>

<div class="form-bloc">
    <div class="top">
        <span class="left"></span><span class="right"></span>
    </div>

    <div class="content">

        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <%-- TODO: NIKO REVOIR
                <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">
                    <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                </c:if>
                --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiser();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>

        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.texteFixeAvant" />
            </span>
            <html:textarea styleId="textFixeAvant" property="textFixeAvant" title="Texte fixe avant"
                           cols="" rows="6" styleClass="texte-long mceEditor"
                           errorStyleClass="error-border" />
        </div>
        <div class="line">
            <div class="retour-ligne">
                <html:checkbox styleId="sautTextFixeAvant" property="sautTextFixeAvant" value="true" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>

        <!--Fin Text fixe avant-->
        <!--Debut tableau formulation-->
        <div class="clause-choix-list">
            <table id="table-formulation">
                <thead>
                <tr>
                    <th class="num-formulation">
                        <bean:message key="ClauseListeChoixCumulatif.txt.numFormulation" />
                    </th>
                    <th class="nb-carac">
                        <bean:message key="ClauseListeChoixCumulatif.txt.tailleChamp" />
                    </th>
                    <th class="long-col">
                        <bean:message key="ClauseListeChoixCumulatif.txt.valeurParDefaut" />
                    </th>
                    <th class="check-col">
                        <bean:message key="ClauseListeChoixCumulatif.txt.precochee" />
                    </th>
                    <th class="actions"></th>
                </tr>
                </thead>

                <c:choose>
                    <c:when test="${empty frmClauseListeChoixCumulatif.formulationCollection}">
                        <script type="text/javascript">
                            var compteur = 2;
                        </script>
                        <tr id="tr_1">
                            <td class="num-formulation">
                                <html:text property="numFormulation" styleId="numFormulation" title="Numéro de formulation" value="1"/>
                            </td>
                            <td class="nb-carac">
                                <html:select property="tailleChamp" styleId="tailleChamp" styleClass="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut0_', 'mceEditor');" value="1">
                                    <html:option value="4">
                                        <bean:message key="redaction.taille.champ.tresLong" />
                                    </html:option>
                                    <html:option value="1">
                                        <bean:message key="redaction.taille.champ.long" />
                                    </html:option>
                                    <html:option value="2">
                                        <bean:message key="redaction.taille.champ.moyen" />
                                    </html:option>
                                    <html:option value="3">
                                        <bean:message key="redaction.taille.champ.court" />
                                    </html:option>
                                </html:select>
                            </td>

                            <td class="long-col">
                                <html:textarea  property="valeurDefautTresLong"  title="Valeur par defaut" styleClass="texte-tres-long mceEditor" styleId="valeurDefaut0_4"  cols="" rows="6" style="display:none"/>
                                <html:textarea  property="valeurDefaut"  title="Valeur par defaut" styleClass="texte-long mceEditor" styleId="valeurDefaut0_1"  cols="" rows="6" style="display:block"/>
                                <html:text property="valeurDefautMoyen"  title="Valeur par defaut" styleClass="texte-moyen" styleId="valeurDefaut0_2"  style="display:none" />
                                <html:text property="valeurDefautCourt"  title="Valeur par defaut" styleClass="texte-court" styleId="valeurDefaut0_3" style="display:none" />
                            </td>

                            <td class="check-col" style="text-align: center">
                                <html:checkbox property="precochee" styleId="precochee" value="0" />
                            </td>
                            <td class="actions">
                                <a href="javascript:removeRowFromTable(document.getElementById('tr_1').rowIndex,'table-formulation')">
                                    <img src="<atexo:href href='images/picto-poubelle.gif'/>" alt="Supprimer la formulation" title="Supprimer la formulation" />
                                </a>
                            </td>
                        </tr>

                        <nested:equal property="tailleChamp" value="0">
                            <script>
                                mettreTailleChampValeurDefaut('tailleChamp');
                            </script>
                        </nested:equal>
                    </c:when>

                    <c:otherwise>
                        <script type="text/javascript">
                            <bean:size id="sizeList" name="frmClauseListeChoixCumulatif" property="formulationCollection"/>
                            var compteur = ${sizeList} + 1;
                        </script>
                        <c:forEach var="clauseRole" items="${frmClauseListeChoixCumulatif.formulationCollection}" varStatus="status">
                            <c:set var="style" />
                            <c:if test="${status.index % 2 == 0}">
                                <c:set var="style">on</c:set>
                            </c:if>

                            <tr class="${style}" id="tr_${status.index + 1}">
                                <td class="num-formulation">
                                    <input type="text" name="numFormulation" value="${clauseRole.numFormulation}" title="Numéro de formulation" />
                                </td>
                                <td class="nb-carac">
                                    <div class="radio-choice-small">
                                        <select id="tailleChamp_${status.index + 1}" name="tailleChamp" class="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut${index}_', 'mceEditor');">
                                            <option value="4" <c:if test="${clauseRole.nombreCarateresMax == 4}">selected="selected"</c:if>>
                                                <bean:message key="redaction.taille.champ.tresLong" />
                                            </option>
                                            <option value="1" <c:if test="${clauseRole.nombreCarateresMax == 1}">selected="selected"</c:if>>
                                                <bean:message key="redaction.taille.champ.long" />
                                            </option>
                                            <option value="2" <c:if test="${clauseRole.nombreCarateresMax == 2}">selected="selected"</c:if>>
                                                <bean:message key="redaction.taille.champ.moyen" />
                                            </option>
                                            <option value="3" <c:if test="${clauseRole.nombreCarateresMax == 3}">selected="selected"</c:if>>
                                                <bean:message key="redaction.taille.champ.court" />
                                            </option>
                                        </select>
                                    </div>
                                </td>

                                <td class="long-col">
                                    <c:set var="styleDisplayTresLong" value="none"/>
                                    <c:set var="styleDisplayLong" value="none"/>
                                    <c:set var="styleDisplayMoyen" value="none"/>
                                    <c:set var="styleDisplayCourt" value="none"/>
                                    <c:choose>
                                        <c:when test="${clauseRole.nombreCarateresMax == 4}">
                                            <c:set var="styleDisplayTresLong" value="block"/>
                                        </c:when>
                                        <c:when test="${clauseRole.nombreCarateresMax == 3}">
                                            <c:set var="styleDisplayMoyen" value="block"/>
                                        </c:when>
                                        <c:when test="${clauseRole.nombreCarateresMax == 2}">
                                            <c:set var="styleDisplayCourt" value="block"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="styleDisplayLong" value="block"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:set var="valTmp"><c:out value="${clauseRole.valeurDefaut}"/></c:set>
                                    <textarea name="valeurDefautTresLong"  title="Valeur par defaut" class="texte-tres-long mceEditor"  id="valeurDefaut${status.index}_4" cols="" rows="6" style="display:${styleDisplayTresLong}">${valTmp}</textarea>
                                    <textarea name="valeurDefaut"  title="Valeur par defaut" class="texte-long mceEditor"  id="valeurDefaut${status.index}_1" cols="" rows="6" style="display:${styleDisplayLong}">${valTmp}</textarea>
                                    <input type="text" value="${valTmp}" name="valeurDefautCourt"  title="Valeur par defaut" class="texte-court" id="valeurDefaut${status.index}_3" style="display:${styleDisplayMoyen}" />
                                    <input type="text" value="${valTmp}" name="valeurDefautMoyen" title="Valeur par defaut" class="texte-moyen" id="valeurDefaut${status.index}_2"  style="display:${styleDisplayCourt}" />
                                </td>
                                <td class="check-col" style="text-align: center">
                                    <input type="checkbox" name="precochee" value="${status.index}"
                                            <c:if test="${clauseRole.precochee == 1}"> checked="checked"</c:if> />
                                </td>
                                <td class="actions">
                                    <a href="javascript:removeRowFromTable(document.getElementById('tr_${status.index + 1}').rowIndex,'table-formulation')">
                                        <img src="<atexo:href href='images/picto-poubelle.gif'/>" alt="Supprimer la formulation" title="Supprimer la formulation" /> </a>
                                </td>
                            </tr>

                            <nested:equal name="clauseRole"  property="nombreCarateresMax" value="0">
                                <script>
                                    var idTailleChamp = '<c:out value="tailleChamp_${status.index + 1}" />';
                                    mettreTailleChampValeurDefaut(idTailleChamp);
                                </script>
                            </nested:equal>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </table>
            <a href="javascript:AddRowTableCumulatif('table-formulation')" class="ajout-choix">
                <bean:message key="ClauseListeChoixCumulatif.btn.ajouterFormulation" />
            </a>
        </div>
        <div class="breaker"></div>
        <div class="column">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiable" />
            </span>
            <div class="radio-choice">
                <html:radio property="formulationModifiable" title="Oui" value="1" styleId="formulationModifiable1"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiableOui" />
            </div>
            <div class="radio-choice">
                <html:radio property="formulationModifiable" title="Non" value="0" styleId="formulationModifiable2"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.formulationModifiableNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirection" />
            </span>
            <div class="radio-choice">
                <html:radio property="parametrableDirection" title="Oui" value="1" styleId="parametrableDirection1"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirectionOui" />
            </div>
            <div class="radio-choice">
                <html:radio property="parametrableDirection" title="Non" value="0" styleId="parametrableDirection2"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableDirectionNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgent" />
            </span>
            <div class="radio-choice">
                <html:radio property="parametrableAgent" title="Oui" value="1" styleId="parametrableAgent1"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgentOui" />
            </div>
            <div class="radio-choice">
                <html:radio property="parametrableAgent" title="Non" value="0" styleId="parametrableAgent2"/>
                <bean:message key="ClauseListeChoixCumulatif.txt.parametrableAgentNon" />
            </div>
        </div>
        <div class="line">
            <div class="retour-ligne">
                <html:checkbox property="sautTextFixeApres" value="true" title="Saut de ligne" styleId="sautTextFixeApres"/>
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>
        <!--Debut Bloc texte fixe apres-->
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixCumulatif.txt.texteFixeApres" />
            </span>
            <html:textarea property="textFixeApres" title="Texte fixe après" styleId="textFixeApres"
                           cols="" rows="6" styleClass="texte-long mceEditor"></html:textarea>
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <%-- TODO: NIKO REVOIR
                <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">
                    <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                </c:if>
                --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiser();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>" />
            </a>
        </div>
        <!--Fin Bloc texte fixe apres-->
        <div class="breaker"></div>
    </div>
    <div class="bottom">
        <span class="left"></span><span class="right"></span>
    </div>
</div>

<script type="text/javascript">
    initEditeursTexteRedaction();
</script>
