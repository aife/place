package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasPub;
import fr.paris.epm.redaction.importExport.bean.CanevasImportExport;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryNamedMapper;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {ChapitrePubMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class})
public abstract class CanevasPubMapper {

    @Mapping(source = "idNaturePrestation", target = "naturePrestation", qualifiedByName = {"DirectoryNamedMapper", "uidNaturePrestationFromId"})
    @Mapping(source = "epmTRefStatutRedactionClausier.uid", target = "statutRedactionClausier")
    @Mapping(source = "idRefCCAG", target = "ccag", qualifiedByName = {"DirectoryNamedMapper", "uidCCAGFromId"})
    @Mapping(source = "epmTRefTypeDocument.uid", target = "document")
    @Mapping(source = "epmTRefTypeContrats", target = "contrats", qualifiedByName = {"DirectoryNamedMapper", "uidsFromContrats"})
    @Mapping(source = "epmTRefProcedures", target = "procedures", qualifiedByName = {"DirectoryNamedMapper", "uidsFromProcedures"})
    @Mapping(source = "epmTChapitres", target = "chapitres")
    public abstract CanevasImportExport toCanevasImportExport(EpmTCanevasPub epmTCanevasPub);

    @Mapping(source = "statutRedactionClausier", target = "epmTRefStatutRedactionClausier", qualifiedByName = {"DirectoryNamedMapper", "statutRedactionClausierFromUid"})
    @Mapping(source = "document", target = "epmTRefTypeDocument", qualifiedByName = {"DirectoryNamedMapper", "typeDocumentFromUid"})
    @Mapping(source = "contrats", target = "epmTRefTypeContrats", qualifiedByName = {"DirectoryNamedMapper", "typesContratsFromUids"})
    @Mapping(source = "procedures", target = "epmTRefProcedures", qualifiedByName = {"DirectoryNamedMapper", "proceduresFromUids"})
    @Mapping(source = "chapitres", target = "epmTChapitres")
    @Mapping(source = "ccag", target = "idRefCCAG", qualifiedByName = {"DirectoryNamedMapper", "idCCAGFromUid"})
    @Mapping(source = "naturePrestation", target = "idNaturePrestation", qualifiedByName = {"DirectoryNamedMapper", "idNaturePrestationFromLibelle"})
    public abstract EpmTCanevasPub toEpmTCanevasPub(CanevasImportExport canevasImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTCanevasPub setIdPublication(@MappingTarget EpmTCanevasPub epmTCanevasPub, @Context Integer idPublication) {
        epmTCanevasPub.setIdPublication(idPublication);
        return epmTCanevasPub;
    }

}
