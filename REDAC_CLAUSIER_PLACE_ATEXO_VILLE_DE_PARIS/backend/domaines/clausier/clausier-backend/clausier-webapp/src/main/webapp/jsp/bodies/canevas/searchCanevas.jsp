<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 08/09/18
  Time: 00:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="searchInstance" type="fr.paris.epm.redaction.presentation.bean.CanevasBean"--%>
<%--@elvariable id="allNatures" type="java.util.List"--%>
<%--@elvariable id="allTypeAuteurs" type="java.util.List"--%>
<%--@elvariable id="allStatutsRedactionClausier" type="java.util.List"--%>
<%--@elvariable id="allTypesDocument" type="java.util.List"--%>
<%--@elvariable id="allTypesContrat" type="java.util.List"--%>
<%--@elvariable id="allProcedures" type="java.util.List"--%>
<%--@elvariable id="editeur" type="java.lang.Boolean"--%>
<form:form modelAttribute="searchInstance" action="searchCanevas.htm" cssClass="form-horizontal">
    <div class="atx-panel panel panel-default">

        <jsp:include page="../panelHeadingRecherche.jsp" />

        <div id="panel-critres-recherche" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true">
            <div class="panel-body">
                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <form:label path="referenceCanevas" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.referenceCanevas" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:input path="referenceCanevas" cssClass="form-control input-sm" />
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="referenceClause" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.referenceClause" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:input path="referenceClause" cssClass="form-control input-sm" />
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="titre" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.titre" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:input path="titre" cssClass="form-control input-sm" />
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="idNaturePrestation" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.naturePrestation" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="idNaturePrestation" cssClass="form-control input-sm">
                                    <form:option value="0"><spring:message code="redaction.toutes" /></form:option>
                                    <form:options items="${allNatures}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>

                        <c:if test="${!editeur}">
                            <div class="form-group">
                                <form:label path="typeAuteur" cssClass="control-label col-md-3">
                                    <spring:message code="redaction.canevas.auteur" /> :
                                </form:label>
                                <div class="col-md-9">
                                    <form:select path="typeAuteur" cssClass="form-control input-sm">
                                        <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                                        <form:options items="${allTypeAuteurs}" itemValue="id" itemLabel="label" />
                                    </form:select>
                                </div>
                            </div>
                        </c:if>

                        <div class="form-group">
                            <form:label path="idStatutRedactionClausier" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.statut" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="idStatutRedactionClausier" cssClass="form-control input-sm">
                                    <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                                    <form:options items="${allStatutsRedactionClausier}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <form:label path="idTypeDocument" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.typeDocument" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="idTypeDocument" cssClass="form-control input-sm">
                                    <%--<form:option value="0"><spring:message code="redaction.tous" /></form:option>--%>
                                    <form:options items="${allTypesDocument}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="idTypeContrat" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.typeContrat" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="idTypeContrat" cssClass="form-control input-sm">
                                    <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                                    <form:options items="${allTypesContrat}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="idProcedure" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.procedurePassation" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="idProcedure" cssClass="form-control input-sm">
                                    <form:option value="0"><spring:message code="redaction.toutes" /></form:option>
                                    <form:options items="${allProcedures}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group">
                            <form:label path="actif" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.afficherActif" /> :
                            </form:label>
                            <div class="col-md-9">
                                <form:select path="actif" cssClass="form-control input-sm">
                                    <form:option value=""><spring:message code="redaction.toutes" /></form:option>
                                    <form:option value="true"><spring:message code="redaction.actives" /></form:option>
                                    <form:option value="false"><spring:message code="redaction.inactives" /></form:option>
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <form:label path="dateModificationMin" cssClass="control-label col-md-3">
                                <spring:message code="redaction.canevas.dateModification" /> :
                            </form:label>
                            <form:label path="dateModificationMin" cssClass="control-label col-md-2">
                                <spring:message code="redaction.entreLe" /> :
                            </form:label>
                            <div class="col-md-3">
                                <div class="input-group date datetimepicker datetimepicker-date" data-provide="datepicker">
                                    <form:input path="dateModificationMin" class="form-control datepicker" data-date-format="DD/MM/YYYY"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <form:label path="dateModificationMax" cssClass="control-label col-md-1">
                                <spring:message code="redaction.etLe" /> :
                            </form:label>
                            <div class="col-md-3">
                                <div class="input-group date datetimepicker datetimepicker-date" data-provide="datepicker">
                                    <form:input path="dateModificationMax" class="form-control datepicker" data-date-format="DD/MM/YYYY"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <button type="reset" class="btn btn-warning btn-sm pull-left">
                        <spring:message code="redaction.action.reset"/>
                    </button>

                    <button type="submit" class="btn btn-primary btn-sm pull-right">
                        <spring:message code="redaction.action.rechercher" />
                    </button>
                </div>
            </div>
        </div>
    </div>
</form:form>
