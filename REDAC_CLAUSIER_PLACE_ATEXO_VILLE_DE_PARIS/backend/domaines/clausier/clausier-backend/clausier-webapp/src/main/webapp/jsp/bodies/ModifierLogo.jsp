<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/modifierLogo.js"></script>

<html:form action="/modifierLogoProcess.epm" method="post" styleId="modifierLogoForm" enctype="multipart/form-data">

	<div class="main-part">
		<div class="breadcrumbs">
			<bean:message key="modifierLogo.titre" />
		</div>
		<div class="breaker"></div>
	
	        <!--Debut Bloc -->
	        <div class="form-saisie">
	            <div class="form-bloc">
	                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	                <div class="content toggle">
	                    <div class="title"><bean:message key="modifierLogo.image.pagegarde" /></div>
	                    <div class="spacer"></div>
	                    <div class="thumbnail"> 
	                    
							<c:if test="${imageLogoBase64 != null}">
								<img src="data:image/${imageLogoType};base64,${imageLogoBase64}"/>
							</c:if>
							<c:if test="${imageLogoBase64 == null}">
								<bean:message key="modifierLogo.aucuneImage" />
							</c:if>
							
	                    </div>
	                    <div> 
	                    	<a class="float-left button button-moyen3 align-left" href="javascript:popUpPrevisualiserLogo();"><span class="button-apercu-dynamique">&nbsp;</span><bean:message key="modifierLogo.previsualiser" /></a>
	                        <div class="float-right"> 
	                        	<a href="javascript:supprimerLogo();"><img alt="Supprimer" src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"></a> 
	                        	<a title="Modification du logo" class="bouton-action" onclick="overlay();" href="javascript:void(0);"><img src="<atexo:href href='images/bouton-modifier.gif'/>" alt="Modifier"></a> 
	                        </div>
	                    </div>
	                    <div class="spacer-small"></div>
	                    <div class="breaker"></div>
	                </div>
	                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	            </div>
	        </div>
	        <!--Fin Bloc -->
	        <div class="spacer"></div>
	        <!--Debut boutons-->
	        <div class="boutons"> <a class="valider" href="confirmerModificationLogo.epm"><bean:message key="modifierLogo.valider" /></a> </div>
	        <!--Fin boutons--> 
	</div>
 
 
	<div id="overlay">
	    <div style="position: absolute; top: 150.5px; left: 547px;" id="container" class="popup-moyen">
	        <div class="handle"></div>
	        <div class="layerBorder-moyen">
	            <iframe frameborder="0" id="floatingIframe" scrolling="no"></iframe>
	            <h4>Modification du logo</h4>
	            
	            <logic:messagesPresent>
		            <script type="text/javascript">
		           		overlay();
		            </script>
	            
					<div class="form-bloc-erreur msg-erreur">
						<div class="top">
							<span class="left"></span><span class="right"></span>
						</div>
						
						<div class="content">
							<div class="title"><bean:message key="erreur.texte.generique"/></div>
					        <html:errors  />
						</div>
						<div class="breaker"></div>
						<div class="bottom">
							<span class="left"></span><span class="right"></span>
						</div>
					</div>
				</logic:messagesPresent>
	            
	            <div class="form-bloc-erreur msg-info">
	                <div class="content no-bg">
	                    <p><bean:message key="modifierLogo.formatsAcceptes" /><br>
	                       <bean:message key="modifierLogo.tailleFichierMax" /></p>
	                </div>
	            </div>
	            <div style="display:none;" class="thumbnail" id="imagePreview"></div>
	            
	            <html:file styleId="imageInput" property="fichierLogo" size="55" styleClass="btn" onchange="loadImageFile();" />
	            
	            <div class="boutons"> 
	            	<a class="annuler" href="javascript:overlay();"><bean:message key="modifierLogo.annuler" /></a> 
	            	<a class="valider" href="javascript:preAjouterModificationLogo();"><bean:message key="modifierLogo.valider" /></a> 
	            </div>
	        </div>
	    </div>
	</div>

    <input name="aiguillage" id="aiguillage" style="display:none">

</html:form>

<script type="text/javascript">
	new Draggable ('container',{ revert : false , ghosting : false, handle : 'handle' } );
	autoPositionLayer();
</script>