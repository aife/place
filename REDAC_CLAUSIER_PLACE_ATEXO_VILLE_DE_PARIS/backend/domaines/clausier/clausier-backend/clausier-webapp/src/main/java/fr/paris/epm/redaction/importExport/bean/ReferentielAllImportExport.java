package fr.paris.epm.redaction.importExport.bean;

import java.util.ArrayList;
import java.util.List;

public class ReferentielAllImportExport {

    private String version;

    private String datePublication;

    private String editeur;

    private String commentaire;

    private List<ReferentielImportExport> contrats = new ArrayList<>();

    private List<ReferentielImportExport> procedures = new ArrayList<>();

    private List<ReferentielImportExport> refsPotentiellementConditionnee = new ArrayList<>();

    private List<ReferentielImportExport> statutsRedactionClausier = new ArrayList<>();

    private List<ReferentielImportExport> themesClause = new ArrayList<>();

    private List<ReferentielImportExport> typesClause = new ArrayList<>();

    private List<ReferentielImportExport> typesDocument = new ArrayList<>();

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public List<ReferentielImportExport> getContrats() {
        return contrats;
    }

    public void setContrats( List<ReferentielImportExport> contrats ) {
        this.contrats = contrats;
    }

    public List<ReferentielImportExport> getProcedures() {
        return procedures;
    }

    public void setProcedures( List<ReferentielImportExport> procedures ) {
        this.procedures = procedures;
    }

    public List<ReferentielImportExport> getRefsPotentiellementConditionnee() {
        return refsPotentiellementConditionnee;
    }

    public void setRefsPotentiellementConditionnee( List<ReferentielImportExport> refsPotentiellementConditionnee ) {
        this.refsPotentiellementConditionnee = refsPotentiellementConditionnee;
    }

    public List<ReferentielImportExport> getStatutsRedactionClausier() {
        return statutsRedactionClausier;
    }

    public void setStatutsRedactionClausier( List<ReferentielImportExport> statutsRedactionClausier ) {
        this.statutsRedactionClausier = statutsRedactionClausier;
    }

    public List<ReferentielImportExport> getThemesClause() {
        return themesClause;
    }

    public void setThemesClause( List<ReferentielImportExport> themesClause ) {
        this.themesClause = themesClause;
    }

    public List<ReferentielImportExport> getTypesClause() {
        return typesClause;
    }

    public void setTypesClause( List<ReferentielImportExport> typesClause ) {
        this.typesClause = typesClause;
    }

    public List<ReferentielImportExport> getTypesDocument() {
        return typesDocument;
    }

    public void setTypesDocument( List<ReferentielImportExport> typesDocument ) {
        this.typesDocument = typesDocument;
    }

}
