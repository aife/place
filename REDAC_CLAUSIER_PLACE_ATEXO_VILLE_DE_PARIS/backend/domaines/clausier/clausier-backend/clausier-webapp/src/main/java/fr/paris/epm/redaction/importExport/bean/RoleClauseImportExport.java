package fr.paris.epm.redaction.importExport.bean;

public class RoleClauseImportExport {

    private int idRoleClause;

    private String valeurDefaut;

    private String precochee;

    private String etat;

    private Integer nombreCarateresMax;

    private String champObligatoire;

    private String valeurHeriteeModifiable;

    private Integer numFormulation;

    public int getIdRoleClause() {
        return idRoleClause;
    }

    public void setIdRoleClause(int idRoleClause) {
        this.idRoleClause = idRoleClause;
    }

    public String getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }


    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public Integer getNombreCarateresMax() {
        return nombreCarateresMax;
    }

    public void setNombreCarateresMax(Integer nombreCarateresMax) {
        this.nombreCarateresMax = nombreCarateresMax;
    }

    public String getChampObligatoire() {
        return champObligatoire;
    }

    public void setChampObligatoire(String champObligatoire) {
        this.champObligatoire = champObligatoire;
    }

    public String getValeurHeriteeModifiable() {
        return valeurHeriteeModifiable;
    }

    public void setValeurHeriteeModifiable(String valeurHeriteeModifiable) {
        this.valeurHeriteeModifiable = valeurHeriteeModifiable;
    }

    public Integer getNumFormulation() {
        return numFormulation;
    }

    public void setNumFormulation(Integer numFormulation) {
        this.numFormulation = numFormulation;
    }

    public String isPrecochee() {
        return precochee;
    }

    public void setPrecochee(String precochee) {
        this.precochee = precochee;
    }

}
