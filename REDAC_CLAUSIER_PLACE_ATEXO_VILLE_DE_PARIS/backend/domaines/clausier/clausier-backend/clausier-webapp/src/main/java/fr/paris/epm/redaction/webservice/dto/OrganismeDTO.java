package fr.paris.epm.redaction.webservice.dto;

public class OrganismeDTO {
    private Long id;

    private String libelle;

    private String acronyme;

    private String plateforme;

	public OrganismeDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}

	public String getAcronyme() {
		return acronyme;
	}

	public void setAcronyme( String acronyme ) {
		this.acronyme = acronyme;
	}

	public String getPlateforme() {
		return plateforme;
	}

	public void setPlateforme( String plateforme ) {
		this.plateforme = plateforme;
	}
}
