package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefPotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur;

import java.util.List;

public interface PotentiellementConditionneeService {

    List<PotentiellementConditionneeValeur> getListValeurById(int id, Integer idOrganisme);

    /**
     * Permet de lister l'ensemble des valeurs de potentiellement conditionnée.
     * @return List
     * @throws TechnicalNoyauException erreur technique
     */
    List lister(Integer idOrganisme);

    public List<PotentiellementConditionneeValeur> getProceduresPassationParOrganisme(Integer idOrganisme);

    public List<PotentiellementConditionneeValeur> getListValeurs(EpmTRefPotentiellementConditionnee epmtPotConditionnee);

    public EpmTRefPotentiellementConditionnee charger(final int id);

}
