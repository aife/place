package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionForm;

import java.util.Collection;

/**
 * Formulaire ParametrageClauseListeChoix.
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ParametrageClauseListeChoixForm extends ActionForm {
    /**
     * Texte fixe avant.
     */
    private String texteFixeAvant = "";
    /**
     * Texte fixe après.
     */
    private String texteFixeApres = "";
    /**
     * marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut valeurs de taille champ par default.
     */
    private String[] nbCaracDefault;
//    /**
//     * Attribut paramètre Agent.
//     */
//    private String parametreAgent;
    /**
     * Attribut type de la clause.
     */
    private String typeClause;
    /**
     * Attribut référence de la clause.
     */
    private String reference;
    /**
     * Attribut paramètre de direction.
     */
    private String parametre;
    /**
     * Attribut numéro de formulation par default.
     */

    private String[] numFormulationDefault;
    /**
     * Attribut valeurs par defaut.
     */
    private String[] valeurDefaut;
    /**
     * Attribut valeurs precochée.
     */
    private String[] precocheeDefault;
    /**
     * Attribut numéro de formulation de la direction.
     */
    private String[] numFormulation;
    /**
     * Attribut taille champ direction (Long,Moyen,Court).
     */

    private String[] nbCarac;
    /**
     * Attribut Valeur par Default de direction.
     */

    private String[] valeur;
    /**
     * Attribut valeur precochée direction.
     */
    private String[] precochee;
    /**
     * Attribut collection de formulations.
     */
    private Collection formulationCollection;

    /**
     * Constructeur de classe ParametrageClauseListeChoixForm.
     */
    public ParametrageClauseListeChoixForm() {
        reset();
    }

    /**
     * permet d'initialiser le formulaire ParametrageClauseListeChoixForm.
     */
    public void reset() {
        typeClause = null;
        reference = null;
        numFormulationDefault = null;
        nbCaracDefault = null;
        valeurDefaut = null;
        precocheeDefault = null;

        formulationCollection = null;
    }



    /**
     * @return taille du champ par default.
     */
    public final String[] getNbCaracDefault() {
        return nbCaracDefault;
    }

    /**
     * @param valeur initialise la taille du champ par default.
     */
    public final void setNbCaracDefault(final String[] valeur) {
        nbCaracDefault = valeur;
    }

    /**
     * @return taille champ direction .
     */
    public final String[] getNbCarac() {
        return nbCarac;
    }

    /**
     * @param valeur positionne la taille du champ direction.
     */
    public final void setNbCarac(final String[] valeur) {
        nbCarac = valeur;
    }


    /**
     * @return  numéro de formulation par default.
     */
    public final String[] getNumFormulationDefault() {
        return numFormulationDefault;
    }

    /**
     * @param valeur initialise le numéro de formulation par default.
     */
    public final void setNumFormulationDefault(final String[] valeur) {
        numFormulationDefault = valeur;
    }

    /**
     * @return  numéro de formulation direction.
     */
    public final String[] getNumFormulationDirection() {
        return numFormulation;
    }

    /**
     * @param valeur positionne le numéro de formulation direction.
     */
    public final void setNumFormulationDirection(final String[] valeur) {
        numFormulation = valeur;
    }

    /**
     * @return valeur precochee par Default .
     */
    public final String[] getPrecocheeDefault() {
        return precocheeDefault;
    }

    /**
     * @param valeur initialise la valeur precochee par Default .
     */
    public final void setPrecocheeDefault(final String[] valeur) {
        precocheeDefault = valeur;
    }

    /**
     * @return valeur precochee direction .
     */
    public final String[] getPrecochee() {
        return precochee;
    }

    /**
     * @param valeur positionne la valeur precochee direction.
     */
    public final void setPrecochee(final String[] valeur) {
        precochee = valeur;
    }

    /**
     * @return valeur par Defaut.
     */
    public final String[] getValeurDefaut() {
        return valeurDefaut;
    }

    /**
     * @param valeur pour positionner la valeur par Defaut.
     */
    public final void setValeurDefaut(final String[] valeur) {
        valeurDefaut = valeur;
    }

    /**
     * @return valeur direction .
     */
    public final String[] getValeur() {
        return valeur;
    }

    /**
     * @param valeur positionne la valeur direction.
     */
    public final void setValeur(final String[] val) {
        valeur = val;
    }

    /**
     * @return référence .
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @param valeur positionne la référence.
     */
    public final void setReference(final String valeur) {
        reference = valeur;
    }

    /**
     * @return type de la  clause.
     */
    public final String getTypeClause() {
        return typeClause;
    }

    /**
     * @param valeur positionne le type de la  clause.
     */
    public final void setTypeClause(final String valeur) {
        typeClause = valeur;
    }

    /**
     * @return parametre Direction.
     */
    public final String getParametre() {
        return parametre;
    }

    /**
     * @param valeur positionne le parametre Direction.
     */
    public final void setParametre(final String valeur) {
        parametre = valeur;
    }

    /**
     * @return Collection de formulations.
     */
    public final Collection getFormulationCollection() {
        return formulationCollection;
    }

    /**
     * @param valeur postionne une collection de formulations.
     */
    public final void setFormulationCollection(final Collection valeur) {
        formulationCollection = valeur;
    }

    /**
     * @return  texteFixeAvant
     */
    public final String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    /**
     * @param valeur pour initialiser l'attribut texteFixeAvant.
     */
    public final void setTexteFixeAvant(final String valeur) {
        texteFixeAvant = valeur;
    }

    /**
     * @return  texteFixeApres
     */
    public final String getTexteFixeApres() {
        return texteFixeApres;
    }

    /**
     * @param valeur positionne texteFixeApres.
     */
    public final void setTexteFixeApres(final String valeur) {
        texteFixeApres = valeur;
    }

}
