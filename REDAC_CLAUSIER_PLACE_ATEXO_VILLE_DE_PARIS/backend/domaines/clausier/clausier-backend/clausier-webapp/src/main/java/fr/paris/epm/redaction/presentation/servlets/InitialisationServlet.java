/**
 * $Id$
 */
package fr.paris.epm.redaction.presentation.servlets;

import fr.paris.epm.global.commun.SessionManagerGlobal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */
public final class InitialisationServlet extends HttpServlet implements
        HttpSessionListener {

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory
            .getLogger(InitialisationServlet.class);


    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**
     * 
     */
    public void sessionCreated(HttpSessionEvent event) {
        LOG.debug("session cree " + event.getSession().getId());
    }

    /**
     * 
     */
    public void sessionDestroyed(HttpSessionEvent event) {
        LOG.debug("session detruite " + event.getSession().getId());
        SessionManagerGlobal sessionManager = SessionManagerGlobal
                .getInstance();
        sessionManager.nettoyerFichier(event.getSession());
    }

}
