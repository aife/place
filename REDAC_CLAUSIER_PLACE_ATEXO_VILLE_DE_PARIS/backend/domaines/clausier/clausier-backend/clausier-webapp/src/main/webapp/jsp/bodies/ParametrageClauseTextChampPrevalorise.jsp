<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<script type="text/javascript"  language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"  language="JavaScript" src="js/editeurTexteRedaction.js"></script>

<div class="main-part">
	<div class="breadcrumbs">
		<logic:equal name="preferences" value="direction">
			<bean:message key="Parametrage.titre.direction" />
		</logic:equal>
		<logic:equal name="preferences" value="agent">
			<bean:message key="Parametrage.titre.agent" />
		</logic:equal>
	</div>
	<atexo:fichePratique reference="" key="common.fichePratique"/>
	<div class="breaker"></div>
	
	<logic:messagesPresent>
		<div class="form-bloc-erreur msg-erreur">
			<div class="top">
				<span class="left"></span><span class="right"></span>
			</div>
			<div class="content">
				<div class="title"><bean:message key="erreur.texte.generique"/></div>
				 <html:errors  />
			</div>
			<div class="breaker"></div>
			<div class="bottom">
				<span class="left"></span><span class="right"></span>
			</div>
		</div>
	</logic:messagesPresent>
	
	<!--Debut bloc recap Type de clause-->
	<%@ include file="tableauRecapitulatifClause.jsp"%>
	<%@ include file="blocRecapClause.jsp"%>
	<!--Fin bloc recap Type de clause-->

	<!--Debut bloc Texte fixe avant-->
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>

        <div class="content">
            <div class="line">
                <span class="intitule">
                    <strong>
                        <bean:message key="ParametrageListeChoixCummlatif.txt.texteFixeAvant" />
                    </strong>
                </span>
                <div class="content-bloc-long">
                    <c:out value="${frmParametrageClauseTextChampPrevalorise.texteFixeAvant}" />
                </div>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom">
            <span class="left"></span><span class="right"></span>
        </div>

	</div>
	<div class="spacer"></div>
	<!--Fin bloc Texte fixe avant-->

	<!--Debut bloc Infos clause-->
	<html:form action="/ParametrageClauseTextChampPrevaloriseProcess.epm">
		<div class="form-saisie">
			<div class="form-bloc">
				<div class="top">
					<span class="left"></span><span class="right"></span>
				</div>
				<div class="content">

					<!--Debut Parametrage par defaut-->
					<div class="line">
						<div class="column-choix">
                            <html:radio property="choixParam" title="Par défaut" value="default" styleId="choixParam"/>
                            <bean:message key="ParametrageClauseTextChampPrevalorise.txt.parDefaut" />
						</div>
						<div class="clause-parametrage">
							<logic:equal name="frmParametrageClauseTextChampPrevalorise"
								property="tailleChamp" value="4">
								<textarea name="defaultValue"
									title="Valeur par default" cols="" rows="4"
									class="texte-tres-long mceEditorSansToolbar" readonly="readonly"><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise" property="defaultValue"/></textarea>
							</logic:equal>
							<logic:equal name="frmParametrageClauseTextChampPrevalorise"
								property="tailleChamp" value="1">
								<textarea name="defaultValue"
									title="Valeur par default" cols="" rows="4"
									class="texte-long mceEditorSansToolbar" readonly="readonly"><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise" property="defaultValue"/></textarea>
							</logic:equal>
							<logic:equal name="frmParametrageClauseTextChampPrevalorise"
								property="tailleChamp" value="2">
								<html:text property="defaultValue" title="Valeur par default"
									styleClass="texte-moyen" styleId="valeurDefaut_2"
									style="display:block"  readonly="true"
									errorStyleClass="error-border" />
							</logic:equal>
							<logic:equal name="frmParametrageClauseTextChampPrevalorise"
								property="tailleChamp" value="3">
								<html:text property="defaultValue" title="Valeur par default"
									styleClass="texte-court" styleId="valeurDefaut_3"
									style="display:block"  readonly="true"
									errorStyleClass="error-border" />
							</logic:equal>
						</div>
					</div>
					<script type="text/javascript">
					 	initEditeursTexteSansToolbarRedaction();
					</script>
					<!--Fin Parametrage par defaut-->

					<div class="separator"></div>
					<logic:equal name="preferences" value="direction">

						<!--Debut Parametrage Direction-->
						<div class="line">
							<div class="column-choix">
								<html:radio property="choixParam" title="Par direction"
									value="direction" styleId="choixParam"/>
								<bean:message
									key="ParametrageClauseTextChampPrevalorise.txt.direction" />
							</div>
							<div class="clause-parametrage">
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="4">
									<textarea name="directionValue" id="valeurParametrage_4"
										title="Valeur par direction" cols="" rows="4"
										class="texte-tres-long mceEditor" ><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise" property="directionValue"/></textarea>
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="1">
									<textarea name="directionValue" id="valeurParametrage_1"
										title="Valeur par direction" cols="" rows="4"
										class="texte-long mceEditor" ><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise" property="directionValue"/></textarea>
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="2">
									<html:text property="directionValue" title="Valeur par direction"
										styleClass="texte-moyen" styleId="valeurParametrage_2"
										style="display:block" errorStyleClass="error-border" />
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="3">
									<html:text property="directionValue" title="Valeur par direction"
										styleClass="texte-court" styleId="valeurParametrage_3"
										style="display:block" errorStyleClass="error-border" />
								</logic:equal>
							</div>
						</div>
						<!--Fin Parametrage Direction-->

					</logic:equal>

					<logic:equal name="preferences" value="agent">

						<!--Debut Parametrage Agent-->
						<div class="line">
							<div class="column-choix">
								<html:radio property="choixParam" title="Par agent"
									value="agent" styleId="choixParam"/>
								<bean:message
									key="ParametrageClauseTextChampPrevalorise.txt.agent" />
							</div>
							<div class="clause-parametrage">
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="4">
									<textarea name="agentValue" id="valeurParametrage_4"
										title="Valeur par agent" cols="" rows="4"
										class="texte-tres-long mceEditor" ><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise"
										property="agentValue"/></textarea>
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="1">
									<textarea name="agentValue"
										title="Valeur par agent" cols="" rows="4" id="valeurParametrage_1"
										class="texte-long mceEditor" ><atexo:retoureLigne name="frmParametrageClauseTextChampPrevalorise"
										property="agentValue"/></textarea>
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="2">
									<html:text property="agentValue" title="Valeur par agent"
										styleClass="texte-moyen" styleId="valeurParametrage_2"
										style="display:block" errorStyleClass="error-border" />
								</logic:equal>
								<logic:equal name="frmParametrageClauseTextChampPrevalorise"
									property="tailleChamp" value="3">
									<html:text property="agentValue" title="Valeur par agent"
										styleClass="texte-court" styleId="valeurParametrage_3"
										style="display:block" errorStyleClass="error-border" />
								</logic:equal>
							</div>
						</div>
						<!--Fin Parametrage Agent-->
					</logic:equal>
					<div class="breaker"></div>

				</div>
				<div class="bottom">
					<span class="left"></span><span class="right"></span>
				</div>
			</div>
		</div>
		<!--Fin bloc Infos clause-->

		<!--Debut bloc Texte fixe apres-->
		<div class="form-bloc">

			<div class="top">
				<span class="left"></span><span class="right"></span>
			</div>
            <div class="content">
                <div class="line">
                    <span class="intitule">
                        <strong>
                            <bean:message key="ParametrageListeChoixCummlatif.txt.texteFixeApres" />
                        </strong>
                    </span>
                    <div class="content-bloc-long">
                        <c:out value="${frmParametrageClauseTextChampPrevalorise.texteFixeApres}" />
                    </div>
                </div>
                <div class="breaker"></div>
            </div>

			<div class="bottom">
				<span class="left"></span><span class="right"></span>
			</div>
		</div>
		<!--Fin bloc Texte fixe apres-->

        <!--Debut boutons-->
        <div class="boutons">
            <a href="ParametrageClauseInit.epm?preferences=${preferences}" class="annuler">
                <bean:message key="ParametrageClauseTextChampPrevalorise.btn.annuler" />
            </a>
            <a href="javascript:document.forms[0].submit();" class="valider">
                <bean:message key="ParametrageClauseTextChampPrevalorise.btn.valider" />
            </a>
        </div>
        <!--Fin boutons-->
    </html:form>

	<script type="text/javascript">
		initEditeursTexteRedaction();
	</script>

</div>
<!--Fin main-part-->
