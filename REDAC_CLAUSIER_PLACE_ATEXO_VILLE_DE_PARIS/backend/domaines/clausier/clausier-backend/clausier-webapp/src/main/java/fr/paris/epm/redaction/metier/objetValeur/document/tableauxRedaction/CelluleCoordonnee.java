//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.05 at 05:36:49 PM CEST 
//


package fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Java class for cellule-coordonnee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cellule-coordonnee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="ligne" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="colonne" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cellule-coordonnee")
public class CelluleCoordonnee
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "ligne", required = true)
    protected int ligne;
    @XmlAttribute(name = "colonne", required = true)
    protected int colonne;

    /**
     * Gets the value of the ligne property.
     * 
     */
    public int getLigne() {
        return ligne;
    }

    /**
     * Sets the value of the ligne property.
     * 
     */
    public void setLigne(int value) {
        this.ligne = value;
    }

    /**
     * Gets the value of the colonne property.
     * 
     */
    public int getColonne() {
        return colonne;
    }

    /**
     * Sets the value of the colonne property.
     * 
     */
    public void setColonne(int value) {
        this.colonne = value;
    }

}
