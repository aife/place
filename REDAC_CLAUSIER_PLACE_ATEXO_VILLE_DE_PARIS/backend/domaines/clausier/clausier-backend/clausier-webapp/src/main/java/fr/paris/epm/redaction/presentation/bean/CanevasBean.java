package fr.paris.epm.redaction.presentation.bean;

import fr.paris.epm.global.coordination.bean.Directory;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Bean Canevas
 * Created by nty on 29/06/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class CanevasBean extends BaseBean {

    /**
     * l'identifiant du canevas.
     */
    private int idCanevas;

    /**
     * null si n'est pas publié
     */
    private Integer idPublication;

    private Integer idLastPublication;

    private Integer idOrganisme;

    /**
     * la réference du canevas (utilisé que pour la recherche).
     */
    private String referenceCanevas;

    /**
     * la réference de la clause (utilisé que pour la recherche).
     */
    private String referenceClause;

    /**
     * le titre du canevas.
     */
    private String titre;

    /**
     * libellé (version) de la dernière publication pour les canevas de type EpmTCanevas,
     * libellé (version) de la publication pour les canevas EpmTCanevasPub -> donc EpmVCanevas
     */
    private String lastVersion;

    private Date dateDerniereValidation;
    private Date datePremiereValidation;


    /**
     * le type de document.
     */
    private int idTypeDocument;

    /**
     * utilisé que pour l'affichage
     */
    private String labelTypeDocument;
    private String codeTypeDocument;

    private List<Integer> idsTypeContrats;

    /**
     * utilisé que pour l'affichage
     */
    private List<String> labelsTypeContrats;

    /**
     * (utilisé que pour la recherche).
     */
    private Integer idTypeContrat;

    private List<Integer> idsProcedures;

    /**
     * utilisé que pour l'affichage
     */
    private List<String> labelsProcedures;

    /**
     * (utilisé que pour la recherche).
     */
    private int idProcedure;

    /**
     * utilisé que pour l'affichage
     */
    private String labelNaturePrestation;

    /**
     * la date de création du canevas.
     */
    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateCreation;

    /**
     * utilisée que pour la recherche
     */
    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMin;

    /**
     * utilisée que pour la recherche
     */
    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMax;


    /**
     * id de CCAG
     */
    private int idRefCCAG;


    /**
     * utilisé que pour l'affichage
     */
    private String labelTypeAuteur;

    /**
     * Détermine si le canevas est un canevas Editeur(true) ou Client(false)
     */
    private boolean canevasEditeur;

    private boolean compatibleEntiteAdjudicatrice;

    private  List<Directory> procedures;
    private  List<Directory> typesContrat;
    private  Directory ccag;

    public int getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdLastPublication() {
        return idLastPublication;
    }

    public void setIdLastPublication(Integer idLastPublication) {
        this.idLastPublication = idLastPublication;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public String getReferenceCanevas() {
        return referenceCanevas;
    }

    public void setReferenceCanevas(String referenceCanevas) {
        this.referenceCanevas = referenceCanevas;
    }

    public String getReferenceClause() {
        return referenceClause;
    }

    public void setReferenceClause(String referenceClause) {
        this.referenceClause = referenceClause;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public int getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public String getLabelTypeDocument() {
        return labelTypeDocument;
    }

    public void setLabelTypeDocument(String labelTypeDocument) {
        this.labelTypeDocument = labelTypeDocument;
    }

    public List<Integer> getIdsTypeContrats() {
        return idsTypeContrats;
    }

    public void setIdsTypeContrats(List<Integer> idsTypeContrats) {
        this.idsTypeContrats = idsTypeContrats;
    }

    public List<String> getLabelsTypeContrats() {
        return labelsTypeContrats;
    }

    public void setLabelsTypeContrats(List<String> labelsTypeContrats) {
        this.labelsTypeContrats = labelsTypeContrats;
    }

    public Integer getIdTypeContrat() {
        return idTypeContrat;
    }

    public void setIdTypeContrat(Integer idTypeContrat) {
        this.idTypeContrat = idTypeContrat;
    }

    public List<Integer> getIdsProcedures() {
        return idsProcedures;
    }

    public void setIdsProcedures(List<Integer> idsProcedures) {
        this.idsProcedures = idsProcedures;
    }

    public List<String> getLabelsProcedures() {
        return labelsProcedures;
    }

    public void setLabelsProcedures(List<String> labelsProcedures) {
        this.labelsProcedures = labelsProcedures;
    }

    public int getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(int idProcedure) {
        this.idProcedure = idProcedure;
    }

    public String getLabelNaturePrestation() {
        return labelNaturePrestation;
    }

    public void setLabelNaturePrestation(String labelNaturePrestation) {
        this.labelNaturePrestation = labelNaturePrestation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public Date getDateModificationMin() {
        return dateModificationMin;
    }

    public void setDateModificationMin(Date dateModificationMin) {
        this.dateModificationMin = dateModificationMin;
    }

    public Date getDateModificationMax() {
        return dateModificationMax;
    }

    public void setDateModificationMax(Date dateModificationMax) {
        this.dateModificationMax = dateModificationMax;
    }



    public int getIdRefCCAG() {
        return idRefCCAG;
    }

    public void setIdRefCCAG(int idRefCCAG) {
        this.idRefCCAG = idRefCCAG;
    }

    public String getLabelTypeAuteur() {
        return labelTypeAuteur;
    }

    public Date getDatePremiereValidation() {
        return datePremiereValidation;
    }

    public void setDatePremiereValidation(Date datePremiereValidation) {
        this.datePremiereValidation = datePremiereValidation;
    }

    public void setLabelTypeAuteur(String labelTypeAuteur) {
        this.labelTypeAuteur = labelTypeAuteur;
    }

    public boolean isCanevasEditeur() {
        return canevasEditeur;
    }

    public void setCanevasEditeur(boolean canevasEditeur) {
        this.canevasEditeur = canevasEditeur;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public String getCodeTypeDocument() {
        return codeTypeDocument;
    }

    public void setCodeTypeDocument(String codeTypeDocument) {
        this.codeTypeDocument = codeTypeDocument;
    }

    public List<Directory> getProcedures() {
        return procedures;
    }

    public void setProcedures(List<Directory> procedures) {
        this.procedures = procedures;
    }

    public List<Directory> getTypesContrat() {
        return typesContrat;
    }

    public void setTypesContrat(List<Directory> typesContrat) {
        this.typesContrat = typesContrat;
    }

    public Directory getCcag() {
        return ccag;
    }

    public void setCcag(Directory ccag) {
        this.ccag = ccag;
    }
}
