//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.05 at 05:36:49 PM CEST 
//


package fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ligne complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ligne">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cellule" type="{}cellule" maxOccurs="unbounded"/>
 *         &lt;element name="regionFusionnee" type="{}regionFusionnee" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="numeroLigneInsertion" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="duplicationStyleLigne" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ligne", propOrder = {
    "cellule",
    "regionFusionnee"
})
public class Ligne implements Serializable {

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected List<Cellule> cellule;
    protected List<RegionFusionnee> regionFusionnee;
    @XmlAttribute(name = "numeroLigneInsertion", required = true)
    protected int numeroLigneInsertion;
    @XmlAttribute(name = "duplicationStyleLigne")
    protected Integer duplicationStyleLigne;

    /**
     * Gets the value of the cellule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cellule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCellule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cellule }
     * 
     * 
     */
    public List<Cellule> getCellule() {
        if (cellule == null) {
            cellule = new ArrayList<Cellule>();
        }
        return this.cellule;
    }

    /**
     * Gets the value of the regionFusionnee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regionFusionnee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionFusionnee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionFusionnee }
     * 
     * 
     */
    public List<RegionFusionnee> getRegionFusionnee() {
        if (regionFusionnee == null) {
            regionFusionnee = new ArrayList<RegionFusionnee>();
        }
        return this.regionFusionnee;
    }

    /**
     * Gets the value of the numeroLigneInsertion property.
     * 
     */
    public int getNumeroLigneInsertion() {
        return numeroLigneInsertion;
    }

    /**
     * Sets the value of the numeroLigneInsertion property.
     * 
     */
    public void setNumeroLigneInsertion(int value) {
        this.numeroLigneInsertion = value;
    }

    /**
     * Gets the value of the duplicationStyleLigne property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDuplicationStyleLigne() {
        return duplicationStyleLigne;
    }

    /**
     * Sets the value of the duplicationStyleLigne property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDuplicationStyleLigne(Integer value) {
        this.duplicationStyleLigne = value;
    }

}
