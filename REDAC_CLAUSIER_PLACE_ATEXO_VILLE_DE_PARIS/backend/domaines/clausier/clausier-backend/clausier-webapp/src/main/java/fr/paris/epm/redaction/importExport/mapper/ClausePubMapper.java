package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTClausePub;
import fr.paris.epm.redaction.importExport.bean.ClauseImportExport;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryNamedMapper;
import org.mapstruct.*;

@Mapper(componentModel = "spring",
        uses = {RoleClausePubMapper.class, ClausePotentiellementConditionneePubMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class})
public abstract class ClausePubMapper {

    @Mapping(source = "epmTRefTypeDocument.uid", target = "document")
    @Mapping(source = "epmTRefTypeClause.uid", target = "type")
    @Mapping(source = "epmTRefThemeClause.uid", target = "theme")
    @Mapping(source = "epmTRefProcedure.uid", target = "procedure")
    @Mapping(source = "epmTRefStatutRedactionClausier.uid", target = "statutRedactionClausier")
    @Mapping(source = "epmTRefTypeContrats", target = "contrats", qualifiedByName = {"DirectoryNamedMapper", "uidsFromContrats"})
    @Mapping(source = "epmTRoleClauses", target = "roles")
    @Mapping(source = "epmTClausePotentiellementConditionnees", target = "clausesPotentiellementConditionnee")
    @Mapping(source = "idNaturePrestation", target = "naturePrestation", qualifiedByName = {"DirectoryNamedMapper", "uidNaturePrestationFromId"})
    public abstract ClauseImportExport toClauseImportExport(EpmTClausePub epmTClausePub);

    @Mapping(source = "document", target = "epmTRefTypeDocument", qualifiedByName = {"DirectoryNamedMapper", "typeDocumentFromUid"})
    @Mapping(source = "type", target = "epmTRefTypeClause", qualifiedByName = {"DirectoryNamedMapper", "typeClauseFromUid"})
    @Mapping(source = "theme", target = "epmTRefThemeClause", qualifiedByName = {"DirectoryNamedMapper", "themeClauseFromUid"})
    @Mapping(source = "procedure", target = "epmTRefProcedure", qualifiedByName = {"DirectoryNamedMapper", "procedureFromUid"})
    @Mapping(source = "statutRedactionClausier", target = "epmTRefStatutRedactionClausier", qualifiedByName = {"DirectoryNamedMapper", "statutRedactionClausierFromUid"})
    @Mapping(source = "contrats", target = "epmTRefTypeContrats", qualifiedByName = {"DirectoryNamedMapper", "typesContratsFromUids"})
    @Mapping(source = "roles", target = "epmTRoleClauses")
    @Mapping(source = "clausesPotentiellementConditionnee", target = "epmTClausePotentiellementConditionnees")
    @Mapping(source = "naturePrestation", target = "idNaturePrestation", qualifiedByName = {"DirectoryNamedMapper", "idNaturePrestationFromLibelle"})
    public abstract EpmTClausePub toEpmTClausePub(ClauseImportExport clauseImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTClausePub setIdPublication(@MappingTarget EpmTClausePub epmTClausePub, @Context Integer idPublication) {
        epmTClausePub.setIdPublication(idPublication);
        return epmTClausePub;
    }

}
