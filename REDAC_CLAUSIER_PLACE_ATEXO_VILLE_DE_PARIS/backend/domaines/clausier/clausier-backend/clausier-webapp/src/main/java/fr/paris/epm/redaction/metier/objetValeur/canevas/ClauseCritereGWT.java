package fr.paris.epm.redaction.metier.objetValeur.canevas;

import java.io.Serializable;

/**
 * classe comportant les critères d'une clause en GWT.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseCritereGWT implements Serializable {

    /**
     * marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * id publication.
     */
    private Integer idPublication;
    /**
     * référence d'une clause.
     */
    private String reference;
    /**
     * type de contrat
     */
    private Integer typeContrat;
    /**
     * procedure de passation.
     */
    private Integer procedureEditeur;
    /**
     * les mots clés.
     */
    private String motsCles;
    /**
     * théme.
     */
    private int theme;
    /**
     * nature de prestation.
     */
    private int naturePrestation;
    /**
     * type de document.
     */
    private int idTypeDocument;
    /**
     * type de document - actif/inactif
     */
    private Boolean typeDocumentActif;
    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private boolean compatibleEntiteAdjudicatrice;

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(Integer typeContrat) {
        this.typeContrat = typeContrat;
    }

    public Integer getProcedureEditeur() {
        return procedureEditeur;
    }

    public void setProcedureEditeur(Integer procedureEditeur) {
        this.procedureEditeur = procedureEditeur;
    }

    public String getMotsCles() {
        return motsCles;
    }

    public void setMotsCles(String motsCles) {
        this.motsCles = motsCles;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getNaturePrestation() {
        return naturePrestation;
    }

    public void setNaturePrestation(int naturePrestation) {
        this.naturePrestation = naturePrestation;
    }

    public int getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public Boolean getTypeDocumentActif() {
        return typeDocumentActif;
    }

    public void setTypeDocumentActif(Boolean typeDocumentActif) {
        this.typeDocumentActif = typeDocumentActif;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

}