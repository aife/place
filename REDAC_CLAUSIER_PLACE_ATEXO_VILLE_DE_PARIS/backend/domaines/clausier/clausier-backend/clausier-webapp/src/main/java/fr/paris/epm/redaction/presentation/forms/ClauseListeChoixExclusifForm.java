package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Formulaire Clause Liste Choix Exclusif.
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseListeChoixExclusifForm extends AbstractClauseListeChoixForm {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attribut precochee.
     */
    private List<ClauseFormulaire> formulaires;
    private String precochee;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    /**
     * Constructeur de la classe ClauseListeChoixExclusifForm().
     */
    public ClauseListeChoixExclusifForm() {
        this.reset();
    }

    /**
     * permet d'initialiser le formulaire.
     */
    public void reset() {
        precochee = "0";
        formulationModifiable = "0";
        parametrableDirection = "0";
        parametrableAgent = "0";
        textFixeAvant = null;
        textFixeApres = null;
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
        numFormulation = null;
        valeurDefaut = null;
        tailleChamp = null;
        valeurDefautMoyen = null;
        valeurDefautCourt = null;
        formulationCollection = null;
        valeurDefautTresLong = null;
    }

    /**
     * @return precochee
     */
    public final String getPrecochee() {
        return precochee;
    }

    /**
     * @param valeur pour positionner l'attribut precochee.
     */
    public final void setPrecochee(final String valeur) {
        precochee = valeur;
    }

    /**
     * methode validate () pour valider les données du formulaire.
     *
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping,
     * javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        boolean erreurTrouve = false;

        ActionErrors erreurs = new ActionErrors();

        Util util = new Util();

        Integer numFormulationPrecoche = null;
        if (precochee != null && !precochee.isEmpty()) {
            numFormulationPrecoche = Integer.valueOf(precochee);
        }

        if (numFormulation == null) {
            erreurs.add("numFormuleExclusif", new ActionMessage("creationClause.valider.numFormuleExclusif"));
            erreurTrouve = true;
        } else {
            LOG.debug("Récupération des formulations");
            Set<EpmTRoleClauseAbstract> epmTRoleClauseAbstracts = new HashSet<>();
            for (int i = 0; i < numFormulation.length; i++) {
                // positionner les ppts externes
                LOG.debug("clauseListeChoixExclusifForm.getNumFormulation()[" + i + "]: " + numFormulation[i]);
                // Les ppts externes ont des valeurs par defaut et donc stockés
                // dans TRoleClause
                EpmTRoleClause epmTRoleClauseInstance = new EpmTRoleClause();
                if (numFormulation[i] != null && util.isInt(numFormulation[i]))
                    epmTRoleClauseInstance.setNumFormulation(Integer.valueOf(numFormulation[i]));
                epmTRoleClauseInstance.setNombreCarateresMax(Integer.parseInt(tailleChamp[i]));

                String defaultValue = "";
                if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_LONG))
                    defaultValue = valeurDefaut[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_MOYEN))
                    defaultValue = valeurDefautMoyen[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_COURT))
                    defaultValue = valeurDefautCourt[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_TRES_LONG))
                    defaultValue = valeurDefautTresLong[i];
                epmTRoleClauseInstance.setValeurDefaut(defaultValue);

                if (numFormulationPrecoche != null && i == numFormulationPrecoche)
                    epmTRoleClauseInstance.setPrecochee(true);
                else
                    epmTRoleClauseInstance.setPrecochee(false);

                LOG.debug("Faire le lien entre EpmTRoleClause et EpmTPptExtClause");

                // Ajouter les roleClauses à une clause
                epmTRoleClauseAbstracts.add(epmTRoleClauseInstance);
            }

            EpmTClause epmTClause = new EpmTClause();
            epmTClause.setEpmTRoleClauses(epmTRoleClauseAbstracts);

            formulationCollection = epmTClause.getEpmTRoleClausesTrie();

            if (numFormulation.length < 2) {
                erreurs.add("numFormuleExclusif", new ActionMessage("creationClause.valider.numFormuleExclusif"));
                erreurTrouve = true;
            } else {
                for (String num : numFormulation) {
                    LOG.debug("numFormulation[i]=" + num);

                    if (!util.isInt(num) || Integer.parseInt(num) < 0) {
                        erreurs.add("numFormule", new ActionMessage("creationClause.valider.numFormule"));
                        erreurTrouve = true;
                        break;
                    }
                }
            }
        }

        if (formulationCollection != null && !erreurTrouve) {
            for (EpmTRoleClauseAbstract element : formulationCollection) {
                if (element != null && element.getValeurDefaut().trim().equals("")) {
                    erreurs.add("valeurDefaut", new ActionMessage("creationClause.valider.valeurDefaut"));
                    erreurTrouve = true;
                    break;
                }
            }
        }

        if (erreurTrouve) {
            numFormulation = null;
        }
        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    public final void setClauseSelectionnee(final String valeur) {
        this.clauseSelectionnee = valeur;
    }

    public List<ClauseFormulaire> getFormulaires() {
        return formulaires;
    }

    public void setFormulaires(List<ClauseFormulaire> formulaires) {
        this.formulaires = formulaires;
    }


}
