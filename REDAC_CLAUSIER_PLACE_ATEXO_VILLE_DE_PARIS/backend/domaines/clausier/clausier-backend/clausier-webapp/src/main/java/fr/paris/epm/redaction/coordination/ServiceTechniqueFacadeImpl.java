package fr.paris.epm.redaction.coordination;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;

/**
 * @see {@link ServiceTechniqueFacade}
 * @author RVI
 */
public class ServiceTechniqueFacadeImpl implements ServiceTechniqueFacade {

    private ServiceTechniqueSecurise serviceTechniqueService;

    /**
     * @see {@link ServiceTechniqueFacade#getUtilisateurExterneConsultation(String)}
     */
    public final UtilisateurExterneConsultation getUtilisateurExterneConsultation(
            final String identifiantExterne) {
        UtilisateurExterneConsultation utilisateurExterneConsultation= serviceTechniqueService.getUtilisateurExterneConsultation(identifiantExterne);
        return utilisateurExterneConsultation;
    }

    public final void setServiceTechniqueService(final ServiceTechniqueSecurise valeur) {
        this.serviceTechniqueService = valeur;
    }

}
