package fr.paris.epm.redaction.presentation.bean;

import java.util.ArrayList;
import java.util.List;

public class ValeurPotentiellementConditionne {
    /**
     * conditionné ou non
     */
    private Boolean potCdt;
    /**
     * List des valeurs pour le chois multiple
     */
    private List<Integer> valeurList = new ArrayList<>();
    /**
     * Critère .
     */
    private Integer critere;
    /**
     * Critère .
     */
    private String critereLabel;

    public Boolean getPotCdt() {
        return potCdt;
    }

    public void setPotCdt(Boolean potCdt) {
        this.potCdt = potCdt;
    }

    public List<Integer> getValeurList() {
        return valeurList;
    }

    public void setValeurList(List<Integer> valeurList) {
        this.valeurList = valeurList;
    }

    public Integer getCritere() {
        return critere;
    }

    public void setCritere(Integer critere) {
        this.critere = critere;
    }

    public String getCritereLabel() {
        return critereLabel;
    }

    public void setCritereLabel(String critereLabel) {
        this.critereLabel = critereLabel;
    }

}
