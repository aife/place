/**
 * 
 */
package fr.paris.epm.redaction.metier.objetValeur.document;

import java.io.Serializable;

/**
 * Bean contenant un document et le resultat de l'appel à la méthode d'enregistrement de Document
 * @author MGA
 *
 */
public class RetourEnregistrementDocument implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le document à retourner
     */
    private Document document;
    /**
     * Le resultat de l'appel à la fonction d'enregistrement
     */
    private Boolean resultat;
    /**
     * @return the document
     */
    public final Document getDocument() {
        return document;
    }
    /**
     * @param document the document to set
     */
    public final void setDocument(final Document valeur) {
        this.document = valeur;
    }
    /**
     * @return the resultat
     */
    public final Boolean getResultat() {
        return resultat;
    }
    /**
     * @param resultat the resultat to set
     */
    public final void setResultat(final Boolean valeur) {
        this.resultat = valeur;
    }
    
    
}
