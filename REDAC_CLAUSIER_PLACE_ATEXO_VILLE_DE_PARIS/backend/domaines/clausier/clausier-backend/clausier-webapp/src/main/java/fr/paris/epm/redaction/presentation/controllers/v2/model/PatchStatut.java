package fr.paris.epm.redaction.presentation.controllers.v2.model;

public class PatchStatut {
    private int idStatus;

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }
}
