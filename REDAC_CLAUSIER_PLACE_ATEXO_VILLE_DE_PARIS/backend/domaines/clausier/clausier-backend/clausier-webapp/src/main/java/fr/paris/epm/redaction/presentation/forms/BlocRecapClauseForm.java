package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionForm;


/**
 * Cette classe permet d initialiser l'affichage du bloc
 *  d'informations relatives à la la clause.
 *  @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */


public class BlocRecapClauseForm extends ActionForm {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut Type de Clause .
     */
    private String typeClause;
    /**
     * Attribut Thème de la clause .
     */
    private String theme;
    /**
     * Attribut  procédure de Passation .
     */
    private String procedurePassation;
    /**
     * Attribut  nature de Préstation .
     */
    private String naturePrestation;
    /**
     * Attribut  mots Clés .
     */
    private String motsCles;
    /**
     * Attribut Type de Document .
     */
    private String typeDocument;
    /**
     * Attribut statut de la Clause .
     */
    private String statutClause;
    /**
     * Attribut Potentiellement conditionnée .
     */
    private String potCdt;
    /**
     * Attribut info Bulle.
     */
    private String infoBulle;

    /**
     * @return the typeClause
     */
    public final String getTypeClause() {
        return typeClause;
    }


    /**
     * cette méthode permet de positionner le type
     * d'une clause avec une valeur donnée.
     * @param valeur initialise le type de document.
     */
    public final void setTypeClause(final String valeur) {
        typeClause = valeur;
    }

    /**
     * cette méthode renvoie le thème de la clause.
     * @return  renvoyer le thème de la clause.
     */
    public final String getTheme() {
        return theme;
    }

    /**
     * cette méthode  positionne le thème
     * d'une clause avec une valeur donnée.
     * @param valeur initialise le thème de la clasue.
     */
    public final void setTheme(final String valeur) {
        theme = valeur;
    }

    /**
     * cette méthode renvoie la procédure de passation.
     * @return  renvoyer la procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * cette méthode  positionne la procédure de passation
     * avec une valeur donnée.
     * @param valeur initialise la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * cette méthode renvoie la nature de prestation.
     * @return  renvoyer la nature de prestation.
     */
    public final String getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * cette méthode  positionne la nature de prestation
     * avec une valeur donnée.
     * @param valeur initialise la nature de prestation.
     */
    public final void setNaturePrestation(final String valeur) {
        naturePrestation = valeur;
    }

    /**
     * cette méthode renvoie les mots clés de la clause.
     * @return  renvoyer les mots clés de la clause.
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * cette méthode  positionne les mots clés de la clause
     * avec une valeur donnée.
     * @param valeur initialise les mots clés de la clause.
     */
    public final void setMotsCles(final String valeur) {
        motsCles = valeur;
    }

    /**
     * cette méthode renvoie le type de document de la clause.
     * @return  renvoyer le type de document de la clause.
     */
    public final String getTypeDocument() {
        return typeDocument;
    }

    /**
     * cette méthode  positionne le type de document de la clause
     * avec une valeur donnée.
     * @param valeur initialise le type de document de la clause.
     */
    public final void setTypeDocument(final String valeur) {
        typeDocument = valeur;
    }

    /**
     * cette méthode renvoie le statut de la clause.
     * @return  renvoyer le statut de la clause.
     */
    public final String getStatutClause() {
        return statutClause;
    }

    /**
     * cette méthode  positionne le Statut de la clause
     * avec une valeur donnée.
     * @param valeur initialise le statut de la clause.
     */
    public final void setStatutClause(final String valeur) {
        statutClause = valeur;
    }

    /**
     * cette méthode renvoie le Potentiellement conditionnée.
     * @return  renvoyer le Potentiellement conditionnée.
     */
    public final String getPotCdt() {
        return potCdt;
    }

    /**
     * cette méthode  positionne le Potentiellement
     * conditionnée de la clause avec une valeur donnée.
     * @param valeur initialise le Potentiellement conditionnée.
     */
    public final void setPotCdt(final String valeur) {
        potCdt = valeur;
    }

    /**
     *  cette méthode renvoie l'info-bulle de la clause.
     * @return  renvoyer l'info-bulle de la clause.
     */
    public final String getInfoBulle() {
        return infoBulle;
    }

    /**
     * cette méthode  positionne l'info-bulle
     * de la clause avec une valeur donnée.
     * @param valeur initialise l'info-bulle de la clause.
     */
    public final void setInfoBulle(final String valeur) {
        infoBulle = valeur;
    }

}
