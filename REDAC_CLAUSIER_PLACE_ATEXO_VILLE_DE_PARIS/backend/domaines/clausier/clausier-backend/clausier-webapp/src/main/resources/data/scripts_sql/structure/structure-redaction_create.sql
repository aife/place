CREATE TABLE epm__t_association_canevas_clause (
    id integer NOT NULL,
    id_clause integer NOT NULL,
    id_canevas integer NOT NULL,
    clause_canevas_modifie character varying,
    element_numerote integer
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_canevas (
    id integer NOT NULL,
    id_document_type integer NOT NULL,
    titre character varying NOT NULL,
    reference character varying NOT NULL,
    xml_data text,
    version integer NOT NULL,
    date_creation timestamp without time zone NOT NULL,
    date_modification timestamp without time zone NOT NULL,
    actif character varying NOT NULL,
    id_nature_prestation integer NOT NULL,
    id_procedure_passation integer NOT NULL,
    auteur character varying NOT NULL,
    etat character varying DEFAULT 0 NOT NULL
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_clause (
    id integer NOT NULL,
    id_ref_type_document integer NOT NULL,
    id_info_bulle integer,
    id_ref_type_clause integer NOT NULL,
    id_theme integer NOT NULL,
    reference character varying NOT NULL,
    parametrable_direction character varying NOT NULL,
    parametrable_agent character varying NOT NULL,
    texte_fixe_avant character varying NOT NULL,
    texte_fixe_apres character varying,
    date_modification timestamp without time zone,
    date_creation timestamp without time zone NOT NULL,
    version integer NOT NULL,
    auteur character varying NOT NULL,
    actif character varying NOT NULL,
    id_nature_prestation integer,
    id_procedure_passation integer,
    etat character varying DEFAULT 0 NOT NULL,
    formulation_modifiable character varying,
    potentionnellement_conditionnee boolean DEFAULT false NOT NULL,
    id_ref_valeur_potentiellement_conditionnee integer,
    id_ref_potentiellement_conditionnee integer,
    saut_ligne_texte_avant boolean DEFAULT false NOT NULL,
    saut_ligne_texte_apres boolean DEFAULT false NOT NULL,
    parametre_agent boolean DEFAULT false NOT NULL,
    parametre_direction boolean DEFAULT false NOT NULL
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_document (
    id integer NOT NULL,
    id_canevas integer NOT NULL,
    consultation integer NOT NULL,
    reference character varying NOT NULL,
    designation character varying,
    lot_juridique integer,
    nom_fichier character varying NOT NULL,
    auteur character varying NOT NULL,
    xml_data text NOT NULL,
    version integer NOT NULL,
    date_creation timestamp without time zone NOT NULL,
    date_modification timestamp without time zone NOT NULL,
    statut character varying NOT NULL,
    invalider_eligibilite boolean,
    titre character varying NOT NULL
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_info_bulle (
    id integer NOT NULL,
    texte character varying,
    nombrecaracteresmax integer,
    texteurl character varying,
    date_modification timestamp without time zone NOT NULL,
    lienurl character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_valeur_tableau (
  id integer NOT NULL,
  libelle character varying,
  nom_attribut_consultation character varying,
  titre_colonne boolean NOT NULL DEFAULT true
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_mots_cles (
    id integer NOT NULL,
    id_clause integer NOT NULL,
    valeur character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ppt_ext_clause (
    id integer NOT NULL,
    nombre_carateres_max integer,
    champ_obligatoire character varying,
    valeur_heritee_modifiable character varying,
    num_formulation integer
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_potentiellement_conditionnee (
    id integer NOT NULL,
    libelle character varying,
    methode_libelle character varying,
    critere_booleen boolean DEFAULT false NOT NULL,
    methode_id character varying,
    methode_consultation character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_theme_clause (
    id integer NOT NULL,
    libelle character varying(50)
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_type_clause (
    id integer NOT NULL,
    libelle character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_type_document (
    id integer NOT NULL,
    libelle character varying,
    extension_fichier character varying,
    template character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_ref_valeur_heritee (
    id integer NOT NULL,
    libelle character varying,
    nom_attribut_consultation character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_role_clause (
    id integer NOT NULL,
    id_clause integer NOT NULL,
    id_ppt_ext_clause integer NOT NULL,
    id_direction integer,
    id_agent integer,
    valeur_defaut character varying,
    precochee character varying,
    etat character varying
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_type_document_clause (
    id integer NOT NULL,
    id_clause integer NOT NULL
) TABLESPACE m13_epmredaction_data;
CREATE TABLE epm__t_test(
  id integer
) TABLESPACE m13_epmredaction_data;

