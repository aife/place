<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!--Debut main-part-->
<div class="form-bloc">
	<div class="top">
		<span class="left"></span><span class="right"></span>
	</div>
	<div class="content">
		<h2 class="float-left">
			<html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee" value="clauseEditeur"></html:radio>
			<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeur"/></label>
		</h2>
		<div class="actions-clause no-padding">
			<logic:equal name="typeAction" value="M">
<%-- 				<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
					<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 				</c:if> --%>
			</logic:equal>
			<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
		</div>
<%-- 		<div class="info-maj"><bean:message key="clause.surcharge.modifieeLe"/> --%>
<!-- 		</div> -->
	
		<div class="line">
			<span class="intitule-bloc"><bean:message key="ClauseTexteFixe.txt.texteFixe" /> </span>
			
			<textarea disabled="disabled" class="texte-long mceEditorSansToolbar" rows="4" cols=""
				title="Texte fixe" id="texteFixeEditeur"
				name="texteFixeEditeur">${clauseTexteFixeFormEditeur.texteFixe}</textarea>
				
		</div>
		
		<div class="actions-clause">
			<logic:equal name="typeAction" value="M">
<%-- 				<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
					<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 				</c:if> --%>
			</logic:equal>
			<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom">
		<span class="left"></span><span class="right"></span>
	</div>
</div>
<div class="spacer"></div>
<script type="text/javascript">
	initEditeursTexteSansToolbarRedaction();
</script>
