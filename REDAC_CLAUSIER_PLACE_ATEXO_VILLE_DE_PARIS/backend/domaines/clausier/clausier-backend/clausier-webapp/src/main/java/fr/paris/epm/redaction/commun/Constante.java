package fr.paris.epm.redaction.commun;

import fr.paris.epm.global.commun.ConstantesGlobales;

/**
 * Classe rassemblant l'ensemble des constantes du module redaction.
 * @author Youness Eddyoumi
 * @version $Revision$, $Date$, $Author$
 */
public abstract class Constante extends ConstantesGlobales {

	/**
	 * Le retour pour ne pas initialiser une action.
	 */
	public static final String INIT = "init";

	/**
	 * Type opération modification.
	 */
	public static final String MODIFIER = "M";

	/**
	 * Type opération création.
	 */
	public static final String CREER = "C";

	/**
	 * Type opération duplication.
	 */
	public static final String DUPLIQUER = "D";

	/**
	 * Paramétre ancien identifiant (clause ou canevas) qui circule dans le
	 * request ou dans la session.
	 */
	public static final String ANCIEN_ID = "ancienId";

	/**
	 * Paramétre type action qui circule dans le request ou dans la session.
	 */
	public static final String TYPE_OPERATION = "typeAction";

	/**
	 * Paramétre référence qui circule dans le request ou dans la session.
	 */
	public static final String REFERENCE = "reference";

	/**
	 * id = 48 -> Tableau de dérogations dans la table "EpmTRefValeurTypeClause"
	 */
	public static final int ID_TABLEAU_DEROGATION = 48;

	/**
	 * Préférence.
	 */
	public static final String PREFERENCES = "preferences";

	/**
	 * Préférence direction.
	 */
	public static final String PRE_DIRECTION = "direction";

	/**
	 * Préférence agent.
	 */
	public static final String PRE_AGENT = "agent";

	/**
	 * Préférences par défaut.
	 */
	public static final String PRE_DEFAULT = "default";

	/**
	 * Le paramétre identifiant de la consultation qui circule dans la session
	 * ou le request.
	 */
	public static final String ID_CONSULTATION = "idCons";

	/**
	 * Le retour réussi d'une action.
	 */
	public static final String SUCCES = "success";
	public static final String SUCCES_EXTERNE = "successExterne";

	/**
	 * Le retour echoué d'une action.
	 */
	public static final String ECHEC = "echec";

	/**
	 * Le paramétre identifiant du canevas qui circule dans la session ou le request.
	 */
	public static final String ID_CANEVAS = "idCanevas";

	/**
	 * Le paramétre identifiant de publication des canevas qui circule dans la session ou le request.
	 */
	public static final String ID_PUBLICATION = "idPublication";

	/**
	 * Le paramétre document qui circule dans la session ou le request.
	 */
	public static final String DOCUMENT_EN_COURS = "document";

	public static final String DOCUMENT_EN_COURS_APERCU_DYNAMIQUE = "documentEnCoursApercuDynamique";

	/**
	 * La liste des dérogations
	 */
	public static final String COULEURS_TABLEAU_DEROGATIONS = "couleursTableauDynamique";

	public static final String COULEUR_GREEN = "green";

	public static final String COULEUR_RED = "red";

	/**
	 * Nature de la prestation.
	 */
	public static final String ID_NATURE_PRESTATION = "ID_NATURE_PRESTATION";

	/**
	 * Nature de la prestation.
	 */
	public static final String ID_PROCEDURE_PASSATION = "ID_PROCEDURE_PASSATION";

	/**
	 * Le paramétre canevas qui circule dans la session ou le request.
	 */
	public static final String CANEVAS_EN_COURS = "canevas";

	/**
	 * Le paramétre canevas initial qui a servi pour la duplication.
	 */
	public static final String CANEVAS_REF_INITIAL = "canevasRefInitial";

	/**
	 * Le paramétre canevas qui circule dans la session ou le request pour l'historique.
	 */
	public static final String CANEVAS_VERSION_EN_COURS = "versionCanevas";


	/**
	 * Le paramétre clause qui circule dans la session ou le request.
	 */
	public static final String CLAUSE = "clause";

	/**
	 * Document
	 */
	public static final String DOCUMENT = "document";

	/**
	 * Le paramétre identifiant du document qui circule dans la session ou le
	 * request.
	 */
	public static final String ID_DOCUMENT = "idDoc";

	/**
	 * Le paramétre de référence du document qui circule dans la session ou le
	 * request.
	 */
	public static final String DOCUMENT_REFERENCE = "refDoc";

	/**
	 * Le séparateur de saisie des mots clés.
	 */
	public static final String SEPARATEUR_SAISIE = ",";

	/**
	 * L'identifiant d'un champ à taille longue.
	 */
	public static final String TAILLE_CHAMP_LONG = "1";

	/**
	 * L'identifiant d'un champ à taille moyenne.
	 */
	public static final String TAILLE_CHAMP_MOYEN = "2";

	/**
	 * L'identifiant d'un champ à taille courte.
	 */
	public static final String TAILLE_CHAMP_COURT = "3";

	public static final String TAILLE_CHAMP_TRES_LONG = "4";

	/**
	 * Retour slache virgule.
	 */
	public static final String ENCODE_COTE = "''";

	/**
	 * Retour virgule.
	 */
	public static final String COTE = "'";

	/**
	 * Document en cours de généreration en attente de telechargement.
	 */
	public static final String MAP_DOCUMENT_GENERER = "DOCUMENT_GENERER";

	public static final String TABLEAU_VIRGULE = "__VIRGULE__";
	public static final String TABLEAU_CROCHET_OUVERT = "__CROCHE_OUVERT__";
	public static final String TABLEAU_CROCHET_FERME = "__CROCHE_FERME__";
	/**
	 * attribut static statuts du document en cours .
	 */
	public static final String STATUT_EN_COURS = "EN COURS";
	/**
	 * Utilisé pour calculer le hashcode d'un bean.
	 */
	public static final int PREMIER = 31;

	public static final String S_CLAUSE = "clause";

	public static final String S_VERSION_CLAUSE = "versionClause";

	public static final String S_CLAUSE_ANCIENNE = "epmTClauseInstanceAncien";

	public static final String S_FORM_CLAUSE_TEXTE_FIXE = "frmClauseTexteFixe";
	public static final String S_FORM_CLAUSE_TEXTE_LIBRE = "frmClauseTexteLibre";
	public static final String S_FORM_CLAUSE_TEXTE_PREVALORISE = "frmClauseTextePrevalorise";
	public static final String S_FORM_CLAUSE_LISTE_CHOIX_CUMULATIF = "frmClauseListeChoixCumulatif";
	public static final String S_FORM_CLAUSE_LISTE_CHOIX_EXCLUSIF = "frmClauseListeChoixExclusif";
	public static final String S_FORM_CLAUSE_VALEUR_HERITEE = "frmClauseValeurHeritee";

	public static final String S_RESUME_CLAUSE = "recapClause";
	public static final String S_DETAIL_CONSULTATION = "detailConsultation";
	public static final String S_ID_CLAUSE = "idClauseEnCours";
	public static final String S_ROLE_CLAUSES_DEFAUT = "roleClausesDefaut";
	public static final String S_ROLE_CLAUSES = "roleClauses";
	public static final String S_ROLE_EDITER_FORMAT_ODT = "ROLE_editerFormatODT";

	public final static String S_CLAUSE_TYPE_CONTRAT_SELECTED = "typeContratTab";

	public static final String LIST_ID_DOCUMENTS = "listIdDocuments";

	public static final String TYPE_CLAUSE = "typeClause";

	public static final String CLAUSE_TABLEAU_DYNAMIQUE = "tableauDynamique";

	public static final String ID_NOUVELLE_CONSULTATION = "idNouvelleConsultation";

	public static final String NOUVELLE_CONSULTATION = "nouvelleConsultation";

	public static final String CONSULTATION_DU_DOCUMENT_A_DUPLIQUE = "consultationDuDocumentADupliquer";

	public static final String NUMERO_CONSULTATION = "numeroConsultation";

	public static final String CANEVAS_DU_DOCUMENT_A_DUPLIQUE = "canevasDuDocumentADupliquer";

	public static final String ID_DOCUMENT_INITIAL = "idDocumentInitial";

	public static final String ID_CONSULTATION_A_DUPLIQUER = "idAncienneConsultation";

	public static final String PARAMETRAGE_CHOIX_EXTENSION = "redaction.choixExtension";

	public static final String PARAMETRAGE_NUMEROTATION_CLAUSE_EDITEUR = "redaction.numerotationClauseEditeur";
	public static final String PARAMETRAGE_REFERENCE_SURCHARGE_CLAUSE = "redaction.referenceSurchargeClause";

	public static final String PARAMETRAGE_NUMEROTATION_CLAUSE_CLIENTS = "redaction.numerotationClauseClients";

	public static final String PARAMETRAGE_NUMEROTATION_CANEVAS_EDITEUR = "redaction.numerotationCanevasEditeur";

	public static final String PARAMETRAGE_NUMEROTATION_CANEVAS_CLIENTS = "redaction.numerotationCanevasClients";

	public static final String PARAMETRAGE_GENERATION_PAGE_GARDE = "redaction.generationPageDeGarde";

	public static final String GENERATION_PAGE_DOCUMENT = "generationPageDocument";

	public static final String TRUE = "true";
	/**
	 * Pour cocher la checkbox
	 */
	public static final String ON = "on";

	public static final String REDACTION_ENTITE_ADJUDICATRICE = "redaction.entiteAdjudicatrice";

	public static final String ENTITE_ADJUDICATRICE = "entiteAdjudicatrice";

	public static final String EDITEUR = "editeur";

	public static final String SURCHARGE = "surcharge";

	public static final String CLAUSE_EDITEUR = "clauseEditeur";

	public static final String CLAUSE_EDITEUR_SURCHARGE = "clauseEditeurSurcharge";

	public static final String NOM_METHODE = "nomMethode";

	public static final String AUTEUR_EDITEUR = "Editeur";

	public static final String AUTEUR_EDITEUR_SURCHARGE = "Editeur surchargée";

	public static final String AUTEUR_CLIENT = "Client";

	public static final String IDENTIFIANT_CONSULTATION = "idConsultation";
	public static final String IDENTIFIANT_PAGE_DE_GARDE = "idPageDeGarde";
	public static final String ID = "id";
	public static final String MODE = "mode";
	public static final String CATEGORIE = "categorie";
	public static final String ONLINE = "online";

	public static final String PARAMETRAGE_EXTENSION_EXPORT_CANEVAS = "redaction.extension.export.canevas";

	public static final String DOC = ".doc";

	public static final String ODT = ".odt";

	public static final String S_CLAUSE_INSTANCE = "epmTClauseInstance";

	public static final String PARAMETRAGE_MISEAJOUR_DATELIMITEREMISE = "miseAJour.dateLimiteRemise.generationDocumentRedaction";

	public static final String IMAGE_LOGO_BYTE = "imageLogoByte";

	public static final String IMAGE_LOGO_BASE64 = "imageLogoBase64";

	public static final String IMAGE_LOGO_TYPE = "imageLogoType";

	public static final String AIGUILLAGE_AJOUTER_LOGO = "ajouterLogo";

	public static final String AIGUILLAGE_SUPPRIMER_LOGO = "supprimerLogo";

	public static final String ACTION_COMPATIBILITE_ALLOTISSEMENT = "actionCompatibiliteAllotissement";

	public static final String TYPE = "type";

	public static final String CONSULTATION = "consultation";

}
