<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<script type="text/javascript"  language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"  language="JavaScript" src="js/editeurTexteRedaction.js"></script>

<!--Debut main-part-->
<div class="main-part">
	<div class="breadcrumbs">
		<logic:equal name="preferences" value="direction">
			<bean:message key="Parametrage.titre.direction" />
		</logic:equal>
		<logic:equal name="preferences" value="agent">
			<bean:message key="Parametrage.titre.agent" />
		</logic:equal>
	</div>
<atexo:fichePratique reference="" key="common.fichePratique"/>
	<div class="breaker"></div>

	<!--Debut bloc recap Type de clause-->
	<%@ include file="tableauRecapitulatifClause.jsp"%>
	<%@ include file="blocRecapClause.jsp"%>
	<!--Fin bloc recap Type de clause-->
	
	<!--Debut bloc Texte fixe avant-->
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>

        <div class="content">
            <div class="line">
                <span class="intitule">
                    <strong>
                        <bean:message key="ParametrageListeChoixCummlatif.txt.texteFixeAvant" />
                    </strong>
                </span>
                <div class="content-bloc-long">
                    <c:out value="${frmParametrageClauseListeChoix.texteFixeAvant}" />
                </div>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom">
            <span class="left"></span><span class="right"></span>
        </div>

	</div>
	<div class="spacer"></div>
	<!--Fin bloc Texte fixe avant-->
	<!--Debut bloc Infos clause-->
	<html:form action="/ParametrageClauseListeChoixCumulatifProcess.epm">
		<div class="form-saisie">
			<div class="form-bloc">
				<div class="top">
					<span class="left"></span><span class="right"></span>
				</div>
				<div class="content">

                    <!--Debut Parametrage par defaut-->
                    <div class="column-choix">
                        <html:radio property="parametre" title="Par défaut" value="false" styleId="parametreDirection" />
                        <bean:message key="ParametrageListeChoixCummlatif.txt.parDefaut" />
                    </div>
					<div class="clause-parametrage">
						<table>
							<thead>
								<tr>
									<th class="num-formulation">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.numFormulation" />
									</th>
									<th class="nb-carac">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.tailleChamp" />
									</th>
									<th class="long-col">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.valeurParDefaut" />
									</th>
									<th class="check-col">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.precoche" />
									</th>
								</tr>
							</thead>
							<logic:notEmpty name="roleClausesDefaut">
								<logic:iterate name="roleClausesDefaut" id="clauseListeRole"
									indexId="index">
									<bean:define id="lignePaire"><%=(index.intValue() % 2)%></bean:define>
									<tr
										<logic:notEqual name="lignePaire" value="0">class="on"</logic:notEqual>>
										<td class="num-formulation">
											<input name="numFormulationDefault"
                                                   title="Numéro de formulation par defaut"
                                                   value="<nested:write name="clauseListeRole" property="numFormulation"/>"
                                                   readonly="readonly" />
										</td>
										<td class="nb-carac">
											<nested:select styleId="tailleChampDefaut_${index}" name="clauseListeRole"
												property="nombreCarateresMax"
												disabled="true" styleClass="auto" title="Taille du Champ">
												<html:option value="1">
													<bean:message key="redaction.taille.champ.long" />
												</html:option>
												<html:option value="2">
													<bean:message key="redaction.taille.champ.moyen" />
												</html:option>
												<html:option value="3">
													<bean:message key="redaction.taille.champ.court" />
												</html:option>
												<html:option value="4">
													<bean:message key="redaction.taille.champ.tresLong" />
												</html:option>
											</nested:select>
										</td>
										<td class="long-col">
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="4">
												<textarea name="valeurDefaut" title="Valeur par defaut"
													class="texte-tres-long mceEditorSansToolbar" id="valeurDefaut_4${index}" cols="" rows="2"
													readonly="readonly" style="display: block"><atexo:retoureLigne name="clauseListeRole"
														property="valeurDefaut" /></textarea>
											</nested:equal>
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="1">
												<textarea name="valeurDefaut" title="Valeur par defaut"
													class="texte-long mceEditorSansToolbar" id="valeurDefaut_1${index}" cols="" rows="2"
													readonly="readonly" style="display: block"><atexo:retoureLigne name="clauseListeRole"
														property="valeurDefaut" /></textarea>
											</nested:equal>
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="2">
												<html:text name="clauseListeRole" property="valeurDefaut"
													title="Valeur par defaut" styleClass="texte-moyen"
													styleId="valeurDefaut_2${index}" style="display:block"
													readonly="true" errorStyleClass="error-border" />
											</nested:equal>
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="3">
												<html:text name="clauseListeRole" property="valeurDefaut"
													title="Valeur par defaut" styleClass="texte-court"
													styleId="valeurDefaut_3${index}" style="display:block"
													readonly="true" errorStyleClass="error-border" />
											</nested:equal>
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="0">
												<textarea name="valeurDefaut" title="Valeur par defaut"
													class="texte-long mceEditorSansToolbar" id="valeurDefaut_1${index}" cols="" rows="2"
													readonly="readonly" style="display: block"><atexo:retoureLigne name="clauseListeRole"
														property="valeurDefaut" /></textarea>
											</nested:equal>
										</td>
                                        <td class="check-col" style="text-align: left">
                                            <c:if test="${idTypeClause == 7}">
                                                <input type="checkbox" name="precocheeDefault" disabled="disabled" value="${index}"
                                                       <logic:equal name="clauseListeRole" property="precochee" value="1">checked="checked"</logic:equal> />
                                            </c:if>
                                            <c:if test="${idTypeClause == 6}">
                                                <input type="radio" name="precocheeDefault" disabled="disabled" value="${index}"
                                                       <logic:equal name="clauseListeRole" property="precochee" value="1">checked="checked"</logic:equal> />
                                            </c:if>
                                        </td>
									</tr>
									<nested:equal name="clauseListeRole" property="nombreCarateresMax" value="0">
										<script>
											var idTailleChamp = 'tailleChampDefaut_${index}';
											mettreTailleChampValeurDefaut(idTailleChamp);
										</script>
						  		  </nested:equal>
								</logic:iterate>
							</logic:notEmpty>
						</table>
					</div>
					<div class="separator"></div>
					<script type="text/javascript">
					 	initEditeursTexteSansToolbarRedaction();
					</script>
					<!--Fin Parametrage par defaut-->
					
					<!--Debut Parametrage -->
					<div class="column-choix">
						<html:radio property="parametre" title="Par direction"
							value="true" styleId="parametreDirection" />
				        <logic:equal name="preferences" value="direction">
						   <bean:message
							key="ParametrageListeChoixCummlatif.txt.parDirection" />
						</logic:equal>
						<logic:equal name="preferences" value="agent">
						 <bean:message
                            key="ParametrageListeChoixCummlatif.txt.parAgent" />
						</logic:equal>
					</div>
					<div class="clause-parametrage">
						<table>
							<thead>
								<tr>
									<th class="num-formulation">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.numFormulation" />
									</th>
									<th class="nb-carac">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.tailleChamp" />
									</th>
									<th class="long-col">
											<logic:equal name="preferences" value="direction">
											   <bean:message
												key="ClauseListeChoixCumulatif.txt.valeurParDirection" />
											</logic:equal>
											<logic:equal name="preferences" value="agent">
											 <bean:message
					                            key="ClauseListeChoixCumulatif.txt.valeurParAgent" />
											</logic:equal>
									</th>
									<th class="check-col">
										<bean:message
											key="ParametrageListeChoixCummlatif.txt.precoche" />
									</th>
								</tr>
							</thead>
							<logic:notEmpty name="roleClauses">
								<logic:iterate name="roleClauses" id="clauseListeRole"
									indexId="index">
									<bean:define id="lignePaire"><%=(index.intValue() % 2)%></bean:define>
									<tr
										<logic:notEqual name="lignePaire" value="0">class="on"</logic:notEqual>>
										<td class="num-formulation">
											<input name="numFormulation"
                                                   title="Numéro de formulation par defaut"
                                                   value="<nested:write name="clauseListeRole" property="numFormulation"/>"
                                                   readonly="readonly" />
										</td>
										<td class="nb-carac">
											<nested:select name="clauseListeRole" styleId="tailleChamp_${index}"
												property="nombreCarateresMax"
												styleClass="auto" title="Taille du Champ" disabled="true">
												<html:option value="1">
													<bean:message key="redaction.taille.champ.long" />
												</html:option>
												<html:option value="2">
													<bean:message key="redaction.taille.champ.moyen" />
												</html:option>
												<html:option value="3">
													<bean:message key="redaction.taille.champ.court" />
												</html:option>
												<html:option value="4">
													<bean:message key="redaction.taille.champ.tresLong" />
												</html:option>
											</nested:select>
										</td>
										<td class="long-col">
											<nested:equal name="clauseListeRole"
												property="nombreCarateresMax" value="4">
												<textarea name="valeur" title="Valeur par direction" class="texte-tres-long mceEditor"
													id="valeurParametrage0_4${index}" cols="" rows="4"
													style="display: block"><atexo:retoureLigne name="clauseListeRole" property="valeurDefaut" />
 												</textarea> 
											</nested:equal>
                                            <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="1">
                                                <textarea name="valeur" title="Valeur par direction" class="texte-long mceEditor"
                                                          id="valeurParametrage0_1${index}" cols="" rows="4" style="display: block">
                                                    <atexo:retoureLigne name="clauseListeRole" property="valeurDefaut" />
                                                </textarea>
                                            </nested:equal>
                                            <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="2">
                                                <input type="text" name="valeur" value="${clauseListeRole.valeurDefaut}" title="Valeur par direction"
                                                       class="texte-moyen" id="valeurParametrage0_2${index}" style="display: block" />
                                            </nested:equal>
                                            <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="3">
                                                <input type="text" name="valeur" value="${clauseListeRole.valeurDefaut}" title="Valeur par direction"
                                                       class="texte-court" id="valeurParametrage0_3${index}" style="display: block" />
                                            </nested:equal>
                                            <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="0">
                                                <textarea name="valeur" title="Valeur par direction" class="texte-long mceEditor"
                                                          id="valeurParametrage0_1${index}" cols="" rows="4" style="display: block">
                                                    <atexo:retoureLigne name="clauseListeRole" property="valeurDefaut" />
                                                </textarea>
                                            </nested:equal>
                                        </td>
                                        <td class="check-col" style="text-align: left">
                                            <c:if test="${idTypeClause == 7}">
                                                <input type="checkbox" name="precochee" value="${index}"
                                                       <logic:equal name="clauseListeRole" property="precochee" value="1">checked="checked"</logic:equal> />
                                            </c:if>
                                            <c:if test="${idTypeClause == 6}">
                                                <input type="radio" name="precochee" value="${index}"
                                                       <logic:equal name="clauseListeRole" property="precochee" value="1">checked="checked"</logic:equal> />
                                            </c:if>
                                        </td>
                                    </tr>
								
									  <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="0">
											<script>
												var idTailleChamp = 'tailleChamp_${index}';
												mettreTailleChampValeurDefaut(idTailleChamp);
											</script>
							  		  </nested:equal>
						  		  
								</logic:iterate>
							</logic:notEmpty>
						</table>
					</div>
				</div>
	            <div class="bottom">
	                <span class="left"></span><span class="right"></span>
	            </div>
			</div>
		</div>

		<div class="breaker"></div>
		<!--Fin bloc Infos clause-->
		
		<!--Debut bloc Texte fixe apres-->
		<div class="form-bloc">

			<div class="top">
				<span class="left"></span><span class="right"></span>
			</div>
			<div class="content">
				<div class="line">
                    <span class="intitule">
                        <strong>
                            <bean:message key="ParametrageListeChoixCummlatif.txt.texteFixeApres" />
                        </strong>
                    </span>
                    <div class="content-bloc-long">
                        <c:out value="${frmParametrageClauseListeChoix.texteFixeApres}" />
                    </div>
                </div>
				<div class="breaker"></div>
			</div>

			<div class="bottom">
				<span class="left"></span><span class="right"></span>
			</div>
		</div>
		<div class="spacer"></div>
		<!--Fin bloc Texte fixe apres-->
	</html:form>
    <!--Debut boutons-->
    <div class="boutons">
        <a href="ParametrageClauseInit.epm?preferences=${preferences}" class="annuler">
            <bean:message key="ParametrageListeChoixCummlatif.btn.annuler" />
        </a>
        <a href="javascript:document.forms[0].submit();" class="valider">
            <bean:message key="ParametrageListeChoixCummlatif.btn.valider" />
        </a>
    </div>
    <!--Fin boutons-->

    <div class="spacer"></div>

	<script type="text/javascript">
		initEditeursTexteRedaction();
	</script>

</div>
<!--Fin main-part-->
