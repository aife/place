package fr.paris.epm.redaction.coordination.bean.referentiels;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefPouvoirAdjudicateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.io.Serializable;
import java.util.List;

/**
 * @author Léon Barsamian
 *
 */
public class ReferentielsDocumentsBean implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 2241902589695008961L;

    
    /**
     * Liste des natures de prestation.
     */
    private List<EpmTRefNature> naturePrestasCollection;
    
    /**
     * Liste des procédures de passation.
     */
    private List<EpmTRefProcedure> procedureCollection;
    
    /**
     * Liste des pouvoirs adjudicateurs.
     */
    private List<EpmTRefPouvoirAdjudicateur> pouvoirAdjudicateurCollection;
    
    /**
     * Liste des directions / services.
     */
    private List<EpmTRefDirectionService> directionServiceCollection;

    /**
     * @return Liste des natures de prestation.
     */
    public final List<EpmTRefNature> getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur Liste des natures de prestation.
     */
    public final void setNaturePrestasCollection(List<EpmTRefNature> valeur) {
        this.naturePrestasCollection = valeur;
    }

    public List<EpmTRefProcedure> getProcedureCollection() {
        return procedureCollection;
    }

    public void setProcedureCollection(List<EpmTRefProcedure> procedureCollection) {
        this.procedureCollection = procedureCollection;
    }

    /**
     * @return Liste des pouvoirs adjudicateurs.
     */
    public final List<EpmTRefPouvoirAdjudicateur> getPouvoirAdjudicateurCollection() {
        return pouvoirAdjudicateurCollection;
    }

    /**
     * @param valeur Liste des pouvoirs adjudicateurs.
     */
    public final void setPouvoirAdjudicateurCollection(
            List<EpmTRefPouvoirAdjudicateur> valeur) {
        this.pouvoirAdjudicateurCollection = valeur;
    }

    /**
     * @return Liste des directions / services.
     */
    public final List<EpmTRefDirectionService> getDirectionServiceCollection() {
        return directionServiceCollection;
    }

    /**
     * @param valeur Liste des directions / services.
     */
    public final void setDirectionServiceCollection(
            List<EpmTRefDirectionService> valeur) {
        directionServiceCollection = valeur;
    }


}
