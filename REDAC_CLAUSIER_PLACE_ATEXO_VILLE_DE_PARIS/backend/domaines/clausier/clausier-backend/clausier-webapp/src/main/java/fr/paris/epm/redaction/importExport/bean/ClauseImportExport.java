package fr.paris.epm.redaction.importExport.bean;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ClauseImportExport {

    private int idClause;

    private String document;

    private String type;

    private String theme;

    private String reference;

    private String texteFixeAvant;

    private String texteFixeApres;

    private Date dateModification;

    private Date dateCreation;

    private Date datePremiereValidation;

    private Date dateDerniereValidation;

    private String auteur;

    private boolean actif;

    private String naturePrestation;

    private String procedure;

    private String etat;

    private boolean formulationModifiable;

    private boolean sautLigneTexteAvant;

    private boolean sautLigneTexteApres;

    private String statutRedactionClausier;

    private boolean compatibleEntiteAdjudicatrice = true;

    private boolean clauseEditeur;

    private String parametrableDirection;

    private String parametrableAgent;

    private Set<String> contrats = new HashSet<>();

    private Set<RoleClauseImportExport> roles = new HashSet<>();

    private Set<ClausePotentiellementConditionneeImportExport> clausesPotentiellementConditionnee = new HashSet<>();

    private String motsCles;

    private String infoBulleText;

    private String infoBulleUrl;

    public int getIdClause() {
        return idClause;
    }

    public void setIdClause(int idClause) {
        this.idClause = idClause;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument( String document ) {
        this.document = document;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme( String theme ) {
        this.theme = theme;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    public void setTexteFixeAvant(String texteFixeAvant) {
        this.texteFixeAvant = texteFixeAvant;
    }

    public String getTexteFixeApres() {
        return texteFixeApres;
    }

    public void setTexteFixeApres(String texteFixeApres) {
        this.texteFixeApres = texteFixeApres;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDatePremiereValidation() {
        return datePremiereValidation;
    }

    public void setDatePremiereValidation(Date datePremiereValidation) {
        this.datePremiereValidation = datePremiereValidation;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getNaturePrestation() {
        return naturePrestation;
    }

    public void setNaturePrestation( String naturePrestation ) {
        this.naturePrestation = naturePrestation;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure( String procedure ) {
        this.procedure = procedure;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public boolean isFormulationModifiable() {
        return formulationModifiable;
    }

    public void setFormulationModifiable(boolean formulationModifiable) {
        this.formulationModifiable = formulationModifiable;
    }

    public boolean isSautLigneTexteAvant() {
        return sautLigneTexteAvant;
    }

    public void setSautLigneTexteAvant(boolean sautLigneTexteAvant) {
        this.sautLigneTexteAvant = sautLigneTexteAvant;
    }

    public boolean isSautLigneTexteApres() {
        return sautLigneTexteApres;
    }

    public void setSautLigneTexteApres(boolean sautLigneTexteApres) {
        this.sautLigneTexteApres = sautLigneTexteApres;
    }

    public String getStatutRedactionClausier() {
        return statutRedactionClausier;
    }

    public void setStatutRedactionClausier( String statutRedactionClausier ) {
        this.statutRedactionClausier = statutRedactionClausier;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    public boolean isClauseEditeur() {
        return clauseEditeur;
    }

    public void setClauseEditeur(boolean clauseEditeur) {
        this.clauseEditeur = clauseEditeur;
    }

    public String getParametrableDirection() {
        return parametrableDirection;
    }

    public void setParametrableDirection(String parametrableDirection) {
        this.parametrableDirection = parametrableDirection;
    }

    public String getParametrableAgent() {
        return parametrableAgent;
    }

    public void setParametrableAgent(String parametrableAgent) {
        this.parametrableAgent = parametrableAgent;
    }

    public Set<String> getContrats() {
        return contrats;
    }

    public void setContrats( Set<String> contrats ) {
        this.contrats = contrats;
    }

    public Set<RoleClauseImportExport> getRoles() {
        return roles;
    }

    public void setRoles( Set<RoleClauseImportExport> roles ) {
        this.roles = roles;
    }

    public Set<ClausePotentiellementConditionneeImportExport> getClausesPotentiellementConditionnee() {
        return clausesPotentiellementConditionnee;
    }

    public void setClausesPotentiellementConditionnee( Set<ClausePotentiellementConditionneeImportExport> clausesPotentiellementConditionnee ) {
        this.clausesPotentiellementConditionnee = clausesPotentiellementConditionnee;
    }

    public String getMotsCles() {
        return motsCles;
    }

    public void setMotsCles(String motsCles) {
        this.motsCles = motsCles;
    }

    public String getInfoBulleText() {
        return infoBulleText;
    }

    public void setInfoBulleText(String infoBulleText) {
        this.infoBulleText = infoBulleText;
    }

    public String getInfoBulleUrl() {
        return infoBulleUrl;
    }

    public void setInfoBulleUrl(String infoBulleUrl) {
        this.infoBulleUrl = infoBulleUrl;
    }

}
