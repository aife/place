package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.facade.Facade;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.bean.CanevasSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;

/**
 * Facade de gestion des Canevas Niveau Ministeriel
 * Created by nty on 04/07/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface CanevasViewFacade extends Facade<CanevasBean> {


    PageRepresentation<CanevasBean> findCanevas(CanevasSearch search, EpmTRefOrganisme epmTRefOrganisme);
}
