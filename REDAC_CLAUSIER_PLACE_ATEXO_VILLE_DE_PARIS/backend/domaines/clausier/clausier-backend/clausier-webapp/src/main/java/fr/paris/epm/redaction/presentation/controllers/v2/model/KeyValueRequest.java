package fr.paris.epm.redaction.presentation.controllers.v2.model;

import javax.validation.constraints.NotEmpty;


public class KeyValueRequest {
    @NotEmpty
    private String key;
    private Object value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
