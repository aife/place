package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class DirectoryMapper { // TODO: fusionner avec fr.paris.epm.global.coordination.mapper.DirectoryMapper après la fusion NOYAU-REDAC

    private RedactionServiceSecurise redactionService;

	private ReferentielsServiceSecurise referentielsServiceSecurise;

    @Mapping(source = "libelle", target = "label")
    public abstract Directory toDirectoryBean( EpmTRef referentiel);

    @Mapping(source = "libelle", target = "label")
    public abstract Directory toDirectoryBean( BaseEpmTRefRedaction abstractRedactionReferentiel);

    @Mapping(source = "libelle", target = "label")
    @Mapping(source = "libelleCourt", target = "shortLabel")
    public abstract Directory toDirectoryBean(EpmTRefProcedure epmTRefProcedure);

    @Mapping(source = "libelle", target = "label")
    @Mapping(source = "abreviation", target = "shortLabel")
    public abstract Directory toDirectoryBean(EpmTRefTypeContrat epmTRefTypeContrat);

    public EpmTRefTypeDocument toEpmTRefTypeDocument(Directory directory) {
        return epmTRefTypeDocumentById(directory.getId());
    }

    public EpmTRefProcedure toEpmTRefProcedure(Directory directory) {
        return epmTRefProcedureById(directory.getId());
    }

    public EpmTRefTypeContrat toEpmTRefTypeContrat(Directory directory) {
        return epmTRefTypeContratById(directory.getId());
    }

	public Integer id( EpmTRef referentiel) {
		if (referentiel == null)
			return null;
		return referentiel.getId();
	}

    public String label( EpmTRef referentiel ) {
        if (referentiel == null)
            return null;
        return referentiel.getLibelle();
    }

    public EpmTRefTypeClause epmTRefTypeClauseById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefTypeClause.class);
    }

    public EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausierById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefStatutRedactionClausier.class);
    }

    public EpmTRefTypeDocument epmTRefTypeDocumentById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefTypeDocument.class);
    }

    public EpmTRefThemeClause epmTRefThemeClauseById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefThemeClause.class);
    }

    public EpmTRefTypeContrat epmTRefTypeContratById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefTypeContrat.class);
    }

    public EpmTRefProcedure epmTRefProcedureById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefProcedure.class);
    }

    public EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionneeById(Integer id) {
        if (id == null)
            return null;
        return redactionService.chercherObject(id, EpmTRefPotentiellementConditionnee.class);
    }

    public void setRedactionService(RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

}
