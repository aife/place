package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTTemplateVersion;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GabaritForm extends ActionForm {

    /**
     * Niveau d'intervention (referentiel : Interministeriel, Ministeriel...)
     */
    private Integer idIntervention;

    private String idTypeDocumentOuPageGarde;

    private Integer idRepresentant;

    private List<String> listClesTemplates = new ArrayList<>(); // utilisé pour trier la map des templates
    private Map<String, String> mapTemplates = new HashMap<>();

    private List<EpmTTemplateVersion> listLastVersions = new ArrayList<EpmTTemplateVersion>();

    private List<EpmTTemplateVersion> listMyVersions = new ArrayList<EpmTTemplateVersion>();

    private String nomFichier;

    private FormFile fichier; // utilisé pendant la création d'une nouvelle version

    private Map<Integer, String> mapCanvas = new HashMap<>(); // ajouté pour réalisé la vérifacation avec un canvas existé

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        // laisse cette méthode vide, my friend
        super.reset(mapping, request);
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        // laisse cette méthode vide, my friend
        return super.validate(mapping, request);
    }

    public Integer getIdIntervention() {
        return idIntervention;
    }

    public void setIdIntervention(Integer idIntervention) {
        this.idIntervention = idIntervention;
    }

    public String getIdTypeDocumentOuPageGarde() {
        return idTypeDocumentOuPageGarde;
    }

    public void setIdTypeDocumentOuPageGarde(String idTypeDocumentOuPageGarde) {
        this.idTypeDocumentOuPageGarde = idTypeDocumentOuPageGarde;
    }

    public Integer getIdRepresentant() {
        return idRepresentant;
    }

    public void setIdRepresentant(Integer idRepresentant) {
        this.idRepresentant = idRepresentant;
    }

    public List<String> getListClesTemplates() {
        return listClesTemplates;
    }

    public void setListClesTemplates(List<String> listClesTemplates) {
        this.listClesTemplates = listClesTemplates;
    }

    public Map<String, String> getMapTemplates() {
        return mapTemplates;
    }

    public void setMapTemplates(Map<String, String> mapTemplates) {
        this.mapTemplates = mapTemplates;
    }

    public List<EpmTTemplateVersion> getListLastVersions() {
        return listLastVersions;
    }

    public void setListLastVersions(List<EpmTTemplateVersion> listLastVersions) {
        this.listLastVersions = listLastVersions;
    }

    public List<EpmTTemplateVersion> getListMyVersions() {
        return listMyVersions;
    }

    public void setListMyVersions(List<EpmTTemplateVersion> listMyVersions) {
        this.listMyVersions = listMyVersions;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public FormFile getFichier() {
        return fichier;
    }

    public void setFichier(FormFile fichier) {
        this.fichier = fichier;
    }

    public Map<Integer, String> getMapCanvas() {
        return mapCanvas;
    }

    public void setMapCanvas(Map<Integer, String> mapCanvas) {
        this.mapCanvas = mapCanvas;
    }

}
