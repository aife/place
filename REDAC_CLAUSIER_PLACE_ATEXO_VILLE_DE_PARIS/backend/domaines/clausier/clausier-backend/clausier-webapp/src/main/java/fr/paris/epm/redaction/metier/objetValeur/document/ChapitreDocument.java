package fr.paris.epm.redaction.metier.objetValeur.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.paris.epm.redaction.metier.objetValeur.commun.Derogation;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;

import java.util.ArrayList;
import java.util.List;

/**
 * la classe ChapitreDocument manipule des chapitre d'un document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ChapitreDocument extends AbstractChapitreDocument implements Cloneable {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * l'attribut identifiant.
     */
    private int id;

    /**
     * l'attribut référence.
     */
    private String ref;

    /**
     * attribut qui indique si le chapitre est valide.
     */
    private boolean valide;

    /**
     * titre du chapitre.
     */
    private String titre;

    /**
     * chapitre parent.
     */
    @JsonIgnore
    private AbstractChapitreDocument parent;

    /**
     * l'objet info-Bulle.
     */
    private InfoBulle infoBulle;

    /**
     * Le style du chapitre
     */
    private String styleChapitre;

    /**
     * la liste des clause d'un document.
     */
    private List<ClauseDocument> clausesDocument = new ArrayList<ClauseDocument>();

    /**
     * l'objet paragrapheDocument.
     */
    private ParagrapheDocument paragrapheDocument;

    /**
     * l'ensemble des sous chapitres du chapitre en cours.
     */
    private List<ChapitreDocument> sousChapitres = new ArrayList<ChapitreDocument>();

    /**
     * numéro du chapitre.
     */
    private String numero;

    private Integer numeroProvisoire;

    private String nombreSautLignes;

    private boolean duplique;

    /**
     *   Suppression logique d'un chapitre
     */
    private boolean supprime;

    /**
     * Dérogation au CCAG 
     */
    private Derogation derogation;

    /**
     * Afficher le message de dérogation à la fin de l'article
     */
    private boolean afficherNBMessageDerogation;

    /**
     * Détermine si le chapitre initial a était repris integralement et que le chapitre en cours
     * a repris les clauses de l'article ont été reprises du canevas et non du document A
     */
    private boolean reprisIntegralementAvantDuplication;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public AbstractChapitreDocument getParent() {
        return parent;
    }

    public void setParent(AbstractChapitreDocument parent) {
        this.parent = parent;
    }

    public InfoBulle getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulle infoBulle) {
        this.infoBulle = infoBulle;
    }

    public String getStyleChapitre() {
        return styleChapitre;
    }

    public void setStyleChapitre(String styleChapitre) {
        this.styleChapitre = styleChapitre;
    }

    public List<ClauseDocument> getClausesDocument() {
        return clausesDocument;
    }

    public void setClausesDocument(List<ClauseDocument> clausesDocument) {
        this.clausesDocument = clausesDocument;
    }

    public ParagrapheDocument getParagrapheDocument() {
        return paragrapheDocument;
    }

    public void setParagrapheDocument(ParagrapheDocument paragrapheDocument) {
        this.paragrapheDocument = paragrapheDocument;
    }

    public List<ChapitreDocument> getSousChapitres() {
        return sousChapitres;
    }

    public void setSousChapitres(List<ChapitreDocument> sousChapitres) {
        this.sousChapitres = sousChapitres;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getNumeroProvisoire() {
        return numeroProvisoire;
    }

    public void setNumeroProvisoire(Integer numeroProvisoire) {
        this.numeroProvisoire = numeroProvisoire;
    }

    public String getNombreSautLignes() {
        return nombreSautLignes;
    }

    public void setNombreSautLignes(String nombreSautLignes) {
        this.nombreSautLignes = nombreSautLignes;
    }

    public boolean isDuplique() {
        return duplique;
    }

    public void setDuplique(boolean duplique) {
        this.duplique = duplique;
    }

    public boolean isSupprime() {
        return supprime;
    }

    public void setSupprime(boolean supprime) {
        this.supprime = supprime;
    }

    public Derogation getDerogation() {
        return derogation;
    }

    public void setDerogation(Derogation derogation) {
        this.derogation = derogation;
    }

    public boolean isAfficherNBMessageDerogation() {
        return afficherNBMessageDerogation;
    }

    public void setAfficherNBMessageDerogation(boolean afficherNBMessageDerogation) {
        this.afficherNBMessageDerogation = afficherNBMessageDerogation;
    }

    public boolean isReprisIntegralementAvantDuplication() {
        return reprisIntegralementAvantDuplication;
    }

    public void setReprisIntegralementAvantDuplication(boolean reprisIntegralementAvantDuplication) {
        this.reprisIntegralementAvantDuplication = reprisIntegralementAvantDuplication;
    }

    public String getNumeroProvisoireAsString() {
        if (parent != null && parent instanceof ChapitreDocument)
            return ((ChapitreDocument) parent).getNumeroProvisoireAsString() + "." + (numeroProvisoire == null ? "" : numeroProvisoire.toString());
        else {
            String no = numero != null ? numero : "";
            return (numeroProvisoire == null ? no : numeroProvisoire.toString());
        }
    }

    /**
     * cette méthode perecise le niveau dans l'arboresence.
     * @return niveau.
     */
    public int getNiveau() {
        if (numero != null)
            return (numero + ".").length() / 2;
        return 0;
    }

    public boolean isTerminer() {
        if (sousChapitres != null)
            for (ChapitreDocument sousChapitre : sousChapitres)
                if (!sousChapitre.isTerminer())
                    return false;

        if (paragrapheDocument != null) {
            return valide;
        } else if (clausesDocument != null) {
            for (ClauseDocument clause : clausesDocument) {
                if (clause.getEtat().equals(ClauseDocument.ETAT_CONDITIONNER))
                    continue;

                if (clause.getType() != ClauseDocument.TYPE_CLAUSE_TEXTE_FIXE &&
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE &&
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_TEXTE_HERITE &&  // support des annciens documents
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_TABLEAU &&       // support des annciens documents
                        !clause.isTerminer())
                        return false;
            }
        }
        return true;
    }

    /**
     * cette méthode teste la validité des clauses d'un document.
     * @return boolean.
     */
    public boolean isGrey() {
        if (paragrapheDocument != null) {
            return false;
        } else if (clausesDocument != null) {
            for (ClauseDocument clause : clausesDocument) {
                if (clause.getEtat().equals(ClauseDocument.ETAT_CONDITIONNER))
                    continue;

                if (clause.getType() != ClauseDocument.TYPE_CLAUSE_TEXTE_FIXE &&
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE &&
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_TEXTE_HERITE &&  // support des annciens documents
                        clause.getType() != ClauseDocument.TYPE_CLAUSE_TABLEAU &&       // support des annciens documents
                        clause.isTexteModifiable())
                    return false;
            }
        }

        if (sousChapitres != null) {
            for (ChapitreDocument sousChapitre : sousChapitres) {
                if (!sousChapitre.isTerminer())
                    return false;
                else if (!sousChapitre.isGrey())
                    return false;
            }
        }
        return true;
    }

    /**
     * Défine si la couleur du chapitre affichera en orange sur le sommaire. Il
     * s'affichera en orange si clause est provenant d'un document dupliqué
     * @return true si orange
     */
    public boolean isOrange() {
        if (paragrapheDocument != null) {
            return false;
        } else if (clausesDocument != null && !clausesDocument.isEmpty()) {
            for (ClauseDocument clause : clausesDocument) {
                if (clause.isDuplique()) {
                    duplique = true;
                    return true;
                }
            }
        } else  if (sousChapitres != null) {
            for (ChapitreDocument sousChapitre : sousChapitres) {
                if (sousChapitre.isOrange()) {
                    duplique = true;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChapitreDocument that = (ChapitreDocument) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + id;
        return result;
    }

    /**
     * cette méthode permet la conversion de type en String.
     * @return la chaîne de caractère convertie.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Chapitre : ");
        sb.append("\nTitre").append(titre);
        sb.append("\nInfobulle: ").append(infoBulle);
        if (clausesDocument != null) {
            sb.append("\nClauses: ").append(clausesDocument.size());

            for (ClauseDocument clause : clausesDocument)
                sb.append(clause);
        }
        if (sousChapitres != null) {
            sb.append("\t\n Sous chapitres: ").append(sousChapitres.size());
            for (ChapitreDocument chap : sousChapitres)
                sb.append(chap);
        }
        return sb.toString();
    }

    /**
     * @return ne clone pas le chapitre parent et les sous chapitres.
     */
    @Override
    public final Object clone() {
        ChapitreDocument chapitre = new ChapitreDocument();
        chapitre.id = id;
        chapitre.ref = ref;
        chapitre.valide = valide;
        chapitre.titre = titre;
        chapitre.supprime = supprime;
        if (infoBulle != null)
            chapitre.infoBulle = (InfoBulle) infoBulle.clone();

        if (clausesDocument != null) {
            for (ClauseDocument clause : clausesDocument) {
                if (clause instanceof ClauseTexte) {
                    ClauseTexte clauseTexte = (ClauseTexte) clause;
                    chapitre.clausesDocument.add((ClauseDocument) clauseTexte.cloneText(chapitre));
                } else {
                    ClauseListe clauseList = (ClauseListe) clause;
                    chapitre.clausesDocument.add((ClauseDocument) clauseList.cloneList(chapitre));
                }
            }
        }

        chapitre.paragrapheDocument = paragrapheDocument;
        chapitre.numero = numero;
        chapitre.nombreSautLignes = nombreSautLignes;
        chapitre.styleChapitre = styleChapitre;
        chapitre.derogation = derogation;
        chapitre.afficherNBMessageDerogation = afficherNBMessageDerogation;

        return chapitre;
    }

    /**
     * @return clone le chapitre avec le chapitre parent et les sous chapitres.
     */
    public final Object clone(AbstractChapitreDocument parent) {
        ChapitreDocument chapitre = (ChapitreDocument) clone();
        chapitre.parent = parent;
        if (sousChapitres != null)
            for (ChapitreDocument chapitreDocument : sousChapitres)
                chapitre.sousChapitres.add((ChapitreDocument) chapitreDocument.clone(chapitre));
        return chapitre;
    }

}
