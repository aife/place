package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.tags.HrefTag;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefValeurTypeClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.metier.documentHandler.GenerationTableauUtil;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseTexte;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

/**
 * Tag gérant l'affichage des propriétés d'une clause.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class PrevisualiserPptExtClauseTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PrevisualiserPptExtClauseTag.class);

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * emplacement de la variable (request, session, parameter).
     */
    private String scope;

    private String user;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * injecté par spring.
     */
    private static ClauseFacadeGWT clauseFacadeGWT;

    private static ReferentielFacade referentielFacade;

    /*
     * (non-Javadoc) @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    public int doStartTag() throws JspException {
        try {
            EpmTUtilisateur connectedUser = null;
            StringBuilder resultat = new StringBuilder();
            Object objetClause = tagUtils.lookup(pageContext, name, scope);
            if (objetClause == null)
                return Tag.SKIP_BODY;

            if (user != null)
                connectedUser = (EpmTUtilisateur) tagUtils.lookup(pageContext, user, null);

            if (objetClause instanceof EpmTClauseAbstract) {
                EpmTClauseAbstract clause = (EpmTClauseAbstract) objetClause;
                LOG.debug("EpmTClause " + clause.getReference());
                List<? extends EpmTRoleClauseAbstract> rolesUser = clauseFacadeGWT.recupererRolesClause(connectedUser, clause, false);
                if (clause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF ||
                        clause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                    resultat.append(listClauses(clause, (List<EpmTRoleClauseAbstract>) rolesUser));
                } else {
                    for (EpmTRoleClauseAbstract epmTRoleClause : rolesUser) {
                        switch (clause.getEpmTRefTypeClause().getId()) {
                            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
                                resultat.append(clausePrevalorisee(epmTRoleClause));
                                break;
                            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
                                resultat.append(clauseTexteLibre(epmTRoleClause));
                                break;
                            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                                throw new JspException("La clause de type Texte Héritée est obsolete; Utilisez le type Valeur Héritée");
                            case EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                                throw new JspException("La clause de type Tableau est obsolete; Utilisez le type Valeur Héritée");
                            case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                                resultat.append(clauseValeurHerite(epmTRoleClause));
                                break;
                        }
                    }
                }
            } else if (objetClause instanceof ClauseTexte) {
                ClauseTexte clause = (ClauseTexte) objetClause;
                LOG.debug("ClauseTexte " + clause.getId());
                if (clause.getType() == ClauseTexte.TYPE_CLAUSE_TEXTE_HERITE || clause.getType() == ClauseTexte.TYPE_CLAUSE_TABLEAU) { // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new JspException("La clause de ces types est obsolete; Utilisez le type Valeur Héritée");
                } else if (clause.getTexteVariable() != null) {
                    resultat.append(clause.getTexteVariable());
                }
            }
            pageContext.getOut().print(resultat.toString());
        } catch (IOException | NumberFormatException | TechnicalException ex) {
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
            throw new JspException("Erreur d'E/S", ex);
        }
        return Tag.SKIP_BODY;
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        user = null;
        name = null;
        scope = null;
    }

    /**
     * @param epmTRoleClause propriété de la clause
     * @return HTML représentant la propriété de la clause à afficher
     */
    private static String clauseTexteLibre(final EpmTRoleClauseAbstract epmTRoleClause) {
        StringBuilder buff = new StringBuilder();
        if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_COURT)) {
            buff.append("<input id='texteLibre' type='text' ");
            buff.append("class='texte-court'/>");
        } else if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_MOYEN)) {
            buff.append("<input id='texteLibre' type='text' ");
            buff.append("class='texte-moyen' />");
        } else if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_LONG)) {
            buff.append("<textarea cols='' rows='2' ");
            buff.append("class='texte-long mceEditorSansToolbar' ></textarea>");
        }else if  (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_TRES_LONG)){
            buff.append("<textarea cols='' rows='2' ");
            buff.append("class='texte-tres-long mceEditorSansToolbar' ></textarea>");
        }
        
        if (epmTRoleClause.getChampObligatoire() != null && epmTRoleClause.getChampObligatoire().equals("1")) {
            buff.append("<IMG src='" + HrefTag.getHrefRacine() + "images/ast.gif' ");
            buff.append("alt='' align='top' />");
        }
        return buff.toString();
    }
    
    /**
     * @param epmTRoleClause propriété de la clause
     * @return HTML represantantnune des propriétés de la clause
     */
    private static String clausePrevalorisee(final EpmTRoleClauseAbstract epmTRoleClause) {
        StringBuilder buff = new StringBuilder();
        if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_COURT)) {
            buff.append("<input id=\"textePrevalorise\" type=\"text\" ");
            buff.append("class=\"texte-court\" value=\"");
            buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
            buff.append("\" />");
        } else if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_MOYEN)) {
            buff.append("<input id=\"textePrevalorise\" type=\"text\" ");
            buff.append("class='texte-moyen' value=\"");
            buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
            buff.append("\" />");
        } else if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_LONG)) {
            buff.append("<textarea cols=\"\" ");
            buff.append("rows=\"2\" class=\"texte-long mceEditorSansToolbar\" >");
            buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
            buff.append("</textarea>");
        }else if  (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_TRES_LONG)){
            buff.append("<textarea cols=\"\" ");
            buff.append("rows=\"2\" class=\"texte-tres-long mceEditorSansToolbar\" >");
            buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
            buff.append("</textarea>");
        }
        return buff.toString();
    }

    /**
     * @param clause en cours.
     * @param rolesUser la liste des roles.
     * @return contenu clause
     */
    private static String listClauses(final EpmTClauseAbstract clause, final List<EpmTRoleClauseAbstract> rolesUser) {
        StringBuilder buff = new StringBuilder("");
        int nbrFormulation = rolesUser.size();
        int index = 0;
        for (EpmTRoleClauseAbstract epmTRoleClause : rolesUser) {
            index++;
            buff.append("<p>");

            if (clause.isFormulationModifiable()) {
                if (epmTRoleClause.getNombreCarateresMax() == 1) {
                    buff.append("<textArea name=\"valeurDefaut\" cols=\"\" rows=\"2\" class=\"texte-long mceEditorSansToolbar\">");
                    buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
                    buff.append("</textArea> ");
                } else if (epmTRoleClause.getNombreCarateresMax() == 2) {
                    buff.append("<input id='valDef' type=\"text\" name=\"valeurDefaut\" class=\"texte-moyen\" value=\"");
                    buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
                    buff.append("\" /> ");
                } else if (epmTRoleClause.getNombreCarateresMax() == 3) {
                    buff.append("<input id='valDef' type=\"text\" name=\"valeurDefaut\" class=\"texte-court\" value=\"");
                    buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
                    buff.append("\" /> ");
                } else if (epmTRoleClause.getNombreCarateresMax() == Integer.parseInt(Constante.TAILLE_CHAMP_TRES_LONG)) {
                    buff.append("<textArea name=\"valeurDefaut\" cols=\"\" rows=\"2\" class=\"texte-tres-long mceEditorSansToolbar\">");
                    buff.append(Util.decodeCaractere(epmTRoleClause.getValeurDefaut()));
                    buff.append("</textArea> ");
                }
            }
            
            if (clause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                buff.append("<input id='checkValDef' class=\"input-precochee\" type=\"radio\" name=\"");
                buff.append(clause.getReference());
                buff.append("\" ");
                if (epmTRoleClause.isPrecochee())
                    buff.append("checked=\"checked\" ");
                buff.append("/> ");
            }

            if (clause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF) {
                buff.append("<input id='checkValDef' class=\"input-precochee\" type=\"checkbox\" name=\"");
                buff.append(clause.getReference());
                buff.append("\" ");
                if (epmTRoleClause.isPrecochee())
                    buff.append("checked=\"checked\" ");
                buff.append("/> ");
            }

            if (!clause.isFormulationModifiable()) {
                buff.append(epmTRoleClause.getValeurDefaut().replaceAll("\r", "<br/>"));
                buff.append(" ");
            }
            if (index < nbrFormulation) { 
                buff.append("<br/> ");
            }
            buff.append("</p>");
        }
        return buff.toString();
    }

    /**
     * @param epmTRoleClause propriété de la clause
     * @return HTML à afficher
     * @throws TechnicalException erreur
     */
    private static String clauseValeurHerite(final EpmTRoleClauseAbstract epmTRoleClause) throws TechnicalException {

        if (epmTRoleClause.getValeurDefaut() == null) {
            LOG.warn("La référence de la valeur héritée est vide pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
            throw new TechnicalException("La référence de la valeur héritée est vide pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
        }

        int idRefValeur = Integer.parseInt(epmTRoleClause.getValeurDefaut());
        if (idRefValeur <= 0) {
            LOG.warn("La référence de la valeur héritée est faut pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
            throw new TechnicalException("La référence de la valeur héritée est faut pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
        }

        EpmTRefValeurTypeClause epmTRefValeurTypeClause = referentielFacade.getValeurTypeClauseById(idRefValeur);

        if (epmTRefValeurTypeClause == null) {
            LOG.warn("La référence de la valeur héritée est faut pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
            throw new TechnicalException("La référence de la valeur héritée est faut pour la clause : " + epmTRoleClause.getEpmTClause().getReference());
        }

        if (epmTRefValeurTypeClause.isSimple()) {
            return "[ " + epmTRefValeurTypeClause.getLibelle() + " ]";
        } else {
            TableauRedaction tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(epmTRefValeurTypeClause.getExpression(), null, null, null);
            return GenerationTableauUtil.tableauRedactionToHTML(tableauRedaction);
        }
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }

    /**
     * @param valeur emplacement de la variable (request, session, parameter)
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }

    /**
     * @param valeur utilisateur connecté
     */
    public final void setUser(final String valeur) {
        this.user = valeur;
    }

    /**
     * @param valeur injecté par spring
     */
    public final void setClauseFacadeGWT(final ClauseFacadeGWT valeur) {
        PrevisualiserPptExtClauseTag.clauseFacadeGWT = valeur;
    }

    /**
     * @param valeur injecté par spring
     */
    public final void setReferentielFacade(final ReferentielFacade valeur) {
        PrevisualiserPptExtClauseTag.referentielFacade = valeur;
    }

}
