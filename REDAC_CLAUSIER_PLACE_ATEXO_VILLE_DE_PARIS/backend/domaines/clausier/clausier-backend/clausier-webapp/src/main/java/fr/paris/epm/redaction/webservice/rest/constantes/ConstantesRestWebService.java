/**
 * $Id$
 */
package fr.paris.epm.redaction.webservice.rest.constantes;

import fr.paris.epm.redaction.metier.objetValeur.document.Document;

/**
 * Constantes utilisees dans le cadre des Web Services REST.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public final class ConstantesRestWebService {
    /**
     * Nombre de digit pour la creation du token des webservices REST.
     */
    public static final String XML_QNAME_DOCUMENTS = "documents";
    /**
     * Balise encapsulant un message REST erreur.
     */
    public static final String XML_QNAME_ERREUR = "erreur";
    /**
     * Balise encapsulant un message REST code de retour de téléchargement du document.
     */
    public static final String XML_QNAME_REPONSE = "reponse";
    /**
     * Pour differencier les codes d'erreur.
     */
    public enum StatutDocumentEnum {

        VALIDER(Document.STATUT_VALIDER), 
        EN_ATTENTE_VALIDATION(Document.STATUT_EN_ATTENTE_VALIDATION), 
        EN_COURS_REDACTION(Document.STATUT_EN_COURS);

        private final String valeur;

        StatutDocumentEnum(String valeurParam) {
            valeur = valeurParam;
        }

        public String getValeur() {
            return valeur;
        }
        
        public static StatutDocumentEnum getStatutDocumentEnum(String valeur){
            for (StatutDocumentEnum statutDocumentEnum : StatutDocumentEnum.values()) {
                if(statutDocumentEnum.valeur.equals(valeur)){
                    return statutDocumentEnum;
                }
            }
            return null;
        }
    };
    
    /**
     * Pour differencier les codes d'erreur.
     */
    public enum ErreurEnum {

        AUTRE(1), TECHNIQUE_BASE_DE_DONNEES(2), AUTHENTIFICATION(3), ENTITE_NON_TROUVEE(4);

        private final int valeur;

        ErreurEnum(int valeurParam) {
            valeur = valeurParam;
        }

        public int getValeur() {
            return valeur;
        }
    };
    
    private ConstantesRestWebService(){}
}
