package fr.paris.epm.redaction.metier.objetValeur.commun;

import java.io.Serializable;

/**
 * Interface Clausier
 */
public interface Clausier extends Serializable, Cloneable {

    /**
     * Actif.
     */
    public static final String ACTIF = "1";

    /**
     * Actif.
     */
    public static final String INACTIF = "0";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_SUPPRIMER = "2";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_MODIFIER = "1";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_DISPONIBLE = "0";

    /**
     * l'attribut static STATUT_BROUILLON
     */
    public static final Integer STATUT_BROUILLON = 1;

    /**
     * l'attribut static STATUT_A_VALIDER
     */
    public static final Integer STATUT_A_VALIDER = 2;

    /**
     * l'attribut static STATUT_VALIDE
     */
    public static final Integer STATUT_VALIDE = 3;

    /**
     * Auteur client
     */
    public static final String AUTEUR_CLIENT = "Client";

    /**
     * Auteur Editeur
     */
    public static final String AUTEUR_EDITEUR = "Editeur";

    /**
     * Auteur Editeur surchargée
     */
    public static final String AUTEUR_EDITEUR_SURCHARGE = "Editeur surchargée";

}