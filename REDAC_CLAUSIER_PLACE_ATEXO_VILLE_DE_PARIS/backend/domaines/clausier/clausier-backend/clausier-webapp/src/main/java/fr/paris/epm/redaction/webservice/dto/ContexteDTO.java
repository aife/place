package fr.paris.epm.redaction.webservice.dto;

import java.util.UUID;

public class ContexteDTO {
	private UUID uuid;

	private ConsultationDTO consultation;

	public ContexteDTO() {
	}

	public ConsultationDTO getConsultation() {
		return consultation;
	}

	public void setConsultation( ConsultationDTO consultation ) {
		this.consultation = consultation;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid( UUID uuid ) {
		this.uuid = uuid;
	}
}
