<!--Debut main-part-->
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="actionEditeur" value="" />
<c:set var="actionPreferences" value="true" />

<div class="main-part">
    <div class="breadcrumbs">
        <logic:equal name="preferences" value="direction">
            <bean:message key="Parametrage.titre.direction"/>
        </logic:equal>
        <logic:equal name="preferences" value="agent">
            <bean:message key="Parametrage.titre.agent"/>
        </logic:equal>
    </div>
    <logic:equal name="preferences" value="direction">
        <atexo:fichePratique reference="FPR01" key="common.fichePratique"/>
    </logic:equal>
    <logic:equal name="preferences" value="agent">
        <atexo:fichePratique reference="FPR02" key="common.fichePratique"/>
    </logic:equal>
    <div class="breaker"></div>
    <!--Debut bloc Recherche clause-->
    <html:form action="/ParametrageClauseRecharge.epm" >

        <div class="form-bloc">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <div class="line">
					<span class="intitule">
						<bean:message key="ParametrageClause.txt.referencePoin"/>
					</span>
                    <html:text property="reference" styleClass="width-200" styleId="reference" title="R&eacute;f&eacute;rence" indexed="ghhh"/>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.typeDoc"/></span>
                    <html:select property="docType" styleClass="width-200" styleId="docType" title="Type de document">
                        <html:optionsCollection property="docTypeCollection" label="libelle" value="id"/>
                    </html:select>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.typeContrat"/></span>
                    <html:select property="typeContrat" styleClass="width-200" styleId="typeContrat" title="Type de contrat">
                        <option value="0"><bean:message key="redaction.txt.Tous"/></option>
                        <html:optionsCollection property="typeContratCollection" label="libelle" value="id"/>
                    </html:select>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.procPass"/></span>
                    <html:select property="procedurePassation" styleClass="width-570" styleId="procedurePassation" title="Proc&eacute;dure de passation">
                        <option value="0"><bean:message key="redaction.txt.Toutes"/></option>
                        <html:optionsCollection property="procedureCollection" label="libelle" value="id"/>
                    </html:select>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.natPres"/></span>
                    <html:select property="naturePrestas" styleClass="width-200" styleId="naturePrestas" title="Nature des prestations">
                        <html:optionsCollection property="naturePrestasCollection" label="libelle" value="id"/>
                    </html:select>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.themePoin"/></span>
                    <html:select property="theme" styleClass="width-570" styleId="theme" title="Théme">
                        <html:optionsCollection property="themeCollection" label="libelle" value="id"/>
                    </html:select>
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.motsCles"/></span>
                    <html:textarea property="motsCles" styleId="motsCles" title="Mots-cl&eacute;s" cols="30" rows="2" styleClass="width-570" />
                </div>

                <div class="line">
                    <span class="intitule"><bean:message key="ParametrageClause.txt.affClauses"/></span>
                    <html:select property="afficheClauses" styleClass="width-200" styleId="afficheClauses" title="Afficher les clauses">
                        <html:option value="-1">
                            <bean:message  key="redaction.txt.Toutes"/>
                        </html:option>
                        <html:option value="1">
                            <bean:message  key="redaction.txt.Actives"/>
                        </html:option>
                        <html:option value="0">
                            <bean:message  key="redaction.txt.Inactives"/>
                        </html:option>
                    </html:select>
                </div>

                <div class="breaker"></div>
                <!--Attention, la fonction 'initResultsTable' ci-dessous ne sert que puor la maquette, il faut la supprimer a l'integration-->
                <a href="javascript:initFormRecherche();" class="init-recherche">
                    <bean:message key="ParametrageClause.btn.effacerCritereRecherche"/>
                </a>
                <html:link href="javascript:document.forms[0].submit();" styleClass="rechercher">
                    <bean:message key="ParametrageClause.btn.rechercher"/>
                </html:link>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
    </html:form>
    <!--Fin bloc Recherche clause-->
    <!--Debut partitioner-->

    <!-- Tableau des resultats  -->
    <%@ include file="TableauRechercheClause.jsp"%>

    <!--Fin legende clauses actives/inactives-->
</div>
<!--Fin main-part-->
