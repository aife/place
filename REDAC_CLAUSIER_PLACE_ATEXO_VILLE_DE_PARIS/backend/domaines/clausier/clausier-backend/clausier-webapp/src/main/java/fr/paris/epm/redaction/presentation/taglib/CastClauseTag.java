package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.redaction.metier.objetValeur.document.ClauseListe;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseTexte;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Ce tag différencie une clause liste, d'une clause texte.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CastClauseTag extends TagSupport {

    /**
     * marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * id représentant l'objet.
     */
    private String id;

    /**
     * clé d'un objet request ou session.
     */
    private String key;

    /**
     * identifiant du type de clause.
     */
    private int value;

    /**
     * @return int
     */
    public int doStartTag() throws JspException {

        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        if (request.getAttribute(key) instanceof ClauseTexte && (value == 8 || value == 9)) {
            ClauseTexte clauseTexte = (ClauseTexte) request.getAttribute(key);
            if (clauseTexte.getType() != 8 && clauseTexte.getType() != 9) {
                return SKIP_BODY; 
            }
            request.setAttribute(id, clauseTexte);
        } else if (request.getAttribute(key) instanceof ClauseTexte && value == 0) {
            ClauseTexte clauseTexte = (ClauseTexte) request.getAttribute(key);
            request.setAttribute(id, clauseTexte);
            if (clauseTexte.getType() == 8 || clauseTexte.getType() == 9) {
                return SKIP_BODY; 
            }
        } else if (request.getAttribute(key) instanceof ClauseListe && value == 1) {
            ClauseListe clauseListe = (ClauseListe) request.getAttribute(key);
            request.setAttribute(id, clauseListe);
        } else {
            return SKIP_BODY;
        }
        return EVAL_BODY_INCLUDE;
    }

    /**
     * @return int
     */
    public int doAfterBody() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * nettoyage des variables utilisées.
     */
    public void release() {
        key = null;
        id = null;
        value = 0;
    }

    /**
     * @param valeur représentant l'objet
     */
    public final void setId(final String valeur) {
        this.id = valeur;
    }

    /**
     * @param valeur clé d'un objet request ou session
     */
    public final void setKey(final String valeur) {
        this.key = valeur;
    }

    /**
     * @param valeur identifiant du type de clause
     */
    public final void setValue(final int valeur) {
        this.value = valeur;
    }

}
