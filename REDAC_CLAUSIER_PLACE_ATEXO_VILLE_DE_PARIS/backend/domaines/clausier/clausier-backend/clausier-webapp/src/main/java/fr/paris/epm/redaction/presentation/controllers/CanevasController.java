package fr.paris.epm.redaction.presentation.controllers;

import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.global.commun.worker.RsemTaskWorker;
import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.worker.RedactionTaskFactory;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.coordination.facade.CanevasFacade;
import fr.paris.epm.redaction.coordination.facade.CanevasViewFacade;
import fr.paris.epm.redaction.coordination.facade.DirectoryFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Canevas Controller
 * Created by nty on 07/09/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Controller
@SessionAttributes({"searchInstance", "resultList", "listLayout", /* sont utilise dans {@link fr.paris.epm.global.presentation.controllers.NavigateController} */
        "canevas", "editeur", "modeSaaS",
        "allNatures", "allTypeAuteurs", "allStatutsRedactionClausier",
        "allTypesDocument", "allTypesContrat", "allProcedures"})
public class CanevasController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(CanevasController.class);

    @Resource
    private RedactionServiceSecurise redactionService;

    @Resource
    private ReferentielFacade referentielFacade;

    @Resource
    private CanevasFacade canevasFacade;

    @Resource
    private CanevasViewFacade canevasViewFacade;

    @Resource
    private DirectoryFacade directoryFacade;

    @Resource
    private RedactionTaskFactory redactionTaskFactory;

    @Resource
    private RsemTaskWorker rsemTaskWorker;

    @ModelAttribute("allNatures")
    public final List<Directory> allNatures() {
        return directoryFacade.findListNatures();
    }

    @ModelAttribute("allTypeAuteurs")
    public final List<Directory> allTypeAuteurs() {
        return directoryFacade.findListTypesAuteurCanevas();
    }

    @ModelAttribute("allStatutsRedactionClausier")
    public final List<Directory> allStatut() {
        return directoryFacade.findListStatutsRedactionClausier();
    }

    @ModelAttribute("allTypesDocument")
    public final List<Directory> allTypesDocument() {
        return directoryFacade.findListTypesDocument();
    }

    @ModelAttribute("allTypesContrat")
    public final List<Directory> allTypesContrat() {
        return directoryFacade.findListTypesContrat();
    }

    @ModelAttribute("allProcedures")
    public final List<Directory> allProcedures() {
        return directoryFacade.findListProcedures();
    }


    @RequestMapping(value = "/previewCanevas")
    public final String previewCanevas(@RequestParam(required = false, defaultValue = "false") final boolean editeur,
                                       @RequestParam(value = "idCanevas", required = false) Integer idCanevas,
                                       @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                       @RequestParam(value = "idTask", required = false) String idTask,
                                       @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur,
                                       final Model uiModel) {

        log.info("/previewCanevas.htm");

        if (idCanevas == null && (idTask == null || idTask.isEmpty()))
            return "redirect:/confirmationGenerique.epm?message=creationCanevas2.erreur.canevasNonEnregistre";

        if (idTask == null || idTask.isEmpty()) {
            RsemTask<Map<String, Object>> task = redactionTaskFactory.newPreviewCanevasTask(epmTUtilisateur, editeur, idCanevas, idPublication);

            idTask = rsemTaskWorker.executeTask(task);
            log.info("Tache {} est lancée", idTask);

            uiModel.addAttribute("idTask", idTask);
            return "popupTaskWorker";

        } else {
            try {
                Map<String, Object> result = rsemTaskWorker.getResult(idTask);
                result.forEach(uiModel::addAttribute);
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
            return "previewCanevasLayout";
        }
    }

}
