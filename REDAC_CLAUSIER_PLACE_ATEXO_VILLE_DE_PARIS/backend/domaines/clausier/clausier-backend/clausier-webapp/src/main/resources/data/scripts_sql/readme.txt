ordre d'execution des scripts
repertoire "structure/"
- structure-redaction_create.sql
- structure-redaction_contrainte.sql
- roles-redaction.sql

repertoire "donnee/"
- referentiels-redaction.sql

repertoire "donnee/client"
- donnees-clausiers.sql
