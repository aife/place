package fr.paris.epm.redaction.presentation.views.mappers;

import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.presentation.views.Agent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AgentViewMapper implements Mapper<EpmTUtilisateur, Agent> {

    @Resource
    private RedactionServiceSecurise redactionService;

    @Resource
    private ServiceViewMapper serviceViewMapper;

    @Override
    public Agent map(EpmTUtilisateur utilisateur) {
        final var result = new Agent();

        result.setIdentifiantExterne(utilisateur.getGuidSso());
        result.setNom(utilisateur.getNom());
        result.setPrenom(utilisateur.getPrenom());
        result.setEmail(utilisateur.getCourriel());

        final var service = redactionService.chercherObject(utilisateur.getDirectionService(), EpmTRefDirectionService.class);
        result.setService(this.serviceViewMapper.map(service));

        return result;
    }
}
