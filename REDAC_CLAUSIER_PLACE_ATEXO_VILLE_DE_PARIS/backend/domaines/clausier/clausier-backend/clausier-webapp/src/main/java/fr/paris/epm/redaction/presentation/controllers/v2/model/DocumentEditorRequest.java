package fr.paris.epm.redaction.presentation.controllers.v2.model;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class DocumentEditorRequest {
    @NotEmpty
    @NotNull
    private String documentId;
    @NotEmpty
    @NotNull
    private String plateformeId;
    private Integer version;
    @NotEmpty
    @NotNull
    private String documentTitle;
    @NotEmpty
    @NotNull
    private String callback;
    private String mode;
    @NotNull
    private User user;

    private DocumentConfiguration configuration;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getPlateformeId() {
        return plateformeId;
    }

    public void setPlateformeId(String plateformeId) {
        this.plateformeId = plateformeId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DocumentConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(DocumentConfiguration configuration) {
        this.configuration = configuration;
    }
}
