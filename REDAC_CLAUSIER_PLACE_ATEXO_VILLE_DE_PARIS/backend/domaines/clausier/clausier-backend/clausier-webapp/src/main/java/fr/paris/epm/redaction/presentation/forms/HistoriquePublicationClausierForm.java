package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * formulaire de choix de la version de publication du clausie
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public class HistoriquePublicationClausierForm extends ActionForm {

    /**
     * Marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * La versions de publication
     */
    private String version;
    
    /**
     * Identifiant de la clause éditeur
     */
    private String idClauseCanevasEditeur;

    /**
     * Ensemble des publications du clausier
     */
    private Collection<EpmTPublicationClausier> versions;
    
    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        version = null;
    }

    /**
     * @return the version
     */
    public final String getVersion() {
        return version;
    }
    
    /**
     * @param version the version to set
     */
    public final void setVersion(final String version) {
        this.version = version;
    }
    
    /**
     * @return the idClauseCanevasEditeur
     */
    public final String getIdClauseCanevasEditeur() {
        return idClauseCanevasEditeur;
    }

    /**
     * @param idClauseCanevasEditeur the idClauseCanevasEditeur to set
     */
    public final void setIdClauseCanevasEditeur(final String idClauseCanevasEditeur) {
        this.idClauseCanevasEditeur = idClauseCanevasEditeur;
    }

    /**
     * @return the versions
     */
    public final Collection<EpmTPublicationClausier> getVersions() {
        return versions;
    }

    /**
     * @param versions the versions to set
     */
    public final void setVersions(final Collection<EpmTPublicationClausier> versions) {
        this.versions = versions;
    }
    
}
