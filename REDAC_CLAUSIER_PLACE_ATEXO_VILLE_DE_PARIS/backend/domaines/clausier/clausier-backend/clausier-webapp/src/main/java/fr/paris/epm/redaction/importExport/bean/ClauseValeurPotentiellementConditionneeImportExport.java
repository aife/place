package fr.paris.epm.redaction.importExport.bean;

public class ClauseValeurPotentiellementConditionneeImportExport {

    private int idClauseValeurPotentiellementConditionnee;

    /**
     * Identifiant de la valeur du critere potentiellement conditionne. (si 0 concerne toutes les procedures).
     */
    private String valeur;

    public int getIdClauseValeurPotentiellementConditionnee() {
        return idClauseValeurPotentiellementConditionnee;
    }

    public void setIdClauseValeurPotentiellementConditionnee(int idClauseValeurPotentiellementConditionnee) {
        this.idClauseValeurPotentiellementConditionnee = idClauseValeurPotentiellementConditionnee;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

}
