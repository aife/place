package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.presentation.bean.CategorieDocument;
import org.apache.struts.taglib.TagUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Tag permattant d'afficher le nombre et le type de document.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ListeDocumentsOfConsultationTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * Clé utilisée dans le scope request.
     */
    private String id;

    /**
     * gére le type de document.
     */
    private String info;
    
    /**
     * Détermine si l'on récupère les EpmTPageDeGarde également
     */
    private String inclurePageDeGarde;

	/**
	 * Catégorie
	 */
	private String categorie;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Injecté par spring.
     */
    private static RedactionServiceSecurise redactionService;

    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        Object objet = tagUtils.lookup(pageContext, name, null);

	    CategorieDocument categorieDocument = CategorieDocument.trouver(categorie).orElse(null);

        return Tag.SKIP_BODY;
    }

    /*
     * (non-Javadoc) @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        info = null;
        name = null;
        id = null;
        inclurePageDeGarde = null;
    }

    /**
     * @param valeur gére le type de document.
     */
    public final void setInfo(final String valeur) {
        this.info = valeur;
    }

    public final void setName(final String valeur) {
        this.name = valeur;
    }

    public final void setId(final String valeur) {
        id = valeur;
    }

    public final void setInclurePageDeGarde(final String valeur) {
        this.inclurePageDeGarde = valeur;
    }

    public final void setRedactionService(RedactionServiceSecurise redactionService) {
        ListeDocumentsOfConsultationTag.redactionService = redactionService;
    }

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie( String categorie ) {
		this.categorie = categorie;
	}
}
