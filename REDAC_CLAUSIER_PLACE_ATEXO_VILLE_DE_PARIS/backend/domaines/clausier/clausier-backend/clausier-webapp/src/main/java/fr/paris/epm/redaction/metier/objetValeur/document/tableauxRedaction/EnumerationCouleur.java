//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.05 at 05:36:49 PM CEST 
//


package fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for enumeration-couleur.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="enumeration-couleur">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="BLACK"/>
 *     &lt;enumeration value="BROWN"/>
 *     &lt;enumeration value="OLIVE_GREEN"/>
 *     &lt;enumeration value="DARK_GREEN"/>
 *     &lt;enumeration value="DARK_TEAL"/>
 *     &lt;enumeration value="DARK_BLUE"/>
 *     &lt;enumeration value="INDIGO"/>
 *     &lt;enumeration value="GREY_80_PERCENT"/>
 *     &lt;enumeration value="ORANGE"/>
 *     &lt;enumeration value="DARK_YELLOW"/>
 *     &lt;enumeration value="GREEN"/>
 *     &lt;enumeration value="TEAL"/>
 *     &lt;enumeration value="BLUE"/>
 *     &lt;enumeration value="BLUE_GREY"/>
 *     &lt;enumeration value="GREY_50_PERCENT"/>
 *     &lt;enumeration value="RED"/>
 *     &lt;enumeration value="LIGHT_ORANGE"/>
 *     &lt;enumeration value="LIME"/>
 *     &lt;enumeration value="SEA_GREEN"/>
 *     &lt;enumeration value="AQUA"/>
 *     &lt;enumeration value="LIGHT_BLUE"/>
 *     &lt;enumeration value="VIOLET"/>
 *     &lt;enumeration value="GREY_40_PERCENT"/>
 *     &lt;enumeration value="PINK"/>
 *     &lt;enumeration value="GOLD"/>
 *     &lt;enumeration value="YELLOW"/>
 *     &lt;enumeration value="BRIGHT_GREEN"/>
 *     &lt;enumeration value="TURQUOISE"/>
 *     &lt;enumeration value="DARK_RED"/>
 *     &lt;enumeration value="SKY_BLUE"/>
 *     &lt;enumeration value="PLUM"/>
 *     &lt;enumeration value="GREY_25_PERCENT"/>
 *     &lt;enumeration value="ROSE"/>
 *     &lt;enumeration value="LIGHT_YELLOW"/>
 *     &lt;enumeration value="LIGHT_GREEN"/>
 *     &lt;enumeration value="LIGHT_TURQUOISE"/>
 *     &lt;enumeration value="PALE_BLUE"/>
 *     &lt;enumeration value="LAVENDER"/>
 *     &lt;enumeration value="WHITE"/>
 *     &lt;enumeration value="CORNFLOWER_BLUE"/>
 *     &lt;enumeration value="LEMON_CHIFFON"/>
 *     &lt;enumeration value="MAROON"/>
 *     &lt;enumeration value="ORCHID"/>
 *     &lt;enumeration value="CORAL"/>
 *     &lt;enumeration value="ROYAL_BLUE"/>
 *     &lt;enumeration value="LIGHT_CORNFLOWER_BLUE"/>
 *     &lt;enumeration value="TAN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "enumeration-couleur")
@XmlEnum
public enum EnumerationCouleur {

    BLACK,
    BROWN,
    OLIVE_GREEN,
    DARK_GREEN,
    DARK_TEAL,
    DARK_BLUE,
    INDIGO,
    GREY_80_PERCENT,
    ORANGE,
    DARK_YELLOW,
    GREEN,
    TEAL,
    BLUE,
    BLUE_GREY,
    GREY_50_PERCENT,
    RED,
    LIGHT_ORANGE,
    LIME,
    SEA_GREEN,
    AQUA,
    LIGHT_BLUE,
    VIOLET,
    GREY_40_PERCENT,
    PINK,
    GOLD,
    YELLOW,
    BRIGHT_GREEN,
    TURQUOISE,
    DARK_RED,
    SKY_BLUE,
    PLUM,
    GREY_25_PERCENT,
    ROSE,
    LIGHT_YELLOW,
    LIGHT_GREEN,
    LIGHT_TURQUOISE,
    PALE_BLUE,
    LAVENDER,
    WHITE,
    CORNFLOWER_BLUE,
    LEMON_CHIFFON,
    MAROON,
    ORCHID,
    CORAL,
    ROYAL_BLUE,
    LIGHT_CORNFLOWER_BLUE,
    TAN;

    public String value() {
        return name();
    }

    public static EnumerationCouleur fromValue(String v) {
        return valueOf(v);
    }

}
