package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.commun.ResultList;
import fr.paris.epm.global.coordination.facade.AbstractMapperFacade;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.global.coordination.mapper.AbstractEpmObjectToBeanMapper;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.redaction.EpmVCanevas;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.coordination.service.CanevasService;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.bean.CanevasSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.mapper.CanevasMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CanevasViewFacadeImpl extends AbstractMapperFacade<CanevasBean, EpmVCanevas> implements CanevasViewFacade {

    private static final Logger LOG = LoggerFactory.getLogger(CanevasViewFacadeImpl.class);

    private CanevasMapper canevasMapper;

    private CanevasService canevasService;

    public CanevasViewFacadeImpl() {
        setCritereContructor(CanevasViewCritere::new);
        setDefaultSortField("dateModification");
        setDefaultIdField("idCanevas");
    }

    @Override
    public CanevasService getService() {
        return canevasService;
    }

    @Override
    protected boolean defaultRemoveTest(int id) {
        return false;
    }

    @Override
    public AbstractBeanToEpmObjectMapper<CanevasBean, EpmVCanevas> getToEpmObjectMapper() {
        return null; // TODO: refaire comme dans les module Administration et Passation après la fusion NOYAU-REDAC
    }


    @Override
    public CanevasBean map(EpmVCanevas epmVCanevas, EpmTRefOrganisme epmTRefOrganisme) {
        CanevasBean canevasBean = canevasMapper.toCanevasBean(epmVCanevas);
        if (canevasBean.isCanevasEditeur()) {
            canevasBean.setTypeAuteur(2);
            canevasBean.setLabelTypeAuteur("Editeur");
        } else {
            canevasBean.setTypeAuteur(1);
            canevasBean.setLabelTypeAuteur("Client");
        }

        if (epmVCanevas.getIdLastPublication() != null && epmVCanevas.getIdLastPublication() != 0) {
            EpmTPublicationClausier publication = canevasService.chercherObject(epmVCanevas.getIdLastPublication(), EpmTPublicationClausier.class);
            canevasBean.setLastVersion(publication.getVersion());
        } else {
            canevasBean.setLastVersion("NA");
        }
        return canevasBean;
    }

    @Override
    public EpmVCanevas map(CanevasBean canevasBean, EpmTRefOrganisme epmTRefOrganisme) {
        CanevasViewCritere canevasViewCritere = new CanevasViewCritere(epmTRefOrganisme.getPlateformeUuid());
        canevasViewCritere.setIdCanevas(canevasBean.getIdCanevas());
        canevasViewCritere.setIdPublication(canevasBean.getIdPublication());
        EpmVCanevas epmVCanevas = safelyUniqueFind(canevasViewCritere);
        canevasMapper.toEpmVCanevas(canevasBean, epmVCanevas);
        return epmVCanevas;
    }

    @Override
    public PageRepresentation<CanevasBean> findCanevas(CanevasSearch search, EpmTRefOrganisme epmTRefOrganisme) {

        ResultList<CanevasBean, ? extends CanevasViewFacade, CanevasViewCritere> resultList = new ResultList<>(this, new CanevasViewCritere(epmTRefOrganisme.getPlateformeUuid()));
        resultList.critere().setReference(search.getReferenceCanevas());
        resultList.critere().setIdOrganisme(epmTRefOrganisme.getId());

        String referenceClause = search.getReferenceClause();
        if (null != referenceClause && !referenceClause.isEmpty()) {
            List<Integer> canevasId = canevasService.chercherListeCanevasPourClauseMinisterielle(referenceClause, epmTRefOrganisme.getId())
                    .stream().map(EpmTCanevas::getId).distinct().collect(Collectors.toList());
            LOG.info("Id des canevas pour la clause {} = {}", referenceClause, canevasId);
            resultList.critere().setListIds(canevasId.isEmpty() ? Collections.singletonList(0) : canevasId);
        } else {
            resultList.critere().setListIds(null);
        }

        resultList.critere().setNaturePrestation(search.getIdNaturePrestation());
        resultList.critere().setAuteur(search.getTypeAuteur());
        resultList.critere().setTitre(search.getTitreCanevas());
        resultList.critere().setIdStatutRedactionClausier(search.getIdStatutRedactionClausier());
        resultList.critere().setIdTypeDocument(search.getIdTypeDocument());
        resultList.critere().setIdTypeContrat(search.getIdTypeContrat());
        resultList.critere().setProcedure(search.getIdProcedure());
        resultList.critere().setActif(search.getActif());
        resultList.critere().setDateModificationDebut(search.getDateModificationMin());
        resultList.critere().setDateModificationFin(search.getDateModificationMax());
        resultList.critere().setChercherNombreResultatTotal(true);
        resultList.critere().setProprieteTriee(search.getSortField());
        resultList.critere().setTriCroissant(search.isAsc());
        resultList.critere().setNumeroPage(search.getPage());
        resultList.critere().setTaillePage(search.getSize());

        List<Integer> ccag = new ArrayList<>();
        if (search.getIdCCAG() != null) {
            ccag.add(search.getIdCCAG());
            resultList.critere().setListeIdCCAG(ccag);
        }

        ResultList<CanevasBean, ? extends CanevasViewFacade, CanevasViewCritere> canevasBeans = resultList.find(epmTRefOrganisme);
        PageRepresentation<CanevasBean> representation = new PageRepresentation<>();
        representation.setContent(canevasBeans.getListResults());
        representation.setFirst(canevasBeans.pageCurrent() == 1);
        representation.setLast(canevasBeans.pageCurrent() == canevasBeans.getCountPages());
        representation.setNumberOfElements(canevasBeans.listResults().size());
        representation.setTotalPages(canevasBeans.countPages());
        representation.setTotalElements(canevasBeans.getCount());
        representation.setSize(search.getSize());
        representation.setNumber(search.getPage());
        return representation;
    }

    @Autowired
    public void setCanevasService(CanevasService canevasService) {
        this.canevasService = canevasService;
    }

    @Autowired
    public void setCanevasMapper(CanevasMapper canevasMapper) {
        this.canevasMapper = canevasMapper;
    }
}
