package fr.paris.epm.redaction.metier.objetValeur.commun;

import fr.paris.epm.redaction.util.Constantes;import org.apache.commons.lang3.StringUtils;

/**
 * la classe Info-bulle pour traiter une info-bulle.
 *
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class InfoBulle implements Clausier {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    /**
     * la description de l'info-bulle.
     */
    private String description = "";
    /**
     * l'attribut lien.
     */
    private String lien = "";
    /**
     * la déscription du lien.
     */
    private String descriptionLien = "";
    /**
     * l'attribut actif qui montre si l'info bull est active ou non.
     */
    private boolean actif = false;

    /**
     * cette méthode permet de positionner la description de l'info-bulle.
     *
     * @param valeur pour initialser la description de l'info-bulle.
     */
    public final void setDescription(final String valeur) {
        description = valeur;
    }

    /**
     * cette méthode permet de récupérer la description de l'info-bulle.
     *
     * @return description de l'info-bulle.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * cette méthode permet de positionner le lien de l'info-bulle avec une
     * valeur donnée.
     *
     * @param valeur pour initialiser le lien.
     */
    public final void setLien(final String valeur) {
        if (valeur != null)
            lien = valeur.replace("&", "&amp;");
        else
            lien = null;
    }

    /**
     * cette méthode permet de récupérer le lien de l'info-bulle.
     *
     * @return lien de l'info-bulle.
     */
    public final String getLien() {
        return lien;
    }

    /**
     * cette méthode permet de positionner la description du lien de
     * l'info-bulle.
     *
     * @param valeur pour initialiser la description du lien.
     */
    public final void setDescriptionLien(final String valeur) {
        descriptionLien = valeur;
    }

    /**
     * cette méthode permet de récupérer la description du lien de l'info-bulle.
     *
     * @return description du lien de l'info-bulle.
     */
    public final String getDescriptionLien() {
        return descriptionLien;
    }

    /**
     * cette méthode positionne l'activité de l'info-bulle.
     *
     * @param valeur pour initialiser l'attribut actif ou non.
     */
    public final void setActif(final boolean valeur) {
        actif = valeur;
    }

    /**
     * cette méthode récupére l'activité de l'info-bulle. vrai si l'info-bulle
     * est active, si non elle renvoie la valeur faux.
     *
     * @return l'info-bulle est-elle active ou non.
     */
    public final boolean isActif() {
        return actif;
    }

    /**
     * cette méthode prevoit si l'info-bulle est complétement vide ou non.
     *
     * @return vrai si l'info bulle est vide, faux si non.
     */
    public final boolean isEmpty() {
        return !actif && StringUtils.isEmpty(description) && StringUtils.isEmpty(descriptionLien) && StringUtils.isEmpty(lien);
    }

    public final boolean hasContent() {
        return !StringUtils.isEmpty(description) || !StringUtils.isEmpty(descriptionLien) || !StringUtils.isEmpty(lien);
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    public final int hashCode() {
        int result = 1;
        if (description != null)
            result = Constantes.PREMIER * result + description.hashCode();
        if (descriptionLien != null)
            result = Constantes.PREMIER * result + descriptionLien.hashCode();
        if (lien != null)
            result = Constantes.PREMIER * result + lien.hashCode();
        return result;
    }

    /**
     * cette méthode teste l'égalité des objets.
     *
     * @param obj correspond à l'objet à tester.
     * @return vrai si les deux objets sont égaux si non renvoyer faux.
     * @see java.lang.Object#equals(java.lang.Object).
     */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof InfoBulle))
            return false;

        final InfoBulle autre = (InfoBulle) obj;
        if (actif != autre.actif)
            return false;
        if (description == null) {
            if (autre.description != null)
                return false;
        } else if (!description.equals(autre.description)) {
            return false;
        }
        if (descriptionLien == null) {
            if (autre.descriptionLien != null)
                return false;
        } else if (!descriptionLien.equals(autre.descriptionLien)) {
            return false;
        }
        if (lien == null)
            return autre.lien == null;
        else
            return lien.equals(autre.lien);
    }

    /**
     * cette méthode clone l'info-Bulle.
     *
     * @return info-Bulle.
     */
    public final Object clone() {
        InfoBulle infoBulle = new InfoBulle();
        infoBulle.description = description;
        infoBulle.lien = lien;
        infoBulle.descriptionLien = descriptionLien;
        infoBulle.actif = actif;
        return infoBulle;
    }

    /**
     * cette méthode permet la conversion de type en String.
     *
     * @return la chaîne de caractères convertie.
     * @see java.lang.Object#toString()
     */
    public final String toString() {
        return "Infobulle" + "\nDescription" + description +
                "\nLien: " + lien + "description lien: " + descriptionLien +
                "\nactif: " + actif;
    }

}
