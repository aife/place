package fr.paris.epm.redaction.metier.objetValeur.document;

import java.io.Serializable;

/**
 * la classe abstraite AbstractChapitreDocument.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class AbstractChapitreDocument implements Serializable {

    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    public Object clone() {
        return new AbstractChapitreDocument();
    }

}
