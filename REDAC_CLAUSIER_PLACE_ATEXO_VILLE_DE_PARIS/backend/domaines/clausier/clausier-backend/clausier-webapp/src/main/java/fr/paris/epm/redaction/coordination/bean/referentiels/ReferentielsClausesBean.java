package fr.paris.epm.redaction.coordination.bean.referentiels;

import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Bean de présentation des référentiel utilisé dans les formulaires liés aux
 * clauses.
 * @author Léon Barsamian
 */
public class ReferentielsClausesBean implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 2078407624474462400L;

    /**
     * Liste des types de clauses.
     */
    private List<EpmTRefTypeClause> listeTousTypeClause;

    /**
     * Liste des thémes des clauses.
     */
    private List<EpmTRefThemeClause> listeTousThemesClause;

    /**
     * Liste des types de documents.
     */
    private List<EpmTRefTypeDocument> listeTousTypesDocument;

    private List<EpmTRefTypeContrat> listeTousTypesContrat;

    /**
     * Liste des conditionnements potentiels.
     */
    private List<Comparable<Object>> listeTousPotentiellementConditionne;

    /**
     * Liste des procédures de passation.
     */
    private List<EpmTRefProcedure> listeTousProcedurePassation;

    /**
     * Liste des natures de prestation.
     */
    private List<EpmTRefNature> listeTousNaturePrestation;

    /**
     * Liste des statuts de rédaction de clauses.
     */
    private List<EpmTRefStatutRedactionClausier> listeTousStatusRedactionClause;
    
    /**
     * Editeur/Client
     */
    private List<EpmTRefAuteur> listeTousAuteur;

    /**
     * @return Liste des types de clauses.
     */
    public final List<EpmTRefTypeClause> getListeTousTypeClause() {
        return listeTousTypeClause;
    }

    /**
     * @param valeur Liste des types de clauses.
     */
    public final void setListeTousTypeClause(List<EpmTRefTypeClause> valeur) {
        this.listeTousTypeClause = valeur;
    }

    /**
     * @return Liste des thémes des clauses.
     */
    public final List<EpmTRefThemeClause> getListeTousThemesClause() {
        return listeTousThemesClause;
    }

    /**
     * @param valeur Liste des thémes des clauses.
     */
    public final void setListeTousThemesClause(
            final List<EpmTRefThemeClause> valeur) {
        this.listeTousThemesClause = valeur;
    }

    /**
     * @return Liste des types de documents.
     */
    public final List<EpmTRefTypeDocument> getListeTousTypesDocument() {
        return listeTousTypesDocument;
    }

    /**
     * @param valeur Liste des types de documents.
     */
    public final void setListeTousTypesDocument(final List<EpmTRefTypeDocument> valeur) {
        this.listeTousTypesDocument = valeur;
    }

    /**
     * @return Liste des conditionnements potentiels.
     */
    public final List<Comparable<Object>> getListeTousPotentiellementConditionne() {
	    List<Comparable<Object>> result = null;

	    if (null!=listeTousPotentiellementConditionne) {
		    result = listeTousPotentiellementConditionne.stream()
			    .sorted(Comparable::compareTo)
			    .collect(Collectors.toList());
	    }

	    return result;
    }

    /**
     * @param valeur Liste des conditionnements potentiels.
     */
    public final void setListeTousPotentiellementConditionne(final List<Comparable<Object>> valeur) {
        this.listeTousPotentiellementConditionne = valeur;
    }

    /**
     * @return Liste des procédures de passation.
     */
    public final List<EpmTRefProcedure> getListeTousProcedurePassation() {
        return listeTousProcedurePassation;
    }

    /**
     * @param valeur Liste des procédures de passation.
     */
    public final void setListeTousProcedurePassation(final List<EpmTRefProcedure> valeur) {
        this.listeTousProcedurePassation = valeur;
    }

    /**
     * @return Liste des natures de prestation.
     */
    public final List<EpmTRefNature> getListeTousNaturePrestation() {
        return listeTousNaturePrestation;
    }

    /**
     * @param valeur Liste des natures de prestation.
     */
    public final void setListeTousNaturePrestation(final List<EpmTRefNature> valeur) {
        this.listeTousNaturePrestation = valeur;
    }

    /**
     * @return the listeTousStatusRedactionClause
     */
    public final List<EpmTRefStatutRedactionClausier> getListeTousStatusRedactionClause() {
        return listeTousStatusRedactionClause;
    }

    /**
     * @param valeur the listeTousStatusRedactionClause to set
     */
    public final void setListeTousStatusRedactionClause(final List<EpmTRefStatutRedactionClausier> valeur) {
        this.listeTousStatusRedactionClause = valeur;
    }

    /**
     * @return the listeTousAuteur
     */
    public final List<EpmTRefAuteur> getListeTousAuteur() {
        return listeTousAuteur;
    }

    /**
     * @param valeur the listeTousAuteur to set
     */
    public final void setListeTousAuteur(final List<EpmTRefAuteur> valeur) {
        this.listeTousAuteur = valeur;
    }

    public List<EpmTRefTypeContrat> getListeTousTypesContrat() {
        return listeTousTypesContrat;
    }

    public void setListeTousTypesContrat(List<EpmTRefTypeContrat> listeTousTypesContrat) {
        this.listeTousTypesContrat = listeTousTypesContrat;
    }
}
