package fr.paris.epm.redaction.webservice.dto;

import java.util.Set;

public class LotDTO {
	private Integer id;

	private String numero;

	private String libelle;

	private String description;

	private Set<CPVDTO> cpv;

	public LotDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId( final Integer id ) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero( final String numero ) {
		this.numero = numero;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( final String libelle ) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( final String description ) {
		this.description = description;
	}

	public Set<CPVDTO> getCpv() {
		return cpv;
	}

	public void setCpv( final Set<CPVDTO> cpv ) {
		this.cpv = cpv;
	}
}
