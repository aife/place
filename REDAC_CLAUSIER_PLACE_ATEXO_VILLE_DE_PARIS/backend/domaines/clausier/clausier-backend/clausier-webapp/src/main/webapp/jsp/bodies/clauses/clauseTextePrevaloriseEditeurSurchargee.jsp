<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--Debut bloc Infos clause-->
<div class="form-saisie">
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>
		<div class="content">
		
			<h2 class="float-left">
				<html:radio property="clauseSelectionnee" styleId="clauseEditeurSurchargeeSelectionnee" value="clauseEditeurSurcharge"></html:radio>
				<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeurSurchargee"/></label>
			</h2>
			<div class="actions-clause no-padding">
				<logic:equal name="typeAction" value="M">
					<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
<!-- 		<div class="info-maj"> -->
<%-- 		<bean:message key="clause.surcharge.modifieeLe"/> --%>
<%-- 			<c:out value="TODO" /> --%>
<!-- 		</div> -->
			<div class="line">
				<span class="intitule-bloc"><bean:message
						key="ClauseTextePrevaloriser.txt.texteFixeAvant" /> </span>
				<html:textarea property="textFixeAvant" title="Texte fixe avant"
					cols="" rows="6" styleClass="texte-long mceEditor"
					errorStyleClass="error-border" styleId="textFixeAvant"/>
			</div>
			<div class="line">
			<div class="retour-ligne">
				<html:checkbox property="sautTextFixeAvant" value="true"
					title="Saut de ligne" styleId="sautTextFixeAvant"/>
				<bean:message key="redaction.txt.sautLigne" />
				<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
					title="Saut de ligne" />
			</div>
		</div>
		<div class="separator"></div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.tailleChamp" /> 
				</span>
				 <c:set var="nbCaractValue" value="1"/>
				 <c:if test="${not empty(frmClauseTextePrevalorise.nbCaract)}">
					<c:set var="nbCaractValue" value="${frmClauseTextePrevalorise.nbCaract}"/>
				 </c:if>
				 <span class="intitule-auto">
					 <html:select property="nbCaract" styleId="nbCaract" styleClass="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut_', 'mceEditor');" value="${nbCaractValue}">
						<html:option value="4">
			               <bean:message key="redaction.taille.champ.tresLong" />
			            </html:option>
			            <html:option value="1">
			               <bean:message key="redaction.taille.champ.long" />
			            </html:option>
			            <html:option value="2">
			               <bean:message key="redaction.taille.champ.moyen" />
			            </html:option>
			            <html:option value="3">
			               <bean:message key="redaction.taille.champ.court" />
			            </html:option>
		       		</html:select>
	       		</span>
			</div>
						
			<c:if test="${nbCaractValue == '0'}">
				<script>
					mettreTailleChampValeurDefaut('nbCaract');
				</script>
		    </c:if>
			
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTextePrevaloriser.txt.valeurParDefaut" /></span>
					<c:set var="styleDisplayTresLong" value="none"/>
					<c:set var="styleDisplayLong" value="none"/>
					<c:set var="styleDisplayMoyen" value="none"/>
					<c:set var="styleDisplayCourt" value="none"/>
					<c:choose>
						<c:when test="${frmClauseTextePrevalorise.nbCaract == 4}">
							<c:set var="styleDisplayTresLong" value="block"/>
						</c:when>
						<c:when test="${frmClauseTextePrevalorise.nbCaract == 2}">
							<c:set var="styleDisplayMoyen" value="block"/>
						</c:when>
						<c:when test="${frmClauseTextePrevalorise.nbCaract == 3}"> 
							<c:set var="styleDisplayCourt" value="block"/>
						</c:when>
						<c:otherwise>
							<c:set var="styleDisplayLong" value="block"/>
						</c:otherwise>
					</c:choose>
				    <html:textarea  property="valeurDefautTresLong"  title="Valeur par defaut" styleClass="texte-tres-long mceEditor" styleId="valeurDefaut_4"  cols="" rows="6" style="display:${styleDisplayTresLong}" errorStyleClass="error-border" ></html:textarea>
				    <html:textarea  property="defaultValue"  title="Valeur par defaut" styleClass="texte-long mceEditor" styleId="valeurDefaut_1"  cols="" rows="6" style="display:${styleDisplayLong}" errorStyleClass="error-border" ></html:textarea>
                    <html:text property="valeurDefautMoyen"  title="Valeur par defaut" styleClass="texte-moyen" styleId="valeurDefaut_2"  style="display:${styleDisplayMoyen}" errorStyleClass="error-border"/>
                    <html:text property="valeurDefautCourt"  title="Valeur par defaut" styleClass="texte-court" styleId="valeurDefaut_3" style="display:${styleDisplayCourt}" errorStyleClass="error-border"/>
			</div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirection" /> </span>
				<div class="radio-choice">
					<html:radio property="parametrableDirection" title="Oui"
						value="1" styleId="parametrableDirection" disabled="true"/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirectionOui" />
				</div>
				<div class="radio-choice">
					<html:radio property="parametrableDirection" title="Non"
						value="0" styleId="parametrableDirection" disabled="true"/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableDirectionNon" />
				</div>
			</div>
			<div class="line">
				<span class="intitule"><bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgent" /> </span>
				<div class="radio-choice">
					<html:radio property="parametrableAgent" title="Oui" value="1" styleId="parametrableAgent" disabled="true"/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgentOui" />
				</div>
				<div class="radio-choice">
					<html:radio property="parametrableAgent" title="Non" value="0" styleId="parametrableAgent" disabled="true"/>
					<bean:message
						key="ClauseTextePrevaloriser.txt.parametrableAgentNon" />
				</div>
			</div>
			<div class="line">
			<div class="retour-ligne">
				<html:checkbox property="sautTextFixeApres" value="true"
					title="Saut de ligne" styleId="sautTextFixeApres"/>
				<bean:message key="redaction.txt.sautLigne" />
				<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
					title="Saut de ligne" />
			</div>
		</div>
		<div class="separator"></div>
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTextePrevaloriser.txt.texteFixeApres" /> </span>
				<html:textarea property="textFixeApres" styleId="textFixeApres" title="Texte fixe après"
							   cols="" rows="6" styleClass="texte-long mceEditor" />
			</div>
			
			<div class="actions-clause">
				<logic:equal name="typeAction" value="M">
					<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
			
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span><span class="right"></span>
		</div>
	</div>
</div>
<div class="spacer"></div>
<script type="text/javascript">
  	initEditeursTexteRedactionSurchargeClauses();
</script>