//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.05 at 05:36:49 PM CEST 
//


package fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Java class for cellule-style-remplissage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cellule-style-remplissage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="type" use="required" type="{}enumeration-remplissage" />
 *       &lt;attribute name="premierPlan" type="{}enumeration-couleur" />
 *       &lt;attribute name="arrierePlan" type="{}enumeration-couleur" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cellule-style-remplissage")
public class CelluleStyleRemplissage
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "type", required = true)
    protected EnumerationRemplissage type;
    @XmlAttribute(name = "premierPlan")
    protected EnumerationCouleur premierPlan;
    @XmlAttribute(name = "arrierePlan")
    protected EnumerationCouleur arrierePlan;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link EnumerationRemplissage }
     *     
     */
    public EnumerationRemplissage getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumerationRemplissage }
     *     
     */
    public void setType(EnumerationRemplissage value) {
        this.type = value;
    }

    /**
     * Gets the value of the premierPlan property.
     * 
     * @return
     *     possible object is
     *     {@link EnumerationCouleur }
     *     
     */
    public EnumerationCouleur getPremierPlan() {
        return premierPlan;
    }

    /**
     * Sets the value of the premierPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumerationCouleur }
     *     
     */
    public void setPremierPlan(EnumerationCouleur value) {
        this.premierPlan = value;
    }

    /**
     * Gets the value of the arrierePlan property.
     * 
     * @return
     *     possible object is
     *     {@link EnumerationCouleur }
     *     
     */
    public EnumerationCouleur getArrierePlan() {
        return arrierePlan;
    }

    /**
     * Sets the value of the arrierePlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumerationCouleur }
     *     
     */
    public void setArrierePlan(EnumerationCouleur value) {
        this.arrierePlan = value;
    }

}
