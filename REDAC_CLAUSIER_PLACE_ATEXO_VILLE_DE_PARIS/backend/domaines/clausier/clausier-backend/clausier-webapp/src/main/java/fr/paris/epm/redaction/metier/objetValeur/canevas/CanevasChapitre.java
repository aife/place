package fr.paris.epm.redaction.metier.objetValeur.canevas;

import fr.paris.epm.redaction.metier.objetValeur.commun.Clausier;

import java.util.List;

/**
 * Interface commune à Canevas et Chapitre.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface CanevasChapitre extends Clausier {

    /**
     * cette méthode permet de confirmer la suppression d'un chapitre.
     * @param chapitre chapitre à supprimer.
     * @return vrai si le chapitre est effectivement supprimé, faux sinon.
     */
    boolean supprimer(Chapitre chapitre);

    /**
     * cette méthode renvoie la liste des chapitres.
     * @return liste des chapitres.
     */
    List<Chapitre> getChapitres();

    /**
     * cette méthode positionne la liste des attributs.
     * @param chapitres nouvelle liste des chapitres.
     */
    void setChapitres(List<Chapitre> chapitres);

    /**
     * cette méthode renvoie le parent du chapitre.
     * @return parent de ce chapitre (chapitre on canevas)
     */
    CanevasChapitre getParent();

    /**
     * cette méthode initialise le parent du chapitre.
     * @param parent positionne le parent de ce chapitre
     */
    void setParent(CanevasChapitre parent);

    /**
     * cette méthode permet de d'insérer un chapitre à la fin de la liste des
     * chapitres.
     * @return index du dernier chapitre + 1
     */
    int dernierChapitre();

    /**
     * cette méthode permet d'insérer un chapitre à la racine du canevas à une
     * certaine position dans la liste.
     * @param chap nouveau chapitre à insérer
     * @param position position absolue
     * @return faux dans le cas ou la position est négative vrai dans le cas ou
     *         la position est positive.
     */
    boolean inserer(Chapitre chap, int position);

    /**
     * cette méthode permet de déplacer un chapitre à partir d'un emplacement
     * donné.
     * @param aDeplacer chapitre à déplacer
     * @param position position absolue dans la liste des chapitres
     * @return faux si la position donné est négative vrai si la position donné
     *         est positve.
     */
    boolean deplacerChapitre(Chapitre aDeplacer, int position);

    /**
     * cette methode permet de parcourir la liste des chapitres et affecte un
     * numéro de la forme 1.1.2.5.
     */
    void renumeroterChapitres();

}