package fr.paris.epm.redaction.config;

import fr.paris.epm.global.coordination.facade.SafelyNoyauInvokator;
import fr.paris.epm.global.coordination.facade.SafelyNoyauInvokatorImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * Created by nty on 24/09/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Configuration
@Import({FacadeConfig.class, ServiceConfig.class})
@ImportResource({"/WEB-INF/applicationContext.xml"})
//@PropertySource({})
//@EnableTransactionManagement
@ComponentScan({
        "fr.paris.epm.global.coordination.validation.validators",
        "fr.paris.epm.global.presentation.tags"})
public class ApplicationConfig {

    @Bean(name = "safelyInvokator")
    public SafelyNoyauInvokator safelyInvokator() {
        return new SafelyNoyauInvokatorImpl();
    }

    @Bean(name = "safelyNoyauInvokator")
    public SafelyNoyauInvokator safelyNoyauInvokator() {
        return new SafelyNoyauInvokatorImpl();
    }

    @Value("${openoffice.fichierTemporaire}")
    private String cheminTemporaire;

    @Value("${openoffice.pathTemplate}")
    private String cheminModeleExportClause;


    @Bean(name = "cheminTemporaire")
    public String cheminTemporaire() {
        return cheminTemporaire;
    }

    @Bean(name = "cheminModeleExportClause")
    public String cheminModeleExportClause() {
        return cheminModeleExportClause;
    }



}
