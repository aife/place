package fr.paris.epm.redaction.metier.objetValeur.canevas;

import fr.paris.epm.redaction.metier.objetValeur.commun.Derogation;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.util.Constantes;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Chapitre qui implemente l'interface CanevasChapitre.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class Chapitre extends AbstractCanevasChapitre {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * identifiant du chapitre.
     */
    private int idChapitre;

    private Integer idPublication;

    /**
     * titre du chapitre.
     */
    private String titre;

    /**
     * l'objet infoBulle.
     */
    private InfoBulle infoBulle = new InfoBulle();
    
    /**
     * Dérogation au CCAG 
     */
    private Derogation derogation;
    
    /**
     * Si afficher l’indication de dérogation au bas des articles
     */
    private boolean afficherMessageDerogation;
    
    /**
     * Le style du chapitre
     */
    private String styleChapitre;

    /**
     * la liste des clauses.
     */
    private List<Clause> clauses = new ArrayList<Clause>(0);

    /**
     * cette méthode insère une clause dans le chapitre courant.
     * @param clause : clause à insérer.
     * @param position : position absolue dans la liste des clauses.
     * @return faux dans le cas ou la position est négative vrai dans le cas ou la position est positive.
     */
    public final boolean inserer(final Clause clause, final int position) {
        if (position < 0)
            return false;
        clause.setParent(this);
        clauses.add(position, clause);
        return false;
    }

    /**
     * cette méthode permet de supprimer un clause donnée.
     * @param valeur clause à supprimer
     * @return vrai si la clause est supprimée, faux si erreur
     */
    public final boolean supprimer(final Clause valeur) {
        return clauses.remove(valeur);
    }

    /**
     * cette méthode permet de déplacer une clause au sein du chapitre courant.
     * @param aDeplacer clause à déplacer
     * @param position position absolue dans la liste des clauses.
     * @return vrai si le deplacement de la clause s'est bien effectué dans le chapitre, faux si non.
     */
    public final boolean deplacerClause(final Clause aDeplacer, final int position) {
        if (!clauses.remove(aDeplacer))
            return false;
        return inserer(aDeplacer, position);
    }

    /**
     * cette méthode permet de vérifier si le chapitre possède cette clause
     * fille à la position donnée.
     * @param position numéro de fille.
     * @param clause clause à chercher.
     * @return résultat du test.
     */
    public final boolean aEnfant(final Clause clause, final int position) {
        if (clause == null || position < 0 || position >= clauses.size())
            return false;
        return clause.equals(clauses.get(position));
    }

    /**
     * cette méthode permet de renvoyer l'index du chapitre courant.
     * @return index du chapitre courant
     * @throws Exception erreur interne
     */
    public final int index() throws Exception {
        if (parent == null)
            throw new Exception("Parent vide, chapitre orphelin");
        int index = parent.getChapitres().indexOf(this);

        if (index == -1)
            throw new Exception("erreur interne, non trouvé");
        return index;
    }

    /**
     * cette méthode est utilisée pour mettre une clause en dernière position.
     * @return index de la dernière clause de ce chapitre + 1
     */
    public final int derniereClause() {
        return clauses.size();
    }
	
	/**
	 * Vérifier si ce chapitre possède au moins une sous-chapitre
	 * @return true si il y a au moins une chapitre
	 */
	public boolean hasSousChapitres() {
        return this.getChapitres() != null && !this.getChapitres().isEmpty();
    }
	
	/**
	 * Vérifier si ce chapitre possède au moins une clause
	 * @return true si il y a au moins une clause
	 */
	public boolean hasClauses() {
        return this.clauses != null && !this.clauses.isEmpty();
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final int hashCode() {

        int result = 1;
        if (chapitres != null)
            result = Constantes.PREMIER * result + chapitres.hashCode();
        if (clauses != null)
            result = Constantes.PREMIER * result + clauses.hashCode();
        if (infoBulle != null)
            result = Constantes.PREMIER * result + infoBulle.hashCode();
        if (titre != null)
            result = Constantes.PREMIER * result + titre.hashCode();
        return result;
    }

    /**
     * cette méthode permet de tester l'egalité entre les objets.
     * @param obj : l'objet à tester.
     * @return vrai si les objets sont egaux si non renvoyer faux.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Chapitre))
            return false;

        final Chapitre autre = (Chapitre) obj;
        if (chapitres == null) {
            if (autre.chapitres != null)
                return false;
        } else if (!chapitres.equals(autre.chapitres)) {
            return false;
        }

        if (clauses == null) {
            if (autre.clauses != null)
                return false;
        } else if (!clauses.equals(autre.clauses)) {
            return false;
        }

        if (infoBulle == null) {
            if (autre.infoBulle != null)
                return false;
        } else if (!infoBulle.equals(autre.infoBulle)) {
            return false;
        }

        if (titre == null) {
            if (autre.titre != null)
                return false;
        } else if (!titre.equals(autre.titre)) {
            return false;
        }
        return true;
    }

    /**
     * cette méthode permet de vérifier si le chapitre possède cette infobulle.
     * @param bulle infobulle à chercher
     * @return résultat du test
     */
    public final boolean aEnfant(final InfoBulle bulle) {
        return (bulle != null) && bulle.equals(infoBulle);
    }

    /**
     * cette méthode clone un objet de type chapitre.
     * @return chapitre
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final Chapitre clone() {
        Chapitre chapitre = new Chapitre();
        chapitre.titre = titre;
        chapitre.infoBulle = (InfoBulle) infoBulle.clone();

        List<Clause> listClause = new ArrayList<>(clauses.size());
        for (Clause clause : clauses)
            listClause.add(clause.clone());
        chapitre.clauses = listClause;

        List<Chapitre> listSousChapitre = new ArrayList<>(chapitres.size());
        for (Chapitre sousChapitre : chapitres)
            listSousChapitre.add(sousChapitre.clone());
        chapitre.setChapitres(listSousChapitre);

        chapitre.styleChapitre = styleChapitre;
        return chapitre;
    }

    /**
     * cette méthode permet la conversion en String.
     * @return renvoie la chaîne convertie.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final String toString() {
        StringBuilder r = new StringBuilder("Chapitre")
                .append("\nTitre").append(titre)
                .append("\nInfobulle: ").append(infoBulle)
                .append("\nClauses: ").append(clauses.size());

        for (Clause clause : clauses)
            r.append(clause);

        r.append("\nChapitres: ").append(chapitres.size());
        for (Chapitre chap : chapitres)
            r.append("\n").append(chap);

        return r.toString();
    }

    public int getIdChapitre() {
        return idChapitre;
    }

    public void setIdChapitre(int idChapitre) {
        this.idChapitre = idChapitre;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public InfoBulle getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulle infoBulle) {
        this.infoBulle = infoBulle;
    }

    public Derogation getDerogation() {
        return derogation;
    }

    public void setDerogation(Derogation derogation) {
        this.derogation = derogation;
    }

    public boolean isAfficherMessageDerogation() {
        return afficherMessageDerogation;
    }

    public void setAfficherMessageDerogation(boolean afficherMessageDerogation) {
        this.afficherMessageDerogation = afficherMessageDerogation;
    }

    public String getStyleChapitre() {
        return styleChapitre;
    }

    public void setStyleChapitre(String styleChapitre) {
        this.styleChapitre = styleChapitre;
    }

    public List<Clause> getClauses() {
        return clauses;
    }

    public void setClauses(List<Clause> clauses) {
        this.clauses = clauses;
    }

}
