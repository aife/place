
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="modal_template_version">
    <%--@elvariable id="gabaritForm" type="fr.paris.epm.redaction.presentation.forms.GabaritForm"--%>

    <div class="content">
        <h4>
            <bean:message key="gabarit.create.title"/>
        </h4>
        <form:form action="createTemplateVersion.htm" method="post" enctype="multipart/form-data">
            <div class="form-bloc">
                <div class="top"><span class="left"></span><span class="right"></span></div>

                <div class="content">
                    <div class="line">
                        <span class="intitule-long4">
                            <bean:message key="gabarit.create.nomFichier"/> * :
                        </span>
                        <input type="text" name="nomFichier" class="width-450" />.odt
                    </div>
                </div>

                <div class="content">
                    <div class="line">
                        <span class="intitule-long4">
                            <bean:message key="gabarit.create.templateFile"/> * :
                        </span>
                        <input type="file" name="fichier" class="width-450" accept=".odt"/>
                    </div>
                </div>

                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>

            <div class="boutons">
                <a id="annuler" href="javascript:void(0);" onclick="jQuery('.ui-dialog-content').dialog('close');" style="width: 70px;
                        background: #8a8a8a;
                        color: #fff;
                        border: 1px solid #8a8a8a;
                        float: left;">
                    <bean:message key="redaction.action.annuler"/>
                </a>
                <button type="submit" id="valider"
                        style="width: 70px;
                        background: #8a8a8a;
                        color: #fff;
                        border: 1px solid #8a8a8a;
                        float: right;
                        font-weight: bold;">
                    <bean:message key="redaction.action.valider"/>
                </button>
            </div>
        </form:form>
    </div>
</div>