<%-- <%@ page errorPage="/jsp/bodies/pageErreur.jsp"%> --%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
</head>
<body onload="popupResize();">
<c:if test="${chapitreSeul.terminer == false}">
    <c:set var="couleur" value="red" scope="request" />
</c:if>
<c:if test="${chapitreSeul.terminer == true }">
    <c:set var="couleur" value="green" scope="request" />
</c:if>

<div class="previsu-doc" id="container">
    <div class="content">
        <center>
            <h5><c:out value="${document.reference}" /></h5>
        </center>
        <ul>
            <li>
                <ul>
                    <li>
                        <span class="<c:out value="${couleur}"/>">
                            <strong>
                                <c:out value="${chapitreSeul.numero}" />.&nbsp;
                                <c:out value="${chapitreSeul.titre}" />
                            </strong>
                        </span>
                        <ul class="liste-clause">
                            <logic:notEmpty name="chapitreSeul" property="paragrapheDocument">
                                <li>
                                    <span class="<c:out value="${couleur}"/>">
                                        <nested:write name="chapitreSeul" property="paragrapheDocument.texte" filter="false" />
                                    </span>
                                </li>
                            </logic:notEmpty>
                            <logic:empty name="chapitreSeul" property="paragrapheDocument">
                                <logic:notEmpty name="chapitreSeul" property="clausesDocument">
                                    <logic:iterate id="clause" name="chapitreSeul" property="clausesDocument">
                                        <c:if test="${clause.actif == '1' && (clause.etat == '0' || clause.etat == '1')}">
                                            <li>
                                                <bean:define id="clause" name="clause" toScope="request" />
                                                <jsp:include page="/jsp/previsualisation/document/Clause.jsp" />
                                            </li>
                                        </c:if>
                                    </logic:iterate>
                                </logic:notEmpty>
                            </logic:empty>
                        </ul>
                        <logic:notEmpty name="chapitreSeul" property="sousChapitres">
                            <bean:define id="premierNiveau" value="false" toScope="request" />
                            <bean:define id="chapitres" name="chapitreSeul" property="sousChapitres" toScope="request" />
                            <jsp:include page="/jsp/previsualisation/document/Chapitre.jsp" />
                        </logic:notEmpty>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <a href="javascript:window.close();" class="fermer">Fermer</a>
    <div class="breaker"></div>
</div>

</body>
</html>
