<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--Debut main-part-->
<!--Debut bloc Infos clause-->
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>
		<div class="content">
		
			<h2 class="float-left">
				<html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee" value="clauseEditeur"></html:radio>
				<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeur"/></label>
			</h2>
			<div class="actions-clause no-padding">
				<logic:equal name="typeAction" value="M">
<%-- 					<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
						<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 					</c:if> --%>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
<!-- 		<div class="info-maj"> -->
<%-- 		<bean:message key="clause.surcharge.modifieeLe"/> --%>
<%-- 			<c:out value="TODO" /> --%>
<!-- 		</div> -->		
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTexteLibre.txt.texteFixeAvant" /></span>
				
				<textarea disabled id="textFixeAvantEditeur" title="Texte fixe avant" class="texte-long mceEditorSansToolbar" rows="4" cols=""><c:out value="${clauseTexteLibreFormEditeur.textFixeAvant}" /></textarea>
			</div>
			<div class="line">
                   <div class="retour-ligne">
                   	<input type="checkbox" id="sautTextFixeAvantEditeur" <c:if test="${clauseTexteLibreFormEditeur.sautTextFixeAvant == 'true'}">checked="checked"</c:if> title="Saut de ligne" />
                   	<bean:message key="redaction.txt.sautLigne"/>
                   	<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
                   </div>
                  </div>
                  <div class="separator"></div>
                  
			<div class="line">
				<span class="intitule"><bean:message key="ClauseTexteLibre.txt.tailleChamp" /></span>
				<span class="intitule-auto"> 
					<c:set var="nbCaractValue" value="1"/>
					<c:if test="${not empty(clauseTexteLibreFormEditeur.nbCaract)}">
						<c:set var="nbCaractValue" value="${clauseTexteLibreFormEditeur.nbCaract}"/>
					</c:if>
					 <select id="nbCaractEditeur" class="auto" title="Taille de Champ" disabled="disabled">
						<option value="4" <logic:equal name="nbCaractValue" value="4">selected="selected"</logic:equal>>
		               		<bean:message key="redaction.taille.champ.tresLong" />
			            </option>
			            <option value="1" <logic:equal name="nbCaractValue" value="1">selected="selected"</logic:equal>>
			               <bean:message key="redaction.taille.champ.long" />
			            </option>
			            <option value="2" <logic:equal name="nbCaractValue" value="2">selected="selected"</logic:equal>>
			               <bean:message key="redaction.taille.champ.moyen" />
			            </option>
			            <option value="3" <logic:equal name="nbCaractValue" value="3">selected="selected"</logic:equal>>
			               <bean:message key="redaction.taille.champ.court" />
			            </option>
					</select>
				</span>
			</div>
			
			<c:if test="${nbCaractValue == '0' }">
				<script>
					mettreTailleChampValeurDefaut('nbCaractEditeur');
				</script>
			</c:if>
			
			<div class="line">
				<span class="intitule"><bean:message key="ClauseTexteLibre.txt.champLibreObligatoire" /></span>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" id="champLibreObligatoireEditeur" title="Oui" 
						<c:if test="${clauseTexteLibreFormEditeur.champLibreObligatoire == 1}">checked="checked"</c:if>/>
					<bean:message key="ClauseTexteLibre.txt.champLibreObligatoireOui" />
				</div>
				<div class="radio-choice">
					<input disabled="disabled" type="radio" id="champLibreObligatoireEditeur" title="Non" 
						<c:if test="${clauseTexteLibreFormEditeur.champLibreObligatoire == 0}">checked="checked"</c:if>/>
					<bean:message key="ClauseTexteLibre.txt.champLibreObligatoireNon" />
				</div>
			</div>
			<div class="line">
                   <div class="retour-ligne">
					<input type="checkbox" id="sautTextFixeApresEditeur" <c:if test="${clauseTexteLibreFormEditeur.sautTextFixeApres == 'true'}">checked="checked"</c:if> title="Saut de ligne" />
                   	<bean:message key="redaction.txt.sautLigne"/>
                   	<img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" /></div>
                  </div>
                  
                  <div class="separator"></div>
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTexteLibre.txt.texteFixeApres" /></span>
				
				<textarea disabled id="textFixeApresEditeur" title="Texte fixe avant" class="texte-long mceEditorSansToolbar" rows="4" cols=""><c:out value="${clauseTexteLibreFormEditeur.textFixeApres}"/></textarea>
			</div>
			
			<div class="actions-clause">
				<logic:equal name="typeAction" value="M">
<%-- 					<c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
						<a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
<%-- 					</c:if> --%>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
			
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span><span class="right"></span>
		</div>
	</div>
<script type="text/javascript">
	initEditeursTexteSansToolbarRedaction();
</script>