package fr.paris.epm.redaction.presentation.controllers.v2.model;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;


public class User {
    @NotEmpty
    private String id;
    @NotEmpty
    private String name;

    private List<String> roles = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
