package fr.paris.epm.redaction.presentation.views;

public class Environnement {

	private String client;

	private String plateforme;

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getPlateforme() {
		return plateforme;
	}

	public void setPlateforme(String plateforme) {
		this.plateforme = plateforme;
	}

	public Environnement(String client, String plateforme) {
		this.client = client;
		this.plateforme = plateforme;
	}
}
