package fr.paris.epm.redaction.metier.documentHandler;

import fr.atexo.commun.document.generation.service.xls.translator.CouleurTranslatorXls;
import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.util.Util;
import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.service.DocumentModeleServiceSecurise;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Classe utilitaire permettant de transformer le tableau de redaction
 * @{TableauxRedaction en un flux HTML. L'objet @{TableauxRedaction} est la
 *                     répresentation sous forme d'objet (générer a partir d'un
 *                     xsd grace à JAXB) du flux XML de chaque clause tableau.
 *                     Ces objets sont parcourus et traités pour générer un
 *                     tableau HTML avec des styles et gestion de rowspan et
 *                     colspan. Ce HTML généré sera utilisé notamment dans le
 *                     cadre de la prévisualisation des clauses du type tableau
 *                     ainsi que des canevas et documents contenant des clauses
 *                     tableaux. Cette classe utilitaire est appelé par
 *                     GestionnaireClausesTableaux.
 * @author Rebeca Dantas
 */
public class GenerationTableauUtil {
    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(GenerationTableauUtil.class);

    private static DocumentModeleServiceSecurise documentModeleService;

    public static void setDocumentModeleService(DocumentModeleServiceSecurise valeur) {
        GenerationTableauUtil.documentModeleService = valeur;
    }

    /**
     * Ensemble des constantes répresentant les balises HTML, utilisés pour
     * générer le flux HTML
     */
    private static final String ELEMENT_DEBUT_DIV = "<div>";
    private static final String ELEMENT_FIN_DIV = "</div>";
    private static final String ELEMENT_SAUT_LIGNE = "<br />";
    private static final String ELEMENT_DEBUT_PARAGRAPHE = "<p>";
    private static final String ELEMENT_FIN_PARAGRAPHE = "</p>";
    private static final String ELEMENT_DEBUT_TABLEAU = "<table class=\"tableau\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\">";
    private static final String ELEMENT_FIN_TABLEAU = "</table>";
    private static final String ELEMENT_DEBUT_BODY = "<tbody>";
    private static final String ELEMENT_FIN_BODY = "</tbody>";
    private static final String ELEMENT_DEBUT_LIGNE = "<tr>";
    private static final String ELEMENT_DEBUT_LIGNE_ENTETE = "<tr class=\"entete\">";
    private static final String ELEMENT_DEBUT_LIGNE_ON = "<tr class=\"on\">";
    private static final String ELEMENT_FIN_LIGNE = "</tr>";
    private static final String ELEMENT_DEBUT_CELLULE = "<td>";
    private static final String ELEMENT_FIN_CELLULE = "</td>";
    private static final String ELEMENT_PARTIEL_DEBUT_CELLULE = "<td";

    // TODO faire un enum d'association
    /**
     * Recupere le border css associé au EnumerationBordure BORDER_THIN,
     * BORDER_MEDIUM, BORDER_DASHED, BORDER_HAIR, BORDER_THICK, BORDER_DOUBLE,
     * BORDER_DOTTED, BORDER_MEDIUM_DASHED, BORDER_DASH_DOT,
     * BORDER_MEDIUM_DASH_DOT, BORDER_DASH_DOT_DOT, BORDER_MEDIUM_DASH_DOT_DOT,
     * BORDER_SLANTED_DASH_DOT;
     */
    private static Map<EnumerationBordure, String> styleBorderHashMap = new HashMap<EnumerationBordure, String>();
    static {
        styleBorderHashMap.put(EnumerationBordure.BORDER_NONE, "none");
        styleBorderHashMap.put(EnumerationBordure.BORDER_THIN, "THIN");
        styleBorderHashMap.put(EnumerationBordure.BORDER_MEDIUM, "MEDIUM");
        styleBorderHashMap.put(EnumerationBordure.BORDER_DASHED, "DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_HAIR, "none");
        styleBorderHashMap.put(EnumerationBordure.BORDER_THICK, "THICK");
        styleBorderHashMap.put(EnumerationBordure.BORDER_DOUBLE, "DOUBLE");
        styleBorderHashMap.put(EnumerationBordure.BORDER_DOTTED, "DOTTED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_MEDIUM_DASHED, "MEDIUM DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_DASH_DOT, "DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_MEDIUM_DASH_DOT, "MEDIUM DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_DASH_DOT_DOT, "DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_MEDIUM_DASH_DOT_DOT, "MEDIUM DASHED");
        styleBorderHashMap.put(EnumerationBordure.BORDER_SLANTED_DASH_DOT, "none");
    }

    /**
     * Transforme le flux XML avec le remplacement freemaker spécifique mode
     * prévisualisation. Ce qui permet de valider le XML et de les transformer
     * en objet java du type @{TableauxRedaction}
     * @param contenuXML le flux XML brut sous format String après lecture du fichier XML
     */
    public static TableauRedaction xmlToTableauRedaction(String contenuXML, EpmTConsultation consultation, Integer idLot, List<Derogation> derogations) throws TechnicalException {
        Map<String, Object> freeMarkerMap = new HashMap<String, Object>();
        Boolean modePrevisualisation = consultation == null && idLot == null && derogations == null ? true : null;
        freeMarkerMap.put("modePrevisualisation", modePrevisualisation);
        freeMarkerMap.put("derogations", derogations); //derogations peuvent etre null

        try {
            String xml;
            if (consultation != null)
                xml = documentModeleService.genererFreeMarker(freeMarkerMap, contenuXML, consultation, idLot);
            else
                xml = FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, contenuXML);
            xml = xml.replaceAll("& ", "&amp; ");

            InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
            JAXBContext context = JAXBContext.newInstance(TableauRedaction.class);
            Unmarshaller unmarshall = context.createUnmarshaller();
            return  (TableauRedaction) unmarshall.unmarshal(is);
        } catch (UnsupportedEncodingException ex) {
            LOG.error("Erreur d'encodage sur le flux XML : " + contenuXML);
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException("Erreur d'encodage sur le flux XML : " + contenuXML, ex);
        } catch (JAXBException ex) {
            LOG.error("Erreur lors de la conversion du flux XML en objet java : " + contenuXML);
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException("Erreur lors de la conversion du flux XML en objet java : " + contenuXML, ex);
        } catch (Exception ex) {
            LOG.error("Erreur lors de la generation freemark sur la XML suivant : " + contenuXML);
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException("Erreur lors de la generation freemark sur le fichier XML suivant : " + contenuXML, ex);
        }
    }

    /**
     * A partir d'un objet @{TableauxRedaction}, un tableau HTML sera créée pour
     * répresenter chaque clause tableau. Parcoure les élements de
     * TableauxRedaction pour créer les balises HTML correspondantes.
     * @param tableauRedaction objet qui répresente le flux XML de chaque clause tableau
     * @return le flux HTML correspondant à la clause tableau sous forme d'objet
     */
    public static String tableauRedactionToHTML(TableauRedaction tableauRedaction) {
        StringBuilder htmlTableau = new StringBuilder(ELEMENT_DEBUT_DIV);

        if (tableauRedaction.getTextAvantTableau() != null) {
            htmlTableau.append(ELEMENT_DEBUT_PARAGRAPHE);
            htmlTableau.append(tableauRedaction.getTextAvantTableau());
            htmlTableau.append(ELEMENT_FIN_PARAGRAPHE);
        }

        for (fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.DocumentXLS document : tableauRedaction.getDocumentXLS()) {
            for (Onglet onglet : document.getOnglet()) {
                htmlTableau.append(ELEMENT_DEBUT_TABLEAU);
                htmlTableau.append(ELEMENT_DEBUT_BODY);
                htmlTableau.append(creerLignes(onglet));
                htmlTableau.append(ELEMENT_FIN_BODY);
                htmlTableau.append(ELEMENT_FIN_TABLEAU);
            }
        }

        htmlTableau.append(ELEMENT_SAUT_LIGNE);
        htmlTableau.append(ELEMENT_FIN_DIV);
        return Util.sanitizeHtmlInput(htmlTableau.toString());
    }

    /**
     * Crée les lignes HTML d'un tableau avec la gestion de merges
     * (colspan/rowspan) a partir des @{Cellule} du @{TableauRedaction}
     * @param onglet
     * @return le flux HTML correspondant à la ligne du tableau HTML créé
     */
    private static String creerLignes(Onglet onglet) {
        Map<Integer, List<Cellule>> cellulesOrdonnees = traiterCellules(onglet.getCellule());
        
        int ligneCourante = 0;
        boolean premierElement = true;
        StringBuilder htmlTableau = new StringBuilder();

        List<Integer> lignes = new ArrayList<>(cellulesOrdonnees.keySet());
        Collections.sort(lignes);

        for (Integer ligne : lignes) {
            if (premierElement) {
                htmlTableau.append(enteteStyle(cellulesOrdonnees.get(ligne)));
                htmlTableau.append(creerCellules(cellulesOrdonnees.get(ligne), onglet.getRegionFusionnee(), onglet.getAttributs()));
                premierElement = false;
            } else if (ligne != ligneCourante) {
                ligneCourante = ligne;
                htmlTableau.append(ELEMENT_FIN_LIGNE);
                htmlTableau.append(enteteStyle(cellulesOrdonnees.get(ligne)));
                htmlTableau.append(creerCellules(cellulesOrdonnees.get(ligne), onglet.getRegionFusionnee(), onglet.getAttributs()));
            }
        }
        htmlTableau.append(ELEMENT_FIN_LIGNE);
        return htmlTableau.toString();
    }

    /**
     * Méthode à évoluer : Vérification si les lignes contiennent une style
     * d'entête ou de tableaux fixe, dont la couleur de fond des cellules sont
     * intercallés
     * @param cellulesParLigne liste des cellules du tableau pour vérifier quel style sera appliqué
     * @return le flux HTML correspondant au style appliqué à la cellule du tableau
     */
    private static String enteteStyle(List<Cellule> cellulesParLigne) {
        String htmlTableau = ELEMENT_DEBUT_LIGNE;
        if (cellulesParLigne != null && !cellulesParLigne.isEmpty()) {
            Cellule cellule = cellulesParLigne.get(0);
            
            if (cellule.getStyle() != null && cellule.getStyle().getRemplissage() != null) {
                EnumerationCouleur couleur = cellule.getStyle().getRemplissage().getArrierePlan();
                if (couleur.equals(EnumerationCouleur.GREY_50_PERCENT)) {
                    htmlTableau = ELEMENT_DEBUT_LIGNE_ENTETE;
                } else if (couleur.equals(EnumerationCouleur.GREY_25_PERCENT)) {
                    htmlTableau = ELEMENT_DEBUT_LIGNE_ON;
                }
            }
        }

        return htmlTableau;
    }

    /**
     * Créer des cellules HTML a partir d'une liste des cellules @{Cellule} de
     * l'objet répresentant le flux XML du tableau
     * @param cellulesLigneSpecifique liste d'objets répresentant les cellules du flux XML d'une clause tableau
     * @param fusions liste d'objets répresentant les régions fusionnées d'un tableau
     * @return le flux HTML correspondant aux cellules crées une même ligne
     */
    private static String creerCellules(List<Cellule> cellulesLigneSpecifique, List<RegionFusionnee> fusions, TableauAttributs attributs) {
        StringBuilder htmlTableau = new StringBuilder();
        // on recupere les cellules de la ligne en question
        for (Cellule cellule : cellulesLigneSpecifique) {
            // verifie colspan ou rowspan
            htmlTableau.append(celluleStyle(cellule, fusions, attributs));
            htmlTableau.append(getValeurAvecTraitement(cellule.getValeur()));
            htmlTableau.append(ELEMENT_FIN_CELLULE);
        }
        return htmlTableau.toString();
    }

    private static String celluleStyle(Cellule cellule, List<RegionFusionnee> fusions, TableauAttributs tableauAttributs) {
        StringBuilder styleHTML = null;
        if (cellule.getStyle() != null) {
            styleHTML = new StringBuilder(" style =\"");

            // background color du HTML
            if (cellule.getStyle().getRemplissage() != null) {
                styleHTML.append(recupererBackgroundStylePourHTML(cellule.getStyle().getRemplissage().getArrierePlan()));
            }

            // alignement des cellules du tableau HTML
            styleHTML.append(recupererAlignementStylePourHTML(cellule.getStyle().getAlignement()));

            // gerer les fonts
            styleHTML.append(recupererFontStylePourHTML(cellule.getStyle().getFont()));

            // gerer les bordures
            styleHTML.append(recupererBordureStylePourHTML(cellule.getStyle().getBordure()));
            
            styleHTML.append("\"");

            LOG.debug("Style défini pour la cellule (ligne " + cellule.getCoordonnee().getLigne() + " - colonne " + cellule.getCoordonnee().getColonne() + " : ", styleHTML.toString());
        }
        
        List<AttributLargeurColonne> attributs = null;
        if(tableauAttributs != null && tableauAttributs.getLargeurColonne() != null && !tableauAttributs.getLargeurColonne().isEmpty()) {
            attributs = tableauAttributs.getLargeurColonne();
        }

        String htmlTableau = ELEMENT_DEBUT_CELLULE;
        if (traiterFusion(cellule, fusions).isEmpty()) {
            if (styleHTML != null && !styleHTML.toString().isEmpty())
                htmlTableau = ELEMENT_PARTIEL_DEBUT_CELLULE + traiterTailleCellule(cellule, attributs) + styleHTML.toString() + ">";
            else
                htmlTableau = ELEMENT_PARTIEL_DEBUT_CELLULE + traiterTailleCellule(cellule, attributs) + ">";
        } else {
            if (styleHTML != null && !styleHTML.toString().isEmpty())
                htmlTableau = ELEMENT_PARTIEL_DEBUT_CELLULE + traiterTailleCellule(cellule, attributs) + styleHTML.toString() + traiterFusion(cellule, fusions);
            else
                htmlTableau = ELEMENT_PARTIEL_DEBUT_CELLULE + traiterTailleCellule(cellule, attributs) + traiterFusion(cellule, fusions);
        }
        return htmlTableau;
    }

    private static String traiterTailleCellule(Cellule cellule, List<AttributLargeurColonne> attributs) {
        String taille = "";
        if(attributs != null && !attributs.isEmpty()) {
            for(AttributLargeurColonne attribut : attributs) {
                if(cellule.getCoordonnee().getColonne() == attribut.getColonne()) {
                    taille = " width='"+attribut.getLargeur()+"%' ";
                    break;
                }
            }
        }
        return taille;
    }

    /**
     * Récupere les informations de la bordure pour le style HTML
     * @param bordure
     * @return le code CSS concernant la bordule pour le HTML
     */
    private static String recupererBordureStylePourHTML(CelluleStyleBordure bordure) {

        StringBuilder style = new StringBuilder();

        if (bordure != null) {
            if (bordure.getBas() != null && styleBorderHashMap.get(bordure.getBas()) != null)
                style.append(" border-bottom:").append(styleBorderHashMap.get(bordure.getBas())).append(";");
            if (bordure.getHaut() != null && styleBorderHashMap.get(bordure.getHaut()) != null)
                style.append(" border-top:").append(styleBorderHashMap.get(bordure.getHaut())).append(";");
            if (bordure.getGauche() != null && styleBorderHashMap.get(bordure.getGauche()) != null)
                style.append(" border-left:").append(styleBorderHashMap.get(bordure.getGauche())).append(";");
            if (bordure.getDroite() != null && styleBorderHashMap.get(bordure.getDroite()) != null)
                style.append(" border-right:").append(styleBorderHashMap.get(bordure.getDroite())).append(";");
        }
        return style.toString();
    }

    /**
     * Récupére les informations pour la CSS de la font du HTML
     * @param celluleStyleFont les informations de la font provenant du tableau XML (elements document type)
     * @return le code CSS concernant la font du HTML
     */
    private static String recupererFontStylePourHTML(CelluleStyleFont celluleStyleFont) {

        StringBuilder fontBuffer = new StringBuilder(" ");

        if (celluleStyleFont != null) {
            fontBuffer.append(" font-style:");
            if (celluleStyleFont.isItalic())
                fontBuffer.append("italic; ");
            else
                fontBuffer.append("normal; ");

            fontBuffer.append(" font-weight:");
            if (celluleStyleFont.isGras())
                fontBuffer.append("bold; ");
            else
                fontBuffer.append("normal; ");

            if (celluleStyleFont.getTaille() != null)
                fontBuffer.append(" font-size: ").append(celluleStyleFont.getTaille()).append("px; ");

            if (celluleStyleFont.getSouligne() != null) {
                if (EnumerationFontSousligne.U_NONE.equals(celluleStyleFont.getSouligne()))
                    fontBuffer.append(" text-decoration:none; ");
                if (EnumerationFontSousligne.U_SINGLE.equals(celluleStyleFont.getSouligne()))
                    fontBuffer.append(" text-decoration:underline; ");
            }

            if (celluleStyleFont.getCouleur() != null) {
                short[] triplet = getRgbCodeDepuisEnumerationCouleur(celluleStyleFont.getCouleur());
                fontBuffer.append(" color: rgb(").append(triplet[0]).append(",").append(triplet[1]).append(",").append(triplet[2]).append(");");
            }
        }
        return fontBuffer.toString();
    }

    /**
     * Récupere la valeur de l'alignement pour la version du tableau en HTML,
     * notammennt pour la prévisualisation et l'aperçu dynamique
     * @param alignement
     * @return le contenu CSS pour le style HTML
     */
    private static String recupererAlignementStylePourHTML(CelluleStyleAlignement alignement) {

        StringBuilder style = new StringBuilder();
        if (alignement != null) {
            if (alignement.getHorizontal() != null) {
                EnumerationAlignementHorizontal AllignementHorizontal = alignement.getHorizontal();
                if (AllignementHorizontal.equals(EnumerationAlignementHorizontal.ALIGN_CENTER))
                    style.append(" text-align:center;");
                if (AllignementHorizontal.equals(EnumerationAlignementHorizontal.ALIGN_LEFT))
                    style.append(" text-align:left;");
                if (AllignementHorizontal.equals(EnumerationAlignementHorizontal.ALIGN_RIGHT))
                    style.append(" text-align:right;");
                if (AllignementHorizontal.equals(EnumerationAlignementHorizontal.ALIGN_JUSTIFY))
                    style.append(" text-align:justify;");
            }
            if (alignement.getVertical() != null) {
                EnumerationAlignementVertical AllignementVertical = alignement.getVertical();
                if (AllignementVertical.equals(EnumerationAlignementVertical.VERTICAL_CENTER))
                    style.append(" vertical-align:center;");
                if (AllignementVertical.equals(EnumerationAlignementVertical.VERTICAL_BOTTOM))
                    style.append(" vertical-align:bottom;");
                if (AllignementVertical.equals(EnumerationAlignementVertical.VERTICAL_TOP))
                    style.append(" vertical-align:top;");
                if (AllignementVertical.equals(EnumerationAlignementVertical.VERTICAL_CENTER))
                    style.append(" vertical-align:middle;");
            }
        }
        return style.toString();
    }

    /**
     * Récupere la valeur du background pour la version du tableau en HTML,
     * notammennt pour la prévisualisation et l'aperçu dynamique
     * @param couleur
     * @return le contenu CSS pour le style HTML
     */
    private static String recupererBackgroundStylePourHTML(EnumerationCouleur couleur) {
        String style = "";
        if (couleur != null) {
            short[] triplet = getRgbCodeDepuisEnumerationCouleur(couleur);
            style = "background-color: rgb(" + triplet[0] + "," + triplet[1] + "," + triplet[2] + ");";
        }
        return style;
    }

    /**
     * Recupere le code rgb d'une couleur open office depuis une enumeration
     * couleur de redaction
     * @param couleur : le code enum d'une couleur open-office
     * @return code RGB de la couleur sous format (red code,green code,blue
     *         code)
     */
    private static short[] getRgbCodeDepuisEnumerationCouleur(final EnumerationCouleur couleur) {
        fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCouleur enumerationCouleur =
                fr.atexo.commun.document.generation.service.tableaudef.bean.EnumerationCouleur.valueOf(couleur.value());

        Integer CodeCouleur = (Integer) (new CouleurTranslatorXls()).transale(enumerationCouleur);
        HSSFColor hSSFColor = (HSSFColor) HSSFColor.getIndexHash().get(CodeCouleur);
        return (hSSFColor.getTriplet());
    }

    /**
     * Traitement pour gérer les sauts de lignes et les espaces. On transforme
     * \n \r en <br />
     * @param valeur le contenu de la balise valeur du XML
     * @return le contenu de la balise avaleur du XML après traitement des sauts
     *         de lignes
     */
    private static String getValeurAvecTraitement(String valeur) {
        String valeurRetournee = valeur;
        if (valeurRetournee != null && !valeurRetournee.isEmpty()) {
            valeurRetournee = valeurRetournee.replaceAll("\n", "<br />");
            valeurRetournee = valeurRetournee.replaceAll("\r", "<br />");
            valeurRetournee = valeurRetournee.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        }
        return valeurRetournee;
    }

    /**
     * Vérifie si la cellule appartient à une region fusionnée, si c'est le cas,
     * on ajout dans le flux HTML retournée le traitement rowspan ou colspan
     * @param cellule cellule à vérifier si appartient à une région fusionnée
     * @param fusions liste des regions fusionnées
     * @return le flux HTML contenant rowspan ou colspan si la cellule était
     *         dans le cas d'une région fusionnée
     */
    private static String traiterFusion(Cellule cellule, List<RegionFusionnee> fusions) {
        String htmlTableau = "";
        for (RegionFusionnee fusion : fusions) {
            if (fusion.getLigneDebut() == cellule.getCoordonnee().getLigne() && fusion.getColonneDebut() == cellule.getCoordonnee().getColonne()) {
                // cas colspan
                if (fusion.getLigneDebut() == fusion.getLigneFin()) {
                    htmlTableau += " colspan=\"" + ((fusion.getColonneFin() - fusion.getColonneDebut()) + 1) + "\">";
                    break;
                    // cas rowspan
                } else if (fusion.getColonneDebut() == fusion.getColonneFin()) {
                    htmlTableau += " rowspan=\"" + ((fusion.getLigneFin() - fusion.getLigneDebut()) + 1) + "\">";
                    break;
                }
            }
        }
        return htmlTableau;
    }

    /**
     * Traitement des cellules afin de les ordonnées par ligne. Cela permettra
     * de créer plus facilement les tableaux HTML avec la suite des lignes
     * ordonnées et avec les bons fusionnements
     * @param cellules liste des cellules à ordonner
     * @return liste de cellules ordonnées
     */
    private static Map<Integer, List<Cellule>> traiterCellules(List<Cellule> cellules) {

        Map<Integer, List<Cellule>> cellulesOrdonnees = new HashMap<Integer, List<Cellule>>();
        List<Cellule> cellulesParLigne = new ArrayList<Cellule>();
        for (Cellule cellule : cellules) {

            if (cellulesOrdonnees.containsKey(cellule.getCoordonnee().getLigne())) {
                cellulesParLigne = cellulesOrdonnees.get(cellule.getCoordonnee().getLigne());
            } else {
                cellulesParLigne = new ArrayList<Cellule>();
            }
            cellulesParLigne.add(cellule);
            cellulesOrdonnees.put(cellule.getCoordonnee().getLigne(), cellulesParLigne);
        }

        return cellulesOrdonnees;
    }

}
