package fr.paris.epm.redaction.commun;

import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasAbstract;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static fr.paris.epm.noyau.commun.Constantes.NON;
import static fr.paris.epm.noyau.commun.Constantes.OUI;

/**
 * Classe utilitaire qui gère les conflits sur le contenu des clauses lors de la duplication d'un document.
 * les méthodes de cette classe correspondent aux types de clauses. chaque méthode prendra en entrée le document à dupliquer
 * et le document créé à partir du canevas ayant servi à la création du document que l'on souhaite dupliquer
 * ************ Effectue des vérifications du côté serveur *****************
 * @author MGA
 */
public class GestionConflitsServeurUtils implements Serializable {

    private enum DuplicationLot {
        TOUS("tous"),
        TOUS_OU_CHAQUE_LOT("tous+choixLot"),
        CHAQUE_LOT("choixLot");

        private final String value;

        DuplicationLot( String value ) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    private static final long serialVersionUID = -6658460106475569461L;

    /**
     * Méthode permettant de voir si une duplication de document de consultation est possible ou pas
     * @return la clé du message d'erreur à afficher en cas d'erreur sinon vide
     */
    public static boolean verifierPossibiliteDuplication(final String ancienneConsultationAllotie, final EpmTConsultation nouvelleConsultation, final int idLot) {
        if ( OUI.equals(ancienneConsultationAllotie) && nouvelleConsultation != null && (nouvelleConsultation.getAllotissement() == null || NON.equals(nouvelleConsultation.getAllotissement())) && idLot != 0) {
            return false;
        }
        return true;
    }

    /**
     * Méthode permettant de voir si une duplication de document de consultation est possible ou pas
     * @return si le retour = tous alors on affiche que "Tous" dans le select de la jsp creationDocumentFormulaire,
     * sinon si c'est choixLot alors on affiche la liste des lots de la consultation
     */
    public static String verifierCompatibiliteAllotissement( final String ancienneConsultationAllotie,
                                                             final EpmTConsultation nouvelleConsultation, final int idLot) {

        final boolean initialeConsultationAllotie = OUI.equals(ancienneConsultationAllotie);

        // Consultation différente
        if (null != nouvelleConsultation) {
            final boolean nouvelleConsultationAllotie = OUI.equals(nouvelleConsultation.getAllotissement());

            // 1. SI consultation initiale ET nouvelle consultation NON ALLOTIES
            if ( !initialeConsultationAllotie && !nouvelleConsultationAllotie ) {
                // ALORS lot non sélectionnable dans le document dupliqué depuis la consultation initiale
                // dans la nouvelle consultation
                return DuplicationLot.TOUS.getValue();
            }

            // 2. SI consultation initiale ET nouvelle consultation ALLOTIES
            else if ( initialeConsultationAllotie && nouvelleConsultationAllotie) {
                boolean lotInconnu = idLot == 0 || idLot == -1;

                // ET document d'origine rédigé pour tous lots,
                if (lotInconnu) {
                    // ALORS tous et n*lots sélectionnables dans le document dupliqué depuis la consultation
                    // initiale dans la nouvelle consultation
                    return DuplicationLot.TOUS_OU_CHAQUE_LOT.getValue();
                } else {
                    // ET document d'origine rédigé pour 1 lot n,
                    // ALORS option x "lots n" est sélectionnable pour le document dupliqué
                    return DuplicationLot.CHAQUE_LOT.getValue();
                }
            }

            // 3. SI consultation initiale ALLOTIE ET nouvelle consultation NON ALLOTIES
            else if ( initialeConsultationAllotie && !nouvelleConsultationAllotie ) {
                // ALORS tous non sélectionnable
                // dans le document dupliqué depuis la consultation initiale dans la nouvelle consultation
                return DuplicationLot.CHAQUE_LOT.getValue();
            }

            // SI consultation initiale NON ALLOTIE ET nouvelle consultation ALLOTIES
            else if ( !initialeConsultationAllotie && nouvelleConsultationAllotie ) {
                // ALORS tous lots et lots n sélectionnables dans le document dupliqué depuis la consultation
                // initiale dans la nouvelle consultation
                return DuplicationLot.TOUS_OU_CHAQUE_LOT.getValue();
            }

        } else {
            // meme consultation
            // SI consultation NON ALLOTIE
            if (!initialeConsultationAllotie) {
                // ALORS option lot non sélectionnable sur le document dupliqué
                return DuplicationLot.TOUS.getValue();
            } else {
                // SI consultation ALLOTIE
                boolean lotInconnu = idLot == 0 || idLot == -1;

                // ET document d'origine rédigé pour tous lots
                if (lotInconnu) {
                    // ALORS tous et n*lots sélectionnables dans le document dupliqué depuis la consultation
                    // initiale dans la nouvelle consultation
                    return DuplicationLot.TOUS_OU_CHAQUE_LOT.getValue();
                } else {
                    // ET document d'origine rédigé pour 1 lot n
                    // ALORS option x "lots n" est sélectionnable pour le document dupliqué
                    return DuplicationLot.CHAQUE_LOT.getValue();
                }
            }
        }

        return DuplicationLot.TOUS_OU_CHAQUE_LOT.getValue();
    }

    /**
     * Vérifie si la procedure de passation et le pouvoir adjudicateur du document à dupliquer sont compatibles avec ceux de la consultation
     * @return le numero de consultation en cas d'incompatibilité
     */
    public static boolean verifierProcedurePassationNaturePrestationsCompatibles(final EpmTConsultation nouvelleConsultation,
                                                                                 final EpmTCanevasAbstract epmTCanevas) {

        final int idRefToutesProcedureEditeur = 1;

        boolean compatible = false;

        if (epmTCanevas.getEpmTRefProcedures() == null || epmTCanevas.getEpmTRefProcedures().isEmpty()) {
            compatible = true;
        } else {
            for (EpmTRefProcedure epmTRefProcedure : epmTCanevas.getEpmTRefProcedures()) {
                if (epmTRefProcedure.getId() == idRefToutesProcedureEditeur ||
                        epmTRefProcedure.getCodeExterne().equals(nouvelleConsultation.getEpmTRefProcedure().getCodeExterne())) {
                    compatible = true;
                    break;
                }
            }
        }

        if (epmTCanevas.getIdNaturePrestation() != 0 && epmTCanevas.getIdNaturePrestation() != nouvelleConsultation.getEpmTRefNature().getId())
            compatible = false;

        return compatible;
    }

    /**
     * Vérifie si le CCAG de référence du document à dupliquer est compatible
     * avec celui de la consultation
     *
     * @param consultation la consultation pour laquelle on veut dupliquer le document
     * @param canevas      le canevas sur laquelle le document était créé
     * @param idLot        le document est dupliqué pour quel lot. -1 si Non applicable, 0 si appliqué pour tous lots
     * @return true si une duplication de document de consultation est possible
     */
    public static boolean verifierCCAGCompatibles(final EpmTConsultation consultation, final EpmTCanevasAbstract canevas, Integer idLot) {
        Integer idRefCCAG = canevas.getIdRefCCAG();

        if (idRefCCAG == null)
            return false;

        if (idRefCCAG == 0) // la valeur CCAG de référence de ce Canevas est Tous
            return true;

        if ( NON.equals(consultation.getAllotissement())) {
            if (consultation.getEpmTBudLotOuConsultation().getEpmTRefCcag() != null) {
                return Objects.equals(consultation.getEpmTBudLotOuConsultation().getEpmTRefCcag().getId(), idRefCCAG);
            }
        } else {
            for (EpmTBudLot lot : consultation.getEpmTBudLots()) {
                if (idLot == null || idLot <= 0 || lot.getId() == idLot) {
                    if (lot.getEpmTBudLotOuConsultation().getEpmTRefCcag() != null
                            && Objects.equals(lot.getEpmTBudLotOuConsultation().getEpmTRefCcag().getId(), idRefCCAG)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}
