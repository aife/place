ALTER TABLE epm__t_canevas_has_ref_procedure_passation OWNER TO m13epmredaction;
GRANT ALL ON TABLE epm__t_canevas_has_ref_procedure_passation TO m13epmredaction;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE epm__t_canevas_has_ref_procedure_passation TO m13_r_saisie_redaction;

ALTER TABLE epm__t_clause_has_valeur_potentiellement_conditionnee OWNER TO m13epmredaction;
GRANT ALL ON TABLE epm__t_clause_has_valeur_potentiellement_conditionnee TO m13epmredaction;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE epm__t_clause_has_valeur_potentiellement_conditionnee TO m13_r_saisie_redaction;

