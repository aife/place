package fr.paris.epm.redaction.presentation.bean;

import java.util.Arrays;
import java.util.Optional;

public enum CategorieDocument {
	DAJ("daj", "ATT1", "ATT2", "OUV3", "OUV4", "OUV5", "OUV6", "OUV7", "OUV8", "OUV9", "OUV10", "OUV11", "NOTI1", "NOTI3", "NOTI4", "NOTI5", "NOTI7", "NOTI8", "DC1", "DC2", "DC4"),
	PGF("pgf", "PG", "PF", "DL"),
	DLI("dli", "DL"),
	DCR("dcr");

	private final String libelle;
	private final String[] types;

	CategorieDocument( String libelle, String... types ) {
		this.libelle = libelle;
		this.types = types;
	}

	public static String[] lister() {
		return Arrays.stream(CategorieDocument.values()).map(CategorieDocument::getTypes).flatMap(Arrays::stream).toArray(String[]::new);
	}

	public String getLibelle() {
		return libelle;
	}

	public String[] getTypes() {
		return types;
	}

	public static Optional<CategorieDocument> trouver( final String value) {
		return Arrays.stream(CategorieDocument.values()).filter(c->c.getLibelle().equalsIgnoreCase(value)).findFirst();
	}

	public boolean contient( String key ) {
		return Arrays.stream(this.getTypes()).anyMatch(key::equalsIgnoreCase);
	}
}
