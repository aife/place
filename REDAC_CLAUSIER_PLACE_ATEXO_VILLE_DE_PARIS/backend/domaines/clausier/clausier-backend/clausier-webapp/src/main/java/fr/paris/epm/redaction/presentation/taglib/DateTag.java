/**
 * 
 */
package fr.paris.epm.redaction.presentation.taglib;

import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Ce tag gére l'affichage et le format d'une date passée en paramètre.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class DateTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * format de la date.
     */
    private String format;

    /**
     * nom de l'instance de l'objet.
     */
    private String name;

    /**
     * emplacement de l'objet.
     */
    private String scope;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DateTag.class);

    /**
     * @return int
     */
    public int doStartTag() throws JspException {
        try {
            String theDate = "";
            Object objetDate = tagUtils.lookup(pageContext, name,
                                               property, scope);
            if (objetDate != null) {
                Date date = null;
                if (objetDate instanceof Calendar) {
                    date = ((Calendar) objetDate).getTime();
                } else if (objetDate instanceof Date) {
                    date = (Date) objetDate;
                }
                SimpleDateFormat sdf = new SimpleDateFormat(this.format);
                if (date != null) {
                  theDate = sdf.format(date);
                  pageContext.getOut().print(theDate);
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    public void release() {
        property = null;
        format = null;
        name = null;
        scope = null;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

    /**
     * @param valeur format de la date.
     */
    public final void setFormat(final String valeur) {
        this.format = valeur;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }

    /**
     * @param valeur emplacement de la variable (request, session, parameter)
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }
}
