package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.commun.ResultList;
import fr.paris.epm.global.coordination.facade.AbstractMapperFacade;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTClausePub;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.coordination.service.ClauseService;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.bean.ClauseSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade de gestion des Clause Niveau Interministeriel
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Service
public class ClauseFacadeImpl extends AbstractMapperFacade<ClauseBean, EpmTClause> implements ClauseFacade {

    private static final Logger LOG = LoggerFactory.getLogger(ClauseFacadeImpl.class);

    private ClauseMapper clauseMapper;

    private ClauseService clauseService;

    public ClauseFacadeImpl() {
        setCritereContructor(ClauseCritere::new);
        setDefaultSortField("dateModification");
        setDefaultIdField("id");
    }

    @Override
    public ClauseService getService() {
        return clauseService;
    }

    @Override
    protected boolean defaultRemoveTest(int id) {
        return false;
    }

    @Override
    public AbstractBeanToEpmObjectMapper<ClauseBean, EpmTClause> getToEpmObjectMapper() {
        return null; // TODO: refaire comme dans les module Administration et Passation après la fusion NOYAU-REDAC
    }

    @Override
    public ClauseBean map(EpmTClause epmTClause, Integer idOrganisme) {
        ClauseBean clauseBean = clauseMapper.toClauseBean(epmTClause);
        if (clauseBean.isClauseEditeur()) {
            clauseBean.setTypeAuteur(2);
            clauseBean.setLabelTypeAuteur("Editeur");
        } else {
            clauseBean.setTypeAuteur(1);
            clauseBean.setLabelTypeAuteur("Client");
        }

        if (epmTClause.getIdLastPublication() != null && epmTClause.getIdLastPublication() != 0) {
            EpmTPublicationClausier publication = clauseService.chercherObject(epmTClause.getIdLastPublication(), EpmTPublicationClausier.class);
            clauseBean.setLastVersion(publication.getVersion());
        } else {
            clauseBean.setLastVersion("NA");
        }
        clauseBean.setReferenceClauseSurchargee(epmTClause.getReferenceClauseSurchargee());
        clauseBean.setIdClauseOrigine(epmTClause.getIdClauseOrigine());
        clauseBean.setSurchargeActif(epmTClause.getSurchargeActif());
        clauseBean.setContext(makeContext(epmTClause));
        clauseBean.setContextHtml(makeContextHtml(epmTClause));
        return clauseBean;
    }

    @Override
    public ClauseBean map(EpmTClause epmTClause, EpmTRefOrganisme epmTRefOrganisme) {
        ClauseBean clauseBean = clauseMapper.toClauseBean(epmTClause);
        EpmTClause epmTClauseEditeurInitiale = null;
        if (!epmTClause.isClauseEditeur()) {
            clauseBean.setTypeAuteur(1);
            clauseBean.setLabelTypeAuteur("Client");
        } else {

            EpmTClause clauseSurcharge = clauseService.chercherClauseSurcharge(epmTClause.getId(), epmTRefOrganisme);
            if (clauseSurcharge != null && Boolean.TRUE.equals(clauseSurcharge.getSurchargeActif())) {
                clauseBean.setTypeAuteur(3);
                clauseBean.setLabelTypeAuteur("Editeur Surchargé");
            } else {
                clauseBean.setTypeAuteur(2);
                clauseBean.setLabelTypeAuteur("Editeur");
            }
        }

        if (epmTClause.getIdLastPublication() != null && epmTClause.getIdLastPublication() != 0) {
            EpmTPublicationClausier publication = clauseService.chercherObject(epmTClause.getIdLastPublication(), EpmTPublicationClausier.class);
            clauseBean.setLastVersion(publication.getVersion());
        } else {
            clauseBean.setLastVersion("NA");
        }

        if (epmTClauseEditeurInitiale != null) {
            clauseBean.setReferenceClauseSurchargee(epmTClauseEditeurInitiale.getReferenceClauseSurchargee());
            clauseBean.setSurchargeActif(epmTClauseEditeurInitiale.getSurchargeActif());
        }
        clauseBean.setContext(makeContext(epmTClause));
        clauseBean.setContextHtml(makeContextHtml(epmTClause));
        return clauseBean;
    }

    @Override
    public EpmTClause map(ClauseBean clauseBean,EpmTRefOrganisme epmTRefOrganisme) {
        EpmTClause epmTClause;
        if (clauseBean.getIdClause() == 0) {
            epmTClause = clauseMapper.toEpmTClause(clauseBean);
        } else {
            epmTClause = safelyUniqueFind("id", clauseBean.getIdClause());
            clauseMapper.toEpmTClause(clauseBean, epmTClause);
        }
        return epmTClause;
    }

    @Override
    public List<ClauseBean> map(Collection<EpmTClause> epmTObjects, EpmTRefOrganisme epmTRefOrganisme) {
        return super.map(epmTObjects, epmTRefOrganisme);
    }

    @Override
    public EpmTClause findEntityById(int idClause) {
        return safelyUniqueFind("id", idClause);
    }

    @Override
    public PageRepresentation<ClauseBean> findClauses(ClauseSearch search, EpmTRefOrganisme epmTRefOrganisme) {

        ResultList<ClauseBean, ? extends ClauseFacade, ClauseCritere> resultList = new ResultList<>(this, new ClauseCritere(epmTRefOrganisme.getPlateformeUuid()));

        resultList.critere().setReference(search.getReferenceClause());

        String referenceCanevas = search.getReferenceCanevas();

        if (null != referenceCanevas && !referenceCanevas.isEmpty()) {
            List<Integer> clausesId = clauseService.chercherListeClausesPourCanevasInterministerielle(referenceCanevas)
                    .stream().map(EpmTClause::getId).distinct().collect(Collectors.toList());

            LOG.info("Id des clauses pour le canevas {} = {}", referenceCanevas, clausesId);

            resultList.critere().setListIds(clausesId.isEmpty() ? Collections.singletonList(0) : clausesId);
        } else {
            resultList.critere().setListIds(null);
        }

        resultList.critere().setNaturePrestation(search.getIdNaturePrestation());
        resultList.critere().setMotscles(search.getMotsCles());
        resultList.critere().setAuteur(search.getTypeAuteur());
        resultList.critere().setIdStatutRedactionClausier(search.getIdStatutRedactionClausier());
        resultList.critere().setIdTypeDocument(search.getIdTypeDocument());
        resultList.critere().setIdTypeContrat(search.getIdTypeContrat());
        resultList.critere().setIdTypeClause(search.getIdTypeClause());
        resultList.critere().setProcedure(search.getIdProcedure());
        resultList.critere().setTheme(search.getIdThemeClause());
        resultList.critere().setActif(search.getActif());
        resultList.critere().setDateModificationDebut(search.getDateModificationMin());
        resultList.critere().setDateModificationFin(search.getDateModificationMax());


        resultList.critere().setIdOrganisme(epmTRefOrganisme.getId());
        resultList.critere().setEditeur(search.isEditeur());

        resultList.critere().setProprieteTriee(search.getSortField());
        resultList.critere().setTriCroissant(search.isAsc());
        resultList.critere().setNumeroPage(search.getPage());
        resultList.critere().setTaillePage(search.getSize());
        resultList.critere().setChercherNombreResultatTotal(true);

        ResultList<ClauseBean, ? extends ClauseFacade, ClauseCritere> clausesBeans = resultList.find(epmTRefOrganisme);

        PageRepresentation<ClauseBean> representation = new PageRepresentation<>();
        representation.setContent(clausesBeans.getListResults());
        representation.setFirst(clausesBeans.pageCurrent() == 1);
        representation.setLast(clausesBeans.pageCurrent() == clausesBeans.getCountPages());
        representation.setNumberOfElements(clausesBeans.listResults().size());
        representation.setTotalPages(clausesBeans.countPages());
        representation.setTotalElements(clausesBeans.getCount());
        representation.setSize(search.getSize());
        representation.setNumber(search.getPage());
        return representation;
    }

    @Autowired
    public void setClauseMapper(ClauseMapper clauseMapper) {
        this.clauseMapper = clauseMapper;
    }

    @Autowired
    public void setClauseService(ClauseService clauseService) {
        this.clauseService = clauseService;
    }

}
