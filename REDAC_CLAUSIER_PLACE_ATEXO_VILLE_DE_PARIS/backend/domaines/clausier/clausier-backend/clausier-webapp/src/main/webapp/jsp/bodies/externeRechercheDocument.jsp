<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="redactionTag" prefix="redaction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    .erreur {
        background: #ff000021;
        width: 100%;
        display: block;
        padding: 24px;
        border-radius: 4px;
        color: #ff0000;
        font-weight: bold;
    }
</style>

<c:if test="${!empty(sessionScope['CLIENT']) && !empty(sessionScope['AGENT']) && !empty(sessionScope['CONTEXTE']) && !empty(sessionScope['ACCESS_TOKEN'])}">
    <meta http-equiv="refresh" content="0;<c:out value="${sessionScope['DOCUMENT_URL']}"/>?redirection=documents&client=<c:out value="${sessionScope['CLIENT']}"/>&utilisateur=<c:out value="${sessionScope['AGENT']}"/>&contexte=<c:out value="${sessionScope['CONTEXTE']}"/>&token=<c:out value="${sessionScope['ACCESS_TOKEN']}"/>">
</c:if>
<c:if test="${empty(sessionScope['CLIENT']) || empty(sessionScope['AGENT']) || empty(sessionScope['CONTEXTE']) || empty(sessionScope['ACCESS_TOKEN'])}">
    <span class="erreur"><c:out value="${sessionScope['ERREUR_DOCUMENT']}"/></span>
</c:if>
