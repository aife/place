<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Elaboration et Passation des Marchés</title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
    <link type="text/css" href="<atexo:href href='css/styles.css'/>" rel="stylesheet" />
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link type="text/css" href="<atexo:href href='css/styles-spec.css'/>" rel="stylesheet"/>
</head>

<body onload="popupResize();" id="layoutPopup">
    <div class="popup-small" id="container">
        <div class="content">
            <div class="form-bloc">
                <div class="top"><span class="left"></span><span class="right"></span></div>
                <div class="content">
                    <div class="line">
                        <html:form action="ChoixExtensionFichier">
                            <div class="line">
                                <div class="intitule-left">
                                    <bean:message key="document.choixExtension.titre" />
                                </div>
                                <html:select styleId="extensions" property="extension">
                                    <html:optionsCollection property="extensions" label="libelle" value="id"/>
                                </html:select>
                            </div>
                        </html:form>
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>
        </div>
        <div class="boutons">
            <a href="javascript:window.close();" class="annuler">Annuler</a>
            <a href="javascript:valider(${id});" class="valider">Valider</a>
        </div>
        <div class="breaker"></div>
    </div>

    <script type="text/JavaScript">
        function valider(idDocument) {
            var extension = document.getElementById('extensions').value;
            popUp('documentServlet?id=' + idDocument + '&extension=' + extension,'yes');
        }
    </script>
</body>
</html>