package fr.paris.epm.redaction.metier.objetValeur.document;

import java.io.Serializable;

/**
 * la classe Formulation pour traiter et manipuler des formulations.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class Formulation implements Serializable {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * libellé de la formulation.
     */
    private String libelle;
    /**
     * numéro de la formulation.
     */
    private int numFormulation;
    /**
     * attribut precoche initialisé à faux.
     */
    private boolean precochee = false;
    /**
     * Coorespond à la taille du champ.
     */
    private int nbrCaraMax = 1;

    /**
     * cette méthode renvoie le numéro de formulation.
     * @return renvoyer le numéro de formulation.
     */
    public final int getNumFormulation() {
        return numFormulation;
    }

    /**
     * cette méthode initialise le numéro de formulation avec une valeur donnée.
     * @param valeur pour positionner le numéro de formulation.
     */
    public final void setNumFormulation(final int valeur) {
        numFormulation = valeur;
    }

    /**
     * cette méthode renvoie la taille du champ.
     * @return renvoyer la taille du champ.
     */
    public final int getNbrCaraMax() {
        return nbrCaraMax;
    }

    /**
     * cette méthode initialise la taille du champ avec une valeur donnée.
     * @param valeur pour positionner la taille du champ.
     */
    public final void setNbrCaraMax(final int valeur) {
    	if(valeur != 0){
    		nbrCaraMax = valeur;
    	}
    }

    /**
     * cette méthode renvoie le libellé de la formulation.
     * @return renvoyer le libellé de la formulation.
     */
    public final String getLibelle() {
        return libelle;
    }

    /**
     * cette méthode initialise le libellé de la formulation avec une valeur donnée.
     * @param valeur pour positionner le libellé de la formulation.
     */
    public final void setLibelle(final String valeur) {
        /*Pattern pattern = Pattern.compile("\\s\\&lt;\\w"); // GWT ne maintient pas Pattern et Matcher
        Matcher matcher = pattern.matcher(libelle);
        while (matcher.find()) {
            libelle = libelle.substring(0, matcher.end() - 1) + " " + libelle.substring(matcher.end() - 1);
            matcher = pattern.matcher(libelle);
        }*/
        libelle = valeur
                .replaceAll("\r\n", "<br/>")
                .replaceAll("\n", "<br/>");
    }

    /**
     * cette méthode prévoit si le boutton radio est coché.
     * @return vrai si le button radio est coché, faux si non.
     */
    public final boolean isPrecochee() {
        return precochee;
    }

    /**
     * cette méthode initialise l'attribut precochee avec une valeur donnée.
     * @param valeur pour positionner l'attribut precochee.
     */
    public final void setPrecochee(final boolean valeur) {
        precochee = valeur;
    }

    /**
     * cette méthode permet la conversion de type en String.
     * @return la chaîne de caractère convertie.
     * @see java.lang.Object#toString()
     */
    public final String toString() {
        return getNumFormulation() + "\t" + getNbrCaraMax() + "\t" + getLibelle() + "\t";
    }

    /**
     * @return clone du formulation de la clause.
     */
    public final Formulation clone() {
        Formulation formulation = new Formulation();
        
        formulation.setLibelle(libelle);
        formulation.setNumFormulation(numFormulation);
        formulation.setPrecochee(precochee);
        formulation.setNbrCaraMax(nbrCaraMax);
        
        return formulation;
    }

}
