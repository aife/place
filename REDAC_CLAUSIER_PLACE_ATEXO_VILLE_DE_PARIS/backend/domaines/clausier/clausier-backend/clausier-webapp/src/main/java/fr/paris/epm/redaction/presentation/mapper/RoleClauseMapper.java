package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.redaction.metier.objetValeur.RoleClause;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DirectoryMapper.class, DirectoryNamedMapper.class})
public interface RoleClauseMapper {

    @Mapping(source = "idRoleClause", target = "id")
    @Mapping(target = "epmTClause", ignore = true)
    @Mapping(target = "idUtilisateur", ignore = true)
    @Mapping(target = "idDirectionService", ignore = true)
    EpmTRoleClause cloneEpmTRoleClause(EpmTRoleClauseAbstract epmTRoleClausePub);

    RoleClause toRoleClause(EpmTRoleClauseAbstract epmTRoleClauseAbstract);

}
