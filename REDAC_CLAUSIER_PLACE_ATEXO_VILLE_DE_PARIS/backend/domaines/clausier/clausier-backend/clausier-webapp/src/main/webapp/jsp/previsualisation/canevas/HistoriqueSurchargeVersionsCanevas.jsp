<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="atexo" uri="AtexoTag" %>

<%@ taglib uri="http://struts.apache.org/tags-html"  prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean"  prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><bean:message key="previsualiserClause.txt.elaborationPassationMarche"/></title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css"/>
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
    <script type="text/javascript" language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/editeurTexteRedaction.js"></script>

    <script type="text/javascript" language="javascript">
        window.onload = function() {
            popupResize();
            initEditeursTexteSansToolbarRedaction();
        }
    </script>
</head>

<body id="layoutPopup" >
<%--@elvariable id="canevas" type="fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas"--%>
<div id="container" class="maj-clause">
    <h1>Historique des versions du canevas ${canevas.referenceCanevas}</h1>

    <div class="previsu-doc previsu-doc client bloc-comparaison-version">
        <div class="content">
            <h2>Version</h2>
            <html:form action="historiquePublicationClausierCanevasAction">
                <%--@elvariable id="frmHistoriquePublicationClausierForm" type="fr.paris.epm.redaction.presentation.forms.HistoriquePublicationClausierForm"--%>
                <html:select styleId="selectionVersionClause" property="version" styleClass="selection-version" onchange="javascript:changerVersion();">
                    <html:optionsCollection property="versions" label="version" value="id"/>
                </html:select>
                <html:hidden property="idClauseCanevasEditeur"/>
            </html:form>

            <ul>
                <li>
                    <ul class="liste-clause">
                        <div class="content">
                            <ul>
                                <li>
                                    <logic:present name="versionCanevas">
                                        <bean:define id="versionChapitres" name="versionCanevas" property="chapitres" toScope="request" />
                                        <bean:define id="premierNiveau" value="true" toScope="request" />
                                        <jsp:include page="/jsp/previsualisation/canevas/VersionChapitre.jsp" />
                                    </logic:present>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="previsu-doc previsu-doc editeur bloc-comparaison-version">
        <div class="content">
            <h2>VERSION <c:out value="${canevas.lastVersion}"/></h2>
            <ul>
                <li>
                    <ul class="liste-clause">
                        <div class="content">
                            <ul>
                                <li>
                                    <bean:define id="chapitres" name="canevas" property="chapitres" toScope="request" />
                                    <bean:define id="premierNiveau" value="true" toScope="request" />
                                    <jsp:include page="/jsp/previsualisation/canevas/Chapitre.jsp" />
                                </li>
                            </ul>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="spacer"></div>

    <div class="boutons">
        <a href="javascript:window.close();" class="valider"> Fermer </a>
    </div>

</div>
<script type="text/javascript">
    new Draggable('container', {
        revert : false,
        ghosting : false
    });
    autoPositionLayer();
</script>

<script type="text/javascript">
    function changerVersion() {
        document.forms[0].submit();
    }
</script>


</body>
</html>
