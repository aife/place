package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.bean.Directory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade de gestion des Annuaires du Module Rédaction
 *          - statuts redaction clausiers
 *          - type d'auteur clause : Editeur, Client, Editeur surchargée
 *          - type d'auteur canevas : Editeur, Client
 *          - type clause
 *          - type document
 *          - type contrat
 *          - procedure d'editeur
 *          - thèmes
 * Created by nty on 01/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Service
public class DirectoryFacadeImpl extends fr.paris.epm.global.coordination.facade.DirectoryFacadeImpl implements DirectoryFacade {

    private static final Logger logger = LoggerFactory.getLogger(DirectoryFacadeImpl.class);

    @Override
    public List<Directory> findListStatutsRedactionClausier() {
        return getReferentielsService().getAllReferentiels().getRefStatutRedactionClausiers().stream()
                .map(statut -> new Directory(statut.getId(), statut.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListTypesAuteurClause() {
        return getReferentielsService().getAllReferentiels().getRefAuteurs().stream()
                .map(auteur -> new Directory(auteur.getId(), auteur.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListTypesAuteurCanevas() {
        return getReferentielsService().getAllReferentiels().getRefAuteurs().stream()
                .filter(auteur -> auteur.getId() != 3)
                .map(auteur -> new Directory(auteur.getId(), auteur.getLibelle()))
                .collect(Collectors.toList());
    }


    @Override
    public List<Directory> findListTypesClause() {
        return getReferentielsService().getAllReferentiels().getRefTypeClauses().stream()
                .map(typeClause -> new Directory(typeClause.getId(), typeClause.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListTypesDocument() {
        return getReferentielsService().getAllReferentiels().getRefTypeDocuments().stream()
                .map(typeDocument -> new Directory(typeDocument.getId(), typeDocument.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListTypesContrat() {
        return getReferentielsService().getAllReferentiels().getRefTypeContrats().stream()
                .map(typeContrat -> new Directory(typeContrat.getId(), typeContrat.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListThemesClause() {
        return getReferentielsService().getAllReferentiels().getRefThemeClauses().stream()
                .map(themeClause -> new Directory(themeClause.getId(), themeClause.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListReferenceHeritee() {
        return getReferentielsService().getAllReferentiels().getRefValeurTypeClauses().stream()
                .map(valeur -> new Directory(valeur.getId(), valeur.getLibelle()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Directory> findListCritereCondition() {
        return getReferentielsService().getAllReferentiels().getRefPotentiellementConditionnees().stream()
                .map(valeur -> new Directory(valeur.getId(), valeur.getLibelle()))
                .collect(Collectors.toList());
    }

}
