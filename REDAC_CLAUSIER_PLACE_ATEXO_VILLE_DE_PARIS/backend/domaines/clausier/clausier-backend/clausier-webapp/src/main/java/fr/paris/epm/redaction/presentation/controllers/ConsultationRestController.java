package fr.paris.epm.redaction.presentation.controllers;

import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTCpv;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.presentation.views.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/consultations")
public class ConsultationRestController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(ConsultationRestController.class);

	@Resource
	private RedactionServiceSecurise redactionService;
	@Resource
	private ConsultationServiceSecurise consultationServiceSecurise;

	@Value("${mpe.client}")
	private String mpePlateformeUuid;

	@GetMapping(value = "/recuperer")
	public final ResponseEntity<Consultation> recuperer(
			@RequestParam(value = "plateformeUuid", required = false) final String plateformeUuid,
			@RequestParam(value = "consultation") final String refConsultation
	) {
		log.debug("Récupération de la consultation {}", refConsultation);

		final var critereConstultation = new ConsultationContexteCritere();
		critereConstultation.setReference(refConsultation);
		if (null != plateformeUuid) {
			critereConstultation.setPlateforme(plateformeUuid);
		} else {
			critereConstultation.setPlateforme(mpePlateformeUuid);
		}

		final EpmTConsultation consultation = consultationServiceSecurise.chercherConsultation(critereConstultation);

		if (null == consultation) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		final var result = new Consultation();
		result.setObjet(consultation.getObjet());
		result.setMps(consultation.getMarchePublicSimplifie());
		result.setIntitule(consultation.getIntituleConsultation());
		result.setReference(consultation.getNumeroConsultationExterne());
		result.setNumero(consultation.getNumeroConsultation());
		if (null!=consultation.getDateRemisePlis()) {
			result.setDateRemisePlis(consultation.getDateRemisePlis().getTime());
		}

		// Organisme
		var organisme = redactionService.chercherObject(consultation.getIdOrganisme(), EpmTRefOrganisme.class);
		result.setOrganisme(map(organisme));

		// Service
		var service = consultation.getEpmTRefDirectionService();
		result.setService(map(service, result.getOrganisme()));

		// Procedure
		var procedure = consultation.getEpmTRefProcedure();
		result.setProcedure(map(procedure));

		// Lots
		var lots = consultation.getEpmTBudLots();
		result.setLots(lots.stream().map(this::map).collect(Collectors.toSet()));

		return ResponseEntity.ok(result);
	}

	private Lot map(EpmTBudLot epmTBudLot) {
		var result = new Lot();

		result.setId(epmTBudLot.getId());
		result.setNumero(epmTBudLot.getNumeroLot());
		result.setLibelle(epmTBudLot.getIntituleLot());
		result.setDescription(epmTBudLot.getDescriptionSuccinte());

		result.setCpv(epmTBudLot.getCpvs().stream().map(this::map).collect(Collectors.toSet()));

		return result;
	}

	private CPV map(EpmTCpv epmTCpv) {
		var result = new CPV();

		result.setCode(epmTCpv.getCodeCpv());
		result.setLibelle(epmTCpv.getLibelleCpv());

		return result;
	}

	private Procedure map(EpmTRefProcedure procedure) {
		var result = new Procedure();
		result.setLibelle(procedure.getLibelle());
		result.setAcronyme(procedure.getCodeExterne());
		result.setIdentifiantExterne(procedure.getId());
		return result;
	}

	private Organisme map(EpmTRefOrganisme organisme) {
		var result = new Organisme();
		result.setLibelle(organisme.getLibelle());
		result.setAcronyme(organisme.getCodeExterne());
		result.setIdentifiantExterne(organisme.getId());
		return result;
	}

	private Service map(EpmTRefDirectionService service, Organisme organisme) {
		var result = new Service();
		result.setLibelle(service.getLibelle());
		result.setOrganisme(organisme);
		result.setIdentifiantExterne(service.getId());
		result.setCodeExterne(result.getCodeExterne());
		if (null!=service.getParent()) {
			result.setParent(this.map(service.getParent(), organisme));
		}
		return result;
	}
}
