package fr.paris.epm.redaction.webservice.dto;

import java.util.Date;

public class AuditDTO {

	private UtilisateurDTO utilisateur;
	private Date date;

	public UtilisateurDTO getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(final UtilisateurDTO utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(final Date date) {
		this.date = date;
	}
}
