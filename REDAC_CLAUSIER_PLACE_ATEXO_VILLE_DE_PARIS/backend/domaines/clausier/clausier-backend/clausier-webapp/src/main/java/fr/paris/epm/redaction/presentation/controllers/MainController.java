package fr.paris.epm.redaction.presentation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

/**
 * MainController
 * Created by nty on 25/09/19.
 * @author Nikolay Tyurin
 * @author nty
 */
@Controller
public class MainController extends fr.paris.epm.global.presentation.controllers.MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

}
