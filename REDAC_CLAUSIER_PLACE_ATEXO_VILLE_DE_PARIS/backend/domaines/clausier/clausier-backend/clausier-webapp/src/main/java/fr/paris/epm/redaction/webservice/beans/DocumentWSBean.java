/**
 * 
 */
package fr.paris.epm.redaction.webservice.beans;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean de réponse du web service Liste des documents
 * @author MGA
 *
 */
@JacksonXmlRootElement(localName ="documents")
public class DocumentWSBean {

	@JacksonXmlElementWrapper(useWrapping = false)
	@JacksonXmlProperty(localName = "document")
    private List<DocumentBean> documents;

    public final void add(final DocumentBean doc) {
    	if (null == this.documents ) {
    		this.documents = new ArrayList<>();
	    }

    	this.documents.add(doc);
    }

	public List<DocumentBean> getDocuments() {
		return documents;
	}

	public void setDocuments( List<DocumentBean> documents ) {
		this.documents = documents;
	}
}
