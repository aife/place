package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.facade.Facade;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierSearch;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface PublicationClausierFacade extends Facade<PublicationClausierBean> {


    PageRepresentation<PublicationClausierBean> findPublications(PublicationClausierSearch search, EpmTRefOrganisme epmTRefOrganisme);

    EpmTPublicationClausier editCommentPublicationClausier(int idPublication, String commentaire);

    List<String> activePublication(int idPublication, EpmTUtilisateur utilisateur);

    EpmTPublicationClausier activeClausierPublication(int idPublication, EpmTUtilisateur utilisateur);

    long downloadPublicationClausier(List<Integer> idsPublications, OutputStream outputStream);

    /**
     * Créer une nouvelle version de la publication clausier
     *
     * @param publicationClausierBean publication clausier bean
     * @return null si elle est bien publiée sinon code d'erreur
     */
    String savePublicationClausier(PublicationClausierBean publicationClausierBean, EpmTUtilisateur utilisateur);

    List<String> importPublicationClausier(InputStream file, EpmTUtilisateur utilisateur);

    String canActivatePublication(EpmTUtilisateur epmTUtilisateur);
}
