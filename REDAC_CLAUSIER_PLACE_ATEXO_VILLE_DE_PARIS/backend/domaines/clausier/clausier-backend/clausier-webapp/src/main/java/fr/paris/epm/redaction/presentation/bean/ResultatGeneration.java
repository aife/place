package fr.paris.epm.redaction.presentation.bean;

import java.util.Date;

public class ResultatGeneration {

	final String xml;
	final Date miseEnCache;

	public ResultatGeneration( String xml) {
		super();
		this.xml = xml;
		this.miseEnCache = new Date();
	}

	public String getXml() {
		return xml;
	}

	public Date getMiseEnCache() {
		return miseEnCache;
	}
}
