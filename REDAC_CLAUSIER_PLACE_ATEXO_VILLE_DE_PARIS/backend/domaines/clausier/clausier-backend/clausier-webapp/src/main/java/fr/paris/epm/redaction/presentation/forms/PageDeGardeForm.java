package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypePageDeGarde;
import fr.paris.epm.redaction.presentation.bean.CategorieDocument;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * formulaire de création de page de garde
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public class PageDeGardeForm extends ActionForm {

    /**
     * Marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Identifiant de la page de garde
     */
    private Integer idPageDeGarde;
    /**
     * Attribut type page de garde.
     */
    private String typePageDeGarde;
    
    /**
     * Défini vers où la redirection sera effectuée
     */
    private String aiguillage;

    /**
     * Ensemble des extensions possibles pour la génération d'un fichier
     */
    private Collection<EpmTRefTypePageDeGarde> typesPageDeGarde;
    
    /**
     * Le titre du document
     */
    private String titreDocument;
    
    /**
     * Le nom du fichier en sortie
     */
    private String nomFichier;

    /**
     * Le nom du fichier en sortie
     */
    private Integer idConsultation;
    
    /**
     * 
     */
    private String action;

	/**
	 * Extension
	 */
	private String extension;

	/**
	 * Type de document
	 */
	private String type = "PG";

	/**
	 * Mode
	 */
	private String mode;

	/**
	 * Mode
	 */
	private String classname;

	/**
	 * Categorie document
	 */
	private String categorieDocument;

	/**
	 * Le fichier
	 */
	private FormFile fichier;

	/**
     * Cette methode pour initialiser le formulaire.
     */
    public void reset() {
        typePageDeGarde = null;
        titreDocument = "";
        nomFichier = "";
        idConsultation = null;
        action = "";
        idPageDeGarde = null;
        fichier = null;
    }
    
    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        
        ActionErrors erreurs = new ActionErrors();
        if(typePageDeGarde != null && typePageDeGarde.equals("0")) {
            erreurs.add("typePageDeGarde", new ActionMessage("GenerationPageDeGarde.valider.typePageDeGarde"));
        }
        if(nomFichier.isEmpty()) {
            erreurs.add("nomFichier", new ActionMessage("GenerationPageDeGarde.valider.nomFichier"));
        }
        if(titreDocument.isEmpty()) {
            erreurs.add("titreDocument", new ActionMessage("GenerationPageDeGarde.valider.titreDocument"));
        } else if(titreDocument.length() >= 120) {
        	erreurs.add("titreDocument", new ActionMessage("GenerationPageDeGarde.valider.limiteTitreDocument"));
        }

        CategorieDocument categorie = CategorieDocument.trouver(this.categorieDocument).orElse(CategorieDocument.DCR);

        if (CategorieDocument.DLI == categorie && null == this.fichier) {
        	erreurs.add("fichier", new ActionMessage("GenerationPageDeGarde.valider.fichier"));
        }

        return erreurs;
    }
    
    /**
     * @return the idPageDeGarde
     */
    public final Integer getIdPageDeGarde() {
        return idPageDeGarde;
    }

    /**
     * @param idPageDeGarde the idPageDeGarde to set
     */
    public final void setIdPageDeGarde(final Integer valeur) {
        this.idPageDeGarde = valeur;
    }

    /**
     * @return the typePageDeGarde
     */
    public final String getTypePageDeGarde() {
        return typePageDeGarde;
    }

    /**
     * @param typePageDeGarde the typePageDeGarde to set
     */
    public final void setTypePageDeGarde(final String valeur) {
        this.typePageDeGarde = valeur;
    }

    /**
     * @return the typesPageDeGarde
     */
    public final Collection<EpmTRefTypePageDeGarde> getTypesPageDeGarde() {
        return typesPageDeGarde;
    }

    /**
     * @param typesPageDeGarde the typesPageDeGarde to set
     */
    public final void setTypesPageDeGarde(final Collection<EpmTRefTypePageDeGarde> valeur) {
        this.typesPageDeGarde = valeur;
    }

    /**
     * @return the titreDocument
     */
    public final String getTitreDocument() {
        return titreDocument;
    }

    /**
     * @param titreDocument the titreDocument to set
     */
    public final void setTitreDocument(final String valeur) {
        this.titreDocument = valeur;
    }

    /**
     * @return the nomFichier
     */
    public final String getNomFichier() {
        return nomFichier;
    }

    /**
     * @param nomFichier the nomFichier to set
     */
    public final void setNomFichier(final String valeur) {
        this.nomFichier = valeur;
    }

    /**
     * @return the idConsultation
     */
    public final Integer getIdConsultation() {
        return idConsultation;
    }

    /**
     * @param idConsultation the idConsultation to set
     */
    public final void setIdConsultation(final Integer valeur) {
        this.idConsultation = valeur;
    }

    /**
     * @return the action
     */
    public final String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public final void setAction(final String valeur) {
        this.action = valeur;
    }

    /**
     * @return the aiguillage
     */
    public final String getAiguillage() {
        return aiguillage;
    }

    /**
     * @param aiguillage the aiguillage to set
     */
    public final void setAiguillage(final String valeur) {
        this.aiguillage = valeur;
    }

	public void setExtension( String extension ) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getMode() {
		return mode;
	}

	public void setMode( String mode ) {
		this.mode = mode;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname( String classname ) {
		this.classname = classname;
	}

	public void setCategorieDocument( String categorieDocument ) {
		this.categorieDocument = categorieDocument;
	}

	public String getCategorieDocument() {
		return categorieDocument;
	}

	public FormFile getFichier() {
		return fichier;
	}

	public void setFichier( FormFile fichier ) {
		this.fichier = fichier;
	}
}
