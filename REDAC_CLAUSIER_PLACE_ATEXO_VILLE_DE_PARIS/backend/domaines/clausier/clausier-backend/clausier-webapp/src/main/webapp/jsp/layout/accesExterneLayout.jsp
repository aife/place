<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
  response.setHeader("Cache-Control","no-cache, must-revalidate, proxy-revalidate");
  response.setHeader("Pragma","no-cache"); 
  response.setDateHeader ("Expires", 0); 
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <!-- <%= request.getLocalName() %> -->
        <!-- <%= System.getProperty("hostname") %> -->
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
        <meta http-equiv="Expires" content="0" />
        <tiles:useAttribute name="title"/>
        <title><bean:message name="title" /></title>
        <atexo:featureJson/>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
        <script language="JavaScript" type="text/JavaScript" src="js/calendrier.js"></script>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>

        <script language="JavaScript" src="js/scriptaculous/lib/prototype.js" type="text/javascript"></script>
        <script language="JavaScript" src="js/scriptaculous/src/scriptaculous.js" type="text/javascript"></script>

        <script type = "text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <!-- 		Plugin jQuery pour l'encodage de caractères en base64 -->
        <script type = "text/javascript" src="js/jquery.base64.min.js"></script>

        <script type="text/javascript">
            jQuery.noConflict();
        </script>

        <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
        <link rel="stylesheet" href="<atexo:href href='css/calendrier.css'/>" type="text/css" />
        <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
        <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
        <script type="text/javascript">
            function noBack() {window.history.forward()}
            noBack();
            window.onload=noBack;
            window.onpageshow=function(evt){if(evt.persisted)noBack()};
            window.onunload = function() {void(0)}
        </script>
        <script type="text/javascript">
            hrefRacine = '<atexo:href href=""/>';
            <c:if test="${not empty(sessionScope.prefixeRedirection)}">
                prefixeRedirection = '${sessionScope.prefixeRedirection}';
            </c:if>
        </script>
    </head>

    <body id="iframe-service">
        <tiles:insertAttribute name="body"/>
    </body>

</html>