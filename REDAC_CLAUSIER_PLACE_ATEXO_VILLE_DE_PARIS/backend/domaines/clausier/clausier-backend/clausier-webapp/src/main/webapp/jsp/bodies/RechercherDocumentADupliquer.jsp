<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="redactionTag" prefix="redaction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    .badge-doc-type {
        padding: 3px;
        border-radius: 4px;
        display: inline-block;
        color: #fff;
        height: 22px;
        font-weight: bold;
        font-size: 76%;
        width: 44px;
        text-align: center;
    }

    .badge-doc-type-dcr {
        color: #3D6BA6;
        border: 2px solid;
    }

    .badge-doc-type-pgf {
        color: #3D6BA6;
        border: 2px solid;
    }

    .action-bouton {
        display: inline-block;
        margin: 1px;
    }

    .button-left {
        display: flex;
    }

    .button-left-first {
        margin-right: 2px;
    }

    .tab {
        padding: 12px 24px;
        float: left;
        border-style: solid;
        border-radius: 0;
        border-width: 1px 1px 0 1px;
        font-weight: bold;
        background: #eeeeee;
        border-color: #eeeeee;
        cursor: pointer;
    }

    .tab-separator {
        padding: 0;
    }

    .tab-separator.tab-hidden {
        display: none;
    }

    .tab-separator span {
        background: #c7c7c7;
        height: 25px;
        display: block;
        width: 1px;
        margin: 8px 0;
    }

    .nombre {
        border-radius: 50%;
        padding: 0 4px;
        display: inline-block;
    }

    .nombre::before {
        content: "(";
    }

    .nombre::after {
        content: ")";
    }

    .nombre-pgf, .label-pgf {
        color: #3D6BA6;
    }

    .nombre-dcr, .label-dcr {
        color: #3D6BA6;
    }

    .tab-separator.tab-invisible span {
        background: #eeeeee !important;
    }

    .tab-on {
        background: #e0e0e0;
        border-color: #e0e0e0;
        border-radius: 4px 4px 0 0;
    }

    .tab-lien:not(.tab-on):nth-child(2) {
        border-radius: 4px 0 0 0;
    }

    .tab-lien:not(.tab-on):nth-last-child(2) {
        border-radius: 0 4px 0 0;
    }

    .results-list table th {
        background-color: #e0e0e0 !important;
    }

    .edition-en-ligne {
        width: 110px;
        height: 44px;
        border-radius: 4px;
        color: #99928f !important;
        text-decoration: none !important;
        margin-top: 1px;
        background: #eeeeee url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAALBAMAAABWnBpSAAAYAXpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZppdiQpsoX/s4peAjPYchjPeTt4y+/PwCNSUkpZVd1dqlQofMBxG67da2DW///fNv/iv1xzNjGVmiVny39RovjGH9Xe/+T8djae3/e/+Xy6z8eN7c8Jz6HAZ7hfS3uubxxPv254PcP1z8dNfc74+gz0evIzYNAn+18zkddpf4+7+Awk6/6RpZaPU+3+fo7nwjOV518oZ+j3IPrdfDwQC1aaiauC9yu4YM/vemcQ7r/GEcdvvYb5ckyPWMNHCu6ZCQb59HqvT2s/Guiz8Z+/zFfrz/q98X17rghfbJlfXsvfn3Dpe+MfE394cHjPyH8+IcPJb6/z/Nt71r3XfbsWMxbNT0RZ87KO3sOFRFUM57bMT+Ff4u9yfoSfapsdOGfaQQR2/hbnsfU2Lrrpmttunc/hBlOMfvnCp/fDh3OshuLFj6B+ivrjti9BwgwVvw2/TAgc9u+5uPNcOc8brvLk6bjUOwZTV//4Y/508p/8mL2HmsipMWc9tmJeXiOXaajn9DdX4RC3H7+lY+DXz+N++yGwCFU8mI6ZKy/YbL9D9OR+xVY4fg5cl/i8WeFMmc8AmIhnJyZDCkRnswvJZWeL98U57FhxUGPmPkTf8YBLyU8m6WMI2Zviq9dnc09x51qffPZ6GGzCESnkUPCNhIazYkzET4mVGGoppJhSyqmkapKklkOOOeWcS1aQayWUWFLJpZRapLQaaqyp5lpqrVKbeAlgYJIsRaqItOZN40GNsRrXN45030OPPfXcS69dehuEz4gjjTzKqENGm36GCUzMPMusU2ZbziyQYsWVVl5l1SWrbWJthx132nmXXbfs9vba49Xffv6B19zjNX88pdeVt9c4akp5DeEUTpL6DI/56PB4UQ8Q0F59ZquL0avn1GdWPEmRPJNM6hsznXoMF8blfNru7btfnvtbfjOp/i2/+b/ynFHX/S88Z3Dd7377xmtT69w4HrtZqDa1gexb01fiqDatac3/k8+BLXPZo/ZQZO+DiMbO3MdofYYy0jkKms3FZTxuK0altoNvSxqPpwrmLHvktJhntzqw7zkMBjp/gyH/8DNMWWeCy4aF+UyaBWPsNcLuTKHpZFfJS2eDRzrRMEp3O7WR9ViNu+/VHJMXYIoj3bUwipl7A93nAHPe3c/iSpqElaiT1th2O6m4Ka4egRFG336NoldbwRsTGNmbFPHHFjuHiGF8mfot2UFu1PMAnMvYxF3pvpYcpO8e8dkec1d/rEqwmJ+nu5cLvGGrOGjxEF4/E1XYZedk13CEhKt1r7yHK4ZbZ3tN5UwE99YzR6biPk3/w+zTSNPFO0e717Rm8KxQFmk+6pqvaeCc90QIuZHrjzMnnmsL00gM3ZZNivE0Xt8mvf1Y0n6YjE6FIx8M+WX25rGkW6O5WcNnO6oVmfe5tEY1m49z50yklKlfUy/C0/Oa2Kg3ynxZa4W2y7Gx1jF9Oaa1or0v17orTEUHDzcDxjmzo81tx2naWHkODO4LPkitOMLHzdhWw2S95ElGF3dftWpWxXgG8PbYL6c06lzJ9D3FMzc/uYnnwVbqJloc/Gbk4hIxn1YESTRLb8Tv1Vc+w01bJ+VNuhhAZYRguRpYcrVXil9sJ4sgQ2Dk30QE8/pj53JTmel3COigmDAHUhBrE2hrdI0d/Lk0PuDAZ0Y4g1ciUE1vq7dE4Q0rd826UqWMvkF/fBkT2Irpzt9kzvefru1lXBWmUbazs40eYhVM0xloraGVzLvFE6a9aR0iwdMwDHPUWNKjDONTM8CzTAxUCi7SBBW4XvEtlQDk7edeguF9NxPZm7tBs9jj7KtjcG/WKtWPtnKqLYrM6WPUcp4kdufhHtARW9uoDUjS3COyJPOZ1vlWvW+Mmk1q1BASTAGeyYNHfhCtJE1Y7Rg02sWrkmV2EqzYZFM2KVmVl+bqUhNU2JpMxcTxARybpe1ZgtMQV3xU5qN28F3tolW7tdl7wqpbSsyYpGAUooRpHPf3Eoa8wNhrMQu99T41rcDNlidmIHopycQoyZl4wJcZm/eUgRAINTDlT6blcZEntzVWdzITyZ3DTCNkqhHIi/4ZZwqFwNsmhesTcJrQ2lURvrVEZmDOAL2Lq/DSxH6GWoRayPjQ51rvMLMnzIzGEzedo4W4gTu4G7xYScP3BC8xLAdnSeRRvisYhpmU2BdabUyeudd2HlOuqFnWlY36OXMLeea1lILkFGP2fSITK0hRilqiBrzGmIGpEhO7a4XzGY7ArV3gsjjLd5/RF1Z9wwD2IBbowmdRh2CQrMYecd4xvxuyFd6M6AUSuVgKtVz2BFvq13vMr5sA3xpd14yQtctxLYB6bIGSKT1XbJahMbCVPr5M0PyaIQbVOSqI3kna1yOhL2eiQO55Kp/63AzbA1541krbFIF54DACgPIABChf2eUkxeLhu0r73irHKGAVcTelm7RHX2Qy6dcUVan1LUG3OsyA97PEHAnFixEbU2JdqVIKmGGv8BrIG/RvOmhNajWuOrpkWB4Tm/lWfFhwJ1bI6/sdoaLQzU2UIK5PDmoIpwQu+MuLQcQ0T3DkgVYdJWx46kwQRz8tiWwzIgcWO8duECLgdpS6+gGTA3azjJPiBkDhPrW1jJOF6Eqt3CdZlm+U7japi5145cqM1UkU0ISkq8E1sTBnCqmRsHqS4UVbNQF6TOl2O+bGdGMOvQMrl5lVeC0Q0ckUu21eMgtlkPeD8M5iosN8MBLKEOzWIw4gwD3lnz1FVWZypfihWIPEyl52NW/kUNxQ+Li4gWSu0zbyMO3QCRZPoEKgJ7ikahBCuOMg37vOiug0mliL4MJGkHYtjTX2AiXJ7sCIwskYP8dYUxnh+jSU/lEWIJpAGng7MPWH/PwU/UXZSXnmDUJ6AlMLCFxm7g7p9papEJPKgcHdOmOFF0Q0CYgVUpboBvDmA1Rb4RkeEoL5TJc5Mi4dDZe09Eu5XmS3laqkLV++xlyxbVLS1g2UvgyL4Gha54kYqsYe4kq5tE/snCQ9cTfOdwiyVjiqa5heJiie/aAcmkpgz4HGEqlLe3CUWUp0+FOJxqteH1TQab4OsLx3sxoT9k0r4jiYQVJdlOYV+F9J4w9nSAhBS5UeayEgv4xz0lOv/22c384kisMZRMcw/4tBdAzz3w3i38LNXOVmLyS8xZsjQcENrABqFIipBwzyJNxcp2gho5OPm/jpBNYqBIbh0XARNOtYEW8manvYKT91tBU4KX8mPxxRMyeCmBDNxO+wQx8E+R+QD2einT7vI8ITVRv9NI9nKDjzqSTLoS+QMj+fA22NpuUq6keF6SsKHJoae8z7fVp9BFQLFkO1dL1MjyBGlurLgq6B4u/MQjKmhPanRGNVGze/G0VnIliyZnDMBzE1qsl3UqVHaN6MKQCPySZclmszwPfansJOySB5SSPpmdHhZEwbxg8pBsQpuvAkAOeh04AMVOmdeGD2zjcdNPcIfVQARYHcwV7kGIjGeWo2qfaLamUdHRDoYDkJFXjnivt9olBSBREiqPIrpk4ZeMijpsThYvFNLL+eSTMZcg9jw5Gp6lS1Q8GTKrLiPs79C2gQn29hoL2IRmS/tEGaflekWtDAhSQwcSbqnx5BRFT8UY6Y7050pm/9DYWM9tV6y+8Jwe4CBgKm2qGixIqmDZX/yPWIAKhNtu2hFaUSvJBW1RvcKtWSxZIw0lPK3bUOg4vcpoenODcQ8lxxz59YOefv2Q4l0D4PAU0xbqJQ+wPzNbCKcfotY0DP11T5OXA+dZQKzf0C6QFptCwUGJC8OiuBhP1gFPPJOi/s/67J0T6G38fo46Uxm/kW+TOqpyZKeEL/VGTrhAKJNu0s0OF1UeYlvZZzAdTZZqnKO12jfjJJYFxX/GI1+1jtHomeDO+JOJOu1GeN0WEavlgpAcwOwWG/Z+A77hkVCnn5Lh5U7rpODRHHB9IQdYRelpVgTnGvZjaBTxWbo4502xZxX0EuvOZWnaYvcDsQSHqv5V+025HKLU6zYeBFrkEUt6KrntvF/4fzM7WFMz+y8fPkXlM7E9MYZ2o/TYxpmTOv36elcGK3VovekQ/g1AIOUetxCIyiF6mDoM82NBdWzd0AHjvO39uBr4LCcxToyBmfM7p5QCvxW74lAi0B3sE38Rqs+GBwJht2ieIjUxyq0RxQFZ1S6kEeMfCLVvh0aMXzHWqAvjOEBHjdNSKGLpmU4DvY98Xe7zjLiFMC3lKS2uD9YYgRxZOGCQPBnQuCcoDlFMCaR11YruPXlUkZDA97Zw4LhLoq1jFKc6ivQ4QBV2kGqekgj75Sf3NvLZbbTQN7MOmhXKTYFgsmaHHDm0xAj9xmwkALKtia2nFg3Ko23LIb2sTtAYE2NQVVVwjPbUm1QiQuZoUXYsgVZzi8j2OgTQGz98RxuHkj/lVDSFo5B3Vim6ItpBndhroWDVRPWSGeytbeliOaKjR7uihmJCo6JCz3FBGxKjs8t+X+vosEOfcpOp47S0P8MRFKxiQC8G3uVFosKAl/LWCLYgkQcim6wb9nCoLyQuhGAmsqJkWVQEGjq5IQWBa+Yr6HslzAiRXbQiAEdQXAHyrChDh80OLLSxqd6+l/XicdF2nQHSch16t27WLV0qgBgFrbQhFGELxbYKoAg/nYA7ul7msX7PtGwuc2GLX/3QlLbYacoVEwBk2Y1CkgbgzNFwetssMKaRluJF1ceEXS1tYYzoZjBUnawRkCngRfoq6JITyVH1x7nGy59jhzfxvkoNPu5rHIVouQ7YgjzKFLzFt7EFDyTTQfGvaK9yof4/3MiYg330/0madbQ2vhhJZ2xI+qraGgcyD53qaQzIFh6mWF7kBfkgt9t1kQ/QlDe1Yh/Jmptq5jm1PK4lXxW74zSmLedz73nW7BtzYpIkl74e50Nsg+vLtPuQxhWoP+IoIBi0ESgnsQRMgGBRoGCCjry6WxHIJaWwegrlsqKfeFbn2pC93AyC0qYdwXebrM40b602NuAtapsquIO8hFqKjNqZRyZiHywDYjWPU2VGQ9+KOVRrQhRya32QNlAFGXHWyecC6QZC07qXMaKKMGUR6aGS1AgKMdTjv0Sls7aJnqS/TdRjxcVK7sc82Tzlw3UspKAtDUBHsx67yC9ePp13xw3Mv8g9JE0ecdMHEje9DbSbueHuHhYE91hGqg9KHXXWCgZVJmmhDmXUg3sATijlJGZHRVAv12kWHoCP5D56yFp4QdpS8zU8AHuqq41EvaJQmtQZQET6JbrTLuhxOk/pETiIrIntZdITCiUaEWPZkN2Qu8Sknw+oRCb4DMsIgFipbC9LUZMt1dHz0ewj/moQLHRXe4r8H/in2err2JSA4UIrFE3xik29QVWox2DUaffs6fsug9ECaofStAYw/AhZzRrgNvWrTrJ474ldmpirY0R1HUTiZlPB3tD7KrTGtdN0XwRWt1+cXi4wsozX+6WPAVJM2nxQJe10NF4+kuUM8CbHtElFdCfTptLth3c8Ht21zQbhQmgNZcqMBT2DHYWYOAQSHDzgk6Pw8B1Q0jVwaGdmQgQXqqxvu4+XSC5CtalAkRKAVu9WFIPu3+o2i1/4OevY0X9KymQkMP8pKGJzsIMSVOSEtCEfSABufEsLoqLrpyhKfJNV6+w3kaztUnPGpJ13OGqiMlinZk4CeAPtrr0hLnKZUeNUuwY0TYy4ZqaontR/09jQX032ksoJUM8YEHoPWdjAfUKOBBtT7AVsJdivvDYljTleGozULTs2vE2hzIGAIDemCVbUa0i43gJkBR+MOH6rUF34/6lkfG59OyVh+iINFQZH9HlqLDMd1A8uXk4Rqr71ZWVYH6F3qNrDGo5iN3nXvJXQgJaZ3cnNA3EJGYWiNiODLCa8cEJ8ExRpIBEg1S2zkXjfjTun2AZpUPK1dn4RtZTsTB2cLEjmgmCENXmE0i2MRH4i1IGIZo/hoiT4B0VJroUoj2nsuZjYjTzP2uCWrGkZCu3N4iNiGrsTujlnUXCpUR3nWliUG5ek0lhBhIdIHosb7Zj/G1DU9cX9vDJDtYTNkBFJwrncTvJApc6ZhekZ9nzfrq3vhgnvYN4wJt1PgMEjpdyqm6qM446JI8Grz/hdi6pelBbGWRt+MAP5pVPrdLvujVj02HTwDzueNg3i0Hv9bTpL8a9CrQEydXgS4IP3yHaG4nbhclTetuobzNYnTN4rVwI+Mkke7d0oit6fbYyVjom/KDTNgumUHXDKaihK44nNZ3MHJ7wzU9vWF7e8Mp3ZWRZ2x8d0ZXl9RnZaTBmHLRzQggZaJku61EUZZWXncEzKsnkU9PAhZY+4/zJU+KLoSYAuH36IqF6lrMHYwKmInKRqEXjXmwIJAIyEhHTGakFHCBDum6+4WT6Szymds6VyJDtbqd2DWvGH4v0SgZLP02fvN8tcWBU/g3WOO1awJhZx7koC6MUprybZwsXTpvrisL04okDc9vXdlISD2CdYF7ywOVjO+UpZg5dF1H9xvBVEjHWyT/bGTQD2WgjdLSS6SYRQaKKyjF0WUGAVb/vC3mrC05LQ+YB/xEkxI6vnOv6QVqkkBfV52iQNPFpbnE56bSOoFCqqYJp2ILkXTb+O9Kexo96l/KEXWOiJtQlNnBGQIXBiK6cxCpaLWtC8QHJBhyzNUuUI+6VnLttNDO4l5Sr2mn9NapeTmku+2Gn30J4xZdc98IxNM5GW2AR/tnNQ9ycE1YXrsRnkiDvgrfJAvzi9iDGFIKCdyrsb1unnDx2dSxyl2FdA/AaA3WecK94NseYot2rdnj5Kz7tOCV8N5tvgMIQkE7mlmXq54FmEAqgEpnAQaEAa6WVC5iels3OWXDK6u57XadpF59NOVF4P169jJQLNoLyL4BtBeemf8W0F54Zr7tobY/gVxs9/TnPpz51YiLssZZrJ+FSOzpWSfXjrZuSajKtSP8AlecglQtFVFXNJcfp8UKiSQB9sOXm/W62FgybkdUjaa7gKp2iwrTuvgf8xv+7z4BqplZBG048aPqFUaLl7VZsnQvBmJk3H0Z634CSTPCx2pSwOXp4SwoRPtsZaBEthr+Qsy0BLTrBff0XVrV0+ckJXsOOBZv38rIe8ZrcfTST9tWvt9mYP7ZPoOftxk8m3SmXeHX3rn+XiHJ66yQaMm5m4QSgHJPfz45Dec8glfNG9It7R/a/sDRDFCo5VPQrgrQ62PVNQqOUAqW1xbt2aXx2jnz8O6DpzxTRVv3Tl0DL4GIMq6OiHAJBLfXTR9UmKEdHISq8wYisYqbQ9V9/6Yl+WWdq+Wqu8ZwvwzeEpQB3RbKzKgiiI0shtHqfgVfIDFUD+1+1qEryL7BjkEpbudh9ofNOszIw2WKSl7iCSEKO0u61PuIjd9e+yjcEVyWovoWzr0dpcAwG2R28JTYiszF85n0PY1YRiN9G+UHrjT1fd1dNCCH031hGKWrIav7N3WpWNAOhnFKcD5rht8x3fjjGV3OYA64B4MH6h0A6MZa0weoa8fq2SMngxuC0yAI6BtPffmKc/pp/gB8hLgyXaJe4C/MA30ddJX4IBX1RREXob90CdH8Yccc5FYbATjN6wo4egraOaNqFd3rNHQt/L2hz3zZ0fdsV1Tg0o7Ellu0u9Zr6JluPtAWPLi7b3vPE91noM/jft0oKAspHPU+6nBEyiHr15yuQq/yUNItPjh+kyIEWFSE1No1734+d9av0lm/AqB5k6gkAGtQwCUiKvzKVfscxPc4yy+6SUfrukboDmc7GZEs99xpj92Tunct3CNc0Iu2muMnfDdvgH+3TfxMCk5L/e2R4jvoHpMQsja6EEtBxV/EThFGWx6rEEdyn2x1Feu0RmAwIUgsMRbd2+Wn8lcYGif1VfJhGMcUyR1cul6b8iLC+Eg3OVLwOogF2UBXaZTkIp2ZLG2mBd1CAD2FH1BoyoeNkk8j6tkpqfskc4c4nOiuwQFtNeo2kBCzdgFFbjX+fSOvOTt5Mxqhg11am+FxuJncBzty1BUCjMTTrSZkOBs3dHdG141pXvcGQgHBfwNe6U5vL4pYcksFsOL/ag/k16JinqryWx4mbV8rIdMiGZ4i2W+NC2cJH6zTjT8hENw2GMIRGZju7o7Pezt0Z4fuKJGzQJG+ZQXvB5tfM/jTTlWdwdbgLJJgCgNWk7xcO0xdtvk10N/81Foq5t9asOZqLlb1PgAAAYVpQ0NQSUNDIHByb2ZpbGUAAHicfZE9SMNQFIVPU0XRioMdpDhkqE4WREV00yoUoUKoFVp1MHnpHzRpSFJcHAXXgoM/i1UHF2ddHVwFQfAHxMnRSdFFSrwvKbSI8cLjfZx3z+G9+wChXmaa1TEGaLptphJxMZNdFbte0YsAQphBRGaWMSdJSfjW1z31Ut3FeJZ/35/Vp+YsBgRE4llmmDbxBvHUpm1w3icOs6KsEp8Tj5p0QeJHrisev3EuuCzwzLCZTs0Th4nFQhsrbcyKpkY8SRxVNZ3yhYzHKuctzlq5ypr35C8M5fSVZa7TGkICi1iCBBEKqiihDBsx2nVSLKToPO7jj7h+iVwKuUpg5FhABRpk1w/+B79na+Unxr2kUBzofHGcj2Ggaxdo1Bzn+9hxGidA8Bm40lv+Sh2Y/iS91tKiR0D/NnBx3dKUPeByBxh8MmRTdqUgLSGfB97P6JuywMAt0LPmza15jtMHIE2zSt4AB4fASIGy133e3d0+t397mvP7AXVecqhwOoxLAAAAFVBMVEVvcm0Ab2y1tbX///+0tLTe3t62trY/UFM9AAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfkCwILCjPVsmzTAAAAMUlEQVQI1yWKwQ0AQAjCeDiYj+4/0x3VEJNCA6S3xjfC74crly3PX85zFwWdUAyVkwdxtwhRQjUpfgAAAABJRU5ErkJggg==) no-repeat 8px 16px;
        padding: 0 0 0 16px;
        align-items: center;
        display: flex !important;
        text-align: center;
        justify-content: center;
        border: 1px solid #cccccc;
    }

    .edition-en-ligne:not(.inactif):hover {
        text-decoration: none;
        background: #3d6ba6 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAALBAMAAABWnBpSAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9bRSmVDmYQcchQnSz4hThqFYpQIdQKrTqYXPoFTRqSFBdHwbXg4Mdi1cHFWVcHV0EQ/ABxcnRSdJES/5cUWsR4cNyPd/ced++AYKPCNKtrDNB020wnE2I2tyr2vCKMKASMIywzy5iTpBR8x9c9Any9i/Ms/3N/jj41bzEgIBLPMsO0iTeIpzdtg/M+scBKskp8Tjxq0gWJH7muePzGuehykGcKZiY9TywQi8UOVjqYlUyNeIo4pmo65QezHquctzhrlRpr3ZO/MJLXV5a5TnMISSxiCRJEKKihjApsxGnVSbGQpv2Ej3/Q9UvkUshVBiPHAqrQILt+8D/43a1VmJzwkiIJoPvFcT6GgZ5doFl3nO9jx2meAKFn4Epv+6sNYOaT9Hpbix0B0W3g4rqtKXvA5Q4w8GTIpuxKIZrBQgF4P6NvygH9t0B4zeuttY/TByBDXaVugINDYKRI2es+7+7t7O3fM63+fgBuQ3KlmXxYhQAAABJQTFRFAG9stbW1////tLS03t7etra2u+2KiwAAAAF0Uk5TAEDm2GYAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5AoPFDIApY2kHgAAADBJREFUCNclioEJADAIw4qXSP4/cmuUIqRNgPTW+Eb4/XDlsuX5y3nuoqATiqFy8gC5AAQpr5tCWwAAAABJRU5ErkJggg==) no-repeat 8px 16px;
        color: #ffffff !important;
    }

    .edition-en-ligne.inactif {
        opacity: 0.4;
        cursor: default !important;
    }

    .results-list tr .liste-docs .layer-docs table tr.on {
        background-color: #e0e0e0a1 !important;
    }

    .documents {
        display: none;
    }

    .icone {
        font-size: 10px;
        margin-right: 4px;
    }

    #documents-pgf .lien-nom-fichier, .document-pgf .lien-nom-fichier {
        color: #3D6BA6 !important;
    }

    #documents-dcr .lien-nom-fichier, .document-dcr .lien-nom-fichier {
        color: #3D6BA6 !important;
    }

    .results-list .liste-docs {
        padding-left: 10px;
        border-left: 2px solid #ebebeb;
        border-bottom: 2px solid #ebebeb;
        margin-bottom: 4px !important;
        margin-left: 6%;
        width: 94%;
    }

    .inactive {
        background: #F6F4F4 !important;
    }

    .active {
        background: #e0e0e0a1 !important;
    }

    .documents {
        display: flex;
        flex-direction: column;
    }

    .boutons-action {
        background: #e0e0e0;
        padding: 8px 6px;
    }
</style>


<script>
	let etats = {
		edition: { title: 'En cours d\'édition', image : 'data:image/gif;base64,R0lGODlhJwALAIQRAP2ZSPvPqv////7Ckf7Jnf7Wtv7kzv2nYP7dwv/48/2gVP2tbf/r2v67hf/x5/60ef7PqpRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkJRrkCH5BAEKAB8ALAAAAAAnAAsAAAVmYCAGH2CeaKquwDe+JCvPpguLpdoIPG8+DEECIbPdcqkGI3VIQBQLR4FlhCFRyhRkaVosqDfcatdbFoi0Vjim454ghtMDAg5fT1lU8/mQ1o9jPTwPAA0OQmgrVS93aWmLI42OM1UhADs=' },
		disponible: { title: 'Disponible pour l\'édition en ligne', image : 'data:image/gif;base64,R0lGODlhJwALAOMMAEXQbLb9yvP89ajpumrZiYPgnbXsxHfdk+b564/jp8Hvztr24pRrkJRrkJRrkJRrkCH5BAEKAA8ALAAAAAAnAAsAAARUMMjwJgU46807sNX0eGSJgaBHCAKiEMDKCoeHiuoSGzqhmzfJqOPLKBJFoChF/AEMAxmr1glempkF0kmyDjlFAi/GtS1xxNkrNqOZhUyTXOOd2zERADs=' },
		enregistrement: { title: 'En cours d\'enregistrement',  image : 'data:image/gif;base64,R0lGODlhJwALAOd3AAAAAAEBAQICAgMDAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgYGBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLCwsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///yH+EUNyZWF0ZWQgd2l0aCBHSU1QACH5BAEKAP4ALAAAAAAnAAsAAAhrACUJHEiwoMGDkvwZW2hMIUOEECMKdLiQojGEtf5p/MdLUsaNHQ9atIhxWcFaJiWOZHjxIMqTKSOufOhy479THm3iFMmSpMuYAl+q7MmyJEyJCYnSNPhRo8mm/4ASnLkQqdWpSqtevUrVWEAAOw==' },
		erreur: { title: 'Erreur lors de l\'enregistrement',  image : 'data:image/gif;base64,R0lGODlhJwALAOMNAP9RUfyysv////+Xl/9/f/+5uf/z8/+urv+iov/o6P9oaP90dP9dXZRrkJRrkJRrkCH5BAEKAA8ALAAAAAAnAAsAAARVEMhJq73ghR00x2AoeRsZYIigCkeqGghommhBITaAJDLX+bVbbtfj0C4uFSFpGBRLQGROggMEeM/fRyqUJKaWWdSSFBSqAIWBgBFvRXCKexOvj3ymCAA7' }
	}
</script>

<div class="main-part">
    <div class="breadcrumbs"><bean:message key="DupliquerDocument.titre"/>&nbsp;(type: ${categorie})</div>

    <div class="breaker"></div>
    <c:if test="${canevasInactif != null || procedurePassationNaturePrestationsIncompatibles != null || possibiliteDuplication != null ||
                  entiteAdjudicatriceErreur != null || ccagReferenceIncompatibles != null}">
        <div class="form-bloc-erreur msg-erreur">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <c:if test="${canevasInactif != null}">
                    <div class="message-left">
                        <bean:message key="RechercherDocument.txt.erreurCanevasInactif.textAvant"/>
                            ${canevasInactif}
                        <bean:message key="RechercherDocument.txt.erreurCanevasInactif.textApres"/>
                    </div>
                </c:if>
                <c:if test="${procedurePassationNaturePrestationsIncompatibles != null}">
                    <div class="message-left">
                        <bean:message key="RechercherDocument.txt.procedurePassationNaturePrestationsIncompatibles.textAvant"/>
                            ${numeroConsultation}
                        <bean:message key="RechercherDocument.txt.procedurePassationNaturePrestationsIncompatibles.textApres"/>
                    </div>
                </c:if>
                <c:if test="${possibiliteDuplication != null}">
                    <div class="message-left">
                        <bean:message key="RechercherDocument.txt.caractereAllotiIncompatibles.textAvant"/>
                            ${numeroConsultation}
                        <bean:message key="RechercherDocument.txt.caractereAllotiIncompatibles.textApres"/>
                    </div>
                </c:if>
                <c:if test="${entiteAdjudicatriceErreur != null}">
                    <div class="message-left">
                        <bean:message key="RechercherDocument.txt.entiteAdjudicatrice.textAvant"/>
                            ${numeroConsultation}
                        <bean:message key="RechercherDocument.txt.entiteAdjudicatrice.textApres"/>
                    </div>
                </c:if>
                <c:if test="${ccagReferenceIncompatibles != null}">
                    <div class="message-left">
                        <bean:message key="RechercherDocument.txt.ccagReferenceIncompatibles.textAvant"/>
                            ${numeroConsultation}
                        <bean:message key="RechercherDocument.txt.ccagReferenceIncompatibles.textApres"/>
                    </div>
                </c:if>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
    </c:if>

    <jsp:include page="/jsp/bodies/resumeConsultation.jsp"></jsp:include>

    <div class="spacer"></div>

    <%-- <atexo:fichePratique reference="" key="common.fichePratique"/> --%>
    <div class="breaker"></div>
    <!--Debut bloc Recherche document-->
    <html:form action="/RechercherDocumentADupliquerProcess.epm?dupliquer=true">
        <input type="hidden" name="typeAction" id="typeAction"/>
        <div class="form-bloc">
            <div class="top"><span class="left"></span><span class="right"></span>
            </div>
            <div class="content">
                <div class="column-long">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.numConsultation"/> </span>
                    <html:text property="numConsultation" styleId="numConsultation" title="Num&eacute;ro de consultation"/>
                </div>
                <div class="breaker"></div>
                <div class="line">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.pouvoirAdjudicateur"/> </span>
                    <html:select property="pouvoirAdjudicateur" styleId="pouvoirAdjudicateur" title="Pouvoir Adjudicateur" styleClass="width-570">
                        <html:optionsCollection property="pouvoirAdjudicateurCollection" value="id" label="libelle"/>
                    </html:select></div>
                <div class="breaker"></div>
                <div class="line">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.procedurePassation"/> </span>
                    <html:select property="procedurePassation" styleId="procedurePassation" title="Procédure de passation" styleClass="width-570">
                        <option value="0"><bean:message key="redaction.txt.Toutes"/></option>
                        <html:optionsCollection property="procedureCollection" label="libelle" value="id"/>
                    </html:select>
                </div>
                <div class="line">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.naturePrestation"/> </span>
                    <html:select property="naturePrestas" styleId="naturePrestas" title="Nature des prestations" styleClass="width-200">
                        <html:optionsCollection property="naturePrestasCollection" label="libelle" value="id"/>
                    </html:select></div>
                <div class="line">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.directionService"/> </span>
                    <html:select property="directionService" styleClass="width-570" title="Direction / Service(s)" styleId="directionService">
                        <html:optionsCollection property="directionServiceCollection" label="libelle" value="id"/>
                    </html:select>
                </div>
                <div class="line">
                    <span class="intitule"><bean:message key="RechercherDocument.txt.motCle"/> </span>
                    <html:text property="motCle" styleId="motCle" title="Mots-cl&eacute;s" styleClass="width-570"/>
                </div>
                <div class="breaker"></div>
                <a href="javascript:initFormRecherche();" class="init-recherche">
                    <bean:message key="RechercherDocument.btn.effacerCritereRecherche"/>
                </a>
                <a href="javascript:submitForm();" class="rechercher">
                    <bean:message key="RechercherDocument.btn.rechercher"/>
                </a>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span>
            </div>
        </div>
    </html:form>
    <!--Fin bloc Recherche document-->

    <!--Debut partitioner-->
    <atexo:tableauPagine name="frmRechercherDocument" property="listeDocument" url="documentDupliqueListe.epm"
                         elementAfficher="10,20,30,40" id="listeDocument">
        <div class="spacer"></div>
        <div class="line">
            <h2>
                <logic:notEmpty name="listeDocument">
                    <bean:message key="redaction.txt.resultats"/>
                    <c:out value="${requestScope.NB_RESULT}"/>
                </logic:notEmpty> <logic:empty name="listeDocument">
                <bean:message key="redaction.txt.aucun"/>
            </logic:empty>
            </h2>
            <div class="partitioner">
                <div class="intitule"><bean:message key="redaction.txt.afficher"/>
                </div>
                <atexo:intervalle/>
                <div class="intitule2"><bean:message key="redaction.txt.resultatParPage"/> &nbsp;&nbsp;
                    <atexo:navigationListe suivant="false" key="redaction.txt.precedent"/> &nbsp;
                    <atexo:compteurPage/> &nbsp;
                    <atexo:navigationListe suivant="true" key="redaction.txt.suivant"/>
                </div>
            </div>
        </div>
        <!--Fin partitioner-->

        <!--Debut Resultats Recherche documents-->
        <div class="results-list" id="results-list">
            <table>
                <thead>
                <tr>
                    <th colspan="6" class="top">
                        <img src="<atexo:href href='images/table-results-top-left.gif'/>" alt="" title="" class="left"/>
                        <img src="<atexo:href href='images/table-results-top-right.gif'/>" alt="" title="" class="right"/>
                    </th>
                </tr>
                <tr>
                    <atexo:href href="" var="hrefRacine">
                        <th class="reference">
                            <div>
                                <bean:message key="RechercherDocument.txt.numConsultation2"/>
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="numeroConsultation"/>
                            </div>
                        </th>
                        <th>&nbsp;</th>

                        <th class="objet-long-420">
                            <div>
                                <bean:message key="rechercheDocument.txt.intituleObjet"/>
                                <a href="">
                                    <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="intituleConsultation"/>
                                </a>
                                <br/>
                                <bean:message key="RechercherDocument.txt.directionService2"/>
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="epmTRefDirectionService.code"/>
                            </div>
                        </th>

                        <th class="col-date-remise">
                            <bean:message key="rechercheConsultation.txt.lancement"/>
                            <br/>
                            <bean:message key="rechercheConsultation.txt.remisePlis"/>
                            <atexo:infoBulle reference="IB-18"/>
                        </th>
                    </atexo:href>
                </tr>
                </thead>

                <!--Debut bloc consultation avec surbrillance-->
                <c:set var="nombreResultatAffiche" scope="page" value="0"/>
                <logic:notEmpty name="listeDocument">
                    <logic:iterate name="listeDocument" id="vConsultation" indexId="compteurConsultation">
                        <redaction:listeDocumentsOfConsultation categorie="${categorie}" inclurePageDeGarde="true" id="documentsOfConsultation" name="vConsultation" info="nbrDocParType"/>
                        <c:set var="nombreResultatAffiche" scope="page" value="${compteurConsultation}"/>
                        <bean:define id="ligneNonPaire">
                            <%=(compteurConsultation.intValue() % 2)%>
                        </bean:define>
                        <logic:equal name="ligneNonPaire" value="0">
                            <c:set var="docFirst">doc-on-first</c:set>
                        </logic:equal>
                        <logic:notEqual name="ligneNonPaire" value="0">
                            <c:set var="docFirst">doc-off-first</c:set>
                        </logic:notEqual>
                        <tr class="${docFirst}">
                            <td id="tableauResultatsPresent" class="reference-200">
                                <div>
                                    <span class="ref-cons"><c:out value="${vConsultation.numeroConsultation}"/></span>
                                    <div class="spacer-mini"></div>
                                    <c:out value="${vConsultation.epmTUtilisateur.prenom} ${vConsultation.epmTUtilisateur.nom}"/>
                                    <div class="spacer-mini"></div>
                                    <abbr title="${vConsultation.epmTRefProcedure.libelle}">${vConsultation.epmTRefProcedure.libelleCourt}</abbr>
                                </div>
                                <div class="etape-procedure">
                                    <c:choose>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 1}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/procedure-statut-1.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 2}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/procedure-statut-2.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 3}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/procedure-statut-3.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 4}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/procedure-statut-4.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 5}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/procedure-statut-5.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 6}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/picto-execution.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 7}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/picto-clos.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 8}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/picto-sans-suite.gif"/>">
                                        </c:when>
                                        <c:when test="${vConsultation.epmTRefStatut.id == 9}">
                                            <img title="${recherche.epmTRefStatut.libelle}" alt="${recherche.epmTRefStatut.libelle}" src="<atexo:href href="images/picto-infructueux.gif"/>">
                                        </c:when>
                                    </c:choose>
                                </div>
                            </td>
                            <td>
                                <c:if test="${empty accesModeSaaS}">
                                    <a href="<atexo:propriete clef="contexte.passation" />/entreePassation.epm?idConsultation=${vConsultation.idConsultation}&idLotDissocie=${vConsultation.idLot}">
                                        <img id='consultation-${vConsultation.numeroConsultation}'
                                             title='<bean:message key="RechercherDocument.txt.passation"/>${vConsultation.numeroConsultation}'
                                             alt="Fiche synthétique" src="<atexo:href href='images/bouton-acces-detail.gif'/>" />
                                    </a>
                                </c:if>
                            </td>
                            <td class="objet-long-420">
                                <div>
                                    <span class="ref-cons"><c:out value="${vConsultation.intituleConsultation}" /></span>
                                    <div class="spacer-mini"></div>
                                    <strong><bean:message key="RechercherDocument.txt.objet"/> : </strong>
                                    <atexo:couperTexte name="vConsultation" property="objet" max="190" etc="true"/>
                                    <div class="spacer-mini"></div>
                                    <atexo:insererEspace name="vConsultation" tailleMot="50" property="epmTRefDirectionService.libelle"/>
                                </div>
                            </td>

                            <c:set var="remiseOffresOK" value="${vConsultation.dateReceptionOffrePrevue != null || vConsultation.dateReceptionOffreHerite != null}"/>
                            <c:set var="lancementOK" value="${vConsultation.dateLancementPrevue != null || vConsultation.dateLancementHerite != null}"/>
                            <c:set var="remiseCandOK" value="${vConsultation.dateCandidaturePrevue != null || vConsultation.dateCandidatureHerite != null}"/>

                            <td class="col-date-remise">
                                <div>
                                    <c:if test="${lancementOK}">
                                        <c:if test="${vConsultation.dateLancementPrevue != null}">
                                            <c:set var="dateLancement" value="${vConsultation.dateLancementPrevue}"/>
                                        </c:if>
                                        <c:if test="${vConsultation.dateLancementHerite != null}">
                                            <c:set var="dateLancement" value="${vConsultation.dateLancementHerite}"/>
                                        </c:if>
                                        <div class="libelle-date">
                                            <abbr title="<bean:message key='rechercheConsultation.lancement'/>"><bean:message key='rechercheConsultation.lancement.petit'/></abbr> :
                                        </div>
                                        <span class="date">
                                            <bean:write name="dateLancement" format="dd/MM/yyyy"/>
                                        </span>
                                    </c:if>
                                    <div class="spacer-mini"></div>
                                    <c:if test="${not vConsultation.epmTRefProcedure.deuxPhases}">
                                        <c:if test="${remiseCandOK}">
                                            <c:if test="${vConsultation.dateCandidaturePrevue != null}">
                                                <c:set var="dateCandidature" value="${vConsultation.dateCandidaturePrevue}"/>
                                            </c:if>
                                            <c:if test="${vConsultation.dateCandidatureHerite != null}">
                                                <c:set var="dateCandidature" value="${vConsultation.dateCandidatureHerite}"/>
                                            </c:if>
                                            <div class="libelle-date">
                                                <abbr title="<bean:message key="rechercheConsultation.remisePlisCandidature"/>">
                                                    <bean:message key="rechercheConsultation.remisePlisCandidature.petit"/>
                                                </abbr> :
                                            </div>
                                            <span class="date"><bean:write name="dateCandidature" format="dd/MM/yyyy HH:mm"/></span>
                                        </c:if>
                                        <c:if test="${remiseOffresOK}">
                                            <c:if test="${vConsultation.dateReceptionOffrePrevue != null}">
                                                <c:set var="dateOffre" value="${vConsultation.dateReceptionOffrePrevue}"/>
                                            </c:if>
                                            <c:if test="${vConsultation.dateReceptionOffreHerite != null}">
                                                <c:set var="dateOffre" value="${vConsultation.dateReceptionOffreHerite}"/>
                                            </c:if>
                                            <div class="libelle-date">
                                                <abbr title="<bean:message key="rechercheConsultation.remisePlisOffre"/>">
                                                    <bean:message key="rechercheConsultation.remisePlisOffre.petit"/>
                                                </abbr> :
                                            </div>
                                            <span class="date"><bean:write name="dateOffre" format="dd/MM/yyyy HH:mm"/></span>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${vConsultation.epmTRefProcedure.deuxPhases}">
                                        <c:if test="${remiseCandOK}">
                                            <c:if test="${vConsultation.dateCandidaturePrevue != null}">
                                                <c:set var="dateCandidature" value="${vConsultation.dateCandidaturePrevue}"/>
                                            </c:if>
                                            <c:if test="${vConsultation.dateCandidatureHerite != null}">
                                                <c:set var="dateCandidature" value="${vConsultation.dateCandidatureHerite}"/>
                                            </c:if>
                                            <div class="libelle-date">
                                                <abbr title="<bean:message key="rechercheConsultation.remisePlisCandidature"/>">
                                                    <bean:message key="rechercheConsultation.remisePlisCandidature.petit"/>
                                                </abbr> :
                                            </div>
                                            <span class="date"><bean:write name="dateCandidature" format="dd/MM/yyyy HH:mm"/></span>
                                        </c:if>
                                        <br/>
                                        <c:if test="${remiseOffresOK}">
                                            <c:if test="${vConsultation.dateReceptionOffrePrevue != null}">
                                                <c:set var="dateOffre" value="${vConsultation.dateReceptionOffrePrevue}"/>
                                            </c:if>
                                            <c:if test="${vConsultation.dateReceptionOffreHerite != null}">
                                                <c:set var="dateOffre" value="${vConsultation.dateReceptionOffreHerite}"/>
                                            </c:if>
                                            <div class="libelle-date">
                                                <abbr title="<bean:message key="rechercheConsultation.remisePlisOffre"/>">
                                                    <bean:message key="rechercheConsultation.remisePlisOffre.petit"/>
                                                </abbr> :
                                            </div>
                                            <span class="date"><bean:write name="dateOffre" format="dd/MM/yyyy HH:mm"/></span>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${(not lancementOK) && (not remiseCandOK) && (not remiseOffresOK)}">
                                        <abbr title="<bean:message key='rechercheConsultation.nonDefini'/>">
                                            <bean:message key='rechercheConsultation.nonDefini.petit'/>
                                        </abbr>
                                    </c:if>
                                </div>
                            </td>
                        </tr>
                        <logic:equal name="ligneNonPaire" value="0">
                            <c:set var="doc">doc-on</c:set>
                        </logic:equal>
                        <logic:notEqual name="ligneNonPaire" value="0">
                            <c:set var="doc">doc-off</c:set>
                        </logic:notEqual>
                        <tr class="${doc}">
                            <td colspan="4"><bean:define id="styleTableau">display: none</bean:define>
                                <logic:present parameter="idCons">
                                    <bean:define id="idConsultationEncours"><%=request.getParameter("idCons")%></bean:define>
                                    <c:if test="${idConsultationEncours == vConsultation.idConsultation }">
                                        <bean:define id="styleTableau">display: block</bean:define>
                                    </c:if>
                                </logic:present>
                                <div class="liste-docs">
                                    <div class="title">

                                        <c:set var="nbDocuments" value="0" scope="page" />
                                        <logic:iterate name="documentsOfConsultation" id="documentId">
                                            <c:if test="${categorie == 'pgf'}">
                                                <c:if test="${documentId.getClass().getName() == 'fr.paris.epm.noyau.persistance.redaction.EpmTPageDeGarde'}">
                                                    <c:set var="nbDocuments" value="${nbDocuments + 1}" scope="page"/>
                                                </c:if>
                                            </c:if>

                                            <c:if test="${categorie == 'dcr' || categorie == null}">
                                                <c:if test="${documentId.getClass().getName() == 'fr.paris.epm.noyau.persistance.redaction.EpmTDocumentRedaction'}">
                                                    <c:set var="nbDocuments" value="${nbDocuments + 1}" scope="page"/>
                                                </c:if>
                                            </c:if>


                                        </logic:iterate>

                                        <c:if test="${nbDocuments > 0}">
                                            <a href="javascript:void(0);">
                                                <img src="<logic:notEqual name='styleTableau' value='display: none'><atexo:href href="images/picto-close-panel.gif" /></logic:notEqual><logic:equal name='styleTableau' value='display: none'><atexo:href href="images/picto-open-panel.gif" /></logic:equal>"
                                                     alt="Ouvrir/Fermer" title="Ouvrir/Fermer" onclick="togglePanel(this,'layer-doc-${compteurConsultation}');"/>
                                            </a>
                                        </c:if>

                                        <span class="label-${categorie}">
                                            <c:out value="${nbDocuments}" />&nbsp;<bean:message key="RechercherDocument.txt.documentsConsultation"/> :
                                            <span class="normal"><c:out value="${nbrDocParType}" /></span>
                                        </span>
                                    </div>
                                    <div class="layer-docs" id="layer-doc-${compteurConsultation}" style="${styleTableau}">
                                        <logic:notEmpty name="documentsOfConsultation">
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th class="col-35">
                                                        <div>
                                                            <bean:message key="RechercherDocument.txt.typeDocument"/>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            <bean:message key="RechercherDocument.txt.nomDocument"/> /
                                                            <bean:message key="RechercherDocument.txt.fichier"/> / <bean:message key="RechercherDocument.txt.redacteur"/>
                                                        </div>
                                                    </th>
                                                    <th class="actions col-50"></th>
                                                    <th class="col-90 statut-document">
                                                        <bean:message key="RechercherDocument.txt.statut"/> / <bean:message key="RechercherDocument.txt.progression"/>
                                                    </th>
                                                    <th class="actions col-35">
                                                        <bean:message key="RechercherDocument.txt.actions"/>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <logic:iterate name="documentsOfConsultation" id="documentId" indexId="compteurDoc">
                                                    <bean:define id="ligneNonPaire"><%=(compteurDoc.intValue() % 2)%></bean:define>
                                                    <logic:equal name="ligneNonPaire" value="0">
                                                        <c:set var="ligne">on</c:set>
                                                    </logic:equal>
                                                    <logic:notEqual name="ligneNonPaire" value="0">
                                                        <c:set var="ligne"></c:set>
                                                    </logic:notEqual>

                                                    <c:if test="${documentId.getClass().getName() == 'fr.paris.epm.noyau.persistance.redaction.EpmTPageDeGarde' }">
                                                        <c:if test="${categorie == 'pgf' || categorie == 'daj'}">
                                                            <tr class="${ligne} document-${categorie}">
                                                                <td class="col-35">
                                                                    <span class="badge-doc-type badge-doc-type-${categorie}" title="${documentId.getEpmTRefTypePageDeGarde().getLibelle()}">${documentId.getEpmTRefTypePageDeGarde().getType()}</span>
                                                                    <div class="spacer-mini"></div>
                                                                    <div class="button-left">
                                                                        <div class="button-left-first">
                                                                            <!-- Historique des événements -->
                                                                            <a href="javascript:popUp('historiqueModificationDocument.epm?type=pg&consultation=${vConsultation.idConsultation}&document=${documentId.id}','yes');">
                                                                                <img title="Historique des événements"
                                                                                     alt="Historique des événements"
                                                                                     src="<atexo:href href='images/bouton-historique.gif'/>"/>
                                                                            </a>
                                                                        </div>
                                                                        <!-- prévisualiser -->
                                                                        <a id="view-link-${documentId.id}" target="_blank" href="openDocument.htm?document=${documentId.id}&type=${documentId.getClass().getName()}&consultation=${vConsultation.idConsultation}&mode=view">
                                                                            <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="Prévisualiser le document" title="Prévisualiser le document"/>
                                                                        </a>
                                                                        <div>
                                                                        </div>
                                                                    </div>

                                                                </td>

                                                                <td class="col-200">
                                                                    <div class="strong">${documentId.titre}</div>
                                                                    <div>
                                                                        <a class="lien-nom-fichier" target="_blank" href="openDocument.htm?document=${documentId.id}&type=${documentId.getClass().getName()}&consultation=${vConsultation.idConsultation}&mode=view">
                                                                                ${documentId.nomFichier}.${null != documentId.getExtension() ? documentId.getExtension() : documentId.getEpmTRefTypePageDeGarde().getExtension()}
                                                                        </a>
                                                                    </div>
                                                                    <div>${documentId.auteur}</div>
                                                                </td>
                                                                <td>

                                                                </td>
                                                                <td class="col-120 statut-document" id="badge-statut-container-${documentId.id}">
                                                                    <span id="badge-statut-${documentId.id}"></span>

                                                                    <div class="date">
                                                                        <bean:write name="documentId" property="dateModification" format="dd/MM/yyyy"/>
                                                                        <br>
                                                                        <bean:write name="documentId" property="dateModification" format="HH:mm"/>
                                                                    </div>

                                                                    <script>
																		// Création de l'icone
																		let badge${documentId.id} = document.createElement('img');

																		badge${documentId.id}.id='badge-icon-${documentId.id}';
																		badge${documentId.id}.src = etats.disponible.image;
																		badge${documentId.id}.title = etats.disponible.title;
																		badge${documentId.id}.alt = etats.disponible.title;

																		document.getElementById('badge-statut-container-${documentId.id}').insertBefore(badge${documentId.id}, document.getElementById('badge-statut-${documentId.id}'));
                                                                    </script>
                                                                </td>
                                                                <td class="col-15">
                                                                    <!-- Modifier -->
                                                                    <html:link styleClass="action-bouton" onclick="showLoader()" action="/genererPageDeGarde.epm?id=${documentId.id}&idConsultation=${vConsultation.idConsultation}&mode=maj">
                                                                        <img src="<atexo:href href='images/bouton-parametrer.gif'/>"
                                                                             alt="Modifier le nom du fichier" title="Modifier le nom du fichier"/>
                                                                    </html:link>
                                                                    <!-- Télécharger  -->
                                                                    <a class="action-bouton" href="viewDocument.htm?document=${documentId.id}&type=${documentId.getClass().getName()}&consultation=${vConsultation.idConsultation}">
                                                                        <img title="Télécharger le document" alt="Télécharger le document" src="<atexo:href href='images/bouton-telecharger.gif'/>">
                                                                    </a>
                                                                    <!-- Dupliquer -->
                                                                    <a href="cloneDocument.htm?consultation=${idNouvelleConsultation}&document=${documentId.id}&categorie=${categorie}" class="action-bouton">
                                                                        <img src="<atexo:href href='images/bouton-picto-dupliquer.gif'/>" alt="Dupliquer le document" title="Dupliquer le document" />
                                                                    </a>
                                                                    <!-- Supprimer -->
                                                                    <a class="action-bouton" href="supprimerPageDeGardeProcess.epm?supprimer=true&id=${documentId.id}&categorie=${categorie}">
                                                                        <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"
                                                                             alt="Supprimer le document" title="Supprimer le document"
                                                                             onclick="return confirm('<bean:message key="RechercherDocument.msg.suppression.confirmation"/>');"/>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${documentId.getClass().getName() == 'fr.paris.epm.noyau.persistance.redaction.EpmTDocumentRedaction' }">
                                                        <c:if test="${categorie == 'dcr'}">
                                                            <tr class="${ligne} document-${catagorie}">
                                                                <td class="col-35">
                                                                    <span class="badge-doc-type badge-doc-type-dcr" title="${listCanevasCorrespondantDocs[compteurDoc].epmTRefTypeDocument.description}">${listCanevasCorrespondantDocs[compteurDoc].epmTRefTypeDocument.libelle}</span>
                                                                    <div class="spacer-mini"></div>

                                                                    <div class="button-left">
                                                                        <div class="button-left-first">
                                                                            <!-- Historique des événements -->
                                                                            <a href="javascript:popUp('historiqueModificationDocument.epm?refDoc=${documentId.reference}','yes');">
                                                                                <img title="Historique des évènements" alt="Historique des évènements" src="<atexo:href href='images/bouton-historique.gif'/>">
                                                                            </a>
                                                                        </div>
                                                                        <!-- prévisualiser -->
                                                                        <a href="javascript:popUp('PrevisualiserDocumentExterne.epm?documentId=${documentId.id}','yes')">
                                                                            <img title="Prévisualiser le document" alt="Prévisualiser le document" src="<atexo:href href='images/bouton-previsualiser.gif'/>">
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="strong">${documentId.titre}</div>
                                                                    <div>
                                                                        <a class="lien-nom-fichier" target="_blank" href="javascript:popUp('PrevisualiserDocumentExterne.epm?documentId=${documentId.id}','yes')">
                                                                                ${documentId.nomFichier}.${listCanevasCorrespondantDocs[compteurDoc].epmTRefTypeDocument.extensionFichier}
                                                                        </a>
                                                                    </div>
                                                                    <div>${documentId.auteur}</div>
                                                                </td>
                                                                <td class="actions action-inline col-50">


                                                                </td>
                                                                <td class="col-90 statut-document">
                                                                    <logic:equal name="documentId" property="statut" value="EN COURS"><!-- STATUT : En cours de rédaction -->
                                                                    <img title="En cours de rédaction" alt="En cours de rédaction" src="<atexo:href href='images/document-statut-1.gif'/>">
                                                                    </logic:equal>
                                                                    <logic:equal name="documentId" property="statut" value="DOCUMENT REDIGE ATTENTE VALIDATION"><!-- STATUT : Rédigé, à valider -->
                                                                    <img title="Rédigé, à valider" alt="Rédigé, à valider" src="<atexo:href href='images/document-statut-2.gif'/>">
                                                                    </logic:equal>
                                                                    <logic:equal name="documentId" property="statut" value="ELIGIBLE"><!-- STATUT : Validé, éligible DCE -->
                                                                    <img title="Validé, éligible DCE" alt="Validé, éligible DCE" src="<atexo:href href='images/document-statut-3.gif'/>">
                                                                    </logic:equal>
                                                                    &nbsp;${documentId.tauxProgression}%
                                                                    <div class="date">
                                                                        <bean:write name="documentId" property="dateModification" format="dd/MM/yyyy"/>
                                                                        <br/>
                                                                        <bean:write name="documentId" property="dateModification" format="HH:mm"/>
                                                                    </div>
                                                                </td>
                                                                <td class="actions action-inline col-35">
                                                                    <!-- Dupliquer -->
                                                                    <a href="copyDocument.htm?idDoc=${documentId.id}" class="bouton-action">
                                                                        <img src="<atexo:href href='images/bouton-picto-dupliquer.gif'/>" alt="Dupliquer" title="Dupliquer" />
                                                                    </a>
                                                                    <!-- Générer  -->
                                                                    <a href="javascript:popUp('documentServlet?id=${documentId.id}','yes')">
                                                                        <img title="Générer le document" alt="Générer le document" src="<atexo:href href='images/bouton-generer.gif'/>">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:if>
                                                    </c:if>
                                                </logic:iterate>
                                            </table>
                                        </logic:notEmpty>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </logic:iterate>
                    <c:set var="nombreResultatAffiche" scope="page" value="${nombreResultatAffiche+1}"/>
                </logic:notEmpty>
                <!--Fin bloc avec surbrillance-->
            </table>
        </div>

        <!--Fin Resultats Recherche documents-->
        <logic:notEmpty name="listeDocument">
            <div class="line">
                <h3>
                    <a href="javascript:showAll('layer-doc',<c:out value="${pageScope.nombreResultatAffiche}"/>);">
                        <bean:message key="RechercherDocument.txt.toutOuvrir"/>
                    </a> / <a href="javascript:hideAll('layer-doc',<c:out value="${pageScope.nombreResultatAffiche}"/>);">
                    <bean:message key="RechercherDocument.txt.toutFermer"/>
                </a>
                </h3>
                <div class="partitioner">
                    &nbsp;&nbsp; <atexo:navigationListe suivant="false" key="redaction.txt.precedent"/>
                    &nbsp; <atexo:compteurPage/>
                    &nbsp; <atexo:navigationListe suivant="true" key="redaction.txt.suivant"/>
                </div>
            </div>

            <logic:present name="ouvrirTous">
                <script>
					showAll('layer-doc', <c:out value="${pageScope.nombreResultatAffiche}"/>);
                </script>
            </logic:present>
        </logic:notEmpty>
    </atexo:tableauPagine>
    <div class="breaker"></div>
    <div class="boutons">
        <html:link action="/RechercherDocumentInit.epm?categorie=${categorie}&idNouvelleConsultation=${idNouvelleConsultation}" styleClass="annuler">
            <bean:message key="RechercherDocument.txt.annuler"/>
        </html:link>
    </div>
</div>
<!--Fin main-part-->
