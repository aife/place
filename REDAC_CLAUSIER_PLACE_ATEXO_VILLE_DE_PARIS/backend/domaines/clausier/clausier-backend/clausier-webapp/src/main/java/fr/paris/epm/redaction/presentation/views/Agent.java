package fr.paris.epm.redaction.presentation.views;

import java.text.MessageFormat;
import java.util.List;

public class Agent {
    private String identifiantExterne;

    private String nom;

    private String prenom;

    private String email;
    private String urlMpe;

    private Service service;

    private String client;
    private String idContexte;
    private String token;
    private List<Habilitation> habilitations;
    private List<String> roles;
    private List<String> styles;

    public String getIdentifiantExterne() {
        return identifiantExterne;
    }

    public void setIdentifiantExterne(String identifiantExterne) {
        this.identifiantExterne = identifiantExterne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public List<Habilitation> getHabilitations() {
        return habilitations;
    }

    public void setHabilitations(List<Habilitation> habilitations) {
        this.habilitations = habilitations;
    }

    public String getUrlMpe() {
        return urlMpe;
    }

    public void setUrlMpe(String urlMpe) {
        this.urlMpe = urlMpe;
    }

    public List<String> getStyles() {
        return styles;
    }

    public void setStyles(List<String> styles) {
        this.styles = styles;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdContexte() {
        return idContexte;
    }

    public void setIdContexte(String idContexte) {
        this.idContexte = idContexte;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} {1} ({2})", prenom, nom, identifiantExterne);
    }
}
