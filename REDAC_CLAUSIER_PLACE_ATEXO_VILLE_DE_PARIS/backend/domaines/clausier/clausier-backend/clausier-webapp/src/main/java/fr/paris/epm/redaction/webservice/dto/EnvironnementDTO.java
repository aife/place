package fr.paris.epm.redaction.webservice.dto;

import java.util.UUID;

public class EnvironnementDTO {
	private UUID uuid;

	private String client;

	private String plateforme;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid( UUID uuid ) {
		this.uuid = uuid;
	}

	public String getClient() {
		return client;
	}

	public void setClient( String client ) {
		this.client = client;
	}

	public String getPlateforme() {
		return plateforme;
	}

	public void setPlateforme( String plateforme ) {
		this.plateforme = plateforme;
	}
}
