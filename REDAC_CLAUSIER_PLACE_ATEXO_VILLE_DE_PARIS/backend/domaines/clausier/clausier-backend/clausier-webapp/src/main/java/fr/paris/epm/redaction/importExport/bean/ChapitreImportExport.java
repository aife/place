package fr.paris.epm.redaction.importExport.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChapitreImportExport {

    private Integer idChapitre;

    private String titre;

    private String numero;

    private Map<Integer, String> clauses;

    private List<ChapitreImportExport> sousChapitres = new ArrayList<>();

    private String infoBulleText;

    private String infoBulleUrl;

    private String style;

    private String derogationArticle;

    private String derogationCommentaires;

    private Boolean derogationActive;

    public Integer getIdChapitre() {
        return idChapitre;
    }

    public void setIdChapitre(Integer idChapitre) {
        this.idChapitre = idChapitre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Map<Integer, String> getClauses() {
        return clauses;
    }

    public void setClauses(Map<Integer, String> clauses) {
        this.clauses = clauses;
    }

    public List<ChapitreImportExport> getSousChapitres() {
        return sousChapitres;
    }

    public void setSousChapitres( List<ChapitreImportExport> sousChapitres ) {
        this.sousChapitres = sousChapitres;
    }

    public String getInfoBulleText() {
        return infoBulleText;
    }

    public void setInfoBulleText(String infoBulleText) {
        this.infoBulleText = infoBulleText;
    }

    public String getInfoBulleUrl() {
        return infoBulleUrl;
    }

    public void setInfoBulleUrl(String infoBulleUrl) {
        this.infoBulleUrl = infoBulleUrl;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getDerogationArticle() {
        return derogationArticle;
    }

    public void setDerogationArticle(String derogationArticle) {
        this.derogationArticle = derogationArticle;
    }

    public String getDerogationCommentaires() {
        return derogationCommentaires;
    }

    public void setDerogationCommentaires(String derogationCommentaires) {
        this.derogationCommentaires = derogationCommentaires;
    }

    public Boolean getDerogationActive() {
        return derogationActive;
    }

    public void setDerogationActive(Boolean derogationActive) {
        this.derogationActive = derogationActive;
    }

}
