<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><bean:message key="previsualiserCanevas.titre.titreCanevas" /></title>
<script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
<link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
</head>
<body onload="popupResize();">
	<div class="previsu-doc" id="container">
		<div class="content">
			<center>
				<h5>
					<c:out value="${param.ref}" />
				</h5>
			</center>
			<p class="info-statut statut-clause-suppr">
				<bean:message key="popup.conflit.suppression" />
			</p>
		</div>
		<a href="javascript:window.close();" class="annuler"><bean:message
				key="popup.conflit.suppression.bouton.annuler" /> </a> <a
			href="javascript:opener.supprimerClauseGestionConflitJS('<c:out value="${param.ref}"/>');window.close();"
			class="button-moyen float-right"><bean:message
				key="popup.conflit.suppression.bouton.valider" /> </a>
		<div class="breaker"></div>
	</div>
</body>
</html>
