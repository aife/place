package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.referentiel.*;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.util.Constantes;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe utilisée pour le conditionnement des clause lors de la création du document.
 * @author Régis Menet.
 * @version $Revision$, $Date$, $Author$
 */
public class HeriteConsultation {

	/**
	 * Valeur non définie
	 */
	public static final String NOT_DEFINED = "ND";

	/**
     * Acces aux fichiers de libelle (injection Spring indirecte).
     */
    private ResourceBundleMessageSource messageSource;

    private EpmTConsultation consultation;

    private EpmTBudLotOuConsultation epmTBudLotOuConsultation;

    private Set<EpmTBudLot> epmTBudLots;

    private boolean allotissement = false;

    private List<EpmTValeurConditionnementExterne> listEpmTValeurConditionnementExterne;

    /**
     * Liste des valeurs de conditionnement complexe
     */
    private List<EpmTValeurConditionnementExterneComplexe> listEpmTValeurConditionnementExterneComplexes;

    private Date dateRemisePlis;

    public HeriteConsultation(final EpmTConsultation valeur, final Date dateRemisePlis, List<EpmTValeurConditionnementExterne> valeurExterne,
                              List<EpmTValeurConditionnementExterneComplexe> valeursComplexe) {
        this.consultation = valeur;
        this.listEpmTValeurConditionnementExterne = valeurExterne;
        this.listEpmTValeurConditionnementExterneComplexes = valeursComplexe;
        this.dateRemisePlis = dateRemisePlis;
        if (consultation != null) {
            epmTBudLotOuConsultation = consultation.getEpmTBudLotOuConsultation();
            epmTBudLots = consultation.getEpmTBudLots();
            allotissement = (consultation.getAllotissement() != null && consultation.getAllotissement().equals(Constante.OUI));
        }
    }

    public final String getNaturePrestation(final int idLot) {

        if (!allotissement || idLot <= 0) {
            return consultation.getEpmTRefNature().getLibelle();
        } else {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            for (EpmTBudLot lot : lots)
                if (lot.getId() == idLot)
                    return lot.getEpmTRefNature().getLibelle();
        }
        return NOT_DEFINED;
    }

    public final String getAllotissement() {
        return allotissement ? "allotie" : "non allotie";
    }

    public final String getDelaiValiditeOffres() {
        return consultation.getDelaiValiditeOffres() + " mois";
    }

    public final String getDureeDuMarche(final int idLot) {

        //non alloti
        if (!allotissement) {
            EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription = consultation.getEpmTRefDureeDelaiDescription();
            if (epmTRefDureeDelaiDescription != null) {

                //description durée délai
                if (epmTRefDureeDelaiDescription.getId() == 3) {
                    return consultation.getDescriptionDuree();

                    //durée du marché
                } else if (epmTRefDureeDelaiDescription.getId() == 1) {
                    if (consultation.getEpmTRefChoixMoisJour() != null && consultation.getDureeMarche() != null) {
                        if (consultation.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_JOUR)
                            return consultation.getDureeMarche() + " jours";
                        else if (consultation.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_MOIS)
                            return consultation.getDureeMarche() + " mois";
                        else if (consultation.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_ANNEE)
                            return consultation.getDureeMarche() + " ans";
                    }

                    //date d'éxecution
                } else if (epmTRefDureeDelaiDescription.getId() == 2) {
                    Calendar debut = consultation.getDateExecutionPrestationsDebut();
                    Calendar fin = consultation.getDateExecutionPrestationsFin();
                    if (debut != null && fin != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                        String de = formatter.format(debut.getTime());
                        String a = formatter.format(fin.getTime());
                        return "A compter du " + de + " jusqu'au " + a;
                    }
                }
            }

            //alloti
        } else {
            StringBuilder resultatAlloti = new StringBuilder();
            Set<EpmTBudLot> listeLots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lots = new ArrayList<EpmTBudLot>(listeLots);
            Collections.sort(lots);

            for (EpmTBudLot lot : lots) {
                if (idLot > 0) {
                    if (lot.getId() == idLot) {
                        resultatAlloti.append(getDureeMarcheParLot(lot));
                        break;
                    }
                } else {
                    resultatAlloti.append(getDureeMarcheParLot(lot));
                    resultatAlloti.append("<br />");
                }
            }
            return resultatAlloti.toString();
        }
        return NOT_DEFINED;
    }

    private String getDureeMarcheParLot(EpmTBudLot lot) {
        String prefixeLot = "Lot "+lot.getNumeroLot()+ " : ";
        EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription = lot.getEpmTRefDureeDelaiDescription();
        if (epmTRefDureeDelaiDescription != null) {

            //description durée délai
            if (epmTRefDureeDelaiDescription.getId() == 3) {
                return prefixeLot + lot.getDescriptionDuree();

                //durée du marché
            } else if (epmTRefDureeDelaiDescription.getId() == 1) {
                if (lot.getEpmTRefChoixMoisJour() != null && lot.getDureeMarche() != null) {
                    if (lot.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_JOUR)
                        return prefixeLot + lot.getDureeMarche() + " jours";
                    else if (lot.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_MOIS)
                        return prefixeLot + lot.getDureeMarche() + " mois";
                    else if (lot.getEpmTRefChoixMoisJour().getId() == EpmTRefChoixMoisJour.EN_ANNEE)
                        return prefixeLot + lot.getDureeMarche() + " ans";
                }

                //date d'éxecution
            } else if (epmTRefDureeDelaiDescription.getId() == 2) {
                Calendar debut = lot.getDateDebut();
                Calendar fin = lot.getDateFin();
                if (debut != null && fin != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                    String de = formatter.format(debut.getTime());
                    String a = formatter.format(fin.getTime());
                    return prefixeLot + " A compter du " + de + " jusqu'au " + a;
                }
            }
        }
        return NOT_DEFINED;
    }

    public final String getRefCCAG(final int idLod) {
        if (allotissement) {
            if (idLod != 0) {
                if (epmTBudLots != null)
                    for (EpmTBudLot lot : epmTBudLots)
                        if (lot.getId() == idLod)
                            return lot.getEpmTBudLotOuConsultation().getEpmTRefCcag().getLibelle();
            } else {
                return "tableau";
            }
        } else {
            if (epmTBudLotOuConsultation != null && epmTBudLotOuConsultation.getEpmTRefCcag() != null)
                return epmTBudLotOuConsultation.getEpmTRefCcag().getLibelle();
        }
        return NOT_DEFINED;
    }

    public final String getNumeroLot(final int id) {
        if (epmTBudLots != null)
            for (EpmTBudLot lot : epmTBudLots)
                if (lot.getId() == id)
                    return lot.getNumeroLot();
        return NOT_DEFINED;
    }

    public final String getIntituleLot(final int id) {
        if (epmTBudLots != null)
            for (EpmTBudLot lot : epmTBudLots)
                if (lot.getId() == id)
                    return lot.getIntituleLot();
        return NOT_DEFINED;
    }

    public final String getDescriptionSuccinteLot(final int id) {
        if (epmTBudLots != null)
            for (EpmTBudLot lot : epmTBudLots)
                if (lot.getId() == id)
                    return lot.getDescriptionSuccinte();
        return NOT_DEFINED;
    }

    public final String getCpvs(final int idLot) {
		String result = null;

        if (consultation.getCpvs() != null && idLot <= 0) {
        	result = this.getCpvs();
        } else if (consultation.getEpmTBudLots() != null) {
	        StringBuilder builder = new StringBuilder();

	        List<EpmTBudLot> lots = (idLot > 0) ?
		        consultation.getEpmTBudLots().stream()
			        .filter(lot -> lot.getId() == idLot)
			        .collect(Collectors.toList()) :
		        consultation.getEpmTBudLots().stream()
			        .sorted(Comparator.comparingInt(EpmTBudLot::getId))
			        .collect(Collectors.toList());

	        for (EpmTBudLot lot : lots) {
		        builder.append("Pour le lot ").append(lot.getNumeroLot()).append(" \"").append(lot.getIntituleLot()).append("\" : <br/>");

		        StringBuilder resultLot = new StringBuilder();
		        for (EpmTCpv cpv : consultation.getCpvs()) {
			        if (cpv.isPrincipal())
				        resultLot.insert(0, cpv.getCodeCpv() + " - " + cpv.getLibelleCpv());  // tjrs au début CPV principale de chaque lot (s'il existe)
			        else
				        resultLot.append(cpv.getCodeCpv()).append(" - ").append(cpv.getLibelleCpv());  // lot peut avoir plusieur codes CPV
			        resultLot.append("<br/>");
		        }
		        resultLot.insert(0, "Valeur principale : ");
		        builder.append(resultLot);
	        }

	        result = builder.toString();
        }

        return result;
    }

	public final String getCpvs() {
		StringBuilder result = new StringBuilder();

		if (consultation.getCpvs() != null) {
			for (EpmTCpv cpv : consultation.getCpvs()) {
				if (cpv.isPrincipal())
					result.insert(0, cpv.getCodeCpv() + " - " + cpv.getLibelleCpv()); // tjrs au début CPV principale (s'il existe)
				else
					result.append(cpv.getCodeCpv()).append(" - ").append(cpv.getLibelleCpv()); // consultation peut avoir plusieur codes CPV
				result.append("<br/>");
			}
			result.insert(0, "Valeur principale : ");
		}

		return result.toString();
	}

    /**
     * Formatage du pourcentage des clauses sociales de la consultation et du lot.
     */
    private String formatagePourcentage(final Double valeur) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(ConstantesGlobales.LOCALE_FR));

        DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
        df.setDecimalSeparatorAlwaysShown(true);
        df.setMaximumFractionDigits(2);
        df.setMaximumIntegerDigits(3);
        return df.format(valeur);
    }

    /**
     * Formatage des heures des clauses sociale de la consultation et du lot.
     */
    private String formatageHeureClauseSociale(final Double valeur) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(ConstantesGlobales.LOCALE_FR));

        DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
        df.setDecimalSeparatorAlwaysShown(true);
        df.setMaximumFractionDigits(2);
        df.setMaximumIntegerDigits(5);
        return df.format(valeur);
    }

    public final String getCandidatAdmisPresenterOffre() {
        if (consultation.getNombreCandidatsFixe() != null)
            return String.valueOf(consultation.getNombreCandidatsFixe().intValue());
        else if (consultation.getNombreCandidatsMin() != null && consultation.getNombreCandidatsMax() != null)
            return "minimun " + consultation.getNombreCandidatsMin() + " maximun " + consultation.getNombreCandidatsMax();
        return NOT_DEFINED;
    }

    public final List getLotsTechniques(final int id) {
        List listeLots = new ArrayList<>();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lotsTmp = tri(lots.iterator());
            listeLots.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeLots.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeLots.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.identifiantLotTechnique", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTLotTechniques.iterator.getNumeroLot", id));
            listeLots.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.intituleLotTechnique", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTLotTechniques.iterator.getIntituleLot", id));
            listeLots.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.lotPrincipal", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTLotTechniques.iterator.getPrincipal", id));
            listeLots.add(getTrancheLignes(lotsTmp, messageSource.getMessage("tableau.lotsTechnique.appartenanceTranche", null, Locale.FRENCH), id));
        } else {
            if (consultation != null && consultation.getEpmTBudLotOuConsultation() != null) {
                Set<EpmTLotTechnique> lotsTechniques = consultation.getEpmTBudLotOuConsultation().getEpmTLotTechniques();
                List<EpmTLotTechnique> lotsTechniquesTmp = tri(lotsTechniques.iterator());
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.numeroLot", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.intituleLot", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(lotsTechniquesTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.identifiantLotTechnique", null, Locale.FRENCH), "getNumeroLot", true));
                listeLots.add(getLigne(lotsTechniquesTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.intituleLotTechnique", null, Locale.FRENCH), "getIntituleLot", true));
                listeLots.add(getLigne(lotsTechniquesTmp.iterator(), messageSource.getMessage("tableau.lotsTechnique.lotPrincipal", null, Locale.FRENCH), "getPrincipal", true));
                listeLots.add(getTrancheLigne(lotsTechniquesTmp, messageSource.getMessage("tableau.lotsTechnique.appartenanceTranche", null, Locale.FRENCH)));
            } else {
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.numeroLot", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.intituleLot", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.identifiantLotTechnique", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.intituleLotTechnique", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.lotPrincipal", null, Locale.FRENCH), null, true));
                listeLots.add(getLigne(null, messageSource.getMessage("tableau.lotsTechnique.appartenanceTranche", null, Locale.FRENCH), null, true));
            }
        }
        return listeLots;
    }

    public final List getLots(final int id) {
        List listeLots = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> liste = tri(lots.iterator());
            listeLots.add(getLignes(liste.iterator(), messageSource.getMessage("tableau.lots.numero", null, Locale.FRENCH), "getNumeroLot", id));
            listeLots.add(getLignes(liste.iterator(), messageSource.getMessage("tableau.lots.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
        } else {
            listeLots.add(getLigne(null, messageSource.getMessage("tableau.lots.numero", null, Locale.FRENCH), null, true));
            listeLots.add(getLigne(null, messageSource.getMessage("tableau.lots.intituleLot", null, Locale.FRENCH), null, true));
        }
        return listeLots;
    }

    public final List getTranches(final int id) {
        List listeTranches = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> listeLots = tri(lots.iterator());

            listeTranches.add(getLignes(listeLots.iterator(), messageSource.getMessage("tableau.tranches.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeTranches.add(getLignes(listeLots.iterator(), messageSource.getMessage("tableau.tranches.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeTranches.add(getLignes(listeLots.iterator(), messageSource.getMessage("tableau.tranches.identifiantTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getCodeTranche", id));
            listeTranches.add(getLignes(listeLots.iterator(), messageSource.getMessage("tableau.tranches.intituleTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getIntituleTranche", id));
        } else {
            listeTranches.add(getLigne(null, messageSource.getMessage("tableau.tranches.numeroLot", null, Locale.FRENCH), null, true));
            listeTranches.add(getLigne(null, messageSource.getMessage("tableau.tranches.intituleLot", null, Locale.FRENCH), null, true));
            listeTranches.add(getLigne(consultation, messageSource.getMessage("tableau.tranches.identifiantTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getCodeTranche", true));
            listeTranches.add(getLigne(consultation, messageSource.getMessage("tableau.tranches.intituleTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getIntituleTranche", true));
        }
        return listeTranches;
    }

    public final List getVariantesObligatoires(final int id) {
        List listeLot = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lotsTmp = tri(lots.iterator());
            Collections.sort(lotsTmp);
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesObligatoires.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesObligatoires.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesObligatoires.variantesObligatoires", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getOptionsTechniquesImposees", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesObligatoires.description", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getDescriptionOptionsImposees", id));
        } else if (consultation != null && epmTBudLotOuConsultation != null) {
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.numeroLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.intituleLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.variantesObligatoires.variantesObligatoires", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getOptionsTechniquesImposees", true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.variantesObligatoires.description", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getDescriptionOptionsImposees", true));
        } else {
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.numeroLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.intituleLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.variantesObligatoires", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesObligatoires.description", null, Locale.FRENCH), null, true));
        }
        return listeLot;
    }

    public final List getVariantesAutorisees(final int id) {
        List listeLot = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lotsTmp = tri(lots.iterator());
            Collections.sort(lotsTmp);
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesAutorisees.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesAutorisees.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.variantesAutorisees.variantes", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getVariantesAutorisees", id));
        } else {
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesAutorisees.numeroLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.variantesAutorisees.intituleLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.variantesAutorisees.variantes", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getVariantesAutorisees", true));
        }
        return listeLot;
    }

    public final List getNbReconductions(final int id) {
        List listeLot = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lotsTmp = tri(lots.iterator());
            Collections.sort(lotsTmp);
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.reconduction.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.reconduction.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.reconduction.reconductible", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getReconductible", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.reconduction.nombreReconduction", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getNbReconductions", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.reconduction.duree", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getModalitesReconduction", id));
        } else {
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.reconduction.numeroLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.reconduction.intituleLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.reconduction.reconductible", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getReconductible", true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.reconduction.nombreReconduction", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getNbReconductions", true));
            listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.reconduction.duree", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getModalitesReconduction", true));
        }
        return listeLot;
    }

    public final String getLotReserve() {
        for (EpmTRefClausesSociales ClauseSocialeChoixUtilisateur : consultation.getEpmTBudLotOuConsultation().getClausesSocialesChoixUtilisateur())
            if (ClauseSocialeChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesSociales.CODE_CLAUSE_RESERVE))
                return Constantes.MARCHE_RESERVE;
        return Constantes.MARCHE_NON_RESERVE;
    }

    public final List getFormePrix(final int id) {
        List listeLot = new ArrayList();
        if (allotissement) {
            Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
            List<EpmTBudLot> lotsTmp = tri(lots.iterator());
            Collections.sort(lotsTmp);
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.numeroLot", null, Locale.FRENCH), "getNumeroLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.intituleLot", null, Locale.FRENCH), "getIntituleLot", id));
            listeLot.add(getLignes(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.numeroTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getCodeTranche", id));
            listeLot.add(getLignesTypePrix(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.typePrix", null, Locale.FRENCH), id));
            listeLot.add(getLignesPU(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.typePU", null, Locale.FRENCH), id));
            listeLot.add(getLignesBC(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.BC", null, Locale.FRENCH), id));
            listeLot.add(getLignesMinMax(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.minMax", null, Locale.FRENCH), id));
            listeLot.add(getLignesVariationPrix(lotsTmp.iterator(), messageSource.getMessage("tableau.formePrix.variationPrix", null, Locale.FRENCH), id));
        } else {
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.numeroLot", null, Locale.FRENCH), null, true));
            listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.intituleLot", null, Locale.FRENCH), null, true));
            if (consultation != null && consultation.getEpmTBudLotOuConsultation() != null) {
                Set tranches = consultation.getEpmTBudLotOuConsultation().getEpmTBudTranches();
                if (epmTBudLotOuConsultation != null && epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                    tranches = new HashSet();
                    EpmTBudTranche tranche = new EpmTBudTranche();
                    tranche.setEpmTBudFormePrix(epmTBudLotOuConsultation.getEpmTBudFormePrix());
                    tranches.add(tranche);
                }
                listeLot.add(getLigne(consultation, messageSource.getMessage("tableau.formePrix.numeroTranche", null, Locale.FRENCH), "getEpmTBudLotOuConsultation.getEpmTBudTranches.iterator.getCodeTranche", true));
                List listeTypePrix = new ArrayList();
                listeTypePrix.add(messageSource.getMessage("tableau.formePrix.typePrix", null, Locale.FRENCH));
                List listeTmp = getLigneTypePrix(tranches.iterator());
                if (listeTmp.isEmpty()) {
                    listeTmp.add("-");
                }
                listeTypePrix.add(listeTmp);
                listeLot.add(listeTypePrix);

                List listePU = new ArrayList();
                listePU.add(messageSource.getMessage("tableau.formePrix.typePU", null, Locale.FRENCH));
                listeTmp = getLignePU(tranches.iterator());
                if (listeTmp.isEmpty()) {
                    listeTmp.add("-");
                }
                listePU.add(listeTmp);
                listeLot.add(listePU);

                List listeBC = new ArrayList();
                listeBC.add(messageSource.getMessage("tableau.formePrix.BC", null, Locale.FRENCH));
                listeTmp = getLigneBC(tranches.iterator());
                if (listeTmp.isEmpty()) {
                    listeTmp.add("-");
                }
                listeBC.add(listeTmp);
                listeLot.add(listeBC);

                List listeMinMax = new ArrayList();
                listeMinMax.add(messageSource.getMessage("tableau.formePrix.minMax", null, Locale.FRENCH));
                String libelleUnite = "";
                if (epmTBudLotOuConsultation.getUnite() != null) {
                    libelleUnite = epmTBudLotOuConsultation.getUnite().getLibelle();
                }
                listeTmp = getLigneMinMax(tranches.iterator(), libelleUnite);
                if (listeTmp.isEmpty()) {
                    listeTmp.add("-");
                }
                listeMinMax.add(listeTmp);
                listeLot.add(listeMinMax);

                List variationPrix = new ArrayList();
                variationPrix.add(messageSource.getMessage("tableau.formePrix.variationPrix", null, Locale.FRENCH));
                listeTmp = getLigneVariationPrix(tranches.iterator());
                if (listeTmp.isEmpty()) {
                    listeTmp.add("-");
                }
                variationPrix.add(listeTmp);
                listeLot.add(variationPrix);
            } else {
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.numeroTranche", null, Locale.FRENCH), null, true));
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.typePrix", null, Locale.FRENCH), null, true));
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.typePU", null, Locale.FRENCH), null, true));
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.BC", null, Locale.FRENCH), null, true));
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.minMax", null, Locale.FRENCH), null, true));
                listeLot.add(getLigne(null, messageSource.getMessage("tableau.formePrix.variationPrix", null, Locale.FRENCH), null, true));
            }
        }
        return listeLot;
    }

    private String recupererValeurTableauBonDeCommande(final int idLot, final boolean maximum) {
        String resultat = "";
        EpmTBudLotOuConsultation lotOuConsultation = null;
        if (allotissement) {

            if (idLot != 0) {
                if (epmTBudLots != null) {
                    Iterator it = epmTBudLots.iterator();
                    while (it.hasNext()) {
                        EpmTBudLot lot = (EpmTBudLot) it.next();
                        if (lot.getId() == idLot) {
                            if (lot.getEpmTBudLotOuConsultation() != null && (lot.getEpmTBudLotOuConsultation().getEpmTBudTranches() == null || lot.getEpmTBudLotOuConsultation().getEpmTBudTranches().isEmpty())) {
                                lotOuConsultation = lot.getEpmTBudLotOuConsultation();
                            }
                        }
                    }
                }
            }
        } else {
            if (consultation != null && consultation.getEpmTBudLotOuConsultation() != null && (consultation.getEpmTBudLotOuConsultation().getEpmTBudTranches() == null || consultation.getEpmTBudLotOuConsultation().getEpmTBudTranches().isEmpty())) {
                lotOuConsultation = consultation.getEpmTBudLotOuConsultation();
            }
        }
        if (lotOuConsultation != null && lotOuConsultation.getEpmTBudFormePrix() != null) {
            String valeurBonDeCommande = recupererValeurBonDeCommande(maximum, lotOuConsultation.getEpmTBudFormePrix());
            if (valeurBonDeCommande != null) {
                resultat = valeurBonDeCommande;
            }
        }
        return resultat;
    }

    /**
     * Récupére le montant minimum ht ou maximum ht d'un bon de commande à partir d'une forme de prix.
     * @param maximum si true on retourne le maximim. sinon le minimum
     * @param formeDePrix la forme de prix pour laquelle on cherche les valeurs du bon de commande.
     * @return null, si pas de valeur, le montant formaté si un résultat.
     */
    private String recupererValeurBonDeCommande(final boolean maximum, EpmTBudFormePrix formeDePrix) {
        String resultat = "";
        Double valeurResultat = null;

        Double valeurMin = null;
        Double valeurMax = null;

        if (formeDePrix instanceof EpmTBudFormePrixPm) {
            EpmTBudFormePrixPm formeDePrixPm = (EpmTBudFormePrixPm) formeDePrix;
            valeurMin = formeDePrixPm.getPuMinHt();
            valeurMax = formeDePrixPm.getPuMaxHt();
        } else if (formeDePrix instanceof EpmTBudFormePrixPu) {
            EpmTBudFormePrixPu formeDePrixPu = (EpmTBudFormePrixPu) formeDePrix;
            valeurMin = formeDePrixPu.getPuMinHt();
            valeurMax = formeDePrixPu.getPuMaxHt();
        } else if (formeDePrix instanceof EpmTBudFormePrixPf) {
            return null;
        }

        if (maximum) {
            valeurResultat = valeurMax;
        } else {
            valeurResultat = valeurMin;
        }
        if (valeurResultat != null) {
            resultat = fr.paris.epm.global.commun.Util.doubleToString(valeurResultat);
        }
        return resultat;
    }

    private List getLignesPU(final Iterator it, final String titre, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        while (it.hasNext()) {
            EpmTBudLot lot = (EpmTBudLot) it.next();
            if (lot.getId() != id && id > 0) {
                continue;
            }
            if (lot.getEpmTBudLotOuConsultation() != null) {
                EpmTBudLotOuConsultation lotOuConsultation = (EpmTBudLotOuConsultation) lot.getEpmTBudLotOuConsultation();
                if (lotOuConsultation.getEpmTBudTranches() != null) {
                    List listeTranche = getLignePU(lotOuConsultation.getEpmTBudTranches().iterator());
                    if (listeTranche.isEmpty()) {
                        List listeForme = new ArrayList();
                        EpmTBudTranche tranche = new EpmTBudTranche();
                        tranche.setEpmTBudFormePrix(lotOuConsultation.getEpmTBudFormePrix());
                        listeForme.add(tranche);
                        liste.add(getLignePU(listeForme.iterator()));
                    } else {
                        liste.add(listeTranche);
                    }
                }
            }
        }
        return liste;
    }

    private List getLignePU(final Iterator it) {
        List listeTranche = new ArrayList();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm formePrix = (EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix();
                if (formePrix.getPuEpmTRefTypePrix() != null) {
                    Iterator it3 = formePrix.getPuEpmTRefTypePrix().iterator();
                    StringBuffer resultat = new StringBuffer("");
                    String separateur = "";
                    while (it3.hasNext()) {
                        EpmTRefTypePrix typePrix = (EpmTRefTypePrix) it3.next();
                        resultat.append(separateur).append(typePrix.getLibelle());
                        separateur = "; ";
                    }
                    listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat.toString()));
                }
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu formePrix = (EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix();
                if (formePrix.getEpmTRefTypePrix() != null) {
                    Iterator it3 = formePrix.getEpmTRefTypePrix().iterator();
                    StringBuffer resultat = new StringBuffer("");
                    String separateur = "";
                    while (it3.hasNext()) {
                        EpmTRefTypePrix typePrix = (EpmTRefTypePrix) it3.next();
                        resultat.append(separateur).append(typePrix.getLibelle());
                        separateur = "; ";
                    }
                    listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat.toString()));
                }
            } else {
                listeTranche.add("-");
            }
        }
        return listeTranche;
    }

    private List getLignesVariationPrix(final Iterator it, final String titre, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        while (it.hasNext()) {
            EpmTBudLot lot = (EpmTBudLot) it.next();
            if (lot.getId() != id && id > 0)
                continue;

            if (lot.getEpmTBudLotOuConsultation() != null) {
                EpmTBudLotOuConsultation lotOuConsultation = lot.getEpmTBudLotOuConsultation();
                if (lotOuConsultation.getEpmTBudTranches() != null) {
                    List listeTranche = getLigneVariationPrix(lotOuConsultation.getEpmTBudTranches().iterator());
                    if (listeTranche.isEmpty()) {
                        List listeForme = new ArrayList();
                        EpmTBudTranche tranche = new EpmTBudTranche();
                        tranche.setEpmTBudFormePrix(lotOuConsultation.getEpmTBudFormePrix());
                        listeForme.add(tranche);
                        liste.add(getLigneVariationPrix(listeForme.iterator()));
                    } else {
                        liste.add(listeTranche);
                    }
                }
            }
        }
        return liste;
    }

    private List getLigneVariationPrix(final Iterator it) {
        List listeTranche = new ArrayList();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm formePrix = (EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix();
                String resultat = "";
                if (formePrix.getPfEpmTRefVariations() != null) {
                    Iterator it3 = formePrix.getPfEpmTRefVariations().iterator();
                    resultat = getVariation(it3);
                    if (resultat.equals("")) {
                        resultat = "-";
                    }
                }
                if (formePrix.getPuEpmTRefVariations() != null) {
                    Iterator it3 = formePrix.getPuEpmTRefVariations().iterator();
                    String resultat2 = getVariation(it3);
                    if (resultat2.equals("")) {
                        resultat2 = "-";
                    }
                    resultat = "PF : " + resultat + "; PU : " + resultat2;
                }
                if (resultat.equals("")) {
                    resultat = "-";
                }
                listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat));
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu formePrix = (EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix();
                if (formePrix.getPuEpmTRefVariations() != null) {
                    Iterator it3 = formePrix.getPuEpmTRefVariations().iterator();
                    String resultat = getVariation(it3);
                    if (resultat.equals("")) {
                        resultat = "-";
                    }
                    listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat));
                }
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPf) {
                EpmTBudFormePrixPf formePrix = (EpmTBudFormePrixPf) tranche.getEpmTBudFormePrix();
                if (formePrix.getPfEpmTRefVariations() != null) {
                    Iterator it3 = formePrix.getPfEpmTRefVariations().iterator();
                    String resultat = getVariation(it3);
                    if (resultat.equals("")) {
                        resultat = "-";
                    }
                    listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat));
                }
            } else {
                listeTranche.add("-");
            }
        }
        return listeTranche;
    }

    private String getVariation(final Iterator it) {
        StringBuffer resultat = new StringBuffer("");
        String separateur = "";
        while (it.hasNext()) {
            EpmTRefVariation variation = (EpmTRefVariation) it.next();
            resultat.append(separateur).append(variation.getLibelle());
            separateur = "; ";
        }
        return resultat.toString();
    }

    private List getLignesMinMax(final Iterator it, final String titre, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        while (it.hasNext()) {
            EpmTBudLot lot = (EpmTBudLot) it.next();
            if (lot.getId() != id && id > 0) {
                continue;
            }
            if (lot.getEpmTBudLotOuConsultation() != null) {
                EpmTBudLotOuConsultation lotOuConsultation = lot.getEpmTBudLotOuConsultation();
                String libelleUnite = "";
                if (lotOuConsultation.getUnite() != null) {
                    libelleUnite = lotOuConsultation.getUnite().getLibelle();
                }
                if (lotOuConsultation.getEpmTBudTranches() != null) {
                    List listeTranche = getLigneMinMax(lotOuConsultation.getEpmTBudTranches().iterator(), libelleUnite);
                    if (listeTranche.isEmpty()) {
                        List listeForme = new ArrayList();
                        EpmTBudTranche tranche = new EpmTBudTranche();
                        tranche.setEpmTBudFormePrix(lotOuConsultation.getEpmTBudFormePrix());
                        listeForme.add(tranche);
                        liste.add(getLigneMinMax(listeForme.iterator(), libelleUnite));
                    } else {
                        liste.add(listeTranche);
                    }
                }
            }
        }
        return liste;
    }

    private List getLigneMinMax(final Iterator it, final String libelleUnite) {
        List listeTranche = new ArrayList();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm formePrix = (EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix();
                String resultat = "";
                if (formePrix.getPuMinHt() != null) {
                    resultat = "Min: " + fr.paris.epm.global.commun.Util.doubleToString(formePrix.getPuMinHt());
                    resultat += "\n";
                }
                if (formePrix.getPuMaxHt() != null) {
                    resultat += "Max: " + fr.paris.epm.global.commun.Util.doubleToString(formePrix.getPuMaxHt());
                }
                if (resultat.equals("")) {
                    resultat = "-";
                } else {
                    resultat += "\n " + libelleUnite;
                }
                listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat));
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu formePrix = (EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix();
                String resultat = "";
                if (formePrix.getPuMinHt() != null) {
                    resultat = "Min: " + fr.paris.epm.global.commun.Util.doubleToString(formePrix.getPuMinHt());
                    resultat += "\n";
                }
                if (formePrix.getPuMaxHt() != null) {
                    resultat += "Max: " + fr.paris.epm.global.commun.Util.doubleToString(formePrix.getPuMaxHt());
                }
                if (resultat.equals("")) {
                    resultat = "-";
                } else {
                    resultat += "\n " + libelleUnite;
                }
                listeTranche.add(Util.remplaceTableauPourSauvegarde(resultat));
            } else {
                listeTranche.add("-");
            }
        }
        return listeTranche;
    }

    private List getLignesBC(final Iterator it, final String titre, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        while (it.hasNext()) {
            EpmTBudLot lot = (EpmTBudLot) it.next();
            if (lot.getId() != id && id > 0) {
                continue;
            }
            if (lot.getEpmTBudLotOuConsultation() != null) {
                EpmTBudLotOuConsultation lotOuConsultation = (EpmTBudLotOuConsultation) lot.getEpmTBudLotOuConsultation();
                if (lotOuConsultation.getEpmTBudTranches() != null) {
                    List listeTranche = getLigneBC(lotOuConsultation.getEpmTBudTranches().iterator());
                    if (listeTranche.isEmpty()) {
                        List listeForme = new ArrayList();
                        EpmTBudTranche tranche = new EpmTBudTranche();
                        tranche.setEpmTBudFormePrix(lotOuConsultation.getEpmTBudFormePrix());
                        listeForme.add(tranche);
                        liste.add(getLigneBC(listeForme.iterator()));
                    } else {
                        liste.add(listeTranche);
                    }
                }
            }
        }
        return liste;
    }

    private List getLigneBC(final Iterator it) {
        List listeTranche = new ArrayList();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm formePrix = (EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix();
                if (formePrix.getPuEpmTRefBonQuantite() != null) {
                    listeTranche.add(formePrix.getPuEpmTRefBonQuantite().getLibelle());
                }
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu formePrix = (EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix();
                if (formePrix.getEpmTRefBonQuantite() != null) {
                    listeTranche.add(formePrix.getEpmTRefBonQuantite().getLibelle());
                }
            } else {
                listeTranche.add("-");
            }
        }
        return listeTranche;
    }

    private List getLignesTypePrix(final Iterator it, final String titre, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        while (it.hasNext()) {
            EpmTBudLot lot = (EpmTBudLot) it.next();
            if (lot.getId() != id && id > 0) {
                continue;
            }
            if (lot.getEpmTBudLotOuConsultation() != null) {
                EpmTBudLotOuConsultation lotOuConsultation = lot.getEpmTBudLotOuConsultation();
                if (lotOuConsultation.getEpmTBudTranches() != null) {
                    List listeTranche = getLigneTypePrix(lotOuConsultation.getEpmTBudTranches().iterator());
                    if (listeTranche.isEmpty()) {
                        List listeForme = new ArrayList();
                        EpmTBudTranche tranche = new EpmTBudTranche();
                        tranche.setEpmTBudFormePrix(lotOuConsultation.getEpmTBudFormePrix());
                        listeForme.add(tranche);
                        liste.add(getLigneTypePrix(listeForme.iterator()));
                    } else {
                        liste.add(listeTranche);
                    }
                }
            }
        }
        return liste;
    }

    private List<String> getLigneTypePrix(final Iterator it) {
        List<String> listeTranche = new ArrayList<>();
        while (it.hasNext()) {
            EpmTBudTranche tranche = (EpmTBudTranche) it.next();
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm)
                listeTranche.add("Prix mixte");
            else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu)
                listeTranche.add("Prix unitaire");
            else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPf)
                listeTranche.add("Prix forfaitaire");
        }
        return listeTranche;
    }

    private List<Object> getLignes(final Iterator it, final String titre, final String nomMethode, final int id) {
        List liste = new ArrayList();
        liste.add(titre);
        boolean iterator = false;
        boolean dejaResultat = false;
        while (it.hasNext()) {
            Object objet = it.next();
            if (objet instanceof EpmTBudLot) {
                if (id > 0 && id != ((EpmTBudLot) objet).getId()) {
                    continue;
                }
            }
            String[] methodes = nomMethode.split("\\.");
            Object resultat = null;
            String prefix = "";
            for (int i = 0; i < methodes.length; i++) {
                if (objet instanceof Iterator) {
                    Iterator it2 = tri((Iterator) objet).iterator();
                    List sousListe = new ArrayList();
                    while (it2.hasNext()) {
                        Object tmp = it2.next();
                        iterator = true;
                        if (tmp instanceof EpmTBudTranche && nomMethode.indexOf("getCodeTranche") != -1) {
                            EpmTBudTranche tranche = (EpmTBudTranche) tmp;
                            prefix = tranche.getEpmTRefNatureTranche().getLibelle();
                        }
                        resultat = Util.lancerMethode(tmp, null, methodes[i]);
                        if (resultat instanceof Set) {
                            break;
                        }
                        if (resultat != null) {
                            if (resultat instanceof Integer) {
                                sousListe.add(String.valueOf(((Integer) resultat).intValue()));
                            } else {
                                if (resultat.toString().equals("")) {
                                    sousListe.add(Util.remplaceTableauPourSauvegarde(prefix));
                                } else {
                                    if (prefix.equals("")) {
                                        sousListe.add(Util.remplaceTableauPourSauvegarde(resultat.toString()));
                                    } else {
                                        sousListe.add(Util.remplaceTableauPourSauvegarde(prefix + "-" + resultat));
                                    }
                                }
                            }
                        } else {
                            sousListe.add("-");
                        }
                        prefix = "";
                    }
                    if (!(resultat instanceof Set)) {
                        if (!iterator) {
                            liste.add("-");
                        } else {
                            liste.add(sousListe);
                        }
                        dejaResultat = true;
                    }
                    objet = resultat;
                    continue;
                }
                resultat = Util.lancerMethode(objet, null, methodes[i]);
                objet = resultat;
            }
            if (!dejaResultat) {
                if (resultat != null) {
                    if (resultat instanceof String) {
                        resultat = Util.remplaceTableauPourSauvegarde((String) resultat);
                    }
                    liste.add(resultat);
                } else {
                    liste.add("-");
                }
            }
        }
        if (liste.size() == 1)
            liste.add("-");
        return liste;
    }

    private List<Object> getLigne(Object objet, final String titre, final String nomMethode, final boolean premiereLigne) {
        List<Object> liste = new ArrayList<>();
        if (premiereLigne)
            liste.add(titre);

        Object resultat = null;
        boolean iterator = false;
        boolean dejaResultat = false;
        String prefix = "";
        if (nomMethode != null) {
            String[] methodes = nomMethode.split("\\.");
            for (String methode : methodes) {
                if (objet instanceof Iterator) {
                    Iterator it2 = tri((Iterator) objet).iterator();
                    List sousListe = new ArrayList();
                    while (it2.hasNext()) {
                        iterator = true;
                        Object tmp = it2.next();
                        if (tmp instanceof EpmTBudTranche && nomMethode.indexOf("getCodeTranche") != -1) {
                            EpmTBudTranche tranche = (EpmTBudTranche) tmp;
                            prefix = tranche.getEpmTRefNatureTranche().getLibelle();
                        }
                        resultat = Util.lancerMethode(tmp, null, methode);
                        if (resultat instanceof Set) {
                            break;
                        }
                        if (resultat != null) {
                            if (resultat instanceof Integer) {
                                sousListe.add(String.valueOf(((Integer) resultat).intValue()));
                            } else {
                                if (resultat.toString().equals("")) {
                                    sousListe.add(prefix);
                                } else {
                                    resultat = Util.remplaceTableauPourSauvegarde((String) resultat);
                                    if (prefix.equals("")) {
                                        sousListe.add(resultat);
                                    } else {
                                        sousListe.add(prefix + "-" + resultat);
                                    }
                                }
                            }
                        } else {
                            sousListe.add("-");
                        }
                    }
                    if (!(resultat instanceof Set)) {
                        if (!iterator) {
                            liste.add("-");
                        } else {
                            liste.add(sousListe);
                        }
                        dejaResultat = true;
                    }
                    objet = resultat;
                    continue;
                }
                resultat = Util.lancerMethode(objet, null, methode);
                objet = resultat;
            }
            if (!dejaResultat) {
                if (resultat != null) {
                    if (resultat instanceof String) {
                        resultat = Util.remplaceTableauPourSauvegarde((String) resultat);
                        if (resultat.equals("")) {
                            liste.add("-");
                        } else {
                            liste.add(resultat);
                        }
                    } else {
                        liste.add(resultat);
                    }
                } else {
                    liste.add("-");
                }
            }
        }
        if (premiereLigne && liste.size() == 1) {
            liste.add("-");
        }

        return liste;
    }

    private List<Object> getTrancheLignes(final List<EpmTBudLot> lots, final String titre, final int id) {
        List<Object> liste = new ArrayList<>();
        liste.add(titre);

        for (EpmTBudLot budLot : lots) {
            if (id > 0 && id != budLot.getId())
                continue;

            List<String> tmp = new ArrayList<>();
            if (budLot.getEpmTBudLotOuConsultation() != null && budLot.getEpmTBudLotOuConsultation().getEpmTLotTechniques() != null) {
                List<EpmTLotTechnique> listeLotsTechniques = new ArrayList<>(budLot.getEpmTBudLotOuConsultation().getEpmTLotTechniques());
                Collections.sort(listeLotsTechniques);

                for (EpmTLotTechnique lotTechnique : listeLotsTechniques) {
                    StringBuilder resultat = new StringBuilder();
                    String separateur = "";
                    for (EpmTBudTranche tranche : lotTechnique.getEpmTBudTranches()) {
                        if (tranche.getCodeTranche().length() == 0)
                            resultat.append(separateur).append("TF");
                        else
                            resultat.append(separateur).append("TO-").append(tranche.getCodeTranche());
                        separateur = "; ";
                    }
                    tmp.add(Util.remplaceTableauPourSauvegarde(resultat.toString()));
                }
            }
            if (tmp.isEmpty())
                tmp.add("-");

            liste.add(tmp);
        }
        if (liste.size() == 1)
            liste.add("-");
        return liste;
    }

    private List<Object> getTrancheLigne(final List<EpmTLotTechnique> lotsTechniques, final String titre) {
        List<Object> liste = new ArrayList<>();
        liste.add(titre);

        List<String> tmp = new ArrayList<>();
        for (EpmTLotTechnique lotTechnique : lotsTechniques) {
            StringBuilder resultat = new StringBuilder();
            String separateur = "";
            for (EpmTBudTranche tranche : lotTechnique.getEpmTBudTranches()) {
                if (tranche.getCodeTranche().equals(""))
                    resultat.append(separateur).append("TF");
                else
                    resultat.append(separateur).append("TO-").append(tranche.getCodeTranche());
                separateur = "; ";
            }
            tmp.add(resultat.toString());
        }
        if (tmp.isEmpty())
            tmp.add("-");

        liste.add(tmp);
        return liste;
    }

    /**
     * Retourne la liste des valeurs des paires type par rapport à la clef passé en paramètre en base.
     * Exemple de configuration en base : valeursExternes()[clef]
     * @param id l'identifiant du lot
     * @param clef la clef des valeurs de conditionnement externe
     * @return une liste de valeurs séparée par des virgules
     */
    public final String getValeursExternes(final int id, final String clef) {
        StringBuilder resultat = new StringBuilder();
        if (consultation != null) {
            Set<EpmTValeurConditionnementExterne> valeurConditionnementExternes = new HashSet<EpmTValeurConditionnementExterne>();
            if (id != -1) {
                epmTBudLots = consultation.getEpmTBudLots();
                for (EpmTBudLot epmTBudLot : epmTBudLots) {
                    if (epmTBudLot.getId() == id) {
                        epmTBudLotOuConsultation = epmTBudLot.getEpmTBudLotOuConsultation();
                        valeurConditionnementExternes = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes();
                        break;
                    }
                }
            } else {
                epmTBudLotOuConsultation = consultation.getEpmTBudLotOuConsultation();
                valeurConditionnementExternes = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes();
            }

            for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : valeurConditionnementExternes)
                if (epmTValeurConditionnementExterne.getClef().equals(clef))
                    resultat.append(epmTValeurConditionnementExterne.getValeur()).append(",");

            if (resultat.length() > 0) {
                resultat.deleteCharAt(resultat.length() - 1);
            } else {
                Set<EpmTValeurConditionnementExterneComplexe> valeurConditionnementExternesComplexe = new HashSet<>();
                if (id != -1) {
                    epmTBudLots = consultation.getEpmTBudLots();
                    for (EpmTBudLot epmTBudLot : epmTBudLots) {
                        if (epmTBudLot.getId() == id) {
                            epmTBudLotOuConsultation = epmTBudLot.getEpmTBudLotOuConsultation();
                            valeurConditionnementExternesComplexe = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe();
                            break;
                        }
                    }
                } else {
                    epmTBudLotOuConsultation = consultation.getEpmTBudLotOuConsultation();
                    valeurConditionnementExternesComplexe = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe();
                }

                for (EpmTValeurConditionnementExterneComplexe epmTValeurConditionnementExterneComplexe : valeurConditionnementExternesComplexe)
                    if (epmTValeurConditionnementExterneComplexe.getIdObjetComplexe().equals(clef))
                        for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : epmTValeurConditionnementExterneComplexe.getEpmTValeurConditionnementExternes())
                            resultat.append("[").append(epmTValeurConditionnementExterne.getClef())
                                    .append(",").append(epmTValeurConditionnementExterne.getValeur())
                                    .append("],");

                // Recherche dans les valeurs liées directement à la consultation 
                if (listEpmTValeurConditionnementExterneComplexes != null)
                    for (EpmTValeurConditionnementExterneComplexe epmTValeurConditionnementExterneComplexe : listEpmTValeurConditionnementExterneComplexes)
                        if (epmTValeurConditionnementExterneComplexe.getIdObjetComplexe().equals(clef))
                            for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : epmTValeurConditionnementExterneComplexe.getEpmTValeurConditionnementExternes())
                                resultat.append("[").append(epmTValeurConditionnementExterne.getClef())
                                        .append(",").append(epmTValeurConditionnementExterne.getValeur())
                                        .append("],");

                if (listEpmTValeurConditionnementExterne != null)
                    for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : listEpmTValeurConditionnementExterne)
                        if (epmTValeurConditionnementExterne.getClef().equals(clef))
                            resultat.append(epmTValeurConditionnementExterne.getValeur()).append(",");
            }

            if (resultat.length() > 0 && resultat.charAt(resultat.length() - 1)==',')
                resultat.deleteCharAt(resultat.length() - 1);
        }
        return resultat.toString();
    }

    public final String getTestTableauDynamique(){
        return "";
    }

    /**
     * @return injecté par spring
     */
    public final EpmTConsultation getConsultation() {
        return consultation;
    }

    private <T> List<T> tri(final Iterator<T> it) {
        List<T> liste = new ArrayList<T>();
        while (it.hasNext())
            liste.add(it.next());
        Collections.sort((List) liste);
        return liste;
    }

	/**
	 * @return Retourne "avec signature électronique" ou "sans signature électronique",
	 * selon si la signature est activé ou pas
	 */
	public final String getSignature() {
		String result = null;

		if ( null == getConsultation().getSignature() ) {
			getConsultation().setSignature((short)0);
		}

		switch ( getConsultation().getSignature() ) {
			case 1:
				result = messageSource.getMessage("tableau.signature.requise", null, "La signature électronique est requise pour la réponse des soumissionnaires.", Locale.FRENCH);
				break;
			case 2:
				result = messageSource.getMessage("tableau.signature.autorisee", null, "La signature électronique est autorisée pour la réponse des soumissionnaires.", Locale.FRENCH);
				break;
			case 0: default:
				result = messageSource.getMessage("tableau.signature.non-requise", null, "La signature électronique n'est pas requise pour la réponse des soumissionnaires.", Locale.FRENCH);
				break;
		}


		return result;
	}

    /**
     * @return Retourne "avec acte d'engagement à signature propre" ou "sans acte d'engagement à signature propre",
     * selon s'il y a un acte d'engagement a signature propre.
     */
    public final String getSignatureEngagement() {
        String message = "";
        if (consultation != null) {
            if (getConsultation().isSignatureEngagement())
                message =  messageSource.getMessage("tableau.avec.siganture.engagement", null, Locale.FRENCH);
            else
                message =  messageSource.getMessage("tableau.sans.siganture.engagement", null, Locale.FRENCH);
        }
        return message;
    }

    /**
     * Retourner la date limite de remise de pli
     */
    public final String getDateRemisePlis() {
        String resultat = "";
        SimpleDateFormat formatDateLimite = new SimpleDateFormat(Constantes.DATE_FORMAT_DD_MM_YYYY_HH_mm);

        if (dateRemisePlis != null)
            resultat = formatDateLimite.format(dateRemisePlis);
        return resultat;
    }

    /**
     * @param valeur Acces aux fichiers de libelle (injection Spring indirecte).
     */
    public final void setMessageSource(final ResourceBundleMessageSource valeur) {
        this.messageSource = valeur;
    }

}
