package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTRefValeurTypeClause;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * author nty
 */
public class ClauseValeurHeriteeForm {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attribut texte Fixe Avant.
     */
    private String textFixeAvant;

    /**
     * Attribut reference Valeur de Type Clause.
     */
    private String refValeurTypeClause;
    private int idRefValeurTypeClause;

    /**
     * Attribut texte Fixe Apres.
     */
    private String textFixeApres;
    private String texteVariable;
    private fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction tableauRedaction;
    /**
     * Attribut saut Texte Fixe Avant.
     */
    private String sautTextFixeAvant;
    /**
     * Attribut saut Texte Fixe Apres.
     */
    private String sautTextFixeApres;

    /**
     * Attribut collection de Reference Valeur Heritee.
     */
    private List<EpmTRefValeurTypeClause> listRefValeursTypeClause;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    /**
     * Constructeur de la classe .
     */
    public ClauseValeurHeriteeForm() {
        this.reset();
    }

    /**
     * Cette méthode permet d'initialiser le formulaire.
     */
    public void reset() {
        textFixeAvant = "";
        refValeurTypeClause = "";
        textFixeApres = "";
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
    }

    public int getIdRefValeurTypeClause() {
        return idRefValeurTypeClause;
    }

    public void setIdRefValeurTypeClause(int idRefValeurTypeClause) {
        this.idRefValeurTypeClause = idRefValeurTypeClause;
    }

    /*
     * methode validate () pour valider les données du formulaire
     */
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        ActionErrors erreurs = new ActionErrors();

        if (erreurs.isEmpty())
            request.setAttribute("previsualisation", "true");

        if (refValeurTypeClause == null || refValeurTypeClause.isEmpty() || refValeurTypeClause.equals("0"))
            erreurs.add("refValeurTypeClause", new ActionMessage("redaction.msg.global.erreur"));

        return erreurs;
    }

    /**
     * @return texte Fixe Avant
     */
    public final String getTextFixeAvant() {
        return textFixeAvant;
    }

    /**
     * @param textFixeAvant initialiser l'attribut textFixeAvant.
     */
    public final void setTextFixeAvant(final String textFixeAvant) {
        this.textFixeAvant = textFixeAvant;
    }

    /**
     * @return référence d'une valeur de type clause.
     */
    public final String getRefValeurTypeClause() {
        return refValeurTypeClause;
    }

    /**
     * @param refValeurTypeClause pour initialiser la référence d'une valeur de type clause.
     */
    public final void setRefValeurTypeClause(final String refValeurTypeClause) {
        this.refValeurTypeClause = refValeurTypeClause;
    }

    /**
     * @return texte Fixe Apres
     */
    public final String getTextFixeApres() {
        return textFixeApres;
    }

    /**
     * @param textFixeApres pour initialiser l'attribut textFixeApres.
     */
    public final void setTextFixeApres(final String textFixeApres) {
        this.textFixeApres = textFixeApres;
    }

    public final String getSautTextFixeAvant() {
        return sautTextFixeAvant;
    }

    public final void setSautTextFixeAvant(final String sautTextFixeAvant) {
        this.sautTextFixeAvant = sautTextFixeAvant;
    }

    public final String getSautTextFixeApres() {
        return sautTextFixeApres;
    }

    public final void setSautTextFixeApres(final String sautTextFixeApres) {
        this.sautTextFixeApres = sautTextFixeApres;
    }

    public String getTexteVariable() {
        return texteVariable;
    }

    public void setTexteVariable(String texteVariable) {
        this.texteVariable = texteVariable;
    }

    public TableauRedaction getTableauRedaction() {
        return tableauRedaction;
    }

    public void setTableauRedaction(TableauRedaction tableauRedaction) {
        this.tableauRedaction = tableauRedaction;
    }

    /**
     * @return une collection de références de valeurs de type clause.
     */
    public final List<EpmTRefValeurTypeClause> getListRefValeursTypeClause() {
        return listRefValeursTypeClause;
    }

    /**
     * @param listRefValeursTypeClause pour positionner la collection de références de valeurs de type clause.
     */
    public final void setListRefValeursTypeClause(final List<EpmTRefValeurTypeClause> listRefValeursTypeClause) {
        this.listRefValeursTypeClause = listRefValeursTypeClause;
    }

    /**
     * @return the clauseSelectionnee
     */
    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    /**
     * @param clauseSelectionnee the clauseSelectionnee to set
     */
    public final void setClauseSelectionnee(final String clauseSelectionnee) {
        this.clauseSelectionnee = clauseSelectionnee;
    }

}
