<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<form action="clause${actionEditeur}Liste.epm" id="formulaireRechercheClauses" name="formulaireRechercheClauses" method="post">

    <c:set var="nombreResultatAAfficherParDefaut" value="10" />
    <c:set var="nameTableauPagine" value="frmRechercherClause" />
    <c:set var="urlTableauPagine" value="clause${actionEditeur}Liste.epm" />
    <c:if test="${actionPreferences}">
        <c:set var="nameTableauPagine" value="frmParametrageClause" />
        <c:set var="urlTableauPagine" value="parametrageListe.epm" />
        <c:if test="${!empty accesModeSaaS}">
            <c:set var="urlTableauPagine" value="externeParametrageListe.epm" />
        </c:if>
    </c:if>

    <atexo:tableauPagine id="clauseListe" name="${nameTableauPagine}" url="${urlTableauPagine}" property="listeClause"
                         elementAfficher="10,20,40,60,100" nombreResultatAAfficherParDefaut="${nombreResultatAAfficherParDefaut}">
        <input id="listeProprietes" type="hidden" name="listeProprietes" value="id;idStatutRedactionClausier" />
        <input id="typeSubmit" type="hidden" name="typeSubmit" />
        <input id="intervalleSubmit" type="hidden" name="intervalle" value="${nombreResultatAAfficherParDefaut}" />
        <input id="pageSubmit" type="hidden" name="index" />
        <input id="proprieteSubmit" type="hidden" name="property" />
        <input id="hiddenAction" type="hidden" name="hiddenAction" /> <!-- type d'action � effectuer -->
        <div class="spacer"></div>

        <div class="line">
            <h2>
                <logic:empty name="clauseListe">
                    <bean:message key="redaction.txt.aucun" />
                </logic:empty>
                <logic:notEmpty name="clauseListe">
                    <bean:message key="redaction.txt.resultats" />
                    <c:out value="${requestScope.NB_RESULT}" />
                </logic:notEmpty>
            </h2>
            <div class="partitioner">
                <div class="intitule">
                    <bean:message key="redaction.txt.afficher" />
                </div>
                <atexo:intervalle />
                <div class="intitule2">
                    <bean:message key="redaction.txt.resultatParPage" />
                    &nbsp;&nbsp;
                    <atexo:navigationListe suivant="false" key="redaction.txt.precedent" />
                    &nbsp;
                    <atexo:compteurPage />
                    &nbsp;
                    <atexo:navigationListe suivant="true" key="redaction.txt.suivant" />
                </div>
            </div>
        </div>
        <!--Fin partitioner-->

        <!--Debut Resultats Recherche Consultation-->
        <div class="results-list">
            <table id="resultsTable">
                <thead>
                <tr>
                    <th colspan="8" class="top">
                        <img src="<atexo:href href='images/table-results-top-left.gif'/>" alt="" title="" class="left" />
                        <img src="<atexo:href href='images/table-results-top-right.gif'/>" alt="" title="" class="right" />
                    </th>
                </tr>
                <tr>
                    <atexo:href href="" var="hrefRacine">
                        <c:if test="${!actionPreferences}">
                            <th class="select-col">
                                <input type="checkbox" onclick="isCheckedCheckAll(this,document.getElementsByName('identifiants'));"
                                       title="Tous" name="checkbox1" />
                            </th>
                        </c:if>
                        <th class="reference">
                            <div>
                                <bean:message key="RechercherClause.txt.reference" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="reference" />
                            </div>
                            <div>
                                <bean:message key="RechercherClause.txt.auteur" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="clauseEditeur" />
                            </div>
                            <div>
                                <bean:message key="RechercherClause.txt.statut" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="reference" />
                            </div>
                        </th>
                        <th class="theme"><div>
                            <bean:message key="RechercherClause.txt.theme" />
                            <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="epmTRefThemeClause.libelle" />
                        </div></th>

                        <th class="contenu-long">
                            <div>
                                <!-- TODO : Changer le crit�re de tri -->
                                <c:if test="${editeur != null && editeur == true }">
                                    <bean:message key="RechercherClause.txt.nbSurcharches" />
                                    <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="nombreSurcharges" />)/
                                </c:if>
                                <bean:message key="RechercherClause.txt.contenu" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="" />
                                <!-- contenu -->
                            </div>
                        </th>

                        <th class="col-80">
                            <div>
                                <!-- TODO : Changer le crit?re de tri -->
                                <bean:message key="RechercherClause.txt.creeeLe" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="" />
                                <br />
                                <bean:message key="RechercherClause.txt.modifieeLe" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="" />
                                <br />
                                <bean:message key="RechercherClause.txt.version" />
                                <atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="" />
                            </div>
                        </th>
                        <c:set var="clazz" value="col-150" />
                        <c:if test="${actionPreferences}">
                            <c:set var="clazz" value="col-50" />
                        </c:if>
                        <th class="actions ${clazz} align-left">
                            <div>
                                <bean:message key="RechercherClause.txt.actions" />
                            </div>
                        </th>
                    </atexo:href>
                </tr>
                </thead>
                <logic:notEmpty name="clauseListe">
                    <logic:iterate name="clauseListe" id="clause" indexId="index"><!-- type="fr.paris.epm.redaction.metier.objetValeur.canevas.Clause">-->
                        <%--@elvariable id="clauses" type="java.util.List<fr.paris.epm.redaction.metier.objetValeur.canevas.Clause>"--%>
                        <%--@elvariable id="clause" type="fr.paris.epm.redaction.metier.objetValeur.canevas.Clause"--%>
                        <!-- idClause : ${clause.idClause} ; idPublication : ${clause.idPublication} -->

                        <c:set var="style" value="" />
                        <c:if test="${index % 2 == 0}">
                            <c:set var="style" value="on" />
                        </c:if>
                        <c:if test="${!clause.actif}">
                            <c:set var="style" value="off" />
                        </c:if>

                        <tr class="${style}" id="inactiveClause">
                            <c:set var="checkboxActive">false</c:set>
                            <c:if test="${editeur != null && !editeur && clause.clauseEditeur}">
                                <c:set var="checkboxActive">true</c:set>
                            </c:if>
                            <c:if test="${!actionPreferences}">
                                <td class="select-col">
                                    <atexo:checkBox property="identifiants" value="${clause.idClause};${clause.idStatutRedactionClausier}"
                                                    styleClass="auto" disabled="${checkboxActive}" />
                                </td>
                            </c:if>
                            <td class="reference">
                                <div>
                                    <c:out value="${clause.reference}" />
                                </div>
                                <c:out value="${clause.auteur}" />
                                <div class="spacer-mini"></div>
                                <c:if test="${clause.idStatutRedactionClausier == 1}">
                                    <div>
                                        <img title="Brouillon" alt="Brouillon" src="<atexo:href href='images/clause-statut-1.gif'/>" />
                                    </div>
                                </c:if>
                                <c:if test="${clause.idStatutRedactionClausier == 2}">
                                    <div>
                                        <img title="A valider" alt="A valider" src="<atexo:href href='images/clause-statut-2.gif'/>" />
                                    </div>
                                </c:if>
                                <c:if test="${clause.idStatutRedactionClausier == 3}">
                                    <div>
                                        <img title="Validée" alt="Validée" src="<atexo:href href='images/clause-statut-3.gif'/>" />
                                    </div>
                                </c:if>
                            </td>
                            <td class="theme">
                                <div><nested:write name="clause" property="theme" /></div>
                            </td>
                            <td class="contenu-long">
                                <div>
                                    <c:if test="${editeur != null && editeur == true }">
                                        (<c:out value="${clause.nombreSurcharges}" />)&nbsp;
                                    </c:if>
                                    <nested:write name="clause" property="contenu" filter="false" />
                                </div>
                            </td>
                            <td class="col-80">
                                <div><c:out value="${clause.dateCreation}" /></div>
                                <div><c:out value="${clause.dateModification}" /></div>
                                <div><c:out value="${clause.lastVersion}" /></div>
                            </td>

                            <!-- verification si les boutons de validation/refus doivent s'afficher -->
                            <c:set var="isBoutonValidationVisible">false</c:set>
                            <c:if test="${editeur != null && editeur && clause.clauseEditeur && roleValiderClauseEditeur}">
                                <c:set var="isBoutonValidationVisible">true</c:set>
                            </c:if>
                            <c:if test="${editeur != null && !editeur  && !clause.clauseEditeur && roleValiderClauseClient}">
                                <c:set var="isBoutonValidationVisible">true</c:set>
                            </c:if>

                            <c:if test="${!actionPreferences}">
                                <td class="actions col-160 action-inline">

                                    <c:set var="urlParameters" value="idClause=${clause.idClause}" />
                                    <c:if test="${not empty clause.idPublication}">
                                        <c:set var="urlParameters" value="${urlParameters}&idPublication=${clause.idPublication}" />
                                    </c:if>
                                    <c:set var="urlEditeur" value="" />
                                    <c:if test="${editeur}">
                                        <c:set var="urlEditeur" value="Editeur" />
                                    </c:if>

                                    <c:if test="${clause.idStatutRedactionClausier == 1}">
                                        <c:choose>
                                            <c:when test="${isBoutonValidationVisible}">
                                                <a href="javascript:demanderValidationClause('${clause.idClause}', '${clause.idPublication}', '${editeur}');">
                                                    <img src="<atexo:href href='images/bouton-demande-validation.gif'/>" alt="Demander validation" title="Demander validation" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="empty-action">-</div>
                                            </c:otherwise>
                                        </c:choose>
                                        <div class="empty-action">-</div>
                                    </c:if>
                                    <c:if test="${clause.idStatutRedactionClausier == 2}">
                                        <c:choose>
                                            <c:when test="${isBoutonValidationVisible}">
                                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.validationClause&retour=validerClause(\'${clause.idClause}\', \'${clause.idPublication}\', \'${editeur}\')');"
                                                   id="validerClause" title="Valider">
                                                    <img src="<atexo:href href='images/bouton-valider.gif'/>" alt="Valider" title="Valider" />
                                                </a>
                                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.refuserClause&retour=refuserClause(\'${clause.idClause}\', \'${clause.idPublication}\', \'${editeur}\')');"
                                                   id="refuserClause" title="Refuser / Invalider">
                                                    <img src="<atexo:href href='images/bouton-refuser.gif'/>" alt="Refuser" title="Refuser" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="empty-action">-</div>
                                                <div class="empty-action">-</div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                    <c:if test="${clause.idStatutRedactionClausier == 3}">
                                        <div class="empty-action">-</div>
                                        <c:choose>
                                            <c:when test="${isBoutonValidationVisible}">
                                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.refuserClause&retour=refuserClause(\'${clause.idClause}\', \'${clause.idPublication}\', \'${editeur}\')');"
                                                   id="refuserClause" title="Refuser / Invalider">
                                                    <img src="<atexo:href href='images/bouton-refuser.gif'/>" alt="Refuser" title="Refuser" />
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="empty-action">-</div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>

                                    <!-- Bouton de prévisualisation -->
                                    <c:choose>
                                        <c:when test="${clause.clauseEditeur == true}">
                                            <a href="javascript:popUp('PrevisualiserClauseProcess${urlEditeur}.epm?${urlParameters}&clauseEditeur=true','yes');">
                                                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="Prévisualiser" title="Prévisualiser" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="javascript:popUp('PrevisualiserClauseProcess${urlEditeur}.epm?${urlParameters}','yes');">
                                                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="Prévisualiser" title="Prévisualiser" />
                                            </a>
                                        </c:otherwise>
                                    </c:choose>

                                    <!-- Bouton modification -->
                                    <c:choose>
                                        <c:when test="${editeur != null && editeur == true}">
                                            <a href="ModificationClauseEditeurInit.epm?${urlParameters}&typeAction=M">
                                                <img src="<atexo:href href='images/bouton-modifier.gif'/>" alt="Modifier" title="Modifier" />
                                            </a>
                                            <a href="ModificationClauseEditeurInit.epm?${urlParameters}&typeAction=D">
                                                <img src="<atexo:href href='images/bouton-picto-dupliquer.gif'/>" alt="Dupliquer" title="Dupliquer" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${clause.clauseEditeur == true}">
                                                    <a href="ModificationClauseInit.epm?${urlParameters}&typeAction=M&clauseEditeur=true">
                                                        <img src="<atexo:href href='images/bouton-parametrer.gif'/>" alt="Surcharger" title="Surcharger" />
                                                    </a>
                                                    <a href="ModificationClauseInit.epm?${urlParameters}&typeAction=D&clauseEditeur=true">
                                                        <img src="<atexo:href href='images/bouton-picto-dupliquer.gif'/>" alt="Dupliquer" title="Dupliquer" />
                                                    </a>
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="ModificationClauseInit.epm?${urlParameters}&typeAction=M">
                                                        <img src="<atexo:href href='images/bouton-modifier.gif'/>" alt="Modifier" title="Modifier" />
                                                    </a>
                                                    <a href="ModificationClauseInit.epm?${urlParameters}&typeAction=D">
                                                        <img src="<atexo:href href='images/bouton-picto-dupliquer.gif'/>" alt="Dupliquer" title="Dupliquer" />
                                                    </a>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>

                                    <!-- Bouton suppression -->
                                    <c:choose>
                                        <c:when test="${editeur == false && clause.clauseEditeur == false || editeur == true && clause.clauseEditeur == true}">
                                            <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.supprimerClause&retour=supprimerClause(\'${clause.idClause}\', \'${clause.idPublication}\', \'${editeur}\')');"
                                               id="supprimerClause" title="Demander validation">
                                                <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>" alt="Supprimer" title="Supprimer" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="empty-action">-</div>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </c:if>
                            <c:if test="${actionPreferences}">
                                <c:set var="action" value="M" />
                                <c:if test="${clause.auteur eq 'Editeur surchargée'}">
                                    <c:set var="action" value="S" />
                                </c:if>
                                <td class="actions col-50 action-inline">
                                    <a href="javascript:popUp('PrevisualiserClauseProcess.epm?idClause=${clause.idClause}&idPublication=${clause.idPublication}','yes');">
                                        <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="Prévisualiser" title="Prévisualiser" />
                                    </a>
                                    <c:choose>
                                        <c:when test="${clause.clauseEditeur == true}">
                                            <a href="ParametrageClauseProcess.epm?idClause=${clause.idClause}&typeAction=${action}&clauseEditeur=true">
                                                <img src="<atexo:href href='images/bouton-parametrer.gif'/>" alt="Paramétrage" title="Paramétrage" />
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="ParametrageClauseProcess.epm?idClause=${clause.idClause}&typeAction=${action}">
                                                <img src="<atexo:href href='images/bouton-parametrer.gif'/>" alt="Paramétrage" title="Paramétrage" />
                                            </a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </c:if>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
            </table>
        </div>
        <!--Fin Resultats Recherche Consultation-->
        <logic:notEmpty name="clauseListe">
            <div class="line">
                <div class="partitioner">
                    &nbsp;&nbsp;
                    <atexo:navigationListe suivant="false" key="redaction.txt.precedent" />
                    &nbsp;
                    <atexo:compteurPage />
                    &nbsp;
                    <atexo:navigationListe suivant="true" key="redaction.txt.suivant" />
                </div>
            </div>

            <!--Debut legende clauses actives/inactives-->
            <div class="legende-clause">
                <span class="active">
                    <bean:message key="RechercherClause.txt.clauseActive" />
                </span>
                <span class="inactive">
                    <bean:message key="RechercherClause.txt.clauseInactive" />
                </span>
            </div>
            <!--Fin legende clauses actives/inactives-->

            <c:if test="${!actionPreferences}">
                <div class="layer">
                    <div class="top">
                        <span class="left">&nbsp;</span><span class="right">&nbsp;</span>
                    </div>

                    <div class="content">
                        <div class="float-left">
                            <strong>Actions groupées</strong>
                        </div>
                        <div class="float-right">
                            <c:if test="${editeur && roleValiderClauseEditeur || !editeur && roleValiderClauseClient}">
                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.demanderValidation&retour=demanderValidationClauses(${editeur})');"
                                   id="demanderValidationClauses" title="Demander validation">
                                    <img src="<atexo:href href='images/bouton-demande-validation.gif'/>" alt="Demander validation" title="Demander validation">
                                </a>
                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.refuserValidation&retour=refuserValidationClauses(${editeur})');"
                                   id="refuserValidationClause" title="Demander validation">
                                    <img src="<atexo:href href='images/bouton-refuser.gif'/>" alt="Invalider/Refuser" title="Invalider/Refuser">
                                </a>
                                <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.validation&retour=validerClauses(${editeur})');"
                                   id="validerClauses" title="Demander validation">
                                    <img src="<atexo:href href='images/bouton-valider.gif'/>" alt="Valider" title="Valider">
                                </a>
                            </c:if>
                            <a href="javascript:popUp('confirmationGenerique.epm?message=popupConfirmation.supprimer&retour=supprimerClauses(${editeur})');"
                               id="supprimerClauses" title="Demander validation">
                                <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>" alt="Supprimer" title="Supprimer">
                            </a>
                        </div>
                    </div>

                    <div class="bottom">
                        <span class="left">&nbsp;</span><span class="right">&nbsp;</span>
                    </div>
                </div>
            </c:if>
        </logic:notEmpty>

    </atexo:tableauPagine>
</form>
