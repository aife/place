package fr.paris.epm.redaction.presentation.forms;

public class ClauseFormulaire {


    private int id;
    private boolean precochee;
    private String defaultValeur;
    private String tailleChamps;
    private Integer numFormulation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrecochee() {
        return precochee;
    }

    public void setPrecochee(boolean precochee) {
        this.precochee = precochee;
    }

    public String getDefaultValeur() {
        return defaultValeur;
    }

    public void setDefaultValeur(String defaultValeur) {
        this.defaultValeur = defaultValeur;
    }

    public Integer getNumFormulation() {
        return numFormulation;
    }

    public void setNumFormulation(Integer numFormulation) {
        this.numFormulation = numFormulation;
    }

    public String getTailleChamps() {
        return tailleChamps;
    }

    public void setTailleChamps(String tailleChamps) {
        this.tailleChamps = tailleChamps;
    }
}
