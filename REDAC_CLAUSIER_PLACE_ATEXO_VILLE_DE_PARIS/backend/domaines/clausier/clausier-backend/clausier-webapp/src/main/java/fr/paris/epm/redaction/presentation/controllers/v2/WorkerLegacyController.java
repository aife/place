package fr.paris.epm.redaction.presentation.controllers.v2;

import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.worker.RsemTaskWorker;
import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;

import static fr.paris.epm.global.metier.BaseDAO.logger;

@RestController
@RequestMapping("/v2/worker")
public class WorkerLegacyController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(WorkerLegacyController.class);
    private final RsemTaskWorker rsemTaskWorker;


    public WorkerLegacyController(RsemTaskWorker rsemTaskWorker) {

        this.rsemTaskWorker = rsemTaskWorker;

    }


    @GetMapping(value = "/{idTask}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Ajax findTask(@PathVariable("idTask") String idTask,
                                 @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("/verifierRsemTask.json -> " + idTask);

        try {
            Object result = rsemTaskWorker.getResult(idTask);
            if (result == null) {
                logger.info("La tache {} est encore en train d'execution", idTask);
                return Ajax.emptyResponse(); // tache n'est pas encore finie
            }
            String infoShowResult = result.toString();
            if (infoShowResult.length() > 100)
                infoShowResult = infoShowResult.substring(0, 90) + " ...";
            logger.info("Result d'execution de la tache {} : {}", idTask, infoShowResult);
            return Ajax.successResponse(rsemTaskWorker.getResultHref(idTask));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return Ajax.errorResponse(ex.getMessage());
        }
    }

    @GetMapping(value = "/{idTask}/download")
    public void downloadTaskResult(final HttpServletResponse response,
                                   @PathVariable("idTask") String idTask,
                                    @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) throws Exception {
        logger.info("/downloadTaskResult.htm -> " + idTask);

        Object result = rsemTaskWorker.getResult(idTask);
        FileTmp fileTmp = (FileTmp) result;

        // Entêtes
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileTmp.getNomFichier());
        response.setContentLength((int) fileTmp.length());

        // Flux de sortie
        OutputStream fluxServlet = response.getOutputStream();
        FileInputStream fluxFichier = new FileInputStream(fileTmp);
        IOUtils.copy(fluxFichier, fluxServlet);
        IOUtils.closeQuietly(fluxServlet);
        IOUtils.closeQuietly(fluxFichier);
        response.flushBuffer();
    }


}
