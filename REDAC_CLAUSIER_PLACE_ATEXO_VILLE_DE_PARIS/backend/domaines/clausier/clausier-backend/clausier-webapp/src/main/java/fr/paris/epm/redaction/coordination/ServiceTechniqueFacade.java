package fr.paris.epm.redaction.coordination;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;

/**
 * Facade d'acces aux services techniques (stockage des utilisateurs
 * externes...).
 * @author RVI
 */
public interface ServiceTechniqueFacade {

    /**
     * Retourne l'utilisateur externe et la consultation associe a l'identifiant
     * externe en parametre.
     */
    UtilisateurExterneConsultation getUtilisateurExterneConsultation(final String identifiantExterne);

}
