package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeClause;
import fr.paris.epm.redaction.commun.Constante;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;

/**
 * Formulaire Parametrage Clause Texte ChampPrevalorise .
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ParametrageClauseTextChampPrevaloriseForm extends ActionForm {
    /**
     * Texte fixe avant.
     */
    private String texteFixeAvant = "";
    /**
     * Texte fixe après.
     */
    private String texteFixeApres = "";
    /**
     * marqueur de serialisation .
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut valeur par default .
     */
    private String defaultValue;
    /**
     * Attribut valeur direction .
     */
    private String directionValue;
    /**
     * Attribut valeur agent .
     */
    private String agentValue;
    /**
     * Attribut taille champ (Long,Moyen,Cours) .
     */
    private String tailleChamp;
    /**
     * Attribut choix de parametre .
     */
    private String choixParam;
    /**
     * Attribut type de clause .
     */
    private String typeClause;
    
    /**
     * Attribut type de clause .
     */
    private Integer idTypeClause;
    
    /**
     * Attribut reference .
     */
    private String reference;

    /**
     * @return texte Fixe Avant
     */
    public final String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    /**
     * @param valeur positionne le texte Fixe Avant.
     */
    public final void setTexteFixeAvant(final String valeur) {
        texteFixeAvant = valeur;
    }

    /**
     * @return texte Fixe Apres
     */
    public final String getTexteFixeApres() {
        return texteFixeApres;
    }

    /**
     * @param valeur positionne le texte Fixe Apres.
     */
    public final void setTexteFixeApres(final String valeur) {
        texteFixeApres = valeur;
    }

    /**
     * Cette méthode permet d'initialiser le formulaire .
     */
    public void reset() {
        defaultValue = null;
        choixParam = null;
        directionValue = null;
        agentValue = null;
        typeClause = null;
        reference = null;
        texteFixeApres = null;
        texteFixeAvant = null;
        idTypeClause = null;
    }

    /**
     * Constructeur de la classe ParametrageClauseTextChampPrevaloriseForm.
     */
    public ParametrageClauseTextChampPrevaloriseForm() {
        reset();
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors erreurs = new ActionErrors();
        if (idTypeClause != null && idTypeClause == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            if (Constante.PRE_DIRECTION.equals(choixParam) && (directionValue == null || directionValue.isEmpty())) {
                erreurs.add("valeurDefautObligatoireVide", new ActionMessage("ParametrageClauseTextChampPrevalorise.erreur.valeurDefautObligatoire"));
            } else if (Constante.PRE_AGENT.equals(choixParam) && (agentValue == null || agentValue.isEmpty())) {
                erreurs.add("valeurDefautObligatoireVide", new ActionMessage("ParametrageClauseTextChampPrevalorise.erreur.valeurDefautObligatoire"));
            }
        }
        return erreurs;
    }

//    /**
//     * @return objet epmTRoleClausesAgent.
//     */
//    public final EpmTRoleClause getEpmTRoleClausesAgent() {
//        return epmTRoleClausesAgent;
//    }

//    /**
//     * @param valeur initialise l'objet epmTRoleClausesAgent.
//     */
//    public final void setEpmTRoleClausesAgent(final EpmTRoleClause valeur) {
//        epmTRoleClausesAgent = valeur;
//    }

    /**
     * @return objet epmTRoleClausesDefault.
     */

//    public final EpmTRoleClause getEpmTRoleClausesDefault() {
//        return epmTRoleClausesDefault;
//    }

    /**
     * @param valeur positionne l'objet epmTRoleClausesDefault.
     */
//    public final void setEpmTRoleClausesDefault(final EpmTRoleClause valeur) {
//        epmTRoleClausesDefault = valeur;
//    }

    /**
     * @return objet epmTRoleClausesDirection.
     */
//    public final EpmTRoleClause getEpmTRoleClausesDirection() {
//        return epmTRoleClausesDirection;
//    }

    /**
     * @param valeur initialise l'objet epmTRoleClausesDirection.
     */
//    public final void setEpmTRoleClausesDirection(
//            final EpmTRoleClause valeur) {
//        epmTRoleClausesDirection = valeur;
//    }

    /**
     * @return valeur agent.
     */

    public final String getAgentValue() {
        return agentValue;
    }

    /**
     * @param valeur postionne l'objet agent.
     */
    public final void setAgentValue(final String valeur) {
        agentValue = valeur;
    }

    /**
     * @return choix du paramètre.
     */
    public final String getChoixParam() {
        return choixParam;
    }

    /**
     * @param valeur positionne le choix du paramètre.
     */
    public final void setChoixParam(final String valeur) {
        choixParam = valeur;
    }

    /**
     * @return valeur par default .
     */
    public final String getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param valeur positionne la valeur par default.
     */
    public final void setDefaultValue(final String valeur) {
        defaultValue = valeur;
    }

    /**
     * @return valeur direction.
     */
    public final String getDirectionValue() {
        return directionValue;
    }

    /**
     * @param valeur positionne la valeur de direction.
     */
    public final void setDirectionValue(final String valeur) {
        directionValue = valeur;
    }

    /**
     * @return réference .
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @param valeur positionne la réference de la clause.
     */
    public final void setReference(final String valeur) {
        reference = valeur;
    }

    /**
     * @return type de la clause.
     */
    public final String getTypeClause() {
        return typeClause;
    }

    /**
     * @param valeur positionne le type de la clause .
     */
    public final void setTypeClause(final String valeur) {
        typeClause = valeur;
    }

    public Integer getIdTypeClause() {
        return idTypeClause;
    }

    public void setIdTypeClause(Integer valeur) {
        this.idTypeClause = valeur;
    }

    /**
     * @return taille du champ .
     */
    public final String getTailleChamp() {
        return tailleChamp;
    }

    /**
     * @param valeur positionne la taille du champ.
     */
    public final void setTailleChamp(final String valeur) {
        tailleChamp = valeur;
    }

}
