package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasAbstract;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.metier.documentHandler.HeriteConsultation;
import fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;

import java.util.List;

/**
 * Interface qui présente les services offerts par un document.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public interface DocumentRedactionFacade {


    Document miseAjourValeurHerite(Document document, EpmTConsultation consultation, int idLot, final Integer idOrganisme)
            throws TechnicalException, TechnicalNoyauException;

    /**
     * Permet de générer le squelette du document à partir du canevas qui lui ai
     * associé, il faut associer un canevas au document passé en paramètre.
     * @param document         document avec canevas attaché
     * @param user             Utilisateur demandant le service
     * @param idTypeDoc        identifiant du type de document.
     * @param rechercherClause permet de définir quelle version de la clause sera récupérée : la version du canevas, du document ou la dernière saisie en base.
     * @return Document un objet utilisé au niveau de GWT
     */
    Document genererStructureDocument(final Document document, final EpmTUtilisateur user, int idTypeDoc,
                                      final List<ChapitreDocument> chapitresDocumentInitial, final RechercheClauseEnum rechercherClause)
            throws TechnicalException, TechnicalNoyauException;

    /**
     * Génération d'un objet Document à partir d'un canevas.
     */
    Document genererDocumentPourExportCanevas(final int idCanevas, final Integer idPublication, final EpmTUtilisateur user) throws TechnicalException;


    HeriteConsultation initialiserHeriteConsultation(EpmTConsultation consultation);


    EpmTCanevasAbstract chargerCanevas(int utilisateurId, final String refCanevas, final Integer idOrganisme, String plateformeUuid);

}
