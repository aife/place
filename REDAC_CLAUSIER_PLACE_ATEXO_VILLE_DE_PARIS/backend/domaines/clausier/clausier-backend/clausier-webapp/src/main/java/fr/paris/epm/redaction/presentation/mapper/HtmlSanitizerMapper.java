package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.noyau.commun.util.Util;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface HtmlSanitizerMapper {
    default String sanitizeHtmlInput(String untrustedHTML) {
        return Util.sanitizeHtmlInput(untrustedHTML);
    }

}
