<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<logic:notEmpty name="versionChapitres">
    <logic:iterate id="versionChapitre" name="versionChapitres" >
        <ul>
            <li>
                <logic:equal name="premierNiveau" value="true">
                    <strong>${versionChapitre.numero} ${versionChapitre.titre}</strong>
                </logic:equal>
                <logic:equal name="premierNiveau" value="false">
                    <i>${versionChapitre.numero} ${versionChapitre.titre} </i>
                    <logic:notEmpty name="versionChapitre" property="infoBulle">
                    </logic:notEmpty>
                </logic:equal>
                <nested:equal name="versionChapitre" property="infoBulle.actif" value="true">
                    <img src="<atexo:href href='images/picto-info-bulle-transparent-bk.gif'/>"
                         onmouseover="afficheBulle('infos-chapitres${versionChapitre.id}', this)"
                         onmouseout="cacheBulle('infos-chapitres${versionChapitre.id}')"
                         class="picto-info-bulle"  alt="Info-bulle" title="Info-bulle" />
                    <div id="infos-chapitres${versionChapitre.id}" class="info-bulle"
                         onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                        <div><nested:write name="versionChapitre" property="infoBulle.description"/></div>
                    </div>
                </nested:equal>
                <logic:notEmpty name="versionChapitre" property="clauses">
                    <ul class="liste-clause">
                        <logic:iterate id="clause" name="versionChapitre" property="clauses" indexId="indexClause">
                            <logic:equal name="clause" property="actif" value="1" >
                            <li>
                                <span class="ref-clause">
                                    <nested:equal name="clause" property="infoBulle.actif" value="true">
                                        <img src="<atexo:href href='images/picto-info-bulle-transparent-bk.gif'/>"
                                             onmouseover="afficheBulle('infos-clauses${versionChapitre.id}-${indexClause}', this)"
                                             onmouseout="cacheBulle('infos-clauses${versionChapitre.id}-${indexClause}')"
                                             class="picto-info-bulle"  alt="Info-bulle" title="Info-bulle" />
                                        <div id="infos-clauses${versionChapitre.id}-${indexClause}" class="info-bulle"
                                             onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                                            <div><nested:write name="clause" property="infoBulle.description"/></div>
                                        </div>
                                    </nested:equal>
                                    <c:out value="${clause.reference}" />
                                </span>

                                <bean:define id="clauseRef" name="clause" property="reference" />
                                <redaction:clause ref="${clauseRef}">
                                    <bean:define id="typeClause" name="clause" property="epmTRefTypeClause" scope="request" />
                                    <c:set scope="request" var="type" value="${typeClause.id}" />

                                    <c:if test="${requestScope.type >= 2 && requestScope.type <= 5 || requestScope.type == 8 || requestScope.type == 9}">
                                        <c:set var="classClause" value="clause" />
                                    </c:if>
                                    <c:if test="${requestScope.type == 6 || requestScope.type == 7}">
                                        <c:set var="classClause" value="line" />
                                    </c:if>

                                    <div class="${classClause}">
                                        ${clause.texteFixeAvant}
                                        <c:if test="${clause.sautLigneTexteAvant == true}"><br/></c:if>
                                        <redaction:previsualiserPptExtClause name="clause" user="utilisateur"/>
                                        <c:if test="${clause.sautLigneTexteApres == true}"><br/></c:if>
                                        ${clause.texteFixeApres}
                                    </div>
                                </redaction:clause>
                            </li>
                            </logic:equal>
                        </logic:iterate>
                    </ul>
                </logic:notEmpty>
                <logic:notEmpty name="versionChapitre" property="chapitres">
                    <bean:define id="premierNiveau" value="false" toScope="request" />
                    <bean:define id="versionChapitres" name="versionChapitre" property="chapitres" toScope="request" />
                    <jsp:include page="/jsp/previsualisation/canevas/Chapitre.jsp" />
                    <bean:define id="premierNiveau" value="true" toScope="request" />
                </logic:notEmpty>
            </li>
        </ul>
    </logic:iterate>
</logic:notEmpty>
