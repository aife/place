<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>

<%--@elvariable id="chapitre" type="fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument"--%>
<%--@elvariable id="clause" type="fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument"--%>

<span class="clause">
    <span>
        <c:out escapeXml="false" value="${clause.texteFixeAvant}" />
    </span>
    <c:if test="${clause.texteFixeAvantAlaLigne == true}">
        <div class="avec-saut"></div>
    </c:if>
    <span id="recap_clause_${chapitre.id}_${clause.id}">
        <redaction:castClause id="clauseTexte" key="clause" value="0">
            <logic:notEmpty name="clauseTexte" property="texteVariable">
                <c:out escapeXml="false" value="${clauseTexte.texteVariable}" />
            </logic:notEmpty>
        </redaction:castClause>
        <c:if test="${clause.type == 8}">
            <redaction:castClause id="clauseTexte" key="clause" value="8">
                <redaction:previsualiserPptExtClause name="clauseTexte" />
            </redaction:castClause>
        </c:if>
        <c:if test="${clause.type == 9}">
            <redaction:castClause id="clauseTexte" key="clause" value="9">
                <redaction:previsualiserPptExtClause name="clauseTexte" />
            </redaction:castClause>
        </c:if>

        <redaction:castClause id="clauseListe" key="clause" value="1">
            <logic:notEmpty name="clauseListe" property="formulations">
                <c:set var="formulationExiste" value="false" scope="request" />
                <logic:iterate id="formulation" name="clauseListe" property="formulations" indexId="index">
                    <c:set var="display" value="display: none;" />
                    <c:if test="${formulation.precochee == true}">
                        <c:set var="display" value="display: inline;" />
                    </c:if>
                    <c:if test="${clauseListe.type == 7 and formulationExiste == true}">
                        <span id="recap_clause_${chapitre.id}_${clause.id}_${formulation.numFormulation}_br" style="${display}">
                            <span class="avec-saut"></span>
                        </span>
                    </c:if>
                    <span id="recap_clause_${chapitre.id}_${clause.id}_${formulation.numFormulation}" style="${display}">
                        <c:out escapeXml="false" value="${formulation.libelle}" />
                        <c:set var="formulationExiste" value="true" scope="request" />
                    </span>
                </logic:iterate>
            </logic:notEmpty>
        </redaction:castClause>
    </span>
    <c:if test="${clause.texteFixeApresAlaLigne == true}">
        <div class="avec-saut"></div>
    </c:if>
    <span>
        <c:out value="${clause.texteFixeApres}" />
    </span>
</span>
