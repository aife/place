package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.metier.ParametrageCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CanevasServiceImpl extends GeneriqueService implements CanevasService {

    private final Logger logger = LoggerFactory.getLogger(CanevasServiceImpl.class);

    private NoyauProxy noyauProxy;

    /**
     * Génére une référence unique pour un canevas.
     * @return la référence de la prochaine canevas
     * à créer Exemple : CAN000060
     * @throws TechnicalException erreur technique
     */
    @Override
    public String getReferenceCanevasSuivante(final boolean canevasEditeur, final Integer idOrganisme) {

        EpmTRefParametrage parametrageNumerotationCanevasEditeur =
                (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_NUMEROTATION_CANEVAS_EDITEUR, TypeEpmTRefObject.PARAMETRAGE);

        EpmTRefParametrage parametrageNumerotationCanevasClients =
                (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_NUMEROTATION_CANEVAS_CLIENTS, TypeEpmTRefObject.PARAMETRAGE);

        logger.debug("Générer une référence pour la nouvelle canevas");
        String prefixRef = "CAN";
        String suffixRef = canevasEditeur ? parametrageNumerotationCanevasEditeur.getValeur() : parametrageNumerotationCanevasClients.getValeur();

        String refSuiv;

        List<String> resultQuery = getRedactionService().chercherDerniereReferenceCanevas(idOrganisme, canevasEditeur);

        if (!CollectionUtils.isEmpty(resultQuery)) {
            int valSuffixRefSuiv = resultQuery.stream()
                    .map(s -> s.replace(prefixRef, ""))
                    .mapToInt(Integer::parseInt)
                    .max()
                    .orElse(0)
                    + 1;
            refSuiv = prefixRef.concat(Util.strPadding(String.valueOf(valSuffixRefSuiv), suffixRef.length()));
        } else {
            refSuiv = prefixRef.concat(suffixRef);
        }

        logger.debug("La Référence Générée : " + refSuiv);
        return refSuiv;
    }

    @Override
    public List<EpmTCanevas> chercherListeCanevasPourClauseInterministerielle( String referenceClause ) {
        return this.getRedactionService().chercherListeCanevasPourClauseInterministerielle(referenceClause);
    }

    @Override
    public List<EpmTCanevas> chercherListeCanevasPourClauseMinisterielle( String referenceClause, Integer idOrganisme ) {
        return this.getRedactionService().chercherListeCanevasPourClauseMinisterielle(referenceClause, idOrganisme);
    }

    @Autowired
    public void setNoyauProxy(NoyauProxy noyauProxy) {
        this.noyauProxy = noyauProxy;
    }

}
