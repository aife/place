package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTClausePotentiellementConditionneeAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTClausePotentiellementConditionneePub;
import fr.paris.epm.redaction.importExport.bean.ClausePotentiellementConditionneeImportExport;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryNamedMapper;
import org.mapstruct.*;

import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring", uses = {ClauseValeurPotentiellementConditionneePubMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class})
public abstract class ClausePotentiellementConditionneePubMapper {

    @Mapping(source = "epmTRefPotentiellementConditionnee.uid", target = "refPotentiellementConditionnee")
    @Mapping(source = "epmTClauseValeurPotentiellementConditionnees", target = "clausesValeursPotentiellementConditionnee")
    public abstract ClausePotentiellementConditionneeImportExport toClausePotentiellementConditionneeImportExport(
            EpmTClausePotentiellementConditionneePub epmTClausePotentiellementConditionneePub);

    @Mapping(source = "refPotentiellementConditionnee", target = "epmTRefPotentiellementConditionnee", qualifiedByName = {"DirectoryNamedMapper", "refPotentiellementConditionneeFromUid"})
    @Mapping(source = "clausesValeursPotentiellementConditionnee", target = "epmTClauseValeurPotentiellementConditionnees")
    public abstract EpmTClausePotentiellementConditionneePub toClausePotentiellementConditionneePub(
            ClausePotentiellementConditionneeImportExport clausePotentiellementConditionneeImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTClausePotentiellementConditionneePub setIdPublication(
            @MappingTarget EpmTClausePotentiellementConditionneePub epmTClausePotentiellementConditionneePub, @Context Integer idPublication) {
        epmTClausePotentiellementConditionneePub.setIdPublication(idPublication);
        return epmTClausePotentiellementConditionneePub;
    }

    protected Set<? extends EpmTClausePotentiellementConditionneeAbstract> toEpmTClausePotentiellementConditionneeAbstracts(
            Set<ClausePotentiellementConditionneeImportExport> clausePotentiellementConditionneeImportExports, @Context Integer idPublication) {

        if (clausePotentiellementConditionneeImportExports == null)
            return null;

        Set<EpmTClausePotentiellementConditionneeAbstract> epmTClausePotentiellementConditionneeAbstracts = new HashSet<>();
        for (ClausePotentiellementConditionneeImportExport clausePotentiellementConditionneeImportExport : clausePotentiellementConditionneeImportExports)
            epmTClausePotentiellementConditionneeAbstracts.add(toClausePotentiellementConditionneePub(clausePotentiellementConditionneeImportExport, idPublication));

        return epmTClausePotentiellementConditionneeAbstracts;
    }

}
