package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefValeurTypeClause;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseTexte;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import fr.paris.epm.redaction.presentation.mapper.DocumentMapper;

import java.util.*;

public class DerogationUtil implements XmlDocument {

    private static DocumentMapper documentMapper;

    private static ReferentielFacade referentielFacade;

    /**
     * Créer une chapitre pour présenter le tableau des dérogations. Ce chapitre
     * doit afficher à la fin du document/canevas si la dérogations CCAG est activé.
     * @param derogations les dérogations à transformer au tableau
     * @return le chapitre qui sert à la génération du document
     */
    public static ChapitreDocument creerChapitreDerogations(List<Derogation> derogations) {
        ResourceBundle rb = ResourceBundle.getBundle("ApplicationResources");
        Map<String, Object> freeMarkerMap = new HashMap<String, Object>();
        freeMarkerMap.put("derogations", derogations);

        final ClauseTexte tableauRecapitulatifDerogations = new ClauseTexte();
        // clauses créés pour le cas particulier Derogation, ref doit pas être null même cette propriété n'a pas de sens
        tableauRecapitulatifDerogations.setRef("");
        tableauRecapitulatifDerogations.setType(ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE);

        EpmTRefValeurTypeClause valeurTableauDerogation = referentielFacade.getValeurTypeClauseById(Constante.ID_TABLEAU_DEROGATION);

        TableauRedaction tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(valeurTableauDerogation.getExpression(), null, null, derogations);
        tableauRecapitulatifDerogations.setTableauRedaction(tableauRedaction);
        tableauRecapitulatifDerogations.setTexteVariable(GenerationTableauUtil.tableauRedactionToHTML(tableauRedaction));
        tableauRecapitulatifDerogations.setIdRefValeurTableau(Constante.ID_TABLEAU_DEROGATION);
        tableauRecapitulatifDerogations.setId(-1);

        List<ClauseTexte> clauses = new ArrayList<ClauseTexte>() {{
            add(tableauRecapitulatifDerogations);
        }};

        ChapitreDocument chapitreDocument = new ChapitreDocument();
        chapitreDocument.getClausesDocument().addAll(clauses);
        chapitreDocument.setTitre(rb.getString("document.derogation.titre"));

        return chapitreDocument;
    }

    /**
     * Remplie la liste des dérogations depuis les chapitres du canevas
     * @param chapitres Les chapitres du canevas
     * @param derogations La liste de dérogations à remplir
     */
    public static void chargerDerogationsDepuisChapitres(List<Chapitre> chapitres, List<Derogation> derogations) {
        if (chapitres == null)
            return;

        for (Chapitre chapitre : chapitres) {
            if (chapitre.getDerogation() != null && !chapitre.getDerogation().isEmpty()) {
                Derogation derogation = documentMapper.toDerogation(chapitre.getDerogation());
                derogation.setNumArticle(chapitre.getNumero());
                derogations.add(derogation);
            }
            chargerDerogationsDepuisChapitres(chapitre.getChapitres(), derogations);
        }
    }

    /**
     * injetcé par Spring
     */
    public void setDocumentMapper(DocumentMapper documentMapper) {
        DerogationUtil.documentMapper = documentMapper;
    }

    public void setReferentielFacade(ReferentielFacade referentielFacade) {
        DerogationUtil.referentielFacade = referentielFacade;
    }

}
