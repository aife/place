package fr.paris.epm.redaction.coordination.exception;


public class ActivationPublicationException extends RuntimeException {
    protected final String message;


    public ActivationPublicationException(String errorMessage) {
        super(errorMessage);
        this.message = errorMessage;
    }
}
