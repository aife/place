package fr.paris.epm.redaction.metier.objetValeur;

/**
 * la classe PotentiellementConditionneeValeur qui manipule les valeurs d'un
 * PotentiellementConditionnee.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class PotentiellementConditionneeValeur implements Comparable {
    /**
     * libellé du PotentiellementConditionneeValeur.
     */
    private String libelle;
    /**
     * l'dentifiant du PotentiellementConditionneeValeur.
     */

    private int id;

    /**
     * cette méthode renvoie le libellé du PotentiellementConditionneeValeur.
     * @return renvoyer le libellé du PotentiellementConditionneeValeur.
     */
    public final String getLibelle() {
        return libelle;
    }

    /**
     * cette méthode positionne le libellé du PotentiellementConditionneeValeur.
     * @param valeur pour initialiser le libellé du
     *            PotentiellementConditionneeValeur.
     */
    public final void setLibelle(final String valeur) {
        libelle = valeur;
    }

    /**
     * cette méthode renvoie l'identifiant du PotentiellementConditionneeValeur.
     * @return renvoyer l'identifiant du PotentiellementConditionneeValeur.
     */
    public final int getId() {
        return id;
    }

    /**
     * cette méthode positionne l'identifiant du
     * PotentiellementConditionneeValeur.
     * @param valeur pour initialiser l'identifiant du
     *            PotentiellementConditionneeValeur.
     */
    public final void setId(final int valeur) {
        id = valeur;
    }

    /**
     * cette méthode permet la comparaison entre deux objets.
     * @param objet est le paramètre à comparer.
     * @return resultat de la comparaison.
     */
    public final int compareTo(final Object objet) {
        return libelle
                .compareTo(((PotentiellementConditionneeValeur) objet)
                        .getLibelle());
    }
}
