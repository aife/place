package fr.paris.epm.redaction.metier.objetValeur.canevas;

import fr.paris.epm.redaction.metier.objetValeur.commun.Clausier;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.util.Constantes;
import java.util.List;

/**
 * Classe Clause qui effectue des traitements sur une clause.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class Clause implements Clausier {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * l'attribut identifiant.
     */
    private int idClause;

    private Integer idPublication;

    private String lastVersion;


    /**
     * l'attribut réference de la clause.
     */
    private String reference;

    /**
     * l'attribut contenu de la clause.
     */
    private String contenu = "";

    /**
     * l'attribut texteFixeApres.
     */
    private String texteFixeApres;

    /**
     * l'attribut texteFixeApresAlaLigne.
     */
    private boolean texteFixeApresAlaLigne;

    /**
     * l'attribut texteFixeAvantAlaLigne.
     */
    private boolean texteFixeAvantAlaLigne;

    /**
     * l'attribut texteFixeAvant.
     */
    private String texteFixeAvant;

    /**
     * l'attribut date Modification de la clause.
     */
    private String dateModification;

    /**
     * l'attribut date création de la clause.
     */
    private String dateCreation;

    /**
     * l'attribut tème de la clause.
     */
    private String theme;

    /**
     * l'objet info Bulle.
     */
    private InfoBulle infoBulle;

    /**
     * l'objet chapitre.
     */
    private Chapitre parent;

    /**
     * l'attribut actif.
     */
    private boolean actif = true;

    /**
     * Etat de la clause.
     */
    private String etat = ETAT_DISPONIBLE;

    /**
     * l'attribut potentiellementConditionnee.
     */

    private boolean potentiellementConditionnee = false;

    /**
     * l'attribut potentiellementApplicable.
     */
    private boolean potentiellementApplicable = false;
    
    private Integer idStatutRedactionClausier;
    
    /**
     * Détermine si la clause est de type éditeur ou client
     */
    private boolean clauseEditeur;
    
    private Integer nombreSurcharges = 0;
    
	/**
     * L'auteur de la clause (Editeur, Editeur surchargée, Client)
     */
    private String auteur;

    private List<Integer> idsTypeContrats;

    private Integer idProcedure;

    /**
     * cette méthode vérifie si le chapitre possède l'infobulle passée en parametre.
     * @param bulle infobulle à chercher.
     * @return résultat du test.
     */
    public final boolean aEnfant(final InfoBulle bulle) {
        return (bulle != null) && bulle.equals(infoBulle);
    }

    public Integer getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(Integer idProcedure) {
        this.idProcedure = idProcedure;
    }

    /**
     * cette méthode permet de renvoyer l'index de la clause en cours.
     * @return index de la clause courante.
     * @throws Exception erreur technique.
     */
    public final int index() throws Exception {
        if (parent == null)
            throw new Exception("Parent vide, clause orpheline");
        int index = parent.getClauses().indexOf(this);

        if (index == -1)
            throw new Exception("erreur interne, non trouvée");
        return index;
    }

    /**
     * @return la valeur du hashcode du bean courant.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final int hashCode() {
        int result = 1;
        if (idPublication != null)
            result = Constantes.PREMIER * result + idPublication.hashCode();
        if (reference != null)
            result = Constantes.PREMIER * result + reference.hashCode();
        if (infoBulle != null)
            result = Constantes.PREMIER * result + infoBulle.hashCode();
        return result;
    }

    /**
     * cette méthode teste l'égalité des objets.
     * @param obj correspond à l'objet à tester.
     * @return vrai si les deux objets sont égale, si non faux.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Clause))
            return false;

        final Clause autre = (Clause) obj;
        if (idClause != autre.idClause)
            return false;

        if (idPublication != null) {
            if (!idPublication.equals(autre.idPublication))
                return false;
        } else {
            if (autre.idPublication != null)
                return false;
        }

        if (!reference.equals(autre.reference))
            return false;
        if (infoBulle != autre.infoBulle)
            return false;
        return true;
    }

    /**
     * cette méthode clone l'objet clause.
     * @return clause.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final Clause clone() {
        Clause clause = new Clause();
        clause.idClause = idClause;
        clause.idPublication = idPublication;
        clause.lastVersion = lastVersion;
        clause.reference = reference;
        clause.theme = theme;
        clause.idsTypeContrats = idsTypeContrats;
        clause.contenu = contenu;
        clause.texteFixeApres = texteFixeApres;
        clause.texteFixeAvant = texteFixeAvant;
        clause.potentiellementConditionnee = potentiellementConditionnee;
        clause.potentiellementApplicable = potentiellementApplicable;
        if (infoBulle != null)
            clause.infoBulle = (InfoBulle) infoBulle.clone();
        clause.parent = parent;
        clause.actif = actif;
        return clause;
    }

    /**
     * cette méthode permet la conversion de type en String.
     * @return la chaine de caractère convertie en String.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final String toString() {
        return "Clause" + "\nIdClause: " + idClause + "\nidPublication: " + idPublication +
                "\nRference: " + reference + "\nInfo-Bulle: " + infoBulle;
    }

    public int getIdClause() {
        return idClause;
    }

    public void setIdClause(int idClause) {
        this.idClause = idClause;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getTexteFixeApres() {
        return texteFixeApres;
    }

    public void setTexteFixeApres(String texteFixeApres) {
        this.texteFixeApres = texteFixeApres;
    }

    public boolean isTexteFixeApresAlaLigne() {
        return texteFixeApresAlaLigne;
    }

    public void setTexteFixeApresAlaLigne(boolean texteFixeApresAlaLigne) {
        this.texteFixeApresAlaLigne = texteFixeApresAlaLigne;
    }

    public boolean isTexteFixeAvantAlaLigne() {
        return texteFixeAvantAlaLigne;
    }

    public void setTexteFixeAvantAlaLigne(boolean texteFixeAvantAlaLigne) {
        this.texteFixeAvantAlaLigne = texteFixeAvantAlaLigne;
    }

    public String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    public void setTexteFixeAvant(String texteFixeAvant) {
        this.texteFixeAvant = texteFixeAvant;
    }

    public String getDateModification() {
        return dateModification;
    }

    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public InfoBulle getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulle infoBulle) {
        this.infoBulle = infoBulle;
    }

    public Chapitre getParent() {
        return parent;
    }

    public void setParent(Chapitre parent) {
        this.parent = parent;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public boolean isPotentiellementConditionnee() {
        return potentiellementConditionnee;
    }

    public void setPotentiellementConditionnee(boolean potentiellementConditionnee) {
        this.potentiellementConditionnee = potentiellementConditionnee;
    }

    public boolean isPotentiellementApplicable() {
        return potentiellementApplicable;
    }

    public void setPotentiellementApplicable(boolean potentiellementApplicable) {
        this.potentiellementApplicable = potentiellementApplicable;
    }

    public Integer getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }

    public void setIdStatutRedactionClausier(Integer idStatutRedactionClausier) {
        this.idStatutRedactionClausier = idStatutRedactionClausier;
    }

    public boolean isClauseEditeur() {
        return clauseEditeur;
    }

    public void setClauseEditeur(boolean clauseEditeur) {
        this.clauseEditeur = clauseEditeur;
    }

    public Integer getNombreSurcharges() {
        return nombreSurcharges;
    }

    public void setNombreSurcharges(Integer nombreSurcharges) {
        this.nombreSurcharges = nombreSurcharges;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public List<Integer> getIdsTypeContrats() {
        return idsTypeContrats;
    }

    public void setIdsTypeContrats(List<Integer> idsTypeContrats) {
        this.idsTypeContrats = idsTypeContrats;
    }

}
