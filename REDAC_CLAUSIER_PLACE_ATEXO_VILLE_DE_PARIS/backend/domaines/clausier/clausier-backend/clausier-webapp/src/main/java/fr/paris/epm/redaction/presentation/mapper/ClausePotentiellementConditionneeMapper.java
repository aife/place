package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTClausePotentiellementConditionnee;
import fr.paris.epm.noyau.persistance.redaction.EpmTClausePotentiellementConditionneeAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionnee;
import fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionneeAbstract;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {HtmlSanitizerMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class})
public interface ClausePotentiellementConditionneeMapper {

    EpmTClausePotentiellementConditionnee toEpmTClausePotentiellementConditionnee(
            EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionneeAbstract);

    EpmTClauseValeurPotentiellementConditionnee toEpmTClauseValeurPotentiellementConditionnee(
            EpmTClauseValeurPotentiellementConditionneeAbstract epmTClauseValeurPotentiellementConditionneeAbstract);

}
