package fr.paris.epm.redaction.presentation.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class CommunSearch {
    private int page;
    private int size;
    private String sortField;
    private boolean asc;
    private Boolean actif;
    private boolean editeur;

    private Integer idTypeContrat;
    private Integer idProcedure;
    private int idNaturePrestation;
    private int typeAuteur;

    private int idStatutRedactionClausier;
    private int idTypeDocument;

    private String referenceCanevas;
    private String referenceClause;
    @DateTimeFormat(pattern = "yyyy-MM-dd"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMin;
    @DateTimeFormat(pattern = "yyyy-MM-dd"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMax;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    public Date getDateModificationMin() {
        return dateModificationMin;
    }

    public void setDateModificationMin(Date dateModificationMin) {
        this.dateModificationMin = dateModificationMin;
    }

    public Date getDateModificationMax() {
        return dateModificationMax;
    }

    public void setDateModificationMax(Date dateModificationMax) {
        this.dateModificationMax = dateModificationMax;
    }

    public Boolean getActif() {
        return actif;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public boolean isEditeur() {
        return editeur;
    }

    public void setEditeur(boolean editeur) {
        this.editeur = editeur;
    }

    public Integer getIdTypeContrat() {
        return idTypeContrat;
    }

    public void setIdTypeContrat(Integer idTypeContrat) {
        this.idTypeContrat = idTypeContrat;
    }

    public Integer getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(Integer idProcedure) {
        this.idProcedure = idProcedure;
    }

    public int getIdNaturePrestation() {
        return idNaturePrestation;
    }

    public void setIdNaturePrestation(int idNaturePrestation) {
        this.idNaturePrestation = idNaturePrestation;
    }

    public int getTypeAuteur() {
        return typeAuteur;
    }

    public void setTypeAuteur(int typeAuteur) {
        this.typeAuteur = typeAuteur;
    }

    public int getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }

    public void setIdStatutRedactionClausier(int idStatutRedactionClausier) {
        this.idStatutRedactionClausier = idStatutRedactionClausier;
    }

    public int getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public String getReferenceCanevas() {
        return referenceCanevas;
    }

    public void setReferenceCanevas(String referenceCanevas) {
        this.referenceCanevas = referenceCanevas;
    }

    public String getReferenceClause() {
        return referenceClause;
    }

    public void setReferenceClause(String referenceClause) {
        this.referenceClause = referenceClause;
    }
}
