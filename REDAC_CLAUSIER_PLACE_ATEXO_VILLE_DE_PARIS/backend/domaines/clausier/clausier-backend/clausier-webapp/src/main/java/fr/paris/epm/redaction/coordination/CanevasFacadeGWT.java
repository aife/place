package fr.paris.epm.redaction.coordination;

import fr.paris.epm.noyau.metier.redaction.CanevasCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasAbstract;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;

import java.util.List;

/**
 * Interface qui publie les méthodes qui permet de gérer un canevas.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public interface CanevasFacadeGWT {

    /**
     * Transforme un EpmTCanevas en Canevas.
     * @param epmTCanevasAbstract EpmTCanevas provenant de la base de donnée
     * @return Canevas
     */
    Canevas epmTCanevasVersCanevas(EpmTCanevasAbstract epmTCanevasAbstract, final Integer idOrganisme);

    /**
     * Transforme un Canevas en EpmTCanevas (base de donnée).
     * @param canevasXML Canevas
     * @return EpmTCanevas
     */
    EpmTCanevas canevasVersEpmTCanevas(Canevas canevasXML);

    EpmTCanevas canevasVersEpmTCanevas(Canevas canevasXML, Integer idStatutRedactionClausier);

    /**
     * Permet d'ajouter un objet dans la base de donnée.
     * @param objet l'objet à ajouter
     * @return EpmTCanevas l'objet ajouté
     */
    Canevas ajouter(Canevas objet, final Integer idOrganisme);

    /**
     * Cette méthode permet de modifier un objet Canevas.
     * @param canevas Canevas
     * @return Canevas
     */
    Canevas modifierCanevas(Canevas canevas, final Integer idOrganisme);
    
    /**
     * Cette méthode permet de modifier un objet Canevas.
     * @param canevas Canevas
     * @return Canevas
     */
    Canevas modifierCanevas(Canevas canevas, String action, final Integer idOrganisme);

    /**
     * Recupére le contenu de chaque clause dans la liste.
     * @param canevas Canevas {@link Canevas} en session (peut être null)
     * @param idCanevas id du canevas en session (peut valoir 0).
     * @param idPublication id de la publication (peut valoir NULL)
     * @return le canevas modifié avec le contenu des clauses.
     */
    Canevas getCanevasPourCanevasService(Canevas canevas, int idCanevas, Integer idPublication, final Integer idOrganisme, final boolean isFormeEditeur);

    /**
     * Change le statut de rédaction d'un canevas
     * @param canevas identifiant du canevas
     * @param action le type d'action à réaliser (Demander la validation, valider)
     */
    Canevas changerStatutCanevas(final Canevas canevas, final String action, final Integer idOrganisme) throws CloneNotSupportedException;
    
    /**
     * Dans le cas d'un écran éditeur on recherche la clause dans la table
     * epmtcanevas pour récupérer la version la plus à jour, dans le cas d'un
     * écran client, si le canevas est un canevas éditeur on récupére la dernière
     * version publiée du canevas (table d'historique) si non on recherche la
     * dernière version en cours du canevas.
     * @param id identifiant du canevas
     * @param ecranEditeur booleean indiquant si l'on est dans le cas d'un écran
     *            administration éditeur (true) ou d'un écran client (false)
     * @return le canevas recherché.
     */
    Canevas charger(final int id, final Integer idPublication, final boolean ecranEditeur, final Integer idOrganisme);

    /**
     * Recherche un canevas à partir de son identifiant et de sa version de publication
     * du clausier.
     * @param idCanevas identifiant du canevas
     * @param idPublicationClausier identifiant de l'epmtpublicationclausier pour
     *            lequel la clause a été publiée.
     * @return le canevas ou null si pas de résultat.
     */
    Canevas chargerParIdPublicationClausier(final int idCanevas, final int idPublicationClausier, final Integer idOrganisme);

    void changerStatutCanevas(final Integer idOrganisme, String action, int... idsCanevas);

    List<CanevasBean> chercher(final int idUtilisateur, final CanevasCritere critere);

    List<CanevasBean> chercher(final int idUtilisateur, final CanevasViewCritere critere);

}
