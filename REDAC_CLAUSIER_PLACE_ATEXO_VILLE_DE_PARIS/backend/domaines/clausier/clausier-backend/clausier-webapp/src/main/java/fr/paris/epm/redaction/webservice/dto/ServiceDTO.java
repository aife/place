package fr.paris.epm.redaction.webservice.dto;

public class ServiceDTO {
	private ServiceDTO parent;
	private OrganismeDTO organisme;
    private Long id;
    private String uuid;
    private String libelle;

	public ServiceDTO() {
	}

	public ServiceDTO getParent() {
		return parent;
	}

	public void setParent( ServiceDTO parent ) {
		this.parent = parent;
	}

	public OrganismeDTO getOrganisme() {
		return organisme;
	}

	public void setOrganisme( OrganismeDTO organisme ) {
		this.organisme = organisme;
	}

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid( String uuid ) {
		this.uuid = uuid;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}
}
