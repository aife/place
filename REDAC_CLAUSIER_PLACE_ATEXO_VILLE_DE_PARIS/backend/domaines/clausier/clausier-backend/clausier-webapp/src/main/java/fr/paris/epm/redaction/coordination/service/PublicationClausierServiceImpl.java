package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.metier.redaction.PublicationClausierCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class PublicationClausierServiceImpl extends GeneriqueService implements PublicationClausierService {

    private final Logger logger = LoggerFactory.getLogger(PublicationClausierServiceImpl.class);

    private RedactionServiceSecurise redactionService;

    /**
     * @param idPublication id publication à vérifier
     * @return true si cette publication à été déjà activée au moins une fois
     */
    @Override
    public boolean isAlreadyActivated(int idPublication) {
        return redactionService.isAlreadyActivated(idPublication);
    }

    @Override
    public boolean canActivate(Integer idOrganisme) {
        return redactionService.canActivate(idOrganisme);
    }

    @Override
    public void desactivationAllPublication() {
        redactionService.desactivationAllPublication();
    }


    @Override
    public boolean isVersionNameUnique(String version, String plateformeUuid) {
        PublicationClausierCritere publicationClausierCritere = new PublicationClausierCritere(plateformeUuid);
        publicationClausierCritere.setVersion(version);
        List<EpmTPublicationClausier> epmTPublicationClausier = redactionService.chercherEpmTObject(0, publicationClausierCritere);
        return epmTPublicationClausier.isEmpty();
    }

    @Override
    public void publicationClausierUpdateClausePub(int idPublication) {
        redactionService.publicationClausierUpdateClausePub(idPublication);
    }

    @Override
    public void publicationClausierActivateTClause(int idPublication, List<Integer> idClauses) {
        if (idClauses == null || idClauses.isEmpty()) {
            logger.warn("idClauses est vide");
            return;
        }
        redactionService.publicationClausierUpdateClause(idPublication, idClauses);
    }

    @Override
    public void publicationClausierUpdateClause(int idPublication) {
        redactionService.publicationClausierUpdateClause(idPublication);
    }

    @Override
    public void publicationClausierUpdateRoleClausePub(int idPublication) {
        redactionService.publicationClausierUpdateRoleClausePub(idPublication);
    }

    @Override
    public void publicationClausierUpdateClauseHasTypeContratPub(int idPublication) {
        redactionService.publicationClausierUpdateClauseHasTypeContratPub(idPublication);
    }

    @Override
    public void publicationClausierUpdateClauseHasPotentiellementConditionneePub(int idPublication) {
        redactionService.publicationClausierUpdateClauseHasPotentiellementConditionneePub(idPublication);
    }

    @Override
    public void publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(int idPublication) {
        redactionService.publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(idPublication);
    }

    public void publicationClausierActivateTCanevas(int idPublication, List<Integer> idCanevas) {
        if (idCanevas == null || idCanevas.isEmpty()) {
            logger.warn("idCanevas est vide");
            return;
        }
        redactionService.publicationClausierUpdateCanevas(idPublication, idCanevas);
    }

    @Override
    public void publicationClausierUpdateCanevas(int idPublication) {
        redactionService.publicationClausierUpdateCanevas(idPublication);
    }

    @Override
    public void publicationClausierUpdateCanevasPub(int idPublication) {
        redactionService.publicationClausierUpdateCanevasPub(idPublication);
    }

    @Override
    public void publicationClausierUpdateCanevasHasProcedurePub(int idPublication) {
        redactionService.publicationClausierUpdateCanevasHasProcedurePub(idPublication);
    }

    @Override
    public void publicationClausierUpdateCanevasHasTypeContratPub(int idPublication) {
        redactionService.publicationClausierUpdateCanevasHasTypeContratPub(idPublication);
    }

    @Override
    public void publicationClausierUpdateChapitrePub(int idPublication) {
        redactionService.publicationClausierUpdateChapitrePub(idPublication);
    }

    @Override
    public void publicationClausierUpdateChapitreHasClausePub(int idPublication) {
        redactionService.publicationClausierUpdateChapitreHasClausePub(idPublication);
    }

    @Override
    public void rollbackPublicationClausier(int idPublication) {
        redactionService.rollbackPublicationClausier(idPublication);
    }


    @Autowired
    @Override
    public void setRedactionService(RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

}
