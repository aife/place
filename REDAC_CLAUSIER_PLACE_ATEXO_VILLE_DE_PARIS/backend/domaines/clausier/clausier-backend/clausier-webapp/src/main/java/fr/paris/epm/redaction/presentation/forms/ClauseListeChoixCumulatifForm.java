package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Formulaire de Clause Liste Choix Cumulatif.
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */

public class ClauseListeChoixCumulatifForm extends AbstractClauseListeChoixForm {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attribut precoche.
     */
    private String[] precochee;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    private List<ClauseFormulaire> formulaires;
    /**
     * marqueur de fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClauseListeChoixCumulatifForm.class);

    /**
     * Constructeur de la classe ClauseListeChoixCumulatifForm.
     */
    public ClauseListeChoixCumulatifForm() {
        reset();
    }

    public List<ClauseFormulaire> getFormulaires() {
        return formulaires;
    }

    public void setFormulaires(List<ClauseFormulaire> formulaires) {
        this.formulaires = formulaires;
    }

    /**
     * cette méthode permet d'initialiser le formulaire du Clause Liste Choix
     * Cumulatif.
     */
    public void reset() {
        formulationModifiable = "1";
        parametrableDirection = "0";
        parametrableAgent = "0";
        textFixeAvant = null;
        textFixeApres = null;
        numFormulation = null;
        precochee = null;
        formulationCollection = null;
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
        valeurDefaut = null;
        tailleChamp = null;
        valeurDefautMoyen = null;
        valeurDefautCourt = null;
        valeurDefautTresLong = null;
    }

    /**
     * méthode validate () pour valider les données saisies sur le formulaire.
     *
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest).
     */
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        boolean erreurTrouve = false;

        ActionErrors erreurs = new ActionErrors();

        Util util = new Util();
        if (numFormulation == null) {
            erreurs.add("numFormulation", new ActionMessage("creationClause.valider.numFormulation"));
            erreurTrouve = true;
        } else {
            LOG.debug("Récupération des formulations");
            Set<EpmTRoleClauseAbstract> epmTRoleClauseAbstracts = new HashSet<>();
            for (int i = 0; i < numFormulation.length; i++) {
                LOG.debug("Récupérer la formulation N° : " + i);
                // positionner les ppts externes
                // Les ppts externes ont des valeurs par defaut et donc stockés
                // dans TRoleClause
                EpmTRoleClause epmTRoleClauseInstance = new EpmTRoleClause();
                if (numFormulation[i] != null && !numFormulation[i].isEmpty())
                    epmTRoleClauseInstance.setNumFormulation(Integer.valueOf(numFormulation[i]));
                epmTRoleClauseInstance.setNombreCarateresMax(Integer.parseInt(tailleChamp[i]));

                String defaultValue = "";
                if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_LONG))
                    defaultValue = valeurDefaut[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_MOYEN))
                    defaultValue = valeurDefautMoyen[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_COURT))
                    defaultValue = valeurDefautCourt[i];
                else if (tailleChamp[i].equals(Constante.TAILLE_CHAMP_TRES_LONG))
                    defaultValue = valeurDefautTresLong[i];
                epmTRoleClauseInstance.setValeurDefaut(defaultValue);

                if (Util.isInArray(String.valueOf(i), precochee))
                    epmTRoleClauseInstance.setPrecochee(true);
                else
                    epmTRoleClauseInstance.setPrecochee(false);

                LOG.debug("Ajouter la formulation N° : " + i + " à la clause.");
                // Ajouter les roleClauses à une clause
                epmTRoleClauseAbstracts.add(epmTRoleClauseInstance);
            }

            EpmTClause epmTClause = new EpmTClause();
            epmTClause.setEpmTRoleClauses(epmTRoleClauseAbstracts);

            formulationCollection = epmTClause.getEpmTRoleClausesTrie();

            for (String num : numFormulation) {
                LOG.debug("numFormulation[i] = " + num);

                if (!util.isInt(num) || Integer.parseInt(num) < 0) {
                    erreurs.add("numFormule", new ActionMessage("creationClause.valider.numFormule"));
                    erreurTrouve = true;
                    break;
                }
            }
        }

        if (formulationCollection != null && !erreurTrouve) {
            for (EpmTRoleClauseAbstract element : formulationCollection) {
                if (element != null && element.getValeurDefaut().trim().equals("")) {
                    erreurs.add("valeurDefaut", new ActionMessage("creationClause.valider.valeurDefaut"));
                    erreurTrouve = true;
                    break;
                }
            }
        }
        if (erreurTrouve) {
            LOG.debug("nbCarac = null");
            numFormulation = null;
        }
        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    public final String[] getPrecochee() {
        return precochee;
    }

    public final void setPrecochee(final String[] valeur) {
        this.precochee = valeur;
    }

    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    public final void setClauseSelectionnee(final String valeur) {
        this.clauseSelectionnee = valeur;
    }

}
