<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/scripts-commun.js"></script>
    <script type="text/javascript" src="js/apercuDynamique.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css"/>
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
</head>

<body onload="popupResize(); miseAJourContenuPopUp();" id="apercuDynamique">
<div class="previsu-doc" id="container">
    <div class="content">
        <center>
            <h5>
                <c:out value="${documentEnCoursApercuDynamique.reference}" />
                &nbsp;:&nbsp;
                <c:out value="${documentEnCoursApercuDynamique.nomFichier}" />
                <c:out value="${documentEnCoursApercuDynamique.extension}" />
                <bean:message key="previsualiserDocument.txt.parentheseOuvrante"/>
                <bean:message key="previsualiserDocument.txt.canevas"/>
                <c:out value="${documentEnCoursApercuDynamique.refCanevas}" />
                <bean:message key="previsualiserDocument.txt.parentheseFermante"/>
            </h5>
        </center>
        <ul id="chapitres">
            <li>
                <bean:define id="chapitres" name="documentEnCoursApercuDynamique" property="chapitres" toScope="request"/>
                <bean:define id="premierNiveau" value="true" toScope="request"/>
                <jsp:include page="/jsp/previsualisation/document/ApercuDynamiqueChapitre.jsp"/>
            </li>
        </ul>
    </div>
    <div class="breaker"></div>
</div>
</body>
</html>
