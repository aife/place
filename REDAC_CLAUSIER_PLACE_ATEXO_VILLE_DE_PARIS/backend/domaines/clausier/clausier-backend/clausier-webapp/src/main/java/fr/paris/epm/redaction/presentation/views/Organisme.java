package fr.paris.epm.redaction.presentation.views;

public class Organisme {

	private String libelle;

	private Number identifiantExterne;

	private String acronyme;

	private Environnement environnement;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}

	public Number getIdentifiantExterne() {
		return identifiantExterne;
	}

	public void setIdentifiantExterne( Number identifiantExterne ) {
		this.identifiantExterne = identifiantExterne;
	}

	public String getAcronyme() {
		return acronyme;
	}

	public void setAcronyme( String acronyme ) {
		this.acronyme = acronyme;
	}

	public Environnement getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(Environnement environnement) {
		this.environnement = environnement;
	}
}
