package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.global.commun.Util;
import fr.paris.epm.global.commun.tags.HrefTag;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant l'affichage de boutons dans le module Redaction selon les habilitations de l'utilisateur et le statut du document
 * @author MGA
 *
 */
public class AffichageBoutonsRedaction extends TagSupport{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AffichageBoutonsRedaction.class);

    /**
     * Profil validateur
     */
    private static final String ROLE_VALIDATION = "ROLE_ValidationDocumentsREDAC";

    private String idDocument;

	private String categorie;
    
    private String idConsultation;
    
    private String statutValidationDocument;
    
    
    @Override
    public int doStartTag() throws JspException {
        ServletRequest request = (ServletRequest) pageContext.getRequest();
        
        String url = "";
        String alt = "";
        String  title = "";
        String onClick = "";
        String imagePath = "";
        EpmTUtilisateur utilisateur = (EpmTUtilisateur) request.getAttribute("utilisateur");
        boolean roleValidateur = Util.verificationHabilitation(ROLE_VALIDATION, utilisateur.getProfil());
        StringBuffer boutons = new StringBuffer();
        if (idDocument != null && !idDocument.isEmpty()) {
            try {
                // Bouton Valider le document
                if (Document.STATUT_EN_ATTENTE_VALIDATION.equals(statutValidationDocument) && roleValidateur) {
                    url = "javascript:validerDocument(" + idDocument + ",'" + categorie + "')";
                    alt = "Valider le document";
                    title = "Valider le document";
                    onClick = "";
                    imagePath = HrefTag.getHrefRacine() + "images/bouton-valider.gif";
                    boutons.append(afficherBouton(url, alt, title, onClick, imagePath));
                } else {
                    boutons.append(afficherTiret());
                }
                // Bouton Refuser la validation
                if (Document.STATUT_EN_ATTENTE_VALIDATION.equals(statutValidationDocument) && roleValidateur) {
                    url = "javascript:popUp('refuserValidationDocumentInitAction.epm?categorie="+categorie+"&idDoc=" + idDocument + "&numConsultation=" + idConsultation + "','yes')";
                    alt = "Refuser la validation";
                    title = "Refuser la validation";
                    onClick = "";
                    imagePath = HrefTag.getHrefRacine() + "images/bouton-refuser.gif";
                    boutons.append(afficherBouton(url, alt, title, onClick, imagePath));
                } else {
                    boutons.append(afficherTiret());
                }
                // Bouton Dévalider le document
                if (Document.STATUT_VALIDER.equals(statutValidationDocument) && roleValidateur) {

                    url = "javascript:popUp('devaliderDocumentInitAction.epm?categorie="+categorie+"&idDoc=" + idDocument + "&idCons=" + idConsultation + "','yes')"; //+ "#consultation_" + document.getIdConsultation();
                    alt = "Invalider &eacute;ligibilit&eacute;";
                    title = "Invalider &eacute;ligibilit&eacute;";
                    onClick = "return confirm('A MODIFIER : RechercherDocument.msg.suppression.confirEligib')";
                    imagePath = HrefTag.getHrefRacine() + "images/bouton-picto-invalider.gif";
                    boutons.append(afficherBouton(url, alt, title, onClick, imagePath));
                } else {
                    boutons.append(afficherTiret());
                }
            } catch (NumberFormatException e) {
                LOG.error(e.getMessage(), e);
            }
        } else {
            return SKIP_BODY;
        }
        try {
            pageContext.getOut().print(boutons);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return EVAL_BODY_INCLUDE;
    }
    
    @Override
    public void release() {
        super.release();
        idDocument = null;
    }
    
    private final String afficherBouton(String url, String alt, String  title, String onClick, String imagePath){
        StringBuffer sb = new StringBuffer();
        sb.append("<a href="); 
        sb.append(url);
        sb.append("><img title=");
        sb.append(title);
        sb.append(" alt=");
        sb.append(alt);
        sb.append(" src=");
        sb.append(imagePath);
        sb.append(" onClick=");
        sb.append(onClick);
        sb.append("></a>");
        return sb.toString();
    }
    
    private final String afficherTiret(){
        return "<span class=\"empty-action\">-</span>";
    }

    /**
     * @param idDocument the idDocument to set
     */
    public final void setIdDocument(final String valeur) {
        this.idDocument = valeur;
    }


    public final void setIdConsultation(final String valeur) {
        this.idConsultation = valeur;
    }

    public final void setStatutValidationDocument(final String valeur) {
        this.statutValidationDocument = valeur;
    }

	public void setCategorie( String categorie ) {
		this.categorie = categorie;
	}
}
