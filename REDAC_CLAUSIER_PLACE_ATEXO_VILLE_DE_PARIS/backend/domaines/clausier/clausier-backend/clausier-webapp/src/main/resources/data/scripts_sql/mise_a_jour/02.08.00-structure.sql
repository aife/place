-- Evolution des canevas associes a plusieurs procedures

CREATE TABLE epm__t_canevas_has_ref_procedure_passation
(
   id serial NOT NULL, 
   id_canevas integer, 
   id_ref_procedure_passation integer,
   PRIMARY KEY (id),
   CONSTRAINT epm__fk_canevas FOREIGN KEY (id_canevas) REFERENCES epm__t_canevas (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;

-- Evolution des multi criteres potentiellement conditionnees associes aux clauses

CREATE TABLE epm__t_clause_has_valeur_potentiellement_conditionnee
(
   id serial NOT NULL, 
   id_clause integer, 
   valeur integer, 
    PRIMARY KEY (id), 
   CONSTRAINT epm__fk_clause_valeur_pot_cond_id_clause FOREIGN KEY (id_clause) REFERENCES epm__t_clause (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT epm__t_clause_valeur_pot_cond_clause_valeur_unique UNIQUE (id_clause, valeur)
) 
WITH (
  OIDS = FALSE
)
;
COMMENT ON TABLE epm__t_clause_has_valeur_potentiellement_conditionnee IS 'Permet d''associer plusieurs criteres potentiellement conditionnes a une clause.';

ALTER TABLE epm__t_ref_potentiellement_conditionnee ADD COLUMN multi_choix boolean DEFAULT FALSE;

