package fr.paris.epm.redaction.metier.objetValeur;

import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.util.Constantes;import org.apache.commons.lang3.StringUtils;

public class InfoBulleExport {

    private String description = "";
    private String lien = "";
    private String descriptionLien = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public String getDescriptionLien() {
        return descriptionLien;
    }

    public void setDescriptionLien(String descriptionLien) {
        this.descriptionLien = descriptionLien;
    }

    public final boolean hasContent() {
        return !StringUtils.isEmpty(description) || !StringUtils.isEmpty(descriptionLien) || !StringUtils.isEmpty(lien);
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    public final int hashCode() {
        int result = 1;
        if (description != null)
            result = Constantes.PREMIER * result + description.hashCode();
        if (descriptionLien != null)
            result = Constantes.PREMIER * result + descriptionLien.hashCode();
        if (lien != null)
            result = Constantes.PREMIER * result + lien.hashCode();
        return result;
    }

    /**
     * cette méthode teste l'égalité des objets.
     *
     * @param obj correspond à l'objet à tester.
     * @return vrai si les deux objets sont égaux si non renvoyer faux.
     * @see java.lang.Object#equals(java.lang.Object).
     */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof InfoBulleExport))
            return false;

        final InfoBulleExport autre = (InfoBulleExport) obj;
        if (description == null) {
            if (autre.description != null)
                return false;
        } else if (!description.equals(autre.description)) {
            return false;
        }
        if (descriptionLien == null) {
            if (autre.descriptionLien != null)
                return false;
        } else if (!descriptionLien.equals(autre.descriptionLien)) {
            return false;
        }
        if (lien == null)
            return autre.lien == null;
        else
            return lien.equals(autre.lien);
    }

    /**
     * cette méthode clone l'info-Bulle.
     *
     * @return info-Bulle.
     */
    public final Object clone() {
        InfoBulleExport infoBulle = new InfoBulleExport();
        infoBulle.description = description;
        infoBulle.lien = lien;
        infoBulle.descriptionLien = descriptionLien;
        return infoBulle;
    }

    /**
     * cette méthode permet la conversion de type en String.
     *
     * @return la chaîne de caractères convertie.
     * @see java.lang.Object#toString()
     */
    public final String toString() {
        return "Description : [" + description +
                "] | Lien: [" + lien + "] | Description lien: [" + descriptionLien+"]";
    }

}
