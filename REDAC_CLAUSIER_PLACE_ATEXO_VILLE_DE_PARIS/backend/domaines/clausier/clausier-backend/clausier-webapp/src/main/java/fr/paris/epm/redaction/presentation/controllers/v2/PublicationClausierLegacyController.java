package fr.paris.epm.redaction.presentation.controllers.v2;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.global.commun.worker.RsemTaskWorker;
import fr.paris.epm.global.coordination.bean.AbstractBean;
import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.worker.RedactionTaskFactory;
import fr.paris.epm.redaction.coordination.facade.PublicationClausierFacade;
import fr.paris.epm.redaction.importExport.mapper.PublicationClausierMapper;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierSearch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v2/publications-clausier")
public class PublicationClausierLegacyController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(PublicationClausierLegacyController.class);
    private final PublicationClausierFacade publicationClausierFacade;
    private final PublicationClausierMapper publicationClausierMapper;
    private final RedactionTaskFactory redactionTaskFactory;
    private final RsemTaskWorker rsemTaskWorker;

    public PublicationClausierLegacyController(PublicationClausierFacade publicationClausierFacade, PublicationClausierMapper publicationClausierMapper, RedactionTaskFactory redactionTaskFactory, RsemTaskWorker rsemTaskWorker) {
        this.publicationClausierFacade = publicationClausierFacade;

        this.publicationClausierMapper = publicationClausierMapper;
        this.redactionTaskFactory = redactionTaskFactory;
        this.rsemTaskWorker = rsemTaskWorker;
    }

    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("ApplicationResources", "ApplicationResources-commun");
        return resourceBundleMessageSource;
    }

    @GetMapping(params = {"page", "size"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PageRepresentation<PublicationClausierBean> listVersions(final PublicationClausierSearch search, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("liste Versions");
        return publicationClausierFacade.findPublications(search, epmTUtilisateur.getEpmTRefOrganisme());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String createPublicationClausier(@RequestBody PublicationClausierBean publicationClausierBean, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("création d'une publication clausier");
        return publicationClausierFacade.savePublicationClausier(publicationClausierBean, epmTUtilisateur);

    }


    @PatchMapping(value = "/{idPublication}/activation")
    public String activePublicationClausier(@PathVariable("idPublication") final int idPublication, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("activation de PublicationClausier{}", idPublication);
        RsemTask<PublicationClausierBean> task = redactionTaskFactory.newActivationPublicationTask(epmTUtilisateur, idPublication);
        String idTask = rsemTaskWorker.executeTask(task);
        log.info("Tache {} est lancée", idTask);
        return idTask;
    }

    @GetMapping(value = "/activation-possible")
    public String canActivatePublication(@SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        return publicationClausierFacade.canActivatePublication(epmTUtilisateur);
    }

    @PatchMapping(value = "/{idPublication}/commentaire", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PublicationClausierBean editCommentPublicationClausier(@PathVariable("idPublication") final int idPublication, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur,
                                                                  @RequestBody final String commentaire) {
        log.info("modification du commentaire de PublicationClausier{}", idPublication);
        return publicationClausierMapper.fromPublicationBean(publicationClausierFacade.editCommentPublicationClausier(idPublication, commentaire));
    }

    @GetMapping(value = "/{idPublication}/download")
    public final void downloadPublication(final HttpServletResponse response,
                                          @PathVariable int idPublication,
                                          @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur)
            throws IOException {
        log.info("download Publication Clausier{}", idPublication);
        List<Integer> idsPublications = List.of(idPublication);

        //construire le nom de l'archive zip suivant la norme XXX_AAAA_MM_DD_HH.MM.SS_ID
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY_MM_dd_HH_mm_ss");

        String zipFileName = String.format("%03d", idsPublications.size()) + "_" +
                sdf.format(new Date()) + "_" + epmTUtilisateur.getId() + ".zip";
        // Entêtes
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFileName + "\"");
        long length = publicationClausierFacade.downloadPublicationClausier(idsPublications, response.getOutputStream());
        response.setContentLength((int) length);
        response.flushBuffer();
    }

    @GetMapping(value = "/export")
    public final void exportPublicationClausier(final HttpServletResponse response,
                                                final PublicationClausierSearch search, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur)
            throws IOException {
        log.info("export Publication Clausier");
        List<Integer> idsPublications = new ArrayList<>();
        search.setPage(0);
        search.setSize(50);
        int totalPage;
        do {
            PageRepresentation<PublicationClausierBean> result = publicationClausierFacade.findPublications(search, epmTUtilisateur.getEpmTRefOrganisme());
            idsPublications.addAll(result.getContent().stream().map(AbstractBean::getId).collect(Collectors.toList()));
            totalPage = result.getTotalPages();
            search.setPage(search.getPage() + 1);
        } while (search.getPage() < totalPage);
        //construire le nom de l'archive zip suivant la norme XXX_AAAA_MM_DD_HH.MM.SS_ID
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY_MM_dd_HH_mm_ss");

        String zipFileName = String.format("%03d", idsPublications.size()) + "_" +
                sdf.format(new Date()) + "_" + epmTUtilisateur.getId() + ".zip";
        // Entêtes
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFileName + "\"");
        long length = publicationClausierFacade.downloadPublicationClausier(idsPublications, response.getOutputStream());
        response.setContentLength((int) length);
        response.flushBuffer();
    }

    @PostMapping(value = "/import")
    public final void importPublicationsClausier(@RequestPart @NotEmpty(message = "fichier obligatoire") @Valid MultipartFile file, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) throws IOException {
        log.info("import Publications Clausier");
        publicationClausierFacade.importPublicationClausier(file.getInputStream(), epmTUtilisateur);

    }


}
