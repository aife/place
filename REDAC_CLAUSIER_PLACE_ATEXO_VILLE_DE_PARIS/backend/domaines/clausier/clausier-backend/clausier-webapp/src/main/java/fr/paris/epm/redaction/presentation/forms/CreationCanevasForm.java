package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Formulaire Création Canevas .
 * @author Eddoughmi Younes.
 * @version $Revision$, $Date$, $Author$
 */

public class CreationCanevasForm extends ActionForm {

    /**
     * logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CreationCanevasForm.class);

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Attribut title canevas.
     */
    private String canevasTitle;

    /**
     * Attribut procedure Passation.
     */
    private List<Integer> listProceduresPassationSelectionnees;

    /**
     * id du ref CCAG
     */
    private String refCCAG;

    /**
     * Attribut type document.
     */
    private String typeDocument;

    /**
     * Attribut statut.
     */
    @Deprecated
    private String statut;

    /**
     * Attribut nom Action.
     */
    private String nameAction;

    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private String compatibleEntiteAdjudicatrice;

    /**
     * Attribut nature prestasion.
     */
    private String naturePrestas;

    /**
     * Attribut Collection de type document.
     */
    private Collection typeDocumentCollection;

    /**
     * Attribut Collection de procedure Passation.
     */
    private List<EpmTRefProcedure> procedureCollection;

    /**
     * Attribut Collection nature Prestasion.
     */
    private Collection naturePrestasCollection;

    /**
     * liste ref CCAG
     */
    private Collection listeCCAG;

    private List<Integer> listTypesContratsSelectionnes;

    private Collection typeContratCollection;

    /**
     * Constructeur de la classe CreationCanevasForm.
     */
    public CreationCanevasForm() {
        reset();
    }

    /**
     * permet d'initialiser le formulaire de création de la canevas.
     */
    public void reset() {
        canevasTitle = "";
        typeDocument = "";
        naturePrestas = "";
        statut = "0";
        compatibleEntiteAdjudicatrice = "";

        listProceduresPassationSelectionnees = new ArrayList<>();
        listTypesContratsSelectionnes = new ArrayList<>();
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        compatibleEntiteAdjudicatrice = "";
    }

    /**
     * @return Collection de type Document.
     */
    public final Collection getTypeDocumentCollection() {
        return typeDocumentCollection;
    }

    /**
     * @param valeur pour positionner une collection de type Document.
     */
    public final void setTypeDocumentCollection(final Collection valeur) {
        typeDocumentCollection = valeur;
    }

    /**
     * @return nature de préstation
     */
    public final String getNaturePrestas() {
        return naturePrestas;
    }

    /**
     * @param valeur initialise la nature de préstation.
     */
    public final void setNaturePrestas(final String valeur) {
        naturePrestas = valeur;
    }

    /**
     * @return procedure Passation
     */
    public List<Integer> getListProceduresPassationSelectionnees() {
        return listProceduresPassationSelectionnees;
    }

    /**
     * @param listProceduresPassationSelectionnees initialise la procedure Passation.
     */
    public void setListProceduresPassationSelectionnees(List<Integer> listProceduresPassationSelectionnees) {
        this.listProceduresPassationSelectionnees = listProceduresPassationSelectionnees;
    }

    public List<EpmTRefProcedure> getProcedureCollection() {
        return procedureCollection;
    }

    public void setProcedureCollection(List<EpmTRefProcedure> procedureCollection) {
        this.procedureCollection = procedureCollection;
    }

    /**
     * @return Collection nature Prestasion
     */
    public final Collection getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur initialise la collection de  nature Prestasion.
     */
    public final void setNaturePrestasCollection(final Collection valeur) {
        naturePrestasCollection = valeur;
    }

    /**
     * @return statut du canevas.
     */
    @Deprecated
    public final String getStatut() {
        return statut;
    }

    /**
     * @param valeur initialise le statut du canevas.
     */
    @Deprecated
    public final void setStatut(final String valeur) {
        statut = valeur;
    }

    /**
     * @return type Document
     */
    public final String getTypeDocument() {
        return typeDocument;
    }

    /**
     * @param valeur pour initialiser le type de document.
     */
    public final void setTypeDocument(final String valeur) {
        typeDocument = valeur;
    }

    /**
     * @return Title canevas
     */
    public final String getCanevasTitle() {
        return canevasTitle;
    }

    /**
     * @param valeur pour initialiser le title canevas.
     */
    public final void setCanevasTitle(final String valeur) {
        canevasTitle = valeur;
    }

    /**
     * @return nom Action
     */
    public final String getNameAction() {
        return nameAction;
    }

    /**
     * @param valeur positionne le nom de l'action.
     */
    public final void setNameAction(final String valeur) {
        nameAction = valeur;
    }

    /* methode validate () pour valider le formulaire.
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    public final ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        ActionErrors erreurs = super.validate(mapping, request);
        if (erreurs == null)
            erreurs = new ActionErrors();

        erreurs.clear();
        LOG.debug("canevasTitle: " + canevasTitle);
        if (canevasTitle == null || canevasTitle.trim().equals("")) {
            LOG.debug("canevasTitle error ");
            erreurs.add("canevasTitle", new ActionMessage("creationCanevas.valider.canevasTitle"));
        }

        /* ajout des valeur sélectionnées dans la CanevasForm */
        Iterable<String> proceduresPassationSelectionnees = request.getParameterValues("proceduresPassationSelectionnees") != null ?
                Arrays.asList(request.getParameterValues("proceduresPassationSelectionnees")) :
                (Iterable<String>) request.getSession().getAttribute("proceduresPassationSelectionnees");
        if (proceduresPassationSelectionnees != null) {
            if (proceduresPassationSelectionnees != (Iterable) listProceduresPassationSelectionnees) { // c'est la meme liste ? (meme address)
                listProceduresPassationSelectionnees.clear();
                for (String pps : proceduresPassationSelectionnees)
                    listProceduresPassationSelectionnees.add(Integer.parseInt(pps));
            }
        } else {
            listProceduresPassationSelectionnees.add(0);
        }

        /* vérification que l'utilisateur a fait son choix */
        /*if (listProceduresPassationSelectionnees == null || listProceduresPassationSelectionnees.size() == 0) {
            LOG.debug("procedurePassation error ");
            erreurs.add("procedurePassation", new ActionMessage("creationCanevas.valider.procedurePassation"));
        }*/

        /* si l'utilisateur a chois "TOUS PROCEDURE" -> NULL dans la base de données */
        if (listProceduresPassationSelectionnees.contains(Integer.valueOf(0)))
            listProceduresPassationSelectionnees.clear();

        /* ajout des valeur sélectionnées dans la CanevasForm */
        Iterable<String> typesContratsSelectionnes = request.getParameterValues("typesContratsSelectionnes") != null ?
                Arrays.asList(request.getParameterValues("typesContratsSelectionnes")) :
                (Iterable<String>) request.getSession().getAttribute("typesContratsSelectionnes");
        if (typesContratsSelectionnes != null) {
            if (typesContratsSelectionnes != (Iterable) listTypesContratsSelectionnes) { // c'est la meme liste ? (meme address)
                listTypesContratsSelectionnes.clear();
                for (String tcs : typesContratsSelectionnes)
                    listTypesContratsSelectionnes.add(Integer.parseInt(tcs));
            }
        } else {
            listTypesContratsSelectionnes.add(0);
        }

        /* vérification que l'utilisateur a fait son choix */
        /*if (listTypesContratsSelectionnes == null || listTypesContratsSelectionnes.size() == 0) {
            LOG.debug("typeContrat error ");
            erreurs.add("typeContrat", new ActionMessage("creationCanevas.valider.typeContrat"));
        }*/

        /* si l'utilisateur a chois "TOUS TYPES CONTRATS" -> NULL dans la base de données */
        if (listTypesContratsSelectionnes.contains(Integer.valueOf(0)))
            listTypesContratsSelectionnes.clear();

        LOG.debug("naturePrestas: " + naturePrestas);
        if (naturePrestas == null || naturePrestas.trim().equals("-1")) {
            LOG.debug("naturePrestas error ");
            erreurs.add("naturePrestas", new ActionMessage("creationCanevas.valider.naturePrestas"));
        }

        LOG.debug("typeDocument: " + typeDocument);
        if (typeDocument == null || typeDocument.trim().equals("0")) {
            LOG.debug("typeDocument error ");
            erreurs.add("typeDocument", new ActionMessage("creationCanevas.valider.typeDocument"));
        }

        if (refCCAG == null || refCCAG.trim().equals("-1")) {
            erreurs.add("refCCAG", new ActionMessage(
                    "creationCanevas.valider.ccag"));
        }

        return erreurs;
    }

    /**
     * @return the compatibleEntiteAdjudicatrice
     */
    public final String getCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    /**
     * @param valeur the compatibleEntiteAdjudicatrice to set
     */
    public final void setCompatibleEntiteAdjudicatrice(final String valeur) {
        this.compatibleEntiteAdjudicatrice = valeur;
    }

    public final Collection getListeCCAG() {
        return listeCCAG;
    }

    public final void setListeCCAG(final Collection valeur) {
        this.listeCCAG = valeur;
    }

    public final String getRefCCAG() {
        return refCCAG;
    }

    public final void setRefCCAG(final String valeur) {
        this.refCCAG = valeur;
    }

    public List<Integer> getListTypesContratsSelectionnes() {
        return listTypesContratsSelectionnes;
    }

    public void setListTypesContratsSelectionnes(List<Integer> listTypesContratsSelectionnes) {
        this.listTypesContratsSelectionnes = listTypesContratsSelectionnes;
    }

    public Collection getTypeContratCollection() {
        return typeContratCollection;
    }

    public void setTypeContratCollection(Collection typeContratCollection) {
        this.typeContratCollection = typeContratCollection;
    }

}
