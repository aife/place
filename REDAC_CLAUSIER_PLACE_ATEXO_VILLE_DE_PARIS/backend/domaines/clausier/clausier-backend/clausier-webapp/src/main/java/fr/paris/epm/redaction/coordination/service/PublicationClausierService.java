package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;

import java.util.List;

public interface PublicationClausierService extends GeneriqueServiceSecurise {

    /**
     * @param idPublication id publication à vérifier
     * @return true si cette publication à été déjà activée au moins une fois
     */
    boolean isAlreadyActivated(int idPublication);

    boolean canActivate(Integer idOrganisme);

    void desactivationAllPublication();
    boolean isVersionNameUnique(String version, String plateformeUuid);

    void publicationClausierActivateTClause(int idPublication, List<Integer> idClauses);

    void publicationClausierUpdateClause(int idPublication);

    void publicationClausierUpdateClausePub(int idPublication);

    void publicationClausierUpdateRoleClausePub(int idPublication);

    void publicationClausierUpdateClauseHasTypeContratPub(int idPublication);

    void publicationClausierUpdateClauseHasPotentiellementConditionneePub(int idPublication);

    void publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(int idPublication);

    void publicationClausierActivateTCanevas(int idPublication, List<Integer> idCanevas);

    void publicationClausierUpdateCanevas(int idPublication);

    void publicationClausierUpdateCanevasPub(int idPublication);

    void publicationClausierUpdateCanevasHasProcedurePub(int idPublication);

    void publicationClausierUpdateCanevasHasTypeContratPub(int idPublication);

    void publicationClausierUpdateChapitrePub(int idPublication);

    void publicationClausierUpdateChapitreHasClausePub(int idPublication);

    void rollbackPublicationClausier(int idPublication);


}
