package fr.paris.epm.redaction.metier.objetValeur.document;

import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;

import java.util.Date;
import java.util.Map;

/**
 * la classe clauseTexte qui manipule le texte d'une clause.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$.
 */
public class ClauseTexte extends ClauseDocument {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * correspond selon le type de la clause à soit au : "texte libre" ou "texte prévalorisé" ou "texte hérité".
     */
    private String texteVariable;

    /**
     * texte obligatoire à introduire.
     */
    private boolean texteObligatoire;

    /**
     * la taille du texte variable initialisé à 1.
     */
    private String tailleTexteVariable = Constante.TAILLE_CHAMP_LONG;

    /**
     * date de dernière modification de la clause.
     */
    private Date dateModification;

    /**
     * Id du referentiel tableau utilisé pour appliquer des styles aux tableaux
     * générés.
     */
    private int idRefValeurTableau;
    
    /**
     * Coordonne des cellules invisibles de tableau.
     */
    private Map<String, Boolean> invisibleCoordMap;
    
    private boolean desactiverStyleTitre;

    /**
     * cette méthode renvoie la taille du Texte Variable.
     * @return la taille du Texte Variable.
     */
    public final String getTailleTexteVariable() {
        return tailleTexteVariable;
    }

    /**
     * cette méthode initialise la taille du Texte Variable avec une valeur
     * précise.
     * @param valeur initialise la taille du Texte Variable.
     */
    public final void setTailleTexteVariable(final String valeur) {
        if (!"0".equals(valeur))
            tailleTexteVariable = valeur;
    }

    /**
     * cette méthode prévoit si le champ est obligatoire.
     * @return vrai si le champ est obligatoire, faux siNon.
     */
    public final boolean isTexteObligatoire() {
        return texteObligatoire;
    }

    /**
     * cette méthode initialise l'attribut texteObligatoire avec une valeur
     * donnée.
     * @param valeur initialise l'attribut texteObligatoire.
     */
    public final void setTexteObligatoire(final boolean valeur) {
        texteObligatoire = valeur;
    }

    /**
     * cette méthode renvoie l'attribut texteVariable.
     * @return renvoie l'attribut texteVariable.
     */
    public final String getTexteVariable() {
        return texteVariable;
    }

    /**
     * cette méthode positionne l'attribut texteVariable à une valeur donnée.
     * @param valeur pour initialiser texteVariable.
     */
    public final void setTexteVariable(final String valeur) {
        texteVariable = valeur;
    }

    /**
     * cette méthode permet la conversion de type en String.
     * @return chaîne de caractère convertie.
     * @see java.lang.Object#toString()
     */
    public final String toString() {
        return super.toString().concat("\n     Texte variable? " + texteVariable);
    }

    /**
     * @return date de dernière modification de la clause
     */
    public final Date getDateModification() {
        return dateModification;
    }

    /**
     * @param valeur date de dernière modification de la clause
     */
    public final void setDateModification(final Date valeur) {
        this.dateModification = valeur;
    }

    /**
     * @return clone de clause document.
     */
    public final Object cloneText(ChapitreDocument parent) {
        ClauseTexte clauseTexte = new ClauseTexte();

        clauseTexte.setParent(parent);
        if (getInfoBulle() != null) {
            clauseTexte.setInfoBulle((InfoBulle) getInfoBulle().clone());
        }

        clauseTexte.setId(getId());
        clauseTexte.setRef(getRef());
        clauseTexte.setType(getType());
        clauseTexte.setTexteFixeAvant(getTexteFixeAvant());
        clauseTexte.setTexteFixeApresAlaLigne(isTexteFixeApresAlaLigne());
        clauseTexte.setTexteFixeAvantAlaLigne(isTexteFixeAvantAlaLigne());
        clauseTexte.setTexteFixeApres(getTexteFixeApres());
        clauseTexte.setTerminer(isTerminer());
        clauseTexte.setTexteModifiable(isTexteModifiable());
        clauseTexte.setEtat(getEtat());
        clauseTexte.setActif(getActif());
        clauseTexte.setModifier(isModifier());
        clauseTexte.setTexteVariable(texteVariable);
        clauseTexte.setTexteObligatoire(texteObligatoire);
        clauseTexte.setTailleTexteVariable(tailleTexteVariable);
        clauseTexte.setDateModification(dateModification);
        clauseTexte.setIdRefValeurTableau(idRefValeurTableau);
        clauseTexte.setTableauRedaction(getTableauRedaction());
        return clauseTexte;
    }

    /**
     * @return Id du referentiel tableau utilisé pour appliquer des styles aux
     *         tableaux générés.
     */
    public final int getIdRefValeurTableau() {
        return idRefValeurTableau;
    }

    /**
     * @param valeur Id du referentiel tableau utilisé pour appliquer des styles aux tableaux générés.
     */
    public final void setIdRefValeurTableau(final int valeur) {
        this.idRefValeurTableau = valeur;
    }

    public final Map<String, Boolean> getInvisibleCoordMap() {
        return invisibleCoordMap;
    }

    public final void setInvisibleCoordMap(final Map<String, Boolean> valeur) {
        this.invisibleCoordMap = valeur;
    }

    public final boolean isDesactiverStyleTitre() {
        return desactiverStyleTitre;
    }

    public final void setDesactiverStyleTitre(final boolean valeur) {
        this.desactiverStyleTitre = valeur;
    }
    
    @Override
    public boolean isClauseVide() {
        if (!super.isClauseDocumentVide())
            return false;
        return isChaineVide(texteVariable) && idRefValeurTableau == 0;
    }

}
