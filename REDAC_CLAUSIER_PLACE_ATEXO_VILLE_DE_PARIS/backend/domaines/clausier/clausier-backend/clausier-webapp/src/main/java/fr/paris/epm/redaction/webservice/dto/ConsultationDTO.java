package fr.paris.epm.redaction.webservice.dto;

public class ConsultationDTO {
	private String reference;

	public ConsultationDTO() {
	}

	public String getReference() {
		return reference;
	}

	public void setReference( String reference ) {
		this.reference = reference;
	}

}
