package fr.paris.epm.redaction.presentation.mapper;

import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDocumentType;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
@Named("DirectoryNamedMapper")
public abstract class DirectoryNamedMapper { // TODO: mettre à côté de fr.paris.epm.global.coordination.mapper.DirectoryMapper après la fusion NOYAU-REDAC

	private DirectoryMapper directoryMapper;

	private NoyauProxy noyauProxy;

	@Named("naturePrestationDirectoryBeanById")
	public Directory naturePrestationDirectoryBeanById( final Integer idNaturePrestation ) {
		final EpmTRef epmTRefNature = noyauProxy.getReferentielById(idNaturePrestation, TypeEpmTRefObject.NATURE);
		return directoryMapper.toDirectoryBean(epmTRefNature);
	}

	@Named("labelNaturePrestationById")
	public String labelNaturePrestationById( final Integer idNaturePrestation ) {
		final EpmTRef epmTRefNature = noyauProxy.getReferentielById(idNaturePrestation, TypeEpmTRefObject.NATURE);
		return epmTRefNature.getLibelle();
	}

	@Named("idNaturePrestationFromLibelle")
	public int idNaturePrestationFromLibelle( final String libelle ) {
		if (libelle != null) {
			EpmTRef referentiel = noyauProxy.getReferentielByReference(libelle, TypeEpmTRefObject.NATURE);
			if (referentiel != null) {
				return referentiel.getId();
			}
			else {
				return 0;
			}
		}
		else {
			return 0;
		}
	}

	@Named("uidNaturePrestationFromId")
	public String uidNaturePrestationById( final Integer id ) {
		return  noyauProxy.getReferentielExportableById(id, TypeEpmTRefObject.NATURE).map(EpmTRefImportExport::getUid).orElse(null);
	}

	@Named("uidCCAGFromId")
	public String uidCCAGFromId( final Integer id ) {
		return  noyauProxy.getReferentielExportableById(id, TypeEpmTRefObject.CCAG).map(EpmTRefImportExport::getUid).orElse(null);
	}

	@Named("idCCAGFromUid")
	public Integer idCCAGFromUid( final String uid ) {
		return noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.CCAG).map(EpmTRefImportExport::getId).orElse(null);
	}

	@Named("ccagDirectoryBeanById")
	public Directory ccagDirectoryBeanById( final Integer idCCAG ) {
		EpmTRef epmTRefCCAG = null;
		if (idCCAG == null) {
			epmTRefCCAG = noyauProxy.getReferentielById(0, TypeEpmTRefObject.CCAG);
		} else {
			epmTRefCCAG = noyauProxy.getReferentielById(idCCAG, TypeEpmTRefObject.CCAG);
		}
		return directoryMapper.toDirectoryBean(epmTRefCCAG);
	}

	@Named("uidsFromContrats")
	public Set<String> uidsFromContrats( final Set<EpmTRefTypeContrat> contrats ) {
		return contrats.stream().map(EpmTRefImportExport::getUid).collect(Collectors.toSet());
	}

	@Named("uidsFromProcedures")
	public Set<String> uidsFromProcedures( final Set<EpmTRefProcedure> procedures ) {
		return procedures.stream().map(EpmTRefImportExport::getUid).collect(Collectors.toSet());
	}

	@Named("statutRedactionClausierFromUid")
	public EpmTRefStatutRedactionClausier statutRedactionClausierFromUid( final String uid ) {
		return (EpmTRefStatutRedactionClausier)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.STATUT_REDACTION_CLAUSIERS).orElse(null);
	}

	@Named("documentTypeFromUid")
	public EpmTRefDocumentType documentTypeFromUid( final String uid ) {
		return (EpmTRefDocumentType)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.TYPE_DOCUMENTS).orElse(null);
	}

	@Named("typeDocumentFromUid")
	public EpmTRefTypeDocument typeDocumentFromUid( final String uid ) {
		return (EpmTRefTypeDocument)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.TYPE_DOCUMENTS).orElse(null);
	}

	@Named("typeClauseFromUid")
	public EpmTRefTypeClause typeClauseFromUid( final String uid ) {
		return (EpmTRefTypeClause)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.TYPE_CLAUSES).orElse(null);
	}

	@Named("themeClauseFromUid")
	public EpmTRefThemeClause themeClauseFromUid( final String uid ) {
		return (EpmTRefThemeClause)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.THEME_CLAUSES).orElse(null);
	}

	@Named("procedureFromUid")
	public EpmTRefProcedure procedureFromUid( final String uid ) {
		return (EpmTRefProcedure)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.PROCEDURE).orElse(null);
	}

	@Named("typesContratsFromUids")
	public Set<EpmTRefTypeContrat> typesContratsFromUids( final Set<String> uids ) {
		return uids.stream().map(uid -> this.noyauProxy.<EpmTRefTypeContrat>getReferentielExportableByUid(uid, TypeEpmTRefObject.TYPE_DOCUMENTS))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toSet());
	}

	@Named("proceduresFromUids")
	public Set<EpmTRefProcedure> proceduresFromUids( final Set<String> uids ) {
		return uids.stream().map(uid -> this.noyauProxy.<EpmTRefProcedure>getReferentielExportableByUid(uid, TypeEpmTRefObject.PROCEDURE))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toSet());
	}

	@Named("refPotentiellementConditionneeFromUid")
	public EpmTRefPotentiellementConditionnee refPotentiellementConditionneeFromUid( final String uid ) {
		return (EpmTRefPotentiellementConditionnee)this.noyauProxy.getReferentielExportableByUid(uid, TypeEpmTRefObject.REF_POTENTIELLEMENT_CONDITIONNEES).orElse(null);
	}


	@Named("shortLabelOfProcedureEditeur")
	public static List<String> shortLabelOfProcedureEditeur( final Set<EpmTRefProcedure> procedures ) {
		return procedures.stream().map(DirectoryNamedMapper::shortLabelOfProcedureEditeur).collect(Collectors.toList());
	}

	public static String shortLabelOfProcedureEditeur( final EpmTRefProcedure epmTRefProcedure ) {
		if ( epmTRefProcedure == null ) {
			return null;
		}
		return epmTRefProcedure.getLibelleCourt();
	}

	@Named("shortLabelOfTypeContrat")
	public static List<String> shortLabelOfTypeContrat( final Set<EpmTRefTypeContrat> contrats ) {
		return contrats.stream().map(DirectoryNamedMapper::shortLabelOfTypeContrat).collect(Collectors.toList());
	}


	@Named("directoryListOfTypeContratSet")
	public List<Directory> directoryListOfTypeContratSet( final Set<EpmTRefTypeContrat> contrats ) {
		return contrats.stream().map(epmTRefTypeContrat -> directoryMapper.toDirectoryBean(epmTRefTypeContrat)).collect(Collectors.toList());
	}

	@Named("directoryListOfProcedureSet")
	public List<Directory> directoryListOfProcedureSet( final Set<EpmTRefProcedure> procedures ) {
		return procedures.stream().map(epmTRefProcedure -> directoryMapper.toDirectoryBean(epmTRefProcedure)).collect(Collectors.toList());
	}

	@Named("toDirectoryProcedureBean")
	public Directory toDirectoryProcedureBean( final EpmTRefProcedure procedure ) {
		return directoryMapper.toDirectoryBean(procedure);
	}

	public static String shortLabelOfTypeContrat( final EpmTRefTypeContrat epmTRefTypeContrat ) {
		if ( epmTRefTypeContrat == null ) {
			return null;
		}
		return epmTRefTypeContrat.getAbreviation();
	}

	public void setDirectoryMapper( final DirectoryMapper directoryMapper ) {
		this.directoryMapper = directoryMapper;
	}

	public void setNoyauProxy( final NoyauProxy noyauProxy ) {
		this.noyauProxy = noyauProxy;
	}
}
