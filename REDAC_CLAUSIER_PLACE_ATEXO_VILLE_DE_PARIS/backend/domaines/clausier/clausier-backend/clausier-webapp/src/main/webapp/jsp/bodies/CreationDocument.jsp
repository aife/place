<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${empty accesModeSaaS}">
<div class="main-part">
<table><tr><td nowrap="true"></td></tr></table>

    <logic:equal name="typeAction" value="C">
		<div class="breadcrumbs">
			<bean:message key="CreationDocument.titre.creationDocument" />
		</div>
		<atexo:fichePratique reference="FPR03" key="common.fichePratique"/>
	</logic:equal>
	<logic:equal name="typeAction" value="D">
	<div class="breadcrumbs">
		<bean:message key="CreationDocument.titre.duplicationDocument" />
	</div>
	</logic:equal>
	<logic:equal name="typeAction" value="M">
	<div class="breadcrumbs">
		<bean:message key="CreationDocument.titre.modificationDocument" />
	</div>
	</logic:equal>

	<div class="breaker"></div>
	<logic:messagesPresent>
		<div class="form-bloc-erreur msg-erreur">
			<div class="top">
				<span class="left"></span><span class="right"></span>
			</div>
			<div class="content">
				<div class="title"><bean:message key="erreur.texte.generique"/></div>
		        <html:errors  />
			</div>
			<div class="breaker"></div>
			<div class="bottom">
				<span class="left"></span><span class="right"></span>
			</div>
		</div>
	</logic:messagesPresent>
	<!--Debut bloc Infos clause-->
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span>
			<span class="right"></span>
		</div>
		<div class="content">
			<div class="top-link">
				<a class="toggle-recap" onclick="toggleRecapPanel(this,'recap-details');" href="#"></a>
				<div class="consultation-line">
					<div class="line">
						<span class="intitule-bloc"><bean:message key="CreationDocument.txt.numConsultation" /></span>
						<span class="content-bloc-moyen">${frmCreationDocument.numConsultation} : ${frmCreationDocument.intituleConsultation}</span>
					</div>
				</div>
			</div>
			<div class="breaker"></div>
			<div id="recap-details" style="display: block;">
				<div class="breaker"></div>
				<div class="column-moyen">
					<span class="intitule"><bean:message
							key="CreationDocument.txt.pouvoirAdj" />
					</span>
					<div class="content-bloc">
						${frmCreationDocument.pouvoirAdjudicateur}
					</div>
				</div>
				<div class="column-moyen">
					<span class="intitule"><bean:message
							key="CreationDocument.txt.direcServ" />
					</span>
					<div class="content-bloc">
						${frmCreationDocument.directionService}
					</div>
				</div>
				<div class="column-moyen">
					<span class="intitule"><bean:message
							key="CreationDocument.txt.procedurePassation" />
					</span>
					<span class="content-bloc">${frmCreationDocument.procedurePassation}</span>
				</div>
				<div class="column-moyen">
					<span class="intitule"><bean:message
							key="CreationDocument.txt.naturePrestation" />
					</span>
					${frmCreationDocument.naturePrestation}
				</div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span>
			<span class="right"></span>
		</div>
	</div>
	<div class="spacer"></div>	
	
	<jsp:include page="/jsp/bodies/creationDocumentFormulaire.jsp"></jsp:include>

</div>
<!--Fin main-part-->
</c:if>
<c:if test="${!empty accesModeSaaS}">
	<jsp:include page="/jsp/bodies/creationDocumentFormulaire.jsp"></jsp:include>
</c:if>