
function preAjouterModificationLogo() {
	document.getElementById('aiguillage').value = "ajouterLogo";
	document.getElementById('modifierLogoForm').action = 'modifierLogoProcess.epm';
	document.getElementById('modifierLogoForm').submit();
}

function supprimerLogo() {
	document.getElementById('aiguillage').value = "supprimerLogo";
	document.getElementById('modifierLogoForm').action = 'modifierLogoProcess.epm';
	document.getElementById('modifierLogoForm').submit();
}

function popUpPrevisualiserLogo() {
	var url = 'previsualiserLogoPageGarde.epm';
	window.open(url, 'EPM', 'menubar=no, status=no, scrollbars=yes, width=400, height=190');
}

//~~~~~~~~~~~~Preview file bedore upload~~~~~~~~~~~~//
function loadname(img, previewName) {
	var isIE = false;
	if (jQuery.browser.msie) {
		isIE = true;
	}
	var path = img.value;
	var ext = path.substring(path.lastIndexOf('.') + 1).toLowerCase();
	if (ext = "gif" || "jpeg" || "jpg" || "png") {
		if (isIE) {
		  document.getElementById(previewName.id.toString()).src = path;
			return;
		} else {
			if (img.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#' + previewName).attr('src', e.target.result);
				}
				reader.readAsDataURL(img.files[0]);
			}
		}
	} 
//	else {
//		alert("Veuillez choisir un format valide!");
//	}
}

var loadImageFile = (function () {
	if (window.FileReader) {
		var oPreviewImg = null, oFReader = new window.FileReader(),
		rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

		oFReader.onload = function (oFREvent) {
			if (!oPreviewImg) {
				var newPreview = document.getElementById("imagePreview");
				oPreviewImg = new Image();
				oPreviewImg.style.width = (newPreview.offsetWidth).toString() + "px";
				oPreviewImg.style.height = (newPreview.offsetHeight).toString() + "px";
				newPreview.appendChild(oPreviewImg);
			}
			oPreviewImg.src = oFREvent.target.result;
		};

		return function () {
			var aFiles = document.getElementById("imageInput").files;
			if (aFiles.length === 0) { return; }
			if (!rFilter.test(aFiles[0].type)) { return; }
			oFReader.readAsDataURL(aFiles[0]);
			jQuery('.thumbnail').show();
		}
	}
	if (navigator.appName === "Microsoft Internet Explorer") {
		return function () {
			document.getElementById("imagePreview").filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = document.getElementById("imageInput").value;
			jQuery('.thumbnail').show();
		}
	}
})();
//~~~~~~~~~~~~End Preview file bedore upload~~~~~~~~~~~~//
