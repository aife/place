package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.metier.redaction.ClauseViewCritere;
import fr.paris.epm.noyau.metier.redaction.RoleClauseCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.redaction.metier.objetValeur.ClauseExport;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;

import java.util.List;

/**
 * Interface qui expose les méthodes offertes par la clause.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public interface ClauseFacadeGWT {

    /**
     * Permet d'ajouter un objet dans la base de donnée.
     * @param objet l'objet à ajouter
     * @return EpmTClause l'objet ajouté
     */
    EpmTClause ajouter(EpmTClause objet, String reference);

    /**
     * Modifie une clause en modifiant ou non les roles.
     */
    EpmTClause modifier(EpmTClause epmTClauseInstance);

    /**
     * Permet de charger un objet de la table epm__t_clause à partir de l'identifiant.
     * @param idClause l'identifiant de l'objet à charger
     * @return Object L'objet chargé de la base
     */
    EpmTClause charger(int idClause);

    /**
     * Permet de charger un objet de la table epm__t_clause_pub à partir de l'identifiant.
     * @param idClause l'identifiant de l'objet à charger
     * @param idPublication l'identifiant de la publication
     * @return Object L'objet chargé de la base
     */
    EpmTClausePub charger(int idClause, int idPublication,String plateformeUuid);

    /**
     * Modifie la clause avec les nouvelles EpmTRolesClause associé.
     * @param valeur : la valeur de la clause
     * @param isPrefAgent : true si cas d'une préférence agent
     * @param idAgent : l'identifiant de la preference agent associé à la clause
     * @param direction : l'identifiant de la oreference direction associé à la clause
     * @param clause la clause à modifier
     * @return EpmTClause la clause modifiée
     * @throws TechnicalException Erreur Technique
     */
    <T extends EpmTRoleClauseAbstract> T modifierPreferences(final T valeur, final boolean isPrefAgent, Integer idAgent,
                                       Integer direction, EpmTClauseAbstract clause) throws TechnicalException;

    <T extends EpmTRoleClauseAbstract> List<T> modifierPreferences(List<T> rolesClause, final boolean isPrefAgent, Integer idAgent,
                                             Integer direction, EpmTClauseAbstract clause) throws TechnicalException;

    /**
     * Permet de charger la dernière clause ayant la référence passé en paramètre.
     * La recherche de clause se fait sur la table epm__t_clause
     * @param reference La référence de la clause
     * @param idOrganisme l'identifiant de l'organisme
     * @return EpmTClause la clause demandée
     */
    EpmTClauseAbstract chargerDernierClauseByRef(final Boolean isFormeEditeur, final Integer idPublication,
                                                 final String reference, final Integer idOrganisme);

    EpmTClauseAbstract chargerDernierClauseByRef(final Boolean isFormeEditeur, final Integer idPublication,
                                                 final String reference, final Integer idOrganisme, boolean bricolage);

    /**
     * Permet de récupérer la liste des clauses répondant aux critères de
     * recherche.
     * @param critere Objet rassemblant la liste des critères
     * @return List liste des clauses
     */
    Long chercherParCriteres(ClauseCritere critere, List<? extends EpmTClauseAbstract> epmTClauses);

    List<? extends EpmTClauseAbstract> chercherParCriteres(ClauseCritere critere);

    List<? extends EpmTClauseAbstract> chercherParCriteres(ClauseViewCritere critere);

    Clause epmTClauseToClause(EpmTClauseAbstract epmTClause, Boolean isRechercheEditeur, String plateformeUuid);

    List<Clause> epmTClausesToClauses(List<? extends EpmTClauseAbstract> epmTClauses, Boolean isRechercheEditeur, String plateformeUuid);

    /**
     * Retourne la liste des clause en supprimant le HTML de l'apercu de leur
     * contenu.
     */
    List chercherParCriteresSansHTML(ClauseCritere critere) throws TechnicalException;
    List chercherParCriteresSansHTML(ClauseViewCritere critere) throws TechnicalException;

    /**
     * Permet de récupérer le contenu d'une clause sous forme de : [Texte fixe
     * avant][formulations/texte prévaliorisé ou hérié][Texte fixe après].
     * @param epmTClauseInstance la clause en question
     * @return le contenu sous forme de String
     * @throws TechnicalException erreur technique
     */
    String getContenuClause(final EpmTClauseAbstract epmTClauseInstance) throws TechnicalException;
       
    /**
     * Permet de récupérer le contenu d'une clause sous forme de : [Texte fixe
     * avant][formulations/texte prévaliorisé ou hérié][Texte fixe après].
     * @param epmTClauseInstance la clause en question
     * @param filtrerHTMLContenu true si on doit enlever le html du contenu, tout le formatage est alors enlevé.
     * @return le contenu sous forme de String
     * @throws TechnicalException erreur technique
     */
    String getContenuClause(final EpmTClauseAbstract epmTClauseInstance, final boolean filtrerHTMLContenu) throws TechnicalException;
    
    /**
     * Permet de charger la dernière clause ayant la référence passé en
     * paramètre.
     * @param idOrganisme l'id de l'organisme connecté 
     * @param critere La référence de la clause
     * @return EpmTClause la clause demandée
     * @throws TechnicalException Erreur Technique
     */
    List<ClauseExport> chercherParCriteresPourExport(final ClauseCritere critere, final Integer idOrganisme) throws TechnicalException;

    EpmTClauseAbstract charger(final int idClause, final boolean ecranEditeur, final boolean clauseEditeur,String plateformeUuid);
    

    /**
     * Méthode de modification des roles clauses
     */
    void modifierRoleClause(EpmTRoleClause roleClause);
    
    /**
     * Mise à jour de la date de validation  des clauses
     */
    EpmTClause miseAJourDataValidation(EpmTClause epmTClause);
    

    EpmTClauseAbstract chargerClausePourModification(final int idClause, final boolean ecranEditeur, final boolean clauseEditeur,
                                                     final boolean duplication, final Integer idOrganisme, final boolean surcharge);

    List<EpmTClause> chercherClauseByRef(ClauseCritere critere);

    List<? extends EpmTRoleClauseAbstract> recupererRolesClause(final EpmTUtilisateur utilisateur, final EpmTClauseAbstract clause, boolean editeur);
    
    /**
     * Récupére les EpmTRoleClause correspondant aux préférences Agent.
     * Remplace la méthode getEpmTRoleClausesAgent de EpmtClause.
     * @param idUtilisateur identifiant de l'utilisateur pour lequel on souhaite récupérer les préférences
     * @param clause la clause
     * @return la liste des EpmTClause. Une liste vide si il n'y a pas de résultat.
     */
    List<EpmTRoleClauseAbstract> recupererRolesClauseAgent(final int idUtilisateur, final EpmTClauseAbstract clause, final boolean rolesActifs);
    
    /**
     * Récupére les EpmTRoleClause correspondant aux préférences directions.
     * Remplace la méthode getEpmTRoleClausesDirection de EpmtClause.
     * @param idDirection identifiant de la direction pour laquelle on souhaite récupérer les préférences
     * @param clause la clause
     * @return la liste des EpmTClause. Une liste vide si il n'y a pas de résultat.
     */
    List<EpmTRoleClauseAbstract> recupererRolesClauseDirection(final int idDirection, final EpmTClauseAbstract clause, final boolean rolesActifs);


    
    /**
     * Recherche la surcharge d'une clause éditeur ) partir de l'identifiant de
     * la clause éditeur et de l'identifiant de l'organisme.
     * @param idOrganisme l'identifiant de l'organisme (null si pas d'organisme)
     * @return La surcharge si elle existe, null sinon.
     * @throws TechnicalException en cas d'erreur dans la recherche.
     */
    EpmTClause chercherDerniereSurchargeActif(final Integer idClause, final Integer idOrganisme);

}
