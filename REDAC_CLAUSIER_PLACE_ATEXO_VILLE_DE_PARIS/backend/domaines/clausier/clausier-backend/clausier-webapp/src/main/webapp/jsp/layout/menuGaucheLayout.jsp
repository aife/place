<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!--Debut colonne gauche-->
<div class="left-part" id="left-part">
    <!--Debut menu -->
    <div id="menu">
        <ul id="menuList">
            <c:set var="administrationEditeur" value="false"/>
            <atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
                <c:set var="administrationEditeur" value="true"/>
			</atexo:filtreSecurite>
			<atexo:filtreSecurite role="ROLE_administrerCanevas" egal="true">
				<c:set var="administrationEditeur" value="true"/>
			</atexo:filtreSecurite>

			<c:if test="${administrationEditeur == 'true' }">
				<li class="<tiles:getAsString name="administrationEditeur"/>">
					<span onclick="toggleMenu(this);">
                        <bean:message key="menuGauche.administrationEditeur" />
                    </span>
					<ul class="ss-menu-closed">
						<atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
							<li class="<tiles:getAsString name="administration.clausesEditeur"/>" onclick="changeSsmenu(this);">
								<a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.clausesEditeur" /></a>
								<ul class="ss-lien">
									<li class=<tiles:getAsString name="administration.clausesEditeur.rechercher"/> onclick="highlightMenuItem(this);">
                                        <c:choose>
                                            <c:when test="${empty accesModeSaaS}">
                                                <a href="listClauses.htm?editeur=yes"><bean:message key="menuGauche.administration.clausesEditeur.rechercher" /></a>
                                            </c:when>
                                            <c:otherwise><!-- ORME recherche -->
                                                <a href="listClauses.htm?editeur=yes&accesModeSaaS=yes"><bean:message key="menuGauche.administration.clausesEditeur.rechercher" /></a>
                                            </c:otherwise>
                                        </c:choose>
									</li>
									<li class="<tiles:getAsString name="administration.clausesEditeur.creer"/>" onclick="highlightMenuItem(this);">
										<a href="CreationClauseEditeurInit.epm"><bean:message key="menuGauche.administration.clausesEditeur.creer" /></a>
									</li>
									<li>
										<a href="javascript:popUpSetSize('/exporterClause.htm?editeur=yes','400','190','yes');"><bean:message key="menuGauche.administration.canevas.exporter" /></a>
									</li>
								</ul>
							</li>
						</atexo:filtreSecurite>
						<atexo:filtreSecurite role="ROLE_canevas" egal="true">
							<li class="<tiles:getAsString name="administration.canevasEditeur"/>" onclick="changeSsmenu(this);">
								<a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.canevasEditeur" /></a>
								<ul class="ss-lien">
									<li class="<tiles:getAsString name="administration.canevas.rechercher"/>" onclick="highlightMenuItem(this);">
                                        <c:choose>
                                            <c:when test="${empty accesModeSaaS}">
                                                <a href="listCanevas.htm?editeur=yes"><bean:message key="menuGauche.administration.canevasEditeur.rechercher" /></a>
                                            </c:when>
                                            <c:otherwise><!-- ORME recherche -->
                                                <a href="listCanevas.htm?editeur=yes&accesModeSaaS=yes"><bean:message key="menuGauche.administration.canevasEditeur.rechercher" /></a>
                                            </c:otherwise>
                                        </c:choose>
									</li>
									<li class="<tiles:getAsString name="administration.canevas.creer"/>" onclick="highlightMenuItem(this);">
										<a href="CreationCanevas1EditeurInit.epm"><bean:message key="menuGauche.administration.canevasEditeur.creer" /></a>
									</li>
								</ul>
							</li>
						</atexo:filtreSecurite>
						<atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
							<li class="<tiles:getAsString name="administration.publicationClausier"/>" onclick="changeSsmenu(this);">
								<a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.publicationClausier" /></a>
								<ul class="ss-lien">
									<atexo:filtreSecurite role="ROLE_publicationVersionClauseEditeur" egal="true">
									<li class=<tiles:getAsString name="administration.publicationClausier.creerVersion"/> onclick="highlightMenuItem(this);">
										<a href="createPublicationClausier.htm"><bean:message key="menuGauche.administration.publicationClausier.creerVersion" /></a>
									</li>
									</atexo:filtreSecurite>
									<li class="<tiles:getAsString name="administration.publicationClausier.historiqueVersion"/>" onclick="highlightMenuItem(this);">
										<a href="listPublicationsClausier.htm"><bean:message key="menuGauche.administration.publicationClausier.historique" /></a>
									</li>
                                </ul>
                            </li>
                        </atexo:filtreSecurite>
                        <atexo:filtreSecurite role="ROLE_gabaritInterministeriel" egal="true">
                            <li class="<tiles:getAsString name="administration.gabarit"/>" onclick="changeSsmenu(this);">
                                <a href="#" onclick="resetSsmenu(this);">
                                    <bean:message key="menuGauche.administration.gabarit.interministeriel"/>
                                </a>
                                <ul class="ss-lien">
                                    <li onclick="highlightMenuItem(this);">
                                        <a href="gabaritInterministerielInit.epm"><bean:message key="menuGauche.administration.gabarit.modifier"/></a>
                                    </li>
								</ul>
							</li>
						</atexo:filtreSecurite>
					</ul>
				</li>
			</c:if>

			<c:choose>
				<c:when test="${administrationEditeur == 'true' }">
					<li class="<tiles:getAsString name="administrationAvecRole"/>">
				</c:when>
				<c:otherwise>
					<li class="<tiles:getAsString name="administrationSansRole"/>">
				</c:otherwise>
			</c:choose>
				<span onclick="toggleMenu(this);">
                    <bean:message key="menuGauche.administration" />
                </span>
				<ul class="ss-menu-closed">
					<atexo:filtreSecurite role="ROLE_clause" egal="true">
						<li class="<tiles:getAsString name="administration.clauses"/>" onclick="changeSsmenu(this);">
							<a href="#" onclick="resetSsmenu(this);"><bean:message
                                    key="menuGauche.administration.clauses"/></a>
							<ul class="ss-lien">
								<li class=<tiles:getAsString name="administration.clauses.rechercher"/> onclick="highlightMenuItem(this);">
                                    <c:choose>
                                        <c:when test="${empty accesModeSaaS}">
                                            <a href="listClauses.htm"><bean:message key="menuGauche.administration.clausesEditeur.rechercher" /></a>
                                        </c:when>
                                        <c:otherwise><!-- ORME recherche -->
                                            <a href="listClauses.htm?accesModeSaaS=yes"><bean:message key="menuGauche.administration.clauses.rechercher"/></a>
                                        </c:otherwise>
                                    </c:choose>
								</li>
								<li class="<tiles:getAsString name="administration.clauses.creer"/>" onclick="highlightMenuItem(this);">
									<a href="CreationClauseInit.epm"><bean:message key="menuGauche.administration.clauses.creer"/></a>
								</li>
								<li>
									<a href="javascript:popUpSetSize('/exporterClause.htm?editeur=no','400','190','yes');">
                                        <bean:message key="menuGauche.administration.canevas.exporter" />
                                    </a>
								</li>

							</ul>
						</li>
					</atexo:filtreSecurite>
					<atexo:filtreSecurite role="ROLE_canevas" egal="true">
						<li class="<tiles:getAsString name="administration.canevas"/>" onclick="changeSsmenu(this);">
							<a href="#" onclick="resetSsmenu(this);"><bean:message
                                    key="menuGauche.administration.canevas"/></a>
							<ul class="ss-lien">
								<li class="<tiles:getAsString name="administration.canevas.rechercher"/>" onclick="highlightMenuItem(this);">
                                    <c:choose>
                                        <c:when test="${empty accesModeSaaS}">
                                            <a href="listCanevas.htm"><bean:message key="menuGauche.administration.canevas.rechercher" /></a>
                                        </c:when>
                                        <c:otherwise><!-- ORME recherche -->
                                            <a href="listCanevas.htm?accesModeSaaS=yes"><bean:message key="menuGauche.administration.canevas.rechercher" /></a>
                                        </c:otherwise>
                                    </c:choose>
								</li>
								<li class="<tiles:getAsString name="administration.canevas.creer"/>" onclick="highlightMenuItem(this);">
									<a href="CreationCanevas1Init.epm"><bean:message key="menuGauche.administration.canevas.creer"/></a>
								</li>
							</ul>
						</li>
					</atexo:filtreSecurite>

                    <atexo:filtreSecurite role="ROLE_gabaritMinisteriel" egal="true">
                        <li class="<tiles:getAsString name="administration.gabarit"/>" onclick="changeSsmenu(this);">
                            <a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.gabarit.interministeriel"/></a>
                            <ul class="ss-lien">
                                <li onclick="highlightMenuItem(this);">
                                    <a href="gabaritMinisterielInit.epm"><bean:message key="menuGauche.administration.gabarit.modifier"/></a>
                                </li>
                            </ul>
                        </li>
                    </atexo:filtreSecurite>

					<atexo:filtreSecurite role="ROLE_modifierLogoDocument" egal="true">
						<li class="<tiles:getAsString name="administration.modifier.logo"/>" onclick="changeSsmenu(this);">
							<a href="modifierLogoInit.epm"><bean:message key="menuGauche.administration.modifier.logo" /></a>
						</li>
					</atexo:filtreSecurite>

				</ul>
			</li>


            <li class="<tiles:getAsString name="redaction"/>">
				<span onclick="toggleMenu(this);"><bean:message key="menuGauche.redaction" /></span>
				<ul class="ss-menu-closed">
                    <atexo:filtreSecurite role="ROLE_document" egal="true">
						<li class="<tiles:getAsString name="redaction.gerer.document"/>" onclick="changeSsmenu(this);">
							<a href="RechercherDocumentInit.epm"><bean:message
                                    key="menuGauche.redaction.gerer.document"/></a>
						</li>
					</atexo:filtreSecurite>

                    <logic:empty name="preferences">

                        <li class="off" onclick="changeSsmenu(this);">
                            <a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.EntiteAchat"/></a>
                            <ul class="ss-lien">
                                <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                    <li class="<tiles:getAsString name="administration.preferences.direction"/>" onclick="highlightMenuItem(this);">
                                        <a href="ParametrageClauseInit.epm?preferences=direction" onclick="resetSsmenu(this);">
                                            <bean:message key="menuGauche.administration.preferences.direction"/>
                                        </a>
                                    </li>
                                </atexo:filtreSecurite>
                                <atexo:filtreSecurite role="ROLE_gabaritDirection" egal="true">
                                    <li class="<tiles:getAsString name="administration.gabarit"/>" onclick="highlightMenuItem(this);">
                                        <a href="gabaritDirectionInit.epm"><bean:message key="menuGauche.administration.gabarit"/></a>
                                    </li>
                                </atexo:filtreSecurite>
                            </ul>
                        </li>

                        <li class="off" onclick="changeSsmenu(this);">
                            <a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.Agent"/></a>
                            <ul class="ss-lien">
                                <atexo:filtreSecurite role="ROLE_document" egal="true">
                                    <li class="<tiles:getAsString name="administration.preferences.agent"/>" onclick="changeSsmenu(this);">
                                        <a href="ParametrageClauseInit.epm?preferences=agent" onclick="resetSsmenu(this);">
                                            <bean:message key="menuGauche.administration.preferences.agent"/>
                                        </a>
                                    </li>
                                </atexo:filtreSecurite>
                                <li class="<tiles:getAsString name="administration.gabarit"/>" onclick="highlightMenuItem(this);">
                                    <a href="gabaritAgentInit.epm"><bean:message key="menuGauche.administration.gabarit"/></a>
                                </li>
                            </ul>
                        </li>
					</logic:empty>

                    <logic:notEmpty name="preferences">
						<logic:equal name="preferences" value="direction">
                            <li class="off" onclick="changeSsmenu(this);">
                                <a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.EntiteAchat"/></a>
                                <ul class="ss-lien">
                                    <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                        <li class="<tiles:getAsString name="administration.preferences.direction"/>" onclick="highlightMenuItem(this);">
                                            <a href="ParametrageClauseInit.epm?preferences=direction" onclick="resetSsmenu(this);">
                                                <bean:message key="menuGauche.administration.preferences.direction"/>
                                            </a>
                                        </li>
                                    </atexo:filtreSecurite>
                                    <atexo:filtreSecurite role="ROLE_gabaritDirection" egal="true">
                                        <li onclick="highlightMenuItem(this);">
                                            <a href="gabaritDirectionInit.epm"><bean:message key="menuGauche.administration.gabarit"/></a>
                                        </li>
                                    </atexo:filtreSecurite>
                                </ul>
                            </li>
						</logic:equal>
						<logic:notEqual name="preferences" value="direction">
                            <li class="off" onclick="changeSsmenu(this);">
                                <a href="#" onclick="resetSsmenu(this);"><bean:message key="menuGauche.administration.Agent"/></a>
                                <ul class="ss-lien">
                                    <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                        <li class="off" onclick="changeSsmenu(this);">
                                            <a href="ParametrageClauseInit.epm?preferences=direction" onclick="resetSsmenu(this);"><bean:message
                                                    key="menuGauche.administration.preferences.direction" /></a>
                                        </li>
                                    </atexo:filtreSecurite>
                                    <atexo:filtreSecurite role="ROLE_document" egal="true">
                                        <li class="<tiles:getAsString name="administration.preferences.agent"/>" onclick="changeSsmenu(this);">
                                            <a href="ParametrageClauseInit.epm?preferences=agent" onclick="resetSsmenu(this);">
                                                <bean:message key="menuGauche.administration.preferences.agent"/>
                                            </a>
                                        </li>
                                    </atexo:filtreSecurite>
                                    <li onclick="highlightMenuItem(this);">
                                        <a href="gabaritAgentInit.epm"><bean:message key="menuGauche.administration.gabarit"/></a>
                                    </li>
                                </ul>
                            </li>
                        </logic:notEqual>
					</logic:notEmpty>
				</ul>
			</li>
		</ul>
	</div>
	<!--Fin menu -->
</div>
<!--Fin colonne gauche-->