<!--Debut main-part-->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="js/pageDeGarde.js"></script>

<style>
    .boutons a.bouton {
        background-color: #eeeeee;
        padding: 8px 12px 8px 24px;
        border: 1px solid #cccccc;
        border-radius: 4px;
        margin-bottom: 10px;
        background-repeat: no-repeat;
        background-position: 8px 10px;
        color: #897f7d;
        float: left;
        display: block;
    }

    .boutons a.bouton:hover {
        text-decoration: none;
        background-color: #3d6ba6;
        color: #ffffff;
        border-color: #3d6ba6;
    }

    .bouton-annuler {
        background-image: url(<atexo:href href='images/picto-delete.gif'/>);
    }

    .bouton-sauvegarder-editer {
        background-image: url(<atexo:href href='images/picto-nouveau-doc.gif'/>);
    }

    .bouton-sauvegarder-quitter {
        background-image: url(<atexo:href href='images/picto-generer.gif'/>);
    }

    .boutons {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

    .boutons-suite {
        display: flex;
        flex-direction: column;
    }

    input[readonly="true"] {
        background: #e4e5e6;
        border-color: #d8d9da;
        color: #979899;
    }

    .form-bloc .content .content-bloc, .form-bloc .content .column-moyen {
        width: auto !important
    }
</style>

<div class="main-part">
	<div class="breadcrumbs">
		<bean:message key="GenerationPageDeGarde.titre" />
	</div>
	<atexo:fichePratique reference="" key="common.fichePratique"/>
	<div class="breaker"></div>
	
	<%--début message erreurs--%>
	<c:set scope="page" var="erreurVisible">display:none</c:set>
	<logic:messagesPresent>
		<c:set scope="page" var="erreurVisible">display:block</c:set>
		<div class="form-bloc-erreur msg-erreur"
			style="<c:out value ="${pageScope.erreurVisible}"/>">
			<div class="top">
				<span class="left"></span><span class="right"></span>
			</div>
			<div class="content">
			<div class="title"><bean:message key="erreur.texte.generique"/></div>
					<logic:messagesPresent property="typePageDeGarde">
							<html:errors property="typePageDeGarde" />
					</logic:messagesPresent>
					<logic:messagesPresent property="nomFichier">
							<html:errors property="nomFichier" />
					</logic:messagesPresent>
					<logic:messagesPresent property="titreDocument">
							<html:errors property="titreDocument" />
					</logic:messagesPresent>
			</div>
			<div class="breaker"></div>
			<div class="bottom">
				<span class="left"></span><span class="right"></span>
			</div>
		</div>
	</logic:messagesPresent>
	<%--fin message erreurs--%>
	
	<!--Debut bloc recap Document-->
	<c:set var="resumeConsultation" value="${initialisationDonneesPageDeGarde.resumeConsultation}"></c:set>
	<div class="form-bloc">
	    <div class="top"><span class="left"></span><span class="right"></span></div>
	    <div class="content">
           	<div class="top-link">
				<a class="toggle-recap" onclick="toggleRecapPanel(this,'recap-details');" href="#"></a>
                   <div class="consultation-line">
                   	<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.numConsultation" /></span>
                   	<span class="content-bloc-long">${resumeConsultation.numero} : ${resumeConsultation.intitule}</span>
                   </div>
               </div>
			<div id="recap-details" style="display: block;">
				<div class="breaker"></div>
				<div class="column-moyen">
					<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.pouvoirAdjudicateur" /></span>
					<div class="content-bloc">
						${resumeConsultation.pouvoirAdjudicateur}
					</div>
				</div>
				<div class="breaker"></div>
				<div class="column-moyen">
					<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.directionService" /></span>
					<div class="content-bloc">${resumeConsultation.direction}</div>
				</div>
				<div class="breaker"></div>
				<div class="column-moyen">
					<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.procedurePassation" /></span>
					${resumeConsultation.procedurePassation}
				</div>
				<div class="breaker"></div>
				<div class="column-moyen">
					<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.naturePrestation" /></span>
					${resumeConsultation.naturePrestation}
				</div>
	        	<div class="breaker"></div>
			</div>
	        <div class="breaker"></div>
	    </div>
	    <div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>

	<div class="spacer"></div>
	<!--Fin bloc recap Document-->
	<!--Debut partitioner-->


	<div class="form-saisie">
		<div class="form-bloc">
			<div class="top"><span class="left"></span><span class="right"></span></div>
			<div class="content">

				<html:form action="/genererPageDeGardeProcess.epm" enctype="multipart/form-data">

                    <html:hidden property="aiguillage" styleId="aiguillage"/>
                    <html:hidden property="categorie" styleId="categorie" value="${frmGenerationPageDeGarde.categorieDocument}"/>
                    <html:hidden property="idPageDeGarde" styleId="idPageDeGarde" value="${frmGenerationPageDeGarde.idPageDeGarde}"/>

					<div class="column-auto">
						<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.typePageDeGarde" /></span>

                        <c:forEach var="page" items="${initialisationDonneesPageDeGarde.titresPageDeGarde}">
                            <input type="hidden" id="titre-${page.id}" value="${page.libelle}"/>
                        </c:forEach>

                        <c:forEach var="page" items="${initialisationDonneesPageDeGarde.nomsPageDeGarde}">
                            <input type="hidden" id="nom-${page.id}" value="${page.libelle}"/>
                        </c:forEach>


                        <c:if test="${frmGenerationPageDeGarde.mode == 'creation'}">
                                <c:if test="${initialisationDonneesPageDeGarde.typesPageDeGarde.size() > 1}">
                                    <html:select onchange="maj(this.value)" style="box-sizing: content-box; padding: 3px !important;" styleId="typesPageDeGarde" property="typePageDeGarde" styleClass="champ-540" errorStyleClass="error-border champ-540">
                                        <html:option value="0"><bean:message key="GenerationPageDeGarde.txt.selectionnez" /></html:option>
                                        <html:optionsCollection name="initialisationDonneesPageDeGarde" property="typesPageDeGarde" label="libelle" value="id"/>
                                    </html:select>
                                </c:if>

                                <c:if test="${initialisationDonneesPageDeGarde.typesPageDeGarde.size() == 1}">
                                    <html:select onchange="return false;" style="box-sizing: content-box; padding: 3px !important;" styleId="typesPageDeGarde" property="typePageDeGarde" styleClass="champ-540" errorStyleClass="error-border champ-540">
                                        <html:optionsCollection name="initialisationDonneesPageDeGarde" property="typesPageDeGarde" label="libelle" value="id"/>
                                    </html:select>
                                </c:if>
                        </c:if>

                        <c:if test="${frmGenerationPageDeGarde.mode != 'creation'}">
                            <html:select disabled="true" style="box-sizing: content-box; padding: 3px !important;" styleId="typesPageDeGarde" property="typePageDeGarde" styleClass="champ-540" errorStyleClass="error-border champ-540">
                                <html:option  value="0"><bean:message key="GenerationPageDeGarde.txt.selectionnez" /></html:option>
                                <html:optionsCollection name="initialisationDonneesPageDeGarde" property="typesPageDeGarde" label="libelle" value="id"/>
                            </html:select>
                        </c:if>
					</div>
                    <c:if test="${frmGenerationPageDeGarde.categorieDocument == 'dli'}">
                        <div class="column-auto" id="fichier-box">
                            <span class="intitule"><bean:message key="GenerationPageDeGarde.txt.fichier" /></span>
                            <html:file accept=".docx, .xlsx" property="fichier" title="Fichier" onchange="load()" styleId="fichier" />
                        </div>
                    </c:if>
                    <div class="column-auto">
						<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.titreDocument" /></span>

                        <c:if test="${frmGenerationPageDeGarde.mode == 'creation'}">
                            <html:text property="titreDocument" styleId="titreDocument" title="Titre du document" styleClass="champ-540" errorStyleClass="error-border champ-540"/>
                        </c:if>

                        <c:if test="${frmGenerationPageDeGarde.mode != 'creation'}">
                            <html:text readonly="true" property="titreDocument" styleId="titreDocument" title="Titre du document" styleClass="champ-540" errorStyleClass="error-border champ-540"/>
                        </c:if>
                    </div>
					<div class="breaker"></div>
					<div class="column-auto">
						<span class="intitule"><bean:message key="GenerationPageDeGarde.txt.nomFichier" /></span>
						<html:text property="nomFichier" styleId="nomFichier" title="Nom du fichier" styleClass="champ-540" errorStyleClass="error-border champ-540"/><c:if test="${frmGenerationPageDeGarde.idPageDeGarde != null }">.${frmGenerationPageDeGarde.extension}</c:if>
					</div>
				</html:form>

				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left"></span><span class="right"></span></div>
		</div>
	</div>

	<div class="layer">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="boutons">
			<div class="boutons-retour">
				<a class="bouton bouton-annuler" href="javascript:annuler('${prefixeRedirection}', '${frmGenerationPageDeGarde.categorieDocument}');">
                    <bean:message key="GenerationPageDeGarde.txt.bouton.annuler"/>
                </a>
			</div>
			<div class="boutons-suite">
                <a class="bouton bouton-sauvegarder-editer" href="javascript:enregistrer('revenir', '${frmGenerationPageDeGarde.categorieDocument}');"><bean:message key="GenerationPageDeGarde.txt.bouton.enregistrer"/></a>
                <a class="bouton bouton-sauvegarder-quitter" href="javascript:enregistrer('quitter', '${frmGenerationPageDeGarde.categorieDocument}');"><bean:message key="GenerationPageDeGarde.txt.bouton.quitter"/></a>
			</div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin boutons-->

    <c:if test="${frmGenerationPageDeGarde.mode == 'ouverture'}">
        <script>
			ouvrir(${frmGenerationPageDeGarde.idPageDeGarde == null ? 0 : frmGenerationPageDeGarde.idPageDeGarde}, '${frmGenerationPageDeGarde.classname}', ${frmGenerationPageDeGarde.idConsultation});
			annuler('${prefixeRedirection}', '${frmGenerationPageDeGarde.categorieDocument}');
        </script>
    </c:if>
</div>
<!--Fin main-part-->
