package fr.paris.epm.redaction.importExport.bean;

import java.util.*;

public class CanevasImportExport {

    private int idCanevas;

    private String document;

    private String titre;

    private String reference;

    private Date dateCreation;

    private Date dateModification;

    private Date datePremiereValidation;

    private Date dateDerniereValidation;

    private boolean actif;

    private String naturePrestation;

    private String auteur;

    private String etat = "0";

	private String ccag;

    private boolean compatibleEntiteAdjudicatrice = true;

    private boolean canevasEditeur;

    private String statutRedactionClausier;

    private Set<String> contrats = new HashSet<>();

    private List<ChapitreImportExport> chapitres = new ArrayList<>();

    private Set<String> procedures = new HashSet<>();

    public int getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument( String document ) {
        this.document = document;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Date getDatePremiereValidation() {
        return datePremiereValidation;
    }

    public void setDatePremiereValidation(Date datePremiereValidation) {
        this.datePremiereValidation = datePremiereValidation;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getNaturePrestation() {
        return naturePrestation;
    }

    public void setNaturePrestation( String naturePrestation ) {
        this.naturePrestation = naturePrestation;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    public boolean isCanevasEditeur() {
        return canevasEditeur;
    }

    public void setCanevasEditeur(boolean canevasEditeur) {
        this.canevasEditeur = canevasEditeur;
    }

    public String getStatutRedactionClausier() {
        return statutRedactionClausier;
    }

    public void setStatutRedactionClausier( String statutRedactionClausier ) {
        this.statutRedactionClausier = statutRedactionClausier;
    }

    public Set<String> getContrats() {
        return contrats;
    }

    public void setContrats( Set<String> contrats ) {
        this.contrats = contrats;
    }

    public List<ChapitreImportExport> getChapitres() {
        return chapitres;
    }

    public void setChapitres( List<ChapitreImportExport> chapitres ) {
        this.chapitres = chapitres;
    }

    public Set<String> getProcedures() {
        return procedures;
    }

    public void setProcedures( Set<String> procedures ) {
        this.procedures = procedures;
    }

	public String getCcag() {
		return ccag;
	}

	public void setCcag( String ccag ) {
		this.ccag = ccag;
	}
}
