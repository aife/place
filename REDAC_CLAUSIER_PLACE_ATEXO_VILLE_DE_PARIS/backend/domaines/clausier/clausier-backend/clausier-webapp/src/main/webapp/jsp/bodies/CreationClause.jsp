<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<c:choose>
    <c:when test="${editeur != null && editeur}">
        <c:set var="actionEditeur" value="Editeur" />
    </c:when>
    <c:otherwise>
        <c:set var="actionEditeur" value="" />
    </c:otherwise>
</c:choose>

<%--@elvariable id="frmCreationClause" type="fr.paris.epm.redaction.presentation.forms.CreationClauseForm"--%>

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs m-b-2">
        <strong>
            <logic:equal name="typeAction" value="C">
                <bean:message key="CreationClause${actionEditeur}.titre.cree" />
            </logic:equal>
            <logic:equal name="typeAction" value="M">
                <bean:message key="CreationClause${actionEditeur}.titre.modifier" />
            </logic:equal>
            <logic:equal name="typeAction" value="D">
                <bean:message key="CreationClause${actionEditeur}.titre.dupliquer" />
            </logic:equal>
        </strong>
    </div>

    <atexo:fichePratique reference="" key="common.fichePratique"/>
    <div class="breaker"></div>

    <logic:messagesPresent>
        <div class="form-bloc-erreur msg-erreur">
            <div class="top">
                <span class="left"></span>
                <span class="right"></span>
            </div>
            <div class="content">
                <div class="title"><bean:message key="erreur.texte.generique"/></div>
                <html:errors  />
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
    </logic:messagesPresent>

    <!--Debut bloc Infos clause-->
    <html:form method="POST" action="/CreationClause${actionEditeur}Process.epm">

        <input type="hidden" name="nomMethode" id="nomMethode" />

        <script type="text/javascript">
            var mapContratsProceduresIds = {};
            var toutesProcedures = [];

            <logic:notEmpty name="frmCreationClause" property="critereCollection">
                var listeValue = [];
                var listeMultiChoix = [];

                <c:forEach var="potCdt" items="${frmCreationClause.critereCollection}">
                    <%--@elvariable id="potCdt" type="fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee"--%>
                    var listeAssociee = [];

                    <c:forEach var="epmTRefValeurPotCdt" items="${potCdt.valeurs}">
                        <%--@elvariable id="epmTRefValeurPotCdt" type="fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur"--%>
                        var option = [];
                        option.push("${epmTRefValeurPotCdt.id}", "${epmTRefValeurPotCdt.libelle.replaceAll("\\r\\n|\\r|\\n", " ")}");
                        listeAssociee.push(option);
                    </c:forEach>

                    listeValue["${potCdt.id}"] = listeAssociee;
                    listeMultiChoix["${potCdt.id}"] = "${potCdt.multiChoix}";
                </c:forEach>
            </logic:notEmpty>
        </script>

        <div class="form-saisie">
            <div class="form-bloc">
                <div class="top">
                    <span class="left"></span><span class="right"></span>
                </div>
                <div class="content">
                    <c:choose>
                        <c:when test='${typeAction.equals("M")}'>
                        <%--<c:when test='${typeAction.equals("M") || typeAction.equals("D")}'>--%>
                            <c:set var="isDisabled" value="true"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="isDisabled" value="false"/>
                        </c:otherwise>
                    </c:choose>

                    <c:if test='${typeAction.equals("M")}'>
                        <div class="column-moyen" align="left">
                            <span class="intitule-long width-200"><bean:message key="CreationClause.txt.reference" /></span>
                            <c:out value="${frmCreationClause.reference}" />
                        </div>

                        <div class="column-moyen" >
                            <span class="intitule-long width-200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>
                    </c:if>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.typeClause" /></span>
                        <html:select property="typeClause" title="Type de clause" disabled="${isDisabled}"
                                     styleId="typeClause" styleClass="width-200" errorStyleClass="width-200 error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="typeClauseCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.typeDocument" /></span>
                        <html:select property="typeDocument" title="Type de document" disabled="${isDisabled}"
                                     styleId="typeDocument" styleClass="width-200" errorStyleClass="width-200 error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="typeDocumentCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="creationCanevas1.txt.typeContrat"/></span>
                        <div class="checklist-box liste-procedures" title="Type de contrat" id="typeContratDivId">

                            <c:set var="typesContratsSelectionnes" value="${frmCreationClause.listTypesContratsSelectionnes}" scope="request" />
                            <c:set var="typesContratsSelectionnes" value="${frmCreationClause.listTypesContratsSelectionnes}" scope="session" />

                            <label for="typesContratsSelectionnes_0">
                                <input type="checkbox" id="typesContratsSelectionnes_0" name="typesContratsSelectionnes" value="0"
                                       onclick="javascript:typeContratCheckAll();"
                                       <c:if test="${isDisabled eq true}">disabled="disabled"</c:if>
                                       <c:if test="${frmCreationClause.listTypesContratsSelectionnes.size() == 0 ||
                                        frmCreationClause.listTypesContratsSelectionnes.contains(Integer.valueOf(0))}">checked</c:if> />
                                <bean:message key="redaction.txt.neDependPasDeTypeDeContrat" />
                            </label>

                            <hr>

                            <c:forEach items="${frmCreationClause.typeContratCollection}" var="typeContratItem" varStatus="status">
                                <label for="typesContratsSelectionnes_${typeContratItem.id}">
                                    <input type="checkbox" id="typesContratsSelectionnes_${typeContratItem.id}" class="typesContratsSelectionnes"
                                           name="typesContratsSelectionnes" value="${typeContratItem.id}"
                                           onclick="javascript:typeContratCheck();"
                                           <c:if test="${isDisabled eq true}">disabled="disabled"</c:if>
                                           <c:if test="${frmCreationClause.listTypesContratsSelectionnes.contains(typeContratItem.id)}">checked</c:if> />
                                        ${typeContratItem.libelle}
                                </label>

                                <script type="text/javascript">
                                    mapContratsProceduresIds[${typeContratItem.id}] = ${typeContratItem.idsProcedures};
                                </script>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.procedurePassation" /></span>
                        <html:select property="procedurePassation" title="Procédure de passation" disabled="${isDisabled}"
                                     styleId="procedurePassation" styleClass="width-450" errorStyleClass="liste-procedures error-border">
                            <html:option value="0"><bean:message key="redaction.txt.Toutes"/></html:option>
                            <c:forEach var="procedure" items="${frmCreationClause.procedurePassationCollection}">
                                <html:option styleId="proceduresPassationSelectionnees_${procedure.id}"
                                             styleClass="proceduresPassationSelectionnees" value="${procedure.id}">
                                    ${procedure.libelle}
                                </html:option>
                                <script type="text/javascript">
                                    toutesProcedures.push(${procedure.id})
                                </script>
                            </c:forEach>
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.naturePrestation" /></span>
                        <html:select property="naturePrestas" title="Nature des prestations" disabled="${isDisabled}"
                                     styleId="naturePrestas" styleClass="width-200" errorStyleClass="width-200 error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="naturePrestasCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.theme" /></span>
                        <html:select property="themeClause" title="Théme"
                                     styleId="themeClause" styleClass="width-450" errorStyleClass="width-150 error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="themeClauseCollection" label="libelle" value="id" />
                        </html:select>
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.motCles" /></span>
                        <html:text property="motsCles" title="Mots-clés" styleId="motsCles" styleClass="width-450" />
                    </div>

                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.statutClause" /></span>
                        <div class="content-bloc-moyen">
                            <div class="radio-choice">
                                <html:radio value="1" property="statut" title="Actif" styleId="statut" />
                                <bean:message key="CreationClause.txt.oui" />
                            </div>
                            <div class="radio-choice">
                                <html:radio value="0" property="statut" title="Inactif" styleId="statut" />
                                <bean:message key="CreationClause.txt.non" />
                            </div>
                            <div class="breaker"></div>
                            <c:if test="${entiteAdjudicatrice != null}">
                                <div class="radio-choice">
                                    <html:checkbox property="compatibleEntiteAdjudicatrice">
                                        <bean:message key="CreationClause.txt.compatibleEntiteAdjudicatrice" />
                                    </html:checkbox>
                                </div>
                            </c:if>
                        </div>
                    </div>

                    <div id="infoBulle" class="info-bulle" onmouseover="mouseOverInfoBulle();" onmouseout="mouseOutInfoBulle();">
                    </div>

                    <c:forEach items="${frmCreationClause.valeurPotentiellementConditionneList}" var="valeurPotCond" varStatus="itemStatus">
                        <div id="potentiellementConditionnee${itemStatus.index}Div" <c:if test="${itemStatus.index > 0}"> style="display: none"</c:if>>
                            <div class="separator"></div>
                            <div class="line">
                                <span class="intitule-long width-200">
                                    <bean:message key="CreationClause.txt.PotentiellementConditionnee" />&nbsp;${itemStatus.index + 1}&nbsp;<c:if test="${itemStatus.index == 1}"><atexo:infoBulle reference="IB-37" />&nbsp;</c:if>:
                                </span>

                                <div class="content-bloc-moyen">
                                    <div class="radio-choice">
                                        <html:radio value="true" styleId="potCdtOui${itemStatus.index}" property="valeurPotentiellementConditionneList[${itemStatus.index}].potCdt" title="Oui"
                                                    onclick="conditionnementOui(${itemStatus.index})"
                                                    disabled="${isDisabled}"/>
                                        <bean:message key="CreationClause.txt.oui"/>
                                    </div>
                                    <div class="radio-choice">
                                        <html:radio value="false" styleId="potCdtNon${itemStatus.index}" property="valeurPotentiellementConditionneList[${itemStatus.index}].potCdt" title="Non"
                                                    onclick="conditionnementNon(${itemStatus.index})" disabled="${isDisabled}"/>
                                        <bean:message key="CreationClause.txt.non"/>
                                    </div>
                                </div>
                            </div>

                            <div id="potCdt-criteres${itemStatus.index}" style="display: none">
                                <div class="line">
                                    <span class="intitule-long width-200"><bean:message key="CreationClause.txt.critere"/></span>
                                    <html:select property="valeurPotentiellementConditionneList[${itemStatus.index}].critere" title="Critère" disabled="${isDisabled}"
                                                 styleId="criterePotCdt${itemStatus.index}" styleClass="width-450" errorStyleClass="width-450 error-border"
                                                 onchange="javascript:onChangeCritere(this.value, ${itemStatus.index}, ${isDisabled});">
                                        <html:option value="0"><bean:message key="redaction.txt.selectionner"/></html:option>
                                        <html:optionsCollection property="critereCollection" label="libelle" value="id"/>
                                    </html:select>
                                </div>
                                <div class="breaker"></div>

                                <!-- Liste a choix unique -->
                                <div class="line">
                                    <span class="intitule-long width-200"><bean:message key="CreationClause.txt.valeur"/></span>

                                    <input type="hidden" id="valeurInBase${itemStatus.index}"
                                           value="${frmCreationClause.valeurPotentiellementConditionneList.get(itemStatus.index).valeurList}" />

                                    <logic:messagesPresent property="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees">
                                        <select id="valeurCritere${itemStatus.index}" name="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees"
                                                class="width-200 error-border"
                                                title="Valeur Critére" <c:if test="${isDisabled eq true}">disabled</c:if>>
                                        </select>
                                    </logic:messagesPresent>
                                    <logic:messagesNotPresent property="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees">
                                        <select id="valeurCritere${itemStatus.index}" name="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees"
                                                class="width-200"
                                                title="Valeur Critére" <c:if test="${isDisabled eq true}">disabled</c:if>>
                                        </select>
                                    </logic:messagesNotPresent>
                                </div>

                                <!-- Pour contenir la liste multi choix generee dynamiquement -->
                                <div class="line">
                                    <span class="intitule-long width-200"><bean:message key="CreationClause.txt.valeur"/></span>

                                    <div class="checklist-box liste-procedures" id="critereMultiSelectDivId${itemStatus.index}">
                                        <%--
                                        <label for="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees_0">
                                            <input type="checkbox" value="0"
                                                   id="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees_0"
                                                   name="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees"
                                                   class="valeurPotentiellementConditionneList${itemStatus.index}valeurListSelectionnees"
                                                   <c:if test="${isDisabled eq true}">disabled="disabled"</c:if> />
                                            <bean:message key="redaction.txt.selectionner" />
                                        </label>
                                        --%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <div class="separator"></div>
                    <div class="line">
                        <span class="intitule-long width-200"><bean:message key="CreationClause.txt.infoBulle" /></span>
                        <div class="content-bloc-moyen">
                            <div class="radio-choice">
                                <html:radio value="1" styleId="radioInfoBulleOui" property="infoBulle" title="Oui" onclick="infoBulleOui();" />
                                <bean:message key="CreationClause.txt.oui" />
                            </div>
                            <div class="radio-choice">
                                <html:radio value="0" styleId="radioInfoBulleNon" property="infoBulle" title="Non" onclick="infoBulleNon();" />
                                <bean:message key="CreationClause.txt.non" />
                            </div>
                        </div>
                    </div>

                    <div id="info-bulle-criteres" style="display: none">
                        <div class="line">
                            <div class="column-auto">
                                <span class="intitule-long width-200"><bean:message key="CreationClause.txt.valeur"/></span>
                                <html:textarea rows="4" title="Texte Info-bulle"
                                               property="texteInfoBulle" styleId="texteInfoBulle" styleClass="valeur" errorStyleClass="error-border-valeur"
                                               onkeyup="limite(this,200);" onkeydown="limite(this,200);"/>
                            </div>

                            <div class="column-auto">
                                <span class="intitule-auto"><bean:message key="CreationClause.txt.lienHyperTexte"/></span>
                                <div class="breaker"></div>
                                <span class="intitule-auto">
                                    <html:text property="lienHypertext" styleId="lienHypertext" title="Lien hypertexte" styleClass="champ-file-moyen" size="25" />
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>
        </div>
        <!--Fin bloc Infos clause-->

        <!--Debut boutons-->
        <div class="spacer"></div>
        <div class="boutons">
            <c:choose>
                <c:when test="${typeAction eq 'C'}">
                    <a href="CreationClause${actionEditeur}Init.epm" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
                </c:when>
                <c:when test="${empty accesModeSaaS && (editeur != null && editeur)}">
                    <a href="listClauses.htm?editeur=yes" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
                </c:when>
                <c:when test="${empty accesModeSaaS && (editeur == null || !editeur)}">
                    <a href="listClauses.htm?editeur=no" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
                </c:when>
                <c:when test="${not empty accesModeSaaS && (editeur != null && editeur)}"> <!-- ORME recherche -->
                    <a href="listClauses.htm?editeur=yes&accesModeSaaS=yes" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
                </c:when>
                <c:when test="${not empty accesModeSaaS && (editeur == null || !editeur)}"> <!-- ORME recherche -->
                    <a href="listClauses.htm?editeur=no&accesModeSaaS=yes" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
                </c:when>
            </c:choose>
            <a href="javascript:document.forms[0].submit()" class="valider">
                <bean:message  key="redaction.txt.valider" />
            </a>
            <logic:equal name="typeAction" value="M">
                <a href="javascript:save();" class="enregistrer">
                    <bean:message  key="redaction.txt.enregistrer" />
                </a>
            </logic:equal>
        </div>
    </html:form>
    <!--Fin boutons-->
</div>

<script type="text/javascript">

    filtrerProceduresClause();

    jQuery(document).ready(function () {
        filtrerProceduresClause();

        var index = 0;
        while (true) {
            jQuery('#potentiellementConditionnee' + index + 'Div').show();
            if (jQuery('#potCdtOui' + index).prop('checked')) {
                conditionnementOui(index);

                var critereValeur = jQuery('#criterePotCdt' + index).val();

                jQuery.parseJSON(jQuery('#valeurInBase' + index).val()).forEach(function (v) {
                    if (listeMultiChoix[critereValeur])
                        jQuery('#valeurPotentiellementConditionneList' + index + 'valeurListSelectionnees_' + v).prop('checked', true);
                    else
                        jQuery('#valeurCritere' + index + ' option[value="' + v + '"]').prop('selected', true);
                });
            } else {
                break;
            }
            index++;
        }

        if (jQuery('#radioInfoBulleOui').prop('checked'))
            infoBulleOui();
        else
            infoBulleNon();

    });

    function infoBulleOui() {
        jQuery('#info-bulle-criteres').show();
    }

    function infoBulleNon() {
        jQuery('#info-bulle-criteres').hide();
    }

    function conditionnementOui(index) {
        jQuery('#potCdt-criteres' + index).show();
        jQuery('#potentiellementConditionnee' + (index + 1) + 'Div').show();
        onChangeCritere(jQuery('#criterePotCdt' + index).val(), index, ${isDisabled});
    }

    function conditionnementNon(index) {
        jQuery('#potCdt-criteres' + index).hide();
        jQuery('#potCdt-criteres' + (index + 1)).hide();
        jQuery('#potentiellementConditionnee' + (index + 1) + 'Div').hide();

        annuleConditionnement(index);
        annuleConditionnement(index + 1);
    }

    function annuleConditionnement(index) {
        jQuery('#potCdtOui' + index).prop("checked", false);
        jQuery('#potCdtNon' + index).prop("checked", true);

        jQuery('#criterePotCdt' + index + ' option').prop("selected", false);
        jQuery('#criterePotCdt' + index + ' option[value="0"]').prop("selected", true);

        jQuery('#valeurCritere' + index + ' option').prop("selected", false);
        jQuery('#valeurCritere' + index + ' option[value="0"]').prop("selected", true);

        jQuery('.valeurPotentiellementConditionneList' + index + 'valeurListSelectionnees').prop("checked", false);
    }

    function typeContratCheckAll() {
        if (jQuery("#typesContratsSelectionnes_0").is(":checked"))
            jQuery(".typesContratsSelectionnes").prop("checked", false);

        filtrerProceduresClause();
    }

    function typeContratCheck() {
        if (jQuery(".typesContratsSelectionnes:checked").length > 0)
            jQuery("#typesContratsSelectionnes_0").prop("checked", false);

        filtrerProceduresClause();
    }

    function filtrerProceduresClause() {

        var idsContrats = [];
        if (jQuery("#typesContratsSelectionnes_0").is(":checked")) {
            idsContrats.push(0);
        } else {
            var typesContratsSelectionnes = jQuery(".typesContratsSelectionnes:checked");

            for (var i = 0; i < typesContratsSelectionnes.length; i++) {
                var typeContratId = jQuery(typesContratsSelectionnes[i]).attr("id").substring("typesContratsSelectionnes_".length);
                idsContrats.push(parseInt(typeContratId));
            }
        }

        var proceduresCommunes = toutesProcedures;
        if (idsContrats[0] !== 0) {
            proceduresCommunes = getProceduresCommunes(idsContrats);
        }

        for (var i = 0; i < toutesProcedures.length; i++) {
            var proceduresPassationSelectionnees = jQuery("#proceduresPassationSelectionnees_" + toutesProcedures[i]);

            if (proceduresCommunes.indexOf(toutesProcedures[i]) === -1) {
                proceduresPassationSelectionnees.hide();
                proceduresPassationSelectionnees.prop("disabled", true); // pour IE
                proceduresPassationSelectionnees.prop("selected", false);
            } else {
                proceduresPassationSelectionnees.show();
                proceduresPassationSelectionnees.prop("disabled", false); // pour IE
            }
        }
    }

    function getProceduresCommunes(idsContrats) {
        var proceduresCommunes = toutesProcedures;

        for (var i = 0; i < idsContrats.length; i++) {
            var intersection = [];
            for (var j = 0; j < proceduresCommunes.length; j++) {
                if (mapContratsProceduresIds[idsContrats[i]].indexOf(proceduresCommunes[j]) !== -1)
                    intersection.push(proceduresCommunes[j]);
            }

            proceduresCommunes = intersection;
            if (proceduresCommunes.length === 0)
                break;
        }
        return proceduresCommunes;
    }

    // Declenchee lors du choix d'un critere d'une clause potentiellement
    // conditionnee
    function onChangeCritere(critereSelectionne, indexCritere, isDisabled) {

        var critereMultiSelectDivId = jQuery('#critereMultiSelectDivId' + indexCritere);
        var valeurCritere = jQuery('#valeurCritere' + indexCritere);

        valeurCritere.html('');
        critereMultiSelectDivId.html('');

        valeurCritere.parent().hide();
        critereMultiSelectDivId.parent().hide();

        if (listeValue[critereSelectionne]) {

            if (listeMultiChoix[critereSelectionne]) {
                /*critereMultiSelectDivId.append(
                    '<label for="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees_0">' +
                    '   <input type="checkbox" value="0"' +
                    '          id="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees_0"' +
                    '          name="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees"' +
                    '          class="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees" />' +
                    '   Séléctionnez...' +
                    '</label>');*/

                for (var i = 0; i < listeValue[critereSelectionne].length; i = i + 1)
                    critereMultiSelectDivId.append(
                        '<label for="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees_' + listeValue[critereSelectionne][i][0] + '">' +
                        '   <input type="checkbox" value="' + listeValue[critereSelectionne][i][0] + '"' +
                        '          id="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees_' + listeValue[critereSelectionne][i][0] + '"' +
                        '          name="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees"' +
                        '          class="valeurPotentiellementConditionneList' + indexCritere + 'valeurListSelectionnees" />' +
                        listeValue[critereSelectionne][i][1] +
                        '</label>');

                critereMultiSelectDivId.parent().show();

            } else {
                valeurCritere.append('<option value="0" selected="selected">Séléctionnez...</option>');

                for (var i = 0; i < listeValue[critereSelectionne].length; i = i + 1)
                    valeurCritere.append('<option value="' + listeValue[critereSelectionne][i][0] + '">' + listeValue[critereSelectionne][i][1] + '</option>');

                valeurCritere.parent().show();
            }
        } else {
            valeurCritere.append('<option value="0" selected="selected">Séléctionnez...</option>');
            valeurCritere.parent().show();
        }
    }

</script>
<!--Fin main-part-->
