package fr.paris.epm.redaction.presentation.bean;

public class ActualisationStatut {

	private final Long compteur;

	public ActualisationStatut( Long compteur ) {
		this.compteur = compteur;
	}

	public Long getCompteur() {
		return compteur;
	}

	@Override
	public String toString() {
		return "[ count='" + compteur + " ]";
	}
}
