package fr.paris.epm.redaction.presentation.bean;

public class DocumentStatut {

	private final int id;
	private final String statut;
	private final String etat;
	private final String jeton;
	private final String utilisateur;

	public DocumentStatut( int id, String statut, String etat, String jeton, String utilisateur ) {
		this.id = id;
		this.statut = statut;
		this.etat = etat;
		this.jeton = jeton;
		this.utilisateur = utilisateur;
	}

	public String getStatut() {
		return statut;
	}

	public String getEtat() {
		return etat;
	}

	public String getJeton() {
		return jeton;
	}

	public String getUtilisateur() {
		return utilisateur;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "[ id='" + id + "', statut='" + statut + ", etat = '" + etat + ", jeton = '" + jeton + ", utilisateur = '" + utilisateur + " ]";
	}
}
