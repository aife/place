package fr.paris.epm.redaction.coordination.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.thirdparty.guava.common.base.Stopwatch;
import com.google.gwt.thirdparty.guava.common.collect.Sets;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.metier.redaction.CanevasPubCritere;
import fr.paris.epm.noyau.metier.redaction.ClausePubCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.importExport.bean.*;
import fr.paris.epm.redaction.importExport.mapper.CanevasPubMapper;
import fr.paris.epm.redaction.importExport.mapper.ClausePotentiellementConditionneePubMapper;
import fr.paris.epm.redaction.importExport.mapper.ClausePubMapper;
import fr.paris.epm.redaction.importExport.mapper.ReferentielMapper;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ImportExportServiceImpl implements ImportExportService {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.0");

    private final Logger log = LoggerFactory.getLogger(ImportExportServiceImpl.class);

    private CanevasPubMapper canevasPubMapper;

    private ClausePubMapper clausePubMapper;

    private ClausePotentiellementConditionneePubMapper clausePotentiellementConditionneePubMapper;

    private ReferentielMapper referentielMapper;

    private ObjectMapper objectMapper;

    private RedactionServiceSecurise redactionService;

    private ReferentielsServiceSecurise referentielsService;

    private PublicationClausierService publicationClausierService;

    @Override
    public File exporterClausier(int idPublication, EpmTRefOrganisme epmTRefOrganisme) {
        String clausierImportExportJSON;
        ClausierImportExport clausierImportExport = constituerClausierImportExport(idPublication, epmTRefOrganisme);

        try {
            clausierImportExportJSON = objectMapper.writeValueAsString(clausierImportExport);
        } catch (JsonProcessingException e) {
            log.error("Convertir le DTO clausierImportExport en json", e);
            throw new TechnicalException(e.fillInStackTrace());
        }

        try {
            return creationFichierImportExport(clausierImportExportJSON, File.createTempFile("clausierImportExportJSON", ".json"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public File exporterReferentiel(int idPublication) {
        String referentielImportExportJSON;
        ReferentielAllImportExport referentiels = constituerReferentielImportExport(idPublication);

        try {
            referentielImportExportJSON = objectMapper.writeValueAsString(referentiels);
        } catch (JsonProcessingException e) {
            log.error("Convertir le DTO clausierImportExport en json :", e);
            throw new TechnicalException(e.fillInStackTrace());
        }

        try {
            return creationFichierImportExport(referentielImportExportJSON, File.createTempFile("referentielImportExportJSON", ".json"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String format(final Date input) {
        return DATE_TIME_FORMATTER.withZone(ZoneId.systemDefault()).format(input.toInstant());
    }

    private static Date parse(final String input) {
        return Date.from(LocalDateTime.from(DATE_TIME_FORMATTER.parse(input)).atZone(ZoneId.systemDefault()).toInstant());
    }

    private ClausierImportExport constituerClausierImportExport(int idPublication, EpmTRefOrganisme epmTRefOrganisme) {

        ClausierImportExport clausierImportExport = new ClausierImportExport();

        EpmTPublicationClausier epmTPublicationClausier = redactionService.chercherObject(idPublication, EpmTPublicationClausier.class);
        clausierImportExport.setVersion(epmTPublicationClausier.getVersion());
        clausierImportExport.setEditeur(epmTPublicationClausier.getEditeur());
        clausierImportExport.setCommentaire(epmTPublicationClausier.getCommentaire());

        final Date datePublication = epmTPublicationClausier.getDatePublication();
        final String datePublicationFormatted = format(datePublication);

        log.debug("Date de publication du clausier (id publication = {}). Sans format = '{}', avec = '{}'", idPublication, datePublication, datePublicationFormatted);
        clausierImportExport.setDatePublication(datePublicationFormatted);

        //récupération des clauses à exporter.
        ClausePubCritere clausePubCritere = new ClausePubCritere(epmTRefOrganisme.getPlateformeUuid());
        clausePubCritere.setIdPublication(idPublication);
        List<EpmTClausePub> epmTClausesPub = redactionService.chercherEpmTObject(0, clausePubCritere);
        List<ClauseImportExport> clauseImportExportList = epmTClausesPub.stream()
                .map(clausePubMapper::toClauseImportExport)
                .collect(Collectors.toList());
        clausierImportExport.setClauses(clauseImportExportList);
        log.info("Clauses inpacked : {}", clauseImportExportList.size());

        //récupération des canevas à exporter.
        CanevasPubCritere canevasPubCritere = new CanevasPubCritere(epmTRefOrganisme.getPlateformeUuid());
        canevasPubCritere.setIdPublication(idPublication);
        List<EpmTCanevasPub> epmTCanevasPub = redactionService.chercherEpmTObject(0, canevasPubCritere); //pb les sous chapitres apparaissent aussi comme chapitres
        List<EpmTCanevasPub> epmTCanevasPubFinale = new ArrayList<>();
        for (EpmTCanevasPub tCanevasPub : epmTCanevasPub) {
            if (tCanevasPub.getEpmTChapitres() != null) {
                List<EpmTChapitreAbstract> chapitres = new ArrayList<>();
                for (EpmTChapitreAbstract chapitre : tCanevasPub.getEpmTChapitres()) {
                    //on ajoute les chapitres qui ne sont pas des sous chapitres
                    if (chapitre.getEpmTParentChapitre() == null) {
                        chapitres.add(chapitre);
                    }
                }

                tCanevasPub.setEpmTChapitres(chapitres);
            }

        }







        List<CanevasImportExport> canevasImportExportList = epmTCanevasPub.stream()
                .map(canevasPubMapper::toCanevasImportExport)
                .collect(Collectors.toList());
        clausierImportExport.setCanevas(canevasImportExportList);
        log.info("Canevas inpacked : {}", canevasImportExportList.size());

        return clausierImportExport;
    }

    private ReferentielAllImportExport constituerReferentielImportExport(Integer idPublication) {
        ReferentielAllImportExport referentielAllImportExport = new ReferentielAllImportExport();

        EpmTPublicationClausier epmTPublicationClausier = redactionService.chercherObject(idPublication, EpmTPublicationClausier.class);
        referentielAllImportExport.setVersion(epmTPublicationClausier.getVersion());
        referentielAllImportExport.setEditeur(epmTPublicationClausier.getEditeur());
        referentielAllImportExport.setCommentaire(epmTPublicationClausier.getCommentaire());

        final Date datePublication = epmTPublicationClausier.getDatePublication();
        final String datePublicationFormatted = format(datePublication);

        log.debug("Date de publication pour le referentiel (id publication = {}). Sans format = '{}', avec = '{}'", idPublication, datePublication, datePublicationFormatted);
        referentielAllImportExport.setDatePublication(datePublicationFormatted);

        Referentiels referentiels = referentielsService.getAllReferentiels();

        referentielAllImportExport.setContrats(referentiels.getRefTypeContrats().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefTypeContrat inpacked : {}", referentielAllImportExport.getContrats().size());

        referentielAllImportExport.setProcedures(referentiels.getRefProcedureLecture().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefProcedure inpacked : {}", referentielAllImportExport.getProcedures().size());

        referentielAllImportExport.setTypesDocument(referentiels.getRefTypeDocuments().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefTypeDocument inpacked : {}", referentielAllImportExport.getTypesDocument().size());

        referentielAllImportExport.setRefsPotentiellementConditionnee(referentiels.getRefPotentiellementConditionnees().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefPotentiellementConditionnee inpacked : {}", referentielAllImportExport.getRefsPotentiellementConditionnee().size());

        referentielAllImportExport.setThemesClause(referentiels.getRefThemeClauses().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefThemeClause inpacked : {}", referentielAllImportExport.getThemesClause().size());

        referentielAllImportExport.setStatutsRedactionClausier(referentiels.getRefStatutRedactionClausiers().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefStatutRedactionClausier inpacked : {}", referentielAllImportExport.getStatutsRedactionClausier().size());

        referentielAllImportExport.setTypesClause(referentiels.getRefTypeClauses().stream()
                .map(referentielMapper::toReferentielImportExport)
                .collect(Collectors.toList()));
        log.info("EpmTRefTypeClause inpacked : {}", referentielAllImportExport.getTypesClause().size());

        return referentielAllImportExport;
    }

    private File creationFichierImportExport(String stringImportExportJSON, File fileImportExport) {
        log.info("Methode 'creationFichierImportExport' (stringImportExportJSON -> fileImportExport)");

        try (final FileWriter fileWriter = new FileWriter(fileImportExport)) {
            fileWriter.write(stringImportExportJSON);
            fileWriter.flush();
        } catch (IOException e) {
            log.error("Convertir le DTO clausierImportExport en json", e);
            throw new TechnicalException(e.fillInStackTrace());
        }
        return fileImportExport;
    }

    @Override
    public ClausierImportExport isValidReferentielFile(int idPublication, final List<String> errors) {
        EpmTPublicationClausier epmTPublicationClausier = redactionService.chercherObject(idPublication, EpmTPublicationClausier.class);

        if (epmTPublicationClausier.getIdFileReferentiel() == null || epmTPublicationClausier.getIdFileClausier() == null) {
            log.error("Pas de fichier d'export associer au publication clausier dont l'id est : {}", idPublication);
            errors.add("Pas de fichier d'export associer au publication clausier ayant l'id  : [" + idPublication + "] ");
            return null;
        }

        EpmTPublicationClausierFile epmTClausierFile = redactionService.chercherObject(epmTPublicationClausier.getIdFileClausier(), EpmTPublicationClausierFile.class);
        EpmTPublicationClausierFile epmTReferentielFile = redactionService.chercherObject(epmTPublicationClausier.getIdFileReferentiel(), EpmTPublicationClausierFile.class);
        try {
            //Construction des objets d'import et export depuis le Jsonfinal StringBuilder errors
            ClausierImportExport clausierImportExport = objectMapper.readValue(epmTClausierFile.getFileClausier(), ClausierImportExport.class);
            ReferentielAllImportExport referentiels = objectMapper.readValue(epmTReferentielFile.getFileClausier(), ReferentielAllImportExport.class);
            //controle et verification
            if (verifieRefDansImportExportList(clausierImportExport, referentiels))
                return clausierImportExport;
            return null;
        } catch (IOException e) {
            log.error("Une erreur est survenu lors de la lecture du flux json", e);
            errors.add("Une erreur est survenu lors de la lecture du flux json. Veuillez contacter ATEXO");
            return null;
        } catch (Exception e) {
            log.error("Une erreur est survenu lors de la validation de referentiel", e);
            errors.add("Une erreur est survenu technique lors de la validation de referentiel. Veuillez contacter ATEXO");
            return null;
        }
    }

    private boolean verifieRefDansImportExportList(ClausierImportExport clausierImportExport, ReferentielAllImportExport referentielImportExport) {

        if (clausierImportExport == null || referentielImportExport == null) {
            var message = "Une des fichiers d'export est corrompu et ne permet pas de restituer les objets d'export/import. Veuillez Verifier le contenu";
            log.error(message);
            return false;
        }

        boolean referentielConforme = true;

        var canevasImportExport = clausierImportExport.getCanevas();
        var clausesImportExport = clausierImportExport.getClauses();

        var typesDocumentImportExport = Stream.concat(canevasImportExport.stream().map(CanevasImportExport::getDocument), clausesImportExport.stream().map(ClauseImportExport::getDocument)).collect(Collectors.toSet());
        var statutsRedactionImportExport = Stream.concat(canevasImportExport.stream().map(CanevasImportExport::getStatutRedactionClausier), clausesImportExport.stream().map(ClauseImportExport::getStatutRedactionClausier)).collect(Collectors.toSet());
        var proceduresImportExport = Stream.concat(canevasImportExport.stream().map(CanevasImportExport::getProcedures).flatMap(Collection::stream), clausesImportExport.stream().map(ClauseImportExport::getProcedure)).filter(Objects::nonNull).collect(Collectors.toSet());
        var typesContratsImportExport = Stream.concat(canevasImportExport.stream().map(CanevasImportExport::getContrats), clausesImportExport.stream().map(ClauseImportExport::getContrats)).flatMap(Collection::stream).collect(Collectors.toSet());
        var typesClauseImportExport = clausesImportExport.stream().map(ClauseImportExport::getType).collect(Collectors.toSet());
        var themesClauseImportExport = clausesImportExport.stream().map(ClauseImportExport::getTheme).collect(Collectors.toSet());
        var refPotentiellementConditionneesImportExport = clausesImportExport.stream().map(ClauseImportExport::getClausesPotentiellementConditionnee).flatMap(Collection::stream).map(ClausePotentiellementConditionneeImportExport::getRefPotentiellementConditionnee).collect(Collectors.toSet());

        final var referentiels = referentielsService.getAllReferentiels();

        // Activation
        var modifications = activer(typesDocumentImportExport, referentiels.getRefTypeDocuments()) ||
                activer(statutsRedactionImportExport, referentiels.getRefStatutRedactionClausiers()) ||
                activer(proceduresImportExport, referentiels.getRefProcedureLecture()) ||
                activer(typesContratsImportExport, referentiels.getRefTypeContrats()) ||
                activer(typesClauseImportExport, referentiels.getRefTypeClauses()) ||
                activer(themesClauseImportExport, referentiels.getRefThemeClauses()) ||
                activer(refPotentiellementConditionneesImportExport, referentiels.getRefPotentiellementConditionnees());

        if (modifications) {
            this.referentielsService.rechargerAllReferentiels();
        }

        //Ref procedure editeur
        if (!valider(proceduresImportExport, referentielImportExport.getProcedures(), EpmTRefProcedure.class, referentiels.getRefProcedureLecture())) {
            referentielConforme = false;
        }
        //Ref statut Redac
        if (!valider(statutsRedactionImportExport, referentielImportExport.getStatutsRedactionClausier(),
                EpmTRefStatutRedactionClausier.class, referentiels.getRefStatutRedactionClausiers())) {
            referentielConforme = false;
        }
        //Ref Type Contrat
        if (!valider(typesContratsImportExport, referentielImportExport.getContrats(),
                EpmTRefTypeContrat.class, referentiels.getRefTypeContrats()))
            referentielConforme = false;
        //Ref type doc
        if (!valider(typesDocumentImportExport, referentielImportExport.getTypesDocument(),
                EpmTRefTypeDocument.class, referentiels.getRefTypeDocuments()))
            referentielConforme = false;
        //Ref type clause
        if (!valider(typesClauseImportExport, referentielImportExport.getTypesClause(),
                EpmTRefTypeClause.class, referentiels.getRefTypeClauses()))
            referentielConforme = false;
        //Ref theme clause
        if (!valider(themesClauseImportExport, referentielImportExport.getThemesClause(),
                EpmTRefThemeClause.class, referentiels.getRefThemeClauses()))
            referentielConforme = false;

        //Ref potentiellement conditionné
        if (!valider(refPotentiellementConditionneesImportExport, referentielImportExport.getRefsPotentiellementConditionnee(),
                EpmTRefPotentiellementConditionnee.class, referentiels.getRefPotentiellementConditionnees()))
            referentielConforme = false;


        return referentielConforme;
    }

    private <T extends EpmTRef> boolean activer(final Set<String> uids, final Collection<T> procedures) {
        final var modifications = uids.stream().map(uid -> procedures.stream().filter(p -> uid.equalsIgnoreCase(p.getCodeExterne())).findFirst())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(EpmTRef::isInactif)
                .collect(Collectors.toSet());

        var result = !modifications.isEmpty();
        if (result) {
            modifications.forEach(p -> {
                        p.setActif(true);
                        redactionService.modifierEpmTObject(p);
                    }
            );
        }

        return result;
    }

    private boolean referentielAbsent(ReferentielImportExport procedure) {
        return referentielsService.getAllReferentiels().getRefProcedureEcriture().stream().noneMatch(p -> procedure.getCodeExterne().equalsIgnoreCase(p.getCodeExterne()));
    }

    private <T extends EpmTRefImportExport> boolean valider(
            final Set<String> referencesUtilisees,
            final List<ReferentielImportExport> referentielFichier,
            final Class<T> type,
            final Collection<T> referentielBase
    ) {
        if (referencesUtilisees == null || referencesUtilisees.isEmpty()) { //pas d'id pour ce ref utiliser dans cet export
            log.warn("Le référentiel {} n'est pas utilisé dans le fichier CLA", type.getSimpleName());

            return true;
        }

        if (referentielFichier == null || referentielFichier.isEmpty()) {
            var message = MessageFormat.format("Pour le type <strong>{0}</strong>, aucun élément n'a été exporté dans le fichier REF", type.getSimpleName());

            log.error(message);
            return false;
        }

        if (referentielBase == null || referentielBase.isEmpty()) {
            var message = MessageFormat.format("Le type <strong>{0}</strong> n'existe pas dans le référentiel d''importation (la présente plateforme). " +
                    "<br/>Merci de corriger.", type.getSimpleName());

            log.error(message);

            return false;
        }
        var referentielFichierParUid = new HashMap<String, ReferentielImportExport>();

        referentielFichier.forEach(
                ref -> {
                    if (referentielFichierParUid.containsKey(ref.getCodeExterne())) {
                        var message = MessageFormat.format("L''identifiant ''{0}'' (''{1}'') du type <strong>{2}</strong> existe plusieurs fois dans le fichier. " +
                                "<br/>Veuillez contacter ATEXO afin de corriger le fichier.", ref.getCodeExterne(), ref.getLibelle(), type.getSimpleName());

                        log.error(message);
                    } else {
                        referentielFichierParUid.put(ref.getCodeExterne(), ref);
                    }
                }
        );

        var referentielBaseParUid = new HashMap<String, T>();

        referentielBase
                .stream()
                .filter(EpmTRefImportExport::estExportable)
                .forEach(
                        ref -> {
                            if (referentielBaseParUid.containsKey(ref.getUid())) {
                                var message = MessageFormat.format("L''identifiant ''{0}'' (''{1}'') du type <strong>{2}</strong> existe plusieurs fois dans le référentiel d''importation (la présente plateforme). " +
                                        "<br/>Veuillez contacter ATEXO afin de verifier le code externe et de mettre a jour la présente plateforme.", ref.getUid(), ref.getLibelle(), type.getSimpleName());

                                log.error(message);
                            } else {
                                referentielBaseParUid.put(ref.getUid(), ref);
                            }
                        }
                );


        var refsInutilisees = Sets.difference(referentielFichier.stream().map(ReferentielImportExport::getCodeExterne).collect(Collectors.toSet()), referencesUtilisees);

        if (refsInutilisees.isEmpty()) {
            log.warn("Aucune réference inutilisée pour le type '{}'", type.getSimpleName());
        } else {
            log.warn("Liste des réferences inutilisées pour le type '{}' = {}", type.getSimpleName(), String.join(",", refsInutilisees));
        }

        for (var reference : referencesUtilisees) {
            var refBase = referentielBaseParUid.get(reference);
            var refFichier = referentielFichierParUid.get(reference);

            if (null == refFichier) {
                continue;
            }

            if (null == refBase) {
                var message = MessageFormat.format("L''identifiant ''{0}'' (''{1}'') du type <strong>{2}</strong> ne correspond à aucun élément dans le référentiel d''importation (la présente plateforme). " +
                        "<br/>Veuillez contacter ATEXO afin de mettre a jour la présente plateforme.", reference, refFichier.getLibelle(), type.getSimpleName());

                log.error(message);
            }

            if (null != refBase && refBase.isActif() != refFichier.isActif()) {
                var message = MessageFormat.format("L''identifiant ''{0}'' (''{1}'') du type <strong>{2}</strong> a bien une correspondance dans le référentiel d''importation (la présente plateforme). " +
                        (refBase.isActif() ? "Toutefois, la correspondance est active dans le référentiel d''importation (la présente plateforme) et inactive dans le fichier d''export. " :
                                "<br/>Toutefois, la correspondance est inactive dans le référentiel d''importation et active dans le fichier d''export.") +
                        "<br/>Merci de corriger l''un ou l''autre afin de les faire correspondre.", reference, refFichier.getLibelle(), type.getSimpleName());

                log.error(message);
            }
        }
        //tous les export sont present retour ok
        return true;
    }

    public void constituerClausierPub(ClausierImportExport clausierImportExport, Integer idPublication, EpmTUtilisateur utilisateur) {
        // ----------------------------------
        // Clauses
        // ----------------------------------
        this.constituerClauses(clausierImportExport.getClauses(), referentielsService.getAllReferentiels(), idPublication, utilisateur);

        // ----------------------------------
        // Canevas
        // ----------------------------------
        this.constituerCanevas(clausierImportExport.getCanevas(), referentielsService.getAllReferentiels(), idPublication, utilisateur);
    }

    public void constituerClauses(Collection<ClauseImportExport> clauses, Referentiels referentiels, Integer idPublication, EpmTUtilisateur utilisateur) {
        var globalTimer = Stopwatch.createStarted();

        log.info("{} clauses à importer", clauses.size());

        var count = new AtomicInteger(1);

        clauses.forEach(
                clause -> {
                    try {

                        var loopTimer = Stopwatch.createStarted();
                        var stepTimer = Stopwatch.createStarted();

                        log.debug("\uD83D\uDCA5 clause {} ", clause.getIdClause());

                        var epmTClausePub = clausePubMapper.toEpmTClausePub(clause, idPublication);
                        log.debug("Clause {} = conversion en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Type de document
                        referentiels.findTypeDocument(clause.getDocument()).ifPresent(epmTClausePub::setEpmTRefTypeDocument);
                        log.debug("Clause {} = conversion du type de document en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Type de clause
                        referentiels.findTypeClause(clause.getType()).ifPresent(epmTClausePub::setEpmTRefTypeClause);
                        log.debug("Clause {} = conversion du type de clause en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Theme de clause
                        referentiels.findThemeClause(clause.getTheme()).ifPresent(epmTClausePub::setEpmTRefThemeClause);
                        log.debug("Clause {} = conversion du theme de clause en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Procedure
                        referentiels.findProcedure(clause.getProcedure()).ifPresent(epmTClausePub::setEpmTRefProcedure);
                        log.debug("Clause {} = conversion de la procedure en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Contrat
                        epmTClausePub.setEpmTRefTypeContrats(clause.getContrats().stream().map(referentiels::findTypeContrat)
                                .filter(Optional::isPresent)
                                .map(Optional::get).collect(Collectors.toSet()));
                        log.debug("Clause {} = conversion du contrat en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Ref Potentiellement Conditionnee
                        epmTClausePub.setEpmTClausePotentiellementConditionnees(clause.getClausesPotentiellementConditionnee().stream().map(
                                clausePotentiellementConditionneeImportExport -> {
                                    var clausePotentiellementConditionneePub = clausePotentiellementConditionneePubMapper.toClausePotentiellementConditionneePub(clausePotentiellementConditionneeImportExport, idPublication);

                                    // id clause
                                    clausePotentiellementConditionneePub.setIdClause(epmTClausePub.getIdClause());

                                    // Ref potentiellement conditionnee
                                    referentiels.findRefPotentiellementConditionnee(clausePotentiellementConditionneeImportExport.getRefPotentiellementConditionnee())
                                            .ifPresent(clausePotentiellementConditionneePub::setEpmTRefPotentiellementConditionnee);

                                    // Clause valeur potentiellement conditionnee
                                    clausePotentiellementConditionneePub.getEpmTClauseValeurPotentiellementConditionnees()
                                            .forEach(valeur -> valeur.setIdClausePotentiellementConditionnee(clausePotentiellementConditionneePub.getIdClausePotentiellementConditionnee()));

                                    return clausePotentiellementConditionneePub;
                                }

                        ).collect(Collectors.toSet()));
                        log.debug("Clause {} = gestion des clauses potentiellement conditionnees en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        // Role clause
                        epmTClausePub.getEpmTRoleClauses()
                                .forEach(epmTRoleClausePub -> epmTRoleClausePub.setIdClause(epmTClausePub.getIdClause()));
                        log.debug("Clause {} = gestion du role en {}", clause.getIdClause(), pretty(stepTimer));
                        stepTimer.reset();

                        redactionService.creerEpmTObject(epmTClausePub);


                        log.info("\uD83D\uDCBE Clause {} sauvegardée en {}. Temps écoulé total = {} pour {} clauses traitées sur {} ({}%)", epmTClausePub.getIdClause(), pretty(loopTimer.stop()), pretty(globalTimer), count, clauses.size(), MessageFormat.format("{0,number,#.##}", (count.get() * 100) / (double) clauses.size()));

                        count.incrementAndGet();

                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error("Erreur lors de la sauvegarde de la clause : {}", clause.getIdClause(), e);
                    }
                }
        );

        log.info("Gestion des clause terminée en {}", pretty(globalTimer.stop()));
    }


    public void constituerCanevas(Collection<CanevasImportExport> canevases, Referentiels referentiels, Integer idPublication, EpmTUtilisateur utilisateur) {
        var globalTimer = Stopwatch.createStarted();

        log.info("{} canevas à importer", canevases.size());

        var count = new AtomicInteger(1);

        canevases.forEach(
                canevas -> {
                    var loopTimer = Stopwatch.createStarted();
                    var stepTimer = Stopwatch.createStarted();

                    log.debug("\uD83D\uDCA5 canevas {} ", canevas.getIdCanevas());

                    var epmTCanevasPub = canevasPubMapper.toEpmTCanevasPub(canevas, idPublication);
                    if (epmTCanevasPub.getEpmTRefTypeDocument() == null) {
                        //récupérer le type "tout document" si le type document n'est pas renseigné
                        EpmTRefTypeDocument typeDoc = referentiels.getRefTypeDocuments().stream().filter(t -> t.getCodeExterne().equalsIgnoreCase("tous")).findFirst().orElse(null);
                        if (typeDoc != null) {
                            epmTCanevasPub.setEpmTRefTypeDocument(typeDoc);
                        } else {
                            throw new TechnicalException("Probleme avec le type document du canevas " + canevas.getIdCanevas());
                        }
                    }
                    log.debug("Canevas {} = conversion en {}", canevas.getIdCanevas(), pretty(stepTimer));
                    stepTimer.reset();

                    // Type de document
                    referentiels.findTypeDocument(canevas.getDocument()).ifPresent(epmTCanevasPub::setEpmTRefTypeDocument);
                    log.debug("Clause {} = conversion du type de document en {}", canevas.getIdCanevas(), pretty(stepTimer));
                    stepTimer.reset();

                    // Status redaction clausier
                    referentiels.findStatutRedactionClausier(canevas.getStatutRedactionClausier()).ifPresent(epmTCanevasPub::setEpmTRefStatutRedactionClausier);
                    log.debug("Clause {} = conversion du status du document en {}", canevas.getIdCanevas(), pretty(stepTimer));
                    stepTimer.reset();

                    // Contrat
                    epmTCanevasPub.setEpmTRefTypeContrats(canevas.getContrats().stream().map(referentiels::findTypeContrat)
                            .filter(Optional::isPresent)
                            .map(Optional::get).collect(Collectors.toSet()));
                    log.debug("Clause {} = conversion du contrat en {}", canevas.getIdCanevas(), pretty(stepTimer));
                    stepTimer.reset();

                    // Chapitres
                    constituerChapitre(epmTCanevasPub.getEpmTChapitres(), epmTCanevasPub.getIdCanevas(), null);
                    log.debug("Clause {} = constituation des chapitres en {}", canevas.getIdCanevas(), pretty(stepTimer));
                    stepTimer.reset();

                    epmTCanevasPub.setIdOrganisme(utilisateur.getIdOrganisme());

                    redactionService.creerEpmTObject(epmTCanevasPub);

                    log.info("\uD83D\uDCBE Canevas {} sauvegardée en {}. Temps écoulé total = {} pour {} clauses traitées sur {} ({}%)", epmTCanevasPub.getIdCanevas(), pretty(loopTimer.stop()), pretty(globalTimer), count, canevases.size(), MessageFormat.format("{0,number,#.##}", (count.get() * 100) / (double) canevases.size()));

                    count.incrementAndGet();
                }
        );

        log.info("Gestion des canevas terminée en {}", pretty(globalTimer.stop()));
    }

    private static String pretty(Stopwatch watch) {
        var hours = watch.elapsed(TimeUnit.HOURS);
        var minutes = watch.elapsed(TimeUnit.MINUTES) - 60 * watch.elapsed(TimeUnit.HOURS);
        var seconds = watch.elapsed(TimeUnit.SECONDS) - 60 * minutes - 60 * 60 * hours;
        var milliseconds = watch.elapsed(TimeUnit.MILLISECONDS) - 1000 * seconds - 1000 * 60 * minutes - 1000 * 60 * 60 * hours;

        return (hours > 0 ? hours + "h " : "") +
                (minutes > 0 ? minutes + "m" : "") +
                (seconds > 0 ? seconds + "s" : "") +
                (milliseconds > 0 ? milliseconds : "");
    }

    private void constituerChapitre(List<EpmTChapitrePub> epmTChapitresPub, Integer idCanevas, Integer idParentChapitre) {

        for (EpmTChapitrePub epmTChapitrePub : epmTChapitresPub) {
            epmTChapitrePub.setIdCanevas(idCanevas);
            epmTChapitrePub.setIdParentChapitre(idParentChapitre);

            constituerChapitre(epmTChapitrePub.getEpmTSousChapitres(), null, epmTChapitrePub.getIdChapitre());
        }
    }

    public List<String> importerClausier(List<File> publicationFichiers, EpmTUtilisateur utilisateur) {
        //si version non existante ===> on insere dans les table pub
        //insersion de n version de clausiers
        List<String> errors = new ArrayList<>();
        Map<String, ArrayList<File>> clausiersMapedFiles = new HashMap<>();
        for (File f : publicationFichiers) {
            String clausierKey = f.getName().substring(3);
            if (!clausiersMapedFiles.containsKey(clausierKey))
                clausiersMapedFiles.put(clausierKey, new ArrayList<>());
            clausiersMapedFiles.get(clausierKey).add(f);
        }

        for (Map.Entry<String, ArrayList<File>> element : clausiersMapedFiles.entrySet()) {
            File fileRef = null;
            File fileCla = null;
            for (File f : element.getValue()) {
                if (f.getName().startsWith("CLA"))
                    fileCla = f;
                else if (f.getName().startsWith("REF"))
                    fileRef = f;
            }
            EpmTPublicationClausierFile fichierClausier = new EpmTPublicationClausierFile();
            EpmTPublicationClausierFile fichierReferentiel = new EpmTPublicationClausierFile();
            fichierClausier.setFileName(fileCla.getName());
            fichierReferentiel.setFileName(fileRef.getName());

            String version = "";
            try {
                ClausierImportExport clausierImportExport = objectMapper.readValue(fileCla, ClausierImportExport.class);
                version = clausierImportExport.getVersion();
                if (!publicationClausierService.isVersionNameUnique(version, utilisateur.getEpmTRefOrganisme().getPlateformeUuid())) {
                    errors.add(MessageFormat.format("Le numéro de version de clausier {0} a déjà fait l'objet d'une publication.", version));
                    continue;
                }

                try {
                    fichierClausier.setFileClausier(FileUtils.readFileToByteArray(fileCla));
                    fichierReferentiel.setFileClausier(FileUtils.readFileToByteArray(fileRef));
                } catch (IOException e) {
                    log.error("une erreur est survenue lors de l'enregistrement de fichier de publication du clausier en db", e);
                    errors.add(MessageFormat.format("Une erreur est survenue lors de l''enregistrement en de des fichiers de la publication du clausier numéro {0}.", clausierImportExport.getVersion()));
                    continue;
                }
                fichierClausier = redactionService.modifierEpmTObject(fichierClausier);
                fichierReferentiel = redactionService.modifierEpmTObject(fichierReferentiel);

                EpmTPublicationClausier publicationClausier = new EpmTPublicationClausier();
                publicationClausier.setVersion(clausierImportExport.getVersion());
                publicationClausier.setGestionLocal(false);
                publicationClausier.setIdUtilisateur(utilisateur.getId());
                publicationClausier.setIdOrganisme(utilisateur.getIdOrganisme());
                publicationClausier.setNomCompletUtilisateur(utilisateur.getNomComplet());
                publicationClausier.setGestionLocal(false);
                publicationClausier.setEditeur(clausierImportExport.getEditeur());

                String datePublication = clausierImportExport.getDatePublication();
                Date datePublicationFormatted = parse(datePublication);

                log.debug("Definition de la date de publication. Sans format = {}, avec = {}", datePublication, datePublicationFormatted);

                publicationClausier.setDatePublication(datePublicationFormatted);
                publicationClausier.setDateIntegration(new Date());
                publicationClausier.setCommentaire(clausierImportExport.getCommentaire());
                publicationClausier.setIdFileClausier(fichierClausier.getId());
                publicationClausier.setIdFileReferentiel(fichierReferentiel.getId());
                redactionService.creerEpmTObject(publicationClausier);
            } catch (IOException e) {
                log.error("Une erreur est survenu lors de la deserialisation du fichier clausier", e);
                errors.add("Une erreur est survenue lors de la déserialisation de fichier pour le clausier numéro [" + version + "].");
            }
        }
        return errors;
    }

    public void setCanevasPubMapper(CanevasPubMapper canevasPubMapper) {
        this.canevasPubMapper = canevasPubMapper;
    }

    public void setClausePubMapper(ClausePubMapper clausePubMapper) {
        this.clausePubMapper = clausePubMapper;
    }

    public void setReferentielMapper(ReferentielMapper referentielMapper) {
        this.referentielMapper = referentielMapper;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void setRedactionService(RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

    public void setClausePotentiellementConditionneePubMapper(ClausePotentiellementConditionneePubMapper clausePotentiellementConditionneePubMapper) {
        this.clausePotentiellementConditionneePubMapper = clausePotentiellementConditionneePubMapper;
    }

    public void setReferentielsService(ReferentielsServiceSecurise referentielsService) {
        this.referentielsService = referentielsService;
    }

    public void setPublicationClausierService(PublicationClausierService publicationClausierService) {
        this.publicationClausierService = publicationClausierService;
    }

}
