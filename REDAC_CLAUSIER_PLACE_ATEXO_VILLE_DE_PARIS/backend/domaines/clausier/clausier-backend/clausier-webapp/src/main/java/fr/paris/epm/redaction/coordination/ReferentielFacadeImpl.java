package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.global.coordination.objetValeur.SimplePair;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefPouvoirAdjudicateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.bean.referentiels.ReferentielsCanevasBean;
import fr.paris.epm.redaction.coordination.bean.referentiels.ReferentielsClausesBean;
import fr.paris.epm.redaction.coordination.bean.referentiels.ReferentielsDocumentsBean;
import fr.paris.epm.redaction.coordination.service.PotentiellementConditionneeService;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe implémentant les méthodes exposées par l'interface ReferentielFacade.
 * @author Ramli Tarik
 */
public class ReferentielFacadeImpl implements ReferentielFacade {

	/**
	 * Marqueur du fichier journal.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ReferentielFacadeImpl.class);

	/**
	 * Manipulateur du noyau.
	 */
	private NoyauProxy noyauProxy;

	/**
	 * Manipulateur DAO
	 */
	private RedactionServiceSecurise redactionService;

	private ReferentielsServiceSecurise referentielsService;

	private PotentiellementConditionneeService potentiellementConditionneeService;

	public final EpmTRefTypeDocument getTypeDocumentParId(final int id) throws TechnicalException {
		try {
			return redactionService.chercherObject(id, EpmTRefTypeDocument.class);
		} catch (NonTrouveException e) {
			LOG.error(e.getMessage(), e.fillInStackTrace());
			throw new TechnicalException(e.fillInStackTrace());
		}
	}
	public final List chargerTousPotentiellementConditionne(Integer idOrganisme) throws TechnicalException {
		return potentiellementConditionneeService.lister(idOrganisme);
	}


	public final List<EpmTRefTypeDocument> chargerTousTypesDocument(final boolean avecTous, Boolean actif) throws TechnicalException {
		List<EpmTRefTypeDocument> liste = referentielsService.getAllReferentiels().getRefTypeDocuments();

		if (!avecTous)
			liste.removeIf(typeDocument -> typeDocument.getId() == EpmTRefTypeDocument.ID_TOUS);

		if (actif != null)
			liste.removeIf(typeDocument -> typeDocument.isActif() != actif);

		Collections.sort(liste);
		return liste;
	}

	public final EpmTRefValeurTypeClause getValeurTypeClauseById(final int id) throws TechnicalException {
		try {
			return redactionService.chercherObject(id, EpmTRefValeurTypeClause.class);
		} catch (NonTrouveException e) {
			LOG.error(e.getMessage(), e.fillInStackTrace());
			throw new TechnicalException(e.fillInStackTrace());
		}
	}

	@Override
	public Integer getIdProcedurePassationCorrespondantDansAutreOrganisme(Integer idProcedurePassation, Integer idOrganisme) throws TechnicalException {
		if(idOrganisme == null){
			return idProcedurePassation;
		}

		Collection<EpmTRefProcedure> listeProcedures = noyauProxy.getReferentiels().getRefProcedureLecture();

		String codeEditeur = "";
		for (EpmTRefProcedure proc : listeProcedures) {
			if (proc.isActif() && idProcedurePassation == proc.getId()) {
				codeEditeur = proc.getCodeExterne();
				break;
			}
		}

		Integer idProcedurePassationDansAutreOrganisme = getCorrespondanceIdProcedurePassation(idOrganisme, codeEditeur);

		if (idProcedurePassationDansAutreOrganisme == null)
			return idProcedurePassation;
		else
			return idProcedurePassationDansAutreOrganisme;
	}

	@Override
	public final EpmTRefPotentiellementConditionnee getPotentiellementConditionneeParId(final int id) throws TechnicalException {
		return referentielsService.chercherObject(id, EpmTRefPotentiellementConditionnee.class);
	}


	private Integer getCorrespondanceIdProcedurePassation(final Integer idOrganisme, final String codeEditeur) throws TechnicalException {
		Collection<EpmTRefProcedure> listeProcedures = noyauProxy.getProcedureByIdOrganisme(idOrganisme, false, false);
		for (EpmTRefProcedure procedure : listeProcedures)
			if (codeEditeur.equals(procedure.getCodeExterne()))
				return procedure.getId();
		return null;
	}

	public final void setNoyauProxy(final NoyauProxy valeur) {
		this.noyauProxy = valeur;
	}

	public final void setRedactionService(final RedactionServiceSecurise redactionService) {
		this.redactionService = redactionService;
	}

	public final void setReferentielsService(final ReferentielsServiceSecurise referentielsService) {
		this.referentielsService = referentielsService;
	}

	public void setPotentiellementConditionneeService(PotentiellementConditionneeService potentiellementConditionneeService) {
		this.potentiellementConditionneeService = potentiellementConditionneeService;
	}

}
