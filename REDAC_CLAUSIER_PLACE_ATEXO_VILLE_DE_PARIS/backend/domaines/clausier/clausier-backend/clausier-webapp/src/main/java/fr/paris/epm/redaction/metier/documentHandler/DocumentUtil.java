package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.document.AbstractChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;
import org.jdom.Attribute;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.ArrayList;
import java.util.List;

/**
 * cette classe est l'implementation de la classe DocumentUtil qui effectue des
 * manipulations sur un document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class DocumentUtil implements XmlDocument {

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DocumentUtil.class);
    
    /**
     * Cette méthode permet la conversion d'un chapitre document. vers un
     * element chapitre XML
     * @see fr.paris.epm.redaction.metier.documentHandler.DocumentUtil#versJdom(fr.paris.epm.redaction.metier.objetValeur.document.Document)
     */
    public static Element versJdom(final Document documentInstance) {
        LOG.debug("Construire l'arbre JDOM du document");
        Element arbre = new Element(DOCUMENT_NOM);

        // Les attributs
        LOG.debug("Initialiser les attributs du document");
        Attribute id = new Attribute(DOCUMENT_ID, Integer.toString(documentInstance.getId()));
        arbre.setAttribute(id);
        Attribute ref = new Attribute(DOCUMENT_REF, documentInstance.getReference());
        arbre.setAttribute(ref);

        LOG.debug("Initialiser les Chapitres du document en ignorant les chapitres supprimes");
        List<ChapitreDocument> chapitresDoc = new ArrayList();
        for (ChapitreDocument chapitreDocument : documentInstance.getChapitres())
            if (!chapitreDocument.isSupprime())
                chapitresDoc.add(chapitreDocument);
        List listeChapitresDom = new ArrayList(chapitresDoc.size());
        LOG.debug("Nombre de Chapitres : " + chapitresDoc.size());
        for (ChapitreDocument chapitreEnCours : chapitresDoc) {
            listeChapitresDom.add(ChapitreDocumentUtil.versJdom(chapitreEnCours));
        }
        arbre.addContent(listeChapitresDom);
        return arbre;
    }

    /*
     * cette méthode permet de fabriquer le squelette du document à partir du
     * canevas qui lui a été associé, écrase les anciens chapitres du document.
     * @see fr.paris.epm.redaction.metier.documentHandler.DocumentUtil#construireChapitres(fr.paris.epm.redaction.metier.objetValeur.document.AbstractChapitreDocument,
     *      fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas,
     *      int, fr.paris.epm.noyau.persistance.EpmTUtilisateur)
     */
    public static List<ChapitreDocument> construireChapitres(final AbstractChapitreDocument parent, final Canevas canevas,
                                                             final EpmTConsultation consultation, final EpmTUtilisateur utilidateur,
                                                             final int idLot, final ResourceBundleMessageSource messageSource,
                                                             final RechercheClauseEnum rechercherClause)
            throws NonTrouveException, TechnicalException, TechnicalNoyauException {

        canevas.renumeroterChapitres();
        List<ChapitreDocument> listeDesChapitres = new ArrayList<>(canevas.getChapitres().size());
        for (Chapitre chapitreCanevas : canevas.getChapitres()) {
            ChapitreDocument cd = ChapitreDocumentUtil.chapitreCanevasToChapitreDocument(parent, chapitreCanevas, consultation, utilidateur, idLot, messageSource, rechercherClause);
            listeDesChapitres.add(cd);
        }
        return listeDesChapitres;
    }
    
    /**
     * Construction des chapitres pour l'export de canevas.
     * @param parent document {@link Document} à remplir 
     * @param canevas canevas à exporter
     * @param utilidateur utilisateur courant.
     * @return une liste de chapitre avec leur paragraphe
     */
    public static List<ChapitreDocument> construireChapitresExportCanevas(final AbstractChapitreDocument parent, final Canevas canevas,
                                                                          final EpmTUtilisateur utilidateur, final ResourceBundleMessageSource messageSource)
            throws NonTrouveException, TechnicalException {

        canevas.renumeroterChapitres();
        List<ChapitreDocument> listeDesChapitres = new ArrayList<>(canevas.getChapitres().size());
        for (Chapitre chapitreCanevas : canevas.getChapitres())
            listeDesChapitres.add(ChapitreDocumentUtil.chapitreCanevasToChapitreDocument(parent, chapitreCanevas, utilidateur, messageSource));

        if (canevas.isDerogationActive()) {
            List<Derogation> derogations = new ArrayList<>();
            DerogationUtil.chargerDerogationsDepuisChapitres(canevas.getChapitres(), derogations);
            if(!derogations.isEmpty()) {
                ChapitreDocument chapitreDocument = DerogationUtil.creerChapitreDerogations(derogations);
                chapitreDocument.setNumero(String.valueOf(listeDesChapitres.size() + 1));
                listeDesChapitres.add(chapitreDocument);
            }
        }
        return listeDesChapitres;
    }

}
