package fr.paris.epm.redaction.commun.worker;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.metier.redaction.ClauseViewCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import fr.paris.epm.redaction.metier.objetValeur.ClauseExport;
import fr.paris.epm.redaction.presentation.servlets.ExportClauseXLSBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tache générant l'export des clauses dans un fichier excel
 * Created by nty on 12/03/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Component
@Scope("prototype")
public class ExportClausesTask extends RsemTask<FileTmp> {

    private static final Logger logger = LoggerFactory.getLogger(ExportClausesTask.class);

    @Value("classpath:templates/Modele_extraction_clauses.xls")
    private org.springframework.core.io.Resource template;


    private static final int NOMBRE_RESULTAT_PAR_REQUETE = 50;

    /**
     * repertoire des fichiers générés.
     */
    @Resource(name = "cheminTemporaire")
    private String cheminTemporaire;


    @Resource
    private ClauseFacadeGWT clauseFacadeGWT;

    private boolean editeur;

    private EpmTUtilisateur utilisateur;

    private String nameResultFile;

    private String plateformeUuid;


    @Override
    public FileTmp work() throws Exception {
        logger.info("Début de l'export des clauses " + new Date());
        ClauseCritere critere;
        if (editeur) {
            critere = new ClauseCritere(plateformeUuid);
            critere.setEditeur(true);
        } else {
            critere = new ClauseViewCritere(plateformeUuid);
            critere.setNonAfficherSurcharge(true);
        }
        critere.setIdOrganisme(utilisateur.getIdOrganisme());
        critere.setChercherNombreResultatTotal(false);
        critere.setTaillePage(NOMBRE_RESULTAT_PAR_REQUETE);
        critere.setProprieteTriee("reference");

        try {
            ExportClauseXLSBuilder builder = new ExportClauseXLSBuilder(template.getInputStream());
            builder.build(cheminTemporaire);

            int numeroPage = 0;
            critere.setNumeroPage(numeroPage);
            while (true) {
                List<ClauseExport> resultat = clauseFacadeGWT.chercherParCriteresPourExport(critere, utilisateur.getIdOrganisme());

	            logger.warn("Resultat de l'export : {}", resultat.stream().map(clauseExport -> clauseExport.getIdClause() + " = "+clauseExport.getReferenceCanevas()).collect(Collectors.toList()));

                if (resultat.isEmpty()) {
                    break;
                } else {
                    builder.remplirDocumentAvecListeClause(resultat);
                    numeroPage++;
                    critere.setNumeroPage(numeroPage);
                }
            }
            logger.info("Fin de l'export des clauses " + new Date());
            FileTmp fileTmp = builder.getFichierTmp();
            fileTmp.setNomFichier(nameResultFile);
            return fileTmp;
        } catch (FileNotFoundException | TechnicalException ex) {
            logger.error(ex.getMessage(), ex.fillInStackTrace());
            throw ex;
        }
    }

    public void setEditeur(boolean editeur) {
        this.editeur = editeur;
    }

    public void setUtilisateur(EpmTUtilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public void setNameResultFile(String nameResultFile) {
        this.nameResultFile = nameResultFile;
    }

    public void setPlateformeUuid(String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }
}
