<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
    </title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link type="text/css" href="<atexo:href href='css/styles.css'/>"rel="stylesheet"/>
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link type="text/css" href="<atexo:href href='css/styles-spec.css'/>" rel="stylesheet"/>
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>

    <style type="text/css">
        td {
            border: 1px solid #888;
            height: 40px;
        }

        thead td {
            text-align: center !important;
        }
    </style>
</head>


<body onload="popupResize();">
<div class="previsu-doc" id="container">
    <div class="content">
        <center>
            <h5>
                <bean:message key="historique.document.titre"/> - ${titreDocument}
            </h5>
        </center>
        <ul>
            <li>
                <div class="historique">
                    <table class="tableau" cellspacing="1" cellpadding="0">
                        <thead>
                        <tr class="entete">
                            <td class="col-60 center">Version</td>
                            <td class="col-130"><bean:message key="historique.document.action"/></td>
                            <td class="col-90 center"><bean:message key="historique.document.date"/></td>
                            <td class="col-110"><bean:message key="historique.document.nom"/></td>
                            <td><bean:message key="historique.document.commentaire"/></td>
                            <td class="action col-60 center">Action</td>
                        </tr>
                        </thead>
                        <c:if test="${type == 'pg'}">
                            <logic:iterate indexId="i" name="versions" id="version" type="fr.paris.epm.noyau.persistance.redaction.EpmTPageDeGardeVersion">
                                <tr>
                                    <td class="col-60 center">
                                        ${i+1}
                                    </td>
                                    <td class="col-130">
                                        <c:if test="${version.statut == 'GENERATED'}">
                                            Créé
                                        </c:if>
                                        <c:if test="${version.statut != 'GENERATED'}">
                                            Modifié
                                        </c:if>
                                    </td>
                                    <td class="col-90 center">
                                        <c:if test="${version.dateModification != null}">
                                            <bean:write name="version" property="dateModification" format="dd/MM/yyyy HH:mm"/>
                                        </c:if>
                                    </td>
                                    <td class="col-110">
                                        <c:if test="${version.utilisateur != null}">
                                            <c:out value="${version.utilisateur}" />
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${version.statut == 'GENERATED'}">
                                            Créé
                                        </c:if>
                                        <c:if test="${version.statut == 'REQUEST_TO_OPEN' || version.statut == 'OPENED'}">
                                            Ouvert pour modification
                                        </c:if>

                                        <c:if test="${version.statut == 'SAVED_AND_CLOSED'}">
                                            Sauvegardé après modification
                                        </c:if>

                                        <c:if test="${version.statut == 'CORRUPTED'}">
                                            Fichier illisible
                                        </c:if>

                                        <c:if test="${version.statut == 'CLOSED_WITHOUT_EDITING'}">
                                            Fermé sans modification
                                        </c:if>

                                        <c:if test="${version.statut == 'DOWNLOADED'}">
                                            Téléchargé
                                        </c:if>

                                        <c:if test="${version.statut == 'EDITED_AND_SAVED'}">
                                            Edité et sauvegardé
                                        </c:if>

                                        <c:if test="${version.statut == 'ERROR_WHILE_SAVING'}">
                                            Erreur lors de la sauvegarde
                                        </c:if>

                                        <c:if test="${version.statut == 'NOT_FOUND'}">
                                            Fichier non trouvé
                                        </c:if>

                                        <c:if test="${version.statut == 'DUPLICATION'}">
                                            Fichier dupliqué
                                        </c:if>
                                    </td>
                                    <td class="action col-60 center">
                                        <c:choose>
                                            <c:when test="${version.fichier != null}">
                                                <a href="viewDocument.htm?document=${document.id}&version=${version.id}&type=${document.getClass().getName()}&consultation=${consultation}">
                                                    <img title="Télécharger" alt="Télécharger" src="<atexo:href href="images/bouton-telecharger.gif"/>"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <span>-</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </logic:iterate>
                        </c:if>


                        <c:if test="${type == 'dr'}">

                            <logic:iterate name="historiqueDocuments" id="historique" type="fr.paris.epm.redaction.coordination.bean.HistoriqueDocumentBean">
                                <tr>
                                    <td class="col-60 center">
                                        <c:if test="${historique.version != null}">
                                            <c:out value="${historique.version}" />
                                        </c:if>
                                    </td>
                                    <td class="col-130">
                                        <c:if test="${historique.epmTRefActionDocument.libelle != null}">
                                            <c:out value="${historique.epmTRefActionDocument.libelle}" />
                                        </c:if>
                                    </td>
                                    <td class="col-90 center">
                                        <c:if test="${historique.dateModification != null}">
                                            <bean:write name="historique" property="dateModification" format="dd/MM/yyyy HH:mm"/>
                                        </c:if>
                                    </td>
                                    <td class="col-110">
                                        <c:if test="${historique.auteur != null}">
                                            <c:out value="${historique.auteur}" />
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${historique.commentaire != null}">
                                            <c:out value="${historique.commentaire}" />
                                        </c:if>
                                    </td>
                                    <td class="action col-60 center">
                                        <c:choose>
                                            <c:when test="${historique.idDocument != null}">
                                                <a href="javascript:popUp('ChoixExtensionFichier.epm?id=${historique.idDocument}','yes')">
                                                    <img title="Télécharger" alt="Télécharger" src="<atexo:href href="images/bouton-telecharger.gif"/>"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <span>-</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </logic:iterate>
                        </c:if>
                    </table>
                </div>
            </li>
        </ul>
    </div>
    <a href="javascript:window.close();" class="fermer">
        <bean:message key="historique.document.fermer"/>
    </a>
    <div class="breaker"></div>
</div>
</body>
</html>
