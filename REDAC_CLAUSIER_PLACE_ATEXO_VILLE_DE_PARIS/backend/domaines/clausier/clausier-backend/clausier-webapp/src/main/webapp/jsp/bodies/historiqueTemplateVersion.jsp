
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="modal_template_version">
    <div class="content">
        <div class="results-list">
            <table>
                <thead>
                <tr>
                    <th><bean:message key="gabarit.historique.list.nomFichier"/></th>
                    <th><bean:message key="gabarit.historique.list.download"/></th>
                    <th><bean:message key="gabarit.historique.list.telecharger"/></th>
                </tr>
                </thead>
                <tbody>
                <%--@elvariable id="gabaritForm" type="fr.paris.epm.redaction.presentation.forms.GabaritForm"--%>
                <c:choose>
                    <c:when test="${gabaritForm.listMyVersions.size() > 0}">
                        <c:forEach items="${gabaritForm.listMyVersions}" var="myVersion" varStatus="vs">
                            <tr class="<c:if test="${vs.index%2 eq 1}"><bean:message key="redaction.global.on"/></c:if>">
                                <td>
                                        ${myVersion.nomFichier}
                                </td>
                                <td>
                                        ${myVersion.dateCreation}
                                </td>
                                <td>
                                    <a href="telechargerTemplateVersion.epm?idVersion=${myVersion.id}">
                                        <img src="<atexo:href href='images/picto-generer.gif'/>"
                                             alt="<bean:message key="gabarit.historique.list.telecharger"/>"
                                             title="<bean:message key="gabarit.historique.list.telecharger"/>"/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr colspan="3">
                            <bean:message key="gabarit.historique.list.aucunTemplate" />
                        </tr>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>
        <div class="boutons">
            <a id="annuler" href="javascript:void(0);" onclick="jQuery('.ui-dialog-content').dialog('close');" class="annuler">Fermer</a>
        </div>
    </div>
</div>