<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<%--@elvariable id="resumeConsultation" type="fr.paris.epm.global.presentation.ResumeConsultation"--%>

<!--Debut bloc recap Infos-->
<div class="form-bloc">
    <div class="top"><span class="left"></span><span class="right"></span></div>
    <div class="content">

        <div class="top-link">
            <a onclick="toggleRecapPanel(this,'recap-details');" class="toggle-recap" href="javascript:void(0);"></a>

            <div class="consultation-line">
                <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.numConsultation" /> : </span>
                <span class="content-bloc-long">${resumeConsultation.numero} (${resumeConsultation.acronyme}) :
                    <c:out value="${resumeConsultation.intitule}"/>
                </span>
                <logic:notEmpty name="resumeConsultation" property="statut">
                    <span class="intitule"><bean:message key="resumeConsultation.txt.statut" /> : </span>
                    <span class="content-bloc-long">
                        <strong><c:out value="${resumeConsultation.statut}"/></strong>
                    </span>
                </logic:notEmpty>
            </div>
        </div>

        <div id="recap-details" style="display: block;">
            <div class="line">
                <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.objetConsultation" /> : </span>
                <span class="content-bloc-long">
                    <c:if test="${resumeConsultation.objetCourt != null}">
                        <c:out value="${resumeConsultation.objetCourt}" />
                        <img title="Info-bulle" alt="Info-bulle" class="picto-info-bulle"
                             onmouseout="cacheBulle('infos-objet')" onmouseover="afficheBulle('infos-objet', this)"
                             src="<atexo:href href='images/picto-info-suite.gif'/>"/>
                    </c:if>

                    <c:if test="${resumeConsultation.objetCourt == null}">
                        <c:out value="${resumeConsultation.objet}" />
                    </c:if>
                </span>
            </div>
            <!-- Pouvoir adjudicateur -->
            <div class="line">
                <logic:notPresent name="dspBean">
                    <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.pouvoirAdjudicateur" /> : </span>
                    <div class="content-bloc-long">${resumeConsultation.pouvoirAdjudicateur}</div>
                </logic:notPresent>
                <logic:present name="dspBean">
                    <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.autoriteDelegante" /> : </span>
                    <div class="content-bloc-long">${resumeConsultation.pouvoirAdjudicateur}</div>
                </logic:present>
            </div>
            <!-- Direction Service(s) -->
            <div class="line">
                <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.directionService" /> : </span>
                <div class="content-bloc-long">${resumeConsultation.direction}</div>
            </div>

            <!-- Responsable de la consultation -->
            <div class="line">
                <logic:notPresent name="dspBean">
                    <span class="intitule-bloc"><bean:message key="caracteristiquesConsultation.resumeConsultation.respoConsultation" /> : </span>
                </logic:notPresent>
                <logic:present name="dspBean">
                    <span class="intitule-bloc"><bean:message key="caracteristiquesConsultation.resumeConsultation.responsableDsp" /> : </span>
                </logic:present>
                <div class="content-bloc-long">
                    <c:out value="${resumeConsultation.responsablePrenom}" />
                    <c:out value="${resumeConsultation.responsableNom}" />
                </div>
            </div>
            <!-- Date de lancement -->
            <div class="breaker"></div>
            <div class="line">
                <span class="intitule"><bean:message key="caracteristiquesConsultation.resumeConsultation.dateLancement" /> : </span>
                <span class="content-bloc-long">
                    <c:if test="${resumeConsultation.dateLancement != null}">
                        <bean:write name="resumeConsultation" property="dateLancement" format="dd/MM/yyyy "/>
                    </c:if>
                    <c:if test="${resumeConsultation.dateLancement == null}">
                        <bean:message key="resumeConsultation.txt.nd" />
                    </c:if>
                </span>
            </div>

            <div class="line">
                <c:if test="${resumeConsultation.dateLimiteOffre == null}">
                    <span class="intitule-long"><bean:message key="caracteristiquesConsultation.resumeConsultation.dateLimite" /> : </span>
                    <c:if test="${resumeConsultation.dateLimite != null}">
                        <bean:write name="resumeConsultation" property="dateLimite" format="dd/MM/yyyy HH:mm"/>
                    </c:if>
                    <c:if test="${resumeConsultation.dateLimite == null}">
                        <bean:message key="resumeConsultation.txt.nd" />
                    </c:if>
                </c:if>
                <c:if test="${resumeConsultation.dateLimiteOffre != null}">
                    <span class="intitule-long"><bean:message key="caracteristiquesConsultation.resumeConsultation.dateLimiteCandidature" /> : </span>
                    <c:if test="${resumeConsultation.dateLimite != null}">
                        <bean:write name="resumeConsultation" property="dateLimite" format="dd/MM/yyyy HH:mm"/>
                    </c:if>
                    <c:if test="${resumeConsultation.dateLimite == null}">
                        <bean:message key="resumeConsultation.txt.nd" />
                    </c:if>
                </c:if>
            </div>
            <c:if test="${resumeConsultation.dateLimiteOffre != null}">
                <div class="breaker"></div>
                <div class="line">
                    <span class="intitule">&nbsp;</span>
                </div>

                <div class="line">
                    <span class="intitule-long"><bean:message key="caracteristiquesConsultation.resumeConsultation.dateLimiteOffre" /> : </span>
                    <bean:write name="resumeConsultation" property="dateLimiteOffre" format="dd/MM/yyyy HH:mm"/>
                </div>
            </c:if>

            <logic:notEqual value="0" name="resumeConsultation" property="idConsultationInitiale">
                <div class="breaker"></div>
                <div class="column">
                    <span class="intitule"><bean:message key="resumeConsultation.txt.accordCadre"/></span>
                    <bean:message key="resumeConsultation.txt.numero"/>
                    <a href="javascript:popUp('ficheSynthetique.epm?idConsultation=<c:out value="${resumeConsultation.idConsultationInitiale}"/>','yes');">
                        <c:out value="${resumeConsultation.numeroConsultationInitiale}"/>
                        <logic:notEmpty name="resumeConsultation" property="numeroLotAccordCadre">
                            <bean:message key="resumeConsultation.txt.lot" /><c:out value="${resumeConsultation.numeroLotAccordCadre}"/>
                        </logic:notEmpty>
                    </a>
                </div>
            </logic:notEqual>
            <logic:notEqual value="0" name="resumeConsultation" property="idConsultationLiee">
                <div class="breaker"></div>
                <div class="column">
                    <span class="intitule"><bean:message key="resumeConsultation.txt.consultationLiee"/></span>
                    <bean:message key="resumeConsultation.txt.numero"/>
                    <a href="javascript:popUp('ficheSynthetique.epm?idConsultation=<c:out value="${resumeConsultation.idConsultationLiee}"/>','yes');">
                        <c:out value="${resumeConsultation.numeroConsultationInitiale}"/>
                    </a>
                </div>
            </logic:notEqual>
        </div>
        <div class="breaker"></div>
        <div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infos-objet" style="display: none; left: 967px; top: 208px;">
            <div><c:out value="${resumeConsultation.objet}" /></div>
        </div>
        <div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infos-intitule" style="display: none; left: 967px; top: 208px;">
            <div><c:out value="${resumeConsultation.intitule}" /></div>
        </div>
    </div>

    <div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>
<!--Debut bloc recap Infos-->
