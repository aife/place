package fr.paris.epm.redaction.metier.objetValeur.canevas;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * cette classe assure des manipulations sur les chapitres et les canevas et
 * implemente l'interface CanevasChapitre.
 * @author Guillaume Béraudo.
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractCanevasChapitre implements CanevasChapitre {

    /**
     * Numéro du chapitre ou du canevas.
     */
    private String numero;

    /**
     * Liste des chapitres du canvevas.
     */
    protected List<Chapitre> chapitres = new ArrayList<Chapitre>(0);

    /**
     * Parent (chapitre ou canevas).
     */
    @JsonIgnore
    protected CanevasChapitre parent;

    /**
     * cette méthode permet d'insérer un chapitre à la racine du canevas à une
     * certaine position dans la liste.
     * @param valeurChap nouveau chapitre à insérer
     * @param valeurPosition position absolue
     * @return faux dans le cas ou la position est négative vrai dans le cas ou la position est positive.
     */
    @Override
    public final boolean inserer(final Chapitre valeurChap, final int valeurPosition) {
        if (valeurPosition < 0) {
            return false;
        } else {
            valeurChap.setParent(this);
            chapitres.add(valeurPosition, valeurChap);
            renumeroterChapitres();
            return true;
        }
    }

    /**
     * cette methode permet de parcourir la liste des chapitres et affecte un numéro de la forme 1.1.2.5
     */
    @Override
    public final void renumeroterChapitres() {
        for (int i = 0; i < chapitres.size(); i++) {
            Chapitre chapitre = chapitres.get(i);
            chapitre.setNumero((numero == null) ? Integer.toString(i + 1) : numero + "." + (i + 1));
            chapitre.renumeroterChapitres(); // parcours de l'arbre
        }
    }

    /**
     * cette méthode confirme la suppression d'un chapitre.
     * @param valeur contient le chapitre à supprimer.
     * @return vrai si le chapitre est supprimé, false si erreur.
     */
    @Override
    public final boolean supprimer(final Chapitre valeur) {
        return chapitres.remove(valeur);
    }

    /**
     * cette méthode permet de déplacer un chapitre à partir d'un emplacement donné.
     * @param aDeplacer chapitre à déplacer
     * @param position position absolue dans la liste des chapitres
     * @return faux si la position donné est négative vrai si la position donné est positve.
     */
    @Override
    public final boolean deplacerChapitre(final Chapitre aDeplacer, int position) {

        if (position < 0)
            return false;

        if (this.equals(aDeplacer.getParent())) {
            // Le chapitre à déplacer est déjà situé à la racine
            int positionPrecedente = aDeplacer.getParent().getChapitres().indexOf(aDeplacer);
            if (positionPrecedente == -1)
                return false;
            if (position > positionPrecedente) // La suppression entraîne un décallage
                position--;
        }

        if (!aDeplacer.getParent().supprimer(aDeplacer))
            return false;

        CanevasChapitre parentDuDeplace = aDeplacer.getParent();
        aDeplacer.setParent(null); // sécurité

        inserer(aDeplacer, position);
        parentDuDeplace.renumeroterChapitres();
        return true;
    }

    /**
     * cette méthode permet de vérifier si le chapitre possède ce chapitre fils
     * à la position donnée.
     * @param position numéro de fils
     * @param chap chapitre à chercher
     * @return résultat du test
     */
    public final boolean aEnfant(final Chapitre chap, final int position) {
        if (chap == null || position < 0 || position >= chapitres.size())
            return false;
        return chap.equals(chapitres.get(position));
    }

    /**
     * cette méthode permet de d'insérer un chapitre à la fin de la liste des
     * chapitres.
     * @return index du dernier chapitre + 1
     */
    @Override
    public final int dernierChapitre() {
        return chapitres.size();
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public List<Chapitre> getChapitres() {
        return chapitres;
    }

    @Override
    public void setChapitres(List<Chapitre> chapitres) {
        this.chapitres = chapitres;
    }

    @Override
    public CanevasChapitre getParent() {
        return parent;
    }

    @Override
    public void setParent(CanevasChapitre parent) {
        this.parent = parent;
    }

}
