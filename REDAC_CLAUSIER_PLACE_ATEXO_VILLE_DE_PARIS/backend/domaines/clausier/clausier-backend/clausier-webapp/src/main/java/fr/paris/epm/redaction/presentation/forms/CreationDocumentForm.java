package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevasAbstract;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.GestionConflitsServeurUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.List;

/**
 * formulaire de création de document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class CreationDocumentForm extends ActionForm {

    /**
     * Marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut numero de consultation.
     */
    private String numConsultation;
    /**
     * Attribut procedure Passation.
     */
    private String procedurePassation;
    /**
     * Attribut nature Prestation.
     */
    private String naturePrestation;
    /**
     * Attribut Type de document.
     */
    private String docType;
    /**
     * Attribut lot.
     */
    private String lot;
    /**
     * Attribut direction Service.
     */
    private String directionService;
    /**
     * Attribut pouvoir Adjudicateur.
     */
    private String pouvoirAdjudicateur;
    /**
     * Attribut intitule de la Consultation.
     */
    private String intituleConsultation;
    /**
     * Attribut collection type de document.
     */
    private Collection collectionDocType;
    /**
     * Attribut collection de Lots.
     */
    private List<EpmTBudLot> collectionLots;
    /**
     * si la consultation contient des lots alloti vaut true sinon false.
     */
    private String alloti = null;

    /**
     * Constructeur de la classe CreationDocumentForm.
     */
    public CreationDocumentForm() {
        reset();
    }

    /**
     * Cette methode pour initialiser le formulaire.
     */
    public void reset() {
        collectionDocType = null;
        numConsultation = null;
        procedurePassation = null;
        naturePrestation = null;
        docType = null;
        lot = null;
        collectionLots = null;
        directionService = null;
        intituleConsultation = null;
        pouvoirAdjudicateur = null;
    }

    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

        ActionErrors erreurs = super.validate(mapping, request);
        if (erreurs == null) {
            erreurs = new ActionErrors();
        }
        erreurs.clear();
        if (docType == null || docType.trim().equals("-1")) {
            erreurs.add("docType", new ActionMessage("creationDocument.valider.docType"));
        }

        HttpSession session = request.getSession();
        if (Constante.DUPLIQUER.equals(session.getAttribute(Constante.TYPE_OPERATION))) {
            EpmTCanevasAbstract epmTCanevas = (EpmTCanevasAbstract) session.getAttribute(Constante.CANEVAS_DU_DOCUMENT_A_DUPLIQUE);
            int idLot = Integer.parseInt(this.lot);

            EpmTConsultation nouvelleConsultation = (EpmTConsultation) session.getAttribute(Constante.NOUVELLE_CONSULTATION);
            if (nouvelleConsultation != null) { // document dupliqué d'une autre consultation
                if (!GestionConflitsServeurUtils.verifierCCAGCompatibles(nouvelleConsultation, epmTCanevas, idLot)) {
                    erreurs.add("ccagLotIncompatible",
                            new ActionMessage("DupliquerDocument.erreur.ccagLotIncompatible"));
                }
            } else { // document dupliqué da la même consultation
                EpmTConsultation consultationDuDocumentOriginal = (EpmTConsultation) session.getAttribute(Constante.CONSULTATION_DU_DOCUMENT_A_DUPLIQUE);
                if (!GestionConflitsServeurUtils.verifierCCAGCompatibles(consultationDuDocumentOriginal, epmTCanevas, idLot)) {
                    erreurs.add("ccagLotIncompatible",
                            new ActionMessage("DupliquerDocument.erreur.ccagLotIncompatible"));
                }
            }
        }
        return erreurs;
    }


    /**
     * @return numéro de la consultation.
     */
    public final String getNumConsultation() {
        return numConsultation;
    }

    /**
     * @param valeur pour positionner le numéro de la consultation.
     */
    public final void setNumConsultation(final String valeur) {
        numConsultation = valeur;
    }

    /**
     * @return procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * @param valeur initialise la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * @return nature de préstation
     */
    public final String getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @param valeur positionne la nature de préstation.
     */
    public final void setNaturePrestation(final String valeur) {
        naturePrestation = valeur;
    }

    /**
     * @return Type de document
     */
    public final String getDocType() {
        return docType;
    }

    /**
     * @param valeur positionne le type de document.
     */
    public final void setDocType(final String valeur) {
        docType = valeur;
    }

    /**
     * @return collection de types de document.
     */
    public final Collection getCollectionDocType() {
        return collectionDocType;
    }

    /**
     * @param valeur pour initialiser une collection de types de document.
     */
    public final void setCollectionDocType(final Collection valeur) {
        collectionDocType = valeur;
    }

    /**
     * @return lot du document.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * @param valeur pour initialiser lot du document.
     */
    public final void setLot(final String valeur) {
        lot = valeur;
    }

    /**
     * @return collection de Lots
     */
    public final List<EpmTBudLot> getCollectionLots() {
        return collectionLots;
    }

    /**
     * @param valeur pour initialiser une collection de lots.
     */
    public final void setCollectionLots(final List<EpmTBudLot> valeur) {
        collectionLots = valeur;
    }

    /**
     * @return direction Service
     */
    public final String getDirectionService() {
        return directionService;
    }

    /**
     * @param valeur positionne direction / service
     */
    public final void setDirectionService(final String valeur) {
        directionService = valeur;
    }

    /**
     * @return pouvoir Adjudicateur
     */
    public final String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur pour positionner le pouvoir adjudicateur.
     */
    public final void setPouvoirAdjudicateur(final String valeur) {
        pouvoirAdjudicateur = valeur;
    }

    /**
     * @return intitule de la Consultation
     */
    public final String getIntituleConsultation() {
        return intituleConsultation;
    }

    /**
     * @param valeur positionne l'intitule de la consultation
     */
    public final void setIntituleConsultation(final String valeur) {
        intituleConsultation = valeur;
    }

    /**
     * @return l'attribut alloti
     */
    public final String getAlloti() {
        return alloti;
    }

    /**
     * @param valeur positionne l'attribut alloti.
     */
    public final void setAlloti(final String valeur) {
        alloti = valeur;
    }

}
