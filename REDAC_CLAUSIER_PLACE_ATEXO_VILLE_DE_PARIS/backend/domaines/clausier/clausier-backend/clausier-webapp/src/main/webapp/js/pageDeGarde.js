function enregistrer(aiguillage, categorie) {
	document.forms[0].action = 'genererPageDeGardeProcess.epm';
	document.getElementById('aiguillage').value = aiguillage;
	document.getElementById('categorie').value = categorie;
	document.forms[0].submit();
}

function ouvrir(id, type, consultation) {
	if(id !== 0 && type != null) {
		window.open('openDocument.htm?document='+id+'&type='+type+'&consultation='+consultation+'&mode=edit');
	}
}

function annuler(externe, categorie) {
	document.forms[0].action = externe + "RechercherDocumentInit.epm?init=false&type=&ouvrirTous&categorie="+categorie;
	document.forms[0].submit();
}

function supprimerPageDeGarde(idPageDeGarde) {
	document.location.href = 'supprimerPageDeGardeProcess.epm?supprimer=true&id=' + idPageDeGarde;
}

let load = (function () {
	if (window.FileReader) {
		let reader = new window.FileReader();

		return function () {
			let files = document.getElementById('fichier').files;
			if (files.length === 0) { return; }

			let file = files[0];
			let name = file.name.substring(0, file.name.lastIndexOf('.'));

			let titreDocument = document.getElementById('titreDocument');
			titreDocument.setAttribute('value', name)

			let nomFichier = document.getElementById('nomFichier');
			nomFichier.setAttribute('value', name);

			reader.readAsDataURL(file);
		}
	}
})();

function maj(value) {
	document.getElementById('titreDocument').value = document.getElementById('titre-'+value).value;
	document.getElementById('nomFichier').value = document.getElementById('nom-'+value).value;
}

