package fr.paris.epm.redaction.presentation.views.mappers;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.presentation.views.Environnement;
import fr.paris.epm.redaction.presentation.views.Organisme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OrganismeViewMapper implements Mapper<EpmTRefOrganisme, Organisme> {

	@Value("${plateforme}")
	private String plateforme;

	@Value("${client}")
	private String client;

	@Override
	public Organisme map( EpmTRefOrganisme organisme ) {
		final var result = new Organisme();
		result.setAcronyme(organisme.getCodeExterne());
		result.setIdentifiantExterne(organisme.getId());
		result.setLibelle(organisme.getLibelle());
		result.setEnvironnement(new Environnement(client, plateforme));
		return result;
	}
}
