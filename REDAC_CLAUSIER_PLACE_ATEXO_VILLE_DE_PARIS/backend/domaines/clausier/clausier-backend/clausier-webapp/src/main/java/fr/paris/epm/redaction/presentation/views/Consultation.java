package fr.paris.epm.redaction.presentation.views;

import java.util.Date;
import java.util.Set;

public class Consultation {

	private String reference;

	private Organisme organisme;

	private Set<Lot> lots;

	private String intitule;

	private String objet;

	private Service service;

	private Date dateRemisePlis;

	private Procedure procedure;

	private String numero;

	private Boolean mps;

	public String getReference() {
		return reference;
	}

	public void setReference( final String reference ) {
		this.reference = reference;
	}

	public Set<Lot> getLots() {
		return lots;
	}

	public void setLots( final Set<Lot> lots ) {
		this.lots = lots;
	}

	public void setIntitule( String intitule ) {
		this.intitule = intitule;
	}

	public String getIntitule() {
		return intitule;
	}

	public Organisme getOrganisme() {
		return organisme;
	}

	public void setOrganisme( Organisme organisme ) {
		this.organisme = organisme;
	}

	public void setObjet( String objet ) {
		this.objet = objet;
	}

	public String getObjet() {
		return objet;
	}

	public void setService( Service service ) {
		this.service = service;
	}

	public Service getService() {
		return service;
	}

	public void setDateRemisePlis( Date dateRemisePlis ) {
		this.dateRemisePlis = dateRemisePlis;
	}

	public Date getDateRemisePlis() {
		return dateRemisePlis;
	}

	public void setProcedure( Procedure procedure ) {
		this.procedure = procedure;
	}

	public Procedure getProcedure() {
		return procedure;
	}

	public void setNumero( String numero ) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public Boolean getMps() {
		return mps;
	}

	public void setMps( Boolean mps ) {
		this.mps = mps;
	}
}
