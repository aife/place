<%--
  Created by IntelliJ IDEA.
  User: qba
  Date: 06/06/18
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="searchInstance" type="fr.paris.epm.redaction.presentation.bean.PublicationClausierBean"--%>

<form:form modelAttribute="searchInstance" action="searchPublicationClausier.htm" cssClass="form-horizontal">
    <div class="atx-panel panel panel-default">

        <jsp:include page="../panelHeadingRecherche.jsp" />

        <div id="panel-critres-recherche" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <div class="clearfix">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-sm">
                            <form:label path="version" cssClass="control-label col-md-2">
                                <spring:message code="publicationClausier.listPublicationClausier.version" /> :
                            </form:label>
                            <div class="col-md-8">
                                <form:input path="version" cssClass="form-control input-sm" />
                            </div>
                        </div>
                        <div class="form-group form-group-sm">
                            <form:label path="datePublicationMin" cssClass="control-label col-md-2">
                                <spring:message code="publicationClausier.listPublicationClausier.datePublication" /> :
                            </form:label>
                            <div class="col-md-3">
                                <div class="input-group date datetimepicker datetimepicker-date" data-provide="datepicker">
                                    <form:input path="datePublicationMin" class="form-control datepicker" data-date-format="DD-MM-YYYY"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                            <form:label path="datePublicationMax" cssClass="control-label col-md-2">
                                <spring:message code="redaction.et" /> :
                            </form:label>
                            <div class="col-md-3">
                                <div class="input-group date datetimepicker datetimepicker-date" data-provide="datepicker">
                                    <form:input path="datePublicationMax" class="form-control datepicker" data-date-format="DD-MM-YYYY"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group form-group-sm">
                            <form:label path="gestionLocalRecherche" cssClass="control-label col-md-4">
                                <spring:message code="publicationClausier.listPublicationClausier.gestionLocale" /> :
                            </form:label>
                            <div class="col-md-8">
                                <form:select path="gestionLocalRecherche" data-size="6" data-live-search="true" cssClass="form-control">
                                    <form:options items="${allGestionLocale}" itemValue="id" itemLabel="label" />
                                </form:select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <button type="submit" class="btn btn-primary btn-sm pull-right">
                        <spring:message code="publicationClausier.listPublicationClausier.rechercher" />
                    </button>
                </div>
            </div>
        </div>
    </div>
</form:form>