<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
        <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
        <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
        <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
        <script type="text/javascript">
            hrefRacine = '<atexo:href href=""/>';
        </script>
    </head>

    <%--@elvariable id="document" type="fr.paris.epm.redaction.metier.objetValeur.document.Document"--%>
    <body onload="popupResize();">
        <div class="previsu-doc" id="container">
            <div class="content">
                <center>
                    <h5>
                        <c:out value="${document.reference}" />&nbsp;:&nbsp;
                        <c:out value="${document.nomFichier}" /><c:out value="${document.extension}" />
                        <bean:message key="previsualiserDocument.txt.parentheseOuvrante"/>
                        <bean:message key="previsualiserDocument.txt.canevas"/>
                        <c:out value="${document.refCanevas}" />
                        <bean:message key="previsualiserDocument.txt.parentheseFermante"/>
                    </h5>
                </center>
                <ul>
                    <li>
                        <bean:define id="chapitres" name="document" property="chapitres" toScope="request" />
                        <bean:define id="premierNiveau" value="true" toScope="request" />
                        <jsp:include page="/jsp/previsualisation/document/Chapitre.jsp" />
                    </li>
                </ul>
                <div class="legende">
                    <span><strong><bean:message key="previsualiserDocument.txt.legende"/></strong></span>
                    <span class="orange"><bean:message key="previsualiserDocument.txt.TexteVerif"/></span>
                    <span class="grey"><bean:message key="previsualiserDocument.txt.nonModif"/></span>
                    <span class="green"><bean:message key="previsualiserDocument.txt.saisiValide"/></span>
                    <span class="red"><bean:message key="previsualiserDocument.txt.saisir"/></span>
                    <span class="blue"><bean:message key="previsualiserDocument.txt.herite"/></span>
                </div>
            </div>
            <a href="javascript:window.close();" class="fermer">
                <bean:message key="previsualiserDocument.txt.fermer"/>
            </a>
            <div class="breaker"></div>
        </div>
    </body>
</html>
