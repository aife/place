package fr.paris.epm.redaction.presentation.bean;

import fr.paris.epm.global.coordination.bean.AbstractBean;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class PublicationClausierBean extends AbstractBean {

    private String version;

    /**
     * Identifiant de l'utilisateur ayant publié le clausier.
     */
    private int idUtilisateur;

    /**
     * Nom + prénom de l'utilisateur ayant publié le clausier utilisé pour le tri.
     */
    private String nomCompletUtilisateur;

    /**
     * Identifiant de l'organisme
     */
    private Integer idOrganisme;

    /**
     * Commentaire saisi sur l'écran d'hitorique des version
     */
    private String commentaire;

    /**
     * Date de publication du clausier.
     */
    private Date datePublication;

    /**
     * id du fichier contenant les référentiels lors de cette publication
     */
    private Integer idReferentielFile;

    /**
     * id du fichier contenant le clausier de cette publication
     */
    private Integer idClausierFile;

    private String editeur;

    private Date dateIntegration;

    private Date dateActivation;

    private boolean gestionLocal;

    /**
     * date pour la recherche
     */
    @DateTimeFormat(pattern = "dd-MM-yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date datePublicationMin;

    @DateTimeFormat(pattern = "dd-MM-yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date datePublicationMax;

    private Integer gestionLocalRecherche;

    @NotEmpty(message = "Le premier champ de la version est onbligatoire")
    @NotNull(message = "Le premier champ de la version est onbligatoire")
    private String version1;

    @NotEmpty(message = "Le deuxième champ de la version est onbligatoire")
    @NotNull(message = "Le deuxième champ de la version est onbligatoire")
    private String version2;

    @NotEmpty(message = "Le dernier champ de la version est onbligatoire")
    @NotNull(message = "Le dernier champ de la version est onbligatoire")
    private String version3;

    /**
     * si version est actif ou pas (on ne peut avoir qu'une seul version actif chaque moment)
     */
    private boolean actif;

    private boolean isEnCoursActivation;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomCompletUtilisateur() {
        return nomCompletUtilisateur;
    }

    public void setNomCompletUtilisateur(String nomCompletUtilisateur) {
        this.nomCompletUtilisateur = nomCompletUtilisateur;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public Integer getIdReferentielFile() {
        return idReferentielFile;
    }

    public void setIdReferentielFile(Integer idReferentielFile) {
        this.idReferentielFile = idReferentielFile;
    }

    public Integer getIdClausierFile() {
        return idClausierFile;
    }

    public void setIdClausierFile(Integer idClausierFile) {
        this.idClausierFile = idClausierFile;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public Date getDateIntegration() {
        return dateIntegration;
    }

    public void setDateIntegration(Date dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public Date getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Date dateActivation) {
        this.dateActivation = dateActivation;
    }

    public boolean isGestionLocal() {
        return gestionLocal;
    }

    public void setGestionLocal(boolean gestionLocal) {
        this.gestionLocal = gestionLocal;
    }

    public Date getDatePublicationMin() {
        return datePublicationMin;
    }

    public void setDatePublicationMin(Date datePublicationMin) {
        this.datePublicationMin = datePublicationMin;
    }

    public Date getDatePublicationMax() {
        return datePublicationMax;
    }

    public void setDatePublicationMax(Date datePublicationMax) {
        this.datePublicationMax = datePublicationMax;
    }

    public Integer getGestionLocalRecherche() {
        return gestionLocalRecherche;
    }

    public void setGestionLocalRecherche(Integer gestionLocalRecherche) {
        this.gestionLocalRecherche = gestionLocalRecherche;
    }

    public String getVersion1() {
        return version1;
    }

    public void setVersion1(String version1) {
        this.version1 = version1;
    }

    public String getVersion2() {
        return version2;
    }

    public void setVersion2(String version2) {
        this.version2 = version2;
    }

    public String getVersion3() {
        return version3;
    }

    public void setVersion3(String version3) {
        this.version3 = version3;
    }

    public boolean isEnCoursActivation() {
        return isEnCoursActivation;
    }

    public void setEnCoursActivation(boolean enCoursActivation) {
        isEnCoursActivation = enCoursActivation;
    }
}
