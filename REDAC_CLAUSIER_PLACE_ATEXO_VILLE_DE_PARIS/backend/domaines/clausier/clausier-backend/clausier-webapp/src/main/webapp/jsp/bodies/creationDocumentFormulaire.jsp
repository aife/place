<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!--Debut main-part-->
<c:if test="${not empty sessionScope.prefixeRedirection}">
    <logic:messagesPresent>
        <div class="form-bloc-erreur msg-erreur">
            <div class="top">
                <span class="left"></span><span class="right"></span>
            </div>
            <div class="content">
                <div class="title"><bean:message key="erreur.texte.generique"/></div>
                <html:errors  />
            </div>
            <div class="breaker"></div>
            <div class="bottom">
                <span class="left"></span><span class="right"></span>
            </div>
        </div>
    </logic:messagesPresent>
</c:if>

<!--Debut bloc Infos clause-->
<div class="form-saisie">
    <html:form action="/CreationDocumentProcess.epm">
        <div class="form-bloc">
            <div class="top">
                <span class="left"></span><span class="right"></span>
            </div>
            <div class="content">
                <div class="breaker"></div>
                <div class="column-moyen">

                    <span class="intitule"><bean:message key="CreationDocument.txt.typeDocument" /></span>

                    <logic:equal name="typeAction" value="C">
                        <html:select name="frmCreationDocument" property="docType" styleId="docType" title="Type de document" errorStyleClass="error-border">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="collectionDocType" value="id" label="libelle" />
                        </html:select>
                    </logic:equal>

                    <logic:notEqual name="typeAction" value="C">
                        <html:select name="frmCreationDocument" styleId="docType" property="docType" title="Type de document" errorStyleClass="error-border " disabled="true">
                            <html:option value="-1"><bean:message key="redaction.txt.selectionner" /></html:option>
                            <html:optionsCollection property="collectionDocType" value="id" label="libelle" />
                        </html:select>
                    </logic:notEqual>
                </div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="CreationDocument.txt.lot" /></span>
                    <c:if test="${actionCompatibiliteAllotissement != null}">

                    </c:if>
                    <html:select name="frmCreationDocument" styleId="lot" property="lot" title="Lot">
                        <logic:equal name="frmCreationDocument" property="alloti"
                                     value="oui">
                            <c:if test="${actionCompatibiliteAllotissement != null && actionCompatibiliteAllotissement == 'tous'}">
                                <html:option value="0">
                                    <bean:message key="redaction.txt.Tous" />
                                </html:option>
                            </c:if>
                            <c:if test="${actionCompatibiliteAllotissement != null && actionCompatibiliteAllotissement eq 'choixLot'}">
                                <html:optionsCollection property="collectionLots" value="id" label="intituleLot" />
                            </c:if>
                            <c:if test="${actionCompatibiliteAllotissement == null || actionCompatibiliteAllotissement eq 'tous+choixLot'}">
                                <html:option value="0">
                                    <bean:message key="redaction.txt.Tous" />
                                </html:option>
                                <html:optionsCollection property="collectionLots" value="id" label="intituleLot" />
                            </c:if>
                        </logic:equal>
                        <logic:notEqual name="frmCreationDocument" property="alloti" value="oui">
                            <html:option value="-1">
                                <bean:message key="CreationDocument.txt.nonapplicable" />
                            </html:option>
                        </logic:notEqual>
                    </html:select>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom">
                <span class="left"></span><span class="right"></span>
            </div>
        </div>
        <!--Fin bloc Infos clause-->
        <!--Debut boutons-->
        <div class="spacer"></div>
        <div class="boutons">
            <c:set var="suffixeRedirection" value=""/>
            <c:if test="${not empty sessionScope.prefixeRedirection}">
                <%-- <c:set var="suffixeRedirection" value="&numeroConsultation=${frmCreationDocument.numConsultation}&identifiant=${frmCreationDocument.identifiant}"/> --%>
                <c:set var="suffixeRedirection" value="&numeroConsultation=${frmCreationDocument.numConsultation}"/>
            </c:if>
            <c:set var="typeRecherche" value="${sessionScope.prefixeRedirection}RechercherDocumentInit" />
            <c:if test="${idNouvelleConsultation != null && not empty idNouvelleConsultation}">
                <c:set var="idNouvelleConsultation" value="&idNouvelleConsultation=${idNouvelleConsultation}"/>
                <c:set var="typeRecherche" value="RechercherDocumentADupliquerInit" />
            </c:if>

            <c:if test="${idAncienneConsultation != null && not empty idAncienneConsultation}">
                <c:set var="idAncienneConsultation" value="&idAncienneConsultation=${idAncienneConsultation}"/>
            </c:if>
            <a href="${typeRecherche}.epm?categorie=dcr&init=false${suffixeRedirection}${idNouvelleConsultation}${idAncienneConsultation}&type=&ouvrirTous&numeroCons=${frmCreationDocument.numConsultation}" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
            <a onclick="showLoader()" href="javascript:document.forms[0].submit()" class="valider"><bean:message key="redaction.txt.valider" /></a>
        </div>
        <!--Fin boutons-->
    </html:form>
</div>
