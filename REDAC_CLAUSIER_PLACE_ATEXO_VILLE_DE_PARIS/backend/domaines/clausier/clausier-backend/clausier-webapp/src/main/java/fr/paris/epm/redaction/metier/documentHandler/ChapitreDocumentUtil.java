package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.commun.util.Util;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.commun.Derogation;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.metier.objetValeur.document.*;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * la classe ChapitreDocumentUtil qui traite des chapitres d'un document.
 *
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public abstract class ChapitreDocumentUtil extends AbstractDocument {

    /**
     * marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ChapitreDocumentUtil.class);

    /**
     * Cette méthode permet la conversion d'un element chapitre XML vers un
     * chapitre document.
     *
     * @param modificationDocument date de modification.
     * @param pere                 chapitre père.
     * @param elementChapitreInXML à convertir.
     * @return chapitre document.
     * @throws TechnicalException erreur technique
     */
    public static final ChapitreDocument elementChapitreXMLVersChapitreDocument(final Date modificationDocument, final AbstractChapitreDocument pere,
                                                                                final Element elementChapitreInXML, final Integer idOrganisme) {
        // Récupération des attributs
        LOG.debug("Récupération des attributs");
        ChapitreDocument chapitreDocumentInstance = new ChapitreDocument();

        try {
            chapitreDocumentInstance.setParent(pere);
            chapitreDocumentInstance.setTitre(elementChapitreInXML.getAttribute(CHAPITRE_TITRE).getValue());
            try {
                chapitreDocumentInstance.setId(elementChapitreInXML.getAttribute(CHAPITRE_ID).getIntValue());
            } catch (DataConversionException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
            }
            chapitreDocumentInstance.setNumero(elementChapitreInXML.getAttribute(CHAPITRE_NUMERO).getValue());
            if (elementChapitreInXML.getAttribute(CHAPITRE_VALIDE) != null) {
                if (elementChapitreInXML.getAttribute(CHAPITRE_VALIDE).getValue().equals("true")) {
                    chapitreDocumentInstance.setValide(true);
                } else {
                    chapitreDocumentInstance.setValide(false);
                }
                // chapitreDocumentInstance.setValide(Boolean
                // .getBoolean(elementChapitreInXML
                // .getAttribute(CHAPITRE_VALIDE).getValue()));
            } else {
                chapitreDocumentInstance.setValide(false);
            }

            // Récupération de l'infobulle
            LOG.debug("Récupération l'infobulle");
            Element bulleElm = elementChapitreInXML.getChild("infoBulle");
            InfoBulle bulle = InfoBulleUtil.elementInfoBulleXMLVersInfobulle(bulleElm);
            chapitreDocumentInstance.setInfoBulle(bulle);

            // Récupération de la dérogation
            Element derogationXML = getElement(elementChapitreInXML, DEROGATION);
            if (derogationXML != null) {
                Derogation derogation = new Derogation();
                Element articleDerogation = getElement(derogationXML, ARTICLE_DEROGATION);
                if (articleDerogation != null) {
                    derogation.setArticleDerogationCCAG(articleDerogation.getText());
                }
                Element commentaireDerogation = getElement(derogationXML, COMMENTAIRE_DEROGATION);
                if (commentaireDerogation != null) {
                    derogation.setCommentairesDerogationCCAG(commentaireDerogation.getText());
                }
                Element afficherNBMessage = getElement(derogationXML, AFFICHER_NB_MESSAGE_DEROGATION);
                if (afficherNBMessage != null) {
                    chapitreDocumentInstance.setAfficherNBMessageDerogation(Boolean.valueOf(afficherNBMessage.getValue()));
                }

                chapitreDocumentInstance.setDerogation(derogation);
            }

            if (elementChapitreInXML.getAttribute(CHAPITRE_STYLE) != null)
                chapitreDocumentInstance.setStyleChapitre(elementChapitreInXML.getAttribute(CHAPITRE_STYLE).getValue());

            // Récupération du paragraphe
            LOG.debug("Récupération du paragraphe");
            Element paragraphe = getElement(elementChapitreInXML, PARAGRAPHE_NOM);

            ParagrapheDocument paragrapheDocument;
            paragrapheDocument = ParagrapheUtil.elementParagrapheXMLVersParagrahe(paragraphe);
            chapitreDocumentInstance.setParagrapheDocument(paragrapheDocument);

            // Récupération des clauses
            LOG.debug("Récupération des clauses");
            List<Element> clausesDoc = getElements(elementChapitreInXML, CLAUSE_NOM);
            if (clausesDoc != null) {
                List<ClauseDocument> listeClauses = clausesDoc.stream()
                        .map(xmlClause -> ClauseDocumentUtil.elementClauseXMLVersClause(modificationDocument, xmlClause, idOrganisme))
                        .filter(Objects::nonNull)
                        .peek(clauseDocument -> clauseDocument.setParent(chapitreDocumentInstance))
                        .collect(Collectors.toList());

                LOG.debug("Liste des clauses du document = {}", listeClauses);

                chapitreDocumentInstance.setClausesDocument(listeClauses);
            }

            // Récupération des sous chapitres
            LOG.debug("Récupération des sous chapitres");
            ElementFilter filtreSousChapitre = new ElementFilter(CHAPITRE_NOM);
            List<Element> sousChapitresDoc = elementChapitreInXML.getContent(filtreSousChapitre);
            if (sousChapitresDoc != null)
                sousChapitresDoc.stream()
                        .map(xmlChapitre -> elementChapitreXMLVersChapitreDocument(modificationDocument, chapitreDocumentInstance, xmlChapitre, idOrganisme))
                        .forEach(chapitreDocument -> chapitreDocumentInstance.getSousChapitres().add(chapitreDocument));

            Attribute nbSautLigneAttr = elementChapitreInXML.getAttribute(CHAPITRE_NOMBRE_SAUT_LIGNES);
            if (nbSautLigneAttr != null)
                chapitreDocumentInstance.setNombreSautLignes(nbSautLigneAttr.getValue());

        } catch (Exception e) {
            LOG.error("Erreur lors de la génération du chapitre, titre du chapitre : " + chapitreDocumentInstance.getTitre() +
                    " ,numéro du chapitre : " + chapitreDocumentInstance.getNumero(), e.fillInStackTrace());
        }
        return chapitreDocumentInstance;
    }

    /**
     * Cette méthode permet la conversion d'un chapitre document vers un element
     * chapitre XML.
     *
     * @param chapitreDocument un chapitre du document
     * @return un element chapitre XML.
     * @throws TechnicalException erreur technique
     */
    public static final Element versJdom(final ChapitreDocument chapitreDocument) {
        // Les attributs
        if (chapitreDocument != null && !chapitreDocument.isSupprime() && chapitreDocument.getNumero() != null) {
            LOG.debug("Création du chapitre");
            Element xmlChapitre = new Element(CHAPITRE_NOM);
            LOG.debug("Associer le titre au chapitre");
            if (chapitreDocument.getTitre() != null) {
                Attribute titre = new Attribute(CHAPITRE_TITRE, chapitreDocument.getTitre());
                xmlChapitre.setAttribute(titre);
            } else {
                LOG.error("Le titre du chapitre {} est null", chapitreDocument.getId());
            }

            if (chapitreDocument.getNumero() != null) {
                Attribute numero = new Attribute(CHAPITRE_NUMERO, chapitreDocument.getNumero());
                xmlChapitre.setAttribute(numero);
            } else {
                LOG.error("Le numéro du chapitre {} est null", chapitreDocument.getId());
            }
            Attribute id = new Attribute(CHAPITRE_ID, String.valueOf(chapitreDocument.getId()));
            xmlChapitre.setAttribute(id);

            Attribute valide = new Attribute(CHAPITRE_VALIDE, String.valueOf(chapitreDocument.isValide()));
            xmlChapitre.setAttribute(valide);

            if (chapitreDocument.getStyleChapitre() != null) {
                Attribute styleChapitre = new Attribute(CHAPITRE_STYLE, chapitreDocument.getStyleChapitre());
                xmlChapitre.setAttribute(styleChapitre);
            }

            if ((chapitreDocument.getNombreSautLignes() != null) && !"".equals(chapitreDocument.getNombreSautLignes())) {
                Attribute nombreSautLignes = new Attribute(CHAPITRE_NOMBRE_SAUT_LIGNES, chapitreDocument.getNombreSautLignes());
                xmlChapitre.setAttribute(nombreSautLignes);
            }

            // Récupération de l'infobulle
            LOG.debug("Associer l'info bulle au chapitre");
            xmlChapitre.addContent(CHAPITRE_POS_BULLE, InfoBulleUtil
                    .versJdom(chapitreDocument.getInfoBulle()));

            // Cas des dérogations
            if (chapitreDocument.getDerogation() != null && !chapitreDocument.getDerogation().isEmpty()) {
                Element derogationXML = new Element(DEROGATION);

                if (chapitreDocument.getDerogation().getArticleDerogationCCAG() != null) {
                    Element articleXML = new Element(ARTICLE_DEROGATION);
                    articleXML.setText(chapitreDocument.getDerogation().getArticleDerogationCCAG());
                    derogationXML.addContent(articleXML);
                }

                if (chapitreDocument.getDerogation().getCommentairesDerogationCCAG() != null) {
                    Element commentaireXML = new Element(COMMENTAIRE_DEROGATION);
                    commentaireXML.setText(chapitreDocument.getDerogation().getCommentairesDerogationCCAG());
                    derogationXML.addContent(commentaireXML);
                }

                if (chapitreDocument.isAfficherNBMessageDerogation()) {
                    Element afficherNBMessage = new Element(AFFICHER_NB_MESSAGE_DEROGATION);
                    afficherNBMessage.setText("true");
                    derogationXML.addContent(afficherNBMessage);
                }

                xmlChapitre.addContent(derogationXML);
            } else {
                xmlChapitre.removeChild(DEROGATION);
            }

            // Récupération du paragraphe
            LOG.debug("Associer le paragraphe au chapitre");
            Element paragraphe = ParagrapheUtil.versJdom(chapitreDocument.getParagrapheDocument());
            if (paragraphe != null) {
                xmlChapitre.addContent(CHAPITRE_POS_PARAGRAPHE, paragraphe);
            }
            // Récupération des clauses
            LOG.debug("Associer les clauses au chapitre");
            List clausesDoc = chapitreDocument.getClausesDocument();
            ArrayList listeClausesDoc = new ArrayList(clausesDoc.size());
            LOG.debug("Nombre des clauses : " + clausesDoc.size());
            for (int i = 0; i < clausesDoc.size(); i++) {
                if (clausesDoc.get(i) instanceof ClauseListe) {
                    listeClausesDoc.add(ClauseDocumentUtil.versJdom((ClauseListe) clausesDoc.get(i)));
                } else {
                    listeClausesDoc.add(ClauseDocumentUtil.versJdom((ClauseTexte) clausesDoc.get(i)));
                }
            }
            xmlChapitre.addContent(listeClausesDoc);

            // Récupération des sous chapitres
            LOG.debug("Associer les sous chapitres au chapitre");
            List chapitresDoc = chapitreDocument.getSousChapitres();
            List listeChapitresDom = new ArrayList(chapitresDoc.size());

            for (int i = 0; i < chapitresDoc.size(); i++) {
                ChapitreDocument courantChapitre = (ChapitreDocument) chapitresDoc.get(i);
                listeChapitresDom.add(versJdom(courantChapitre));
            }
            xmlChapitre.addContent(listeChapitresDom);

            return xmlChapitre;
        } else {
            return null;
        }
    }

    /**
     * cette méthode permet la Conversion du chapitre canevas vers chapitre
     * document.
     *
     * @param parent           chapitre parent
     * @param chapitreCanevas  chapitre canevas à convertir.
     * @param consultation     identifiant de la consultation.
     * @param utilisateur      l'utilisateur.
     * @param idLot            id du lot associé au document
     * @param rechercherClause si true alors on récupère l'objet {@link EpmTClause} dont l'id est celui du clauseCanevas
     * @return chapitre document.
     * @throws NonTrouveException      objet non trouvé
     * @throws TechnicalException      erreur technique
     * @throws TechnicalNoyauException
     */
    public static final ChapitreDocument chapitreCanevasToChapitreDocument(final AbstractChapitreDocument parent, final Chapitre chapitreCanevas,
                                                                           final EpmTConsultation consultation, final EpmTUtilisateur utilisateur,
                                                                           final int idLot, final ResourceBundleMessageSource messageSource,
                                                                           final RechercheClauseEnum rechercherClause) {

        LOG.debug("Conversion du chapitre canevas au chapitre document");
        ChapitreDocument chapitreDocument = new ChapitreDocument();
        chapitreDocument.setParent(parent);
        chapitreDocument.setTitre(chapitreCanevas.getTitre());
        chapitreDocument.setId(chapitreCanevas.getIdChapitre());
        chapitreDocument.setNumero(chapitreCanevas.getNumero());
        chapitreDocument.setValide(false);
        chapitreDocument.setInfoBulle(chapitreCanevas.getInfoBulle());
        if (chapitreCanevas.getStyleChapitre() != null) {
            chapitreDocument.setStyleChapitre(chapitreCanevas.getStyleChapitre());
        }
        chapitreDocument.setDerogation(chapitreCanevas.getDerogation());
        chapitreDocument.setAfficherNBMessageDerogation(chapitreCanevas.isAfficherMessageDerogation());

        // Récupération des sous chapitres
        LOG.debug("Nombre des sous chapitres : {}", chapitreCanevas.getChapitres().size());
        List<Chapitre> chapitresCanevas = chapitreCanevas.getChapitres();
        List<ChapitreDocument> listeSousChapitresDoc = new ArrayList<>(chapitresCanevas.size());
        for (Chapitre chapitre : chapitresCanevas) {
            ChapitreDocument cd = chapitreCanevasToChapitreDocument(chapitreDocument, chapitre, consultation, utilisateur, idLot, messageSource, rechercherClause);
            listeSousChapitresDoc.add(cd);
        }
        chapitreDocument.setSousChapitres(listeSousChapitresDoc);

        // Récupération des clauses
        LOG.debug("Nombre des clauses : {}", chapitreCanevas.getClauses().size());
        //Ne garder que les clauses compatibles avec le type contrat de la consultation
        List<Clause> clausesCanevas = new ArrayList<>();
        List<Clause> clausesCanevasComplete = chapitreCanevas.getClauses();
        for (Clause clause : clausesCanevasComplete) {
            boolean isCompatibleWithConsultation = true;
            //affichage ou non de la clause en fonction du type contrat de la consultation
            if (!clause.getIdsTypeContrats().isEmpty() && consultation.getEpmTRefTypeContrat() != null) {
                if (!clause.getIdsTypeContrats().contains(consultation.getEpmTRefTypeContrat().getId())) {
                    isCompatibleWithConsultation = false;
                }
            }
            //affichage ou non de la clause en fonction de la procédure de la consultation
            if (clause.getIdProcedure() != null && consultation.getEpmTRefProcedure() != null) {
                if (!clause.getIdProcedure().equals(consultation.getEpmTRefProcedure().getId())) {
                    isCompatibleWithConsultation = false;
                }
            }
            if (isCompatibleWithConsultation) {
                clausesCanevas.add(clause);
            }
        }
        List<ClauseDocument> listeClausesDoc = new ArrayList<>(clausesCanevas.size());
        for (Clause clauseCanevas : clausesCanevas) {
            ClauseDocument clause = ClauseDocumentUtil.clauseCanevasToClauseDocument(clauseCanevas, consultation, utilisateur, idLot, messageSource, rechercherClause);
            if (clause != null) {
                clause.setParent(chapitreDocument);
                listeClausesDoc.add(clause);
            }
        }
        chapitreDocument.setClausesDocument(listeClausesDoc);

        return chapitreDocument;
    }

    /**
     * cette méthode permet la Conversion du chapitre canevas vers chapitre
     * document pour l'export du modéle de canevas.
     *
     * @param parent          chapitre parent
     * @param chapitreCanevas chapitre canevas à convertir.
     * @param utilisateur     l'utilisateur.
     * @return chapitre document.
     * @throws NonTrouveException objet non trouvé
     * @throws TechnicalException erreur technique
     */
    public static final ChapitreDocument chapitreCanevasToChapitreDocument(
            final AbstractChapitreDocument parent,
            final Chapitre chapitreCanevas,
            final EpmTUtilisateur utilisateur, final ResourceBundleMessageSource messageSource) {

        LOG.debug("Conversion du chapitre canevas au chapitre document");
        ChapitreDocument chapitreDocument = new ChapitreDocument();
        chapitreDocument.setParent(parent);
        chapitreDocument.setTitre(chapitreCanevas.getTitre());
        chapitreDocument.setId(chapitreCanevas.getIdChapitre());
        chapitreDocument.setNumero(chapitreCanevas.getNumero());
        chapitreDocument.setValide(false);
        chapitreDocument.setInfoBulle(chapitreCanevas.getInfoBulle());
        if (chapitreCanevas.getStyleChapitre() != null)
            chapitreDocument.setStyleChapitre(chapitreCanevas.getStyleChapitre());
        chapitreDocument.setDerogation(chapitreCanevas.getDerogation());
        chapitreDocument.setAfficherNBMessageDerogation(chapitreCanevas.isAfficherMessageDerogation());

        // Récupération des sous chapitres
        LOG.debug("Nombre des sous chapitres : " + chapitreCanevas.getChapitres().size());
        List<Chapitre> chapitresCanevas = chapitreCanevas.getChapitres();
        List<ChapitreDocument> listeSousChapitresDoc = new ArrayList<ChapitreDocument>(chapitresCanevas.size());
        for (Chapitre chapitre : chapitresCanevas)
            listeSousChapitresDoc.add(chapitreCanevasToChapitreDocument(chapitreDocument, chapitre, utilisateur, messageSource));
        chapitreDocument.setSousChapitres(listeSousChapitresDoc);

        // Récupération des clauses
        LOG.debug("Nombre des clauses : " + chapitreCanevas.getClauses().size());
        List<Clause> clausesCanevas = chapitreCanevas.getClauses();
        List<ClauseDocument> listeClausesDoc = new ArrayList<ClauseDocument>(clausesCanevas.size());
        for (Clause clauseCanevas : clausesCanevas) {
            ClauseDocument clause = ClauseDocumentUtil.clauseCanevasToClauseDocument(clauseCanevas, utilisateur, messageSource, false);
            if (clause != null && "1".equals(clause.getActif())) {
                clause.setTexteFixeAvant(Util.sanitizeHtmlInput("\r" + clause.getTexteFixeAvant()));
                clause.setParent(chapitreDocument);
                listeClausesDoc.add(clause);
            }
        }
        chapitreDocument.setClausesDocument(listeClausesDoc);

        return chapitreDocument;
    }

}
