package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.EtapeSimple;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.metier.redaction.ClausePubCritere;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.noyau.service.DocumentModeleServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.coordination.service.PotentiellementConditionneeService;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseListe;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseTexte;
import fr.paris.epm.redaction.metier.objetValeur.document.Formulation;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.*;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.forms.ClauseFormulaire;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionneeAbstract.containsValue;

/**
 * classe DocumentUtil qui manipule des clauses d'un document.
 *
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseDocumentUtil extends AbstractDocument {

    /**
     * constante PRECOCHE_OUI ("1").
     */
    public static final String PRECOCHE_OUI = "1";

    /**
     * marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClauseDocumentUtil.class);


    private static RedactionServiceSecurise redactionService;

    /**
     * ClauseFacade injecté par spring
     */
    private static ClauseFacadeGWT clauseFacadeGWT;

    private static PotentiellementConditionneeService potentiellementConditionneeService;

    /**
     * Manipulateur des référentiels.
     */
    private static ReferentielFacade referentielFacade;

    /**
     * Manipulateur des consultations.
     */
    private transient static ConsultationServiceSecurise consultationService;

    /**
     * cette méthode permet de remplir les propriétés externes d'une clause.
     *
     * @param xmlClause calsue XML à remplir.
     * @return objet ClauseDocument
     * @throws TechnicalException erreur technique.
     */
    private static ClauseDocument remplirPptExtClause(final Element xmlClause) {
        try {
            int typeClause = xmlClause.getAttribute(CLAUSE_TYPE).getIntValue();
            LOG.debug("Clause {} de type {}", xmlClause, typeClause);

            if (typeClause == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF || typeClause == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                ClauseListe clauseListe = new ClauseListe();
                clauseListe.setFormulationsModifiable(xmlClause.getAttribute(CLAUSE_FORMULATIONS_MODIFIABLE).getBooleanValue());
                Element formulaireListe = getElement(xmlClause, CLAUSE_FORMULAIRE_LISTE_TYPE);
                List listeFormulations = getElements(formulaireListe, CLAUSE_FORMULATION);
                if (listeFormulations != null && !listeFormulations.isEmpty()) {
                    List<Formulation> formulations = new ArrayList<>(listeFormulations.size());
                    for (int i = 0; i < listeFormulations.size(); i++) {
                        Element formulationElement = (Element) listeFormulations.get(i);
                        Formulation formulationInstance = new Formulation();
                        formulationInstance.setNumFormulation(i + 1);

                        formulationInstance.setPrecochee(formulationElement.getAttribute(PRECOCHE).getBooleanValue());
                        if (formulationElement.getAttribute(TAILLE) != null) {
                            formulationInstance.setNbrCaraMax(formulationElement.getAttribute(TAILLE).getIntValue());
                        } else {
                            formulationInstance.setNbrCaraMax(1);
                        }
                        formulationInstance.setLibelle(formulationElement.getText());
                        formulations.add(formulationInstance);
                    }

                    clauseListe.setFormulations(formulations);
                }
                return clauseListe;
            } else {
                LOG.debug("Clause de type 'texte'");
                ClauseTexte clauseTexte = new ClauseTexte();
                // Récupération du texte variable dans le cas de texte
                // prévalorisé ou hérité
                if (typeClause == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE || typeClause == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE || typeClause == EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE || typeClause == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE || typeClause == EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU) { // pour les documents créés avec mis à jour

                    Element texteVariable = getElement(xmlClause, CLAUSE_TEXTE_VARIABLE);

                    LOG.debug("Récupération du texte variable qui vaut = {}", texteVariable);

                    if (null != texteVariable) {

                        clauseTexte.setTexteVariable(Util.encodeCaractere(texteVariable.getText()));

                        LOG.debug("Après définition, le texte variable vaut = {}", clauseTexte.getTexteVariable());

                        Attribute clauseTexteModifiable = texteVariable.getAttribute(CLAUSE_TEXTE_MODIFIABLE);
                        if (null != clauseTexteModifiable) {
                            LOG.debug("Modifiable? = {}", clauseTexteModifiable.getBooleanValue());

                            clauseTexte.setTexteModifiable(clauseTexteModifiable.getBooleanValue());
                        }

                        Attribute clauseTexteObligatoire = texteVariable.getAttribute(CLAUSE_TEXTE_OBLIGATOIRE);
                        if (null != clauseTexteObligatoire) {
                            LOG.debug("Obligatoire? = {}", clauseTexteObligatoire.getBooleanValue());

                            clauseTexte.setTexteObligatoire(clauseTexteObligatoire.getBooleanValue());
                        }

                        if (texteVariable.getAttribute(TAILLE) != null) {
                            clauseTexte.setTailleTexteVariable(texteVariable.getAttribute(TAILLE).getValue());
                        }
                    }


                    Element tableauRedactionElem = getElement(xmlClause, CLAUSE_TABLEAU_REDACTION);
                    if (tableauRedactionElem != null) {
                        TableauRedaction tableauRedaction = tableauRedactionXMLVersTableauxRedaction(tableauRedactionElem);
                        clauseTexte.setTableauRedaction(tableauRedaction);
                    }

                    Element idReferentielValeurTableau = getElement(xmlClause, ID_REFERENTIEL_VALEUR_TABLEAU);
                    if (idReferentielValeurTableau != null) {
                        int idReferentiel = Integer.parseInt(idReferentielValeurTableau.getValue());
                        clauseTexte.setIdRefValeurTableau(idReferentiel);
                    }
                }
                return clauseTexte;
            }
        } catch (DataConversionException e1) {
            LOG.error(e1.getMessage(), e1.fillInStackTrace());
            throw new TechnicalException(e1);
        }
    }

    /**
     * cette méthode permet de covertir une clause XML en un objet clause.
     *
     * @param dateModification date de modificatin de la clause.
     * @param xmlClause        la clause à convertir.
     * @return objet clause.
     * @throws TechnicalException erreur technique.
     */
    public static ClauseDocument elementClauseXMLVersClause(final Date dateModification, final Element xmlClause, final Integer idOrganisme) {
        // Conversion des ppt externes
        ClauseDocument clauseDocument = remplirPptExtClause(xmlClause);

        try {
            // Conversion des ppt internes
            clauseDocument.setId(xmlClause.getAttribute(CLAUSE_ID).getIntValue());

            if (xmlClause.getAttribute(ClAUSE_ACTIVE) != null) {
                if (xmlClause.getAttribute(ClAUSE_ACTIVE).getBooleanValue()) clauseDocument.setActif("1");
                else clauseDocument.setActif("0");
            }

            if (xmlClause.getAttribute(ClAUSE_REF) != null) {
                clauseDocument.setRef(xmlClause.getAttribute(ClAUSE_REF).getValue());
            }

            LOG.debug("Chargement de la clause de plus grand identifiant parmi ceux qui possèdent la même référence {}", clauseDocument.getRef());
            EpmTClauseAbstract clause = clauseFacadeGWT.chargerDernierClauseByRef(false, null, clauseDocument.getRef(), idOrganisme, false);

            if (clause == null) {
                LOG.warn("La clause {} est nulle", clauseDocument.getRef());
                return null; // on est dans le cas ou la clause ne fait plus partie du clausier publier, on considère qu'elle est supprimée

            } else {
                LOG.info("La clause {} n'est pas nulle", clauseDocument.getRef());
            }

            if (!xmlClause.getAttribute(ClAUSE_ETAT).getValue().equals(ClauseDocument.ETAT_CONDITIONNER)) {
                clauseDocument.setEtat(clause.getEtat());
                if (clause.getDateModification().after(dateModification))
                    clauseDocument.setEtat(ClauseDocument.ETAT_MODIFIER);
            } else {
                clauseDocument.setEtat(xmlClause.getAttribute(ClAUSE_ETAT).getValue());
            }

            clauseDocument.setType(Integer.parseInt(xmlClause.getAttribute(CLAUSE_TYPE).getValue()));

            clauseDocument.setTerminer(xmlClause.getAttribute(CLAUSE_VALIDE).getBooleanValue());

            if (xmlClause.getAttribute(CLAUSE_DUPLIQUE) != null)
                clauseDocument.setDuplique(xmlClause.getAttribute(CLAUSE_DUPLIQUE).getBooleanValue());

            // Récupération du texte fixe avant
            LOG.debug("Récupération du texte fixe avant");
            Element texteFixeAvant = getElement(xmlClause, CLAUSE_TEXTE_FIXE_AVANT);
            clauseDocument.setTexteFixeAvant(texteFixeAvant.getText());

            clauseDocument.setTexteFixeAvantAlaLigne(xmlClause.getAttribute(CLAUSE_TEXTE_FIXE_AVANT_A_LA_LIGNE).getBooleanValue());

            clauseDocument.setTexteFixeApresAlaLigne(xmlClause.getAttribute(CLAUSE_TEXTE_FIXE_APRES_A_LA_LIGNE).getBooleanValue());

            // Récupération du texte fixe après
            LOG.trace("Définition du texte fixe après");
            Element texteFixeApres = getElement(xmlClause, CLAUSE_TEXTE_FIXE_APRES);

            if (null != texteFixeApres) {
                clauseDocument.setTexteFixeApres(texteFixeApres.getText());
            }

            // Récupération du tableau redaction
            LOG.trace("Définition du tableau redaction");
            Element tableauRedactionElem = getElement(xmlClause, CLAUSE_TABLEAU_REDACTION);
            TableauRedaction tableauRedaction = tableauRedactionXMLVersTableauxRedaction(tableauRedactionElem);
            clauseDocument.setTableauRedaction(tableauRedaction);

            // Récupération de l'infobulle
            LOG.trace("Définition de l'infobulle");
            Element infoBulle = getElement(xmlClause, BULLE_NOM);
            clauseDocument.setInfoBulle(InfoBulleUtil.elementInfoBulleXMLVersInfobulle(infoBulle));

            LOG.debug("Résultat: {}", clauseDocument);

            return clauseDocument;
        } catch (Exception e) {
            String refClause = clauseDocument.getRef();

            LOG.error("Erreur lors de la conversion d'une clause XML en un objet clause {}", refClause, e);
            throw new TechnicalException(e);
        }

    }

    private static TableauRedaction tableauRedactionXMLVersTableauxRedaction(Element tableauRedactionXML) {

        TableauRedaction tableauRedaction = new TableauRedaction();
        if (tableauRedactionXML == null || tableauRedactionXML.getAttributes().isEmpty() && tableauRedactionXML.getChildren().isEmpty()) {
            return tableauRedaction;
        } else {

            if (tableauRedactionXML.getChild(TEXT_AVANT_TABLEAU) != null)
                tableauRedaction.setTextAvantTableau(tableauRedactionXML.getChild(TEXT_AVANT_TABLEAU).getValue());

            List<Element> listeDocumentsXLSXML = tableauRedactionXML.getChildren(DOCUMENT_XLS);
            for (Element documentXLSElem : listeDocumentsXLSXML) {
                DocumentXLS documentXLS = new DocumentXLS();
                tableauRedaction.getDocumentXLS().add(documentXLS);

                List<Element> ongletsXML = documentXLSElem.getChildren(ONGLET);
                for (Element ongletXML : ongletsXML) {
                    Onglet onglet = new Onglet();
                    documentXLS.getOnglet().add(onglet);

                    if (ongletXML.getChild(ATTRIBUTS) != null) {
                        Element attributXML = ongletXML.getChild(ATTRIBUTS);
                        TableauAttributs attributs = new TableauAttributs();
                        onglet.setAttributs(attributs);

                        List<Element> attributsXML = attributXML.getChildren(LARGEUR_COLONNE);
                        for (Element largeurColonneXML : attributsXML) {
                            AttributLargeurColonne largeurColoenne = new AttributLargeurColonne();
                            attributs.getLargeurColonne().add(largeurColoenne);

                            largeurColoenne.setColonne(Integer.valueOf(largeurColonneXML.getAttribute(COLONNE).getValue()));
                            largeurColoenne.setLargeur(Integer.valueOf(largeurColonneXML.getAttribute(LARGEUR).getValue()));
                        }
                    }

                    List<Element> regionsFusioneesXML = ongletXML.getChildren(REGION_FUSIONEE);
                    for (Element regionFusioneesXML : regionsFusioneesXML) {
                        RegionFusionnee region = new RegionFusionnee();
                        onglet.getRegionFusionnee().add(region);

                        region.setLigneDebut(Integer.valueOf((regionFusioneesXML.getAttribute(LIGNE_DEBUT).getValue())));
                        region.setLigneFin(Integer.valueOf((regionFusioneesXML.getAttribute(LIGNE_FIN).getValue())));
                        region.setColonneDebut(Integer.valueOf((regionFusioneesXML.getAttribute(COLONNE_DEBUT).getValue())));
                        region.setColonneFin(Integer.valueOf((regionFusioneesXML.getAttribute(COLONNE_FIN).getValue())));
                    }

                    List<Element> cellulesXML = ongletXML.getChildren(CELLULE);
                    for (Element celluleXML : cellulesXML) {
                        Cellule cellule = new Cellule();
                        onglet.getCellule().add(cellule);

                        Element coordonneXML = celluleXML.getChild(COORDONNEE);
                        CelluleCoordonnee coordoonee = new CelluleCoordonnee();
                        coordoonee.setLigne(Integer.valueOf(coordonneXML.getAttribute(LIGNE).getValue()));
                        coordoonee.setColonne(Integer.valueOf(coordonneXML.getAttribute(COLONNE).getValue()));
                        cellule.setCoordonnee(coordoonee);

                        Element valeurXML = celluleXML.getChild(VALEUR);
                        if (valeurXML.getText() != null) {
                            cellule.setValeur(valeurXML.getText());
                        }

                        Element styleXML = celluleXML.getChild(STYLE);
                        if (styleXML != null) {
                            CelluleStyle style = new CelluleStyle();
                            if (styleXML.getChild(REMPLISSAGE) != null) {
                                Element remplissageXML = styleXML.getChild(REMPLISSAGE);
                                if (remplissageXML.getAttribute(TYPE) != null && remplissageXML.getAttribute(ARRIERE_PLAN) != null) {
                                    CelluleStyleRemplissage remplissage = new CelluleStyleRemplissage();
                                    remplissage.setType(EnumerationRemplissage.valueOf(remplissageXML.getAttribute(TYPE).getValue()));
                                    remplissage.setArrierePlan(EnumerationCouleur.valueOf(remplissageXML.getAttribute(ARRIERE_PLAN).getValue()));
                                    style.setRemplissage(remplissage);
                                }

                            }

                            if (styleXML.getChild(FONT) != null) {
                                Element fontXML = styleXML.getChild(FONT);
                                CelluleStyleFont font = new CelluleStyleFont();
                                if (fontXML.getAttribute(GRAS) != null) {
                                    font.setGras(Boolean.valueOf(fontXML.getAttribute(GRAS).getValue()));
                                }

                                if (fontXML.getAttribute(ITALIC) != null) {
                                    font.setItalic(Boolean.valueOf(fontXML.getAttribute(ITALIC).getValue()));
                                }

                                if (fontXML.getAttribute(SOULIGNE) != null) {
                                    font.setSouligne(EnumerationFontSousligne.valueOf(fontXML.getAttribute(SOULIGNE).getValue()));
                                }

                                if (fontXML.getAttribute(COULEUR) != null) {
                                    font.setCouleur(EnumerationCouleur.valueOf(fontXML.getAttribute(COULEUR).getValue()));
                                }

                                style.setFont(font);
                            }

                            if (styleXML.getChild(BORDURE) != null) {
                                Element BordureXML = styleXML.getChild(BORDURE);
                                CelluleStyleBordure bordure = new CelluleStyleBordure();
                                if (BordureXML.getAttribute(HAUT) != null) {
                                    bordure.setHaut(EnumerationBordure.valueOf(BordureXML.getAttribute(HAUT).getValue()));
                                }
                                if (BordureXML.getAttribute(BAS) != null) {
                                    bordure.setBas(EnumerationBordure.valueOf(BordureXML.getAttribute(BAS).getValue()));
                                }
                                if (BordureXML.getAttribute(GAUCHE) != null) {
                                    bordure.setGauche(EnumerationBordure.valueOf(BordureXML.getAttribute(GAUCHE).getValue()));
                                }
                                if (BordureXML.getAttribute(DROITE) != null) {
                                    bordure.setDroite(EnumerationBordure.valueOf(BordureXML.getAttribute(DROITE).getValue()));
                                }
                                style.setBordure(bordure);

                            }
                            if (styleXML.getChild(ALIGNEMENT) != null) {
                                Element alignementXML = styleXML.getChild(ALIGNEMENT);
                                CelluleStyleAlignement alignement = new CelluleStyleAlignement();
                                if (alignementXML.getAttribute(ROTATION) != null) {
                                    alignement.setRotation(Integer.valueOf(alignementXML.getAttribute(ROTATION).getValue()));
                                }
                                if (alignementXML.getAttribute(HORIZONTAL) != null) {
                                    alignement.setHorizontal(EnumerationAlignementHorizontal.valueOf(alignementXML.getAttribute(HORIZONTAL).getValue()));
                                }
                                if (alignementXML.getAttribute(VERTICAL) != null) {
                                    alignement.setVertical(EnumerationAlignementVertical.valueOf(alignementXML.getAttribute(VERTICAL).getValue()));
                                }
                                style.setAlignement(alignement);
                            }
                            cellule.setStyle(style);
                        }
                    }
                }
            }
        }
        return tableauRedaction;
    }

    /**
     * Méthode de transformation d'un objet tableauDynamique vers un objet xml à
     * representant le tableau dynamique
     *
     * @return
     */
    private static Element tableauRedactionVersTableauRedactionXML(final TableauRedaction tableauRedaction) {
        if (tableauRedaction != null) {
            Element tableauRedactionXML = new Element(CLAUSE_TABLEAU_REDACTION);

            Element textAvant = new Element(TEXT_AVANT_TABLEAU);
            textAvant.setText(tableauRedaction.getTextAvantTableau());
            tableauRedactionXML.addContent(textAvant);

            List<Element> documentXLSList = new ArrayList<Element>();
            for (DocumentXLS documentXLS : tableauRedaction.getDocumentXLS()) {
                Element documentXLSXML = new Element(DOCUMENT_XLS);


                List<Element> ongletList = new ArrayList<Element>();
                for (Onglet onglet : documentXLS.getOnglet()) {
                    Element ongletXML = new Element(ONGLET);
                    ongletList.add(ongletXML);

                    if (onglet.getAttributs() != null && onglet.getAttributs().getLargeurColonne() != null && !onglet.getAttributs().getLargeurColonne().isEmpty()) {
                        Element attributsXML = new Element(ATTRIBUTS);

                        List<Element> attributsList = new ArrayList<Element>();
                        for (AttributLargeurColonne attribut : onglet.getAttributs().getLargeurColonne()) {
                            Element largeurColonneXML = new Element(LARGEUR_COLONNE);
                            attributsList.add(largeurColonneXML);

                            Attribute largeur = new Attribute(LARGEUR, String.valueOf(attribut.getLargeur()));
                            largeurColonneXML.setAttribute(largeur);

                            Attribute colonne = new Attribute(COLONNE, String.valueOf(attribut.getColonne()));
                            largeurColonneXML.setAttribute(colonne);

                        }
                        attributsXML.addContent(attributsList);
                        ongletXML.addContent(attributsXML);
                    }

                    List<Element> regionFusioneeList = new ArrayList<Element>();
                    for (RegionFusionnee regionFusionnee : onglet.getRegionFusionnee()) {
                        Element regionFusionneeXML = new Element(REGION_FUSIONEE);
                        regionFusioneeList.add(regionFusionneeXML);

                        Attribute ligneDebut = new Attribute(LIGNE_DEBUT, String.valueOf(regionFusionnee.getLigneDebut()));
                        regionFusionneeXML.setAttribute(ligneDebut);
                        Attribute ligneFin = new Attribute(LIGNE_FIN, String.valueOf(regionFusionnee.getLigneFin()));
                        regionFusionneeXML.setAttribute(ligneFin);
                        Attribute colonneDebut = new Attribute(COLONNE_DEBUT, String.valueOf(regionFusionnee.getColonneDebut()));
                        regionFusionneeXML.setAttribute(colonneDebut);
                        Attribute colonneFin = new Attribute(COLONNE_FIN, String.valueOf(regionFusionnee.getColonneFin()));
                        regionFusionneeXML.setAttribute(colonneFin);
                    }
                    ongletXML.addContent(regionFusioneeList);

                    List<Element> celluleList = new ArrayList<Element>();
                    for (Cellule cellule : onglet.getCellule()) {
                        Element celluleXML = new Element(CELLULE);
                        celluleList.add(celluleXML);

                        Element coordonneeXML = new Element(COORDONNEE);
                        Attribute colonne = new Attribute(COLONNE, String.valueOf(cellule.getCoordonnee().getColonne()));
                        coordonneeXML.setAttribute(colonne);
                        Attribute ligne = new Attribute(LIGNE, String.valueOf(cellule.getCoordonnee().getLigne()));
                        coordonneeXML.setAttribute(ligne);
                        celluleXML.addContent(coordonneeXML);

                        if (cellule.getValeur() != null) {
                            Element valeurXML = new Element(VALEUR);
                            valeurXML.setText(cellule.getValeur());
                            celluleXML.addContent(valeurXML);
                        }

                        Element styleXML = new Element(STYLE);
                        if (cellule.getStyle() != null) {
                            List<Element> styleList = new ArrayList<Element>();
                            // gestion du remplissage
                            if (cellule.getStyle().getRemplissage() != null) {
                                Element remplissageXML = new Element(REMPLISSAGE);

                                if (cellule.getStyle().getRemplissage().getType() != null) {
                                    Attribute type = new Attribute(TYPE, String.valueOf(cellule.getStyle().getRemplissage().getType()));
                                    remplissageXML.setAttribute(type);
                                }
                                if (cellule.getStyle().getRemplissage().getArrierePlan() != null) {
                                    Attribute arrierePlan = new Attribute(ARRIERE_PLAN, String.valueOf(cellule.getStyle().getRemplissage().getArrierePlan()));
                                    remplissageXML.setAttribute(arrierePlan);
                                }
                                styleList.add(remplissageXML);
                            }
                            // gestion des fonts

                            if (cellule.getStyle().getFont() != null) {
                                Element fontXML = new Element(FONT);
                                List<Attribute> fontAttributes = new ArrayList<Attribute>();
                                fontAttributes.add(new Attribute(GRAS, String.valueOf(cellule.getStyle().getFont().isGras())));
                                fontAttributes.add(new Attribute(ITALIC, String.valueOf(cellule.getStyle().getFont().isItalic())));
                                if (cellule.getStyle().getFont().getSouligne() != null)
                                    fontAttributes.add(new Attribute(SOULIGNE, String.valueOf(cellule.getStyle().getFont().getSouligne())));
                                if (cellule.getStyle().getFont().getCouleur() != null)
                                    fontAttributes.add(new Attribute(COULEUR, String.valueOf(cellule.getStyle().getFont().getCouleur())));

                                fontXML.setAttributes(fontAttributes);
                                styleList.add(fontXML);
                            }
                            // getsion des bordures
                            if (cellule.getStyle().getBordure() != null) {
                                Element bordureXML = new Element(BORDURE);
                                List<Attribute> bordureAttributes = new ArrayList<Attribute>();

                                if (cellule.getStyle().getBordure().getHaut() != null)
                                    bordureAttributes.add(new Attribute(HAUT, String.valueOf(cellule.getStyle().getBordure().getHaut())));
                                if (cellule.getStyle().getBordure().getBas() != null)
                                    bordureAttributes.add(new Attribute(BAS, String.valueOf(cellule.getStyle().getBordure().getBas())));
                                if (cellule.getStyle().getBordure().getGauche() != null)
                                    bordureAttributes.add(new Attribute(GAUCHE, String.valueOf(cellule.getStyle().getBordure().getGauche())));
                                if (cellule.getStyle().getBordure().getDroite() != null)
                                    bordureAttributes.add(new Attribute(DROITE, String.valueOf(cellule.getStyle().getBordure().getDroite())));

                                bordureXML.setAttributes(bordureAttributes);
                                styleList.add(bordureXML);
                            }
                            // gestiondes alignements
                            if (cellule.getStyle().getAlignement() != null) {
                                Element alignementXML = new Element(ALIGNEMENT);
                                List<Attribute> alignementAttributes = new ArrayList<Attribute>();

                                if (cellule.getStyle().getAlignement().getRotation() != null)
                                    alignementAttributes.add(new Attribute(ROTATION, String.valueOf(cellule.getStyle().getAlignement().getRotation())));
                                if (cellule.getStyle().getAlignement().getHorizontal() != null)
                                    alignementAttributes.add(new Attribute(HORIZONTAL, String.valueOf(cellule.getStyle().getAlignement().getHorizontal())));
                                if (cellule.getStyle().getAlignement().getVertical() != null)
                                    alignementAttributes.add(new Attribute(VERTICAL, String.valueOf(cellule.getStyle().getAlignement().getVertical())));

                                alignementXML.setAttributes(alignementAttributes);
                                styleList.add(alignementXML);
                            }
                            styleXML.addContent(styleList);
                        }
                        celluleXML.addContent(styleXML);
                    }
                    ongletXML.addContent(celluleList);
                }
                documentXLSXML.addContent(ongletList);
                documentXLSList.add(documentXLSXML);
            }
            tableauRedactionXML.addContent(documentXLSList);
            return tableauRedactionXML;
        }
        return null;
    }

    /**
     * Cette méthode permet la conversion d'une clause d'un document vers une
     * clause document XML.
     *
     * @param clauseDocument à convertir.
     * @return clause XML.
     */
    public static Element versJdom(final ClauseDocument clauseDocument) throws TechnicalException {

        LOG.debug("Construire l'arbre JDOM de la clause");

        Element clauseElement = new Element(CLAUSE_NOM);
        if (clauseDocument != null) {

            // Les attributs
            LOG.debug("Initialiser les attributs de la clause");
            Attribute id = new Attribute(CLAUSE_ID, Integer.toString(clauseDocument.getId()));
            clauseElement.setAttribute(id);

            if (clauseDocument.getActif().equals(ClauseDocument.ACTIF)) {
                Attribute active = new Attribute(ClAUSE_ACTIVE, "true");
                clauseElement.setAttribute(active);
            } else {
                Attribute active = new Attribute(ClAUSE_ACTIVE, Boolean.toString(false));
                clauseElement.setAttribute(active);
            }

            if (clauseDocument.getRef() != null) {
                Attribute ref = new Attribute(ClAUSE_REF, clauseDocument.getRef());
                clauseElement.setAttribute(ref);
            }
            if (clauseDocument.getEtat() != null) {
                Attribute etat = new Attribute(ClAUSE_ETAT, clauseDocument.getEtat());
                clauseElement.setAttribute(etat);
            }
            Attribute type = new Attribute(CLAUSE_TYPE, String.valueOf(clauseDocument.getType()));
            clauseElement.setAttribute(type);
            Attribute clauseValide = new Attribute(CLAUSE_VALIDE, Boolean.toString(clauseDocument.isTerminer()));
            clauseElement.setAttribute(clauseValide);

            Attribute clauseDuplique = new Attribute(CLAUSE_DUPLIQUE, Boolean.toString(clauseDocument.isDuplique()));
            clauseElement.setAttribute(clauseDuplique);

            Attribute avantALaLigne = new Attribute(CLAUSE_TEXTE_FIXE_AVANT_A_LA_LIGNE, Boolean.toString(clauseDocument.isTexteFixeAvantAlaLigne()));
            clauseElement.setAttribute(avantALaLigne);

            Attribute apresALaLigne = new Attribute(CLAUSE_TEXTE_FIXE_APRES_A_LA_LIGNE, Boolean.toString(clauseDocument.isTexteFixeApresAlaLigne()));
            clauseElement.setAttribute(apresALaLigne);

            // Récupération du texte fixe avant
            LOG.debug("Récupération du texte fixe avant");
            Element texteFixeAvant = new Element(CLAUSE_TEXTE_FIXE_AVANT);
            texteFixeAvant.setText(clauseDocument.getTexteFixeAvant());
            clauseElement.addContent(CLAUSE_POS_TEXTE_FIXE_AVANT, texteFixeAvant);

            // Récupération du texte fixe après
            LOG.debug("Récupération du texte fixe après");
            Element texteFixeApres = new Element(CLAUSE_TEXTE_FIXE_APRES);
            texteFixeApres.setText(clauseDocument.getTexteFixeApres());
            clauseElement.addContent(CLAUSE_POS_TEXTE_FIXE_APRES, texteFixeApres);

            // Récupération de l'infobulle
            LOG.debug("Récupération de l'infobulle");
            clauseElement.addContent(CLAUSE_POS_BULLE, InfoBulleUtil.versJdom(clauseDocument.getInfoBulle()));

            if (clauseDocument.getType() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE ||          // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    clauseDocument.getType() == EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU) {            // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                LOG.warn("WARNING: Les clauses de types Texte Héritée et Tableau sont obsoletes; Utilisez le type Valeur Héritée");
                clauseDocument.setType(EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE);
            }

            // Etudier les cas
            LOG.debug("Récupération des ppt externes de la clause");
            if (clauseDocument instanceof ClauseListe) {
                // Récupération du texte variable
                LOG.debug("Récupération des formulations");
                ClauseListe clauseListe = (ClauseListe) clauseDocument;
                Attribute modifiable = new Attribute(CLAUSE_FORMULATIONS_MODIFIABLE, Boolean.toString(clauseListe.isFormulationsModifiable()));
                clauseElement.setAttribute(modifiable);

                List listeFormulations = clauseListe.getFormulations();
                if (listeFormulations != null && listeFormulations.size() > 0) {
                    Element formulaireListe = new Element(CLAUSE_FORMULAIRE_LISTE_TYPE);
                    Attribute exclusif = new Attribute(EXCLUSIF, clauseListe.getType() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF ? "true" : Boolean.toString(false));
                    formulaireListe.setAttribute(exclusif);
                    for (int i = 0; i < listeFormulations.size(); i++) {
                        Formulation formulationInstance = (Formulation) listeFormulations.get(i);
                        Element formulation = new Element(CLAUSE_FORMULATION);
                        Attribute precoche = new Attribute(PRECOCHE, formulationInstance.isPrecochee() ? "true" : Boolean.toString(false));
                        formulation.setAttribute(precoche);
                        Attribute taille = new Attribute(TAILLE, String.valueOf(formulationInstance.getNbrCaraMax()));
                        formulation.setAttribute(taille);
                        formulation.setText(formulationInstance.getLibelle());
                        formulaireListe.addContent(i, formulation);
                    }
                    clauseElement.addContent(CLAUSE_POS_FORMULAIRE_LISTE_TYPE, formulaireListe);
                    LOG.debug("Les formulations sont ajoutés à la clause");
                } else {
                    LOG.warn("La liste de formulation de clause est null pour la clause : " + clauseListe.getRef());
                }
            } else if (clauseDocument.getType() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE || clauseDocument.getType() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE || clauseDocument.getType() == ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE) {
                // Récupération du texte variable
                LOG.debug("Récupération du texte variable ");
                ClauseTexte clauseTexte = (ClauseTexte) clauseDocument;
                Element texteVariable = new Element(CLAUSE_TEXTE_VARIABLE);
                texteVariable.setText(clauseTexte.getTexteVariable());
                Attribute texteModifiable = new Attribute(CLAUSE_TEXTE_MODIFIABLE, Boolean.toString(clauseTexte.isTexteModifiable()));
                texteVariable.setAttribute(texteModifiable);
                Attribute texteObligatoire = new Attribute(CLAUSE_TEXTE_OBLIGATOIRE, Boolean.toString(clauseTexte.isTexteObligatoire()));
                texteVariable.setAttribute(texteObligatoire);
                Attribute taille = new Attribute(TAILLE, clauseTexte.getTailleTexteVariable());
                texteVariable.setAttribute(taille);
                clauseElement.addContent(CLAUSE_POS_TEXTE_VARIABLE, texteVariable);

                if (clauseTexte.getIdRefValeurTableau() != 0) {
                    Element idReferentielValeurTableau = new Element(ID_REFERENTIEL_VALEUR_TABLEAU);
                    idReferentielValeurTableau.setText(String.valueOf(clauseTexte.getIdRefValeurTableau()));
                    clauseElement.addContent(idReferentielValeurTableau);
                }

                if (clauseTexte.getTableauRedaction() != null) {
                    Element tableauRedactionElement = tableauRedactionVersTableauRedactionXML(clauseTexte.getTableauRedaction());
                    if (tableauRedactionElement != null) clauseElement.addContent(tableauRedactionElement);
                }
            }
        }
        return clauseElement;
    }

    /**
     * cette méthode permet de convertir une clasue canevas vers une clause
     * document.
     *
     * @param clauseCanevas    la clause à convertir.
     * @param consultation     EpmTConsultation (peut être null dans le cas de la duplication de document).
     * @param utilisateur      utilisateur
     * @param idLot            identifiant du lot de la consultation
     * @param rechercherClause si true alors on récupère l'objet {@link EpmTClause} dont l'id est celui du clauseCanevas
     * @return objet clause.
     */
    public static ClauseDocument clauseCanevasToClauseDocument(final Clause clauseCanevas, final EpmTConsultation consultation, final EpmTUtilisateur utilisateur, final int idLot, ResourceBundleMessageSource messageSource, final RechercheClauseEnum rechercherClause) {
        EpmTClauseAbstract epmTClause = null;

        if (rechercherClause.equals(RechercheClauseEnum.CLAUSE_CANEVAS) || rechercherClause.equals(RechercheClauseEnum.CLAUSE_DOCUMENT)) {
            if (clauseCanevas.getIdPublication() == null)
                epmTClause = clauseFacadeGWT.charger(clauseCanevas.getIdClause());
            else epmTClause = clauseFacadeGWT.charger(clauseCanevas.getIdClause(), clauseCanevas.getIdPublication(), utilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        } else if (rechercherClause.equals(RechercheClauseEnum.DERNIERE_CLAUSE)) {
            LOG.debug("Chargement de la clause de plus grand identifiant parmi ceux qui possèdent la même référence");
            epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(false, null, clauseCanevas.getReference(), utilisateur.getIdOrganisme(), false);

            if (epmTClause == null)
                return null; // on est dans le cas ou la clause ne fait plus partie du clausier publier, on considère qu'elle est supprimée
        }

        boolean surcharge = false;
        List<EpmTRoleClauseAbstract> listeRoles = new ArrayList<>();
        if (epmTClause.isClauseEditeur()) {// clause éditeur

            ClauseCritere clauseCritere = new ClauseCritere(utilisateur.getEpmTRefOrganisme().getPlateformeUuid());
            clauseCritere.setIdClauseOrigine(epmTClause.getIdClause());
            clauseCritere.setIdOrganisme(utilisateur.getIdOrganisme());
            clauseCritere.setSurchargeActif(true);
            List<? extends EpmTClauseAbstract> listeEpmTClause = clauseFacadeGWT.chercherParCriteres(clauseCritere);
            if (listeEpmTClause != null && !listeEpmTClause.isEmpty()) {

                EpmTClauseAbstract clauseSurcharge = listeEpmTClause.get(0);
                EpmTClauseAbstract clauseEditeur = epmTClause;

                try {
                    epmTClause = clauseSurcharge.clone();

                    epmTClause.setReference(clauseEditeur.getReference());
                    epmTClause.setEpmTRefTypeClause(clauseEditeur.getEpmTRefTypeClause());
                    epmTClause.setEpmTRefTypeDocument(clauseEditeur.getEpmTRefTypeDocument());
                    epmTClause.setEpmTRefProcedure(clauseEditeur.getEpmTRefProcedure());
                    epmTClause.setIdNaturePrestation(clauseEditeur.getIdNaturePrestation());
                    epmTClause.setEpmTRefThemeClause(clauseEditeur.getEpmTRefThemeClause());
                    epmTClause.setActif(clauseEditeur.isActif());
                    epmTClause.setCompatibleEntiteAdjudicatrice(clauseEditeur.isCompatibleEntiteAdjudicatrice());

                    List<EpmTClausePotentiellementConditionnee> clausePotCondClone = new ArrayList<>();
                    Set<? extends EpmTClausePotentiellementConditionneeAbstract> clausePotCond = clauseEditeur.getEpmTClausePotentiellementConditionnees();
                    if (clausePotCond != null) {
                        for (EpmTClausePotentiellementConditionneeAbstract item : clausePotCond) {
                            EpmTClausePotentiellementConditionnee clausePotCondTmp = new EpmTClausePotentiellementConditionnee();
                            clausePotCondTmp.setEpmTClause(epmTClause);
                            clausePotCondTmp.setEpmTRefPotentiellementConditionnee(item.getEpmTRefPotentiellementConditionnee());

                            List<EpmTClauseValeurPotentiellementConditionnee> clauseValPotCondClone = new ArrayList<>();
                            Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> clauseValPotCond = item.getEpmTClauseValeurPotentiellementConditionnees();
                            if (clauseValPotCond != null) {
                                for (EpmTClauseValeurPotentiellementConditionneeAbstract i : clauseValPotCond) {
                                    EpmTClauseValeurPotentiellementConditionnee clauseValPotCondTmp = new EpmTClauseValeurPotentiellementConditionnee();
                                    clauseValPotCondTmp.setEpmTClausePotentiellementConditionnee(clausePotCondTmp);
                                    clauseValPotCondTmp.setValeur(i.getValeur());

                                    clauseValPotCondClone.add(clauseValPotCondTmp);
                                }
                            }
                            clausePotCondTmp.setEpmTClauseValeurPotentiellementConditionnees(new HashSet<>(clauseValPotCondClone));

                            clausePotCondClone.add(clausePotCondTmp);
                        }
                    }
                    epmTClause.setEpmTClausePotentiellementConditionnees(new HashSet<>(clausePotCondClone));

                    listeRoles = (List<EpmTRoleClauseAbstract>) clauseFacadeGWT.recupererRolesClause(utilisateur, clauseSurcharge, false);
                    surcharge = true;

                } catch (CloneNotSupportedException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    throw new TechnicalException(e.fillInStackTrace());
                }

            }

        }

        if (!epmTClause.isActif() || epmTClause.getEtat().equals(EpmTClause.ETAT_SUPPRIMER)) {
            return null;
        }

        if (!surcharge)
            listeRoles = (List<EpmTRoleClauseAbstract>) clauseFacadeGWT.recupererRolesClause(utilisateur, epmTClause, false);

        boolean conditionnement = true;
        for (EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee : epmTClause.getEpmTClausePotentiellementConditionnees()) {

            EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee = epmTClausePotentiellementConditionnee.getEpmTRefPotentiellementConditionnee();
            if (epmTRefPotentiellementConditionnee != null) {
                clauseCanevas.setPotentiellementConditionnee(true);
                conditionnement = isConditionnementVerifie(consultation, idLot, epmTClausePotentiellementConditionnee, epmTRefPotentiellementConditionnee);
            }

            if (!conditionnement) break;
        }

        LOG.info("Conditionnement de la clause {}? {}", epmTClause, conditionnement);
        switch (epmTClause.getEpmTRefTypeClause().getId()) {
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_FIXE:
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
            case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                return clauseCanevasToClauseTexte(clauseCanevas, epmTClause, consultation, listeRoles, idLot, conditionnement, messageSource, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF:
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF:
                return clauseCanevasToClauseListe(clauseCanevas, epmTClause, listeRoles, conditionnement);
            default:
                throw new TechnicalException("Ce type de clause n'existe pas: " + epmTClause.getEpmTRefTypeClause().getId());
        }
    }

    public static boolean isConditionnementVerifie(EpmTConsultation consultation, int idLot, EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee, EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee) {
        boolean conditionnement = true;
        String cheminValeur = epmTRefPotentiellementConditionnee.getMethodeConsultation();

        LOG.info("Methode de consultation = {}", cheminValeur);

        if (cheminValeur != null) {
            String[] argumentTableau = cheminValeur.split(",|\\(|\\)");
            Object[] arguments = null;
            List<Integer> argumentsValeurPotCondList = new ArrayList<Integer>();
            if (argumentTableau.length > 1) {
                arguments = new Object[argumentTableau.length - 1];
            }
            int ouvrante = cheminValeur.indexOf("(");
            String methode = cheminValeur.substring(0, ouvrante);
            int index = 0;
            int valeurIdx = -1;
            List<EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotCondSet = new ArrayList<>(
                    epmTClausePotentiellementConditionnee.getEpmTClauseValeurPotentiellementConditionnees());

            if (epmTRefPotentiellementConditionnee.isProcedurePassation()) {
                LOG.trace("{} EST une procedure de passation", epmTRefPotentiellementConditionnee);
                Integer idOrganismeConsultation = consultation.getIdOrganisme();

                List<EpmTClauseValeurPotentiellementConditionneeAbstract> valeursConditionneeProcedurepassation = new ArrayList<>();
                for (EpmTClauseValeurPotentiellementConditionneeAbstract item : epmTClauseValeurPotCondSet) {
                    Integer idProcedurePassation =
                            referentielFacade.getIdProcedurePassationCorrespondantDansAutreOrganisme(item.getValeur(), idOrganismeConsultation);
                    EpmTClauseValeurPotentiellementConditionnee valeurConditionneeProcedurepassation = new EpmTClauseValeurPotentiellementConditionnee();
                    valeurConditionneeProcedurepassation.setValeur(idProcedurePassation);
                    valeursConditionneeProcedurepassation.add(valeurConditionneeProcedurepassation);
                }
                epmTClauseValeurPotCondSet = valeursConditionneeProcedurepassation;
            } else {
                LOG.trace("{} n'est pas une procedure de passation", epmTRefPotentiellementConditionnee);
            }

            for (int i = 1; i < argumentTableau.length; i++) {
                if (argumentTableau[i].equals("lot")) {
                    arguments[index] = idLot;
                } else if (argumentTableau[i].equals("id") && (epmTClauseValeurPotCondSet != null)) {
                    valeurIdx = index;
                    for (EpmTClauseValeurPotentiellementConditionneeAbstract item : epmTClauseValeurPotCondSet) {
                        argumentsValeurPotCondList.add(item.getValeur());
                    }
                } else if (argumentTableau[i].contains("[")) {
                    String clef = argumentTableau[i].substring(1, argumentTableau[i].length() - 1);
                    arguments[index] = clef;
                }
                index++;
            }

            if (consultation != null) {
                try {
                    List<Object> resultatList = new ArrayList<>();
                    String[] methodes = methode.split("\\.");
                    Object resultatTmp = new ConditionnementConsultation(consultation);
                    ((ConditionnementConsultation) resultatTmp).setConsultationService(consultationService);
                    for (int i = 0; i < methodes.length; i++) {
                        methode = Util.creationMethode(methodes[i]);

                        if (valeurIdx != -1) {
                            if (i == (methodes.length - 1)) {
                                for (Integer item : argumentsValeurPotCondList) {
                                    arguments[valeurIdx] = item;
                                    resultatList.add(Util.lancerMethode(resultatTmp, arguments, methode));
                                }
                            } else {
                                resultatTmp = Util.lancerMethode(resultatTmp, arguments, methode);
                            }
                        } else {
                            if (i == (methodes.length - 1)) {
                                resultatList.add(Util.lancerMethode(resultatTmp, arguments, methode));
                            } else {
                                resultatTmp = Util.lancerMethode(resultatTmp, arguments, methode);
                            }
                        }
                    }


                    LOG.info("Liste des résultat = {}", resultatList);


                    // conditionnement est true s'il y a au moins une condition correspondante
                    conditionnement = false;
                    for (Object resultat : resultatList) {
                        if (resultat instanceof Integer) {
                            if ((containsValue(epmTClauseValeurPotCondSet, 0) || containsValue(epmTClauseValeurPotCondSet, (Integer) resultat))) {
                                conditionnement = true;
                            }

                        } else if (resultat instanceof String) {
                            int id = ((String) resultat).equalsIgnoreCase("oui") ? 2 : 1;

                            if (containsValue(epmTClauseValeurPotCondSet, id)) {
                                conditionnement = true;
                            }

                        } else if (resultat instanceof Boolean) {
                            if ((boolean) resultat)
                                conditionnement = true;
                        } else if (resultat instanceof List) {
                            List<Integer> resultats = (List<Integer>) resultat;
                            conditionnement = true;
                            for (EpmTClauseValeurPotentiellementConditionneeAbstract conditionneeAbstract : epmTClauseValeurPotCondSet) {
                                if (resultats.stream().noneMatch(c -> c.equals(conditionneeAbstract.getValeur()))) {
                                    LOG.info("Aucun résultat parmi {} ne correspond à {}", resultats, conditionneeAbstract.getValeur());

                                    conditionnement = false;
                                }
                            }
                        }
                    }
                } catch (SecurityException | IllegalArgumentException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    throw new TechnicalException(e.fillInStackTrace());
                }
            }
        }
        return conditionnement;
    }




    /**
     * cette méthode permet de convertir une clasue canevas vers une clause
     * document pour l'export du modéle de canevas canevas.
     *
     * @param clauseCanevas la clause à convertir.
     * @param utilisateur   utilisateur
     * @return objet clause.
     * @throws NonTrouveException clause non trouvée.
     * @throws TechnicalException erreur technique.
     */
    public static ClauseDocument clauseCanevasToClauseDocument(final Clause clauseCanevas, final EpmTUtilisateur utilisateur, final ResourceBundleMessageSource messageSource, final boolean editeur) throws TechnicalException {

        EpmTClauseAbstract epmTClause;
        //try resolve pb idPub null
        if (clauseCanevas.getIdPublication() != null) {
            epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(false, clauseCanevas.getIdPublication(), clauseCanevas.getReference(), utilisateur.getIdOrganisme(), false);
        } else {
            epmTClause = clauseFacadeGWT.charger(clauseCanevas.getIdClause());
        }


        if (clauseCanevas.getIdPublication() != null && epmTClause == null) {
            ClausePubCritere clausePubCritere = new ClausePubCritere(utilisateur.getEpmTRefOrganisme().getPlateformeUuid());
            clausePubCritere.setIdClause(clauseCanevas.getIdClause());
            clausePubCritere.setIdPublication(clauseCanevas.getIdPublication());
            epmTClause = redactionService.chercherUniqueEpmTObject(0, clausePubCritere);
        }

        if (epmTClause == null)
            return null; // on est dans le cas ou la clause ne fait plus partie du clausier publier, on considère qu'elle est supprimée

        List<EpmTRoleClauseAbstract> listeRoles = (List<EpmTRoleClauseAbstract>) clauseFacadeGWT.recupererRolesClause(utilisateur, epmTClause, editeur);
        switch (epmTClause.getEpmTRefTypeClause().getId()) {
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_FIXE:
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
            case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                return clauseCanevasToClauseTexte(clauseCanevas, epmTClause, listeRoles, messageSource, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF:
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF:
                return clauseCanevasToClauseListe(clauseCanevas, epmTClause, listeRoles, true);
            default:
                throw new TechnicalException("Ce type de clause n'existe pas: " + epmTClause.getEpmTRefTypeClause().getId());
        }
    }

    /**
     * cette méthode permet de convertir une clause canevas vers une clause de
     * type liste.
     *
     * @param clauseCanevas   la clause à convertir.
     * @param epmTClause      objet empTClause.
     * @param listeRoles      Utilisateur
     * @param conditionnement Utilisateur
     * @return clause convertie.
     * @throws TechnicalException erreur technique
     */
    private static ClauseListe clauseCanevasToClauseListe(final Clause clauseCanevas, final EpmTClauseAbstract epmTClause, final List<EpmTRoleClauseAbstract> listeRoles, final boolean conditionnement) throws TechnicalException {
        ClauseListe clauseListe = new ClauseListe();
        clauseListe.setId(clauseCanevas.getIdClause()); // TODO: NIKO 2018-02.00.02
        clauseListe.setEtat(clauseCanevas.getEtat());
        clauseListe.setActif(clauseCanevas.isActif() ? "1" : "0");
        if (!conditionnement && !clauseCanevas.getEtat().equals(Clause.ETAT_SUPPRIMER)) {

            LOG.info("Clause potentiellement conditionné KO lors de la conversion en liste");
            clauseListe.setEtat(ClauseDocument.ETAT_CONDITIONNER);
        }
        clauseListe.setRef(clauseCanevas.getReference());
        clauseListe.setInfoBulle(clauseCanevas.getInfoBulle());
        clauseListe.setType(epmTClause.getEpmTRefTypeClause().getId());
        clauseListe.setFormulationsModifiable(epmTClause.isFormulationModifiable());
        clauseListe.setTexteFixeAvant(epmTClause.getTexteFixeAvant());
        clauseListe.setTexteFixeApresAlaLigne(epmTClause.isSautLigneTexteApres());
        clauseListe.setTexteFixeAvantAlaLigne(epmTClause.isSautLigneTexteAvant());
        clauseListe.setTexteFixeApres(epmTClause.getTexteFixeApres());
        if (listeRoles != null) {
            ArrayList<Formulation> listeDesFormulation = new ArrayList<>();
            for (EpmTRoleClauseAbstract epmTRoleClause : listeRoles) {
                Formulation uneFormulation = new Formulation();
                // FIXME on gére que les valeurs par defaut
                // if (epmTRoleClause.getIdAgent() == null && epmTRoleClause.getIdDirection() == null) {
                uneFormulation.setPrecochee(epmTRoleClause.isPrecochee());
                uneFormulation.setLibelle(epmTRoleClause.getValeurDefaut());
                if (epmTRoleClause.getNumFormulation() != null)
                    uneFormulation.setNumFormulation(epmTRoleClause.getNumFormulation());
                if (epmTRoleClause.getNombreCarateresMax() != null)
                    uneFormulation.setNbrCaraMax(epmTRoleClause.getNombreCarateresMax());
                listeDesFormulation.add(uneFormulation);
                // }
            }
            clauseListe.setFormulations(listeDesFormulation);
        }
        return clauseListe;
    }

    /**
     * cette méthode permet de convertir une clause canevas vers une clause de
     * type liste dans le cas de l'export de canevas.
     *
     * @param clauseCanevas la clause à convertir.
     * @param epmTClause    objet empTClause.
     * @param listeRoles    listeRoles
     * @return clause convertie.
     * @throws TechnicalException erreur technique
     */
    private static ClauseListe clauseCanevasToClauseListePourExportCanevas(final Clause clauseCanevas, final EpmTClauseAbstract epmTClause, final List<EpmTRoleClauseAbstract> listeRoles, final ResourceBundleMessageSource messageSource, EpmTUtilisateur utilisateur) throws TechnicalException {

        ClauseListe clauseListe = clauseCanevasToClauseListe(clauseCanevas, epmTClause, listeRoles, true);
        String symboleFinIntervale = "";
        String symboleDebutIntervale = "";
        String symboleSelectionne = "";
        String symboleNonSelectionne = "";
        if (messageSource != null) {
            if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                symboleSelectionne = messageSource.getMessage("canevas.export.radioBouton.nonSelectionne", null, Locale.FRENCH);
                symboleNonSelectionne = messageSource.getMessage("canevas.export.radioBouton.nonSelectionne", null, Locale.FRENCH);
            } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF) {
                symboleSelectionne = messageSource.getMessage("canevas.export.checkbox.selectionne", null, Locale.FRENCH);
                symboleNonSelectionne = messageSource.getMessage("canevas.export.checkbox.nonSelectionne", null, Locale.FRENCH);
            }
            // on itére sur les formulations pour les rendre précochée et ajouter
            // les caractéres spéciaux pour affichage.
            symboleDebutIntervale = messageSource.getMessage("canevas.export.debutIntervale", null, Locale.FRENCH);
            symboleFinIntervale = messageSource.getMessage("canevas.export.finIntervale", null, Locale.FRENCH);

        }

        if (clauseListe.getFormulations() != null) {
            Iterator it = clauseListe.getFormulations().iterator();
            while (it.hasNext()) {
                Formulation uneFormulation = (Formulation) it.next();

                if (uneFormulation.isPrecochee()) {
                    uneFormulation.setLibelle("\r" + symboleDebutIntervale + uneFormulation.getLibelle() + symboleFinIntervale);
                } else {
                    uneFormulation.setLibelle("\r" + symboleDebutIntervale + uneFormulation.getLibelle() + symboleFinIntervale);
                }
            }
        } else {
            LOG.warn("La liste de formulation de clause est null pour la clause : " + epmTClause.getReference());
        }

        setConditionnement(epmTClause, clauseListe, utilisateur);
        return clauseListe;
    }

    /**
     * cette méthode permet de convertir une clause canevas vers une clause de
     * type texte pour l'export du canevas.
     *
     * @param clauseCanevas la clause à convertir.
     * @param epmTClause    l'objet epmTClause.
     * @param listeRoles    identifiant de la consultation.
     * @return la clause convertie.
     * @throws TechnicalException erreur technique.
     */
    private static ClauseTexte clauseCanevasToClauseTexte(final Clause clauseCanevas, final EpmTClauseAbstract epmTClause, final List<EpmTRoleClauseAbstract> listeRoles, final ResourceBundleMessageSource messageSource, EpmTUtilisateur utilisateur) throws TechnicalException {

        LOG.debug("Aller de clauseCanevas au ClauseTexte");
        ClauseTexte clauseTexte = new ClauseTexte();
        clauseTexte.setId(clauseCanevas.getIdClause()); // TODO: NIKO 2018-02.00.02
        clauseTexte.setRef(clauseCanevas.getReference());
        clauseTexte.setEtat(clauseCanevas.getEtat());
        clauseTexte.setActif(clauseCanevas.isActif() ? "1" : "0");

        setConditionnement(epmTClause, clauseTexte, utilisateur);
        clauseTexte.setInfoBulle(clauseCanevas.getInfoBulle());
        clauseTexte.setType(epmTClause.getEpmTRefTypeClause().getId());
        clauseTexte.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(epmTClause.getTexteFixeAvant()));
        clauseTexte.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(epmTClause.getTexteFixeApres()));
        clauseTexte.setTexteFixeApresAlaLigne(epmTClause.isSautLigneTexteApres());
        clauseTexte.setTexteFixeAvantAlaLigne(epmTClause.isSautLigneTexteAvant());
        clauseTexte.setTexteModifiable(true);
        clauseTexte.setTexteObligatoire(false);
        if (listeRoles == null) return clauseTexte;
        String symboleDebutIntervale = messageSource.getMessage("canevas.export.debutIntervale", null, Locale.FRENCH);
        String symboleFinIntervale = messageSource.getMessage("canevas.export.finIntervale", null, Locale.FRENCH);
        for (EpmTRoleClauseAbstract epmTRoleClause : listeRoles) {
            // FIXME on gére que les valeurs par defaut
            // if (epmTRoleClause.getIdAgent() == null && epmTRoleClause.getIdDirection() == null) {
            switch (clauseTexte.getType()) {
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
                    clauseTexte.setTexteVariable(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(symboleDebutIntervale + epmTRoleClause.getValeurDefaut() + symboleFinIntervale));
                    clauseTexte.setTailleTexteVariable(String.valueOf(epmTRoleClause.getNombreCarateresMax()));
                    break;
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
                    if (epmTRoleClause.getChampObligatoire() != null && epmTRoleClause.getChampObligatoire().equals("1")) {
                        clauseTexte.setTexteObligatoire(true);
                        clauseTexte.setTailleTexteVariable(String.valueOf(epmTRoleClause.getNombreCarateresMax()));
                        clauseTexte.setTexteVariable(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(symboleDebutIntervale + symboleDebutIntervale + "  " + symboleFinIntervale + symboleFinIntervale));
                    }
                    break;
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new TechnicalException("La clause de type Texte Héritée est obsolete; Utilisez le type Valeur Héritée");
                case EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new TechnicalException("La clause de type Tableau est obsolete; Utilisez le type Valeur Héritée");
                case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                    try {
                        clauseValeurHerite(clauseTexte, epmTRoleClause);
                    } catch (NonTrouveException e) {
                        LOG.error(e.getMessage(), e.fillInStackTrace());
                        throw new TechnicalException(e);
                    }
                    break;
            }
        }
        return clauseTexte;
    }

    private static void setConditionnement(EpmTClauseAbstract epmTClause, ClauseDocument clauseTexte, EpmTUtilisateur utilisateur) throws TechnicalException {

        clauseTexte.setConditionee(false);

        for (EpmTClausePotentiellementConditionneeAbstract epmTClausePotentiellementConditionnee : epmTClause.getEpmTClausePotentiellementConditionnees()) {
            EpmTRefPotentiellementConditionnee potentiellementConditionnee = epmTClausePotentiellementConditionnee.getEpmTRefPotentiellementConditionnee();
            if (potentiellementConditionnee != null) {

                if (clauseTexte.getTypeConditionnementList() == null)
                    clauseTexte.setTypeConditionnementList(new ArrayList<String>());
                clauseTexte.getTypeConditionnementList().add(potentiellementConditionnee.getLibelle());

                if (clauseTexte.getValeurConditionnementList() == null)
                    clauseTexte.setValeurConditionnementList(new ArrayList<List<String>>());
                clauseTexte.getValeurConditionnementList().add(new ArrayList<String>());

                List<EpmTClauseValeurPotentiellementConditionneeAbstract> valeurConditionneeList = new ArrayList<>(epmTClausePotentiellementConditionnee.getEpmTClauseValeurPotentiellementConditionnees());

                if (valeurConditionneeList != null && !valeurConditionneeList.isEmpty()) {

                    EpmTClauseValeurPotentiellementConditionneeAbstract valeurConditionnee = valeurConditionneeList.get(0);
                    if (valeurConditionnee.getValeur() != null) {

                        List<String> valeursConditionnement =
                                clauseTexte.getValeurConditionnementList().get(clauseTexte.getValeurConditionnementList().size() - 1);

                        if (potentiellementConditionnee.isCritereBooleen()) {
                            valeursConditionnement.add(valeurConditionnee.getValeur().equals(1) ? Constantes.NON : Constantes.OUI);
                        } else {
                            List<PotentiellementConditionneeValeur> resultList;
                            if (potentiellementConditionnee.isProcedurePassation() && utilisateur.getIdOrganisme() != null)
                                resultList = potentiellementConditionneeService.getProceduresPassationParOrganisme(utilisateur.getIdOrganisme());
                            else
                                resultList = potentiellementConditionneeService.getListValeurs(potentiellementConditionnee);

                            if (resultList != null)
                                for (PotentiellementConditionneeValeur potentiellementConditionneeValeur : resultList)
                                    for (EpmTClauseValeurPotentiellementConditionneeAbstract potentiellementConditionneeValeurAbstract : valeurConditionneeList) {
                                        if (potentiellementConditionneeValeur.getId() == potentiellementConditionneeValeurAbstract.getValeur())
                                            valeursConditionnement.add(potentiellementConditionneeValeur.getLibelle());
                                    }

                        }
                        clauseTexte.setConditionee(true);
                    }
                }

            }
        }

        if (clauseTexte.isConditionee() && !clauseTexte.getTypeConditionnementList().isEmpty() && !clauseTexte.getValeurConditionnementList().isEmpty()) {
            String operateur = "ET";
            StringBuilder condition1 = new StringBuilder();
            String condition2 = "";
            String condition1Type = clauseTexte.getTypeConditionnementList().get(0);
            List<String> condition1Valeur = clauseTexte.getValeurConditionnementList().get(0);
            if (condition1Valeur.size() > 1) {
                condition1 = new StringBuilder(condition1Type + " ( ");
                for (int i = 0; i < condition1Valeur.size() - 1; i++) {
                    condition1.append(condition1Valeur.get(i)).append(", ");
                }
                condition1.append(condition1Valeur.get(condition1Valeur.size() - 1)).append(")");
            } else if (condition1Valeur.size() == 1) {
                condition1 = new StringBuilder(condition1Type + " ( " + condition1Valeur.get(0) + " )");
            }

            if (clauseTexte.getTypeConditionnementList().size() == 1) {
                clauseTexte.setTexteConditions(condition1.toString());
                return;
            }
            String condition2Type = clauseTexte.getTypeConditionnementList().get(1);
            List<String> condition2Valeur = clauseTexte.getValeurConditionnementList().get(1);
            if (condition2Valeur.size() > 1) {
                condition2 = condition2Type + " ( ";
                for (int i = 0; i < condition2Valeur.size() - 1; i++) {
                    condition2 = condition2 + condition2Valeur.get(i) + ", ";
                }
                condition2 = condition2 + condition2Valeur.get(condition2Valeur.size() - 1) + ")";
            } else {
                condition2 = condition2Type + " ( " + condition2Valeur.get(0) + " )";
            }

            clauseTexte.setTexteConditions(condition1 + " " + operateur + " " + condition2);
        }

    }



    /**
     * cette méthode permet de convertir une clause canevas vers une clause de
     * type texte.
     *
     * @param clauseCanevas   la clause à convertir.
     * @param epmTClause      l'objet epmTClause.
     * @param consultation    : Utilisateur.
     * @param listeRoles      identifiant de la consultation.
     * @param idLot           lot de la consultation
     * @param conditionnement lot de la consultation
     * @return la clause convertie.
     */
    private static ClauseTexte clauseCanevasToClauseTexte(final Clause clauseCanevas, final EpmTClauseAbstract epmTClause, final EpmTConsultation consultation, final List<EpmTRoleClauseAbstract> listeRoles, final int idLot, final boolean conditionnement, ResourceBundleMessageSource messageSource, EpmTUtilisateur utilisateur) throws TechnicalException, TechnicalNoyauException {

        ClauseTexte clauseTexte = new ClauseTexte();
        clauseTexte.setId(clauseCanevas.getIdClause()); // TODO: NIKO 2018-02.00.02
        clauseTexte.setRef(clauseCanevas.getReference());
        clauseTexte.setEtat(clauseCanevas.getEtat());
        clauseTexte.setActif(clauseCanevas.isActif() ? "1" : "0");
        if (!conditionnement && !clauseCanevas.getEtat().equals(Clause.ETAT_SUPPRIMER)) {

            LOG.info("Clause potentiellement conditionné KO lors de la conversion en texte");
            clauseTexte.setEtat(ClauseDocument.ETAT_CONDITIONNER);
        }

        setConditionnement(epmTClause, clauseTexte, utilisateur);
        clauseTexte.setInfoBulle(clauseCanevas.getInfoBulle());
        clauseTexte.setType(epmTClause.getEpmTRefTypeClause().getId());
        clauseTexte.setTexteFixeAvant(epmTClause.getTexteFixeAvant());
        clauseTexte.setTexteFixeApres(epmTClause.getTexteFixeApres());
        clauseTexte.setTexteFixeApresAlaLigne(epmTClause.isSautLigneTexteApres());
        clauseTexte.setTexteFixeAvantAlaLigne(epmTClause.isSautLigneTexteAvant());
        clauseTexte.setTexteModifiable(true);
        clauseTexte.setTexteObligatoire(false);
        if (listeRoles == null) {
            return clauseTexte;
        }
        for (EpmTRoleClauseAbstract epmTRoleClause : listeRoles) {
            switch (clauseTexte.getType()) {
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
                    clauseTexte.setTexteVariable(epmTRoleClause.getValeurDefaut());
                    clauseTexte.setTailleTexteVariable(String.valueOf(epmTRoleClause.getNombreCarateresMax()));
                    break;
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
                    if (epmTRoleClause.getChampObligatoire() != null && epmTRoleClause.getChampObligatoire().equals("1")) {
                        clauseTexte.setTexteObligatoire(true);
                        clauseTexte.setTailleTexteVariable(String.valueOf(epmTRoleClause.getNombreCarateresMax()));
                    }
                    break;
                case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new TechnicalException("La clause de type Texte Héritée est obsolete; Utilisez le type Valeur Héritée");
                case EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU: // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new TechnicalException("La clause de type Tableau est obsolete; Utilisez le type Valeur Héritée");
                case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                    try {
                        clauseValeurHerite(clauseTexte, epmTRoleClause, consultation, idLot, messageSource);
                    } catch (NonTrouveException e) {
                        LOG.error(e.getMessage(), e.fillInStackTrace());
                        throw new TechnicalException(e);
                    }
                    break;
            }
        }
        return clauseTexte;
    }

    /**
     * Cette méthode permet de gérer une clause hérité.
     *
     * @param clauseTexte    {@link ClauseTexte}
     * @param epmTRoleClause {@link EpmTRoleClause}
     * @param consultation   {@link EpmTConsultation}
     * @param idLot          Integer
     * @throws TechnicalException erreur technique
     * @throws NonTrouveException clause non trouvée
     */
    private static void clauseValeurHerite(final ClauseTexte clauseTexte, final EpmTRoleClauseAbstract epmTRoleClause, final EpmTConsultation consultation, final Integer idLot, final ResourceBundleMessageSource messageSource) throws TechnicalException, NonTrouveException {

        if (epmTRoleClause.getValeurDefaut() == null) {
            LOG.warn("La référence de la valeur héritée est vide pour la clause : " + clauseTexte.getRef());
            throw new TechnicalException("La référence de la valeur héritée est vide pour la clause : " + clauseTexte.getRef());
        }

        int idRefValeur;
        try {
            idRefValeur = Integer.parseInt(epmTRoleClause.getValeurDefaut());
        } catch (NumberFormatException ex) {
            throw new TechnicalException("La référence de la valeur héritée est incorrect pour la clause : " + clauseTexte.getRef());
        }

        if (idRefValeur <= 0) {
            LOG.warn("La référence de la valeur héritée est faut pour la clause : " + clauseTexte.getRef());
            throw new TechnicalException("La référence de la valeur héritée est faut pour la clause : " + clauseTexte.getRef());
        }

        EpmTRefValeurTypeClause epmTRefValeurTypeClause = redactionService.chercherObject(idRefValeur, EpmTRefValeurTypeClause.class);

        if (epmTRefValeurTypeClause == null) {
            LOG.warn("La référence de la valeur héritée est faut pour la clause : " + clauseTexte.getRef());
            throw new TechnicalException("La référence de la valeur héritée est faut pour la clause : " + clauseTexte.getRef());
        }

        if (epmTRefValeurTypeClause.isSimple()) {

            if (consultation != null) {
                HeriteConsultation heriteConsultation = initialiserHeriteConsultation(consultation);
                heriteConsultation.setMessageSource(messageSource);
                String texteHerite = reflection(epmTRefValeurTypeClause.getExpression(), heriteConsultation, idLot);
                clauseTexte.setTexteVariable(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(null == texteHerite ? HeriteConsultation.NOT_DEFINED : texteHerite));
            } else {
                clauseTexte.setTexteVariable(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(epmTRefValeurTypeClause.getLibelle()));
            }

        } else {

            String xmlTableau;
            TableauRedaction tableauRedaction;
            if (consultation != null) {
                tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(epmTRefValeurTypeClause.getExpression(), consultation, idLot, null);
                xmlTableau = GenerationTableauUtil.tableauRedactionToHTML(tableauRedaction);
            } else {
                tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(epmTRefValeurTypeClause.getExpression(), null, null, null);
                xmlTableau = GenerationTableauUtil.tableauRedactionToHTML(tableauRedaction);
            }

            clauseTexte.setTexteVariable(xmlTableau);
            clauseTexte.setIdRefValeurTableau(idRefValeur);
            clauseTexte.setTableauRedaction(tableauRedaction);
        }

        clauseTexte.setTailleTexteVariable(Constante.TAILLE_CHAMP_LONG);
        if (epmTRoleClause.getValeurHeriteeModifiable() != null && epmTRoleClause.getValeurHeriteeModifiable().equals("1")) {
            clauseTexte.setTexteModifiable(true);
        } else {
            clauseTexte.setTexteModifiable(false);
        }
    }

    private static void clauseValeurHerite(final ClauseTexte clauseTexte, final EpmTRoleClauseAbstract epmTRoleClause) throws TechnicalException, NonTrouveException {
        clauseValeurHerite(clauseTexte, epmTRoleClause, null, null, null);
    }

    /**
     * @param cheminValeur       Path des objets à parcourir
     * @param heriteConsultation contient l'ensemble des valeurs héritées à afficher.
     * @param idLot              lot associé au document.
     * @return valeur héritée
     * @throws TechnicalException erreur technique
     */
    public static String reflection(final String cheminValeur, final HeriteConsultation heriteConsultation, final int idLot) {

        String[] methodes = cheminValeur.split("\\.");

        LOG.debug("Introspection sur les méthodes suivantes = {}", Arrays.stream(methodes).collect(Collectors.toSet()));

        Object[] arguments = null;
        for (int i = 0; i < methodes.length; i++) {
            int ouvrante = methodes[i].indexOf("(");
            if (ouvrante != -1) {
                int accolade = cheminValeur.indexOf("[");

                if (accolade != -1) {
                    arguments = new Object[2];
                    arguments[0] = idLot;
                    arguments[1] = cheminValeur.substring(accolade + 1, cheminValeur.length() - 1);
                } else {
                    arguments = new Object[1];
                    arguments[0] = idLot;
                }
                methodes[i] = methodes[i].substring(0, ouvrante);
            }
        }

        LOG.debug("Liste des methodes à appeler = {}", Arrays.stream(methodes).collect(Collectors.toSet()));

        Object resultat = heriteConsultation;
        for (String s : methodes) {
            String methode = Util.creationMethode(s);
            resultat = Util.lancerMethode(resultat, arguments, methode);

            LOG.debug("Appel de la méthode {}({}) avec pour résultat = {}, ", methode, arguments, resultat);
        }
        if (resultat instanceof String) {
            if (!resultat.equals("")) {
                return (String) resultat;
            } else {
                return null;
            }
        } else if (resultat instanceof List) {
            remplaceVirguleClauseTableau((List) resultat);
            return resultat.toString();
        }
        return null;
    }

    /**
     * @param cheminValeur Path des objets à parcourir
     * @return valeur héritée
     * @throws TechnicalException erreur technique
     */
    private static String reflection(final String cheminValeur) {

        String[] methodes = cheminValeur.split("\\.");

        Object[] arguments = null;
        for (int i = 0; i < methodes.length; i++) {
            int ouvrante = methodes[i].indexOf("(");
            if (ouvrante != -1) {
                arguments = new Object[methodes.length];
                methodes[i] = methodes[i].substring(0, ouvrante);
            }
        }
        return "ND";
    }

    private static void remplaceVirguleClauseTableau(List liste) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i) instanceof List) {
                remplaceVirguleClauseTableau((List) liste.get(i));
            } else if (liste.get(i) instanceof String) {
                liste.set(i, Util.remplaceTableauPourSauvegarde((String) liste.get(i)));
            }
        }
    }

    /**
     * @param consultation
     * @return
     * @throws TechnicalNoyauException
     */
    private static HeriteConsultation initialiserHeriteConsultation(final EpmTConsultation consultation) {

        List<EpmTValeurConditionnementExterneComplexe> listeExterneComplexe = consultation.getEpmTValeurConditionnementExterneComplexeList();

        List<EpmTValeurConditionnementExterne> listExterne = consultation.getEpmTValeurConditionnementExterneList();

        Date dateRemisePlis = null;
        List<EpmTCalendrier> calendriers = consultation.getCalendriers();
        if (!CollectionUtils.isEmpty(calendriers)) {
            EtapeSimple etapeRemisePlis = consultationService.chargerEtapeDateCal(calendriers.stream().map(EpmTCalendrier::getEtapes).flatMap(Collection::stream).filter(etapeCal -> EpmTEtapeCal.RECEPTION_CANDIDATURES == etapeCal.getTypeEtape()).findFirst().orElse(null));
            if (etapeRemisePlis != null) dateRemisePlis = etapeRemisePlis.getDate();
        }

        return new HeriteConsultation(consultation, dateRemisePlis, listExterne, listeExterneComplexe);
    }

    public static void setReferentielFacade(ReferentielFacade valeur) {
        ClauseDocumentUtil.referentielFacade = valeur;
    }

    public static void setPotentiellementConditionneeService(PotentiellementConditionneeService potentiellementConditionneeService) {
        ClauseDocumentUtil.potentiellementConditionneeService = potentiellementConditionneeService;
    }

    /**
     * @param redactionService Injecté par Spring
     */
    public static void setRedactionService(RedactionServiceSecurise redactionService) {
        ClauseDocumentUtil.redactionService = redactionService;
    }

    /**
     * @param valeur the clauseFacade to set
     */
    public final void setClauseFacadeGWT(final ClauseFacadeGWT valeur) {
        ClauseDocumentUtil.clauseFacadeGWT = valeur;
    }

    /**
     * @param valeur the consultationService to set
     */
    public final void setConsultationService(final ConsultationServiceSecurise valeur) {
        ClauseDocumentUtil.consultationService = valeur;
    }

    public void setDocumentModeleService(DocumentModeleServiceSecurise valeur) {
        /* FAIT ATTENTION de ce methode */
        GenerationTableauUtil.setDocumentModeleService(valeur);
    }

    public static ClauseDocument getClauseTextePrevalorise(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseTexte clauseTexte = getBasicClauseTexte(clauseBean, utilisateur);
        clauseTexte.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseTextePrevalorise().getTextFixeAvant()));
        clauseTexte.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseTextePrevalorise().getTextFixeApres()));
        clauseTexte.setTexteFixeApresAlaLigne(clauseBean.getClauseTextePrevalorise().getSautTextFixeApres().equals("true"));
        clauseTexte.setTexteFixeAvantAlaLigne(clauseBean.getClauseTextePrevalorise().getSautTextFixeAvant().equals("true"));
        clauseTexte.setTexteModifiable(true);
        clauseTexte.setTexteObligatoire(false);
        clauseTexte.setTailleTexteVariable(String.valueOf(clauseBean.getClauseTextePrevalorise().getTailleChamps()));
        clauseTexte.setTexteVariable(clauseBean.getClauseTextePrevalorise().getDefaultValue());
        return clauseTexte;
    }

    public static ClauseDocument getClauseListeCumulatif(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseListe clauseListe = getBasicClauseListe(clauseBean, utilisateur);
        clauseListe.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseListeChoixCumulatif().getTextFixeAvant()));
        clauseListe.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseListeChoixCumulatif().getTextFixeApres()));
        clauseListe.setTexteFixeApresAlaLigne(clauseBean.getClauseListeChoixCumulatif().getSautTextFixeApres().equals("true"));
        clauseListe.setTexteFixeAvantAlaLigne(clauseBean.getClauseListeChoixCumulatif().getSautTextFixeAvant().equals("true"));
        clauseListe.setTexteModifiable(true);
        List<ClauseFormulaire> formulaires = clauseBean.getClauseListeChoixCumulatif().getFormulaires();
        if (formulaires != null) {
            ArrayList<Formulation> listeDesFormulation = new ArrayList<>();
            for (ClauseFormulaire formulation : formulaires) {
                Formulation uneFormulation = new Formulation();
                uneFormulation.setPrecochee(formulation.isPrecochee());
                uneFormulation.setLibelle(formulation.getDefaultValeur());
                uneFormulation.setNumFormulation(formulation.getNumFormulation());
                listeDesFormulation.add(uneFormulation);
            }
            clauseListe.setFormulations(listeDesFormulation);
        }
        return clauseListe;
    }

    public static ClauseDocument getClauseListeExclusif(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseListe clauseListe = getBasicClauseListe(clauseBean, utilisateur);
        clauseListe.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseListeChoixExclusif().getTextFixeAvant()));
        clauseListe.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseListeChoixExclusif().getTextFixeApres()));
        clauseListe.setTexteFixeApresAlaLigne(clauseBean.getClauseListeChoixExclusif().getSautTextFixeApres().equals("true"));
        clauseListe.setTexteFixeAvantAlaLigne(clauseBean.getClauseListeChoixExclusif().getSautTextFixeAvant().equals("true"));
        clauseListe.setTexteModifiable(true);
        List<ClauseFormulaire> formulaires = clauseBean.getClauseListeChoixExclusif().getFormulaires();
        if (formulaires != null) {
            ArrayList<Formulation> listeDesFormulation = new ArrayList<>();
            for (ClauseFormulaire formulation : formulaires) {
                Formulation uneFormulation = new Formulation();
                uneFormulation.setPrecochee(formulation.isPrecochee());
                uneFormulation.setLibelle(formulation.getDefaultValeur());
                uneFormulation.setNumFormulation(formulation.getNumFormulation());
                listeDesFormulation.add(uneFormulation);
            }
            clauseListe.setFormulations(listeDesFormulation);
        }
        return clauseListe;
    }

    public static ClauseListe getBasicClauseListe(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseListe clauseListe = new ClauseListe();
        clauseListe.setRef(clauseBean.getReferenceClause());
        setConditionnementToClause(clauseBean, clauseListe, utilisateur);
        clauseListe.setInfoBulle(clauseBean.getInfoBulle());
        clauseListe.setType(clauseBean.getIdTypeClause());
        return clauseListe;
    }

    public static ClauseDocument getClauseTexteValeurHeritee(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseTexte clauseTexte = getBasicClauseTexte(clauseBean, utilisateur);
        clauseTexte.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseValeurHeritee().getTextFixeAvant()));
        clauseTexte.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseValeurHeritee().getTextFixeApres()));
        clauseTexte.setTexteFixeApresAlaLigne(clauseBean.getClauseValeurHeritee().getSautTextFixeApres().equals("true"));
        clauseTexte.setTexteFixeAvantAlaLigne(clauseBean.getClauseValeurHeritee().getSautTextFixeAvant().equals("true"));
        clauseTexte.setTexteModifiable(true);
        clauseTexte.setTexteObligatoire(false);
        EpmTRefValeurTypeClause epmTRefValeur = referentielFacade.getValeurTypeClauseById(clauseBean.getClauseValeurHeritee().getIdRefValeurTypeClause());
        clauseTexte.setTexteVariable(epmTRefValeur.getLibelle());
        return clauseTexte;
    }

    public static ClauseDocument getClauseTexteFixe(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseTexte clauseTexte = getBasicClauseTexte(clauseBean, utilisateur);
        clauseTexte.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseTexteFixe().getTexteFixe()));
        return clauseTexte;
    }

    public static ClauseTexte getBasicClauseTexte(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseTexte clauseTexte = new ClauseTexte();
        clauseTexte.setRef(clauseBean.getReferenceClause());
        setConditionnementToClause(clauseBean, clauseTexte, utilisateur);
        clauseTexte.setInfoBulle(clauseBean.getInfoBulle());
        clauseTexte.setType(clauseBean.getIdTypeClause());
        return clauseTexte;
    }

    public static ClauseDocument getClauseTexteLibre(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {
        ClauseTexte clauseTexte = getBasicClauseTexte(clauseBean, utilisateur);
        clauseTexte.setTexteFixeAvant(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseTexteLibre().getTextFixeAvant()));
        clauseTexte.setTexteFixeApres(fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(clauseBean.getClauseTexteLibre().getTextFixeApres()));
        clauseTexte.setTexteFixeApresAlaLigne(clauseBean.getClauseTexteLibre().getSautTextFixeApres().equals("true"));
        clauseTexte.setTexteFixeAvantAlaLigne(clauseBean.getClauseTexteLibre().getSautTextFixeAvant().equals("true"));
        clauseTexte.setTexteModifiable(true);
        clauseTexte.setTexteObligatoire(false);
        clauseTexte.setTailleTexteVariable(String.valueOf(clauseBean.getClauseTexteLibre().getTailleChamps()));
        return clauseTexte;
    }

    private static void setConditionnementToClause(ClauseBean clauseBean, ClauseDocument clauseDocument, EpmTUtilisateur utilisateur) {
        clauseDocument.setConditionee(false);
        if (clauseBean.getPotentiellementConditionnees() != null) {
            clauseDocument.setConditionee(true);
            List<String> typeConditionnementList = new ArrayList<>();
            List<List<String>> valeurConditionnementList = new ArrayList<>();

            for (PotentiellementConditionnee condition : clauseBean.getPotentiellementConditionnees()) {
                EpmTRefPotentiellementConditionnee potentiellementConditionnee = referentielFacade.getPotentiellementConditionneeParId(condition.getId());
                List<String> valeurList = new ArrayList<>();
                if (potentiellementConditionnee != null) {
                    typeConditionnementList.add(potentiellementConditionnee.getLibelle());
                    if (potentiellementConditionnee.isCritereBooleen()) {
                        valeurConditionnementList.add(Collections.singletonList(condition.getValeurId().equals(1) ? Constantes.NON : Constantes.OUI));
                    } else {
                        List<PotentiellementConditionneeValeur> resultList;
                        if (potentiellementConditionnee.isProcedurePassation() && utilisateur.getIdOrganisme() != null)
                            resultList = potentiellementConditionneeService.getProceduresPassationParOrganisme(utilisateur.getIdOrganisme());
                        else
                            resultList = potentiellementConditionneeService.getListValeurs(potentiellementConditionnee);

                        if (resultList != null) {

                            for (PotentiellementConditionneeValeur potentiellementConditionneeValeur : resultList) {
                                if (!condition.getValeurs().isEmpty() && (condition.getValeurs().contains(potentiellementConditionneeValeur.getId()))) {
                                    valeurList.add(potentiellementConditionneeValeur.getLibelle());

                                }

                                if (condition.getValeurId() != null && (condition.getValeurId().equals(potentiellementConditionneeValeur.getId()))) {
                                    valeurList.add(potentiellementConditionneeValeur.getLibelle());

                                }
                            }

                        }
                        valeurConditionnementList.add(valeurList);

                    }
                }
            }

            if (clauseDocument.isConditionee() && !typeConditionnementList.isEmpty() && !valeurConditionnementList.isEmpty()) {
                String operateur = "ET";
                String condition1 = "";
                String condition2 = "";
                String condition1Type = typeConditionnementList.get(0);
                List<String> condition1Valeur = valeurConditionnementList.get(0);
                if (condition1Valeur.size() > 1) {
                    condition1 = condition1Type + " ( ";
                    for (int i = 0; i < condition1Valeur.size() - 1; i++) {
                        condition1 = condition1 + condition1Valeur.get(i) + ", ";
                    }
                    condition1 = condition1 + condition1Valeur.get(condition1Valeur.size() - 1) + ")";
                } else {
                    condition1 = condition1Type + " ( " + condition1Valeur.get(0) + " )";
                }

                if (typeConditionnementList.size() == 1) {
                    clauseDocument.setTexteConditions(condition1);
                    return;
                }
                String condition2Type = typeConditionnementList.get(1);
                List<String> condition2Valeur = valeurConditionnementList.get(1);
                if (condition2Valeur.size() > 1) {
                    condition2 = condition2Type + " ( ";
                    for (int i = 0; i < condition2Valeur.size() - 1; i++) {
                        condition2 = condition2 + condition2Valeur.get(i) + ", ";
                    }
                    condition2 = condition2 + condition2Valeur.get(condition2Valeur.size() - 1) + ")";
                } else {
                    condition2 = condition2Type + " ( " + condition2Valeur.get(0) + " )";
                }

                clauseDocument.setTexteConditions(condition1 + " " + operateur + " " + condition2);
            }
        }
    }



}
