package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.presentation.controllers.RecherchePagineeBean;
import fr.paris.epm.global.presentation.forms.TableauPagineGeneriqueForm;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefAuteur;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefStatutRedactionClausier;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.redaction.util.Constantes;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Formulaire de la rechercher d'une clause.
 * author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class RechercherClauseForm extends /*TableauPagineGeneriqueForm,*/ RecherchePagineeBean {
    /**
     * marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(RechercherClauseForm.class);

    /**
     * Attribut référence.
     */
    private String reference;
    /**
     *
     */
    private String referenceCanevas;
    /**
     * Attribut mots clés.
     */
    private String motsCles = "";
    /**
     * Attribut thème.
     */
    private String theme = "1";
    /**
     * Attribut type de contrat.
     */
    private String typeContrat;
    /**
     * Attribut procédure de passation.
     */
    private String procedurePassation = null;
    /**
     * Attribut nature de préstation.
     */
    private String naturePrestas = "0";
    /**
     * Attribut type de document.
     */
    private String docType = "1";
    /**
     * Attribut afficher clause.
     */
    private String afficheClauses = null;
    /**
     * Attribut resultats.
     */
    private String results;
    /**
     * Attribut date de modification.
     */
    private GregorianCalendar dateModification;
    /**
     * Attribut date de création.
     */
    private GregorianCalendar dateCreation;
    /**
     * Attribut collection des thèmes.
     */
    private Collection themeCollection; // que pour la version 2016
    /**
     * Attribut collection des types de contrat.
     */
    private List typeContratCollection; // que pour la version 2016
    /**
     * Attribut collection des procédures de passation .
     */
    private List<EpmTRefProcedure> procedureCollection; // que pour la version 2016
    /**
     * Attribut collection des natures de préstations.
     */
    private Collection naturePrestasCollection; // que pour la version 2016
    /**
     * Attribut collection des types de documents.
     */
    private Collection docTypeCollection; // que pour la version 2016
    /**
     * Attribut collection des listes Clauses.
     */
    private Collection listeClause; // que pour la version 2016
    /**
     * Identifiant du statut de la rédaction de la clause
     */
    private String idStatutRedactionClausier = "0";
    /**
     * Attribut collection des listes de status de rédaction de clauses
     */
    private Collection<EpmTRefStatutRedactionClausier> listeTousStatusRedactionClause; // que pour la version 2016
    /**
     * Date de modification début
     */
    private String dateModificationDebut;
    /**
     * Date de modification fin
     */
    private String dateModificationFin;
    /**
     * L'action selectionnée (Demander la validation, Refuser, Valider)
     */
    private String hiddenAction;
    /**
     * Editeur/Editeur surchargée/Client
     */
    private String auteur;
    /**
     * Liste des referentiels des auteurs
     */
    private List<EpmTRefAuteur> listeTousAuteur; // que pour la version 2016

    private boolean editeur;

    private boolean erreurs;

    public RechercherClauseForm() {
        super();
        setTriPropriete("dateModification");
    }

    /**
     * Cette méthode permet d'initialiser le formulaire .
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        reference = null;
        referenceCanevas = null;
        motsCles = "";
        theme = "1";
        typeContrat = null;
        procedurePassation = null;
        naturePrestas = "0";
        docType = "1";
        afficheClauses = null;
        results = null;
        //themeCollection = null;
        //typeContratCollection = null;
        //procedurePassationCollection = null;
        //naturePrestasCollection = null;
        //docTypeCollection = null;
        listeClause = null;
        idStatutRedactionClausier = null;
        dateModificationDebut = null;
        dateModificationFin = null;
        hiddenAction = null;
        auteur = null;
    }

    private void addError(BindingResult result, String value, String propriete) {
        ObjectError objectError = new ObjectError(value, ResourceBundle.getBundle("ApplicationResources").getString(propriete));
        if (!result.getAllErrors().contains(objectError))
            result.addError(objectError);
    }

    public void validate(BindingResult result, HttpServletRequest request) {

        if (dateModificationDebut != null && !(dateModificationDebut.equals(""))
                && !dateModificationDebut.matches(ConstantesGlobales.DATE_REGEX_dd_MM_yyyy)) {
            addError(result, "dateModificationDebut", "RechercherClause.txt.erreurDate");
        }
        if (dateModificationFin != null && !(dateModificationFin.equals(""))
                && !dateModificationFin.matches(ConstantesGlobales.DATE_REGEX_dd_MM_yyyy)) {
            addError(result, "dateModificationFin", "RechercherClause.txt.erreurDate");
        }

        //Code executé en cas de plusieurs clauses cochées
        // On vérifie si l'application du nouveau statut est possible sur la liste des clauses
        if (hiddenAction != null && !hiddenAction.isEmpty()) {
            SessionManagerGlobal sessionManager = SessionManagerGlobal.getInstance();
            String nomResultat = (String) request.getSession().getAttribute(ConstantesGlobales.S_NOM_RESULTAT);

            if (nomResultat != null) {
                List<Object> listeResultat = (List<Object>) request.getSession().getAttribute(nomResultat);
                String proprietesRequete = request.getParameter(ConstantesGlobales.LISTE_PROPRIETES);
                if (proprietesRequete != null) {
                    String[] proprietes = proprietesRequete.split(";");

                    List<String> identifiants = (List<String>) request.getSession().getAttribute(ConstantesGlobales.LISTE_IDENTIFIANTS);
                    if (identifiants == null)
                        identifiants = new ArrayList<String>();

                    identifiants = miseAJourPageCourante(this, identifiants, listeResultat,
                            proprietes);
                    sessionManager.ajouterElement(request.getSession(), ConstantesGlobales.LISTE_IDENTIFIANTS, identifiants);
                    Set<String> statuts = new HashSet<String>();
                    recupererSetStatuts(identifiants, statuts);

                    String validerClause = "validerClause";
                    if (!identifiants.isEmpty()) {
                        if (hiddenAction.equals(Constantes.DEMANDER_VALIDATION_CLAUSE) && statuts.size() > 1) {
                            addError(result, validerClause, "RechercherClause.txt.erreurDemanderValidation");
                        } else if (hiddenAction.equals(Constantes.REFUSER_CLAUSE)
                                && (statuts.contains(String.valueOf(Constantes.BROUILLON)) || statuts.size() > 2)) {
                            addError(result, validerClause, "RechercherClause.txt.erreurRefus");
                        } else if (hiddenAction.equals(Constantes.VALIDER_CLAUSE) && (statuts.size() > 1 || !statuts.contains(String.valueOf(Constantes.A_VALIDER)))) {
                            addError(result, validerClause, "RechercherClause.txt.erreurValidation");
//                        } else if(hiddenAction.equals(Constantes.SUPPRIMER_CLAUSE) && (statuts.size() > 1 || !statuts.contains(String.valueOf(Constantes.VALIDEE)))) {
//                            erreurs.add(validerClause, new ActionMessage("RechercherClause.txt.erreurSuppression"));
                        }
                    }
                }
            }
        }
        erreurs = result.hasErrors();
    }

    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors erreurs = new ActionErrors();

        if (dateModificationDebut != null && !(dateModificationDebut.equals("")) &&
                !dateModificationDebut.matches(ConstantesGlobales.DATE_REGEX_dd_MM_yyyy))
            erreurs.add("dateModificationDebut", new ActionMessage("RechercherClause.txt.erreurDate"));

        if (dateModificationFin != null && !(dateModificationFin.equals("")) &&
                !dateModificationFin.matches(ConstantesGlobales.DATE_REGEX_dd_MM_yyyy))
            erreurs.add("dateModificationFin", new ActionMessage("RechercherClause.txt.erreurDate"));

        //Code executé en cas de plusieurs clauses cochées
        // On vérifie si l'application du nouveau statut est possible sur la liste des clauses
        if(hiddenAction != null && !hiddenAction.isEmpty()) {
            SessionManagerGlobal sessionManager = SessionManagerGlobal.getInstance();
            String nomResultat = (String) request.getSession().getAttribute(ConstantesGlobales.S_NOM_RESULTAT);

            if (nomResultat != null) {
                List<Object> listeResultat = (List<Object>) request.getSession().getAttribute(nomResultat);
                String proprietesRequete = request.getParameter(ConstantesGlobales.LISTE_PROPRIETES);
                if (proprietesRequete != null) {
                    String[] proprietes = proprietesRequete.split(";");

                    List<String> identifiants = (List<String>) request.getSession().getAttribute(ConstantesGlobales.LISTE_IDENTIFIANTS);
                    if (identifiants == null)
                        identifiants = new ArrayList<String>();

                    identifiants = miseAJourPageCourante(this, identifiants, listeResultat,
                            proprietes);
                    sessionManager.ajouterElement(request.getSession(), ConstantesGlobales.LISTE_IDENTIFIANTS, identifiants);
                    Set<String> statuts = new HashSet<String>();
                    recupererSetStatuts(identifiants, statuts);

                    String validerClause = "validerClause";
                    if (!identifiants.isEmpty()) {
                        if (hiddenAction.equals(Constantes.DEMANDER_VALIDATION_CLAUSE) && statuts.size() > 1)
                            erreurs.add(validerClause, new ActionMessage("RechercherClause.txt.erreurDemanderValidation"));
                        else if (hiddenAction.equals(Constantes.REFUSER_CLAUSE) && (statuts.contains(String.valueOf(Constantes.BROUILLON)) || statuts.size() > 2))
                            erreurs.add(validerClause, new ActionMessage("RechercherClause.txt.erreurRefus"));
                        else if (hiddenAction.equals(Constantes.VALIDER_CLAUSE) && (statuts.size() > 1 || !statuts.contains(String.valueOf(Constantes.A_VALIDER))))
                            erreurs.add(validerClause, new ActionMessage("RechercherClause.txt.erreurValidation"));
                        //else if (hiddenAction.equals(Constantes.SUPPRIMER_CLAUSE) && (statuts.size() > 1 || !statuts.contains(String.valueOf(Constantes.VALIDEE))))
                        //erreurs.add(validerClause, new ActionMessage("RechercherClause.txt.erreurSuppression"));
                    }
                }
            }
        }
        return erreurs;
    }

    private List<String> miseAJourPageCourante(final TableauPagineGeneriqueForm formulaire, List<String> identifiants,
                                               final List<Object> listeResultat, final String[] proprietes) {
        identifiants = supprimerIdentifiantPageCourante(identifiants, listeResultat, proprietes);
        identifiants = miseAjourDepuisFormulaire(formulaire, identifiants);
        return identifiants;
    }

    private List<String> miseAjourDepuisFormulaire(final TableauPagineGeneriqueForm formulaire, List<String> identifiants) {
        String[] identifiantsFormulaire = formulaire.getIdentifiants();
        if (identifiantsFormulaire != null)
            identifiants.addAll(Arrays.asList(identifiantsFormulaire));
        return identifiants;
    }

    private List<String> supprimerIdentifiantPageCourante(List<String> identifiants, final List<Object> listeResultat, final String[] proprietes) {
        for (Object objet : listeResultat) {
            if (!(objet instanceof Long)) {
                String identifiant = recupererIdentifiantObjet(proprietes, objet);
                identifiants.remove(identifiant);
            }
        }
        return identifiants;
    }

    private String recupererIdentifiantObjet(final String[] proprietes, final Object objet) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (String prop : proprietes) {
            try {
                if (!first)
                    sb.append(";");
                Object propriete = PropertyUtils.getProperty(objet, prop);
                sb.append(propriete.toString());
                first = false;
            } catch (Exception e) {
                logger.error(e.getMessage(), e.fillInStackTrace());
            }
        }
        return sb.toString();

    }

    /**
     * Rempli un set par l'ensemble des statuts de clauses selectionnées
     * @param identifiants
     * @param statuts
     */
    private void recupererSetStatuts(final List<String> identifiants, Set<String> statuts) {
        for (String identifiant : identifiants) {
            StringTokenizer st = new StringTokenizer(identifiant, ";");
            // on passe le premier token
            st.nextToken();
            statuts.add(st.nextToken());
        }
    }

    /**
     * @return afficher clauses
     */
    public final String getAfficheClauses() {
        return afficheClauses;
    }

    /**
     * @param valeur positionne l'attribut afficheClauses.
     */
    public final void setAfficheClauses(final String valeur) {
        afficheClauses = valeur;
    }

    /**
     * @return type de document.
     */
    public final String getDocType() {
        return docType;
    }

    /**
     * @param valeur positionne le type de document.
     */
    public final void setDocType(final String valeur) {
        docType = valeur;
    }

    /**
     * @return collection de types de document.
     */
    public final Collection getDocTypeCollection() {
        return docTypeCollection;
    }

    /**
     * @param valeur initialise une collection de types de document.
     */
    public final void setDocTypeCollection(final Collection valeur) {
        docTypeCollection = valeur;
    }

    /**
     * @return mots clés.
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * @param valeur positionne les mots clés.
     */
    public final void setMotsCles(final String valeur) {
        motsCles = valeur;
    }

    /**
     * @return nature de préstation.
     */
    public final String getNaturePrestas() {
        return naturePrestas;
    }

    /**
     * @param valeur positionne la nature de préstation.
     */
    public final void setNaturePrestas(final String valeur) {
        naturePrestas = valeur;
    }

    /**
     * @return Collection de natures de préstation
     */
    public final Collection getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur positionne une collection de natures de préstation.
     */
    public final void setNaturePrestasCollection(final Collection valeur) {
        naturePrestasCollection = valeur;
    }

    /**
     * @return  type de contrat
     */
    public String getTypeContrat() {
        return typeContrat;
    }

    /**
     * @param typeContrat type de contrat
     */
    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    /**
     * @return procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * @param valeur positionne la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * @return   Collection des types de contrat
     */
    public List getTypeContratCollection() {
        return typeContratCollection;
    }

    /**
     * @param typeContratCollection pour positionner une collection des types de contrat.
     */
    public void setTypeContratCollection(List typeContratCollection) {
        this.typeContratCollection = typeContratCollection;
    }

    public List<EpmTRefProcedure> getProcedureCollection() {
        return procedureCollection;
    }

    public void setProcedureCollection(List<EpmTRefProcedure> procedureCollection) {
        this.procedureCollection = procedureCollection;
    }

    /**
     * @return référence de la clause.
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @param valeur positionner la référence de la clause.
     */
    public final void setReference(final String valeur) {
        reference = valeur;
    }

    /**
     * @return the referenceCanevas
     */
    public final String getReferenceCanevas() {
        return referenceCanevas;
    }

    /**
     * @param valeur the referenceCanevas to set
     */
    public final void setReferenceCanevas(final String valeur) {
        this.referenceCanevas = valeur;
    }

    /**
     * @return résultats de la recherche.
     */
    public final String getResults() {
        return results;
    }

    /**
     * @param valeur initialiser les résultats de la recherche.
     */
    public final void setResults(final String valeur) {
        results = valeur;
    }

    /**
     * @return thème.
     */
    public final String getTheme() {
        return theme;
    }

    /**
     * @param valeur pour initialiser l'attribut thème.
     */
    public final void setTheme(final String valeur) {
        theme = valeur;
    }

    /**
     * @return Collection de thèmes.
     */
    public final Collection getThemeCollection() {
        return themeCollection;
    }

    /**
     * @param valeur positionne une collection de thèmes.
     */
    public final void setThemeCollection(final Collection valeur) {
        themeCollection = valeur;
    }

    /**
     * @return collection de  listes Clause.
     */
    public final Collection getListeClause() {
        return listeClause;
    }

    /**
     * @param valeur positionne une collection de  listes Clause.
     */
    public final void setListeClause(final Collection valeur) {
        listeClause = valeur;
    }

    /**
     * @return date de création de la clause.
     */
    public final GregorianCalendar getDateCreation() {
        return dateCreation;
    }

    /**
     * @param valeur initialise la date de création de la clause.
     */
    public final void setDateCreation(final GregorianCalendar valeur) {
        dateCreation = valeur;
    }

    /**
     * @return date modification.
     */
    public final GregorianCalendar getDateModification() {
        return dateModification;
    }

    /**
     * @param valeur positionne la date de modification.
     */
    public final void setDateModification(final GregorianCalendar valeur) {
        dateModification = valeur;
    }

    /**
     * @return the idStatutRedactionClausier
     */
    public final String getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }

    /**
     * @param valeur the idStatutRedactionClausier to set
     */
    public final void setIdStatutRedactionClausier(final String valeur) {
        this.idStatutRedactionClausier = valeur;
    }

    /**
     * @return the listeTousStatusRedactionClause
     */
    public final Collection<EpmTRefStatutRedactionClausier> getListeTousStatusRedactionClause() {
        return listeTousStatusRedactionClause;
    }

    /**
     * @param valeur the listeTousStatusRedactionClause to set
     */
    public final void setListeTousStatusRedactionClause(final Collection<EpmTRefStatutRedactionClausier> valeur) {
        this.listeTousStatusRedactionClause = valeur;
    }

    /**
     * @return the dateModificationDebut
     */
    public final String getDateModificationDebut() {
        return dateModificationDebut;
    }

    /**
     * @param valeur the dateModificationDebut to set
     */
    public final void setDateModificationDebut(final String valeur) {
        this.dateModificationDebut = valeur;
    }

    /**
     * @return the dateModificationFin
     */
    public final String getDateModificationFin() {
        return dateModificationFin;
    }

    /**
     * @param valeur the dateModificationFin to set
     */
    public final void setDateModificationFin(final String valeur) {
        this.dateModificationFin = valeur;
    }

    /**
     * @return the hiddenAction
     */
    public final String getHiddenAction() {
        return hiddenAction;
    }

    /**
     * @param valeur the hiddenAction to set
     */
    public final void setHiddenAction(final String valeur) {
        this.hiddenAction = valeur;
    }

    /**
     * @return the auteur
     */
    public final String getAuteur() {
        return auteur;
    }

    /**
     * @param valeur the auteur to set
     */
    public final void setAuteur(final String valeur) {
        this.auteur = valeur;
    }

    /**
     * @return the listeTousAuteur
     */
    public final List<EpmTRefAuteur> getListeTousAuteur() {
        return listeTousAuteur;
    }

    /**
     * @param valeur the listeTousAuteur to set
     */
    public final void setListeTousAuteur(final List<EpmTRefAuteur> valeur) {
        this.listeTousAuteur = valeur;
    }

    public boolean isEditeur() {
        return editeur;
    }

    public void setEditeur(boolean editeur) {
        this.editeur = editeur;
    }

    public boolean isErreurs() {
        return erreurs;
    }

}
