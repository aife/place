package fr.paris.epm.redaction.metier.objetValeur;

import java.io.Serializable;
import java.util.List;

/**
 * classe PotentiellementConditionnee.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class PotentiellementConditionnee implements Comparable<PotentiellementConditionnee>, Serializable {
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -4253707317521256740L;

    /**
     * libellé du PotentiellementConditionnee.
     */
    private String libelle;

    /**
     * l'identifiant du PotentiellementConditionnee.
     */
    private int id;

    /**
     * les valeurs du PotentiellementConditionnee.
     */
    private List valeurs;
    private Integer valeurId;


    /**
     * si le choix multiple est autorisé
     */
    private boolean multiChoix;

    /**
     * cette méthode renvoie le libellé du PotentiellementConditionnee.
     * @return renvoyer le libellé du PotentiellementConditionnee.
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * cette méthode positionne le libellé du PotentiellementConditionnee.
     * @param valeur pour initialiser le libellé du PotentiellementConditionnee.
     */
    public final void setLibelle(final String valeur) {
        libelle = valeur;
    }

    /**
     * cette méthode renvoie l'identifiant du PotentiellementConditionnee.
     * @return renvoyer l'identifiant du PotentiellementConditionnee.
     */
    public final int getId() {
        return id;
    }

    /**
     * cette méthode positionne l'identifiant du PotentiellementConditionnee.
     * @param valeur pour initialiser l'identifiant du
     *            PotentiellementConditionnee.
     */
    public final void setId(final int valeur) {
        id = valeur;
    }

    /**
     * cette méthode renvoie la liste des valeurs du
     * PotentiellementConditionnee.
     * @return renvoyer la liste des valeurs du PotentiellementConditionnee.
     */
    public final List getValeurs() {
        return valeurs;
    }

    /**
     * cette méthode positionne la liste des valeurst du
     * PotentiellementConditionnee.
     * @param valeur pour initialiser la liste des valeurs du
     *            PotentiellementConditionnee.
     */
    public final void setValeurs(final List valeur) {
        valeurs = valeur;
    }
    
    public final boolean isMultiChoix() {
        return multiChoix;
    }

    public final void setMultiChoix(final boolean valeur) {
        this.multiChoix = valeur;
    }

    public Integer getValeurId() {
        return valeurId;
    }

    public void setValeurId(Integer valeurId) {
        this.valeurId = valeurId;
    }

    /**
     * cette méthode permet la comparaison entre deux objets.
     * @return resultat de la comparaison.
     * @param objet est le paramètre à comparer.
     */
	@Override
    public final int compareTo(final PotentiellementConditionnee objet) {
        return libelle.compareTo(objet.getLibelle());
    }

}
