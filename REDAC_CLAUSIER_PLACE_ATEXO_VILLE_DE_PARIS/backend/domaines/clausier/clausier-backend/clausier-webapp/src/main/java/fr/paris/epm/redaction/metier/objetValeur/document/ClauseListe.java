package fr.paris.epm.redaction.metier.objetValeur.document;

import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;

import java.util.List;
import java.util.stream.Collectors;

/**
 * classe ClauseListe qui manipule les formulations d'une clause.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseListe extends ClauseDocument {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    // une liste des formulations
    /**
     * liste de formulations d'une clause.
     */
    private List<Formulation> formulations;
    /**
     * attribut qui indique si les formulations sont modifiables.
     */
    private boolean formulationsModifiable;

    /**
     * cette méthode renvoie une collection de formulation.
     * @return collectionFormulation.
     */
    public final List<Formulation> getFormulations() {
        return formulations;
    }

    /**
     * cette méthode initialise la liste des formulations.
     * @param valeur pour initialiser la liste des formulations.
     */
    public final void setFormulations(final List<Formulation> valeur) {
        formulations = valeur;
    }

    /**
     * cette méthode indique si les formulations sont modifiables ou non.
     * @return vrai si les formulations sont modifiables, faux siNon.
     */
    public final boolean isFormulationsModifiable() {
        return formulationsModifiable;
    }

    /**
     * cette méthode initialise la l'attribut formulationsModifiable par une
     * valeur donnée.
     * @param valeur pour positionner l'attribut formulationsModifiable.
     */
    public final void setFormulationsModifiable(final boolean valeur) {
        formulationsModifiable = valeur;
    }

    /**
     * cette méthode permet la conversion du type en String.
     * @return la chaîne de caractère convertie.
     * @see java.lang.Object#toString().
     */
    public final String toString() {
        StringBuffer r = new StringBuffer("");
        if (formulations != null) {
            for (int i = 0; i < formulations.size(); i++) {
                r.append("\n     Formulation n°? " + (i + 1) + "\n")
                        .append(formulations.get(i).toString());

            }
        }
        return super.toString().concat(r.toString());
    }

    /**
     * @return clone de clause document.
     */
    public final Object cloneList(ChapitreDocument parent) {
        
        ClauseListe clauseListe = new ClauseListe() ;

        clauseListe.setParent(parent);
        if (getInfoBulle() != null) {
            clauseListe.setInfoBulle((InfoBulle) getInfoBulle().clone());
        }

        clauseListe.setId(getId());
        clauseListe.setRef(getRef());
        clauseListe.setType(getType());
        clauseListe.setTexteFixeAvant(getTexteFixeAvant());
        clauseListe.setTexteFixeApresAlaLigne(isTexteFixeApresAlaLigne());
        clauseListe.setTexteFixeAvantAlaLigne(isTexteFixeAvantAlaLigne());
        clauseListe.setTexteFixeApres(getTexteFixeApres());
        clauseListe.setTerminer(isTerminer());
        clauseListe.setTexteModifiable(isTexteModifiable());
        clauseListe.setEtat(getEtat());
        clauseListe.setActif(getActif());

        clauseListe.setFormulationsModifiable(formulationsModifiable);
        
        if (formulations != null && formulations.size() > 0){
            clauseListe.setFormulations(formulations.stream()
                    .map(formulation -> (Formulation) formulation.clone())
                    .collect(Collectors.toList()));
        }
        return clauseListe;
    }

    @Override
    public boolean isClauseVide() {
    	if (!super.isClauseDocumentVide())
            return false;

        boolean sansFormulationPrecoche = true;
        if (formulations != null && formulations.size() > 0) {
            for (Formulation formulation : formulations) {
                if (formulation.isPrecochee()) {
                    if (formulation.getLibelle() == null || formulation.getLibelle().isEmpty())
                        return true;
                    else
                        sansFormulationPrecoche = false;
                }
            }
        }
        return sansFormulationPrecoche;
    }

}
