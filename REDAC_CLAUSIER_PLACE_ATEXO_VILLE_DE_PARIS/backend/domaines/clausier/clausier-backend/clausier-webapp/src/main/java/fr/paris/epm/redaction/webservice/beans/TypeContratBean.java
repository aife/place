package fr.paris.epm.redaction.webservice.beans;

import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeContrat;

/**
 * Bean de presentation d'un TypeContrat
 *
 * @author MCA
 */
public class TypeContratBean {

    public final static String CANEVAS = "CANEVAS";
    public final static String CLAUSE = "CLAUSE";
    private String objet;
    private String reference;
    private String typeContrat;

    public TypeContratBean(EpmTCanevas canevas, EpmTRefTypeContrat typeContrat) {
        this.objet = "CANEVAS";
        this.reference = canevas.getReference();
        this.typeContrat = typeContrat.getAbreviation();
    }

    public TypeContratBean(EpmTClause clause, EpmTRefTypeContrat typeContrat) {
        this.objet = "CLAUSE";
        this.reference = clause.getReference();
        this.typeContrat = typeContrat.getAbreviation();
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

}