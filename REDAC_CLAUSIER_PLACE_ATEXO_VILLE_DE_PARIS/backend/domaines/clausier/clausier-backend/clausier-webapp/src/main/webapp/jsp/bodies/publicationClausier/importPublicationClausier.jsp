<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<!--Debut main-part-->
<div class="main-part">
    <div class="table panel panel-default">
        <div class="panel-body">
            <div class="pull-left text-primary m-t-1">
                <spring:message code="publicationClausier.importPublicationClausier.upload" />
            </div>
            <div class="actions text-right">
                <button type="button" data-toggle="modal" data-target="#modalUpload" class="btn bt-sm btn-primary"
                        title="<spring:message code="publicationClausier.importPublicationClausier.upload.button" />" >
                    <i class="fa fa-upload"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin main-part-->

<!--Debut modale-->
<div class="modal" id="modalUpload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <spring:message code="publicationClausier.importPublicationClausier.upload" />
            </div>
            <div class="modal-body">
                <form:form action="importPublicationsClausier.htm" method="post" enctype="multipart/form-data" cssClass="form-horizontal">
                    <div class="form-group form-group-sm">
                        <label class=" col-md-4 col-sm-4 col-xs-12 control-label">
                            <spring:message code="publicationClausier.importPublicationClausier.selectionnez" />
                        </label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <input type="file" name="fileImport" accept=".zip"/>
                        </div>
                    </div>

                    <div class="clearfix">
                        <button type="button" id="annuler1" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <spring:message code="redaction.action.annuler" />
                        </button>
                        <button type="submit" class="btn btn-primary btn-sm pull-right">
                            <spring:message code="redaction.action.valider" />
                        </button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<!--Fin modale-->