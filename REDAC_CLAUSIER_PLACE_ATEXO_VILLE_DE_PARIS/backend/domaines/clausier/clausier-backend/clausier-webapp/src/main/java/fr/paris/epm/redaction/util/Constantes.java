/**
 * 
 */
package fr.paris.epm.redaction.util;

import fr.paris.epm.global.commun.ConstantesGlobales;

/**
 * Rassemble l'ensemble des constantes commun à toutes les classes.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public abstract class Constantes extends ConstantesGlobales {
    /**
     * String values pour réaliser des vérifications
     */
    public static final String OUI = "oui";
    
    public static final String NON = "non";
    
    /**
     * L'identifiant du document chargé par le jeu de donnée.
     */
    public static final int ID_DOCUMENT = 12;
    /**
     * L'identifiant qui n'existe pas en base.
     */
    public static final int ID_NON_EN_BASE = 0;
    /**
     * L'identifiant du canevas chargé par le jeu de donnée.
     */
    public static final int ID_CANEVAS = 53;
    /**
     * L'identifiant de la clause chargée pour le jeu de donnée.
     */
    public static final int ID_CLAUSE = 200;
    /**
     * L'identifiant de la clause chargée pour le jeu de donnée.
     */
    public static final int ID_CLAUSE_LIST = 22;
    /**
     * Maximum boucle.
     */
    public static final int MAX_BOUCLE = 10;

    /**
     * L'identifiant du potentiellement conditionnee chargé par le jeu de
     * donnée.
     */
    public static final int ID_POT_CONDITIONNEE = 1;

    /**
     * Identifiant de l'utilisateur.
     */
    public static final int ID_UTILISATEUR = 0;
    /**
     * Le nom du fichier de jeu de données.
     */
    public static final String FICHIER_JEU_DONNEES = "persistance/jeuDonnees.xml";

    /**
     * Le nombre de type de clause.
     */
    public static final int NBR_TYPE_CLAUSE = 7;

    /**
     * L'identifiant de la consultation.
     */
    public static final int ID_CONSULTATION = 21;
    /**
     * L'identifiant de la clause texte fixe dans le jeu de données.
     */
    public static final int ID_CLAUSE_TEXTE_FIXE = 197;
    /**
     * Le nombre de type du potentiellement conditionnee.
     */
    public static final int NBR_TYPE_POTENTIELLEMENT_CONDITIONNE = 25;
    /**
     * Le nombre de thème d'une clause.
     */
    public static final int NBR_THEME_CLAUSE = 9;
    /**
     * Le nombre de type de document.
     */
    public static final int NBR_TYPE_DOCUMENT = 4;
    /**
     * le nombre des valeurs héritées.
     */
    public static final int NBR_VALEUR_HERITEE = 20;
    /**
     * Le nombre des valeurs tableau.
     */
    public static final int NBR_VALEUR_TABLEAU = 7;
    /**
     * L'identifiant de la nature de prestation.
     */
    public static final int ID_NATURE_PRESTATION = 2;
    /**
     * L'identifiant de la procedure de passation.
     */
    public static final int ID_PROCEDURE_PASSATION = 3;
    /**
     * L'identifiant du pouvoir adjudicateur.
     */
    public static final int ID_POUVOIR_ADJUDICATEUR = 2;
    /**
     * Le nombre des pouvoirs adjudicateurs.
     */
    public static final int NBR_POUVOIR_ADJUDICATEUR = 7;
    /**
     * le nombre des natures de préstations.
     */
    public static final int NBR_NATURE_PRESTATION = 4;
    /**
     * Le nombre de procedure de passation.
     */
    public static final int NBR_PROCEDURE_PASSATION = 7;
    /**
     * Le nombre de directions et services.
     */
    public static final int NBR_DIRECTION_SERVICE = 4;

    /**
     * Action d'enregistrement d'une clause
     */
    public static final String ENREGISTRER_CLAUSE = "enregistrer";

    /**
     * Action de demande de validation de clause
     */
    public static final String DEMANDER_VALIDATION_CLAUSE = "demanderValidation";
    /**
     * Action de refus de validation de clause
     */
    public static final String REFUSER_CLAUSE = "refuser";
    /**
     * Action de validation de clause
     */
    public static final String VALIDER_CLAUSE = "valider";
    /**
     * Action de suppression de clause
     */
    public static final String SUPPRIMER_CLAUSE = "supprimer";
    
    /**
     * Statut lors de la création
     */
    public static final int BROUILLON = 1;
    /**
     * Statut après demande de validation
     */
    public static final int A_VALIDER = 2;
    /**
     * Statut après validation
     */
    public static final int VALIDEE = 3;
    
    public static final String IDENTIFIANT_TABLEAU_DYNAMIQUE = "tableau_dynamique_";
    
    public static final String INFO_BULLE_OUI = "1";
    
    public static final String POT_CDT_OUI = "1";
    
    public static final String DATE_FORMAT_DD_MM_YYYY_HH_mm = "dd/MM/yyyy HH:mm";
    
    public static final String MARCHE_RESERVE = "Le marché est réservé à des ESAT/EA";
    
    public static final String MARCHE_NON_RESERVE = "Le marché n’est pas réservé à des ESAT/EA";
    public static final String TEXTE_FIXE = "Texte fixe";
    public static final String TOUTES_CLAUSES = "Toutes clauses";
    public static final String CHOIX_EXCLUSIF = "Choix exclusif";
    public static final String CHOIX_MULTIPLE = "Choix multiple";
    public static final String CHAMP_LIBRE = "Champ libre";
    public static final String CHAMP_PREVALORISE = "Champ prévalorisé";
    public static final String VALEUR_HERITEE = "Valeur héritée";

}
