package fr.paris.epm.redaction.commun.worker;

import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.global.commun.worker.RsemTaskFactory;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Classe-Fabrique des Taches de Redaction.
 * Created by nty on 12/03/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Component
public class RedactionTaskFactory extends RsemTaskFactory {

    public RsemTask<FileTmp> newExportClausesTask(EpmTUtilisateur epmTUtilisateur, boolean editeur, String nameResultFile) {
        ExportClausesTask task = getContext().getBean(ExportClausesTask.class);
        task.setUtilisateur(epmTUtilisateur);
        task.setEditeur(editeur);
        task.setNameResultFile(nameResultFile);
        task.setPlateformeUuid(epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        task.setResultHref("downloadTaskResult.htm");
        return task;
    }

    public RsemTask<Map<String, Object>> newPreviewCanevasTask(EpmTUtilisateur epmTUtilisateur, boolean editeur,
                                                               int idCanevas, Integer idPublication) {
        PreviewCanevasTask task = getContext().getBean(PreviewCanevasTask.class);
        task.setUtilisateur(epmTUtilisateur);
        task.setEditeur(editeur);
        task.setIdCanevas(idCanevas);
        task.setIdPublication(idPublication);
        task.setResultHref("previewCanevas.htm");
        return task;
    }

    public RsemTask<PublicationClausierBean> newActivationPublicationTask(EpmTUtilisateur epmTUtilisateur, int idPublication) {
        ActivationPublicationTask task = getContext().getBean(ActivationPublicationTask.class);
        task.setUtilisateur(epmTUtilisateur);
        task.setIdPublication(idPublication);
        task.setResultHref("activationPublicationTaskResult.htm");
        return task;
    }

}
