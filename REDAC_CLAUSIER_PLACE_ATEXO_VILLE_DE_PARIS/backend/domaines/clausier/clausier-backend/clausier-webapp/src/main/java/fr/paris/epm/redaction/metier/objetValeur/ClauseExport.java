package fr.paris.epm.redaction.metier.objetValeur;

import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.commun.Clausier;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.util.Constantes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe Clause qui effectue des traitements sur une clause.
 *
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseExport implements Clausier {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * l'attribut identifiant.
     */
    private int idClause;

    /**
     * l'attribut identifiant de la publication.
     */
    private Integer idPublication;

    /**
     * l'attribut réference de la clause.
     */
    private String reference;

    /**
     * l'attribut texteFixeApres.
     */
    private String texteFixeApres;

    /**
     * l'attribut texteFixeApresAlaLigne.
     */
    private boolean texteFixeApresAlaLigne;

    /**
     * l'attribut texteFixeAvantAlaLigne.
     */
    private boolean texteFixeAvantAlaLigne;

    /**
     * l'attribut texteFixeAvant.
     */
    private String texteFixeAvant;
    /**
     * l'attribut date Modification de la clause.
     */
    private Date dateModification;

    /**
     * date à trier.
     */
    private Date date;
    /**
     * l'attribut date création de la clause.
     */
    private Date dateCreation;
    /**
     * l'attribut tème de la clause.
     */
    private String theme;
    /**
     * l'objet info Bulle.
     */
    private InfoBulleExport infoBulle;
    /**
     * l'objet chapitre.
     */
    private Chapitre parent;
    /**
     * l'attribut actif.
     */
    private boolean actif;
    /**
     * Etat de la clause.
     */
    private String etat = ETAT_DISPONIBLE;

    /**
     * l'attribut contenu de la clause.
     */
    private String contenu = "";

    /**
     * Type de clause.
     */
    private String libelleTypeClause;

    /**
     * Identifiant du type de clause.
     */
    private int idTypeClause;

    /**
     * Type de document pour lequel la clause est applicable.
     */
    private String libelleTypeDocument;

    /**
     * Libelle de la procédure pour laquelle la clause s'applique.
     */
    private String libelleProcedure;

    /**
     * l'attribut potentiellementApplicable.
     */
    private boolean potentiellementApplicable = false;

    /**
     * l'attribut potentiellementConditionnee 1.
     */

    private boolean potentiellementConditionnee1 = false;

    /**
     * l'attribut potentiellementConditionnee 2.
     */

    private boolean potentiellementConditionnee2 = false;

    /**
     * Libellé du critére de conditionnement.
     */
    private String libelleCritereConditionnement1;

    /**
     * Valeur appliqué au critére pour le conditionnement.
     */
    private String libelleValeurConditionnement1;

    /**
     * Libellé du critére de conditionnement 2.
     */
    private String libelleCritereConditionnement2;

    /**
     * Valeur appliqué au critére pour le conditionnement 2.
     */
    private String libelleValeurConditionnement2;

    /**
     * Libellé nature préstation.
     */
    private String libelleNaturePrestation;

    /**
     * l'attribut sautLigneTexteAvant.
     */

    private boolean sautLigneTexteAvant;
    /**
     * l'attribut sautLigneTexteApres.
     */

    private boolean sautLigneTexteApres;

    /**
     * Attribut qui montre si la formulation est modifiable ou non.
     */

    private String formulationModifiable;

    /**
     * attribut qui précise si la clause est parametrable par la direction.
     */

    private String parametrableDirection;
    /**
     * attribut qui precise si la clause est parametrable par l'agent.
     */

    private String parametrableAgent;

    private List<RoleClause> listeRoleClause;

    private String version;

    private String statut;

    private String auteur;

    /**
     * Liste des références des canevas utilisants la clause.
     */
    private List<String> referenceCanevas = new ArrayList<String>();

    /**
     * cette méthode vérifie si le chapitre possède l'infobulle passée en
     * parametre.
     *
     * @param bulle infobulle à chercher.
     * @return résultat du test.
     */
    public final boolean aEnfant(final InfoBulle bulle) {
        return (bulle == null) ? false : this.infoBulle.equals(bulle);
    }

    /**
     * cette méthode permet de renvoyer l'index de la clause en cours.
     *
     * @return index de la clause courante.
     * @throws Exception erreur technique.
     */
    public final int index() throws Exception {
        if (parent == null)
            throw new Exception("Parent vide, clause orpheline");

        int index = parent.getClauses().indexOf(this);

        if (index == -1)
            throw new Exception("erreur interne, non trouvée");

        return index;
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    public final int hashCode() {
        int result = 1;
        if (reference != null)
            result = Constantes.PREMIER * result + reference.hashCode();
        if (infoBulle != null)
            result = Constantes.PREMIER * result + infoBulle.hashCode();
        return result;
    }

    /**
     * cette méthode teste l'égalité des objets.
     *
     * @param obj correspond à l'objet à tester.
     * @return vrai si les deux objets sont égale, si non faux.
     * @see java.lang.Object#equals(java.lang.Object).
     */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ClauseExport))
            return false;

        final ClauseExport autre = (ClauseExport) obj;
        if (idClause != autre.idClause)
            return false;
        if (!reference.equals(autre.reference))
            return false;
        if (!idPublication.equals(autre.idPublication))
            return false;
        if (infoBulle != autre.infoBulle)
            return false;
        return true;
    }

    /**
     * cette méthode clone l'objet clause.
     *
     * @return clause.
     */
    public final Object clone() {
        ClauseExport clause = new ClauseExport();
        clause.idClause = idClause;
        clause.idPublication = idPublication;
        clause.reference = reference;
        clause.theme = theme;
        clause.contenu = contenu;
        clause.texteFixeApres = texteFixeApres;
        clause.texteFixeAvant = texteFixeAvant;
        clause.potentiellementConditionnee1 = potentiellementConditionnee1;
        clause.potentiellementConditionnee2 = potentiellementConditionnee2;
        clause.potentiellementApplicable = potentiellementApplicable;
        if (infoBulle != null)
            clause.infoBulle = (InfoBulleExport) infoBulle.clone();
        clause.parent = parent;
        clause.actif = actif;
        return clause;
    }

    /**
     * cette méthode permet la conversion de type en String.
     *
     * @return la chaine de caractère convertie en String.
     * @see java.lang.Object#toString()
     */
    public final String toString() {
        return "Clause" + "\nIdClause: " + idClause + "\nidPublication: " + idPublication +
                "\nRference: " + reference + "\nInfo-Bulle: " + infoBulle;
    }

    public int getIdClause() {
        return idClause;
    }

    public void setIdClause(int idClause) {
        this.idClause = idClause;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTexteFixeApres() {
        return texteFixeApres;
    }

    public void setTexteFixeApres(String texteFixeApres) {
        this.texteFixeApres = texteFixeApres;
    }

    public boolean isTexteFixeApresAlaLigne() {
        return texteFixeApresAlaLigne;
    }

    public void setTexteFixeApresAlaLigne(boolean texteFixeApresAlaLigne) {
        this.texteFixeApresAlaLigne = texteFixeApresAlaLigne;
    }

    public boolean isTexteFixeAvantAlaLigne() {
        return texteFixeAvantAlaLigne;
    }

    public void setTexteFixeAvantAlaLigne(boolean texteFixeAvantAlaLigne) {
        this.texteFixeAvantAlaLigne = texteFixeAvantAlaLigne;
    }

    public String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    public void setTexteFixeAvant(String texteFixeAvant) {
        this.texteFixeAvant = texteFixeAvant;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public InfoBulleExport getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulleExport infoBulle) {
        this.infoBulle = infoBulle;
    }

    public Chapitre getParent() {
        return parent;
    }

    public void setParent(Chapitre parent) {
        this.parent = parent;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getLibelleTypeClause() {
        return libelleTypeClause;
    }

    public void setLibelleTypeClause(String libelleTypeClause) {
        this.libelleTypeClause = libelleTypeClause;
    }

    public int getIdTypeClause() {
        return idTypeClause;
    }

    public void setIdTypeClause(int idTypeClause) {
        this.idTypeClause = idTypeClause;
    }

    public String getLibelleTypeDocument() {
        return libelleTypeDocument;
    }

    public void setLibelleTypeDocument(String libelleTypeDocument) {
        this.libelleTypeDocument = libelleTypeDocument;
    }

    public String getLibelleProcedure() {
        return libelleProcedure;
    }

    public void setLibelleProcedure(String libelleProcedure) {
        this.libelleProcedure = libelleProcedure;
    }

    public boolean isPotentiellementApplicable() {
        return potentiellementApplicable;
    }

    public void setPotentiellementApplicable(boolean potentiellementApplicable) {
        this.potentiellementApplicable = potentiellementApplicable;
    }

    public boolean isPotentiellementConditionnee1() {
        return potentiellementConditionnee1;
    }

    public void setPotentiellementConditionnee1(boolean potentiellementConditionnee1) {
        this.potentiellementConditionnee1 = potentiellementConditionnee1;
    }

    public boolean isPotentiellementConditionnee2() {
        return potentiellementConditionnee2;
    }

    public void setPotentiellementConditionnee2(boolean potentiellementConditionnee2) {
        this.potentiellementConditionnee2 = potentiellementConditionnee2;
    }

    public String getLibelleCritereConditionnement1() {
        return libelleCritereConditionnement1;
    }

    public void setLibelleCritereConditionnement1(String libelleCritereConditionnement1) {
        this.libelleCritereConditionnement1 = libelleCritereConditionnement1;
    }

    public String getLibelleValeurConditionnement1() {
        return libelleValeurConditionnement1;
    }

    public void setLibelleValeurConditionnement1(String libelleValeurConditionnement1) {
        this.libelleValeurConditionnement1 = libelleValeurConditionnement1;
    }

    public String getLibelleCritereConditionnement2() {
        return libelleCritereConditionnement2;
    }

    public void setLibelleCritereConditionnement2(String libelleCritereConditionnement2) {
        this.libelleCritereConditionnement2 = libelleCritereConditionnement2;
    }

    public String getLibelleValeurConditionnement2() {
        return libelleValeurConditionnement2;
    }

    public void setLibelleValeurConditionnement2(String libelleValeurConditionnement2) {
        this.libelleValeurConditionnement2 = libelleValeurConditionnement2;
    }

    public String getLibelleNaturePrestation() {
        return libelleNaturePrestation;
    }

    public void setLibelleNaturePrestation(String libelleNaturePrestation) {
        this.libelleNaturePrestation = libelleNaturePrestation;
    }

    public boolean isSautLigneTexteAvant() {
        return sautLigneTexteAvant;
    }

    public void setSautLigneTexteAvant(boolean sautLigneTexteAvant) {
        this.sautLigneTexteAvant = sautLigneTexteAvant;
    }

    public boolean isSautLigneTexteApres() {
        return sautLigneTexteApres;
    }

    public void setSautLigneTexteApres(boolean sautLigneTexteApres) {
        this.sautLigneTexteApres = sautLigneTexteApres;
    }

    public String getFormulationModifiable() {
        return formulationModifiable;
    }

    public void setFormulationModifiable(String formulationModifiable) {
        this.formulationModifiable = formulationModifiable;
    }

    public String getParametrableDirection() {
        return parametrableDirection;
    }

    public void setParametrableDirection(String parametrableDirection) {
        this.parametrableDirection = Util.encodeCaractere(parametrableDirection);
    }

    public String getParametrableAgent() {
        return parametrableAgent;
    }

    public void setParametrableAgent(String parametrableAgent) {
        this.parametrableAgent = Util.encodeCaractere(parametrableAgent);
    }

    public List<RoleClause> getListeRoleClause() {
        return listeRoleClause;
    }

    public void setListeRoleClause(List<RoleClause> listeRoleClause) {
        this.listeRoleClause = listeRoleClause;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public List<String> getReferenceCanevas() {
        return referenceCanevas;
    }

    public void setReferenceCanevas(List<String> referenceCanevas) {
        this.referenceCanevas = referenceCanevas;
    }

}
