package fr.paris.epm.redaction.urlvie;

import fr.atexo.urlvie.toolbox.helpers.impl.AbstractHelper;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

public class DataSourceHelper extends AbstractHelper {

    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        String initial = (String) parametres
                .get(ParametrageDataSourceTesteur.JAVA_NAMING_FACTORY_INITIAL.toString());
        String pkgs = (String) parametres
                .get(ParametrageDataSourceTesteur.JAVA_NAMING_FACTORY_URL_PKGS.toString());
        String prov = (String) parametres.get(ParametrageDataSourceTesteur.JAVA_NAMING_PROVIDER_URL
                .toString());
        String name = (String) parametres.get(ParametrageDataSourceTesteur.DATASOURCE_JNDINAME
                .toString());
        String sql = (String) parametres.get(ParametrageDataSourceTesteur.SQL.toString());

        Properties ppt = new Properties();
        ppt.setProperty(initial.split("=")[0].trim(), initial.split("=")[1].trim());
        ppt.setProperty(pkgs.split("=")[0].trim(), pkgs.split("=")[1].trim());
        ppt.setProperty(prov.split("=")[0].trim(), prov.split("=")[1].trim());

        InitialContext ctx;
        try {
            ctx = new InitialContext(ppt);
            DataSource datasource = (DataSource) ctx.lookup(name);
            getReponse(reponseBrute, datasource, sql);
        } catch (NamingException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        }
    }

    private void getReponse(ReponseBrute reponse, DataSource datasource, String sql) {
        Connection conn = null;
        try {
            conn = datasource.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement prepSt = conn.prepareStatement(sql);
            prepSt.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponse.getExceptionsInterceptees().add(e);
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
                log.debug(e.getMessage(), e);
            } finally {
                if (conn != null) {
                    try {
                        if (conn != null) {
                            conn.close();
                        }
                    } catch (SQLException e) {
                        log.error(e.getMessage());
                        log.debug(e.getMessage(), e);
                    }
                }
            }
        }
    }

}
