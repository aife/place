package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.redaction.metier.objetValeur.document.ParagrapheDocument;
import org.jdom.Element;

/**
 * la classe ParagrapheUtil effectue des conversions de type sur un paragraphe
 * d'un document, implemente l'intrface XmlDocument.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ParagrapheUtil implements XmlDocument {
    /**
     * Cette méthode permet la conversion d'une paragraphe XML vers un objet
     * paragraphe.
     * @param xmlParagraphe element à convertir.
     * @return Objet java Paragraphe.
     */
    public static ParagrapheDocument elementParagrapheXMLVersParagrahe(
            final Element xmlParagraphe)  {
        if (xmlParagraphe != null) {
            ParagrapheDocument paragrapheDocument = new ParagrapheDocument();
            // paragrapheDocument.setId(
            // xmlParagraphe.getAttribute(PARAGRAPHE_ID).getIntValue());
            paragrapheDocument.setTexte(xmlParagraphe.getText());
            return paragrapheDocument;
        } else {
            return null;
        }
    }

    /**
     * Cette méthode permet la conversion d'un paragraphe du document vers un
     * paragraphe XML.
     * @param paragrapheDocument element à convertir.
     * @return paragraphe XML.
     */
    public static Element versJdom(
            final ParagrapheDocument paragrapheDocument) {
        if (paragrapheDocument != null) {
            Element paragraphe = new Element(PARAGRAPHE_NOM);
            paragraphe.setText(paragrapheDocument.getTexte());
            return paragraphe;
        } else {
            return null;
        }
    }

}
