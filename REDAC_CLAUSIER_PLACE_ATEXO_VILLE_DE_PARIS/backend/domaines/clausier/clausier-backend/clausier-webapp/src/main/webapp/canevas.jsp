<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<script language="javascript" src="fr.paris.epm.redaction.canevas.nocache.js"></script>

<iframe id="__gwt_historyFrame" style="width: 0; height: 0; border: 0">

</iframe>

<c:set var="action" value="creer" />
<c:if test="${typeAction == 'M'}">
    <c:set var="action" value="modifier" />
</c:if>

<c:set var="actionEditeur" value="" />
<c:if test="${editeur != null && editeur == true }">
    <c:set var="actionEditeur" value="Editeur" />
</c:if>

<div class="main-part canevas">
    <div class='breadcrumbs'>
        <bean:message key="canevas${actionEditeur}.titre.${action}"/>
    </div>
    <atexo:fichePratique reference="FPR04" key="common.fichePratique"/>
    <div class='breaker'></div>
    <div id="notificationClauses"></div>
    <div id="main-part">
    </div>
</div>
