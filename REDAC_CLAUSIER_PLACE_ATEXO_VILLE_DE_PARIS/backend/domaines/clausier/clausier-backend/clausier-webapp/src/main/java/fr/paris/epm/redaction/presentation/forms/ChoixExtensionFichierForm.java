package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.redaction.EpmTRefExtensionFichier;
import org.apache.struts.action.ActionForm;

import java.util.Collection;

/**
 * formulaire de choix de l'extension de génération du document
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public class ChoixExtensionFichierForm extends ActionForm {

    /**
     * Marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut numero de consultation.
     */
    private String extension;

    /**
     * Ensemble des extensions possibles pour la génération d'un fichier
     */
    private Collection<EpmTRefExtensionFichier> extensions;

    /**
     * Cette methode pour initialiser le formulaire.
     */
    public void reset() {
        extension = null;
    }

    /**
     * @return the extension
     */
    public final String getExtension() {
        return extension;
    }

    /**
     * @param extension the extension to set
     */
    public final void setExtension(final String valeur) {
        this.extension = valeur;
    }

    /**
     * @return the extensions
     */
    public final Collection<EpmTRefExtensionFichier> getExtensions() {
        return extensions;
    }

    /**
     * @param extensions the extensions to set
     */
    public final void setExtensions(final Collection<EpmTRefExtensionFichier> valeur) {
        this.extensions = valeur;
    }
    
}
