INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (9, 'Tableau de critére(s) d''attribution', 'listeCritereAttribution');

INSERT INTO epm__t_ref_valeur_heritee(
            id, libelle, nom_attribut_consultation)
    VALUES (21, 'Nombre d''heures (clauses sociales)', 'nombreHeureClauseSociale');
INSERT INTO epm__t_ref_valeur_heritee(
            id, libelle, nom_attribut_consultation)
    VALUES (22, 'Pourcentage (clauses sociales)', 'pourcentageClauseSociale');    
INSERT INTO epm__t_ref_valeur_heritee(
            id, libelle, nom_attribut_consultation)
    VALUES (23, 'Nombre d''heures (clauses sociales) pour le lot n ', 'nombreHeureClauseSocialeLot()');
INSERT INTO epm__t_ref_valeur_heritee(
            id, libelle, nom_attribut_consultation)
    VALUES (24, 'Pourcentage (clauses sociales) pour le lot n ', 'pourcentageClauseSocialeLot()');  