package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Service
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Service
public class ClauseServiceImpl extends GeneriqueService implements ClauseService {

    private final Logger logger = LoggerFactory.getLogger(ClauseServiceImpl.class);

    private NoyauProxy noyauProxy;

    @Autowired
    public void setNoyauProxy(NoyauProxy noyauProxy) {
        this.noyauProxy = noyauProxy;
    }

    @Override
    public List<EpmTClause> chercherListeClausesPourCanevasMinisterielle(final String referenceCanevas, final Integer organisme) {
        return this.getRedactionService().chercherListeClausesPourCanevasMinisterielle(referenceCanevas, organisme);
    }

    @Override
    public EpmTClause chercherDerniereSurchargeActif(final Integer idClause, final EpmTRefOrganisme organisme) {
        ClauseCritere criteres = new ClauseCritere(organisme.getPlateformeUuid());
        criteres.setIdOrganisme(organisme.getId());
        criteres.setIdClauseOrigine(idClause);
        criteres.setSurchargeActif(true);
        List<EpmTClause> epmTObjects = this.chercherEpmTObject(0, criteres);
        return CollectionUtils.isEmpty(epmTObjects) ? null : epmTObjects.get(0);
    }

    @Override
    public EpmTClause chercherClauseSurcharge(final Integer idClause, final EpmTRefOrganisme organisme) {
        ClauseCritere criteres = new ClauseCritere(organisme.getPlateformeUuid());
        criteres.setIdOrganisme(organisme.getId());
        criteres.setIdClauseOrigine(idClause);
        List<EpmTClause> epmTObjects = this.chercherEpmTObject(0, criteres);
        return CollectionUtils.isEmpty(epmTObjects) ? null : epmTObjects.get(0);
    }

    @Override
    public List<EpmTClause> chercherListeClausesPourCanevasInterministerielle(String referenceCanevas) {
        return this.getRedactionService().chercherListeClausesPourCanevasInterministerielle(referenceCanevas);
    }
}
