package fr.paris.epm.redaction.presentation.bean;

public class ClauseSearch extends CommunSearch {


    private String motsCles;

    private int idThemeClause; // ou Integer ?

    private Integer idTypeClause;
    private Boolean parametrableDirection;

    private Boolean parametrableAgent;

    public void setMotsCles(String motsCles) {
        this.motsCles = motsCles;
    }

    public void setIdThemeClause(int idThemeClause) {
        this.idThemeClause = idThemeClause;
    }

    public String getMotsCles() {
        return motsCles;
    }

    public int getIdThemeClause() {
        return idThemeClause;
    }


    public Integer getIdTypeClause() {
        return idTypeClause;
    }

    public void setIdTypeClause(Integer idTypeClause) {
        this.idTypeClause = idTypeClause;
    }

    public Boolean getParametrableDirection() {
        return parametrableDirection;
    }

    public void setParametrableDirection(Boolean parametrableDirection) {
        this.parametrableDirection = parametrableDirection;
    }

    public Boolean getParametrableAgent() {
        return parametrableAgent;
    }

    public void setParametrableAgent(Boolean parametrableAgent) {
        this.parametrableAgent = parametrableAgent;
    }
}
