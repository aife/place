<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>

<!--Debut Main part-->
<div class="main-part">
    <!--Debut Message accueil-->
    <div class="message-accueil">
        <div class="form-saisie">
            <div class="form-bloc">
                <div class="top"><span class="left"></span><span class="right"></span></div>
                <div class="content">
                    <div class="titre">Bienvenue dans le Module Aide à la rédaction des pièces administratives</div>
                    Le Module d'aide à la rédaction des pièces propose les fonctionnalités de rédaction des pièces
                    administratives (AE, CCAP, RC) à partir de modèles type. <br/>
                    Ce Module propose aussi les fonctionnalités d'administration des clauses et des canevas
                    type.<br/><br/>
                    <a href="RechercherDocumentInit.epm" class="ajout-el">Gérer les documents</a>
                    <br/><br/>
                    L'ensemble des fonctionnalités du Module est accessible depuis le menu gauche.
                </div>

                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>
        </div>
    </div>
    <!--Fin Message accueil-->
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="breaker"></div>
</div>