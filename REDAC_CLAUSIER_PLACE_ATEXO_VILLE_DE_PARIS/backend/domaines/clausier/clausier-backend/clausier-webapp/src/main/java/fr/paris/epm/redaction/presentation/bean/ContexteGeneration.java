package fr.paris.epm.redaction.presentation.bean;

import java.util.Objects;

public class ContexteGeneration {

	final String refConsultation;
	final String refCanevas;
	final Integer idOrganisme;
	final String type;
	final Integer idLot;

	public ContexteGeneration( String refConsultation, String refCanevas, Integer idOrganisme, String type, Integer idLot) {
		this.refConsultation = refConsultation;
		this.refCanevas = refCanevas;
		this.idOrganisme = idOrganisme;
		this.type = type;
		this.idLot = idLot;
	}

	public String getRefConsultation() {
		return refConsultation;
	};

	public String getRefCanevas() {
		return refCanevas;
	}

	public Integer getIdOrganisme() {
		return idOrganisme;
	}

	public String getType() {
		return type;
	}

	public Integer getIdLot() {
		return idLot;
	}

	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( o == null || getClass() != o.getClass() ) return false;
		ContexteGeneration that = (ContexteGeneration) o;
		return Objects.equals(refConsultation, that.refConsultation) && Objects.equals(refCanevas, that.refCanevas) && Objects.equals(idOrganisme, that.idOrganisme) && Objects.equals(type, that.type) && Objects.equals(idLot, that.idLot);
	}

	@Override
	public int hashCode() {
		return Objects.hash(refConsultation, refCanevas, idOrganisme, type, idLot);
	}
}
