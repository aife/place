<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--Debut bloc Infos clause-->
<div class="form-saisie">
	<div class="form-bloc">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>
		<div class="content">
		<h2 class="float-left">
			<html:radio property="clauseSelectionnee" styleId="clauseEditeurSurchargeeSelectionnee" value="clauseEditeurSurcharge"></html:radio>
			<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeurSurchargee"/></label>
		</h2>
		<div class="actions-clause no-padding">
			<logic:equal name="typeAction" value="M">
				<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
			</logic:equal>
			<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
		</div>
<!-- 		<div class="info-maj"> -->
<%-- 		<bean:message key="clause.surcharge.modifieeLe"/> --%>
<%-- 			<c:out value="TODO" /> --%>
<!-- 		</div> -->		
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTexteLibre.txt.texteFixeAvant" /></span>
				<html:textarea property="textFixeAvant" styleId="textFixeAvant" title="Texte fixe avant"
					cols="" rows="6" styleClass="texte-long mceEditor" errorStyleClass="error-border"/>
			</div>
			<div class="line">
                   <div class="retour-ligne"><html:checkbox property="sautTextFixeAvant" styleId="sautTextFixeAvant" value="true" title="Saut de ligne" />
                   <bean:message key="redaction.txt.sautLigne"/>
                   <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
                   </div>
                  </div>
                  <div class="separator"></div>
			<div class="line">
				<span class="intitule"><bean:message key="ClauseTexteLibre.txt.tailleChamp" /></span>
				<span class="intitule-auto"> 
								<c:set var="nbCaractValue" value="1"/>
								<c:if test="${not empty(frmClauseTexteLibre.nbCaract)}">
									<c:set var="nbCaractValue" value="${frmClauseTexteLibre.nbCaract}"/>
								</c:if>
        								<html:select property="nbCaract" styleId="nbCaract" title="Taille de Champ" styleClass="auto" value="${nbCaractValue}">
        									<html:option value="4">
						               <bean:message key="redaction.taille.champ.tresLong" />
						            </html:option>
						            <html:option value="1">
						               <bean:message key="redaction.taille.champ.long" />
						            </html:option>
						            <html:option value="2">
						               <bean:message key="redaction.taille.champ.moyen" />
						            </html:option>
						            <html:option value="3">
 								               <bean:message key="redaction.taille.champ.court" />
 								            </html:option>
             							 </html:select>
				</span>
			</div>
			
			<c:if test="${nbCaractValue == '0' }">
				<script>
					mettreTailleChampValeurDefaut('nbCaract');
				</script>
			</c:if>
			
			<div class="line">
				<span class="intitule"><bean:message key="ClauseTexteLibre.txt.champLibreObligatoire" /></span>
				<div class="radio-choice">
					<html:radio property="champLibreObligatoire" styleId="champLibreObligatoire" title="Oui"
						value="1" />
					<bean:message key="ClauseTexteLibre.txt.champLibreObligatoireOui" />
				</div>
				<div class="radio-choice">
					<html:radio property="champLibreObligatoire" title="Non" styleId="champLibreObligatoire"
						value="0" />
					<bean:message key="ClauseTexteLibre.txt.champLibreObligatoireNon" />
				</div>
			</div>
			<div class="line">
                   <div class="retour-ligne"><html:checkbox property="sautTextFixeApres" styleId="sautTextFixeApres" value="true" title="Saut de ligne" />
                   <bean:message key="redaction.txt.sautLigne"/>
                   <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" /></div>
                  </div>
                  
                  <div class="separator"></div>
			<div class="line">
				<span class="intitule-bloc"><bean:message key="ClauseTexteLibre.txt.texteFixeApres" /></span>
                <html:textarea property="textFixeApres" styleId="textFixeApres" title="Texte fixe après"
                               cols="" rows="6" styleClass="texte-long mceEditor" />
			</div>
			
			<div class="actions-clause">
				<logic:equal name="typeAction" value="M">
					<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
				</logic:equal>
				<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
			</div>
			
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span><span class="right"></span>
		</div>
	</div>
</div>
<script type="text/javascript">
	initEditeursTexteRedactionSurchargeClauses();
</script>