package fr.paris.epm.redaction.presentation.views;

public class Contrat {

	private Integer id;

	private String libelle;

	public Integer getId() {
		return id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}
}
