package fr.paris.epm.redaction.importExport.bean;

import java.util.Set;

public class ClausePotentiellementConditionneeImportExport {

    private int idClausePotentiellementConditionnee;

    private String refPotentiellementConditionnee;

    private Set<ClauseValeurPotentiellementConditionneeImportExport> clausesValeursPotentiellementConditionnee;

    public int getIdClausePotentiellementConditionnee() {
        return idClausePotentiellementConditionnee;
    }

    public void setIdClausePotentiellementConditionnee(int idClausePotentiellementConditionnee) {
        this.idClausePotentiellementConditionnee = idClausePotentiellementConditionnee;
    }

    public String getRefPotentiellementConditionnee() {
        return refPotentiellementConditionnee;
    }

    public void setRefPotentiellementConditionnee( String refPotentiellementConditionnee ) {
        this.refPotentiellementConditionnee = refPotentiellementConditionnee;
    }

    public Set<ClauseValeurPotentiellementConditionneeImportExport> getClausesValeursPotentiellementConditionnee() {
        return clausesValeursPotentiellementConditionnee;
    }

    public void setClausesValeursPotentiellementConditionnee( Set<ClauseValeurPotentiellementConditionneeImportExport> clausesValeursPotentiellementConditionnee ) {
        this.clausesValeursPotentiellementConditionnee = clausesValeursPotentiellementConditionnee;
    }

}
