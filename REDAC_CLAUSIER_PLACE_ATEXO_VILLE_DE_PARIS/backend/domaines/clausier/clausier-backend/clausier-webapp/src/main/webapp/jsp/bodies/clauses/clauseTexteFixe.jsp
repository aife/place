<!--Debut main-part-->
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%--@elvariable id="epmTClauseInstance" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>
<%--@elvariable id="frmClauseTexteFixe" type="fr.paris.epm.redaction.presentation.forms.ClauseTexteFixeForm"--%>

<div class="form-bloc">
    <div class="top">
        <span class="left"></span><span class="right"></span>
    </div>
    <div class="content">
        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <%-- TODO: NIKO REVOIR
                <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">
                    <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                </c:if>
                --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiser();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>
        <div class="breaker"></div>

        <div class="line">
            <span class="intitule"><bean:message key="ClauseTexteFixe.txt.texteFixe" /> </span>
            <html:textarea property="texteFixe" styleId="texteFixe" title="Texte fixe" cols="" 	rows="6" styleClass="texte-long mceEditor" errorStyleClass="error-border" />
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <%-- TODO: NIKO REVOIR
                <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">
                    <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                </c:if>
                --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiser();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom">
        <span class="left"></span><span class="right"></span>
    </div>
</div>

<div class="spacer"></div>

<script type="text/javascript">
    initEditeursTexteRedaction();
</script>