package fr.paris.epm.redaction.metier.documentHandler;

/**
 * Interface XmlDocument comporte les données d'un document XML.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public interface XmlDocument {
    /**
     * Nom du document.
     */
    String DOCUMENT_NOM = "document";
    /**
     * Identifiant du document.
     */
    String DOCUMENT_ID = "id";
    /**
     * Référence du document.
     */
    String DOCUMENT_REF = "ref";
    /**
     * Nom du chapitre d'un document.
     */
    String CHAPITRE_NOM = "chapitre";
    /**
     * Nom info-bulle.
     */
    String BULLE_NOM = "infoBulle";
    /**
     * Position de la déscription de l'info-bulle.
     */
    int BULLE_POS_DESCRIPTION = 0;
    /**
     * Position du lien de l'info-bulle.
     */
    int BULLE_POS_LIEN = 1;
    /**
     * Position de la déscription du lien de l'info-bulle.
     */
    int BULLE_POS_DESCRIPTION_LIEN = 1;
    /**
     * Position de l'URL du lien de l'info-bulle.
     */
    int BULLE_POS_URL = 0;
    /**
     * Identifiant du paragraphe.
     */
    String PARAGRAPHE_ID = "id";
    /**
     * Contenu du paragraphe.
     */
    String PARAGRAPHE_TEXTE = "texte";
    /**
     * Nom du paragraphe.
     */
    String PARAGRAPHE_NOM = "paragraphe";
    /**
     * Identifiant du chapitre.
     */
    String CHAPITRE_ID = "id";
    /**
     * Référence du chapitre.
     */
    String CHAPITRE_REF = "ref";
    /**
     * Titre du chapitre.
     */
    String CHAPITRE_TITRE = "titre";
    /**
     * Numéro du chapitre.
     */
    String CHAPITRE_NUMERO = "numero";
    /**
     * Validité du chapitre.
     */
    String CHAPITRE_VALIDE = "valide";
    /**
     * Attribut d'une balise chapitre contenu dans le xmlData d'une clause.
     */
    String CHAPITRE_NOMBRE_SAUT_LIGNES = "nombreSautLignes";
    
    /**
     * Position de l'info-bulle du chapitre.
     */
    int CHAPITRE_POS_BULLE = 0;
    /**
     * Position du paragraphe du chapitre.
     */
    int CHAPITRE_POS_PARAGRAPHE = 1;
    
    String CHAPITRE_STYLE = "style";
    /**
     * Nom de la clause.
     */
    String CLAUSE_NOM = "clause";

    /**
     * Lien de l'info-bulle.
     */
    String BULLE_LIEN = "lien";

    /**
     * Activité de l'info-bulle.
     */
    String BULLE_ACTIF = "actif";

    /**
     * Identifiant de la clause.
     */
    String CLAUSE_ID = "id";

    /**
     * Type de la clause.
     */
    String CLAUSE_TYPE = "type";

    /**
     * Formulations modifiables dans clause.
     */
    String CLAUSE_FORMULATIONS_MODIFIABLE = "CLAUSE_FORMULATIONS_MODIFIABLE";

    /**
     * Référence d'une clause.
     */
    String ClAUSE_REF = "ref";

    /**
     * etat de la clause.
     */
    String ClAUSE_ETAT = "etat";

    /**
     * Clause actif ou non.
     */
    String ClAUSE_ACTIVE = "active";

    /**
     * Lien URL.
     */
    String LIEN_URL = "url";
    /**
     * Déscription de l'Info-Bulle.
     */
    String BULLE_DESCRIPTION = "description";
    /**
     * Déscription du lien de l'info-bulle..
     */
    String LIEN_DESCRIPTION = "description";
    /**
     * Clause texte fixe avant.
     */
    String CLAUSE_TEXTE_FIXE_AVANT = "texteFixeAvant";
    /**
     * Clause texte fixe avant à la ligne.
     */
    String CLAUSE_TEXTE_FIXE_AVANT_A_LA_LIGNE = "texteFixeAvantALaLigne";
    /**
     * Clause texte fixe aprés à la ligne.
     */
    String CLAUSE_TEXTE_FIXE_APRES_A_LA_LIGNE = "texteFixeAresALaLigne";
    /**
     * Position du texte fixe avant d'une clause.
     */
    int CLAUSE_POS_TEXTE_FIXE_AVANT = 0;
    /**
     * Clause texte fixe aprés.
     */
    String CLAUSE_TEXTE_FIXE_APRES = "texteFixeApres";
    /**
     * Clause texte variable.
     */
    String CLAUSE_TEXTE_VARIABLE = "texte";
    /**
     * Position du texte variable d'une clause.
     */
    int CLAUSE_POS_TEXTE_VARIABLE = 3;
    /**
     * Position du formulaire liste type d'une clause.
     */
    int CLAUSE_POS_FORMULAIRE_LISTE_TYPE = 3;
    /**
     * Position du texte fixe aprés d'une clause.
     */
    int CLAUSE_POS_TEXTE_FIXE_APRES = 1;
    /**
     * Position de l'info bulle d'une clause.
     */
    int CLAUSE_POS_BULLE = 2;
    /**
     * Clause formulaire liste type.
     */
    String CLAUSE_FORMULAIRE_LISTE_TYPE = "formulairesListeType";
    /**
     * validité d'une clause.
     */
    String CLAUSE_VALIDE = "CLAUSE_VALIDE";
    /**
     * Clause provenant d'un document dupliqué
     */
    String CLAUSE_DUPLIQUE = "duplique";

    /**
     * formulations d'une clause.
     */
    String CLAUSE_FORMULATION = "formulation";
    /**
     * Exclusif.
     */
    String EXCLUSIF = "exclusif";
    /**
     * Precoche.
     */
    String PRECOCHE = "precoche";
    /**
     * Taille.
     */
    String TAILLE = "taille";
    /**
     * Texte Modifiable d'une clause.
     */
    String CLAUSE_TEXTE_MODIFIABLE = "modifiable";
    /**
     * Texte Obligatoire clause.
     */
    String CLAUSE_TEXTE_OBLIGATOIRE = "obligatoire";
    
    String ID_REFERENTIEL_VALEUR_TABLEAU = "idReferentielValeurTableau";
    
    String REFERENCE = "reference";
    
    String NOMBRE_COLONNE = "nombreColonne";
    
    String LIGNES = "lignes";
    
    String LIGNE = "ligne";
    
    String COLONNES = "colonnes";
    
    String COLONNE = "colonne";
    
    String CELLULE = "cellule";
    
    String LONGUEUR = "longueur";
    
    String VALIDATION_AUTOMATIQUE = "validationAutomatique";
    
    String DEROGATION = "derogation";
    
    String ARTICLE_DEROGATION = "article";
    
    String COMMENTAIRE_DEROGATION = "commentaire";
    
    String AFFICHER_NB_MESSAGE_DEROGATION = "nbMessage";
    
    String CLAUSE_TABLEAU_REDACTION = "tableauRedaction";
    
    String TEXT_AVANT_TABLEAU = "textAvantTableau";
    
    String DOCUMENT_XLS = "documentXLS";
    
    String ONGLET = "onglet";
    
    String COORDONNEE = "coordonnee";
    
    String TYPE = "type";
    
    String ARRIERE_PLAN = "arrierePlan";
    
    String  premierPlan ="premierPlan";
    
    String VALEUR = "valeur";
    
    String STYLE = "style";
    
    String REMPLISSAGE = "remplissage";
    
    String FONT ="font";
    
    String ALIGNEMENT ="alignement";
    
    String BORDURE ="bordure";
    
    String POLICE ="police";
    
    String GRAS ="gras";
    
    String ITALIC ="italic";
    
    String SOULIGNE ="souligne";
    
    String COULEUR ="couleur";
    
    String  ROTATION = "rotation";
    
    String HORIZONTAL = "horizontal";
    
    String VERTICAL = "vertical";
    
    String HAUT ="haut";
    
    String BAS ="bas";
    
    String GAUCHE= "gauche";
    
    String DROITE ="droite";
    
    String REGION_FUSIONEE = "regionFusionnee"; 
    
    String ATTRIBUTS = "attributs";
    
    String LARGEUR_COLONNE = "largeurColonne";
    
    String LARGEUR = "largeur";
    
    String LIGNE_DEBUT = "ligneDebut";
    
    String LIGNE_FIN = "ligneFin";
    
    String COLONNE_DEBUT = "colonneDebut";
    
    String COLONNE_FIN = "colonneFin";
      
}
