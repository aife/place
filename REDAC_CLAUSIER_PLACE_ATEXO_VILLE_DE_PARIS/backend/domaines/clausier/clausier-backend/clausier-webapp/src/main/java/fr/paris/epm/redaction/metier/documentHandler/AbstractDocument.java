package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commons.exception.TechnicalException;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * la classe abstraite document qui implemente l'interface XmlDocument.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractDocument implements XmlDocument {
    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDocument.class);

    /**
     * Cette méthode permet de changer des élments entrés en parametres selon un
     * format demandé.
     * @param valeurElement pour positionner l'objet Element.
     * @param valeurFormat pour positionner l'objet Format.
     * @return renvoie une chaîne de caractère contenant les données xml.
     * @throws TechnicalException erreur technique.
     */
    public static String toXml(final Element valeurElement, final Format valeurFormat) {
        StringWriter ecrivain = new StringWriter();
        try {
            XMLOutputter sortie = new XMLOutputter(valeurFormat);
            sortie.output(valeurElement, ecrivain);
        } catch (IOException e) {
            throw new TechnicalException(e);
        }
        return ecrivain.toString();
    }

    /**
     * Cette méthode permet de changer des élments entrés en parametres selon un
     * format Prédifini.
     * @param valeur pour positionner l'objet element.
     * @return String
     * @throws TechnicalException erreur technique.
     */
    public static String toXml(final Element valeur) {
        return toXml(valeur, Format.getRawFormat());
    }

    /**
     * cette méthode permet de renvoyer un objet de type Element.
     * @param valeurRoot pour initialiser l'objet Element.
     * @param valeurNomContenu pour initialiser le contenu.
     * @return renvoie un objet de type Element.
     */
    public static Element getElement(final Element valeurRoot, final String valeurNomContenu) {
        ElementFilter filtreClause = new ElementFilter(valeurNomContenu);
        if (valeurRoot != null) {
            List contenu = valeurRoot.getContent(filtreClause);
            if (contenu != null && !contenu.isEmpty()) {
                return (Element) contenu.get(0);
            }
        }
        LOG.debug("Aucun élément trouvé de type : {}", valeurNomContenu);
        return null;
    }

    /**
     * cette méthode permet de renvoyer une liste d'objets de type Element.
     * @param valeurRoot pour initialiser l'objet Element.
     * @param valeurNomContenu pour initialiser le contenu.
     * @return renvoie une liste d'objets de type Element.
     */
    public static List getElements(final Element valeurRoot, final String valeurNomContenu) {
        ElementFilter filtreClause = new ElementFilter(valeurNomContenu);
        if (valeurRoot != null) {
            List contenu = valeurRoot.getContent(filtreClause);
            return contenu;

        }
        return null;
    }
}
