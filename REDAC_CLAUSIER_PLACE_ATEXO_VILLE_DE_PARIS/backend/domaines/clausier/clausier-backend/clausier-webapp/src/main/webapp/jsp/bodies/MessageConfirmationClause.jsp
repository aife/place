<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<!--Debut main-part-->
	<c:set var="actionEditeur" value=""></c:set>
	<c:if test="${editeur != null && editeur == true }">
		<c:set var="actionEditeur" value="Editeur"></c:set>
	</c:if>
	<div class="main-part">
		<div class="breadcrumbs">
			<logic:equal name="typeAction" value="C">
				<bean:message key="previsualiserClause${actionEditeur}.titre.cree"/>
			</logic:equal>
			<logic:equal name="typeAction" value="M">
				<bean:message key="previsualiserClause${actionEditeur}.titre.modifier"/>
			</logic:equal>
			<logic:equal name="typeAction" value="D">
				<bean:message key="previsualiserClause${actionEditeur}.titre.dupliquer"/>
			</logic:equal>
		</div>
		<atexo:fichePratique reference="" key="common.fichePratique"/>
        <div class="breaker"></div>
        <!--Debut message confirmation-->
        <div class="form-bloc-message msg-conf">
        	<div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
            	<div class="message-left">
					<logic:equal name="typeAction" value="C">
						<bean:message key="previsualiserClause.txt.clause"/> <c:out value="${epmTClauseInstance.reference}"/> <bean:message key="previsualiserClause.txt.bien"/> 
						<bean:message key="previsualiserClause.txt.creee"/>
					</logic:equal>
					<logic:equal name="typeAction" value="M">
						<bean:message key="previsualiserClause.txt.clause"/> <c:out value="${epmTClauseInstance.reference}"/> <bean:message key="previsualiserClause.txt.bien"/>
						<bean:message key="previsualiserClause.txt.modifiee"/>
					</logic:equal>
					<logic:equal name="typeAction" value="D">
						<bean:message key="previsualiserClause.txt.clauseDupliquee" arg0="${reference}" arg1="${epmTClauseInstance.reference}"/>
					</logic:equal>
            	
            	</div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
        <!--Fin message confirmation-->
        <!--Debut boutons-->
        <div class="spacer"></div>
        <div class="boutons">
            <a href="CreationClause${actionEditeur}Init.epm" class="nouvelle-clause"><bean:message key="previsualiserClause.txt.nouvelle"/></a>
		</div>
        <!--Fin boutons-->
	</div>
	<!--Fin main-part-->
