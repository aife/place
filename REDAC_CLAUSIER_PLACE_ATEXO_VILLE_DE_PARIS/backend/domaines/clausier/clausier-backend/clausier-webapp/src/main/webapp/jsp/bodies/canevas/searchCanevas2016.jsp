<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 08/09/18
  Time: 00:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="searchInstance" type="fr.paris.epm.redaction.presentation.bean.CanevasBean"--%>
<%--@elvariable id="allNatures" type="java.util.List"--%>
<%--@elvariable id="allTypeAuteurs" type="java.util.List"--%>
<%--@elvariable id="allStatutsRedactionClausier" type="java.util.List"--%>
<%--@elvariable id="allTypesDocument" type="java.util.List"--%>
<%--@elvariable id="allTypesContrat" type="java.util.List"--%>
<%--@elvariable id="allProcedures" type="java.util.List"--%>
<%--@elvariable id="editeur" type="java.lang.Boolean"--%>
<form:form modelAttribute="searchInstance" action="searchCanevas.htm" cssClass="form-horizontal">
    <div class="form-bloc">
        <div class="content">
            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.referenceCanevas" /> :</span>
                <form:input path="referenceCanevas" cssClass="width-200" />
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.referenceClause" /> :</span>
                <form:input path="referenceClause" cssClass="width-200" />
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.typeDocument" /> :</span>
                <form:select path="idTypeDocument" cssClass="width-200">
                    <%--<form:option value="0"><spring:message code="redaction.tous" /></form:option>--%>
                    <form:options items="${allTypesDocument}" itemValue="id" itemLabel="label" />
                </form:select>
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.typeContrat" /> :</span>
                <form:select path="idTypeContrat" cssClass="width-200">
                    <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                    <form:options items="${allTypesContrat}" itemValue="id" itemLabel="label" />
                </form:select>
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.procedurePassation" /> :</span>
                <form:select path="idProcedure" cssClass="liste-procedures">
                    <form:option value="0"><spring:message code="redaction.toutes" /></form:option>
                    <form:options items="${allProcedures}" itemValue="id" itemLabel="label" />
                </form:select>
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.naturePrestation" /> :</span>
                <form:select path="idNaturePrestation" cssClass="width-200">
                    <form:option value="0"><spring:message code="redaction.toutes" /></form:option>
                    <form:options items="${allNatures}" itemValue="id" itemLabel="label" />
                </form:select>
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.titre" /> :</span>
                <form:input path="titre" cssClass="width-570" />
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.afficherActif" /> :</span>
                <form:select path="actif" cssClass="width-200">
                    <form:option value=""><spring:message code="redaction.tous" /></form:option>
                    <form:option value="true"><spring:message code="redaction.actifs" /></form:option>
                    <form:option value="false"><spring:message code="redaction.inactifs" /></form:option>
                </form:select>
            </div>

            <c:if test="${!editeur}">
                <div class="line">
                    <span class="intitule"><spring:message code="redaction.canevas.auteur" /> :</span>
                    <form:select path="typeAuteur" cssClass="width-200">
                        <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                        <form:options items="${allTypeAuteurs}" itemValue="id" itemLabel="label" />
                    </form:select>
                </div>
            </c:if>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.canevas.statut" /> :</span>
                <form:select path="idStatutRedactionClausier" cssClass="width-200">
                    <form:option value="0"><spring:message code="redaction.tous" /></form:option>
                    <form:options items="${allStatutsRedactionClausier}" itemValue="id" itemLabel="label" />
                </form:select>
            </div>

            <div class="line">
                <span class="intitule"><spring:message code="redaction.clause.dateModification" /> :</span>
                <span class="intitule-auto"><spring:message code="redaction.entreLe" /> </span>
                <div class="calendar date datetimepicker datetimepicker-date" data-provide="datepicker">
                    <form:input path="dateModificationMin" class="datepicker" data-date-format="DD/MM/YYYY"/>
                    <span><i class="fa fa-calendar"></i></span>
                </div>
                <span class="intitule-auto"> <spring:message code="redaction.etLe" /> </span>
                <div class="calendar date datetimepicker datetimepicker-date" data-provide="datepicker">
                    <form:input path="dateModificationMax" class="datepicker" data-date-format="DD/MM/YYYY"/>
                    <span class=""><i class="fa fa-calendar"></i></span>
                </div>
            </div>

            <div class="breaker"></div>
            <a href="javascript:initFormRecherche();" class="init-recherche">
                <spring:message code="redaction.action.reset"/>
            </a>
            <a href="javascript:document.forms[0].submit();" class="rechercher">
                <spring:message code="redaction.action.rechercher" />
            </a>
            <div class="breaker"></div>
        </div>
    </div>
</form:form>
