package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;

import java.util.List;

/**
 * Service
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public interface ClauseService extends GeneriqueServiceSecurise {

	EpmTClause chercherDerniereSurchargeActif(final Integer idClause, final EpmTRefOrganisme idOrganisme);
    EpmTClause chercherClauseSurcharge(Integer idClause, EpmTRefOrganisme idOrganisme);

    /**
	 * Rechercher clause pour canevas
	 *
	 * @param referenceCanevas la reference du canevas
	 *
	 * @return les identifiants des clauses
	 */
	List<EpmTClause> chercherListeClausesPourCanevasInterministerielle( final String referenceCanevas);

	/**
	 * Rechercher clause pour canevas
	 *
	 * @param referenceCanevas la reference du canevas
	 * @param idOrganisme l'identifiant de l'organisme
	 *
	 * @return les identifiants des clauses
	 */
	List<EpmTClause> chercherListeClausesPourCanevasMinisterielle( final String referenceCanevas, final Integer idOrganisme );
}
