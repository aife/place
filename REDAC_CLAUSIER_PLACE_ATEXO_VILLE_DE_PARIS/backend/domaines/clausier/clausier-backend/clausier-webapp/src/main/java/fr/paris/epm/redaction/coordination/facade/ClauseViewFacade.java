package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.facade.Facade;
import fr.paris.epm.noyau.persistance.redaction.EpmVClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.bean.ClauseSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;

/**
 * Facade de gestion des Clause Niveau Ministeriel
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public interface ClauseViewFacade extends Facade<ClauseBean>, ClauseFacadeCommun {

    EpmVClause findEntityById(int id);

    EpmVClause findByIdClausePublicationAndIdPublication(int idClausePublication, int idPublication);


    PageRepresentation<ClauseBean> findClauses(ClauseSearch search, EpmTRefOrganisme epmTRefOrganisme);


}
