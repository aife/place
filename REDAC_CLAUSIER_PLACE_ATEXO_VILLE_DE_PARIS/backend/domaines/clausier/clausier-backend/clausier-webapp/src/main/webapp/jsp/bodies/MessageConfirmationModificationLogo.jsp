<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page isErrorPage="false"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="main-part">
	<div class="breadcrumbs">
		<bean:message key="modifierLogo.titre" />
	</div>
	<div class="breaker"></div>
	<!--Debut message confirmation-->
	<div class="form-bloc-message msg-conf">
		<div class="top">
			<span class="left"></span><span class="right"></span>
		</div>
		<div class="content">
			<div class="message-left">
				<bean:message key="modifierLogo.messageConfirmation" />
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom">
			<span class="left"></span><span class="right"></span>
		</div>
	</div>
	<!--Fin message confirmation-->
	<div class="spacer"></div>
</div>
