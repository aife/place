package fr.paris.epm.redaction.metier.referentiels;

import fr.paris.epm.noyau.persistance.redaction.BaseEpmTRefRedaction;

import java.util.List;

public class ReferentielsRedacionBean {

    private List<BaseEpmTRefRedaction> refProcedureEditeur;

    /**
     * @return the refProcedureEditeur
     */
    public final List<BaseEpmTRefRedaction> getRefProcedureEditeur() {
        return refProcedureEditeur;
    }

    /**
     * @param refProcedureEditeur the refProcedureEditeur to set
     */
    public final void setRefProcedureEditeur(final List<BaseEpmTRefRedaction> valeur) {
        this.refProcedureEditeur = valeur;
    }
    
}
