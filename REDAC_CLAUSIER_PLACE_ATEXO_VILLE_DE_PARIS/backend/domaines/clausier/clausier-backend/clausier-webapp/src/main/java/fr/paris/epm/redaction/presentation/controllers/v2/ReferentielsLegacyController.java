package fr.paris.epm.redaction.presentation.controllers.v2;

import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.coordination.facade.DirectoryFacade;
import fr.paris.epm.redaction.coordination.service.PotentiellementConditionneeService;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v2/referentiels")
public class ReferentielsLegacyController {

    private final DirectoryFacade directoryFacade;
    private final PotentiellementConditionneeService potentiellementConditionneeService;

    public ReferentielsLegacyController(DirectoryFacade directoryFacade, PotentiellementConditionneeService potentiellementConditionneeService) {
        this.directoryFacade = directoryFacade;
        this.potentiellementConditionneeService = potentiellementConditionneeService;
    }

    @GetMapping(value = "natures", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allNatures() {
        return directoryFacade.findListNatures();
    }

    @GetMapping(value = "type-auteurs-canevas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allTypeAuteursCanevas() {
        return directoryFacade.findListTypesAuteurCanevas();
    }

    @GetMapping(value = "type-auteurs-clause", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allTypeAuteurs() {
        return directoryFacade.findListTypesAuteurClause();
    }

    @GetMapping(value = "statuts-redaction-clausier", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allStatut() {
        return directoryFacade.findListStatutsRedactionClausier();
    }

    @GetMapping(value = "types-document", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allTypesDocument() {
        return directoryFacade.findListTypesDocument();
    }

    @GetMapping(value = "types-contrat", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allTypesContrat() {
        return directoryFacade.findListTypesContrat();
    }

    @GetMapping(value = "procedures", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allProcedures() {
        return directoryFacade.findListProcedures();
    }

    @GetMapping(value = "/organisme/procedures", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allProceduresByOrganisme(@SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        return directoryFacade.findListProceduresByOrganisme(epmTUtilisateur.getIdOrganisme());
    }

    @GetMapping(value = "ccag", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allRefCcag() {
        return directoryFacade.findListCcag();
    }

    @GetMapping(value = "theme-clause", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allThemesClause() {
        return directoryFacade.findListThemesClause();
    }

    @GetMapping(value = "types-clause", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allTypesClause() {
        return directoryFacade.findListTypesClause();
    }

    @GetMapping(value = "valeur-heritee", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Directory> allValeursHeritees() {
        return directoryFacade.findListReferenceHeritee();
    }

    @GetMapping(value = "criteres-condition", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PotentiellementConditionnee> allCritereConditions(@SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        return potentiellementConditionneeService.lister(epmTUtilisateur.getIdOrganisme());
    }

    @GetMapping(value = "criteres-condition/{id}/valeurs", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PotentiellementConditionneeValeur> allCritereConditionsValeur(@PathVariable("id") int id,
                                                                              @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        return potentiellementConditionneeService.getListValeurById(id, epmTUtilisateur.getIdOrganisme());
    }


}
