package fr.paris.epm.redaction.presentation.controllers.v2;

import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.metier.redaction.ClauseViewCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmVClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.CanevasFacadeGWT;
import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import fr.paris.epm.redaction.coordination.DocumentRedactionFacade;
import fr.paris.epm.redaction.coordination.facade.CanevasFacade;
import fr.paris.epm.redaction.coordination.facade.CanevasViewFacade;
import fr.paris.epm.redaction.metier.documentHandler.AbstractDocument;
import fr.paris.epm.redaction.metier.documentHandler.DocumentUtil;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.bean.CanevasSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.controllers.v2.model.DocumentEditorRequest;
import fr.paris.epm.redaction.presentation.controllers.v2.model.KeyValueRequest;
import fr.paris.epm.redaction.presentation.controllers.v2.model.PatchStatut;
import fr.paris.epm.redaction.presentation.controllers.v2.model.User;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import fr.paris.epm.redaction.util.Constantes;
import org.apache.commons.io.IOUtils;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@RequestMapping("/v2/canevas")
public class CanevasLegacyController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(CanevasLegacyController.class);
    private final CanevasFacade canevasFacade;
    private final DocumentRedactionFacade documentRedactionFacade;
    private final CanevasFacadeGWT canevasFacadeGWT;
    private final ClauseFacadeGWT clauseFacadeGWT;
    private final RedactionServiceSecurise redactionService;

    private final ClauseMapper clauseMapper;
    private final CanevasViewFacade canevasViewFacade;


    @Value("classpath:templates/preview-canevas.docx")
    private Resource template;

    @Value("${docgen.url:https://gendoc-release.local-trust.com/docgen}")
    private String docgenUrl;
    @Value("${onlyoffice.url:https://editeur-en-ligne-release.local-trust.com}")
    private String onlyofficeUrl;
    @Value("${plateforme}")
    private String plateforme;

    private final ResourceBundleMessageSource messageSource;

    public CanevasLegacyController(CanevasFacade canevasFacade, DocumentRedactionFacade documentRedactionFacade, CanevasFacadeGWT canevasFacadeGWT, ClauseFacadeGWT clauseFacadeGWT, RedactionServiceSecurise redactionService, ClauseMapper clauseMapper, CanevasViewFacade canevasViewFacade) {
        this.canevasFacade = canevasFacade;
        this.documentRedactionFacade = documentRedactionFacade;
        this.canevasFacadeGWT = canevasFacadeGWT;
        this.clauseFacadeGWT = clauseFacadeGWT;
        this.redactionService = redactionService;
        this.clauseMapper = clauseMapper;
        this.canevasViewFacade = canevasViewFacade;
        this.messageSource = messageSource();
    }

    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("ApplicationResources", "ApplicationResources-commun");
        return resourceBundleMessageSource;
    }

    @GetMapping(params = {"page", "size"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PageRepresentation<CanevasBean> listCanevas(final CanevasSearch search, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("/liste Canevas");
        EpmTRefOrganisme organisme = epmTUtilisateur.getEpmTRefOrganisme();
        if (search.getDateModificationMin() != null && search.getDateModificationMax() != null && search.getDateModificationMin().equals(search.getDateModificationMax())) {
            //ajouter une durée de 24 heures à la date max pour avoir les canevas modifiés le jour même
            search.setDateModificationMax(new Date(search.getDateModificationMax().getTime() + 24 * 60 * 60 * 1000));
        }
        if (search.getDateModificationMin() == null && search.getDateModificationMax() != null) {
            search.setDateModificationMin(new Date(0));
            search.setDateModificationMax(search.getDateModificationMax());
        }
        if (search.getDateModificationMin() != null && search.getDateModificationMax() == null) {
            search.setDateModificationMin(search.getDateModificationMin());
            search.setDateModificationMax(new Date());
        }
        if (search.isEditeur()) return canevasFacade.findCanevas(search, epmTUtilisateur.getEpmTRefOrganisme());
        else return canevasViewFacade.findCanevas(search, epmTUtilisateur.getEpmTRefOrganisme());

    }

    @GetMapping(value = "/{idCanevas}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Canevas findCanevas(@PathVariable("idCanevas") int idCanevas, @RequestParam(value = "idPublication", required = false) Integer idPublication, @RequestParam("editeur") boolean editeur, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("get Canevas");
        return canevasFacadeGWT.charger(idCanevas, idPublication, editeur, epmTUtilisateur.getIdOrganisme());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Canevas addCanevas(@RequestBody Canevas canevas, @RequestParam("editeur") boolean editeur, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("ajout du canevas");
        canevas.setAuteur(epmTUtilisateur.getNomComplet());
        canevas.setCanevasEditeur(editeur);
        canevas.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        return canevasFacadeGWT.ajouter(canevas, epmTUtilisateur.getIdOrganisme());
    }

    @PutMapping(value = "/{idCanevas}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Canevas modifyCanevas(@PathVariable("idCanevas") int idCanevas, @RequestBody Canevas canevas, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("modifier du canevas {}", idCanevas);
        canevas.setAuteur(epmTUtilisateur.getNomComplet());
        return canevasFacadeGWT.modifierCanevas(canevas, epmTUtilisateur.getIdOrganisme());
    }

    @PutMapping(value = "/{idCanevas}/clone", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Canevas clone(@PathVariable("idCanevas") int idCanevas, @RequestParam("editeur") boolean editeur, @RequestBody Canevas canevas, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("duplique du canevas {}", idCanevas);
        canevas.setIdCanevas(0);
        canevas.setAuteur(epmTUtilisateur.getNomComplet());
        canevas.setCanevasEditeur(editeur);
        canevas.setCanevasEditeur(editeur);
        canevas.setIdPublication(null);
        canevas.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        List<Chapitre> chapitres = canevas.getChapitres();
        resetChapitre(chapitres);
        return canevasFacadeGWT.ajouter(canevas, epmTUtilisateur.getIdOrganisme());
    }

    private static void resetChapitre(List<Chapitre> chapitres) {
        if (!CollectionUtils.isEmpty(chapitres)) {
            chapitres.forEach(chapitre -> {
                chapitre.setIdChapitre(0);
                if (!CollectionUtils.isEmpty(chapitre.getChapitres())) {
                    resetChapitre(chapitre.getChapitres());
                }
            });
        }
    }

    @PatchMapping(value = "/{idCanevas}/statut", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CanevasBean changeStatusCanevas(@PathVariable("idCanevas") final int idCanevas, @RequestBody @NotNull final PatchStatut patchStatut, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        if (patchStatut == null) {
            throw new RuntimeException(messageError().getTitle());
        }
        int idStatus = patchStatut.getIdStatus();
        if (idStatus != 1 && idStatus != 2 && idStatus != 3) {
            throw new RuntimeException(messageError().getTitle());
        }
        String action;
        log.info("changement de statut pour {} à {}", idCanevas, idStatus);
        switch (idStatus) {
            case 1:
                action = Constantes.REFUSER_CLAUSE;
                break;
            case 2:
                action = Constantes.DEMANDER_VALIDATION_CLAUSE;
                break;
            case 3:
                action = Constantes.VALIDER_CLAUSE;
                break;
            default:
                action = null;
        }
        CanevasBean canevasBean = canevasFacade.findById(idCanevas, epmTUtilisateur.getEpmTRefOrganisme());
        canevasBean.setIdStatutRedactionClausier(idStatus);
        canevasFacadeGWT.changerStatutCanevas(epmTUtilisateur.getEpmTRefOrganisme().getId(), action, idCanevas);
        return canevasBean;
    }

    @DeleteMapping(value = "/{idCanevas}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public final CanevasBean removeCanevas(@PathVariable("idCanevas") final int idCanevas, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Suppression du canvas {}", idCanevas);

        CanevasBean canevasBean = canevasFacade.findById(idCanevas, epmTUtilisateur.getEpmTRefOrganisme());
        canevasBean.setEtat(2); // '2' vaut l'état 'Supprimé'
        canevasBean = canevasFacade.save(canevasBean, epmTUtilisateur.getEpmTRefOrganisme());

        return canevasBean;

    }

    @GetMapping(value = "/clauses/{idClause}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Clause findClause(@PathVariable("idClause") int idClause, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur, @RequestParam(required = false) Integer idPublication, @RequestParam("editeur") boolean editeur) {
        log.info("get Clause Pour l'ajout dans un chapitre");
        String plateformeUuid = epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid();
        EpmTClauseAbstract epmTClause = null;
        Clause clause = null;
        if (idPublication != null) {
            ClauseViewCritere critereClause = new ClauseViewCritere(plateformeUuid);
            critereClause.setIdClausePublication(idClause);
            EpmVClause clauseEditeur = redactionService.chercherUniqueEpmTObject(0, critereClause);
            epmTClause = clauseFacadeGWT.charger(clauseEditeur.getEpmTClausePub().getIdClause(), clauseEditeur.getIdPublication(), plateformeUuid);


        } else {
            epmTClause = clauseFacadeGWT.charger(idClause);


        }
        clause = clauseMapper.toClause(epmTClause);
        return clause;
    }


    @GetMapping(value = "{idCanevas}/export")
    public void exportCanevas(HttpServletResponse response, @PathVariable(value = "idCanevas") final int idCanevas, @RequestParam("outputFormat") String outputFormat, @RequestParam(value = "idPublication", required = false) Integer idPublication, @SessionAttribute("utilisateur") EpmTUtilisateur utilisateur) throws IOException {
        Document document = documentRedactionFacade.genererDocumentPourExportCanevas(idCanevas, idPublication, utilisateur);
        Element element = DocumentUtil.versJdom(document);

        var xml = Util.encodeXML(AbstractDocument.toXml(element));
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + document.getTitre() + "." + outputFormat + "\"");
        long length = export(xml, utilisateur, document, outputFormat, response.getOutputStream());
        response.setContentLength((int) length);
        response.flushBuffer();

    }

    @GetMapping(value = "{idCanevas}/preview")
    public List<ChapitreDocument> previewCanevas(@PathVariable(value = "idCanevas") final int idCanevas, @RequestParam(value = "idPublication", required = false) Integer idPublication, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) throws IOException {


        Document document = documentRedactionFacade.genererDocumentPourExportCanevas(idCanevas, idPublication, epmTUtilisateur);
        return document.getChapitres();
    }


    @PostMapping(value = "/preview")
    public List<ChapitreDocument> previewCanevas(@RequestBody Canevas canevas, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        return DocumentUtil.construireChapitresExportCanevas(null, canevas, epmTUtilisateur, messageSource);
    }

    @GetMapping(value = "{idCanevas}/edit")
    public String editCanevas(@PathVariable(value = "idCanevas") final int idCanevas, @RequestParam(value = "idPublication", required = false) Integer idPublication, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) throws IOException {


        Document document = documentRedactionFacade.genererDocumentPourExportCanevas(idCanevas, idPublication, epmTUtilisateur);
        Element element = DocumentUtil.versJdom(document);
        var xml = Util.encodeXML(AbstractDocument.toXml(element));
        return generateToEdit(xml, epmTUtilisateur, document);


    }


    public long export(String xml, EpmTUtilisateur epmTUtilisateur, Document document, String outputFormat, ServletOutputStream outputStream) throws IOException {


        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        Path tempXml = Files.createTempFile("export-canevas-document-", ".xml");

        // Writes a string to the above temporary file
        Files.write(tempXml, xml.getBytes(StandardCharsets.UTF_8));
        bodyMap.add("xml", new FileSystemResource(tempXml.toFile()));
        bodyMap.add(TEMPLATE, new FileSystemResource(template.getFile()));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        KeyValueRequest titre = new KeyValueRequest();
        titre.setKey("TITRE");
        titre.setValue(document.getTitre());
        KeyValueRequest version = new KeyValueRequest();
        version.setKey("VERSION");
        version.setValue(document.getVersion());
        KeyValueRequest footer = new KeyValueRequest();
        footer.setKey("FOOTER");
        footer.setValue("Document de test ");
        bodyMap.add("keyValues", new HttpEntity<>(List.of(titre, version, footer), theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenUrl + "/api/v1/document-convertor/convert-xml";
        RestTemplate restTemplate = new RestTemplate();
        Resource resource = restTemplate.postForObject(url, requestEntity, Resource.class);
        Path tempZip = Files.createTempFile("export-canevas-document-", ".zip");
        Files.copy(resource.getInputStream(), tempZip, StandardCopyOption.REPLACE_EXISTING);

        UUID uuid = UUID.randomUUID();
        File destDir = Files.createTempDirectory(uuid.toString()).toFile();

        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(tempZip.toFile()));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = createNewFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                }

            }
            zipEntry = zis.getNextEntry();

        }
        zis.closeEntry();
        zis.close();
        bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("document", new FileSystemResource(new File(destDir, "output.docx")));
        DocumentEditorRequest editorRequest = new DocumentEditorRequest();
        editorRequest.setDocumentId(document.getIdCanevas() + "-" + uuid);
        editorRequest.setDocumentTitle("Export de " + document.getTitre() + ".docx");
        editorRequest.setPlateformeId(plateforme);
        editorRequest.setMode("redac");
        User user = new User();
        user.setId(String.valueOf(epmTUtilisateur.getId()));
        user.setName(epmTUtilisateur.getNomComplet());
        user.setRoles(List.of("reprendre", "modification"));
        editorRequest.setUser(user);
        requestEntity = getEditorConfigMultipart(editorRequest, bodyMap);
        Resource documentExporte = restTemplate.postForObject(docgenUrl + "/api/v1/document-convertor/" + outputFormat + "/finalize", requestEntity, Resource.class);
        Files.deleteIfExists(tempXml);
        Files.deleteIfExists(tempZip);
        try (Stream<Path> walk = Files.walk(destDir.toPath())) {
            walk // Traverse the file tree in depth-first order
                    .sorted(Comparator.reverseOrder()).forEach(path -> {
                        try {
                            Files.delete(path);  //delete each file or directory
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }

        Files.deleteIfExists(destDir.toPath());

        IOUtils.copy(documentExporte.getInputStream(), outputStream);

        return documentExporte.contentLength();

    }

    public String generateToEdit(String xml, EpmTUtilisateur epmTUtilisateur, Document document) throws IOException {


        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        Path tempXml = Files.createTempFile("preview-canevas-document-", ".xml");

        // Writes a string to the above temporary file
        Files.write(tempXml, xml.getBytes(StandardCharsets.UTF_8));
        bodyMap.add("xml", new FileSystemResource(tempXml.toFile()));

        bodyMap.add(TEMPLATE, new FileSystemResource(template.getFile()));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        KeyValueRequest titre = new KeyValueRequest();
        titre.setKey("TITRE");
        titre.setValue(document.getTitre());
        KeyValueRequest version = new KeyValueRequest();
        version.setKey("VERSION");
        version.setValue(document.getVersion());
        KeyValueRequest footer = new KeyValueRequest();
        footer.setKey("FOOTER");
        footer.setValue("Document de test ");
        bodyMap.add("keyValues", new HttpEntity<>(List.of(titre, version, footer), theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
        String url = docgenUrl + "/api/v1/document-convertor/convert-xml";
        RestTemplate restTemplate = new RestTemplate();
        Resource resource = restTemplate.postForObject(url, requestEntity, Resource.class);
        Path tempZip = Files.createTempFile("preview-canevas-document-", ".zip");
        Files.copy(resource.getInputStream(), tempZip, StandardCopyOption.REPLACE_EXISTING);

        UUID uuid = UUID.randomUUID();
        File destDir = Files.createTempDirectory(uuid.toString()).toFile();

        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(tempZip.toFile()));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = createNewFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();

        }
        zis.closeEntry();
        zis.close();
        bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("xml", new FileSystemResource(new File(destDir, "output.xml")));
        bodyMap.add("document", new FileSystemResource(new File(destDir, "output.docx")));
        DocumentEditorRequest editorRequest = new DocumentEditorRequest();
        editorRequest.setDocumentId(document.getIdCanevas() + "-" + uuid);
        editorRequest.setDocumentTitle("Prévisualisation de " + document.getTitre() + ".docx");
        editorRequest.setPlateformeId(plateforme);
        editorRequest.setMode("redac");
        User user = new User();
        user.setId(String.valueOf(epmTUtilisateur.getId()));
        user.setName(epmTUtilisateur.getNomComplet());
        user.setRoles(List.of("reprendre", "modification"));
        editorRequest.setUser(user);
        requestEntity = getEditorConfigMultipart(editorRequest, bodyMap);
        String token = restTemplate.postForObject(docgenUrl + "/api/v1/document-editor/request-xml", requestEntity, String.class);
        Files.deleteIfExists(tempXml);
        Files.deleteIfExists(tempZip);
        try (Stream<Path> walk = Files.walk(destDir.toPath())) {
            walk // Traverse the file tree in depth-first order
                    .sorted(Comparator.reverseOrder()).forEach(path -> {
                        try {
                            Files.delete(path);  //delete each file or directory
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }

        Files.deleteIfExists(destDir.toPath());

        return onlyofficeUrl + "?token=" + token;


    }

    public HttpEntity<MultiValueMap<String, Object>> getEditorConfigMultipart(DocumentEditorRequest editorRequest, MultiValueMap<String, Object> bodyMap) {
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return new HttpEntity<>(bodyMap, headers);
    }

    private static final String TEMPLATE = "template";

    public static File createNewFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}
