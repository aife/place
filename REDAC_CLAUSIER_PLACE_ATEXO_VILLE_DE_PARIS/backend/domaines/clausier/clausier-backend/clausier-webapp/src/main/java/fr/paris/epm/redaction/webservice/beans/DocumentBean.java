/**
 * 
 */
package fr.paris.epm.redaction.webservice.beans;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fr.paris.epm.redaction.webservice.rest.constantes.ConstantesRestWebService;

import java.util.List;

/**
 * Bean de presentation d'un Document
 * @author MGA
 *
 */
@JsonPropertyOrder({"identifiant", "nom", "statut", "titre", "type", "taille", "typeContratBeanList"})
public class DocumentBean {

	private Long identifiant;
	private String nom;
	private ConstantesRestWebService.StatutDocumentEnum statut;
	private String titre;
	private String type;
	private Long taille;
	private List<TypeContratBean> typeContratBeanList;

	public List<TypeContratBean> getTypeContratBeanList() {
		return typeContratBeanList;
	}

	public void setTypeContratBeanList( List<TypeContratBean> typeContratBeanList ) {
		this.typeContratBeanList = typeContratBeanList;
	}

	public String getNom() {
		return nom;
	}

	public void setNom( String nom ) {
		this.nom = nom;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre( String titre ) {
		this.titre = titre;
	}

	public Long getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant( Long identifiant ) {
		this.identifiant = identifiant;
	}

	public Long getTaille() {
		return taille;
	}

	public void setTaille( Long taille ) {
		this.taille = taille;
	}

	public ConstantesRestWebService.StatutDocumentEnum getStatut() {
		return statut;
	}

	public void setStatut( ConstantesRestWebService.StatutDocumentEnum statut ) {
		this.statut = statut;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}
}
