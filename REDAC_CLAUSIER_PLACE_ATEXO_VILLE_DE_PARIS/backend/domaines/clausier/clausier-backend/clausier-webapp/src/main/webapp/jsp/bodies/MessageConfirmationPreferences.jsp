<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page isErrorPage="false"%> 
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<!--Debut main-part-->
	<div class="main-part">
		<div class="breadcrumbs">
			<logic:equal name="preferences" value="direction">
				<bean:message key="previsualiserPreferences.titre.direction"/>
			</logic:equal>
			<logic:equal name="preferences" value="agent">
				<bean:message key="previsualiserPreferences.titre.agent"/>
			</logic:equal>
		</div>
	<atexo:fichePratique reference="" key="common.fichePratique"/>
        <div class="breaker"></div>
        <!--Debut message confirmation-->
        <div class="form-bloc-message msg-conf">
        	<div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
            	<div class="message-left">
					<logic:equal name="preferences" value="direction">
						<bean:message key="previsualiserPreferences.txt.direction"/>
					</logic:equal>
					<logic:equal name="preferences" value="agent">
						<bean:message key="previsualiserPreferences.txt.agent"/>
					</logic:equal>
            		 <c:out value="${reference}"/> <bean:message key="previsualiserPreferences.txt.bien"/> 
            	</div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
        <!--Fin message confirmation-->
        <!--Debut boutons-->
        <div class="spacer"></div>
        <div class="boutons">
            
            <a href="ParametrageClauseInit.epm?preferences=<logic:equal name="preferences" value="direction">direction</logic:equal><logic:equal name="preferences" value="agent">agent</logic:equal>" class="nouvelle-clause">
            	<bean:message key="previsualiserPreferences.txt.nouvelle"/>
            </a>
		</div>
        <!--Fin boutons-->
	</div>
	<!--Fin main-part-->
