INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (1, 'Toutes', NULL);
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (4, 'Pouvoir adjudicateur', 'consultation.epmTRefPouvoirAdjudicateur.libelle');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (6, 'Objet de la consultation', 'consultation.objet');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (9, 'Nature des prestations', 'naturePrestation()');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (13, 'Forme attendue du groupement attributaire', 'consultation.epmTRefGroupementAttributaire.libelle');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (14, 'Allotissement', 'allotissement');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (16, 'Nombre de candidats admis à présenter une offre', 'candidatAdmisPresenterOffre');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (20, 'Description succincte du lot n', 'descriptionSuccinteLot()');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (10, 'Direction / Service(s)', 'consultation.epmTRefDirectionService.libelle');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (2, 'Numéro de consultation', 'consultation.numeroConsultation');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (3, 'Opération de travaux', 'consultation.operationTravaux');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (5, 'Intitulé de la consultation ', 'consultation.IntituleConsultation');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (7, 'Procédure de passation', 'consultation.epmTRefProcedure.documentLibelle');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (11, 'Autre opération', 'consultation.autreOperation');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (15, 'Durée du marché', 'dureeDuMarche()');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (17, 'Délai de validité des offres (en jours)', 'delaiValiditeOffres');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (18, 'Numéro du lot n', 'numeroLot()');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (19, 'Intitulé du lot n', 'intituleLot()');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (8, 'Article(s) de référence', 'consultation.epmTRefArticle.libelle');
INSERT INTO epm__t_ref_valeur_heritee (id, libelle, nom_attribut_consultation) VALUES (12, 'Marché réservé', 'lotReserve');

INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (4, 'Liste des tranches', 'tranches()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (2, 'Liste des postes techniques', 'lotsTechniques()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (3, 'Liste des lots juridiques', 'lots()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (6, 'Prestations supplémentaires éventuelles', 'variantesObligatoires()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (5, 'Variantes autorisées', 'variantesAutorisees()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (1, 'Forme de prix', 'formePrix()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (7, 'Reconductions', 'nbReconductions()');
INSERT INTO epm__t_ref_valeur_tableau (id, libelle, nom_attribut_consultation) VALUES (8, 'Tableau de prix', 'tableauPrix');

INSERT INTO epm__t_ref_type_document (id, libelle) VALUES (1, 'Tous');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (2, 'RC', 'pdf', 'ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (3, 'CCAP', 'pdf', 'ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (4, 'AE', 'doc', 'ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (5, 'CCAP-AE', 'doc', 'ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (6,'CCP','pdf','ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (7,'CCP-AE','doc','ParisTemplate.odt');
INSERT INTO epm__t_ref_type_document (id, libelle, extension_fichier, template) VALUES (8,'CCTP','pdf','ParisTemplate.odt');


INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (1, 'Tous');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (2, 'Texte fixe');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (3, 'Texte incluant un champ libre');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (4, 'Texte incluant un champ prévalorisé');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (5, 'Texte incluant une valeur héritée');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (6, 'Liste à choix exclusif');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (7, 'Liste à choix multiple');
INSERT INTO epm__t_ref_type_clause (id, libelle) VALUES (8, 'Tableau');


INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (1, 'Tous');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (2, 'Information administrative et financière');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (3, 'Caractéristiques du marché');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (4, 'Caractéristiques de la consultation');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (5, 'Conditions de participation à la consultation');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (6, 'Dématérialisation');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (7, 'Durée-Délai');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (8, 'Dispositions économiques et financières');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (9, 'Exécution des prestations');
INSERT INTO epm__t_ref_theme_clause (id, libelle) VALUES (10, 'Identification');


INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (10, 'Allotissement', NULL, true, NULL, 'consultation.allotissement()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (4, 'Article(s) de référence', 'refArticle.libelle', false, 'refArticle.id', 'consultation.epmTRefArticle.id()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (9, 'Au moins un lot du marché ou le marché est reservé', 'reservationLot.libelle', false, 'reservationLot.id', 'reservationLots(id)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (15, 'CCAG de référence', 'refCcag.libelle', false, 'refCcag.id', 'CCAG(id,lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (8, 'Clauses environnementales', NULL, true, NULL, 'consultation.clausesEnvironnementales()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (7, 'Clauses sociales', NULL, true, NULL, 'consultation.clausesSociales()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (6, 'Direction / Service(s)', 'RefDirectionServiceLecture.nom', false, 'RefDirectionServiceLecture.id', 'consultation.epmTRefDirectionService.id()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (2, 'En vue d''attribution de', 'attribution.libelle', false, 'refAttribution.id', 'consultation.epmTRefAttribution.id()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (13, 'Enveloppe unique', NULL, true, NULL, 'consultation.enveloppeUniqueReponse()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (11, 'Nombre de candidats admis à présenter une offre', NULL, true, NULL, 'candidatAdmisPresenter()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (5, 'Nature des prestations', 'refNature.libelle', false, 'refNature.id', 'naturePrestations(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (1, 'Pouvoir adjudicateur', 'refPouvoirAdjudicateur.libelle', false, 'refPouvoirAdjudicateur.id', 'consultation.epmTRefPouvoirAdjudicateur.id()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (3, 'Procédure de passation', 'refProcedure.libelle', false, 'refProcedure.id', 'consultation.epmTRefProcedure.id()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (12, 'Procédure en phases successives afin de réduire le nombre d''offres à  négocier', NULL, true, NULL, 'consultation.enPhasesSuccessives()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (14, 'Réponse par voie électronique', NULL, true, NULL, 'consultation.reponseElectronique()');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (21, 'Forme de prix', 'choixFormePrix.libelle', false, 'choixFormePrix.id', 'formePrix(id,lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (22, 'Type de prix', 'typePrix.libelle', false, 'typePrix.id', 'typePrix(id,lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (24, 'Variation du prix', 'variation.libelle', false, 'variation.id', 'variationPrix(id,lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (25, 'Prix unitaire', 'bonQuantite.libelle', false, 'bonQuantite.id', 'prixUnitaire(id,lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (20, 'Lots techniques', NULL, true, NULL, 'lotTechnique(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (19, 'Tranches', NULL, true, NULL, 'tranche(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (18, 'Prestations supplémentaires éventuelles', NULL, true, NULL, 'variantesTechniquesObligatoires(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (17, 'Variantes autorisées', NULL, true, NULL, 'variantesAutorisees(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (16, 'Reconductible', NULL, true, NULL, 'reconduction(lot)');
INSERT INTO epm__t_ref_potentiellement_conditionnee (id, libelle, methode_libelle, critere_booleen, methode_id, methode_consultation) VALUES (23, 'Bons de commande Min/max', 'minMax.libelle', false, 'minMax.id', 'bonCommandeMinMax(id,lot)');


UPDATE epm__t_ref_potentiellement_conditionnee SET multi_choix=TRUE WHERE libelle = 'Procédure de passation';
--
UPDATE epm__t_ref_potentiellement_conditionnee SET multi_choix=TRUE WHERE libelle = 'Variation du prix';
--