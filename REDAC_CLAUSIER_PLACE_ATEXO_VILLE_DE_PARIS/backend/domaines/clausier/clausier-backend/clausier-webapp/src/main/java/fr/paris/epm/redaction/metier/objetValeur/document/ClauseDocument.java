package fr.paris.epm.redaction.metier.objetValeur.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import fr.paris.epm.redaction.util.Constantes;
import java.io.Serializable;
import java.util.List;

/**
 * classe ClauseDocument manipule les clauses d'un document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseDocument implements Serializable, IClauseDocument {
    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Actif.
     */
    public static final String ACTIF = "1";

    /**
     * Actif.
     */
    public static final String INACTIF = "0";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_SUPPRIMER = "2";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_MODIFIER = "1";

    /**
     * Etat de la clause.
     * cette etat ne signifie pas que la clause est patentionnement conditionnée
     * si etat de clause = 3 alors cad que les conditions de la clause ne corresponde pas à la consultation choisie
     */
    public static final String ETAT_CONDITIONNER = "3";

    /**
     * Etat de la clause.
     */
    public static final String ETAT_DISPONIBLE = "0";

    /**
     * l'attribut static TYPE_CLAUSE_TEXTE_FIXE initialisé à 2.
     */
    public static final int TYPE_CLAUSE_TEXTE_FIXE = 2;

    /**
     * l'attribut static TYPE_CLAUSE_TEXTE_LIBRE initialisé à 3.
     */
    public static final int TYPE_CLAUSE_TEXTE_LIBRE = 3;

    /**
     * l'attribut static TYPE_CLAUSE_TEXTE_PREVALORISE initialisé à 4.
     */
    public static final int TYPE_CLAUSE_TEXTE_PREVALORISE = 4;

    /**
     * l'attribut static TYPE_CLAUSE_TEXTE_HERITE initialisé à 5.
     */
    public static final int TYPE_CLAUSE_TEXTE_HERITE = 5;   // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}

    /**
     * l'attribut static TYPE_CLAUSE_LISTE_EXCLUSIVE initialisé à 6.
     */
    public static final int TYPE_CLAUSE_LISTE_EXCLUSIVE = 6;

    /**
     * l'attribut static TYPE_CLAUSE_LISTE_MULTIPLE initialisé à 7.
     */
    public static final int TYPE_CLAUSE_LISTE_MULTIPLE = 7;

    /**
     * Clause tableau.
     */
    public static final int TYPE_CLAUSE_TABLEAU = 8;    // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}

    /**
     * Clause valeur héritée - remplaçant TYPE_CLAUSE_TEXTE_HERITEE et TYPE_CLAUSE_TABLEAU .
     */
    public static final int TYPE_CLAUSE_VALEUR_HERITEE = 9;

    /**
     * l'identifiant de la clause.
     */
    private int id;

    /**
     * référence de la clause.
     */
    private String ref;
    
    /**
     * true si valeur a été modifier depuis le dernier enregitrement.
     */
    private boolean modifier = false;

    /**
     * l'info-Bulle de la clause.
     */
    private InfoBulle infoBulle;

    /**
     * l'attribut type.
     */
    private int type;

    /**
     * l'attribut texteFixeAvant.
     */
    private String texteFixeAvant;

    /**
     * l'attribut texteFixeApresAlaLigne.
     */
    private boolean texteFixeApresAlaLigne;

    /**
     * l'attribut texteFixeAvantAlaLigne.
     */
    private boolean texteFixeAvantAlaLigne;

    /**
     * l'attribut texteFixeApres.
     */
    private String texteFixeApres;

    /**
     * le chapitre parent.
     */
    @JsonIgnore
    private ChapitreDocument parent;

    /**
     * l'attribut terminer.
     */
    private boolean terminer;

    /**
     * l'attribut texteModifiable.
     */
    private boolean texteModifiable;

    /**
     * True si la clause est pontentiellement conditionnée
     */
    private boolean conditionee;

    private String texteConditions;

    /**
     * Valeur du conditionnement de la clause : Oui / Non
     */
    private List<List<String>> valeurConditionnementList;

    /**
     * Le type de conditionnement de la clause : Allotissement, Nature des prestations, etc
     */
    private List<String> typeConditionnementList;

    /**
     * etat de la clause.
     */
    private String etat = ETAT_DISPONIBLE;

    /**
     * Actif.
     */
    private String actif = ACTIF;

    /**
     * Objet répresentant les clauses du type tableau
     */
    private TableauRedaction tableauRedaction;

    /**
     * Vérifie si la clause du document est provenant d'un document dupliqué
     */
    private boolean duplique;
    
    // Accesseurs
    /**
     * cette classe positionne l'identifiant de la clause .
     * @param valeur pour initialiser l'identifiant.
     */
    public final void setId(final int valeur) {
        id = valeur;
    }

    /**
     * cette méthode récupére l'identifiant de la clause.
     * @return identifiant de la clause.
     */
    public final int getId() {
        return id;
    }

    /**
     * cette méthode modifie la référence de la clause par une valeur donnée.
     * @param valeur pour positionner la référence.
     */
    public final void setRef(final String valeur) {
        ref = valeur;
    }

    /**
     * cette méthode récupére la référence de la clause.
     * @return renvoyer la référence de la clause.
     */
    public final String getRef() {
        return ref;
    }

    /**
     * cette méthode renvoie le chapitre parent de la clause.
     * @return chapitre parent de cette clause.
     */
    public final ChapitreDocument getParent() {
        return parent;
    }

    /**
     * cette méthode initialise le chapitre parent de la clause.
     * @param valeur pour positionner le chapitre parent de la clause.
     */
    public final void setParent(final ChapitreDocument valeur) {
        parent = valeur;
    }

    /**
     * cette méthode renvoie le type.
     * @return type.
     */
    public final int getType() {
        return type;
    }

    /**
     * cette méthode positionne le type par une valeur donnée.
     * @param valeur pour initialiser le type.
     */
    public final void setType(final int valeur) {
        type = valeur;
    }

    /**
     * cette méthode renvoie l'attribut texteFixeAvant.
     * @return texteFixeAvant.
     */
    public final String getTexteFixeAvant() {
        return texteFixeAvant;
    }

    /**
     * cette méthode initialise l'attribut texteFixeAvant.
     * @param valeur pour initialiser l'attribut texteFixeAvant.
     */
    public final void setTexteFixeAvant(final String valeur) {
        texteFixeAvant = valeur;
    }

    /**
     * cette méthode renvoie l'attribut texteFixeApres.
     * @return texteFixeApres.
     */
    public final String getTexteFixeApres() {
        return texteFixeApres;
    }

    /**
     * cette méthode positionne l'attribut texteFixeApres avec une valeur
     * précise.
     * @param valeur pour positionner l'attribut texteFixeApres.
     */
    public final void setTexteFixeApres(final String valeur) {
        texteFixeApres = valeur;
    }

    /**
     * cette méthode vérifie si le chapitre posséde l'infobulle passée en
     * parametre.
     * @param bulle infobulle à chercher.
     * @return résultat du test.
     */
    public final boolean aEnfant(final InfoBulle bulle) {
        return (bulle == null) ? false : this.infoBulle.equals(bulle);
    }

    /**
     * cette méthode renvoie l'index de la clause courante.
     * @return index de la clause courante.
     * @throws Exception erreur technique.
     */
    public final int index() throws Exception {
        if (parent == null) {
            throw new Exception("Parent vide, clause orpheline");
        }
        int index = parent.getClausesDocument().indexOf(this);

        if (index == -1) {
            throw new Exception("erreur interne, non trouvée");
        }

        return index;
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    public final int hashCode() {
        int result = 1;
        if (infoBulle != null) {
            result = Constantes.PREMIER * result + infoBulle.hashCode();
        }
        return result;
    }

    /**
     * cette méthode teste l'égalité entre deux objets.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj correspond à l'bjet à tester.
     * @return renvoie vrai si les deux objets sont égaux si non elle retoune
     *         faux.
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ClauseDocument)) {
            return false;
        }

        final ClauseDocument autre = (ClauseDocument) obj;
        if (id != autre.id) {
            return false;
        }
        if (!ref.equals(autre.ref)) {
            return false;
        }
        if (this.infoBulle != autre.infoBulle) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Clause (type:" + this.getClass().getName() + ") '#"+id+"' : " +
			"\n     ref? '" + ref + '\'' +
			"\n     modifier? " + modifier +
			"\n     infoBulle? " + infoBulle +
			"\n     type? " + type +
			"\n     texteFixeAvant? '" + texteFixeAvant + '\'' +
			"\n     texteFixeApresAlaLigne? " + texteFixeApresAlaLigne +
			"\n     texteFixeAvantAlaLigne? " + texteFixeAvantAlaLigne +
			"\n     texteFixeApres? '" + texteFixeApres + '\'' +
			"\n     terminer? " + terminer +
			"\n     texteModifiable? " + texteModifiable +
			"\n     conditionee? " + conditionee +
			"\n     valeurConditionnementList? " + valeurConditionnementList +
			"\n     typeConditionnementList? " + typeConditionnementList +
			"\n     etat? '" + etat + '\'' +
			"\n     actif? '" + actif + '\'' +
			"\n     duplique? " + duplique;
	}

	/**
     * cette méthode renvoie l'info-Bulle.
     * @return infoBulle
     */
    public final InfoBulle getInfoBulle() {
        return infoBulle;
    }

    /**
     * cette méthode initialise l'objet info bulle par une valeur précise.
     * @param valeur pour initialiser infoBulle.
     */
    public final void setInfoBulle(final InfoBulle valeur) {
        infoBulle = valeur;
    }

    /**
     * Indique si une clause est validée dans l'édition du document.
     * @return true si la clause est validée, false si non.
     */
    public final boolean isTerminer() {
        return terminer;
    }

    /**
     * Indique si une clause est validée dans l'édition du document.
     * @param valeur true si la clause est validée, false si non.
     */
    public final void setTerminer(final boolean valeur) {
        terminer = valeur;
    }

    /**
     * cette méthode prévoit s'il s'agit d'un texte fixe aprés à la ligne.
     * @return vrai si oui, faux siNon.
     */
    public final boolean isTexteFixeApresAlaLigne() {
        return texteFixeApresAlaLigne;
    }

    /**
     * cette méthode positionne l'attribut texteFixeApresAlaLigne avec une
     * valeur donnée.
     * @param valeur pour initialiser l'attribut texteFixeApresAlaLigne.
     */
    public final void setTexteFixeApresAlaLigne(final boolean valeur) {
        texteFixeApresAlaLigne = valeur;
    }

    /**
     * cette méthode prévoit s'il s'agit d'un texte fixe avant à la ligne.
     * @return vrai si oui, faux siNon.
     */
    public final boolean isTexteFixeAvantAlaLigne() {
        return texteFixeAvantAlaLigne;
    }

    /**
     * cette méthode positionne l'attribut texteFixeAvantAlaLigne avec une
     * valeur donnée.
     * @param valeur pour initialiser l'attribut texteFixeAvantAlaLigne.
     */
    public final void setTexteFixeAvantAlaLigne(final boolean valeur) {
        texteFixeAvantAlaLigne = valeur;
    }

    /**
     * cette méthode indique si le texte est modifiable ou non.
     * @return vrai si le texte est modifiable, faux si non.
     */
    public final boolean isTexteModifiable() {
        return texteModifiable;
    }

    /**
     * cette méthode pour positionner l'attribut texteModifiable.
     * @param valeur pour initialiser l'attribut du texteModifiable.
     */
    public final void setTexteModifiable(final boolean valeur) {
        texteModifiable = valeur;
    }

    /**
     * @return etat de la clause
     */
    public final String getEtat() {
        return etat;
    }

    /**
     * @param valeur etat de la clause
     */
    public final void setEtat(final String valeur) {
        this.etat = valeur;
    }

    /**
     * @return actif (1) ou inactif (o)
     */
    public final String getActif() {
        return actif;
    }

    /**
     * @param valeur actif (1) ou inactif (o)
     */
    public final void setActif(final String valeur) {
        this.actif = valeur;
    }

    /**
     * @return true si modifié depuis le dernier enregistrement
     */
    public final boolean isModifier() {
        return modifier;
    }

    /**
     * @param valeur true si modifié depuis le dernier enregistrement
     */
    public final void setModifier(final boolean valeur) {
        this.modifier = valeur;
    }

    /**
     * Vérifie si le clause est provenant d'un document dupliqué
     * @return true si provient d'un document dupliqué
     */
    public boolean isDuplique() {
        return duplique;
    }

    /**
     * Vérifie si le clause est provenant d'un document dupliqué
     * @param duplique : true si provient d'un document dupliqué
     */
    public void setDuplique(boolean duplique) {
        this.duplique = duplique;
    }

    /**
     * @return clone de clause document.
     */
    public final Object clone(ChapitreDocument parent) {

        ClauseDocument clauseDocument = new ClauseDocument();

        clauseDocument.setParent(parent);
        if (infoBulle != null) {
            clauseDocument.setInfoBulle((InfoBulle) infoBulle.clone());
        }

        clauseDocument.setId(id);
        clauseDocument.setRef(ref);
        clauseDocument.setType(type);
        clauseDocument.setModifier(modifier);
        clauseDocument.setTexteFixeAvant(texteFixeAvant);
        clauseDocument.setTexteFixeApresAlaLigne(texteFixeApresAlaLigne);
        clauseDocument.setTexteFixeAvantAlaLigne(texteFixeAvantAlaLigne);
        clauseDocument.setTexteFixeApres(texteFixeApres);
        clauseDocument.setTerminer(terminer);
        clauseDocument.setTexteModifiable(texteModifiable);
        clauseDocument.setEtat(etat);
        clauseDocument.setActif(actif);
        clauseDocument.setTableauRedaction(tableauRedaction);
        clauseDocument.setConditionee(conditionee);
        clauseDocument.setTypeConditionnementList(typeConditionnementList);
        clauseDocument.setValeurConditionnementList(valeurConditionnementList);

        return clauseDocument;
    }
    
    protected final boolean isClauseDocumentVide() {
        return isChaineVide(texteFixeApres) && isChaineVide(texteFixeAvant);
    }
    
    protected final boolean isChaineVide(final String chaine) {
        return chaine == null || chaine.isEmpty();
    }
    
	public final TableauRedaction getTableauRedaction() {
		return tableauRedaction;
	}

	public final void setTableauRedaction(final TableauRedaction valeur) {
		this.tableauRedaction = valeur;
	}

	@Override
    public boolean isClauseVide() {
        return isClauseDocumentVide();
    }

    public final boolean isConditionee() {
        return conditionee;
    }

    public final void setConditionee(final boolean valeur) {
        this.conditionee = valeur;
    }

    public List<List<String>> getValeurConditionnementList() {
        return valeurConditionnementList;
    }

    public void setValeurConditionnementList(List<List<String>> valeurConditionnementList) {
        this.valeurConditionnementList = valeurConditionnementList;
    }

    public List<String> getTypeConditionnementList() {
        return typeConditionnementList;
    }

    public void setTypeConditionnementList(List<String> typeConditionnementList) {
        this.typeConditionnementList = typeConditionnementList;
    }

    public String getTexteConditions() {
        return texteConditions;
    }

    public void setTexteConditions(String texteConditions) {
        this.texteConditions = texteConditions;
    }
}
