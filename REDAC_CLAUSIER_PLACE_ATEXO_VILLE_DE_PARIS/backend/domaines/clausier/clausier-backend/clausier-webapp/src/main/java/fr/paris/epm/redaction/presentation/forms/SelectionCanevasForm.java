package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionForm;

import java.util.Collection;

/**
 * Formulaire de selection des canevas.
 * @author RAMLI Tarik .
 * @version $Revision$, $Date$, $Author$
 */
public class SelectionCanevasForm extends ActionForm {

    /**
     * marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut numéro de consultation.
     */
    private String numConsultation;
    /**
     * Attribut procédure de passation.
     */
    private String procedurePassation;
    /**
     * Attribut nature de préstation.
     */
    private String naturePrestation;
    /**
     * Attribut type de document.
     */
    private String typeDocument;
    /**
     * Attribut lot.
     */
    private String lot;
    /**
     * Attribut direction/service.
     */
    private String directionService;
    /**
     * Attribut pouvoir adjudicateur.
     */
    private String pouvoirAdjudicateur;
    /**
     * Attribut intitulé de la consultation.
     */
    private String intituleConsultation;
    /**
     * Attribut collection des Canevas.
     */
    private Collection collectionCanevas;

    /**
     * constructeur de la classe SelectionCanevasForm.
     */
    public SelectionCanevasForm() {
        reset();
    }

    /**
     * reset() permet d'initialiser le formulaire .
     */
    public void reset() {
        numConsultation = null;
        procedurePassation = null;
        naturePrestation = null;
        typeDocument = null;
        lot = null;
        directionService = null;
        pouvoirAdjudicateur = null;
        collectionCanevas = null;
        intituleConsultation = null;
    }

    /**
     * @return numéro de consultation.
     */
    public final String getNumConsultation() {
        return numConsultation;
    }

    /**
     * @param valeur positionne le numéro de consultation.
     */
    public final void setNumConsultation(final String valeur) {
        numConsultation = valeur;
    }

    /**
     * @return procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * @param valeur positionne la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * @return nature de préstation.
     */
    public final String getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * @param valeur initialise la nature de préstation.
     */
    public final void setNaturePrestation(final String valeur) {
        naturePrestation = valeur;
    }

    /**
     * @return type de document.
     */
    public final String getTypeDocument() {
        return typeDocument;
    }

    /**
     * @param valeur positionne le type de document.
     */
    public final void setTypeDocument(final String valeur) {
        typeDocument = valeur;
    }

    /**
     * @return lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * @param valeur positionne l'attribut lot.
     */
    public final void setLot(final String valeur) {
        lot = valeur;
    }

    /**
     * @return collection de Canevas.
     */
    public final Collection getCollectionCanevas() {
        return collectionCanevas;
    }

    /**
     * @param valeur positionne une collection de Canevas.
     */
    public final void setCollectionCanevas(final Collection valeur) {
        collectionCanevas = valeur;
    }

    /**
     * @return direction/service.
     */
    public final String getDirectionService() {
        return directionService;
    }

    /**
     * @param valeur positionne l'attribut direction/service.
     */
    public final void setDirectionService(final String valeur) {
        directionService = valeur;
    }

    /**
     * @return pouvoir adjudicateur.
     */
    public final String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur positionne le pouvoir adjudicateur.
     */
    public final void setPouvoirAdjudicateur(final String valeur) {
        pouvoirAdjudicateur = valeur;
    }

    /**
     * @return intitulé de la consultation.
     */
    public final String getIntituleConsultation() {
        return intituleConsultation;
    }

    /**
     * @param valeur positionne l'intitulé de la consultation.
     */
    public final void setIntituleConsultation(final String valeur) {
        intituleConsultation = valeur;
    }

}
