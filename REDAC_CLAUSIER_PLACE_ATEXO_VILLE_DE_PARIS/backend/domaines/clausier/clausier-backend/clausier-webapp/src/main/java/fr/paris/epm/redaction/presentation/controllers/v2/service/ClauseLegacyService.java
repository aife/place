package fr.paris.epm.redaction.presentation.controllers.v2.service;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.commun.util.Util;
import fr.paris.epm.noyau.metier.redaction.ClauseCritere;
import fr.paris.epm.noyau.metier.redaction.ClauseViewCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.ClauseFacadeGWT;
import fr.paris.epm.redaction.coordination.facade.ClauseFacade;
import fr.paris.epm.redaction.coordination.service.ClauseService;
import fr.paris.epm.redaction.metier.documentHandler.ClauseDocumentUtil;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.forms.*;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import fr.paris.epm.redaction.presentation.mapper.ClausePotentiellementConditionneeMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.RoleClauseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class ClauseLegacyService {

    private static final Logger log = LoggerFactory.getLogger(ClauseLegacyService.class);
    private final ClauseFacadeGWT clauseFacadeGWT;
    private final RedactionServiceSecurise redactionService;
    private final ClauseService clauseService;
    private final ClauseFacade clauseFacade;
    private final ClauseMapper clauseMapper;
    private final ReferentielsServiceSecurise referentielsService;
    private final DirectoryMapper directoryMapper;
    private final ClausePotentiellementConditionneeMapper clausePotentiellementConditionneeMapper;
    private final RoleClauseMapper roleClauseMapper;
    private final NoyauProxy noyauProxy;

    public ClauseLegacyService(ClauseFacadeGWT clauseFacadeGWT, RedactionServiceSecurise redactionService, ClauseService clauseService, ClauseFacade clauseFacade, ClauseMapper clauseMapper, ReferentielsServiceSecurise referentielsService, DirectoryMapper directoryMapper, ClausePotentiellementConditionneeMapper clausePotentiellementConditionneeMapper, RoleClauseMapper roleClauseMapper, NoyauProxy noyauProxy) {
        this.clauseFacadeGWT = clauseFacadeGWT;
        this.redactionService = redactionService;
        this.clauseService = clauseService;
        this.clauseFacade = clauseFacade;
        this.clauseMapper = clauseMapper;
        this.referentielsService = referentielsService;
        this.directoryMapper = directoryMapper;
        this.clausePotentiellementConditionneeMapper = clausePotentiellementConditionneeMapper;
        this.roleClauseMapper = roleClauseMapper;
        this.noyauProxy = noyauProxy;
    }


    public ClauseBean surcharger(int idClause, Integer idPublication, ClauseBean newClauseSurcharge, EpmTUtilisateur epmTUtilisateur) {
        EpmTClause clauseInitiale = clauseFacadeGWT.charger(idClause);
        EpmTClauseAbstract clausePublie = getEpmTClauseAbstract(idClause, idPublication, true, false, false, epmTUtilisateur);
        if (clausePublie == null) {
            throw new NonTrouveException("Clause non trouvée");
        }
        if (clauseInitiale == null || !clauseInitiale.isClauseEditeur() || clauseInitiale.getIdLastPublication() == null || !clauseInitiale.isActif()) {
            clauseInitiale = null;
        }
        EpmTClause clauseSurcharge = clauseService.chercherClauseSurcharge(idClause, epmTUtilisateur.getEpmTRefOrganisme());
        if (newClauseSurcharge.getInfoBulle() != null) {
            newClauseSurcharge.setInfoBulleText(newClauseSurcharge.getInfoBulle().getDescription());
            newClauseSurcharge.setInfoBulleUrl(newClauseSurcharge.getInfoBulle().getLien());
        }
        if (clauseSurcharge == null) {
            newClauseSurcharge.setIdClause(0);
            newClauseSurcharge.setId(0);


            clauseSurcharge = clauseMapper.toEpmTClause(clausePublie);

        } else {
            newClauseSurcharge.setIdClause(clauseSurcharge.getIdClause());
            newClauseSurcharge.setId(clauseSurcharge.getIdClause());
        }
        newClauseSurcharge.setReferenceClauseSurchargee(clausePublie.getReference());
        clauseSurcharge.setIdClauseOrigine(clausePublie.getIdClause());
        if (clauseInitiale == null) {
            clauseSurcharge = updateEpmTClause(clauseSurcharge, false, epmTUtilisateur, newClauseSurcharge, clausePublie.getReference());
        } else {
            clauseSurcharge = updateEpmTClause(clauseSurcharge, false, epmTUtilisateur, newClauseSurcharge, clauseInitiale.getReference());
        }


        if (clauseInitiale != null) {
            clauseInitiale.setSurchargeActif(clauseSurcharge.getSurchargeActif());
            if (clauseInitiale.getNombreSurcharges() == null) {
                clauseInitiale.setNombreSurcharges(1);
            } else {
                clauseInitiale.setNombreSurcharges(1 + clauseInitiale.getNombreSurcharges());
            }
            clauseInitiale.setDateModification(new Date());
            clauseFacadeGWT.modifier(clauseInitiale);
        }

        return findClauseSurcharge(idClause, idPublication, epmTUtilisateur);
    }


    private EpmTClause updateEpmTClause(EpmTClause epmTClauseInstance, boolean editeur, EpmTUtilisateur epmTUtilisateur, ClauseBean newClause, String referenceInitiale) {
        epmTClauseInstance.setAuteur(epmTUtilisateur.getNomComplet());
        epmTClauseInstance.setClauseEditeur(editeur);
        epmTClauseInstance.setSurchargeActif(newClause.getSurchargeActif());
        epmTClauseInstance.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        //récupération des données selon type de clause
        if (newClause.getClauseListeChoixCumulatif() != null) {
            epmTClauseInstance = this.setDataChoixCumulatif(epmTClauseInstance, newClause.getClauseListeChoixCumulatif());
        }
        if (newClause.getClauseListeChoixExclusif() != null) {
            epmTClauseInstance = this.setDataChoixExclusif(epmTClauseInstance, newClause.getClauseListeChoixExclusif());
        }
        if (newClause.getClauseTexteFixe() != null) {
            epmTClauseInstance = this.setDataTexteFixe(epmTClauseInstance, newClause.getClauseTexteFixe());
        }
        if (newClause.getClauseTexteLibre() != null) {
            epmTClauseInstance = this.setDataTexteLibre(epmTClauseInstance, newClause.getClauseTexteLibre());
        }
        if (newClause.getClauseTextePrevalorise() != null) {
            epmTClauseInstance = this.setDataTextePrevalorise(epmTClauseInstance, newClause.getClauseTextePrevalorise());
        }
        if (newClause.getClauseValeurHeritee() != null) {
            epmTClauseInstance = this.setDataValeurHeritee(epmTClauseInstance, newClause.getClauseValeurHeritee());
        }
        if (newClause.getPotentiellementConditionnees() != null && !newClause.getPotentiellementConditionnees().isEmpty()) {
            addPotentiellementConditionneeData(newClause, epmTClauseInstance);
        }
        epmTClauseInstance.setDateModification(new Date());
        if (!epmTClauseInstance.getReference().equals(referenceInitiale)) {
            //cas où référence surcharge existe déjà
            epmTClauseInstance = clauseFacadeGWT.ajouter(epmTClauseInstance, epmTClauseInstance.getReference());
        } else {
            //cas création première surcharge avec une nouvelle référence pour la surcharge
            EpmTRefParametrage parametrageReferenceSurcharge =
                    (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_REFERENCE_SURCHARGE_CLAUSE, TypeEpmTRefObject.PARAMETRAGE);
            epmTClauseInstance = clauseFacadeGWT.ajouter(epmTClauseInstance, referenceInitiale + parametrageReferenceSurcharge.getValeur());
        }
        //mettre une valeur à référence
        return epmTClauseInstance;

    }

    public ClauseBean findClause(int idClause, Integer idPublication, boolean editeur, boolean duplication, boolean surcharge, EpmTUtilisateur epmTUtilisateur) {

        ClauseBean clauseDetails = null;
        EpmTClauseAbstract epmTClause = getEpmTClauseAbstract(idClause, idPublication, editeur, duplication, surcharge, epmTUtilisateur);
        clauseDetails = clauseMapper.toClauseBean(epmTClause);
        mapRoles(clauseDetails, epmTClause, epmTUtilisateur);
        return clauseDetails;

    }

    private EpmTClauseAbstract getEpmTClauseAbstract(int idClause, Integer idPublication, boolean editeur, boolean duplication, boolean surcharge, EpmTUtilisateur epmTUtilisateur) {
        EpmTClauseAbstract epmTClause;
        if (idPublication != null) {
            EpmTRefOrganisme organisme = epmTUtilisateur.getEpmTRefOrganisme();
            ClauseViewCritere critereClause = new ClauseViewCritere(organisme.getPlateformeUuid());
            critereClause.setIdClausePublication(idClause);
            EpmVClause clauseEditeur = redactionService.chercherUniqueEpmTObject(0, critereClause);
            epmTClause = clauseFacadeGWT.charger(clauseEditeur.getEpmTClausePub().getIdClause(), clauseEditeur.getIdPublication(), organisme.getPlateformeUuid());

        } else {
            EpmTClause clause = clauseFacadeGWT.charger(idClause);
            epmTClause = clauseFacadeGWT.chargerClausePourModification(idClause, editeur, clause.isClauseEditeur(), duplication, epmTUtilisateur.getIdOrganisme(), surcharge);

        }
        return epmTClause;
    }

    private void mapRoles(ClauseBean clauseDetails, EpmTClauseAbstract epmTClause, EpmTUtilisateur epmTUtilisateur) {
        Set<PotentiellementConditionnee> potentiellementConditionneeSet = new HashSet<>();
        if (!epmTClause.getEpmTClausePotentiellementConditionnees().isEmpty()) {

            epmTClause.getEpmTClausePotentiellementConditionnees().forEach(epmTClausePotentiellementConditionnee -> {
                PotentiellementConditionnee potentiellementConditionnee = new PotentiellementConditionnee();
                potentiellementConditionnee.setId(epmTClausePotentiellementConditionnee.getEpmTRefPotentiellementConditionnee().getId());
                potentiellementConditionnee.setLibelle(epmTClausePotentiellementConditionnee.getEpmTRefPotentiellementConditionnee().getLibelle());
                potentiellementConditionnee.setMultiChoix(epmTClausePotentiellementConditionnee.getEpmTRefPotentiellementConditionnee().isMultiChoix());
                List<Integer> valeurPotentiellementConditionneList = new ArrayList<>();
                Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> valeursSet = epmTClausePotentiellementConditionnee.getEpmTClauseValeurPotentiellementConditionnees();
                valeursSet.forEach(valeur -> valeurPotentiellementConditionneList.add(valeur.getValeur()));
                potentiellementConditionnee.setValeurs(valeurPotentiellementConditionneList);
                if (!potentiellementConditionnee.isMultiChoix()) {
                    potentiellementConditionnee.setValeurId(epmTClausePotentiellementConditionnee.getEpmTClauseValeurPotentiellementConditionnees().stream().findFirst().map(EpmTClauseValeurPotentiellementConditionneeAbstract::getValeur).orElse(null));
                }

                potentiellementConditionneeSet.add(potentiellementConditionnee);
            });

            clauseDetails.setPotentiellementConditionnees(potentiellementConditionneeSet);
        }
        if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
            clauseDetails.setClauseListeChoixExclusif(clauseMapper.toClauseListeChoixExclusif(epmTClause));
        } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF) {
            clauseDetails.setClauseListeChoixCumulatif(clauseMapper.toClauseListeChoixCommulatif(epmTClause));
        } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE) {
            clauseDetails.setClauseTexteLibre(clauseMapper.toClauseTexteLibreForm(epmTClause));
        } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_FIXE) {
            clauseDetails.setClauseTexteFixe(clauseMapper.toClauseTexteFixeForm(epmTClause));
        } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            clauseDetails.setClauseTextePrevalorise(clauseMapper.toClauseTextePrevalorise(epmTClause));
        } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE) {
            ClauseValeurHeriteeForm clauseValeurHeritee = clauseMapper.toClauseValeurHeritee(epmTClause);
            clauseValeurHeritee.setTexteVariable("");
            clauseDetails.setClauseValeurHeritee(clauseValeurHeritee);
        }
        InfoBulle infoBulle = new InfoBulle();
        if ((epmTClause.getInfoBulleText() == null || epmTClause.getInfoBulleText().isEmpty()) &&
                (epmTClause.getInfoBulleUrl() == null || epmTClause.getInfoBulleUrl().isEmpty())) {
            infoBulle.setActif(false);
        } else {
            infoBulle.setDescription(epmTClause.getInfoBulleText());
            infoBulle.setLien(epmTClause.getInfoBulleUrl());
            infoBulle.setActif(true);
        }
        clauseDetails.setInfoBulle(infoBulle);
    }


    private EpmTClause setDataTexteFixe(EpmTClause epmTClauseInstance, ClauseTexteFixeForm clauseTexteFixe) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseTexteFixe.getTexteFixe()));
        return epmTClauseInstance;
    }

    private EpmTClause setDataTexteLibre(EpmTClause epmTClauseInstance, ClauseTexteLibreForm clauseTexteLibre) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseTexteLibre.getTextFixeAvant()));
        epmTClauseInstance.setTexteFixeApres(Util.sanitizeHtmlInput(clauseTexteLibre.getTextFixeApres()));
        epmTClauseInstance.setSautLigneTexteAvant(clauseTexteLibre.getSautTextFixeAvant().equals("true"));
        epmTClauseInstance.setSautLigneTexteApres(clauseTexteLibre.getSautTextFixeApres().equals("true"));
        Set<EpmTRoleClauseAbstract> rolesClauses = new HashSet<>();
        EpmTRoleClause role = new EpmTRoleClause();
        role.setEtat(EpmTRoleClauseAbstract.ROLE_ACTIVE);
        role.setEpmTClause(epmTClauseInstance);
        role.setNombreCarateresMax(clauseTexteLibre.getTailleChamps());
        role.setChampObligatoire(clauseTexteLibre.getChampLibreObligatoire());
        rolesClauses.add(role);
        epmTClauseInstance.setEpmTRoleClauses(rolesClauses);
        return epmTClauseInstance;
    }

    private EpmTClause setDataTextePrevalorise(EpmTClause epmTClauseInstance, ClauseTextePrevaloriseForm clauseTextePrevalorise) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseTextePrevalorise.getTextFixeAvant()));
        epmTClauseInstance.setTexteFixeApres(Util.sanitizeHtmlInput(clauseTextePrevalorise.getTextFixeApres()));
        epmTClauseInstance.setSautLigneTexteAvant(clauseTextePrevalorise.getSautTextFixeAvant().equals("true"));
        epmTClauseInstance.setSautLigneTexteApres(clauseTextePrevalorise.getSautTextFixeApres().equals("true"));
        epmTClauseInstance.setParametrableAgent(clauseTextePrevalorise.getParametrableAgent());
        epmTClauseInstance.setParametrableDirection(clauseTextePrevalorise.getParametrableDirection());
        Set<EpmTRoleClauseAbstract> rolesClauses = new HashSet<>();
        EpmTRoleClause role = new EpmTRoleClause();
        role.setEtat(EpmTRoleClauseAbstract.ROLE_ACTIVE);
        role.setEpmTClause(epmTClauseInstance);
        role.setNombreCarateresMax(clauseTextePrevalorise.getTailleChamps());
        role.setValeurDefaut(Util.sanitizeHtmlInput(clauseTextePrevalorise.getDefaultValue()));
        rolesClauses.add(role);
        epmTClauseInstance.setEpmTRoleClauses(rolesClauses);
        return epmTClauseInstance;
    }

    private EpmTClause setDataValeurHeritee(EpmTClause epmTClauseInstance, ClauseValeurHeriteeForm clauseValeurHeritee) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseValeurHeritee.getTextFixeAvant()));
        epmTClauseInstance.setTexteFixeApres(Util.sanitizeHtmlInput(clauseValeurHeritee.getTextFixeApres()));
        epmTClauseInstance.setSautLigneTexteAvant(clauseValeurHeritee.getSautTextFixeAvant().equals("true"));
        epmTClauseInstance.setSautLigneTexteApres(clauseValeurHeritee.getSautTextFixeApres().equals("true"));
        Set<EpmTRoleClauseAbstract> rolesClauses = new HashSet<>();
        EpmTRoleClause role = new EpmTRoleClause();
        role.setEtat(EpmTRoleClauseAbstract.ROLE_ACTIVE);
        role.setEpmTClause(epmTClauseInstance);
        role.setValeurDefaut(Util.sanitizeHtmlInput(String.valueOf(clauseValeurHeritee.getIdRefValeurTypeClause())));
        rolesClauses.add(role);
        epmTClauseInstance.setEpmTRoleClauses(rolesClauses);
        return epmTClauseInstance;
    }


    public EpmTClause createEpmTClause(boolean editeur, ClauseBean clause, EpmTUtilisateur epmTUtilisateur) {
        EpmTClause epmTClauseInstance = clauseMapper.toEpmTClause(clause);
        epmTClauseInstance = clauseFacadeGWT.miseAJourDataValidation(epmTClauseInstance);
        epmTClauseInstance.setAuteur(epmTUtilisateur.getNomComplet());
        epmTClauseInstance.setClauseEditeur(editeur);
        epmTClauseInstance.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        if (clause.getInfoBulle() != null) {
            epmTClauseInstance.setInfoBulleText(clause.getInfoBulle().getDescription());
            epmTClauseInstance.setInfoBulleUrl(clause.getInfoBulle().getLien());
        }
        epmTClauseInstance.setParametrableAgent(String.valueOf(0));
        epmTClauseInstance.setParametrableDirection(String.valueOf(0));
        //récupération des données selon type de clause
        if (clause.getClauseListeChoixCumulatif() != null) {
            epmTClauseInstance = this.setDataChoixCumulatif(epmTClauseInstance, clause.getClauseListeChoixCumulatif());
        }
        if (clause.getClauseListeChoixExclusif() != null) {
            epmTClauseInstance = this.setDataChoixExclusif(epmTClauseInstance, clause.getClauseListeChoixExclusif());
        }
        if (clause.getClauseTexteFixe() != null) {
            epmTClauseInstance = this.setDataTexteFixe(epmTClauseInstance, clause.getClauseTexteFixe());
        }
        if (clause.getClauseTexteLibre() != null) {
            epmTClauseInstance = this.setDataTexteLibre(epmTClauseInstance, clause.getClauseTexteLibre());
        }
        if (clause.getClauseTextePrevalorise() != null) {
            epmTClauseInstance = this.setDataTextePrevalorise(epmTClauseInstance, clause.getClauseTextePrevalorise());
        }
        if (clause.getClauseValeurHeritee() != null) {
            epmTClauseInstance = this.setDataValeurHeritee(epmTClauseInstance, clause.getClauseValeurHeritee());
        }
        if (clause.getPotentiellementConditionnees() != null && !clause.getPotentiellementConditionnees().isEmpty()) {
            addPotentiellementConditionneeData(clause, epmTClauseInstance);
        }
        epmTClauseInstance = clauseFacadeGWT.ajouter(epmTClauseInstance, null);
        return epmTClauseInstance;

    }


    private void addPotentiellementConditionneeData(ClauseBean clause, EpmTClause epmTClauseInstance) {
        Set<EpmTClausePotentiellementConditionnee> valeursPC = new HashSet<>();
        clause.getPotentiellementConditionnees().forEach(potentiellementConditionnee -> {
            EpmTClausePotentiellementConditionnee clausePC = new EpmTClausePotentiellementConditionnee();
            clausePC.setEpmTClause(epmTClauseInstance);
            clausePC.setEpmTRefPotentiellementConditionnee(referentielsService.chercherObject(potentiellementConditionnee.getId(), EpmTRefPotentiellementConditionnee.class));

            Set<EpmTClauseValeurPotentiellementConditionnee> valeursSet = new HashSet<>();
            if (potentiellementConditionnee.getValeurs() != null && !potentiellementConditionnee.getValeurs().isEmpty()) {
                potentiellementConditionnee.getValeurs().forEach(valeur -> {
                    EpmTClauseValeurPotentiellementConditionnee valeurPC = new EpmTClauseValeurPotentiellementConditionnee();
                    valeurPC.setEpmTClausePotentiellementConditionnee(clausePC);
                    valeurPC.setValeur((Integer) valeur);
                    valeursSet.add(valeurPC);
                });
            } else if (potentiellementConditionnee.getValeurId() != null) {
                EpmTClauseValeurPotentiellementConditionnee valeurPC = new EpmTClauseValeurPotentiellementConditionnee();
                valeurPC.setEpmTClausePotentiellementConditionnee(clausePC);
                valeurPC.setValeur(potentiellementConditionnee.getValeurId());
                valeursSet.add(valeurPC);
            }
            clausePC.setEpmTClauseValeurPotentiellementConditionnees(valeursSet);
            valeursPC.add(clausePC);
        });

        epmTClauseInstance.setEpmTClausePotentiellementConditionnees(valeursPC);

    }


    private EpmTClause setDataChoixCumulatif(EpmTClause epmTClauseInstance, ClauseListeChoixCumulatifForm clauseListeChoixCumulatif) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseListeChoixCumulatif.getTextFixeAvant()));
        epmTClauseInstance.setTexteFixeApres(Util.sanitizeHtmlInput(clauseListeChoixCumulatif.getTextFixeApres()));
        epmTClauseInstance.setSautLigneTexteAvant(clauseListeChoixCumulatif.getSautTextFixeAvant().equals("true"));
        epmTClauseInstance.setSautLigneTexteApres(clauseListeChoixCumulatif.getSautTextFixeApres().equals("true"));
        epmTClauseInstance.setFormulationModifiable(clauseListeChoixCumulatif.isModifiable());
        epmTClauseInstance.setParametrableAgent(clauseListeChoixCumulatif.getParametrableAgent());
        epmTClauseInstance.setParametrableDirection(clauseListeChoixCumulatif.getParametrableDirection());
        Set<EpmTRoleClauseAbstract> rolesClauses = new HashSet<>();
        setListChoixCumulatifRoles(epmTClauseInstance, clauseListeChoixCumulatif, rolesClauses);
        epmTClauseInstance.setEpmTRoleClauses(rolesClauses);
        return epmTClauseInstance;
    }

    private void setListChoixCumulatifRoles(EpmTClause epmTClauseInstance, ClauseListeChoixCumulatifForm clauseListeChoixCumulatif, Set<EpmTRoleClauseAbstract> rolesClauses) {
        for (ClauseFormulaire formulaire : clauseListeChoixCumulatif.getFormulaires()) {
            EpmTRoleClause role = new EpmTRoleClause();
            role.setEpmTClause(epmTClauseInstance);
            role.setEtat(EpmTRoleClauseAbstract.ROLE_ACTIVE);
            role.setValeurDefaut(Util.sanitizeHtmlInput(formulaire.getDefaultValeur()));
            role.setNumFormulation(formulaire.getNumFormulation());
            if (formulaire.isPrecochee()) {
                role.setPrecochee(true);
            } else {
                role.setPrecochee(false);
            }
            role.setNombreCarateresMax(Integer.parseInt(formulaire.getTailleChamps()));
            rolesClauses.add(role);
        }
    }

    private EpmTClause setDataChoixExclusif(EpmTClause epmTClauseInstance, ClauseListeChoixExclusifForm clauseListeChoixExclusif) {
        epmTClauseInstance.setTexteFixeAvant(Util.sanitizeHtmlInput(clauseListeChoixExclusif.getTextFixeAvant()));
        epmTClauseInstance.setTexteFixeApres(Util.sanitizeHtmlInput(clauseListeChoixExclusif.getTextFixeApres()));
        epmTClauseInstance.setSautLigneTexteAvant(clauseListeChoixExclusif.getSautTextFixeAvant().equals("true"));
        epmTClauseInstance.setSautLigneTexteApres(clauseListeChoixExclusif.getSautTextFixeApres().equals("true"));
        epmTClauseInstance.setFormulationModifiable(clauseListeChoixExclusif.isModifiable());
        epmTClauseInstance.setParametrableAgent(clauseListeChoixExclusif.getParametrableAgent());
        epmTClauseInstance.setParametrableDirection(clauseListeChoixExclusif.getParametrableDirection());
        Set<EpmTRoleClauseAbstract> rolesClauses = new HashSet<>();
        for (ClauseFormulaire formulaire : clauseListeChoixExclusif.getFormulaires()) {
            EpmTRoleClause role = new EpmTRoleClause();
            role.setEpmTClause(epmTClauseInstance);
            role.setEtat(EpmTRoleClauseAbstract.ROLE_ACTIVE);
            role.setValeurDefaut(Util.sanitizeHtmlInput(formulaire.getDefaultValeur()));
            role.setNumFormulation(formulaire.getNumFormulation());
            if (formulaire.isPrecochee()) {
                role.setPrecochee(true);
            } else {
                role.setPrecochee(false);
            }
            role.setNombreCarateresMax(Integer.parseInt(formulaire.getTailleChamps()));
            rolesClauses.add(role);
        }
        epmTClauseInstance.setEpmTRoleClauses(rolesClauses);
        return epmTClauseInstance;
    }

    public ClauseBean modifyClause(ClauseBean clause, EpmTUtilisateur epmTUtilisateur) {
        EpmTClause epmTClauseInstance = clauseMapper.toEpmTClause(clause);
        epmTClauseInstance.setAuteur(epmTUtilisateur.getNom());
        epmTClauseInstance.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        if (clause.getInfoBulle() != null) {
            epmTClauseInstance.setInfoBulleText(clause.getInfoBulle().getDescription());
            epmTClauseInstance.setInfoBulleUrl(clause.getInfoBulle().getLien());
        }
        epmTClauseInstance.setParametrableAgent(String.valueOf(0));
        epmTClauseInstance.setParametrableDirection(String.valueOf(0));
        //récupération des données selon type de clause
        if (clause.getClauseListeChoixCumulatif() != null) {
            epmTClauseInstance = this.setDataChoixCumulatif(epmTClauseInstance, clause.getClauseListeChoixCumulatif());
        }
        if (clause.getClauseListeChoixExclusif() != null) {
            epmTClauseInstance = this.setDataChoixExclusif(epmTClauseInstance, clause.getClauseListeChoixExclusif());
        }
        if (clause.getClauseTexteFixe() != null) {
            epmTClauseInstance = this.setDataTexteFixe(epmTClauseInstance, clause.getClauseTexteFixe());
        }
        if (clause.getClauseTexteLibre() != null) {
            epmTClauseInstance = this.setDataTexteLibre(epmTClauseInstance, clause.getClauseTexteLibre());
        }
        if (clause.getClauseTextePrevalorise() != null) {
            epmTClauseInstance = this.setDataTextePrevalorise(epmTClauseInstance, clause.getClauseTextePrevalorise());
        }
        if (clause.getClauseValeurHeritee() != null) {
            epmTClauseInstance = this.setDataValeurHeritee(epmTClauseInstance, clause.getClauseValeurHeritee());
        }
        epmTClauseInstance.setDateModification(new Date());
        //si paramétrage agent et/ou direction désactivés passer l'état des roles clauses correspondant à 0
        EpmTClause tClause = clauseFacadeGWT.charger(epmTClauseInstance.getIdClause());
        if (epmTClauseInstance.getParametrableDirection().equals("0")) {
            List<? extends EpmTRoleClauseAbstract> rolesClauseDirection = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), tClause, false);
            if (!CollectionUtils.isEmpty(rolesClauseDirection)) {
                rolesClauseDirection.forEach(role -> {
                    role.setEtat(EpmTRoleClauseAbstract.ROLE_INACTIVE);
                    redactionService.modifierEpmTObject(role);
                });
            }
        }
        if (epmTClauseInstance.getParametrableAgent().equals("0")) {
            List<? extends EpmTRoleClauseAbstract> rolesClauseAgent = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), tClause, false);
            if (!CollectionUtils.isEmpty(rolesClauseAgent)) {
                rolesClauseAgent.forEach(role -> {
                    role.setEtat(EpmTRoleClauseAbstract.ROLE_INACTIVE);
                    redactionService.modifierEpmTObject(role);
                });
            }
        }
        epmTClauseInstance = clauseFacadeGWT.modifier(epmTClauseInstance);
        return clauseMapper.toClauseBean(epmTClauseInstance);
    }

    public ClauseBean cloneClause(boolean editeur, ClauseBean clause, EpmTUtilisateur epmTUtilisateur) {
        clause.setIdClause(0);
        clause.setId(0);
        clause.setReferenceClauseSurchargee(null);
        clause.setSurchargeActif(false);
        EpmTClause epmTClauseInstance = createEpmTClause(editeur, clause, epmTUtilisateur);
        return clauseMapper.toClauseBean(epmTClauseInstance);
    }

    public ClauseBean findClauseSurcharge(int idClause, Integer idPublication, EpmTUtilisateur epmTUtilisateur) {
        EpmTClauseAbstract epmTClauseInstance;
        String plateformeUuid = epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid();
        if (idPublication != null) {
            epmTClauseInstance = clauseFacadeGWT.charger(idClause, idPublication, plateformeUuid);
        } else {
            epmTClauseInstance = clauseFacadeGWT.charger(idClause);
        }

        if (null == epmTClauseInstance || !epmTClauseInstance.isClauseEditeur() || epmTClauseInstance.getIdLastPublication() == null || !epmTClauseInstance.isActif()) {
            return null;
        }
        ClauseCritere clauseCritere = new ClauseCritere(plateformeUuid);
        clauseCritere.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
        clauseCritere.setEditeur(false);
        clauseCritere.setIdClauseOrigine(epmTClauseInstance.getIdClause());

        EpmTClause epmTClauseSurcharge = clauseService.chercherClauseSurcharge(idClause, epmTUtilisateur.getEpmTRefOrganisme());

        if (epmTClauseSurcharge == null) {
            epmTClauseSurcharge = clauseMapper.toEpmTClause(epmTClauseInstance);
            epmTClauseSurcharge.setId(0);

            epmTClauseSurcharge.setEpmTRoleClauses(new HashSet<>());
            for (EpmTRoleClauseAbstract role : epmTClauseInstance.getEpmTRoleClauses()) {
                EpmTRoleClause r = roleClauseMapper.cloneEpmTRoleClause(role);
                r.setEpmTClause(epmTClauseSurcharge);
                epmTClauseSurcharge.getEpmTRoleClauses().add(r);
            }
            epmTClauseSurcharge.setEpmTClausePotentiellementConditionnees(new HashSet<>());
            for (EpmTClausePotentiellementConditionneeAbstract condition : epmTClauseInstance.getEpmTClausePotentiellementConditionnees()) {

                EpmTClauseAbstract epmTClauseInit = condition.getEpmTClause();
                condition.setEpmTClause(null); // petit bricolage pour bien passer le mapper

                EpmTClausePotentiellementConditionnee c = clausePotentiellementConditionneeMapper.toEpmTClausePotentiellementConditionnee(condition);
                c.setEpmTClause(epmTClauseSurcharge);

                c.setEpmTClauseValeurPotentiellementConditionnees(new HashSet<>());
                for (EpmTClauseValeurPotentiellementConditionneeAbstract valeur : condition.getEpmTClauseValeurPotentiellementConditionnees()) {
                    EpmTClauseValeurPotentiellementConditionnee v = clausePotentiellementConditionneeMapper.toEpmTClauseValeurPotentiellementConditionnee(valeur);
                    v.setEpmTClausePotentiellementConditionnee(c);
                    c.getEpmTClauseValeurPotentiellementConditionnees().add(v);
                }

                epmTClauseSurcharge.getEpmTClausePotentiellementConditionnees().add(c);
                condition.setEpmTClause(epmTClauseInit); // petit bricolage pour bien passer le mapper (retour en arrière comme ça a été pour les conditions initiales)
            }

            epmTClauseSurcharge.setIdOrganisme(epmTUtilisateur.getIdOrganisme());
            epmTClauseSurcharge.setReferenceClauseSurchargee(epmTClauseInstance.getReference());
            epmTClauseSurcharge.setClauseEditeur(false);
        }


        ClauseBean clauseBean = clauseFacade.map(epmTClauseSurcharge, epmTUtilisateur.getIdOrganisme());
        mapRoles(clauseBean, epmTClauseSurcharge, epmTUtilisateur);
        clauseBean.setSurchargeActif(epmTClauseSurcharge.getSurchargeActif());
        return clauseBean;
    }


    public ClauseBean findClausePreferenceAgent(int idClausePub, Integer idPublication, EpmTUtilisateur epmTUtilisateur, boolean rolesActifs) {
        EpmTClause tClauseSurchargee = clauseService.chercherClauseSurcharge(idClausePub, epmTUtilisateur.getEpmTRefOrganisme());
        EpmTClausePub clausePub = clauseFacadeGWT.charger(idClausePub, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        if ("0".equals(clausePub.getParametrableAgent()))
            throw new TechnicalException("Clause non paramétrable agent");
        Integer id = clausePub.getEpmTRefTypeClause().getId();
        if (id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                && id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF && id != EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            throw new TechnicalException("Type clause non paramétrable agent");
        }
        ClauseBean clauseBean = clauseMapper.toClauseBean(clausePub);
        List<? extends EpmTRoleClauseAbstract> rolesClause = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), clausePub, rolesActifs);
        if (rolesClause.isEmpty()) {
            //pas encore de paramétrage agent on regarde si on a un paramétrage direction actif
            clauseBean.setParametreActif(false);
            rolesClause = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), clausePub, true);
            if (rolesClause.isEmpty()) {
                //pas de paramétrage direction actif on regarde si surcharge active
                if (tClauseSurchargee != null && tClauseSurchargee.getSurchargeActif()) {
                    mapRoles(clauseBean, tClauseSurchargee, epmTUtilisateur);
                } else {
                    //sinon on affiche les roles clause par defaut
                    mapRoles(clauseBean, clausePub, epmTUtilisateur);
                }
            } else {
                //on affiche paramétrage direction
                clausePub.setEpmTRoleClauses(new HashSet<>(rolesClause));
                mapRoles(clauseBean, clausePub, epmTUtilisateur);
            }
        } else {
            //le paramétrage agent agent existe on affiche les roles clause du paramétrage agent
            if (rolesClause.get(0).getEtat().equals("0")) {
                clauseBean.setParametreActif(false);
            } else {
                clauseBean.setParametreActif(true);
            }
            clausePub.setEpmTRoleClauses(new HashSet<>(rolesClause));
            mapRoles(clauseBean, clausePub, epmTUtilisateur);
        }

        return clauseBean;
    }

    public ClauseBean findClausePreferenceDirection(int idClausePub, Integer idPublication, EpmTUtilisateur epmTUtilisateur, boolean rolesActifs) {
        EpmTClause tClauseSurchargee = clauseService.chercherClauseSurcharge(idClausePub, epmTUtilisateur.getEpmTRefOrganisme());
        EpmTClausePub epmTClausePub = clauseFacadeGWT.charger(idClausePub, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());

        if ("0".equals(epmTClausePub.getParametrableDirection())) {
            throw new TechnicalException("Clause non paramétrable direction");
        }
        Integer id = epmTClausePub.getEpmTRefTypeClause().getId();
        if (id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                && id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF && id != EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            throw new TechnicalException("Type clause non paramétrable direction");
        }

        ClauseBean clauseBean = clauseMapper.toClauseBean(epmTClausePub);
        List<? extends EpmTRoleClauseAbstract> rolesClauseDirection = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), epmTClausePub, rolesActifs);
        if (!rolesClauseDirection.isEmpty()) {
            //on affiche les roles clause du paramétrage direction existant
            epmTClausePub.setEpmTRoleClauses(new HashSet<>(rolesClauseDirection));
            mapRoles(clauseBean, epmTClausePub, epmTUtilisateur);
            clauseBean.setParametreActif(rolesClauseDirection.get(0).getEtat().equals("1"));

        } else {
            clauseBean.setParametreActif(false);
            //si la surcharge est active on affiche les roles clause de la surcharge
            if (tClauseSurchargee != null && tClauseSurchargee.getSurchargeActif()) {
                mapRoles(clauseBean, tClauseSurchargee, epmTUtilisateur);
            } else {
                //sinon on affiche les roles clause par defaut
                mapRoles(clauseBean, epmTClausePub, epmTUtilisateur);
            }
        }

        return clauseBean;
    }


    private void traitementParametreDefaut(EpmTUtilisateur user,
                                           String ecranPreferences, EpmTClauseAbstract clause, boolean actif) {

        List<EpmTRoleClauseAbstract> roleClauseAgent = clauseFacadeGWT.recupererRolesClauseAgent(user.getId(), clause, false);
        List<EpmTRoleClauseAbstract> rolesClauseDirection = clauseFacadeGWT.recupererRolesClauseDirection(user.getDirectionService(), clause, false);
        if (ecranPreferences.equals(Constante.PRE_DIRECTION)) {
            if (rolesClauseDirection.isEmpty()) {
                updateEtatRoleClause(!actif, rolesClauseDirection);
            }
        } else {
            if (roleClauseAgent.isEmpty()) {
                updateEtatRoleClause(!actif, roleClauseAgent);
            }
        }
    }

    private void updateEtatRoleClause(boolean actif, List<? extends EpmTRoleClauseAbstract> rolesClausesListe) {
        for (EpmTRoleClauseAbstract roleClause : rolesClausesListe) {
            if (roleClause instanceof EpmTRoleClause) {
                roleClause.setEtat(actif ? EpmTRoleClause.ROLE_INACTIVE : EpmTRoleClause.ROLE_ACTIVE);
                clauseFacadeGWT.modifierRoleClause((EpmTRoleClause) roleClause);
            }
        }
    }


    public ClauseBean modifyClausePreferenceDirection(int idClause, Integer idPublication, EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean) {
        //EpmTClauseAbstract clause = clauseService.chercherClauseSurcharge(idClause, epmTUtilisateur.getEpmTRefOrganisme());
        EpmTClausePub clause = clauseFacadeGWT.charger(idClause, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        /*if (clause == null || (epmTClause != null && !epmTClause.getSurchargeActif())) {
            clause = getEpmTClauseAbstract(idClause, idPublication, true, false, false, epmTUtilisateur);
        }*/
        if ("0".equals(clause.getParametrableDirection()))
            throw new TechnicalException("Clause non paramétrable direction");
        Integer id = clause.getEpmTRefTypeClause().getId();
        if (id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                && id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF && id != EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            throw new TechnicalException("Le type de clause ne permet pas le paramétrage direction");
        }
        //modification ou création du paramétrage direction
        if (id == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                || id == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF) {
            modifyDirectionList(epmTUtilisateur, clauseBean, clause);
        }
        if (id == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            modifyDirectionTextePrevalorise(epmTUtilisateur, clauseBean, clause);
        }
        //
        traitementParametreDefaut(epmTUtilisateur, Constante.PRE_DIRECTION, clause, clauseBean.getParametreActif());
        return null;
    }

    public ClauseBean modifyClausePreferenceAgent(int idClause, Integer idPublication, EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean) {
        //EpmTClauseAbstract clause = clauseService.chercherClauseSurcharge(idClause, epmTUtilisateur.getEpmTRefOrganisme());
        EpmTClausePub clause = clauseFacadeGWT.charger(idClause, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        /*if (clause == null || (epmTClause != null && !epmTClause.getSurchargeActif())) {
            clause = getEpmTClauseAbstract(idClause, idPublication, true, false, false, epmTUtilisateur);
        }*/
        if ("0".equals(clause.getParametrableAgent()))
            throw new TechnicalException("Clause non paramétrable agent");
        Integer id = clause.getEpmTRefTypeClause().getId();
        if (id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                && id != EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF && id != EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            throw new TechnicalException("Le type de clause ne permet pas le paramétrage agent");
        }
        //modification ou création du paramétrage agent
        if (id == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF
                || id == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF) {
            modifyAgentListe(epmTUtilisateur, clauseBean, clause);
        }
        if (id == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
            modifyAgentTextePrevalorise(epmTUtilisateur, clauseBean, clause);
        }

        traitementParametreDefaut(epmTUtilisateur, Constante.PRE_AGENT, clause, clauseBean.getParametreActif());
        return null;
    }

    private void modifyDirectionTextePrevalorise(EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean, EpmTClauseAbstract clause) {
        List<? extends EpmTRoleClauseAbstract> liste = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), clause, false);
        EpmTRoleClauseAbstract roleClause = getEpmTRoleClauseTextePrevalorise(clause, liste, clauseBean);
        clauseFacadeGWT.modifierPreferences((EpmTRoleClause) roleClause, false, epmTUtilisateur.getId(), epmTUtilisateur.getDirectionService(), clause);
    }


    private void modifyAgentTextePrevalorise(EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean, EpmTClauseAbstract clause) {
        List<? extends EpmTRoleClauseAbstract> liste = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), clause, false);
        EpmTRoleClauseAbstract roleClause = getEpmTRoleClauseTextePrevalorise(clause, liste, clauseBean);
        clauseFacadeGWT.modifierPreferences((EpmTRoleClause) roleClause, true, epmTUtilisateur.getId(), epmTUtilisateur.getDirectionService(), clause);
    }

    private EpmTRoleClauseAbstract getEpmTRoleClauseTextePrevalorise(EpmTClauseAbstract clause, List<? extends EpmTRoleClauseAbstract> liste, ClauseBean clauseBean) {
        EpmTRoleClauseAbstract roleClause;
        if (liste.isEmpty()) {
            //paramétrage inexistant: création du role paramétrage agent/direction à partir du role par defaut
            roleClause = cloneRoleClauseTextePrevalorise(clause);
        } else {
            //paramétrage existant
            roleClause = liste.get(0);
        }
        roleClause.setEtat(clauseBean.getParametreActif() ? EpmTRoleClause.ROLE_ACTIVE : EpmTRoleClause.ROLE_INACTIVE);
        roleClause.setPrecochee(true);
        roleClause.setValeurDefaut(clauseBean.getClauseTextePrevalorise().getDefaultValue());
        return roleClause;
    }

    private EpmTRoleClauseAbstract cloneRoleClauseTextePrevalorise(EpmTClauseAbstract clause) {
        EpmTRoleClauseAbstract roleClause = clause.getEpmTRoleClausesDefault().get(0);
        try {
            if (roleClause instanceof EpmTRoleClause) {
                roleClause = roleClause.clone();
            } else if (roleClause instanceof EpmTRoleClausePub) {
                EpmTRoleClause roleClauseClone = roleClauseMapper.cloneEpmTRoleClause(roleClause);
                roleClause = roleClauseClone.clone();
            } else {
                throw new TechnicalException("Type de role clause non reconnu");
            }
        } catch (CloneNotSupportedException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        }
        return roleClause;
    }

    private void modifyDirectionList(EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean, EpmTClauseAbstract clause) {
        List<? extends EpmTRoleClauseAbstract> liste = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), clause, false);
        List<EpmTRoleClauseAbstract> liste2 = getEpmTRoleClauseListe(clause, liste);
        traitementParametreAgentOuDirection(liste2, clauseBean, epmTUtilisateur, Constante.PRE_DIRECTION, clause);
    }

    private void modifyAgentListe(EpmTUtilisateur epmTUtilisateur, ClauseBean clauseBean, EpmTClauseAbstract clause) {
        List<? extends EpmTRoleClauseAbstract> liste = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), clause, false);
        List<EpmTRoleClauseAbstract> liste2 = getEpmTRoleClauseListe(clause, liste);
        traitementParametreAgentOuDirection(liste2, clauseBean, epmTUtilisateur, Constante.PRE_AGENT, clause);
    }

    private List<EpmTRoleClauseAbstract> getEpmTRoleClauseListe(EpmTClauseAbstract clause, List<? extends EpmTRoleClauseAbstract> liste) {
        List<EpmTRoleClauseAbstract> liste2;
        if (liste.isEmpty()) {
            //paramétrage agent inexistant: création de roles paramétrages agent à partir des roles par defaut
            liste = clause.getEpmTRoleClausesDefault();
            liste2 = new ArrayList<>();
            cloneRolesClausesDefautListes(liste, liste2);
        } else {
            //paramétrage agent existant
            liste2 = (List<EpmTRoleClauseAbstract>) liste;
        }
        return liste2;
    }

    private void cloneRolesClausesDefautListes(List<? extends EpmTRoleClauseAbstract> liste, List<EpmTRoleClauseAbstract> liste2) {
        for (EpmTRoleClauseAbstract roleClause : liste) {
            try {
                EpmTRoleClauseAbstract cloneRole;
                if (roleClause instanceof EpmTRoleClause) {
                    cloneRole = roleClause.clone();
                } else if (roleClause instanceof EpmTRoleClausePub) {
                    EpmTRoleClause roleClauseClone = roleClauseMapper.cloneEpmTRoleClause(roleClause);
                    cloneRole = roleClauseClone.clone();
                } else {
                    throw new TechnicalException("Type de role clause non reconnu");
                }
                liste2.add(cloneRole);
            } catch (CloneNotSupportedException e) {
                log.error(e.getMessage(), e.fillInStackTrace());
                throw new TechnicalException(e.fillInStackTrace());
            }
        }
    }


    /**
     * Permet mettre à jour les informations du role clause (notamment la valeur, si c'est active ou preocchée)
     *
     * @param rolesClausesListe liste des roles clauses à ajouter
     * @param form              le formulaire pour récupérer la valeur de la role clause et pour savoir si la role clause en question sera cochée sur l'écran
     * @param user              informations de l'utilisateur (direction et l'id de l'utilisateur)
     * @param ecranPreferences  boolean pour connaître si le traitement est sur une écran agent ou direction
     * @param clause            la clause en question
     */
    private void traitementParametreAgentOuDirection(List<? extends
            EpmTRoleClauseAbstract> rolesClausesListe, ClauseBean form,
                                                     EpmTUtilisateur user, String ecranPreferences, EpmTClauseAbstract clause) {
        ClauseListeChoixCumulatifForm clauseListeChoixCumulatif = form.getClauseListeChoixCumulatif();
        ClauseListeChoixExclusifForm clauseListeChoixExclusif = form.getClauseListeChoixExclusif();
        List<ClauseFormulaire> formulaires = new ArrayList<>();
        if (clauseListeChoixCumulatif != null && !CollectionUtils.isEmpty(clauseListeChoixCumulatif.getFormulaires()))
            formulaires = clauseListeChoixCumulatif.getFormulaires();
        if (clauseListeChoixExclusif != null && !CollectionUtils.isEmpty(clauseListeChoixExclusif.getFormulaires()))
            formulaires = clauseListeChoixExclusif.getFormulaires();
        if (!CollectionUtils.isEmpty(formulaires)) {
            for (int i = 0; i < rolesClausesListe.size(); i++) {
                EpmTRoleClauseAbstract roleClause = rolesClausesListe.get(i);
                roleClause.setEtat(form.getParametreActif() ? EpmTRoleClause.ROLE_ACTIVE : EpmTRoleClause.ROLE_INACTIVE);
                roleClause.setValeurDefaut(formulaires.get(i).getDefaultValeur());
                if (formulaires.get(i).isPrecochee())
                    roleClause.setPrecochee(true);
                else
                    roleClause.setPrecochee(false);
            }
        }
        clauseFacadeGWT.modifierPreferences(rolesClausesListe, ecranPreferences.equals(Constante.PRE_AGENT), user.getId(), user.getDirectionService(), clause);
    }

    public ClauseDocument previewClauseDocument(int idClause, boolean editeur, Integer idPublication, Boolean surcharge, Boolean agent, Boolean direction, EpmTUtilisateur epmTUtilisateur, ResourceBundleMessageSource messageSource) {

        EpmTClauseAbstract clause = null;
        if (editeur) {
            clause = getClauseOrigine(idClause, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        } else {
            //on affiche la surcharge si elle est active ou la clause initiale
            if (Boolean.TRUE.equals(surcharge)) {
                //récupérer surcharge
                clause = clauseService.chercherDerniereSurchargeActif(idClause, epmTUtilisateur.getEpmTRefOrganisme());
            } else {
                clause = getClauseOrigine(idClause, idPublication, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
            }
            //récupérer les éventuelles surcharges agent ou direction
            List<? extends EpmTRoleClauseAbstract> rolesClauseDirection = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), clause, true);
            if (!CollectionUtils.isEmpty(rolesClauseDirection)) {
                clause.setEpmTRoleClauses(new HashSet<>(rolesClauseDirection));
            }
            List<? extends EpmTRoleClauseAbstract> rolesClauseAgent = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), clause, true);
            if (!CollectionUtils.isEmpty(rolesClauseAgent)) {
                clause.setEpmTRoleClauses(new HashSet<>(rolesClauseAgent));
            }
            /*if (Boolean.TRUE.equals(agent)) {
                List<? extends EpmTRoleClauseAbstract> rolesClause = clauseFacadeGWT.recupererRolesClauseAgent(epmTUtilisateur.getId(), clause, true);
                if (!CollectionUtils.isEmpty(rolesClause)) {
                    clause.setEpmTRoleClauses(new HashSet<>(rolesClause));
                }
            } else if (Boolean.TRUE.equals(direction)) {
                List<? extends EpmTRoleClauseAbstract> rolesClause = clauseFacadeGWT.recupererRolesClauseDirection(epmTUtilisateur.getDirectionService(), clause, true);
                if (!CollectionUtils.isEmpty(rolesClause)) {
                    clause.setEpmTRoleClauses(new HashSet<>(rolesClause));
                }
            }*/


        }
        Clause clauseCanevas = clauseFacadeGWT.epmTClauseToClause(clause, editeur, epmTUtilisateur.getEpmTRefOrganisme().getPlateformeUuid());
        return ClauseDocumentUtil.clauseCanevasToClauseDocument(clauseCanevas, epmTUtilisateur, messageSource, editeur);
    }

    private EpmTClauseAbstract getClauseOrigine(int idClause, Integer idPublication, String plateformeUuid) {
        EpmTClauseAbstract clause;
        //récupérer clause origine
        if (idPublication != null) {
            ClauseViewCritere critereClause = new ClauseViewCritere(plateformeUuid);
            critereClause.setIdClausePublication(idClause);
            clause = clauseFacadeGWT.charger(idClause, idPublication, plateformeUuid);
        } else {
            clause = clauseFacadeGWT.charger(idClause);
        }
        return clause;
    }


    public ClauseDocument getClauseDocument(ClauseBean clauseBean, EpmTUtilisateur utilisateur) {


        EpmTRefTypeClause type = directoryMapper.epmTRefTypeClauseById(clauseBean.getIdTypeClause());

        switch (type.getId()) {
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE:
                log.debug("Aller de clauseBean type texte libre à ClauseTexte");
                return ClauseDocumentUtil.getClauseTexteLibre(clauseBean, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_FIXE:
                log.debug("Aller de clauseBean type texte fixe à ClauseTexte");
                return ClauseDocumentUtil.getClauseTexteFixe(clauseBean, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE:
                log.debug("Aller de clauseBean type texte prévalorisé à ClauseTexte");
                return ClauseDocumentUtil.getClauseTextePrevalorise(clauseBean, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE:
                log.debug("Aller de clauseBean type texte valeur héritée à ClauseTexte");
                return ClauseDocumentUtil.getClauseTexteValeurHeritee(clauseBean, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF:
                log.debug("Aller de clauseBean type liste exclusif à ClauseListe");
                return ClauseDocumentUtil.getClauseListeExclusif(clauseBean, utilisateur);
            case EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF:
                log.debug("Aller de clauseBean type liste cumulatif à ClauseListe");
                return ClauseDocumentUtil.getClauseListeCumulatif(clauseBean, utilisateur);
            default:
                throw new TechnicalException("Cet id type de clause n'existe pas: " + clauseBean.getIdTypeClause());
        }

    }


    public ClauseBean updateStatutClause(int idClause, int idStatus) {
        log.info("changement de statut pour {} à {}", idClause, idStatus);
        EpmTClause epmTClauseInstance = clauseFacadeGWT.charger(idClause);
        epmTClauseInstance.setEpmTRefStatutRedactionClausier(redactionService.chercherObject(idStatus, EpmTRefStatutRedactionClausier.class));
        epmTClauseInstance = clauseFacadeGWT.miseAJourDataValidation(epmTClauseInstance);
        epmTClauseInstance = clauseFacadeGWT.modifier(epmTClauseInstance);
        return clauseFacade.map(epmTClauseInstance, epmTClauseInstance.getIdOrganisme());
    }
}
