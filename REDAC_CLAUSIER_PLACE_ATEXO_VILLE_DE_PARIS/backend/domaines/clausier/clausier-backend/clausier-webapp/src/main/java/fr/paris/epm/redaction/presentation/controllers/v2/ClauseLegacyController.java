package fr.paris.epm.redaction.presentation.controllers.v2;

import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.global.commun.worker.RsemTaskWorker;
import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.commun.worker.RedactionTaskFactory;
import fr.paris.epm.redaction.coordination.ClausierFacade;
import fr.paris.epm.redaction.coordination.facade.ClauseFacade;
import fr.paris.epm.redaction.coordination.facade.ClauseViewFacade;
import fr.paris.epm.redaction.importExport.mapper.PublicationClausierMapper;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.bean.ClauseSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import fr.paris.epm.redaction.presentation.controllers.v2.model.PatchStatut;
import fr.paris.epm.redaction.presentation.controllers.v2.service.ClauseLegacyService;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v2/clauses")
public class ClauseLegacyController extends AbstractController {
    private static final Logger log = LoggerFactory.getLogger(ClauseLegacyController.class);
    private final ClauseLegacyService clauseLegacyService;
    private final ClauseFacade clauseFacade;
    private final ClausierFacade clausierFacade;
    private final ClauseMapper clauseMapper;
    private final RedactionTaskFactory redactionTaskFactory;
    private final RsemTaskWorker rsemTaskWorker;
    private final PublicationClausierMapper publicationClausierMapper;
    private final ResourceBundleMessageSource messageSource;
    private final ClauseViewFacade clauseViewFacade;




    public ClauseLegacyController(ClauseLegacyService clauseLegacyService, ClauseFacade clauseFacade, ClausierFacade clausierFacade, ClauseMapper clauseMapper, RedactionTaskFactory redactionTaskFactory, RsemTaskWorker rsemTaskWorker, PublicationClausierMapper publicationClausierMapper, ClauseViewFacade clauseViewFacade) {
        this.clauseLegacyService = clauseLegacyService;
        this.clauseFacade = clauseFacade;
        this.clausierFacade = clausierFacade;
        this.clauseMapper = clauseMapper;
        this.redactionTaskFactory = redactionTaskFactory;
        this.rsemTaskWorker = rsemTaskWorker;
        this.publicationClausierMapper = publicationClausierMapper;
        this.messageSource = messageSource();
        this.clauseViewFacade = clauseViewFacade;
    }


    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("ApplicationResources", "ApplicationResources-commun");
        return resourceBundleMessageSource;
    }

    @GetMapping(params = {"page", "size"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PageRepresentation<ClauseBean> listClauses(final ClauseSearch search, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("/liste Clauses");
        if (search.getDateModificationMin() != null && search.getDateModificationMax() != null && search.getDateModificationMin().equals(search.getDateModificationMax())) {
            //ajouter une durée de 24 heures à la date max pour avoir les clauses modifiées le jour même
            search.setDateModificationMax(new Date(search.getDateModificationMax().getTime() + 24 * 60 * 60 * 1000));
        }
        if (search.getDateModificationMin() == null && search.getDateModificationMax() != null) {
            search.setDateModificationMin(new Date(0));
            search.setDateModificationMax(search.getDateModificationMax());
        }
        if (search.getDateModificationMin() != null && search.getDateModificationMax() == null) {
            search.setDateModificationMin(search.getDateModificationMin());
            search.setDateModificationMax(new Date());
        }
        if (search.isEditeur())
            return clauseFacade.findClauses(search, epmTUtilisateur.getEpmTRefOrganisme());
        else
            return clauseViewFacade.findClauses(search, epmTUtilisateur.getEpmTRefOrganisme());
    }

    @GetMapping(value = "/{idClause}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean findClause(@PathVariable("idClause") int idClause,
                                 @RequestParam("editeur") boolean editeur,
                                 @RequestParam(required = false) Integer idPublication,
                                 @RequestParam("duplication") boolean duplication,
                                 @RequestParam("surcharge") boolean surcharge,
                                 @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Récupération de la clause {} pour l'organisme {}", idClause, epmTUtilisateur.getIdOrganisme());
        return clauseLegacyService.findClause(idClause, idPublication, editeur, duplication, surcharge, epmTUtilisateur);
    }

    @GetMapping(value = "/{idClause}/surcharge", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean findClauseSurcharge(@PathVariable("idClause") int idClause,
                                          @RequestParam Integer idPublication,
                                          @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Réupération de la clause surchargée {} pour l'organisme {}", idClause, epmTUtilisateur.getIdOrganisme());
        return clauseLegacyService.findClauseSurcharge(idClause, idPublication, epmTUtilisateur);
    }

    @GetMapping(value = "/{idClause}/preference-direction", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean findClausePreferenceDirection(@PathVariable("idClause") int idClause,
                                                    @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                                    @RequestParam(value = "rolesActifs", required = false, defaultValue = "false") boolean rolesActifs,
                                                    @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Récupération parametrage direction de la clause id {} pour la direction {}", idClause, epmTUtilisateur.getDirectionService());
        return clauseLegacyService.findClausePreferenceDirection(idClause, idPublication, epmTUtilisateur, rolesActifs);
    }

    @GetMapping(value = "/{idClause}/preference-agent", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean findClausePreferenceAgent(@PathVariable("idClause") int idClause,
                                                @RequestParam(required = false) Integer idPublication,
                                                @RequestParam(value = "rolesActifs", required = false, defaultValue = "false") boolean rolesActifs,
                                                @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Récupération de la clause agent {} pour l'organisme {}", idClause, epmTUtilisateur.getIdOrganisme());
        return clauseLegacyService.findClausePreferenceAgent(idClause, idPublication, epmTUtilisateur, rolesActifs);
    }


    @PatchMapping(value = "/{idClause}/preference-direction", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean modifyClausePreferenceDirection(@PathVariable("idClause") int idClause,
                                                      @RequestBody ClauseBean clauseBean,
                                                      @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                                      @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Récupération parametrage direction defaut clause id {}", idClause);
        return clauseLegacyService.modifyClausePreferenceDirection(idClause, idPublication, epmTUtilisateur, clauseBean);
    }

    @PatchMapping(value = "/{idClause}/preference-agent", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean modifyClausePreferenceAgent(@PathVariable("idClause") int idClause,
                                                  @RequestBody ClauseBean clauseBean,
                                                  @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                                  @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Modification parametrage agent defaut clause id {}", idClause);
        return clauseLegacyService.modifyClausePreferenceAgent(idClause, idPublication, epmTUtilisateur, clauseBean);
    }

    @GetMapping(value = "/export")
    public String exportClauses(@RequestParam("editeur") boolean editeur, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Export des clauses de l'organisme {} / editeur {}", epmTUtilisateur.getIdOrganisme(), editeur);
        RsemTask<FileTmp> task = redactionTaskFactory.newExportClausesTask(epmTUtilisateur, editeur, "export_clauses.xls");
        String idTask = rsemTaskWorker.executeTask(task);
        log.info("Tache {} est lancée", idTask);
        return idTask;
    }

    @GetMapping(value = "/{idClause}/preview", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseDocument previewClauseDocument(@PathVariable("idClause") int idClause,
                                                @RequestParam("editeur") boolean editeur,
                                                @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                                @RequestParam(required = false) Boolean surcharge,
                                                @RequestParam(required = false) Boolean agent,
                                                @RequestParam(required = false) Boolean direction,
                                                @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("preview de la clause {}", idClause);
        return clauseLegacyService.previewClauseDocument(idClause, editeur, idPublication, surcharge, agent, direction, epmTUtilisateur, messageSource);

    }


    @PostMapping(value = "/preview", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseDocument previewClause(@RequestBody ClauseBean clause,
                                        @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("preview de la clause {}", clause.getId());
        return clauseLegacyService.getClauseDocument(clause, epmTUtilisateur);

    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean addClause(@RequestParam("editeur") boolean editeur, @RequestBody ClauseBean clause, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("ajout de la clause {} / editeur {}", clause.getReferenceClause(), editeur);
        EpmTClause epmTClauseInstance = clauseLegacyService.createEpmTClause(editeur, clause, epmTUtilisateur);
        return clauseMapper.toClauseBean(epmTClauseInstance);
    }

    @PutMapping(value = "/{idClause}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean modifyClause(@PathVariable("idClause") int idClause, @RequestBody ClauseBean clause, @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("modification de la clause {}", idClause);
        return clauseLegacyService.modifyClause(clause, epmTUtilisateur);
    }

    @PutMapping(value = "/{idClause}/clone", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean clone(@PathVariable("idClause") int idClause,
                            @RequestParam("editeur") boolean editeur,
                            @RequestBody ClauseBean clause,
                            @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("clone de la clause {}", idClause);
        return clauseLegacyService.cloneClause(editeur, clause, epmTUtilisateur);
    }

    @PutMapping(value = "/{idClause}/surcharge", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean surcharge(@PathVariable("idClause") int idClause,
                                @RequestBody ClauseBean clause,
                                @RequestParam(value = "idPublication", required = false) Integer idPublication,
                                @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("surcharge de la clause {}", idClause);
        return clauseLegacyService.surcharger(idClause, idPublication, clause, epmTUtilisateur);
    }


    @PatchMapping(value = "/{idClause}/statut", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ClauseBean changeStatusClause(@PathVariable("idClause") final int idClause,
                                         @RequestBody @NotNull final PatchStatut patchStatut) {
        if (patchStatut == null) {
            throw new RuntimeException(messageError().getTitle());
        }
        int idStatus = patchStatut.getIdStatus();
        if (idStatus != 1 && idStatus != 2 && idStatus != 3) {
            throw new RuntimeException(messageError().getTitle());
        }
        return clauseLegacyService.updateStatutClause(idClause, idStatus);

    }

    @DeleteMapping(value = "/{idClause}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public final ClauseBean removeClause(@PathVariable("idClause") final int idClause,
                                         @SessionAttribute("utilisateur") EpmTUtilisateur epmTUtilisateur) {
        log.info("Suppression de la clause {}", idClause);
        EpmTRefOrganisme epmTRefOrganisme = epmTUtilisateur.getEpmTRefOrganisme();
        ClauseBean clauseBean = clauseFacade.findById(idClause, epmTRefOrganisme);
        clauseBean.setEtat(2); // '2' vaut l'état 'Supprimé'
        return clauseFacade.save(clauseBean, epmTRefOrganisme);

    }

    @GetMapping(value = "/{idClause}/historique-version", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PublicationClausierBean> getHistoriqueVersion(@PathVariable("idClause") final int idClause) {
        List<EpmTPublicationClausier> publicationsClausier = clausierFacade.recupererPublicationsParClause(idClause);
        if (publicationsClausier == null || publicationsClausier.isEmpty()) {
            return List.of();
        }
        return publicationsClausier.stream().sorted((o1, o2) -> {
            Date valeuro1 = o1.getDatePublication();
            Date valeuro2 = o2.getDatePublication();
            return valeuro1.compareTo(valeuro2);
        }).map(publicationClausierMapper::fromPublicationBean).collect(Collectors.toList());
    }


}
