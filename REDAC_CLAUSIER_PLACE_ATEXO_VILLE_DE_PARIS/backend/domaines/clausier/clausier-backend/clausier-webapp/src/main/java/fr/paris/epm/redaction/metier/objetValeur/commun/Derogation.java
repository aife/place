package fr.paris.epm.redaction.metier.objetValeur.commun;

import java.io.Serializable;

/**
 * Classe Derogation representant une dérogation au  CCAG
 * @author MGA
 */
public class Derogation implements Serializable{

    /**
     * ID de serialisation
     */
    private static final long serialVersionUID = -4832383224318444144L;
    
    /**
     * L'article de dérogation
     */
    private String articleDerogationCCAG;
    /**
     * Les commentaires de dérogation
     */
    private String commentairesDerogationCCAG;
    /**
     * Le numéro de l'article auquel la dérogation est liée (sert à la génération du tableau de dérogations)
     */
    private String numArticle;

    private boolean active;

    /**
     * @return true si les champs articleDerogationCCAG commentairesDerogationCCAG et numArticle sont tous les trois null ou vide.
     */
    public boolean isEmpty() {
        return (articleDerogationCCAG == null || articleDerogationCCAG.isEmpty()) &&
                (commentairesDerogationCCAG == null || commentairesDerogationCCAG.isEmpty()) &&
                (numArticle == null || numArticle.isEmpty());
    }

    /**
     * @return the articleDerogationCCAG
     */
    public final String getArticleDerogationCCAG() {
        return articleDerogationCCAG;
    }
    /**
     * @param valeur the articleDerogationCCAG to set
     */
    public final void setArticleDerogationCCAG(final String valeur) {
        this.articleDerogationCCAG = valeur;
    }
    /**
     * @return the commentairesDerogationCCAG
     */
    public final String getCommentairesDerogationCCAG() {
        return commentairesDerogationCCAG;
    }
    /**
     * @param valeur the commentairesDerogationCCAG to set
     */
    public final void setCommentairesDerogationCCAG(final String valeur) {
        this.commentairesDerogationCCAG = valeur;
    }
    /**
     * @return the numArticle
     */
    public final String getNumArticle() {
        return numArticle;
    }
    /**
     * @param valeur the numArticle to set
     */
    public final void setNumArticle(final String valeur) {
        this.numArticle = valeur;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
