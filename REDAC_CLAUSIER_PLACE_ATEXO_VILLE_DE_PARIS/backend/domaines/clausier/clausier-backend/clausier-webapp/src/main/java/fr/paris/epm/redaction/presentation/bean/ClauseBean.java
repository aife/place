package fr.paris.epm.redaction.presentation.bean;

import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.presentation.forms.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Bean Clause
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class ClauseBean extends BaseBean {

    /**
     * l'identifiant de la clause.
     */
    private int idClause;

    /**
     * null si n'est pas publié
     */
    private Integer idPublication;

    private Integer idLastPublication;

    private Integer idOrganisme;

    /**
     * la réference de la clause.
     */
    private String referenceClause;

    /**
     * la réference du canevas (utilisé que pour la recherche).
     */
    private String referenceCanevas;

    private int idTypeClause;

    private String labelTypeClause;

    private int idTypeDocument;

    private List<Integer> idsTypeContrats;

    /**
     * (utilisé que pour la recherche).
     */
    private Integer idTypeContrat;

    private Integer idProcedure;

    private Directory procedure;

    private  List<Directory> typesContrat;

    private int idThemeClause;

    private String labelThemeClause;


    private List<Integer> idsRolesClauses;

    private List<Integer> idsClausePotentiellementConditionnees;


    private String motsCles;

    private String infoBulleText;

    private String infoBulleUrl;

    private String context;
    private String contextHtml;

    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateCreation;

    /**
     * utilisée que pour la recherche
     */
    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMin;

    /**
     * utilisée que pour la recherche
     */
    @DateTimeFormat(pattern = "dd/MM/yyyy"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date dateModificationMax;

    /**
     * libellé (version) de la dernière publication pour les clauses de type EpmTCanevas,
     * libellé (version) de la publication pour les clauses EpmTCanevasPub -> donc EpmVCanevas
     */
    private String lastVersion;

    private boolean clauseEditeur;
    private Boolean parametreActif;

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    private boolean compatibleEntiteAdjudicatrice;

    private Date dateDerniereValidation;

    public Date getDatePremiereValidation() {
        return datePremiereValidation;
    }

    public void setDatePremiereValidation(Date datePremiereValidation) {
        this.datePremiereValidation = datePremiereValidation;
    }

    private Date datePremiereValidation;

    private ClauseListeChoixExclusifForm clauseListeChoixExclusif;
    private ClauseListeChoixCumulatifForm clauseListeChoixCumulatif;
    private ClauseValeurHeriteeForm clauseValeurHeritee;
    private ClauseTexteFixeForm clauseTexteFixe;

    private ClauseTexteLibreForm clauseTexteLibre;

    private ClauseTextePrevaloriseForm clauseTextePrevalorise;


    private InfoBulle infoBulle;
    private Set<PotentiellementConditionnee> potentiellementConditionnees;


    public ClauseListeChoixExclusifForm getClauseListeChoixExclusif() {
        return clauseListeChoixExclusif;
    }

    public void setClauseListeChoixExclusif(ClauseListeChoixExclusifForm clauseListeChoixExclusif) {
        this.clauseListeChoixExclusif = clauseListeChoixExclusif;
    }

    public ClauseListeChoixCumulatifForm getClauseListeChoixCumulatif() {
        return clauseListeChoixCumulatif;
    }

    public void setClauseListeChoixCumulatif(ClauseListeChoixCumulatifForm clauseListeChoixCumulatif) {
        this.clauseListeChoixCumulatif = clauseListeChoixCumulatif;
    }

    public ClauseValeurHeriteeForm getClauseValeurHeritee() {
        return clauseValeurHeritee;
    }

    public void setClauseValeurHeritee(ClauseValeurHeriteeForm clauseValeurHeritee) {
        this.clauseValeurHeritee = clauseValeurHeritee;
    }

    public ClauseTexteFixeForm getClauseTexteFixe() {
        return clauseTexteFixe;
    }

    public void setClauseTexteFixe(ClauseTexteFixeForm clauseTexteFixe) {
        this.clauseTexteFixe = clauseTexteFixe;
    }

    public ClauseTexteLibreForm getClauseTexteLibre() {
        return clauseTexteLibre;
    }

    public void setClauseTexteLibre(ClauseTexteLibreForm clauseTexteLibre) {
        this.clauseTexteLibre = clauseTexteLibre;
    }

    public ClauseTextePrevaloriseForm getClauseTextePrevalorise() {
        return clauseTextePrevalorise;
    }

    public void setClauseTextePrevalorise(ClauseTextePrevaloriseForm clauseTextePrevalorise) {
        this.clauseTextePrevalorise = clauseTextePrevalorise;
    }

    public InfoBulle getInfoBulle() {
        return infoBulle;
    }

    public void setInfoBulle(InfoBulle infoBulle) {
        this.infoBulle = infoBulle;
    }

    public Set<PotentiellementConditionnee> getPotentiellementConditionnees() {
        return potentiellementConditionnees;
    }

    public void setPotentiellementConditionnees(Set<PotentiellementConditionnee> potentiellementConditionnees) {
        this.potentiellementConditionnees = potentiellementConditionnees;
    }

    /**
     * utilisé que pour l'affichage
     */
    private String labelTypeAuteur;

    public int getIdClause() {
        return idClause;
    }

    public void setIdClause(int idClause) {
        this.idClause = idClause;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public Integer getIdLastPublication() {
        return idLastPublication;
    }

    public void setIdLastPublication(Integer idLastPublication) {
        this.idLastPublication = idLastPublication;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public String getReferenceClause() {
        return referenceClause;
    }

    public void setReferenceClause(String referenceClause) {
        this.referenceClause = referenceClause;
    }

    public String getReferenceCanevas() {
        return referenceCanevas;
    }

    public void setReferenceCanevas(String referenceCanevas) {
        this.referenceCanevas = referenceCanevas;
    }

    public int getIdTypeClause() {
        return idTypeClause;
    }

    public void setIdTypeClause(int idTypeClause) {
        this.idTypeClause = idTypeClause;
    }

    public int getIdTypeDocument() {
        return idTypeDocument;
    }

    public void setIdTypeDocument(int idTypeDocument) {
        this.idTypeDocument = idTypeDocument;
    }

    public List<Integer> getIdsTypeContrats() {
        return idsTypeContrats;
    }

    public void setIdsTypeContrats(List<Integer> idsTypeContrats) {
        this.idsTypeContrats = idsTypeContrats;
    }

    public Integer getIdTypeContrat() {
        return idTypeContrat;
    }

    public void setIdTypeContrat(Integer idTypeContrat) {
        this.idTypeContrat = idTypeContrat;
    }

    public Integer getIdProcedure() {
        return idProcedure;
    }

    public void setIdProcedure(Integer idProcedure) {
        this.idProcedure = idProcedure;
    }

    public int getIdThemeClause() {
        return idThemeClause;
    }

    public void setIdThemeClause(int idThemeClause) {
        this.idThemeClause = idThemeClause;
    }

    public String getLabelThemeClause() {
        return labelThemeClause;
    }

    public void setLabelThemeClause(String labelThemeClause) {
        this.labelThemeClause = labelThemeClause;
    }

    public List<Integer> getIdsRolesClauses() {
        return idsRolesClauses;
    }

    public void setIdsRolesClauses(List<Integer> idsRolesClauses) {
        this.idsRolesClauses = idsRolesClauses;
    }

    public List<Integer> getIdsClausePotentiellementConditionnees() {
        return idsClausePotentiellementConditionnees;
    }

    public void setIdsClausePotentiellementConditionnees(List<Integer> idsClausePotentiellementConditionnees) {
        this.idsClausePotentiellementConditionnees = idsClausePotentiellementConditionnees;
    }

    public String getMotsCles() {
        return motsCles;
    }

    public void setMotsCles(String motsCles) {
        this.motsCles = motsCles;
    }

    public String getInfoBulleText() {
        return infoBulleText;
    }

    public void setInfoBulleText(String infoBulleText) {
        this.infoBulleText = infoBulleText;
    }

    public String getInfoBulleUrl() {
        return infoBulleUrl;
    }

    public void setInfoBulleUrl(String infoBulleUrl) {
        this.infoBulleUrl = infoBulleUrl;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getContextHtml() {
        return contextHtml;
    }

    public void setContextHtml(String contextHtml) {
        this.contextHtml = contextHtml;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModificationMin() {
        return dateModificationMin;
    }

    public void setDateModificationMin(Date dateModificationMin) {
        this.dateModificationMin = dateModificationMin;
    }

    public Date getDateModificationMax() {
        return dateModificationMax;
    }

    public void setDateModificationMax(Date dateModificationMax) {
        this.dateModificationMax = dateModificationMax;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public boolean isClauseEditeur() {
        return clauseEditeur;
    }

    public void setClauseEditeur(boolean clauseEditeur) {
        this.clauseEditeur = clauseEditeur;
    }

    public String getLabelTypeAuteur() {
        return labelTypeAuteur;
    }

    public void setLabelTypeAuteur(String labelTypeAuteur) {
        this.labelTypeAuteur = labelTypeAuteur;
    }

    public String getLabelTypeClause() {
        return labelTypeClause;
    }

    public void setLabelTypeClause(String labelTypeClause) {
        this.labelTypeClause = labelTypeClause;
    }

    public Date getDateDerniereValidation() {
        return dateDerniereValidation;
    }

    public void setDateDerniereValidation(Date dateDerniereValidation) {
        this.dateDerniereValidation = dateDerniereValidation;
    }

    public Boolean getParametreActif() {
        return parametreActif;
    }

    public void setParametreActif(Boolean parametreActif) {
        this.parametreActif = parametreActif;
    }

    public Directory getProcedure() {
        return procedure;
    }

    public void setProcedure(Directory procedure) {
        this.procedure = procedure;
    }

    public List<Directory> getTypesContrat() {
        return typesContrat;
    }

    public void setTypesContrat(List<Directory> typesContrat) {
        this.typesContrat = typesContrat;
    }


}
