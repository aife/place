package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.metier.redaction.*;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.bean.forms.BlocRecapClauseFormBean;
import fr.paris.epm.redaction.coordination.service.CanevasService;
import fr.paris.epm.redaction.metier.objetValeur.ClauseExport;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionnee;
import fr.paris.epm.redaction.metier.objetValeur.PotentiellementConditionneeValeur;
import fr.paris.epm.redaction.metier.objetValeur.RoleClause;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.presentation.mapper.ClauseMapper;
import fr.paris.epm.redaction.presentation.mapper.RoleClauseMapper;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe implementant les méthodes exposées par l'interface ClauseFacade.
 *
 * @author RAMLI Tarik & Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */

public class ClauseFacadeGWTImpl implements ClauseFacadeGWT {

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ClauseFacadeGWTImpl.class);

    /**
     * La taille maximale du contenu d'une clause.
     */
    public static final int TAILLE_MAX_CONTENU_CLAUSE = 60;

    /**
     * La taille maximale d'un mot dans la clause.
     */
    public static final int TAILLE_MAX_MOT_CLAUSE = 40;

    /**
     * La taille logique d'un mot.
     */
    public static final int TAILLE_LOGIC_MOT = 15;

    private NoyauProxy noyauProxy;

    /**
     * Canevas service
     */
    private CanevasService canevasService;

    /**
     * attribut referentielFacade.
     */
    private ReferentielFacade referentielFacade;

    /**
     * Acces aux fichiers de libelle (injection Spring).
     */
    private ResourceBundleMessageSource messageSource;

    private RedactionServiceSecurise redactionService;

    private ClauseMapper clauseMapper;

    private RoleClauseMapper roleClauseMapper;

    /**
     * (non-Javadoc)
     */
    public final EpmTClause ajouter(final EpmTClause epmTClauseInstance, String reference) {

        EpmTRefParametrage parametrageNumerotationClauseEditeur =
                (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_NUMEROTATION_CLAUSE_EDITEUR, TypeEpmTRefObject.PARAMETRAGE);

        EpmTRefParametrage parametrageNumerotationClauseClients =
                (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_NUMEROTATION_CLAUSE_CLIENTS, TypeEpmTRefObject.PARAMETRAGE);

        LOG.debug("Création d'une clause");
        if (reference == null) {
            //String reference = getReferenceClauseSuivante(epmTClauseInstance.isClauseEditeur(), "000001", "100001"); /* TEST */
            reference = getReferenceClauseSuivante(epmTClauseInstance.isClauseEditeur(), epmTClauseInstance.getIdOrganisme(), parametrageNumerotationClauseEditeur.getValeur(), parametrageNumerotationClauseClients.getValeur());
        }

        epmTClauseInstance.setReference(reference);
        epmTClauseInstance.setDateCreation(Calendar.getInstance().getTime());
        epmTClauseInstance.setDateModification(Calendar.getInstance().getTime());
        epmTClauseInstance.setEtat(EpmTClause.ETAT_DISPONIBLE);

        Set<EpmTClausePotentiellementConditionnee> clausePotCondListCloneSource = epmTClauseInstance.getEpmTClausePotentiellementConditionnees();
        if (epmTClauseInstance.getEpmTClausePotentiellementConditionnees() != null) {
            Set<EpmTClausePotentiellementConditionnee> clausePotCondListCloneCible = new HashSet<EpmTClausePotentiellementConditionnee>(clausePotCondListCloneSource);
            epmTClauseInstance.setEpmTClausePotentiellementConditionnees(clausePotCondListCloneCible);
            for (EpmTClausePotentiellementConditionnee cpc : epmTClauseInstance.getEpmTClausePotentiellementConditionnees()) {
                cpc.setEpmTClause(epmTClauseInstance);
                for (EpmTClauseValeurPotentiellementConditionnee cvpv : cpc.getEpmTClauseValeurPotentiellementConditionnees()) {
                    cvpv.setEpmTClausePotentiellementConditionnee(cpc);
                }
            }
        }


        return redactionService.modifierEpmTObject(epmTClauseInstance);
    }

    /**
     * Génére une référence pour la prochaine clause à créer.
     *
     * @return la référence de la prochaine clause à créer Exemple : CLA000060
     */
    private String getReferenceClauseSuivante(final boolean clauseEditeur, final Integer idOrganisme, final String numerotationClauseEditeur, final String numerotationClauseClients) {
        LOG.debug("Constuire une référence pour la nouvelle clause");
        try {

            String prefixRef = "CLA";
            String suffixRef = clauseEditeur ? numerotationClauseEditeur : numerotationClauseClients;
            String refSuiv;
            List<String> distinctRefs = redactionService.chercherDerniereReferenceClause(idOrganisme, clauseEditeur);
            if (!CollectionUtils.isEmpty(distinctRefs)) {
                int valSuffixRefSuiv = distinctRefs.stream()
                        .map(s -> s.replace(prefixRef, ""))
                        .filter(NumberUtils::isDigits)
                        .mapToInt(Integer::parseInt)
                        .max()
                        .orElse(0)
                        + 1;
                refSuiv = prefixRef.concat(Util.strPadding(String.valueOf(valSuffixRefSuiv), suffixRef.length()));
            } else {
                refSuiv = prefixRef.concat(suffixRef);
            }
            LOG.debug("Référence construite avec succès : " + refSuiv);
            return refSuiv;
        } catch (RuntimeException re) {
            LOG.error("Echec de construction de la référence de la clause", re);
            throw new TechnicalException(re);
        }
    }

    /**
     * @param valeur        : la valeur de la clause
     * @param isPrefAgent   : true si cas d'une préférence agent
     * @param idUtilisateur : l'identifiant de la preference agent associé à la clause
     * @param direction
     * @param clause        la clause à modifier
     */
    public final <T extends EpmTRoleClauseAbstract> T modifierPreferences(final T valeur, final boolean isPrefAgent, Integer idUtilisateur, Integer direction, EpmTClauseAbstract clause) {
        try {
            EpmTPreference preference = new EpmTPreference();

            if (valeur.getPreference() != null && valeur.getPreference().getId() != 0)
                preference = valeur.getPreference();

            if (isPrefAgent)
                preference.setIdUtilisateur(idUtilisateur);
            else
                preference.setIdDirectionService(direction);
            preference.setIdClause(clause.getIdClause());

            valeur.setEpmTClause(null);
            valeur.setPreference(preference);
            redactionService.modifierEpmTObject(valeur); //ajoute une ligne dans t_role_clause et une ligne dans preference au premier parametrage activé
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public final <T extends EpmTRoleClauseAbstract> List<T> modifierPreferences(List<T> rolesClause, final boolean isPrefAgent, Integer idUtilisateur, Integer idDirection, EpmTClauseAbstract clause) {
        List<T> resultats = new ArrayList<>();
        for (T roleClause : rolesClause)
            resultats.add(modifierPreferences(roleClause, isPrefAgent, idUtilisateur, idDirection, clause));
        return resultats;
    }

    public void modifierRoleClause(EpmTRoleClause roleClause) {
        redactionService.modifierEpmTObject(roleClause);
    }

    /**
     * Permet de charger la dernière clause ayant la référence passé en
     * paramètre.
     * La recherche de clause se fait sur la table epm__t_clause
     *
     * @param reference   La référence de la clause
     * @param idOrganisme l'identifiant de l'organisme
     * @return EpmTClause la clause demandée
     */
    public final EpmTClauseAbstract chargerDernierClauseByRef(final Boolean isFormeEditeur, final Integer idPublication,
                                                              final String reference, final Integer idOrganisme) {

        return chargerDernierClauseByRef(isFormeEditeur, idPublication, reference, idOrganisme, true);
    }

    public final EpmTClauseAbstract chargerDernierClauseByRef(final Boolean isFormeEditeur, final Integer idPublication,
                                                              final String reference, final Integer idOrganisme, boolean bricolage) {

        return redactionService.chercherClauseMemeReferenceGrandIdentifiant(idOrganisme, reference, idPublication, isFormeEditeur, bricolage);
    }

    /**
     * Permet de charger un objet de la table epm__t_clause à partir de l'identifiant.
     *
     * @param idClause l'identifiant de l'objet à charger
     * @return Object L'objet chargé de la base
     */
    public final EpmTClause charger(final int idClause) {
        try {
            return redactionService.chercherObject(idClause, EpmTClause.class);
        } catch (NonTrouveException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        }
    }

    /**
     * Permet de charger un objet de la table epm__t_clause_pub à partir de l'identifiant.
     *
     * @param idClause      l'identifiant de l'objet à charger
     * @param idPublication l'identifiant de la publication
     * @return Object L'objet chargé de la base
     */
    public final EpmTClausePub charger(final int idClause, final int idPublication, String plateformeUuid) {
        try {
            ClausePubCritere clausePubCritere = new ClausePubCritere(plateformeUuid);
            clausePubCritere.setIdClause(idClause);
            clausePubCritere.setIdPublication(idPublication);
            return redactionService.chercherUniqueEpmTObject(0, clausePubCritere);
        } catch (NonTrouveException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        }
    }

    /**
     * Dans le cas d'un écran éditeur on recherche la clause dans la table
     * epmtclause pour récupérer la version la plus à jour, dans le cas d'un
     * écran client, si la clause est une clause éditeur on récupére la dernière
     * version publiée de la clause (table d'historique) si non on recherche la
     * dernière version en cours de la clause.
     *
     * @param idClause     identifiant de la clause
     * @param ecranEditeur booleean indiquant si l'on est dans le cas d'un écran
     *                     administration éditeur (true) ou d'un écran client (false)
     * @return la clause recherchée.
     */
    public final EpmTClauseAbstract charger(final int idClause, final boolean ecranEditeur, final boolean clauseEditeur, String plateformeUuid) {
        if (!ecranEditeur && clauseEditeur) {
            // clause d'éditeur sur la forme de non-éditeur => EpmTClausePub
            ClauseViewCritere critereClause = new ClauseViewCritere(plateformeUuid); // utilise ClauseViewCritere pour avoir EpmTClausePub de la publication active
            critereClause.setIdClausePublication(idClause);
            List<EpmTClauseAbstract> resultatRecherche = redactionService.chercherEpmTObject(0, critereClause);
            return CollectionUtils.isEmpty(resultatRecherche) ? null : resultatRecherche.get(0);
        }
        return charger(idClause);
    }

    /**
     * (non-Javadoc)
     */
    public final EpmTClause modifier(EpmTClause objet, BlocRecapClauseFormBean recapClause) {
        EpmTClause clauseModifie = modifier(objet);
        modifierBlocRecapitulatif(clauseModifie, recapClause);
        return clauseModifie;
    }

    public final EpmTClause modifier(EpmTClause epmTClauseInstance) {
        LOG.debug("Modification d'une clause");
        /* TODO: NIKO REVOIR
        epmTClauseInstance.setDernierePublicationClausier(null);
        */
        /*if (epmTClauseInstance.getId() == 0)
            throw new TechnicalException("L'objet à modifier n'a pas d'identifiant");
        if (epmTClauseInstance.getEpmTClausePotentiellementConditionnees() != null) {
            for (EpmTClausePotentiellementConditionnee cpc : epmTClauseInstance.getEpmTClausePotentiellementConditionnees()) {
                cpc.setEpmTClause(epmTClauseInstance);
                for (EpmTClauseValeurPotentiellementConditionnee cvpv : cpc.getEpmTClauseValeurPotentiellementConditionnees()) {
                    cvpv.setEpmTClausePotentiellementConditionnee(cpc);
                }
            }
        }*/


        try {
            return redactionService.modifierEpmTObject(epmTClauseInstance);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    @Override
    public final Long chercherParCriteres(final ClauseCritere critere, List<? extends EpmTClauseAbstract> epmTClauses) {
        List result = redactionService.chercherEpmTObject(0, critere);
        Long l = (Long) result.remove(0);
        epmTClauses.addAll(result);
        return l;
    }

    @Override
    public final List<EpmTClauseAbstract> chercherParCriteres(final ClauseCritere critere) {
        return redactionService.chercherEpmTObject(0, critere);
    }

    @Override
    public final List<EpmTClauseAbstract> chercherParCriteres(final ClauseViewCritere critere) {
        return redactionService.chercherEpmTObject(0, critere);
    }

    @Override
    public Clause epmTClauseToClause(EpmTClauseAbstract epmTClause, Boolean isRechercheEditeur, String plateformeUuid) {

        Clause clause = clauseMapper.toClause(epmTClause);
        if (epmTClause.getEpmTRefProcedure() != null) {
            clause.setIdProcedure(epmTClause.getEpmTRefProcedure().getId());
        }
        clause.setIdClause(epmTClause.getIdClause());
        clause.setIdPublication(epmTClause.getIdPublication());
        clause.setActif(epmTClause.isActif());
        List<Integer> idsContrat = new ArrayList<>();
        epmTClause.getEpmTRefTypeContrats().forEach(typeContrat -> idsContrat.add(typeContrat.getId()));
        clause.setIdsTypeContrats(idsContrat);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        clause.setDateCreation(sdf.format(epmTClause.getDateCreation()));
        clause.setDateModification(sdf.format(epmTClause.getDateModification()));

        if (epmTClause.getDatePremiereValidation() != null)
            clause.setDateCreation(sdf.format(epmTClause.getDatePremiereValidation()));
        if (epmTClause.getDateDerniereValidation() != null)
            clause.setDateModification(sdf.format(epmTClause.getDateDerniereValidation()));

        String auteur = getAuteurClause(epmTClause, isRechercheEditeur, plateformeUuid);
        clause.setAuteur(auteur);

        String contenu = getContenuClause(epmTClause, true);
        clause.setContenu(contenu);

        InfoBulle infoBulle = new InfoBulle();
        if ((epmTClause.getInfoBulleText() == null || epmTClause.getInfoBulleText().isEmpty()) &&
                (epmTClause.getInfoBulleUrl() == null || epmTClause.getInfoBulleUrl().isEmpty())) {
            infoBulle.setActif(false);
        } else {
            infoBulle.setDescription(epmTClause.getInfoBulleText());
            infoBulle.setLien(epmTClause.getInfoBulleUrl());
            infoBulle.setActif(true);
        }
        clause.setInfoBulle(infoBulle);

        if (epmTClause.getIdLastPublication() != null) {
            String lastVesion = redactionService.chercherLastVersion(epmTClause.getIdLastPublication());
            clause.setLastVersion(lastVesion);
        }

        return clause;
    }


    @Override
    public List<Clause> epmTClausesToClauses(List<? extends EpmTClauseAbstract> epmTClauses, Boolean isRechercheEditeur, String plateformeUuid) {

        List<Clause> listeResult = new ArrayList<>();
        for (EpmTClauseAbstract epmTClause : epmTClauses)
            listeResult.add(epmTClauseToClause(epmTClause, isRechercheEditeur, plateformeUuid));
        return listeResult;
    }

    private String recupererVersion(EpmTPublicationClausier versionClausierDerniereModification,
                                    EpmTPublicationClausier premierePublicationClausier) {

        if (versionClausierDerniereModification != null) {
            return versionClausierDerniereModification.getVersion();
        } else if (premierePublicationClausier != null) {
            return premierePublicationClausier.getVersion();
        } else
            return messageSource.getMessage("modificationClause.tableauRecapitulatif.statut.nonPublie", null, Locale.FRENCH);
    }

    private String recupererDate(
            EpmTPublicationClausier publicationClausier) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        if (publicationClausier != null) {
            return sdf.format(publicationClausier.getDatePublication());
        }

        return "ND";
    }

    /**
     * Retourne la liste des clause en supprimant le HTML de l'apercu de leur
     * contenu.
     */
    @Override
    public final List<Clause> chercherParCriteresSansHTML(final ClauseCritere critere) {
        List<EpmTClauseAbstract> epmTClauses = new ArrayList<EpmTClauseAbstract>();
        Long countTotal = chercherParCriteres(critere, epmTClauses);

        List<Clause> resultat = epmTClausesToClauses(epmTClauses, !(critere instanceof ClauseViewCritere), critere.getPlateformeUuid());
        for (Clause clauseItem : resultat) {
            clauseItem.setContenu(StringEscapeUtils
                    .unescapeHtml(clauseItem.getContenu().replaceAll("<[^<>]*>", ""))
                    .replaceAll("<[^>]*...$", "...").replaceAll("&", ""));
        }
        ((List) resultat).add(0, countTotal);
        return resultat;
    }

    @Override
    public final List<Clause> chercherParCriteresSansHTML(final ClauseViewCritere critere) {
        return chercherParCriteresSansHTML((ClauseCritere) critere);
    }

    /**
     * Permet de récupérer le contenu d'une clause sous forme de : [Texte fixe
     * avant][formulations/texte prévaliorisé ou hérié][Texte fixe après].
     *
     * @param epmTClauseInstance la clause en question
     * @return le contenu sous forme de String
     * @throws TechnicalException erreur technique
     */
    @Override
    public String getContenuClause(EpmTClauseAbstract epmTClauseInstance) throws TechnicalException {
        return getContenuClause(epmTClauseInstance, false);
    }

    /**
     * Permet de récupérer le contenu d'une clause sous forme de : [Texte fixe
     * avant][formulations/texte prévaliorisé ou hérié][Texte fixe après].
     *
     * @param epmTClauseInstance la clause en question
     * @param filtrerHTMLContenu true si on doit enlever le html du contenu, tout le formatage est alors enlevé.
     * @return le contenu sous forme de String
     * @throws TechnicalException erreur technique
     */
    public final String getContenuClause(final EpmTClauseAbstract epmTClauseInstance, final boolean filtrerHTMLContenu) {
        StringBuffer contenu = new StringBuffer("");
        if (epmTClauseInstance.getTexteFixeAvant() != null)
            contenu.append(epmTClauseInstance.getTexteFixeAvant());

        List<EpmTRoleClauseAbstract> lesRoles = (List<EpmTRoleClauseAbstract>) epmTClauseInstance.getEpmTRoleClausesTrie();

        if (lesRoles != null) {
            // Dans le cas d'une clause Liste
            if (epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF
                    || epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                // Récupérer l'ensemble des formulations
                for (EpmTRoleClauseAbstract epmTRoleClause : lesRoles) {
                    // Récupérer juste les valeurs par defaut
                    if (epmTRoleClause.getIdUtilisateur() == null && epmTRoleClause.getIdDirectionService() == null) {
                        ajouterAvecSeparateur(contenu, epmTRoleClause.getValeurDefaut(), "\n");
                    }
                }
                if (epmTClauseInstance.getTexteFixeApres() != null) {
                    ajouterAvecSeparateur(contenu, epmTClauseInstance.getTexteFixeApres(), "\n");
                }
            } else {
                if (epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE) {
                    // Récupérer la valeur héritée
                    for (EpmTRoleClauseAbstract epmTRoleClause : lesRoles) {
                        // Récupérer juste la valeur par defaut
                        if (epmTRoleClause.getIdUtilisateur() == null && epmTRoleClause.getIdDirectionService() == null) {
                            try {
                                EpmTRefValeurTypeClause valeur = referentielFacade.getValeurTypeClauseById(Integer.parseInt(epmTRoleClause.getValeurDefaut()));
                                if (valeur != null)
                                    ajouterAvecSeparateur(contenu, "[ " + valeur.getLibelle() + " ]", " ");
                            } catch (NumberFormatException | TechnicalException e) {
                                LOG.error(e.getMessage(), e.fillInStackTrace());
                            }
                        }
                    }
                } else if (epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TABLEAU ||
                        epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_HERITEE) {
                    // INACTIF @see ${TYPE_CLAUSE_VALEUR_HERITEE}
                    throw new TechnicalException("Les clauses de types Texte Héritée et Tableau sont obsoletes; Utilisez le type Valeur Héritée");

                } else
                    // Dans le cas d'une clause à  texte Prévalorisé
                    if (epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
                        // Récupérer la valeur héritée
                        for (EpmTRoleClauseAbstract epmTRoleClause : lesRoles) {
                            // Récupérer juste la valeur par defaut
                            if (epmTRoleClause.getIdUtilisateur() == null && epmTRoleClause.getIdDirectionService() == null) {
                                ajouterAvecSeparateur(contenu, epmTRoleClause.getValeurDefaut(), " ");
                            }
                        }
                    } else
                        // Dans le cas d'une clause à  texte libre
                        if (epmTClauseInstance.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE) {
                            // Récupérer la valeur héritée
                            for (EpmTRoleClauseAbstract epmTRoleClause : lesRoles) {
                                // Récupérer juste la valeur par defaut
                                if (epmTRoleClause.getIdUtilisateur() == null && epmTRoleClause.getIdDirectionService() == null) {
                                    ajouterAvecSeparateur(contenu, "[ texte libre ]", " ");
                                }
                            }
                        }
                if (epmTClauseInstance.getTexteFixeApres() != null) {
                    ajouterAvecSeparateur(contenu, epmTClauseInstance.getTexteFixeApres(), " ");
                }
            }
        }

        //Filtrer tout le contenu html.
        String contenuTraite = contenu.toString();
        contenuTraite = fr.paris.epm.noyau.commun.util.Util.sanitizeHtmlInput(contenuTraite);
        if (filtrerHTMLContenu) {
            contenuTraite = contenuTraite.replaceAll("<br[^>]*>", " ");
            contenuTraite = contenuTraite.replaceAll("<ul>", " ");
            contenuTraite = contenuTraite.replaceAll("</li>", " ");
            contenuTraite = contenuTraite.replaceAll("</ul>", " ");
            contenuTraite = contenuTraite.replaceAll("<[^>]*>", "");
        }

        if (contenuTraite.length() > TAILLE_MAX_CONTENU_CLAUSE - 1) {

            StringTokenizer text = new StringTokenizer(contenuTraite, " ");
            StringBuffer contenuTronque = new StringBuffer();
            while (text.hasMoreElements()) {
                String textEncour = (String) text.nextElement();
                if (textEncour.length() > TAILLE_MAX_MOT_CLAUSE - 1) {
                    StringBuffer textTmp = new StringBuffer();
                    boolean b = false;
                    while (textEncour.length() > TAILLE_MAX_MOT_CLAUSE - 1) {
                        if (b) {
                            textTmp.append("\n");
                        }
                        textTmp.append(textEncour.substring(0, TAILLE_MAX_MOT_CLAUSE));
                        b = true;
                        textEncour = textEncour.substring(TAILLE_MAX_MOT_CLAUSE, textEncour.length());
                    }
                    textEncour = textTmp.append("\n").append(textEncour).toString();
                }
                if (textEncour.length() > TAILLE_LOGIC_MOT - 1 && contenuTronque.length() + textEncour.length() > TAILLE_MAX_CONTENU_CLAUSE - 1) {
                    if (TAILLE_MAX_CONTENU_CLAUSE - contenuTronque.length() > 0) {
                        textEncour = textEncour.substring(0, TAILLE_MAX_CONTENU_CLAUSE - contenuTronque.length());
                    }
                }

                contenuTronque.append(textEncour).append(" ");

                if (contenuTronque.toString().trim().length() > TAILLE_MAX_CONTENU_CLAUSE - 1 && contenuTronque.toString().trim().length() != contenuTraite.length()) {
                    contenuTronque.append("...");
                    return contenuTronque.toString();
                }
            }
            return contenuTronque.toString();
        } else {
            return contenuTraite;
        }
    }

    /**
     * Ajouter un texte à une chaine de caractère selon le séparateur spécifié.
     *
     * @param texte      le texte initial
     * @param newTexte   le texte à ajouter
     * @param separateur le séparateur voulu
     * @return le texte formaté
     */
    private static StringBuffer ajouterAvecSeparateur(final StringBuffer texte, final String newTexte, final String separateur) {
        return (texte.toString().trim().equals("") ? texte.append(newTexte) : texte.append(separateur).append(newTexte));
    }

    /*
     * (non-Javadoc)
     * @see fr.paris.epm.redaction.coordination.ClauseFacade#chercherParCriteres(java.lang.Object)
     */
    public final List<ClauseExport> chercherParCriteresPourExport(final ClauseCritere critere, final Integer idOrganisme) {
        List<EpmTClauseAbstract> clauses = chercherParCriteres(critere);

        EpmTRefOrganisme organisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
        int debut = 0;
        List<ClauseExport> resultat = new ArrayList<ClauseExport>(clauses.size());
        List<PotentiellementConditionnee> refCritereConditionnement = referentielFacade.chargerTousPotentiellementConditionne(idOrganisme);
        //clé de la forme id,idCritere
        Map<String, String> associationIdPotentiellementConditionneLibelleValeurCritere = new HashMap<String, String>();
        for (PotentiellementConditionnee potentiellementConditionnee : refCritereConditionnement) {
            if (null != potentiellementConditionnee.getValeurs()) {
                Iterator itCritere = potentiellementConditionnee.getValeurs().iterator();
                while (itCritere.hasNext()) {
                    PotentiellementConditionneeValeur valeurCritere = (PotentiellementConditionneeValeur) itCritere.next();
                    String id = potentiellementConditionnee.getId() + "," + valeurCritere.getId();
                    associationIdPotentiellementConditionneLibelleValeurCritere.put(id, valeurCritere.getLibelle());
                }
            }

        }
        for (EpmTClauseAbstract source : clauses) {
            ClauseExport clause = clauseMapper.toClauseExport(source);
            if (source.getIdLastPublication() != null) {
                PublicationClausierCritere publicationClausierCritere = new PublicationClausierCritere(organisme.getPlateformeUuid());
                publicationClausierCritere.setId(source.getIdLastPublication());
                EpmTPublicationClausier publicationActif = redactionService.chercherUniqueEpmTObject(0, publicationClausierCritere);
                clause.setVersion(String.valueOf(publicationActif.getVersion()));
            } else {
                clause.setVersion("NA");
            }

            String auteur = getAuteurClause(source, critere.isEditeur(), organisme.getPlateformeUuid());
            clause.setAuteur(auteur);

            clause.setContenu(getContenuClause(source, true));

            if (source.getEpmTRefProcedure() != null && source.getEpmTRefProcedure().getId() != 0) {
                EpmTRefProcedure procedure = (EpmTRefProcedure) noyauProxy.getReferentielById(source.getEpmTRefProcedure().getId(), TypeEpmTRefObject.PROCEDURE);
                clause.setLibelleProcedure(procedure.getLibelleCourt());
            } else {
                clause.setLibelleProcedure("Toutes");
            }

            if (source.getIdNaturePrestation() != 0) {
                EpmTRefNature prestation = (EpmTRefNature) noyauProxy.getReferentielById(source.getIdNaturePrestation(), TypeEpmTRefObject.NATURE);
                clause.setLibelleNaturePrestation(prestation.getLibelle());
            } else {
                clause.setLibelleNaturePrestation("Toutes");
            }

            clause.setPotentiellementConditionnee1(source.getEpmTClausePotentiellementConditionnees() != null && !source.getEpmTClausePotentiellementConditionnees().isEmpty());
            clause.setPotentiellementConditionnee2(source.getEpmTClausePotentiellementConditionnees() != null && !source.getEpmTClausePotentiellementConditionnees().isEmpty()
                    && source.getEpmTClausePotentiellementConditionnees().size() > 1);

            List<EpmTClausePotentiellementConditionneeAbstract> potentiellementConditionnee = new ArrayList<>(source.getEpmTClausePotentiellementConditionnees());

            //critere et valeur pour le premier conditionement
            if (clause.isPotentiellementConditionnee1()) {
                EpmTClausePotentiellementConditionneeAbstract potentiellementConditionnee1 = potentiellementConditionnee.get(0);
                EpmTRefPotentiellementConditionnee referentielCritere1 = potentiellementConditionnee1.getEpmTRefPotentiellementConditionnee();

                clause.setLibelleCritereConditionnement1(referentielCritere1.getLibelle());
                List<EpmTClauseValeurPotentiellementConditionneeAbstract> valeurPotentiellementConditionnees1 = new ArrayList<>(potentiellementConditionnee1.getEpmTClauseValeurPotentiellementConditionnees());
                StringBuilder stringBuilder = new StringBuilder();
                boolean first = false;
                for (EpmTClauseValeurPotentiellementConditionneeAbstract valeur : valeurPotentiellementConditionnees1) {
                    String libelleValeur = associationIdPotentiellementConditionneLibelleValeurCritere.get(referentielCritere1.getId() + "," + valeur.getValeur());
                    if (first != false) {
                        stringBuilder.append(", ");
                    }
                    stringBuilder.append(libelleValeur);
                    first = true;
                }
                clause.setLibelleValeurConditionnement1(stringBuilder.toString());
            }

            //critere et valeur pour le deuxieme conditionement
            if (clause.isPotentiellementConditionnee2()) {
                EpmTClausePotentiellementConditionneeAbstract potentiellementConditionnee2 = potentiellementConditionnee.get(1);
                EpmTRefPotentiellementConditionnee referentielCritere2 = potentiellementConditionnee2.getEpmTRefPotentiellementConditionnee();
                clause.setLibelleCritereConditionnement2(referentielCritere2.getLibelle());
                List<EpmTClauseValeurPotentiellementConditionneeAbstract> valeurPotentiellementConditionnees2 = new ArrayList<>(potentiellementConditionnee2.getEpmTClauseValeurPotentiellementConditionnees());
                StringBuilder stringBuilder = new StringBuilder();
                boolean first = false;
                for (EpmTClauseValeurPotentiellementConditionneeAbstract valeur : valeurPotentiellementConditionnees2) {
                    String libelleValeur = associationIdPotentiellementConditionneLibelleValeurCritere.get(referentielCritere2.getId() + "," + valeur.getValeur());
                    if (first != false) {
                        stringBuilder.append(", ");
                    }
                    stringBuilder.append(libelleValeur);
                    first = true;
                }
                clause.setLibelleValeurConditionnement2(stringBuilder.toString());
            }

            if (source.getEpmTRoleClausesTrie() != null) {
                List<RoleClause> listeRoleClause = new ArrayList<RoleClause>();
                for (EpmTRoleClauseAbstract epmTRoleClause : source.getEpmTRoleClausesTrie()) {
                    RoleClause roleClause = roleClauseMapper.toRoleClause(epmTRoleClause);

                    String tailleChamp = "";
                    switch (roleClause.getNombreCarateresMax()) {
                        case 1:
                            tailleChamp = messageSource.getMessage("redaction.taille.champ.long", null, Locale.FRENCH);
                            break;
                        case 2:
                            tailleChamp = messageSource.getMessage("redaction.taille.champ.moyen", null, Locale.FRENCH);
                            break;
                        case 3:
                            tailleChamp = messageSource.getMessage("redaction.taille.champ.court", null, Locale.FRENCH);
                            break;
                        default:
                            break;
                    }
                    roleClause.setLibelleTailleChamp(tailleChamp);

                    if (clause.getIdTypeClause() == 8) { //id 8 n'existe plus, ancien type tableau ?

                        int idRef = NumberUtils.toInt(roleClause.getValeurDefaut());

                        if (idRef > 0) {
                            EpmTRefValeurTypeClause valeur = referentielFacade.getValeurTypeClauseById(Integer.parseInt(epmTRoleClause.getValeurDefaut()));
                            if (valeur != null)
                                roleClause.setNomTableau(valeur.getLibelle());
                        } else {
                            LOG.warn("La référence du nom tableau est faut pour la clause : " + source.getReference());
                        }

                    } else if (clause.getIdTypeClause() == 5 || clause.getIdTypeClause() == 9) {
                        int idRef = NumberUtils.toInt(roleClause.getValeurDefaut());
                        if (idRef > 0) {
                            EpmTRefValeurTypeClause epmTRefValeur = referentielFacade.getValeurTypeClauseById(idRef);
                            if (epmTRefValeur != null)
                                roleClause.setLibelleValeurHeritee(epmTRefValeur.getLibelle());
                        }
                    }

                    listeRoleClause.add(roleClause);
                }
                clause.setListeRoleClause(listeRoleClause);
            }
            List<EpmTCanevas> canevas;

            if (null != critere.isEditeur() && critere.isEditeur()) {
                LOG.debug("Critere editeur - export interministériel");
                canevas = canevasService.chercherListeCanevasPourClauseInterministerielle(clause.getReference());
            } else {
                LOG.debug("Critere non editeur - export ministériel seulement");
                canevas = canevasService.chercherListeCanevasPourClauseMinisterielle(clause.getReference(), idOrganisme);
            }

            clause.setReferenceCanevas(canevas.stream().map(EpmTCanevas::getReference).collect(Collectors.toList()));

            // Ajout de la clause
            resultat.add(clause);
        }
        return resultat;
    }

    public EpmTClause miseAJourDataValidation(EpmTClause epmTClause) {
        if (epmTClause.getEpmTRefStatutRedactionClausier().getId() == EpmTRefStatutRedactionClausier.VALIDEE) {

            //si 1ere validation
            if (epmTClause.getDatePremiereValidation() == null) {
                epmTClause.setDatePremiereValidation(new Date());
            }

            epmTClause.setDateDerniereValidation(new Date());
        }

        return epmTClause;
    }

    /**
     * Rempli le bloc récapitulatif d'une clause
     */
    private void modifierBlocRecapitulatif(EpmTClause epmTClauseInstance, BlocRecapClauseFormBean recapClause) {
        String contenu = getContenuClause(epmTClauseInstance, true);
        if (epmTClauseInstance.getReferenceClauseSurchargee() != null) {
            if (epmTClauseInstance.getSurchargeActif()) {
                contenu = getContenuClause(epmTClauseInstance, true);
            } else {
                EpmTClauseAbstract epmTClauseEditeur = chargerDernierClauseByRef(null, null, epmTClauseInstance.getReference(), null);
                contenu = getContenuClause(epmTClauseEditeur, true);
            }
        }
        recapClause.setContenu(contenu);
    }

    /**
     * Recherche une clause de son identifiant et de sa version de publication du clausier.
     *
     * @param idClause              identifiant de la clause
     * @param idPublicationClausier identifiant de l'epmtpublicationclausier pour lequel la clause a été publiée.
     * @return la clause.
     */
    public EpmTClausePub chargerParIdPublicationClausier(final int idClause, final int idPublicationClausier, String plateformeUuid) {
        ClausePubCritere clausePubCritere = new ClausePubCritere(plateformeUuid);
        clausePubCritere.setIdClause(idClause);
        clausePubCritere.setIdPublication(idPublicationClausier);
        EpmTClausePub epmTClausePub = redactionService.chercherUniqueEpmTObject(0, clausePubCritere);
        return epmTClausePub;
    }

    /**
     * Dans le cas d'un écran éditeur on recherche la clause dans la table
     * epmtclause pour récupérer la version la plus à jour, dans le cas d'un
     * écran client, si la clause est une clause éditeur on récupére la dernière
     * version publiée de la clause (table d'historique) si non on recherche la
     * dernière version en cours de la clause. <br/>
     * Si on dupplique une clause éditeur dans un écran client et si une
     * surcharage existe on récupére également la surcharge et on crée un nouvel
     * objet epmtclause à partir de la surcharge et de clause éditeur.
     *
     * @param idClause     identifiant de la clause
     * @param ecranEditeur booleean indiquant si l'on est dans le cas d'un écran administration éditeur (true) ou d'un écran client (false)
     * @param duplication  : true si on est dans le cas d'une duplication, false sinon.
     * @param idOrganisme  l'identifiant de l'organisme (null si pas d'organisme)
     * @param surcharge    : true si on est dans le cas d'une clause surchargée
     * @return la clause recherchée.
     */
    @Override
    public EpmTClauseAbstract chargerClausePourModification(final int idClause, final boolean ecranEditeur, final boolean clauseEditeur,
                                                            final boolean duplication, final Integer idOrganisme, final boolean surcharge) {
        EpmTClauseAbstract resultat = null;
        EpmTRefOrganisme organisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
        EpmTClauseAbstract clauseInitiale = resultat = charger(idClause, ecranEditeur, clauseEditeur, organisme.getPlateformeUuid());
        if (!ecranEditeur && duplication) {
            EpmTClause surchargeClause = chercherDerniereSurchargeActif(clauseInitiale.getIdClause(), idOrganisme);
            if (surchargeClause != null && surchargeClause.getSurchargeActif() != null && surchargeClause.getSurchargeActif()) {
                try {
                    EpmTClauseAbstract cloneClauseInitiale = clauseInitiale.clone();
                    EpmTClause cloneSurchargeClause = surchargeClause.clone();
                    resultat = clauseMapper.toEpmTClauseDuplication(cloneClauseInitiale);
                    clauseMapper.toEpmTClauseSurchargeDuplication(cloneSurchargeClause, resultat);

                    ((EpmTClause) resultat).setReferenceClauseSurchargee(null);
                    ((EpmTClause) resultat).setIdClauseOrigine(null);
                    ((EpmTClause) resultat).setSurchargeActif(null);
                } catch (Exception e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    throw new TechnicalException(e);
                }
            }
        }

        if (surcharge) {
            //recherche la surcharge de la clause éditeur (null ou vide si il n'y pas de surcharge pour l'organisme)
            ClauseCritere clauseCritere = new ClauseCritere(organisme.getPlateformeUuid());
            clauseCritere.setIdOrganisme(idOrganisme);
            clauseCritere.setIdClauseOrigine(resultat.getIdClause());
            List<EpmTClauseAbstract> clauses = chercherParCriteres(clauseCritere);
            if (clauses != null && !clauses.isEmpty())
                return clauses.get(0);
        }
        return resultat;
    }

    /**
     * Recherche la surcharge d'une clause éditeur ) partir de l'identifiant de
     * la clause éditeur et de l'identifiant de l'organisme.
     *
     * @param reference   la référence de la clause éditeur surchargée
     * @param idOrganisme l'identifiant de l'organisme (null si pas d'organisme)
     * @return La surcharge si elle existe, null sinon.
     * @throws TechnicalException en cas d'erreur dans la recherche.
     */
    @Override
    public EpmTClause chercherDerniereSurchargeActif(final Integer idClause, final Integer idOrganisme) {
        EpmTRefOrganisme organisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
        ClauseCritere critere = new ClauseCritere(organisme.getPlateformeUuid());
        critere.setIdClauseOrigine(idClause);
        critere.setIdOrganisme(idOrganisme);
        critere.setEditeur(false);
        critere.setSurchargeActif(true);
        critere.setChercherNombreResultatTotal(false);
        try {
            List<EpmTClause> clauses = redactionService.chercherEpmTObject(0, critere);
            if (clauses != null && !clauses.isEmpty())
                return clauses.get(0); // TODO : NIKO 2018-02.00.02 régler le problème de nombe du surcharge
            return null;
        } catch (HibernateException ex) {
            LOG.error(ex.getMessage());
            throw new TechnicalException("Une erreur est survenue lors de la recherche de la surchage pour la clause éditeur =" +
                    idClause + " et pour l'organisme " + idOrganisme);
        }
    }

    @Override
    public List<EpmTClause> chercherClauseByRef(ClauseCritere critere) {

        try {
            List<EpmTClause> clauses = redactionService.chercherEpmTObject(0, critere);
            if (clauses != null && !clauses.isEmpty())
                return clauses;
            return List.of();
        } catch (HibernateException ex) {
            LOG.error(ex.getMessage());
            throw new TechnicalException("Une erreur est survenue lors de la recherche ");
        }
    }


    /**
     * Cette méthode retourle la liste des roleClause du paramétrage agent si il
     * y en a, sinon, retourne la liste des roles closes direction si il y en a,
     * sinon retourne la liste des roleClause par défaut de la clause.
     * Remplace la méthode getEpmTRolesOfUser de EpmtClause.
     *
     * @param utilisateur l'utilisateur connecté.
     * @param clause      la clause pour laquelle on souhaite récupérér les EpmTRoleClause
     * @return la liste des EpmTClause. Une liste vide si il n'y a pas de résultat.
     */
    @Override
    public List<? extends EpmTRoleClauseAbstract> recupererRolesClause(final EpmTUtilisateur utilisateur, final EpmTClauseAbstract clause, boolean editeur) {

        if (!editeur && "1".equals(clause.getParametrableAgent())) {
            List<EpmTRoleClauseAbstract> resultat = recupererRolesClauseAgent(utilisateur.getId(), clause, true);

            if (resultat != null && !resultat.isEmpty())
                return resultat;
        }

        if (!editeur && "1".equals(clause.getParametrableDirection())) {
            List<EpmTRoleClauseAbstract> resultat = recupererRolesClauseDirection(utilisateur.getDirectionService(), clause, true);

            if (resultat != null && !resultat.isEmpty())
                return resultat;
        }

        return new ArrayList<>(clause.getEpmTRoleClausesTrie());
    }

    /**
     * Récupére les EpmTRoleClause correspondant aux préférences Agent.
     * Remplace la méthode getEpmTRoleClausesAgent de EpmtClause.
     *
     * @param idUtilisateur identifiant de l'utilisateur pour lequel on souhaite récupérer les préférences
     * @param clause        la clause
     * @return la liste des EpmTClause.  Une liste vide si il n'y a pas de résultat.
     */
    public List<EpmTRoleClauseAbstract> recupererRolesClauseAgent(final int idUtilisateur, final EpmTClauseAbstract clause, final boolean rolesActifs) {
        return recupererRolesClauseAgentOuDirection(idUtilisateur, null, clause, rolesActifs);
    }

    /**
     * Récupére les EpmTRoleClause correspondant aux préférences directions.
     * Remplace la méthode getEpmTRoleClausesDirection de EpmtClause.
     *
     * @param idDirection identifiant de la direction pour laquelle on souhaite récupérer les préférences
     * @param clause      la clause
     * @return la liste des EpmTClause. Une liste vide si il n'y a pas de résultat.
     */
    public List<EpmTRoleClauseAbstract> recupererRolesClauseDirection(final int idDirection, final EpmTClauseAbstract clause, final boolean rolesActifs) {
        return recupererRolesClauseAgentOuDirection(null, idDirection, clause, rolesActifs);
    }

    /**
     * Retourne la liste des EpmTRoleClause des préférences agent ou direction.
     *
     * @param idUtilisateur identifiant de l'utilisateur
     * @param idDirection   identifiant de la direction
     * @param clause        la clause
     * @return la liste des EpmTClause. Une liste vide si il n'y a pas de résultat.
     */
    private List<EpmTRoleClauseAbstract> recupererRolesClauseAgentOuDirection(final Integer idUtilisateur, final Integer idDirection, final EpmTClauseAbstract clause, final boolean rolesActifs) {

        List<EpmTRoleClauseAbstract> result = null;

        if (clause.getIdPublication() != null) {
            RoleClausePubCritere roleClausePubCritere = new RoleClausePubCritere();

            roleClausePubCritere.setIdUtilisateur(idUtilisateur);
            roleClausePubCritere.setIdDirectionService(idDirection);
            roleClausePubCritere.setIdClause(clause.getIdClause());
            roleClausePubCritere.setIdPublication(clause.getIdPublication());
            result = redactionService.chercherEpmTObject(0, roleClausePubCritere);
        }

        if (result == null || result.isEmpty()) {
            RoleClauseCritere roleClauseCritere = new RoleClauseCritere();
            roleClauseCritere.setIdUtilisateur(idUtilisateur);
            roleClauseCritere.setIdDirectionService(idDirection);
            //si la clause est une surcharge récupérer l'id de la clause origine
            EpmTClause tClause = this.charger(clause.getIdClause());
            if (tClause != null && tClause.getIdClauseOrigine() != null) {
                roleClauseCritere.setIdClause(tClause.getIdClauseOrigine());
            } else {
                roleClauseCritere.setIdClause(clause.getIdClause());
            }
            result = redactionService.chercherEpmTObject(0, roleClauseCritere);
        }
        if (rolesActifs) {
            List<EpmTRoleClauseAbstract> listeRolesActifs = new ArrayList<>();
            if (result != null && !result.isEmpty()) {
                for (EpmTRoleClauseAbstract role : result) {
                    if (role.getEtat().equals(EpmTRoleClauseAbstract.ROLE_ACTIVE)) {
                        listeRolesActifs.add(role);
                    }
                }
            }
            return listeRolesActifs;
        } else {
            return result;
        }

    }

    /**
     * A partir d'un objet Clause, calculer son auteur (Editeur/Client/Editeur surchargée).
     *
     * @param clause             un objet EpmTClause en base
     * @param isRechercheEditeur indiquer si c'est le cas de recherche Clause Editeur. Si oui, il y a pas l'auteur - Editeur surchargée.
     * @param epmTRefOrganisme
     * @return auteur obtenu
     */
    private String getAuteurClause(EpmTClauseAbstract clause, Boolean isRechercheEditeur, String plateformeUuid) throws TechnicalException {

        String auteur;

        if (clause.isClauseEditeur()) { // si clause editeur

            auteur = Clause.AUTEUR_EDITEUR;

            if (isRechercheEditeur == null || !isRechercheEditeur) {
                ClauseCritere clauseCritere = new ClauseCritere(plateformeUuid);
                clauseCritere.setIdClauseOrigine(clause.getIdClause());
                clauseCritere.setIdOrganisme(clause.getIdOrganisme());
                List<EpmTClause> clauseSurchargee = redactionService.chercherEpmTObject(0, clauseCritere);
                if (clauseSurchargee != null && clauseSurchargee.stream().anyMatch(EpmTClause::getSurchargeActif))
                    auteur = Clause.AUTEUR_EDITEUR_SURCHARGE;
            }
        } else { // si clause client
            auteur = Clause.AUTEUR_CLIENT;
        }
        return auteur;
    }

    public final void setNoyauProxy(final NoyauProxy valeur) {
        this.noyauProxy = valeur;
    }

    public final void setReferentielFacade(final ReferentielFacade valeur) {
        this.referentielFacade = valeur;
    }

    public void setCanevasService(CanevasService canevasService) {
        this.canevasService = canevasService;
    }

    /**
     * @param valeur Acces aux fichiers de libelle (injection Spring).
     */
    public final void setMessageSource(final ResourceBundleMessageSource valeur) {
        this.messageSource = valeur;
    }

    public void setRedactionService(RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

    public void setClauseMapper(ClauseMapper clauseMapper) {
        this.clauseMapper = clauseMapper;
    }

    public void setRoleClauseMapper(RoleClauseMapper roleClauseMapper) {
        this.roleClauseMapper = roleClauseMapper;
    }

}
