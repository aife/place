/**
 * 
 */
package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Formulaire de refus de validation du document
 * @author MGA
 *
 */
public class RefuserValidationForm extends ActionForm{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * L'id du document
     */
    private int idDoc;
    /**
     * Commentaire de refus de validation
     */
    private String commentaire;
    
    /* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        
        return super.validate(mapping, request);
    }

    /**
     * @return the commentaire
     */
    public final String getCommentaire() {
        return commentaire;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public final void setCommentaire(final String valeur) {
        this.commentaire = valeur;
    }

    /**
     * @return the idDoc
     */
    public final int getIdDoc() {
        return idDoc;
    }

    /**
     * @param idDoc the idDoc to set
     */
    public final void setIdDoc(final int valeur) {
        this.idDoc = valeur;
    }
    
    
    
}
