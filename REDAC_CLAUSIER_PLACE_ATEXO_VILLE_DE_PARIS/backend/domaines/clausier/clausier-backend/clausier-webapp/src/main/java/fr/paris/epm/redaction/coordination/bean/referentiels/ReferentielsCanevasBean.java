package fr.paris.epm.redaction.coordination.bean.referentiels;

import fr.paris.epm.noyau.persistance.redaction.EpmTRefThemeClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeDocument;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.io.Serializable;
import java.util.List;

/**
 * Bean pour les référentiels de la rercherche de canevas.
 * @author Léon Barsamian
 */
public class ReferentielsCanevasBean implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -1863184782205359666L;

    /**
     * Liste des thémes des clauses.
     */
    private List<EpmTRefThemeClause> themeCollection;

    /**
     * Liste des types de documents.
     */
    private List<EpmTRefTypeDocument> docTypeCollection;

    /**
     * Liste des procédures de passation.
     */
    private List<EpmTRefProcedure> procedureCollection;

    /**
     * Liste des natures de prestation.
     */
    private List<EpmTRefNature> naturePrestasCollection;

    /**
     * @return Liste des thémes des clauses.
     */
    public final List<EpmTRefThemeClause> getThemeCollection() {
        return themeCollection;
    }

    /**
     * @param valeur Liste des thémes des clauses.
     */
    public final void setThemeCollection(List<EpmTRefThemeClause> valeur) {
        this.themeCollection = valeur;
    }

    /**
     * @return Liste des types de documents.
     */
    public final List<EpmTRefTypeDocument> getDocTypeCollection() {
        return docTypeCollection;
    }

    /**
     * @param valeur Liste des types de documents.
     */
    public final void setDocTypeCollection(List<EpmTRefTypeDocument> valeur) {
        this.docTypeCollection = valeur;
    }

    /**
     * @return Liste des procédures de passation.
     */
    public final List<EpmTRefProcedure> getProcedureCollection() {
        return procedureCollection;
    }

    /**
     * @param procedureCollection Liste des procédures de passation.
     */
    public final void setProcedureCollection(List<EpmTRefProcedure> procedureCollection) {
        this.procedureCollection = procedureCollection;
    }

    /**
     * @return Liste des natures de prestation.
     */
    public final List<EpmTRefNature> getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur Liste des natures de prestation.
     */
    public final void setNaturePrestasCollection(List<EpmTRefNature> valeur) {
        this.naturePrestasCollection = valeur;
    }

}
