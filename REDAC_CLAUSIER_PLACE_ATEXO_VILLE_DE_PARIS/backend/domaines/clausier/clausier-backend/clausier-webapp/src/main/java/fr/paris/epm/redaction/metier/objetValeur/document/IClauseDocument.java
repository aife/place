package fr.paris.epm.redaction.metier.objetValeur.document;

/**
 * Interface pour les bean clausedocument pour forcer la présence de la méthode isClauseVide().
 * @author lba
 *
 */
public interface IClauseDocument {
    
    /**
     * @return true si la clause est vide, c'est à dire si aucun contenu n'est saisi (texte avant et après vide, contenu de la clause vide).
     */
    boolean isClauseVide();

}
