package fr.paris.epm.redaction.presentation.bean;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class PublicationClausierSearch {


    private int page;
    private int size;
    private String sortField;
    private boolean asc;
    private String version;
    private Integer gestionLocale;
    @DateTimeFormat(pattern = "yyyy-MM-dd"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date datePublicationMin;
    @DateTimeFormat(pattern = "yyyy-MM-dd"/*iso = DateTimeFormat.ISO.DATE*/)
    private Date datePubicationMax;



    public Integer getGestionLocale() {
        return gestionLocale;
    }

    public void setGestionLocale(Integer gestionLocale) {
        this.gestionLocale = gestionLocale;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public boolean isAsc() {
        return asc;
    }

    public void setAsc(boolean asc) {
        this.asc = asc;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getDatePublicationMin() {
        return datePublicationMin;
    }

    public void setDatePublicationMin(Date datePublicationMin) {
        this.datePublicationMin = datePublicationMin;
    }

    public Date getDatePubicationMax() {
        return datePubicationMax;
    }

    public void setDatePubicationMax(Date datePubicationMax) {
        this.datePubicationMax = datePubicationMax;
    }
}
