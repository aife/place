package fr.paris.epm.redaction.presentation.mapper;

import fr.atexo.commun.document.generation.service.tableaudef.bean.AttributLargeurColonne;
import fr.atexo.commun.document.generation.service.tableaudef.bean.Cellule;
import fr.atexo.commun.document.generation.service.tableaudef.bean.RegionFusionnee;
import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {HtmlSanitizerMapper.class, DirectoryMapper.class, DirectoryNamedMapper.class, CommunMapper.class})
public interface DocumentMapper {

    AttributLargeurColonne toAttributLargeurColonne(fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.AttributLargeurColonne attributLargeurColonne);


    Cellule toCellule(fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.Cellule cellule);

    RegionFusionnee toRegionFusionnee(fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.RegionFusionnee regionFusionnee);

    Derogation toDerogation(fr.paris.epm.redaction.metier.objetValeur.commun.Derogation derogation);

}
