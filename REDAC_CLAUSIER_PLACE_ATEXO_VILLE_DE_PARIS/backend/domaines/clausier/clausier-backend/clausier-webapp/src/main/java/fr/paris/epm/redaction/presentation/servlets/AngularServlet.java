package fr.paris.epm.redaction.presentation.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;


@Controller
public class AngularServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(AngularServlet.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo != null && pathInfo.contains("WEB-INF/classes/templates/index.html")) {
            getPage(request, response);
            return;
        }
        getExtensionByStringHandling(pathInfo).ifPresentOrElse(
                value -> {
                    try {
                        if ("epm".equalsIgnoreCase(value) || "htm".equalsIgnoreCase(value)) {
                            getPage(request, response);
                        } else {
                            getRessources(response, pathInfo);
                        }
                    } catch (ServletException | IOException e) {
                        LOG.error("erreur lors de la récupération de ressources angular {}", e.getMessage());
                    }
                },
                () -> {
                    try {
                        getPage(request, response);
                    } catch (ServletException | IOException e) {
                        LOG.error("erreur lors de la récupération de la page angular {}", e.getMessage());
                    }
                }
        );


    }

    private void getRessources(HttpServletResponse response, String pathInfo) throws IOException {
        String fileName = pathInfo.substring(1);

        // Get the file path in the "static" directory
        String filePath = getServletContext().getRealPath("/") + "WEB-INF/classes/templates/" + fileName;

        // Set the response content type based on the file type
        String contentType = getServletContext().getMimeType(filePath);
        response.setContentType(contentType);

        // Read the file from the "static" directory and write it to the response output stream
        FileInputStream inputStream = new FileInputStream(filePath);
        OutputStream outputStream = response.getOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        inputStream.close();
        outputStream.close();
    }

    private static void getPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/classes/templates/index.html");
        dispatcher.forward(request, response);
    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
