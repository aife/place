package fr.paris.epm.redaction.presentation.controllers;

import fr.paris.epm.global.presentation.controllers.AbstractController;
import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.metier.OrganismeCritere;
import fr.paris.epm.noyau.metier.ProcedureCritere;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefNature;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.DocumentRedactionFacade;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.metier.documentHandler.AbstractDocument;
import fr.paris.epm.redaction.metier.documentHandler.DocumentUtil;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;
import fr.paris.epm.redaction.presentation.bean.ContexteGeneration;
import fr.paris.epm.redaction.presentation.bean.ResultatGeneration;
import fr.paris.epm.redaction.presentation.views.*;
import org.hibernate.NonUniqueResultException;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/canevas")
@PropertySource("classpath:clausier.properties")
public class CanevasRestController extends AbstractController {

    private static final Logger log = LoggerFactory.getLogger(CanevasRestController.class);

    public static final int OPTION_TOUS = 0;

    @Resource
    private RedactionServiceSecurise redactionService;
    @Resource
    private ConsultationServiceSecurise consultationServiceSecurise;

    @Resource
    private ReferentielFacade referentielFacade;

    @Resource
    private DocumentRedactionFacade documentRedactionFacade;

    public static List<EpmTRefTypeDocument> TYPES_DOCUMENTS;

    public static Map<ContexteGeneration, ResultatGeneration> CACHES_XML = new HashMap<>();


    @Value("${mpe.client}")
    private String mpePlateformeUuid;


    @Value("${canevas.generation.derogations.active:true}")
    private boolean derogationActive;

    @Value("${mpe.client}")
    private String plateformeUuid;

    @GetMapping(value = "/creer", produces = "text/plain;charset=UTF-8")
    public final ResponseEntity<String> creer(@RequestParam(value = "consultation") final String refConsultation, @RequestParam(value = "canevas") final String refCanevas, @RequestParam(value = "utilisateur") final String refUtilisateur, @RequestParam(value = "plateformeUuid", required = false) String plateformeUuid,
                                              @RequestParam(value = "type") final String type,
                                              @RequestParam(value = "lot", required = false) final Integer idLot
    ) {
        ResponseEntity<String> result = null;

        var watch = new StopWatch();

        log.info("Création de la trame du document avec consultation = {}, reference du canevas = {}, et utilisateur  = {} et lot = {}", refConsultation, refCanevas, refUtilisateur, idLot);

        Element element;

        // Recherche de la consultation
        watch.start("Recherche de consultation");
        log.info("Recherche de la consultation '{}'", refConsultation);

        final var critereConstultation = new ConsultationContexteCritere();
        critereConstultation.setReference(refConsultation);
        if (null == plateformeUuid) {
            plateformeUuid = mpePlateformeUuid;
        }
        critereConstultation.setPlateforme(plateformeUuid);

        final EpmTConsultation consultation = consultationServiceSecurise.chercherConsultation(critereConstultation);

        if (null == consultation) {
            throw new RuntimeException(MessageFormatter.format("La consultation {} n'existe pas", refConsultation).getMessage());
        }
        watch.stop();

        // Recherche de l'utilisateur
        watch.start("Recherche de l'utilisateur");
        log.debug("Recherche de l'utilisateur {} / {}", refUtilisateur, consultation.getIdOrganisme());

        final OrganismeCritere orgCritere = new OrganismeCritere(mpePlateformeUuid);
        orgCritere.setIdOrganisme(consultation.getIdOrganisme());
        final EpmTRefOrganisme organisme = redactionService.chercherUniqueEpmTObject(0, orgCritere);

        final EpmTUtilisateur utilisateur = this.rechercheUtilisateur(refUtilisateur, organisme, plateformeUuid);

        if (null == utilisateur) {
            throw new RuntimeException(MessageFormatter.format("L'utilisateur {} n'existe pas", refUtilisateur).getMessage());
        }
        watch.stop();
        // Recherche du canevas
        watch.start("Recherche de canevas");

        final var canevasCritere = new CanevasViewCritere(organisme.getPlateformeUuid());

        log.info("Recherche du canevas depuis sa référence '{}'", refCanevas);

        canevasCritere.setReference(refCanevas);
        canevasCritere.setIdOrganisme(organisme.getId());

        final EpmVCanevas canevas = redactionService.chercherUniqueEpmTObject(0, canevasCritere);

        if (null == canevas) {
            throw new RuntimeException(MessageFormatter.format("Le canevas '{}' n'existe pas", refCanevas).getMessage());
        }

        watch.stop();

        var contexte = new ContexteGeneration(refConsultation, refCanevas, consultation.getIdOrganisme(), type, idLot);

        watch.start("Gestion du cache");
        if (CACHES_XML.containsKey(contexte)) {
            log.info("Données en cache trouvées pour le contexte {}", contexte);

            var cache = CACHES_XML.get(contexte);
            var miseEnCache = cache.getMiseEnCache();

            if (canevas.getDateModification().before(miseEnCache) && consultation.getDateModification().getTime().before(miseEnCache)) {
                log.debug("Pas de modification du canevas ET de la consultation depuis la mise en cache, renvoit du résultat en cache en {} secondes", watch.getTotalTimeSeconds());
                HttpHeaders headers = new HttpHeaders();
                headers.set(HttpHeaders.CONTENT_TYPE, "text/plain;charset=UTF-8");

                result = new ResponseEntity<>(cache.getXml(), headers, HttpStatus.OK);

            } else {
                CACHES_XML.remove(contexte);
            }
        }
        watch.stop();

        if (null == result) {
            log.info("Aucune données en cache pour le contexte {}", contexte);

            try {
                var document = new Document();

                document.setIdCanevas(canevas.getIdCanevas());
                document.setIdPublication(canevas.getIdPublication());
                document.setIdConsultation(consultation);
                document.setActif(Document.STATUT_EN_COURS);
                document.setAuteur(utilisateur.getNomComplet());
                document.setIdUtilisateur(utilisateur.getId());

                if (null != idLot) {
                    watch.start("Gestion des lots");
                    document.setIdLot(idLot);
                    document.setLot(getIntituleLotParId(consultation.getEpmTBudLots(), idLot-1));
                    watch.stop();
                }
                document.setIntituleConsultation(consultation.getIntituleConsultation());
                document.setNumConsultation(consultation.getNumeroConsultation());
                document.setPouvoirAdjudicateur(consultation.getEpmTRefPouvoirAdjudicateur().getLibelle());
                document.setProcedurePassation(consultation.getEpmTRefProcedure().getLibelle());
                document.setNaturePrestation(consultation.getEpmTRefNature().getLibelle());

                watch.start("Gestion des types de documents");
                if (null == TYPES_DOCUMENTS) {
                    log.debug("Chargement des types de documents");

                    TYPES_DOCUMENTS = referentielFacade.chargerTousTypesDocument(false, null);
                }

                final var epmTRefTypeDocument = TYPES_DOCUMENTS
                        .stream()
                        .filter(typeDocument -> correspond(typeDocument, type))
                        .findFirst().orElseThrow(() -> new IllegalArgumentException("Impossible de trouver le type " + type));

                document.setTypeDocument(epmTRefTypeDocument.getId());

                watch.stop();

                document.setDocType(epmTRefTypeDocument.getLibelle());

                watch.start("Generation de la structure du document");

                Document resultat = documentRedactionFacade.genererStructureDocument(document, utilisateur, epmTRefTypeDocument.getId(), null, RechercheClauseEnum.DERNIERE_CLAUSE);

                watch.stop();

                watch.start("Filtrage des CCAG");

                final var listeCCAG = filtrerCanevasDeQuelCcag(consultation, idLot);
                log.debug("Liste des ccags de la consultation = {}", listeCCAG);

                watch.stop();

                watch.start("Gestion des valeurs héritées");
                try {
                    // Mise à jour de la comsultation avec condtionnement et champs hérités
                    resultat = documentRedactionFacade.miseAjourValeurHerite(resultat, consultation, null == idLot ? -1 : idLot, canevas.getIdCanevas());
                } catch (Exception e) {
                    log.debug("Erreur lors du conditionnement", e);
                }
                watch.stop();

                watch.start("Transformation XML");
                element = DocumentUtil.versJdom(resultat);

                if (null != type && derogationActive) {
                    element.setAttribute("type", type.toUpperCase());
                }

                log.info("Géneré en {} secondes", watch.getTotalTimeSeconds());

                var xml = Util.encodeXML(AbstractDocument.toXml(element));
                watch.stop();

                CACHES_XML.put(contexte, new ResultatGeneration(xml));

                result = ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).body("<document></document>");
            } catch (Throwable t) {
                log.error("Erreur lors de la generation du document", t);
            }
        }

        log.info(watch.prettyPrint());

        return result;
    }

    public static String getIntituleLotParId(final Collection<EpmTBudLot> collectionLots, final int idLot) {
        if (idLot == Util.ID_TOUS) return Util.INTITULE_TOUS;
        else if (idLot == Util.ID_NON_APPLICABLE) return Util.INTITULE_NON_APPLICABLE;
        else {
            for (int i = 1; i <= collectionLots.size(); i++) {
                if (collectionLots.toArray()[i] instanceof EpmTBudLot) {
                    EpmTBudLot lot = (EpmTBudLot) collectionLots.toArray()[i];
                    if (i == idLot) {
                        return lot.getIntituleLot();
                    }
                }
            }
        }
        return "";
    }
    public static boolean correspond(EpmTRefTypeDocument document, String input) {
        log.debug("Recherche de correspondance dans le type de document depuis {}", input);

        String label = document.getCodeExterne();

        log.debug("Type en base: code externe = {}", label);

        boolean result = label.equalsIgnoreCase(input);

        log.debug("Correspondance avec {} ? {}", input, result);

        return result;
    }

    /**
     * Retourne l'utilisateur externe correspondant a celui indique dans le flux
     * XML en parametre, null si utilisateur inexistant en base.
     */
    private EpmTUtilisateur rechercheUtilisateur(final String guid, final EpmTRefOrganisme organisme, String plateformeUuid) {
        if (null == plateformeUuid) {
            plateformeUuid = mpePlateformeUuid;
        }
        final UtilisateurCritere utilisateurCritere = new UtilisateurCritere(plateformeUuid);
        utilisateurCritere.setGuidsSso(guid);
        utilisateurCritere.setIdOrganisme(organisme.getId());
        try {
            return redactionService.chercherUniqueEpmTObject(0, utilisateurCritere);
        } catch (final NonUniqueResultException ex) {
            throw new RuntimeException(MessageFormatter.format("L'utilisateur {} n'est pas unique", guid).getMessage());
        } catch (final TechnicalNoyauException | ApplicationNoyauException ex) {
            log.error("Erreur lors de la récupération de l'utilisateur", ex);
        }
        return null;
    }

    @GetMapping(value = "/rechercher")
    public final ResponseEntity<Set<Canevas>> rechercher(@RequestParam(value = "consultation") final String reference, @RequestParam(value = "type") final String type, @RequestParam(value = "editeur", required = false) final Boolean editeur, @RequestParam(value = "plateformeUuid", required = false) String plateformeUuid,
                                                         @RequestParam(value = "organisme", required = false) final String acronymeOrganisme,
                                                         @RequestParam(value = "lot", required = false) final Integer idLot
    ) {
        var watch = new StopWatch();

        watch.start();

        log.info("Recherche du type de canevas avec consultation = {}, type de document = {}, et editeur = {} et lot = {}", reference, type, editeur, idLot);

        Set<Canevas> result = null;
        try {

            final ConsultationContexteCritere critereConstultation = new ConsultationContexteCritere();
            critereConstultation.setReference(reference);
            if (null == plateformeUuid) {
                plateformeUuid = mpePlateformeUuid;
            }
            critereConstultation.setPlateforme(plateformeUuid);

            final EpmTConsultation consultation = consultationServiceSecurise.chercherConsultation(critereConstultation);

            log.debug("Consultation trouvée pour l'id {}", consultation.getNumeroConsultationExterne());

            final CanevasViewCritere critere = new CanevasViewCritere(plateformeUuid);
            critere.setActif(true);

            final OrganismeCritere organismeCritere = new OrganismeCritere(plateformeUuid);

            if (null == acronymeOrganisme) {
                log.debug("Recherche de l'organisme depuis son id {}", consultation.getIdOrganisme());

                organismeCritere.setIdOrganisme(consultation.getIdOrganisme());
            } else {
                log.debug("Recherche de l'organisme depuis son acronysme '{}'", acronymeOrganisme);
                organismeCritere.setCodesExternes(acronymeOrganisme);
            }

            final EpmTRefOrganisme organisme = redactionService.chercherUniqueEpmTObject(0, organismeCritere);

            log.debug("Organisme '{}' trouvé!", organisme.getLibelle());

            critere.setIdOrganisme(organisme.getId());
            critere.setEditeur(editeur);

            if (null != type) {
                log.debug("Filtre sur le type de document demandé est = {}", type);

                final EpmTRefTypeDocument epmTRefTypeDocument = referentielFacade.chargerTousTypesDocument(false, null)
                        .stream().filter(typeDocument -> correspond(typeDocument, type))
                        .findFirst().orElseThrow(() -> new IllegalArgumentException("Impossible de trouver le type " + type));

                critere.setIdTypeDocument(epmTRefTypeDocument.getId());
            } else {
                log.debug("Aucun filtre sur le type de document");
            }

            if (null != idLot && idLot > 0) {
                log.debug("L'id du lot est fourni = {}", idLot);
            }

            if (null != consultation.getEpmTBudLots()) {
                log.debug("Il y a  {} lots dans la consultation", consultation.getEpmTBudLots().size());
            }

            // Nature prestation
            if (null != idLot && idLot > 0 && consultation.getEpmTBudLots() != null) {

                log.debug("Identifiant du lot {} fourni en entrée ou {} lots dans la consultation", idLot, consultation.getEpmTBudLots().size());
                for (final EpmTBudLot lot : consultation.getEpmTBudLots()) {
                    if (lot.getId() == idLot) {
                        critere.setNaturePrestation(lot.getEpmTRefNature().getId());
                        log.info("Le lot {} a été trouvé, sa nature de prestation vaut {}", lot.getId(), lot.getEpmTRefNature().getId());

                        break;
                    }
                }
            } else {
                log.debug("Pas d'identifiant du lot en entrée ou aucun lot dans la consultation");

                critere.setNaturePrestation(consultation.getEpmTRefNature().getId());
            }

            log.debug("La procedure de la consultation = {}", consultation.getEpmTRefProcedure());
            ProcedureCritere procedureCritere = new ProcedureCritere();
            procedureCritere.setCodeExterne(consultation.getEpmTRefProcedure().getCodeExterne());
            List<EpmTRefProcedure> procedures = redactionService.chercherEpmTObject(0, procedureCritere);
            final List<Integer> listeCCAG = filtrerCanevasDeQuelCcag(consultation, idLot);
            critere.setListeIdCCAG(listeCCAG);

            log.debug("Liste des CCAG = {}", listeCCAG);

            if (consultation.getEpmTRefTypeContrat() != null && consultation.getEpmTRefTypeContrat().getId() != null) {
                log.debug("Ref attribution = {}", consultation.getEpmTRefTypeContrat().getId());

                critere.setIdTypeContrat(consultation.getEpmTRefTypeContrat().getId());
            } else {
                log.debug("Aucune ref attribution");
            }
            critere.setIdStatutRedactionClausier(EpmTRefStatutRedactionClausier.VALIDEE);
            critere.setChercherNombreResultatTotal(false);
            critere.setProprieteTriee("reference");
            critere.setTriCroissant(true);

            final Set<EpmTCanevasAbstract> listeCanevas = new HashSet<>();
            final Set<Integer> listeCanevasId = new HashSet<>();
            for (EpmTRefProcedure procedure : procedures) {
                critere.setProcedure(procedure.getId());
                final List<EpmTCanevasAbstract> listeCanevasProcedure = redactionService.chercherEpmTObject(0, critere);
                for (EpmTCanevasAbstract canevasAbstract : listeCanevasProcedure) {
                    if(!listeCanevasId.contains(canevasAbstract.getIdCanevas())) {
                        listeCanevas.add(canevasAbstract);
                        listeCanevasId.add(canevasAbstract.getIdCanevas());
                    }
                }

            }

            log.debug("Nombre de canevas trouvée = {}", listeCanevas.size());

            result = listeCanevas.stream().map(
                    canevas -> {
                        final Canevas vueCanevas = new Canevas();

                        log.debug("Id du canevas = {}", canevas.getIdCanevas());
                        vueCanevas.setId(canevas.getIdCanevas());

                        log.debug("Ref du canevas = {}", canevas.getReference());
                        vueCanevas.setReference(canevas.getReference());

                        log.debug("Auteur du canevas = {}", canevas.getAuteur());
                        vueCanevas.setAuteur(canevas.getAuteur());

                        final Statut vueStatut = new Statut();

                        log.debug("Id status du canevas = {}", canevas.getEpmTRefStatutRedactionClausier().getId());
                        vueStatut.setId(canevas.getEpmTRefStatutRedactionClausier().getId());

                        log.debug("Libelle status du canevas = {}", canevas.getEpmTRefStatutRedactionClausier().getLibelle());
                        vueStatut.setLibelle(canevas.getEpmTRefStatutRedactionClausier().getLibelle());
                        vueCanevas.setStatut(vueStatut);

                        log.debug("Titre du canevas = {}", canevas.getAuteur());
                        vueCanevas.setTitre(canevas.getTitre());

                        final EpmTRefNature nature = redactionService.chercherObject(canevas.getIdNaturePrestation(), EpmTRefNature.class);

                        if (null != nature) {
                            final NaturePrestation vueNaturePrestation = new NaturePrestation();
                            log.debug("Id nature du canevas = {}", nature.getId());
                            vueNaturePrestation.setId(nature.getId());

                            log.debug("Libelle nature du canevas = {}", nature.getLibelle());
                            vueNaturePrestation.setLibelle(nature.getLibelle());
                            vueCanevas.setNaturePrestation(vueNaturePrestation);
                        } else {
                            log.debug("Aucune nature du canevas");
                        }

                        log.debug("Procedures du canevas = {}", canevas.getEpmTRefProcedures());

                        vueCanevas.setProcedures(
                                canevas.getEpmTRefProcedures().stream()
                                        .map(procedure -> {
                                            final Procedure vueProcedure = new Procedure();
                                            vueProcedure.setIdentifiantExterne(procedure.getId());
                                            vueProcedure.setAcronyme(procedure.getLibelleCourt());
                                            vueProcedure.setLibelle(procedure.getLibelle());

                                            return vueProcedure;
                                        }).collect(Collectors.toSet())
                        );

                        log.debug("Contrats du canevas = {}", canevas.getEpmTRefTypeContrats());

                        vueCanevas.setContrats(canevas.getEpmTRefTypeContrats().stream().map(
                                contrat -> {
                                    final Contrat vueContrat = new Contrat();
                                    vueContrat.setId(contrat.getId());
                                    vueContrat.setLibelle(contrat.getLibelle());

                                    return vueContrat;
                                }
                        ).collect(Collectors.toSet()));

                        log.debug("Type du canevas = {}", type);

                        vueCanevas.setType(type);

                        log.debug("Date de crátion du canevas = {}", canevas.getDateCreation());

                        vueCanevas.setDateCreation(canevas.getDateCreation());

                        if (!canevas.getDateModification().equals(canevas.getDateCreation())) {
                            log.debug("Date de modificqtion du canevas = {}", canevas.getDateModification());
                            vueCanevas.setDateModification(canevas.getDateModification());
                        } else {
                            log.debug("Pas de date de modificqtion du canevas");
                        }

                        final Organisme vueOrganisme = new Organisme();

                        log.debug("Id de l'organisme du canevas = {}", canevas.getIdOrganisme());
                        vueOrganisme.setIdentifiantExterne(canevas.getIdOrganisme());
                        log.debug("Libellé de l'organisme du canevas = {}", organisme.getLibelle());
                        vueOrganisme.setLibelle(organisme.getLibelle());
                        vueCanevas.setOrganisme(vueOrganisme);

                        log.debug("Id publication = {}", canevas.getIdPublication());
                        if (canevas.getIdPublication() != null) {
                            EpmTPublicationClausier publication = redactionService.chercherObject(canevas.getIdPublication(), EpmTPublicationClausier.class);
                            vueCanevas.setVersion(publication.getVersion());
                        } else if (canevas.getIdLastPublication() != null) {
                            EpmTPublicationClausier publication = redactionService.chercherObject(canevas.getIdLastPublication(), EpmTPublicationClausier.class);
                            vueCanevas.setVersion(publication.getVersion());
                        }

                        return vueCanevas;
                    }
            ).collect(Collectors.toSet());
        } catch (Throwable t) {
            log.error("Erreur lors de la generation du document", t);
        }

        watch.stop();

        log.info("{} canevas trouvé en {} secondes", null == result ? 0 : result.size(), watch.getTotalTimeSeconds());

        return ResponseEntity.ok(result);
    }

    /**
     * Rechercher une liste des ids ref CCAG en fonction du CCAG de référence
     * renseigné dans le formulaire amont de la consultation qui va être utilisé
     * pour filtrer les canevas disponibles pour la création du document.
     *
     * @param idLot indiquer le document est créé pour quel lot. 0 si tous lots.
     *              -1 si non applicable
     * @return liste CCAG que le canevas doit satisfaire
     */
    public static List<Integer> filtrerCanevasDeQuelCcag(final EpmTConsultation consultation, final Integer idLot) {
        final List<Integer> listeIdCCAG = new ArrayList<>();

        listeIdCCAG.add(OPTION_TOUS);

        if (consultation.getEpmTBudLotOuConsultation() != null && consultation.getEpmTBudLotOuConsultation().getEpmTRefCcag() != null) {
            if (Constantes.NON.equals(consultation.getAllotissement())) {
                listeIdCCAG.add(consultation.getEpmTBudLotOuConsultation().getEpmTRefCcag().getId());
            } else if (null != idLot) {
                for (int i = 0; i < consultation.getEpmTBudLots().size(); i++) {
                    EpmTBudLot lot = consultation.getEpmTBudLots().toArray(new EpmTBudLot[0])[i];
                    if (i == idLot) {
                        listeIdCCAG.add(lot.getEpmTBudLotOuConsultation().getEpmTRefCcag().getId());
                    }
                }
            }
        }

        return listeIdCCAG;
    }
}
