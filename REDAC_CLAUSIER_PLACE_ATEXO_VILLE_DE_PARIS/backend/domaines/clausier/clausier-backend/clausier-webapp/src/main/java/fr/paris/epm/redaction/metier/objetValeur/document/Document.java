package fr.paris.epm.redaction.metier.objetValeur.document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.redaction.util.Constantes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * la classe Document pour traiter et manipuler des documents.
 *
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class Document extends AbstractChapitreDocument {
    /**
     * l'attribut static STATUT_EN_COURS initialisé à : EN COURS.
     */
    public static final String STATUT_EN_COURS = "EN COURS";

    /**
     * attribut static Demande de validation du document.
     */
    public static final String STATUT_EN_ATTENTE_VALIDATION = "DOCUMENT REDIGE ATTENTE VALIDATION";

    /**
     * l'attribut static STATUT_VALIDER initialisé à : ELIGIBLE.
     */
    public static final String STATUT_VALIDER = "ELIGIBLE";

    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * intitulé de la consultation.
     */
    private String intituleConsultation;

    /**
     * numéro de la consultation.
     */
    private String numConsultation;

    /**
     * pouvoir adjudicateur.
     */
    private String pouvoirAdjudicateur;

    /**
     * direction / service.
     */
    private String directionServic;

    /**
     * procedure de passation.
     */
    private String procedurePassation;

    /**
     * nature de préstation.
     */
    private String naturePrestation;

    /**
     * autorise la reprise integrale de l'article.
     */
    private boolean repIntegralement;

    /**
     * autorise la suppression d'articles/sous articles.
     */
    private boolean autoriseSupprimerArticles;

    /**
     * type de document.
     */
    private String docType;

    /**
     * lot du document.
     */
    private String lot;

    /**
     * identifiant du lot.
     */
    private int idLot = 0;

    /**
     * titre du document.
     */
    private String titre;

    /**
     * nom du fichier.
     */

    private String nomFichier = null;

    /**
     * reference du document.
     */
    private String reference = "";

    /**
     * l'identifiant du document.
     */
    private int id;

    /**
     * liste des chapitres du document.
     */
    private List<ChapitreDocument> chapitres = new ArrayList<>();

    /**
     * identifiant du canevas.
     */
    private int idCanevas;

    /**
     * identifiant de la publication.
     * NULL si c'est un canevas de client
     */
    private Integer idPublication;

    /**
     * auteur.
     */
    private String auteur;

    private Integer idUtilisateur;

    /**
     * version.
     */
    private int version;

    /**
     * l'attribut actif.
     */
    private String actif;

    /**
     * date de création.
     */
    private Date dateCreation;

    /**
     * date de modification.
     */
    private Date dateModification;

    /**
     * identifiant de la consultation.
     */
    private EpmTConsultation idConsultation;

    /**
     * extension.
     */
    private String extension;

    /**
     * reference du canevas.
     */
    private String refCanevas;

    /**
     * canevas modifiée ou non.
     */
    private boolean canevasModifie;

    /**
     * Message expliquant les modifications apportées dans le canevas.
     */
    private String messageModification;

    /**
     * Format de sortie du document (odt, pdf, doc...).
     */
    private int typeDocument;
    /**
     * Détermine si c'est le mode service
     */
    private boolean externe;

    /**
     * Détermine si l'utilisateur a le profil "Rédaction des documents REDAC"
     */
    private boolean profilRedacteur;

    /**
     * Détermine si l'utilisateur a le profil "Rédaction des documents REDAC"
     */
    private boolean profilValidateur;

    /**
     * identifiant
     */
    private String identifiant;

    /**
     * Url de redirection
     */
    private String url;

    /**
     * Commentaire de validation/refus de document
     */
    private String commentaire;

    /**
     * L'id de l'action du document {@EpmTRefActionDocument}
     */
    private int idActionDocument;

    /**
     * Attribut qui défini si le sommaire doit être généré ou pas
     */
    private boolean sommaire;

    /**
     * Determine si le canevas est actif ou pas
     */
    private boolean canevasActif;

    /**
     * Identifiant du document ayant servi à la duplication
     */
    private int idDocumentInitial;

    /**
     * Détermine si c'est un premier accès au document en duplication afin de
     * déclencher le traitement d'initialisation spécifique
     */
    private boolean premierAccesInitialisationDocument;

    /**
     * Détermine si on affiche la gestion des dérogations ou pas
     */
    private boolean derogationActive;

    /**
     * Détermine si l'extension du document peut être choisie
     */
    private boolean choixExtensionAutorise;

    /**
     * Attribut template utilisé pour la génération de la page de garde
     */
    private String template;

    /**
     * indiquer si le document est un page De Garde
     */
    private boolean pageDeGarde;

    /**
     * La date limite remise affichée dans le document
     */
    private Date dateLimiteRemise;

    /**
     * Le logo dans la page de garde
     */
    @JsonIgnore
    private byte[] logo;

    /**
     * organisme de l'utilisateur qui génère le document.
     */
    private Integer idOrganisme;

    /**
     *
     */
    private boolean editeur;

    /**
     * Statut du document
     */
    private String statut;

    public String getIntituleConsultation() {
        return intituleConsultation;
    }

    public void setIntituleConsultation(String intituleConsultation) {
        this.intituleConsultation = intituleConsultation;
    }

    public String getNumConsultation() {
        return numConsultation;
    }

    public void setNumConsultation(String numConsultation) {
        this.numConsultation = numConsultation;
    }

    public String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    public void setPouvoirAdjudicateur(String pouvoirAdjudicateur) {
        this.pouvoirAdjudicateur = pouvoirAdjudicateur;
    }

    public String getDirectionServic() {
        return directionServic;
    }

    public void setDirectionServic(String directionServic) {
        this.directionServic = directionServic;
    }

    public String getProcedurePassation() {
        return procedurePassation;
    }

    public void setProcedurePassation(String procedurePassation) {
        this.procedurePassation = procedurePassation;
    }

    public String getNaturePrestation() {
        return naturePrestation;
    }

    public void setNaturePrestation(String naturePrestation) {
        this.naturePrestation = naturePrestation;
    }

    public boolean isRepIntegralement() {
        return repIntegralement;
    }

    public void setRepIntegralement(boolean repIntegralement) {
        this.repIntegralement = repIntegralement;
    }

    public boolean isAutoriseSupprimerArticles() {
        return autoriseSupprimerArticles;
    }

    public void setAutoriseSupprimerArticles(boolean autoriseSupprimerArticles) {
        this.autoriseSupprimerArticles = autoriseSupprimerArticles;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public int getIdLot() {
        return idLot;
    }

    public void setIdLot(int idLot) {
        this.idLot = idLot;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ChapitreDocument> getChapitres() {
        return chapitres;
    }

    public void setChapitres(List<ChapitreDocument> chapitres) {
        this.chapitres = chapitres;
    }

    public int getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getActif() {
        return actif;
    }

    public void setActif(String actif) {
        this.actif = actif;

        if (Document.STATUT_EN_COURS.equals(actif))
            statut = "images/document-statut-1.gif";
        else if (Document.STATUT_EN_ATTENTE_VALIDATION.equals(actif))
            statut = "images/document-statut-2.gif";
        else if (Document.STATUT_VALIDER.equals(actif))
            statut = "images/document-statut-3.gif";
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public EpmTConsultation getIdConsultation() {
        return idConsultation;
    }

    public void setIdConsultation(EpmTConsultation idConsultation) {
        this.idConsultation = idConsultation;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getRefCanevas() {
        return refCanevas;
    }

    public void setRefCanevas(String refCanevas) {
        this.refCanevas = refCanevas;
    }

    public boolean isCanevasModifie() {
        return canevasModifie;
    }

    public void setCanevasModifie(boolean canevasModifie) {
        this.canevasModifie = canevasModifie;
    }

    public String getMessageModification() {
        return messageModification;
    }

    public void setMessageModification(String messageModification) {
        this.messageModification = messageModification;
    }

    public int getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(int typeDocument) {
        this.typeDocument = typeDocument;
    }

    public boolean isExterne() {
        return externe;
    }

    public void setExterne(boolean externe) {
        this.externe = externe;
    }

    public boolean isProfilRedacteur() {
        return profilRedacteur;
    }

    public void setProfilRedacteur(boolean profilRedacteur) {
        this.profilRedacteur = profilRedacteur;
    }

    public boolean isProfilValidateur() {
        return profilValidateur;
    }

    public void setProfilValidateur(boolean profilValidateur) {
        this.profilValidateur = profilValidateur;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getIdActionDocument() {
        return idActionDocument;
    }

    public void setIdActionDocument(int idActionDocument) {
        this.idActionDocument = idActionDocument;
    }

    public boolean isSommaire() {
        return sommaire;
    }

    public void setSommaire(boolean sommaire) {
        this.sommaire = sommaire;
    }

    public boolean isCanevasActif() {
        return canevasActif;
    }

    public void setCanevasActif(boolean canevasActif) {
        this.canevasActif = canevasActif;
    }

    public int getIdDocumentInitial() {
        return idDocumentInitial;
    }

    public void setIdDocumentInitial(int idDocumentInitial) {
        this.idDocumentInitial = idDocumentInitial;
    }

    public boolean isPremierAccesInitialisationDocument() {
        return premierAccesInitialisationDocument;
    }

    public void setPremierAccesInitialisationDocument(boolean premierAccesInitialisationDocument) {
        this.premierAccesInitialisationDocument = premierAccesInitialisationDocument;
    }

    public boolean isDerogationActive() {
        return derogationActive;
    }

    public void setDerogationActive(boolean derogationActive) {
        this.derogationActive = derogationActive;
    }

    public boolean isChoixExtensionAutorise() {
        return choixExtensionAutorise;
    }

    public void setChoixExtensionAutorise(boolean choixExtensionAutorise) {
        this.choixExtensionAutorise = choixExtensionAutorise;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public boolean isPageDeGarde() {
        return pageDeGarde;
    }

    public void setPageDeGarde(boolean pageDeGarde) {
        this.pageDeGarde = pageDeGarde;
    }

    public Date getDateLimiteRemise() {
        return dateLimiteRemise;
    }

    public void setDateLimiteRemise(Date dateLimiteRemise) {
        this.dateLimiteRemise = dateLimiteRemise;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public boolean isEditeur() {
        return editeur;
    }

    public void setEditeur(boolean editeur) {
        this.editeur = editeur;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    @Override
    public final int hashCode() {
        int result = 1;
        if (chapitres != null) {
            result = Constantes.PREMIER * result + chapitres.hashCode();
        }
        if (reference != null) {
            result = Constantes.PREMIER * result + reference.hashCode();
        }
        return result;
    }

    /**
     * cette méthode teste l'égalité entre deux objets.
     *
     * @param obj est le parametre à tester.
     * @return vrai si les deux objets sont égaux si non retourner faux.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Document))
            return false;

        final Document autre = (Document) obj;
        if (chapitres == null)
            if (autre.chapitres != null)
                return false;
            else if (!chapitres.equals(autre.chapitres)) // c'est de la merde car compare des adresse
                return false;

        if (this.id != autre.id)
            return false;

        if (reference == null)
            return autre.reference == null;
        else
            return reference.equals(autre.reference);
    }

    /**
     * cette méthode permet la conversion de type en String.
     *
     * @return la chaîne de caractère convertie.
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString() {

        StringBuilder r = new StringBuilder("Document")
                .append("\nId: ").append(id)
                .append("\nRéférence: ").append(this.reference)
                .append("\nChapitres: ").append(chapitres.size());
        for (ChapitreDocument chap : chapitres)
            r.append("\n ").append(chap);

        return r.toString();
    }

    @Override
    public final Document clone() {
        Document document = new Document();

        document.intituleConsultation = intituleConsultation;
        document.numConsultation = numConsultation;
        document.pouvoirAdjudicateur = pouvoirAdjudicateur;
        document.directionServic = directionServic;
        document.procedurePassation = procedurePassation;
        document.naturePrestation = naturePrestation;
        document.docType = docType;
        document.lot = lot;
        document.idLot = idLot;
        document.titre = titre;
        document.nomFichier = nomFichier;
        document.reference = reference;
        document.id = id;
        document.idCanevas = idCanevas;
        document.idPublication = idPublication;
        document.auteur = auteur;
        document.idUtilisateur = idUtilisateur;
        document.version = version;
        document.actif = actif;
        document.dateCreation = dateCreation;
        document.dateModification = dateModification;
        document.idConsultation = idConsultation;
        document.extension = extension;
        document.refCanevas = refCanevas;
        document.canevasModifie = canevasModifie;
        document.messageModification = messageModification;
        document.typeDocument = typeDocument;
        document.derogationActive = derogationActive;

        if (chapitres != null)
            document.chapitres = chapitres.stream()
                    .map(chapitre -> (ChapitreDocument) chapitre.clone(document))
                    .collect(Collectors.toList());

        return document;
    }

}
