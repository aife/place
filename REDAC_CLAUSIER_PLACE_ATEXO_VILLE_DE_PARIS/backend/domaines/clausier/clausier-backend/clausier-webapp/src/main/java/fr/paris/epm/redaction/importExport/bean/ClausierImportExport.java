package fr.paris.epm.redaction.importExport.bean;

import java.util.ArrayList;
import java.util.List;

public class ClausierImportExport {

    private String version;

    private String datePublication;

    private String editeur;

    private String commentaire;

    private List<CanevasImportExport> canevas = new ArrayList<>();

    private List<ClauseImportExport> clauses = new ArrayList<>();

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public List<CanevasImportExport> getCanevas() {
        return canevas;
    }

    public void setCanevas( List<CanevasImportExport> canevas ) {
        this.canevas = canevas;
    }

    public List<ClauseImportExport> getClauses() {
        return clauses;
    }

    public void setClauses( List<ClauseImportExport> clauses ) {
        this.clauses = clauses;
    }

}
