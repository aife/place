package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Formulaire ClauseTexteHeritee.
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */

public class SurchargerClauseTexteHeriteeForm extends ActionForm {
    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut texte Fixe Avant.
     */
    private String textFixeAvant;
    /**
     * Attribut reference Valeur Heritee.
     */
    private String refValHeritee;
    /**
     * Attribut taille Champ.
     */
    private String nbCaract;
    /**
     * Attribut valeur Heritee Modifiable.
     */
    private String valHeriteeModifiable = "0";
    /**
     * Attribut texte Fixe Apres.
     */
    private String textFixeApres;
    /**
     * Attribut saut Texte Fixe Avant.
     */
    private String sautTextFixeAvant;
    /**
     * Attribut saut Texte Fixe Apres.
     */
    private String sautTextFixeApres;
    /**
     * Attribut collection de Reference Valeur Heritee.
     */
    private Collection collectionRefValHeritee = new ArrayList();

    /**
     * Constructeur de la classe .
     */
    public SurchargerClauseTexteHeriteeForm() {
        this.reset();
    }

    /**
     * Cette méthode permet d'initialiser le formulaire.
     */
    public void reset() {
        valHeriteeModifiable = "0";
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
        textFixeAvant = "";
        refValHeritee = "";
        nbCaract = "";
        textFixeApres = "";
    }

    /**
     * @return une collection de références de valeurs héritées.
     */
    public final Collection getCollectionRefValHeritee() {
        return collectionRefValHeritee;
    }

    /**
     * @param valeur pour positionner la collection de références de valeurs
     *            héritées.
     */
    public final void setCollectionRefValHeritee(final Collection valeur) {
        collectionRefValHeritee = valeur;
    }

    /**
     * @return texte Fixe Avant
     */
    public final String getTextFixeAvant() {
        return textFixeAvant;
    }

    /**
     * @param valeur initialiser l'attribut textFixeAvant.
     */
    public final void setTextFixeAvant(final String valeur) {
        textFixeAvant = valeur;
    }

    /**
     * @return référence d'une valeur héritée.
     */
    public final String getRefValHeritee() {
        return refValHeritee;
    }

    /**
     * @param valeur pour initialiser la référence d'une valeur héritée.
     */
    public final void setRefValHeritee(final String valeur) {
        refValHeritee = valeur;
    }

    /**
     * @return valeur héritée Modifiable.
     */
    public final String getValHeriteeModifiable() {
        return valHeriteeModifiable;
    }

    /**
     * @param valeur pour positionner la valeur héritée modifiable.
     */
    public final void setValHeriteeModifiable(final String valeur) {
        valHeriteeModifiable = valeur;
    }

    /**
     * @return texte Fixe Apres
     */
    public final String getTextFixeApres() {
        return textFixeApres;
    }

    /**
     * @param valeur pour initialiser l'attribut textFixeApres.
     */
    public final void setTextFixeApres(final String valeur) {
        textFixeApres = valeur;
    }

    /**
     * @return taille champ
     */
    public final String getNbCaract() {
        return nbCaract;
    }

    /**
     * @param valeur pour positionner la taille du champ.
     */
    public final void setNbCaract(final String valeur) {
        nbCaract = valeur;
    }

    /*
     * methode validate () pour valider les données du formulaire.
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping,
     *      javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(final ActionMapping mapping,
            final HttpServletRequest request) {

        ActionErrors erreurs = super.validate(mapping, request);
        if (erreurs == null) {
            erreurs = new ActionErrors();
        }
        erreurs.clear();
        if (valHeriteeModifiable == null
                || valHeriteeModifiable.trim().equals("")) {
            erreurs.add("valHeriteeModifiable", new ActionMessage(
                "redaction.msg.global.erreur"));
        }
        if (refValHeritee != null && refValHeritee.equals("-1")) {
            erreurs.add("refValHeritee", new ActionMessage(
            "redaction.msg.global.erreur"));
        }
        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    /**
     * @return saut Texte Fixe Avant
     */
    public final String getSautTextFixeAvant() {
        return sautTextFixeAvant;
    }

    /**
     * @param valeur initialise l'attribut sautTextFixeAvant.
     */
    public final void setSautTextFixeAvant(final String valeur) {
        sautTextFixeAvant = valeur;
    }

    /**
     * @return saut Texte Fixe Apres
     */
    public final String getSautTextFixeApres() {
        return sautTextFixeApres;
    }

    /**
     * @param valeur initialise l'attribut sautTextFixeApres.
     */
    public final void setSautTextFixeApres(final String valeur) {
        sautTextFixeApres = valeur;
    }

}
