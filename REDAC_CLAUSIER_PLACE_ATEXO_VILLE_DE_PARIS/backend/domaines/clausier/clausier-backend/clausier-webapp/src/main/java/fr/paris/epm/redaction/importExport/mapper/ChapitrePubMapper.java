package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTChapitreAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTChapitrePub;
import fr.paris.epm.redaction.importExport.bean.ChapitreImportExport;
import fr.paris.epm.redaction.presentation.mapper.DirectoryMapper;
import fr.paris.epm.redaction.presentation.mapper.DirectoryNamedMapper;
import org.mapstruct.*;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring", uses = {DirectoryMapper.class, DirectoryNamedMapper.class})
public abstract class ChapitrePubMapper {

    @Mapping(source = "epmTSousChapitres", target = "sousChapitres")
    public abstract ChapitreImportExport toChapitreImportExport(EpmTChapitrePub epmTChapitrePub);

    @Mapping(source = "sousChapitres", target = "epmTSousChapitres")
    public abstract EpmTChapitrePub toEpmTChapitrePub(ChapitreImportExport chapitreImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTChapitrePub setIdPublication(@MappingTarget EpmTChapitrePub epmTChapitrePub, @Context Integer idPublication) {
        epmTChapitrePub.setIdPublication(idPublication);
        return epmTChapitrePub;
    }

    protected List<? extends EpmTChapitreAbstract> toListEpmTChapitresAbstracts(List<ChapitreImportExport> chapitresImportExport, @Context Integer idPublication) {
        if (chapitresImportExport == null)
            return null;

        List<EpmTChapitreAbstract> epmTChapitresAbstracts = new ArrayList<>();
        for (ChapitreImportExport chapitreImportExport : chapitresImportExport)
            epmTChapitresAbstracts.add(toEpmTChapitrePub(chapitreImportExport, idPublication));

        return epmTChapitresAbstracts;
    }

}
