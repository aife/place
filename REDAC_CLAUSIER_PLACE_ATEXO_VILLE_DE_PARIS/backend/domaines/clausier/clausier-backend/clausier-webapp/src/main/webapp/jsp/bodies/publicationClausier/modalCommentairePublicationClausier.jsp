<%--
  Created by IntelliJ IDEA.
  User: qba
  Date: 08/06/18
  Time: 14:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>

<!--Debut modale-->
<div class="modal" id="modalCommentairePublicationClausier_${param.id}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title text-left"><spring:message code="publicationClausier.listPublicationClausier.commentaires" /></h4>
            </div>
            <div class="modal-body">
                <div class="atx-panel panel panel-default form-horizontal">
                    <div class="clearfix">
                        <textarea id="commentaire_${param.id}" class="form-control input-sm" placeholder="${param.commentaire}"></textarea>
                        <input id="id_${param.id}" type="hidden"  class="form-control input-sm" value="${param.id}" />
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" id="valider_${param.id}" class="btn btn-primary btn-sm pull-right" data-dismiss="modal">
                    <spring:message code="redaction.action.valider" />
                </button>
                <button type="button" id="annuler" class="btn btn-warning btn-sm pull-left" data-dismiss="modal">
                    <spring:message code="redaction.action.annuler" />
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal -->

<script type="application/javascript" language="JavaScript" lang="javascript">
    $(document).ready(function () {
        $("#valider_${param.id}").click(function () {
            $.get('editCommentPublicationClausier.json', {idPublication: ${param.id}, commentaire: $("#commentaire_${param.id}").val()}, function(response)
            {
                if (response.result === "success"){
                } else {
                    alert("error");
                }
            });
        });
    });
</script>
