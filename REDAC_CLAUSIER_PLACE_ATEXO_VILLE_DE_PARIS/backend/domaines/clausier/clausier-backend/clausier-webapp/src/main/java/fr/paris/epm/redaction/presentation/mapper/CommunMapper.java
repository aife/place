package fr.paris.epm.redaction.presentation.mapper;

import org.mapstruct.Mapper;

import java.util.Date;

@Mapper(componentModel = "spring")
public interface CommunMapper {

    default Date toDate(Long valeurDate) {
        return valeurDate == null ? null : new Date(valeurDate);
    }

}
