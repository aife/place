<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="form-bloc">
	<div class="top">
		<span class="left"></span><span class="right"></span>
	</div>
	<div class="content">
	
		<div class="top-link">
			<a onclick="toggleRecapPanel(this,'recap-details');" class="toggle-recap-closed" href="javascript:void(0);"></a>
			<div class="consultation-line">
				<span class="intitule"><bean:message key="BlocRecapClause.txt.typeClause" /></span>
				<c:out value="${recapClause.typeClause}" />
			</div>
			<div class="line">
				<span class="intitule"><bean:message key="BlocRecapClause.txt.typeDocument" /></span>
				<c:out value="${recapClause.typeDocument}" />
			</div>
		</div>
		<div id="recap-details" style="display: none;">
			<div class="line">
				<span class="intitule"><bean:message key="BlocRecapClause.txt.procedurePassation" /></span>
				<c:out value="${recapClause.procedurePassation}" />
			</div>
			<div class="line">
				<span class="intitule"><bean:message key="BlocRecapClause.txt.naturePrestation" /></span>
				<c:out value="${recapClause.naturePrestation}" />
			</div>
			<div class="line">
				<span class="intitule"><bean:message key="BlocRecapClause.txt.theme" /></span>
				<c:out value="${recapClause.theme}" />
			</div>
			<div class="line">
				<div class="intitule">
					<bean:message key="BlocRecapClause.txt.motsCles" />
				</div>
				<c:out value="${recapClause.motsCles}" />
			</div>		
			<div class="line">
				<div class="intitule">
					<bean:message key="BlocRecapClause.txt.statutClause" />
				</div>
				<logic:equal name="recapClause" property="statutClause" value="1"><bean:message key="BlocRecapClause.txt.statutClauseActif" /></logic:equal>
				<logic:equal name="recapClause" property="statutClause" value="0"><bean:message key="BlocRecapClause.txt.statutClauseInactif" /></logic:equal>
			</div>
			<c:if test="${entiteAdjudicatrice != null}">
				<div class="line">
					<div class="intitule-bloc">
						<bean:message key="BlocRecapClause.txt.compatibleEntiteAdjudicatrice" />
					</div>
					<logic:equal name="recapClause" property="compatibleEntiteAdjudicatrice" value="oui"><bean:message key="BlocRecapClause.txt.compatibleEntiteAdjudicatriceOui" /></logic:equal>
					<logic:equal name="recapClause" property="compatibleEntiteAdjudicatrice" value="non"><bean:message key="BlocRecapClause.txt.compatibleEntiteAdjudicatriceNon" /></logic:equal>
				</div>
			</c:if>
            <div class="line">
                <div class="intitule">
                    <bean:message key="BlocRecapClause.txt.potCdt" />
                </div>
                <logic:equal name="recapClause" property="potCdt" value="oui"><bean:message key="BlocRecapClause.txt.potCdtOui" /></logic:equal>
                <logic:notEqual name="recapClause" property="potCdt" value="oui"><bean:message key="BlocRecapClause.txt.potCdtNon" /></logic:notEqual>
            </div>
			<div class="line">
				<div class="intitule">
					<bean:message key="BlocRecapClause.txt.infoBulle" />
				</div>
				<logic:equal name="recapClause" property="infoBulle" value="1"><bean:message key="BlocRecapClause.txt.infoBulleOui" /></logic:equal>
				<logic:notEqual name="recapClause" property="infoBulle" value="1"><bean:message key="BlocRecapClause.txt.infoBulleNon" /></logic:notEqual>
			</div>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom">
		<span class="left"></span><span class="right"></span>
	</div>
</div>
<div class="spacer"></div>
<!--Fin bloc recap Type de clause-->
