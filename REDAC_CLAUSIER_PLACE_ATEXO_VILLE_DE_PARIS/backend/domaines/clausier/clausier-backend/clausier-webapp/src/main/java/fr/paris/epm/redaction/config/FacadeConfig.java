package fr.paris.epm.redaction.config;

import fr.paris.epm.redaction.coordination.facade.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration-class contient toutes les Façade à injecter
 * Created by nty on 24/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Configuration
//@EnableTransactionManagement
public class FacadeConfig {

    @Bean(name = "directoryFacade")
    public DirectoryFacade directoryFacade() {
        return new DirectoryFacadeImpl();
    }

    @Bean(name = "clauseFacade")
    public ClauseFacade clauseFacade() {
        return new ClauseFacadeImpl();
    }

    @Bean(name = "clauseViewFacade")
    public ClauseViewFacade clauseViewFacade() {
        return new ClauseViewFacadeImpl();
    }

    @Bean(name = "canevasFacade")
    public CanevasFacade canevasFacade() {
        return new CanevasFacadeImpl();
    }

    @Bean(name = "canevasViewFacade")
    public CanevasViewFacade canevasViewFacade() {
        return new CanevasViewFacadeImpl();
    }

}