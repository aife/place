/**
 * 
 */
package fr.paris.epm.redaction.presentation.taglib;

import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Tag permettant de découper un mot.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class InsererEspaceTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'instance de l'objet.
     */
    private String name;

    /**
     * emplacement de l'objet.
     */
    private String scope;

    /**
     * Taille maximale d'un Mot.
     */
    private static final int TAILLE_MOT = 35;

    /**
     * Taille maximale d'un Mot.
     */
    private int tailleMot = TAILLE_MOT;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InsererEspaceTag.class);

    /**
     * @return int
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           scope);
            if (texte != null) {
                String texteWithSpaces = (String) texte;
                StringTokenizer contenu = new StringTokenizer(
                    texteWithSpaces.trim(), " ");
                StringBuffer sb = new StringBuffer();

                while (contenu.hasMoreElements()) {
                    String morceau = (String) contenu.nextElement();
                    morceau = morceau.trim();
                    if (morceau.trim().length() > tailleMot) {
                        int cpt = morceau.length() / tailleMot;
                        for (int i = 0; i <= cpt; i++) {
                            int beginIndex = i * tailleMot;
                            int endIndex = beginIndex + tailleMot;

                            if (i == cpt) {
                                endIndex = morceau.length();
                            }

                            sb.append(morceau.substring(beginIndex,
                                                        endIndex)
                                    + " ");
                        }
                    } else {
                        sb.append(morceau.trim() + " ");
                    }
                }
                pageContext.getOut().print(sb.toString());
            } else {
                pageContext.getOut().print("");
            }

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
        scope = null;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }

    /**
     * @param valeur emplacement de la variable (request, session, parameter)
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }

    /**
     * @param valeur la taille maximale d'un Mot
     */
    public final void setTailleMot(final int valeur) {
        tailleMot = valeur;
    }

}
