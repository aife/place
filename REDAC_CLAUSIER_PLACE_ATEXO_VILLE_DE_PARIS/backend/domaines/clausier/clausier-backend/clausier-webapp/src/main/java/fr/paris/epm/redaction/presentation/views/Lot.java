package fr.paris.epm.redaction.presentation.views;

import java.util.Set;

public class Lot {

	private Integer id;

	private String libelle;

	private String description;

	private String numero;

	private Set<CPV> cpv;

	public Integer getId() {
		return id;
	}

	public void setId( final Integer id ) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( final String libelle ) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( final String description ) {
		this.description = description;
	}

	public void setNumero( final String numero ) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setCpv( Set<CPV> cpv ) {
		this.cpv = cpv;
	}

	public Set<CPV> getCpv() {
		return cpv;
	}
}
