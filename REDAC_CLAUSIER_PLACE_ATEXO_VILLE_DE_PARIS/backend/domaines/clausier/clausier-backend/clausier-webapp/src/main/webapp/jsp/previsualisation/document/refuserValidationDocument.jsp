<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<title>Elaboration et Passation des Marchés</title>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
        <link type="text/css" href="<atexo:href href='css/styles.css'/>" rel="stylesheet" />
		<link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
        <link type="text/css" href="<atexo:href href='css/styles-spec.css'/>" rel="stylesheet"/>
        <script type="text/javascript">
			 hrefRacine = '<atexo:href href=""/>';
		</script>
    </head>
    <body onload="popupResize();">
    	<script type="text/JavaScript>">
			var prefixeRedirection = '';
		</script>
		<c:if test="${actionTermine != null}">
   			<script type="text/JavaScript">
   				if('${prefixe}' == 'externe'){
		    		window.opener.location.href = "/epm.redaction/externeRechercheDocument.epm";
   				} else {
   					window.opener.location.href = "/epm.redaction/RechercherDocumentInit.epm?type=&ouvrirTous=&numeroCons=" + '${numConsultation}';
   				}
   				window.close();
    		</script>
   		</c:if>
    	<script type="text/JavaScript">
	    	function valider(numeroConsultation) {
	    		document.forms[0].submit();
	    	}
    	</script>
        <div class="previsu-doc" id="container">
			<div class="content">
		        <center><h5><bean:message key="redaction.txt.refusserValidation"/></h5></center>
		
		        <ul>
		        	<li>
		                <div class="clause"><bean:message key="redaction.txt.commentaire"/><br />
 		                <html:form action="/refuserValidationDocument.epm">
							<textarea id="commentaire" name="commentaire" class="textarea-600" rows="30" cols="0"></textarea>
							<input type="hidden" name="idDoc" value="${idDoc}"/>
							<input type="hidden" name="numConsultation" value="${numConsultation}"/>
 						</html:form>
						</div>
		            
		            </li>
		        </ul>
		    </div>
		
			<div class="boutons">
				<a href="javascript:window.close();" class="annuler"><bean:message key="redaction.txt.annuler" /></a>
				<a href="javascript:valider(${numConsultation});" class="valider" title="<bean:message key="redaction.txt.confirmerRefusValidation"/>">
					<bean:message key="redaction.txt.confirmer" />
				</a>
			</div>
		    <div class="breaker"></div>
		</div>
    </body>
</html>