package fr.paris.epm.redaction.presentation.actions;

import fr.paris.epm.global.commons.exception.ApplicationException;
import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commons.exception.UtilisateurException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.metier.vues.VueConsultationCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.ServiceTechniqueFacade;
import fr.paris.epm.redaction.presentation.bean.CategorieDocument;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Declenchee lors de l'acces au module redaction depuis MPE.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class ExterneRechercheDocumentAction extends AbstractDocumentAction {

    private ServiceTechniqueFacade serviceTechniqueFacade;

    @Override
    public ActionForward execute(SessionManagerGlobal sessionManager, ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws SessionExpireeException, UtilisateurException {

        HttpSession session = sessionManager.getSession(request);
        String identifiantExterne = (String) session.getAttribute(ConstantesGlobales.IDENTIFIANT_SAAS);

        // Recuperation utilisateur et consultation
        UtilisateurExterneConsultation userExteneConsult = serviceTechniqueFacade.getUtilisateurExterneConsultation(identifiantExterne);
        if (userExteneConsult == null)
            throw new ApplicationException("Utilisateur absent.");

        EpmTUtilisateur userExterne = userExteneConsult.getUtilisateur();
        if (userExterne == null)
            throw new ApplicationException("Utilisateur absent.");

        EpmTConsultationContexte consult = userExteneConsult.getConsultation();
        if (consult == null)
            throw new ApplicationException("Consultation absente.");

        // Authentification de l'utilisateur

        // Recherche de la vue pour affichage
        VueConsultationCritere vueConsultationCritere = new VueConsultationCritere();
        vueConsultationCritere.setIdOrganisme(userExterne.getIdOrganisme());
        vueConsultationCritere.setNumeroConsultationExterne(consult.getReference());

        session.setAttribute("prefixeRedirection", "externe");
        session.setAttribute("vConsultation", consult);

        EpmTRefParametrage parametragePageDocument =
                (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_GENERATION_PAGE_GARDE, TypeEpmTRefObject.PARAMETRAGE);
        if (parametragePageDocument != null && parametragePageDocument.getValeur().equals("true"))
            sessionManager.ajouterElement(session, Constante.GENERATION_PAGE_DOCUMENT, "true");

	    String categorie = request.getParameter(Constante.CATEGORIE);

	    if (null==categorie || categorie.isEmpty()) {
		    categorie = (String)session.getAttribute(Constante.CATEGORIE);
	    }

	    if (null==categorie || categorie.isEmpty()) {
		    sessionManager.ajouterElement(session, Constante.CATEGORIE, CategorieDocument.DAJ.getLibelle());
	    } else {
		    sessionManager.ajouterElement(session, Constante.CATEGORIE, categorie);
	    }

        return mapping.findForward(Constante.SUCCES);
    }

    public final void setServiceTechniqueFacade(final ServiceTechniqueFacade valeur) {
        this.serviceTechniqueFacade = valeur;
    }

}
