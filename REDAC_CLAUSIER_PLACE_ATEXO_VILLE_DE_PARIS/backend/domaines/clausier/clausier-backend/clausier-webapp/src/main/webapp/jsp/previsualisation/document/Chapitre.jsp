<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<logic:notEmpty name="chapitres">
    <logic:iterate id="chapitre" name="chapitres" indexId="index" type="fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument">
        <%--@elvariable id="chapitre" type="fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument"--%>
        <c:if test="${chapitre.niveau == 1}">
            <c:set var="premierNiveau" value="true" scope="request" />
        </c:if>
        <c:if test="${chapitre.niveau != 1}">
            <c:set var="premierNiveau" value="false" scope="request" />
        </c:if>
        <c:if test="${chapitre.terminer == false && chapitre.duplique == false}">
            <c:set var="couleur" value="red" scope="request" />
        </c:if>
        <c:if test="${chapitre.terminer == false && chapitre.duplique == true}">
            <c:set var="couleur" value="orange" scope="request" />
        </c:if>
        <c:if test="${chapitre.terminer != false}">
            <c:set var="couleur" value="green" scope="request" />
        </c:if>
        <ul id="chapitre_${index+1}">
            <li>
                <logic:equal name="premierNiveau" value="true">
                    <strong>
                        <c:out value="${chapitre.numero}" />.&nbsp;<c:out value="${chapitre.titre}" />
                    </strong>
                </logic:equal>
                <logic:equal name="premierNiveau" value="false">
                    <i>
                        <c:out value="${chapitre.numero}" />.&nbsp;<c:out value="${chapitre.titre}" />
                    </i>
                </logic:equal>
                <ul class="liste-clause">
                    <logic:notEmpty name="chapitre" property="paragrapheDocument">
                        <li>
                            <span class="<c:out value="${couleur}"/>">
                                <nested:write name="chapitre" property="paragrapheDocument.texte" filter="false" />
                            </span>
                        </li>
                    </logic:notEmpty>
                    <logic:empty name="chapitre" property="paragrapheDocument">
                        <logic:notEmpty name="chapitre" property="clausesDocument">
                            <logic:iterate id="clause" name="chapitre" property="clausesDocument">
                                <c:if test="${clause.etat != '3'}">
                                    <li>
                                        <bean:define id="clause" name="clause" toScope="request" />
                                        <jsp:include page="/jsp/previsualisation/document/Clause.jsp" />
                                    </li>
                                </c:if>
                            </logic:iterate>
                        </logic:notEmpty>
                    </logic:empty>
                    <c:if test="${chapitre.derogation != null && chapitre.afficherNBMessageDerogation == true}">
                        <li>
                            <div class="clause">
                                <strong>NB</strong> : cet article déroge à l'article
                                <c:out value="${chapitre.derogation.articleDerogationCCAG}" />
                                du CCAG
                            </div>
                        </li>
                    </c:if>
                </ul>
                <logic:notEmpty name="chapitre" property="sousChapitres">
                    <bean:define id="premierNiveau" value="false" toScope="request" />
                    <bean:define id="chapitres" name="chapitre" property="sousChapitres" toScope="request" />
                    <jsp:include page="/jsp/previsualisation/document/Chapitre.jsp" />
                </logic:notEmpty>
            </li>
        </ul>
    </logic:iterate>
</logic:notEmpty>
