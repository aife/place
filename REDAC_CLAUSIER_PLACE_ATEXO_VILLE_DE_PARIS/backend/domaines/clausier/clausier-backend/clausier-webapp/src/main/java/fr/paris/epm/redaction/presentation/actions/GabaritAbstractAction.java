package fr.paris.epm.redaction.presentation.actions;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.presentation.AbstractAction;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.redaction.presentation.forms.GabaritForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public abstract class GabaritAbstractAction extends AbstractAction {

    static String levelInterministeriel = "Interministeriel";
    static String levelMinisteriel = "Ministeriel";
    static String levelDirectionService = "DirectionService";
    static String levelAgent = "Agent";

    static int idLevelInterministeriel = 1;
    static int idLevelMinisteriel = 2;
    static int idLevelDirectionService = 3;
    static int idLevelAgent = 4;

    Boolean isPageDeGarde(GabaritForm gabaritForm) {
        String typeDocumentOuPageGarde = gabaritForm.getIdTypeDocumentOuPageGarde();

        if (typeDocumentOuPageGarde == null || typeDocumentOuPageGarde.equals("null"))
            return null;
        else if (typeDocumentOuPageGarde.startsWith("td_"))
            return false;
        else if (typeDocumentOuPageGarde.startsWith("pg_"))
            return true;
        else
            return null;
    }

    Integer idTypeDocument(GabaritForm gabaritForm) {
        String typeDocumentOuPageGarde = gabaritForm.getIdTypeDocumentOuPageGarde();

        if (typeDocumentOuPageGarde == null || typeDocumentOuPageGarde.equals("null"))
            return null;
        return Integer.parseInt(typeDocumentOuPageGarde.substring(3));
    }

    EpmTUtilisateur getUtilisateur(SessionManagerGlobal sessionManager, HttpServletRequest request) throws SessionExpireeException {
        HttpSession session = sessionManager.getSession(request);
        return (EpmTUtilisateur) session.getAttribute("utilisateur");
    }

    String forwardByIntervention(GabaritForm gabaritForm, EpmTUtilisateur utilisateur) {
        switch (gabaritForm.getIdIntervention()) {
            case 1:
                gabaritForm.setIdRepresentant(null);
                return "successInterministeriel";
            case 2:
                gabaritForm.setIdRepresentant(utilisateur.getIdOrganisme());
                return "successMinisteriel";
            case 3:
                gabaritForm.setIdRepresentant(utilisateur.getDirectionService());
                return "successDirection";
            case 4:
                gabaritForm.setIdRepresentant(utilisateur.getId());
                return "successAgent";
            default:
                // erreur
                return "erreur";
        }
    }

}
