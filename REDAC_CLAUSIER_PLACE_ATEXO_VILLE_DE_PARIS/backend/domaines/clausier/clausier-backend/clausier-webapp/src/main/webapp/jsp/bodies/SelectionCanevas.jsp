<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs">
        <bean:message key="SelectionCanevas.titre" />
    </div>
    <atexo:fichePratique reference="" key="common.fichePratique"/>
    <div class="breaker"></div>
    <c:if test="${prefixeRedirection == null}">

        <!--Debut bloc recap Document-->
        <div class="form-bloc">
            <div class="top">
                <span class="left"></span><span class="right"></span>
            </div>
            <div class="content">
                <div class="column-auto">
                    <span class="intitule"><bean:message
                            key="SelectionCanevas.txt.numConsultation" /> </span>
                    <div class="content-bloc-long">
                            ${frmSelectionCanevas.numConsultation} : ${frmSelectionCanevas.intituleConsultation}
                    </div>
                </div>
                <div class="breaker"></div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.pouvoirAdj" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.pouvoirAdjudicateur}
                    </span>
                </div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.direcServ" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.directionService}
                    </span>
                </div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.procedurePassation" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.procedurePassation}
                    </span>
                </div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.naturePrestation" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.naturePrestation}
                    </span>
                </div>
                <div class="breaker"></div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.typeDocument" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.typeDocument}
                    </span>
                </div>
                <div class="column-moyen">
                    <span class="intitule"><bean:message key="SelectionCanevas.txt.lot" /></span>
                    <span class="content-bloc">
                            ${frmSelectionCanevas.lot}
                    </span>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom">
                <span class="left"></span><span class="right"></span>
            </div>
        </div>
        <div class="spacer"></div>
    </c:if>
    <!--Fin bloc recap Document-->
    <!--Debut partitioner-->

    <c:set var="urlTableauPagine" value="selectionCanevasListe.epm" />
    <c:if test="${!empty sessionScope.prefixeRedirection}">
        <c:set var="urlTableauPagine" value="${sessionScope.prefixeRedirection}SelectionCanevasListe.epm" />
    </c:if>

    <atexo:tableauPagine name="frmSelectionCanevas" property="collectionCanevas" url="${urlTableauPagine}" elementAfficher="10,20,30,40" id="canevasListe">
        <div class="spacer"></div>
        <div class="line">
            <h2>
                <%--@elvariable id="canevasListe" type="java.util.List<fr.paris.epm.redaction.presentation.bean.CanevasBean>"--%>
                <logic:notEmpty name="canevasListe">
                    <bean:message key="SelectionCanevas.txt.canevasDisponible" />
                    <logic:notEmpty name="canevasListe">
                        <c:out value="${requestScope.NB_RESULT}"/>
                    </logic:notEmpty>
                </logic:notEmpty>
                <logic:empty name="canevasListe">
                    <bean:message key="SelectionCanevas.txt.aucun.canevasDisponible" />
                </logic:empty>
            </h2>
            <div class="partitioner">
                <div class="intitule">
                    <bean:message key="redaction.txt.afficher" />
                </div>
                <atexo:intervalle />
                <div class="intitule2">
                    <bean:message key="redaction.txt.resultatParPage" />
                    &nbsp;&nbsp;
                    <atexo:navigationListe suivant="false" key="redaction.txt.precedent" />
                    &nbsp;
                    <atexo:compteurPage />
                    &nbsp;
                    <atexo:navigationListe suivant="true" key="redaction.txt.suivant" />
                </div>
            </div>
        </div>
        <!--Fin partitioner-->
        <!--Debut Resultats Liste des canevas-->

        <div class="results-list">
            <table id="resultsTable">
                <thead>
                <tr>
                    <th colspan="9" class="top"><img src="<atexo:href href='images/table-results-top-left.gif'/>" alt="" title="" class="left" /><img src="<atexo:href href='images/table-results-top-right.gif'/>" alt="" title="" class="right" /></th>
                </tr>
                <tr>
                    <atexo:href href="" var="hrefRacine">

                        <th class="col-80">
                            <div>
                                <bean:message key="rechercherCanevas.txt.reference" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="reference" /><br/>
                                <bean:message key="rechercherCanevas.txt.auteur" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="canevasEditeur" /><br/>
                                <bean:message key="rechercherCanevas.txt.Statut" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="epmTRefStatutRedactionClausier.id" />
                            </div>
                        </th>
                        <th class="theme">
                            <div>
                                <bean:message key="rechercherCanevas.txt.type" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="epmTRefTypeDocument.libelle" /><br/>
                                <bean:message key="rechercherCanevas.txt.titreCanevas" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="titre" /><br/>
                                <bean:message key="rechercherCanevas.txt.natPres" /><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="idNaturePrestation" />
                            </div>
                        </th>
                        <th class="contenu-long">
                            <div><bean:message key="rechercherCanevas.txt.procPass"/></div>
                            <div><bean:message key="RechercherClause.txt.typeContrat"/></div>
                        </th>
                        <th class="col-80">
                            <!-- TODO : Changer le critére de tri -->
                            <bean:message key="rechercherCanevas.txt.creeLe"/><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="dateCreation"/>
                            <br/>
                            <bean:message key="rechercherCanevas.txt.modifieLe"/><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="dateModification"/>
                            <br/>
                            <bean:message key="rechercherCanevas.txt.version"/><atexo:colonne img="${hrefRacine}images/arrow-tri.gif" property="version"/>
                            <br/>
                        </th>
                        <th class="actions align-left"><bean:message key="rechercherCanevas.txt.actions"/></th>
                    </atexo:href>
                </tr>
                </thead>

                <logic:notEmpty name="canevasListe">
                    <logic:iterate name="canevasListe" id="canevas" indexId="index" type="fr.paris.epm.redaction.presentation.bean.CanevasBean">
                        <%--@elvariable id="canevas" type="fr.paris.epm.redaction.presentation.bean.CanevasBean"--%>
                        <c:set var="style" />
                        <c:if test="${index % 2 == 0}">
                            <c:set var="style">on</c:set>
                        </c:if>
                        <c:if test="${!canevas.actif}">
                            <c:set var="style">off</c:set>
                        </c:if>
                        <tr class="${style}" id="inactiveClause">
                            <c:set var="checkboxActive">false</c:set>
                            <c:if test="${editeur != null && !editeur && canevas.canevasEditeur}">
                                <c:set var="checkboxActive">true</c:set>
                            </c:if>

                            <td class="col-80">
                                <div><c:out value="${canevas.referenceCanevas}" /></div>
                                <c:choose>
                                    <c:when test="${canevas.canevasEditeur == true}">
                                        <bean:message key="rechercherCanevas.txt.canevasEditeur"/>
                                    </c:when>
                                    <c:otherwise>
                                        <bean:message key="rechercherCanevas.txt.canevasClient"/>
                                    </c:otherwise>
                                </c:choose>
                                <div class="spacer-mini"></div>
                                <c:if test="${canevas.idStatutRedactionClausier == 1}">
                                    <div>
                                        <img title="Brouillon" alt="Brouillon" src="<atexo:href href='images/clause-statut-1.gif'/>" />
                                    </div>
                                </c:if>
                                <c:if test="${canevas.idStatutRedactionClausier == 2}">
                                    <div>
                                        <img title="A valider" alt="A valider" src="<atexo:href href='images/clause-statut-2.gif'/>" />
                                    </div>
                                </c:if>
                                <c:if test="${canevas.idStatutRedactionClausier == 3}">
                                    <div>
                                        <img title="Validée" alt="Validée" src="<atexo:href href='images/clause-statut-3.gif'/>" />
                                    </div>
                                </c:if>
                            </td>
                            <td class="theme">
                                <div>${canevas.labelTypeDocument}</div>
                                <div><redaction:insererEspace name="canevas" property="titre" tailleMot="30"/></div>
                                <div>${canevas.labelNaturePrestation}</div>
                            </td>
                            <td class="contenu-long">
                                <div>
                                    <c:choose>
                                        <c:when test="${not empty canevas.labelsProcedures}">
                                            <c:out value="${canevas.labelsProcedures}" />
                                        </c:when>
                                        <c:otherwise>
                                            <span title="Toutes procedures">
                                                <bean:message key="rechercherCanevas.txt.toutesProcedures" />
                                            </span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <br/>
                                <div>
                                    <c:choose>
                                        <c:when test="${not empty canevas.labelsTypeContrats}">
                                            <c:out value="${canevas.labelsTypeContrats}" />
                                        </c:when>
                                        <c:otherwise>
                                            <span title="Toutes types de contrat">
                                                <bean:message key="rechercherCanevas.txt.toutesTypesContrat" />
                                            </span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </td>
                            <td class="col-80">
                                <div>
                                    <c:choose>
                                        <c:when test="${canevas.dateCreation != null}">
                                            <fmt:formatDate pattern="dd/MM/yyyy" value="${canevas.dateCreation}" />
                                        </c:when>
                                        <c:otherwise>
                                            ND
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div>
                                    <c:choose>
                                        <c:when test="${canevas.dateModification != null}">
                                            <fmt:formatDate pattern="dd/MM/yyyy" value="${canevas.dateModification}" />
                                        </c:when>
                                        <c:otherwise>
                                            ND
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div>
                                    <c:choose>
                                        <c:when test="${canevas.lastVersion != null}">
                                            <c:out value="${canevas.lastVersion}" />
                                        </c:when>
                                        <c:otherwise>
                                            ND
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </td>

                            <td class="actions action-inline">
                                <c:set var="urlParameters" value="idCanevas=${canevas.idCanevas}" />
                                <c:if test="${not empty canevas.idPublication}">
                                    <c:set var="urlParameters" value="${urlParameters}&idPublication=${canevas.idPublication}" />
                                </c:if>
                                <c:set var="urlEditeur" value="" />
                                <c:if test="${editeur}">
                                    <c:set var="urlEditeur" value="Editeur" />
                                    <c:set var="urlParameters" value="${urlParameters}&editeur=yes" />
                                </c:if>
                                <c:if test="${not empty accesModeSaaS}"> <!-- REDAC -->
                                    <c:set var="urlParameters" value="${urlParameters}&accesModeSaaS=yes" />
                                </c:if>

                                <!-- Prévisualisation -->
                                <a href="javascript:popUp('previewCanevas.htm?${urlParameters}', 'yes');">
                                    <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="Pr&eacute;visualiser" title="Pr&eacute;visualiser" />
                                </a>
                                <!-- Sélectionner -->
                                <a href="createDocument.htm?${urlParameters}" class="bouton-action">
                                    <img src="<atexo:href href='images/icone-check.png'/>" onclick="showLoader()" alt="Sélectionner" title="Sélectionner ce canevas" class="icone" />
                                </a>
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
            </table>
        </div>

        <!--Fin Resultats Liste des canevas-->

        <logic:notEmpty name="canevasListe">
            <div class="line">
                <div class="partitioner">
                    &nbsp;&nbsp;
                    <atexo:navigationListe suivant="false" key="redaction.txt.precedent" />
                    &nbsp;
                    <atexo:compteurPage />
                    &nbsp;
                    <atexo:navigationListe suivant="true" key="redaction.txt.suivant" />
                </div>
            </div>
        </logic:notEmpty>
    </atexo:tableauPagine>
    <!--Debut boutons-->
    <div class="spacer"></div>
    <div class="boutons">
        <a href="${sessionScope.prefixeRedirection}CreationDocumentInit.epm?idCons=${idCons}"
           class="annuler"><bean:message key="redaction.txt.annuler" />
        </a>
    </div>
    <!--Fin boutons-->
</div>

<script>
    jQuery(document).ready(function () {
        showLoader();
    });

    jQuery("#navigationListePRECEDENT").click(function () {
        showLoader();
    });

    jQuery("#navigationListeSUIVANT").click(function () {
        showLoader();
    });

    jQuery("#nbResults").change(function () {
        showLoader();
    });

</script>
<!--Fin main-part-->
