<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main-part">
    <div class="table panel panel-default">
        <div class="panel-body">
            <div class="pull-left text-primary m-t-1">
                <spring:message code="publicationClausier.exportPublicationClausier.download"/>
            </div>
            <div class="actions text-right">
                <button type="button" data-toggle="modal" data-target="#modalDowload" class="btn bt-sm btn-primary"
                        onclick="pubSelectionOn()"
                        title="<spring:message code="publicationClausier.exportPublicationClausier.download.button" />">
                    <i class="fa fa-download"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<!--Fin main-part-->

<!--Debut modale-->
<div class="modal" id="modalDowload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <spring:message code="publicationClausier.exportPublicationClausier.download"/>
            </div>
            <div class="modal-body">
                <form:form action="downloadPublicationClausier.htm?idPublication=0" method="post"
                           enctype="multipart/form-data" cssClass="form-horizontal">
                    <div class="text-center">
                        <label class="text-center" id="msgModalOK">
                            <spring:message code="publicationClausier.exportPublicationClausier.modal.title"/>
                        </label>
                        <label class="text-center" id="msgModalKO">
                            <spring:message code="publicationClausier.exportPublicationClausier.modal.no.selection"/>
                        </label>
                        <input type="hidden" name="selectedPubIds" id="selectedPubIds" value="">
                    </div>

                    <div class="clearfix">
                        <button type="button" id="annuler1" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <spring:message code="redaction.action.annuler"/>
                        </button>

                        <button id="valider-export" type="submit" class="btn btn-primary btn-sm pull-right" onclick="onDoanload()">
                            <spring:message code="redaction.action.valider"/>
                        </button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

<script>
    function onDoanload() {
        $("#modalDowload").modal('hide');
    }

    function pubSelectionOn() {
        var arr = "";
        checkboxes = document.getElementsByName("publication-select-enabled");
        for (var i = 0; i < checkboxes.length; i++)  {
            if (checkboxes[i].checked){
                if (arr.length === 0)
                    arr = checkboxes[i].value;
                else
                    arr = arr + "," + checkboxes[i].value;
            }
            console.log("checkboxes[" + i + "].value : " + checkboxes[i].value + " ; .checked : " + checkboxes[i].checked);
        }
        $("#selectedPubIds").val(arr);
        console.log("arr : " + arr);

        if (arr.length === 0) {
            $("#valider-export").prop("disabled", true);
            $("msgModalOK").hide();
            $("msgModalKO").show();
        } else {
            $("#valider-export").prop("disabled", false);
            $("msgModalOK").show();
            $("msgModalKO").hide();
        }
    }

</script>