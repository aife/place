<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><bean:message key="previsualiserClause.txt.elaborationPassationMarche" /></title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
    <script type="text/javascript">hrefRacine = '<atexo:href href=""/>';</script>
    <script type="text/javascript"  language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript"  language="JavaScript" src="js/editeurTexteRedaction.js"></script>


    <script language="javascript">
        window.onload = function() {
            popupResize();
            initEditeursTexteSansToolbarRedaction();
        }
    </script>

</head>

<body id="layoutPopup" >
<div id="container" class="maj-clause">
    <h1>Historique des versions de la clause ${clause.reference}</h1>

    <div class="previsu-doc previsu-doc client bloc-comparaison-version">
        <div class="content">
            <h2>Version</h2>

            <html:form action="historiquePublicationClausierAction">
                <html:select styleId="selectionVersionClause" property="version" styleClass="selection-version" onchange="javascript:changerVersion();">
                    <html:optionsCollection property="versions" label="version" value="id"/>
                </html:select>
                <html:hidden property="idClauseCanevasEditeur"/>
            </html:form>

            <ul>
                <li>
                    <ul class="liste-clause">
                        <logic:empty name="versionClause">
                            <center>
                                <bean:message key="clause.historiquePublication.nonTrouvee" />
                            </center>
                        </logic:empty>
                        <logic:notEmpty name="versionClause">
                            <ul>
                                <li>
                                    <bean:define id="typeClause" name="clause" property="epmTRefTypeClause" />
                                    <c:set scope="request" var="type">
                                        <nested:write name="versionClause" property="epmTRefTypeClause.id" />
                                    </c:set>

                                    <c:if test="${requestScope.type >= 2 && requestScope.type <= 5 || requestScope.type == 8 || requestScope.type == 9}">
                                        <c:set var="classClause" value="clause" />
                                    </c:if>
                                    <c:if test="${requestScope.type == 6 || requestScope.type == 7}">
                                        <c:set var="classClause" value="line" />
                                    </c:if>

                                    <div class="${classClause}">
                                        ${versionClause.texteFixeAvant}
                                        <c:if test="${versionClause.sautLigneTexteAvant == true}"><br/></c:if>
                                        <redaction:previsualiserPptExtClause name="versionClause" user="utilisateur" />
                                        <c:if test="${versionClause.sautLigneTexteApres == true}"><br/></c:if>
                                        ${versionClause.texteFixeApres}
                                    </div>
                                </li>
                            </ul>
                        </logic:notEmpty>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="previsu-doc previsu-doc editeur bloc-comparaison-version">
        <div class="content">
            <h2>VERSION ${clause.dernierePublicationClausier.version}</h2>
            <ul>
                <li>
                    <ul class="liste-clause">
                        <logic:empty name="clause">
                            <center>
                                <bean:message key="clause.historiquePublication.nonTrouvee" />
                            </center>
                        </logic:empty>
                        <logic:notEmpty name="clause">
                            <ul>
                                <li>
                                    <bean:define id="typeClause" name="clause" property="epmTRefTypeClause" />
                                    <c:set scope="request" var="type">
                                        <nested:write name="clause" property="epmTRefTypeClause.id" />
                                    </c:set>

                                    <c:if test="${requestScope.type >= 2 && requestScope.type <= 5 || requestScope.type == 8 || requestScope.type == 9}">
                                        <c:set var="classClause" value="clause" />
                                    </c:if>
                                    <c:if test="${requestScope.type == 6 || requestScope.type == 7}">
                                        <c:set var="classClause" value="line" />
                                    </c:if>

                                    <div class="${classClause}">
                                        ${clause.texteFixeAvant}
                                        <c:if test="${clause.sautLigneTexteAvant == true}"><br/></c:if>
                                        <redaction:previsualiserPptExtClause name="clause" user="utilisateur"/>
                                        <c:if test="${clause.sautLigneTexteApres == true}"><br/></c:if>
                                        ${clause.texteFixeApres}
                                    </div>
                                </li>
                            </ul>
                        </logic:notEmpty>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="spacer"></div>
    <div class="boutons">
        <a href="javascript:window.close();" class="valider"> Fermer </a>
    </div>
</div>

<script type="text/javascript">
    new Draggable('container', {
        revert : false,
        ghosting : false
    });
    autoPositionLayer();

    function changerVersion() {
        document.forms[0].submit();
    }
</script>

</body>
</html>
