package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClausePub;
import fr.paris.epm.redaction.importExport.bean.RoleClauseImportExport;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class RoleClausePubMapper {

    //@Mapping(source = "preference.id", target = "idPreference")
    public abstract RoleClauseImportExport toRoleClauseImportExport(EpmTRoleClausePub epmTRoleClausePub);

    //@Mapping(source = "idPreference", target = "preference.id", ignore = true)
    public abstract EpmTRoleClausePub toEpmTRoleClausePub(RoleClauseImportExport roleClauseImportExport, @Context Integer idPublication);

    @AfterMapping
    protected EpmTRoleClausePub setIdPublication(@MappingTarget EpmTRoleClausePub epmTRoleClausePub, @Context Integer idPublication) {
        epmTRoleClausePub.setIdPublication(idPublication);
        return epmTRoleClausePub;
    }

    protected Set<? extends EpmTRoleClauseAbstract> toEpmTRoleClauseAbstracts(Set<RoleClauseImportExport> roleClauseImportExports, @Context Integer idPublication) {
        if (roleClauseImportExports == null)
            return null;

        Set<EpmTRoleClauseAbstract> epmTRoleClauseAbstracts = new HashSet<>();
        for (RoleClauseImportExport clausePotentiellementConditionneeImportExport : roleClauseImportExports)
            epmTRoleClauseAbstracts.add(toEpmTRoleClausePub(clausePotentiellementConditionneeImportExport, idPublication));

        return epmTRoleClauseAbstracts;
    }

}
