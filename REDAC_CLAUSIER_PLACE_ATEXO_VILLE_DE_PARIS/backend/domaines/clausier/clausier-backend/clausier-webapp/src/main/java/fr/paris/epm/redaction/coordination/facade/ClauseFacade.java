package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.facade.Facade;
import fr.paris.epm.noyau.persistance.redaction.EpmTClause;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.presentation.bean.ClauseBean;
import fr.paris.epm.redaction.presentation.bean.ClauseSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;

/**
 * Facade de gestion des Clause Niveau Interministeriel
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public interface ClauseFacade extends Facade<ClauseBean>, ClauseFacadeCommun {

    EpmTClause findEntityById(int idClaude);

    PageRepresentation<ClauseBean> findClauses(ClauseSearch search, EpmTRefOrganisme epmTRefOrganisme);

    ClauseBean map(EpmTClause epmTObject, Integer idOrganisme);

}
