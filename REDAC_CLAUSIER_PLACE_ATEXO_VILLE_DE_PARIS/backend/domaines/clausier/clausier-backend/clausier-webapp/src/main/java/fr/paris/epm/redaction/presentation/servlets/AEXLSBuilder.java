package fr.paris.epm.redaction.presentation.servlets;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.doc.AbstractXLSBuilder;
import fr.paris.epm.noyau.persistance.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class AEXLSBuilder extends AbstractXLSBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(AEXLSBuilder.class);
    
    /**
     * Acces aux fichiers de libelle (injection Spring indirecte).
     */
    private ResourceBundleMessageSource messageSource;

    /**
     * Position objet de la consultation page 1.
     */
    private static final int[] COORD_OBJET_CONSULTATION = { 2, 1 };
    
    /**
     * Position objet de la consultation page 2.
     */
    private static final int[] COORD_OBJET_CONSULTATION_PAGE_2 = { 2, 2 };
    
    /**
     * Position objet de la consultation page 3.
     */
    private static final int[] COORD_OBJET_CONSULTATION_PAGE_3 = { 2, 1 };

    /**
     * Indique si il y a des lots techniques. Conditionne la génération du
     * document.
     */
    private boolean lotTechniquePresent = false;

    /**
     * Premiére ligne du tableau des tranches.
     */
    private static final int DEBUT_TRANCHE_ONGLET_1 = 8;
    
    /**
     * Nombre de colonne du tableau tranche.
     */
    private static final short NOMBRE_COLONNE_TABLEAU_TRANCHE = 7;
    
    /**
     * Nombre des ligne entre 2 tableaux lot technique.
     */
    private static final short NOMBRE_LIGNE_ENTRE_TABLEAU_LOT_TECHQNIQUE = 4;
    
    /**
     * Numero de ligne du premier lot technique.
     */
    private static final short NUMERO_LIGNE_PREMIER_TABLEAU_LOT_TECHQNIQUE = 7;
    
    /**
     * Nombre de cellule du tableau lot technique.
     */
    private static final int NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE = 6;
    
    /**
     * Numero de la premiére ligne du tableau rabais.
     */
    private static final int NUMERO_PREMIERE_LIGNE_RABAIS = 7;
    
    /**
     * Nombre de cellule du tableau de rabais. 
     */
    private static final int NOMBRE_CELLULE_TABLEAU_RABAIS = 2;
    
    /**
     * id du lot pour lequel on génère le document.
     */
    private int idLot;
    private EpmTConsultation eConsult;

    /**
     * Constructeur.
     * @param fis le flux d'entrée du document template
     */
    public AEXLSBuilder(final FileInputStream fis) {
        setFluxModel(fis);
    }

    /**
     * Méthode testant la présence des objets nécessaires à la génération.
     * @throws TechnicalException exception technique
     */
    public final void verification() {
        if (getEConsult() == null)
            throw new TechnicalException(
                "Objets manquants pour la génération");
    }

    /**
     * Cette méthode renvoie une cellule à partir de ses coordonnées La cellule
     * est créée si elle n'existe pas .
     * @param page page Excel de travail
     * @param indligne indice ligne
     * @param indcolonne indice colonne
     * @return une cellule
     */
    public final HSSFCell creerCellule(final HSSFSheet page,
            final int indligne, final int indcolonne) {
        HSSFRow ligne = page.getRow(indligne);
        HSSFCell colonne = null;
        if (ligne == null) {
            ligne = page.createRow((short) indligne);
        }

        colonne = ligne.getCell((short) indcolonne);
        if (colonne == null) {
            colonne = ligne.createCell((short) indcolonne);
            colonne.setCellType(HSSFCell.CELL_TYPE_BLANK);
        }
        return colonne;
    }

    /**
     * Méthode réalisant la génération effective du document à partir d'un
     * template.
     * @throws TechnicalException erreur technique
     */
    public final void build(String path) {
        dossierTmp = path;
        try {
            POIFSFileSystem poifs = new POIFSFileSystem(getFluxModel());
            setWb(new HSSFWorkbook(poifs));
            HSSFSheet page = getWb().getSheetAt(0);

            EpmTBudLotOuConsultation epmtloc = null;
            creerCellule(page, COORD_OBJET_CONSULTATION[0],
                COORD_OBJET_CONSULTATION[1]).setCellValue(
                eConsult.getObjet());
            verification();
            

            // si id lot de la consultation = -1, non allotie

            // on récupère la structure de propriétés du lot ou de la
            // consultation

            if (getIdLot() == -1) {
                // non allotie
                epmtloc = eConsult.getEpmTBudLotOuConsultation();
            } else {
                // sinon rechercher l'objet EpmtBudLot (juridique) qui va bien

                for (Iterator iter = eConsult.getEpmTBudLots().iterator(); iter
                        .hasNext();) {
                    EpmTBudLot unLot = (EpmTBudLot) iter.next();
                    if (unLot.getId() == getIdLot()) {
                        epmtloc = unLot.getEpmTBudLotOuConsultation();
                        break;
                    }
                }
            }

            List lTranches = genererTranche(epmtloc);
            
            remplirTableauTranche(lTranches, page);
            
            Map association = genererLotsTechniques(epmtloc);
            page = getWb().getSheetAt(1);
            creerCellule(page, COORD_OBJET_CONSULTATION_PAGE_2[0],
                COORD_OBJET_CONSULTATION_PAGE_2[1]).setCellValue(
                eConsult.getObjet());
            int nombreLigneCrees = 0;
            int premiereLigne = NUMERO_LIGNE_PREMIER_TABLEAU_LOT_TECHQNIQUE;
            if (!association.isEmpty()) {
                for (int i = 0; i < lTranches.size(); i++) {
                    EpmTBudTranche tranche = (EpmTBudTranche) lTranches
                            .get(i);
                    List listeLotsTechniques = (List) association
                            .get(tranche);
                    if (listeLotsTechniques != null) {
                        Collections.sort(listeLotsTechniques);
                        nombreLigneCrees = creerTableauAssociationLotTechniquesTranches(
                            page, premiereLigne, listeLotsTechniques,
                            tranche);
                        premiereLigne = nombreLigneCrees
                                + listeLotsTechniques.size()
                                + NOMBRE_LIGNE_ENTRE_TABLEAU_LOT_TECHQNIQUE;
                    }
                }
            }

            page = getWb().getSheetAt(2);
            creerCellule(page, COORD_OBJET_CONSULTATION_PAGE_3[0],
                COORD_OBJET_CONSULTATION_PAGE_3[1]).setCellValue(
                eConsult.getObjet());
            premiereLigne = NUMERO_PREMIERE_LIGNE_RABAIS;
            for (int i = 0; i < lTranches.size(); i++) {
                EpmTBudTranche tranche = (EpmTBudTranche) lTranches.get(i);
                premiereLigne = remplirRabais(page, tranche, premiereLigne, i+1);
                premiereLigne++;
            }

        } catch (IOException e) {
            LOG.error("impossible de générer le fichier", e);
        }
    }
    
    /**
     * Parcours le lotOuConsultation et retourne la liste des tranches triées
     * TF en premier, puis les TC classées par numéro.
     * @param epmtloc {@link EpmTBudLotOuConsultation}
     * @return {@link List}
     */
    private List genererTranche(final EpmTBudLotOuConsultation epmtloc) {
        List lTranches = new ArrayList();

        Set tranchesVrac = new HashSet(0);

        // recopie préalable de la liste des tranches
        if (epmtloc != null) {
            for (Iterator itTranche = epmtloc.getEpmTBudTranches()
                    .iterator(); itTranche.hasNext();) {
                tranchesVrac.add(itTranche.next());
            }
        }
        // tri intermédiaire
        // la TF en premier
        if (tranchesVrac != null) {
            for (Iterator tIter = tranchesVrac.iterator(); tIter.hasNext();) {
                EpmTBudTranche uneTranche = (EpmTBudTranche) tIter.next();
                if (uneTranche.getEpmTRefNatureTranche().getId() == 1) {
                    lTranches.add(uneTranche);
                    tranchesVrac.remove(uneTranche);
                    break;
                }
            }

            // les TC selon le code

            while (!tranchesVrac.isEmpty()) {

                Iterator tIter = tranchesVrac.iterator();
                EpmTBudTranche trancheTemp = (EpmTBudTranche) tIter.next();

                while (tIter.hasNext()) {
                    EpmTBudTranche trancheTempB = (EpmTBudTranche) tIter.next();
                    if (trancheTemp.getCodeTranche().compareTo(
                        trancheTempB.getCodeTranche()) > 0) {
                        trancheTemp = trancheTempB;
                    }
                }
                lTranches.add(trancheTemp);
                tranchesVrac.remove(trancheTemp);
            }
        }
        return lTranches;
    }

    /**
     * Affiche une liste des tranches dans un tableau de l'onglet 1.
     * @param listeTranche la liste des tranches
     * @param page la page du document
     */
    private void remplirTableauTranche(List listeTranche, HSSFSheet page) {
        if (listeTranche != null) {
            int ligneEnCours = DEBUT_TRANCHE_ONGLET_1;
            short colonne = 0;
            for (Iterator tIter = listeTranche.iterator(); tIter.hasNext();) {
                EpmTBudTranche uneTranche = (EpmTBudTranche) tIter.next();
                HSSFRow row = creerLigne(ligneEnCours, NOMBRE_COLONNE_TABLEAU_TRANCHE, colonne, page);
                String typeDeTranche = "";
                if (uneTranche.getEpmTRefNatureTranche().getId() == 1) {
                    typeDeTranche = messageSource.getMessage("xls.ae.titre.intituleTf", null, Locale.FRENCH);
                } else {
                    typeDeTranche = messageSource.getMessage("xls.ae.titre.intituleTc", null, Locale.FRENCH)
                        +" ("+ messageSource.getMessage("xls.ae.titre.tc", null, Locale.FRENCH)
                        +uneTranche.getCodeTranche()+")";
                }
                row.getCell(colonne).setCellValue(typeDeTranche);
                appliquerStyleALigneTableauTranche(row, true, false);
                ligneEnCours++;
                row = creerLigne(ligneEnCours, NOMBRE_COLONNE_TABLEAU_TRANCHE, colonne, page);
                row.getCell((short) (colonne)).setCellValue(uneTranche.getIntituleTranche());
                appliquerStyleALigneTableauTranche(row, false, true);
                ligneEnCours++;
            }
            HSSFRow rowTotal = creerLigne(ligneEnCours, NOMBRE_COLONNE_TABLEAU_TRANCHE, colonne, page);
            rowTotal.getCell((short)0).setCellValue(messageSource.getMessage("xls.ae.libelle.total", null, Locale.FRENCH));
            appliquerStyleALigneTableauTranche(rowTotal, true, true);
            appliquerStyleGras(rowTotal.getCell((short)0));
        }
    }
    
    /**
     * Création d'une ligne
     * @param ligne le numéro de la ligne
     * @param nombreCellule le nombre de cellule de la ligne
     * @param celluleDebut le numéro de la premiére cellule
     * @param page la page du document
     * @return la ligne crée
     */
    private HSSFRow creerLigne(final int ligne, final int nombreCellule, final int celluleDebut, HSSFSheet page) {
        HSSFRow row = page.createRow(ligne);
        for (int i = celluleDebut ; i < nombreCellule ; i++) {
            row.createCell((short) i);
        }
        return row;
    }
    
    /**
     * Crée l'encadrement d'une ligne.
     * @param row la ligne courante.
     * @param premiereLigne true pour afficher la bordure haute
     * @param secondeLigne true pour afficher la bordure basse
     */
    private void appliquerStyleALigneTableauTranche(HSSFRow row, final boolean premiereLigne, final boolean secondeLigne) {
        short premiereCellule = row.getFirstCellNum();
        short derniereCellule = row.getLastCellNum();
        if (premiereCellule != -1) {
            for (short i = premiereCellule; i <= derniereCellule ; i++) {
                HSSFCell cell = row.getCell(i);
                if (cell != null) {
                    cell.setCellStyle(getStyleTableauTranches(premiereLigne, secondeLigne));
                }
            }
        }
    }
    
    
    /**
     * Crée l'encradrement d'une cellule
     * @param premiereLigne true pour afficher la bordure haute
     * @param secondeLigne true pour afficher la bordure basse
     * @return le style modifiée 
     */
    private HSSFCellStyle getStyleTableauTranches(final boolean premiereLigne, final boolean secondeLigne) {
        HSSFCellStyle styleDonne = null;
        styleDonne = getWb().createCellStyle();
        if (premiereLigne) {
            styleDonne.setBorderTop(HSSFCellStyle.BORDER_THIN);
        }
        if (secondeLigne) {
            styleDonne.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        }
        styleDonne.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        styleDonne.setBorderRight(HSSFCellStyle.BORDER_THIN);

        return styleDonne;
    }

    /**
     * Parcours les lots techniques pour faire les associations tranches / lots
     * techniques
     * @param epmtloc lot ou consultation
     * @return map d'association tranche / lots techniques
     */
    private Map genererLotsTechniques(final EpmTBudLotOuConsultation epmtloc) {
      Map associationTranchesLots = new HashMap();
      // recopie préalable de la liste des lots
      if (epmtloc != null) {
          if (epmtloc.getEpmTLotTechniques() != null && !epmtloc.getEpmTLotTechniques().isEmpty()) {
              lotTechniquePresent = true;
              for (Iterator itLot = epmtloc.getEpmTLotTechniques()
                      .iterator(); itLot.hasNext();) {
                  EpmTLotTechnique lotTechnique = (EpmTLotTechnique)itLot.next();
                  if (lotTechnique.getEpmTBudTranches() != null) {
                      Iterator itTranche = lotTechnique.getEpmTBudTranches().iterator();
                      while (itTranche.hasNext()) {
                          EpmTBudTranche tranche = (EpmTBudTranche)itTranche.next();
                          List listeLotTechnique = null;
                          if (!associationTranchesLots.containsKey(tranche)) {
                              listeLotTechnique = new ArrayList();
                              associationTranchesLots.put(tranche, listeLotTechnique);
                          } else {
                              listeLotTechnique = (List)associationTranchesLots.get(tranche);
                              
                          }
                          listeLotTechnique.add(lotTechnique);
                      }
                  }
              }
          }
      }
      
      return associationTranchesLots;
      
    }
    
    /**
     * Création des lignes des tableaux de lots techniques page 2
     * @param listeLotsTechniques la liste des lots techniques
     * @param page la page courante
     * @param premiereLigne la premiére ligne d'insertion
     * @return le nombre de ligne créée
     */
    private int remplirTableauLotsTechniques(
            final List listeLotsTechniques,
            HSSFSheet page, final int premiereLigne) {
        int nombreLigne = premiereLigne;
        HSSFRow row = null;
        for (int i = 0 ; i < listeLotsTechniques.size() ; i++) {
            EpmTLotTechnique lotTechnique = (EpmTLotTechnique)listeLotsTechniques.get(i);
            row  = creerLigne(nombreLigne, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
            row.getCell((short)0).setCellValue(lotTechnique.getNumeroLot());
            row.getCell((short)1).setCellValue(lotTechnique.getIntituleLot());
            appliquerStyleALigneTableauTranche(row, true, true);
            nombreLigne++;
        }
        
        return nombreLigne;
    }
    
    /**
     * 
     * Crée un tableau vide par défaut.
     * @param page la page
     * @param ligneDebut la premiére ligne d'insertion
     * @param listeLotsTechniques la liste des lots techniques
     * @param tranche la tranche courante 
     * @return la derniére ligne inserée
     */
    private int creerTableauAssociationLotTechniquesTranches(
            HSSFSheet page, final int ligneDebut,
            final List listeLotsTechniques, final EpmTBudTranche tranche) {
        int ligneCourrante = ligneDebut;
        //creation de la ligne de titre du tableau
        String titreTranche = "";
        HSSFRow rowTitreTranche = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
        if (tranche.getEpmTRefNatureTranche().getId() == 1) {
            titreTranche = messageSource.getMessage("xls.ae.titre.titreTrancheFerme", null, Locale.FRENCH);
        } else {
            titreTranche = messageSource.getMessage("xls.ae.titre.titreTrancheConditionnelle", null, Locale.FRENCH);
            titreTranche = titreTranche
                    + " "
                    + tranche.getCodeTranche()
                    + " "
                    + messageSource.getMessage("xls.ae.titre.rabais", null, Locale.FRENCH);
        }
        rowTitreTranche.getCell((short)0).setCellValue(titreTranche);
        rowTitreTranche.getCell((short)0).setCellStyle(getWb().createCellStyle());
        appliquerStyleGras(rowTitreTranche.getCell((short)0));
        ligneCourrante++;
        
        //creation de la ligne d'intitulé de la tranche
        HSSFRow rowLibelleTranche = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
        rowLibelleTranche.getCell((short)0).setCellValue(tranche.getIntituleTranche());
        rowLibelleTranche.getCell((short)0).setCellStyle(getWb().createCellStyle());
        appliquerStyleItalique(rowLibelleTranche.getCell((short)0));
        ligneCourrante++;
        
        //création de la premiére ligne de titre de colonne du tableau
        HSSFRow rowTitre = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
        
        rowTitre.getCell((short) 0).setCellValue(messageSource.getMessage("xls.ae.titre.lot", null, Locale.FRENCH));
        rowTitre.getCell((short) 1).setCellValue(messageSource.getMessage("xls.ae.titre.corpEtat", null, Locale.FRENCH));
        rowTitre.getCell((short) 2).setCellValue(messageSource.getMessage("xls.ae.titre.excecutant", null, Locale.FRENCH));
        rowTitre.getCell((short) 3).setCellValue(messageSource.getMessage("xls.ae.titre.montant", null, Locale.FRENCH));
        
        appliquerStyleALigneTableauTranche(rowTitre, true, true);
        appliquerStyleGras(rowTitre.getCell((short) 0));
        appliquerStyleCentre(rowTitre.getCell((short) 0));
        appliquerStyleVerticalCentre(rowTitre.getCell((short) 0));
        appliquerStyleGras(rowTitre.getCell((short) 1));
        appliquerStyleCentre(rowTitre.getCell((short) 1));
        appliquerStyleVerticalCentre(rowTitre.getCell((short) 1));
        appliquerStyleGras(rowTitre.getCell((short) 2));
        appliquerStyleCentre(rowTitre.getCell((short) 2));
        appliquerStyleGras(rowTitre.getCell((short) 3));
        appliquerStyleCentre(rowTitre.getCell((short) 3));
        //fusion des cellules 3 à 5
        Region reg = new Region(ligneCourrante, (short) 3,
            ligneCourrante, (short) 5);
        page.addMergedRegion(reg);
        
        //création de la seconde ligne de titre de colonne du tableau
        ligneCourrante++;
        HSSFRow rowTitre2 = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
        //fusion de l'executant des lots.
        reg = new Region(ligneCourrante-1, (short) 0,
            ligneCourrante, (short) 0);
        page.addMergedRegion(reg);
        reg = new Region(ligneCourrante-1, (short) 1,
            ligneCourrante, (short) 1);
        page.addMergedRegion(reg);
        page.addMergedRegion(reg);
        reg = new Region(ligneCourrante-1, (short) 2,
            ligneCourrante, (short) 2);
        page.addMergedRegion(reg);
        rowTitre2.getCell((short) 3).setCellValue(messageSource.getMessage("xls.ae.titre.montantHT", null, Locale.FRENCH));
        rowTitre2.getCell((short) 4).setCellValue(messageSource.getMessage("xls.ae.titre.tva", null, Locale.FRENCH));
        rowTitre2.getCell((short) 5).setCellValue(messageSource.getMessage("xls.ae.titre.ttc", null, Locale.FRENCH));
        appliquerStyleALigneTableauTranche(rowTitre2, true, true);
        appliquerStyleCentre(rowTitre2.getCell((short) 3));
        appliquerStyleCentre(rowTitre2.getCell((short) 4));
        appliquerStyleCentre(rowTitre2.getCell((short) 5));
        ligneCourrante++;
        ligneCourrante = remplirTableauLotsTechniques(listeLotsTechniques, page, ligneCourrante);
        
        //création de la ligne total
        HSSFRow rowTotal = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 1, page);
        appliquerStyleALigneTableauTranche(rowTotal, true, true);
        String total = "";
        if (tranche.getEpmTRefNatureTranche().getId() == 1) {
            total = messageSource.getMessage("xls.ae.libelle.totalTrancheFerme", null, Locale.FRENCH);
        } else {
            total = messageSource.getMessage("xls.ae.libelle.totalTrancheConditionnelle", null, Locale.FRENCH)
                    + " " + tranche.getCodeTranche();
        }
        rowTotal.getCell((short)1).setCellValue(total);
        appliquerStyleGrasItalique(rowTotal.getCell((short)1));
        appliquerStyleAlignementDroite(rowTotal.getCell((short)1));
        ligneCourrante++;
        
        //saut de ligne
        creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 0, page);
        ligneCourrante++;
        //tableau montant en toute lettre
        HSSFRow rowMontant = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 1, page);
        ligneCourrante++;
        HSSFRow rowMontant2 = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_LOT_TECHQNIQUE, 1, page);
        appliquerStyleALigneTableauTranche(rowMontant, true, true);
        appliquerStyleALigneTableauTranche(rowMontant2, true, true);
        rowMontant.getCell((short) 1).setCellValue(messageSource.getMessage("xls.ae.titre.montantLettre", null, Locale.FRENCH));
        appliquerStyleItalique(rowMontant.getCell((short) 1));
        appliquerStyleVerticalHaut(rowMontant.getCell((short) 1));
        reg = new Region(ligneCourrante-1, (short) 1,
            ligneCourrante, (short) 5);
        page.addMergedRegion(reg);
        
        return ligneCourrante;
    }

    
    /**
     * Applique le style italique
     * @param cell cellule courante
     * @return le style modifié
     */
    private HSSFCellStyle appliquerStyleItalique(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        HSSFFont font = getWb().createFont();
        font.setItalic(true);
        styleDonne.setFont(font);
        cell.setCellStyle(styleDonne);
        return styleDonne;
    }
    
    /**
     * Applique le style centrage vertical haut à une cellule
     * @param cell la cellule
     * @return le style modifié
     */
    private HSSFCellStyle appliquerStyleVerticalHaut(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        styleDonne.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
        return styleDonne;
    }
    
    /**
     * Applique le style centrage vertical centré
     * @param cell la cellule
     * @return le style modifié
     */
    private HSSFCellStyle appliquerStyleVerticalCentre(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        styleDonne.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        return styleDonne;
    }
    
    /**
     * Applique le style centrée
     * @param cell la cellule
     * @return le style modifié
     */
    private HSSFCellStyle appliquerStyleCentre(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        styleDonne.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        return styleDonne;
    }
    
    /**
     * Alignement à droite du texte
     * @param cell la cellule
     * @return le style modifié
     */
    private HSSFCellStyle appliquerStyleAlignementDroite(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        styleDonne.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        return styleDonne;
    }
    
    /**
     * Texte mis en gras
     * @param cell la cellule
     */
    private void appliquerStyleGras(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        HSSFFont font = getWb().createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleDonne.setFont(font);
        cell.setCellStyle(styleDonne);
    }
    
    /**
     * Texte mis en gras et italique
     * @param cell la cellule
     */
    private void appliquerStyleGrasItalique(HSSFCell cell) {
        HSSFCellStyle styleDonne = cell.getCellStyle();
        HSSFFont font = getWb().createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(true);
        styleDonne.setFont(font);
        cell.setCellStyle(styleDonne);
    }
    
    /**
     * Insertion des données des tranches dans le tableau de rabais.
     * @param page page courante
     * @param tranche tranche courante 
     * @param premiereLigne premiére ligne d'insertion des données
     * @param index index de la tranche à partir du numero 1
     * @return le nombre de ligne modifiées
     */
    private int remplirRabais(HSSFSheet page, EpmTBudTranche tranche, final int premiereLigne, final int index) {
        int ligneCourrante = premiereLigne;
        HSSFRow rowPremiereLigneRabais = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_RABAIS, 0, page);
        String textePremiereLigne = "";
        if (tranche.getEpmTRefNatureTranche().getId() == 1) {
            textePremiereLigne = messageSource.getMessage("xls.ae.titre.intituleTf", null, Locale.FRENCH)
            +" "+messageSource.getMessage("xls.ae.libelle.rabais", null, Locale.FRENCH)+index;
        } else {
            textePremiereLigne = messageSource.getMessage("xls.ae.titre.intituleTc", null, Locale.FRENCH) 
            +"("+ messageSource.getMessage("xls.ae.titre.tc", null, Locale.FRENCH)
            +" - "+tranche.getCodeTranche()+") "
            + messageSource.getMessage("xls.ae.libelle.rabais", null, Locale.FRENCH)+index;
        }
        rowPremiereLigneRabais.getCell((short)0).setCellValue(textePremiereLigne);
        ligneCourrante++;
        HSSFRow rowsecondeLigneRabais = creerLigne(ligneCourrante, NOMBRE_CELLULE_TABLEAU_RABAIS, 0, page);
        rowsecondeLigneRabais.getCell((short)0).setCellValue(tranche.getIntituleTranche());
        appliquerStyleALigneTableauTranche(rowPremiereLigneRabais, true, false);
        appliquerStyleALigneTableauTranche(rowsecondeLigneRabais, false, true);
        return ligneCourrante;
    }

    public boolean lotTechniquePresent() {
        return lotTechniquePresent;
    }

    public EpmTConsultation getEConsult() {
        return eConsult;
    }

    public final void setEConsult(final EpmTConsultation valeur) {
        eConsult = valeur;
    }

    public final int getIdLot() {
        return idLot;
    }

    public final void setIdLot(final int valeur) {
        this.idLot = valeur;
    }    
    
    /**
     * @param valeur Acces aux fichiers de libelle (injection Spring).
     */
    public final void setMessageSource(final ResourceBundleMessageSource valeur) {
        this.messageSource = valeur;
    }
}
