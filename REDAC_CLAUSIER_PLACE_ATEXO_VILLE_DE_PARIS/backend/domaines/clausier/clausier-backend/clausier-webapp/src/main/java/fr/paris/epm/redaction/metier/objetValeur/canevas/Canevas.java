package fr.paris.epm.redaction.metier.objetValeur.canevas;


import fr.paris.epm.redaction.util.Constantes;

import java.util.List;

/**
 * Classe Canevas qui implemente l'interface CanevasChapitre.
 *
 * @author Guillaume Béraudo.
 * @version $Revision$, $Date$, $Author$
 */

public class Canevas extends AbstractCanevasChapitre {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant du canevas.
     */
    private int idCanevas;

    /**
     * Identifiant du canevas.
     */
    private Integer idPublication;

    /**
     * l'attribut version.
     */
    private String lastVersion;

    /**
     * Référence du canevas.
     */
    private String referenceCanevas = ""; // pour permettre le déboggage

    /**
     * le titre du canevas.
     */
    private String titre;

    /**
     * L'ensemble des procedures liees au canevas.
     */
    private List<Integer> procedurePassationList;

    private List<Integer> typeContratList;

    /**
     * l'attribut type du document.
     */
    private int typeDocument;

    /**
     * l'attribut nature des prestation.
     */
    private int naturePrestation;

    /**
     * l'attribut statut.
     */
    private boolean statut;

    /**
     * l'attribut auteur.
     */
    private String auteur;

    /**
     * Message à afficher au moment du chargement du canevas.
     */
    private String message;

    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private boolean compatibleEntiteAdjudicatrice = true;

    /**
     * L'identifiant de l'organisme auquel la consultation est liée
     */
    private Integer idOrganisme;

    /**
     * Détermine si la clause est de type éditeur ou client
     */
    private boolean canevasEditeur;

    /**
     * Détermine si l'on est dans le cas de canevas Editeur ou Clients
     */
    private Boolean editeur;

    /**
     * Identifiant du statut de rédaction du canevas (Brouillon, A valider, Validé)
     */
    private Integer idStatutRedactionClausier;

    /**
     * Date de première publication
     */
    private String creeLe;

    /**
     * Date de dernière publication
     */
    private String modifieLe;

    /**
     * reference du canevas initial qui a servi pour la duplication
     */
    private String referenceInitialDuplication;

    /**
     * référentiel CCAG saisi lors de la création du canevas
     */
    private Integer idRefCCAG;

    /**
     * Déterminer si on peut gérer les dérogations sur ce canevas. C'est le cas
     * pour le type CCAP, CCAP-AE, CCP et CCP-AE.
     */
    private boolean derogationActive;

    /**
     * True si la valeur précédente du champ 'Type de document' pour lequel
     * l'option Dérogation CCAG est activée(CCAP, CCAP-AE, CCP ou CCP-AE) ET si
     * la nouvelle valeur est différente de CCAP, CCAP-AE, CCP ou CCP-AE
     * (c'est-à-dire la dérogation devient inactif pour ce Canevas)
     */
    private boolean derogationDevenirInactif;

    /*
     * renvoie l'id du type de document du cote de passation , cet ID est recupere à partir du TypeDocument dans EpmTCanevas
     *
     * */
    // TODO: possible que c'est inutil après le merge de TypeContrat
    private Integer idTypePassation;

    /**
     * @return la vaeur du hashcode du bean courant.
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final int hashCode() {
        int result = 1;
        if (idPublication != null)
            result = Constantes.PREMIER * result + idPublication.hashCode();
        if (chapitres != null)
            result = Constantes.PREMIER * result + chapitres.hashCode();
        if (referenceCanevas != null)
            result = Constantes.PREMIER * result + referenceCanevas.hashCode();
        return result;
    }

    /**
     * cette méthode est utilisée pour tester l'égalité entre les objets.
     *
     * @param obj est l'objet à comparer.
     * @return renvoyer vrai si les objets sont égaux si non renvoyer faux
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Canevas))
            return false;
        final Canevas autre = (Canevas) obj;
        if (chapitres == null) {
            if (autre.chapitres != null)
                return false;
        } else if (!chapitres.equals(autre.chapitres)) {
            return false;
        }

        if (idCanevas != autre.idCanevas)
            return false;

        if (idPublication != null) {
            if (!idPublication.equals(autre.idPublication))
                return false;
        } else {
            if (autre.idPublication != null)
                return false;
        }

        if (referenceCanevas == null) {
            if (autre.referenceCanevas != null)
                return false;
        } else if (!referenceCanevas.equals(autre.referenceCanevas)) {
            return false;
        }
        return true;
    }

    /**
     * cette méthode permet la conversion en String.
     *
     * @return la chaîne de caractère convertie.
     * @see java.lang.Object#toString()
     */
    //@Override /* ne pas décommenter cette ligne pour ne pas avoir des problèmes avec GWT (car java.lang.Object n'est pas reconnu par GWT) */
    public final String toString() {
        StringBuilder buff = new StringBuilder("Canevas")
                .append("\nIdCanevas: ").append(idCanevas)
                .append("\nidPublication: ").append(idPublication)
                .append("\nRéférence: ").append(referenceCanevas)
                .append("\nChapitres: ").append(chapitres.size());

        for (Chapitre chapitre : chapitres)
            buff.append("\n ").append(chapitre);
        return buff.toString();
    }

    public int getIdCanevas() {
        return idCanevas;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public Integer getIdPublication() {
        return idPublication;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public String getReferenceCanevas() {
        return referenceCanevas;
    }

    public void setReferenceCanevas(String referenceCanevas) {
        this.referenceCanevas = referenceCanevas;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public List<Integer> getProcedurePassationList() {
        return procedurePassationList;
    }

    public void setProcedurePassationList(List<Integer> procedurePassationList) {
        this.procedurePassationList = procedurePassationList;
    }

    public List<Integer> getTypeContratList() {
        return typeContratList;
    }

    public void setTypeContratList(List<Integer> typeContratList) {
        this.typeContratList = typeContratList;
    }

    public int getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(int typeDocument) {
        this.typeDocument = typeDocument;
    }

    public int getNaturePrestation() {
        return naturePrestation;
    }

    public void setNaturePrestation(int naturePrestation) {
        this.naturePrestation = naturePrestation;
    }

    public boolean getStatut() {
        return statut;
    }

    public void setStatut(boolean statut) {
        this.statut = statut;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }

    public void setCompatibleEntiteAdjudicatrice(boolean compatibleEntiteAdjudicatrice) {
        this.compatibleEntiteAdjudicatrice = compatibleEntiteAdjudicatrice;
    }

    public Integer getIdOrganisme() {
        return idOrganisme;
    }

    public void setIdOrganisme(Integer idOrganisme) {
        this.idOrganisme = idOrganisme;
    }

    public boolean isCanevasEditeur() {
        return canevasEditeur;
    }

    public void setCanevasEditeur(boolean canevasEditeur) {
        this.canevasEditeur = canevasEditeur;
    }

    public Boolean getEditeur() {
        return editeur;
    }

    public void setEditeur(Boolean editeur) {
        this.editeur = editeur;
    }

    public Integer getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }

    public void setIdStatutRedactionClausier(Integer idStatutRedactionClausier) {
        this.idStatutRedactionClausier = idStatutRedactionClausier;
    }

    public String getCreeLe() {
        return creeLe;
    }

    public void setCreeLe(String creeLe) {
        this.creeLe = creeLe;
    }

    public String getModifieLe() {
        return modifieLe;
    }

    public void setModifieLe(String modifieLe) {
        this.modifieLe = modifieLe;
    }

    public String getReferenceInitialDuplication() {
        return referenceInitialDuplication;
    }

    public void setReferenceInitialDuplication(String referenceInitialDuplication) {
        this.referenceInitialDuplication = referenceInitialDuplication;
    }

    public Integer getIdRefCCAG() {
        return idRefCCAG;
    }

    public void setIdRefCCAG(Integer idRefCCAG) {
        this.idRefCCAG = idRefCCAG;
    }

    public boolean isDerogationActive() {
        return derogationActive;
    }

    public void setDerogationActive(boolean derogationActive) {
        this.derogationActive = derogationActive;
    }

    public boolean isDerogationDevenirInactif() {
        return derogationDevenirInactif;
    }

    public void setDerogationDevenirInactif(boolean derogationDevenirInactif) {
        this.derogationDevenirInactif = derogationDevenirInactif;
    }

    // TODO: possible que c'est inutil après le merge de TypeContrat
    public Integer getIdTypePassation() {
        return idTypePassation;
    }

    // TODO: possible que c'est inutil après le merge de TypeContrat
    public void setIdTypePassation(Integer idTypePassation) {
        this.idTypePassation = idTypePassation;
    }

}
