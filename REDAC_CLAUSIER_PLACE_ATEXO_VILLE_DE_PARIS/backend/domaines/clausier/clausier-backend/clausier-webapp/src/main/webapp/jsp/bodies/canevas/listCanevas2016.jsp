<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 08/09/18
  Time: 00:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="atexo"   uri="AtexoTag" %>
<%@ taglib prefix="atexo2"  uri="../../../WEB-INF/tld/atexo-rsem.tld" %>

<script src="webjars/jquery/3.2.1/jquery.min.js"></script>

<%--@elvariable id="editeur" type="java.lang.Boolean"--%>

<!--Debut Main part-->
<div class="main-part main-part-2018">

    <div class="atx-breadcrumbs breadcrumbs">
        <strong>
            <spring:message code="redaction.header.administration"/>
            <c:if test="${editeur}"><spring:message code="redaction.header.editeur"/></c:if>&nbsp;&gt;&nbsp;
            <spring:message code="redaction.header.canevas"/>&nbsp;&gt;&nbsp;
            <spring:message code="redaction.header.rechercher"/>
        </strong>
    </div>

    <jsp:include page="../message.jsp" />
    <div class="breaker"></div>

    <!--Bloc recherche profil utilisateur-->
    <jsp:include page="searchCanevas2016.jsp" />

    <!-- Fenêtre-modale de suppresion -->
    <jsp:include page="../modalConfirmation.jsp" />

    <%--@elvariable id="resultList" type="fr.paris.epm.global.commun.ResultList"--%>
    <%--@elvariable id="editeur" type="java.lang.Boolean"--%>

    <!-- Navigation bloque -->
    <div class="atx-pagination">
        <div class="nbr-result pull-left">
            <h2 class="h5">
                <c:out value="${resultList.count}"/>&nbsp;<spring:message code="recherche.resultats"/>
            </h2>
        </div>
        <jsp:include page="../navigate.jsp" />
    </div>

    <!--Debut bloc resultats profil utilisateur-->
    <div class="results-list">
        <table>
            <thead>
            <tr>
                <th class="select-col">
                    <div>
                        <input id="select-all-canevas" type="checkbox" title="Tous" />
                    </div>
                </th>
                <th class="reference">
                    <div>
                        <spring:message code="redaction.canevas.reference" />
                        <a href="sort.htm?property=reference"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.auteur" />
                        <a href="sort.htm?property=canevasEditeur"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.statut" />
                        <a href="sort.htm?property=epmTRefStatutRedactionClausier.id"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="theme">
                    <div>
                        <spring:message code="redaction.canevas.type" />
                        <a href="sort.htm?property=epmTRefTypeDocument.libelle"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.titre" />
                        <a href="sort.htm?property=titre"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.naturePrestation" />
                        <a href="sort.htm?property=idNaturePrestation"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="contenu-long">
                    <div><spring:message code="redaction.canevas.procedurePassation" /></div>
                    <div><spring:message code="redaction.canevas.typeContrat" /></div>
                </th>
                <th class="col-80">
                    <div>
                        <spring:message code="redaction.canevas.creeLe" />
                        <a href="sort.htm?property=dateCreation"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.modifieeLe" />
                        <a href="sort.htm?property=dateModification"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.canevas.version" />
                        <a href="sort.htm?property=idLastPublication"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="col-150">
                    <div>
                        <spring:message code="redaction.actions" />
                    </div>
                </th>
            </tr>
            </thead>

            <tbody>
            <c:forEach var="canevas" items="${resultList.listResults}" varStatus="vs">
                <%--@elvariable id="canevas" type="fr.paris.epm.redaction.presentation.bean.CanevasBean"--%>

                <c:set var="style" value="" />
                <c:if test="${vs.index % 2 == 0}">
                    <c:set var="style" value="on" />
                </c:if>
                <c:if test="${!canevas.actif}">
                    <c:set var="style" value="off" />
                </c:if>

                <tr class="${style}">
                    <td class="select-col">
                        <input class="select-canevas" type="checkbox" title="Tous"
                               data-id-canevas="${canevas.idCanevas}"
                               data-reference-canevas="${canevas.referenceCanevas}"
                                <c:if test="${(!editeur && canevas.canevasEditeur) || (editeur && !canevas.canevasEditeur)}">
                                    disabled="disabled"
                                </c:if> />
                    </td>
                    <td class="reference">
                        <div><c:out value="${canevas.referenceCanevas}" /></div>
                        <div><c:out value="${canevas.labelTypeAuteur}" /></div>
                        <div>
                            <c:choose>
                                <c:when test="${canevas.idStatutRedactionClausier == 1}">
                                    <div>
                                        <img title="Brouillon" alt="Brouillon" src="<atexo:href href='images/clause-statut-1.gif'/>" />
                                    </div>
                                </c:when>
                                <c:when test="${canevas.idStatutRedactionClausier == 2}">
                                    <div>
                                        <img title="A valider" alt="A valider" src="<atexo:href href='images/clause-statut-2.gif'/>" />
                                    </div>
                                </c:when>
                                <c:when test="${canevas.idStatutRedactionClausier == 3}">
                                    <div>
                                        <img title="Validée" alt="Validée" src="<atexo:href href='images/clause-statut-3.gif'/>" />
                                    </div>
                                </c:when>
                            </c:choose>
                        </div>
                    </td>
                    <td class="theme">
                        <div><c:out value="${canevas.labelTypeDocument}" /></div>
                        <div><c:out value="${canevas.titre}" /></div>
                        <div><c:out value="${canevas.labelNaturePrestation}" /></div>
                    </td>
                    <td class="contenu-long">
                        <div>
                            <c:choose>
                                <c:when test="${not empty canevas.labelsProcedures}">
                                    <c:out value="${canevas.labelsProcedures}" />
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="redaction.toutesProcedure" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div>
                            <c:choose>
                                <c:when test="${not empty canevas.labelsTypeContrats}">
                                    <c:out value="${canevas.labelsTypeContrats}" />
                                </c:when>
                                <c:otherwise>
                                    <spring:message code="redaction.toustypeContrat" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </td>
                    <td class="col-80">
                        <c:set var="dateCreationStr"><fmt:formatDate value="${canevas.dateCreation}" pattern="dd/MM/yyyy" /></c:set>
                        <div><c:out value="${dateCreationStr}" /></div>
                        <c:set var="dateModificationStr"><fmt:formatDate value="${canevas.dateModification}" pattern="dd/MM/yyyy" /></c:set>
                        <div><c:out value="${dateModificationStr}" /></div>
                        <div><c:out value="${canevas.lastVersion}" /></div>
                    </td>
                    <td class="col-150">
                        <c:set var="urlParameters" value="idCanevas=${canevas.idCanevas}" />
                        <c:if test="${not empty canevas.idPublication}">
                            <c:set var="urlParameters" value="${urlParameters}&idPublication=${canevas.idPublication}" />
                        </c:if>

                        <c:set var="urlEditeur" value="" />
                        <c:if test="${editeur}">
                            <c:set var="urlEditeur" value="Editeur" />
                            <c:set var="urlParameters" value="${urlParameters}&editeur=yes" />
                        </c:if>

                        <!--  verification role de validation de canevas editeur / client -->
                        <c:set var="roleValiderCanevasClient" value="${atexo2:filtreSecurite(utilisateur, \"ROLE_validerCanevas\", true)}" />
                        <c:set var="roleValiderCanevasEditeur" value="${atexo2:filtreSecurite(utilisateur, \"ROLE_validerCanevasEditeur\", true)}" />

                        <!-- verification si les boutons de validation/refus doivent s'afficher -->
                        <c:choose>
                            <c:when test="${editeur && canevas.canevasEditeur && roleValiderCanevasEditeur}">
                                <c:set var="isBoutonValidationVisible" value="true" />
                            </c:when>
                            <c:when test="${!editeur && !canevas.canevasEditeur && roleValiderCanevasClient}">
                                <c:set var="isBoutonValidationVisible" value="true" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="isBoutonValidationVisible" value="false" />
                            </c:otherwise>
                        </c:choose>

                        <!-- Boutons de validation -->
                        <c:choose>
                            <c:when test="${canevas.idStatutRedactionClausier == 1}">
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.canevas.confirmation.prevalider.message" />"
                                           data-text="${canevas.referenceCanevas}"
                                           data-href="changeStatusCanevas.htm?idCanevas=${canevas.idCanevas}&idStatus=2">
                                            <img src="<atexo:href href='images/bouton-demande-validation.gif'/>"
                                                 alt="<spring:message code="redaction.action.demanderValidation"/>"
                                                 title="<spring:message code="redaction.action.demanderValidation"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                                <span class="empty-action">-</span>
                            </c:when>
                            <c:when test="${canevas.idStatutRedactionClausier == 2}">
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.canevas.confirmation.valider.message" />"
                                           data-text="${canevas.referenceCanevas}"
                                           data-href="changeStatusCanevas.htm?idCanevas=${canevas.idCanevas}&idStatus=3">
                                            <img src="<atexo:href href='images/bouton-valider.gif'/>"
                                                 alt="<spring:message code="redaction.action.valider"/>"
                                                 title="<spring:message code="redaction.action.valider"/>" />
                                        </a>
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.canevas.confirmation.refuser.message" />"
                                           data-text="${canevas.referenceCanevas}"
                                           data-href="changeStatusCanevas.htm?idCanevas=${canevas.idCanevas}&idStatus=1">
                                            <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                                                 alt="<spring:message code="redaction.action.refuser"/>"
                                                 title="<spring:message code="redaction.action.refuser"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${canevas.idStatutRedactionClausier == 3}">
                                <span class="empty-action">-</span>
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.canevas.confirmation.refuser.message" />"
                                           data-text="${canevas.referenceCanevas}"
                                           data-href="changeStatusCanevas.htm?idCanevas=${canevas.idCanevas}&idStatus=1">
                                            <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                                                 alt="<spring:message code="redaction.action.refuser"/>"
                                                 title="<spring:message code="redaction.action.refuser"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>

                        <!-- Bouton de prévisualisation -->
                        <a href="javascript:popUp('previewCanevas.htm?${urlParameters}', 'yes');">
                            <img src="<atexo:href href='images/bouton-previsualiser.gif' />"
                                 alt="<spring:message code="redaction.action.previsualiser"/>"
                                 title="<spring:message code="redaction.action.previsualiser"/>" />
                        </a>

                        <!-- Bouton Modification -->
                        <c:choose>
                            <c:when test="${editeur || !canevas.canevasEditeur}"> <%--<c:when test="${(!editeur && !canevas.canevasEditeur) || (editeur && canevas.canevasEditeur)}">--%>
                                <a href="ModificationCanevas${urlEditeur}Init.epm?${urlParameters}&typeAction=M&initialisationSession=true">
                                    <img src="<atexo:href href='images/bouton-modifier.gif' />"
                                         alt="<spring:message code="redaction.action.modifier"/>"
                                         title="<spring:message code="redaction.action.modifier"/>" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="empty-action">-</span>
                            </c:otherwise>
                        </c:choose>
                        <!-- Bouton Duplication -->
                        <a href="ModificationCanevas${urlEditeur}Init.epm?${urlParameters}&typeAction=D&initialisationSession=true">
                            <img src="<atexo:href href='images/bouton-picto-dupliquer.gif' />"
                                 alt="<spring:message code="redaction.action.dupliquer"/>"
                                 title="<spring:message code="redaction.action.dupliquer"/>" />
                        </a>

                        <!-- Bouton suppression -->
                        <c:choose>
                            <c:when test="${editeur || !canevas.canevasEditeur}"> <%--<c:when test="${(!editeur && !canevas.canevasEditeur) || (editeur && canevas.canevasEditeur)}">--%>
                                <a data-toggle="modal" href="#modalConfirmation"
                                   data-title="<spring:message code="redaction.canevas.confirmation.supprimer.message" />"
                                   data-text="${canevas.referenceCanevas}"
                                   data-href="removeCanevas.htm?idCanevas=${canevas.idCanevas}">
                                    <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"
                                         alt="<spring:message code="redaction.action.supprimer"/>"
                                         title="<spring:message code="redaction.action.supprimer"/>" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="empty-action">-</span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!--Fin bloc resultats recherche-->

    <!-- Navigation bloque -->
    <div class="atx-pagination">
        <jsp:include page="../navigate.jsp" />
    </div>

    <div id="actions-panel" class="layer">
        <div class="content">
            <div class="float-left">
                <strong><spring:message code="redaction.actionsGroupees" /></strong>
            </div>
            <div class="float-right">
                <c:if test="${editeur && roleValiderCanevasEditeur || !editeur && roleValiderCanevasClient}">
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.canevas.confirmation.prevalider.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusCanevas.htm?' + listIds + '&idStatus=2'">
                        <img src="<atexo:href href='images/bouton-demande-validation.gif'/>"
                             alt="<spring:message code="redaction.action.demanderValidation"/>"
                             title="<spring:message code="redaction.action.demanderValidation"/>" />
                    </a>
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.canevas.confirmation.refuser.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusCanevas.htm?' + listIds + '&idStatus=1'">
                        <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                             alt="<spring:message code="redaction.action.refuser"/>"
                             title="<spring:message code="redaction.action.refuser"/>" />
                    </a>
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.canevas.confirmation.valider.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusCanevas.htm?' + listIds + '&idStatus=3'">
                        <img src="<atexo:href href='images/bouton-valider.gif'/>"
                             alt="<spring:message code="redaction.action.valider"/>"
                             title="<spring:message code="redaction.action.valider"/>" />
                    </a>
                </c:if>
                <a data-toggle="modal" href="#modalConfirmation"
                   data-title="<spring:message code="redaction.canevas.confirmation.supprimer.message" />"
                   v-bind:data-text="listReferences"
                   v-bind:data-href="'removeCanevas.htm?' + listIds">
                    <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"
                         alt="<spring:message code="redaction.action.supprimer"/>"
                         title="<spring:message code="redaction.action.supprimer"/>" />
                </a>
            </div>
        </div>
    </div>

    <jsp:include page="listCanevas.vue" />

</div>
<!--Fin Main part-->
