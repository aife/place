<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<logic:notEmpty name="chapitres">
    <logic:iterate id="chapitre" name="chapitres" indexId="index"><%-- type="fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument"--%>
        <%--@elvariable id="chapitre" type="fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument"--%>
        <c:set var="chapitreNumero" value="${chapitre.niveau}" scope="request" />
        <c:if test="${chapitre.niveau > 5}">
            <c:set var="chapitreNumero" value="5" scope="request" />
        </c:if>
        <c:if test="${chapitre.niveau == 1}">
            <c:set var="premierNiveau" value="true" scope="request" />
        </c:if>
        <c:if test="${chapitre.niveau != 1}">
            <c:set var="premierNiveau" value="false" scope="request" />
        </c:if>
        <ul id="chapitre_${chapitre.numero}">
            <li>
                <logic:equal name="premierNiveau" value="true">
                    <h1>
                        <c:out value="${chapitre.numero}" />.&nbsp;<c:out value="${chapitre.titre}" />
                    </h1>
                </logic:equal>
                <logic:equal name="premierNiveau" value="false">
                    <h${chapitreNumero}>
                        <c:out value="${chapitre.numero}" />.&nbsp;<c:out value="${chapitre.titre}" />
                    </h${chapitreNumero}>
                </logic:equal>
                <ul class="liste-clause">
                    <logic:notEmpty name="chapitre" property="paragrapheDocument">
                        <li>
                            <nested:write name="chapitre" property="paragrapheDocument.texte" filter="false" />
                        </li>
                    </logic:notEmpty>
                    <logic:empty name="chapitre" property="paragrapheDocument">
                        <logic:notEmpty name="chapitre" property="clausesDocument">
                            <logic:iterate id="clause" name="chapitre" property="clausesDocument"><%-- type="fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument"--%>
                                <%--@elvariable id="clause" type="fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument"--%>
                                <c:if test="${clause.etat != '3'}">
                                    <li>
                                        <bean:define id="clause" name="clause" toScope="request" />
                                        <bean:define id="chapitre" name="chapitre" toScope="request" />
                                        <jsp:include page="/jsp/previsualisation/document/ApercuDynamiqueClause.jsp" />
                                    </li>
                                </c:if>
                            </logic:iterate>
                        </logic:notEmpty>
                    </logic:empty>
                    <c:if test="${chapitre.derogation != null && chapitre.afficherNBMessageDerogation == true}">
                        <li>
                            <div class="clause">
                                <strong>NB</strong> : cet article déroge à l'article
                                <c:out escapeXml="false" value="${chapitre.derogation.articleDerogationCCAG}" />du CCAG
                            </div>
                        </li>
                    </c:if>
                </ul>
                <logic:notEmpty name="chapitre" property="sousChapitres">
                    <bean:define id="premierNiveau" value="false" toScope="request" />
                    <bean:define id="chapitres" name="chapitre" property="sousChapitres" toScope="request" />
                    <jsp:include page="/jsp/previsualisation/document/ApercuDynamiqueChapitre.jsp" />
                </logic:notEmpty>
            </li>
        </ul>
    </logic:iterate>
</logic:notEmpty>
