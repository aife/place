var tinyConfig = [
		{
			theme : "advanced",
			mode : "specific_textareas",
			editor_selector : "mceEditorSansToolbar",
			plugins : "paste",
			language : "fr",

			// Theme options
			theme_advanced_buttons1 : "",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path : false,
			theme_advanced_more_colors : false,
			readonly : 1,
			content_css : hrefRacine+"css/styles-tinymce-spec.css",
			paste_text_sticky : true,
			paste_text_sticky_default : true,
			paste_text_linebreaktype : "br",
            paste_auto_cleanup_on_paste: true,
			entity_encoding : "raw"
		},
		{
			theme : "advanced",
			mode : "specific_textareas",
			editor_selector : "mceEditor",
			plugins : "paste",
			// br a la place de p pour newline
			forced_root_block : false,
			force_br_newlines : true,
			force_p_newlines : false,
			language : "fr",
			content_css : hrefRacine+"css/styles-tinymce-spec.css", 

			// Theme options
			theme_advanced_toolbar_location : "top",
			theme_advanced_buttons1 : "bold,italic,underline,|,forecolor,fontsizeselect,|,justifyleft,justifycenter,justifyright,|,bullist,outdent,indent,|,undo,redo",
			theme_advanced_buttons2 : "",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path : false,
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_more_colors : false,

			paste_text_sticky : true,
			paste_text_sticky_default : true,
			paste_text_linebreaktype : "br",
            paste_auto_cleanup_on_paste: true,
			entity_encoding : "raw",
			// callback
			onchange_callback : "tinyMCETextChanged",
			setup : function(ed) {
				ed.onClick.add(function(ed, e) {
					if (typeof miseAJourPopupPosition == "function") {
						miseAJourPopupPosition(ed);
					}
				});
			}
		} ];
/**
 * Gestion des champs editables avec TinyMCE
 */

/**
 * Initialise les champs editables pour TinyMCE sans boutons.
 */
function initEditeursTexteSansToolbarRedaction() {
	tinyMCE.init(tinyConfig[0]);
}

/**
 * Initialise les champs editables pour TinyMCE (choix des boutons, etc).
 */
function initEditeursTexteRedaction() {
	tinyMCE.init(tinyConfig[1]);
}

/**
 * Initialise les champs editables pour TinyMCE (choix des boutons, etc) dans le
 * cas de surcharge de clauses
 */
function initEditeursTexteRedactionSurchargeClauses() {
	tinyMCE
			.init({
				theme : "advanced",
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				plugins : "paste",
				// br a la place de p pour newline
				forced_root_block : false,
				force_br_newlines : true,
				force_p_newlines : false,
				language : "fr",
				content_css : hrefRacine+"css/styles-tinymce-spec.css", 

				// Theme options
				theme_advanced_toolbar_location : "top",
				theme_advanced_buttons1 : "bold,italic,underline,|,forecolor,fontsizeselect",
				theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,|,bullist,outdent,indent,|,undo,redo",
				theme_advanced_buttons3 : "",
				theme_advanced_toolbar_align : "left",
				theme_advanced_resize_horizontal : false,
				theme_advanced_path : false,
				theme_advanced_resizing : true,
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_more_colors : false,

				paste_text_sticky : true,
				paste_text_sticky_default : true,
				paste_text_linebreaktype : "br",
				entity_encoding : "raw",
				// callback
				onchange_callback : "tinyMCETextChanged",
				setup : function(ed) {
					ed.onClick.add(function(ed, e) {
						if (typeof miseAJourPopupPosition == "function") {
							miseAJourPopupPosition(ed);
						}
					});
				}

			});
}

function tinyMCETextChanged() {
	if (typeof textChanged == "function") {
		textChanged();
	}
}


/**
 * Supprime les iframes de TinyMCE pour dans le cas d'affichage dynamique de
 * differents textarea.
 */
function supprimerTinyMCEEditors() {
	var i, t = tinyMCE.editors;
	for (i in t) {
		if (t.hasOwnProperty(i)
				&& (tinyMCE.editors[i].settings.readonly == null)) {
			t[i].remove();
		}
	}
}

/**
 * Reaffiche les editeurs TinyMCE des textarea ayant la classe de style
 * mceClassName.
 */
function reinitTinyMCE(mceClassName) {
	var elementToInit = new Array();
	var textAreaElementTab = document.getElementsByTagName('textarea');
	var j = 0;
	for ( var i = 0; i < textAreaElementTab.length; i++) {
		var item = textAreaElementTab[i];
		if ((item.style.display != 'none')
				&& (item.className.search(mceClassName) != -1)) {
			tinyMCE.execCommand('mceAddControl', false, item.id);
		}
	}
}

function unescapeHTML(html) {
	var res;
	var htmlNode = document.createElement("unescapeElementTmp");
	htmlNode.innerHTML = html;
	if (htmlNode.innerText !== undefined) {
		res = htmlNode.innerText; // IE
	} else {
		res = htmlNode.textContent; // FF
	}
	return res;
}

// Ensemble des valurs de la taille de texte pour l'edition des clauses
var TEXT_OPTION_TRES_LONG = 4;
var TEXT_OPTION_LONG = 1;
var TEXT_OPTION_MOYEN = 2;
var TEXT_OPTION_COURT = 3;

/**
 * Convertir les richText en text si besoin lors de la selection d'une nouvelle
 * taille de texte dans l'edition des clauses.
 */
function convertRichTextOnSelect(debutTextId, selectElement) {
	var selectOption = selectElement.options[selectElement.selectedIndex];
	var selectOptionValue = parseInt(selectOption.value);
	var textElement = document.getElementById(debutTextId + selectOptionValue);
	if ((selectOptionValue != TEXT_OPTION_TRES_LONG)
			&& (selectOptionValue != TEXT_OPTION_LONG)) {
		textElement.value = unescapeHTML(textElement.value);
	}
}

/**
 * Comme displayOptionChoice avec gestion des richText par TinyMCE.
 */
function displayOptionChoiceWithTinyMCE(mySelect, prefix_texte, mceClassName) {
	supprimerTinyMCEEditors();
	displayOptionChoice(mySelect, prefix_texte);
	convertRichTextOnSelect(prefix_texte, mySelect);
	reinitTinyMCE(mceClassName);
}
