<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="redactionTag" prefix="redaction" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>

<logic:notEmpty name="chapitres">
    <logic:iterate id="chapitre" name="chapitres" type="fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre">
        <%--@elvariable id="chapitre" type="fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre"--%>
        <ul>
            <li>
                <logic:equal name="premierNiveau" value="true">
                    <strong>${chapitre.numero} ${chapitre.titre}</strong>
                </logic:equal>
                <logic:equal name="premierNiveau" value="false">
                    <i>${chapitre.numero} ${chapitre.titre}</i>
                    <logic:notEmpty name="chapitre" property="infoBulle">
                    </logic:notEmpty>
                </logic:equal>
                <c:if test="${chapitre.infoBulle != null && chapitre.infoBulle.actif}">
                    <img src="<atexo:href href='images/picto-info-bulle-transparent-bk.gif'/>"
                         onmouseover="afficheBulle('infos-chapitres${chapitre.idChapitre}', this)"
                         onmouseout="cacheBulle('infos-chapitres${chapitre.idChapitre}')"
                         class="picto-info-bulle" alt="Info-bulle" title="Info-bulle"/>
                    <div id="infos-chapitres${chapitre.idChapitre}" class="info-bulle"
                         onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                        <div><c:out value="${chapitre.infoBulle.description}"/></div>
                    </div>
                </c:if>

                <logic:notEmpty name="chapitre" property="clauses">
                    <ul class="liste-clause">
                        <logic:iterate id="clauseChapitre" name="chapitre" property="clauses" indexId="indexClause"
                                       type="fr.paris.epm.redaction.metier.objetValeur.canevas.Clause">
                            <%--@elvariable id="clauseChapitre" type="fr.paris.epm.redaction.metier.objetValeur.canevas.Clause"--%>
                            <logic:equal name="clauseChapitre" property="actif" value="true">
                                <li>
                                    <span class="ref-clause">
                                        <c:if test="${clauseChapitre.infoBulle != null}">
                                            <c:if test="${clauseChapitre.infoBulle.actif}">
                                                <img src="<atexo:href href='images/picto-info-bulle-transparent-bk.gif'/>"
                                                     onmouseover="afficheBulle('infos-clauses${chapitre.idChapitre}-${indexClause}', this)"
                                                     onmouseout="cacheBulle('infos-clauses${chapitre.idChapitre}-${indexClause}')"
                                                     class="picto-info-bulle" alt="Info-bulle" title="Info-bulle" />
                                                <div id="infos-clauses${chapitre.idChapitre}-${indexClause}" class="info-bulle"
                                                     onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                                                    <div><c:out value="${clauseChapitre.infoBulle.description}" /></div>
                                                </div>
                                            </c:if>
                                            <c:out value="${clauseChapitre.reference}" />
                                        </c:if>
                                    </span>

                                    <bean:define id="clauseRef" name="clauseChapitre" property="reference" />
                                    <redaction:clause ref="${clauseRef}" idClause="${clauseChapitre.idClause}" idPublication="${clauseChapitre.idPublication}">
                                        <%--@elvariable id="clause" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>
                                        <div class="${(clause.epmTRefTypeClause.id == 6 || clause.epmTRefTypeClause.id == 7) ? 'line' : 'clause'}">
                                            ${clause.texteFixeAvant}
                                            <c:if test="${clause.sautLigneTexteAvant == true}"><br/></c:if>
                                            <redaction:previsualiserPptExtClause name="clause" user="utilisateur"/>
                                            <c:if test="${clause.sautLigneTexteApres == true}"><br/></c:if>
                                            ${clause.texteFixeApres}
                                        </div>
                                    </redaction:clause>
                                </li>
                            </logic:equal>
                        </logic:iterate>
                        <c:if test="${chapitre.derogation != null && chapitre.afficherMessageDerogation == true}">
                            <li>
                                <div class="clause">
                                    <strong>NB</strong> : cet article déroge à l'article
                                    <c:out value="${chapitre.derogation.articleDerogationCCAG}"/> du CCAG
                                </div>
                            </li>
                        </c:if>
                    </ul>
                </logic:notEmpty>
                <logic:notEmpty name="chapitre" property="chapitres">
                    <bean:define id="premierNiveau" value="false" toScope="request" />
                    <bean:define id="chapitres" name="chapitre" property="chapitres" toScope="request" />
                    <jsp:include page="/jsp/previsualisation/canevas/Chapitre.jsp" />
                    <bean:define id="premierNiveau" value="true" toScope="request" />
                </logic:notEmpty>
                <!-- tableau des dérogations CCAG -->
                <c:if test="${chapitre.idChapitre == -1}">
                    <ul class="liste-clause">
                        <li>
                            <span class="ref-clause"> </span> <span class="clause">
                            <c:out value="${requestScope.htmlTableauDerogations}" escapeXml="false"/>
                            </span>
                        </li>
                    </ul>
                </c:if>
            </li>
        </ul>
    </logic:iterate>

</logic:notEmpty>

