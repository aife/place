<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Elaboration et Passation des Marchés</title>
        
    	<link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
		<link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
		<link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
        
        
        <script language="javascript" src='js/tiny_mce/tiny_mce.js' ></script>
        <script language="javascript" src='js/scripts-commun.js' ></script>
		
		<script language="javascript" src='js/editeurTexteRedaction.js' ></script>
		<script language="javascript" src="fr.paris.epm.redaction.popup.nocache.js"></script>
		<script type="text/javascript">
			initEditeursTexteRedaction();
		</script>
        
        <script type="text/javascript">
			 hrefRacine = '<atexo:href href=""/>';
		</script>
    </head>
	<body onresize="initLayer();">
	
	<div class="maj-clause" id="containerPopup">
	
	</div>
	<!--Fin layer flottant Ajouter un doc-->
	</body>
</html>