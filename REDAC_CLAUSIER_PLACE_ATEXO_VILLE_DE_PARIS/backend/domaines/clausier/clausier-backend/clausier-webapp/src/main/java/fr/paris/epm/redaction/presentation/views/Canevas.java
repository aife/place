package fr.paris.epm.redaction.presentation.views;

import java.util.Date;
import java.util.Set;

public class Canevas {

	private Integer id;

	private String titre;

	private String type;

	private String auteur;

	private Date dateModification;

	private Date dateCreation;

	private Set<Procedure> procedures;

	private String reference;

	private Statut statut;

	private Organisme organisme;

	private NaturePrestation naturePrestation;

	private Set<Contrat> contrats;

	private String version;

	public Integer getId() {
		return id;
	}

	public void setId( Integer id ) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre( String titre ) {
		this.titre = titre;
	}

	public String getType() {
		return type;
	}

	public void setType( String type ) {
		this.type = type;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur( String auteur ) {
		this.auteur = auteur;
	}

	public void setDateModification( Date dateModification ) {
		this.dateModification = dateModification;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateCreation( Date dateCreation ) {
		this.dateCreation = dateCreation;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public <R> void setProcedures( Set<Procedure> procedures ) {
		this.procedures = procedures;
	}

	public Set<Procedure> getProcedures() {
		return procedures;
	}

	public void setReference( String reference ) {
		this.reference = reference;
	}

	public String getReference() {
		return reference;
	}

	public void setStatut( Statut statut ) {
		this.statut = statut;
	}

	public Statut getStatut() {
		return statut;
	}

	public void setOrganisme( Organisme organisme ) {
		this.organisme = organisme;
	}

	public Organisme getOrganisme() {
		return organisme;
	}

	public void setNaturePrestation( NaturePrestation naturePrestation ) {
		this.naturePrestation = naturePrestation;
	}

	public NaturePrestation getNaturePrestation() {
		return naturePrestation;
	}

	public void setContrats( Set<Contrat> contrats ) {
		this.contrats = contrats;
	}

	public Set<Contrat> getContrats() {
		return contrats;
	}

	public void setVersion( String version ) {
		this.version = version;
	}

	public String getVersion() {
		return version;
	}
}
