<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <bean:message key="previsualiserCanevas.titre.titreCanevas"/>
    </title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
    <script type="text/javascript" language="JavaScript" src="js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" language="JavaScript" src="js/editeurTexteRedaction.js"></script>
    <script type="text/javascript">
        initEditeursTexteSansToolbarRedaction();
    </script>
</head>

<body onload="popupResize();initEditeursTexteSansToolbarRedaction();">
<div class="previsu-doc" id="container">
    <c:if test="${requestScope.activerExportCanevas eq 'true'}">
        <logic:empty name="canevas" property="idPublication">
            <a class="fermer" href="javascript:window.popUp('ExporterCanevas.epm?idCanevas=${canevas.idCanevas}', 'yes, width=400, height=190','exportCanevas');">
                Exporter
            </a>
        </logic:empty>
        <logic:notEmpty name="canevas" property="idPublication">
            <a class="fermer" href="javascript:window.popUp('ExporterCanevas.epm?idCanevas=${canevas.idCanevas}&idPublication=${canevas.idPublication}', 'yes, width=400, height=190','exportCanevas');">
                Exporter
            </a>
        </logic:notEmpty>
    </c:if>

    <div class="content">
        <center>
            <h5>
                <c:out value="${canevas.ref}" /> : <c:out value="${canevas.titre}" />
            </h5>
        </center>

        <ul>
            <li>
                <bean:define id="chapitres" name="canevas" property="chapitres" toScope="request" />
                <bean:define id="premierNiveau" value="true" toScope="request" />
                <jsp:include page="/jsp/previsualisation/canevas/Chapitre.jsp" />
            </li>
        </ul>
    </div>
    <a href="javascript:window.close();" class="fermer">Fermer</a>
    <div class="breaker"></div>
</div>
</body>
</html>
