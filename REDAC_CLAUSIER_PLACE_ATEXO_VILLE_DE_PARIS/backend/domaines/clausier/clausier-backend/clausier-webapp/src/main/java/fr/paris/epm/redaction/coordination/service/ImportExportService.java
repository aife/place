package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.importExport.bean.ClausierImportExport;

import java.io.File;
import java.util.List;

public interface ImportExportService {

    File exporterClausier(int idPublication, EpmTRefOrganisme epmTRefOrganisme);

    File exporterReferentiel(int idPublication);

    ClausierImportExport isValidReferentielFile(int idPublication, final List<String> errors);

    void constituerClausierPub(ClausierImportExport clausierImportExport, Integer idPublication, EpmTUtilisateur utilisateur);

    List<String> importerClausier(List<File> publicationFichiers, EpmTUtilisateur utilisateur);

}
