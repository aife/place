package fr.paris.epm.redaction.presentation.bean;

public class CanevasSearch extends CommunSearch {

    private String titreCanevas;
    private Integer idCCAG;

    public Integer getIdCCAG() {
        return idCCAG;
    }

    public void setIdCCAG(Integer idCCAG) {
        this.idCCAG = idCCAG;
    }

    public String getTitreCanevas() {
        return titreCanevas;
    }

    public void setTitreCanevas(String titreCanevas) {
        this.titreCanevas = titreCanevas;
    }
}
