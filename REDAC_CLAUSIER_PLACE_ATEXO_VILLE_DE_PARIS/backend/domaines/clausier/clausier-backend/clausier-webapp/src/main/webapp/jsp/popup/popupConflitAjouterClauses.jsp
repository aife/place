<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Elaboration et Passation des Marchés</title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <script language="javascript" src="fr.paris.epm.redaction.popupConflitAjouterClauses.nocache.js"></script>
    <script language="javascript" src='js/tiny_mce/tiny_mce.js'></script>
    <script language="javascript" src='js/editeurTexteRedaction.js'></script>

    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
</head>
<!-- onload="popupResize();" -->
<body onresize="initLayer();">
	<script type="text/javascript">
			initEditeursTexteRedaction();
		</script>

	<script type="text/javascript">
			 hrefRacine = '<atexo:href href=""/>';
		</script>
	<div class="previsu-doc" id="container"></div>


</body>
</html>