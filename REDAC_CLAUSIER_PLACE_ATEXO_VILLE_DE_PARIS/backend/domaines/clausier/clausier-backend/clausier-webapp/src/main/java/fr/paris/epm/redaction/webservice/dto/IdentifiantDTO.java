package fr.paris.epm.redaction.webservice.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.UUID;

public class IdentifiantDTO {
	private static final Logger LOG = LoggerFactory.getLogger(IdentifiantDTO.class);

	public final Long id;
	public final UUID uuid;

	public IdentifiantDTO( DocumentDTO documentDTO ) {
		this.id = documentDTO.getId();
		this.uuid = documentDTO.getUuid();
	}

	public String toJSON() {
		String json = null;

		try {
			json = new ObjectMapper().writeValueAsString(this);
		} catch ( final JsonProcessingException e ) {
			LOG.error("Impossible de convertir l'objet en json", e);
		}

		return json;
	}

	public static IdentifiantDTO fromJSON(String json) {
		IdentifiantDTO result = null;

		try {
			result = new ObjectMapper().readValue(json, IdentifiantDTO.class);
		} catch ( final JsonProcessingException e ) {

		} catch ( IOException e ) {
			LOG.error("Impossible de convertir le json en objet", e);
		}

		return result;
	}

	public Long getId() {
		return id;
	}

	public UUID getUuid() {
		return uuid;
	}
}
