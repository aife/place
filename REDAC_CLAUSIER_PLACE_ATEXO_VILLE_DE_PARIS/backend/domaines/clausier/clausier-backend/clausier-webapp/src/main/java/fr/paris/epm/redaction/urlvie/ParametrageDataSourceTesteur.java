package fr.paris.epm.redaction.urlvie;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageDataSourceTesteur implements DecrireUnParametreDeTesteur {

    JAVA_NAMING_FACTORY_INITIAL(true, String.class),
    JAVA_NAMING_FACTORY_URL_PKGS(true, String.class), 
    JAVA_NAMING_PROVIDER_URL(true, String.class),
 DATASOURCE_JNDINAME(true,
            String.class), SQL(true, String.class),
    /**
     * Puisqu'on ne teste que la possibilité d'une connexion effective à la
     * base, inutile d'attendre une réponse précise. Ce paramètre ne doit pas
     * exister.
     */
    REPONSE_ATTENDUE(false, null);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageDataSourceTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return returnReponseAttendue();
    }

    private static DecrireUnParametreDeTesteur returnReponseAttendue() {
        return REPONSE_ATTENDUE;
    }

}
