<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--Debut main-part-->
<!--Debut bloc Infos clause-->
<div class="form-bloc">
    <div class="top">
        <span class="left"></span><span class="right"></span>
    </div>
    <div class="breaker"></div>

    <div class="content">
        <h2 class="float-left">
            <html:radio property="clauseSelectionnee" styleId="clauseEditeurSurchargeeSelectionnee" value="clauseEditeurSurcharge"></html:radio>
            <label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeurSurchargee"/></label>
        </h2>
        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>

        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixExclusif.txt.texteFixeAvant" />
            </span>
            <html:textarea property="textFixeAvant" styleId="textFixeAvant" title="Texte fixe avant" cols="" rows="6" styleClass="texte-long mceEditor" errorStyleClass="error-border" />
        </div>
        <div class="line">
            <div class="retour-ligne">
                <html:checkbox property="sautTextFixeAvant" styleId="sautTextFixeAvant" value="true" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>"
                     alt="Saut de ligne" title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>
        <div class="clause-choix-list">
            <table id="table-formulation">
                <thead>
                <tr>
                    <th class="num-formulation">
                        <bean:message key="ClauseListeChoixExclusif.txt.numFormulation" />
                    </th>
                    <th class="nb-carac">
                        <bean:message key="ClauseListeChoixExclusif.txt.tailleChamp" />
                    </th>
                    <th class="check-col">
                        <bean:message key="ClauseListeChoixExclusif.txt.precochee" />
                    </th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <logic:empty name="frmClauseListeChoixExclusif" property="formulationCollection">
                    <script type="text/javascript">
                        var compteur = 3;
                    </script>
                    <tr id="tr_1">
                        <td class="num-formulation">
                            <html:text property="numFormulation" styleId="numFormulation" title="Numéro de formulation" value="1"/>
                        </td>
                        <c:set var="tailleChampSelectValue" value="1"/>
                        <c:if test="${not empty(frmClauseListeChoixExclusif.tailleChamp[0])}">
                            <c:set var="tailleChampSelectValue" value="${frmClauseListeChoixExclusif.tailleChamp[0]}"/>
                        </c:if>
                        <td class="nb-carac">
                            <html:select property="tailleChamp" value="${tailleChampSelectValue}" styleId="tailleChamp_1" styleClass="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut0_', 'mceEditor');">
                                <html:option value="4">
                                    <bean:message key="redaction.taille.champ.tresLong" />
                                </html:option>
                                <html:option value="1">
                                    <bean:message key="redaction.taille.champ.long" />
                                </html:option>
                                <html:option value="2">
                                    <bean:message key="redaction.taille.champ.moyen" />
                                </html:option>
                                <html:option value="3">
                                    <bean:message key="redaction.taille.champ.court" />
                                </html:option>
                            </html:select>
                        </td>
                        <td class="check-col" style="text-align: center">
                            <html:radio property="precochee" value="0" styleId="precochee"/>
                        </td>
                        <td class="actions">
                            <a
                                    href="javascript:removeRowFromTable(document.getElementById('tr_1').rowIndex,'table-formulation')"><img
                                    src="<atexo:href href='images/picto-poubelle.gif'/>"
                                    alt="Supprimer la formulation"
                                    title="Supprimer la formulation" />
                            </a>
                        </td>
                    </tr>

                    <c:if test="${tailleChampSelectValue == '0' }">
                        <script>
                            mettreTailleChampValeurDefaut('tailleChamp_1');
                        </script>
                    </c:if>

                    <tr>
                        <td class="col-champ" colspan="4">
                            <html:textarea property="valeurDefautTresLong"  title="Valeur par defaut" styleClass="texte-tres-long mceEditor" styleId="valeurDefaut0_4" style="display:none" />
                            <html:textarea  property="valeurDefaut"  title="Valeur par defaut" styleClass="texte-long mceEditor" styleId="valeurDefaut0_1"  cols="" rows="6" style="display:block"/>
                            <html:text property="valeurDefautMoyen"  title="Valeur par defaut" styleClass="texte-long" styleId="valeurDefaut0_2"  style="display:none" />
                            <html:text property="valeurDefautCourt"  title="Valeur par defaut" styleClass="texte-court" styleId="valeurDefaut0_3" style="display:none" />
                        </td>
                    </tr>
                    <tr class ="on" id="tr_2">
                        <td class="num-formulation">
                            <html:text property="numFormulation" title="Numéro de formulation" value="2" styleId="numFormulation"/>
                        </td>
                        <td class="nb-carac">
                            <c:set var="tailleChampSelectValue" value="1"/>
                            <c:if test="${not empty(frmClauseListeChoixExclusif.tailleChamp[1])}">
                                <c:set var="tailleChampSelectValue" value="${frmClauseListeChoixExclusif.tailleChamp[1]}"/>
                            </c:if>
                            <html:select property="tailleChamp" value="${tailleChampSelectValue}" styleId="tailleChamp_2" styleClass="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut1_', 'mceEditor');">
                                <html:option value="4">
                                    <bean:message key="redaction.taille.champ.tresLong" />
                                </html:option>
                                <html:option value="1">
                                    <bean:message key="redaction.taille.champ.long" />
                                </html:option>
                                <html:option value="2">
                                    <bean:message key="redaction.taille.champ.moyen" />
                                </html:option>
                                <html:option value="3">
                                    <bean:message key="redaction.taille.champ.court" />
                                </html:option>
                            </html:select>
                        </td>
                        <td class="check-col" style="text-align: center">
                            <html:radio property="precochee" value="1" styleId="precochee"/>
                        </td>
                        <td class="actions">
                            <a
                                    href="javascript:removeRowFromTable(document.getElementById('tr_2').rowIndex,'table-formulation')"><img
                                    src="<atexo:href href='images/picto-poubelle.gif'/>"
                                    alt="Supprimer la formulation"
                                    title="Supprimer la formulation"/>
                            </a>
                        </td>
                    </tr>
                    <tr class ="on" >
                        <td class="col-champ" colspan="4">
                            <html:textarea  property="valeurDefautTresLong"  title="Valeur par defaut" styleClass="texte-tres-long mceEditor"  styleId="valeurDefaut1_4"   cols="" rows="6" style="display:none" />
                            <html:textarea  property="valeurDefaut"  title="Valeur par defaut" styleClass="texte-long mceEditor"  styleId="valeurDefaut1_1"   cols="" rows="6" style="display:block" />
                            <html:text property="valeurDefautMoyen"  title="Valeur par defaut" styleClass="texte-long" styleId="valeurDefaut1_2"  style="display:none" />
                            <html:text property="valeurDefautCourt"  title="Valeur par defaut" styleClass="texte-court" styleId="valeurDefaut1_3"  style="display:none" />
                        </td>
                    </tr>

                    <c:if test="${tailleChampSelectValue == '0' }">
                        <script>
                            mettreTailleChampValeurDefaut('tailleChamp_2');
                        </script>
                    </c:if>
                </logic:empty>

                <logic:notEmpty name="frmClauseListeChoixExclusif" property="formulationCollection">
                    <script type="text/javascript">
                        <bean:size id="sizeList" name="frmClauseListeChoixExclusif" property="formulationCollection"/>
                        var compteur = ${sizeList} + 1;
                    </script>
                    <logic:iterate name="frmClauseListeChoixExclusif" property="formulationCollection" id="clauseListeRole" indexId="index">
                        <bean:define id="lignePaire">${index % 2}</bean:define>
                        <tr <logic:notEqual name="lignePaire" value="0">class ="on"</logic:notEqual> id="tr_${index + 1}">
                            <td class="num-formulation">
                                <input type="text" name="numFormulation" value="<nested:write name="clauseListeRole" property="numFormulation"/>"
                                       title="Numéro de formulation">
                            </td>
                            <td class="nb-carac">
                                <div class="radio-choice-small">
                                    <select id="tailleChamp_${index + 1}" name="tailleChamp" class="auto" title="Taille du Champ"
                                            onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'valeurDefaut${index}_', 'mceEditor');">
                                        <option value="4" <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="4">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.tresLong" />
                                        </option>
                                        <option value="1" <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="1">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.long" />
                                        </option>
                                        <option value="2" <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="2">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.moyen" />
                                        </option>
                                        <option value="3" <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="3">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.court" />
                                        </option>
                                    </select>
                                </div>
                            </td>
                            <td class="check-col" style="text-align: center">
                                <logic:equal name="clauseListeRole" property="precochee" value="0">
                                    <input  type="radio" name="precochee" value="${index}">
                                </logic:equal>
                                <logic:equal name="clauseListeRole" property="precochee" value="1">
                                    <input type="radio" name="precochee" value="${index}" checked="checked">
                                </logic:equal>
                            </td>
                            <td class="actions">
                                <a href="javascript:removeRowFromTableSurcharge(document.getElementById('tr_${index + 1}').rowIndex,'table-formulation')"><img
                                        src="<atexo:href href='images/picto-poubelle.gif'/>"
                                        alt="Supprimer la formulation"
                                        title="Supprimer la formulation" />
                                </a>
                            </td>
                        </tr>

                        <nested:equal name="clauseListeRole" property="nombreCarateresMax" value="0">
                            <script>
                                var idTailleChamp = 'tailleChamp_${index + 1}';
                                mettreTailleChampValeurDefaut(idTailleChamp);
                            </script>
                        </nested:equal>

                        <tr <logic:notEqual name="lignePaire" value="0">class ="on"</logic:notEqual> id="tr_${index + 1}">
                            <td class="col-champ" colspan="4">
                                <c:set var="styleDisplayTresLong" value="none"/>
                                <c:set var="styleDisplayLong" value="none"/>
                                <c:set var="styleDisplayMoyen" value="none"/>
                                <c:set var="styleDisplayCourt" value="none"/>
                                <c:choose>
                                    <c:when test="${clauseListeRole.nombreCarateresMax == 4}">
                                        <c:set var="styleDisplayTresLong" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRole.nombreCarateresMax == 3}">
                                        <c:set var="styleDisplayMoyen" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRole.nombreCarateresMax == 2}">
                                        <c:set var="styleDisplayCourt" value="block"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="styleDisplayLong" value="block"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="valTmp"><c:out value="${clauseListeRole.valeurDefaut}"/></c:set>
                                <textarea name="valeurDefautTresLong"  title="Valeur par defaut" class="texte-tres-long mceEditor"  id="valeurDefaut${index}_4" cols="" rows="6" style="display:${styleDisplayTresLong}">${valTmp}</textarea>
                                <textarea name="valeurDefaut"  title="Valeur par defaut" class="texte-long mceEditor"  id="valeurDefaut${index}_1" cols="" rows="6" style="display:${styleDisplayLong}">${valTmp}</textarea>
                                <input type="text" value="${valTmp}" name="valeurDefautCourt"  title="Valeur par defaut" class="texte-court" id="valeurDefaut${index}_3" style="display:${styleDisplayMoyen}" />
                                <input type="text" value="${valTmp}" name="valeurDefautMoyen" title="Valeur par defaut" class="texte-long" id="valeurDefaut${index}_2"  style="display:${styleDisplayCourt}" />
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
            </table>
            <a href="javascript:AddRowTableExclusifSurcharge('table-formulation')"
               class="ajout-choix"><bean:message
                    key="ClauseListeChoixExclusif.btn.ajouterFormulation" />
            </a>
        </div>
        <div class="column">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.formulationModifiable" />
            </span>
            <div class="radio-choice">
                <html:radio property="formulationModifiable" title="Oui" value="1" styleId="formulationModifiable"/>
                <bean:message key="ClauseListeChoixExclusif.txt.formulationModifiableOui" />
            </div>
            <div class="radio-choice">
                <html:radio property="formulationModifiable" title="Non" value="0" styleId="formulationModifiable"/>
                <bean:message key="ClauseListeChoixExclusif.txt.formulationModifiableNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableDirection" />
            </span>
            <div class="radio-choice">
                <html:radio property="parametrableDirection" title="Oui" value="1" styleId="parametrableDirection" disabled="disabled"/>
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableDirectionOui" />
            </div>
            <div class="radio-choice">
                <html:radio property="parametrableDirection" title="Non" value="0" styleId="parametrableDirection" disabled="disabled"/>
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableDirectionNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableAgent" />
            </span>
            <div class="radio-choice">
                <html:radio styleId="parametrableAgent" property="parametrableAgent" title="Oui" value="1" disabled="disabled" />
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableAgentOui" />
            </div>
            <div class="radio-choice">
                <html:radio styleId="parametrableAgent" property="parametrableAgent" title="Non" value="0" disabled="disabled" />
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableAgentNon" />
            </div>
        </div>
        <div class="line">
            <div class="retour-ligne">
                <html:checkbox styleId="sautTextFixeApres" property="sautTextFixeApres" value="true" title="Saut de ligne" />
                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne" title="Saut de ligne" />
            </div>

        </div>
        <div class="separator"></div>
        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixExclusif.txt.texteFixeApres" />
            </span>
            <html:textarea property="textFixeApres" title="Texte fixe après"
                           cols="" rows="6" styleClass="texte-long mceEditor" styleId="textFixeApres"></html:textarea>
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();">
                    <img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>">
                </a>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();">
                <img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>">
            </a>
        </div>

        <div class="breaker"></div>
    </div>

    <div class="bottom">
        <span class="left"></span><span class="right"></span>
    </div>
</div>

<div class="spacer"></div>
<script type="text/javascript">
    initEditeursTexteRedactionSurchargeClauses();
</script>
