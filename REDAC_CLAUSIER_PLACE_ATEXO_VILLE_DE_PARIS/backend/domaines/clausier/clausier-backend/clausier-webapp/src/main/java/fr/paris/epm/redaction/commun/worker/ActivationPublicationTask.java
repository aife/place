package fr.paris.epm.redaction.commun.worker;

import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefValeurTypeClause;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.CanevasFacadeGWT;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.coordination.facade.PublicationClausierFacade;
import fr.paris.epm.redaction.importExport.mapper.PublicationClausierMapper;
import fr.paris.epm.redaction.metier.documentHandler.DerogationUtil;
import fr.paris.epm.redaction.metier.documentHandler.GenerationTableauUtil;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import fr.paris.epm.redaction.presentation.bean.PublicationClausierBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

/**
 * Tache générant l'activation d'une publication
 */
@Component
@Scope("prototype")
public class ActivationPublicationTask extends RsemTask<PublicationClausierBean> {

    private static final Logger logger = LoggerFactory.getLogger(ActivationPublicationTask.class);

    private Integer idPublication;

    private EpmTUtilisateur utilisateur;

    @Resource
    private PublicationClausierFacade publicationClausierFacade;

    @Resource
    private PublicationClausierMapper publicationClausierMapper;

    @Override
    public PublicationClausierBean work() throws Exception {

       return publicationClausierMapper.fromPublicationBean(publicationClausierFacade.activeClausierPublication(idPublication, utilisateur));
    }



    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public void setUtilisateur(EpmTUtilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

}
