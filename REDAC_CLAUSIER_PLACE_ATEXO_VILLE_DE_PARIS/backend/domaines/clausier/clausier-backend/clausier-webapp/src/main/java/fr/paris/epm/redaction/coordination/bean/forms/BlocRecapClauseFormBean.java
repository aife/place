package fr.paris.epm.redaction.coordination.bean.forms;

import java.io.Serializable;

/**
 * Bean coordination pour le formulaire BlocRecapClauseForm
 * @author Léon Barsamian
 *
 */
public class BlocRecapClauseFormBean implements Serializable {
    /**
     * Attribut Type de Clause .
     */
    private String typeClause;
    /**
     * Attribut Thème de la clause .
     */
    private String theme;
    /**
     * Attribut  procédure de Passation .
     */
    private String procedurePassation;
    /**
     * Attribut  nature de Préstation .
     */
    private String naturePrestation;
    /**
     * Attribut  mots Clés .
     */
    private String motsCles;
    /**
     * Attribut Type de Document .
     */
    private String typeDocument;
    /**
     * Attribut statut de la Clause .
     */
    @Deprecated
    private String statutClause;
    /**
     * Attribut Potentiellement conditionnée .
     */
    private String potCdt;
    /**
     * Attribut info Bulle.
     */
    private String infoBulle;
    
    /**
     * Attribut compatibilité entité adjudicatrice.
     */
    private String compatibleEntiteAdjudicatrice;
    
    /**
     * Reference de la clause
     */
    private String reference;

    /**
     * Editeur ou Client
     */
    private String auteur;
    
    /**
     * Statut de rédaction de la clause
     */
    private Integer idStatutRedactionClausier;
    
    /**
     * Contenu récapitulatif de la clause
     */
    private String contenu;

    private String dateCreation;

    private String dateModification;

    private String lastVersion;
    
    /**
     * @return the typeClause
     */
    public final String getTypeClause() {
        return typeClause;
    }


    /**
     * cette méthode permet de positionner le type
     * d'une clause avec une valeur donnée.
     * @param valeur initialise le type de document.
     */
    public final void setTypeClause(final String valeur) {
        typeClause = valeur;
    }

    /**
     * cette méthode renvoie le thème de la clause.
     * @return  renvoyer le thème de la clause.
     */
    public final String getTheme() {
        return theme;
    }

    /**
     * cette méthode  positionne le thème
     * d'une clause avec une valeur donnée.
     * @param valeur initialise le thème de la clasue.
     */
    public final void setTheme(final String valeur) {
        theme = valeur;
    }

    /**
     * cette méthode renvoie la procédure de passation.
     * @return  renvoyer la procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * cette méthode  positionne la procédure de passation
     * avec une valeur donnée.
     * @param valeur initialise la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * cette méthode renvoie la nature de prestation.
     * @return  renvoyer la nature de prestation.
     */
    public final String getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * cette méthode  positionne la nature de prestation
     * avec une valeur donnée.
     * @param valeur initialise la nature de prestation.
     */
    public final void setNaturePrestation(final String valeur) {
        naturePrestation = valeur;
    }

    /**
     * cette méthode renvoie les mots clés de la clause.
     * @return  renvoyer les mots clés de la clause.
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * cette méthode  positionne les mots clés de la clause
     * avec une valeur donnée.
     * @param valeur initialise les mots clés de la clause.
     */
    public final void setMotsCles(final String valeur) {
        motsCles = valeur;
    }

    /**
     * cette méthode renvoie le type de document de la clause.
     * @return  renvoyer le type de document de la clause.
     */
    public final String getTypeDocument() {
        return typeDocument;
    }

    /**
     * cette méthode  positionne le type de document de la clause
     * avec une valeur donnée.
     * @param valeur initialise le type de document de la clause.
     */
    public final void setTypeDocument(final String valeur) {
        typeDocument = valeur;
    }

    /**
     * cette méthode renvoie le statut de la clause.
     * @return  renvoyer le statut de la clause.
     */
    @Deprecated
    public final String getStatutClause() {
        return statutClause;
    }

    /**
     * cette méthode  positionne le Statut de la clause
     * avec une valeur donnée.
     * @param valeur initialise le statut de la clause.
     */
    @Deprecated
    public final void setStatutClause(final String valeur) {
        statutClause = valeur;
    }

    /**
     * cette méthode renvoie le Potentiellement conditionnée.
     * @return  renvoyer le Potentiellement conditionnée.
     */
    public final String getPotCdt() {
        return potCdt;
    }

    /**
     * cette méthode  positionne le Potentiellement
     * conditionnée de la clause avec une valeur donnée.
     * @param valeur initialise le Potentiellement conditionnée.
     */
    public final void setPotCdt(final String valeur) {
        potCdt = valeur;
    }

    /**
     *  cette méthode renvoie l'info-bulle de la clause.
     * @return  renvoyer l'info-bulle de la clause.
     */
    public final String getInfoBulle() {
        return infoBulle;
    }

    /**
     * cette méthode  positionne l'info-bulle
     * de la clause avec une valeur donnée.
     * @param valeur initialise l'info-bulle de la clause.
     */
    public final void setInfoBulle(final String valeur) {
        infoBulle = valeur;
    }


    /**
     * @return the compatibleEntiteAdjudicatrice
     */
    public final String getCompatibleEntiteAdjudicatrice() {
        return compatibleEntiteAdjudicatrice;
    }


    /**
     * @param compatibleEntiteAdjudicatrice the compatibleEntiteAdjudicatrice to set
     */
    public final void setCompatibleEntiteAdjudicatrice(final String valeur) {
        this.compatibleEntiteAdjudicatrice = valeur;
    }


    /**
     * @return the contenuClause
     */
    public final String getContenu() {
        return contenu;
    }

    /**
     * @param contenuClause the contenuClause to set
     */
    public final void setContenu(final String valeur) {
        this.contenu = valeur;
    }

    /**
     * @return the reference
     */
    public final String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public final void setReference(final String valeur) {
        this.reference = valeur;
    }

    /**
     * @return the auteur
     */
    public final String getAuteur() {
        return auteur;
    }

    /**
     * @param auteur the auteur to set
     */
    public final void setAuteur(final String valeur) {
        this.auteur = valeur;
    }


    /**
     * @return the idStatutRedactionClausier
     */
    public final Integer getIdStatutRedactionClausier() {
        return idStatutRedactionClausier;
    }


    /**
     * @param idStatutRedactionClausier the idStatutRedactionClausier to set
     */
    public final void setIdStatutRedactionClausier(final Integer valeur) {
        this.idStatutRedactionClausier = valeur;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getDateModification() {
        return dateModification;
    }

    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

}