package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeClause;
import fr.paris.epm.noyau.persistance.redaction.EpmTRoleClauseAbstract;

/**
 * Facade commune de gestion des Clause Niveau Interministeriel et Niveau Ministeriel
 * Created by nty on 31/08/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public interface ClauseFacadeCommun {

    public default String makeContext(EpmTClauseAbstract epmTClause) {
        StringBuilder sb = new StringBuilder();
        if (epmTClause.getTexteFixeAvant() != null)
            sb.append(epmTClause.getTexteFixeAvant());

        if (epmTClause.getEpmTRoleClauses() != null && !epmTClause.getEpmTRoleClauses().isEmpty()) {

            if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF
                    || epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                // Dans le cas d'une Clause-Liste
                for (EpmTRoleClauseAbstract epmTRoleClause : epmTClause.getEpmTRoleClausesTrie())
                    sb.append(sb.length() > 0 ? "\n" : "").append(epmTRoleClause.getValeurDefaut());

            } else {
                // Dans le cas d'une Clause n'est pas une Liste alors on a forcement une EpmTRoleClause
                EpmTRoleClauseAbstract epmTRoleClause = epmTClause.getEpmTRoleClausesTrie().iterator().next();

                if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE) {
                    // Dans le cas d'une clause est Valeur Heritée
                    /* TODO : NIKO
                    EpmTRefValeurTypeClause valeur = getService().chercherObject(Integer.parseInt(epmTRoleClause.getValeurDefaut()), EpmTRefValeurTypeClause.class);
                    if (valeur != null)
                        sb.append(sb.length() > 0 ? " " : "").append("[ ").append(valeur.getLibelle()).append(" ]");
                    */
                    sb.append(sb.length() > 0 ? " " : "").append("[ valeur heritée ]");
                } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
                    // Dans le cas d'une clause est Texte Prévalorisé
                    sb.append(sb.length() > 0 ? " " : "").append(epmTRoleClause.getValeurDefaut());
                } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE) {
                    // Dans le cas d'une clause est Texte Libre
                    sb.append(sb.length() > 0 ? " " : "").append("[ texte libre ]");
                }
            }
        }

        if (epmTClause.getTexteFixeApres() != null)
            sb.append(sb.length() > 0 ? "\n" : "").append(epmTClause.getTexteFixeApres());

        //Filtrer tout le contenu html.
        String context = sb.toString();
        context = context.replaceAll("<br[^>]*>", " ");
        context = context.replaceAll("<ul>", " ");
        context = context.replaceAll("</li>", " ");
        context = context.replaceAll("</ul>", " ");
        context = context.replaceAll("<[^>]*>", "");

        if (context.length() > 300)
            context = context.substring(0, 296) + "...";
        return context;
    }

    public default String makeContextHtml(EpmTClauseAbstract epmTClause) {
        StringBuilder sb = new StringBuilder();
        if (epmTClause.getTexteFixeAvant() != null)
            sb.append(epmTClause.getTexteFixeAvant());

        if (epmTClause.getEpmTRoleClauses() != null && !epmTClause.getEpmTRoleClauses().isEmpty()) {

            if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_CUMMULATIF
                    || epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_LISTE_EXCLUSIF) {
                // Dans le cas d'une Clause-Liste
                for (EpmTRoleClauseAbstract epmTRoleClause : epmTClause.getEpmTRoleClausesTrie())
                    sb.append(sb.length() > 0 ? "\n" : "").append(epmTRoleClause.getValeurDefaut());

            } else {
                // Dans le cas d'une Clause n'est pas une Liste alors on a forcement une EpmTRoleClause
                EpmTRoleClauseAbstract epmTRoleClause = epmTClause.getEpmTRoleClausesTrie().iterator().next();

                if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_VALEUR_HERITEE) {
                    // Dans le cas d'une clause est Valeur Heritée
                    /* TODO : NIKO
                    EpmTRefValeurTypeClause valeur = getService().chercherObject(Integer.parseInt(epmTRoleClause.getValeurDefaut()), EpmTRefValeurTypeClause.class);
                    if (valeur != null)
                        sb.append(sb.length() > 0 ? " " : "").append("[ ").append(valeur.getLibelle()).append(" ]");
                    */
                    sb.append(sb.length() > 0 ? " " : "").append("[ valeur heritée ]");
                } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_PREVALORISE) {
                    // Dans le cas d'une clause est Texte Prévalorisé
                    sb.append(sb.length() > 0 ? " " : "").append(epmTRoleClause.getValeurDefaut());
                } else if (epmTClause.getEpmTRefTypeClause().getId() == EpmTRefTypeClause.TYPE_CLAUSE_TEXTE_LIBRE) {
                    // Dans le cas d'une clause est Texte Libre
                    sb.append(sb.length() > 0 ? " " : "").append("[ texte libre ]");
                }
            }
        }

        if (epmTClause.getTexteFixeApres() != null)
            sb.append(sb.length() > 0 ? "\n" : "").append(epmTClause.getTexteFixeApres());

        //Filtrer tout le contenu html.
        return sb.toString();

    }

}
