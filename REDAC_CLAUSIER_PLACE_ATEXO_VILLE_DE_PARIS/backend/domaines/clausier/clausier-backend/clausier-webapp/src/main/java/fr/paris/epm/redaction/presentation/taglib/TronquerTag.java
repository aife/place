/**
 * 
 */
package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.redaction.commun.Util;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant de tronquer un mot.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class TronquerTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * nombre maximal de caractères.
     */
    private String max;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(TronquerTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           null);
            if (texte == null) {
                return Tag.SKIP_BODY;
            }
            if (max == null) {
                max = "60";
            }
            pageContext.getOut()
                    .print(
                           Util.tronquer((String) texte, Integer
                                   .parseInt(max)));

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
        max = null;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        name = valeur;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

    /**
     * @param valeur nombre de caratère maximal.
     */
    public final void setMax(final String valeur) {
        max = valeur;
    }

}
