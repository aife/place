<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="atexo"   uri="AtexoTag"  %>

<script src="webjars/jquery/3.2.1/jquery.min.js"></script>

<c:set var="plateformeEditeur" scope="session" value="" />
<atexo:parametrage clef="plateforme.editeur" testEgal="0">
    <c:set var="plateformeEditeur" scope="session" value="false"/>
</atexo:parametrage>
<atexo:parametrage clef="plateforme.editeur" testEgal="1">
    <c:set var="plateformeEditeur" scope="session" value="true"/>
</atexo:parametrage>
<!-- plateforme is "${plateformeEditeur}" -->

<!-- Fenêtre-modale de confirmation -->
<jsp:include page="../modalConfirmation.jsp" />

<!--Debut main-part-->
<div class="main-part">
    <div class="breadcrumbs m-b-2">
        <strong>
            <spring:message code="publicationClausier.listPublicationClausier.titre" />
        </strong>
    </div>

    <jsp:include page="../message.jsp" />

    <!-- Bloc recherche Publication Clausier -->
    <jsp:include page="searchPublicationClausier.jsp" />

    <div class="page-header">
        <h2 class="h4 m-0"><spring:message code="publicationClausier.listPublicationClausier.historique"/></h2>
    </div>

    <jsp:include page="listPublicationClausierBody.jsp" />

    <c:if test="${empty plateformeEditeur or not plateformeEditeur}">
        <jsp:include page="importPublicationClausier.jsp" />
    </c:if>
    <c:if test="${empty plateformeEditeur or plateformeEditeur}">
        <jsp:include page="exportPublicationClausier.jsp" />
    </c:if>

    <script>

        jQuery('.js-scroll-to-searchInstance').click(function() {
            scrollerToId('#searchInstance');
        });

        jQuery(document).ready(function() {
            miseAJourTailleIFrame();
        });

    </script>

</div>
<!--Fin main-part-->