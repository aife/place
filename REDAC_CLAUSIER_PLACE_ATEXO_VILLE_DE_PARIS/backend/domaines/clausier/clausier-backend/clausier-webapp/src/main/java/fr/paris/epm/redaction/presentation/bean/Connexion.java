package fr.paris.epm.redaction.presentation.bean;

import java.util.Timer;

public class Connexion {

	private Timer timer;
	private Long clients;

	public Connexion( Timer timer ) {
		this.timer = timer;
		this.clients = 1L;
	}

	public void inc() {
		this.clients++;
	}

	public void dec() {
		this.clients--;
	}

	public boolean vide() {
		return this.clients == 0;
	}

	public Timer getTimer() {
		return timer;
	}

	public Long getClients() {
		return clients;
	}
}
