package fr.paris.epm.redaction.webservice.dto;

public class ReferentielDTO {
	private String code;

	private String libelle;

	private Integer ordre;

	private ReferentielDTO parent;

	private Long id;

	public ReferentielDTO() {
	}

	public String getCode() {
		return code;
	}

	public void setCode( final String code ) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( final String libelle ) {
		this.libelle = libelle;
	}

	public Integer getOrdre() {
		return ordre;
	}

	public void setOrdre( final Integer ordre ) {
		this.ordre = ordre;
	}

	public ReferentielDTO getParent() {
		return parent;
	}

	public void setParent( final ReferentielDTO parent ) {
		this.parent = parent;
	}

	public Long getId() {
		return id;
	}

	public void setId( final Long id ) {
		this.id = id;
	}
}
