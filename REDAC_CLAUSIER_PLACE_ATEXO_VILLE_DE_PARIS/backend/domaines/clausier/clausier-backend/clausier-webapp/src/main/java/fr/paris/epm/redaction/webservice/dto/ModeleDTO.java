package fr.paris.epm.redaction.webservice.dto;


public class ModeleDTO {
    private Long id;

    private String libelle;

    private String nomFichier;

    private ReferentielDTO type;

    private int ordre;

    private Boolean administrable;

    private boolean actif;

	public ModeleDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle( String libelle ) {
		this.libelle = libelle;
	}

	public String getNomFichier() {
		return nomFichier;
	}

	public void setNomFichier( String nomFichier ) {
		this.nomFichier = nomFichier;
	}

	public ReferentielDTO getType() {
		return type;
	}

	public void setType( ReferentielDTO type ) {
		this.type = type;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre( int ordre ) {
		this.ordre = ordre;
	}

	public Boolean getAdministrable() {
		return administrable;
	}

	public void setAdministrable( Boolean administrable ) {
		this.administrable = administrable;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif( boolean actif ) {
		this.actif = actif;
	}
}
