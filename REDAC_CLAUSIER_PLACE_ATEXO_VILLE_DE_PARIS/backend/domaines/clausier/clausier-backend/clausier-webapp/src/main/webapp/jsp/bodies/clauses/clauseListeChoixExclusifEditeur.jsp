<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--Debut bloc Infos clause-->
<div class="form-bloc">
    <div class="top">
        <span class="left"></span><span class="right"></span>
    </div>
    <div class="breaker"></div>

    <div class="content">

        <h2 class="float-left">
            <html:radio property="clauseSelectionnee" styleId="clauseEditeurSelectionnee" value="clauseEditeur"></html:radio>
            <label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeur"/></label>
        </h2>
        <div class="actions-clause no-padding">
            <logic:equal name="typeAction" value="M">
                <%-- <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}"> --%>
                <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                <%-- </c:if> --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>

        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixExclusif.txt.texteFixeAvant" />
            </span>

            <textarea class="texte-long mceEditorSansToolbar" rows="6" cols=""
                      title="Texte fixe avant" id="textFixeAvantEditeur"
                      name="textFixeAvantEditeur" disabled="disabled">${clauseListeChoixExclusifFormEditeur.textFixeAvant}</textarea>
        </div>
        <div class="line">
            <div class="retour-ligne">

                <input type="checkbox" id="sautTextFixeAvantEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.sautTextFixeAvant == 'true'}">checked="checked"</c:if> disabled="disabled" title="Saut de ligne"/>

                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
                     title="Saut de ligne" />
            </div>
        </div>
        <div class="separator"></div>
        <div class="clause-choix-list">
            <table id="table-formulation-editeur">
                <thead>
                <tr>
                    <th class="num-formulation">
                        <bean:message key="ClauseListeChoixExclusif.txt.numFormulation" />
                    </th>
                    <th class="nb-carac">
                        <bean:message  key="ClauseListeChoixExclusif.txt.tailleChamp" />
                    </th>
                    <th class="check-col">
                        <bean:message  key="ClauseListeChoixExclusif.txt.precochee" />
                    </th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <logic:notEmpty name="clauseListeChoixExclusifFormEditeur"
                                property="formulationCollection">
                    <script type="text/javascript">
                        <bean:size id="sizeListEditeur" name="clauseListeChoixExclusifFormEditeur" property="formulationCollection"/>
                        var compteurEditeur = ${sizeListEditeur} + 1;
                    </script>

                    <logic:iterate name="clauseListeChoixExclusifFormEditeur"
                                   property="formulationCollection" id="clauseListeRoleEditeur"
                                   indexId="indexEditeur">
                        <bean:define id="lignePaire"><%=(indexEditeur.intValue()%2)%></bean:define>
                        <tr <logic:notEqual name="lignePaire" value="0">class ="on"</logic:notEqual> id="editeur_tr_<%=indexEditeur.intValue() + 1%>">
                            <td class="num-formulation">
                                <input type="text"
                                       value="<nested:write name="clauseListeRoleEditeur" property="numFormulation"/>"
                                       title="Numéro de formulation" disabled="disabled">
                            </td>
                            <td class="nb-carac">
                                <div class="radio-choice-small">
                                    <select id="tailleChampEditeur_${indexEditeur}" name="tailleChampEditeur" class="auto" title="Taille du Champ" onchange="javascript:displayOptionChoiceWithTinyMCE(this, 'editeur_valeurDefaut${indexEditeur}_', 'mceEditorSansToolbar');" disabled="disabled">
                                        <option value="4" <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="4">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.tresLong" />
                                        </option>
                                        <option value="1" <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="1">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.long" />
                                        </option>
                                        <option value="2" <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="2">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.moyen" />
                                        </option>
                                        <option value="3" <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="3">selected="selected"</nested:equal>>
                                            <bean:message key="redaction.taille.champ.court" />
                                        </option>
                                    </select>
                                </div>
                            </td>
                            <td class="check-col" style="text-align: center">
                                <logic:equal name="clauseListeRoleEditeur" property="precochee" value="0">
                                    <input  type="radio" name="precochee" value="${indexEditeur}" disabled="disabled">
                                </logic:equal>
                                <logic:equal name="clauseListeRoleEditeur" property="precochee" value="1">
                                    <input type="radio" name="precochee" value="${indexEditeur}" checked="checked" disabled="disabled">
                                </logic:equal>
                            </td>
                            <td class="actions">
                                <a class="disabled-link" href="#">
                                    <img title="Supprimer la formulation (inactif)" alt="Supprimer la formulation (inactif)" src="<atexo:href href='images/picto-poubelle.gif'/>">
                                </a>
                            </td>
                        </tr>

                        <nested:equal name="clauseListeRoleEditeur" property="nombreCarateresMax" value="0">
                            <script>
                                var idTailleChamp = 'tailleChampEditeur_${indexEditeur}';
                                mettreTailleChampValeurDefaut(idTailleChamp);
                            </script>
                        </nested:equal>

                        <tr <logic:notEqual name="lignePaire" value="0">class ="on"</logic:notEqual> id="editeur_tr_<%=indexEditeur.intValue() + 1%>">
                            <td class="col-champ" colspan="4">
                                <div class="intitule"><bean:message key="ClauseListeChoixCumulatif.txt.valeurParDefaut"/></div>
                                <c:set var="styleDisplayTresLong" value="none"/>
                                <c:set var="styleDisplayLong" value="none"/>
                                <c:set var="styleDisplayMoyen" value="none"/>
                                <c:set var="styleDisplayCourt" value="none"/>
                                <c:choose>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 4}">
                                        <c:set var="styleDisplayTresLong" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 3}">
                                        <c:set var="styleDisplayMoyen" value="block"/>
                                    </c:when>
                                    <c:when test="${clauseListeRoleEditeur.nombreCarateresMax == 2}">
                                        <c:set var="styleDisplayCourt" value="block"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="styleDisplayLong" value="block"/>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="valTmp"><c:out value="${clauseListeRoleEditeur.valeurDefaut}"/></c:set>
                                <textarea disabled="disabled" name="valeurDefautTresLongEditeur"  title="Valeur par defaut" class="texte-tres-long mceEditorSansToolbar"  id="editeur_valeurDefaut${indexEditeur}_4" cols="" rows="6" style="display:${styleDisplayTresLong}">${valTmp}</textarea>
                                <textarea disabled="disabled" name="valeurDefautEditeur"  title="Valeur par defaut" class="texte-long mceEditorSansToolbar"  id="editeur_valeurDefaut${indexEditeur}_1" cols="" rows="6" style="display:${styleDisplayLong}">${valTmp}</textarea>
                                <input disabled="disabled" type="text" value="${valTmp}" name="valeurDefautCourtEditeur"  title="Valeur par defaut" class="texte-court" id="editeur_valeurDefaut${indexEditeur}_3" style="display:${styleDisplayMoyen}" />
                                <input disabled="disabled" type="text" value="${valTmp}" name="valeurDefautMoyenEditeur" title="Valeur par defaut" class="texte-long" id="editeur_valeurDefaut${indexEditeur}_2"  style="display:${styleDisplayCourt}" />
                            </td>
                        </tr>
                    </logic:iterate>
                </logic:notEmpty>
            </table>
            <a class="ajout-choix ajout-choix-inactive disabled-link" title="<bean:message key="ClauseListeChoixCumulatif.btn.ajouterFormulation" />"">
            <bean:message key="ClauseListeChoixCumulatif.btn.ajouterFormulation" />
            </a>
        </div>
        <div class="column">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.formulationModifiable" />
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="formulationModifiableEditeur" name="formulationModifiableEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.formulationModifiable == '1'}">checked="checked"</c:if>/>

                <bean:message
                        key="ClauseListeChoixExclusif.txt.formulationModifiableOui" />
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Non" id="formulationModifiableEditeur" name="formulationModifiableEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.formulationModifiable == '0'}">checked="checked"</c:if>/>
                <bean:message key="ClauseListeChoixExclusif.txt.formulationModifiableNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableDirection" />
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="parametrableDirectionEditeur" name="parametrableDirectionEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.parametrableDirection == '1'}">checked="checked"</c:if>/>
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableDirectionOui" />
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Non" id="parametrableDirectionEditeur" name="parametrableDirectionEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.parametrableDirection == '0'}">checked="checked"</c:if>/>
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableDirectionNon" />
            </div>
        </div>
        <div class="line">
            <span class="intitule">
                <bean:message key="ClauseListeChoixExclusif.txt.parametrableAgent" />
            </span>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="parametrableAgent" name="parametrableAgentEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.parametrableAgent == '1'}">checked="checked"</c:if>/>
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableAgentOui" />
            </div>
            <div class="radio-choice">
                <input disabled="disabled" type="radio" title="Oui" id="parametrableAgent" name="parametrableAgentEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.parametrableAgent == '0'}">checked="checked"</c:if>/>
                <bean:message
                        key="ClauseListeChoixExclusif.txt.parametrableAgentNon" />
            </div>
        </div>
        <div class="line">
            <div class="retour-ligne">
                <input type="checkbox" id="sautTextFixeApresEditeur"
                       <c:if test="${clauseListeChoixExclusifFormEditeur.sautTextFixeApres == 'true'}">checked="checked"</c:if> disabled="disabled" title="Saut de ligne"/>

                <bean:message key="redaction.txt.sautLigne" />
                <img src="<atexo:href href='images/picto-retour-ligne.gif'/>" alt="Saut de ligne"
                     title="Saut de ligne" />
            </div>

        </div>
        <div class="separator"></div>
        <div class="line">
            <span class="intitule-bloc">
                <bean:message key="ClauseListeChoixExclusif.txt.texteFixeApres" />
            </span>

            <textarea class="texte-long mceEditorSansToolbar" rows="6" cols=""
                      title="Texte fixe après" id="textFixeApresEditeur"
                      name="texteFixeApresEditeur">${clauseListeChoixExclusifFormEditeur.textFixeApres}</textarea>
        </div>

        <div class="actions-clause">
            <logic:equal name="typeAction" value="M">
                <%-- <c:if test="${epmTClauseInstance.clauseEditeur && epmTClauseInstance.premierePublicationClausier != null && epmTClauseInstance.premierePublicationClausier.datePublication != null}">			 --%>
                <a title="<bean:message key="clause.txt.historiqueVersions"/>" href="javascript:popUp('historiquePublicationClausier.epm?clauseId=${idClauseEditeur}','yes')"><img src="<atexo:href href='images/bouton-historique.gif'/>" alt="Historique"></a>
                <%-- </c:if> --%>
            </logic:equal>
            <a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeur();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
        </div>

        <div class="breaker"></div>
    </div>


    <div class="bottom">
        <span class="left"></span><span class="right"></span>
    </div>
</div>

<div class="spacer"></div>
<script type="text/javascript">
    initEditeursTexteSansToolbarRedaction();
</script>
