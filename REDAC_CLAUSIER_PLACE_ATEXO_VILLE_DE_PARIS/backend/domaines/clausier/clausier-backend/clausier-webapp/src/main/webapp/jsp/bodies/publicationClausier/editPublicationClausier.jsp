<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>

<!--Debut main-part-->
<div class="main-part">
    <script type="text/javascript">
        $(function(){
            $('#version1,#version2,#version3').keyup(function(e){
                if($(this).val().length==$(this).attr('maxlength'))
                    $(this).next(':input').focus()
            })
        })
    </script>
    <div class="breadcrumbs m-b-2">
        <strong>
            <spring:message code="publicationClausier.editPublicationClausier.titre"/>
        </strong>
    </div>
    <jsp:include page="../message.jsp" />

    <jsp:include page="../modalConfirmation.jsp" />

    <!--Debut bloc-->
    <%--@elvariable id="publication" type="fr.paris.epm.redaction.presentation.bean.PublicationClausierBean"--%>
    <form:form modelAttribute="publication" action="savePublicationClausier.htm" method="post" cssClass="form-horizontal">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="section clearfix">
                    <div class="section-header">
                        <div class="page-header m-t-2">
                            <div class="h5 text-primary">
                                <spring:message code="publicationClausier.editPublicationClausier.publication"/>
                            </div>
                        </div>
                    </div>

                    <div class="section-body">
                        <div class="form-group form-group-sm">
                            <form:label path="version" class="control-label col-md-2 col-xs-3">
                                <spring:message code="publicationClausier.editPublicationClausier.numeroVersion" />
                            </form:label>
                            <div class="col-md-2 col-xs-2">
                                <div class="form-group form-group-sm">
                                    <div class="input-group" style="display: inline-flex;">
                                        <form:input path="version1" cssClass="form-control" maxlength="2" pattern="[0-9][0-9]" id="version1" name="version1"/>
                                        <form:input path="version2" cssClass="form-control" maxlength="2" pattern="[0-9][0-9]" id="version2" name="version2"/>
                                        <form:input path="version3" cssClass="form-control" maxlength="2" pattern="[0-9][0-9]"  id="version3" name=" version3 "/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <form:label path="commentaire" class="control-label col-md-2 col-xs-3">
                                <spring:message code="publicationClausier.editPublicationClausier.commentaire" /> :
                            </form:label>
                           <div class="col-md-6 p-0 col-xs-6">
                               <form:textarea path="commentaire" cssClass="form-control input-sm" cssErrorClass="form-control input-sm error-border" cssStyle="height: 120px;"/>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <button type="submit" class="btn btn-primary btn-sm pull-right" onclick="ATX.component.loader.show()">
                <spring:message code="publicationClausier.editPublicationClausier.publier" />
            </button>
        </div>
    </form:form>
    <!--Fin bloc-->
</div>
<!--Debut main-part-->