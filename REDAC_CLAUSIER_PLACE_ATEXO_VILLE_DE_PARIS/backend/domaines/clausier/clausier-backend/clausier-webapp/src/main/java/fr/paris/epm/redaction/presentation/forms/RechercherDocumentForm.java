package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.apache.struts.action.ActionForm;

import java.util.Collection;
import java.util.List;

/**
 * Formulaire Rechercher Document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */

public class RechercherDocumentForm extends ActionForm {

    /**
     * marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut numéro de consultation.
     */
    private String numConsultation;
    /**
     * Attribut pouvoir adjudicateur.
     */
    private String pouvoirAdjudicateur;
    /**
     * Attribut direction service.
     */
    private String directionService;
    /**
     * Attribut procédure de passation.
     */
    private String procedurePassation;
    /**
     * Attribut nature de Prestation.
     */
    private String naturePrestas;
    /**
     * Attribut mots clés.
     */
    private String motCle;
    /**
     * Attribut collection de listes de document.
     */
    private Collection listeDocument;
    /**
     * Attribut collection de natures de prestation.
     */
    private Collection naturePrestasCollection;
    /**
     * Attribut collection des procédure de préstation.
     */
    private List<EpmTRefProcedure> procedureCollection;
    /**
     * Attribut collection de pouvoirs adjudicateur.
     */
    private Collection pouvoirAdjudicateurCollection;
    /**
     * collection de directions de service.
     */
    private Collection directionServiceCollection;

    /**
     * Constructeur de la classe RechercherDocumentForm.
     */
    public RechercherDocumentForm() {
        this.reset();
    }

    /**
     * Cette méthode permet d'initialiser le formulaire .
     */
    public void reset() {
        pouvoirAdjudicateur = "";
        numConsultation = "";
        listeDocument = null;
        directionService = null;
        procedurePassation = null;
        naturePrestas = null;
        motCle = null;
        naturePrestasCollection = null;
        procedureCollection = null;
        pouvoirAdjudicateurCollection = null;
        directionServiceCollection = null;
    }

    /**
     * @return Collection de natures de préstation .
     */
    public final Collection getNaturePrestasCollection() {
        return naturePrestasCollection;
    }

    /**
     * @param valeur positionne une collection de natures de préstation.
     */
    public final void setNaturePrestasCollection(final Collection valeur) {
        naturePrestasCollection = valeur;
    }

    public List<EpmTRefProcedure> getProcedureCollection() {
        return procedureCollection;
    }

    public void setProcedureCollection(List<EpmTRefProcedure> procedureCollection) {
        this.procedureCollection = procedureCollection;
    }

    /**
     * @return collection de listes de document.
     */
    public final Collection getListeDocument() {
        return listeDocument;
    }

    /**
     * @param valeur positionne une collection de listes de document.
     */
    public final void setListeDocument(final Collection valeur) {
        listeDocument = valeur;
    }

    /**
     * @return numéro de consultation.
     */
    public final String getNumConsultation() {
        return numConsultation;
    }

    /**
     * @param valeur positionne un numéro de consultation.
     */
    public final void setNumConsultation(final String valeur) {
        numConsultation = valeur;
    }

    /**
     * @return pouvoir adjudicateur
     */
    public final String getPouvoirAdjudicateur() {
        return pouvoirAdjudicateur;
    }

    /**
     * @param valeur positionne un pouvoir adjudicateur.
     */
    public final void setPouvoirAdjudicateur(final String valeur) {
        pouvoirAdjudicateur = valeur;
    }

    /**
     * @return direction service.
     */
    public final String getDirectionService() {
        return directionService;
    }

    /**
     * @param valeur initialise l'attribut direction/service.
     */
    public void setDirectionService(final String valeur) {
        directionService = valeur;
    }

    /**
     * @return procédure de passation.
     */
    public final String getProcedurePassation() {
        return procedurePassation;
    }

    /**
     * @param valeur positionne la procédure de passation.
     */
    public final void setProcedurePassation(final String valeur) {
        procedurePassation = valeur;
    }

    /**
     * @return nature de préstation.
     */
    public final String getNaturePrestas() {
        return naturePrestas;
    }

    /**
     * @param valeur postionne la nature de préstation.
     */
    public final void setNaturePrestas(final String valeur) {
        naturePrestas = valeur;
    }

    /**
     * @return mots clés.
     */
    public final String getMotCle() {
        return motCle;
    }

    /**
     * @param valeur positionne les mots-clés.
     */
    public final void setMotCle(final String valeur) {
        motCle = valeur;
    }

    /**
     * @return Collection de pouvoirs adjudicateur.
     */
    public final Collection getPouvoirAdjudicateurCollection() {
        return pouvoirAdjudicateurCollection;
    }

    /**
     * @param valeur positionne une collection de pouvoirs adjudicateur.
     */
    public final void setPouvoirAdjudicateurCollection(final Collection valeur) {
        pouvoirAdjudicateurCollection = valeur;
    }

    /**
     * @return Collection de directions/Services.
     */
    public final Collection getDirectionServiceCollection() {
        return directionServiceCollection;
    }

    /**
     * @param valeur positionne une collection de directions/Services.
     */
    public final void setDirectionServiceCollection(final Collection valeur) {
        directionServiceCollection = valeur;
    }
}
