package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.facade.Facade;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.bean.CanevasSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;

public interface CanevasFacade extends Facade<CanevasBean> {


    PageRepresentation<CanevasBean> findCanevas(CanevasSearch search, EpmTRefOrganisme epmTRefOrganisme);

    Canevas findCanevasById(Integer id);
}
