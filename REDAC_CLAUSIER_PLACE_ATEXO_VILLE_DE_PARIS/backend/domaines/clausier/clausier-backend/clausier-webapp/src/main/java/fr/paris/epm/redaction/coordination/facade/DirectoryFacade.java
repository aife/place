package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.coordination.bean.Directory;

import java.util.List;

/**
 * Facade de gestion des Annuaires du Module Rédaction
 *          - statuts redaction clausiers
 *          - type d'auteur clause : Editeur, Client, Editeur surchargée
 *          - type d'auteur canevas : Editeur, Client
 *          - type clause
 *          - type document
 *          - type contrat
 *          - procedure d'editeur
 *          - thèmes
 * Created by nty on 01/09/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface DirectoryFacade extends fr.paris.epm.global.coordination.facade.DirectoryFacade {

    /**
     * Retourne la liste des statuts redaction clausiers
     * @return liste des des statuts redaction clausiers
     */
    public List<Directory> findListStatutsRedactionClausier() ;

    /**
     * Retourne la liste des types d'auteur clause : Editeur, Client, Editeur surchargée
     * @return liste des types d'auteur clause : Editeur, Client, Editeur surchargée
     */
    public List<Directory> findListTypesAuteurClause() ;

    /**
     * Retourne la liste des types d'auteur canevas : Editeur, Client
     * @return liste des types d'auteur canevas : Editeur, Client
     */
    public List<Directory> findListTypesAuteurCanevas() ;

    public List<Directory> findListTypesClause() ;

    public List<Directory> findListTypesDocument() ;

    public List<Directory> findListTypesContrat() ;

    public List<Directory> findListThemesClause() ;

    public List<Directory> findListReferenceHeritee() ;
    public List<Directory> findListCritereCondition() ;

}
