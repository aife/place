/** partie reservee a la gestion interne **/
var mceElt = new Array();
var dirtyChamp = new Array();
var recappopup = null;
var excludeList = new Array();
var nombreSautLignes = 0;
var identifiantsChoixExclusifActifList = new Array();
var identifiantsChoixExclusifInactifList = new Array();
var identifiantsChoixMultipleActifList = new Array();
var identifiantsChoixMultipleInactifList = new Array();
var identifiantsLignesTableauDynamiqueList = new Array();
var chapitreIdentifiant = "";

function arrayIndexOf(array, value) {
	if (array != null) {
		var l = array.length;
		for ( var i = 0; i < l; i++) {
			if (array[i] == value) {
				return i;
			}
		}
	}
	return -1;
}

function getMCE(elt) {
	var mce = mceElt[elt];
	if (mce == null) {
		mceElt[elt] = tinyMCE.get(elt);
		return mceElt[elt];
	}
	return mce;
}

function isDirty(elt) {
	var mce = getMCE(elt);
	if (mce == null) {
		return;
	}
	var strOrig = dirtyChamp[elt];
	if (strOrig == null) {
		dirtyChamp[elt] = tinymce.trim(mce.getContent({
			format : 'raw',
			no_events : 1
		}));
		return true;
	}
	var content = tinymce.trim(mce.getContent({
		format : 'raw',
		no_events : 1
	}));
	dirtyChamp[elt] = content;
	return strOrig != content;
}

function getNomRecap(elt) {
	var e = jQuery('#' + elt);
	var nomrecap = e.attr('recapid');
	if (nomrecap == null) {
		nomrecap = e.attr('id');
		return nomrecap;
	}

	return null;
}

function checkTinyMCEElt(elt, isPremierClause) {
	if (isDirty(elt) && isPopUpOuverte()) {
		if (recappopup.closed) {
			recappopup = null;
			return;
		}
		//try{
		var nomrecap = getNomRecap(elt);
		if (nomrecap != null) {
			recappopup.actualize(nomrecap, getMCE(elt).getContent(), true, isPremierClause);
		}
		//}catch(e){}
	}
}

function isExcluded(editor) {
	if (editor == null) {
		return true;
	}
	return arrayIndexOf(excludeList, editor) >= 0;
}

function textChanged() {
	var isPremierClause = true;
	jQuery('textarea').each(function() {
		if (!isExcluded(jQuery(this).attr('id'))) {
			checkTinyMCEElt(jQuery(this).attr('id'), isPremierClause);
			
			if(isPremierClause) {
				isPremierClause = false;
			}
		}
	});
}

/**  fin partie interne **/
/** methode exposees pour l'intergration **/

function miseAJourPopupPosition(ed) {
	if(isPopUpOuverte()) {
		recappopup.scrollPopup(ed.id);
	}
}

function reset() {
	mceElt = new Array();
	dirtyChamp = new Array();
	excludeList = new Array();
}

function exclude(editor) {
	if (arrayIndexOf(excludeList, editor) < 0) {
		excludeList[excludeList.length] = editor;
	}
}

function include(editor) {
	var idx = arrayIndexOf(excludeList, editor);
	if (idx >= 0) {
		excludeList[idx] = null;
	}
}

function actualize(id, html, scroll) {
	if (recappopup.closed) {
		recappopup = null;
	}
	if (isPopUpOuverte()) {
		recappopup.actualize(id, html, scroll, false);
	}
}

function openPopUp(url, width, height) {
	recappopup = window.open(url, 'menubar=0,location=0,status=0,resizable=1,width=' + width + ',height=' + height);
	return recappopup;
}

function initialiserPopup(url, nbLignesSaut, chapitreId) {

	nombreSautLignes = nbLignesSaut;
	chapitreIdentifiant = chapitreId;
	ouvrirPopUp(url);
}

function ouvrirPopUp(url) {
	var strOptions = "toolbar=no,menubar=no,scrollbars=yes,resizable,location=no";
	recappopup = window.open(url, 'newWin', strOptions);
	recappopup.focus();
}

function isPopUpOuverte() {
	return !isEmpty(recappopup) && recappopup.opener != null;
}

function decocherCaseClauseChoixMultiple(id, style) {
	if (isPopUpOuverte() && id != null) {
		recappopup.document.getElementById(id).style.display = style;
		
		if (recappopup.document.getElementById(id+"_br") != null) {
			recappopup.document.getElementById(id+"_br").style.display = style;
		}
	}
}

function actualiserPrevisualisation(id) {
	if (isPopUpOuverte() && document.getElementById(id) != null) {
		recappopup.actualizeTableauDynamiqueTextarea(id, document.getElementById(id).value, true);
	}
}

/**
 * Ajoute une nouvelle ligne du tableau dynamique lors de la prévisualisation dynamique
 * @param idBody 
 * @param nouveauId
 */
function ajouterLigne(idBody, nouveauId, indexLigneACloner) {
	if (isPopUpOuverte()) {
		var bodyElem = recappopup.document.getElementById(idBody);
		var nodeList = bodyElem.childNodes;
		if (nodeList.length > 0) {
			var ligneADupliquer = nodeList[indexLigneACloner];
			var nouvelleLigne = ligneADupliquer.cloneNode();
			//nettoyer les ids
			nouvelleLigne.id = nouveauId;
			bodyElem.insertBefore(nouvelleLigne, ligneADupliquer);
		}
	}
}

/**
 * Suppression d'une ligne du tableau dynamique lors de la prévisualisation dynamique
 */
function supprimerLigne(idBody, indexLigne) {
	var bodyElem = recappopup.document.getElementById(idBody);
	var nodeList = bodyElem.childNodes;
	bodyElem.removeChild(nodeList[indexLigne]);
}

function ajouterSautLigne(nombreLignes, chapitreId) {
	// mise a jour saut de ligne
	nombreSautLignes = nombreLignes;
	chapitreIdentifiant = chapitreId;
	
	if(chapitreIdentifiant.indexOf(".",0) != -1) {
		if (isPopUpOuverte()) {
			var divSautLigne;
	
			
				// si il y a déjà de saut de ligne
				if (recappopup.document.getElementById('sautLigne_'
						+ chapitreId) != null) {
					divSautLigne = recappopup.document
							.getElementById('sautLigne_' + chapitreId);
					divSautLigne.innerHTML = '';
				} else {
					divSautLigne = recappopup.document.createElement("li");
					divSautLigne.setAttribute("class", "saut-ligne");
					divSautLigne.id = "sautLigne_" + chapitreId;
				}
			
			if (divSautLigne != null && divSautLigne != undefined) {
				for (var i = 0; i < nombreSautLignes; i++) {
					divSautLigne.appendChild(recappopup.document.createElement("br"));
				}
				
				if (recappopup.document != null && recappopup.document.getElementById("chapitre_"+chapitreId) != null) {
					recappopup.document.getElementById("chapitre_"+chapitreId).appendChild(divSautLigne);
				}
			}
			
		}
	
	}
}

function miseAJourContenuPopUpApresChangerArticle(nombreLignes, chapitreId, identifiantsActifListeChoixExclusif,
		identifiantsInactifListeChoixExclusif,
		identifiantsActifListeChoixMultiple,
		identifiantsInactifListeChoixMultiple,
		identifiantsLignesTableauDynamique) {
	
	if(isPopUpOuverte()) {
		recappopup.miseAJourContenuTextArea();
		miseAJourContenuRadioButtons(identifiantsActifListeChoixExclusif, identifiantsInactifListeChoixExclusif);
		miseAJourCheckBox(identifiantsActifListeChoixMultiple, identifiantsInactifListeChoixMultiple);
		recappopup.miseAJourTableauDynamique();
		ajouterSautLigne(nombreLignes, chapitreId);
	}
}

function miseAJourContenuRadioButtons(identifiantsActifListeChoixExclusif, idsInactifListeChoixExclusif) {
	// choix exclusif actif
	if (identifiantsActifListeChoixExclusif != undefined && identifiantsActifListeChoixExclusif != null) {
		for (var i = 0; i < identifiantsActifListeChoixExclusif.length; i++) {
			decocherCaseClauseChoixMultiple(identifiantsActifListeChoixExclusif[i], 'inline');
		}
	}

	// choix exclusif inactif
	if (idsInactifListeChoixExclusif != undefined && idsInactifListeChoixExclusif != null) {
		for (var i = 0; i < idsInactifListeChoixExclusif.length; i++) {
			decocherCaseClauseChoixMultiple(idsInactifListeChoixExclusif[i], 'none');
		}
	}
	
}

function miseAJourCheckBox(idsActifListeChoixMultiple, idsInactifListeChoixMultiple) {
	// checkbox actif
	if (idsActifListeChoixMultiple != undefined && idsActifListeChoixMultiple != null) {
		for (var i = 0; i < idsActifListeChoixMultiple.length; i++) {
			decocherCaseClauseChoixMultiple(idsActifListeChoixMultiple[i], 'inline');
		}
	}

	// checkbox inactif
	if (idsInactifListeChoixMultiple != undefined) {
		for (var i = 0; i < idsInactifListeChoixMultiple.length; i++) {
			decocherCaseClauseChoixMultiple(idsInactifListeChoixMultiple[i], 'none');
		}
	}
}

function mettreAJourIdentifiantsPrevisualisation(identifiantsActifListeChoixExclusif,
		identifiantsInactifListeChoixExclusif,
		identifiantsActifListeChoixMultiple,
		identifiantsInactifListeChoixMultiple,
		identifiantsLignesTableauDynamique) {
	
	identifiantsChoixExclusifActifList = identifiantsActifListeChoixExclusif;
	identifiantsChoixExclusifInactifList = identifiantsInactifListeChoixExclusif;
	identifiantsChoixMultipleActifList = identifiantsActifListeChoixMultiple;
	identifiantsChoixMultipleInactifList = identifiantsInactifListeChoixMultiple;

	if (identifiantsLignesTableauDynamique != null && identifiantsLignesTableauDynamique != undefined) {
		identifiantsLignesTableauDynamiqueList = identifiantsLignesTableauDynamique;
	}
}

/**
 * cette fonction 
 * return true si on est sous ie  
 * Il retourne false sinon
 * */
function msieversion() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) 
        return true;
    else                 
        return false;

return false;
}

/**
 * 
 */
function isEmpty(obj) {
    if (obj == null) return true;
    for (var key in obj) {
    	if (key!=null)
         return false;
    }

    return true;
}
/** fin methodes exposees pour int�gration **/
