<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%--@elvariable id="recapClause" type="fr.paris.epm.redaction.coordination.bean.forms.BlocRecapClauseFormBean"--%>
<%--@elvariable id="clauseEditeur" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>
<%--@elvariable id="epmTClauseInstance" type="fr.paris.epm.noyau.persistance.redaction.EpmTClauseAbstract"--%>

<div class="breaker"></div>
<div class="results-list recap-tableau">
    <table>
        <thead>
        <tr>
            <th class="top" colspan="3">
                <img class="left" title="" alt="" src="<atexo:href href='images/table-results-top-left.gif'/>" />
                <img class="right" title="" alt="" src="<atexo:href href='images/table-results-top-right.gif'/>" />
            </th>
        </tr>
        <tr>
            <th class="col-80">
                <div>
                    <bean:message key="modificationClause.tableauRecapitulatif.reference" /><br/>
                    <bean:message key="modificationClause.tableauRecapitulatif.auteur" /><br/>
                    <bean:message key="modificationClause.tableauRecapitulatif.statut" />
                </div>
            </th>
            <th class="col-325">
                <div>
                    <bean:message key="modificationClause.tableauRecapitulatif.theme" /><br/>
                    <bean:message key="modificationClause.tableauRecapitulatif.contenu" />
                </div>
            </th>
            <th class="col-80">
                <div>
                    <bean:message key="modificationClause.tableauRecapitulatif.creeeLe" /><br/>
                    <bean:message key="modificationClause.tableauRecapitulatif.modifiee" /><br/>
                    <bean:message key="modificationClause.tableauRecapitulatif.version" />
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="col-80">
                <div><c:out value="${recapClause.reference}" /></div>

                <c:choose>
                    <c:when test="${recapClause.auteur == 'Editeur surchargée'}">
                        <div><bean:message key="clause.txt.editeurSurchargee" /></div>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${recapClause.auteur == 'Editeur' || epmTClauseInstance.clauseEditeur == true}">
                                <div><bean:message key="clause.txt.editeur" /></div>
                            </c:when>
                            <c:otherwise>
                                <div><bean:message key="clause.txt.client" /></div>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>

                <div class="spacer-mini"></div>
                <c:if test="${recapClause.idStatutRedactionClausier == 1}">
                    <div>
                        <img title="Brouillon" alt="Brouillon" src="<atexo:href href='images/clause-statut-1.gif'/>" />
                    </div>
                </c:if>
                <c:if test="${recapClause.idStatutRedactionClausier == 2}">
                    <div>
                        <img title="A valider" alt="A valider" src="<atexo:href href='images/clause-statut-2.gif'/>" />
                    </div>
                </c:if>
                <c:if test="${recapClause.idStatutRedactionClausier == 3}">
                    <div>
                        <img title="Validée" alt="Validée" src="<atexo:href href='images/clause-statut-3.gif'/>" />
                    </div>
                </c:if>
            </td>
            <td class="col-325">
                <div><c:out value="${recapClause.theme}" /></div>
                <div><nested:write name="recapClause" property="contenu" filter="false" /></div>
            </td>
            <td class="col-80">
                <c:out value="${recapClause.dateCreation}" />
                <c:out value="${recapClause.dateModification}" />
                <c:out value="${recapClause.lastVersion}" />
            </td>
        </tr>
        </tbody>
    </table>
</div>

<div class="spacer-small"></div>
