<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isErrorPage="false"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="redactionTag" prefix="redaction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="actionEditeur" value="" />
<c:set var="actionPreferences" value="true" />

<c:set var="nombreResultatAAfficherParDefaut" value="10" />
<c:set var="nameTableauPagine" value="frmRechercherClause" />
<c:set var="urlTableauPagine" value="clause${actionEditeur}Liste.epm" />
<c:if test="${actionPreferences}">
    <c:set var="nameTableauPagine" value="frmParametrageClause" />
    <c:set var="urlTableauPagine" value="parametrageListe.epm" />
    <c:if test="${!empty accesModeSaaS}">
        <c:set var="urlTableauPagine" value="externeParametrageListe.epm" />
    </c:if>
</c:if>
<atexo:tableauPagine name="${nameTableauPagine}"
                     url="${urlTableauPagine}"
                     property="listeClause"
                     elementAfficher="10,20,40,60,100"
                     nombreResultatAAfficherParDefaut="${nombreResultatAAfficherParDefaut}"
                     id="clauseListe"></atexo:tableauPagine>

<div class="main-part">
    <div class="breadcrumbs">
        <logic:equal name="preferences" value="direction">
            <bean:message key="Parametrage.titre.direction"/>
        </logic:equal>
        <logic:equal name="preferences" value="agent">
            <bean:message key="Parametrage.titre.agent"/>
        </logic:equal>
    </div>
    <logic:equal name="preferences" value="direction">
        <atexo:fichePratique reference="FPR01" key="common.fichePratique"/>
    </logic:equal>
    <logic:equal name="preferences" value="agent">
        <atexo:fichePratique reference="FPR02" key="common.fichePratique"/>
    </logic:equal>
    <div class="breaker"></div>

    <!--Debut bloc Recherche clause-->
    <html:form action="/ParametrageClauseRecharge.epm" >

        <div class="form-bloc atx-form-panel form-horizontal">
            <div class="atx-form-panel_container panel-toggle clearfix ${requestScope.NB_RESULT gt 0 ? '' : 'open'}">
                <a href="#" class="atx-form-panel_headeing" aria-expanded="false">
                    <div class="atx-form-panel_headeing_container">
                        <div class="toggle">
                            <i class="fa fa-minus"></i>
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="title"><spring:message code="moteurRecherche.title"/></div>
                    </div>
                </a>

                <div class="atx-form-panel_body">
                    <div class="row">
                        <!-- Left row -->
                        <div class="col-md-6">
                            <!-- form-group -->
                            <div class="form-group">
                                <label for="numconsultation" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.referencePoin"/>
                                </label>
                                <div class="col-md-8">
                                    <html:text property="reference" styleClass="form-control" styleId="reference"
                                               title="Référence" indexed="ghhh" onkeypress="handle(event)"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="typeContrat" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.typeContrat"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="typeContrat" title="Type de contrat"
                                                 styleClass="form-control" styleId="typeContrat" >
                                        <option value="0"><bean:message key="redaction.txt.Tous"/></option>
                                        <html:optionsCollection property="typeContratCollection" label="libelle" value="id"/>
                                    </html:select>
                                </div>
                            </div>

                            <!-- form-group -->
                            <div class="form-group">
                                <label for="procedurePassation" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.procPass"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="procedurePassation" title="Procédure de passation"
                                                 styleClass="form-control" styleId="procedurePassation">
                                        <html:optionsCollection property="procedureCollection" label="libelle" value="id" />
                                    </html:select>
                                </div>
                            </div>

                            <!-- form-group -->
                            <div class="form-group">
                                <label for="motsCles" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.motsCles"/>
                                </label>
                                <div class="col-md-8">
                                    <html:textarea property="motsCles" styleId="motsCles" title="Mots-cl&eacute;s"
                                                   cols="30" rows="2" styleClass="form-control">
                                    </html:textarea>
                                </div>
                            </div>

                            <!-- form-group -->
                            <div class="form-group">
                                <label for="numconsultation" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.affClauses"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="afficheClauses" styleClass="form-control" styleId="afficheClauses" title="Afficher les clauses">
                                        <html:option value="-1">
                                            <bean:message  key="redaction.txt.Toutes"/>
                                        </html:option>
                                        <html:option value="1">
                                            <bean:message  key="redaction.txt.Actives"/>
                                        </html:option>
                                        <html:option value="0">
                                            <bean:message  key="redaction.txt.Inactives"/>
                                        </html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <!-- Right row -->
                        <div class="col-md-6">
                            <!-- form-group -->
                            <div class="form-group">
                                <label for="theme" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.themePoin"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="theme" styleClass="form-control" styleId="theme" title="Th�me" >
                                        <html:optionsCollection property="themeCollection" label="libelle" value="id"/>
                                    </html:select>
                                </div>
                            </div>

                            <!-- form-group -->
                            <div class="form-group">
                                <label for="numconsultation" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.natPres"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="naturePrestas" styleClass="form-control" styleId="naturePrestas" title="Nature des prestations">
                                        <html:optionsCollection property="naturePrestasCollection"
                                                                label="libelle" value="id" />
                                    </html:select>
                                </div>
                            </div>

                            <!-- form-group -->
                            <div class="form-group">
                                <label for="docType" class="col-md-3 control-label">
                                    <bean:message key="ParametrageClause.txt.typeDoc"/>
                                </label>
                                <div class="col-md-8">
                                    <html:select property="docType" styleClass="form-control" styleId="docType" title="Type de document">
                                        <html:optionsCollection property="docTypeCollection" label="libelle" value="id"/>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script> function handle(e){ if(e.keyCode === 13){ document.forms[0].submit(); } return false; } </script>

                    <div class=clearfix">
                        <!--Attention, la fonction 'initResultsTable' ci-dessous ne sert que puor la maquette, il faut la supprimer a l'integration-->
                        <a href="javascript:initFormRecherche();" class="btn btn-warning btn-sm">
                            <i class="fa fa-eraser" aria-hidden="true"></i>&nbsp;
                            <bean:message key="ParametrageClause.btn.effacerCritereRecherche"/>
                        </a>
                        <html:link href="javascript:document.forms[0].submit();" styleClass="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-search" aria-hidden="true"></i>&nbsp;
                            <bean:message key="ParametrageClause.btn.rechercher"/>
                        </html:link>
                    </div>
                </div>
            </div>
        </div>

    </html:form>
    <!--Fin bloc Recherche clause-->


    <!--Debut partitioner-->
    <!-- Tableau des resultats  -->
    <%@ include file="TableauRechercheClause.jsp"%>

    <!--Fin legende clauses actives/inactives-->
</div>
<!--Fin main-part-->
