<!--Debut main-part-->
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="form-bloc">
	<div class="top">
		<span class="left"></span><span class="right"></span>
	</div>
	<div class="content">
		<h2 class="float-left">
			<html:radio property="clauseSelectionnee" styleId="clauseEditeurSurchargeeSelectionnee" value="clauseEditeurSurcharge"></html:radio>
			<label for="choixClauseEditeur"><bean:message key="clause.surcharge.clauseEditeurSurchargee"/></label>
		</h2>
		<div class="actions-clause no-padding">
			<logic:equal name="typeAction" value="M">
				<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
			</logic:equal>
			<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
		</div>
		
<!-- 		<div class="info-maj"> -->
<%-- 		<bean:message key="clause.surcharge.modifieeLe"/> --%>
<%-- 			<c:out value="TODO" /> --%>
<!-- 		</div> -->
		<div class="line">
			<span class="intitule-bloc"><bean:message	key="ClauseTexteFixe.txt.texteFixe" /> </span>
			<html:textarea property="texteFixe" styleId="texteFixe" title="Texte fixe" cols="" 	rows="6" styleClass="texte-long mceEditor" errorStyleClass="error-border" />
		</div>
		
		<div class="actions-clause">
			<logic:equal name="typeAction" value="M">
				<a title="<bean:message key="clause.txt.reinitialiser"/>" href="javascript:reinitialiser();"><img src="<atexo:href href='images/bouton-reinitialiser.gif'/>" alt="<bean:message key="clause.txt.reinitialiser"/>"></a>
			</logic:equal>
			<a title="<bean:message key="clause.txt.previsualiser"/>" href="javascript:previsualiserEditeurSurchargee();"><img src="<atexo:href href='images/bouton-previsualiser.gif'/>" alt="<bean:message key="clause.txt.previsualiser"/>"></a>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom">
		<span class="left"></span><span class="right"></span>
	</div>
</div>
<div class="spacer"></div>
<script type="text/javascript">
 	initEditeursTexteRedactionSurchargeClauses();
</script>