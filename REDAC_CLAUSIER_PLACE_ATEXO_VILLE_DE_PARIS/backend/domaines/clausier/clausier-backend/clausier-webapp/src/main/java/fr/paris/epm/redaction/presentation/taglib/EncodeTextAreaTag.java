/**
 * 
 */
package fr.paris.epm.redaction.presentation.taglib;

import fr.paris.epm.redaction.commun.Util;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag remplacant une chaine de caractère par une autre.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class EncodeTextAreaTag extends TagSupport {

    /**
     * Marqueur de sérailization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * emplacement de la variable (request, session, parameter).
     */
    private String scope;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EncodeTextAreaTag.class);


    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           scope);
            if (texte != null) {
                pageContext.getOut()
                        .print(Util.decodeCaractere((String) texte));
            } else {
                pageContext.getOut().print("");
            }

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        scope = null;
        property = null;
        name = null;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }

    /**
     * @param valeur accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        this.property = valeur;
    }

    /**
     * @param valeur emplacement de la variable (request, session, parameter)
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }

}
