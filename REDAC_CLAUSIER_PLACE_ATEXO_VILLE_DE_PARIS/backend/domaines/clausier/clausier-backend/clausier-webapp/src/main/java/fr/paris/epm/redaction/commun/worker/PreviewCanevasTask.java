package fr.paris.epm.redaction.commun.worker;

import fr.paris.epm.global.commun.worker.RsemTask;
import fr.paris.epm.noyau.metier.objetvaleur.Derogation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefValeurTypeClause;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.coordination.CanevasFacadeGWT;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.metier.documentHandler.DerogationUtil;
import fr.paris.epm.redaction.metier.documentHandler.GenerationTableauUtil;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

/**
 * Tache générant la forme de la prévisualision des Canevas
 * Created by nty on 27/06/19.
 * @author Nikolay Tyurin
 * @author nty
 */
@Component
@Scope("prototype")
public class PreviewCanevasTask extends RsemTask<Map<String, Object>> {

    private static final Logger logger = LoggerFactory.getLogger(PreviewCanevasTask.class);

    @Resource
    private CanevasFacadeGWT canevasFacadeGWT;

    @Resource
    private ReferentielFacade referentielFacade;

    @Value("${activation.export.canevas:true}")
    private String activerExportCanevas;

    private int idCanevas;

    private Integer idPublication;

    private boolean editeur;

    private EpmTUtilisateur utilisateur;

    @Override
    public Map<String, Object> work() throws Exception {

        Map<String, Object> result = new HashMap<>();
        Canevas canevas = canevasFacadeGWT.charger(idCanevas, idPublication, editeur, utilisateur.getIdOrganisme());
        result.put(Constante.CANEVAS_EN_COURS, canevas);

        // préparer la prévisualisation du tableau des dérogations si cela est activé
        if (canevas.isDerogationActive()) {
            List<Derogation> derogations = new ArrayList<Derogation>();
            DerogationUtil.chargerDerogationsDepuisChapitres(canevas.getChapitres(), derogations);

            // créer le chapitre temporaire contenant le tableau dérogations qui va être affiché à la fin
            if (!derogations.isEmpty()) {
                Chapitre chapitre = new Chapitre();
                chapitre.setIdChapitre(-1);
                int numeroChapitre = canevas.getChapitres().size() + 1;
                chapitre.setNumero(String.valueOf(numeroChapitre));
                ResourceBundle rb = ResourceBundle.getBundle("ApplicationResources");
                chapitre.setTitre(rb.getString("document.derogation.titre"));
                canevas.getChapitres().add(chapitre);

                EpmTRefValeurTypeClause valeurTableauDerogation = referentielFacade.getValeurTypeClauseById(Constante.ID_TABLEAU_DEROGATION);
                TableauRedaction tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(valeurTableauDerogation.getExpression(), null, null, derogations);
                result.put("htmlTableauDerogations", GenerationTableauUtil.tableauRedactionToHTML(tableauRedaction));
            }
        }
        result.put("activerExportCanevas", activerExportCanevas);
        return result;
    }

    public void setIdCanevas(int idCanevas) {
        this.idCanevas = idCanevas;
    }

    public void setIdPublication(Integer idPublication) {
        this.idPublication = idPublication;
    }

    public void setEditeur(boolean editeur) {
        this.editeur = editeur;
    }

    public void setUtilisateur(EpmTUtilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

}
