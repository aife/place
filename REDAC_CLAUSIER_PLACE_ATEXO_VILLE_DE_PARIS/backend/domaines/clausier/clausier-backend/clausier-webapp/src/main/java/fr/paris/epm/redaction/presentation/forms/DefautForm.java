package fr.paris.epm.redaction.presentation.forms;

import org.apache.struts.action.ActionForm;

/**
 * formulaire par defaut.
 * @author Ramli Tarik.
 *@version $Revision$, $Date$, $Author$
 */
public class DefautForm extends ActionForm {
    /**
     * Marqueur de sérialisation.
     */

    private static final long serialVersionUID = 1L;
}
