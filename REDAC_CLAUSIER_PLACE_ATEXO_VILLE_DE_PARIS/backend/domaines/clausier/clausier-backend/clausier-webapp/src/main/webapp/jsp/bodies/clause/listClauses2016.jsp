<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 02/09/18
  Time: 13:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="atexo"   uri="AtexoTag" %>
<%@ taglib prefix="atexo2"  uri="../../../WEB-INF/tld/atexo-rsem.tld" %>

<script src="webjars/jquery/3.2.1/jquery.min.js"></script>

<%--@elvariable id="editeur" type="java.lang.Boolean"--%>

<!--Debut Main part-->
<div class="main-part main-part-2018">

    <div class="atx-breadcrumbs breadcrumbs">
        <strong>
            <spring:message code="redaction.header.administration"/>
            <c:if test="${editeur}"><spring:message code="redaction.header.editeur"/></c:if>&nbsp;&gt;&nbsp;
            <spring:message code="redaction.header.clauses"/>&nbsp;&gt;&nbsp;
            <spring:message code="redaction.header.rechercher"/>
        </strong>
    </div>

    <jsp:include page="../message.jsp" />
    <div class="breaker"></div>

    <!--Bloc recherche profil utilisateur-->
    <jsp:include page="searchClauses2016.jsp" />

    <!-- Fenêtre-modale de suppresion -->
    <jsp:include page="../modalConfirmation.jsp" />

    <%--@elvariable id="resultList" type="fr.paris.epm.global.commun.ResultList"--%>

    <!-- Navigation bloque -->
    <div class="atx-pagination">
        <div class="nbr-result pull-left">
            <h2 class="h5">
                <c:out value="${resultList.count}"/>&nbsp;<spring:message code="recherche.resultats"/>
            </h2>
        </div>
        <jsp:include page="../navigate.jsp" />
    </div>

    <!--Debut bloc resultats profil utilisateur-->
    <div class="results-list">
        <table>
            <thead>
            <tr>
                <th class="select-col">
                    <div>
                        <input id="select-all-clauses" type="checkbox" title="Tous" />
                    </div>
                </th>
                <th class="reference">
                    <div>
                        <spring:message code="redaction.clause.reference" />
                        <a href="sort.htm?property=reference"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.clause.auteur" />
                        <a href="sort.htm?property=clauseEditeur"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.clause.statut" />
                        <a href="sort.htm?property=epmTRefStatutRedactionClausier.id"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="theme">
                    <div>
                        <spring:message code="redaction.clause.theme" />
                        <a href="sort.htm?property=epmTRefThemeClause.libelle"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="contenu-long">
                    <div>
                        (<spring:message code="redaction.clause.nbSurcharges" />)
                        <spring:message code="redaction.clause.contenu" />
                    </div>
                </th>
                <th class="col-80">
                    <div>
                        <spring:message code="redaction.clause.creeLe" />
                        <a href="sort.htm?property=dateCreation"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.clause.modifieeLe" />
                        <a href="sort.htm?property=dateModification"><i class="fa fa-sort"></i></a>
                    </div>
                    <div>
                        <spring:message code="redaction.clause.version" />
                        <a href="sort.htm?property=idLastPublication"><i class="fa fa-sort"></i></a>
                    </div>
                </th>
                <th class="col-150">
                    <div>
                        <spring:message code="redaction.actions" />
                    </div>
                </th>
            </tr>
            </thead>

            <tbody>
            <c:forEach var="clause" items="${resultList.listResults}" varStatus="vs">
                <%--@elvariable id="clause" type="fr.paris.epm.redaction.presentation.bean.ClauseBean"--%>

                <c:set var="style" value="" />
                <c:if test="${vs.index % 2 == 0}">
                    <c:set var="style" value="on" />
                </c:if>
                <c:if test="${!clause.actif}">
                    <c:set var="style" value="off" />
                </c:if>

                <tr class="${style}">
                    <td class="select-col">
                        <input class="select-clause" type="checkbox" title="Tous"
                               data-id-clause="${clause.idClause}"
                               data-reference-clause="${clause.referenceClause}"
                               <c:if test="${(!editeur && clause.clauseEditeur) || (editeur && !clause.clauseEditeur)}">
                                     disabled="disabled"
                               </c:if> />
                    </td>
                    <td class="reference">
                        <div><c:out value="${clause.referenceClause}" /></div>
                        <div><c:out value="${clause.labelTypeAuteur}" /></div>
                        <div>
                            <c:choose>
                                <c:when test="${clause.idStatutRedactionClausier == 1}">
                                    <div>
                                        <img title="Brouillon" alt="Brouillon" src="<atexo:href href='images/clause-statut-1.gif'/>" />
                                    </div>
                                </c:when>
                                <c:when test="${clause.idStatutRedactionClausier == 2}">
                                    <div>
                                        <img title="A valider" alt="A valider" src="<atexo:href href='images/clause-statut-2.gif'/>" />
                                    </div>
                                </c:when>
                                <c:when test="${clause.idStatutRedactionClausier == 3}">
                                    <div>
                                        <img title="Validée" alt="Validée" src="<atexo:href href='images/clause-statut-3.gif'/>" />
                                    </div>
                                </c:when>
                            </c:choose>
                        </div>
                    </td>
                    <td class="theme">
                        <div><c:out value="${clause.labelThemeClause}" /></div>
                    </td>
                    <td class="contenu-long">
                        <div>( ) <c:out value="${clause.context}" /></div>
                    </td>
                    <td class="col-80">
                        <c:set var="dateCreationStr"><fmt:formatDate value="${clause.dateCreation}" pattern="dd/MM/yyyy" /></c:set>
                        <div><c:out value="${dateCreationStr}" /></div>
                        <c:set var="dateModificationStr"><fmt:formatDate value="${clause.dateModification}" pattern="dd/MM/yyyy" /></c:set>
                        <div><c:out value="${dateModificationStr}" /></div>
                        <div><c:out value="${clause.lastVersion}" /></div>
                    </td>
                    <td class="col-150">
                        <c:set var="urlParameters" value="idClause=${clause.idClause}" />
                        <c:if test="${not empty clause.idPublication}">
                            <c:set var="urlParameters" value="${urlParameters}&idPublication=${clause.idPublication}" />
                        </c:if>

                        <c:set var="urlEditeur" value="" />
                        <c:if test="${editeur}">
                            <c:set var="urlEditeur" value="Editeur" />
                        </c:if>

                        <!-- verification role de validation de clause editeur / client -->
                        <c:set var="roleValiderClauseClient" value="${atexo2:filtreSecurite(utilisateur, \"ROLE_validerClauses\", true)}" />
                        <c:set var="roleValiderClauseEditeur" value="${atexo2:filtreSecurite(utilisateur, \"ROLE_validerClausesEditeur\", true)}" />

                        <!-- verification si les boutons de validation/refus doivent s'afficher -->
                        <c:choose>
                            <c:when test="${editeur && clause.clauseEditeur && roleValiderClauseEditeur}">
                                <c:set var="isBoutonValidationVisible" value="true" />
                            </c:when>
                            <c:when test="${!editeur && !clause.clauseEditeur && roleValiderClauseClient}">
                                <c:set var="isBoutonValidationVisible" value="true" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="isBoutonValidationVisible" value="false" />
                            </c:otherwise>
                        </c:choose>

                        <!-- Boutons de validation -->
                        <c:choose>
                            <c:when test="${clause.idStatutRedactionClausier == 1}">
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.clause.confirmation.prevalider.message" />"
                                           data-text="${clause.referenceClause}"
                                           data-href="changeStatusClause.htm?idClause=${clause.idClause}&idStatus=2">
                                            <img src="<atexo:href href='images/bouton-demande-validation.gif'/>"
                                                 alt="<spring:message code="redaction.action.demanderValidation"/>"
                                                 title="<spring:message code="redaction.action.demanderValidation"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                                <span class="empty-action">-</span>
                            </c:when>
                            <c:when test="${clause.idStatutRedactionClausier == 2}">
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.clause.confirmation.valider.message" />"
                                           data-text="${clause.referenceClause}"
                                           data-href="changeStatusClause.htm?idClause=${clause.idClause}&idStatus=3">
                                            <img src="<atexo:href href='images/bouton-valider.gif'/>"
                                                 alt="<spring:message code="redaction.action.valider"/>"
                                                 title="<spring:message code="redaction.action.valider"/>" />
                                        </a>
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.clause.confirmation.refuser.message" />"
                                           data-text="${clause.referenceClause}"
                                           data-href="changeStatusClause.htm?idClause=${clause.idClause}&idStatus=1">
                                            <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                                                 alt="<spring:message code="redaction.action.refuser"/>"
                                                 title="<spring:message code="redaction.action.refuser"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${clause.idStatutRedactionClausier == 3}">
                                <span class="empty-action">-</span>
                                <c:choose>
                                    <c:when test="${isBoutonValidationVisible}">
                                        <a data-toggle="modal" href="#modalConfirmation"
                                           data-title="<spring:message code="redaction.clause.confirmation.refuser.message" />"
                                           data-text="${clause.referenceClause}"
                                           data-href="changeStatusClause.htm?idClause=${clause.idClause}&idStatus=1">
                                            <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                                                 alt="<spring:message code="redaction.action.refuser"/>"
                                                 title="<spring:message code="redaction.action.refuser"/>" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="empty-action">-</span>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>

                        <!-- Bouton de prévisualisation -->
                        <c:choose>
                            <c:when test="${clause.clauseEditeur == true}">
                                <a href="javascript:popUp('PrevisualiserClauseProcess${urlEditeur}.epm?initialisationSession=true&${urlParameters}&clauseEditeur=true','yes');">
                                    <img src="<atexo:href href='images/bouton-previsualiser.gif' />"
                                         alt="<spring:message code="redaction.action.previsualiser"/>"
                                         title="<spring:message code="redaction.action.previsualiser"/>" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:popUp('PrevisualiserClauseProcess${urlEditeur}.epm?initialisationSession=true&${urlParameters}','yes');">
                                    <img src="<atexo:href href='images/bouton-previsualiser.gif' />"
                                         alt="<spring:message code="redaction.action.previsualiser"/>"
                                         title="<spring:message code="redaction.action.previsualiser"/>" />
                                </a>
                            </c:otherwise>
                        </c:choose>

                        <!-- Bouton modification -->
                        <c:choose>
                            <c:when test="${(!editeur && !clause.clauseEditeur) || (editeur && clause.clauseEditeur)}">
                                <a href="ModificationClause${urlEditeur}Init.epm?initialisationSession=true&${urlParameters}&typeAction=M">
                                    <img src="<atexo:href href='images/bouton-modifier.gif' />"
                                         alt="<spring:message code="redaction.action.modifier"/>"
                                         title="<spring:message code="redaction.action.modifier"/>" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="ModificationClauseInit.epm?initialisationSession=true&${urlParameters}&typeAction=M&clauseEditeur=true">
                                    <img src="<atexo:href href='images/bouton-parametrer.gif' />"
                                         alt="<spring:message code="redaction.action.surcharger"/>"
                                         title="<spring:message code="redaction.action.surcharger"/>" />
                                </a>
                            </c:otherwise>
                        </c:choose>
                        <a href="ModificationClause${urlEditeur}Init.epm?initialisationSession=true&${urlParameters}&typeAction=D">
                            <img src="<atexo:href href='images/bouton-picto-dupliquer.gif' />"
                                 alt="<spring:message code="redaction.action.dupliquer"/>"
                                 title="<spring:message code="redaction.action.dupliquer"/>" />
                        </a>

                        <!-- Bouton suppression -->
                        <c:choose>
                            <c:when test="${(!editeur && !clause.clauseEditeur) || (editeur && clause.clauseEditeur)}">
                                <a data-toggle="modal" href="#modalConfirmation"
                                   data-title="<spring:message code="redaction.clause.confirmation.supprimer.message" />"
                                   data-text="${clause.referenceClause}"
                                   data-href="removeClause.htm?idClause=${clause.idClause}">
                                    <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"
                                         alt="<spring:message code="redaction.action.supprimer"/>"
                                         title="<spring:message code="redaction.action.supprimer"/>" />
                                </a>
                            </c:when>
                            <c:otherwise>
                                <span class="empty-action">-</span>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!--Fin bloc resultats recherche-->

    <!-- Navigation bloque -->
    <div class="atx-pagination">
        <jsp:include page="../navigate.jsp" />
    </div>

    <div id="actions-panel" class="layer">
        <div class="content">
            <div class="float-left">
                <strong><spring:message code="redaction.actionsGroupees" /></strong>
            </div>
            <div class="float-right">
                <c:if test="${editeur && roleValiderClauseEditeur || !editeur && roleValiderClauseClient}">
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.clause.confirmation.prevalider.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusClause.htm?' + listIds + '&idStatus=2'">
                        <img src="<atexo:href href='images/bouton-demande-validation.gif'/>"
                             alt="<spring:message code="redaction.action.demanderValidation"/>"
                             title="<spring:message code="redaction.action.demanderValidation"/>" />
                    </a>
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.clause.confirmation.refuser.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusClause.htm?' + listIds + '&idStatus=1'">
                        <img src="<atexo:href href='images/bouton-refuser.gif'/>"
                             alt="<spring:message code="redaction.action.refuser"/>"
                             title="<spring:message code="redaction.action.refuser"/>" />
                    </a>
                    <a data-toggle="modal" href="#modalConfirmation"
                       data-title="<spring:message code="redaction.clause.confirmation.valider.message" />"
                       v-bind:data-text="listReferences"
                       v-bind:data-href="'changeStatusClause.htm?' + listIds + '&idStatus=3'">
                        <img src="<atexo:href href='images/bouton-valider.gif'/>"
                             alt="<spring:message code="redaction.action.valider"/>"
                             title="<spring:message code="redaction.action.valider"/>" />
                    </a>
                </c:if>
                <a data-toggle="modal" href="#modalConfirmation"
                   data-title="<spring:message code="redaction.clause.confirmation.supprimer.message" />"
                   v-bind:data-text="listReferences"
                   v-bind:data-href="'removeClause.htm?' + listIds">
                    <img src="<atexo:href href='images/bouton-picto-supprimer.gif'/>"
                         alt="<spring:message code="redaction.action.supprimer"/>"
                         title="<spring:message code="redaction.action.supprimer"/>" />
                </a>
            </div>
        </div>
    </div>


    <jsp:include page="listClauses.vue" />

</div>
<!--Fin Main part-->