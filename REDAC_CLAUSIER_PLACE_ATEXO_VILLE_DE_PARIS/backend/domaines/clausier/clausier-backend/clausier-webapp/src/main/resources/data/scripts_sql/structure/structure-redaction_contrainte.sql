CREATE SEQUENCE epm__t_association_canevas_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_association_canevas_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_association_canevas_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_canevas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_canevas ALTER COLUMN id SET DEFAULT nextval('epm__t_canevas_id_seq'::regclass);	
CREATE SEQUENCE epm__t_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_document ALTER COLUMN id SET DEFAULT nextval('epm__t_document_id_seq'::regclass);
CREATE SEQUENCE epm__t_info_bulle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_info_bulle ALTER COLUMN id SET DEFAULT nextval('epm__t_info_bulle_id_seq'::regclass);
CREATE SEQUENCE epm__t_ppt_ext_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ppt_ext_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_ppt_ext_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_potentiellement_conditionnee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_potentiellement_conditionnee ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_potentiellement_conditionnee_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_theme_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_theme_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_theme_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_type_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_type_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_type_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_type_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_type_document ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_type_document_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_valeur_heritee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_valeur_heritee ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_valeur_heritee_id_seq'::regclass);
CREATE SEQUENCE epm__t_ref_valeur_tableau_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_ref_valeur_tableau ALTER COLUMN id SET DEFAULT nextval('epm__t_ref_valeur_tableau_id_seq'::regclass);
CREATE SEQUENCE epm__t_mots_cles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_mots_cles ALTER COLUMN id SET DEFAULT nextval('epm__t_mots_cles_id_seq'::regclass);
CREATE SEQUENCE epm__t_role_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_role_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_role_clause_id_seq'::regclass);
CREATE SEQUENCE epm__t_type_document_clause_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
ALTER TABLE epm__t_type_document_clause ALTER COLUMN id SET DEFAULT nextval('epm__t_type_document_clause_id_seq'::regclass);


ALTER TABLE ONLY epm__t_association_canevas_clause
    ADD CONSTRAINT epm__t_association_canevas_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_canevas
    ADD CONSTRAINT epm__t_canevas_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_valeur_tableau
    ADD CONSTRAINT epm__t_ref_valeur_tableau_pkey PRIMARY KEY (id);
	
ALTER TABLE ONLY epm__t_clause
    ADD CONSTRAINT epm__t_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_document
    ADD CONSTRAINT epm__t_document_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_info_bulle
    ADD CONSTRAINT epm__t_info_bulle_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_mots_cles
    ADD CONSTRAINT epm__t_mots_cles_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_potentiellement_conditionnee
    ADD CONSTRAINT epm__t_ref_potentiellement_conditionnee_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_type_clause
    ADD CONSTRAINT epm__t_ref_type_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_type_document
    ADD CONSTRAINT epm__t_ref_type_document_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_valeur_heritee
    ADD CONSTRAINT epm__t_ref_valeur_heritee_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_role_clause
    ADD CONSTRAINT epm__t_role_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ppt_ext_clause
    ADD CONSTRAINT epm__t_texte_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_ref_theme_clause
    ADD CONSTRAINT epm__t_theme_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_type_document_clause
    ADD CONSTRAINT epm__t_type_document_clause_pkey PRIMARY KEY (id);

ALTER TABLE ONLY epm__t_association_canevas_clause
    ADD CONSTRAINT epm__fk_canevas_clause FOREIGN KEY (id_canevas) REFERENCES epm__t_canevas(id);

ALTER TABLE ONLY epm__t_document
    ADD CONSTRAINT epm__fk_canevas_document FOREIGN KEY (id_canevas) REFERENCES epm__t_canevas(id);

ALTER TABLE ONLY epm__t_role_clause
    ADD CONSTRAINT epm__fk_clause FOREIGN KEY (id_clause) REFERENCES epm__t_clause(id) ON DELETE CASCADE ;

ALTER TABLE ONLY epm__t_association_canevas_clause
    ADD CONSTRAINT epm__fk_clause_canevas FOREIGN KEY (id_clause) REFERENCES epm__t_clause(id);

ALTER TABLE ONLY epm__t_type_document_clause
    ADD CONSTRAINT epm__fk_clause_document FOREIGN KEY (id_clause) REFERENCES epm__t_clause(id);

ALTER TABLE ONLY epm__t_canevas
    ADD CONSTRAINT epm__fk_document FOREIGN KEY (id_document_type) REFERENCES epm__t_ref_type_document(id);

ALTER TABLE ONLY epm__t_clause
    ADD CONSTRAINT epm__fk_info_bulle FOREIGN KEY (id_info_bulle) REFERENCES epm__t_info_bulle(id);

ALTER TABLE ONLY epm__t_clause
    ADD CONSTRAINT epm__fk_ref_type_document_clause FOREIGN KEY (id_ref_type_document) REFERENCES epm__t_ref_type_document(id);

ALTER TABLE ONLY epm__t_role_clause
    ADD CONSTRAINT epm__fk_texte_clause FOREIGN KEY (id_ppt_ext_clause) REFERENCES epm__t_ppt_ext_clause(id) ON DELETE CASCADE ;

ALTER TABLE ONLY epm__t_clause
    ADD CONSTRAINT epm__fk_theme FOREIGN KEY (id_theme) REFERENCES epm__t_ref_theme_clause(id);

ALTER TABLE ONLY epm__t_clause
    ADD CONSTRAINT epm__fk_type_clause FOREIGN KEY (id_ref_type_clause) REFERENCES epm__t_ref_type_clause(id);

ALTER TABLE ONLY epm__t_mots_cles
    ADD CONSTRAINT epm__t_mots_cles_id_clause_fkey FOREIGN KEY (id_clause) REFERENCES epm__t_clause(id);
   
    
CREATE INDEX epm__t_canevas_reference_index ON epm__t_canevas (reference) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_canevas_titre_index ON epm__t_canevas (titre) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_canevas_id_procedure_passation_index ON epm__t_canevas (id_procedure_passation) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_canevas_id_document_type_index ON epm__t_canevas (id_document_type) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_canevas_id_nature_prestation_index ON epm__t_canevas (id_nature_prestation) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_canevas_actif_index ON epm__t_canevas (actif) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_reference_index ON epm__t_clause (reference) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_id_procedure_passation_index ON epm__t_clause (id_procedure_passation) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_id_document_type_index ON epm__t_clause (id_ref_type_document) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_id_nature_prestation_index ON epm__t_clause (id_nature_prestation) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_actif_index ON epm__t_clause (actif) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_clause_theme_index ON epm__t_clause (id_theme) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_role_id_clause_index ON epm__t_role_clause (id_clause) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_role_id_ppt_ext_clause_theme_index ON epm__t_role_clause (id_ppt_ext_clause) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_role_id_direction_index ON epm__t_role_clause (id_direction) TABLESPACE m13_epmredaction_index;
CREATE INDEX epm__t_role_id_agent_index ON epm__t_role_clause (id_agent) TABLESPACE m13_epmredaction_index;

