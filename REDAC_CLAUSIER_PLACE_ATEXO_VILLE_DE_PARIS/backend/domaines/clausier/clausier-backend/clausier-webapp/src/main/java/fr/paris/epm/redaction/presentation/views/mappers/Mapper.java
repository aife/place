package fr.paris.epm.redaction.presentation.views.mappers;

public interface Mapper<U, T> {

	T map(U u);
}
