package fr.paris.epm.redaction.presentation.servlets;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.doc.AbstractXLSBuilder;
import fr.paris.epm.redaction.metier.objetValeur.ClauseExport;
import fr.paris.epm.redaction.metier.objetValeur.RoleClause;
import fr.paris.epm.redaction.util.Constantes;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Builder générant le fichier excel d'export de clause.
 *
 * @author Léon Barsamian
 */
public class ExportClauseXLSBuilder extends AbstractXLSBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(ExportClauseXLSBuilder.class);

    private static final String HTML_RETOUR_LIGNE = "<br>";
    private static final String HTML_DEBUT_PARAGRAPHE = "<p>";
    private static final String HTML_FIN_PARAGRAPHE = "</p>";
    private static final String HTML_DEBUT_LISTE_NUMEROTE = "<ul>";
    private static final String HTML_FIN_LISTE_NUMEROTE = "</ul>";
    private static final String HTML_DEBUT_ELEMENT_NUMEROTE = "<(li|LI).*>";
    private static final String HTML_FIN_ELEMENT_NUMEROTE = "</li>";
    private static final String HTML_DEBUT_STYLE = "<span";
    private static final String HTML_FIN_STYLE = "</span>";

    // onglet1
    private static final int TOUTES_CLAUSES_NOMBRE_CELLULE_TABLEAU = 16;

    private static final int TOUTES_CLAUSES_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short TOUTES_CLAUSES_COL_REFERENCE = 0;

    private static final short TOUTES_CLAUSES_COL_TYPE_CLAUSE = 1;

    private static final short TOUTES_CLAUSES_COL_TYPE_DOC = 2;

    private static final short TOUTES_CLAUSES_COL_PROCEDURE = 3;

    private static final short TOUTES_CLAUSES_COL_NAT_PRESTA = 4;

    private static final short TOUTES_CLAUSES_COL_THEME = 5;

    private static final short TOUTES_CLAUSES_COL_ACTIVE = 6;

    private static final short TOUTES_CLAUSES_COL_CONDITIONNEMENT1 = 7;

    private static final short TOUTES_CLAUSES_COL_CRITERE1 = 8;

    private static final short TOUTES_CLAUSES_COL_VALEUR1 = 9;

    private static final short TOUTES_CLAUSES_COL_CONDITIONNEMENT2 = 10;

    private static final short TOUTES_CLAUSES_COL_CRITERE2 = 11;

    private static final short TOUTES_CLAUSES_COL_VALEUR2 = 12;

    private static final short TOUTES_CLAUSES_COL_CANEVAS = 13;

    private static final short TOUTES_CLAUSES_COL_STATUT = 14;

    private static final short TOUTES_CLAUSES_COL_AUTEUR = 15;

    private static final short TOUTES_CLAUSES_COL_DATE_CREATION = 16;

    private static final short TOUTES_CLAUSES_COL_DATE_MODIFICATION = 17;

    private static final short TOUTES_CLAUSES_COL_VERSION = 18;
    private static final short TOUTES_CLAUSES_COL_INFO_BULLE = 19;

    private int indexLigneTableauTouteClause = 0;

    // onglet2
    private static final int CHOIX_EXCLUSIF_NOMBRE_CELLULE_TABLEAU = 20;

    private static final int CHOIX_EXCLUSIF_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short CHOIX_EXCLUSIF_COL_REFERENCE = 0;

    private static final short CHOIX_EXCLUSIF_COL_TYPE_DOC = 1;

    private static final short CHOIX_EXCLUSIF_COL_PROCEDURE = 2;

    private static final short CHOIX_EXCLUSIF_COL_NAT_PRESTA = 3;

    private static final short CHOIX_EXCLUSIF_COL_THEME = 4;

    private static final short CHOIX_EXCLUSIF_COL_ACTIVE = 5;

    private static final short CHOIX_EXCLUSIF_COL_CONDITIONNEMENT1 = 6;

    private static final short CHOIX_EXCLUSIF_COL_CRITERE1 = 7;

    private static final short CHOIX_EXCLUSIF_COL_VALEUR1 = 8;

    private static final short CHOIX_EXCLUSIF_COL_CONDITIONNEMENT2 = 9;

    private static final short CHOIX_EXCLUSIF_COL_CRITERE2 = 10;

    private static final short CHOIX_EXCLUSIF_COL_VALEUR2 = 11;

    private static final short CHOIX_EXCLUSIF_COL_STATUT = 12;

    private static final short CHOIX_EXCLUSIF_COL_AUTEUR = 13;

    private static final short CHOIX_EXCLUSIF_COL_DATE_CREATION = 14;

    private static final short CHOIX_EXCLUSIF_COL_DATE_MODIFICATION = 15;

    private static final short CHOIX_EXCLUSIF_COL_VERSION = 16;

    private static final short CHOIX_EXCLUSIF_COL_TXT_FIXE_AVANT = 17;
    private static final short CHOIX_EXCLUSIF_COL_NUMERO_FORMULATION = 19;
    private static final short CHOIX_EXCLUSIF_COL_TAILLE_CHAMP = 20;
    private static final short CHOIX_EXCLUSIF_COL_VALEUR_DEFAUT = 21;
    private static final short CHOIX_EXCLUSIF_COL_PRE_COCHE = 22;
    private static final short CHOIX_EXCLUSIF_COL_FORMULATION_MODIFIABLE = 23;
    private static final short CHOIX_EXCLUSIF_COL_PARAM_DIRECTION = 24;
    private static final short CHOIX_EXCLUSIF_COL_PARAM_AGENT = 25;
    private static final short CHOIX_EXCLUSIF_COL_TXT_FIXE_APRES = 27;
    private static final short CHOIX_EXCLUSIF_COL_INFO_BULLE = 28;
    private static short CHOIX_EXCLUSIF_COL_SAUT_LIGNE_1 = 18;
    private static short CHOIX_EXCLUSIF_COL_SAUT_LIGNE_2 = 26;

    private int indexLigneTableauChoixExclusif = 0;

    // onglet3
    private int indexLigneTableauChoixMultiple = 0;

    private static final int CHOIX_MULTIPLE_NOMBRE_CELLULE_TABLEAU = 25;


    // onglet 5 texte fixe

    private static final int TEXTE_FIXE_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short TEXTE_FIXE_NOMBRE_CELLULE_TABLEAU = 15;
    private static final short TEXTE_FIXE_COL_INFO_BULLE = 16;

    private int indexLigneTexteFixe = 0;

    // onglet 6 texte libre
    private static final int TEXTE_LIBRE_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short TEXTE_LIBRE_NOMBRE_CELLULE_TABLEAU = 20;

    private static final short TEXTE_LIBRE_COL_TAILLE_CHAMP = 16;

    private static final short TEXTE_LIBRE_COL_CHAMP_LIBRE_OBLIGATOIRE = 17;

    private static short TEXTE_LIBRE_COL_SAUT_LIGNE_2 = 18;

    private static final short TEXTE_LIBRE_COL_TXT_FIXE_APRES = 19;
    private static final short TEXTE_LIBRE_COL_INFO_BULLE = 23;

    private int indexLigneTexteLibre = 0;

    // onglet 7 prévalorisé
    private static final int CHOIX_PREVALORISE_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short CHOIX_PREVALORISE_NOMBRE_CELLULE_TABLEAU = 22;

    private static final short CHOIX_PREVALORISE_COL_TAILLE_CHAMP = 16;

    private static final short CHOIX_PREVALORISE_COL_VALEUR_DEFAUT = 17;

    private static final short CHOIX_PREVALORISE_COL_PARAM_DIRECTION = 18;

    private static final short CHOIX_PREVALORISE_COL_PARAM_AGENT = 19;

    private static final short CHOIX_PREVALORISE_COL_SAUT_LIGNE_2 = 20;

    private static final short CHOIX_PREVALORISE_COL_TXT_FIXE_APRES = 21;
    private static final short CHOIX_PREVALORISE_COL_INFO_BULLE = 23;

    private int indexLigneTextePrevalorise = 0;

    // onglet7 hérité
    private static final short TEXTE_HERITE_COL_LIBELLE_VAL_HERITE = 16;

    private static final int TEXTE_HERITE_NUMERO_PREMIERE_LIGNE_TABLEAU = 1;

    private static final short TEXTE_HERITE_NOMBRE_CELLULE_TABLEAU = 19;

    private static final short TEXTE_HERITE_COL_CHAMP_HERITE_MODIFIABLE = 17;

    private int indexLigneTexteHerite = 0;

    public ExportClauseXLSBuilder(final InputStream fis) {
        setFluxModel(fis);
    }

    /**
     * Attention cette méthode ne rempli pas le document elle crée juste l'instance du document.
     *
     * @param path repertoire dans lequel le fichier temporaire sera généré.
     * @throws TechnicalException
     */
    @Override
    public void build(String path) {
        dossierTmp = path;
        POIFSFileSystem poifs;
        try {
            poifs = new POIFSFileSystem(getFluxModel());
            setWb(new HSSFWorkbook(poifs));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    /**
     * Rempli le document avec la liste des clauses passée en paramétre.
     *
     * @param listeClauses liste des clauses à insérer dans le document.
     */
    public void remplirDocumentAvecListeClause(final List<ClauseExport> listeClauses) {
        if (indexLigneTableauTouteClause == 0) {
            indexLigneTableauTouteClause = TOUTES_CLAUSES_NUMERO_PREMIERE_LIGNE_TABLEAU;
        }
        for (ClauseExport clause : listeClauses) {
            remplirOngletTouteClause(clause, indexLigneTableauTouteClause);
            int typeClause = clause.getIdTypeClause();
            switch (typeClause) {
                case 2:
                    if (indexLigneTexteFixe == 0) {
                        indexLigneTexteFixe = TEXTE_FIXE_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    remplirLigneOngletTexteFixe(clause, indexLigneTexteFixe);
                    break;
                case 3:
                    if (indexLigneTexteLibre == 0) {
                        indexLigneTexteLibre = TEXTE_LIBRE_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    remplirLigneOngletTexteLibre(clause, indexLigneTexteLibre);
                    break;
                case 4:
                    if (indexLigneTextePrevalorise == 0) {
                        indexLigneTextePrevalorise = CHOIX_PREVALORISE_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    remplirLigneOngletPrevalorise(clause, indexLigneTextePrevalorise);
                    break;
                case 9:
                    if (indexLigneTexteHerite == 0) {
                        indexLigneTexteHerite = TEXTE_HERITE_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    remplirLigneOngletTexteHerite(clause, indexLigneTexteHerite);
                    break;
                case 6:
                    if (indexLigneTableauChoixExclusif == 0) {
                        indexLigneTableauChoixExclusif = CHOIX_EXCLUSIF_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    indexLigneTableauChoixExclusif = remplirOngletChoixExclusif(clause, indexLigneTableauChoixExclusif, 1, CHOIX_EXCLUSIF_NOMBRE_CELLULE_TABLEAU);
                    break;
                case 7:
                    if (indexLigneTableauChoixMultiple == 0) {
                        indexLigneTableauChoixMultiple = CHOIX_EXCLUSIF_NUMERO_PREMIERE_LIGNE_TABLEAU;
                    }
                    indexLigneTableauChoixMultiple = remplirOngletChoixExclusif(clause, indexLigneTableauChoixMultiple, 2, CHOIX_MULTIPLE_NOMBRE_CELLULE_TABLEAU);
                    break;
                default:
                    break;
            }
            indexLigneTableauTouteClause++;
        }
    }

    /**
     * Rempli une ligne de l'onglet 1 du tableau.
     *
     * @param clause     la clause utilisée pour replir la ligne
     * @param indexLigne le numéro de la ligne à remplir.
     */
    private void remplirOngletTouteClause(final ClauseExport clause, int indexLigne) {
        HSSFSheet page = getWb().getSheet(Constantes.TOUTES_CLAUSES);
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null) {
            ligne = creerLigne(indexLigne, TOUTES_CLAUSES_NOMBRE_CELLULE_TABLEAU, 0, page);
        }
        remplirCellule(ligne, TOUTES_CLAUSES_COL_REFERENCE, clause.getReference());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_TYPE_CLAUSE, clause.getLibelleTypeClause());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_TYPE_DOC, clause.getLibelleTypeDocument());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_PROCEDURE, clause.getLibelleProcedure());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_NAT_PRESTA, clause.getLibelleNaturePrestation());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_THEME, clause.getTheme());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_ACTIVE, (clause.isActif() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, TOUTES_CLAUSES_COL_CONDITIONNEMENT1, (clause.isPotentiellementConditionnee1() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, TOUTES_CLAUSES_COL_CONDITIONNEMENT2, (clause.isPotentiellementConditionnee2() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, TOUTES_CLAUSES_COL_CRITERE1, clause.getLibelleCritereConditionnement1());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_VALEUR1, clause.getLibelleValeurConditionnement1());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_CRITERE2, clause.getLibelleCritereConditionnement2());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_VALEUR2, clause.getLibelleValeurConditionnement2());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_STATUT, clause.getStatut());
        remplirCellule(ligne, TOUTES_CLAUSES_COL_AUTEUR, clause.getAuteur());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateCreation = "";
        if (clause.getDateCreation() != null) {
            dateCreation = sdf.format(clause.getDateCreation());
        }
        remplirCellule(ligne, TOUTES_CLAUSES_COL_DATE_CREATION, dateCreation);

        String dateModification = "";
        if (clause.getDateModification() != null) {
            dateModification = sdf.format(clause.getDateModification());
        }
        remplirCellule(ligne, TOUTES_CLAUSES_COL_DATE_MODIFICATION, dateModification);

        remplirCellule(ligne, TOUTES_CLAUSES_COL_VERSION, clause.getVersion());
        StringBuilder reference = new StringBuilder();
        if (!clause.getReferenceCanevas().isEmpty()) {
            for (int i = 0; i < clause.getReferenceCanevas().size() - 1; i++) {
                reference.append(clause.getReferenceCanevas().get(i));
                reference.append(", ");
            }
            reference.append(clause.getReferenceCanevas().get(clause.getReferenceCanevas().size() - 1));
            remplirCellule(ligne, TOUTES_CLAUSES_COL_CANEVAS, reference.toString());
        }
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, TOUTES_CLAUSES_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }
    }

    /**
     * Rempli une ligne de l'onglet 2 et 3 du tableau.
     *
     * @param clause     la clause utilisée pour replir la ligne
     * @param indexLigne le numéro de la ligne à remplir.
     * @parma nombreCelluleTableau le nombre de cellule du tableau
     * @rerturn le nouvel index mis à jour pour l'insertion de la ligne suivante.
     */
    private int remplirOngletChoixExclusif(final ClauseExport clause, int indexLigne, final int numeroOnglet, final int nombreCelluleTableau) {
        HSSFSheet page;
        switch (numeroOnglet) {
            case 1:
                page = getWb().getSheet(Constantes.CHOIX_EXCLUSIF);
                break;
            case 2:
                page = getWb().getSheet(Constantes.CHOIX_MULTIPLE);
                break;
            default:
                page = getWb().getSheetAt(numeroOnglet);
                break;
        }
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null) {
            ligne = creerLigne(indexLigne, nombreCelluleTableau, 0, page);
        }
        if (clause.getListeRoleClause() == null) {
            remplirLigneOngletChoixExclusif(clause, ligne, null);
            indexLigne++;
        } else {
            for (RoleClause roleClause : clause.getListeRoleClause()) {
                ligne = creerLigne(indexLigne, nombreCelluleTableau, 0, page);
                remplirLigneOngletChoixExclusif(clause, ligne, roleClause);
                indexLigne++;
            }
        }
        return indexLigne;
    }

    /**
     * Rempli une ligne de l'onglet 2 et 3 du tableau.
     *
     * @param clause     clause la clause utilisée pour replir la ligne
     * @param ligne      la ligne du fichier excel.
     * @param roleClause le {@link RoleClause} qui contient les informations supplémentaire.
     */
    private void remplirLigneOngletChoixExclusif(final ClauseExport clause, HSSFRow ligne, final RoleClause roleClause) {
        remplirLigneCommun(ligne, clause);
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_SAUT_LIGNE_1, (clause.isSautLigneTexteAvant() ? Constantes.OUI : Constantes.NON));
        if (roleClause != null) {
            remplirCellule(ligne, CHOIX_EXCLUSIF_COL_NUMERO_FORMULATION, String.valueOf(roleClause.getNumFormulation()));
            remplirCellule(ligne, CHOIX_EXCLUSIF_COL_TAILLE_CHAMP, roleClause.getLibelleTailleChamp());
            remplirCellule(ligne, CHOIX_EXCLUSIF_COL_VALEUR_DEFAUT, roleClause.getValeurDefaut());
            remplirCellule(ligne, CHOIX_EXCLUSIF_COL_PRE_COCHE, roleClause.isPrecochee() ? Constantes.OUI : Constantes.NON);
        }
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_FORMULATION_MODIFIABLE, ("1".equals(clause.getFormulationModifiable()) ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_PARAM_DIRECTION, ("1".equals(clause.getParametrableDirection()) ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_PARAM_AGENT, ("1".equals(clause.getParametrableAgent()) ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_SAUT_LIGNE_2, (clause.isSautLigneTexteApres() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_TXT_FIXE_APRES, filtrerContenu(clause.getTexteFixeApres()));
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, CHOIX_EXCLUSIF_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }

    }

    /**
     * Création d'une ligne de l'onglet clause texte fixe.
     *
     * @param clause     la clause {@link ClauseExport}
     * @param indexLigne le numéro de la clause.
     */
    private void remplirLigneOngletTexteFixe(final ClauseExport clause, int indexLigne) {
        HSSFSheet page = getWb().getSheet(Constantes.TEXTE_FIXE);
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null)
            ligne = creerLigne(indexLigne, TEXTE_FIXE_NOMBRE_CELLULE_TABLEAU, 0, page);
        remplirLigneCommun(ligne, clause);
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, TEXTE_FIXE_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }

        indexLigneTexteFixe++;
    }

    /**
     * Création d'une ligne de l'onglet clause texte libre.
     *
     * @param clause     la clause {@link ClauseExport}
     * @param indexLigne le numéro de la clause.
     */
    private void remplirLigneOngletTexteLibre(final ClauseExport clause, int indexLigne) {
        HSSFSheet page = getWb().getSheet(Constantes.CHAMP_LIBRE);
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null)
            ligne = creerLigne(indexLigne, TEXTE_LIBRE_NOMBRE_CELLULE_TABLEAU, 0, page);
        remplirLigneCommun(ligne, clause);
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_SAUT_LIGNE_1, (clause.isSautLigneTexteAvant() ? Constantes.OUI : Constantes.NON));
        if (clause.getListeRoleClause() != null && !clause.getListeRoleClause().isEmpty()) {
            RoleClause roleClause = clause.getListeRoleClause().get(0);
            if (roleClause != null) {
                remplirCellule(ligne, TEXTE_LIBRE_COL_TAILLE_CHAMP, roleClause.getLibelleTailleChamp());
                remplirCellule(ligne, TEXTE_LIBRE_COL_CHAMP_LIBRE_OBLIGATOIRE, ("1".equals(roleClause.getChampObligatoire()) ? Constantes.OUI : Constantes.NON));
            }
        }
        remplirCellule(ligne, TEXTE_LIBRE_COL_SAUT_LIGNE_2, (clause.isSautLigneTexteApres() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, TEXTE_LIBRE_COL_TXT_FIXE_APRES, filtrerContenu(clause.getTexteFixeApres()));
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, TEXTE_LIBRE_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }

        indexLigneTexteLibre++;
    }

    /**
     * Création d'une ligne de l'onglet clause champ prévalorisé.
     *
     * @param clause     la clause {@link ClauseExport}
     * @param indexLigne le numéro de la clause.
     */
    private void remplirLigneOngletPrevalorise(final ClauseExport clause, int indexLigne) {
        HSSFSheet page = getWb().getSheet(Constantes.CHAMP_PREVALORISE);
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null)
            ligne = creerLigne(indexLigne, CHOIX_PREVALORISE_NOMBRE_CELLULE_TABLEAU, 0, page);
        remplirLigneCommun(ligne, clause);
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_SAUT_LIGNE_1, (clause.isSautLigneTexteAvant() ? Constantes.OUI : Constantes.NON));
        if (clause.getListeRoleClause() != null && !clause.getListeRoleClause().isEmpty()) {

            RoleClause roleClause = clause.getListeRoleClause().get(0);
            if (roleClause != null) {
                remplirCellule(ligne, CHOIX_PREVALORISE_COL_TAILLE_CHAMP, roleClause.getLibelleTailleChamp());
                remplirCellule(ligne, CHOIX_PREVALORISE_COL_VALEUR_DEFAUT, roleClause.getValeurDefaut());
            }
        }
        remplirCellule(ligne, CHOIX_PREVALORISE_COL_PARAM_DIRECTION, ("1".equals(clause.getParametrableDirection()) ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_PREVALORISE_COL_PARAM_AGENT, ("1".equals(clause.getParametrableAgent()) ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_PREVALORISE_COL_SAUT_LIGNE_2, (clause.isSautLigneTexteApres() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_PREVALORISE_COL_TXT_FIXE_APRES, filtrerContenu(clause.getTexteFixeApres()));
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, CHOIX_PREVALORISE_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }

        indexLigneTextePrevalorise++;
    }

    /**
     * Création d'une ligne de l'onglet texte hérité.
     *
     * @param clause     la clause {@link ClauseExport}
     * @param indexLigne le numéro de la clause.
     */
    private void remplirLigneOngletTexteHerite(final ClauseExport clause, int indexLigne) {
        HSSFSheet page = getWb().getSheet(Constantes.VALEUR_HERITEE);
        HSSFRow ligne = page.getRow(indexLigne);
        if (ligne == null)
            ligne = creerLigne(indexLigne, TEXTE_HERITE_NOMBRE_CELLULE_TABLEAU, 0, page);
        remplirLigneCommun(ligne, clause);
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_SAUT_LIGNE_1, (clause.isSautLigneTexteAvant() ? Constantes.OUI : Constantes.NON));
        if (clause.getListeRoleClause() != null && !clause.getListeRoleClause().isEmpty()) {
            RoleClause roleClause = clause.getListeRoleClause().get(0);
            if (roleClause != null) {
                remplirCellule(ligne, TEXTE_HERITE_COL_LIBELLE_VAL_HERITE, roleClause.getLibelleValeurHeritee());
                remplirCellule(ligne, TEXTE_HERITE_COL_CHAMP_HERITE_MODIFIABLE, ("1".equals(roleClause.getValeurHeriteeModifiable()) ? Constantes.OUI : Constantes.NON));
            }
        }
        remplirCellule(ligne, TEXTE_LIBRE_COL_SAUT_LIGNE_2, (clause.isSautLigneTexteApres() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, TEXTE_LIBRE_COL_TXT_FIXE_APRES, filtrerContenu(clause.getTexteFixeApres()));
        if (clause.getInfoBulle() != null && clause.getInfoBulle().hasContent()) {
            remplirCellule(ligne, TEXTE_LIBRE_COL_INFO_BULLE, clause.getInfoBulle().toString());
        }

        indexLigneTexteHerite++;
    }

    /**
     * Création des cellules communes à plusieurs tableau.
     *
     * @param ligne  la ligne courante.
     * @param clause la clause {@link ClauseExport}
     */
    private void remplirLigneCommun(HSSFRow ligne, final ClauseExport clause) {
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_REFERENCE, clause.getReference());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_TYPE_DOC, clause.getLibelleTypeDocument());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_PROCEDURE, clause.getLibelleProcedure());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_NAT_PRESTA, clause.getLibelleNaturePrestation());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_THEME, clause.getTheme());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_ACTIVE, (clause.isActif() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_CONDITIONNEMENT1, (clause.isPotentiellementConditionnee1() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_CONDITIONNEMENT2, (clause.isPotentiellementConditionnee2() ? Constantes.OUI : Constantes.NON));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_CRITERE1, clause.getLibelleCritereConditionnement1());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_VALEUR1, clause.getLibelleValeurConditionnement1());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_CRITERE2, clause.getLibelleCritereConditionnement2());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_VALEUR2, clause.getLibelleValeurConditionnement2());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_TXT_FIXE_AVANT, filtrerContenu(clause.getTexteFixeAvant()));
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_STATUT, clause.getStatut());
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_AUTEUR, clause.getAuteur());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateCreation = "";
        if (clause.getDateCreation() != null) {
            dateCreation = sdf.format(clause.getDateCreation());
        }
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_DATE_CREATION, dateCreation);

        String dateModification = "";
        if (clause.getDateModification() != null) {
            dateModification = sdf.format(clause.getDateModification());
        }
        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_DATE_MODIFICATION, dateModification);

        remplirCellule(ligne, CHOIX_EXCLUSIF_COL_VERSION, clause.getVersion());


    }

    /**
     * Création d'une ligne
     *
     * @param ligne         le numéro de la ligne
     * @param nombreCellule le nombre de cellule de la ligne
     * @param celluleDebut  le numéro de la premiére cellule
     * @param page          la page du document
     * @return la ligne crée
     */
    private HSSFRow creerLigne(final int ligne, final int nombreCellule, final int celluleDebut, HSSFSheet page) {
        HSSFRow row = page.createRow(ligne);
        for (int i = celluleDebut; i < nombreCellule; i++) {
            row.createCell((short) i);
        }
        return row;
    }

    /**
     * Rempli la cellule avec la chaîne.
     *
     * @param ligne        ligne où se situe la cellule
     * @param numeroColone numeroColone de la cellule
     * @param chaine       texte à mettre dans la cellule
     */
    private void remplirCellule(final HSSFRow ligne, final short numeroColone, final String chaine) {

        HSSFCell cellule = ligne.getCell(numeroColone);
        if (cellule == null) {
            cellule = ligne.createCell(numeroColone);
        }

        cellule.setCellValue(new HSSFRichTextString((chaine == null ? "" : chaine)));
    }

    /**
     * @param contenu
     * @return String
     */
    private String filtrerContenu(final String contenu) {
        if (contenu == null) {
            return "";
        }
        String contenuFiltre = contenu.replaceAll(HTML_RETOUR_LIGNE, "\n");
        contenuFiltre = contenuFiltre.replaceAll("<br/>", "\n");
        contenuFiltre = contenuFiltre.replaceAll(HTML_DEBUT_ELEMENT_NUMEROTE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_DEBUT_LISTE_NUMEROTE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_DEBUT_PARAGRAPHE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_DEBUT_STYLE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_FIN_ELEMENT_NUMEROTE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_FIN_LISTE_NUMEROTE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_FIN_PARAGRAPHE, "");
        contenuFiltre = contenuFiltre.replaceAll(HTML_FIN_STYLE, "");

        return contenuFiltre;
    }
}
