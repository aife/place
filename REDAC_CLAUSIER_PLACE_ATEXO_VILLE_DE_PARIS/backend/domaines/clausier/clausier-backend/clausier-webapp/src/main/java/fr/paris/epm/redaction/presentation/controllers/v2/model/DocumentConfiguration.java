package fr.paris.epm.redaction.presentation.controllers.v2.model;

import java.util.List;


public class DocumentConfiguration {

    private List<String> activatedSheetNames;

    private String defaultSheetName;

    private int initialDefaultSheetIndex;

    public List<String> getActivatedSheetNames() {
        return activatedSheetNames;
    }

    public void setActivatedSheetNames(List<String> activatedSheetNames) {
        this.activatedSheetNames = activatedSheetNames;
    }

    public String getDefaultSheetName() {
        return defaultSheetName;
    }

    public void setDefaultSheetName(String defaultSheetName) {
        this.defaultSheetName = defaultSheetName;
    }

    public int getInitialDefaultSheetIndex() {
        return initialDefaultSheetIndex;
    }

    public void setInitialDefaultSheetIndex(int initialDefaultSheetIndex) {
        this.initialDefaultSheetIndex = initialDefaultSheetIndex;
    }
}
