function actualize(id, html, scrollTo, isPremierClause) {
    console.log("id = #" + id + " -> html = " + html);

    var elt = jQuery('#' + id);
    elt.html(html);
    if (scrollTo && isPremierClause && elt.offset() !== undefined)
        jQuery(document).scrollTop(elt.offset().top-150);
    elt = null;
}

function actualizeTableauDynamiqueTextarea(id, html, scrollTo) {
    console.log("id = #" + id + " -> html = " + html);

    var elt = jQuery('#' + id);
    elt.text(html);
    if (scrollTo && elt.offset() !== undefined)
        jQuery(document).scrollTop(elt.offset().top-150);
    elt = null;
}

function scrollPopup(id) {
    console.log("id = #" + id);

    var elt = jQuery('#' + id);
    if (elt.offset() !== undefined)
        jQuery(document).scrollTop(elt.offset().top-150);
}

/**
 * Mettre à jour tous les clauses contenant un textarea dans la popup de
 * prévisualisation
 */
function miseAJourContenuPopUp() {

	miseAJourContenuTextAreaTinyMCE();
	miseAJourContenuRadioButtons();
	miseAJourCheckBox();
	miseAJourTableauDynamique();

	// mise a jour saut de ligne
	opener.ajouterSautLigne(opener.nombreSautLignes, opener.chapitreIdentifiant);

}

function miseAJourContenuTextAreaTinyMCE() {
	opener.jQuery('textarea').each(function() {
		var id = jQuery(this).attr('id');
		if (!opener.isExcluded(id)) {
			if (opener.getMCE(id) != null) {
				actualize(id, opener.getMCE(id).getContent(), true);
			}
		}
	});
}

function miseAJourContenuTextArea() {
	opener.jQuery('textarea').each(function() {
		var id = jQuery(this).attr('id');
		if (!opener.isExcluded(id)) {
			if (opener.document.getElementById(id) != undefined) {
				actualize(id, opener.document.getElementById(id).value, false);
			}
		}
	});
}

function miseAJourContenuRadioButtons() {
	// choix exclusif actif
	if (opener.identifiantsChoixExclusifActifList != undefined) {
		for (var i = 0; i < opener.identifiantsChoixExclusifActifList.length; i++) {
			opener.decocherCaseClauseChoixMultiple(
					opener.identifiantsChoixExclusifActifList[i], 'inline');
		}
	}

	// choix exclusif inactif
	if (opener.identifiantsChoixExclusifInactifList != undefined) {
		for (var i = 0; i < opener.identifiantsChoixExclusifInactifList.length; i++) {
			opener.decocherCaseClauseChoixMultiple(
					opener.identifiantsChoixExclusifInactifList[i], 'none');
		}
	}
}

function miseAJourCheckBox() {
	// checkbox actif
	if (opener.identifiantsChoixMultipleActifList != undefined) {
		for (var i = 0; i < opener.identifiantsChoixMultipleActifList.length; i++) {
			opener.decocherCaseClauseChoixMultiple(
					opener.identifiantsChoixMultipleActifList[i], 'inline');
		}
	}

	// checkbox inactif
	if (opener.identifiantsChoixMultipleInactifList != undefined) {
		for (var i = 0; i < opener.identifiantsChoixMultipleInactifList.length; i++) {
			opener.decocherCaseClauseChoixMultiple(
					opener.identifiantsChoixMultipleInactifList[i], 'none');
		}
	}
}

function miseAJourTableauDynamique() {
	jQuery('tbody[id^=body_tableau_dynamique_]')
	.each(
			function() {
				//efface tous les lignes du tableau
				var id = jQuery(this).attr('id');
				var tbody = document.getElementById(id);
				var total = tbody.childNodes.length;
				for (var i = 0; i < total; i++) {
					tbody.removeChild(tbody.childNodes[0]);
				}
				
				var idClause = id.substring(id.lastIndexOf("_"), id.legth);
				
				//recreer la structure du tableau
				recreerStructureTableauDynamiqueAppercu(tbody, idClause);
				
			});	
}
			
			
function recreerStructureTableauDynamiqueAppercu(tbody, idClause) {
	// recreer la structure du tableau
	for (var i = 0; i < opener.identifiantsLignesTableauDynamiqueList.length; i++) {
		var idLigne = opener.identifiantsLignesTableauDynamiqueList[i];
		var tr = creerElement("tr", idLigne);
		tr = remplirDonneesTableauDynamiqueAppercu(i, tr, idClause);
		tbody.appendChild(tr);
	}
}

function remplirDonneesTableauDynamiqueAppercu(position, trApercu, idClause) {
	var table = opener.document.getElementById('table_tableau_dynamique'+idClause);
	if(table != null && table != undefined) {
		var tbody = table.getElementsByTagName('tbody');
		if(tbody != null && tbody != undefined) {
			var tr = tbody.item(0).childNodes[position];
			for(var j = 0; j < tr.childNodes.length; j++) {
				var tdApercu = creerColonne(tr.childNodes[j]);
				trApercu.appendChild(tdApercu);
			}
		}
	}
	
	return trApercu;
}

function creerElement(elemName, id) {
	var elem = document.createElement(elemName);
	elem.setAttribute('id', id);
	return elem;
}

function creerColonne(td) {
	var tdApercu = creerElement("td", td.childNodes[0].id);
	tdApercu.textContent = td.childNodes[0].value;
	if(tdApercu.textContent == "" || tdApercu.textContent == undefined || tdApercu.textContent == "undefined") {
		tdApercu.innerHTML = td.childNodes[0].innerHTML;
	}
	return tdApercu;
}
