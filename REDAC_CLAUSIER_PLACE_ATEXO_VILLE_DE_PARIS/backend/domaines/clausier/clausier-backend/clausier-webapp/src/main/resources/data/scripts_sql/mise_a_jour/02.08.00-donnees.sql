-- Reprise de donnes de l'association canevas procedures

CREATE OR REPLACE FUNCTION repriseCanevasProcedure()
RETURNS void AS
$BODY$
DECLARE
  rec RECORD;
BEGIN
 -- Reprise des canevas associes a une seule procedure
 FOR rec IN 	SELECT *	
		FROM epm__t_canevas
 LOOP
	INSERT INTO epm__t_canevas_has_ref_procedure_passation(
            id_canevas, id_ref_procedure_passation)
	VALUES (rec.id, rec.id_procedure_passation);
 END LOOP;
 RETURN;
END;   
$BODY$
LANGUAGE PLPGSQL VOLATILE;

SELECT * FROM repriseCanevasProcedure();

DROP FUNCTION repriseCanevasProcedure();

ALTER TABLE epm__t_canevas DROP COLUMN id_procedure_passation;

-- Reprise de donnees de l'association d'une clause a plusieurs valeurs potentiellement conditionnees

CREATE OR REPLACE FUNCTION repriseClauseValeurPotentiellementConditionnee()
RETURNS void AS
$BODY$
DECLARE
  rec RECORD;
BEGIN
 -- Reprise des canevas associes a une seule procedure
 FOR rec IN 	SELECT *	
		FROM epm__t_clause
		WHERE id_ref_potentiellement_conditionnee IS NOT NULL
 LOOP
	INSERT INTO epm__t_clause_has_valeur_potentiellement_conditionnee(id_clause, valeur)
	VALUES (rec.id, rec.id_ref_valeur_potentiellement_conditionnee);
 END LOOP;
 RETURN;
END;   
$BODY$
LANGUAGE PLPGSQL VOLATILE;

SELECT * FROM repriseClauseValeurPotentiellementConditionnee();

DROP FUNCTION repriseClauseValeurPotentiellementConditionnee();

ALTER TABLE epm__t_clause DROP COLUMN id_ref_valeur_potentiellement_conditionnee;

UPDATE epm__t_ref_potentiellement_conditionnee
   SET multi_choix = TRUE
 WHERE id IN (3, 24); -- Procedure et Variation du prix

