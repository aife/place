package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.referentiel.*;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.util.Constantes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe utilisée pour le conditionnement des clause lors de la création du
 * document.
 *
 * @author Régis Menet.
 * @version $Revision$, $Date$, $Author$
 */
public class ConditionnementConsultation {

    /**
     * marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ConditionnementConsultation.class);

    /**
     * consultation associé au futur document.
     */
    private EpmTConsultation consultation;

    /**
     * propriété de la consultation.
     */
    private EpmTBudLotOuConsultation epmTBudLotOuConsultation;

    /**
     * lots associés à la consultation.
     */
    private Set<EpmTBudLot> epmTBudLots;

    /**
     *
     */
    private Set lotsReserves;

    private ConsultationServiceSecurise consultationService = null;

    /**
     * @param valeur consultation pour tester les valeurs potentiellement
     *               conditionnées.
     */
    public ConditionnementConsultation(final EpmTConsultation valeur) {
        this.consultation = valeur;
        epmTBudLotOuConsultation = consultation
                .getEpmTBudLotOuConsultation();
        epmTBudLots = consultation.getEpmTBudLots();
        lotsReserves = consultation.getLotsReserves();
    }

    /**
     * Test si variantes autorisées présentent dans la consultation ou ses lots.
     *
     * @return true si variantes autorisées présent dans la consultation
     */
    public final String getVariantesAutorisees(final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getVariantesAutorisees() != null
                    && epmTBudLotOuConsultation.getVariantesAutorisees().equalsIgnoreCase(Constantes.OUI)) {
                return Constantes.OUI;
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    if (lot.getEpmTBudLotOuConsultation()
                            .getVariantesAutorisees() != null
                            && lot.getEpmTBudLotOuConsultation().getVariantesAutorisees().equalsIgnoreCase(Constantes.OUI)) {
                        return Constantes.OUI;
                    }
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * Test si variantes exigées présentent dans la consultation ou ses lots.
     *
     * @return true si variantes autorisées présent dans la consultation ou un des lots
     */
    public final String getVariantesExigees(int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (Constantes.OUI.equalsIgnoreCase(epmTBudLotOuConsultation.getVariantesExigees())) {
                return Constantes.OUI;
            }
        }
        if (epmTBudLots != null) {
            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    if (Constantes.OUI.equalsIgnoreCase(lot.getEpmTBudLotOuConsultation().getVariantesAutorisees())) {
                        return Constantes.OUI;
                    }
                }
            }
        }
        return Constantes.NON;
    }


    /**
     * Teste si les variantes techniques obligatoires sont présentes.
     *
     * @return boolean
     */
    public final String getVariantesTechniquesObligatoires(final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getOptionsTechniquesImposees() != null
                    && epmTBudLotOuConsultation.getOptionsTechniquesImposees().equalsIgnoreCase(Constantes.OUI)) {
                return Constantes.OUI;
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    if (lot.getEpmTBudLotOuConsultation()
                            .getOptionsTechniquesImposees() != null
                            && lot.getEpmTBudLotOuConsultation().getOptionsTechniquesImposees().equalsIgnoreCase(Constantes.OUI)) {
                        return Constantes.OUI;
                    }
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * teste si candidat présent.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getCandidatAdmisPresenter() {
        if (consultation.getNombreCandidatsFixe() != null) {
            return Constantes.OUI;
        } else if (consultation.getNombreCandidatsMin() != null
                && consultation.getNombreCandidatsMax() != null) {
            return Constantes.OUI;
        }
        return Constantes.NON;
    }

    /**
     * @param idLot identifiant du lot
     * @return renvoie le type de nature de prestation présente dans la
     * consultation ou ses lots.
     */
    public final int getNaturePrestations(final int idLot) {
        String allotissement = consultation.getAllotissement();
        if (Constantes.NON.equalsIgnoreCase(allotissement)) {
            return consultation.getEpmTRefNature().getId();
        } else if (Constantes.OUI.equalsIgnoreCase(allotissement) && idLot > 0 && !CollectionUtils.isEmpty(epmTBudLots)) {
            var lot = epmTBudLots.stream().filter(epmTBudLot -> epmTBudLot.getId() == idLot).findFirst().orElse(null);
            if (lot != null) {
                return lot.getId();
            }
        }

        return consultation.getEpmTRefNature().getId();
    }

    /**
     * Teste la valeur du CCAG présent dans la consultation et les lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final boolean getCCAG(final int id, final int idLot) {
        //on considèrera que la condition CCAG n'est pas respectée si le CCAG n'est pas renseigné dans la consultation ou dans les lots
        LOG.info("Methode CCAG pour condition CCAG = {} et LOT = {}", id, idLot);
        String allotissement = consultation.getAllotissement();
        if (allotissement != null && allotissement.equalsIgnoreCase(Constantes.NON)) {
            //on vérifie si la condition CCAG est respectée pour la consultation
            LOG.info("Consultation non allotie");
            if (epmTBudLotOuConsultation != null
                    && epmTBudLotOuConsultation.getEpmTRefCcag() != null) {
                LOG.info("CCAG correspondant à l'id {} trouvé pour la consultation", id);

                return epmTBudLotOuConsultation.getEpmTRefCcag().getId() == id;
            } else {
                LOG.info("CCAG de consultation non renseigné");
                return false;
            }
        } else if (idLot < 1) {
            //on vérifie si la condition CCAG est respectée pour au moins un des lots
            Boolean conditionRespectee = Boolean.FALSE;
            LOG.info("Consultation allotie: lot non défini, parcours de tous les lots pour CCAG = {}", id);
            if (epmTBudLots != null) {
                Iterator it = epmTBudLots.iterator();
                while (it.hasNext()) {
                    EpmTBudLot lot = (EpmTBudLot) it.next();
                    if (lot != null && lot.getEpmTBudLotOuConsultation() != null && lot.getEpmTBudLotOuConsultation().getEpmTRefCcag() != null && lot.getEpmTBudLotOuConsultation().getEpmTRefCcag()
                            .getId() == id) {
                        LOG.info("CCAG avec l'id {} trouvé dans le lot #{} - {}", id, lot.getId(), lot.getIntituleLot());
                        conditionRespectee = Boolean.TRUE;
                    }
                }
                if (conditionRespectee.equals(Boolean.TRUE)) {
                    return true;
                } else {
                    LOG.info("CCAG avec l'id {} NON trouvé dans aucun des lots", id);
                    return false;
                }
            }
        } else {
            //on vérifie si la condition CCAG est respectée pour le lot
            LOG.info("Allotissement pour le lot {}", idLot);
            Boolean conditionRespectee = Boolean.FALSE;
            if (epmTBudLots != null) {
                Iterator it = epmTBudLots.iterator();
                while (it.hasNext()) {
                    EpmTBudLot lot = (EpmTBudLot) it.next();
                    if (lot != null && lot.getEpmTBudLotOuConsultation() != null && lot.getEpmTBudLotOuConsultation().getEpmTRefCcag() != null && lot.getId() == idLot && lot.getEpmTBudLotOuConsultation().getEpmTRefCcag().getId() == id) {
                        LOG.info("Lot correspondant = #{} - {}", id, lot.getId(), lot.getIntituleLot());
                        conditionRespectee = Boolean.TRUE;
                    }
                }
                if (conditionRespectee.equals(Boolean.TRUE)) {
                    return true;
                } else {
                    LOG.info("CCAG avec l'id {} NON trouvé dans le lot #{}", id, idLot);
                    return false;
                }

            } else {
                LOG.info("Aucun lot dans la consultation");
                return false;
            }
        }
        return false;
    }

    /**
     * Teste su des tranches sont présentes dans la consultation et ses lots
     * associés.
     *
     * @return boolean
     */
    public final String getTranche(final int idLot) {
        if (epmTBudLotOuConsultation != null
                && epmTBudLotOuConsultation.getEpmTBudTranches() != null
                && !epmTBudLotOuConsultation.getEpmTBudTranches().isEmpty()) {
            return Constantes.OUI;
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getEpmTBudTranches() != null
                        && !lot.getEpmTBudLotOuConsultation().getEpmTBudTranches().isEmpty()) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * teste si la "reconduction" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getReconduction(final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getReconductible() != null
                    && epmTBudLotOuConsultation.getReconductible().equalsIgnoreCase(Constantes.OUI)) {
                return Constantes.OUI;
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getReconductible() != null
                        && lot.getEpmTBudLotOuConsultation().getReconductible().equalsIgnoreCase(Constantes.OUI)) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * @param idLot identifiant du lot
     * @return conditionnement
     */
    public final String getReservationLots(final int idLot) {
        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {

            for (EpmTRefClausesSociales ClauseSocialeChoixUtilisateur : consultation.getEpmTBudLotOuConsultation().getClausesSocialesChoixUtilisateur()) {
                if (ClauseSocialeChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesSociales.CODE_CLAUSE_RESERVE)) {
                    return Constantes.OUI;
                }
            }

        } else if (epmTBudLots != null) {

            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null && lot.getEpmTBudLotOuConsultation().getClausesSociales()) {

                    for (EpmTRefClausesSociales ClauseSocialeChoixUtilisateur : lot.getEpmTBudLotOuConsultation().getClausesSocialesChoixUtilisateur()) {
                        if (ClauseSocialeChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesSociales.CODE_CLAUSE_RESERVE)) {
                            return Constantes.OUI;
                        }
                    }
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * @return conditionnement
     * test si la consultation est MPS
     */
    public final String getMps() {
        if (consultation.getMarchePublicSimplifie()) {
            return Constante.OUI;
        } else {
            return Constante.NON;
        }
    }

    public final boolean getFormeGroupement(final int i) {
        return null != consultation.getEpmTRefGroupementAttributaire() && consultation.getEpmTRefGroupementAttributaire().getId() == i;
    }

    /**
     * @param idLot
     * @return
     */
    public final String getCritereAttributionCahierCharges(final int idLot) {
        if (consultation != null && consultation.getCritereAttribution() != null && consultation.getCritereAttribution().getId() == 4) {
            return Constantes.OUI;
        }

        if (epmTBudLots != null) {
            Iterator<?> it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getCritereAttribution() != null
                        && lot.getCritereAttribution().getId() == 4) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;

    }

    /**
     * Teste si un lot technique est associé à la consultation ou a ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getLotTechnique(final int idLot) {
        if ((epmTBudLotOuConsultation != null
                && epmTBudLotOuConsultation.getEpmTLotTechniques() != null && !epmTBudLotOuConsultation
                .getEpmTLotTechniques().isEmpty())) {
            return Constantes.OUI;
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getEpmTLotTechniques() != null
                        && !lot.getEpmTBudLotOuConsultation().getEpmTLotTechniques().isEmpty()) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * @param id identifiant du prix unitaire à comparer avec celui présent dans
     *           la consultation.
     * @return
     */
    public final boolean getPrixUnitaire(final int id, int idLot) {
        // niveau consultation
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                if (verifieBonQuantite(epmTBudLotOuConsultation
                        .getEpmTBudFormePrix(), id)) {
                    return true;
                }
            }
            // niveau tranche
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null) {
                Iterator it = epmTBudLotOuConsultation
                        .getEpmTBudTranches().iterator();
                while (it.hasNext()) {
                    EpmTBudTranche tranche = (EpmTBudTranche) it.next();
                    if (tranche.getEpmTBudFormePrix() != null) {
                        if (verifieBonQuantite(tranche
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                }
            }
        }
        // niveau lots
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    EpmTBudLotOuConsultation lotOuCons = lot
                            .getEpmTBudLotOuConsultation();
                    if (lotOuCons.getEpmTBudFormePrix() != null) {
                        if (verifieBonQuantite(lotOuCons
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                    // niveau tranche des lots
                    if (lotOuCons.getEpmTBudTranches() != null) {
                        Iterator it2 = lotOuCons.getEpmTBudTranches()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTBudTranche tranche = (EpmTBudTranche) it2
                                    .next();
                            if (tranche.getEpmTBudFormePrix() != null) {
                                if (verifieBonQuantite(tranche
                                        .getEpmTBudFormePrix(), id)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param emptBudFormePrix {@link EpmTBudFormePrix}
     * @param id               identifiant à comparer
     * @return true si id présent.
     */
    private boolean verifieBonQuantite(
            final EpmTBudFormePrix emptBudFormePrix, final int id) {
        if (emptBudFormePrix != null) {
            if (emptBudFormePrix instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) emptBudFormePrix;
                if (epmTBudFormePrixPm.getPuEpmTRefBonQuantite() != null) {
                    if (epmTBudFormePrixPm.getPuEpmTRefBonQuantite()
                            .getId() == id) {
                        return true;
                    }
                }
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) emptBudFormePrix;
                if (epmTBudFormePrixPu.getEpmTRefBonQuantite() != null) {
                    if (epmTBudFormePrixPu.getEpmTRefBonQuantite().getId() == id) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @return test la forme de prix disponible dans la consultation ou dans ses
     * lots associés.
     */
    public final boolean getFormePrix(final int id, final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null
                    && !epmTBudLotOuConsultation.getEpmTBudTranches().isEmpty()) {
                Iterator it = epmTBudLotOuConsultation
                        .getEpmTBudTranches().iterator();
                while (it.hasNext()) {
                    EpmTBudTranche tranche = (EpmTBudTranche) it.next();
                    if (tranche.getEpmTBudFormePrix() != null) {
                        if (verifieFormePrix(tranche.getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                }
            } else if (epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                if (verifieFormePrix(epmTBudLotOuConsultation
                        .getEpmTBudFormePrix(), id)) {
                    return true;
                }
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    EpmTBudLotOuConsultation lotOuCons = lot
                            .getEpmTBudLotOuConsultation();
                    if (lotOuCons.getEpmTBudTranches() != null
                            && !lotOuCons.getEpmTBudTranches().isEmpty()) {
                        Iterator it2 = lotOuCons.getEpmTBudTranches()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTBudTranche tranche = (EpmTBudTranche) it2
                                    .next();
                            if (tranche.getEpmTBudFormePrix() != null) {
                                if (verifieFormePrix(tranche
                                        .getEpmTBudFormePrix(), id)) {
                                    return true;
                                }
                            }
                        }
                    } else if (lotOuCons.getEpmTBudFormePrix() != null) {
                        if (verifieFormePrix(lotOuCons
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param id identifiant à comparer
     * @return true si identifiant present dans la consultation
     */
    public final boolean getVariationPrix(final int id, final int idLot) {
        // option Tous est choisie en tant que conditionnement
        if (id == 0) {
            return true;
        }
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                if (verifieVariationPrix(epmTBudLotOuConsultation
                        .getEpmTBudFormePrix(), id)) {
                    return true;
                }
            }
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null) {
                Iterator it = epmTBudLotOuConsultation
                        .getEpmTBudTranches().iterator();
                while (it.hasNext()) {
                    EpmTBudTranche tranche = (EpmTBudTranche) it.next();
                    if (tranche.getEpmTBudFormePrix() != null) {
                        if (verifieVariationPrix(tranche
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                }
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    EpmTBudLotOuConsultation lotOuCons = lot
                            .getEpmTBudLotOuConsultation();
                    if (lotOuCons.getEpmTBudFormePrix() != null) {
                        if (verifieVariationPrix(lotOuCons
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                    if (lotOuCons.getEpmTBudTranches() != null) {
                        Iterator it2 = lotOuCons.getEpmTBudTranches()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTBudTranche tranche = (EpmTBudTranche) it2
                                    .next();
                            if (tranche.getEpmTBudFormePrix() != null) {
                                if (verifieVariationPrix(tranche
                                        .getEpmTBudFormePrix(), id)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param id identifiant à comparer
     * @return true si identifiant présent.
     */
    public final boolean getTypePrix(final int id, final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                if (verifieTypePrix(epmTBudLotOuConsultation
                        .getEpmTBudFormePrix(), id)) {
                    return true;
                }
            }
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null) {
                Iterator it = epmTBudLotOuConsultation
                        .getEpmTBudTranches().iterator();
                while (it.hasNext()) {
                    EpmTBudTranche tranche = (EpmTBudTranche) it.next();
                    if (tranche.getEpmTBudFormePrix() != null) {
                        if (verifieTypePrix(tranche.getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                }
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    EpmTBudLotOuConsultation lotOuCons = lot
                            .getEpmTBudLotOuConsultation();
                    if (lotOuCons.getEpmTBudFormePrix() != null) {
                        if (verifieTypePrix(lotOuCons
                                .getEpmTBudFormePrix(), id)) {
                            return true;
                        }
                    }
                    if (lotOuCons.getEpmTBudTranches() != null) {
                        Iterator it2 = lotOuCons.getEpmTBudTranches()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTBudTranche tranche = (EpmTBudTranche) it2
                                    .next();
                            if (tranche.getEpmTBudFormePrix() != null) {
                                if (verifieTypePrix(tranche
                                        .getEpmTBudFormePrix(), id)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param emptBudFormePrix forme de prix
     * @param id               identifiant à comparer
     * @return true si identifiant présent
     */
    private final boolean verifieTypePrix(
            final EpmTBudFormePrix emptBudFormePrix, final int id) {

        if (emptBudFormePrix != null) {
            if (emptBudFormePrix instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) emptBudFormePrix;
                if (epmTBudFormePrixPm.getPuEpmTRefTypePrix() != null
                        && !epmTBudFormePrixPm.getPuEpmTRefTypePrix().isEmpty()) {
                    Iterator it = epmTBudFormePrixPm
                            .getPuEpmTRefTypePrix().iterator();
                    while (it.hasNext()) {
                        EpmTRefTypePrix prix = (EpmTRefTypePrix) it.next();
                        if (prix.getId() == id) {
                            return true;
                        }
                    }
                }
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) emptBudFormePrix;
                if (epmTBudFormePrixPu.getEpmTRefTypePrix() != null
                        && !epmTBudFormePrixPu.getEpmTRefTypePrix().isEmpty()) {
                    Iterator it = epmTBudFormePrixPu.getEpmTRefTypePrix()
                            .iterator();
                    while (it.hasNext()) {
                        EpmTRefTypePrix prix = (EpmTRefTypePrix) it.next();
                        if (prix.getId() == id) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Teste si la consultation ou ses lots associés est à bon de commande.
     *
     * @return true si "à bon de commande"
     */
    public final boolean getBonCommandeMinMax(final int id, final int idLot) {
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getEpmTBudFormePrix() != null) {
                if (epmTBudLotOuConsultation.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                    EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) epmTBudLotOuConsultation
                            .getEpmTBudFormePrix();
                    if (epmTBudFormePrixPu.getEpmTRefBonQuantite() != null
                            && epmTBudFormePrixPu.getEpmTRefBonQuantite().getId() == 1
                            && epmTBudFormePrixPu.getEpmTRefMinMax() != null) {
                        if (epmTBudFormePrixPu.getEpmTRefMinMax().getId() == id) {
                            return true;
                        }
                    }
                } else if (epmTBudLotOuConsultation.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                    EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) epmTBudLotOuConsultation
                            .getEpmTBudFormePrix();
                    if (epmTBudFormePrixPm.getPuEpmTRefMinMax() != null
                            && epmTBudFormePrixPm.getPuEpmTRefBonQuantite().getId() == 1
                            && epmTBudFormePrixPm.getPuEpmTRefMinMax() != null) {
                        if (epmTBudFormePrixPm.getPuEpmTRefMinMax()
                                .getId() == id) {
                            return true;
                        }
                    }
                }
            }
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null) {
                Iterator it = epmTBudLotOuConsultation
                        .getEpmTBudTranches().iterator();
                while (it.hasNext()) {
                    EpmTBudTranche tranche = (EpmTBudTranche) it.next();
                    if (tranche.getEpmTBudFormePrix() != null) {
                        if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                            EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) tranche
                                    .getEpmTBudFormePrix();
                            if (epmTBudFormePrixPu.getEpmTRefBonQuantite() != null
                                    && epmTBudFormePrixPu.getEpmTRefBonQuantite().getId() == 1
                                    && epmTBudFormePrixPu.getEpmTRefMinMax() != null) {
                                if (epmTBudFormePrixPu.getEpmTRefMinMax()
                                        .getId() == id) {
                                    return true;
                                }
                            }
                        } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                            EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) tranche
                                    .getEpmTBudFormePrix();
                            if (epmTBudFormePrixPm.getPuEpmTRefMinMax() != null
                                    && epmTBudFormePrixPm.getPuEpmTRefBonQuantite().getId() == 1
                                    && epmTBudFormePrixPm.getPuEpmTRefMinMax() != null) {
                                if (epmTBudFormePrixPm
                                        .getPuEpmTRefMinMax().getId() == id) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (epmTBudLots != null) {
            Iterator it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = (EpmTBudLot) it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null) {
                    EpmTBudLotOuConsultation lotOuCons = lot
                            .getEpmTBudLotOuConsultation();
                    if (lotOuCons.getEpmTBudFormePrix() != null) {
                        if (lotOuCons.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                            EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) lotOuCons
                                    .getEpmTBudFormePrix();
                            if (epmTBudFormePrixPu.getEpmTRefBonQuantite() != null
                                    && epmTBudFormePrixPu.getEpmTRefBonQuantite().getId() == 1
                                    && epmTBudFormePrixPu.getEpmTRefMinMax() != null) {
                                if (epmTBudFormePrixPu.getEpmTRefMinMax()
                                        .getId() == id) {
                                    return true;
                                }
                            }
                        } else if (lotOuCons.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                            EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) lotOuCons
                                    .getEpmTBudFormePrix();
                            if (epmTBudFormePrixPm.getPuEpmTRefMinMax() != null
                                    && epmTBudFormePrixPm.getPuEpmTRefBonQuantite().getId() == 1
                                    && epmTBudFormePrixPm.getPuEpmTRefMinMax() != null) {
                                if (epmTBudFormePrixPm
                                        .getPuEpmTRefMinMax().getId() == id) {
                                    return true;
                                }
                            }
                        }
                    }
                    if (lotOuCons.getEpmTBudTranches() != null) {
                        Iterator it2 = lotOuCons.getEpmTBudTranches()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTBudTranche tranche = (EpmTBudTranche) it2
                                    .next();
                            if (tranche.getEpmTBudFormePrix() != null) {
                                if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                                    EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) tranche
                                            .getEpmTBudFormePrix();
                                    if (epmTBudFormePrixPu
                                            .getEpmTRefBonQuantite() != null
                                            && epmTBudFormePrixPu.getEpmTRefBonQuantite().getId() == 1
                                            && epmTBudFormePrixPu.getEpmTRefMinMax() != null) {
                                        if (epmTBudFormePrixPu
                                                .getEpmTRefMinMax()
                                                .getId() == id) {
                                            return true;
                                        }
                                    }
                                } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                                    EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) tranche
                                            .getEpmTBudFormePrix();
                                    if (epmTBudFormePrixPm
                                            .getPuEpmTRefMinMax() != null
                                            && epmTBudFormePrixPm.getPuEpmTRefBonQuantite().getId() == 1
                                            && epmTBudFormePrixPm.getPuEpmTRefMinMax() != null) {
                                        if (epmTBudFormePrixPm
                                                .getPuEpmTRefMinMax()
                                                .getId() == id) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param id identifiant du lot
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getLotsReserves(final int id) {
        if (lotsReserves != null) {
            Iterator it = lotsReserves.iterator();
            while (it.hasNext()) {
                EpmTRefReservationLot reservationLot = (EpmTRefReservationLot) it
                        .next();
                if (reservationLot.getId() == id) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * Teste si une forme de prix est : unitaire, forfaitaire ou mixte.
     *
     * @param emptBudFormePrix forme de prix de la consultation
     * @param id               identifiant de la forme de prix à comparer
     * @return true si forme de prix présente
     */
    private boolean verifieFormePrix(
            final EpmTBudFormePrix emptBudFormePrix, final int id) {
        if (emptBudFormePrix != null) {
            if (emptBudFormePrix instanceof EpmTBudFormePrixPm) {
                return EpmTBudFormePrix.FORME_PRIX_MIXTE == id;
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPu) {
                return EpmTBudFormePrix.FORME_PRIX_UNITAIRE == id;
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPf) {
                return EpmTBudFormePrix.FORME_PRIX_FORFAITAIRE == id;
            }
        }
        return false;
    }

    /**
     * @param emptBudFormePrix forme de prix associé à la consultation
     * @param id               identifiant de la variation de prix
     * @return true si variation présente dans la base
     */
    private boolean verifieVariationPrix(
            final EpmTBudFormePrix emptBudFormePrix, final int id) {

        if (emptBudFormePrix != null) {
            if (emptBudFormePrix instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm epmTBudFormePrixPm = (EpmTBudFormePrixPm) emptBudFormePrix;
                if (epmTBudFormePrixPm.getPfEpmTRefVariations() != null
                        && !epmTBudFormePrixPm.getPfEpmTRefVariations().isEmpty()) {
                    Iterator it = epmTBudFormePrixPm.getPfEpmTRefVariations().iterator();
                    while (it.hasNext()) {
                        EpmTRefVariation variation = (EpmTRefVariation) it.next();
                        if (variation.getId() == id) {
                            return true;
                        }
                    }
                }
                if (epmTBudFormePrixPm.getPuEpmTRefVariations() != null
                        && !epmTBudFormePrixPm.getPuEpmTRefVariations().isEmpty()) {
                    Iterator it = epmTBudFormePrixPm.getPuEpmTRefVariations().iterator();
                    while (it.hasNext()) {
                        EpmTRefVariation variation = (EpmTRefVariation) it.next();
                        if (variation.getId() == id) {
                            return true;
                        }
                    }
                }
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPf) {
                EpmTBudFormePrixPf epmTBudFormePrixPf = (EpmTBudFormePrixPf) emptBudFormePrix;
                if (epmTBudFormePrixPf.getPfEpmTRefVariations() != null
                        && !epmTBudFormePrixPf.getPfEpmTRefVariations().isEmpty()) {
                    Iterator it = epmTBudFormePrixPf
                            .getPfEpmTRefVariations().iterator();
                    while (it.hasNext()) {
                        EpmTRefVariation variation = (EpmTRefVariation) it
                                .next();
                        if (variation.getId() == id) {
                            return true;
                        }
                    }
                }
            } else if (emptBudFormePrix instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu epmTBudFormePrixPu = (EpmTBudFormePrixPu) emptBudFormePrix;
                if (epmTBudFormePrixPu.getPuEpmTRefVariations() != null
                        && !epmTBudFormePrixPu.getPuEpmTRefVariations().isEmpty()) {
                    Iterator it = epmTBudFormePrixPu
                            .getPuEpmTRefVariations().iterator();
                    while (it.hasNext()) {
                        EpmTRefVariation variation = (EpmTRefVariation) it
                                .next();
                        if (variation.getId() == id) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Retourne vrai ou faux dans le cas où la clef est trouvé dans parmi les paireType.
     * Exemple de configuration en base : conditionnementExterneOuiNon()[clef]
     *
     * @param lot  l'identifiant du lot
     * @param clef la clef des valeurs de conditionnement externe
     * @return
     */
    public final String getConditionnementExterneOuiNon(final int lot, final String clef) {
        if (consultation != null) {
            Collection<EpmTValeurConditionnementExterne> valeurConditionnementExternes = null;
            if (lot > 0) {
                epmTBudLots = consultation.getEpmTBudLots();
                for (EpmTBudLot epmTBudLot : epmTBudLots) {
                    if (epmTBudLot.getId() == lot) {
                        epmTBudLotOuConsultation = epmTBudLot.getEpmTBudLotOuConsultation();
                        valeurConditionnementExternes = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes();
                        break;
                    }
                }

            } else {
                valeurConditionnementExternes = consultation.getEpmTValeurConditionnementExterneList();
            }

            if (valeurConditionnementExternes != null) {
                for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : valeurConditionnementExternes) {
                    if (epmTValeurConditionnementExterne.getClef().equalsIgnoreCase(clef)) {
                        if (Constantes.OUI.equalsIgnoreCase(epmTValeurConditionnementExterne.getValeur())) {
                            return Constantes.OUI;
                        } else {
                            return Constantes.NON;
                        }
                    }
                }
            }
        }

        return Constantes.NON;
    }

    /**
     * Permet d'obtenir la valeur de la signature électronique
     *
     * @return requise, autorisée ou non-requise
     */
    public final Integer getSignatureElectronique() {
        return getConsultation().getSignature().intValue() + 1;
    }

    /**
     * @return Retourne "oui" ou "non" selon s'il y a un acte d'engagement a signature propre.
     */
    public final String getSignatureActeEngagementPropreOuiNon() {
        if (getConsultation().isSignatureEngagement()) {
            return Constantes.OUI;
        } else {
            return Constantes.NON;
        }
    }

    /**
     * teste si la "clauses sociales" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getSociales(final int idLot) {
        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {
            if (Constantes.OUI.equalsIgnoreCase(consultation.getClausesSociales())) {
                return Constantes.OUI;
            }
        } else if (epmTBudLots != null) {
            Iterator<EpmTBudLot> it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getClausesSociales()) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * Permet de verifier si les categories et sous-categories
     * sociales sont presents sur la consultation ou sur  ses lots associés
     *
     * @param idLot
     * @return
     */
    public final List<Integer> getCategoriesSociale(final int idLot) {
        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {
            if (consultation.getEpmTBudLotOuConsultation() != null && consultation.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves() != null
                    && !consultation.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves().isEmpty()) {
                return consultation.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves().stream().map(EpmTRef::getId).collect(Collectors.toList());
            }
        } else if (epmTBudLots != null) {
            List<Integer> listDispositionSociale = new ArrayList<>();
            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves() != null && !lot.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves().isEmpty()) {
                    listDispositionSociale.addAll(lot.getEpmTBudLotOuConsultation().getListeStructureSocialeReserves().stream().map(EpmTRef::getId).collect(Collectors.toList()));
                }
            }
            return listDispositionSociale;
        }
        return null;
    }

    /**
     * teste si la "clauses environnementales" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getEnvironnementales(final int idLot) {
        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {
            if (Constantes.OUI.equalsIgnoreCase(consultation.getClausesEnvironnementales())) {
                return Constantes.OUI;
            }
        } else if (epmTBudLots != null) {
            Iterator<EpmTBudLot> it = epmTBudLots.iterator();
            while (it.hasNext()) {
                EpmTBudLot lot = it.next();
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getClausesEnvironnementales()) {
                    return Constantes.OUI;
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * teste si les valeur choisit pour "clauses environnementales" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final List<Integer> getCategoriesEnvironnementales(final int idLot) {
        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {
            if (consultation.getEpmTBudLotOuConsultation() != null && consultation.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur() != null
                    && !consultation.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur().isEmpty()) {
                return consultation.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur().stream()
                        .map(EpmTRef::getId)
                        .collect(Collectors.toList());
            }
        } else if (epmTBudLots != null) {
            List<Integer> listDispositionEnvironementales = new ArrayList<>();
            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur() != null && !lot.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur().isEmpty()) {
                    listDispositionEnvironementales.addAll(lot.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur().stream()
                            .map(EpmTRef::getId)
                            .collect(Collectors.toList()));
                }
            }
            return listDispositionEnvironementales;
        }
        return null;
    }

    /**
     * teste si la "clauses sociales -condition d'execution" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getSocialesConditionExecution(final int idLot) {

        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {

            for (EpmTRefClausesSociales ClauseSocialeChoixUtilisateur : consultation.getEpmTBudLotOuConsultation().getClausesSocialesChoixUtilisateur()) {
                if (ClauseSocialeChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesSociales.CODE_CLAUSE_INSERTION)) {
                    return Constantes.OUI;
                }
            }

        } else if (epmTBudLots != null) {

            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getClausesSociales()) {

                    for (EpmTRefClausesSociales ClauseSocialeChoixUtilisateur : lot.getEpmTBudLotOuConsultation().getClausesSocialesChoixUtilisateur()) {
                        if (ClauseSocialeChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesSociales.CODE_CLAUSE_INSERTION)) {
                            return Constantes.OUI;
                        }
                    }
                }
            }
        }
        return Constantes.NON;
    }

    /**
     * teste si la "clauses environnementales -condition d'execution" est présente dans la consultation ou ses lots
     * associés.
     *
     * @return Constantes.OUI ou Constantes.NON
     */
    public final String getEnvironnementalesConditionExecution(final int idLot) {

        if (Constantes.NON.equalsIgnoreCase(consultation.getAllotissement())) {

            for (EpmTRefClausesEnvironnementales ClauseEnvironnementaleChoixUtilisateur : consultation.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur()) {
                if (ClauseEnvironnementaleChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesEnvironnementales.CODE_CLAUSE_SPECIFICATION)) {
                    return Constantes.OUI;
                }
            }

        } else if (epmTBudLots != null) {

            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                if (lot.getEpmTBudLotOuConsultation() != null
                        && lot.getEpmTBudLotOuConsultation().getClausesEnvironnementales()) {

                    for (EpmTRefClausesEnvironnementales ClauseEnvironnementaleChoixUtilisateur : lot.getEpmTBudLotOuConsultation().getClausesEnvironnementalesChoixUtilisateur()) {
                        if (ClauseEnvironnementaleChoixUtilisateur.getCodeExterne().equalsIgnoreCase(EpmTRefClausesEnvironnementales.CODE_CLAUSE_SPECIFICATION)) {
                            return Constantes.OUI;
                        }
                    }
                }
            }
        }

        return Constantes.NON;
    }

    /**
     * Méthode appelée par introspection. Elle est utilisée pour vérifier le
     * conditionnement sur le régime de droits de propriété intellectuelle pour
     * les CCAG-PI et CCAG-TIC
     *
     * @param id    du referentiel EpmTRefDroitProprieteIntellectuelle. Il est
     *              choisie en quant que la valeur potentiellement conditionnée
     *              quand créer une clause. Il vaut 0 quand l'option Tous est
     *              choisie.
     * @param idLot il vaut une valeur > 0 si le document est créé pour un lot
     *              spécifique
     * @return true si satisfaire les conditions
     */
    public final boolean getDroitProprieteIntellectuelle(final int id, final int idLot) {
        // le cas non allotie
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getDroitProprieteIntellectuelle() != null && (epmTBudLotOuConsultation.getDroitProprieteIntellectuelle().getId() == id || id == 0)) {
                return true;
            }
        }

        // le cas allotie
        if (epmTBudLots != null) {
            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                EpmTBudLotOuConsultation lotOuCons = lot.getEpmTBudLotOuConsultation();
                if (lotOuCons.getDroitProprieteIntellectuelle() != null && (lotOuCons.getDroitProprieteIntellectuelle().getId() == id || id == 0)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Méthode appelée par introspection. Elle est utilisée pour vérifier le
     * conditionnement : Au moins un lot du marché ou le marché est reservé à...
     *
     * @param id    du referentiel EpmTRefTypeStructureSociale.
     * @param idLot il vaut une valeur > 0 si le document est créé pour un lot
     *              spécifique
     * @return true si remplir les conditions
     */
    public final boolean getStructureSocialeReserves(final int id, final int idLot) {
        // le cas non allotie
        if (epmTBudLotOuConsultation != null) {
            if (epmTBudLotOuConsultation.getListeStructureSocialeReserves() != null) {
                for (EpmTRefTypeStructureSociale refTypeStructureSociale : epmTBudLotOuConsultation.getListeStructureSocialeReserves()) {
                    if (id == refTypeStructureSociale.getId()) {
                        return true;
                    }
                }
            }
        }

        // le cas allotie
        if (epmTBudLots != null) {
            for (EpmTBudLot lot : epmTBudLots) {
                if (idLot > 0 && lot.getId() != idLot) {
                    continue;
                }
                EpmTBudLotOuConsultation lotOuCons = lot.getEpmTBudLotOuConsultation();
                if (lotOuCons.getListeStructureSocialeReserves() != null) {
                    for (EpmTRefTypeStructureSociale refTypeStructureSociale : lotOuCons.getListeStructureSocialeReserves()) {
                        if (id == refTypeStructureSociale.getId()) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    /**
     * @return injecté par Spring.
     */
    public final EpmTConsultation getConsultation() {
        return consultation;
    }

    /**
     * @param valeur injecté par spring.
     */
    public final void setConsultation(final EpmTConsultation valeur) {
        this.consultation = valeur;
    }

    /**
     * @param valeur the consultationService to set
     */
    public final void setConsultationService(final ConsultationServiceSecurise valeur) {
        consultationService = valeur;
    }

}
