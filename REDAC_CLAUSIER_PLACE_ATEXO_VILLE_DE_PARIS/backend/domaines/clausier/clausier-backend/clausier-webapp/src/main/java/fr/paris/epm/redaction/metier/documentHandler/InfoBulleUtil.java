package fr.paris.epm.redaction.metier.documentHandler;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * la classe InfoBulleUtil effectue des conversions de type sur l'info-bulle.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class InfoBulleUtil extends AbstractDocument {
    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InfoBulleUtil.class);

    /**
     * Cette méthode permet la conversion d'une info-bulle XML vers un objet
     * info-bulle.
     * @param elementInfoBulleXML arbre JDOM réprésentant une infobulle.
     * @return Objet java Infobulle.
     * @throws TechnicalException erreur technique.
     */
    public static InfoBulle elementInfoBulleXMLVersInfobulle(final Element elementInfoBulleXML) {
        if (elementInfoBulleXML == null
                || !elementInfoBulleXML.getName().equals(BULLE_NOM)) {
            throw new TechnicalException(
                "arbre vide ou n'ayant pas une structure d'infobulle");
        }
        InfoBulle bulle = new InfoBulle();
        if (elementInfoBulleXML.getAttributes().isEmpty()
                && elementInfoBulleXML.getChildren().isEmpty()) {
            // Infobulle vide
            return bulle;
        }
        try {
            bulle.setActif(elementInfoBulleXML.getAttribute(BULLE_ACTIF)
                    .getBooleanValue());
        } catch (DataConversionException e) {
        	LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }

        Element description = (Element) getElement(elementInfoBulleXML,
                                                   BULLE_DESCRIPTION);
        bulle.setDescription(description.getText());

        Element blocLien = (Element) getElement(elementInfoBulleXML,
                                                BULLE_LIEN);
        Element url = (Element) getElement(blocLien, LIEN_URL);
        Element descLien = (Element) getElement(blocLien, LIEN_DESCRIPTION);
        bulle.setDescriptionLien(descLien.getText());
        bulle.setLien(url.getText());

        return bulle;
    }

    /**
     * Cette méthode permet la conversion d'une info-bulle vers une info-Bulle
     * XML.
     * @param infoBulle l'info-bulle à convertir.
     * @return info-Bulle XML.
     */
    public static Element versJdom(final InfoBulle infoBulle) {

        boolean infoBulleNonExistante = (infoBulle == null);

        LOG.debug("Création des éléments de l'info bulle ");
        Element rootInfoBulle = new Element(BULLE_NOM);
        if (!infoBulleNonExistante) {
            Element conteneurLien = new Element(BULLE_LIEN);
            Element lien = new Element(LIEN_URL);
            lien.setText(infoBulle.getLien());
            Element descriptionLien = new Element(LIEN_DESCRIPTION);
            descriptionLien.setText(infoBulle.getDescriptionLien());
            Element description = new Element(BULLE_DESCRIPTION);
            description.setText(infoBulle.getDescription());

            LOG.debug("Associer l'attribut actif à l'info bulle ");
            Attribute actif = new Attribute(BULLE_ACTIF,
                infoBulleNonExistante ? Boolean.toString(false) : Boolean
                        .toString(infoBulle.isActif()));
            rootInfoBulle.setAttribute(actif);

            LOG.debug("Construire l'info bulle ");
            rootInfoBulle.addContent(BULLE_POS_DESCRIPTION, description);
            rootInfoBulle.addContent(BULLE_POS_LIEN, conteneurLien);
            conteneurLien.addContent(BULLE_POS_URL, lien);
            conteneurLien.addContent(BULLE_POS_DESCRIPTION_LIEN,
                                     descriptionLien);
        }
        LOG.debug("Fin Construction de l'info bulle");
        return rootInfoBulle;
    }
}
