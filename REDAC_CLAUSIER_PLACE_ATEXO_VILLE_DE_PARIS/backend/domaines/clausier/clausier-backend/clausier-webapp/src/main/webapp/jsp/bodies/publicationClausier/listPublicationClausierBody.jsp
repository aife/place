<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="atexo"   uri="AtexoTag"  %>

<c:set var="plateformeEditeur" scope="session" value="" />
<atexo:parametrage clef="plateforme.editeur" testEgal="0">
    <c:set var="plateformeEditeur" scope="session" value="false"/>
</atexo:parametrage>
<atexo:parametrage clef="plateforme.editeur" testEgal="1">
    <c:set var="plateformeEditeur" scope="session" value="true"/>
</atexo:parametrage>
<!-- plateforme is "${plateformeEditeur}" -->

<!-- Navigation bloque -->
<%--@elvariable id="resultList" type="fr.paris.epm.global.commun.ResultList"--%>
<div class="atx-pagination">
    <div class="nbr-result pull-left">
        <h2 class="h5">
            <c:out value="${resultList.count}"/>&nbsp;<spring:message code="recherche.resultats"/>
        </h2>
    </div>
    <jsp:include page="../navigate.jsp" />
</div>

<!--Debut bloc listes des publications clausier -->
<table class="table table-striped table-hover" summary="<spring:message code="navigation.listeResultat" />">
    <thead>
    <tr class="active">
        <c:if test="${empty plateformeEditeur or plateformeEditeur}">
            <th class="select-col col-md-0 top">
                <input id="publication-select-all" type="checkbox" title="Tous" onclick="pubSelectionAll()">
            </th>
        </c:if>
        <th class="col-md-3 top">
            <spring:message code="publicationClausier.listPublicationClausier.editeur"/>
        </th>
        <th class="col-md-2 top">
            <spring:message code="publicationClausier.listPublicationClausier.gestionLocale"/>
        </th>
        <th class="col-md-2 top">
            <div>
                <spring:message code="publicationClausier.listPublicationClausier.version"/>
                <a href="sort.htm?property=version"><i class="fa fa-sort"></i></a>
            </div>
            <div>
                <spring:message code="publicationClausier.listPublicationClausier.datePublication"/>
            </div>
        </th>
        <c:if test="${empty plateformeEditeur or not plateformeEditeur}">
            <th class="col-md-2 top">
                <div><spring:message code="publicationClausier.listPublicationClausier.dateActivation"/></div>
                <div><spring:message code="publicationClausier.listPublicationClausier.dateIntegration"/></div>
            </th>
        </c:if>
        <th class="col-md-1 top text-center">
            <spring:message code="publicationClausier.listPublicationClausier.telechargement"/>
        </th>
        <c:if test="${empty plateformeEditeur or not plateformeEditeur}">
            <atexo:filtreSecurite role="ROLE_activerVersionClausier" egal="true">
                <th class="col-md-1 top text-center">
                    <spring:message code="publicationClausier.listPublicationClausier.activation"/>
                </th>
            </atexo:filtreSecurite>
            <th class="col-md-1 top text-center">
                <spring:message code="publicationClausier.listPublicationClausier.statut"/>
            </th>
        </c:if>
        <th class="col-md-1 top text-right">
            <spring:message code="publicationClausier.listPublicationClausier.commentaires"/>
        </th>
    </thead>

    <tbody>
    <c:forEach var="publication" items="${resultList.listResults}" varStatus="vs">
        <%--@elvariable id="publication" type="fr.paris.epm.redaction.presentation.bean.PublicationClausierBean"--%>
        <tr>
            <c:if test="${empty plateformeEditeur or plateformeEditeur}">
                <td id="${publication.id}">
                    <label for="publication-${publication.version}">
                        <c:choose>
                            <c:when test="${not empty publication.idClausierFile}">
                                <input type="checkbox" value="${publication.id}"
                                       name="publication-select-enabled"/>
                            </c:when>
                            <c:otherwise>
                                <input type="checkbox" value="${publication.id}"
                                       name="publication-select-disabled" disabled="disabled"/>
                            </c:otherwise>
                        </c:choose>
                    </label>
                </td>
            </c:if>
            <td>
                <c:out value="${publication.editeur}"/>
            </td>
            <td>
                <c:choose>
                    <c:when test="${publication.gestionLocal == true}">
                        <spring:message code="redaction.oui"/>
                    </c:when>
                    <c:otherwise>
                        <spring:message code="redaction.non"/>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                <div><c:out value="${publication.version}"/></div>
                <div><fmt:formatDate value="${publication.datePublication}" pattern="dd/MM/yyyy" /></div>
            </td>
            <c:if test="${empty plateformeEditeur or not plateformeEditeur}">
                <td>
                    <div>
                        <c:choose>
                            <c:when test="${not empty publication.dateActivation}">
                                <fmt:formatDate value="${publication.dateActivation}" pattern="dd/MM/yyyy" />
                            </c:when>
                            <c:otherwise>
                                <spring:message code="publicationClausier.listPublicationClausier.sansObjet"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div>
                        <c:choose>
                            <c:when test="${not empty publication.dateIntegration}">
                                <fmt:formatDate value="${publication.dateIntegration}" pattern="dd/MM/yyyy" />
                            </c:when>
                            <c:otherwise>
                                <spring:message code="publicationClausier.listPublicationClausier.sansObjet"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </td>
            </c:if>
            <td class="text-center">
                <c:choose>
                    <c:when test="${not empty publication.idClausierFile}">
                        <a href="downloadPublicationClausier.htm?idPublication=${publication.id}" class="btn bt-sm btn-primary" style="font-size: 12px;">
                            <i class="fa fa-download" data-toggle="toolltip" title="${publication.version}"></i>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <span class="fa-stack">
                            <i class="fa fa-download fa-stack-1x"></i>
                            <i class="fa fa-ban fa-stack-2x text-danger"></i>
                        </span>
                    </c:otherwise>
                </c:choose>
            </td>
            <c:if test="${empty plateformeEditeur or not plateformeEditeur}">
                <atexo:filtreSecurite role="ROLE_activerVersionClausier" egal="true">
                    <td class="text-center">
                        <c:choose>
                            <c:when test="${publication.actif}">
                                <i class="fa fa-toggle-on fa-2x text-success"></i>
                            </c:when>
                            <c:otherwise>
                                <a href="activePublicationClausier.htm?idPublication=${publication.id}" onclick="ATX.component.loader.show()">
                                    <i class="fa fa-toggle-off fa-2x text-danger"></i>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </atexo:filtreSecurite>
                <td class="text-center">
                    <c:choose>
                        <c:when test="${publication.actif}">
                            <spring:message code="publicationClausier.listPublicationClausier.active"/>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="publicationClausier.listPublicationClausier.nonActive"/>
                        </c:otherwise>
                    </c:choose>
                </td>
            </c:if>
            <td class="text-right">
                <a title="Info-bulle" class="picto-info-bulle picto-commentaire" href="#"></a>
                <span class="input-group-btn">
                        <button type="button" data-toggle="modal"
                                data-target="#modalCommentairePublicationClausier_${publication.id}"
                                class="btn btn-default btn-sm p-1 js-scroll-to-searchInstance" title="Commentaire">
                            <i class="fa fa-commenting-o fa-lg"></i>
                        </button>
                    </span>

                <jsp:include page="modalCommentairePublicationClausier.jsp">
                    <jsp:param name="id" value="${publication.id}" />
                    <jsp:param name="commentaire" value="${publication.commentaire}" />
                </jsp:include>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<!-- Fin bloc listes des des publications clausier -->

<!-- Navigation bloque -->
<div class="atx-pagination clearfix">
    <jsp:include page="../navigate.jsp" />
</div>

<script>
    function pubSelectionAll() {
        var checkboxes = new Array();
        checkboxes = document.getElementsByName("publication-select-enabled");
        var selectAllCheckBox = document.getElementById("publication-select-all");
        console.log("selectAllCheckBox.checked:" + selectAllCheckBox.checked);
        for (var i = 0; i < checkboxes.length; i++) {
            console.log("checkboxes[i].checked:" + checkboxes[i].checked);
            checkboxes[i].checked = selectAllCheckBox.checked;
        }
    }
</script>