package fr.paris.epm.redaction.presentation.actions;

import fr.paris.epm.global.presentation.AbstractAction;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.redaction.coordination.DocumentRedactionFacade;
import fr.paris.epm.redaction.coordination.ReferentielFacade;
import fr.paris.epm.redaction.presentation.mapper.DocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * Classe Abstraite document.
 * @author RAMLI Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractDocumentAction extends AbstractAction {

    /**
     * marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDocumentAction.class);

    @Resource
    protected RedactionServiceSecurise redactionService;

    @Resource
    protected ReferentielFacade referentielFacade;


}
