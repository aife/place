package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.noyau.metier.redaction.CanevasCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasPubCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.BaseEpmTRefReferentiel;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import fr.paris.epm.redaction.coordination.service.CanevasService;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.CanevasChapitre;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.commun.Derogation;
import fr.paris.epm.redaction.metier.objetValeur.commun.InfoBulle;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.mapper.CanevasMapper;
import fr.paris.epm.redaction.util.Constantes;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe implémentant les méthodes exposées par l'interface CanevasFacade.
 *
 * @author Ramli Tarik & Yeddoughmi younes
 * @version $Revision$, $Date$, $Author$
 */
public class CanevasFacadeGWTImpl implements CanevasFacadeGWT {

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CanevasFacadeGWTImpl.class);

    private CanevasMapper canevasMapper;

    private CanevasService canevasService;

    private ClauseFacadeGWT clauseFacadeGWT;

    private RedactionServiceSecurise redactionService;

    private ReferentielsServiceSecurise referentielsService;

    /**
     * Acces aux fichiers de libelle (injection Spring).
     */
    private ResourceBundleMessageSource messageSource;

    private static int compareChapitreNum(String num1, String num2) {
        if (num1.equals(num2))
            return 0;

        String[] nums1 = num1.split("\\.");
        String[] nums2 = num2.split("\\.");

        int lengthMin = Math.min(nums1.length, nums2.length);
        for (int i = 0; i < lengthMin; i++) {
            int res = Integer.valueOf(nums1[i]).compareTo(Integer.valueOf(nums2[i]));
            if (res != 0)
                return res;
        }
        return nums1.length < num2.length() ? -1 : 1;
    }

    private Chapitre epmTChapitreVersChapitre(EpmTChapitreAbstract epmTChapitreAbstract, CanevasChapitre parent,
                                              boolean isFormeEditeur, final Integer idOrganisme) {
        Chapitre chapitre = new Chapitre();
        EpmTRefOrganisme epmTRefOrganisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);

        chapitre.setIdChapitre(epmTChapitreAbstract.getIdChapitre());
        chapitre.setIdPublication(epmTChapitreAbstract.getIdPublication());
        chapitre.setTitre(epmTChapitreAbstract.getTitre());
        chapitre.setNumero(epmTChapitreAbstract.getNumero());
        chapitre.setParent(parent);

        InfoBulle infoBulle = new InfoBulle();
        if ((epmTChapitreAbstract.getInfoBulleText() == null || epmTChapitreAbstract.getInfoBulleText().isEmpty()) &&
                (epmTChapitreAbstract.getInfoBulleUrl() == null || epmTChapitreAbstract.getInfoBulleUrl().isEmpty())) {
            infoBulle.setActif(false);
        } else {
            infoBulle.setDescription(epmTChapitreAbstract.getInfoBulleText());
            infoBulle.setLien(epmTChapitreAbstract.getInfoBulleUrl());
            infoBulle.setActif(true);
        }
        chapitre.setInfoBulle(infoBulle);

        Derogation derogation = new Derogation();
        if (epmTChapitreAbstract.getDerogationActive() != null) {
            //derogation.setNumArticle(epmTChapitreAbstract.getDerogationActive().toString());//aucun sens
            derogation.setNumArticle(epmTChapitreAbstract.getNumero());
            derogation.setArticleDerogationCCAG(epmTChapitreAbstract.getDerogationArticle());
            derogation.setCommentairesDerogationCCAG(epmTChapitreAbstract.getDerogationCommentaires());
            derogation.setActive(epmTChapitreAbstract.getDerogationActive());
        }
        chapitre.setDerogation(derogation);
        chapitre.setAfficherMessageDerogation(epmTChapitreAbstract.getAfficherMessageDerogation()!= null? epmTChapitreAbstract.getAfficherMessageDerogation() : false);
        chapitre.setStyleChapitre(epmTChapitreAbstract.getStyle());

        List<Clause> clauses = new ArrayList<>(epmTChapitreAbstract.getClauses().size());
        List<Integer> orders = epmTChapitreAbstract.getClauses().keySet().stream()
                .sorted()
                .collect(Collectors.toList());

        LOG.debug("Liste des clauses du chapitre = {}", epmTChapitreAbstract.getClauses().values());

        for (Integer num : orders) {
            String reference = epmTChapitreAbstract.getClauses().get(num);

            LOG.debug("Référence de la clause = {}", reference);

            EpmTClauseAbstract epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(isFormeEditeur, null, reference, idOrganisme, false);

            if (null == epmTClause) {
                LOG.debug("La clause '{}' est nulle (elle a été désactivée/supprimée?)", reference);
            } else {
                LOG.debug("Ajout de la clause '{}' à la liste des clauses", reference);

                clauses.add(clauseFacadeGWT.epmTClauseToClause(epmTClause, null,epmTRefOrganisme.getPlateformeUuid()));
            }

        }
        chapitre.setClauses(clauses);

        List<Chapitre> sousChapitres = new ArrayList<>();
        for (EpmTChapitreAbstract sousChapitre : epmTChapitreAbstract.getEpmTSousChapitres()) {
            sousChapitres.add(epmTChapitreVersChapitre(sousChapitre, chapitre, isFormeEditeur, idOrganisme));
        }
        sousChapitres.sort((ch1, ch2) -> compareChapitreNum(ch1.getNumero(), ch2.getNumero()));
        chapitre.setChapitres(sousChapitres);

        return chapitre;
    }

    /**
     * Transforme un EpmTCanevas en Canevas pour afficher la liste des canevas.
     *
     * @param epmTCanevasAbstract EpmTCanevas provenant de la base de donnée
     * @return Canevas
     */
    @Override
    public final Canevas epmTCanevasVersCanevas(EpmTCanevasAbstract epmTCanevasAbstract, final Integer idOrganisme) {

        Canevas canevas = canevasMapper.toCanevas(epmTCanevasAbstract);
        canevas.setIdCanevas(epmTCanevasAbstract.getIdCanevas());
        canevas.setIdPublication(epmTCanevasAbstract.getIdPublication());

        if (epmTCanevasAbstract.getEpmTChapitres() != null) {
            List<Chapitre> chapitres = new ArrayList<>();
            for (EpmTChapitreAbstract chapitre : epmTCanevasAbstract.getEpmTChapitres())
                //ne traiter que les chapitres de niveau 1
                if(chapitre.getEpmTParentChapitre() == null) {
                    chapitres.add(epmTChapitreVersChapitre(chapitre, canevas, epmTCanevasAbstract instanceof EpmTCanevas, idOrganisme));
                }
            chapitres.sort((ch1, ch2) -> compareChapitreNum(ch1.getNumero(), ch2.getNumero()));
            canevas.setChapitres(chapitres);
        }

        canevas.setTypeDocument(epmTCanevasAbstract.getEpmTRefTypeDocument().getId());
        canevas.setDerogationActive(epmTCanevasAbstract.getEpmTRefTypeDocument().isActiverDerogation());

        StringBuilder message = new StringBuilder();
        verifiePresenceClause(canevas.getChapitres(), epmTCanevasAbstract.getDateModification(), epmTCanevasAbstract instanceof EpmTCanevas, message, idOrganisme);
        if (!message.toString().equals(""))
            canevas.setMessage(messageSource.getMessage("erreur.clauseSupprimee", null, Locale.FRENCH) + " " + message.toString());

        Set<EpmTRefProcedure> epmTRefProcedures = epmTCanevasAbstract.getEpmTRefProcedures();
        List<Integer> procedurePassationList = null;
        if (!CollectionUtils.isEmpty(epmTRefProcedures)) {
            procedurePassationList = epmTRefProcedures.stream().map(BaseEpmTRefReferentiel::getId).collect(Collectors.toList());
        }
        canevas.setProcedurePassationList(procedurePassationList);

        List<Integer> typeContratList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(epmTCanevasAbstract.getEpmTRefTypeContrats())) {
            typeContratList = epmTCanevasAbstract.getEpmTRefTypeContrats().stream().map(EpmTRefTypeContrat::getId).collect(Collectors.toList());
        }
        canevas.setTypeContratList(typeContratList);

        SimpleDateFormat sdf = new SimpleDateFormat(ConstantesGlobales.DATE);

        if (epmTCanevasAbstract.getDateCreation() != null)
            canevas.setCreeLe(sdf.format(epmTCanevasAbstract.getDateCreation()));
        else
            canevas.setCreeLe("ND");

        if (epmTCanevasAbstract.getDateModification() != null)
            canevas.setModifieLe(sdf.format(epmTCanevasAbstract.getDateModification()));
        else
            canevas.setModifieLe("ND");

        if (epmTCanevasAbstract.getIdPublication() != null) {
            EpmTPublicationClausier publication = redactionService.chercherObject(epmTCanevasAbstract.getIdPublication(), EpmTPublicationClausier.class);
            canevas.setLastVersion(publication.getVersion());
        } else if (epmTCanevasAbstract.getIdLastPublication() != null) {
            EpmTPublicationClausier publication = redactionService.chercherObject(epmTCanevasAbstract.getIdLastPublication(), EpmTPublicationClausier.class);
            canevas.setLastVersion(publication.getVersion());
        } else {
            canevas.setLastVersion("NA");
        }
        canevas.setIdOrganisme(epmTCanevasAbstract.getIdOrganisme());

        return canevas;
    }

    /**
     * Cette méthode supprime les clauses qui ont été supprimées.
     *
     * @param chapitres liste des chapitres du canevas
     */
    private void verifiePresenceClause(final List<Chapitre> chapitres, final Date dateModificationCanevas,
                                       final Boolean isFormeEditeur, final StringBuilder message, final Integer idOrganisme) {

        if (chapitres == null || chapitres.isEmpty())
            return;

        for (Chapitre chapitre : chapitres) {
            verifiePresenceClause(chapitre.getChapitres(), dateModificationCanevas, isFormeEditeur, message, idOrganisme);

            if (chapitre.getClauses() == null || chapitre.getClauses().isEmpty())
                continue;

            List<Clause> clauses = Collections.synchronizedList(chapitre.getClauses());
            for (Iterator<Clause> iterClauses = clauses.iterator(); iterClauses.hasNext(); ) {
                Clause clause = iterClauses.next();

                EpmTClauseAbstract epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(isFormeEditeur, null, clause.getReference(), idOrganisme);

                EpmTClauseAbstract epmTClausePub = null;
                if (clause.getIdPublication() != null)
                    epmTClausePub = clauseFacadeGWT.chargerDernierClauseByRef(isFormeEditeur, clause.getIdPublication(), clause.getReference(), idOrganisme);

                if (epmTClause == null && epmTClausePub == null)
                    throw new TechnicalException("Clause ref = " + clause.getReference() + " n'est pas trouvée nulle part");

                if (epmTClause == null && epmTClausePub != null || // supprimée entre deux publications
                        epmTClause.getEtat().equals(EpmTClause.ETAT_SUPPRIMER)) { // sois recherche Interministeriele ou clause Ministeriele
                    if (message.length() > 0)
                        message.append(", ");
                    message.append(epmTClause == null ? epmTClausePub.getReference() : epmTClause.getReference());
                    iterClauses.remove();
                } else if (epmTClausePub != null && !epmTClausePub.getIdPublication().equals(epmTClause.getIdPublication()) ||
                        dateModificationCanevas != null && epmTClause.getDateModification().after(dateModificationCanevas)) {
                    if (epmTClause.isActif() != clause.isActif()) {
                        clause.setActif(epmTClause.isActif());
                        if (message.length() > 0)
                            message.append(", ");
                        message.append(epmTClause.getReference());
                    }
                    clause.setEtat(Clause.ETAT_MODIFIER);

                    if (clause.getInfoBulle() == null)
                        clause.setInfoBulle(new InfoBulle());

                    InfoBulle infoBulle = clause.getInfoBulle();
                    infoBulle.setActif(epmTClause.getInfoBulleText() != null && !epmTClause.getInfoBulleText().isEmpty() ||
                            epmTClause.getInfoBulleUrl() != null && epmTClause.getInfoBulleUrl().isEmpty());
                    infoBulle.setDescription(epmTClause.getInfoBulleText());
                    infoBulle.setLien(epmTClause.getInfoBulleUrl());
                    infoBulle.setDescriptionLien(epmTClause.getInfoBulleUrl());

                    clause.setPotentiellementApplicable(epmTClause.getEpmTClausePotentiellementConditionnees() != null &&
                            !epmTClause.getEpmTClausePotentiellementConditionnees().isEmpty());
                }
            }
        }
    }

    private EpmTChapitre chapitreVersEpmTChapitre(Chapitre chapitre, EpmTCanevas parentCanevas, EpmTChapitre parentChapitre) {

        EpmTChapitre epmTChapitre = null;

        if (chapitre.getIdChapitre() == 0)
            epmTChapitre = new EpmTChapitre();
        else {
            if (parentCanevas != null && parentCanevas.getEpmTChapitres() != null)
                epmTChapitre = parentCanevas.getEpmTChapitres().stream()
                        .filter(ch -> chapitre.getIdChapitre() == ch.getId())
                        .findFirst()
                        .orElse(null);
            else if (parentChapitre != null && parentChapitre.getEpmTSousChapitres() != null)
                epmTChapitre = parentChapitre.getEpmTSousChapitres().stream()
                        .filter(ch -> chapitre.getIdChapitre() == ch.getId())
                        .findFirst()
                        .orElse(null);

            if (epmTChapitre == null) // chapitre a été deplacé -> recherche dans la base
                epmTChapitre = redactionService.chercherObject(chapitre.getIdChapitre(), EpmTChapitre.class);
            if (epmTChapitre == null) // chapitre a été supprimé
                epmTChapitre = new EpmTChapitre();
        }

        epmTChapitre.setId(chapitre.getIdChapitre());
        epmTChapitre.setTitre(chapitre.getTitre());
        epmTChapitre.setNumero(chapitre.getNumero());
        epmTChapitre.setEpmTCanevas(parentCanevas);
        epmTChapitre.setEpmTParentChapitre(parentChapitre);

        if (chapitre.getInfoBulle() != null && chapitre.getInfoBulle().isActif()) {
            epmTChapitre.setInfoBulleText(chapitre.getInfoBulle().getDescription());
            epmTChapitre.setInfoBulleUrl(chapitre.getInfoBulle().getLien());
        } else {
            epmTChapitre.setInfoBulleText(null);
            epmTChapitre.setInfoBulleUrl(null);
        }
        epmTChapitre.setStyle(chapitre.getStyleChapitre());

        if (chapitre.getDerogation() != null) {
            epmTChapitre.setDerogationArticle(chapitre.getDerogation().getArticleDerogationCCAG());
            epmTChapitre.setDerogationCommentaires(chapitre.getDerogation().getCommentairesDerogationCCAG());
            epmTChapitre.setDerogationActive(chapitre.getDerogation().isActive());
        } else {
            epmTChapitre.setDerogationActive(false);
        }
        epmTChapitre.setAfficherMessageDerogation(chapitre.isAfficherMessageDerogation());

        Map<Integer, String> clauses = new HashMap<>();
        int order = 1;
        for (Clause clause : chapitre.getClauses())
            clauses.put(order++, clause.getReference());
        epmTChapitre.setClauses(clauses);

        List<EpmTChapitre> sousChapitres = new ArrayList<>();
        for (Chapitre sousChapitre : chapitre.getChapitres())
            sousChapitres.add(chapitreVersEpmTChapitre(sousChapitre, parentCanevas, epmTChapitre));

        epmTChapitre.setEpmTSousChapitres(sousChapitres);
        return epmTChapitre;
    }

    /**
     * Méthode est utilisée pour créer ou modifier des canevas => convert qu'en EpmTCanevas (jamais EpmTCanevasPub, ni EpmVCanevas)
     */
    public final EpmTCanevas canevasVersEpmTCanevas(final Canevas canevas, Integer idStatutRedactionClausier) {

        if (canevas == null)
            throw new TechnicalException("canevas sans identifiant ou nul");
        if (canevas.getIdPublication() != null)
            throw new TechnicalException("une tentative de modifier un canevas publié");

        EpmTCanevas epmTCanevas;
        List<EpmTChapitre> canevasBDDChapitres = new ArrayList<>();
        Integer idOrganisme = canevas.getIdOrganisme();
        if (canevas.getIdCanevas() != 0) {
            EpmTRefOrganisme epmTRefOrganisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
            CanevasCritere canevasCritere = new CanevasCritere(epmTRefOrganisme.getPlateformeUuid());
            canevasCritere.setIdOrganisme(idOrganisme);
            canevasCritere.setIdCanevas(canevas.getIdCanevas());
            epmTCanevas = redactionService.chercherUniqueEpmTObject(0, canevasCritere);
            canevasBDDChapitres = epmTCanevas.getEpmTChapitres();
            //supprimer les sous chapitres de la liste de chapitres
            epmTCanevas.setEpmTChapitres(epmTCanevas.getEpmTChapitres().stream().filter(ch -> ch.getEpmTParentChapitre() == null).collect(Collectors.toList()));
        } else {
            epmTCanevas = new EpmTCanevas();
        }
        epmTCanevas.setIdOrganisme(idOrganisme);

        if (idStatutRedactionClausier == null)
            if (epmTCanevas.getEpmTRefStatutRedactionClausier() != null)
                idStatutRedactionClausier = epmTCanevas.getEpmTRefStatutRedactionClausier().getId();

        if (epmTCanevas.getReference() == null)
            canevasMapper.toEpmTCanevas(canevas, epmTCanevas);

        epmTCanevas.setActif(canevas.getStatut());
        if (!epmTCanevas.getTitre().equals(canevas.getTitre()))
            epmTCanevas.setTitre(canevas.getTitre());

        if (idStatutRedactionClausier != null) {
            EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier = redactionService.chercherObject(idStatutRedactionClausier, EpmTRefStatutRedactionClausier.class);
            epmTCanevas.setEpmTRefStatutRedactionClausier(epmTRefStatutRedactionClausier);
        }

        try {
            EpmTRefTypeDocument typeDocument = redactionService.chercherObject(canevas.getTypeDocument(), EpmTRefTypeDocument.class);
            epmTCanevas.setEpmTRefTypeDocument(typeDocument);
        } catch (NonTrouveException e) {
            throw new TechnicalException("canevas sans identifiant ou nul");
        }

        //Mise à jour des chapitres
        List<EpmTChapitre> epmTChapitres = new ArrayList<>();
        for (Chapitre chapitre : canevas.getChapitres()) {
            epmTChapitres.add(chapitreVersEpmTChapitre(chapitre, epmTCanevas, null));
        }

        //récupération de tous les ids des chapitres et sous chapitres de epmtChapitres
        List<Integer> idsEpmTChapitres = new ArrayList<>();
        for (EpmTChapitre chapitre : epmTChapitres) {
            idsEpmTChapitres.addAll(recupererIds(chapitre));
        }

        epmTCanevas.setEpmTChapitres(epmTChapitres);

        Set<EpmTRefProcedure> epmTRefProcedures = epmTCanevas.getEpmTRefProcedures();
        if (epmTRefProcedures == null)
            epmTRefProcedures = new HashSet<>();
        else
            epmTRefProcedures.clear();

        List<Integer> procedurePassationList = canevas.getProcedurePassationList();
        if (!CollectionUtils.isEmpty(procedurePassationList)) {
            epmTRefProcedures = referentielsService.getAllReferentiels().getRefProcedure()
                    .stream().filter(epmTRefProcedure -> procedurePassationList.contains(epmTRefProcedure.getId()))
                    .collect(Collectors.toSet());
        }

        epmTCanevas.setEpmTRefProcedures(epmTRefProcedures);

        if (!CollectionUtils.isEmpty(canevas.getTypeContratList()))
            epmTCanevas.setEpmTRefTypeContrats(canevas.getTypeContratList()
                    .stream()
                    .map(idTypeContrat -> redactionService.chercherObject(idTypeContrat, EpmTRefTypeContrat.class))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet()));

        if (!canevasBDDChapitres.isEmpty()) {
            //on doit supprimer tous les chapitres de canevasBDDChapitres dont l'id n'est pas dans idsEpmTChapitres

            List<EpmTChapitre> chapitresASupprimer = new ArrayList<>();
            for (EpmTChapitre chapitre : canevasBDDChapitres) {
                if (!idsEpmTChapitres.contains(chapitre.getId())) {
                    chapitresASupprimer.add(chapitre);
                }
            }

            //Enlever les données qui empêchent la suppression (relation chapitre-canevas + chapitre parent-enfant)
            List<EpmTChapitre> chapitresPretsASupprimer = new ArrayList<>();
            for (EpmTChapitre chapitre : chapitresASupprimer) {
                chapitre.setEpmTCanevas(null);
                chapitre.setEpmTParentChapitre(null);
                chapitre.setEpmTSousChapitres(null);
                EpmTChapitre chapitreModifie = redactionService.modifierEpmTObject(chapitre);
                chapitresPretsASupprimer.add(chapitreModifie);
            }

            //suppression des chapitres
            for (EpmTChapitre chapitre : chapitresPretsASupprimer) {
                EpmTChapitre chapitreASupprimer = redactionService.chercherObject(chapitre.getId(), EpmTChapitre.class);
                redactionService.supprimerEpmTObject(0, chapitreASupprimer);
            }
        }
        return epmTCanevas;
    }



    private Collection<Integer> recupererIds(EpmTChapitre chapitre) {
        List<Integer> idsEpmTChapitres = new ArrayList<>();
        idsEpmTChapitres.add(chapitre.getId());
        for (EpmTChapitre sousChapitre : chapitre.getEpmTSousChapitres()) {
            idsEpmTChapitres.addAll(recupererIds(sousChapitre));
        }
        return idsEpmTChapitres;
    }

    public EpmTCanevas canevasVersEpmTCanevas(Canevas canevasXML) {
        return canevasVersEpmTCanevas(canevasXML, null);
    }

    private static List<Integer> getListIdsClause(Chapitre chapitre) {
        List<Integer> ids = new ArrayList<Integer>();

        if (chapitre.getChapitres() != null && chapitre.getChapitres().size() > 0)
            chapitre.getChapitres().forEach(sousChapitre -> ids.addAll(getListIdsClause(sousChapitre)));

        if (chapitre.getClauses() != null && chapitre.getClauses().size() > 0)
            chapitre.getClauses().forEach(clause -> ids.add(clause.getIdClause()));

        return ids;
    }

    /**
     * Permet d'ajouter un objet dans la base de donnée.
     *
     * @param canevas l'objet à ajouter
     * @return EpmTCanevas l'objet ajouté
     */
    public final Canevas ajouter(final Canevas canevas, final Integer idOrganisme) {
        EpmTCanevas epmTCanevas = canevasVersEpmTCanevas(canevas);

        Date date = new Date(System.currentTimeMillis());
        epmTCanevas.setDateCreation(date);
        epmTCanevas.setDateModification(date);
        epmTCanevas = miseAJourDateValidation(epmTCanevas);

        String reference = canevasService.getReferenceCanevasSuivante(epmTCanevas.isCanevasEditeur(), epmTCanevas.getIdOrganisme());
        epmTCanevas.setReference(reference);

        epmTCanevas = redactionService.modifierEpmTObject(epmTCanevas);

        return epmTCanevasVersCanevas(epmTCanevas, idOrganisme);
    }

    /**
     * Cette méthode permet de modifier un objet Canevas.
     *
     * @param canevas Canevas
     * @return Canevas
     */
    public final Canevas modifierCanevas(final Canevas canevas, final Integer idOrganisme) {
        Integer idStatus = canevas.getIdStatutRedactionClausier();
        String action;
        switch (idStatus) {
            case 2:
                action = Constantes.DEMANDER_VALIDATION_CLAUSE;
                break;
            case 3:
                action = Constantes.VALIDER_CLAUSE;
                break;
            default:
                action = Constantes.REFUSER_CLAUSE;
        }
        return modifierCanevas(canevas, action, idOrganisme);
    }

    /**
     * Cette méthode permet de modifier un objet Canevas.
     *
     * @param canevas Canevas
     * @return Canevas
     */

    public final Canevas modifierCanevas(Canevas canevas, final String action, final Integer idOrganisme) {
        try {
            Integer idStatutRedactionClausier;
            switch (action) {
                case Constantes.REFUSER_CLAUSE:
                    idStatutRedactionClausier = EpmTRefStatutRedactionClausier.BROUILLON;
                    break;
                case Constantes.DEMANDER_VALIDATION_CLAUSE:
                    idStatutRedactionClausier = EpmTRefStatutRedactionClausier.A_VALIDER;
                    break;
                case Constantes.VALIDER_CLAUSE:
                    idStatutRedactionClausier = EpmTRefStatutRedactionClausier.VALIDEE;
                    break;
                default:
                    idStatutRedactionClausier = canevas.getIdStatutRedactionClausier();
            }
            //Mise à jour de leur numéro pour les chapitres et sous-chapitres existants
            for (Chapitre chapitre : canevas.getChapitres()) {
                updateNumeroChapitre(chapitre);
            }
            EpmTCanevas canevasBdd = canevasVersEpmTCanevas(canevas, idStatutRedactionClausier);
            canevasBdd.setEtat("1");
            canevasBdd.setIdNaturePrestation(canevas.getNaturePrestation());
            canevasBdd.setCompatibleEntiteAdjudicatrice(canevas.isCompatibleEntiteAdjudicatrice());
            canevasBdd.setIdRefCCAG(canevas.getIdRefCCAG());
            canevasBdd.setDateModification(new Date());
            if (action != null)
                canevasBdd = miseAJourDateValidation(canevasBdd);
            canevasBdd = redactionService.modifierEpmTObject(canevasBdd);
            return epmTCanevasVersCanevas(canevasBdd, idOrganisme);
        } catch (NonTrouveException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    private void updateNumeroChapitre(Chapitre chapitre) {
        if (chapitre.getIdChapitre() != 0) {
            EpmTChapitre chapitreExistant = redactionService.chercherObject(chapitre.getIdChapitre(), EpmTChapitre.class);
            chapitreExistant.setNumero(chapitre.getNumero());
            redactionService.modifierEpmTObject(chapitreExistant);
            for (Chapitre sousChapitre : chapitre.getChapitres()) {
                updateNumeroChapitre(sousChapitre);
            }
        }
    }

    /**
     * Dans le cas d'un écran éditeur on recherche la clause dans la table
     * epmtcanevas pour récupérer la version la plus à jour, dans le cas d'un
     * écran client, si le canevas est un canevas éditeur on récupére la dernière
     * version publiée du canevas (table d'historique) si non on recherche la
     * dernière version en cours du canevas.
     *
     * @param idCanevas    identifiant du canevas
     * @param ecranEditeur booleean indiquant si l'on est dans le cas d'un écran administration éditeur (true) ou d'un écran client (false)
     * @return le canevas recherché.
     */
    public final Canevas charger(final int idCanevas, final Integer idPublication, final boolean ecranEditeur, final Integer idOrganisme) {
        if (ecranEditeur || idPublication == null) {
            EpmTCanevas epmTCanevas = redactionService.chercherObject(idCanevas, EpmTCanevas.class);
            return epmTCanevasVersCanevas(epmTCanevas, idOrganisme);
        } else {
            EpmTRefOrganisme epmTRefOrganisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
            CanevasViewCritere canevasViewCritere = new CanevasViewCritere(epmTRefOrganisme.getPlateformeUuid());
            canevasViewCritere.setIdCanevas(idCanevas);
            if (idPublication == null)
                canevasViewCritere.setHasPublication(false);
            else
                canevasViewCritere.setIdPublication(idPublication);
            EpmVCanevas epmVCanevas = redactionService.chercherUniqueEpmTObject(0, canevasViewCritere);

            return epmTCanevasVersCanevas(epmVCanevas.getEpmTCanevasPub(), idOrganisme);
        }
    }

    @Override
    public Canevas getCanevasPourCanevasService(Canevas canevas, int idCanevas, Integer idPublication, final Integer idOrganisme, final boolean isFormeEditeur) {

        if (idCanevas != 0) {
            EpmTCanevasAbstract epmTCanevas;
            if (idPublication == null) {
                epmTCanevas = redactionService.chercherObject(idCanevas, EpmTCanevas.class);
            } else {
                EpmTRefOrganisme epmTRefOrganisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
                CanevasPubCritere canevasPubCritere = new CanevasPubCritere(epmTRefOrganisme.getPlateformeUuid());
                canevasPubCritere.setIdCanevas(idCanevas);
                canevasPubCritere.setIdPublication(idPublication);
                epmTCanevas = redactionService.chercherUniqueEpmTObject(0, canevasPubCritere);
            }
            canevas = epmTCanevasVersCanevas(epmTCanevas, idOrganisme);
        }

        if (canevas != null && !canevas.getChapitres().isEmpty()) {
            LOG.debug("ref Canevas : " + canevas.getReferenceCanevas());
            LOG.debug("size Canevas.chapitre : " + canevas.getChapitres().size());
            try {
                parcoursChapitre(canevas.getChapitres(), canevas.getIdOrganisme(), isFormeEditeur);
            } catch (TechnicalException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
                return null;
            }
        }
        return canevas;
    }

    /**
     * Parcours l'ensemble des chapitres et modifie le contenu de la clause.
     *
     * @param chapitres liste des {@link Chapitre}
     */
    private void parcoursChapitre(List<Chapitre> chapitres, Integer idOrganisme, boolean isFormeEditeur) {

        for (Chapitre chapitre : chapitres) {
            if (chapitre.getClauses() != null && !chapitre.getClauses().isEmpty()) {
                LOG.debug("titre chapitre : {}", chapitre.getTitre());
                LOG.debug("size chapitre.clasue : {}", chapitre.getClauses().size());

                List<Clause> listeClause = chapitre.getClauses();
                for (Clause clause : listeClause) {
                    EpmTClauseAbstract epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(isFormeEditeur, clause.getIdPublication(), clause.getReference(), idOrganisme);
                    clause.setContenu(clauseFacadeGWT.getContenuClause(epmTClause));
                    LOG.debug("Contenu clause : {}", clause.getContenu());
                }

            }
            if (chapitre.getChapitres() != null)
                parcoursChapitre(chapitre.getChapitres(), idOrganisme, isFormeEditeur);
        }
    }

    private EpmTCanevas miseAJourDateValidation(EpmTCanevas epmTCanevas) {

        if (epmTCanevas.getEpmTRefStatutRedactionClausier().getId() == EpmTRefStatutRedactionClausier.VALIDEE) {
            //si 1ere validation
            if (epmTCanevas.getDatePremiereValidation() == null)
                epmTCanevas.setDatePremiereValidation(new Date());
            epmTCanevas.setDateDerniereValidation(new Date());
        }
        return epmTCanevas;
    }

    /**
     * Change le statut d'un canevas depuis la page d'édition de canevas en GWT
     */
    public Canevas changerStatutCanevas(final Canevas canevas, final String action, final Integer idOrganisme) {
        try {
            EpmTRefStatutRedactionClausier epmTRefStatutRedactionClausier = null;
            switch (action) {
                case Constantes.REFUSER_CLAUSE:
                    epmTRefStatutRedactionClausier = redactionService.chercherObject(EpmTRefStatutRedactionClausier.BROUILLON, EpmTRefStatutRedactionClausier.class);
                    break;
                case Constantes.DEMANDER_VALIDATION_CLAUSE:
                    epmTRefStatutRedactionClausier = redactionService.chercherObject(EpmTRefStatutRedactionClausier.A_VALIDER, EpmTRefStatutRedactionClausier.class);
                    break;
                case Constantes.VALIDER_CLAUSE:
                    epmTRefStatutRedactionClausier = redactionService.chercherObject(EpmTRefStatutRedactionClausier.VALIDEE, EpmTRefStatutRedactionClausier.class);
                    break;
            }

            EpmTCanevas epmTCanevas = canevasVersEpmTCanevas(canevas);
            epmTCanevas.setEpmTRefStatutRedactionClausier(epmTRefStatutRedactionClausier);
            epmTCanevas = miseAJourDateValidation(epmTCanevas);
            return epmTCanevasVersCanevas(epmTCanevas, idOrganisme);
        } catch (NonTrouveException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    /**
     * Recherche un canevas à partir de son identifiant et de sa version de publication
     * du clausier.
     *
     * @param idCanevas identifiant du canevas
     * @return le canevas ou null si pas de résultat.
     */
    public Canevas chargerParIdPublicationClausier(final int idCanevas, final int idPublicationClausier, final Integer idOrganisme) {

        EpmTRefOrganisme epmTRefOrganisme = redactionService.chercherObject(idOrganisme, EpmTRefOrganisme.class);
        CanevasPubCritere canevasPubCritere = new CanevasPubCritere(epmTRefOrganisme.getPlateformeUuid());
        canevasPubCritere.setIdCanevas(idCanevas);
        canevasPubCritere.setIdPublication(idPublicationClausier);
        EpmTCanevasPub epmTCanevasPub = redactionService.chercherUniqueEpmTObject(0, canevasPubCritere);

        if (epmTCanevasPub != null)
            return epmTCanevasVersCanevas(epmTCanevasPub, idOrganisme);
        return null;
    }

    @Override
    public void changerStatutCanevas(final Integer idOrganisme, String action, int... idsCanevas) {
        if (idsCanevas != null) {
            for (int idCanevas : idsCanevas) {
                EpmTCanevas epmTCanevas = redactionService.chercherObject(idCanevas, EpmTCanevas.class);
                Canevas canevas = epmTCanevasVersCanevas(epmTCanevas, idOrganisme);
                modifierCanevas(canevas, action, idOrganisme);
            }
        }
    }

    /**
     * Permet d'effectuer une recherche par critères.
     *
     * @param critere objet rassemblant l'ensemble des critères de recherche
     * @return List Liste d'objets répondant aux critères de recherche
     */
    @Deprecated
    @Override
    public final List<CanevasBean> chercher(final int idUtilisateur, final CanevasCritere critere) {

        List<EpmTCanevasAbstract> listeCanevas = redactionService.chercherEpmTObject(0, critere);

        List listeResult = new ArrayList<>();
        if (critere.isChercherNombreResultatTotal()) {
            listeResult.add(listeCanevas.get(0));
            listeCanevas.remove(0);
        }

        for (EpmTCanevasAbstract epmTCanevas : listeCanevas) {
            CanevasBean canevasBean = canevasMapper.toCanevasBean(epmTCanevas);
            String lastVersion = redactionService.chercherLastVersion(epmTCanevas.getIdLastPublication());
            canevasBean.setLastVersion(lastVersion);
            canevasBean.setTypeAuteur(epmTCanevas.isCanevasEditeur() ? 2 : 1);
            listeResult.add(canevasBean);
        }
        return listeResult;
    }

    @Deprecated
    @Override
    public final List<CanevasBean> chercher(final int idUtilisateur, final CanevasViewCritere critere) {
        return chercher(idUtilisateur, (CanevasCritere) critere);
    }

    public final void setCanevasMapper(CanevasMapper canevasMapper) {
        this.canevasMapper = canevasMapper;
    }

    public final void setClauseFacadeGWT(final ClauseFacadeGWT valeur) {
        this.clauseFacadeGWT = valeur;
    }

    public void setCanevasService(CanevasService canevasService) {
        this.canevasService = canevasService;
    }

    /**
     * @param valeur Acces aux fichiers de libelle (injection Spring).
     */
    public final void setMessageSource(final ResourceBundleMessageSource valeur) {
        this.messageSource = valeur;
    }


    public void setRedactionService(RedactionServiceSecurise redactionService) {
        this.redactionService = redactionService;
    }

    public void setReferentielsService(ReferentielsServiceSecurise referentielsService) {
        this.referentielsService = referentielsService;
    }

}
