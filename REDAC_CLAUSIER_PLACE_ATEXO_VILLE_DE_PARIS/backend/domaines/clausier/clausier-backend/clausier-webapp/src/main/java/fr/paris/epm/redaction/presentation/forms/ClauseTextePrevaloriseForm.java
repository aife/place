package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.redaction.commun.Constante;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;

/**
 * Le formulaire ClauseTextePrevalorise.
 *
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */
public class ClauseTextePrevaloriseForm {
    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Attribut texte Fixe Avant.
     */
    private Integer tailleChamps;
    private String textFixeAvant;
    /**
     * Attribut taille du Champ (long, court, moyen) .
     */
    private String nbCaract;
    /**
     * Attribut precochee.
     */
    private boolean precochee;
    /**
     * Attribut texte Fixe Apres.
     */
    private String textFixeApres;
    /**
     * Attribut valeur Defaut (champ de saisie long ou cas ou la valeur du
     * taille champ est : long) .
     */
    private String defaultValue;
    /**
     * Attribut valeur Defaut Moyen (champ de saisie moyen ou cas ou la valeur
     * du taille champ est : moyen).
     */
    private String valeurDefautMoyen;
    /**
     * Attribut valeur Defaut Court (champ de saisie court ou cas ou la valeur
     * du taille champ est : court).
     */
    private String valeurDefautCourt;

    private String valeurDefautTresLong;
    /**
     * Attribut parametrable Direction.
     */
    private String parametrableDirection;
    /**
     * Attribut parametrable Agent.
     */
    private String parametrableAgent;
    /**
     * Attribut saut Texte Fixe Avant.
     */
    private String sautTextFixeAvant;
    /**
     * Attribut saut Texte Fixe Apres.
     */
    private String sautTextFixeApres;

    /**
     * Clause CLAUSE_EDITEUR ou CLAUSE_EDITEUR_SURCHARGE
     */
    private String clauseSelectionnee = Constante.CLAUSE_EDITEUR;

    /**
     * @return saut Texte Fixe Avant
     */
    public final String getSautTextFixeAvant() {
        return sautTextFixeAvant;
    }

    /**
     * @param valeur positionne l'attribut sautTextFixeAvant
     */
    public final void setSautTextFixeAvant(final String valeur) {
        sautTextFixeAvant = valeur;
    }

    /**
     * @return saut Texte  Fixe Apres
     */
    public final String getSautTextFixeApres() {
        return sautTextFixeApres;
    }

    /**
     * @param valeur positionne l'attribut sautTextFixeApres
     */
    public final void setSautTextFixeApres(final String valeur) {
        sautTextFixeApres = valeur;
    }

    /**
     * Constructeur de la classe ClauseTextePrevaloriseForm.
     */
    public ClauseTextePrevaloriseForm() {
        reset();
    }

    public Integer getTailleChamps() {
        return tailleChamps;
    }

    public void setTailleChamps(Integer tailleChamps) {
        this.tailleChamps = tailleChamps;
    }

    /**
     * Cette méthode permet d'initialiser le formulaire ClauseTextePrevaloriseForm.
     */
    public void reset() {
        parametrableDirection = "0";
        parametrableAgent = "0";
        precochee = false;
        textFixeAvant = "";
        nbCaract = "1";
        textFixeApres = "";
        defaultValue = "";
        valeurDefautCourt = "";
        valeurDefautMoyen = "";
        valeurDefautTresLong = "";
        sautTextFixeApres = null;
        sautTextFixeAvant = null;
    }

    /**
     * @return Valeur par default
     */
    public final String getDefaultValue() {
        return defaultValue;
    }

    public boolean isPrecochee() {
        return precochee;
    }

    public void setPrecochee(boolean precochee) {
        this.precochee = precochee;
    }

    /**
     * @param valeur positionne la valeur par defaut.
     */
    public final void setDefaultValue(final String valeur) {
        defaultValue = valeur;
    }

    /**
     * @return parametrable Direction
     */
    public final String getParametrableDirection() {
        return parametrableDirection;
    }

    /**
     * @param valeur positionne l'attribut parametrableDirection.
     */
    public final void setParametrableDirection(final String valeur) {
        parametrableDirection = valeur;
    }

    /**
     * @return parametrable Agent
     */
    public final String getParametrableAgent() {
        return parametrableAgent;
    }

    /**
     * @param valeur initialise l'attribut parametrableAgent.
     */
    public final void setParametrableAgent(final String valeur) {
        parametrableAgent = valeur;
    }

    /**
     * @return texte Fixe Avant
     */
    public final String getTextFixeAvant() {
        return textFixeAvant;
    }

    /**
     * @param valeur positionne l'attribut textFixeAvant.
     */
    public final void setTextFixeAvant(final String valeur) {
        textFixeAvant = valeur;
    }

    /**
     * @return taille champ
     */
    public final String getNbCaract() {
        return nbCaract;
    }

    /**
     * @param valeur inititialise la taille du champ.
     */
    public final void setNbCaract(final String valeur) {
        nbCaract = valeur;
    }



    /**
     * @return texte Fixe Apres
     */
    public final String getTextFixeApres() {
        return textFixeApres;
    }

    /**
     * @param valeur positionne l'attribut textFixeApres.
     */
    public final void setTextFixeApres(final String valeur) {
        textFixeApres = valeur;
    }

    /* methode validate () pour valider le formulaire.
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(final ActionMapping mapping,
                                 final HttpServletRequest request) {

        ActionErrors erreurs = new ActionErrors();
        String valeurParDefaut = "";
        if (getNbCaract().equals(Constante.TAILLE_CHAMP_COURT)) {
            valeurParDefaut = getValeurDefautCourt();
        } else if (getNbCaract().equals(Constante.TAILLE_CHAMP_MOYEN)) {
            valeurParDefaut = getValeurDefautMoyen();
        } else if (getNbCaract().equals(Constante.TAILLE_CHAMP_LONG)) {
            valeurParDefaut = getDefaultValue();
        } else if (getNbCaract().equals(Constante.TAILLE_CHAMP_TRES_LONG)) {
            valeurParDefaut = getValeurDefautTresLong();
        }
        if (valeurParDefaut.trim().equals("")) {
            erreurs.add("defaultValue", new ActionMessage(
                    "redaction.msg.global.erreur"));
            erreurs.add("valeurDefautMoyen", new ActionMessage(
                    "redaction.msg.global.erreur"));
            erreurs.add("valeurDefautCourt", new ActionMessage(
                    "redaction.msg.global.erreur"));
        }
        if (erreurs.isEmpty()) {
            request.setAttribute("previsualisation", "true");
        }
        return erreurs;
    }

    /**
     * @return valeur par Defaut est : Court
     */
    public final String getValeurDefautCourt() {
        return valeurDefautCourt;
    }

    /**
     * @param valeur positionne la valeur par defaut du champ de saisie court.
     */
    public final void setValeurDefautCourt(final String valeur) {
        valeurDefautCourt = valeur;
    }

    /**
     * @return valeur par Defaut est : Moyen
     */
    public final String getValeurDefautMoyen() {
        return valeurDefautMoyen;
    }

    /**
     * @param valeur positionne la valeur par defaut du champ de saisie moyen.
     */
    public final void setValeurDefautMoyen(final String valeur) {
        valeurDefautMoyen = valeur;
    }

    public final String getValeurDefautTresLong() {
        return valeurDefautTresLong;
    }

    public final void setValeurDefautTresLong(final String valeur) {
        this.valeurDefautTresLong = valeur;
    }

    /**
     * @return the clauseSelectionnee
     */
    public final String getClauseSelectionnee() {
        return clauseSelectionnee;
    }

    /**
     * @param clauseSelectionnee the clauseSelectionnee to set
     */
    public final void setClauseSelectionnee(final String valeur) {
        this.clauseSelectionnee = valeur;
    }

}
