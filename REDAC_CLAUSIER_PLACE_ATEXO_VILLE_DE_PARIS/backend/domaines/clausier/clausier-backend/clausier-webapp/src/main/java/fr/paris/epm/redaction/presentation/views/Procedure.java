package fr.paris.epm.redaction.presentation.views;

public class Procedure {

	private Integer identifiantExterne;

	private String libelle;

	private String acronyme;

	public void setLibelle( final String libelle ) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public Integer getIdentifiantExterne() {
		return identifiantExterne;
	}

	public void setIdentifiantExterne( Integer identifiantExterne ) {
		this.identifiantExterne = identifiantExterne;
	}

	public void setAcronyme( final String acronyme ) {
		this.acronyme = acronyme;
	}

	public String getAcronyme() {
		return acronyme;
	}
}
