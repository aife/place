package fr.paris.epm.redaction.coordination;

import fr.paris.epm.global.commons.exception.NonTrouveException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.metier.objetvaleur.EtapeSimple;
import fr.paris.epm.noyau.metier.redaction.CanevasCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasPubCritere;
import fr.paris.epm.noyau.metier.redaction.CanevasViewCritere;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import fr.paris.epm.noyau.service.ConsultationServiceSecurise;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;
import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import fr.paris.epm.redaction.coordination.bean.RechercheClauseEnum;
import fr.paris.epm.redaction.metier.documentHandler.*;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Chapitre;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Clause;
import fr.paris.epm.redaction.metier.objetValeur.document.ChapitreDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseDocument;
import fr.paris.epm.redaction.metier.objetValeur.document.ClauseTexte;
import fr.paris.epm.redaction.metier.objetValeur.document.Document;
import fr.paris.epm.redaction.metier.objetValeur.document.tableauxRedaction.TableauRedaction;
import fr.paris.epm.redaction.presentation.mapper.DocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static fr.paris.epm.noyau.persistance.redaction.EpmTClauseValeurPotentiellementConditionneeAbstract.containsValue;

/**
 * Classe implémentant les méthodes exposées par l'interfaceDocumentFacade.
 * @author RAMLI Tarik
 * @version $Revision$, $Date$, $Author$
 */
public class DocumentRedactionFacadeImpl implements DocumentRedactionFacade, Serializable {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = 3316504794431150838L;

	/**
	 * Marqueur du fichier journal.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DocumentRedactionFacadeImpl.class);

	/**
	 * variable injectée par spring.
	 */
	@Autowired
	private RedactionServiceSecurise redactionService;

	/**
	 * variable injectée par spring.
	 */
	@Autowired
	private AdministrationServiceSecurise administrationService;

	/**
	 * Manipulateur des consultations.
	 */
	private transient ConsultationServiceSecurise consultationService = null;

	/**
	 * Acces aux fichiers de libelle (injection Spring).
	 */
	private ResourceBundleMessageSource messageSource;


	/**
	 * Manipulateur des référentiels.
	 */
	private ReferentielFacade referentielFacade = null;

	/**
	 * Manipulateur d'information métier du canevas.
	 */
	private CanevasFacadeGWT canevasFacadeGWT;

	private NoyauProxy noyauProxy;

	private ServiceTechniqueSecurise serviceTechniqueService;

	private DocumentMapper documentMapper;

	private ClauseFacadeGWT clauseFacadeGWT;


	public final Document genererStructureDocument(final Document document, final EpmTUtilisateur user, final int typeDocument,
												   final List<ChapitreDocument> chapitresDocumentInitial, final RechercheClauseEnum rechercheClause) {

		EpmTCanevasAbstract epmTCanevas;

		if (document.getIdPublication() == null) {
			epmTCanevas = redactionService.chercherObject(document.getIdCanevas(), EpmTCanevas.class);
		} else {
			EpmTRefOrganisme organisme = user.getEpmTRefOrganisme();
			CanevasPubCritere canevasPubCritere = new CanevasPubCritere(organisme.getPlateformeUuid());
			canevasPubCritere.setIdCanevas(document.getIdCanevas());
			canevasPubCritere.setIdPublication(document.getIdPublication());
			epmTCanevas = redactionService.chercherUniqueEpmTObject(0, canevasPubCritere);
		}

		if (epmTCanevas == null) {// bidouillage TODO: NIKO REVOIR
			LOG.error("ATTENTION : BRICOLAGE");
			EpmTRefOrganisme organisme = user.getEpmTRefOrganisme();
			epmTCanevas = chargerCanevas(user.getId(), document.getRefCanevas(), user.getIdOrganisme(), organisme.getPlateformeUuid());
		}

		if (epmTCanevas == null) {// super merde-bidouillage TODO: NIKO REVOIR
			LOG.error("ATTENTION : BRICOLAGE BIDOUILLAGE");
			epmTCanevas = redactionService.chercherObject(document.getIdCanevas(), EpmTCanevas.class);
		}

		try {
			EpmTRefTypeDocument typeDoc = epmTCanevas.getEpmTRefTypeDocument();
			document.setDerogationActive(typeDoc.isActiverDerogation());
			// TODO: NIKO REVOIR
			//document.setCanevasModifie(enversGIM.isDerniereVersion(document.getIdCanevas(), EpmTCanevas.class, document.getVersionCanevas()));
			document.setDocType(referentielFacade.getTypeDocumentParId(typeDocument).getLibelle());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e.fillInStackTrace());
			throw new TechnicalException(e);
		}
		EpmTConsultation consultation = document.getIdConsultation();
		if (consultation == null) {
			throw new TechnicalException("La consultation n'est pas renseignée");
		}

		Canevas canevas = canevasFacadeGWT.epmTCanevasVersCanevas(epmTCanevas, user.getIdOrganisme());

		try {
			canevas.setChapitres(getChapitresWithSurcharges(canevas.getChapitres(), user));
			document.setChapitres(DocumentUtil.construireChapitres(document, canevas, consultation, user, document.getIdLot(), messageSource, rechercheClause));
		} catch (NonTrouveException e) {
			LOG.error(e.getMessage(), e.fillInStackTrace());
			throw new TechnicalException(e);
		}
		document.setExtension("." + epmTCanevas.getEpmTRefTypeDocument().getExtensionFichier());
		document.setSommaire(epmTCanevas.getEpmTRefTypeDocument().isSommaire());
		return document;
	}

	/**
	 * Génération d'un objet {@link Document} pour l'export des modéles de
	 * canevas au format word.
	 * @param idCanevas l'identifiant du canevas a éxporter.
	 * @param user      l'utilisateur
	 * @return {@link Document} le document généré.
	 */
	public final Document genererDocumentPourExportCanevas(final int idCanevas, final Integer idPublication, final EpmTUtilisateur user) {
		Document document = new Document();

		try {
			EpmTCanevasAbstract epmTCanevas;
			if (idPublication == null) {
				epmTCanevas = redactionService.chercherObject(idCanevas, EpmTCanevas.class);
			} else {
				EpmTRefOrganisme organisme = user.getEpmTRefOrganisme();
				CanevasPubCritere canevasPubCritere = new CanevasPubCritere(organisme.getPlateformeUuid());
				canevasPubCritere.setIdCanevas(idCanevas);
				canevasPubCritere.setIdPublication(idPublication);
				epmTCanevas = redactionService.chercherUniqueEpmTObject(0, canevasPubCritere);
			}
			Canevas canevas = canevasFacadeGWT.epmTCanevasVersCanevas(epmTCanevas, user.getIdOrganisme());
			document.setNomFichier(canevas.getReferenceCanevas() + "_" + canevas.getTitre());

			String extension = Constante.ODT;
			EpmTRefParametrage parametrage = (EpmTRefParametrage) noyauProxy.getReferentielByReference(Constante.PARAMETRAGE_EXTENSION_EXPORT_CANEVAS, TypeEpmTRefObject.PARAMETRAGE);
			//Si le paramétrage est présent, on surcharge l'extenstion
			if (parametrage != null)
				extension = parametrage.getValeur();

			document.setExtension(extension);
			document.setTitre(document.getNomFichier());
			document.setTypeDocument(epmTCanevas.getEpmTRefTypeDocument().getId());


			canevas.setChapitres(getChapitresWithSurcharges(canevas.getChapitres(), user));

			List<ChapitreDocument> chapitres = DocumentUtil.construireChapitresExportCanevas(document, canevas, user, messageSource);
			document.setChapitres(chapitres);
		} catch (NonTrouveException e) {
			LOG.error(e.getMessage(), e.fillInStackTrace());
			throw new TechnicalException(e);
		}
		return document;
	}

	private List<Chapitre> getChapitresWithSurcharges(List<Chapitre> chapitresInitiaux, EpmTUtilisateur user) {
		//pour chaque chapitre du canevas récupérer les clauses du chapitre, pour chaque clause si la clause est surchargée récupérer la surcharge et l'affecter au chapitre
		List<Chapitre> chapitresAvecSurcharges = new ArrayList<>();
		for (Chapitre chapitre : chapitresInitiaux) {
			List<Clause> clauses = chapitre.getClauses();
			List<Clause> clausesEtSurcharges = new ArrayList<>();
			List<Chapitre> sousChapitres = new ArrayList<>();
			for (Clause clause : clauses) {
				EpmTClause surcharge = clauseFacadeGWT.chercherDerniereSurchargeActif(clause.getIdClause(), user.getIdOrganisme());
				if (surcharge != null && surcharge.getSurchargeActif().equals(Boolean.TRUE)) {
					clausesEtSurcharges.add(clauseFacadeGWT.epmTClauseToClause(surcharge, null, user.getEpmTRefOrganisme().getPlateformeUuid()));
				} else {
					clausesEtSurcharges.add(clause);
				}
			}
			chapitre.setClauses(clausesEtSurcharges);
			chapitre.setChapitres(getChapitresWithSurcharges(chapitre.getChapitres(), user));
			chapitresAvecSurcharges.add(chapitre);
		}

		return chapitresAvecSurcharges;
	}


	public final Document miseAjourValeurHerite(final Document document, final EpmTConsultation consultation,
												final int idLot, final Integer idOrganisme) {

		document.setNumConsultation(consultation.getNumeroConsultation());
		document.setIntituleConsultation(consultation.getIntituleConsultation());
		document.setPouvoirAdjudicateur(consultation.getEpmTRefPouvoirAdjudicateur().getLibelle());
		document.setDirectionServic(consultation.getEpmTRefDirectionService().getLibelle());
		document.setProcedurePassation(consultation.getEpmTRefProcedure().getLibelle());
		document.setNaturePrestation(consultation.getEpmTRefNature().getLibelle());

		LOG.info("Type de document = {}", document.getTypeDocument());

		EpmTRefTypeDocument type = referentielFacade.getTypeDocumentParId(document.getTypeDocument());

		LOG.info("Doc type = {}", type);
		document.setDocType(type.getLibelle());
		if (consultation.getEpmTBudLots() != null) {
			for (EpmTBudLot lot : consultation.getEpmTBudLots()) {
				if (lot.getId() == idLot) {
					document.setLot(lot.getIntituleLot());
					break;
				}
			}
			if (document.getLot() == null)
				document.setLot("Tous");
		}
		if (document.getLot() == null)
			document.setLot("Non applicable");

		document.setIdLot(idLot);
		ConditionnementConsultation condConsultation = new ConditionnementConsultation(consultation);
		parcourirClauseConditionnement(document.getChapitres(), condConsultation, idLot, idOrganisme);

		HeriteConsultation herite = initialiserHeriteConsultation(consultation);
		herite.setMessageSource(messageSource);
		parcourirClause(document.getChapitres(), herite, idLot, idOrganisme);

		return document;
	}



	/**
	 * @param valeur la consultationService à positionner.
	 */
	public final void setConsultationService(final ConsultationServiceSecurise valeur) {
		consultationService = valeur;
	}

	public final void setCanevasFacadeGWT(final CanevasFacadeGWT canevasFacadeGWT) {
		this.canevasFacadeGWT = canevasFacadeGWT;
	}

	/**
	 * @param idChapitre id du chapitre du document à modifier
	 * @param chapitre   chapitre à sauvegarder
	 * @param chapitres  l'ensemble des chapitres du document
	 * @return true si chapitre modifié
	 */
	private boolean modifieChapitre(final int idChapitre, final ChapitreDocument chapitre, final List chapitres) {
		if (chapitres != null) {
			for (int i = 0; i < chapitres.size(); i++) {
				ChapitreDocument chapitreTmp = (ChapitreDocument) chapitres.get(i);
				if (idChapitre == chapitreTmp.getId()) {
					chapitre.setParent(chapitreTmp.getParent());
					chapitre.setSousChapitres(chapitreTmp.getSousChapitres());
					chapitres.set(i, chapitre);
					return true;
				}
				if (modifieChapitre(idChapitre, chapitre, chapitreTmp.getSousChapitres())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param chapitres    l'ensemble des chapitres du document
	 * @param consultation consultation contenant les valeurs héritées
	 * @param idLot        id du lot de la consultation
	 */
	private void parcourirClause(final List<ChapitreDocument> chapitres, final HeriteConsultation consultation,
								 final int idLot, final Integer idOrganisme) {

		if (chapitres != null) {
			for (ChapitreDocument chapitre : chapitres) {
				parcourirClause(chapitre.getSousChapitres(), consultation, idLot, idOrganisme);
				List<ClauseDocument> clauses = chapitre.getClausesDocument();
				if (clauses == null)
					continue;

				for (ClauseDocument clause : clauses) {
					if (clause instanceof ClauseTexte && clause.getType() == ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE) {
						try {
							EpmTClauseAbstract epmTClause =
									clauseFacadeGWT.chargerDernierClauseByRef(false, null, clause.getRef(), idOrganisme);

							List<? extends EpmTRoleClauseAbstract> roleClauses = epmTClause.getEpmTRoleClausesDefault();
							if (!roleClauses.isEmpty()) {
								ClauseTexte clauseTexte = (ClauseTexte) clause;
								EpmTRoleClauseAbstract role = roleClauses.get(roleClauses.size() - 1);

								int idValeurHerite = Integer.parseInt(role.getValeurDefaut());
								if (idValeurHerite != 0) {

									EpmTRefValeurTypeClause epmTRefValeur = referentielFacade.getValeurTypeClauseById(idValeurHerite);
									if (epmTRefValeur.isSimple()) {
										String text = ClauseDocumentUtil.reflection(epmTRefValeur.getExpression(), consultation, idLot);

										LOG.info("Définition du texte variable: expression = {}, texte={}", epmTRefValeur.getExpression(), text);
										clauseTexte.setTexteVariable(null == text ? HeriteConsultation.NOT_DEFINED : text);
									} else {
										TableauRedaction tableauRedaction = GenerationTableauUtil.xmlToTableauRedaction(epmTRefValeur.getExpression(), consultation.getConsultation(), idLot, null);
										clauseTexte.setTableauRedaction(tableauRedaction);
									}
								}
							}
						} catch (NumberFormatException e) {
							LOG.debug("La valeur hétitée n'est pas un ID");
						}
					}
				}
			}
		}
	}

	/**
	 * @param chapitres chapitres du document
	 */
	private void parcourirClauseInitialisation(final List<ChapitreDocument> chapitres) {
		if (chapitres != null) {
			for (ChapitreDocument chapitre : chapitres) {
				parcourirClauseInitialisation(chapitre.getSousChapitres());
				chapitre.setValide(false);
				List<ClauseDocument> clauses = chapitre.getClausesDocument();
				if (clauses == null)
					continue;

				for (ClauseDocument clause : clauses)
					if (clause.getType() != ClauseDocument.TYPE_CLAUSE_TEXTE_FIXE && clause.getType() != ClauseDocument.TYPE_CLAUSE_VALEUR_HERITEE)
						clause.setTerminer(false);
			}
		}
	}

	/**
	 * @param chapitres    chapitres d'un document
	 * @param consultation consultation associé au document
	 * @param idLot        lot associé au document
	 */
	private void parcourirClauseConditionnement(final List<ChapitreDocument> chapitres, final ConditionnementConsultation consultation,
												final int idLot, final Integer idOrganisme) {
		if (chapitres != null) {
			for (ChapitreDocument chapitre : chapitres) {
				parcourirClauseConditionnement(chapitre.getSousChapitres(), consultation, idLot, idOrganisme);
				List<ClauseDocument> clauses = chapitre.getClausesDocument();
				if (clauses == null)
					continue;

				for (ClauseDocument clause : clauses) {
					EpmTClauseAbstract epmTClause = clauseFacadeGWT.chargerDernierClauseByRef(false, null, clause.getRef(), idOrganisme);

					List<EpmTClausePotentiellementConditionneeAbstract> clausePotentiellementConditionneeList =
							new ArrayList<>(epmTClause.getEpmTClausePotentiellementConditionnees());

					if (!clausePotentiellementConditionneeList.isEmpty()) {
						if (isPotentiellemementConditionne(epmTClause, consultation, idLot)) {
							LOG.info("La consultation vérifie les conditions de la clause potentiellement conditionnée, la clause doit être affichée");
							clause.setEtat(ClauseDocument.ETAT_DISPONIBLE);
						} else {
							LOG.info("La consultation ne vérifie pas les conditions de la clause potentiellement conditionnée, la clause ne doit pas être affichée");
							clause.setEtat(ClauseDocument.ETAT_CONDITIONNER);
						}
					}
				}
			}
		}
	}

	/**
	 * @param epmtClause      clause à tester
	 * @param conditionnement test le conditionnement d'une clause
	 * @param idLot           lot associé au document
	 * @return true si conditionné
	 */
	public boolean isPotentiellemementConditionne(final EpmTClauseAbstract epmtClause, final ConditionnementConsultation conditionnement, final int idLot) {

		Set<? extends EpmTClausePotentiellementConditionneeAbstract> epmTRefPotentiellementConditionneeList = epmtClause.getEpmTClausePotentiellementConditionnees();
		for (EpmTClausePotentiellementConditionneeAbstract clausePotCond : epmTRefPotentiellementConditionneeList) {
			EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee = clausePotCond.getEpmTRefPotentiellementConditionnee();
			Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> valeurPotentiellementConditionneeSet = clausePotCond.getEpmTClauseValeurPotentiellementConditionnees();
			LOG.info("Evaluation du conditionnement de la clause {}", clausePotCond.getEpmTClause());
			boolean isPotCond = isPotentiellemementConditionne(epmTRefPotentiellementConditionnee, valeurPotentiellementConditionneeSet, conditionnement, idLot);
			if (epmTRefPotentiellementConditionnee != null && isPotCond) {
				LOG.info("La consultation vérifie la condition {} ", epmTRefPotentiellementConditionnee.getMethodeLibelle());
			} else {
				LOG.info("La consultation ne vérifie pas la condition {} ", epmTRefPotentiellementConditionnee.getMethodeLibelle());
				return false;
			}
		}

		return true;
	}

	private boolean isPotentiellemementConditionne(final EpmTRefPotentiellementConditionnee epmTRefPotentiellementConditionnee,
												   Set<? extends EpmTClauseValeurPotentiellementConditionneeAbstract> epmTClauseValeurPotCondList,
												   final ConditionnementConsultation conditionnement, final int idLot) throws TechnicalException {
		LOG.info("Conditionnement potentiel = {}", epmTRefPotentiellementConditionnee);

		if (null!=epmTClauseValeurPotCondList) {
			LOG.info("Liste des valeurs de conditionnement = {}", epmTClauseValeurPotCondList.stream()
					.map(EpmTClauseValeurPotentiellementConditionneeAbstract::getEpmTClausePotentiellementConditionnee)
					.map(EpmTClausePotentiellementConditionneeAbstract::getEpmTClause)
					.collect(Collectors.toSet()));
		}
		if (epmTRefPotentiellementConditionnee != null) {
			String cheminValeur = epmTRefPotentiellementConditionnee.getMethodeConsultation();

			LOG.info("Methode de consultation = {}", cheminValeur);

			if (cheminValeur != null) {
				String[] argumentTableau = cheminValeur.split(",|\\(|\\)");
				Object[] arguments = null;
				List<Integer> argumentsValeurPotCondList = new ArrayList<>();
				if (argumentTableau.length > 1) {
					arguments = new Object[argumentTableau.length - 1];
				}

				int ouvrante = cheminValeur.indexOf("(");
				String methode = cheminValeur.substring(0, ouvrante);
				int index = 0;
				int valeurIdx = -1;

				if (epmTRefPotentiellementConditionnee.isProcedurePassation()) {
					LOG.info("{} EST une procedure de passation", epmTRefPotentiellementConditionnee);
					Integer idOrganismeConsultation = conditionnement.getConsultation().getIdOrganisme();

					List<EpmTClauseValeurPotentiellementConditionnee> valeursConditionneeProcedurepassation = new ArrayList<EpmTClauseValeurPotentiellementConditionnee>();
					for (EpmTClauseValeurPotentiellementConditionneeAbstract item : epmTClauseValeurPotCondList) {
						Integer idProcedurePassation = referentielFacade.getIdProcedurePassationCorrespondantDansAutreOrganisme(item.getValeur(), idOrganismeConsultation);
						EpmTClauseValeurPotentiellementConditionnee valeurConditionneeProcedurepassation = new EpmTClauseValeurPotentiellementConditionnee();
						valeurConditionneeProcedurepassation.setValeur(idProcedurePassation);
						valeursConditionneeProcedurepassation.add(valeurConditionneeProcedurepassation);
					}
					epmTClauseValeurPotCondList = new HashSet<>(valeursConditionneeProcedurepassation);
				} else {
					LOG.info("{} n'est pas une procedure de passation", epmTRefPotentiellementConditionnee);
				}

				for (int i = 1; i < argumentTableau.length; i++) {
					if (argumentTableau[i].equals("lot")) {
						arguments[index] = idLot;
					} else if (argumentTableau[i].equals("id") && (epmTClauseValeurPotCondList != null)) {
						valeurIdx = index;
						for (EpmTClauseValeurPotentiellementConditionneeAbstract item : epmTClauseValeurPotCondList)
							argumentsValeurPotCondList.add(item.getValeur());
					} else if (argumentTableau[i].contains("[")) {
						String clef = argumentTableau[i].substring(1, argumentTableau[i].length() - 1);
						arguments[index] = clef;
					}
					index++;
				}
				try {
					List<Object> resultatList = new ArrayList<>();
					String[] methodes = methode.split("\\.");
					Object resultatTmp = conditionnement;
					((ConditionnementConsultation) resultatTmp).setConsultationService(consultationService);
					for (int i = 0; i < methodes.length; i++) {
						methode = Util.creationMethode(methodes[i]);

						if (valeurIdx != -1) {
							if (i == (methodes.length - 1)) {
								for (Integer item : argumentsValeurPotCondList) {
									arguments[valeurIdx] = item;
									resultatList.add(Util.lancerMethode(resultatTmp, arguments, methode));
								}
							} else {
								resultatTmp = Util.lancerMethode(resultatTmp, arguments, methode);
							}
						} else {
							if (i == (methodes.length - 1))
								resultatList.add(Util.lancerMethode(resultatTmp, arguments, methode));
							else
								resultatTmp = Util.lancerMethode(resultatTmp, arguments, methode);
						}
					}

					LOG.info("Liste des résultat = {}", resultatList);

					for (Object resultat : resultatList) {
						if (resultat instanceof Integer) {
							if ( ( epmTClauseValeurPotCondList != null ) && ( containsValue(epmTClauseValeurPotCondList, 0) || containsValue(epmTClauseValeurPotCondList, (Integer) resultat))) {
								return true;
							}
						} else if (resultat instanceof String) {
							int id = ((String) resultat).equalsIgnoreCase("oui") ? 2 : 1;

							if ( (epmTClauseValeurPotCondList != null) && containsValue(epmTClauseValeurPotCondList, id) ) {
								return true;
							}
						} else if (resultat instanceof Boolean) {
							return (boolean) resultat;
						} else if (resultat instanceof List) {
							List<Integer> resultats = (List<Integer>)  resultat;
							boolean cond = true;
							if (null!=epmTClauseValeurPotCondList) {
								for ( EpmTClauseValeurPotentiellementConditionneeAbstract conditionneeAbstract : epmTClauseValeurPotCondList ) {
									if ( resultats.stream().noneMatch(c -> c.equals(conditionneeAbstract.getValeur())) ) {

										LOG.info("Aucun résultat parmi {} ne correspond à {}", resultats, conditionneeAbstract.getValeur());
										cond = false;
									}
								}
							}
							return cond;
						}
					}
					return false;
				} catch (final SecurityException | IllegalArgumentException e) {
					LOG.error("Erreur lors de l'évaluation du conditionnement = {}", e.getMessage(), e);
					throw new TechnicalException(e);
				}
			}
		}
		return true;
	}

	/**
	 * @param cheminValeur       chemin d'acces aux methodes
	 * @param heriteConsultation calcul des valeurs héritées
	 * @param idLot              lot associé au document
	 * @return valeur de la consultation
	 * @throws TechnicalException erreur technique
	 */
	private String reflection(final String cheminValeur, final HeriteConsultation heriteConsultation, final int idLot) {

		String[] methodes = cheminValeur.split("\\.");

		Object[] arguments = null;
		for (int i = 0; i < methodes.length; i++) {
			int ouvrante = methodes[i].indexOf("(");
			if (ouvrante != -1) {
				int accolade = cheminValeur.indexOf("[");


				if (accolade != -1) {
					arguments = new Object[2];
					arguments[0] = idLot;
					arguments[1] = cheminValeur.substring(accolade + 1, cheminValeur.length() - 1);

				} else {
					arguments = new Object[1];
					arguments[0] = idLot;
				}

				methodes[i] = methodes[i].substring(0, ouvrante);
			}
		}
		Object resultat = heriteConsultation;
		for (String m : methodes) {
			String methode = Util.creationMethode(m);
			resultat = Util.lancerMethode(resultat, arguments, methode);
		}
		if (resultat instanceof String) {
			return (String) resultat;
		} else if (resultat instanceof List) {
			return resultat.toString();
		}
		return "ND";
	}

	/**
	 * permet d'injecter un ReferentielFacade dans la classe.
	 * @param valeur une instance de referentielFacade
	 */
	public final void setReferentielFacade(final ReferentielFacade valeur) {
		referentielFacade = valeur;
	}


	@Override
	public HeriteConsultation initialiserHeriteConsultation(final EpmTConsultation consultation) {
		List<EpmTValeurConditionnementExterneComplexe> listeExterneComplexe = consultation.getEpmTValeurConditionnementExterneComplexeList();

		List<EpmTValeurConditionnementExterne> listExterne = consultation.getEpmTValeurConditionnementExterneList();

		Date dateRemisePlis = null;
		if (consultation.getCalendriers() != null) {
			EtapeSimple etapeRemisePlis = consultationService.chargerEtapeDateCal(consultation.getCalendriers().stream().map(EpmTCalendrier::getEtapes).flatMap(Collection::stream).filter(epmTEtapeCal -> epmTEtapeCal.getTypeEtape() == EpmTEtapeCal.RECEPTION_CANDIDATURES).findFirst().orElse(null));
			dateRemisePlis = etapeRemisePlis == null ? null : etapeRemisePlis.getDate();
		}

		return new HeriteConsultation(consultation, dateRemisePlis, listExterne, listeExterneComplexe);
	}

	public EpmTCanevasAbstract chargerCanevas(final int utilisateurId, final String refCanevas, final Integer idOrganisme, String plateformeUuid) {
		CanevasViewCritere viewCritere = new CanevasViewCritere(plateformeUuid);
		viewCritere.setReference(refCanevas);
		viewCritere.setIdOrganisme(idOrganisme);
		List<EpmTCanevasAbstract> listCanevas = redactionService.chercherEpmTObject(0, viewCritere);

		if (listCanevas == null) { // TODO : c'est la MERDE
			CanevasCritere critere = new CanevasCritere(plateformeUuid);
			critere.setReference(refCanevas);
			critere.setIdOrganisme(idOrganisme);
			listCanevas = redactionService.chercherEpmTObject(0, critere);
		}
		if (listCanevas != null && !listCanevas.isEmpty())
			return listCanevas.get(0); // TODO : c'est la GROSSE MERDE
		return null;
	}

	public final void setNoyauProxy(final NoyauProxy valeur) {
		this.noyauProxy = valeur;
	}

	/**
	 * @param valeur Acces aux fichiers de libelle (injection Spring).
	 */
	public final void setMessageSource(final ResourceBundleMessageSource valeur) {
		this.messageSource = valeur;
	}

	public void setServiceTechniqueService(ServiceTechniqueSecurise valeur) {
		this.serviceTechniqueService = valeur;
	}

	public void setRedactionService(RedactionServiceSecurise redactionService) {
		this.redactionService = redactionService;
	}

	public void setAdministrationService(AdministrationServiceSecurise administrationService) {
		this.administrationService = administrationService;
	}
	public void setDocumentMapper(DocumentMapper documentMapper) {
		this.documentMapper = documentMapper;
	}

	public final void setClauseFacadeGWT(final ClauseFacadeGWT valeur) {
		this.clauseFacadeGWT = valeur;
	}

}
