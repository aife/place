package fr.paris.epm.redaction.presentation.forms;

import fr.paris.epm.redaction.commun.Constante;
import fr.paris.epm.redaction.commun.Util;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;


public class ModifierLogoForm extends ActionForm {

    /**
     * Marqueur de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Les formats image autorisé pour upload logo
     */
    private static final List<String> FORMAT_IMAGE_ACCEPTES = Arrays.asList("gif", "jpeg", "jpg", "png");

    /**
     * Taille de fichier max est 150 KB, soit 150*1024 bytes
     */
    private static final int TAILLE_FICHIER_MAX = 150 * 1024;

    /**
     * L'image uploadé
     */
    private FormFile fichierLogo;

    /**
     * Le type d'action qu'effectuera la classe d'action.
     */
    private String aiguillage;

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        fichierLogo = null;
        aiguillage = "";
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors erreurs = new ActionErrors();
        if (Constante.AIGUILLAGE_AJOUTER_LOGO.equalsIgnoreCase(aiguillage)) {
            if (fichierLogo == null || fichierLogo.getFileSize() == 0) {
                erreurs.add("fichierLogoVide", new ActionMessage("modifierLogo.erreur.fichierLogoVide"));
            } else {
                String extension = Util.getExtensionFichier(fichierLogo.getFileName());
                if (extension.isEmpty() || !FORMAT_IMAGE_ACCEPTES.contains(extension)) {
                    erreurs.add("formatFichierNonValide", new ActionMessage("modifierLogo.erreur.formatFichierNonValide"));
                } else if (fichierLogo.getFileSize() > TAILLE_FICHIER_MAX) {
                    erreurs.add("formatFichierNonValide", new ActionMessage("modifierLogo.erreur.tailleFichierDepasse"));
                }
            }
        }
        return erreurs;
    }

    public FormFile getFichierLogo() {
        return fichierLogo;
    }

    public void setFichierLogo(FormFile valeur) {
        this.fichierLogo = valeur;
    }

    public final String getAiguillage() {
        return aiguillage;
    }

    public final void setAiguillage(final String valeur) {
        this.aiguillage = valeur;
    }
}
