package fr.paris.epm.redaction.coordination.service;

import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;

import java.util.List;

public interface CanevasService extends GeneriqueServiceSecurise {

	public String getReferenceCanevasSuivante( final boolean canevasEditeur, final Integer idOrganisme );

	/**
	 * Recherche de canevas interministerielle
	 *
	 * @param referenceClause la référence de la clause
	 *
	 * @return la liste des canevas
	 */
	List<EpmTCanevas> chercherListeCanevasPourClauseInterministerielle( final String referenceClause );

	/**
	 * Recherche de canevas ministerielle
	 *
	 * @param referenceClause la référence de la clause
	 * @param idOrganisme l'identifiant de l'organisme
	 *
	 * @return la liste des canevas
	 */
	List<EpmTCanevas> chercherListeCanevasPourClauseMinisterielle( final String referenceClause, final Integer idOrganisme );
}
