package fr.paris.epm.redaction.webservice.beans;

import fr.paris.epm.redaction.webservice.rest.constantes.ConstantesRestWebService.ErreurEnum;

import javax.xml.bind.annotation.XmlElement;

/**
 * Utilise pour l'echange d'informations avec des entites externes a RSEM.
 * Envoye pour decrire une erreur.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class ErreurBean {

    @XmlElement(name = "code")
    private int code;

    private String message;

    public String toString() {
        return code + " : " + message;
    }

    public void setCodeMessage(ErreurEnum valeurCode, String valeurMessage) {
        code = valeurCode.getValeur();
        message = valeurMessage;
    }

    public final void setCode(ErreurEnum valeur) {
        this.code = valeur.getValeur();
    }
    
    public final int getCode() {
        return code;
    }

    public final void setMessage(final String valeur) {
        this.message = valeur;
    }

    public final String getMessage() {
        return message;
    }
}
