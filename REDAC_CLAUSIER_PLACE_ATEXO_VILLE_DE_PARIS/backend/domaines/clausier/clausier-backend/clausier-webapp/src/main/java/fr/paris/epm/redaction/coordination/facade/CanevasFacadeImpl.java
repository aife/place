package fr.paris.epm.redaction.coordination.facade;

import fr.paris.epm.global.commun.ResultList;
import fr.paris.epm.global.coordination.facade.AbstractMapperFacade;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.global.coordination.mapper.AbstractEpmObjectToBeanMapper;
import fr.paris.epm.noyau.metier.redaction.CanevasCritere;
import fr.paris.epm.noyau.persistance.redaction.EpmTCanevas;
import fr.paris.epm.noyau.persistance.redaction.EpmTPublicationClausier;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.redaction.coordination.service.CanevasService;
import fr.paris.epm.redaction.metier.objetValeur.canevas.Canevas;
import fr.paris.epm.redaction.presentation.bean.CanevasBean;
import fr.paris.epm.redaction.presentation.bean.CanevasSearch;
import fr.paris.epm.redaction.presentation.bean.PageRepresentation;
import fr.paris.epm.redaction.presentation.mapper.CanevasMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade de gestion des Canevas Niveau Interministeriel
 * Created by nty on 04/07/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Service
public class CanevasFacadeImpl extends AbstractMapperFacade<CanevasBean, EpmTCanevas> implements CanevasFacade {

    private static final Logger LOG = LoggerFactory.getLogger(CanevasFacadeImpl.class);

    private CanevasMapper canevasMapper;

    private CanevasService canevasService;

    public CanevasFacadeImpl() {
        setCritereContructor(CanevasCritere::new);
        setDefaultSortField("dateModification");
        setDefaultIdField("idCanevas");
    }

    @Override
    public CanevasService getService() {
        return canevasService;
    }

    @Override
    protected boolean defaultRemoveTest(int id) {
        return false;
    }

    @Override
    public AbstractBeanToEpmObjectMapper<CanevasBean, EpmTCanevas> getToEpmObjectMapper() {
        return null; // TODO: refaire comme dans les module Administration et Passation après la fusion NOYAU-REDAC
    }


    @Override
    public CanevasBean map(EpmTCanevas epmTCanevas, EpmTRefOrganisme epmTRefOrganisme) {
        CanevasBean canevasBean = canevasMapper.toCanevasBean(epmTCanevas);
        if (canevasBean.isCanevasEditeur()) {
            canevasBean.setTypeAuteur(2);
            canevasBean.setLabelTypeAuteur("Editeur");
        } else {
            canevasBean.setTypeAuteur(1);
            canevasBean.setLabelTypeAuteur("Client");
        }

        if (epmTCanevas.getIdLastPublication() != null && epmTCanevas.getIdLastPublication() != 0) {
            EpmTPublicationClausier publication = canevasService.chercherObject(epmTCanevas.getIdLastPublication(), EpmTPublicationClausier.class);
            canevasBean.setLastVersion(publication.getVersion());
        } else {
            canevasBean.setLastVersion("NA");
        }
        return canevasBean;
    }

    @Override
    public EpmTCanevas map(CanevasBean canevasBean,EpmTRefOrganisme epmTRefOrganisme) {
        EpmTCanevas epmTCanevas;
        if (canevasBean.getIdCanevas() == 0) {
            epmTCanevas = canevasMapper.toEpmTCanevas(canevasBean);
        } else {
            epmTCanevas = safelyUniqueFind("idCanevas", canevasBean.getIdCanevas());
            canevasMapper.toEpmTCanevas(canevasBean, epmTCanevas);
        }
        return epmTCanevas;
    }

    @Override
    public PageRepresentation<CanevasBean> findCanevas(CanevasSearch search, EpmTRefOrganisme epmTRefOrganisme) {

        ResultList<CanevasBean, ? extends CanevasFacade, CanevasCritere> resultList = new ResultList<>(this, new CanevasCritere(epmTRefOrganisme.getPlateformeUuid()));

        resultList.critere().setReference(search.getReferenceCanevas());

        String referenceClause = search.getReferenceClause();
        if (null != referenceClause && !referenceClause.isEmpty()) {
            List<Integer> canevasId = canevasService.chercherListeCanevasPourClauseInterministerielle(referenceClause)
                    .stream().map(EpmTCanevas::getId).distinct().collect(Collectors.toList());

            LOG.info("{} ids distincts trouves", canevasId.size());

            resultList.critere().setListIds(canevasId.isEmpty() ? Collections.singletonList(0) : canevasId);
        } else {
            resultList.critere().setListIds(null);
        }

        resultList.critere().setNaturePrestation(search.getIdNaturePrestation());
        resultList.critere().setAuteur(search.getTypeAuteur());
        resultList.critere().setIdStatutRedactionClausier(search.getIdStatutRedactionClausier());
        resultList.critere().setIdTypeDocument(search.getIdTypeDocument());
        resultList.critere().setIdTypeContrat(search.getIdTypeContrat());
        resultList.critere().setProcedure(search.getIdProcedure());
        resultList.critere().setActif(search.getActif());
        resultList.critere().setDateModificationDebut(search.getDateModificationMin());
        resultList.critere().setDateModificationFin(search.getDateModificationMax());
        resultList.critere().setIdOrganisme(epmTRefOrganisme.getId());
        resultList.critere().setTitre(search.getTitreCanevas());

        resultList.critere().setEditeur(search.isEditeur());

        resultList.critere().setProprieteTriee(search.getSortField());
        resultList.critere().setTriCroissant(search.isAsc());
        resultList.critere().setNumeroPage(search.getPage());
        resultList.critere().setTaillePage(search.getSize());

        resultList.critere().setChercherNombreResultatTotal(true);

        List<Integer> ccag = new ArrayList<>();
        if (search.getIdCCAG() != null) {
            ccag.add(search.getIdCCAG());
            resultList.critere().setListeIdCCAG(ccag);
        }

        ResultList<CanevasBean, ? extends CanevasFacade, CanevasCritere> canevasBeans = resultList.find(epmTRefOrganisme);

        PageRepresentation<CanevasBean> representation = new PageRepresentation<>();
        representation.setContent(canevasBeans.getListResults());
        representation.setFirst(canevasBeans.pageCurrent() == 1);
        representation.setLast(canevasBeans.pageCurrent() == canevasBeans.getCountPages());
        representation.setNumberOfElements(canevasBeans.listResults().size());
        representation.setTotalPages(canevasBeans.countPages());
        representation.setTotalElements(canevasBeans.getCount());
        representation.setSize(search.getSize());
        representation.setNumber(search.getPage());
        return representation;
    }

    @Override
    public Canevas findCanevasById(Integer id) {
        EpmTCanevas epmTObject = safelyUniqueFind("idCanevas", id);
        return canevasMapper.toCanevas(epmTObject);
    }

    @Autowired
    public void setCanevasMapper(CanevasMapper canevasMapper) {
        this.canevasMapper = canevasMapper;
    }

    @Autowired
    public void setCanevasService(CanevasService canevasService) {
        this.canevasService = canevasService;
    }

}
