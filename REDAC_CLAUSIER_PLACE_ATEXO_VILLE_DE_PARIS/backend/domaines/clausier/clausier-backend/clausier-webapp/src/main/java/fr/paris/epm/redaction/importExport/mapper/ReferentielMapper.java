package fr.paris.epm.redaction.importExport.mapper;

import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.redaction.BaseEpmTRefRedaction;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.redaction.importExport.bean.ReferentielImportExport;
import org.mapstruct.Mapper;

@Mapper
public interface ReferentielMapper {

    ReferentielImportExport toReferentielImportExport( EpmTRef referentiel);

    BaseEpmTRefRedaction toRedactionReferentiel( ReferentielImportExport referentiel);

    EpmTRefProcedure toReferentielProcedure(ReferentielImportExport referentiel);

}
