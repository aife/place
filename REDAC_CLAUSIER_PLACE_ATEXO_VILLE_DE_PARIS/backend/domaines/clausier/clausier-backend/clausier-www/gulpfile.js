var gulp = require('gulp');
var xml2js = require('xml2js');
var fs = require('fs');
var pomXml = fs.readFileSync('pom.xml', "utf8");
var tsc = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');

var projectVersion;

xml2js.parseString(pomXml, function (err, result) {
    projectVersion = result.project.parent[0].version[0];
});

gulp.task('nodeFlatModules', function () {

    return gulp.src([

        'node_modules/angular2/bundles/angular2-polyfills.min.js',
        'node_modules/angular2/bundles/angular2.min.js',
        'node_modules/angular2/bundles/router.min.js',
        'node_modules/angular2/bundles/http.min.js',
        'node_modules/es6-shim/es6-shim.min.js',
        'node_modules/traceur/bin/traceur-runtime.js',
        'node_modules/systemjs/dist/*',
        'node_modules/whatwg-fetch/fetch.js',
        'node_modules/rxjs/bundles/Rx.min.js'

    ]).pipe(gulp.dest('target/commun.webapp-' + projectVersion + '/front_modules'));
});

gulp.task('bowerModules', function () {
    return gulp.src([

        'bower_components/**/*.*'

    ]).pipe(gulp.dest('target/commun.webapp-' + projectVersion + '/front_modules'));
});


var SRC_BASE_PATH = './src/main/webapp/ng2';

var BASE_PATH = 'target/commun.webapp-' + projectVersion + '/ng2';

var tsProject = tsc.createProject('tsconfig.json', {typescript: require('typescript')});

gulp.task('ts', function () {
    var result = gulp.src(SRC_BASE_PATH + '/**/*.ts')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));

    return result.js
        .pipe(sourcemaps.write('source'))
        .pipe(gulp.dest(BASE_PATH));
});

gulp.task('default', ['nodeFlatModules', 'bowerModules', 'ts'], function () {

});