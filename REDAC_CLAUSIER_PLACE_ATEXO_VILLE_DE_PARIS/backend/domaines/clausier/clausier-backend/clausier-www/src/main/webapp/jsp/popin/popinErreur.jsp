<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class='form-bloc'>
    <div class='top'>
        <span class='left'></span><span class='right'></span>
    </div>
    <div class='content'>
        <div class='spacer'></div>
        <div class='center'>${popInMessageErreur}</div>
        <div class='spacer'></div>
    </div>
    <div class='bottom'>
        <span class='left'></span><span class='right'></span>
    </div>
</div>
<div class="boutons">
    <div id="annulerId">
        <div><a href="javascript:void(0)" class="annuler">Fermer</a></div>
    </div>
</div>
