<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 31/03/17
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>

<!--Debut confirmation-->
<div class="modal" id="modalConfirmation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title"><spring:message code="popupConfirmation.titre" /></h4>
            </div>
            <div class="modal-body">
                <div class="spacer"></div>
                <div id="title" class="text-center"></div>
                <div id="text" class="text-center"></div>
                <div class="spacer"></div>

                <div class="clearfix">
                    <button type="button" id="annuler" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <spring:message code="popupConfirmation.annuler" />
                    </button>
                    <a id="valider" class="btn btn-danger btn-sm pull-right" href="#">
                        <spring:message code="popupConfirmation.valider" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Fin confirmation-->

<script type="application/javascript" language="JavaScript" lang="javascript">

    $(document).ready(function() {

        $("#modalConfirmation").on("show.bs.modal", function (event) {

            var button = $(event.relatedTarget); // Button that triggered the modal
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            modal.find('#title').text(button.data('title')); // Extract info from data-* attributes
            modal.find('#text').text(button.data('text'));
            modal.find("#valider").attr("href", button.data('href'));
        });
    });

</script>
