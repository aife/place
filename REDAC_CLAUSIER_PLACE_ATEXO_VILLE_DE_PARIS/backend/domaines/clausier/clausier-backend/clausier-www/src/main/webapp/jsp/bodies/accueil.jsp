<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="html"  uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="bean"  uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="atexo" uri="AtexoTag" %>

<div class="main-part">
    <!--Debut Message accueil-->
    <div class="message-accueil">
        <div class="form-saisie">
            <div class="form-bloc">
                <div class="top"><span class="left"></span><span class="right"></span></div>
                <div class="content">
                    <div class="titre">Bienvenue dans EPM </div>
                    Cette application doit vous permettre de piloter toutes les étapes de la passation des marchés : aide à la rédaction des pièces, planification, gestion des commissions, etc.
                    N'hésitez pas à nos faire vos commentaires pour les évolutions que vous souhaitez : <a href="mailto:epm@epm.fr">epm@epm.fr</a>
                </div>
                <div class="bottom"><span class="left"></span><span class="right"></span></div>
            </div>
        </div>
    </div>
    <!--Fin Message accueil-->
    <!--Debut Actualites-->
    <div class="actualites">
        <div class="titre">&nbsp;</div>
        <%--            <p><strong>5 septembre 2007 </strong><br /> --%>
        <%--            Une nouvelle jurisprudence du Conseil d'Etat produit différentes exigences pour les avis d'attribution. Veuillez-vous référer aux fiches pratiques récemment mises sur la Base de connaissance</p>--%>

        <!--les Actualites sont separes par un DIV 'separator', sauf le dernier -->
        <!--
        <div class="separator"></div>
        <p><strong>5 septembre 2007 </strong><br />
        Une nouvelle jurisprudence du Conseil d'Etat produit différentes exigences pour les avis d'attribution. Veuillez-vous référer aux fiches pratiques récemment mises sur la Base de connaissance</p>
        -->
        <!--Fin Actualites sont separes par un DIV 'separator', sauf le dernier -->

        <logic:iterate id="actualite" name="gererActualite">
            <p>
                <strong>
                    <bean:write name="actualite" property="date"/>
                </strong>
                <br/>
                <bean:write name="actualite" property="contenu"/>
                <br/>
            </p>
        </logic:iterate>

    </div>
    <!--Fin Actualites-->
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="breaker"></div>
</div>