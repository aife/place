<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 23/09/18
  Time: 09:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="idTask" type="java.lang.String"--%>

<div class="popup-moyen text-center" id="contentPopup-${idTask}">
    <div id='loader-${idTask}' class='center'>
        <img src='images/loader.gif' alt='Veuillez patienter' title='Veuillez patienter'/>
    </div>
    <div id="contentMessage-${idTask}" class="center text-center">
        <spring:message code="popupTaskWorker.contentMessage" />
    </div>
    <div id="contentErrorMessage-${idTask}" class="center text-center">
        <font color="red"><spring:message code="popupTaskWorker.contentMessage.erreur" /></font>
    </div>
    <div id="executionResultLink-${idTask}" class="center text-center">
        <a class="link hvr-fade" href="">
            <span class="text s-ml" id="fichier"><spring:message code="popupTaskWorker.resultLinkText" /></span>
        </a>
    </div>
</div>

<script type="application/javascript" id="scriptPopup-${idTask}">

    var _TASK_TIMEOUT = 3000;

    jQuery(document).ready(function () {
        jQuery('#executionResultLink-${idTask}').hide();
        jQuery('#contentErrorMessage-${idTask}').hide();
        setTimeout(function () {
            verifier_${idTask.replaceAll("-", "_")}();

            function verifier_${idTask.replaceAll("-", "_")}() {
                jQuery.get('verifierRsemTask.json', {idTask: "${idTask}", timeStamp: new Date().getTime()})
                    .done(function (reponse) {
                        if (reponse.data) {
                            if (reponse.data === "downloadTaskResult.htm") {
                                jQuery('#executionResultLink-${idTask}').show();
                                jQuery('#loader-${idTask}').hide();
                                jQuery('#contentMessage-${idTask}').hide();
                                jQuery('#executionResultLink-${idTask} a').attr('href', reponse.data + '?idTask=${idTask}');
                            } if (reponse.data.indexOf("ajaxTaskResult.json", 0) === 0) {
                                var indexCallBack = reponse.data.indexOf('callback');
                                var nameCallback = reponse.data.substring(indexCallBack + 'callback'.length + 1);
                                var linkRedirect = reponse.data.substring(0, indexCallBack -1 );

                                console.log("Task is finished -> go to '" + linkRedirect + "', result function '" + nameCallback + "'");
                                var funcCallback;
                                if (typeof window[nameCallback] === typeof undefined)
                                    funcCallback = ATX.fn.$obj(window, nameCallback);
                                else
                                    funcCallback = window[nameCallback];
                                jQuery.get(reponse.data, {idTask: "${idTask}"})
                                    //.done(funcCallback);
                                    .done(function (data) {
                                        console.log(data);
                                        funcCallback(data);
                                    });
                            } else {
                                window.location.href = reponse.data + '?idTask=${idTask}';
                            }
                        } else if (reponse.result === "error") {
                            jQuery('#loader-${idTask}').hide();
                            jQuery('#contentMessage-${idTask}').hide();
                            jQuery('#contentErrorMessage-${idTask}').show();
                        } else {
                            setTimeout(verifier_${idTask.replaceAll("-", "_")}, _TASK_TIMEOUT);
                        }
                    })
                    .fail(function () {
                        //setTimeout(verifier_${idTask.replaceAll("-", "_")}, _TASK_TIMEOUT);
                        alert("Erreur de la génération communication html");
                        jQuery('#loader-${idTask}').hide();
                        jQuery('#contentMessage-${idTask}').hide();
                        jQuery('#contentErrorMessage-${idTask}').show();
                    });
            }
        }, _TASK_TIMEOUT);
    });
</script>
