<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 08/03/17
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="message" type="fr.paris.epm.global.presentation.controllers.AbstractController.Message"--%>
<c:if test="${not empty message}">

    <c:if test="${message.type eq 'ERROR'}">
        <%--<c:set var="typeMessage" value="error" />--%>
        <c:set var="typeMessage" value="danger" />
    </c:if>
    <c:if test="${message.type eq 'SUCCESS'}">
        <c:set var="typeMessage" value="success" />
    </c:if>
    <c:if test="${message.type eq 'WARNING'}">
        <c:set var="typeMessage" value="warning" />
    </c:if>

    <div id="message-block" class="atx-panel panel panel-${typeMessage}">
        <div class="panel-body text-${typeMessage} has-${typeMessage} bg-${typeMessage} atx-message-${typeMessage}">
            <div class="clearfix">
                <div class="col-md-12">
                    <div class="title"><strong>${message.title}</strong></div>
                    <ul>
                        <c:forEach var="msg" items="${message.lines}">
                            <li>${msg}</li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</c:if>
