(function ($) {

    var ATX = {

        header: {
            affix: {
                init: function () {
                    var classAffixHeader = 'affix-header',
                        limit = 72;
                    $(window).on('scroll', function (eventData) {
                        if (($(document).height() - $(window).height()) > limit) {
                            ($(window).scrollTop() > limit) ?
                                $('body').addClass(classAffixHeader) :
                                $('body').removeClass(classAffixHeader);
                        }

                    });
                    return true;
                },
                ready: function () {
                    ATX.header.affix.init();
                    return true;
                }
            },
            avatar: {
                init: function () {
                    var $userAvatar = $('.subnav-user .avatar'),
                        userName = $userAvatar.data('user'),
                        userNameTab = userName.split(' '),
                        login = '';
                    for (index = 0; index < userNameTab.length; index++) {
                        login += userNameTab[index][0];
                    }
                    $userAvatar.html(login);
                    return true;
                },
                ready: function () {
                    ATX.header.avatar.init();
                    return true;
                }
            },
            init: function () {
                ATX.header.avatar.ready();
                ATX.header.affix.ready();
                return true;
            },
            ready: function () {
                ATX.header.init();
                return true;
            }
        },
        bootstrap: {
            init: function () {
                $('[data-toggle="tooltip"]').tooltip();
                return true;
            },
            ready: function () {
                ATX.bootstrap.init();
                return true;
            }
        },
        component: {
            panel: {
                options: {
                    classOpen: 'open',
                },
                element: function ($this) {
                    return {
                        $panel: $this.parents('.atx-form-panel_container.panel-toggle'),
                        $panelHeadeing: $this,
                        $panelBody: $this.parents('.atx-form-panel_container.panel-toggle').find('.atx-form-panel_body')
                    };
                },
                toggle: function ($obj) {
                    $obj.$panelHeadeing.attr('aria-expanded', function () {
                        return ($obj.$panelHeadeing.attr('aria-expanded') === 'true') ? 'false' : 'true';
                    });
                    $obj.$panelBody.slideToggle();
                    $obj.$panel.toggleClass(ATX.component.panel.options.classOpen);
                    return false;
                },
                init: function () {
                    var panelClassOpen = 'open',
                        $obj = {};
                    $('.atx-form-panel_container.panel-toggle').each(function () {
                        $obj.$panel = $(this);
                        $obj.$panelHeadeing = $(this).find('.atx-form-panel_headeing_container');
                        $obj.$panelBody = $(this).find('.atx-form-panel_body');

                        // Init
                        if ($obj.$panel.hasClass(ATX.component.panel.options.classOpen)) {
                            $obj.$panelBody.show();
                            $obj.$panelHeadeing.attr('aria-expanded', 'true');
                        }

                        // Toggle Event Click
                        $obj.$panelHeadeing.click(function () {
                            ATX.component.panel.toggle(ATX.component.panel.element($(this)));
                            e.preventDefault();
                            return false;
                        });

                        // Toggle Event Keyup
                        $obj.$panelHeadeing.keypress(function (e) {
                            if (e.keyCode === 13 || e.keyCode === 32) {
                                e.preventDefault();
                                ATX.component.panel.toggle($obj);
                                return false;
                            }

                            return false;
                        });

                    });
                    return true;
                },
                ready: function () {
                    ATX.component.panel.init();
                    return true;
                }

            },
            search: {
                options: {
                    url: {
                        service: '',
                        submit: ''
                    },
                    milliseconds: 500,
                    timeOut: '',
                    encode: false
                },
                q: '',
                elem: '',
                service: function () {
                    console.log(ATX.component.search.q);
                    $.ajax({
                        url: ATX.component.search.options.url.service + ATX.component.search.q,
                        success: function (data) {
                            ATX.component.search.data(data);
                        },
                        error: function () {
                            console.log('error')
                        }
                    });
                    return true;
                },
                data: function (data) {
                    var index = 0, length = data.length, rows = '';
                    $elem = ATX.component.search.element(ATX.component.search.elem);
                    if (length !== 0) {
                        for (index = 0; index < data.length; index++) {
                            rows += ATX.component.search.row(data[index]);
                        }
                        $elem.search.removeClass('hide-result').addClass('show-result');
                        $elem.result.html(rows);
                    }
                },
                row: function (data) {
                    return '<li class="item" data-idRow="' + data['idConsultation'] + '"><a class="item_link" href="' + ATX.component.search.url(data['numeroConsultation']) + '">' + data['numeroConsultation'] + ' ' + data['intituleConsultation'] + '</a></li>'
                },
                clear: function ($elem) {
                    $elem.q.val('');
                    $elem.btn.attr('href', '');
                    ATX.component.search.q = '';
                    $elem = ATX.component.search.element(ATX.component.search.elem);
                    $elem.search.removeClass('show-result').addClass('hide-result');
                    $elem.result.html('');
                    return true;
                },
                encode: function (q) {
                    q = q.replace('<B>', '').replace('<b>', '').replace('</B>', '').replace('</b>', '');
                    if (ATX.component.search.options.encode) {
                        return Base64.encode(encodeURIComponent(q));
                    }
                    return q;

                },
                url: function (q) {
                    return ATX.component.search.options.url.submit + ATX.component.search.encode(q);
                },
                href: function ($elem) {
                    $elem.btn.attr('href', ATX.component.search.url(ATX.component.search.q));
                },
                submit: function () {
                    document.location.href = ATX.component.search.url(ATX.component.search.q);
                },
                element: function ($q) {
                    return {
                        q: $q,
                        search: $q.parents('.atx-component_search'),
                        btn: $q.parents('.atx-component_search').find('.atx-component_search-ok'),
                        result: $q.parents('.atx-component_search').find('.atx-component_search-result .atx-component_search-result_list')
                    };
                },
                search: function () {
                    clearTimeout(ATX.component.search.options.timeOut);
                    ATX.component.search.options.timeOut = setTimeout(function () {
                        ATX.component.search.service();
                    }, ATX.component.search.options.milliseconds);
                },
                init: function () {
                    $this = $('.atx-component_search');

                    $this.each(function () {
                        $(this).append('<div class="atx-component_search-result"><ul class="atx-component_search-result_list"></ul></div>');

                        $(this).find('.atx-component_search-q').on('keyup', function (event) {
                            ATX.component.search.elem = $(this);
                            $elem = ATX.component.search.element(ATX.component.search.elem);
                            if (event.which == 27) { // Escapes
                                ATX.component.search.clear($elem);
                                event.preventDefault();
                            } else {
                                ATX.component.search.q = $elem.q.val();
                                ATX.component.search.options.url.submit = $elem.q.attr('data-urlSubmit');
                                ATX.component.search.options.url.service = $elem.q.attr('data-urlService');

                                if (ATX.component.search.q === '') {
                                    ATX.component.search.clear($elem);
                                    event.preventDefault();
                                } else {
                                    if (event.which == 13) { // Enter
                                        ATX.component.search.submit();
                                        event.preventDefault();
                                    } else {
                                        ATX.component.search.href($elem);
                                        ATX.component.search.search();
                                    }
                                }
                            }
                        });

                        $(this).find('.atx-component_search-ok').on('click', function (event) {
                            if ($(this).attr('href') === '') {
                                return false;
                            }
                            return true;
                        });

                    });


                    return true;
                },
                ready: function () {
                    ATX.component.search.init();
                    return true;
                }
            }
        },
        document: {
            oldJs: function () {
                if (typeof(Draggable) !== 'undefined') {
                    new Draggable('containerBlocNotes', {revert: false, ghosting: false});
                }
                autoPositionLayerBlocNotes();
            },
            init: function () {
                ATX.document.oldJs();
                ATX.bootstrap.ready();
                ATX.header.ready();
                ATX.component.panel.ready();
                ATX.component.search.ready();

                return true;
            },
            ready: function () {
                $(document).ready(function () {
                    ATX.fn.$prototypeNoConflict();
                    ATX.document.init();
                });
                return true;
            }
        },
        fn: {
            $id: function (_id) {
                return $('#' + _id);
            },
            $class: function (_class) {
                return $('.' + _class);
            },
            $prototypeNoConflict: function () {
                if (typeof(Prototype) !== 'undefined') {
                    if (Prototype.BrowserFeatures.ElementExtensions) {
                        var disablePrototypeJS = function (method, pluginsToDisable) {
                                var handler = function (event) {
                                    event.target[method] = undefined;
                                    setTimeout(function () {
                                        delete event.target[method];
                                    }, 0);
                                };
                                pluginsToDisable.each(function (plugin) {
                                    jQuery(window).on(method + '.bs.' + plugin, handler);
                                });
                            },
                            pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover'];
                        disablePrototypeJS('show', pluginsToDisable);
                        disablePrototypeJS('hide', pluginsToDisable);
                    }
                }

            }
        }

    };

    ATX.document.ready();
})(jQuery);