<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="StrutsBean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="StrutsHtml" prefix="html"%>
<%@ taglib uri="StrutsLogic" prefix="logic"%>
<%
  response.setHeader("Cache-Control","no-cache, must-revalidate, proxy-revalidate");
  response.setHeader("Pragma","no-cache"); 
  response.setDateHeader ("Expires", 0); 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><bean:message name="titre"/></title>
<script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
<script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
<link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
<script type="text/javascript">
	 hrefRacine = '<atexo:href href=""/>';
</script>
</head>
<body onload="popupResize();" id="layoutPopup">
<c:if test="${!empty fermerPopup}">
    <script type="text/javascript">
           window.close();
    </script>
</c:if>
<div class="popup-moyen2" id="container">
    <h1><bean:message name="titre"/></h1>
    <div class="spacer"></div>

        <logic:messagesPresent>
            <div class="form-bloc-erreur msg-erreur">
                <div class="top">
                    <span class="left"></span><span class="right"></span>
                </div>
                <div class="content">
					<div class="title"><bean:message key="erreur.texte.generique"/></div>
                    <ul>
                        <logic:messagesPresent property="tailleMaxAutorisee" >
                            
                                <html:errors property="tailleMaxAutorisee"/>
                        </logic:messagesPresent>
                        <logic:messagesPresent property="destinataires" >
                            
                                <html:errors property="destinataires"/>
                        </logic:messagesPresent>
                    </ul>            
                </div>
                <div class="breaker"></div>
                <div class="bottom">
                    <span class="left"></span><span class="right"></span>
                </div>
            </div>
        </logic:messagesPresent>
        <!--Debut bloc Liste des courriers electroniques-->
        <div class="form-saisie" id="envoiCourrier">
            <div class="form-bloc">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <!--Debut bloc courrier-->
                    <html:form action="/popUpEnvoiCourrielAction.epm">
                        <div style="display:none">
                            <html:text property="tailleMaxAutorisee"/>
                        </div>
                         <c:forEach items="${listeCourriels}" var="couriel" varStatus="status">
                             <div class="title"><img src="<atexo:href href='images/picto-close-panel.gif'/>" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" onclick="togglePanel(this,'courrier-<c:out value="${status.index}"/>');" />Envoi <c:out value="${status.index +1}"/></div>
                             <div id="courrier-<c:out value="${status.index}"/>" style="display:block">
         
                                 <div class="line">
                                     <div class="intitule-short"><bean:message key="envoiCourriel.destinataires"/> :</div>
                                     <html:textarea property="destinataires[${status.index}]" titleKey="envoiCourriel.destinataires" styleClass="destinataire" errorStyleClass="destinataire-error" value="${couriel.destinataire }"></html:textarea>
                                     <atexo:infoBulle reference="IB-20" />
                                 </div>
                                 <div class="line">
                                     <div class="intitule-short"><bean:message key="envoiCourriel.objet"/> :</div>
         
                                     <input type="text" name="objets" title="<bean:message key="envoiCourriel.objet"/>" class="objet-courrier" value="${couriel.objet}" />
                                 </div>
                                 <div class="line">
                                     <div class="intitule-short"><bean:message key="envoiCourriel.message"/> :</div>
                                     <textarea name="messages" title="<bean:message key="envoiCourriel.message"/>" class="corps-courrier high-160" rows="4" cols="60"><c:out value="${couriel.message }"/></textarea>
                                 </div>
                                 <div class="line pj">
                                     <div class="intitule-short"><img src="<atexo:href href='images/picto-piece-jointe.gif'/>" alt="" /><bean:message key="envoiCourriel.piecesJointes"/> :</div>
                                     <div class="content-bloc-long">
                                           <c:forEach items="${couriel.listePiecesJointes}" var="pieceJointe" varStatus="statusPieceJointe">
                                               <a href="<c:out value="${pieceJointe.url}"/>"><c:out value="${pieceJointe.nomPieceJointe}"/> <c:out value="${pieceJointe.taillePieceJointeAffichage}"/></a>
                                               <br/>                                    
                                           </c:forEach>
                                     </div>
                                 </div>
                             </div>
                             <!--Fin bloc courrier-->
                             <div class="breaker"></div>
                         </c:forEach>
                    </html:form>
                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>

         </div>
        <!--Fin bloc Liste des courriers electroniques-->
    <div class="spacer"></div>
    <div class="boutons">
        <a href="javascript:window.close();" class="annuler">Annuler</a>
        <a href="javascript:window.opener.validerPopupEnvoiMail(document.forms[0]);" class="valider">Envoyer</a>
    </div>
</div>
<!-- Pour les infos bulles -->
<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infoBulle">
 <div>
 </div>
</div>
</body>
</html>
