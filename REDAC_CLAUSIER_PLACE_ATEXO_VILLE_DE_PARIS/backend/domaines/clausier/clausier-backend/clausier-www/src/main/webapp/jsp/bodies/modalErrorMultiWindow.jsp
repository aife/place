<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 25/09/19
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!--Debut Modal-->
<body onload="javascript:jQuery('#modalErrorMultiWindow').modal({ keyboard: false, backdrop: 'static' })">
    <div class="modal" id="modalErrorMultiWindow">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><spring:message code="popupConfirmation.titre" /></h4>
                </div>
                <div class="modal-body">
                    <div id="error" class="text-center">
                        Désolé, vous ne pouvez pas avoir plusieurs fenêtres de RSEM en même temps.
                    </div>
                    <div class="spacer"></div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery('#modalErrorMultiWindow').modal({ keyboard: false, backdrop: 'static' });
    </script>
</body>
<!--Fin Modal-->
