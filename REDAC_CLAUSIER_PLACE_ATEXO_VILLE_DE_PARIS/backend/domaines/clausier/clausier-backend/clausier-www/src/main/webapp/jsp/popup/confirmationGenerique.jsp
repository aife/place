<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="StrutsBean" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<%
    response.setHeader("Cache-Control","no-cache, must-revalidate, proxy-revalidate");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader ("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><bean:message key="popupConfirmation.titre"/></title>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
    <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css" />
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
    <script type="text/javascript">
        hrefRacine = '<atexo:href href=""/>';
    </script>
</head>


<body onload="popupResize();">

<c:set var="monMessage" value="${param.message}"/>
<c:set var="nomFonctionRetour" value="${param.retour}"/>
<c:if test="${empty nomFonctionRetour}">
    <c:set var="nomFonctionRetour" value="confirmationPopUpGenerique()"/>
</c:if>

<div class="popup-small" id="container">
    <!--Debut Formulaire-->
    <div class="form-bloc">

        <div class="content clearfix">
            <div class="spacer"></div>
            <bean:message name="monMessage" arg1="${param.parametreMessage1}" arg2="${param.parametreMessage2}" arg3="${param.parametreMessage3}" arg4="${param.parametreMessage4}"/>
            <div class="spacer"></div>

            <div class="clearfix">
                <a id="annuler" href="javascript:window.close();" class="btn btn-warning btn-sm"><bean:message key="popupConfirmation.annuler"/></a>
                <a id="valider" href="javascript:window.close();window.opener.<c:out value="${nomFonctionRetour}"/>;" class="btn btn-primary btn-sm pull-right"><bean:message key="popupConfirmation.valider"/></a>
            </div>
        </div>
    </div>
    <!--Fin Formulaire-->
</div>
</body>
</html>
