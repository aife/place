<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp" %>

<%@ taglib prefix="tiles"   uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib prefix="atexo"   uri="AtexoTag"  %>
<%@ taglib prefix="bean"    uri="http://struts.apache.org/tags-bean" %>

<c:set var="contextePassation">
    <atexo:propriete clef="contexte.passation" />
</c:set>
<c:set var="contexteRedaction">
    <atexo:propriete clef="contexte.redaction" />
</c:set>
<c:set var="contexteCommission">
    <atexo:propriete clef="contexte.commission" />
</c:set>
<c:set var="contexteExecution">
    <atexo:propriete clef="contexte.execution" />
</c:set>
<c:set var="contexteInfocentre">
    <atexo:propriete clef="contexte.infocentre" />
</c:set>

<c:set var="plateformeEditeur" value="" />
<atexo:parametrage clef="plateforme.editeur" testEgal="0">
    <c:set var="plateformeEditeur" value="false" />
</atexo:parametrage>
<atexo:parametrage clef="plateforme.editeur" testEgal="1">
    <c:set var="plateformeEditeur" value="true" />
</atexo:parametrage>

<div class="atx-header_nav clearfix">
    <div class="accueil">
        <a class="item clearfix" href="${contextePassation}/accueil.epm">
            <i class="fa fa-home"></i>
            <span class="text"><bean:message bundle="commun" key="header.accueil"/></span>
        </a>
    </div>

    <ul class="navmenu">
        <li class="dropdown">
            <div class="item" name="gestion-procedures">
                <span class="lg"><bean:message bundle="commun" key="header.gestion.procedures"/></span>
                <span class="md"><bean:message bundle="commun" key="header.gestion.procedures.md"/></span>
            </div>
            <div class="dropdown-navmenu">
                <ul class="subnavmenu">
                    <atexo:parametrage clef="activation.demandeAchat" testEgal="true">
                        <li class="subnavmenu-item col-md-12">
                            <span class="level-title">
                                <bean:message bundle="commun" key="menuGauche.demandeAchat"/>
                            </span>
                            <ul class="sub_navmenu-level sub_navmenu-level-a">
                                <atexo:filtreSecurite role="ROLE_creationDemandeAchat" egal="true">
                                    <li id="creationDemandeAchat" class="col-md-4">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" name="demandeAchatCreation" href="${contextePassation}/createDemandeAchat.htm">
                                                <bean:message bundle="commun" key="menuGauche.demandeAchat.creation"/>
                                            </a>
                                        </div>
                                    </li>
                                </atexo:filtreSecurite>

                                <li id="rechercheDemandeAchat" class="grid col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" name="demandeAchatRecherche" href="${contextePassation}/listDemandesAchat.htm">
                                            <bean:message bundle="commun" key="menuGauche.demandeAchat.recherche"/>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </atexo:parametrage>

                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title">
                            <bean:message bundle="commun" key="menuGauche.mes.consultations"/>
                        </span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <li id="rechercheConsultation" class="grid-search">
                                <div class="grid-container">
                                    <div class="search-container atx-component_search clearfix">
                                        <input type="text" name="Recherche" title="Recherche" placeholder="Recherche rapide"
                                               class="input-rechercher atx-component_search-q" value="" id="search-consultation" data-urlService="rechercheRapideServlet?term="
                                               data-urlSubmit="${contextePassation}/rechercherConsultation.htm?texte="/>
                                        <a href="" class="submit-search atx-component_search-ok" alt="Ok" title="Ok">
                                            <span class="fa fa-search"></span>
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <atexo:filtreSecurite role="ROLE_nouvelleConsultation" egal="true">
                                <li id="nouvelleConsultationSimplifie" class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" name="nouvelleConsultation" href="${contextePassation}/creerConsultation.htm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.nouvelle.consultation"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <li id="tousConsulsations" class="col-md-4">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?afficher=0&initialisationSession=true">
                                        <bean:message bundle="commun" key="menuGaucheLayout.recherche.mesConsultation"/>
                                    </a>
                                </div>
                            </li>

                            <li id="rechercheAvancee" class="col-md-4">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?initialisationSession=true">
                                        <bean:message bundle="commun" key="menuGaucheLayout.recherche.consultation"/>
                                    </a>
                                </div>
                            </li>

                            <atexo:filtreSecurite role="ROLE_declarerClos" egal="true">
                                <li id="declarerClos" class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?action=DECLARER_CLOS&initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGaucheLayout.declarerClos"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_dupliquerConsultation" egal="true">
                                <li id="dupliquerConsultation" class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?action=DUPLIQUER&initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGaucheLayout.dupliquerConsultation"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_supprimerConsultationOuAvenant" egal="true">
                                <li id="supprimerConsultation" class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?action=SUPPRIMER&initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGaucheLayout.recherche.supprimerConsultation"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>
                        </ul>
                    </li>

                    <li class="subnavmenu-item col-md-12">
                        <c:set var="menuGaucheDsp" scope="page">
                            <bean:message key="menuGauche.mes.dsp"/>
                        </c:set>
                        <span class="level-title">
                            <c:choose>
                                <c:when test='${not (fn:startsWith(menuGaucheDsp, "??"))}'>
                                    ${menuGaucheDsp}
                                </c:when>
                                <c:otherwise>
                                    <bean:message bundle="commun" key="menuGauche.mes.dsp"/>
                                </c:otherwise>
                            </c:choose>
                        </span>

                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <atexo:filtreSecurite role="ROLE_nouvelleDsp" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <c:set var="menuGaucheNouvelleDsp" scope="page">
                                            <bean:message key="menuGauche.nouvelle.dsp"/>
                                        </c:set>
                                        <a class="link hvr-fade" href="${contextePassation}/nouvelleDsp.epm?type=DSP&initialisationSession=true">
                                            <c:choose>
                                                <c:when test='${not (fn:startsWith(menuGaucheNouvelleDsp, "??"))}'>
                                                    ${menuGaucheNouvelleDsp}
                                                </c:when>
                                                <c:otherwise>
                                                    <bean:message bundle="commun" key="menuGauche.nouvelle.dsp"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <li class="col-md-4">
                                <div class="grid-container">
                                    <c:set var="menuGaucheToutesDsp" scope="page">
                                        <bean:message key="menuGauche.toutes.dsp"/>
                                    </c:set>
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?afficher=0&type=DSP&initialisationSession=true">
                                        <c:choose>
                                            <c:when test='${not (fn:startsWith(menuGaucheToutesDsp, "??"))}'>
                                                ${menuGaucheToutesDsp}
                                            </c:when>
                                            <c:otherwise>
                                                <bean:message bundle="commun" key="menuGauche.toutes.dsp"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </a>
                                </div>
                            </li>

                            <li class="col-md-4">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?type=DSP&initialisationSession=true">
                                        <bean:message bundle="commun" key="menuGauche.recherche.avancee.dsp"/>
                                    </a>
                                </div>
                            </li>

                            <atexo:filtreSecurite role="ROLE_declarerClosDSP" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?afficher=0&action=DECLARER_CLOS&type=DSP&initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGaucheLayout.declarerClos"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_dupliquerDSP" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <c:set var="menuGaucheDupliquerDsp" scope="page">
                                            <bean:message key="menuGaucheLayout.dupliquerDsp"/>
                                        </c:set>
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?afficher=0&action=DUPLIQUER&type=DSP&initialisationSession=true">
                                            <c:choose>
                                                <c:when test='${not (fn:startsWith(menuGaucheDupliquerDsp, "??"))}'>
                                                    ${menuGaucheDupliquerDsp}
                                                </c:when>
                                                <c:otherwise>
                                                    <bean:message bundle="commun" key="menuGaucheLayout.dupliquerDsp"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_supprimerConsultationOuAvenant" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <c:set var="menuGaucheSupprimerDsp" scope="page">
                                            <bean:message key="menuGaucheLayout.recherche.supprimerDSP"/>
                                        </c:set>
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?afficher=0&action=SUPPRIMER&type=DSP&initialisationSession=true">
                                            <c:choose>
                                                <c:when test='${not (fn:startsWith(menuGaucheSupprimerDsp, "??"))}'>
                                                    ${menuGaucheSupprimerDsp}
                                                </c:when>
                                                <c:otherwise>
                                                    <bean:message bundle="commun" key="menuGaucheLayout.recherche.supprimerDSP"/>
                                                </c:otherwise>
                                            </c:choose>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>
                        </ul>
                    </li>

                    <li id="faireSuite" class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.faire.suite"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <atexo:filtreSecurite role="ROLE_faireSuite" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherConsultation.htm?action=FAIRE_SUITE&initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.infructueux.ou.sans.suite"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_accordCadre" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercheAccordCadreAttribue.epm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.accord.cadre.notifie"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_accordCadre" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercherAccordCadre.epm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.accord.cadre.non.notifie"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>
                        </ul>
                    </li>

                    <atexo:filtreSecurite role="ROLE_consulterMessageDemat" egal="true">
                        <li class="subnavmenu-item col-md-12">
                            <span class="level-title"><bean:message bundle="commun" key="menuGauche.echange.demat"/></span>
                            <ul class="sub_navmenu-level sub_navmenu-level-a">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contextePassation}/rechercheMessagesEchangeDemat.epm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.echange.demat.consulter"/>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </atexo:filtreSecurite>
                </ul>
            </div>
        </li>

        <li class="dropdown">
            <c:set var="administrationEditeur" value="false"/>
            <atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
                <c:set var="administrationEditeur" value="true"/>
            </atexo:filtreSecurite>

            <atexo:filtreSecurite role="ROLE_administrerCanevas" egal="true">
                <c:set var="administrationEditeur" value="true"/>
            </atexo:filtreSecurite>

            <atexo:filtreSecurite egal="true" role="ROLE_REDACTION">
                <div class="item" name="redaction-pieces">
                    <span class="lg"><bean:message bundle="commun" key="header.redaction.pieces"/></span>
                    <span class="md"><bean:message bundle="commun" key="header.redaction.pieces.md"/></span>
                </div>
            </atexo:filtreSecurite>

            <div class="dropdown-navmenu">
                <ul class="subnavmenu">
                    <c:if test="${administrationEditeur == 'true' }">
                        <li class="subnavmenu-item col-md-12">
                            <span class="level-title"><bean:message bundle="commun" key="menuGauche.administrationEditeur"/></span>
                            <ul class="sub_navmenu-level sub_navmenu-level-a">
                                <c:if test="${empty plateformeEditeur or plateformeEditeur}">
                                    <atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
                                        <li class="col-md-12">
                                            <div class="grid-container">
                                                <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.clausesEditeur"/></span>
                                                <ul class="sub_navmenu-level sub_navmenu-level-b">
                                                    <li class="col-md-4">
                                                        <div class="grid-container">
                                                            <a class="link hvr-fade" href="${contexteRedaction}/listClauses.htm?editeur=yes">
                                                                <bean:message bundle="commun" key="menuGauche.administration.clausesEditeur.rechercher"/>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="col-md-4">
                                                        <div class="grid-container">
                                                            <a class="link hvr-fade" href="${contexteRedaction}/CreationClauseEditeurInit.epm?initialisationSession=true">
                                                                <bean:message bundle="commun" key="menuGauche.administration.clausesEditeur.creer"/>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="col-md-4">
                                                        <div class="grid-container">
                                                            <a class="link hvr-fade" href="javascript:popUpSetSize('/epm.redaction/exporterClauses.htm?editeur=yes','400','190','yes');">
                                                                <bean:message bundle="commun" key="menuGauche.administration.canevas.exporter"/>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>

                                    <atexo:filtreSecurite role="ROLE_canevas" egal="true">
                                        <li class="col-md-12">
                                            <div class="grid-container">
                                                <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.canevasEditeur"/></span>
                                                <ul class="sub_navmenu-level sub_navmenu-level-b">
                                                    <li class="col-md-4">
                                                        <div class="grid-container">
                                                            <a class="link hvr-fade" href="${contexteRedaction}/listCanevas.htm?editeur=yes">
                                                                <bean:message bundle="commun" key="menuGauche.administration.canevasEditeur.rechercher"/>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="col-md-4">
                                                        <div class="grid-container">
                                                            <a class="link hvr-fade" href="${contexteRedaction}/CreationCanevas1EditeurInit.epm?initialisationSession=true">
                                                                <bean:message bundle="commun" key="menuGauche.administration.canevasEditeur.creer"/>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>
                                </c:if>
                                <atexo:filtreSecurite role="ROLE_administrerClauses" egal="true">
                                    <li class="col-md-12">
                                        <div class="grid-container">
                                            <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.publicationClausier"/></span>
                                            <ul class="sub_navmenu-level sub_navmenu-level-b">
                                                <c:if test="${empty plateformeEditeur or plateformeEditeur}">
                                                    <atexo:filtreSecurite role="ROLE_publicationVersionClauseEditeur" egal="true">
                                                        <li class="col-md-4">
                                                            <div class="grid-container">
                                                                <a class="link hvr-fade" href="${contexteRedaction}/createPublicationClausier.htm">
                                                                    <bean:message bundle="commun" key="menuGauche.administration.publicationClausier.creerVersion"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    </atexo:filtreSecurite>
                                                </c:if>
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteRedaction}/listPublicationsClausier.htm">
                                                            <bean:message bundle="commun" key="menuGauche.administration.publicationClausier.historique"/>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </atexo:filtreSecurite>

                                <atexo:filtreSecurite role="ROLE_gabaritInterministeriel" egal="true">
                                    <li class="col-md-12">
                                        <div class="grid-container">
                                            <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.gabarit.interministeriel"/></span>
                                            <ul class="sub_navmenu-level sub_navmenu-level-b">
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteRedaction}/gabaritInterministerielInit.epm?initialisationSession=true">
                                                            <bean:message bundle="commun" key="menuGauche.administration.gabarit.modifier"/>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </atexo:filtreSecurite>
                            </ul>
                        </li>
                    </c:if>

                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <atexo:filtreSecurite role="ROLE_clause" egal="true">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.clauses"/></span>
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/listClauses.htm?editeur=no">
                                                        <bean:message bundle="commun" key="menuGauche.administration.clauses.rechercher"/>
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/CreationClauseInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.administration.clauses.creer"/>
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="javascript:popUpSetSize('/epm.redaction/exporterClauses.htm?editeur=no','400','190','yes');">
                                                        <bean:message bundle="commun" key="menuGauche.administration.canevas.exporter"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_canevas" egal="true">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.canevas"/></span>
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/listCanevas.htm?editeur=no">
                                                        <bean:message bundle="commun" key="menuGauche.administration.canevas.rechercher"/>
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/CreationCanevas1Init.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.administration.canevas.creer"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_modifierLogoDocument" egal="true">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <span class="level-title">Le logo</span>
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <li class="col-md-12">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/modifierLogoInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.administration.modifier.logo"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <atexo:filtreSecurite role="ROLE_gabaritMinisteriel" egal="true">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.administration.gabarit.interministeriel"/></span>
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteRedaction}/gabaritMinisterielInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.administration.gabarit.modifier"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>
                        </ul>
                    </li>

                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.redaction"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <atexo:filtreSecurite role="ROLE_document" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contexteRedaction}/RechercherDocumentInit.epm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.redaction.gerer.document"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>
                            <c:if test="${empty preferences}">
                                <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                    <li class="col-md-4">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=direction&initialisationSession=true" onclick="resetSsmenu(this);">
                                                <bean:message bundle="commun" key="menuGauche.administration.preferences.direction"/>
                                            </a>
                                        </div>
                                    </li>
                                </atexo:filtreSecurite>
                                <atexo:filtreSecurite role="ROLE_document" egal="true">
                                    <li class="col-md-4">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=agent&initialisationSession=true" onclick="resetSsmenu(this);">
                                                <bean:message bundle="commun" key="menuGauche.administration.preferences.agent"/>
                                            </a>
                                        </div>
                                    </li>
                                </atexo:filtreSecurite>
                            </c:if>
                            <c:if test="${not empty preferences}">
                                <c:if test="${preferences eq direction}">
                                    <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                        <li class="col-md-4 on">
                                            <div class="grid-container">
                                                <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=direction&initialisationSession=true" onclick="resetSsmenu(this);">
                                                    <bean:message bundle="commun" key="menuGauche.administration.preferences.direction"/>
                                                </a>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>
                                    <atexo:filtreSecurite role="ROLE_document" egal="true">
                                        <li class="col-md-4 off">
                                            <div class="grid-container">
                                                <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=agent&initialisationSession=true" onclick="resetSsmenu(this);">
                                                    <bean:message bundle="commun" key="menuGauche.administration.preferences.agent"/>
                                                </a>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>
                                </c:if>
                                <c:if test="${preferences ne direction}">
                                    <atexo:filtreSecurite role="ROLE_direction_service" egal="true">
                                        <li class="col-md-4 off">
                                            <div class="grid-container">
                                                <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=direction&initialisationSession=true" onclick="resetSsmenu(this);">
                                                    <bean:message bundle="commun" key="menuGauche.administration.preferences.direction"/>
                                                </a>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>
                                    <atexo:filtreSecurite role="ROLE_document" egal="true">
                                        <li class="col-md-4 on">
                                            <div class="grid-container">
                                                <a class="link hvr-fade" href="${contexteRedaction}/ParametrageClauseInit.epm?preferences=agent&initialisationSession=true" onclick="resetSsmenu(this);">
                                                    <bean:message bundle="commun" key="menuGauche.administration.preferences.agent"/>
                                                </a>
                                            </div>
                                        </li>
                                    </atexo:filtreSecurite>
                                </c:if>
                            </c:if>

                            <atexo:filtreSecurite role="ROLE_gabaritDirection" egal="true">
                                <li class="col-md-4">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contexteRedaction}/gabaritDirectionInit.epm?initialisationSession=true">
                                            <bean:message bundle="commun"
                                                          key="menuGauche.administration.gabaritDirection"/>
                                        </a>
                                    </div>
                                </li>
                            </atexo:filtreSecurite>

                            <li class="col-md-4">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contexteRedaction}/gabaritAgentInit.epm?initialisationSession=true">
                                        <bean:message bundle="commun" key="menuGauche.administration.gabaritAgent"/>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </li>

        <li class="dropdown">
            <div class="item" name="gestion-commmisions">
                <span class="lg"><bean:message bundle="commun" key="header.gestion.commissions"/></span>
                <span class="md"><bean:message bundle="commun" key="header.gestion.commissions.md"/></span>
            </div>

            <div class="dropdown-navmenu">
                <ul class="subnavmenu">
                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.organisation"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <li class="col-md-12 mr">
                                <div class="grid-container">
                                    <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.instances"/></span>
                                    <atexo:filtreSecurite role="ROLE_instanceCIM,ROLE_instanceCAO" egal="true">
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteCommission}/creerInstanceInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.commission.creerInstance"/>
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteCommission}/rechercheInstanceInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.commission.modifierInstance"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </atexo:filtreSecurite>
                                </div>
                            </li>

                            <li class="col-md-12">
                                <div class="grid-container">
                                    <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.reunions"/></span>
                                    <atexo:filtreSecurite role="ROLE_instanceCIM,ROLE_instanceCAO" egal="true">
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <atexo:filtreSecurite role="ROLE_reunionCIM,ROLE_reunionCAO" egal="true">
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteCommission}/creerReunionInit.epm?initialisationSession=true">
                                                            <bean:message bundle="commun" key="menuGauche.commission.creerReunion"/>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteCommission}/modifierReunionInit.epm?initialisationSession=true">
                                                            <bean:message bundle="commun" key="menuGauche.commission.modifierReunion"/>
                                                        </a>
                                                    </div>
                                                </li>
                                            </atexo:filtreSecurite>
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteCommission}/calendrierReunionInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.commission.calendrierReunion"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </atexo:filtreSecurite>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.ordreDuJour"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <li class="col-md-12">
                                <div class="grid-container">
                                    <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.ordreDuJour.preparation"/></span>
                                    <ul class="sub_navmenu-level sub_navmenu-level-b">
                                        <atexo:filtreSecurite role="ROLE_preInscrireConsultationCIM,ROLE_preinscriptionRecommandationCandidaturesAdmissiblesPA,ROLE_preinscriptionRecommandationAttributairesPressentisEnveloppeUnique,ROLE_preinscriptionSuiviEchange,ROLE_preinscriptionRecommandationDspPA1,ROLE_preinscriptionRecommandationAttributairesPressentisPA,ROLE_preinscriptionRecommandationAttributairesPressentis,ROLE_preinscriptionCandidature,ROLE_preinscriptionOffre, ROLE_preinscriptionOffreFinale,ROLE_attribution,ROLE_preinscriptionAvisCommission" egal="true">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteCommission}/preinscrireInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.commission.preinscrireConsultation"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </atexo:filtreSecurite>
                                        <atexo:filtreSecurite role="ROLE_definirOrdreDuJourCIM,ROLE_definirOrdreDuJourCAO" egal="true">
                                            <li class="col-md-4">
                                                <div class="grid-container">
                                                    <a class="link hvr-fade" href="${contexteCommission}/definirModifierInit.epm?initialisationSession=true">
                                                        <bean:message bundle="commun" key="menuGauche.commission.definirModifier"/>
                                                    </a>
                                                </div>
                                            </li>
                                        </atexo:filtreSecurite>
                                    </ul>
                                </div>
                            </li>

                            <li class="col-md-12">
                                <div class="grid-container">
                                    <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.envoiDossier"/></span>
                                    <atexo:filtreSecurite role="ROLE_instanceCIM,ROLE_instanceCAO" egal="true">
                                        <ul class="sub_navmenu-level sub_navmenu-level-b">
                                            <atexo:filtreSecurite role="ROLE_envoyerDossierCIM" egal="true">
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteCommission}/rechercheReunionEnvoisDossierInit.epm?initialisationSession=true">
                                                            <bean:message bundle="commun" key="menuGauche.commission.envoiDossier.instancesDirection"/>
                                                        </a>
                                                    </div>
                                                </li>
                                            </atexo:filtreSecurite>
                                            <atexo:filtreSecurite role="ROLE_envoiDossier,ROLE_envoyerDossierCAO" egal="true">
                                                <li class="col-md-4">
                                                    <div class="grid-container">
                                                        <a class="link hvr-fade" href="${contexteCommission}/gererEnvoisInit.epm?initialisationSession=true">
                                                            <bean:message bundle="commun" key="menuGauche.commission.envoiDossier.instancesTransversales"/>
                                                        </a>
                                                    </div>
                                                </li>
                                            </atexo:filtreSecurite>
                                        </ul>
                                    </atexo:filtreSecurite>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <atexo:filtreSecurite role="ROLE_quorumCIM,ROLE_quorumCAO" egal="true">
                        <li class="subnavmenu-item col-md-6 mr">
                            <div class="grid-container">
                                <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.quorum"/></span>
                                <ul class="sub_navmenu-level sub_navmenu-level-a">
                                    <li class="col-md-12">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteCommission}/rechercheQuorumInit.epm?initialisationSession=true">
                                                <bean:message bundle="commun" key="menuGauche.commission.quorum.gerer"/>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </atexo:filtreSecurite>

                    <li class="subnavmenu-item col-md-6 ml">
                        <div class="grid-container">
                            <span class="level-title"><bean:message bundle="commun"
                                                                    key="menuGauche.commission.decisions"/></span>
                            <ul class="sub_navmenu-level sub_navmenu-level-a">
                                <li class="col-md-12">
                                    <div class="grid-container">
                                        <a class="link hvr-fade" href="${contexteCommission}/selectionnerReunionInit.epm?initialisationSession=true">
                                            <bean:message bundle="commun" key="menuGauche.commission.decisions.enregistrer"/>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <atexo:filtreSecurite role="ROLE_commissionDeliberations" egal="true">
                        <li class="subnavmenu-item  col-md-6 mr">
                            <div class="grid-container">
                                <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.deliberations"/></span>
                                <ul class="sub_navmenu-level sub_navmenu-level-a">
                                    <li class="col-md-12">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteCommission}/deliberationsInit.epm?initialisationSession=true">
                                                <bean:message bundle="commun" key="menuGauche.commission.deliberations.gerer"/>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </atexo:filtreSecurite>

                    <atexo:filtreSecurite role="ROLE_accesAuxRapportsMisADisposition" egal="true">
                        <li class="subnavmenu-item  col-md-6 ml">
                            <div class="grid-container">
                                <span class="level-title"><bean:message bundle="commun" key="menuGauche.commission.dossiers"/></span>
                                <ul class="sub_navmenu-level sub_navmenu-level-a">
                                    <li class="col-md-12">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteCommission}/rechercheSeancesTelechargementDossiersInit.epm?initialisationSession=true">
                                                <bean:message bundle="commun" key="menuGauche.commission.dossiers.telechargement"/>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </atexo:filtreSecurite>
                </ul>
            </div>
        </li>

        <li class="dropdown">
            <div class="item" name="gestion-execution">
                <span class="lg"><bean:message bundle="commun" key="header.gestion.execution"/></span>
                <span class="md"><bean:message bundle="commun" key="header.gestion.execution.md"/></span>
            </div>

            <div class="dropdown-navmenu">
                <ul class="subnavmenu">
                    <atexo:parametrage clef="activation.module.execution" testEgal="true">
                        <li class="subnavmenu-item col-md-12">
                            <span class="level-title"><bean:message bundle="commun" key="menuGauche.execution.contrats"/></span>
                            <ul class="sub_navmenu-level sub_navmenu-level-a">
                                    <%--
                                    <li class="col-md-12 col-md-search">
                                        <div class="search-container atx-component_search clearfix">
                                            <input type="text" name="Recherche" title="Recherche" placeholder="Recherche rapide" class="input-rechercher atx-component_search-q" value="" id="search-contarts" data-urlService="${contextePassation}/rechercheRapideServlet?term=" data-urlSubmit="rechercherContarts.epm?motsClefs="/>
                                            <a href="" class="submit-search atx-component_search-ok" alt="Ok" title="Ok"><span class="fa fa-search"></span></a>
                                        </div>
                                    </li>
                                    --%>
                                <atexo:feature profile="base-fournisseur" activated="true">
                                    <li class="col-md-6">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" onclick="window.open('baseFournisseur.htm?contrats=SERVICE')" href="#">
                                                <bean:message bundle="commun" key="menuGauche.execution.suiviContrats"/>
                                            </a>
                                        </div>
                                    </li>
                                </atexo:feature>

                                <!-- TODO afficher pour les clients qui n'ont pas le nouveau exec -->
                                <atexo:feature profile="base-fournisseur" activated="false">
                                    <li class="col-md-6">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteExecution}/main.epm?aiguillage=rechercheTousMesContrats&initialisationSession=true">
                                                <bean:message bundle="commun" key="menuGauche.execution.tousMesContrats"/>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="col-md-6">
                                        <div class="grid-container">
                                            <a class="link hvr-fade" href="${contexteExecution}/main.epm?aiguillage=rechercheAvancee&initialisationSession=true">
                                                <bean:message bundle="commun" key="menuGauche.execution.rechercheAavancee"/>
                                            </a>
                                        </div>
                                    </li>
                                </atexo:feature>
                            </ul>
                        </li>
                    </atexo:parametrage>

                    <li class="subnavmenu-item col-md-12">
                        <span class="level-title"><bean:message bundle="commun" key="menuGauche.execution.avenants"/></span>
                        <ul class="sub_navmenu-level sub_navmenu-level-a">
                            <li class="col-md-6">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/nouvelAvenant.epm?initialisationSession=true">
                                        <bean:message bundle="commun" key="menuGauche.execution.avenants.creer"/>
                                    </a>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/supprimerAvenant.epm?initialisationSession=true&resultat=recherchePourSupprimer">
                                        <bean:message bundle="commun" key="menuGauche.execution.avenants.supprimer"/>
                                    </a>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherAvenant.epm?initialisationSession=true&resultat=avenants">
                                        <bean:message bundle="commun" key="menuGauche.execution.avenants.rechercher"/>
                                    </a>
                                </div>
                            </li>
                            <li class="col-md-6">
                                <div class="grid-container">
                                    <a class="link hvr-fade" href="${contextePassation}/rechercherMesAvenant.epm?initialisationSession=true&resultat=tousMesAvenants#">
                                        <bean:message bundle="commun" key="menuGauche.execution.avenants.mesAvenants"/>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </li>

        <li>
            <a href="${contexteInfocentre}/main.epm" class="item <tiles:getAsString name="lienInfoCentre"/>" name="statistiques">
                <span class="lg"><bean:message bundle="commun" key="header.base.infocentre"/></span>
                <span class="md"><bean:message bundle="commun" key="header.base.infocentre"/></span>
            </a>
        </li>
    </ul>
</div>
