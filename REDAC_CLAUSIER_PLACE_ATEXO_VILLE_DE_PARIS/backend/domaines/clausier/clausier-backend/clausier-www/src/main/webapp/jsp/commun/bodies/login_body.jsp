<%@ page language="java" %>
<%@ taglib uri="StrutsHtml" prefix="html" %>
<%@ taglib uri="StrutsBean" prefix="bean" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!--Debut partie centrale-->
<div id="middle">
	<div class="login">
		<img src="<atexo:href href='images/login-visuel.jpg'/>" class="login-visuel" alt="Identification" title="Identification" />
		<div class="login-box">
			<div class="top"></div>
			<div class="content">
			<html:form action="login">
				<html:hidden property="formprocessed" value="true"/>
				
				<div class="error">
				
				<html:errors/>
				
				<html:messages id="msg" message="true">
					<bean:write name="msg"/><br>
				</html:messages>
				
				<!--<strong>Mot de passe invalide, veuillez recommencer</strong>-->
				</div>

				<span class="intitule"><bean:message bundle="commun" key="loginForm.username"/> : </span><html:text property="username" titleKey="loginForm.username"/>
				<span class="intitule"><bean:message bundle="commun" key="loginForm.password"/> : </span><html:password property="password" titleKey="loginForm.password"/>
				
				
				<a href="#" class="valider" onClick="document.forms.loginForm.submit()"><bean:message bundle="commun" key="loginForm.valider"/></a>
				<span class="oubli-pass"><a href="#"><bean:message bundle="commun" key="loginForm.forgotpassword"/></a></span>
				
				<div class="breaker"></div>	
				</html:form>
			</div>
			<div class="bottom"></div>
		</div>
	</div>
</div>
<!--Fin partie centrale-->