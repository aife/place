<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page errorPage="/jsp/bodies/pageErreur.jsp"%>

<%@ taglib prefix="bean"  uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="c"     uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="atexo" uri="AtexoTag" %>

<%
    response.setHeader("Cache-Control", "no-cache, must-revalidate, proxy-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><bean:message key="login.titreAccueil"/></title>

    <link href="front_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/calendrier.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css"/>
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.link.reset').click(function () {
                jQuery('#' + jQuery(this).data('for')).val('');
                return false;
            })
        });
    </script>
    <script type="text/javascript" src="js/scripts-commun.js"></script>
    <script type="text/javascript">
        function valider() {
            username = document.getElementById("username").value.trim();
            if ((username == null) || (username == '')) {
                document.f.j_username.value = '';
                document.f.j_username.focus();
                return false;
            }
            return true;
        }
    </script>
</head>
<body onload='document.f.j_username.focus();' class="atx-page-login">

<div class="atx-header">
    <div class="atx-header_top">
        <div class="clearfix">
            <div class="pull-right">
                <div class="subnav clearfix">
                    <div class="subnav_item date">
                        <script type="text/javascript">writeDate();</script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Debut partie centrale-->
<div class="atx-body">
    <div class="login">
        <div class="login-container">
            <div class="form-bloc">
                <div class="login_container">

                    <c:set var="parCertificat"><atexo:propriete
                            clef="application.gestionUtilisateurCertificat"/></c:set>

                    <c:if test="${parCertificat eq 'false'}">
                        <form id="f" name='f' class="login-form" action='j_spring_security_check' method='post' onSubmit="return valider();">
                            <input  type="hidden" id="login" name="login" value="${login}" />
                            <input type="hidden" id="token" name="token" value="${token}"/>
                            <div class="login-form_top">
                                <h1 class="title"><bean:message key="login.titre"/></h1>
                            </div>
                            <div class="login-form_body">


                                <div class="line">
                                    <label class="label" for="username"><bean:message key="login.login"/> :</label>
                                    <input class="input input-text" type="text" id="username" name="j_username"
                                           title="Identifiant" <c:if test="${login != null}">value="${login}"</c:if>/>
                                    <a href="#" name="button" title="Annuler identifiant" class="link reset"
                                       value="Annuler identifiant" data-for="username"><i class="fa fa-times"></i></a>
                                </div>

                                <div class="line">
                                    <label class="label" for="password"><bean:message key="login.motDePasse"/> :</label>
                                    <input class="input input-password" id="password" type="password" name="j_password"
                                           title="Mot de passe"/>
                                    <a href="#" type="button" name="reset" title="Annuler mot de passe"
                                       class="link reset" value="Annuler mot de passe" data-for="password"><i
                                            class="fa fa-times"></i></a>
                                </div>

                                <div style="display:none;"><input type="checkbox" name='_spring_security_remember_me'
                                                                  checked="checked"/></div>

                                <c:if test="${not empty erreurKey}">
                                    <div class="line">
                                        <div class="error">
                                            <bean:message key="${erreurKey}"/>
                                        </div>
                                    </div>
                                </c:if>

                                <div class="button-group">
                                    <input type="submit" title="Valider" class="btn submit" value="Valider"/>
                                </div>


                            </div>
                        </form>
                    </c:if>
                    <c:if test='${parCertificat eq "true"}'>
                        <form id="f" name='f'
                              action='.<atexo:propriete clef="formProcessingFilter.targetUrl"/>'
                              method='post'>
                            <div class="title">Identification par certificat</div>
                            <div class="boutons">
                                <input type="submit"
                                       value="<bean:message key='login.certificat.button' />"
                                       class="login-certificat">
                            </div>
                        </form>
                    </c:if>

                </div>

            </div>
        </div>
    </div>
</div>

<div id="atx-footer" class="atx-footer">
    <div class="atx-footer-container">
        <div class="clearfix">
            <div class="pull-left">

            </div>
            <div class="pull-right">
                <a href="#" title="<bean:message key="login.conseilUtilisation"/>">
                    <bean:message key="login.conseilUtilisation"/>
                </a>

                <a href="#" title="<bean:message key="login.mentionLegale"/>">
                    <bean:message key="login.mentionLegale"/>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Pour les infos bulles -->
</body>
</html>
