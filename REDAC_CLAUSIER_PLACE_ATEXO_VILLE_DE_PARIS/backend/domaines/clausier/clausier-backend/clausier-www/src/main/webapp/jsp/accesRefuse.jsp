<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="StrutsLogic" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="StrutsHtml" prefix="html"%>
<%@ page isErrorPage="true"%>
<%@ taglib uri="StrutsBean" prefix="bean"%>
<div class="main-part">
	<div align="center">
		<br>
		<br>
		<br>
		<br>
		<br>
		<logic:messagesPresent>
			<html:errors />
		</logic:messagesPresent>
		<logic:messagesNotPresent>
			<bean:message key="erreur.nonAutorise" />
		</logic:messagesNotPresent>
	</div>
</div>
