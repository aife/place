<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!--Debut modal attestations -->
<div id="modal_attestations">
    <h2 class="title"><spring:message code="attestationEntreprise.title"/> ${nomLigneLigneEntreprise}</h2>
    <div class="form-saisie">
        <div class="form-bloc">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <h3><spring:message code="attestationEntreprise.sgmap"/></h3>
                <div class="results-list">
                    <table class="tableau-custom" summary="Attestations" id="tableau_historique" >
                        <caption><spring:message code="attestationEntreprise.historique"/></caption>
                        <thead>
                        <tr>
                            <th class="top" colspan="4"><img src="<atexo:href href='images/table-results-top-left.gif'/>" alt="" title="" class="left" /><img src="<atexo:href href='images/table-results-top-right.gif'/>" alt="" title="" class="right" /></th>
                        </tr>
                        <tr>
                            <th scope="col"><spring:message code="attestationEntreprise.nom"/></th>
                            <th class="actions center" scope="col"><spring:message code="attestationEntreprise.telecharger"/></th>
                        </tr>
                        </thead>

                        <c:forEach var="attestationEntreprise" items="${attestationsEntreprise}" varStatus="status">
                            <tr class="${status.index%2 eq 0 ? 'on' : 'off'}" id="attestations_${idLigneEntreprise}">
                                <td class="${attestationEntreprise.status.titre}">
                                    <c:out value="${attestationEntreprise.type.libelle}"/>
                                </td>
                                <td class="center">
                                    <c:if test="${attestationEntreprise.lienTelechargement != null }">
                                        <a href="${attestationEntreprise.lienTelechargement}" class="btn-action"><img src="<atexo:href href='images/bouton-telecharger.gif'/>" alt="Télécharger" /></a>
                                    </c:if>
                                    &nbsp;
                                </td>
                            </tr>
                        </c:forEach>

                    </table>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="boutons">
                <div id="annulerId">
                    <div><a href="javascript:void(0)" class="annuler"><spring:message code="common.fermer"/></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Fin modal attestations -->


