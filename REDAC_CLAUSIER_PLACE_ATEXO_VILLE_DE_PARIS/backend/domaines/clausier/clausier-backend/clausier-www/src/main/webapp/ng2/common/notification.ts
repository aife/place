

export function messageNotification(message : string) {	
	 //$.growl.notice({ title: "", message: message });
}

export function warningNotification(alert : string) {	
	 //$.growl.warning({ title: "", message: alert });
}


export function serverErrorNotification(error : any) {		
	if( error != null && error.responseJSON != null ) {	
		//$.growl.error({ title: "Erreur", message: error.responseJSON.message });
	} else {
		genericErrorNotification();
	}	
}

export function errorNotification(error : string) {	
	 //$.growl.error({ title: "Erreur", message: error });
}

export function genericErrorNotification() {	
	 //$.growl.error({ title: "Erreur", message: "Une erreur est survenue" });
}