import {CORE_DIRECTIVES} from 'angular2/common';
import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output
} from 'angular2/core';

export interface ChosenOption {
    value: string | number;
    label: string | number;
    group?: string;
    object?: any;
}

export interface ChosenOptionsGroup {
    value: string | number;
    label: string | number;
}

@Component({
    selector: 'ltexec-chosen',
    template: `

<select  [id]="identifier" class="chosen-select" [multiple]="multiple"  >


	<template [ngIf]="options != null">

		<template [ngIf]="optionsGroups == null">

			<option *ngFor="#option of options" [value]="option.value">{{option.label}}</option>

	    </template>

		<template [ngIf]="optionsGroups != null">

            <template ngFor #option [ngForOf]="options" #i="index">

                <template [ngIf]="option.group == null">

        			<option [value]="option.value">{{option.label}}</option>

			    </template>

			</template>

			<optgroup *ngFor="#group of optionsGroups" label="{{group.label}}">

            <template ngFor #option [ngForOf]="options" #i="index">

                <template [ngIf]="option.group == group.value">

			        <option [value]="option.value">{{option.label}}</option>

			    </template>

			</template>

		    </optgroup>

	</template>

	</template>

</select>

    `,
    directives: [CORE_DIRECTIVES]
})
export class ChosenComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

    static identifierIdx:number = 0;

    @Input() identifier:string;

    uuid:string;

    @Output() change:EventEmitter<any>;
    @Input('disable-search') disableSearch:boolean = true;
    @Input('no-results-text')noResulText:string;
    @Input('placeholder-text-multiple') placeholderTextMultiple:string;
    @Input('placeholder-text-single') placeholderTextSingle : string;
    @Input('search-contains') searchContains:boolean = false;
    @Input() freestyle:boolean = false;
    @Input('freestyle-value-prefix') freestyleValuePrefix:string;
    @Input('freestyle-value-validator') freestyleValueValidator:any; // function
    @Input() freestyleGroup:string;
    @Input() multiple:boolean;
    @Input() model:Object;
    @Input('model-property') modelProperty:string;
    @Input() options:Array<ChosenOption>;
    @Input('options-groups') optionsGroups:Array<ChosenOptionsGroup>;

    chosenConfig  : ChosenOptions = {};
    groups:Array<ChosenOptionsGroup>;
    elementRef:ElementRef;
    selectElement: JQuery;
    updateChosen:boolean = false;

    constructor(@Inject(ElementRef) elementRef:ElementRef) {
        this.elementRef = elementRef;
        ChosenComponent.identifierIdx++;
        this.identifier = `chosen_select_${ChosenComponent.identifierIdx}`;
        this.change = new EventEmitter();
    }

    ngOnInit() {
        var el:any = this.elementRef.nativeElement;

        this.selectElement = jQuery(el).find(`#${this.identifier}`);

        if (this.disableSearch != null) {
            this.chosenConfig.disable_search = this.disableSearch;
        }

        if (this.noResulText != null) {
            this.chosenConfig.no_results_text = this.noResulText;
        }

        if (this.placeholderTextMultiple != null) {
            this.chosenConfig.placeholder_text_multiple = this.placeholderTextMultiple;
        }

        if (this.placeholderTextSingle != null) {
            this.chosenConfig.placeholder_text_single = this.placeholderTextSingle;
        }

        if (this.searchContains != null) {
            this.chosenConfig.search_contains = this.searchContains;
        }
        this.selectElement.trigger("chosen:updated");
    }

    ngOnChanges(changes) {
        if (this.model != null && this.modelProperty != null) {
            if (this.selectElement != null) {
                this.selectElement.val(this.model[this.modelProperty]);
                this.selectElement.trigger("chosen:updated");

            }
        }
        this.updateChosen = true;
        if (this.selectElement != null) {
            this.selectElement.trigger("chosen:updated");
        }
    }

    ngAfterViewInit() {

        var el:any = this.elementRef.nativeElement;
        this.selectElement = jQuery(el).find(`#${this.identifier}`);


        if (this.model != null && this.modelProperty != null && this.model[this.modelProperty] != null) {
            this.selectElement.val(this.model[this.modelProperty]);
        }

        this.selectElement.chosen(this.chosenConfig);

        let identifier_ = this.identifier;
        let freestyleValuePrefix_ = this.freestyleValuePrefix;
        let freestyleValueValidator_ = this.freestyleValueValidator;

        this.selectElement.on('change', (ev, e) => {
            let values = this.selectElement.val();
            if (this.model != null && this.modelProperty != null) {
                this.model[this.modelProperty] = values;
            }

            if (this.freestyle) {
                // si c'est une saisie freestyle alors on la supprime de la liste de séléction             
                if (e != null && e.deselected != null && e.deselected.startsWith(freestyleValuePrefix_)) {
                    jQuery(el).find(`#${identifier_}`).find(`option[value='${e.deselected}']`).remove();
                    jQuery(el).find(`#${identifier_}`).trigger('chosen:updated');
                }
            }

            this.change.next(values);
        });

        if (this.freestyle) {

            jQuery(el).find(`#${this.identifier}_chosen`).find("input").on("keydown", function (evt) {
                var stroke;
                let _ref;
                stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
                if (stroke == 13 || stroke == 9 || stroke == 32) {
                    let value = jQuery(this).val();

                    if (freestyleValueValidator_ != null) {
                        if (!freestyleValueValidator_(value)) {
                            return;
                        }
                    }

                    if (freestyleValuePrefix_ != null) {
                        value = freestyleValuePrefix_ + value;
                    }

                    // existant -> on l'ajoute pas 
                    let existants = jQuery(el).find(`#${identifier_}`).find(`option[value='${value}']`);
                    if (existants.length != 0) {
                        return;
                    }

                    // ajout
                    jQuery(el).find(`#${identifier_}`).append('<option value="' + value + '" selected="selected">' + jQuery(this).val() + '</option>');
                    jQuery(el).find(`#${identifier_}`).trigger('chosen:updated');
                    jQuery(el).find(`#${identifier_}`).trigger('change');

                    if (stroke == 13) {
                        this.blur();
                    }

                    return false;
                }
            });
        }

        this.selectElement.trigger("chosen:updated");
    }


    routerCanReuse() {
        return false;
    }

    ngAfterViewChecked() {
        if (this.updateChosen) {
            if (this.model != null && this.modelProperty != null) {
                if (this.selectElement != null) {
                    this.selectElement.val(this.model[this.modelProperty]);
                }
            }
            this.selectElement.trigger("chosen:updated");
            this.updateChosen = false;
        }
    }

    ngOnDestroy() {
        jQuery(`#${this.identifier}`).chosen('destroy');
    }
}
