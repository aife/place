/* Indique ou sont localiser les ressources (images, css) d'apres le fichier de configuration.
 * A affecter en debut de jsp avec le taglib <atexo:href. */

var hrefRacine = '<atexo:href href=""/>';

// JavaScript Document

String.prototype.trim = function () {
	return this.replace(/^\s+/, "").replace(/\s+$/, "");
}

function timeOutMsg() {
	if (features.dev) return;
	alert('Votre session est sur le point d\'expirer. Veuillez proc\351der \340 l\'enregistrement.');
}

comp = (setTimeout(timeOutMsg, 3000000));

/////////////////////////////////////////////////////
var idInfoBulleT;
var chrono = null;
var delai = "500";
var numberRegexp = /^[0-9]+(\.||,){0,1}[0-9]{0,2}$/;


function remplaceEspace(inputString) {
	var espaceRegexp = new RegExp("[^0-9,\\.]", "g");
	var resultat = inputString.replace(espaceRegexp, "");
	return resultat;
}

// date au format dd/mm/yyyy
function verifieDate(strDate) {
	var strPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
	if ((strDate.match(strPattern)) && (strDate != '')) {
		return true;
	} else {
		return false;
	}
}

// date au format mm/yyyy
function verifieDateMMYYYY(strDate) {
	var datePattern = /^(0[1-9]|1[012])[/.](19|20|21)\d\d$/;
	if (datePattern.test(strDate)) {
		return true;
	} else {
		return false;
	}
}

function afficheInfoBulleLien(idBulle, texte, url, parent) {

	var bulle = document.getElementById(idBulle);
	if (url != null && url != '' && url != "null") {
		var reg = new RegExp("'", "g");
		url = url.replace(reg, "\\'");
		bulle.innerHTML = "<div>" + texte + ".<a href=\"javascript:popUpSetSize('" + url + "',1024,600,'yes')\" target='_blank'>D\351tail.</a></div>";
	} else {
		bulle.innerHTML = "<div>" + texte + "</div>";
	}
	var offset;
	var exp = new RegExp("^td_", "gi");

	if (chrono != null) {
		clearTimeout(chrono);
		cacheInfoBulleT();
	}

	bulle.style.display = "block";

	var bulleLeft = parent.offsetLeft + 10;
	var bulleTop = parent.offsetTop - bulle.offsetHeight;

	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

	bulle.style.left = bulleLeft + 'px';
	bulle.style.top = bulleTop + 'px';
	if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
		bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';
	}
	//check Internet Exporer VErsion

	//create iframe to fix ie6 select z-index bug
	if (version == '6.0;') {
		var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe"' + ' id="' + idBulle + '-Iframe"' + '></iframe>';
		var myMessage = bulle.innerHTML;
		bulle.innerHTML = myBulleIframe + myMessage;
		var myBulleHeight = bulle.offsetHeight;
		var myBulleWidth = bulle.offsetWidth;
		myNewID = idBulle + '-Iframe';
		var myNewIframe = document.getElementById(myNewID);


		var parentClass = myNewIframe.parentNode.className;
		var iframeNewClass = parentClass + '-Iframe';
		myNewIframe.className = iframeNewClass;
		myNewIframe.style.height = myBulleHeight;
		myNewIframe.style.width = myBulleWidth;
		//alert(myNewIframe.style.height);
		//alert(myNewIframe.className);
	}
}

function afficheInfoBulle(idBulle, texte, parent) {
	var bulle = document.getElementById(idBulle);
	bulle.innerHTML = "<div>" + texte + "</div>";

	var offset;
	var exp = new RegExp("^td_", "gi");

	if (chrono != null) {
		clearTimeout(chrono);
		cacheInfoBulleT();
	}

	bulle.style.display = "block";

	var bulleLeft = parent.offsetLeft + 10;
	var bulleTop = parent.offsetTop - bulle.offsetHeight;

	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

	bulle.style.left = bulleLeft + 'px';
	bulle.style.top = bulleTop + 'px';
	if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
		bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';
	}

	//check Internet Exporer VErsion

	//create iframe to fix ie6 select z-index bug
	if (version == '6.0;') {
		var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe"' + ' id="' + idBulle + '-Iframe"' + '></iframe>';
		var myMessage = bulle.innerHTML;
		bulle.innerHTML = myBulleIframe + myMessage;
		var myBulleHeight = bulle.offsetHeight;
		var myBulleWidth = bulle.offsetWidth;
		myNewID = idBulle + '-Iframe';
		var myNewIframe = document.getElementById(myNewID);


		var parentClass = myNewIframe.parentNode.className;
		var iframeNewClass = parentClass + '-Iframe';
		myNewIframe.className = iframeNewClass;
		myNewIframe.style.height = myBulleHeight;
		myNewIframe.style.width = myBulleWidth;
		//alert(myNewIframe.style.height);
		//alert(myNewIframe.className);
	}
}

function cacheInfoBulleT() {
	document.getElementById(idInfoBulleT).style.display = "none";
	chrono = null;
}

function cacheInfoBulle(idBulle) {
	idInfoBulleT = idBulle;
	chrono = setTimeout(cacheInfoBulleT, delai);
}

function mouseOverInfoBulle() {
	clearTimeout(chrono);
	chrono = null;
}

function mouseOutInfoBulle() {
	chrono = setTimeout(cacheInfoBulleT, delai);
}

/////////////////////////////////////////////////////
try {
	document.execCommand("BackgroundImageCache", false, true);
} catch (err) {
}


/*date dans le header*/
function writeDate() {

	var now = new Date();
	var jour = now.getDate();
	var mois = now.getMonth() + 1;
	var annee = now.getFullYear();
	var heure = now.getHours();
	var minute = now.getMinutes();

	heure = ((heure < 10) ? "0" + heure : heure);
	minute = ((minute < 10) ? "0" + minute : minute);

	var lettres = ("" + now.toGMTString() + "");

	//On recherche le jour de la semaine
	var joursemaine = lettres.substring(0, 3);
	var js = "";
	var mois2 = "";

	if (joursemaine == "Mon") {
		js = "Lundi";
	}
	if (joursemaine == "Tue") {
		js = "Mardi";
	}
	if (joursemaine == "Wed") {
		js = "Mercredi";
	}
	if (joursemaine == "Thu") {
		js = "Jeudi";
	}
	if (joursemaine == "Fri") {
		js = "Vendredi";
	}
	if (joursemaine == "Sat") {
		js = "Samedi";
	}
	if (joursemaine == "Sun") {
		js = "Dimanche";
	}

	//On recherche le mois
	if (mois == "1") {
		mois2 = "Janvier";
	}
	if (mois == "2") {
		mois2 = "F&eacute;vrier";
	}
	if (mois == "3") {
		mois2 = "Mars";
	}
	if (mois == "4") {
		mois2 = "Avril";
	}
	if (mois == "5") {
		mois2 = "Mai";
	}
	if (mois == "6") {
		mois2 = "Juin";
	}
	if (mois == "7") {
		mois2 = "Juillet";
	}
	if (mois == "8") {
		mois2 = "Ao&ucirc;t";
	}
	if (mois == "9") {
		mois2 = "Septembre";
	}
	if (mois == "10") {
		mois2 = "Octobre";
	}
	if (mois == "11") {
		mois2 = "Novembre";
	}
	if (mois == "12") {
		mois2 = "D&eacute;cembre";
	}

	var eme = "";

	if (jour == 1) {
		eme = " er";
	}

	document.write(jour + eme + ' ' + mois2 + ' ' + annee + ' ' + heure + ':' + minute);

}

/*Checks IE 6.0*/
var name = navigator.appName;
var version = navigator.appVersion;

if (name == 'Microsoft Internet Explorer') {
	id = version.indexOf('MSIE');
	version = version.substring(id + 5, id + 9);
}

/*menu gauche*/
function toggleMenu(myItem) {

	var myParent = myItem.parentNode;
	if (myParent.className == 'menu-top-open') {
		myParent.className = 'menu-top-closed';

		for (i = 0; i < myParent.childNodes.length; i++) {
			if (myParent.childNodes[i].className == 'ss-menu-open') {
				//alert('check');
				myParent.childNodes[i].className = 'ss-menu-closed';
			}
		}
	} else if (myParent.className == 'menu-top-closed') {
		myParent.className = 'menu-top-open';
	}

	var myParent = myItem.parentNode;
	if (myParent.className == 'menu-open') {
		myParent.className = 'menu-closed';

		for (i = 0; i < myParent.childNodes.length; i++) {
			if (myParent.childNodes[i].className == 'ss-menu-open') {
				myParent.childNodes[i].className = 'ss-menu-closed';
			}

		}
	} else if (myParent.className == 'menu-closed') {
		myParent.className = 'menu-open';
	}
}

function changeSsmenu(MySsmenu) {

	var myParent = document.getElementById('menuList');

	for (i = 0; i < myParent.childNodes.length; i++) {
		if (myParent.childNodes[i].nodeType == 1) {
			var totalLi = myParent.childNodes[i];
			for (t = 0; t < myParent.childNodes.length; t++) {
				if (myParent.childNodes[t].nodeType == 1) {
					var mySsMenuParent = myParent.childNodes[t];
					var totalUl = mySsMenuParent;
					for (u = 0; u < totalUl.childNodes.length; u++) {
						if (totalUl.childNodes[u].nodeType == 1 && totalUl.childNodes[u].tagName == 'UL') {
							var totalSsMenu = totalUl.childNodes[u];
							for (x = 0; x < totalSsMenu.childNodes.length; x++) {
								if ((totalSsMenu.childNodes[x].nodeType == 1)
									&& (totalSsMenu.childNodes[x].tagName == 'LI')) {
									totalSsMenu.childNodes[x].className = 'off';
								}
							}
						}
					}
				}
			}
		}
	}
	MySsmenu.className = 'on';
}

function resetSsmenu(myPosition) {
	var mySsliste = myPosition.parentNode.childNodes;
	for (g = 0; g < mySsliste.length; g++) {
		if (mySsliste[g].tagName == 'UL') {
			var mySilbing = mySsliste[g].childNodes;
			for (a = 0; a < mySilbing.length; a++) {
				if (mySilbing[a].nodeType == 1 && mySilbing[a].tagName == 'LI') {
					mySilbing[a].className = 'off';
					//alert(mySilbing[1].tagName);
					if (navigator.appName == "Microsoft Internet Explorer") {
						mySilbing[0].className = 'on';
					} else {
						mySilbing[1].className = 'on';
					}
				}
			}
		}
	}
}

function highlightMenuItem(MySsmenu) {

	var myParent = document.getElementById('menuList');
	for (i = 0; i < myParent.childNodes.length; i++) {
		if (myParent.childNodes[i].nodeType == 1) {
			var totalLi = myParent.childNodes[i];
			for (t = 0; t < myParent.childNodes.length; t++) {
				if (myParent.childNodes[t].nodeType == 1) {
					var mySsMenuParent = myParent.childNodes[t];
					var totalUl = mySsMenuParent;
					for (u = 0; u < totalUl.childNodes.length; u++) {
						if (totalUl.childNodes[u].nodeType == 1 && totalUl.childNodes[u].tagName == 'UL') {
							var totalSsMenu = totalUl.childNodes[u];
							for (x = 0; x < totalSsMenu.childNodes.length; x++) {
								if ((totalSsMenu.childNodes[x].nodeType == 1)
									&& (totalSsMenu.childNodes[x].tagName == 'LI')) {
									var mySibling = totalSsMenu.childNodes[x];
									for (n = 0; n < mySibling.childNodes.length; n++) {
										if (mySibling.childNodes[n].tagName == 'UL') {
											var mySsLien = mySibling.childNodes[n];
											for (w = 0; w < mySsLien.childNodes.length; w++) {
												if (mySsLien.childNodes[w].className == 'on') {
													mySsLien.childNodes[w].className = 'off';
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	MySsmenu.className = 'on';
}

/*Show layer according to tabs*/
function showHide(myLayer, myParent, nbOnglet) {
	for (t = 1; t <= nbOnglet; t++) {
		layers = 'layer' + t;
		document.getElementById(layers).style.display = 'none';
	}
	document.getElementById(myLayer).style.display = 'block';

	var myNav = document.getElementById('form-nav');
	for (i = 0; i < myNav.childNodes.length; i++) {
		if (myNav.childNodes[i].className == 'tab-on') {
			if (myNav.childNodes[i].className == 'tab-inactive') {
				myNav.childNodes[i].className = 'tab-inactive';
			} else {
				myNav.childNodes[i].className = 'tab';
			}
		}
	}
	myParent.parentNode.className = 'tab-on';
}


function showDiv(myDiv) {
	var div = document.getElementById(myDiv);
	if (div == null || div == undefined) {
		return;
	}
	div.style.display = 'block';
	initFloatingIframe();
}

function hideDiv(myDiv) {
	var div = document.getElementById(myDiv);
	if (div == null || div == undefined) {
		return;
	}
	div.style.display = 'none';
	initFloatingIframe();
}

/*onclick, show element1 and hide element2 */
function showHideElement(element1, element2) {
	document.getElementById(element1).style.display = 'block';
	document.getElementById(element2).style.display = 'none';
}

/*display panel according to option choice*/
function displayOptionChoice(mySelect) {
	var totalOptions = mySelect.options;
	for (i = 0; i < totalOptions.length; i++) {
		var totalLayers = (totalOptions[i].getAttribute('value'));
		document.getElementById(totalLayers).style.display = 'none';
	}
	document.getElementById(mySelect.options[mySelect.selectedIndex].value).style.display = 'block';
	initFloatingIframe();
}


/*display panel according to 1 option choice*/
function displayOneOptionPanel(mySelect, myIndex, myDiv) {
	if (mySelect.selectedIndex == myIndex) {
		document.getElementById(myDiv).style.display = 'block';
	} else {
		document.getElementById(myDiv).style.display = 'none';
	}
	initFloatingIframe();
}


/*hide panel according to 1 option choice*/
function hideOneOptionPanel(mySelect, myIndex, myDiv) {
	if (mySelect.selectedIndex == myIndex) {
		document.getElementById(myDiv).style.display = 'none';
	} else {
		document.getElementById(myDiv).style.display = 'block';
	}
	initFloatingIframe();
}


/*If checkbox is checked, displays 1 Div*/
function isChecked(myInput, myDiv) {
	if (myInput.checked == true) {
		document.getElementById(myDiv).style.display = 'block';
	}
	if (myInput.checked == false) {
		document.getElementById(myDiv).style.display = 'none';
	}
	initFloatingIframe();
}

function isCheckedHide(myInput, myDiv) {
	if (myInput.checked == true) {
		document.getElementById(myDiv).style.display = 'none';
	}
	if (myInput.checked == false) {
		document.getElementById(myDiv).style.display = 'block';
	}
}

/*If checkbox is checked, displays 1 Div and hide 1 div */
function isChecked2(myInput, myDiv1, myDiv2) {
	if (myInput.checked == true) {
		document.getElementById(myDiv1).style.display = 'block';
		document.getElementById(myDiv2).style.display = 'none';
	}
	if (myInput.checked == false) {
		document.getElementById(myDiv1).style.display = 'none';
		document.getElementById(myDiv2).style.display = 'block';
	}
	initFloatingIframe();
}

/*If checkbox is checked, displays table line*/
function isCheckedShowLine(myInput, myLine) {
	if (myInput.checked == true) {
		document.getElementById(myLine).style.display = '';
	}
	if (myInput.checked == false) {
		document.getElementById(myLine).style.display = 'none';
	}
}

/*If checked, check/uncheck collection*/
function isCheckedCheckCol(myInput, myInputCol, totalCol) {
	if (myInput.checked == true) {
		for (i = 1; i < totalCol + 1; i++) {
			document.getElementById(myInputCol + i).checked = true;
		}
	}
	if (myInput.checked == false) {
		for (i = 1; i < totalCol + 1; i++) {
			document.getElementById(myInputCol + i).checked = false;
		}
	}
}

/*show or hide previsu link*/
function showPrevisuLink(mySelect) {
	if (mySelect.selectedIndex == 0) {
		hideDiv('noPrevisu');
		hideDiv('odjUpload');
		showDiv('previsu');
	} else {
		showDiv('noPrevisu');
		showDiv('odjUpload');
		hideDiv('previsu');
	}
}

function activateSelect(mySelect) {
	document.getElementById(mySelect).disabled = false;
}

function disactivateSelect(mySelect) {
	document.getElementById(mySelect).disabled = true;
}

function disableInput(myCheckbox, myInput) {
	if (myCheckbox.checked == true) {
		document.getElementById(myInput).disabled = true;
	}
	if (myCheckbox.checked == false) {
		document.getElementById(myInput).disabled = false;
	}
}

/*Add new line plannification initiale */
var myLine = 0;

function addNewLine(taskLine, totalLine) {
	myLine++;
	if (myLine < (totalLine + 1)) {
		document.getElementById(taskLine + myLine).style.display = '';
	}
}

/*Clear input default value onclick */
function clearOnFocus(e, o) {
	if (o.firstTime)
		return;
	o.firstTime = true;
	o.value = "";
}

/******* Popin modale pour le téléchargement des fichiers *****/

/**
 * Fetch the content, add it to the JSZip object
 * and use a jQuery deferred to hold the result.
 * @param {String} url the url of the content to fetch.
 * @param {String} filename the filename to use in the JSZip object.
 * @param {JSZip} zip the JSZip instance.
 * @return {jQuery.Deferred} the deferred containing the data.
 */
function deferredAddZip(url, filename, zip) {
	var deferred = jQuery.Deferred();
	JSZipUtils.getBinaryContent(url, function (err, data) {
		if (err) {
			deferred.reject(err);
		} else {
			zip.file(filename, data, {binary: true});
			deferred.resolve(data);
		}
	});
	return deferred;
}

/**
 * Télécharge tous les fichiers contenus dans une popin sous la forme d'un seul zip , et renomme les fichiers qui sont en doublons
 * @param lienFichiers liste de liens vers les fichiers à téléacharger <a href={lien}>{nomFichier}</a>
 * @param callBack (facultatif) sera appelé une fois que le zip est prêt
 */
function dlAll(liensFichiers, callBack) {
	var zip = new JSZip();
	var deferreds = [];
	var tableau = [];
	var dico = {};
	// peupler un tableau d'objets
	liensFichiers.each(function () {
		var fileName = this.innerHTML;
		var filePath = this.attributes['href'].value;
		tableau.push({filePath: filePath, fileName: fileName});
		if (dico.hasOwnProperty(fileName))
			dico[fileName] = dico[fileName] + 1;
		else
			dico[fileName] = 0;
	});
	tableau.forEach(function (el) {

		var fileName = el.fileName;
		var filePath = el.filePath;
		var nbDoublons = dico[fileName];
		var lastComma = fileName.lastIndexOf(".");
		if (nbDoublons !== 0) {
			dico[fileName] = nbDoublons - 1;
			fileName = fileName.substring(0, lastComma) + '(' + nbDoublons + ')' + "." + fileName.substring(lastComma + 1, fileName.length);
		}
		deferreds.push(deferredAddZip(filePath, fileName, zip));

	});
	// when everything has been downloaded, we can trigger the dl
	jQuery.when.apply(jQuery, deferreds).done(function () {
		var blob = zip.generate({type: "blob"});
		// see FileSaver.js
		saveAs(blob, "documents.zip");
		if (callBack) callBack();
	}).fail(function (err) {
		if (console && console.log) {
			console.log(document.getElementById('jszip_utils'), e);
		}
	});

}

var $modalDialog = null;
var $modalAttenteUrl = "popin/attentePopin.htm";

function getModal(options) {
	//Initialisation d'une fenêtre modale vide avec gif d'attente
	if (!$modalDialog) {
		$modalDialog = jQuery('<div/>', {
			'class': 'popin',
			'id': 'mainModal'
		}).appendTo('body');
		$modalDialog.reload = function () {
			return $modalDialog;
		}
		$modalDialog.reload()
			.bind('dialogclose', $modalDialog.reload);
	}
	return $modalDialog;
}

function loadModalOptions(options) {
	//Quelques valeurs par défaut, surchargeables et extensibles comme on veut via le paramètre 'options'
	//doc: http://api.jqueryui.com/dialog/
	var dialogArgs = Object.extend({
		params: {},                     //paramètres de la requête Ajax
		resizable: false,               //la modale ne peut pas être redimensionnée par l'utilisateur
		autoOpen: false,                //cache la modale après initialisation
		show: 500,                      //fondu à l'affichage
		closeOnEscape: true,            //ferme la popin lorsque l'utilisateur appuie sur 'Escape'
		modal: true,                    //rend la popin modale (bloque les actions sur l'écran principal et les autres popin)
		width: 'auto',                  //largeur automatique en fonction du contenu au retour de la commande 'load'
		height: 'auto'                  //hauteur automatique en fonction du contenu au retour de la commande 'load'
	}, options);
	return dialogArgs;
}

function popIn(strURL, options) {
	var modalOptions = loadModalOptions(options);
	var modal = getModal(options);
	//Chargement dynamique du contenu de la modale
	modal.dialog(modalOptions)
		.dialog("open")
		.load(compileUrl(strURL, modalOptions.params));
}

/**
 * Ajoute les paramètres passés en arguments dans une url
 * @param url : base de l'url sous forme de texte
 * @param params : paramètre à ajouter sous la forme d'un objet JSON {key1 :param1, key2: param2, etc.}
 */
function compileUrl(url, params) {
	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			url += url.indexOf('?') > 0 ? '&' : '?';
			url += key + "=" + params[key];
		}
	}
	return url;
}

/*popup*/
var newWin = null;

function popUp(strURL, strScrollbars, target) {
	/* Manage several popups by giving them a name */
	if (target == null)
		target = "newWin";
	var strOptions = "";

	strOptions = "toolbar=no,menubar=no,scrollbars=" + strScrollbars + ",resizable,location=no";
	newWin = window.open(strURL, target, strOptions);
	newWin.focus();
}

function popupResize(containerId) {
	if (containerId == null) {
		containerId = "container";
	}

	var container = document.getElementById(containerId);
	var pageHeight = container.offsetHeight;
	var pageWidth = container.offsetWidth;

	var name = navigator.appName;
	var version = navigator.appVersion;

	var autoLeft = (screen.availWidth - pageWidth) / 2;
	var autoTop = (screen.availHeight - pageHeight) / 2;

	/*if popup content higher than screen height*/
	if (pageHeight > screen.availHeight) {
		/*if IE*/
		if (name == 'Microsoft Internet Explorer') {
			id = version.indexOf('MSIE');
			version = version.substring(id + 5, id + 9);
			/*if IE 7*/
			if (version == '7.0;') {
				//alert(version);
				window.resizeTo(pageWidth + 50, screen.availHeight - 100);
				window.moveTo(autoLeft - 20, 50);
			}
			/*if other IE version*/
			else {
				window.resizeTo(pageWidth + 50, screen.availHeight - 100);
				window.moveTo(autoLeft, 50);
			}
		}
		/*if other navitor*/
		else {
			window.resizeTo(pageWidth + 50, screen.availHeight - 100);
			window.moveTo(autoLeft, 0);
		}
	}
	/*else if content smaller than screen height */
	else {
		/*if IE*/
		if (name == 'Microsoft Internet Explorer') {
			id = version.indexOf('MSIE');
			version = version.substring(id + 5, id + 9);
			/*if IE 7*/
			if (version == '7.0;') {
				//alert(version);
				window.resizeTo(pageWidth + 50, pageHeight + 100);
				window.moveTo(autoLeft - 20, autoTop);
			}
			/*if other IE version*/
			else {
				window.resizeTo(pageWidth + 50, pageHeight + 80);
				window.moveTo(autoLeft, autoTop);
			}
		}
		/*if other navitor*/
		else {
			window.resizeTo(pageWidth + 50, pageHeight + 100);
			window.moveTo(autoLeft, autoTop);
		}
	}
}


/*onresize, SaisieBloc div content adapts*/
function resizeBlocContent(containerId) {
	if (containerId == null) {
		containerId = "container";
	}

	var myTableWidth = document.getElementById('consultationsTable').offsetWidth;
	var myRecapBox = document.getElementById('BlocContent');
	if (document.getElementById(containerId).style.width < myTableWidth) {
		document.getElementById(containerId).style.width = (myTableWidth + 42) + 'px';
	}
	if (document.getElementById('SaisieBlocContent')) {
		var saisieBoxWidth = document.getElementById('SaisieBlocContent').offsetWidth;
		//alert(myRecapBox.offsetWidth);
		myRecapBox.style.width = (saisieBoxWidth - 22) + 'px';
	}
}


/*popup with set height and width*/
var newWinSetSize = null;

function popUpSetSize(strURL, strWidth, strHeight, strScrollbars) {

	if (newWinSetSize != null) {
		if (!newWinSetSize.closed) {
			newWinSetSize.close();
		}
	}

	var strOptions = "";
	strOptions = "toolbar=no,menubar=no,scrollbars=" + strScrollbars + ",resizable,location=no,height=" + strHeight + ",width=" + strWidth;
	newWinSetSize = window.open(strURL, 'newWin', strOptions);
	newWinSetSize.focus();
}


function popupCenter(strURL, w, h, strScrollbars) {
	// Fixes dual-screen position
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	var top = ((height / 2) - (h / 2)) + dualScreenTop;
	var newWindow = window.open(strURL, 'newWin', 'toolbar=no,menubar=no,resizable,location=no,scrollbars=' + strScrollbars + ', width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	// Puts focus on the newWindow
	if (window.focus) {
		newWindow.focus();
	}
}

//Check/Uncheck All checkboxes

function CheckAll(chk) {
	for (i = 0; i < chk.length; i++)
		chk[i].checked = true;
}

function UnCheckAll(chk) {
	for (i = 0; i < chk.length; i++)
		chk[i].checked = false;
}


function isCheckedCheckAll(myInput, myInputList) {

	if (myInput.checked == true) {
		CheckAll(myInputList);
	}
	if (myInput.checked == false) {
		UnCheckAll(myInputList);
	}
}

/*show or hide collection of input */
function showHideCollection(myInput, collection1, collection2, totalCollection) {
	var myCollectionToShow = collection1 + "_";
	var myCollectionToHide = collection2 + "_";

	if (myInput.checked == true) {
		//show col 2 and hide col 1
		for (i = 1; i < (totalCollection + 1); i++) {
			if (document.getElementById(myCollectionToShow + [i]) != null) {
				document.getElementById(myCollectionToShow + [i]).style.display = 'block';
			}
			if (document.getElementById(myCollectionToHide + [i]) != null) {
				document.getElementById(myCollectionToHide + [i]).style.display = 'none';
			}
			//alert(myCollectionToShow+[i]);
		}
	}
	if (myInput.checked == false) {
		//show col 1 and hide col 2
		for (i = 1; i < (totalCollection + 1); i++) {
			if (document.getElementById(myCollectionToHide + [i]) != null) {
				document.getElementById(myCollectionToHide + [i]).style.display = 'block';
			}
			if (document.getElementById(myCollectionToShow + [i]) != null) {
				document.getElementById(myCollectionToShow + [i]).style.display = 'none';
			}
		}
	}
}


function displayCollection(mySelect, collection1, collection2, totalCollection, myDiv, myNoPrevisuDiv) {
	var myCollectionToShow = collection1 + "_";
	var myCollectionToHide = collection2 + "_";

	var selectedOption = mySelect.options[mySelect.selectedIndex].value;
	if (selectedOption == 'commun') {
		//show col 2 and hide col 1
		for (i = 1; i < (totalCollection + 1); i++) {
			document.getElementById(myCollectionToShow + [i]).style.display = 'block';
			document.getElementById(myCollectionToHide + [i]).style.display = 'none';
			showDiv(myDiv);
			if (document.getElementById(myNoPrevisuDiv)) {
				hideDiv(myNoPrevisuDiv);
			}
		}
	}
	if (selectedOption == 'individuel') {
		//show col 1 and hide col 2
		for (i = 1; i < (totalCollection + 1); i++) {
			document.getElementById(myCollectionToHide + [i]).style.display = 'block';
			document.getElementById(myCollectionToShow + [i]).style.display = 'none';
			hideDiv(myDiv);
			if (document.getElementById(myNoPrevisuDiv)) {
				showDiv(myNoPrevisuDiv);
			}
		}
	}
}

/*Check Uncheck collection of radio buttons */
function isCheckedCheckAllRadio(myInput, myVersion, version, totalCollection) {
	var myCollection = myVersion + "_";
	for (i = 1; i < (totalCollection + 1); i++) {
		var myTotalCollectionToCheckId = myCollection + [i] + '_' + version;
		document.getElementById(myTotalCollectionToCheckId).checked = true;
	}
}


/*open close panel*/
function togglePanel(myPosition, myPanel) {
	var layer = document.getElementById(myPanel);
	if (layer.style.display == 'none') {
		layer.style.display = 'block';
		myPosition.src = hrefRacine + 'images/picto-close-panel.gif';
	} else if (layer.style.display == 'block') {
		layer.style.display = 'none';
		myPosition.src = hrefRacine + 'images/picto-open-panel.gif';
	}
}

function toggleRecapPanel(myPosition, myPanel) {
	var layer = document.getElementById(myPanel);
	if (layer.style.display == 'none') {
		layer.style.display = 'block';
		myPosition.className = 'toggle-recap';
	} else if (layer.style.display == 'block') {
		layer.style.display = 'none';
		myPosition.className = 'toggle-recap-closed';
	}
}

/*Onclick swap image*/
var status = false;

function swapImage(myImg, img1, img2) {
	var imgRoot = 'images/';
	if (status == false) {
		myImg.src = imgRoot + img2;
		status = true;
	} else if (status == true) {
		myImg.src = imgRoot + img1;
		status = false;
	}
}

/////////////////////////////////////////////Floating div////////////////////////////////////////////////
/*Fix for Firefox cursor disapear bug*/
function firefoxPositionFix() {
	firefoxPositionFixGenerique("overlay", 'container');
}

function firefoxPositionFixGenerique(idDivOverlay, idContainer) {
	el = document.getElementById(idDivOverlay);
	if (el) {
		var overlayChildren = el.childNodes;
		for (i = 1; i < overlayChildren.length; i++) {
			if (overlayChildren[i].id == idContainer) {
				var containerClass = document.getElementById(overlayChildren[i].id).className;
//document.getElementById(overlayChildren[i].id).style.position = 'absolute';
				if (el.style.visibility == 'visible') {
					document.getElementById(overlayChildren[i].id).style.position = 'fixed';
					document.getElementById(overlayChildren[i].id).className = containerClass + ' ' + 'containerClass';
				} else {
					document.getElementById(overlayChildren[i].id).style.position = 'absolute';
				}
			}
		}
	}
}

/*Transparent layer that makes the the popup parent window inactive*/
function overlay() {
	overlayGenerique("overlay", "container", "floatingIframe");
}

function overlayGenerique(idDivOverlay, idContainer, idFloatingIframe) {
	autoPositionLayerGenerique(idContainer, idDivOverlay);
	el = document.getElementById(idDivOverlay);
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
	var myPageHeight = document.getElementById('middle').offsetHeight;
	initFloatingIframeGenerique(idFloatingIframe, idContainer);
	firefoxPositionFixGenerique(idDivOverlay, idContainer);
}


function overlayPopup() {
	autoPositionLayer();
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
	var myPageHeight = document.getElementById('contentPopup').offsetHeight;
	//document.getElementById('overlay').style.height = myPageHeight+200+'px';
	document.getElementById('overlay').style.height = myPageHeight + 'px';
	initFloatingIframe();
	firefoxPositionFix();
}

function autoPositionLayer() {
	autoPositionLayerGenerique('container', 'overlay');
}

function autoPositionLayerBlocNotes() {
	autoPositionLayerGenerique('containerBlocNotes', 'overlayBlocNotes');
}

function autoPositionLayerGenerique(idContainer, idOverlay) {
	firefoxPositionFixGenerique(idOverlay, idContainer);

	var myLayer = document.getElementById(idContainer);


	/*Positionnement horizontal du Div container par rapport a la taille de la fenetre*/
	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}
	var newWidth = windowWidth / 2;
	myLayer.style.top = newWidth + 'px';
	var myContainerWidth = document.getElementById(idContainer).offsetWidth;
	var myContainerWidthPosition = myContainerWidth / 2;
	myLayer.style.left = newWidth - myContainerWidthPosition + 'px';

	/*Positionnement vertical du Div container par rapport a la taille de la fenetre*/
	var windowHeight = 0;
	if (typeof (window.innerHeight) == 'number') {
		windowHeight = window.innerHeight;
	} else {
		if (document.documentElement && document.documentElement.clientHeight) {
			windowHeight = document.documentElement.clientHeight;

		} else {
			if (document.body && document.body.clientHeight) {
				windowHeight = document.body.clientHeight;
			}
		}
	}
	var newHeight = Math.round(windowHeight / 2);
	if (jQuery(document).find('#iframe-service').length) {
		newHeight = jQuery(window.parent.document).scrollTop() + 200;
	}
	myLayer.style.top = newHeight + 'px';
	var myContainerHeight = document.getElementById(idContainer).offsetHeight;
	var myContainerHeightPosition = myContainerHeight / 2;


	if (version == '6.0;') {
		var windowScroll = document.documentElement.scrollTop;
		myLayer.style.top = ((windowHeight / 2) + windowScroll - myContainerHeightPosition) + 'px';
	} else {
		var top = newHeight - myContainerHeightPosition;
		if (top < 0) {
			top = newHeight;
		}
		myLayer.style.top = top + 'px';
	}
}


/*Reinitialization of Layer position on Window resize event*/
function initLayer() {
	if (document.getElementById('overlay') != "") {
		autoPositionLayer();
		initFloatingIframe();
	}
}

/*Set Floating Iframe size*/
function initFloatingIframe() {
	initFloatingIframeGenerique('floatingIframe', 'container');
}

function initFloatingIframeGenerique(idFloatingIframe, idContainer) {
	if (document.getElementById(idFloatingIframe)) {
		var myIframe = document.getElementById(idFloatingIframe);
		var myContainerHeight = document.getElementById(idContainer).offsetHeight;
		var myContainerWidth = document.getElementById(idContainer).offsetWidth;
		myIframe.style.height = myContainerHeight + 'px';
		myIframe.style.width = myContainerWidth + 'px';
	}
}


/////////////////////////////////////////////End Floating div////////////////////////////////////////////////


/*show or hide all panels*/
function showAll(myPanel, NbPanels) {
	for (i = 0; i < NbPanels; i++) {
		var panel = myPanel + '-' + i;
		//alert(panel);
		if (document.getElementById(panel).style.display == 'none') {
			document.getElementById(panel).style.display = 'block';
		}
		var mypanelParent = document.getElementById(panel).parentNode;
		for (j = 0; j < mypanelParent.childNodes.length; j++) {
			if (mypanelParent.childNodes[j].className == 'title') {
				var titlechildnodes = mypanelParent.childNodes[j].childNodes;
				for (p = 0; p < titlechildnodes.length; p++) {
					if (titlechildnodes[p].tagName == 'IMG')
						titlechildnodes[p].src = hrefRacine + 'images/picto-close-panel.gif';
				}
			}
		}
	}
}

function hideAll(myPanel, NbPanels) {
	for (i = 0; i < NbPanels; i++) {
		var panel = myPanel + '-' + i;
		//alert(panel);
		if (document.getElementById(panel).style.display == 'block') {
			document.getElementById(panel).style.display = 'none';
		}
		var mypanelParent = document.getElementById(panel).parentNode;
		for (j = 0; j < mypanelParent.childNodes.length; j++) {
			if (mypanelParent.childNodes[j].className == 'title') {
				var titlechildnodes = mypanelParent.childNodes[j].childNodes;
				for (p = 0; p < titlechildnodes.length; p++) {
					if (titlechildnodes[p].tagName == 'IMG')
						titlechildnodes[p].src = hrefRacine + 'images/picto-open-panel.gif';
				}
			}
		}
	}
}

/*Clausier next step*/
function clausierNextStep() {
	mySelect = document.getElementById('clausierType');
	var selectedOption = mySelect.options[mySelect.selectedIndex].value;
	if (selectedOption == 1) {
		window.location = 'clause-texte-fixe.htm';
	}
	if (selectedOption == 2) {
		window.location = 'clause-text-champ-libre.htm';
	}
	if (selectedOption == 3) {
		window.location = 'clause-text-champ-prevalorise.htm';
	}
	if (selectedOption == 4) {
		window.location = 'clause-text-valeur-heritee.htm';
	}
	if (selectedOption == 5) {
		window.location = 'clause-liste-choix-exclusif.htm';
	}
	if (selectedOption == 6) {
		window.location = 'clause-liste-choix-cumulatif.htm';
	}
	if (selectedOption == 7) {
		window.location = 'clause-tableau-prix.htm';
	}
}

//////////////////////////////////////Infos-bulles//////////////////////////////////


var idBulleT;
var chrono = null;
var delai = "500";


function afficheCPV(idBulle, cpv, parent) {
	var bulle = document.getElementById(idBulle);
	var offset;
	var exp = new RegExp("^td_", "gi");
	if (jQuery.trim(document.getElementById(cpv).innerHTML) == '') {
		clearTimeout(chrono);
		cacheBulleT();
		return;
	}
	if (chrono != null) {
		clearTimeout(chrono);
		cacheBulleT();
	}

	bulle.style.display = "block";

	var bulleLeft = parent.offsetLeft + 10;
	var bulleTop = parent.offsetTop - bulle.offsetHeight;

	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

	bulle.style.left = bulleLeft + 'px';
	bulle.style.top = bulleTop + 'px';
	if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
		bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';
	}

	//check Internet Exporer VErsion

	//create iframe to fix ie6 select z-index bug
	if (version == '6.0;') {

		var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe"' + ' id="' + idBulle + '-Iframe"' + '></iframe>';
		var myMessage = bulle.innerHTML;
		bulle.innerHTML = myBulleIframe + myMessage;
		var myBulleHeight = bulle.offsetHeight;
		var myBulleWidth = bulle.offsetWidth;
		myNewID = idBulle + '-Iframe';
		var myNewIframe = document.getElementById(myNewID);


		var parentClass = myNewIframe.parentNode.className;
		var iframeNewClass = parentClass + '-Iframe';
		myNewIframe.className = iframeNewClass;
		myNewIframe.style.height = myBulleHeight;
		myNewIframe.style.width = myBulleWidth;
		//alert(myNewIframe.style.height);
		//alert(myNewIframe.className);
	}
}


function afficheBulle(idBulle, parent) {
	var bulle = document.getElementById(idBulle);
	var offset;
	var exp = new RegExp("^td_", "gi");

	if (chrono != null) {
		clearTimeout(chrono);
		cacheBulleT();
	}

	bulle.style.display = "block";

	var bulleLeft = parent.offsetLeft + 10;
	var bulleTop = parent.offsetTop - bulle.offsetHeight;

	var windowWidth = 0;
	if (typeof (window.innerWidth) == 'number') {
		windowWidth = window.innerWidth;
	} else {
		if (document.documentElement && document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			if (document.body && document.body.clientWidth) {
				windowWidth = document.body.clientWidth;
			}
		}
	}

	bulle.style.left = bulleLeft + 'px';
	bulle.style.top = bulleTop + 'px';
	if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {
		bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';
	}

	//check Internet Exporer VErsion

	//create iframe to fix ie6 select z-index bug
	if (version == '6.0;') {

		var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe"' + ' id="' + idBulle + '-Iframe"' + '></iframe>';
		var myMessage = bulle.innerHTML;
		bulle.innerHTML = myBulleIframe + myMessage;
		var myBulleHeight = bulle.offsetHeight;
		var myBulleWidth = bulle.offsetWidth;
		myNewID = idBulle + '-Iframe';
		var myNewIframe = document.getElementById(myNewID);


		var parentClass = myNewIframe.parentNode.className;
		var iframeNewClass = parentClass + '-Iframe';
		myNewIframe.className = iframeNewClass;
		myNewIframe.style.height = myBulleHeight;
		myNewIframe.style.width = myBulleWidth;
		//alert(myNewIframe.style.height);
		//alert(myNewIframe.className);
	}
}

function cacheBulleT() {
	if (idBulleT != null) {
		document.getElementById(idBulleT).style.display = "none";
		chrono = null;
	}
}

function cacheBulle(idBulle) {
	idBulleT = idBulle;
	chrono = setTimeout(cacheBulleT, delai);

	//document.getElementById(idBulle).style.display = "none";
}

function mouseOverBulle() {
	clearTimeout(chrono);
	chrono = null;
}

function mouseOutBulle() {
	chrono = setTimeout(cacheBulleT, delai);
}


//////////////////////////////////////End Infos-bulles//////////////////////////////////


/*disable option in selectbox */
function disableOption(mySelect, myIndex) {
	if (mySelect.selectedIndex == myIndex) {
		mySelect.selectedIndex = 0;
	}
}

function removeElementById(id) {
	var node = document.getElementById(id);
	if (node != null) {
		node.parentNode.removeChild(node);
	}
}


//////////////////////////////////////Debut Loader a l'interieur de la page//////////////////////////////////

function getDocument() {
	var frame = document.getElementById("iframeRedac");
	return frame ? frame.ownerDocument : document;
}

function showLoader() {
	el = getDocument().getElementById("pageLoader");

	if (!el) {
		var body = getDocument().getElementsByTagName("body")[0];
		var myLoader = document.createElement("div");
		myLoader.setAttribute('id', 'pageLoader');

		var pageHeight = getDocument().body.offsetHeight;
		var pageWidth = getDocument().body.offsetWidth;

		myLoader.innerHTML = '<div><img src="' + hrefRacine + 'images/loader.gif" alt="" title="Merci de bien vouloir patienter" /><p>Merci de bien vouloir patienter</p></div>';
		if (version === '6.0;')
			myLoader.innerHTML = '<div><iframe src="blank.html" scrolling="no" frameborder="0" id="loaderIframe"></iframe><img src="' + hrefRacine + 'images/loader.gif" alt="" title="Merci de bien vouloir patienter" /><p>Merci de bien vouloir patienter</p></div>';

		/*Positionnement vertical du Div container par rapport a la taille de la fenetre*/
		var windowHeight = 0;
		if (typeof (window.innerHeight) == 'number')
			windowHeight = window.innerHeight;

		else {
			if (document.documentElement && document.documentElement.clientHeight)
				windowHeight = document.documentElement.clientHeight;
			else if (document.body && document.body.clientHeight)
				windowHeight = document.body.clientHeight;
		}
		var newHeight = Math.round(windowHeight / 2);

		var windowScroll = window.pageYOffset || document.documentElement.scrollTop;
		if (windowScroll > 70)
			myLoader.style.top = (windowHeight / 2 + windowScroll - 70) + 'px';
		else
			myLoader.style.top = (windowHeight / 2) + 'px';
		myLoader.style.left = (pageWidth / 2 - 80) + 'px';

		myLoader.style.top = '50px';

		body.appendChild(myLoader);

	} else {
		getDocument().body.removeChild(el);
	}
}

function loaderSommaire() {
	autoPositionLayer();
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
	var myPageHeight = document.body.offsetHeight;
	//document.getElementById('overlay').style.height = myPageHeight+200+'px';
	document.getElementById('overlay').style.height = myPageHeight + 'px';
	initFloatingIframe();
	firefoxPositionFix();
}

function synchronisationAtexoIntervalle(select) {
	var listeSelectIntervalle = document.getElementsByName('nbResults');
	for (var i = 0; i < listeSelectIntervalle.length; i++) {
		var s = listeSelectIntervalle[i];
		if (s != select) {
			s.selectedIndex = select.selectedIndex;
		}
	}
}

//////////////////////////////////////Fin Loader a l'interieur de la page//////////////////////////////////

function verifierChaineSANS(valeur) {
	var regExpSans = new RegExp("^(sans)$", "i");
	return (regExpSans.test(valeur));
}

//-->

/**
 * Pour compatibilite ie6.
 */
function getElementsByName_iefix(tag, name) {
	var elem = document.getElementsByTagName(tag);
	var arr = new Array();
	for (i = 0, iarr = 0; i < elem.length; i++) {
		att = elem[i].getAttribute("name");
		if (att == name) {
			arr[iarr] = elem[i];
			iarr++;
		}
	}
	return arr;
}

/**
 * Pour compatibilite ie6.
 */
function getElementsByName(name) {
	var res = document.getElementsByName(name);
	if (res.length == 0) {
		res = getElementsByName_iefix('div', name);
	}
	return res;
}

function confirmeSupp(hrefId) {
	if (window.confirm("Voulez-vous supprimer cet \xe9l\xe9ment?")) {
		document.location.href = hrefId;
	}
}

//javascript appelé par rsem pour mettre à jour la taille de l'iframe lors que rsem est intégré dans MPE
// cette fonction doit être apellé lors de tout changement de la structure DOM pour la taille de l'iframe soit mise à jour.
function miseAJourTailleIFrame() {
	console.log("Plus de retaillage de l'iframe");
}

function miseAJourTailleIFrameTimeout() {
	setTimeout(miseAJourTailleIFrame, 500);
}

//appelée dans le onload lorsque l'on est en mode saas.
function miseAJourTailleIFrameSurLien() {
	jQuery('#iframe-service a').each(function () {
		jQuery(this).click(function () {
			miseAJourTailleIFrameTimeout();
		});
	});

	jQuery('#iframe-service select').each(function () {
		jQuery(this).change(function () {
			miseAJourTailleIFrameTimeout();
		});
	});

	jQuery('#iframe-service :radio').each(function () {
		jQuery(this).change(function () {
			miseAJourTailleIFrameTimeout();
		});
	});
}


/**
 * Retourne la valeur d'une selection multiple (checkbox) sous forme de tableau
 * @param checkboxName
 */
function selectionxMultiple(checkboxName) {
	var checked = jQuery('input[name=' + checkboxName + ']:checked');
	if (checked) {
		return checked.map(function (_, el) {
			return jQuery(el).val();
		}).get();
	} else {
		return null;
	}
}

/**
 * Spécifie si une liste de noeuds ne contient aucune valeur
 * @param name : le nom de la balise HTML
 */

function isCellulesVides(name) {
	var types = document.getElementsByName(name);
	if (types)
		for (i = 0; i < types.length; i++) {
			if (isEmpty(types[i]))
				return true;
		}
	else
		return false;
}

/**
 * Spécifie si une liste de noeuds ne contient aucune valeur
 * @param name : le nom de la balise HTML
 */
function isEmpty(str) {
	return (!str || 0 === str.length || str == "" || str == "-" || str.trim() == "" || str.trim() == "-");
}

function scrollerToId(_tag) {

	// cas redac
	try {
		var _offsetTop = jQuery(_tag).offset().top;
		jQuery('html, body').animate({
			scrollTop: _offsetTop
		}, 200);
		window.parent.documentScrollTop(_offsetTop);
	} catch (err) {
		console.log('Iframe Redac introuvable')
	}
}

function scrollerToIdSearchInstance() {
	jQuery('.js-scroll-to-searchInstance').click(function () {
		scrollerToId('#searchInstance');
	});

}

jQuery(document).ready(function () {
	scrollerToIdSearchInstance();
});

function envoyerContexte() {
	let data = {application: 'REDAC', version: '@version@'};
	let current = JSON.parse(localStorage.getItem('contexte'));
	let contexte = {...data, ...current};

	console.log('Initialisation du contexte REDAC et diffusion', contexte);

	window.parent.postMessage(contexte, '*');
}

jQuery(document).ready(function () {
	envoyerContexte();
});

jQuery(window).unload(function () {
	localStorage.removeItem('contexte');
});
