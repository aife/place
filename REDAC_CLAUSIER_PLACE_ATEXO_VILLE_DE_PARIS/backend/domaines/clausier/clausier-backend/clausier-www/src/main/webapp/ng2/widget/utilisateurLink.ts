import {AfterViewInit, Component, Input} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';

@Component({
    selector: 'ltexec-utilisateur-link',
    templateUrl: 'ng2/widget/utilisateurLink.html',
    directives: [CORE_DIRECTIVES]
})
export class UtilisateurLinkComponent implements AfterViewInit {

    @Input() utilisateur:Object;

    static identifier_:number = 0;

    identifier:number = 0;

    constructor() {
        UtilisateurLinkComponent.identifier_++;
        this.identifier = UtilisateurLinkComponent.identifier_;
    }

    ngAfterViewInit() {
        jQuery(`#detail_agent_${this.identifier}`).popover({
            html: true,
            content: function () {
                var content = jQuery(this).attr("id") + '_popover';
                return jQuery('#' + content).children(".popover-body").html();
            },
            title: function () {
                var content = jQuery(this).attr("id") + '_popover';
                return jQuery('#' + content).children(".popover-heading").html();
            }

        }).on('click', function (e) {
            e.preventDefault();
        })
    }
}
