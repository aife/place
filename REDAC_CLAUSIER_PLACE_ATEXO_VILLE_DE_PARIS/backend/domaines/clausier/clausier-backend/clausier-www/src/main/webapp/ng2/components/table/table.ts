import {CORE_DIRECTIVES} from 'angular2/common';
import {Component, Input, OnChanges} from 'angular2/core';
import {PaginationPage, PaginationPropertySort} from '../../common/pagination'
import * as Rx from "rxjs/Rx";
import {hideLoading, showLoading} from "../../common/loader"

export interface Table {

    fetchPage(pageNumber:number, pageSize:number, sort:PaginationPropertySort): Rx.Observable<any>;

}

@Component({
    selector: 'ltexec-table-elements-count',
    template: `
    <h3 class="repeater-header-title">Nombre de résultats : {{page.totalElements}}</h3>
    `,
    directives: [CORE_DIRECTIVES]
})
export class TableElementsCount<T> {
    @Input() page:PaginationPage<T>;
}

@Component({
    selector: 'ltexec-table-pagination',
    template: `

<div class="repeater-itemization" *ngIf="page != null">
    <div data-resize="auto" class="btn-group selectlist">
        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
            <span class="selected-label">{{page.size}}</span>
            <span class="caret"></span>
        </button>
        <ul role="menu" class="dropdown-menu">
            <li *ngFor="#itemsPerPage of [5,10,20,50,100]">
                <a (click)="fetchPageSize(itemsPerPage)">{{itemsPerPage}}</a>
            </li>
        </ul>
        <input type="text" aria-hidden="true" readonly name="itemsPerPage" class="hidden hidden-field">
    </div>
    <span> Résultats par Pages</span>
</div>

<div class="repeater-pagination" *ngIf="page != null">
    <button class="btn btn-default btn-xs repeater-prev" type="button" [disabled]="page.first"
            (click)="fetchPreviousPage()">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </button>
    <span>Page</span>
    <input type="text" class="form-control repeater-secondaryPaging active" [value]="page.number+1"
           (keyup)="fetchPageNumber($event)">
    <span>sur <span class="repeater-pages"> {{page.totalPages}} </span></span>
    <button class="btn btn-default btn-xs repeater-next" type="button" [disabled]="page.last"
            (click)="fetchNextPage()">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </button>
</div>
    `,
    directives: [CORE_DIRECTIVES]
})
export class TablePagination<T> {
    @Input() table:Table;
    @Input() page:PaginationPage<T>;

    fetchPageNumber($event) {

        if ($event.which !== 13) {
            return;
        }
        if (isNaN($event.target.value)) {
            return;
        }
        let observable:Rx.Observable<any> = this.table.fetchPage(+$event.target.value - 1, this.page.size, this.getSort());
        if (observable != null) {
            showLoading();
            observable.subscribe(() => {
            }, (e) => {
                hideLoading()
            }, hideLoading);
        }
    }

    fetchPageSize(pageSize:number) {
        let observable:Rx.Observable<any> = this.table.fetchPage(this.page.number, pageSize, this.getSort());
        if (observable != null) {
            showLoading();
            observable.subscribe(() => {
            }, (e) => {
                hideLoading()
            }, hideLoading);
        }
    }

    fetchNextPage() {
        let observable:Rx.Observable<any> = this.table.fetchPage(this.page.number + 1, this.page.size, this.getSort());
        if (observable != null) {
            showLoading();
            observable.subscribe(() => {
            }, (e) => {
                hideLoading()
            }, hideLoading);
        }
    }

    fetchPreviousPage() {
        let observable:Rx.Observable<any> = this.table.fetchPage(this.page.number - 1, this.page.size, this.getSort());
        if (observable != null) {
            showLoading();
            observable.subscribe(() => {
            }, (e) => {
                hideLoading()
            }, hideLoading);
        }
    }

    private getSort():PaginationPropertySort {
        if (this.page.sort != null && this.page.sort.length > 0) {
            return this.page.sort[0];
        } else {
            return null;
        }
    }
}

@Component({
    selector: 'ltexec-table-sort',
    template: `
        <a style="cursor: pointer;" (click)="sortByProperty()" >{{label}}<i class="fa" [class.fa-sort]="sortClass"  [class.fa-sort-asc]="sortAscClass" [class.fa-sort-desc]="sortDescClass"  ></i></a>
    `,
    directives: [CORE_DIRECTIVES]
})
export class TableSort<T> implements OnChanges {

    @Input() label:string;
    @Input() property:string;
    @Input() table:Table;
    @Input() page:PaginationPage<T>;

    sortDirection:string;
    sortClass:boolean = false;
    sortAscClass:boolean = false;
    sortDescClass:boolean = false;

    ngOnChanges(changes) {

        if (changes['page']) {

            var defineValues = (s, sa, sd, dir) => {
                this.sortClass = s;
                this.sortAscClass = sa;
                this.sortDescClass = sd;
                this.sortDirection = dir;
            };

            if (this.page.sort == null) {
                defineValues(true, false, false, 'ASC');
                return;
            }
            var one:PaginationPropertySort = this.page.sort.find(e => e.property === this.property);

            if (one == null) {
                defineValues(true, false, false, 'ASC');
            } else {
                if (one.direction === 'ASC') {
                    defineValues(false, true, false, 'DESC');
                } else {
                    defineValues(false, false, true, 'ASC');
                }
            }
        }
    }

    sortByProperty() {

        let sort:PaginationPropertySort;
        sort = {property: this.property, direction: this.sortDirection};

        let pageNumber = this.page.number - 1;
        if (pageNumber < 0) {
            pageNumber = 0;
        }

        let observable:Rx.Observable<any> = this.table.fetchPage(pageNumber, this.page.size, sort);

        if (observable != null) {
            showLoading();
            observable.subscribe(() => {
            }, () => {
                hideLoading()
            }, hideLoading);
        }
    }
}

export var tableDirectives:Array<any> = [
    TableElementsCount,
    TablePagination,
    TableSort,
];
