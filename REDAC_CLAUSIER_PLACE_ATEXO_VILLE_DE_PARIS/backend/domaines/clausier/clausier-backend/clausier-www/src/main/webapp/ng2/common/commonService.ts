/**
 * Created by MZO on 23/07/2015.
 */


import {PaginationPropertySort} from './pagination';

import * as Rx from "rxjs/Rx";

export class HeadersBuilder {

    headers:any;

    constructor() {
        this.headers = {}
        this.setAuthorization();
    }

    private setAuthorization():HeadersBuilder {
        this.headers['Authorization'] = "Bearer qsdqs" ;
        return this;
    }

    setContentTypeFormUrlEncoded():HeadersBuilder {
        this.headers['Content-Type'] = "application/x-www-form-urlencoded; charset=UTF-8";
        return this;
    }

    setContentTypeJson():HeadersBuilder {
        this.headers['Content-Type'] = "application/json; charset=utf-8";
        return this;
    }

    build():{ [ key : string ] : any } {
        return this.headers;
    }
}

export class PaginationRequestParamsBuilder {
    page:number = 0;
    pageSize:number = 10;
    sort:PaginationPropertySort

    constructor() {

    }

    setPage(page:number):PaginationRequestParamsBuilder {
        this.page = page;
        return this;
    }

    setPageSize(pageSize:number):PaginationRequestParamsBuilder {
        this.pageSize = pageSize;
        return this;
    }

    setSort(sort:PaginationPropertySort):PaginationRequestParamsBuilder {
        this.sort = sort;
        return this;
    }

    build(params:any) {
        params.size = this.pageSize;
        params.page = this.page;
        if (this.sort != null) {
            params.sort = `${this.sort.property},${this.sort.direction}`;
        }
    }
}

export function getObservable(settings:JQueryAjaxSettings):Rx.Observable {
    return Rx.Observable.fromPromise($.ajax(settings)).publish().refCount();
}