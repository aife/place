<%--
  Created by IntelliJ IDEA.
  User: sta
  Date: 11/01/16
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div>
    <div>
        <div class='form-bloc'>
            <div class='top'>
                <span class='left'></span><span class='right'></span>
            </div>
            <div id="loadingContent" class='content' style="display: none">
                <div class='center'>
                    <img src='images/loader.gif' alt='Veuillez patienter' title='Veuillez patienter'/>
                </div>
                <div class='spacer-small'></div>
                <div class='center' id='messageAttente'>
                    Veuillez patienter : Téléchargement du document en cours...<br/>
                    <strong>Attention, le temps de téléchargement peut être long</strong><br/>
                    (plusieurs minutes dans certains cas)<br/>
                    <i>La fermeture de cette fenêtre annule l'action.</i>
                </div>
                <div class='spacer'></div>
            </div>
            <div id="mainContent" class='content'>
                <div class='line'>
                    <strong>Document(s) générés(s) :</strong>
                </div>
                <c:choose>
                    <c:when test="${empty fichiersATelecharger}">
                        <spring:message code="${aucunFichierMessage}"/>
                    </c:when>
                    <c:otherwise>
                        <ul class='liste-docs-generes'>
                            <c:forEach var="fichier" items="${fichiersATelecharger}">
                                <li>- <a href='${fichier.url}'>${fichier.nomFichier}</a></li>
                            </c:forEach>
                        </ul>
                    </c:otherwise>
                </c:choose>
                <div class='spacer'></div>
            </div>
            <div class='bottom'>
                <span class='left'></span><span class='right'></span>
            </div>
        </div>
        <div class="boutons">
            <div id="annulerId">
                <c:if test="${fichiersATelecharger.size() gt 1}">
                <div id="allId">
                    <div><a href="javascript:void(0)" class="telecharger-tout float-right">Tout télécharger</a></div>
                </div>
                </c:if>
                <div><a href="javascript:void(0)" class="annuler"><spring:message code="common.fermer"/></a></div>
            </div>
        </div>
    </div>
</div>

