<%--
  Template utilisé pour les modales popin.
  1) Ajouter un controller inspiré de fr.paris.epm.passation.presentation.controllers.popin.PopinTelechargerController
  2) Une méthode du controller correspond à l'url "/{motif}.htm" et retourne un identifiant tiles
  3) Ajouter une définition dans tiles-def.xml dont l'attribut "name" sera cet identifiant et utilisant ce template
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="" id="container">
    <tiles:insertAttribute name="body"/>
</div>
<script type="text/javascript">
    function beforeTelecharger() {
        jQuery('#mainContent').hide();
        jQuery(".telecharger-tout").hide();
        jQuery('#loadingContent').show();
    }
    function afterTelecharger() {
        jQuery('#loadingContent').hide();
        jQuery('#mainContent').show();
        jQuery(".telecharger-tout").show();
    }
    var btnAnnuler = jQuery(".annuler");
    if (btnAnnuler) {
        btnAnnuler.click(function () {
            getModal().dialog("close");
        });
    }
    var btnAnnuler = jQuery(".btn-annuler");
    if (btnAnnuler) {
        btnAnnuler.click(function () {
            getModal().dialog("close");
        });
    }
    var btnTelechargerTout = jQuery(".telecharger-tout");
    if (btnTelechargerTout) {
        btnTelechargerTout.click(function () {
            beforeTelecharger();
            dlAll(btnTelechargerTout.closest('.ui-dialog').find('.liste-docs-generes a'), afterTelecharger);
        });
    }
</script>
