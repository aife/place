<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="StrutsHtml" prefix="html" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="StrutsBean" prefix="bean"%>
<script type="text/javascript">
	 hrefRacine = '<atexo:href href=""/>';
</script>
<!--Debut Bloc Navigation par mots cles-->
<div class="content" id="layer2" style="display:block">
   <div>
     <div id="ligneRech" class="line">
       <html:form action="/codeAchatRechercheDo.epm" styleId="codeAchatRechercheDo">
       <table class="ligne" cellpadding="0" cellspacing="0" summary="">
         <tr>
           <td style="vertical-align: top;" align="left"><div class="intitule"><bean:message key="codeAchatRecherche.motsCles" /></div></td>
           <td style="vertical-align: top;" align="left"><html:text  property="motsCles" titleKey="codeAchatRecherche.motsCles" styleClass="mots-clefs" /></td>
           <td style="vertical-align: top;" align="left"><div class="gwt-HTML">
           <a href="javascript:this.document.codeAchatRechercheForm.submit();" class="rechercher-code"><bean:message key="codeAchatRecherche.bouton.rechercher" /></a></div></td>
         </tr>
       </table>
       </html:form>
     </div>

     <div class="spacer"></div>
    <div class="infos-recherche"><bean:message key="codeAchatRecherche.rechercheMotcle.legende.titre" />
       <div class="indent">
         <bean:message key="codeAchatRecherche.rechercheMotcle.1.information" /><br />
         <bean:message key="codeAchatRecherche.rechercheMotcle.2.information" /><br />
         </div>
     </div>
         <jsp:include page="codeAchatResultat.jsp" />
     <div class="breaker"></div>
   </div>
</div>
<div class="breaker"></div>
<!--Fin Bloc Navigation par mots cles-->