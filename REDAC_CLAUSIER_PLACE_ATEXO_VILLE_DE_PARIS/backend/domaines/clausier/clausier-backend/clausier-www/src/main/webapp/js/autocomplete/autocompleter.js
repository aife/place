jQuery(document).ready(function ($) {
    $(function () {
        if (typeof autocomplete !== typeof undefined) {
            $("#rechercher").autocomplete({

                source: function (request, response) {
                    $.ajax({
                        url: "rechercheRapideServlet",
                        type: "GET",
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        success: function (data) {
                            if (!data.length) {
                                var result = [
                                    {
                                        label: 'Aucun résultat',
                                        value: response.term
                                    }
                                ];
                                response(result);
                            }
                            else {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.numeroConsultation + ' ' + item.intituleConsultation,
                                        value: item.numeroConsultation
                                    }
                                }));
                            }
                        }
                    });
                },
                minLength: 3,
                delay: 400
            });
        }
    });
});
