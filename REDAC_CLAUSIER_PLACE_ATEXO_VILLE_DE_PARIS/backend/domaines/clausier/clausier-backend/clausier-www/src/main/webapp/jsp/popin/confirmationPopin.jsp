<%--
  Created by IntelliJ IDEA.
  User: sta
  Date: 11/01/16
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${empty retour}">
    <c:set var="retour" value="confirmationPopinGenerique()"/>
</c:if>

<div>
    <div class="popup-small modal-dialog" id="container">
        <div class='form-bloc modal-content'>
            <div class='modal-body'>
                <spring:message code="${messageKey}"/>
            </div>
            <div class="clearfix modal-footer">
                <a href="javascript:void(0)" class="btn btn-warning btn-sm btn-annuler pull-left"><spring:message code="popupConfirmation.annuler"/></a>
                <a id="valider" href="javascript:${retour}" class="btn btn-primary btn-sm pull-right"><spring:message code="popupConfirmation.valider"/></a>
            </div>
        </div>
    </div>
</div>




