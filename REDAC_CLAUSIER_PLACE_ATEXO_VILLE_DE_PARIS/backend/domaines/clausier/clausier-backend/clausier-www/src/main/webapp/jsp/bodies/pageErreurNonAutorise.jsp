<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<div class="main-part">
	<div align="center">
		<br>
		<br>
		<br>
		<br>
		<br>
		<logic:messagesPresent>
			<html:errors />
		</logic:messagesPresent>
		<logic:messagesNotPresent>
			<bean:message key="erreur.nonAutorise" />
		</logic:messagesNotPresent>
	</div>
</div>
