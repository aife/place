<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 06/03/17
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="resultList" type="fr.paris.epm.global.commun.ResultList"--%>

<div class="pagination form-inline pull-right">

    <div class="form-group interval">
        <label for="interval"><spring:message code="navigation.afficher"/>&nbsp;</label>

        <select id="interval" name="interval" class="form-control" onchange="if (this.value) window.location.href=this.value">
            <option value="navigate.htm?interval=10&direction=" <c:if test="${resultList.interval == 10}">selected</c:if>>10</option>
            <option value="navigate.htm?interval=20&direction=" <c:if test="${resultList.interval == 20}">selected</c:if>>20</option>
            <option value="navigate.htm?interval=30&direction=" <c:if test="${resultList.interval == 30}">selected</c:if>>30</option>
            <option value="navigate.htm?interval=40&direction=" <c:if test="${resultList.interval == 40}">selected</c:if>>40</option>
        </select>
    </div>

    <div class="form-group">
        <label class="s-ml s-mr">
            <spring:message code="navigation.resultatsParPage"/>
        </label>
    </div>

    <div class="form-group">
        <nav aria-label="pagination" class="pull-left">
            <ul class="pager">
                <li><a href="navigate.htm?direction=prev&interval=0"><spring:message code="navigation.precedent"/></a></li>
                <li><c:out value="${resultList.pageCurrent}/${resultList.countPages}"/></li>
                <li><a href="navigate.htm?direction=next&interval=0"><spring:message code="navigation.suivant"/></a></li>
            </ul>
        </nav>
    </div>

</div>