<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<script language="javascript">
window.onload = function(){
	document.getElementById('divIframe').innerHTML = '<iframe width=\"0\" height=\"0\" frameborder=\"0\" src=\"/logout\"\></iframe\>'; 
}
</script>

<!--Debut Main part-->
<div class="main-part">
	<div class="form-bloc">
	   <div class="top"><span class="left"></span><span class="right"></span></div>
			
	   <div class="content">
	   <div class="spacer"></div>
	   <div class="center"><bean:message key="deconnexion.message"/></div>
	      <div class="spacer"></div>
	   </div>
<!--	   <iframe width="0" height="0" frameborder="0" src="/logout"></iframe> -->
       <div id="divIframe"></div>
	   <div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<script language="javascript">
	 function getCookieVal(offset) {
		 var endstr=document.cookie.indexOf (";", offset);
		 if (endstr==-1)
		 endstr=document.cookie.length;
		 return unescape(document.cookie.substring(offset, endstr));
	}
	 
	 function GetCookie (name) {
		 var arg=name+"=";
		 var alen=arg.length;
		 var clen=document.cookie.length;
		 var i=0;
		 while (i<clen) {
		  var j=i+alen;
		  if (document.cookie.substring(i, j)==arg)
		   return getCookieVal (j);
		  i=document.cookie.indexOf(" ",i)+1;
		  if (i==0) break;
		 }
		 return null;
	} 
	
	function deleteCookie (name) {
		var exp=new Date();
		exp.setTime (exp.getTime() - 100000);
		var cval=GetCookie (name);
		document.cookie=name+"="+cval+"; expires="+exp.toGMTString();
	}

	deleteCookie("JSESSIONID");
	</script>
</div>
<!--Fin Main part-->