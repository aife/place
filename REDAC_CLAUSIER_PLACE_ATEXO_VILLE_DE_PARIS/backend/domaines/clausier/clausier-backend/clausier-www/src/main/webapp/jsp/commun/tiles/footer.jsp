<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<div id="atx-footer" class="atx-footer">
    <div class="atx-footer-container">
        <div class="clearfix">
            <div class="pull-left">

            </div>
            <div class="pull-right">
                <a class="link" href="<bean:message bundle="commun" key="footer.conseils.url"/>">
                    <bean:message bundle="commun" key="footer.conseils"/>
                </a>

                <a class="link" href="<bean:message bundle="commun" key="footer.mentions.legales.url"/>">
                    <bean:message bundle="commun" key="footer.mentions.legales"/>
                </a>

                <a class="link" href="<bean:message bundle="commun" key="footer.version.url"/>">
                    <bean:message bundle="commun" key="footer.version"/> @version@
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Pour les infos bulles -->
<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infoBulle">
    <div></div>
</div>

<!-- Div Flottant Bloc note -->
<div id="overlayBlocNotes">
    <div id="containerBlocNotes" class="popup-post-it">
        <div class="layerBorder-post-it">
            <iframe scrolling="no" frameborder="0" id="floatingIframeBlocNotes" src="blank.html"></iframe>

            <!--Debut Formulaire Post-it-->
            <div class="post-it-form">
                <textarea rows="10" cols="" title="Message" name="" id="contenuBlocNotes">${contenuBlocNotes}</textarea>
            </div>
            <!--Fin  Formulaire Post-it-->
            <div class="boutons">
                <a class="annuler" href="javascript:annulerBlocNotes();">Annuler</a>
                <a class="valider"
                   href="javascript:validerBlocnotes('${idNote}', '${idConsultationNote}', '${idBudLotNote}', '${idAvenantNote}');">Valider</a>
            </div>
        </div>
    </div>
</div>

<%--Placer ici les scripts à charger après le téléchargement de la page--%>
<script language="JavaScript" type="text/JavaScript" src="js/jszip.min.js"></script>
<script language="JavaScript" type="text/JavaScript" src="js/jszip-utils.js"></script>
<script language="JavaScript" type="text/JavaScript" src="js/FileSaver.min.js"></script>
<script src="js/autocomplete/autocompleter.js"></script>
