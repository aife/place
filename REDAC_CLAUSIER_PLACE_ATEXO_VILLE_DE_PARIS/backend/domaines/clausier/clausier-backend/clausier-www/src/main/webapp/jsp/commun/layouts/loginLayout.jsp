<%@ page language="java"%>
<%@ taglib uri="StrutsHtml" prefix="html" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><tiles:getAsString name="title"/></title>
<script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>
<link rel="stylesheet" href="<atexo:href href='css/styles-commun.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
<link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
<script type="text/javascript">
	 hrefRacine = '<atexo:href href=""/>';
</script>
</head>

<body>

<tiles:insertAttribute name="header"/>
<!--
<tiles:insertAttribute name='menu'/>
 -->
<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="footer"/>

</body>
</html>
