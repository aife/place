<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="tiles"    uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c"        uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bean"     uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="atexo"    uri="AtexoTag" %>

<%
    response.setHeader("Cache-Control", "no-cache, must-revalidate, proxy-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>

<tiles:useAttribute name="classBody" ignore="true"/>
<tiles:useAttribute name="useBootstrap" ignore="true"/>
<tiles:useAttribute name="frontVersion" ignore="true"/>
<tiles:useAttribute name="jQuery_3" ignore="true"/>
<tiles:useAttribute name="vueJs" ignore="true"/>
<tiles:useAttribute name="acceptMultiWindow" ignore="true"/>

<!--
    classBody = "${classBody}"
    useBootstrap = "${useBootstrap}"
    jQuery_3 = "${jQuery_3}"
    frontVersion = "${frontVersion}"
    vueJs = "${vueJs}"

    mode SaaS (RECAD d'ORME) = ${accesModeSaaS}
-->

<c:choose>
    <c:when test="${not empty useBootstrap}">
        <!DOCTYPE html>
    </c:when>
    <c:otherwise>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    </c:otherwise>
</c:choose>

<html class="no-js">
    <head>
        <!-- <%= request.getLocalName() %> -->
        <!-- <%= System.getProperty("hostname") %> -->
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
        <meta http-equiv="Expires" content="0" />
        <meta name="gwt:property" content="locale=fr_FR" />
        <meta http-equiv="X-UA-Compatible" content="edge"/>

        <tiles:useAttribute name="title" />
        <title><bean:message name="title" /></title>
        <atexo:featureJson/>

        <script type="text/javascript" src="js/modernizr-custom.js"></script>

        <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>

        <%-- Plugin jQuery pour l'encodage de caractères en base64 --%>
        <script language="JavaScript" type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.base64.min.js"></script>

        <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.collapsible.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.widgets.min.js"></script>
        <script type="text/javascript">jQuery.noConflict();</script>

        <script language="JavaScript" type="text/JavaScript" src="js/scripts-commun.js"></script>
        <script language="JavaScript" type="text/JavaScript" src="js/calendrier.js"></script>
        <script language="JavaScript" type="text/JavaScript" src="js/scripts.js"></script>

        <c:if test="${not empty useBootstrap}">
            <link href="front_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="front_modules/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
            <link href="front_modules/bootstrap-table/dist/bootstrap-table.min.css" rel="stylesheet">
            <%-- Le style de chosene et dropzone sont srchargés  --%>
            <link href="front_modules/chosen/chosen.min.css" rel="stylesheet">
            <link href="front_modules/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
            <%-- Adaptation de bootstrap  --%>
            <c:if test="${empty frontVersion}">
                <link rel="stylesheet" href="<atexo:href href='css/bootstrap-custom.css'/>" type="text/css" />
            </c:if>
        </c:if>

        <c:if test="${empty useBootstrap and empty jQuery_3 or frontVersion eq 1}">
            <script language="JavaScript" src="js/scriptaculous/lib/prototype.js" type="text/javascript"></script>
            <script language="JavaScript" src="js/scriptaculous/src/scriptaculous.js" type="text/javascript"></script>
        </c:if>

        <c:if test="${empty frontVersion or frontVersion eq 0 or frontVersion eq 1}">
            <link rel="stylesheet" href="css/jquery-ui.css">
        </c:if>

        <%--@elvariable id="classBody" type="java.lang.String"--%>
        <c:if test="${not empty jQuery_3 and frontVersion eq 2}">
            <!-- NTY - Nikolay Tyurin : JQuery + Boostrap + JsTree + SummerNote -->
            <link rel="stylesheet" href="webjars/jstree/3.3.3/themes/default/style.min.css"/>
            <link rel="stylesheet" href="webjars/summernote/0.8.2/dist/summernote.css">

            <script src="webjars/jquery/3.2.1/jquery.min.js"></script>
            <script src="webjars/jstree/3.3.3/jstree.min.js"></script>

            <%-- inclue directement sur la page des Actualités --%>
            <!--<script src="webjars/summernote/0.8.2/src/js/summernote.js"></script>-->
            <!-- NTY - Nikolay Tyurin : JQuery + Boostrap + JsTree + SummerNote -->
        </c:if>

        <c:if test="${not empty vueJs}">
            <!-- VUE.JS -->
            <script src="webjars/vue/2.5.16/vue.min.js"></script>
            <script src="webjars/vue-resource/1.3.4/dist/vue-resource.min.js"></script>
            <!--<script src="webjars/vue-router/3.0.1/dist/vue-router.min.js"></script>-->

            <!--<script src="https://cdn.jsdelivr.net/npm/vue"></script>-->
            <!--<script src="https://cdn.jsdelivr.net/npm/vue-resource"></script>-->
        </c:if>

        <link rel="stylesheet" href="<atexo:href href='css/ng-style.css'/>" type="text/css"/>
        <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>

        <c:if test="${not empty useBootstrap}">
            <%-- Adaptation de bootstrap  --%>
            <c:if test="${empty frontVersion or frontVersion eq 0 or frontVersion eq 1}">
                <link rel="stylesheet" href="<atexo:href href='css/bootstrap-rsem.css'/>" type="text/css" />
            </c:if>
        </c:if>

        <link rel="stylesheet" href="<atexo:href href='css/toggle-switch.css'/>" type="text/css"/>
        <%-- Ne doit pas être utilisé avec bootstrap --%>
        <c:if test="${empty useBootstrap}">
            <c:if test="${empty frontVersion or frontVersion eq 0 or frontVersion eq 1}">
                <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css" />
                <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css" />
            </c:if>
        </c:if>

        <link rel="stylesheet" href="<atexo:href href='css/calendrier.css'/>" type="text/css" />

        <c:set var="activationIframe">
            <atexo:propriete clef='activation.iframe'/>
        </c:set>
        <c:if test="${ activationIframe == 'true'}">
            <c:set scope="session" var="accesModeSaaS" value="accesModeSaaS"/>
        </c:if>

        <c:choose>
            <c:when test="${!empty accesModeSaaS}">
                <script type="text/javascript">
                    function noBackParentResize() { miseAJourTailleIFrameTimeout(); miseAJourTailleIFrameSurLien(); }
                    window.onload = noBackParentResize;
                    window.onresize = miseAJourTailleIFrame;
                </script>
            </c:when>
            <c:otherwise>
                <script type="text/javascript">
                    function noBack() { window.history.forward(); }
                    noBack();
                    window.onload = noBack;
                    window.onpageshow = function(evt) { if(evt.persisted) noBack(); };
                    window.onunload = function() { void(0); };
                </script>
            </c:otherwise>
        </c:choose>

        <script type="text/javascript">
            hrefRacine = '<atexo:href href=""/>';
            var prefixeUrlGWT = hrefRacine;
        </script>
        <![CDATA[Version @version@]]>
    </head>

    <body <c:if test="${!empty accesModeSaaS}"> id="iframe-service" </c:if> class="${classBody}">
        <c:if test="${empty accesModeSaaS}">
            <tiles:insertAttribute name="header" />
        </c:if>

        <tiles:insertAttribute name="body" />

        <c:if test="${empty accesModeSaaS}">
            <tiles:insertAttribute name="footer" />
        </c:if>

        <c:if test="${!empty accesModeSaaS}">
            <script type="text/javascript">miseAJourTailleIFrame();</script>
        </c:if>

        <c:if test="${empty accesModeSaaS or not empty jQuery_3 or frontVersion eq 2}">
            <script type="text/javascript" src="<atexo:href href='lib/bootstrap/bootstrap.min.js' />"></script>
            <script type="text/javascript" src="<atexo:href href='lib/moment/moment.min.js' />"></script>
            <script type="text/javascript" src="<atexo:href href='lib/moment/locale/fr.js' />"></script>
            <script type="text/javascript" src="<atexo:href href='lib/datetimepicker/bootstrap-datetimepicker.min.js' />"></script>
            <script type="text/javascript" src="<atexo:href href='js/ng-script.js' />"></script>
        </c:if>
        <%--
        <c:if test="${empty accesModeSaaS and empty acceptMultiWindow}">
            <script type="text/javascript" src="js/multiWindowManage.js"></script>
        </c:if>
        --%>
    </body>
</html>
