

export function isoDateToDate(isoDateAsString) {
        if (isoDateAsString == null || isoDateAsString.trim().length == 0) {
                return null;
        }
        return new Date(isoDateAsString.trim());
}

export function formatIsoDate(isoDateAsString: string, format: string): string {
        let date = isoDateToDate(isoDateAsString);
        if (date == null) {
                return null;
        }
        return moment(date).format(format);
}

export function getDateFromInput(inputSelector: string, addTime: boolean): string {
        let dateAsString = jQuery(inputSelector).val();
        if (dateAsString != null && dateAsString.trim().length > 0) {
                dateAsString = moment(dateAsString.trim(), "DD/MM/YYYY").format("YYYY-MM-DD");
                if (addTime) {
                        let timeAsString = moment().format("HH:mm:ssZ");
                        dateAsString = dateAsString + "T" + timeAsString;
                }
                return dateAsString;

        } else {
                return null;
        }
}
