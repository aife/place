<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>
<%@ taglib prefix="atexo" uri="AtexoTag"  %>

<div id="menu">
			<ul id="menuList">
				<li class="menu-top">
					<span id="gestionProcedures"><a href="<atexo:propriete clef="contexte.passation" />/main.epm">
						<bean:message bundle="commun" key="header.gestion.procedures" />
					</a></span>
				</li>
                <li class="menu">
                	<span id="redaction"><a href="<atexo:propriete clef="contexte.redaction" />/main.epm">
	                	<bean:message bundle="commun" key="header.redaction.pieces" />
	                </a></span>
                </li>
                <li class="menu">
                	<span id="commission"><a href="<atexo:propriete clef="contexte.commission" />/main.epm">
						<bean:message bundle="commun" key="header.gestion.commissions" />
					</a></span>
                </li>
                <li class="menu">
                	<span id="baseDocumentaire"><a href="javascript:popUpSetSize('<atexo:propriete clef="contexte.baseDocumentaire" />',1024,600,'yes');">
                		<bean:message bundle="commun" key="header.base.documentaire" />
                	</a></span>
                </li>
                <li class="menu">
                    <span id="statistiques"><a href="<atexo:propriete clef="contexte.infocentre" />/main.epm">
                        <bean:message bundle="commun" key="header.base.infocentre" />
                    </a></span>
                </li>
                <li class="menu">
                	<span id="administration"><a href="<atexo:propriete clef="contexte.administration" />/main.epm">
                		<bean:message bundle="commun" key="header.administration" />
                	</a></span>
                </li>
    </ul>
</div>