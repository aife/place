import {Pipe} from 'angular2/src/core/metadata';
import {Injectable, PipeTransform} from 'angular2/core';

@Pipe({name: 'fileSize'})
@Injectable()
export class FileSizePipe implements PipeTransform {
    // FROM  http://scratch99.com/web-development/javascript/convert-bytes-to-mb-kb/
    transform(value:number, args:Array<any> = null):string {
        let units = [' Octets', ' KB', ' MB', ' GB', ' TB'];

        if (isNaN(value)) {
            return "0";
        }
        var amountOf2s = Math.floor(Math.log(+value) / Math.log(2));
        if (amountOf2s < 1) {
            amountOf2s = 0;
        }
        var i = Math.floor(amountOf2s / 10);

        value = +value / Math.pow(2, 10 * i);

        // arrondi 2 chiffres après la virgule
        if (value.toString().length > value.toFixed(2).toString().length) {
            value = value.toFixed(2);
        }
        return value + units[i];
    }
}

@Pipe({name: 'stringISODateToDate'})
@Injectable()
export class StringISODateToDatePipe implements PipeTransform {
    transform(value:string, args:Array<any> = null):Date {
        let date = null;
        try {
            if (value) {
              return new Date(value);
            }
        } catch (e) {
        }
        return date;
    }
}


@Pipe({name: 'formatDate'})
@Injectable()
export class FormatDatePipe implements PipeTransform {
    transform(value:Date, args:Array<any> = null):string {
        let date = null;
        try {
            if (value) {
                let pattern:string = args != null && args.length > 0 ? args[0] : 'DD/MM/YYYY';
                date = moment(value).format(pattern);
            }
        } catch (e) {
        }
        return date;
    }
}


@Pipe({name: 'currencyEuro'})
export class CurrencyEuroPipe implements PipeTransform {
    transform(value:number, args:Array<any> = null):string {
        if (value && !isNaN(value)) {
            return value.toLocaleString("fr", {
                style: "currency",
                currency: "EUR",
                minimumFractionDigits: 2
            })
        }
        return '0,00 €';
    }
}

@Injectable()
@Pipe({name: 'timestampToDate'})
export class TimestampToDatePipe implements PipeTransform {
    transform(value:number, args:Array<any> = null):string {
        let date = null;
        try {
            date = new Date(value);
        } catch (e) {
        }
        return date;
    }
}


@Pipe({name: 'newlineToBreakline'})
@Injectable()
export class NewlineToBreaklinePipe implements PipeTransform {
    transform(value:string, args:Array<any> = null):string {
        if (value != null) {
            return value.replace(/(\r\n|\n|\r)/gm, "<br/>")
        }
        return null;
    }
}
