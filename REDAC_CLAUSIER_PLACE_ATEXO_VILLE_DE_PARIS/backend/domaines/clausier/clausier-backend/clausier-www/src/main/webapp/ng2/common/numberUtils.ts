
export function getMontantValid(value : any )  : string {
		   
  if( value == null) {
    return null;
  }
    
  if(!(value instanceof String)) {
      value = value+"";
  } 
  
  value = value.replace(" ","");
  value = value.replace(",",".");  
  
  if (isNaN(value)) {
    return null;
  }  
  
  return value;
}