import {AfterViewInit, Component, Input} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';

@Component({
    selector: 'ltexec-contact-link',
    templateUrl: 'ng2/widget/contactLink.html',
    directives: [CORE_DIRECTIVES]
})
export class ContactLinkComponent implements AfterViewInit {

    @Input() contact:Object;

    static identifier_:number = 0;

    identifier:number = 0;

    constructor() {
        ContactLinkComponent.identifier_++;
        this.identifier = ContactLinkComponent.identifier_;
    }

    ngAfterViewInit() {

        jQuery(`#detail_contact_${this.identifier}`).popover({
            html: true,
            content: function () {
                var content = jQuery(this).attr("id") + '_popover';
                return jQuery('#' + content).children(".popover-body").html();
            },
            title: function () {
                var content = jQuery(this).attr("id") + '_popover';
                return jQuery('#' + content).children(".popover-heading").html();
            }

        }).on('click', function (e) {
            e.preventDefault();
        })
    }
}
