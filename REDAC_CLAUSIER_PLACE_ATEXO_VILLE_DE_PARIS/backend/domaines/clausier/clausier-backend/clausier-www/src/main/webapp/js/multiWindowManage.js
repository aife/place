
//jQuery(document).ready(function () {
    var statusWindow = document.getElementById('status');

    (function (win) {
        //Переменные Private
        var _LOCALSTORAGE_KEY = 'WINDOW_VALIDATION';
        var RECHECK_WINDOW_DELAY_MS = 100;
        var _initialized = false;
        var _isMainWindow = false;
        var _unloaded = false;
        var _windowArray;
        var _windowId;
        var _isNewWindowPromotedToMain = false;
        var _onWindowUpdated;

        function WindowStateManager(isNewWindowPromotedToMain, onWindowUpdated) {
            //this.resetWindows();
            _onWindowUpdated = onWindowUpdated;
            _isNewWindowPromotedToMain = isNewWindowPromotedToMain;
            _windowId = Date.now().toString();

            bindUnload();

            determineWindowState.call(this);

            _initialized = true;

            _onWindowUpdated.call(this);
        }

        // Определяем окно : если это основное окно или нет
        function determineWindowState() {
            //console.log('determineWindowState');
            var self = this;
            var _previousState = _isMainWindow;

            _windowArray = localStorage.getItem(_LOCALSTORAGE_KEY);

            if (_windowArray === null || _windowArray === "NaN") {
                _windowArray = [];
            } else {
                _windowArray = JSON.parse(_windowArray);
            }

            if (_initialized) {
                // Определяем, нужно ли продвигать это окно
                if (_windowArray.length <= 1 ||
                    (_isNewWindowPromotedToMain ? _windowArray[_windowArray.length - 1] : _windowArray[0]) === _windowId) {
                    _isMainWindow = true;
                } else {
                    _isMainWindow = false;
                }
            } else {
                if (_windowArray.length === 0) {
                    _isMainWindow = true;
                    _windowArray[0] = _windowId;
                    localStorage.setItem(_LOCALSTORAGE_KEY, JSON.stringify(_windowArray));
                } else {
                    _isMainWindow = false;
                    _windowArray.push(_windowId);
                    localStorage.setItem(_LOCALSTORAGE_KEY, JSON.stringify(_windowArray));
                }
            }

            //Если состояние окна было обновлено, вызвать обратный вызов
            if (_previousState !== _isMainWindow) {
                _onWindowUpdated.call(this);
            }

            // Выполнить перепроверку окна (с задержкой)
            setTimeout(function () {
                determineWindowState.call(self);
            }, RECHECK_WINDOW_DELAY_MS);
        }

        // Удаляем окно из списка
        function removeWindow() {
            console.log('removeWindow');
            var __windowArray = JSON.parse(localStorage.getItem(_LOCALSTORAGE_KEY));
            for (var i = 0, length = __windowArray.length; i < length; i++) {
                if (__windowArray[i] === _windowId) {
                    __windowArray.splice(i, 1);
                    break;
                }
            }
            // Обновляем локальное хранилище новым массивом
            localStorage.setItem(_LOCALSTORAGE_KEY, JSON.stringify(__windowArray));
        }

        // Связать события выгрузки
        function bindUnload() {
            win.addEventListener('beforeunload', function () {
                if (!_unloaded) {
                    removeWindow();
                }
            });
            win.addEventListener('unload', function () {
                if (!_unloaded) {
                    removeWindow();
                }
            });
        }

        WindowStateManager.prototype.isMainWindow = function () {
            return _isMainWindow;
        };

        WindowStateManager.prototype.resetWindows = function () {
            localStorage.removeItem(_LOCALSTORAGE_KEY);
        };

        win.WindowStateManager = WindowStateManager;
    })(window);

    var WindowStateManager = new WindowStateManager(true, windowUpdated);

    function windowUpdated() {
        //"this" является ссылкой на WindowStateManager
        //statusWindow.className = (this.isMainWindow() ? 'main' : 'child');
        if (this.isMainWindow() === false) {
            //alert("Это не основное окно")
            console.log('Pas fenêtre principale');
            location.href = "errorMultiWindow.htm";
        } else {
            //alert("Это основное окно")
            console.log('Fenêtre principale');
        }

    }
    //Сбрасывает счетчик, если в коде что-то идет не так
    //WindowStateManager.resetWindows()
//});
