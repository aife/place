<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="breadcrumb"><tiles:getAsString name="breadcrumb"/></c:set>
<c:set var="actionPath"><tiles:getAsString name="actionPath"/></c:set>
<c:set var="formId"><tiles:getAsString name="formId"/></c:set>
<c:set var="messageNoResult"><tiles:getAsString name="messageNoResult"/></c:set>

<div class="main-part">
    <div class="atx-breadcrumbs breadcrumbs">
        <atexo:surchargeTitreEcran libelleParDefault="${breadcrumb}"/>
    </div>

    <form:form action="${actionPath}" modelAttribute="rechercheBean" method="get" id="${formId}">
        <form:errors cssClass="form-bloc-erreur msg-erreur"/>

        <tiles:insertAttribute name="critereRecherche"/>

        <c:choose>
            <c:when test="${not empty resultats.resultats}">
                <div class="atx-pagination">
                    <div class="clearfix">
                        <div class="nbr-result pull-left">
                            <h2 class="h5">
                                <c:out value="${resultats.nbResultats}"/>&nbsp;
                                <spring:message code="recherche.resultats"/>
                            </h2>
                        </div>

                        <div class="pagination form-inline pull-right">

                            <div class="form-group interval">
                                <label for="interval" class=""><spring:message code="recherche.afficher"/>&nbsp;</label>
                                <form:select id="interval" class="form-control" path="intervalle" items="${intervalles}"
                                             onchange="gotoFirstPage()"/>
                            </div>

                            <div class="form-group">
                                <label class=" s-ml s-mr">
                                    <spring:message code="recherche.resultatsParPage"/>
                                </label>
                            </div>

                            <div class="form-group">
                                <form:hidden path="numeroPage"/>
                                <form:hidden path="triPropriete"/>
                                <form:hidden path="triCroissant"/>

                                <nav aria-label="pagination" class="pull-left">
                                    <ul class="pager">
                                        <li>
                                            <a href="javascript:previousPage();">
                                                <spring:message code="recherche.precedent"/>
                                            </a>
                                        </li>
                                        <li>
                                                ${rechercheBean.numeroPage} / ${resultats.nbPages}
                                        </li>
                                        <li>
                                            <a href="javascript:nextPage();">
                                                <spring:message code="recherche.suivant"/>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>

                            </div>
                            <script type="application/javascript">
                                var form = document.forms[0];
                                function nextPage() {
                                    var pageActuelle = jQuery('#numeroPage').val();
                                    if (pageActuelle < ${resultats.nbPages}) {
                                        jQuery('#numeroPage').val(++pageActuelle);
                                        document.forms[0].submit();
                                    }
                                }
                                function previousPage() {
                                    var pageActuelle = jQuery('#numeroPage').val();
                                    if (pageActuelle > 1) {
                                        jQuery('#numeroPage').val(--pageActuelle);
                                        document.forms[0].submit();
                                    }
                                }
                                function sortBy(property) {
                                    jQuery('#triPropriete').val(property);
                                    jQuery('#triCroissant').val('false' === jQuery('#triCroissant').val());
                                    gotoFirstPage();
                                }
                                function gotoFirstPage() {
                                    jQuery('#numeroPage').val(1);
                                    document.forms[0].submit();
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <tiles:insertAttribute name="tableauResultats"/>
            </c:when>
            <c:otherwise>
                <div class="alert alert-warning" role="alert"><spring:message code="${messageNoResult}"/></div>
            </c:otherwise>
        </c:choose>
    </form:form>

    <tiles:insertAttribute name="footer"/>
</div>

<!-- Infos Bulles -->
<div id="infoBulle" class="info-bulle" onmouseover="mouseOverInfoBulle();" onmouseout="mouseOutInfoBulle();">
</div>