<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 12/03/19
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="AtexoTag" prefix="atexo"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <link rel="stylesheet" href="<atexo:href href='css/styles.css'/>" type="text/css"/>
        <link rel="stylesheet" href="<atexo:href href='css/common.css'/>" type="text/css"/>
        <link rel="stylesheet" href="<atexo:href href='css/styles-spec.css'/>" type="text/css"/>
    </head>

    <body>
        <jsp:include page="modalTaskWorker.jsp" />
    </body>
</html>
