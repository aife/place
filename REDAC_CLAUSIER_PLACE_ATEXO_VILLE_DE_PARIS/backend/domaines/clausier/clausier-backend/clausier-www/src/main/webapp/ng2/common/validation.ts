import {Binding, Component, Directive, Host} from "angular2/core";
import {CORE_DIRECTIVES, NG_VALIDATORS, NgForm} from "angular2/common";
import * as NumberUtils from "./numberUtils";

function moneyValidator(c):{[key: string]: boolean} {
    let value = c.value;
    if (value == null) {
        return null;
    }
    value = NumberUtils.getMontantValid(value);
    if (value == null) {
        return {"invalidMoney": true};
    } else {
        return null;
    }
}

const moneyValidatorBinding =
    new Binding(NG_VALIDATORS, {toValue: moneyValidator, multi: true});

@Directive({selector: '[money]', bindings: [moneyValidatorBinding]})
export class MoneyValidator {

}

function dateValidator(c):{[key: string]: boolean} {
    let value = c.value;
    if (value == null || value.length == 0) {
        return null;
    }

    if (!moment(value, "DD/MM/YYYY").isValid()) {
        return {"invalidDate": true};
    } else {
        return null;
    }
}

const dateValidatorBinding =
    new Binding(NG_VALIDATORS, {toValue: dateValidator, multi: true});

@Directive({selector: '[date]', bindings: [dateValidatorBinding]})
export class DateValidator {

}

@Component({
    selector: 'show-error', inputs: ['controlPath: control', 'errorTypes: errors'],
    template: `
    <span *ngIf="errorMessage !== null">{{errorMessage}}</span>
  `,
    directives: [CORE_DIRECTIVES]
})
export class ShowError {

    formDir;
    controlPath:string;
    errorTypes:string[];

    constructor(@Host() formDir:NgForm) {
        this.formDir = formDir;
    }

    get errorMessage():string {
        var control = this.formDir.form.find(this.controlPath);

        if (isPresent(control) && !control.pristine) {
            for (var i = 0; i < this.errorTypes.length; ++i) {
                if (control.hasError(this.errorTypes[i])) {
                    return this._errorMessage(this.errorTypes[i]);
                }
            }
        }
        return null;
    }

    _errorMessage(code:string):string {
        var config = {
            'required': 'Ce champ est obligatoire',
            'invalidMoney': 'Le montant est invalide',
            'invalidDate': 'La date est invalide'
        };
        return config[code];
    }
}

export function isEmpty(field:any) {
    return isBlank(field) || field.length === 0;
}


export function emailValidator(c):{[key: string]: boolean} {

    let value = c.input;
    if (value == null || value.length == 0) {
        return null;
    }

    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (!re.test(value)) {
        return {"invalidDate": true};
    } else {
        return null;
    }
}

export function isValidEmail(email:string):boolean {

    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (!re.test(email)) {
        return false
    } else {
        return true;
    }
}

export function isPresent(obj:any):boolean {
    return obj !== undefined && obj !== null;
}

export function isBlank(obj:any):boolean {
    return obj === undefined || obj === null;
}

export function isString(obj:any):boolean {
    return typeof obj === "string";
}

export function isFunction(obj:any):boolean {
    return typeof obj === "function";
}

export function isType(obj:any):boolean {
    return isFunction(obj);
}

export function isStringMap(obj:any):boolean {
    return typeof obj === 'object' && obj !== null;
}

export function isArray(obj:any):boolean {
    return Array.isArray(obj);
}

export function isNumber(obj):boolean {
    return typeof obj === 'number';
}

export function isDate(obj):boolean {
    return obj instanceof Date && !isNaN(obj.valueOf());
}
