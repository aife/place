<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="AtexoTag" prefix="atexo" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>

<atexo:filtreSecurite egal="true" role="ROLE_profil">
    <c:set var="adminProfilEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_utilisateur">
    <c:set var="adminUtilisateurEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<%--sous menu BOAMP--%>
<atexo:filtreSecurite egal="true" role="ROLE_compteBOAMP">
    <c:set var="adminBOAMPEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_administrerChampsAAPC">
    <c:set var="adminAAPCEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_referentielSupportExterne">
    <c:set var="adminRefSupExternEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<%--menu documents--%>
<atexo:filtreSecurite egal="true" role="ROLE_gestionDocumentModele">
    <c:set var="adminGestDocEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_gestionDocumentModele">
    <atexo:parametrage clef="activation.module.execution" testEgal="true">
        <c:set var="adminDocExecEnabled" value="true" scope="request" />
    </atexo:parametrage>
</atexo:filtreSecurite>
<%--menu documents execution
habilitation: ROLE_gestionDocumentModele && parametrage :activation.module.execution
--%>

<atexo:filtreSecurite egal="true" role="ROLE_infoBulle">
    <c:set var="adminInfoBulleEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_fichePratique">
    <c:set var="adminFichePratiqueEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<%--menu Ref code achats
habilitation: ROLE_administrerCodesAchat && parametrage :activationCodeAchat.administration
--%>

<atexo:filtreSecurite egal="true" role="ROLE_administrerCodesAchat">
    <atexo:parametrage clef="activationCodeAchat.administration" testEgal="true">
        <c:set var="adminCodeAchatEnabled" value="true" scope="request" />
    </atexo:parametrage>
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_annonceAccueil">
    <c:set var="adminActualitesEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_administrerLesProcedures">
    <c:set var="adminProcedureEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_organigrammeDirection">
    <c:set var="adminOrganigrameEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_biCles">
    <c:set var="adminBiClesEnabled" value="true" scope="request" />
</atexo:filtreSecurite>

<atexo:filtreSecurite egal="true" role="ROLE_administrerOperationUniteFonctionnelle">
    <c:set var="adminOperationUniteFctEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_administrerImputationBudgetaire">
    <c:set var="adminImputBugetEnabled" value="true" scope="request" />
</atexo:filtreSecurite>
<atexo:filtreSecurite egal="true" role="ROLE_administrerDemandeAchat">
    <atexo:parametrage clef="activation.demandeAchat" testEgal="true">
        <c:set var="adminDemandeAchatEnabled" value="true" scope="request" />
    </atexo:parametrage>
</atexo:filtreSecurite>

<div class="atx-header active-affix" atx-class-affix="affix-header" atx-offset-top="72">

    <div class="atx-header_top">
        <div class="clearfix">
            <div class="pull-right">
                <div class="subnav clearfix">

                    <div class="atx-notification pull-left" atx-toggle="cron"
                         atx-cron-target="api/notificationsParUtilisateur/"
                         atx-cron-callback="ATX.header.notification.update"
                         atx-cron-intervals="60000"
                         atx-notification-item-update="api/mettreAJourNotification">
                        <a href="#" id="AtxDropdownAlertBody" class="subnav_item subnav-notification dropdown-toggle"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell-o"></i>
                            <span class="badge notification-danger hide">0</span>
                            <!--audio id="atx-notification-sound"
                                   atx-src="http://192.168.2.163/rsem_chart/sond/another-hand-bell.mp3"
                                   src="http://192.168.2.163/rsem_chart/sond/another-hand-bell.mp3"
                                   preload="preload"></audio-->
                        </a>
                        <div class="atx-notification-body dropdown-menu dropdown-menu-right" id="atx-notification-body"
                             aria-labelledby="AtxDropdownAlertBody">
                            <div class="notification-body_header">Notifications</div>
                            <ul class="notification-body_list no-items">
                                <li class="notification-item">
                                    <div class="item">
                                        <div class="vertical-align">
                                            <div class="col-md-12 text-center text-primary">
                                                <bean:message bundle="commun" key="header.notification.null"/>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="notification-body_list with-item hide"></ul>
                            <div class="notification-body_footer clearfix">
                                <a  href="#" id="btn-markAllAsRead" class="btn btn-primary btn-sm pull-right" data-href="api/mettreAJourToutesLesNotifications">
                                    <bean:message bundle="commun" key="header.notification.vider"/>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="subnav_item date">
                        <script type="text/javascript">writeDate();</script>
                    </div>

                    <atexo:feature profile="base-fournisseur">
                        <a href="<atexo:propriete clef="contexte.passation" />/baseFournisseur.htm"
                           class="subnav_item subnav-baseFournisseur" name="baseFournisseur" target="_blank"
                           data-toggle="tooltip" data-placement="bottom" title="Base fournisseur">
                            <i class="fa fa-users hvr-fade"></i>
                        </a>
                    </atexo:feature>

                    <a href="<atexo:propriete clef="contexte.baseDocumentaire" />"
                       class="subnav_item subnav-base-documentaire" name="base-documentaire" target="_blank"
                       data-toggle="tooltip" data-placement="bottom"
                       title="<bean:message bundle="commun" key="header.base.documentaire" />">
                        <i class="fa fa-files-o hvr-fade"></i>
                    </a>

                    <div class="subnav_item">
                        <div class="subnav-admin">
                            <a href="javascript: return false;" class="subnav-administration" name="administration"
                               data-toggle="tooltip" data-placement="bottom"
                               title="<bean:message bundle="commun" key="header.administration" />">
                                <i class="fa fa-cog hvr-fade"></i>
                            </a>

                            <div class="subnav-admin_wrap">
                                <div class="items clearfix">
                                    <div class="row">
                                        <div class=" col-md-12 items-list">
                                            <c:set var="disabled" value="${(adminProfilEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-1">
                                                <div class="fa fa-sliders fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.profil"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminUtilisateurEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-2">
                                                <div class="fa fa-user fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.utilisateur"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminOrganigrameEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-5">
                                                <div class="fa fa-share-alt fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.organigramme"/></div>
                                            </div>

                                            <c:set var="disabled" value="${(adminBOAMPEnabled == 'true' || adminAAPCEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item item-boamp ${disabled}" atx-subnav="subnav-admin-3">
                                                <div class="fa fa-boamp fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.compteBOAMP"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sub-items-list">
                                        <c:if test="${adminProfilEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-1">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listProfiles.htm"><spring:message code="administration.menuGauche.profil.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminUtilisateurEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-2">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listUsers.htm"><spring:message code="administration.menuGauche.utilisateur.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminOrganigrameEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-5">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/organigramme.htm"><spring:message code="administration.menuGauche.organigramme.consulter"/></a>
                                                    </li>
                                                    <c:if test="${adminBiClesEnabled == 'true'}">
                                                        <li>
                                                            <a href="/epm.administration/listBicles.htm"><spring:message code="administration.menuGauche.organigramme.gererBiCles"/></a>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminBOAMPEnabled == 'true' || adminAAPCEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-3">
                                                <ul>
                                                    <c:if test="${adminBOAMPEnabled == 'true'}">
                                                        <li>
                                                            <a href="/epm.administration/listBoampAccounts.htm"><spring:message code="administration.menuGauche.compteBOAMP.consulter"/></a>
                                                        </li>
                                                    </c:if>
                                                    <c:if test="${adminAAPCEnabled == 'true'}">
                                                        <li>
                                                            <a href="/epm.administration/showChampsAAPC.htm"><spring:message code="administration.menuGauche.compteBOAMP.champsAAPC"/></a>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="items clearfix">
                                    <div class="row">
                                        <div class=" col-md-12 items-list">
                                            <c:set var="disabled" value="${(adminOperationUniteFctEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-10">
                                                <div class="fa fa-list-ul fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.pouvoirAdjudicateur"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminProcedureEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-6">
                                                <div class="fa fa-list-alt fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.procedurePassation"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminGestDocEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-13">
                                                <div class="fa fa-files-o fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.documentModele"/></div>
                                            </div>
                                            <%--document execution--%>
                                            <c:set var="disabled" value="${(adminDocExecEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-14">
                                                <div class="fa fa-file-text fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.documentModeleExecution"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sub-items-list">
                                        <c:if test="${adminOperationUniteFctEnabled==true}">
                                            <div class="sub-items" id="subnav-admin-10">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listPouvoirAdjudicateurs.htm"><spring:message code="administration.menuGauche.pouvoirAdjudicateur.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminProcedureEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-6">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listProcedures.htm"><spring:message code="administration.menuGauche.procedurePassation.consulter"/></a>
                                                    </li>
                                                    <li>
                                                        <a href="/epm.administration/listWorkFlows.htm"><spring:message code="administration.menuGauche.procedurePassation.workFlows"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminGestDocEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-13">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listDocumentsModeles.htm"><spring:message code="administration.menuGauche.documentModele.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <%--pour le doc d'exec il faut verifier encore le parametrage --%>
                                            <c:if test="${adminDocExecEnabled == 'true'}">
                                                <div class="sub-items" id="subnav-admin-14">
                                                    <ul>
                                                        <li>
                                                            <a href="/epm.administration/listDocumentsModelesExecution.htm"><spring:message code="administration.menuGauche.documentModeleExecution.consulter"/></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </c:if>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="items clearfix">
                                    <div class="row">
                                        <div class=" col-md-12 items-list">
                                            <c:set var="disabled" value="${(adminCodeAchatEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-7">
                                                <div class="fa fa-shopping-cart fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.codeAchat"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminOperationUniteFctEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-8">
                                                <div class="fa fa-building-o fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.operationUnite"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminImputBugetEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-11">
                                                <div class="fa fa-list-ul fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.imputationBudgetaire"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminDemandeAchatEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-9">
                                                <div class="fa fa-shopping-basket fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.demandeAchat"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sub-items-list">
                                        <c:if test="${adminCodeAchatEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-7">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listCodesAchat.htm"><spring:message code="administration.menuGauche.codeAchat.consulter"/></a>
                                                    </li>
                                                    <li>
                                                        <a href="/epm.administration/listCodesAchatCPV.htm"><spring:message code="administration.menuGauche.codeAchat.correspondances"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminOperationUniteFctEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-8">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listOperationsUnites.htm"><spring:message code="administration.menuGauche.operationUnite.consulter"/></a></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminImputBugetEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-11">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listImputationsBudgetaire.htm"><spring:message code="administration.menuGauche.imputationBudgetaire.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminDemandeAchatEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-9">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/validationDemandeAchat.htm"><spring:message code="administration.menuGauche.demandeAchat.validation"/></a>
                                                    </li>
                                                    <li>
                                                        <a href="/epm.administration/listChampsAdditionnels.htm"><spring:message code="administration.menuGauche.demandeAchat.champsAdditionnels"/></a>
                                                    </li>
                                                    <li>
                                                        <a href="/epm.administration/listNomenclatures.htm"><spring:message code="administration.menuGauche.demandeAchat.nomenclature"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>

                                <div class="items clearfix">
                                    <div class="row">
                                        <div class=" col-md-12 items-list">
                                            <c:set var="disabled" value="${(adminInfoBulleEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-12">
                                                <div class="fa fa-comments fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.infosBulles"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminActualitesEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-4">
                                                <div class="fa fa-newspaper-o fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.actualites"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminFichePratiqueEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-15">
                                                <div class="fa fa-file-code-o fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.fichePratique"/></div>
                                            </div>
                                            <c:set var="disabled" value="${(adminRefSupExternEnabled == 'true') ? '' : 'disabled'}" />
                                            <div class="col-md-3 item ${disabled}" atx-subnav="subnav-admin-16">
                                                <div class="fa fa-cubes fa-3x"></div>
                                                <div class="text"><spring:message code="administration.menuGauche.supportsExterne"/></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sub-items-list">
                                        <c:if test="${adminInfoBulleEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-12">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listInfoBulles.htm"><spring:message code="administration.menuGauche.infosBulles.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminActualitesEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-4">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listActualities.htm"><spring:message code="administration.menuGauche.actualites.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminFichePratiqueEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-15">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listFichesPratiques.htm"><spring:message code="administration.menuGauche.fichePratique.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                        <c:if test="${adminRefSupExternEnabled == 'true'}">
                                            <div class="sub-items" id="subnav-admin-16">
                                                <ul>
                                                    <li>
                                                        <a href="/epm.administration/listPublicities.htm"><spring:message code="administration.menuGauche.supportsExterne.consulter"/></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="<atexo:propriete clef="contexte.passation" />/guidesUtilisateurs.epm" class="subnav_item subnav-administration" name="aide" data-toggle="tooltip" data-placement="bottom" title="<bean:message bundle="commun" key="header.base.guidesUtilisateurs"/>">
                        <i class="fa fa-question hvr-fade"></i>
                    </a>

                    <tiles:insertAttribute name="headerUserInfos"/>
                </div>

            </div>
        </div>
    </div>

    <!--tiles:insertAttribute name="headerMiddle"/-->
    <tiles:insertAttribute name="headerNav"/>

    <script>
        jQuery('#btn-markAllAsRead').click(function () {
            jQuery.get(jQuery(this).data('href'), function () {
                jQuery('.notification-body_list .notification-item').removeClass('new-item').addClass('old-item');
            });
        });
    </script>
</div>