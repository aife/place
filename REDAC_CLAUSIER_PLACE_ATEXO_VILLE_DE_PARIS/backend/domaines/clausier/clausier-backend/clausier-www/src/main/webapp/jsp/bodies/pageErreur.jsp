<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="logic" uri="http://struts.apache.org/tags-logic" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>
<%@ taglib prefix="bean" uri="http://struts.apache.org/tags-bean" %>

<div class="main-part atx-page-error">
	<div align="center">

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="bg-warning atx-message-warning">
						<logic:messagesPresent>
							<html:errors />
						</logic:messagesPresent>
						<logic:messagesNotPresent>
							<bean:message key="erreur.technique" />
							<div class="clearfix">
								<a href="javascript:popUp('exception.epm', 'yes')">Détail</a>
							</div>
						</logic:messagesNotPresent>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
