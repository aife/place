<%@ page language="java"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!--Debut partie centrale-->
<c:if test="${empty accesModeSaaS}">
<div id="middle" class="atx-body">
</c:if>

<c:if test="${empty accesModeSaaS}">
	<!--Debut colonne gauche-->
	<div class="left-part" id="left-part">
		<!--Debut menu -->

		<tiles:insertAttribute name="menuGauche"/>
		
		<!--Fin menu -->
	</div>
	<!--Fin colonne gauche-->
</c:if>
	<!--Debut main-part-->
	<tiles:insertAttribute name="mainPart"/>
	
	<!--Fin main-part-->
	
<c:if test="${empty accesModeSaaS}">	
</div>
</c:if>
<!--Fin partie centrale-->