<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="main-part">
	<div align="center">
		<br>
		<br>
		<br>
		<br>
		<br>
		<spring:message code="erreur.technique"/>
		<c:if test="${not empty stackTrace}">
			<a href="#" id="toggleStackTrace">Détail</a>
			<div class="left stackTrace">
				<textarea cols="150" rows="15" tabindex="0"><c:out value="${stackTrace}"/></textarea>
			</div>
			<script type="application/javascript">
				jQuery('.stackTrace').hide();
				jQuery('#toggleStackTrace').click(
						function() {
							jQuery('.stackTrace').toggle(400);
						}
				);
			</script>
		</c:if>
	</div>
</div>
