<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 28/04/17
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>

<!--Debut download-->
<div class="modal" id="modalDownload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title"><spring:message code="popupDownload.titre" /></h4>
            </div>
            <div class="modal-body">
                <div class="spacer"></div>
                <div id="title" class="text-center"></div>
                <div id="url" class="text-center"><a href="#"></a></div>
                <div id="error" class="text-center"></div>
                <div class="spacer"></div>
            </div>
            <div class="modal-footer">
                <div class="clearfix">
                    <button type="button" id="annuler" class="btn btn-warning btn-sm" data-dismiss="modal">
                        <spring:message code="popupDownload.annuler" />
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Fin download-->

<script type="application/javascript" language="JavaScript" lang="javascript">

    $(document).ready(function() {
        $("#modalDownload").on("show.bs.modal", function (event) {

            var button = $(event.relatedTarget); // Button that triggered the modal

            var modal = $(this);
            modal.find('#title').text(button.data('title')); // Extract info from data-* attributes

            var url = button.data('url');
            if (url != null) {
                modal.find("#url a").attr("href", url).text(button.data('file-name'));
            } else {
                var href = button.data('href');

                modal.find("#error").text("");
                modal.find("#url a").attr("href", "#").text("");

                // initiate an AJAX request and then do the updating in a callback
                $.get(href, function (response) {
                    if (response.result === "success")
                        modal.find("#url a").attr("href", response.data.url).text(response.data.nomFichier);
                    else
                        modal.find("#error").text(response.message);
                    showLoader();
                });
                showLoader();
            }
        });
    });

</script>