<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="StrutsHtml" prefix="html" %>
<%@ taglib uri="AtexoTag" prefix="atexo"%>
<%@ taglib uri="StrutsBean" prefix="bean"%>
<script type="text/javascript">
	 hrefRacine = '<atexo:href href=""/>';
</script>
<!--Debut Bloc de resultats-->
 <form id="resultatRechercheForm" name="resultatRechercheForm" method="get">
<atexo:tableauPagine name="resultatRecherche" url="${codeAchatUrl}" id="resultat" scope="request" elementAfficher="10,20,50">
 <div class="results-list" id="correspondancesNav" style="display:block;">
     <div class="separator"></div>
     <div class="line">
     	<!-- nbResults == intervalle de resultats -->
     	<input type="hidden" id="nbResults" value="10"/>
         <h3><c:out value="${requestScope.NB_RESULT}" /> <bean:message key="codeAchatRecherche.tableau.titreTableau"/></h3>
         <div class="partitioner">
               <<
               <atexo:navigationListe suivant="false" key="codeAchatRecherche.precedent" />
             &nbsp;<atexo:compteurPage />&nbsp;
                <atexo:navigationListe suivant="true" key="codeAchatRecherche.suivant" />
                >>
         </div>
         <div class="breaker"></div>
     </div>
     <table id="resultsList" summary="">
         <thead>
             <tr>
                 <th colspan="3" class="top">
                   <img src="<atexo:href href='nav/images/table-results-top-left.gif'/>" alt="" class="left" />
                   <img src="<atexo:href href='nav/images/table-results-top-right.gif'/>" alt="" class="right" />
                 </th>
             </tr>
             <tr>
             	<atexo:href href="" var="hrefRacine">
	                <th class="code"><div><bean:message key="codeAchatRecherche.tableau.titreCode"/><atexo:colonne img="${hrefRacine}nav/images/arrow-tri.gif" property="codeAchat" /></div></th>
	                <th class="libelle"><div><bean:message key="codeAchatRecherche.tableau.titreLibelle"/><atexo:colonne img="${hrefRacine}nav/images/arrow-tri.gif" property="libelle" /></div></th>
	                <th class="actions"><div><bean:message key="codeAchatRecherche.tableau.titreSelection" /></div></th>
	            </atexo:href>
            </tr>

         </thead>
         <c:forEach var="codeAchat" items="${resultat}" >
         <tr>
             <td class="code">${codeAchat.codeAchat}</td>
             <td class="libelle" >${codeAchat.libelle}</td>
             <td class="actions">
               <div>
               		<!-- Passage par une balise hidden pour passer le probleme des quotes en parametre -->
	               	<input type="hidden" id="codeAchatLibelle_${codeAchat.id}" value="${codeAchat.libelle}"/>
               		<input type="radio" value="${codeAchat.codeAchat}" name="correspondances_code" onclick="javascript:selectionCodeAchat('${codeAchat.codeAchat}', '${codeAchat.id}')" />
               </div>
            </td>
         </tr>
         </c:forEach>
     </table>
     <input type="hidden" id="id_codeAchat" />
     <input type="hidden" id="code_codeAchat" />
     <input type="hidden" id="libelle_codeAchat" />
     <div class="breaker"></div>

   <div class="spacer-small"></div>
</div>
</atexo:tableauPagine>   
</form>
<script>
 function selectionCodeAchat(code, id) {
	 var libelle = document.getElementById('codeAchatLibelle_' + id).value;
     document.getElementById('code_codeAchat').value = code;
     document.getElementById('libelle_codeAchat').value = libelle;
     document.getElementById('id_codeAchat').value = id;
 }
 
</script>
   <!--Fin Bloc de resultats-->