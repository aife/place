<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 05/05/17
  Time: 13:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>

<div class="panel-heading">
    <div class="<%--collapsed--%>" role="button" data-toggle="collapse" data-target="#panel-critres-recherche"
                                    aria-expanded="true" aria-controls="panel-critres-recherche">
        <div class="clearfix">
            <div class="title pull-left">
                <i class="fa fa-minus atx-icon-minus s-mr"></i>
                <i class="fa fa-plus atx-icon-plus s-mr"></i>
                <spring:message code="navigation.critresRecherche" />
            </div>
        </div>
    </div>
</div>
