<%@ page language="java" %>
<div id="menu">

   <div class="menu_section">
       <div class="menutitle">Mon Compte</div>
	   <ul  class="menu_level_1">
	   <li>Niveau 1.1</li>
	   <li>Niveau 1.2
	   	<ul class="menu_level_2">
			<li>Niveau 2.1</li>
			<li>Niveau 2.2</li>
		</ul>
	   </li>
	   <li>Niveau 1.3</li>
	   </ul>
   </div>

   <div class="menu_section">
       <div class="menutitle">Texte du second menu</div>
	   <ul  class="menu_level_1">
	   <li>Niveau 1.1</li>
	   <li>Niveau 1.2
	   	<ul class="menu_level_2">
			<li>Niveau 2.1</li>
			<li>Niveau 2.2</li>
		</ul>
	   </li>
	   <li>Niveau 1.3</li>
	   </ul>
   </div>

</div>