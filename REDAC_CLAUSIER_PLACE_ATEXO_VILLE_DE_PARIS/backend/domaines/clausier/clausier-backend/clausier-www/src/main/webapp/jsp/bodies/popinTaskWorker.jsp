<%--
  Created by IntelliJ IDEA.
  User: nty
  Date: 23/09/18
  Time: 09:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<body onload="javascript:$('.modal').modal({ keyboard: false, backdrop: 'static' })">
    <div class="modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header"></div>
                <div class="modal-body">
                    <jsp:include page="modalTaskWorker.jsp" />
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</body>
