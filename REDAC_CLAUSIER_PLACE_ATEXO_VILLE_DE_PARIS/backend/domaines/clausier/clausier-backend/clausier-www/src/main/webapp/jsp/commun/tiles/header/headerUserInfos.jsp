<%@ page language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="AtexoTag" prefix="atexo" %>

<script type="text/javascript">

    function setCookie(nom, valeur, expire, chemin, domaine, securite) {
        document.cookie = nom + ' = ' + escape(valeur) + '  ' +
            ((expire == undefined) ? '' : ('; expires = ' + expire.toGMTString())) +
            ((chemin == undefined) ? '' : ('; path = ' + chemin)) +
            ((domaine == undefined) ? '' : ('; domain = ' + domaine)) +
            ((securite == true) ? '; secure' : '');
    }

    var nbDeconnexion = 0;

    var urlDeconnexionTab = [
        "<atexo:propriete clef="contexte.administration" />/j_spring_security_logout",
        "<atexo:propriete clef="contexte.commission" />/j_spring_security_logout",
        "<atexo:propriete clef="contexte.passation" />/j_spring_security_logout",
        "<atexo:propriete clef="contexte.infocentre" />/j_spring_security_logout",
        "<atexo:propriete clef="contexte.redaction" />/j_spring_security_logout",
        "<atexo:propriete clef="contexte.execution" />/j_spring_security_logout"
    ];

    jQuery(document).ready(function () {
        // add markup to container and apply click handlers to anchors
        jQuery("#deconnexion").click(function () {
            var dtExpireDel = new Date();
            dtExpireDel.setTime(dtExpireDel.getTime() - 1);
            // en cas de dysfonctionnement -> vérifier le variable "AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY"
            setCookie('remember-me', '', dtExpireDel, '/');

            localHref = window.location.href;
            hrefTab = localHref.split("/");
            nomModule = hrefTab[hrefTab.length - 2];

            jQuery.each(urlDeconnexionTab, function (index, value) {
                if (value.indexOf(nomModule) == -1) {
                    jQuery.ajax({
                        type: 'POST',
                        url: urlDeconnexionTab[index]
                    });
                }
            });
            hrefTab = localHref.split("/");
            hrefTab[hrefTab.length - 1] = 'j_spring_security_logout';
            nomModule = hrefTab[hrefTab.length - 1];
            var url = '';
            for (i = 0; i < hrefTab.length; i++) {
                url += hrefTab[i];
                if (i < hrefTab.length - 1) {
                    url += '/';
                }
            }
            window.location.href = url;
        });

    });
</script>

<div class="subnav_item">

	<logic:notEmpty name="utilisateur2">
	<div class="subnav-user">
		<a href="#" id="AtxDropdownUserInfo" class="subnav_item subnav-alert dropdown-toggle"
		   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="icon fa fa-user"></i>
            <span class="text"><atexo:user key="utilisateur" type="prenom" /> <atexo:user key="utilisateur" type="nom" /></span>
        </a>
        <div class="atx-user-info-body dropdown-menu dropdown-menu-right" id="atx-user-info-body" aria-labelledby="AtxDropdownUserInfo">
            <div class="pull-left">
                <div class="img">
                    <div class="avatar" data-user="<atexo:user key="utilisateur" type="prenom" /> <atexo:user key="utilisateur" type="nom" />"></div>
                </div>
            </div>

            <div class="pull-overflow">
                <div class="info-nom-prenom">
                    <span class="prenom">
                        <atexo:user key="utilisateur" type="prenom" />
                    </span>
                    <span class="nom">
                        <atexo:user key="utilisateur" type="nom" />
                    </span>
                </div>
                <div class="info-other">
                    <strong><atexo:user key="utilisateur" type="service" /></strong>
                </div>
            </div>

            <div class="info-options">
                <a class="btn btn-primary btn-sm compte" role="button" href="<atexo:propriete clef="contexte.administration" />/userAccount.htm">
                    <bean:message bundle="commun" key="header.monCompte"/>
                </a>
                <a class="btn btn-default btn-sm deconnexion" role="button" id="deconnexion" href="#">
                    <bean:message bundle="commun" key="header.deconnexion"/>
                </a>
            </div>
        </div>
    </div>
    </logic:notEmpty>
</div>
