export var defaultItemsCountPerPage:number = 10;

export var DATE_TIME_ISO_FORMAT:string = "YYYY-MM-DDTHH:mm:ss.SSSZZ";
