<%--
  Created by IntelliJ IDEA.
  User: sta
  Date: 12/01/16
  Time: 11:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class='form-bloc'>
    <div class='top'>
        <span class='left'></span><span class='right'></span>
    </div>
    <div class='content'>
        <div class='center'>
            <img src='images/loader.gif' alt='Veuillez patienter' title='Veuillez patienter'/>
        </div>
        <div class='spacer-small'></div>
        <div class='center' id='messageAttente'>
            <spring:message code="${attenteMessage}"/>
        </div>
        <div class='spacer'></div>
    </div>
    <div class='bottom'>
        <span class='left'></span><span class='right'></span>
    </div>
    <div class="boutons">
        <div id="annulerId">
            <div><a href="javascript:void(0)" class="annuler">Fermer</a></div>
        </div>
    </div>
</div>
