package fr.paris.epm.global.commun.worker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Classe abstaite pour toutes les Fabriques des Taches.
 * Created by nty on 12/03/18.
 * @author Nikolay Tyurin
 * @author nty
 */
@Component
public abstract class RsemTaskFactory {

    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    public ApplicationContext getContext() {
        return context;
    }

}