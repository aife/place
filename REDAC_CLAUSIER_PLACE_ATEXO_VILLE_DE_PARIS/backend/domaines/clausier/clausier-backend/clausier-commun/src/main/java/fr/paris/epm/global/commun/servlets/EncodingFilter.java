package fr.paris.epm.global.commun.servlets;

import javax.servlet.*;
import java.io.IOException;

/**
 * Classe EncodingFilter permet d'encoder le contenu d'une rêquete .
 * @author Ramli Tarik.
 * @version $Revision$, $Date$, $Author$
 */

public class EncodingFilter implements javax.servlet.Filter {
    /**
     * Attribut encoding.
     */
    private String encoding;
    /**
     * méthode d'initialisation.
     */
    public void init(final FilterConfig filterConfig)
            throws ServletException {
        encoding = filterConfig.getInitParameter("encoding");
    }
    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(final ServletRequest request,
            final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        request.setCharacterEncoding(encoding);
        filterChain.doFilter(request, response);
    }

    /**
     * destructeur.
     */
    public void destroy() {
    }
}
