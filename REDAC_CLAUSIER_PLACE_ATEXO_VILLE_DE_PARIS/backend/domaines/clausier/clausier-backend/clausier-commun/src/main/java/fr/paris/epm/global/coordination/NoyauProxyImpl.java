package fr.paris.epm.global.coordination;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.noyau.metier.FichePratiqueCritere;
import fr.paris.epm.noyau.metier.InfoBulleCritere;
import fr.paris.epm.noyau.metier.NatureCritere;
import fr.paris.epm.noyau.metier.ParametrageCritere;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class NoyauProxyImpl implements NoyauProxy {

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NoyauProxyImpl.class);

    /**
     * Service de récupération des référentiels.
     */
    @Resource
    private ReferentielsServiceSecurise referentielsService;

    private void charger() {
	    referentielsService.getAllReferentiels();
    }

    @Override
    public final DirectionService getDirService(final Integer id) {
	    return referentielsService.getAllReferentiels().getMapDirService().get(id);
    }

    @Override
    public final Referentiels getReferentiels() {
	    return referentielsService.getAllReferentiels();
    }

    public void initialiserReferentiels() {
	    if (null == getReferentiels()) {
		    charger();
	    }
    }


    @Override
    public EpmTRef getReferentielById( final int id, final TypeEpmTRefObject typeReferentiel) throws TechnicalException {
        initialiserReferentiels();
        Collection<? extends EpmTRef> listReferentiels = null;
        switch (typeReferentiel) {
            case DIRECTION_SERVICE:
                listReferentiels = getReferentiels().getRefDirectionServices();
                break;
            case PROCEDURE:
                listReferentiels = getReferentiels().getRefProcedureLecture();
                break;
            case POUVOIR_ADJUDICATEUR:
                listReferentiels = getReferentiels().getRefPouvoirAdjudicateurTous();
                break;
            case AUTORITE_DELEGANTE:
                listReferentiels = getReferentiels().getRefAutoriteDelegante();
                break;
            case ARTICLE:
                listReferentiels = getReferentiels().getRefArticleTous();
                break;
            case NATURE:
                listReferentiels = getReferentiels().getRefNatureTous();
                break;
            case CCAG:
                listReferentiels = getReferentiels().getRefCcag();
                break;
            case BENEFICIAIRE_MARCHE:
                listReferentiels = getReferentiels().getRefBeneficiaireMarcheListTous();
                break;
            case CODE_ACHAT:
                listReferentiels = getReferentiels().getRefCodeAchat();
                break;
            case IMPUTATION_BUDGETAIRE:
                listReferentiels = getReferentiels().getRefImputationBudgetaire();
                break;
            case CHAMP_ADDITIONNEL:
                listReferentiels = getReferentiels().getRefChampAdditionnel();
                break;
            case NOMENCLATURE:
                listReferentiels = getReferentiels().getRefNomenclature();
                break;
            case CRITERE_ATTRIBUTION:
                listReferentiels = getReferentiels().getRefCritereAttribution();
                break;
            case ARRONDISSEMENT:
                listReferentiels = getReferentiels().getRefArrondissement();
                break;
            case TYPE_DSP:
                listReferentiels = getReferentiels().getRefTypeDsp();
                break;
            case TYPE_CONSULTATION:
                listReferentiels = getReferentiels().getRefTypeConsultation();
                break;
            case DOCUMENT_UTILISE:
                listReferentiels = getReferentiels().getRefDocumentUtilise();
                break;
            case TYPE_CONTRAT_AVENANT:
                listReferentiels = getReferentiels().getRefTypeContratAvenant();
                break;
            case TYPE_OBJET_CONTRAT:
                listReferentiels = getReferentiels().getRefTypeObjetContrat();
                break;
            case TYPE_MARCHE:
                listReferentiels = getReferentiels().getRefTypeMarche();
                break;
            case TYPE_DOCUMENTS:
                listReferentiels = getReferentiels().getRefTypeDocumentContrat();
                break;
            case CATEGORIE_DOCUMENT_CONTRAT:
                listReferentiels = getReferentiels().getRefCategorieDocumentContrat();
                break;
            case STATUT_CONTRAT:
                listReferentiels = getReferentiels().getRefStatutNotificationContrat();
                break;
            case TRANCHE_BUDGETAIRE:
                listReferentiels = getReferentiels().getRefTrancheBudgetaire();
                break;
            case TYPE_AVENANT:
                listReferentiels = getReferentiels().getRefTypeAvenant();
                break;
            case REPONSE_ELECTRONIQUE:
                listReferentiels = getReferentiels().getRefReponseElectronique();
                break;
            case LIEU_EXECUTION:
                listReferentiels = getReferentiels().getRefLieuExecution();
                break;
            case STATUT_ENTREPRISE:
                listReferentiels = getReferentiels().getStatutEntreprise();
                break;
            case STATUT_AVENANT:
                listReferentiels = getReferentiels().getStatutAvenant();
                break;
            case DUREE_DELAI_DESCRIPTION:
                listReferentiels = getReferentiels().getDureeDelaiDescription();
                break;
            case STATUT_CONSULTATION:
                listReferentiels = getReferentiels().getStatut();
                break;
            case ATTRIBUTION:
                listReferentiels = getReferentiels().getRefTypeContrats();
                break;
            case MOYEN_NOTIFICATION:
                listReferentiels = getReferentiels().getMoyenNotification();
                break;
            case NOMBRE_CANDIDATS:
                listReferentiels = getReferentiels().getNbCandidatsAdmisTous();
                break;
            case CHOIX_MOIS_JOUR:
                listReferentiels = getReferentiels().getChoixMoisJour();
                break;
            default:
                break;
        }

        return listReferentiels.stream()
                .filter(ref -> ObjectUtils.defaultIfNull(ref.getId(),0).intValue() == ObjectUtils.defaultIfNull(id,0).intValue())
                .findFirst()
                .orElse(null);
    }


	@Override
	public <T extends EpmTRefImportExport> Optional<T> getReferentielExportableById( final int id, final TypeEpmTRefObject typeReferentiel) throws TechnicalException {
		initialiserReferentiels();
		Collection<T> listReferentiels = null;
		switch (typeReferentiel) {
			case PROCEDURE:
				listReferentiels = (Collection<T>) getReferentiels().getRefProcedureLecture();
				break;
			case NATURE:
				listReferentiels = (Collection<T>) getReferentiels().getRefNatureTous();
				break;
			case STATUT_REDACTION_CLAUSIERS:
				listReferentiels = (Collection<T>) getReferentiels().getRefStatutRedactionClausiers();
				break;
			case CCAG:
				listReferentiels = (Collection<T>) getReferentiels().getRefCcag();
				break;
			default:
				break;
		}

		return listReferentiels.stream()
			.filter(ref -> ref.getId() == id)
			.findFirst();
	}

	@Override
	public <T extends EpmTRefImportExport> Optional<T> getReferentielExportableByUid( final String uid, final TypeEpmTRefObject typeReferentiel) throws TechnicalException {
		initialiserReferentiels();
		Collection<T> listReferentiels = null;
		switch (typeReferentiel) {
			case PROCEDURE:
				listReferentiels = (Collection<T>) getReferentiels().getRefProcedureLecture();
				break;
			case NATURE:
				listReferentiels = (Collection<T>) getReferentiels().getRefNatureTous();
				break;
			case CCAG:
				listReferentiels = (Collection<T>) getReferentiels().getRefCcag();
				break;
			case STATUT_REDACTION_CLAUSIERS:
				listReferentiels = (Collection<T>) getReferentiels().getRefStatutRedactionClausiers();
				break;
			case TYPE_DOCUMENTS:
				listReferentiels = (Collection<T>) getReferentiels().getRefTypeDocuments();
				break;
			case TYPE_DOCUMENT_CONTRAT:
				listReferentiels = (Collection<T>) getReferentiels().getRefTypeDocumentContrat();
				break;
			case TYPE_CLAUSES:
				listReferentiels = (Collection<T>) getReferentiels().getRefTypeClauses();
				break;
			case THEME_CLAUSES:
				listReferentiels = (Collection<T>) getReferentiels().getRefThemeClauses();
				break;
			case REF_POTENTIELLEMENT_CONDITIONNEES:
				listReferentiels = (Collection<T>) getReferentiels().getRefPotentiellementConditionnees();
				break;
			default:
				break;
		}

		return listReferentiels.stream()
			.filter(ref -> ref.getUid().equals(uid))
			.findFirst();
	}

    public EpmTRef getReferentielByReference( final String reference, TypeEpmTRefObject typeReferentiel) throws TechnicalException {
        EpmTRef resultat = null;
        switch (typeReferentiel) {
            case NATURE:
                NatureCritere natureCritere = new NatureCritere();
                natureCritere.setLibelle(reference);
                resultat = referentielsService.chercherUniqueEpmTObject(0, natureCritere);
                break;
            case INFO_BULLE:
                InfoBulleCritere infoBulleCritere = new InfoBulleCritere();
                infoBulleCritere.setLibelle(reference);
                resultat = referentielsService.chercherUniqueEpmTObject(0, infoBulleCritere);
                break;
            case FICHE_PRATIQUE:
                FichePratiqueCritere fichePratiqueCritere = new FichePratiqueCritere();
                fichePratiqueCritere.setLibelle(reference);
                resultat = referentielsService.chercherUniqueEpmTObject(0, fichePratiqueCritere);
                break;
            case PARAMETRAGE:
                ParametrageCritere parametrageCritere = new ParametrageCritere();
                parametrageCritere.setClef(reference);
                resultat = referentielsService.chercherUniqueEpmTObject(0, parametrageCritere);
                break;
            default:
                break;
        }
        return resultat;
    }


    public Collection<EpmTRefProcedure> getProcedureByIdOrganisme(final Integer idOrganisme, final boolean avecTous,
                                                                  final boolean actif) throws TechnicalException {
        initialiserReferentiels();

        Collection<EpmTRefProcedure> procedures;
        if (avecTous)
            procedures = getReferentiels().getRefProcedureTous(actif);
        else
            procedures = actif ? getReferentiels().getRefProcedureEcriture() : getReferentiels().getRefProcedureLecture();

        if (idOrganisme != null) {
            List<EpmTRefProcedure> resultat = procedures.stream()
                    .filter(p -> p.getId() == 0 ||
                            (p.getEpmTRefOrganismeSet() != null && p.getEpmTRefOrganismeSet().stream().anyMatch(o -> o.getId() == idOrganisme)))
                    .collect(Collectors.toList());

            if (resultat.isEmpty() || (resultat.size() == 1 && resultat.get(0).getId() == 0)) {
                resultat = resultat.stream()
                        .filter(p -> p.getEpmTRefOrganismeSet() == null || p.getEpmTRefOrganismeSet().isEmpty())
                        .collect(Collectors.toList());
            }
            procedures = resultat;
        }
        return procedures;
    }

}
