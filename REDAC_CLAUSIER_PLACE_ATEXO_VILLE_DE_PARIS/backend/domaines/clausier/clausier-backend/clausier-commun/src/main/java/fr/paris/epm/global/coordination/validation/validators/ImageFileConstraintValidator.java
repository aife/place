package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.ImageFile;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nty on 05/04/17.
 */
public class ImageFileConstraintValidator implements ConstraintValidator<ImageFile, MultipartFile> {

    @Override
    public void initialize(ImageFile image) {

    }

    private boolean checkExt(String ext) {
        return ext.equals("jpeg") || ext.equals("jpg") || ext.equals("jpe") ||      // JPEG
                ext.equals("jif") || ext.equals("jfif") || ext.equals("jfi") ||     // JPEG
                ext.equals("jp2") || ext.equals("j2k") || ext.equals("jpf") ||      // JPEG2000
                ext.equals("jpx") || ext.equals("jpm") || ext.equals("mj2") ||      // JPEG2000
                ext.equals("tiff") || ext.equals("tif") ||                          // TIFF
                ext.equals("gif") ||                                                // GIF
                ext.equals("bmp") || ext.equals("dib") ||                           // BMP
                ext.equals("png") ||                                                // PNG
                ext.equals("svg") || ext.equals("svgz");                            // SVG
    }

    private boolean checkContentType(String contentType) {
        return contentType.equals("image/jpeg") || contentType.equals("image/tiff") ||
                contentType.equals("image/gif") || contentType.equals("image/bmp") ||
                contentType.equals("image/png") || contentType.equals("image/svg");
    }

    @Override
    public boolean isValid(MultipartFile field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {

            String name = field.getOriginalFilename();
            String ext = name.substring(name.lastIndexOf(".") + 1);

            if (!checkExt(ext))
                return false;

            String contentType = field.getContentType();
            if (!checkContentType(contentType))
                return false;

        }
        return true;
    }

}
