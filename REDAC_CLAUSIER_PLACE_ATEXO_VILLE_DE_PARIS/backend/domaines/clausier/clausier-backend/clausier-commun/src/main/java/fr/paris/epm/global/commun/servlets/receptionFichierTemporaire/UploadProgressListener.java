/**
 * 
 */
package fr.paris.epm.global.commun.servlets.receptionFichierTemporaire;

import org.apache.commons.fileupload.ProgressListener;

/**
 * Calcul de la progression de l'upload.
 * @author Léon Barsamian
 *
 */
public class UploadProgressListener implements ProgressListener {

    private UploadInfo uploadInfo;
    
    public UploadProgressListener(UploadInfo valeurUploadInfo) {
        this.uploadInfo = valeurUploadInfo;
    }
    /* (non-Javadoc)
     * @see org.apache.commons.fileupload.ProgressListener#update(long, long, int)
     */
    public void update(long byteRecu, long byteLu, int pItems) {
        uploadInfo.setStatus("progress");
        long pourcentageLu = Math.round((new Double(byteRecu) / new Double(byteLu)) * 100);
        uploadInfo.setBytesRead(pourcentageLu);
    }

}
