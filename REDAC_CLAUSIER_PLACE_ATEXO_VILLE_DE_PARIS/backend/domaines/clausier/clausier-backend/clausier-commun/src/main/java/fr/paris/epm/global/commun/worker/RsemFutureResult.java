package fr.paris.epm.global.commun.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Classe abstaite pour toutes les Rsem-Taches-Future.
 * Created by nty on 28/06/19.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class RsemFutureResult<T> {
    private static final Logger logger = LoggerFactory.getLogger(RsemFutureResult.class);

    private final Future<T> future;

    private final String resultHref;

    public RsemFutureResult(Future<T> future, String resultHref) {
        this.future = future;
        this.resultHref = resultHref;
    }

    public boolean isDone() {
        if (future == null) {
            logger.error("isDone : The future is null");
            throw new IllegalStateException("The future is null");
        }
        return future.isDone();
    }

    public T get() throws InterruptedException, ExecutionException {
        if (future == null) {
            logger.error("get : The future is null");
            throw new IllegalStateException("The future is null");
        }
        return future.get();
    }

    public String getResultHref() {
        return resultHref;
    }

}
