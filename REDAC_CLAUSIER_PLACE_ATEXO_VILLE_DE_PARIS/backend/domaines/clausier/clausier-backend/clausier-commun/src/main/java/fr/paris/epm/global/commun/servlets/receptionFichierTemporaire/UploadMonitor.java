package fr.paris.epm.global.commun.servlets.receptionFichierTemporaire;

import org.directwebremoting.WebContextFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe de récupération du suivi de l'upload.
 * @author Léon Barsamian
 */
public class UploadMonitor
{
    public UploadInfo getUploadInfo()
    {
        HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();

        if (req.getSession().getAttribute("uploadInfo") != null)
            return (UploadInfo) req.getSession().getAttribute("uploadInfo");
        else {
            return new UploadInfo();
        }   
    }
}
