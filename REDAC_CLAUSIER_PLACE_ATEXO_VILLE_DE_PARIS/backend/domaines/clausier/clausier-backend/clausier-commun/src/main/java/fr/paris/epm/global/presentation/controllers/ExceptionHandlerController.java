package fr.paris.epm.global.presentation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

import static java.lang.String.format;

@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(fr.paris.epm.global.commons.exception.UtilisateurException.class)
    public ResponseEntity<WsMessage> utilisateurExceptionHandler(fr.paris.epm.global.commons.exception.UtilisateurException ex) {
        log.error("utilisateurExceptionHandler {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getMessage())
                .code(HttpStatus.BAD_REQUEST.value())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(fr.paris.epm.global.commons.exception.SessionExpireeException.class)
    public ResponseEntity<WsMessage> sessionExpireeExceptionHandler(fr.paris.epm.global.commons.exception.SessionExpireeException ex) {
        log.error("sessionExpireeExceptionHandler {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getMessage())
                .code(HttpStatus.BAD_REQUEST.value())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(fr.paris.epm.global.commons.exception.TechnicalException.class)
    public ResponseEntity<WsMessage> technicalExceptionHandler(fr.paris.epm.global.commons.exception.TechnicalException ex) {
        log.error("technicalExceptionHandler {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getMessage())
                .code(HttpStatus.BAD_REQUEST.value())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(fr.paris.epm.global.commons.exception.ApplicationException.class)
    public ResponseEntity<WsMessage> applicationExceptionHandler(fr.paris.epm.global.commons.exception.ApplicationException ex) {
        log.error("applicationExceptionHandler {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getMessage())
                .code(HttpStatus.BAD_REQUEST.value())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<WsMessage> handleClientError(HttpClientErrorException ex) {
        log.error("handleClientError {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getResponseBodyAsString())
                .code(ex.getStatusCode().value())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<WsMessage> handleResourceNotFoundException(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .sorted((s, anotherString) -> s != null ? s.compareTo(anotherString) : 0)
                .collect(Collectors.joining(", "));
        log.error("handleResourceNotFoundException {}", message);
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }


    @ExceptionHandler({UnsatisfiedServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {

        final String message = String.join(", ", e.getParamConditions()) + " sont manquant(e)s";
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<WsMessage> handleResourceMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        String message = e.getName() + " est du mauvais type";
        Class<?> requiredType = e.getRequiredType();
        if (requiredType != null) {
            message = e.getName() + " est doit être de type " + requiredType.getName();
        }
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.DATA.getCode())
                .type(ExceptionEnum.DATA.getType())
                .build();
        log.error("handleResourceMethodArgumentTypeMismatchException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.DATA.getCode()));
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(MissingServletRequestParameterException e) {

        final String message = format("Le paramètre '%s' est obligatoire", e.getParameterName());
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(ExceptionEnum.MANDATORY.getCode())
                .type(ExceptionEnum.MANDATORY.getType())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ExceptionEnum.MANDATORY.getCode()));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<WsMessage> handleException(RuntimeException ex) {
        StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        ex.printStackTrace(pw);
        log.error("handleException {}", ex.getMessage());
        log.error("stackException {}", writer.toString());
        WsMessage wsMessage = WsMessage.builder()
                .message("Veuillez contacter un administrateur")
                .code(HttpStatus.BAD_REQUEST.value())
                .type(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

}
