package fr.paris.epm.global.commun;

import java.io.File;

/**
 * Classe à utiliser lors d'un telechargement de document via la popup
 * "telechargement".
 * @author Regis Menet
 */
public class FileTmp extends File {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 8710858082379936589L;

    /**
     * nom du fichier qui sera affiché dans la popup.
     */
    private String nomFichier;

    private String url;

    @Override
    public int compareTo(File fichier) {
        // TODO Auto-generated method stub
        if (fichier instanceof FileTmp) {
            FileTmp fichierTmp = (FileTmp) fichier;
            if (nomFichier != null && fichierTmp.getNomFichier() != null) {
                return nomFichier.compareTo(fichierTmp.getNomFichier());
            }
        }
        return this.compareTo(fichier);
    }

    /**
     * @param pathname
     */
    public FileTmp(String pathname) {
        super(pathname);
    }

    /**
     * @return nom du fichier qui sera affiché dans la popup.
     */
    public final String getNomFichier() {
        return nomFichier;
    }

    /**
     * @param valeur nom du fichier qui sera affiché dans la popup.
     */
    public final void setNomFichier(final String valeur) {
        this.nomFichier = valeur.replaceAll(" ", "_"); // nom de fichier à télécharger ne peutpas contenir des éspaces
    }

    /**
     * @return url
     */
    public final String getUrl() {
        return url;
    }

    /**
     * @param valeur url
     */
    public final void setUrl(final String valeur) {
        this.url = valeur;
    }

}
