package fr.paris.epm.global.coordination.validation.annotations;


import fr.paris.epm.global.coordination.validation.validators.SuperiorConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { SuperiorConstraintValidator.class })
public @interface Superior {

    String superiorOfField();

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
