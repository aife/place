package fr.paris.epm.global.coordination.mapper;

import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.noyau.persistance.EpmTObject;
import org.mapstruct.MappingTarget;

/**
 * Created by nty on 08/01/19.
 */
public interface AbstractEpmObjectToBeanMapper<E extends EpmTObject, T extends Bean> {

    public abstract T toBean(E emTObject);

    public abstract void toBean(E emTObject, @MappingTarget T bean);

}