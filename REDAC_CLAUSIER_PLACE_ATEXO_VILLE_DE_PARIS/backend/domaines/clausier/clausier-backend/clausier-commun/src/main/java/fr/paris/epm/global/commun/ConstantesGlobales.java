package fr.paris.epm.global.commun;

/**
 * Constantes globales.
 *
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class ConstantesGlobales {


    /**
     * Classe utilitaire.
     */
    protected ConstantesGlobales() {
    }

    public static final String SESSION_MANAGER = "SESSION_MANAGER";
    public static final String SESSION_MANAGER_FICHIERS = "SESSION_MANAGER_FICHIER";
    public static final String SESSION_MANAGER_DOCUMENT = "SESSION_MANAGER_DOCUMENT";
    public static final String SESSION_MANAGER_RECHERCHE_TABLEAU = "SESSION_MANAGER_RECHERCHE_TABLEAU";
    public static final String SESSION_VALIDE = "SESSION_VALIDE";

    public static final String ERREUR_DOCUMENT = "ERREUR_DOCUMENT";
    public static final String CONTEXTE = "CONTEXTE";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String CLIENT = "CLIENT";
    public static final String DOCUMENT_URL = "DOCUMENT_URL";
    public static final String REDIRECTION = "REDIRECTION";
    public static final String HABILITATIONS = "HABILITATIONS";
    public static final String CONSULTATION = "CONSULTATION";
    public static final String ID_CONTEXTE_CLAUSIER = "ID_CONTEXTE";

    public static final String PLATEFORME = "PLATEFORME";
    public static final String NIVEAU_INTERVENTION = "NIVEAU_INTERVENTION";
    public static final String AGENT = "AGENT";

    /**
     * identifiant de l'ecran en cours.
     */
    public static final String ID_TOKEN_ECRAN = "idToken";

    /**
     * Utilisateur de présentation.
     */
    public static final String UTILISATEUR_P = "utilisateurP";

    /**
     * identifiant du lot dissocié utilisé dans parametre et session.
     */
    public static final String ID_LOT_DISSOCIE = "idLotDissocie";

    /**
     * Nom du paramètre de requête de l'identifiant de la consultation.
     */
    public static final String S_ID_CONSULTATION = "idConsultation";

    /**
     * Nom du paramètre de requête de l'identifiant de la consultation.
     */
    public static final String S_ID_AVENANT = "idAvenant";

    /**
     * Nom du paramètre de requête de l'identifiant de la consultation.
     */
    public static final String P_ID_CONSULTATION = "idConsultation";

    /**
     * Nom du paramètre de requête de l'identifiant de la consultation.
     */
    public static final String P_ID_AVENANT = "idAvenant";

    /**
     * Constante permettant de récupérer l'objet utilisateur (base de donnée)
     * authentifié en session.
     */
    public static final String UTILISATEUR = "utilisateur";

    /**
     * Constante permettant de récupérer l'objet utilisateur (coordination)
     * authentifié en session.
     */
    public static final String UTILISATEUR_COORDINATION = "utilisateur2";

    /**
     * Expression réguliére pour la vérification d'un nombre.
     */
    public static final String NUMERIC_REGEX = "[0-9]+";

    /**
     * Format de date et heure.
     */
    public static final String DATE_HEURE = "dd/MM/yyyy HH:mm";

    /**
     * Action erreur utilisée dans le cas d'une erreur dans le traitement.
     */
    public static final String ERREUR = "erreur";

    /**
     * Format de date et heure.
     */
    public static final String DATE = "dd/MM/yyyy";

    /**
     * Statut des documents en cours de generations
     */
    public static final String S_DOCUMENT_STATUT = "statutDocument";

    /**
     * Statut lorsque les documents ont ete generes
     */
    public static final String S_DOCUMENT_GENERER = "documentGenerer";

    /**
     * Attribut pour liste des fichier dans la session.
     */
    public static final String S_DOCUMENT_LISTE_FICHIERS = "listeFichiers";

    /**
     * Attribut pour désactiver le timer de la popup d'attente.
     */
    public static final String S_DOCUMENT_DESACTIVER_TIMER = "desactiverTimerPopUpAttente";

    /**
     * Parametre pour la clef du fichier à charger.
     */
    public static final String P_CLEF = "clef";

    /**
     * Parametre pour la clef du fichier à charger.
     */
    public static final String P_CLEF_ZIP = "clef_zip";

    /**
     * Valeur de etatValide d'une consultation aprés validation du lancement
     * consultation.
     */
    public static final int LANCEMENT_CONSULTATION_VALIDE = 2500;

    /**
     * Valeur de etatValide d'une consultation aprés dissociation.
     */
    public static final int ACTIVE_DISSOCIATION = 5000;

    /**
     * Valeur de etatValide d'une consultation aprés accord cadre avenant.
     */
    public static final int ACTIVE_ACCORD_CADRE_AVENANT = 10000;

    /**
     * Valeur de etatValide d'une consultation aprés avoir validé
     * l'attribution.
     */
    public static final int ATTRIBUTION_VALIDEE = 10000;

    /**
     * Valeur de etatValide d'une consultation apr�s avoir valid� l'�tape de
     * d�pot (phase1).
     */
    public static final int DEPOT_VALIDEE = 4900;
    /**
     * Valeur de etatValide d'une consultation apr�s avoir valid� l'�tape de
     * d�pot (phase2).
     */
    public static final int DEPOT_OFFRE_VALIDEE = 5200;
    /**
     * Valeur de etatValide d'une consultation apr�s avoir valid� l'�tape de
     * d�pot (phase 3 pour proc�dure dc & concours).
     */
    public static final int DEPOT_OFFRE_FINALE = 5300;

    /**
     * Arret transfert depot .
     */
    public static final int STOP_TRANSFERT_CANDIDATURE = 5150;

    /**
     * Arret transfert reponse / projet.
     */
    public static final int STOP_TRANSFERT_PROJET_REPONSE = 5250;

    /**
     * motif pour un prix diff�rent de 0, contenant que des chiffres ayant pour
     * caract�re de s�paration '.' et 2 chiffres apr�s la virgule.
     */
    public static final String REGEX_PRIX =
            "(^[0-9]([0-9]+)?|0+[1-9]+)((\\.||,)[0-9]?[0-9]?)?";

    /**
     * motif pour un prix contenant que des chiffres ayant pour caractère de
     * séparation '.' et 2 chiffres après la virgule. Mais 0 et négatif sont
     * autorisés.
     */
    public static final String REGEX_PRIX_NEGATIF =
            "(^[0-9]+||\\-?[0-9]+)((\\.||,)[0-9]?[0-9]?)?";

    /**
     * Expression régulière destinée à la validation des champs Montants.
     */
    public static final String MONTANT_REGEX = "[0-9\\s]+[,\\.]{0,1}[0-9]{0,2}";

    /**
     * Idem, mais 0 autorise
     */
    public static final String REGEX_PRIX_ZERO =
            "(^[0-9]+)((\\.||,)[0-9]?[0-9]?)?";

    public static final String LOCALE_FR = "fr";
    /**
     * Variable pour le hashcode de beans.
     */
    public static final int PREMIER = 31;
    /**
     * Nom de la variable sous lequel le resultat doit �tre mis en session.
     */
    public static final String S_NOM_RESULTAT = "resultatSession";
    /**
     * Objet criteres en session.
     */
    public static final String S_CRITERES = "criteresSession";
    /**
     * Proxy Facade en session.
     */
    public static final String S_FACADE = "facadeSession";
    /**
     * Methode � appeler.
     */
    public static final String S_METHODE = "methodeSession";
    /**
     * Type de recherche.
     */
    public static final String S_TYPE_RECHERCHE = "typeRechercheSession";

    /**
     * Valeur "sans" pour les champs bcMin et BcMax.
     */
    public static final String BC_SANS = "SANS";

    /**
     * Valeur numérique pour la valeur "sans" des champs bcMin et bcMax.
     */
    public static final String BC_SANS_VALEUR = "-1";

    /**
     * Valeur numérique pour la valeur "sans" des champs bcMin et bcMax.
     */
    public static final String BC_SANS_VALEUR_TO_STRING = "-1,00";

    /**
     * Nom du référentiel directions services en session.
     */
    public static final String DIRECTIONS_SERVICES = "directionsServices";

    /**
     * Nom du référentiel pouvoirs adjudicateur en session.
     */
    public static final String POUVOIRS_ADJUDICATEURS = "pouvoirsAdjudicateurs";

    /**
     * Nom du référentiel nature des prestations en session.
     */
    public static final String NATURES_PRESTATIONS = "naturesPrestations";

    /**
     * Nom de la liste des fichiers uploadés en session.
     */
    public static final String S_LISTE_FICHIERS_TEMPORAIRES_UPLOAD = "listeFichierUploadSession";

    /**
     * Nom du parametre pour l'identifiant du fichier a télécharger.
     */
    public static final String R_ID_FICHIER = "idFichier";

    /**
     * Nom du parametre pour l'identifiant du fichier a télécharger.
     */
    public static final String R_ID_PIECE_JOINTE = "idPieceJointe";

    /**
     * Nom du parametre pour l'identifiant du document contenant le fichier a télécharger.
     */
    public static final String R_ID_DOCUMENT = "idDocument";

    /**
     * Nom du parametre token d'un document permettant de retouver le document generer par le noyau,
     * avec la gestion avancee des champ de fusion.
     */
    public static final String TOKEN_DOCUMENT = "documentToken";
    /**
     * Nom du parametre nom d'un document permettant au servlet de connaitre le nom du document a generer.
     */
    public static final String NOM_DOCUMENT = "documentToken";

    /**
     * Pattern de validation d'un email.
     */
    public static final String PATTERN_MAIL = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$";

    /**
     * Message d'erreur à afficher en cas d'erreur lors de la génération de
     * document. Si vide afficahge du message par défaut.
     */
    public static final String S_MESSAGE_ERREUR_GENERATION_DOCUMENT = "messageErreurGenerationDocument";

    /**
     * Nom du paramètre affichant le bouton de retour à commission.
     */
    public static final String S_RETOUR_COMMISSION = "urlRetourCommission";

    /**
     * Nom du paramètre affichant le bouton de retour à commission.
     */
    public static final String R_RETOUR_COMMISSION = "urlRetourCommission";

    public static final String PARAM_RETOUR_RECHERCHE = "retourRecherche";

    public static final String FORWARD_RETOUR_RECHERCHE = "retourRecherche";

    public static final String FORWARD_RETOUR_RECHERCHE_EXTERNE = "retourRechercheExterne";

    /**
     * Nom de forward Stuts.
     */
    public static final String FORWARD_RETOUR_FORM = "backToForm";

    public static final String RETOUR_RECHERCHE = "retourRechercheBean";

    public static final String AAPC_CODE_NUTS = "boamp.nuts.defaut";

    public static final String AAPC_AUTRES_INFORMATIONS = "boamp.autresInformations";

    public static final String AAPC_AUTRES_CONDITIONS_CLAUSES_SOCIALES_ENVIRONNEMENTALES = "boamp.conditionsMarche.autresConditions.clausesSocialesEnvironnementales";

    public static final String AAPC_CONDITION_MARCHE_CAPA_ECO_FIN = "boamp.conditionsMarche.capaEcoFin";

    public static final String AAPC_CONDITION_MARCHE_CAPA_TECH = "boamp.conditionsMarche.capaTech";

    public static final String AAPC_CONDITION_MARCHE_GARANTIES = "boamp.conditionsMarche.garanties";

    public static final String AAPC_CONDITION_MARCHE_MOD_FINANCEMENT = "boamp.conditionsMarche.modFinancement";

    public static final String AAPC_CONDITION_MARCHE_SITUATION_JURIDIQUE = "boamp.conditionsMarche.situationJuridique";

    public static final String AAPC_PROCEDURE_RECOURS_INTRO = "boamp.init.introductionRecoursPrecisions";

    public static final String AAPC_PROCEDURE_RECOURS_ADR = "boamp.init.procedureRecours.adr";

    public static final String AAPC_PROCEDURE_RECOURS_CP = "boamp.init.procedureRecours.cp";

    public static final String AAPC_PROCEDURE_RECOURS_FAX = "boamp.init.procedureRecours.fax";

    public static final String AAPC_PROCEDURE_RECOURS_MAIL = "boamp.init.procedureRecours.mail";

    public static final String AAPC_PROCEDURE_RECOURS_NOM = "boamp.init.procedureRecours.nomOfficiel";

    public static final String AAPC_PROCEDURE_RECOURS_TEL = "boamp.init.procedureRecours.tel";

    public static final String AAPC_PROCEDURE_RECOURS_VILLE = "boamp.init.procedureRecours.ville";

    public static final String AAPC_RGT_COMPLEMENTAIRE_AOO = "boamp.init.rgtComplementaire.aoo";

    public static final String AAPC_RGT_COMPLEMENTAIRE_DC = "boamp.init.rgtComplementaire.aor.mn.dc";

    public static final String AAPC_RGT_COMPLEMENTAIRE_M13 = "boamp.init.rgtComplementaire.m13";

    public static final String AAPC_RGT_COMPLEMENTAIRE_MAPA_O = "boamp.init.rgtComplementaire.m13.mapaO";

    public static final String AAPC_RGT_COMPLEMENTAIRE_MAPA_R = "boamp.init.rgtComplementaire.m13.mapaR";

    public static final String LIST_ID_DOCUMENTS = "listIdDocuments";
    public static final String DIRECTION = "direction";

    public static final String LISTE_IDENTIFIANTS = "listeIdentifiants";

    public static final String LISTE_PROPRIETES = "listeProprietes";

    public static final String LISTE_LOT_CONSULTATION_CRITERES_ATTRIBUTION = "listeCriteresAttribution";

    /**
     * Taille maxi de l'intitulé de la consultation.
     */
    public static final int TAILLE_INTITULE_CONSULTATION = 50;

    /**
     * Taille de l'objet à afficher au niveau du resume de consultation.
     */
    public static final int TAILLE_OBJET_CONSULTATION = 100;

    /**
     * Le nom des proprietes d'ecran en session sauvegardees par l'intermediare
     * de Jbpm doit commence par cette constante
     */
    public static final String S_PROPRIETE_ECRAN = "proprieteEcran_";

    /**
     * type de l'instance de workflow en cours mis en session par Jbpm
     */
    public static final String PROPRIETE_ECRAN_TYPE_PROCEDURE = "typeProcedure";

    /**
     * Nom de la variable statique en session faisant référence au résumé de
     * consultation.
     */
    public static final String RESUME_CONSULTATION = "resumeConsultation";

    /**
     * Valeur de string
     */
    public static final String OUI = "oui";

    public static final String NON = "non";

    public static final String NOM_PARAMETRAGE_NOMBRE_RESULTAT_PAR_DEFAUT = "nombreResultatAfficheParDefaut";

    /**
     * Expression régulière destinée la validation des champs de date au format
     * dd/MM/yyyy.
     */
    public static final String DATE_REGEX_dd_MM_yyyy = "(0[1-9]|[1-9]|[12][0-9]|3[01])/"
            + "(0[1-9]|1[012]|[1-9])/(19|20|21|22)\\d{2}";

    public static final String DATE_REGEX_dd_MM_yyyy_HH_mm = "(0[1-9]|[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012]|[1-9])/(19|20|21|22)\\d{2} (([0-1][0-9])|([2][0-3])):([0-5][0-9])";

    /**
     * Libellé "Tous".
     */
    public static final String TOUS = "Tous";

    /**
     * Libellé "Toutes".
     */
    public static final String TOUTES = "Toutes";

    /**
     * jeton d'authentification utilisé lors d'une connection SaaS. Le jeton est
     * récupéré après authentification via le webService du noyau.
     */
    public static final String IDENTIFIANT_SAAS = "identifiantSaaS";
    public static final String AUTHORIZATION = "Authorization";

    /**
     * Objet mis en session pour dire que l'utilisateur accéde à l'application
     * en mode SaaS. Il faut donc se baser sur cet objets pour changer la
     * structure des pages.
     */
    public static final String ACCES_MODE_SAAS = "accesModeSaaS";

    /**
     * Constante true
     */
    public static final String TRUE = "true";
    /**
     * Constante false
     */
    public static final String FALSE = "false";

    /**
     * Constantes pour indiquer la provenance au retour de recherche. Dans ce
     * cas, si provient de la validation d'un écran
     */
    public static final String VALIDER = "valider";

    /**
     * Constantes pour indiquer la provenance au retour de recherche. Dans ce
     * cas, si provient du bouton annuler de l'écran
     */
    public static final String ANNULER = "annuler";

    /**
     * titre de l'ecran clé resourcebundle
     */
    public static final String S_TITRE_ECRAN = "titreEcran";

    /**
     * Variable en session pour activer mode lecture seule
     */
    public static final String SESSION_LECTURE_SEULE = "lectureSeule";

    /**
     * Paramétrage pour activer le contrôle du critère d'attribution. Quand
     * activé, il aura un contrôle de pondération à la saisie des sous critères
     * de façon que la somme de sous-critères soit égale à 100%
     */
    public static final String PARAMETRAGE_ACTIVATION_CONTROLE_CRITERE_ATTRIBUTION = "activation.critereAttribution.controlePonderation";

}
