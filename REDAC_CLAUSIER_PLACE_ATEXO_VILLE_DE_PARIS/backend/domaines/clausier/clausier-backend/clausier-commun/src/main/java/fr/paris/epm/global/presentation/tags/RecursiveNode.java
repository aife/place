package fr.paris.epm.global.presentation.tags;

import java.util.List;

/**
 * Created by nty on 21/02/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface RecursiveNode<T extends RecursiveNode> {

    public int getId() ;

    public String getName() ;

    public List<T> getListChilds() ;

    public void setListChilds(List<T> childs) ;

    public void addChild(T childs) ;

    public default RecursiveNode<T> findById(int id) {
        if (this.getId() == id)
            return this;

        List<T> childs = this.getListChilds();
        if (childs.size() > 0) {
            for (T el : childs) {
                RecursiveNode<T> res = el.findById(id);
                if (res != null)
                    return res;
            }
        }
        return null;
    }

}
