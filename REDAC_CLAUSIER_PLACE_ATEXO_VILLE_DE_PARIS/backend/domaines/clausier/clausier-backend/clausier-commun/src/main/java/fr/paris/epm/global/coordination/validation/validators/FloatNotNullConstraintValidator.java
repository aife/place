package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.FloatNotNull;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

/**
 * Created by nty on 10/04/17.
 */
public class FloatNotNullConstraintValidator implements ConstraintValidator<FloatNotNull, BigDecimal> {

    @Override
    public void initialize(FloatNotNull floatNotNull) {

    }

    @Override
    public boolean isValid(BigDecimal field, ConstraintValidatorContext cxt) {
        return field != null && !field.equals(BigDecimal.ZERO);
    }

}
