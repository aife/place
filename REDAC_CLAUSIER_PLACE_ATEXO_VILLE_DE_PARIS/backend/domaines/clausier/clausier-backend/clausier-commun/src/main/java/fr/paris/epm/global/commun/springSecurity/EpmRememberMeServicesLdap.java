package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.global.commun.securite.RememberMeServices;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * Utilise par spring-security pour enregistrer et authentifier par un cookie un utilisateur, par l'intermediaire d'un LDAP. 
 * 
 * @author Rémi Villé
 */
public class EpmRememberMeServicesLdap extends RememberMeServices  {
	
	/**
	 * Informations sur l'utilisateur depuis la BDD (injection).
	 */
	private UserDetailsService userDetailsServiceJdbc;

    public EpmRememberMeServicesLdap(String key, UserDetailsService userDetailsService) {
        super(key, userDetailsService);
    }

	/**
	 * Utilise quand cookie remember-me present (pour authentification LDAP).
	 */
    public UserDetails processAutoLoginCookie(String[] cookieTokens, HttpServletRequest request,
            HttpServletResponse response) {
    	
        if (cookieTokens.length != 3)
            throw new InvalidCookieException("Cookie token did not contain " + 3 + " tokens, but contained '" + Arrays.asList(cookieTokens) + "'");

        String username = cookieTokens[0];
    	
        UserDetails userDetailsLdap = getUserDetailsService().loadUserByUsername(username);
        UserDetails userDeatailsJdbc = userDetailsServiceJdbc.loadUserByUsername(username);
        
        String password = userDetailsLdap.getPassword();
        if (password == null)
        	password = "";
        
        // Creation d'un utilisateur reconnu par LDAP et recuperation des autorisations en BDD 
        User user = new User(	userDetailsLdap.getUsername(),
        						password,
				        		userDetailsLdap.isEnabled(),
				        		userDetailsLdap.isAccountNonExpired(),
				        		userDetailsLdap.isCredentialsNonExpired(),
				        		userDetailsLdap.isAccountNonLocked(),
				        		userDeatailsJdbc.getAuthorities());

        return user;
    }
	
    /**
     * @param userDetailsServiceJdbc Informations sur l'utilisateur depuis la BDD (injection).
     */
	public void setUserDetailsServiceJdbc(final UserDetailsService userDetailsServiceJdbc) {
	    this.userDetailsServiceJdbc = userDetailsServiceJdbc;
    }

}
