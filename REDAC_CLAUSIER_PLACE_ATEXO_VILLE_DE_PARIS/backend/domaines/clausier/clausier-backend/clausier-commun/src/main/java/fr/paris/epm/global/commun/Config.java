package fr.paris.epm.global.commun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

/**
 * Initialise le conteneur properties des proprietes de configuration du module.
 * Prend en compte les fichiers de configuration declares dans applicationContext.xml, en considerant s'il sont presents, ceux de l'archive
 * et ceux du dossier d'externalisation.
 * Si des clef sont communes entre ces fichiers la priorite donnee est la suivante
 * (avec fichier1 et fichier2 déclarés dans cet ordre dans applicationContext.xml) :
 * prio(fichier1(externe)) > prio(fichier2(externe)) > prio(fichier1(WEB-INF)) > prio(fichier2(WEB-INF)).
 * 
 * @author Remi Ville
 * @version 2010/06/15
 */
public class Config {

	/**
	 * Messages applicatifs.
	 */
    private static final Logger LOG = LoggerFactory.getLogger(Config.class);
    
    /**
     * Conteneur des proprietes de configuration du module.
     */
    private static Properties properties = null;

    private Config() {
    }
    
    /**
     * Creation du conteneur des proprietes de configuration du module.
     * Ne devrait être appelé qu'une seule fois.
     * 
     * @param fileNameList liste des nom de fichiers de configuration.
     */
    public static void init(List<String> fileNameList) {
    	
    	Properties propertiesTmp;
    	ListIterator<String> li;
    	String fileName;
    	int priorite;
    	
    	// Executer une seul fois
    	if(properties != null) {
    		return;
    	}	 
    	
    	for (priorite=0; priorite<2; priorite++) {
	    	li = fileNameList.listIterator(fileNameList.size());
	    	while(li.hasPrevious()) {
	    		fileName = li.previous();
	    		propertiesTmp = getProperties(fileName, priorite);
	    		propertiesPutAll(propertiesTmp);
	    	}
    	}
    }
    
    /**
     * Surcharge les proprietes de properties par celles de props.
     * 
     * @param props proprietes de configuration a surcharger sur properties.
     */
    private static void propertiesPutAll(Properties props) {
    	
    	if (properties == null) {
    		properties = props;
    	} else if (props != null) {
    		properties.putAll(props);
    	}
    }
    
    /**
     * Pour un meme fichier de configuration, retourne soit les proprietes par defaut (dans WEB-INF/)
     * soit celles externes, selon la priorite.
     * priorite == 0 => defaut
     * priorite == 1 => externe
     * 
     * @param fileName nom du fichier de configuration
     * @param priorite determine si l'operation concerne le repertoire WEB-INF/ ou le repertoire d'externalisation 
     * @return les proprietes du fichier fileName
     */
    private static Properties getProperties(String fileName, int priorite) {
    	
    	Properties res = null;
    	
    	if ((priorite < 0) || (priorite > 1)) {
    		LOG.error("Priorité {} non géré", priorite);
    		return null;
    	}
    	
    	if (priorite == 0) {
    		res = getPropertiesDefaut(fileName);
    	} else  {
    		res = getPropertiesExterne(fileName);
    	}
    	
    	return res;
    }
    
    /**
     * Retourne les proprietes contenu dans WEB-INF/
     * 
     * @param fileName nom du fichier de configuration
     * @return proprietes contenu dans WEB-INF/
     */
    private static Properties getPropertiesDefaut(String fileName) {
    	
    	Properties res = null;
    	InputStream is = null;
    	ClassLoader classLoader;
    	String totalPath = "WEB-INF/classes/" + fileName;
    	
    	LOG.debug("Recherche du fichier interne (dans le war) '{}'", fileName);
    	
    	classLoader = Config.class.getClassLoader();
    	
    	is = classLoader.getResourceAsStream(fileName);
    	if(is == null) {
    		LOG.debug("Fichier interne '{}' absent", totalPath);
    	} else {
        	res = new Properties();
        	try {
				res.load(is);
		        LOG.info("Fichier de configuration '{}' correctement chargé", totalPath);
			} catch (IOException e) {
				LOG.error("Erreur lors de l'ouverture du fichier de configuration '{}'",  totalPath);
				res = null;
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					LOG.error("Erreur lors de la fermture du fichier de configuration '{}'",  totalPath);
				}
			}
        }
    	
        return res;
    }
    
    /**
     * Retourne les proprietes contenu dans le dossier d'externalisation.
     * 
     * @param fileName nom du fichier de configuration
     * @return proprietes contenu dans le dossier d'externalisation.
     */
    private static Properties getPropertiesExterne(String fileName) {
    	
    	String appPropertiesPath, appPropertiesTotalPath;
    	InputStream is = null;
    	Properties res = null;
    	
    	appPropertiesPath = System.getProperty("app.properties.path");
        
        if(appPropertiesPath == null) {
        	return null;
        }
        appPropertiesTotalPath = appPropertiesPath + "/" + fileName;

	    LOG.debug("Recherche du fichier externe '{}'", appPropertiesTotalPath);
    	
        File file = new File(appPropertiesPath, fileName);
        if (!file.exists()) {
        	LOG.debug("Le fichier externe '{}' n'existe pas", appPropertiesTotalPath);
        } else {
            try {
				is = new FileInputStream(file);
				res = new Properties();
				res.load(is);
	            LOG.info("Fichier de configuration externe '{}' correctement chargé", appPropertiesTotalPath);
			} catch (FileNotFoundException e) {
				LOG.error("Le fichier externe '{}' n'a pas éte trouvé", appPropertiesTotalPath);
				res = null;
			} catch (IOException e) {
				LOG.error("Erreur lors de l'ouverture du fichier de configuration '{}'", appPropertiesTotalPath);
				res = null;
			} finally {
				if(is != null) {
					try {
						is.close();
					} catch (IOException e) {
						LOG.error("Erreur lors de la fermeture du fichier de configuration '{}'", appPropertiesTotalPath);
					}
				}
			}
        }
        
    	return res;
    }

    /**
     * @return les proprietes de configuration du module.
     */
	public static final Properties getProperties() {
		return properties;
	}
}
