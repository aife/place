package fr.paris.epm.global.commun.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * A partir de taille en byte, transformer la taille du fichier en unité comme o, Mo, Ko, To.
 * 
 * @author GAO Xuesong
 * @version: $, $Revision: $, $Author: $
 */
public class TailleFichierTag extends TagSupport {

    /**
     * identifiant de serialisation
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(TailleFichierTag.class);
    
    /**
     * les multiples de l'octet
     * 
     * 1 kibioctet (Kio) = 1 024 octets
     * 1 mébioctet (Mio) = 1 024 Kio
     * 1 gibioctet (Gio) = 1 024 Mio     
     * 1 tébioctet (Tio) = 1 024 Gio     
     */
    private static final long MULTIPLE_OCTET = 1024;
    
    /**
     * unité octet
     */
    private static final String OCTET = "o";
    
    /**
     * unité kilooctet
     */
    private static final String KILO_OCTET = "Ko";
    
    /**
     * unité mégaoctet
     */
    private static final String MEGA_OCTET = "Mo";
    
    /**
     * unité gigaoctet
     */
    private static final String GIGA_OCTET = "Go";
    
    /**
     * unité téraoctet
     */
    private static final String TERA_OCTET = "To";
    
    /**
     * marquer que la taille est zéro
     */
    private static final String TAILLE_ZERO = "0";

    /**
     * Valeur de la taille en byte
     */
    private String tailleEnByte;

    /**
     * toutes les suffixes de la taille
     */
    private String[] suffixes = new String[] { OCTET, KILO_OCTET, MEGA_OCTET, GIGA_OCTET, TERA_OCTET };

    @Override
    public int doStartTag() throws JspException {

        String tailleAffichage = "";

        if(tailleEnByte == null){
            return SKIP_BODY;
        }
        
        double taille = Double.parseDouble(tailleEnByte);
        
        if (taille < Double.MAX_VALUE) {
            
            // diviser par multiple 
            int i = 0;
            while (taille >= MULTIPLE_OCTET) {
                taille /= MULTIPLE_OCTET;
                i++;
            }

            String pattern = "#.##";
            String tailleStr = new DecimalFormat(pattern).format(taille);
            tailleAffichage = tailleStr + " " + suffixes[i];
        }

        try {
            pageContext.getOut().print(tailleAffichage);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException(e.fillInStackTrace());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        tailleEnByte = TAILLE_ZERO;
    }


    public void setTailleEnByte(String valeur) {
        this.tailleEnByte = valeur;
    }
}
