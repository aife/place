package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import fr.paris.epm.noyau.service.DocumentModeleServiceSecurise;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Facade abstrait : contient
 *          - safelyInvokator
 *          - proxy vers le noyau
 *          - le service d'administration du noyau
 *          - le service des referentiels du noyau
 *          - le service des documents du noyau
 *          - le service de la conversion
 * Created by nty on 23/02/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class AbstractFacade {

    private SafelyNoyauInvokator safelyInvokator;

    /**
     * proxy vers le noyau permettant d'obtenir les objets en base quasi immuables (referentiels, selects).
     */
    private NoyauProxy noyauProxy;

    /**
     * façade vers le service d'administration du noyau
     */
    private AdministrationServiceSecurise administrationService;

    private ReferentielsServiceSecurise referentielsService;

    private DocumentModeleServiceSecurise documentModeleService;

    @Autowired
    public void setSafelyInvokator(SafelyNoyauInvokator safelyInvokator) {
        this.safelyInvokator = safelyInvokator;
    }

    public SafelyNoyauInvokator getSafelyInvokator() {
        return safelyInvokator;
    }

    @Autowired
    public void setNoyauProxy(NoyauProxy noyauProxy) {
        this.noyauProxy = noyauProxy;
    }

    public NoyauProxy getNoyauProxy() {
        return noyauProxy;
    }

    @Autowired
    public void setAdministrationService(AdministrationServiceSecurise administrationService) {
        this.administrationService = administrationService;
    }

    public AdministrationServiceSecurise getAdministrationService() {
        return administrationService;
    }

    @Autowired
    public void setReferentielsService(ReferentielsServiceSecurise referentielsService) {
        this.referentielsService = referentielsService;
    }

    public ReferentielsServiceSecurise getReferentielsService() {
        return referentielsService;
    }

    @Autowired
    public void setDocumentModeleService(DocumentModeleServiceSecurise documentModeleService) {
        this.documentModeleService = documentModeleService;
    }

    public DocumentModeleServiceSecurise getDocumentModeleService() {
        return documentModeleService;
    }

}