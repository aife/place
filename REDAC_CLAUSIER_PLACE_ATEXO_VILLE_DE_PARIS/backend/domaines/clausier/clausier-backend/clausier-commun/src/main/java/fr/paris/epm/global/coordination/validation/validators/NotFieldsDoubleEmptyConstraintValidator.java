package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.NotFieldsDoubleEmpty;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

/**
 * Created by nty on 06/04/17.
 */
public class NotFieldsDoubleEmptyConstraintValidator
        extends AbstractDoubleFieldsConstraintValidator<NotFieldsDoubleEmpty, Object> {

    @Override
    public void initialize(NotFieldsDoubleEmpty constraintAnnotation) {
        setFirstFieldName(constraintAnnotation.firstFieldName());
        setSecondFieldName(constraintAnnotation.secondFieldName());
    }

    @Override
    public boolean isValid(Object instance, ConstraintValidatorContext cxt) {
        if (instance == null)
            return true;

        try {
            Object first = getValueOfFirstField(instance);
            Object second = getValueOfSecondField(instance);

            if (checkFieldEmpty(first) && checkFieldEmpty(second)) {
                ConstraintViolationBuilder cvb = cxt.buildConstraintViolationWithTemplate(cxt.getDefaultConstraintMessageTemplate());
                cvb.addNode(getFirstFieldName()).addConstraintViolation();
                //cvb.addNode(getSecondFieldName()).addConstraintViolation();
                return false;
            }
        } catch (IllegalAccessException ex) {
            //log.error("Cannot validate fileds not empty in '" + value + "'!", ex);
            return false;
        }

        return true;
    }

}
