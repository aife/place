package fr.paris.epm.global.commun.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * Classe abstaite pour toutes les Rsem-Taches.
 * Created by nty on 12/03/18.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class RsemTask<T> implements Callable<T> {

    private static Logger logger = LoggerFactory.getLogger(RsemTask.class);

    private String idTask;

    private String resultHref;

    public void setIdTask(String idTask) {
        this.idTask = idTask;
    }

    public String getIdTask() {
        return idTask;
    }

    public String getResultHref() {
        return resultHref;
    }

    public void setResultHref(String resultHref) {
        this.resultHref = resultHref;
    }

    public abstract T work() throws Exception ;

    @Override
    public final T call() throws Exception {
        try {
            T result = work();
            logger.info("Task {} dont id = {} est fini son work avec succes", Thread.currentThread().getName(), idTask);
            return result;
        } catch (Exception ex) {
            logger.error("Task {} dont id = {} est echoue avec erreur {}", Thread.currentThread().getName(), idTask, ex.getMessage());
            throw ex;
        }
    }

}
