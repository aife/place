package fr.paris.epm.global.presentation.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Formulaire générique pour la gestion de la pagination et la conservation de l'historique des checkbox.
 *
 */
public class TableauPagineGeneriqueForm extends ActionForm {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 425837626398376427L;

    /**
     * Tableau d'idenfiants à mettre à jours.
     */
    private String[] identifiants = null;

    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        identifiants = null;
    }

    /**
     * @return Tableau d'idenfiants à mettre à jours.
     */
    public final String[] getIdentifiants() {
        return identifiants;
    }

    /**
     * @param valeur Tableau d'idenfiants à mettre à jours.
     */
    public final void setIdentifiants(String[] valeur) {
        this.identifiants = valeur;
    }

}
