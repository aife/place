package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.BankAccountConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by nty on 21/03/17.
 */
@Documented
@Constraint(validatedBy = BankAccountConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface BankAccount {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
