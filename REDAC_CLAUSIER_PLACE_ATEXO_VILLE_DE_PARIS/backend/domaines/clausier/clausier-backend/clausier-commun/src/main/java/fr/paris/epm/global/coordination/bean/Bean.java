package fr.paris.epm.global.coordination.bean;

/**
 * Interface pour toutes les beans
 * Created by nty on 03/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface Bean {

    public int getId() ;

    public void setId(int id) ;

}
