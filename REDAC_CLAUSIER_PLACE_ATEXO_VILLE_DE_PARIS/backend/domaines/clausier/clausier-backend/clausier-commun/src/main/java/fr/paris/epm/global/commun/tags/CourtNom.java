package fr.paris.epm.global.commun.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class CourtNom extends TagSupport {
    
    /**
     * Id de serialisation.
     */
    private static final long serialVersionUID = 1L;
    private String value;
    private int from;
    private int to;
    
    public int doStartTag() throws JspException {
        try {
            value = (String) pageContext.getAttribute(value);
            pageContext.getOut().print(value.substring(from, to));  
            
        } catch (IOException e) {
            return SKIP_BODY;
        }
        return SKIP_BODY;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    

}
