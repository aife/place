package fr.paris.epm.global.commun.exceptions;

public class FirewallException extends RuntimeException{

	public FirewallException( String parametre) {
		super("Le parametre saisi est invalide = "+parametre);
	}
}
