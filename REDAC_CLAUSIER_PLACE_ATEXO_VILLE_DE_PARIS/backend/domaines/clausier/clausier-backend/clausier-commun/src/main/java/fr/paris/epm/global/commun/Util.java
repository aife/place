package fr.paris.epm.global.commun;

import de.schlichtherle.util.zip.ZipEntry;
import de.schlichtherle.util.zip.ZipOutputStream;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.coordination.objetValeur.SimpleCouple;
import fr.paris.epm.global.coordination.objetValeur.SimplePair;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTHabilitation;
import fr.paris.epm.noyau.persistance.EpmTProfil;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.Deflater;

/**
 * Classe des utilitaires commun a tout le projet.
 *
 * @author EDDOUGHMI Younes
 * @version $Revision$, $Date$, $Author$
 */
public class Util {

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Util.class);

    /**
     * Motif pour trouver les double quote dans une chaine.
     */
    private static final String DOUBLE_QUOTE = "\\\"";

    /**
     * Motif d'échapement des doubles quote.
     */
    private static final String DOUBLE_QUOTE_ECHAPE = "\\\\\"";

    /**
     * Motif pour trouver les simple quote dans une chaine.
     */
    private static final String SIMPLE_QUOTE = "\\'";

    /**
     * Motif d'échapement des simple quote.
     */
    private static final String SIMPLE_QUOTE_ECHAPE = "\\\\'";

    /**
     * Echappe les simple et double quote d'une chaine par \' ou \".
     * @param texte le texte à échapper
     * @return le texte echappé
     */
    public static final String echapperQuotes(final String texte) {
        String resultat = texte.replaceAll(DOUBLE_QUOTE,
                DOUBLE_QUOTE_ECHAPE);
        resultat = resultat.replaceAll(SIMPLE_QUOTE, SIMPLE_QUOTE_ECHAPE);
        return resultat;
    }

    /**
     * Permet de tronquer une chaine de caractere.
     *
     * @param chaine
     *            la chaine de caractere a tronquer
     * @param nombreCaractereMax
     *            nombre de caractere maximale
     * @return chaine tronquee
     */
    public static final String tronquer(final String chaine,
            final int nombreCaractereMax, boolean etc) {
        if (chaine.trim().length() > nombreCaractereMax) {
            StringTokenizer contenu = new StringTokenizer(chaine.trim(), " ");
            StringBuffer contenuTronque = new StringBuffer();
            while (contenu.hasMoreElements()) {
                String morceau = (String) contenu.nextElement();
                if (contenuTronque.toString().trim().length() < nombreCaractereMax) {
                    if (contenuTronque.toString().trim().length()
                            + morceau.length() > nombreCaractereMax) {
                        break;
                    }
                    contenuTronque.append(" " + morceau);
                } else {
                    break;
                }
            }
            if (contenuTronque.toString().equals("")) {
                contenuTronque.append(chaine.trim().substring(0,
                        nombreCaractereMax));
            }
            if (contenuTronque.toString().trim().length() < chaine.trim()
                    .length()
                    && etc) {
                contenuTronque.append("...");
            }
            return contenuTronque.toString().trim();
        } else {
            return chaine.trim();
        }
    }

    /**
     * Converti un objet String en objet Double. Retourne -1 si la chaine vaut
     * SANS (valeur possible des BC)
     *
     * @param inputString
     *            la chaine de caracteres à convertir
     * @return un objet Integer à partir d'un objet string
     */
    public static Double stringToDoubleOuSANS(String valeur) {
        Double resultat = null;
        if (ConstantesGlobales.BC_SANS.equalsIgnoreCase(valeur)) {
            valeur = ConstantesGlobales.BC_SANS_VALEUR;
            return new Double(valeur);
        }
        resultat = stringToDouble(valeur);
        return resultat;
    }

    /**
     * Converti un objet String en objet Double.
     *
     * @param inputString
     *            la chaine de caracteres à convertir
     * @return un objet Double à partir d'un objet string
     */
    public static Double stringToDouble(final String inputString) {
        Double i = null;
        if (!"".equals(inputString)) {
            String strFiltre = filtreFormulaire(inputString);
            String floaT = remplaceVirgulePointDeNombre(strFiltre);
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(
                    ConstantesGlobales.LOCALE_FR));

            DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
            df.setMaximumFractionDigits(2);
            df.setMaximumIntegerDigits(13);
            df.setDecimalSeparatorAlwaysShown(true);

            try {
                Number nb = df.parse(floaT);
                i = new Double(nb.doubleValue());
            } catch (Exception e) {

            }
        }
        return i;
    }

    /**
     * Extension de la methode Double.toString() avec gestion du cas a l'objet
     * Double est null.
     *
     * @param i
     *            l'entier a convertir en chaine de caracteres
     * @return la chaine de caracteres a convertir
     */
    public static String doubleToString(final Double i) {
        if (i == null) {
            return "";
        } else {
            String s;
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(
                    ConstantesGlobales.LOCALE_FR));

            DecimalFormat df = new DecimalFormat("#,##0.00", dfs);
            df.setMaximumFractionDigits(2);
            df.setMaximumIntegerDigits(13);
            df.setDecimalSeparatorAlwaysShown(true);

            try {
                s = df.format(i.doubleValue());

            } catch (Exception e) {
                s = "";
            }
            return s;
        }
    }

    public static String doubleToStringSansDecimal(final Double valeur) {
        if (valeur == null) {
            return "";
        } else {
            String s;
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(
                    ConstantesGlobales.LOCALE_FR));

            DecimalFormat df = new DecimalFormat("#,##0", dfs);
            df.setMaximumFractionDigits(2);
            df.setMaximumIntegerDigits(13);
            df.setDecimalSeparatorAlwaysShown(true);

            try {
                s = df.format(Math.ceil(valeur.doubleValue()));

            } catch (Exception e) {
                s = "";
            }
            return s;
        }
    }

    /**
     * Converti un objet Double en objet String. Retourne SANS si la chaine vaut
     * -1 (valeur possible des BC)
     *
     * @param inputString
     *            la chaine de caracteres à convertir
     * @return un objet Integer à partir d'un objet string
     */
    public static String doubleToStringOuSANS(Double valeur) {
        String resultat = doubleToString(valeur);
        if (ConstantesGlobales.BC_SANS_VALEUR_TO_STRING
                .equalsIgnoreCase(resultat)) {
            resultat = ConstantesGlobales.BC_SANS;
        }
        return resultat;
    }

    public static String remplacePointVirguleDeNombre(final String str) {
        return str.replace(',', '.');
    }

    public static String remplaceVirgulePointDeNombre(final String str) {
        return str.replace('.', ',');
    }

    public static String filtreFormulaire(final String str) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(new Locale(
                ConstantesGlobales.LOCALE_FR));
        if (str.indexOf(".") != -1) {
            dfs.setDecimalSeparator('.');
        } else {
            dfs.setDecimalSeparator(',');
        }

        DecimalFormat df = new DecimalFormat("#####0.00", dfs);

        df.setMaximumFractionDigits(2);
        df.setMaximumIntegerDigits(13);

        String s;
        if (str.replaceAll("[\\W*&&[^,||\\.]]", "").matches(
                ConstantesGlobales.REGEX_PRIX_ZERO)) {
            try {
                Number nb = df.parse(str.replaceAll("[\\W*&&[^,||\\.]]", ""));
                s = df.format(nb.doubleValue());
            } catch (Exception e) {
                return str;
            }
            return remplacePointVirguleDeNombre(s);
        } else {
            return str;
        }
    }

    /**
     *
     * @param texte
     *            texte à decouper
     * @param tailleTexte
     *            insertion espace tous les tailleTexte
     * @return chaine avec l'insertion des espaces
     */
    public static String insererEspace(String texte, int tailleTexte) {
        if (texte == null) {
            return "";
        }
        String texteWithSpaces = (String) texte;
        StringTokenizer contenu = new StringTokenizer(texteWithSpaces.trim(),
                " ");
        StringBuffer sb = new StringBuffer();

        while (contenu.hasMoreElements()) {
            String morceau = (String) contenu.nextElement();
            morceau = morceau.trim();
            if (morceau.trim().length() > tailleTexte) {
                int cpt = morceau.length() / tailleTexte;
                for (int i = 0; i <= cpt; i++) {
                    int beginIndex = i * tailleTexte;
                    int endIndex = beginIndex + tailleTexte;

                    if (i == cpt) {
                        endIndex = morceau.length();
                    }

                    sb.append(morceau.substring(beginIndex, endIndex) + " ");
                }
            } else {
                sb.append(morceau.trim() + " ");
            }
        }
        return sb.toString();
    }

    /**
     * @param directory
     * @return
     * @throws TechnicalException
     */
    public static synchronized FileTmp creeFichierTmp(String directory) {
        if (directory == null) {
            throw new TechnicalException(
                    "le path du fichier temporaire est null");
        }
        FileTmp fichier = new FileTmp(directory + System.nanoTime());
        if (fichier.exists()) {
            throw new TechnicalException("le fichier temporaire existe deja");
        }
        return fichier;
    }

    /**
     * Création d'un zip à partir d'une liste de fichiers
     * @param listeDesFichiers {@link FileTmp};
     * @param cheminZIP chemin de desintation et nom du fichier zip à créer.
     * @param supprimerFilesApresCompression
     * @return
     * @throws TechnicalException
     */
    public static boolean creerFichierZIP(List listeDesFichiers, String cheminZIP, boolean supprimerFilesApresCompression) {
        /*
         * Création d'un flux d'écriture vers un fichier, ce fichier sera
         * l'archive ZIP finale
         */
        FileOutputStream fileOutputStream;
        File filePart = new File(cheminZIP + ".filepart");
        ;
        try {
            fileOutputStream = new FileOutputStream(filePart);

            /*
             * Création d'un buffer de sortie afin d'améliorer les performances
             * d'écriture
             */
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                    fileOutputStream);

            /*
             * Création d'un flux d'écriture ZIP vers ce fichier à travers le
             * buffer
             */
            ZipOutputStream zipOutputStream = new ZipOutputStream(
                    bufferedOutputStream);

            /* Spécifier la méthode de compression désirée */
            //            zipOutputStream.setMethod(ZipOutputStream.DEFLATED);

            /* Spécifier le taux de compression (entier positif entre 0 et 9) */
            zipOutputStream.setLevel(Deflater.BEST_COMPRESSION);

            /* Lister les fichiers à compresser */
            File fichier;
            for (int i = 0; i < listeDesFichiers.size(); i++) {
                /* Recuperation du i ème fichier */
                fichier = (File) listeDesFichiers.get(i);

                /* Création d'un flux de lecture vers le fichier */
                FileInputStream fileInputStream = new FileInputStream(fichier);
                

                /* Créer une entrée ZIP */
                String nomFichier = recupererNonFichier(fichier);
                ZipEntry zipEntry = new ZipEntry(nomFichier);

                /* Affecter l'entrée crée au flux de sortie */
                zipOutputStream.putNextEntry(zipEntry);

                /* Écriture des entrées dans le flux de sortie */
                IOUtils.copy(fileInputStream, zipOutputStream);

                /* Fermeture de l’entrée en cours */
                zipOutputStream.closeEntry();

                /* Fermeture des flux */
                fileInputStream.close();
            }

            /* Fermeture des flux */
            zipOutputStream.close();
            bufferedOutputStream.close();
            fileOutputStream.close();
            // supprimer les fichiers compressés
            if (supprimerFilesApresCompression) {
                for (int i = 0; i < listeDesFichiers.size(); i++) {
                    File path = (File) listeDesFichiers.get(i);
                    FileUtils.deleteQuietly(path);
                }
            }
            FileUtils.moveFile(filePart, new File(cheminZIP));
        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e.fillInStackTrace());
        } finally {
            if (filePart != null) {
                if (filePart.exists()) {
                    filePart.delete();
                }
            }
        }
        return true;
    }

    /**
     * Retourne le nom du fichier à donner au zipEntry en fonction du type de
     * l'objet.
     * @param fichier
     * @return le nom du fichier à donner au zipEntry en fonction du type de
     * l'objet.
     */
    private static String recupererNonFichier(File fichier) {
        String nomFichier = "";
        if (fichier instanceof FileTmp) {
            FileTmp fichierTemporaire = (FileTmp) fichier;
            nomFichier = fichierTemporaire.getNomFichier();
        } else {
            nomFichier = FilenameUtils
                    .getName((fichier.getAbsolutePath()));
        }
        return nomFichier;
    }

    /**
     * @param fichierASauvegarder le fichier à sauvegarder
     * @param urlConnectionNoyau l'url de connection au noyau(indiquer
     *            uniquement l'url du noyau, le nom de la servlet est rajouté
     *            dans le corps de la méthode.
     * @param timeoutHTTP timeout pour la requette http.
     * @return l'identifiant du fichier sur disque crée.
     * @throws TechnicalException Erreur en cas de mauvais retour de la requete
     *             http, d'erreur d'enregistrement du noyau
     */
    public static int sauvegarderFichier(final File fichierASauvegarder, final String urlConnectionNoyau, final int timeoutHTTP) {
        int idFichierSurDisque = 0;
        PostMethod filePost = new PostMethod(urlConnectionNoyau + "/enregistrerFichier");
        try {

            Part[] parts = { new FilePart(fichierASauvegarder.getName(), fichierASauvegarder) };
            filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
            HttpClient client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(timeoutHTTP);
            int status = client.executeMethod(filePost);
            if (status == HttpStatus.SC_OK) {
                String idFichier = filePost.getResponseBodyAsString();
                if (idFichier.matches(ConstantesGlobales.NUMERIC_REGEX) && !"0".equals(idFichier)) {
                    idFichierSurDisque = Integer.parseInt(idFichier);
                } else {
                    throw new TechnicalException("Echec de l'enregistrement du fichier "
                            + fichierASauvegarder.getName());
                }

            } else {
                throw new TechnicalException("Echec de l'enregistrement du fichier "
                        + fichierASauvegarder.getName() + " " + HttpStatus.getStatusText(status));
            }
        } catch (IOException ex) {
            throw new TechnicalException(ex.getMessage(), ex.fillInStackTrace());
        } finally {
            LOG.debug("release connection");
            filePost.releaseConnection();
        }
        return idFichierSurDisque;
    }

    /**
     * @param taille la taille du fichier en bytes.
     * @return la taille du fichier lisible par un humain avec l'unité ex : 156672 -> 153 ko
     */
    public static String afficheTailleFichier(final long taille) {
        long tailleTmp = taille;
        String[] suffixes = new String[] { "o", "Ko", "Mo", "Go", "To" };
        int i = 0;
        while (tailleTmp >= 1024) {
            tailleTmp /= 1024.0;
            i++;
        }

        tailleTmp *= 100;
        tailleTmp = (int) (tailleTmp + 0.5);
        tailleTmp /= 100;

        return tailleTmp + " " + suffixes[i];
    }

    /**
     * @param urlNoyau
     * @param pathTmp
     * @param nomFichierZipGenere
     * @param nomFichierFinal
     * @return
     * @throws TechnicalException
     */
    public static FileTmp recupererFichierZip(final String urlNoyau, final String pathTmp, final String nomFichierZipGenere, final String nomFichierFinal) {
        FileTmp fichierTemporaire = null;
        try {
            URL url;
            url = new URL(urlNoyau + "/recupererFichier?zipFichier="
                    + URLEncoder.encode(nomFichierZipGenere));
            URLConnection connection = url.openConnection();
            InputStream in = connection.getInputStream();
            fichierTemporaire = new FileTmp(pathTmp + System.nanoTime() + ".zip");
            final FileOutputStream fluxZip = new FileOutputStream(fichierTemporaire);
            IOUtils.copyLarge(in, fluxZip);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(fluxZip);
            fichierTemporaire.setNomFichier(nomFichierFinal);
        } catch (Exception e) {
            throw new TechnicalException(e);
        }
        return fichierTemporaire;
    }

    /**
     * Verifie si le profil a une habilitation
     * @param nomRole
     * @param profils
     * @return
     */
    public static boolean verificationHabilitation(final String nomRole, Collection<EpmTProfil> profils) {
        boolean resultat = false;
        Iterator<EpmTProfil> it = profils.iterator();

        while (it.hasNext()) {
            EpmTProfil profil = (EpmTProfil) it.next();
            Collection<EpmTHabilitation> habilitations = profil.getHabilitationAssocies();
            Iterator<EpmTHabilitation> it2 = habilitations.iterator();
            while (it2.hasNext()) {
                EpmTHabilitation habilitation = it2.next();
                String[] roleTab = null;
                if (habilitation.getRole() != null) {
                    roleTab = habilitation.getRole().split(",");
                    List<String> listeRole = Arrays.asList(roleTab);
                    if (listeRole.contains(nomRole)) {
                        resultat = true;
                        break;
                    }
                }

            }
        }
        return resultat;
    }

    public static String getNomLotFromIdLot(Collection lots, int idLot) {
        for (int i = 1; i <= lots.size(); i++) {
            EpmTBudLot lot = (EpmTBudLot) lots.toArray()[i];
            if (i == idLot) {
                return lot.getNumeroLot();
            }
        }

        return "";
    }

    /**
     * @param consultation la consultation
     * @param idLotDissocie id du lot dissocié ou 0 si pas lot dissocié.
     * @return la liste des lots de la consultation.
     */
    public static List<EpmTBudLot> getListeLots(EpmTConsultation consultation, int idLotDissocie) {
        Set<EpmTBudLot> lots = consultation.getEpmTBudLots();
        List<EpmTBudLot> liste = new ArrayList<>();
        if (lots != null) {
            for (EpmTBudLot lot : lots) {
                if (consultation.isLotDissocie() && lot.getId() == idLotDissocie) {
                    liste.add(lot);
                    return liste;
                }
                if (!lot.getEpmTBudLotOuConsultation().isLotDissocie() && idLotDissocie == 0)
                    liste.add(lot);
            }
        }
        return liste;
    }

    public static String getStatutLotFromIdLot(Collection<EpmTBudLot> lots, int idLot) {
        for (EpmTBudLot lot : lots)
            if (lot.getId() == idLot)
                return lot.getEpmTRefStatut().getLibelle();
        return "";
    }


    public static List<SimplePair> initSelect(ResourceBundleMessageSource messageSource, String... listLibelle){
        List<SimplePair> select = new ArrayList<SimplePair>();
        int i = 0;
        for (String libelle : listLibelle)
            select.add(new SimplePair(i++, messageSource.getMessage(libelle, null, Locale.FRENCH)));
        return select;
    }

    public static List<SimpleCouple> initSelectNullOuiNon(ResourceBundleMessageSource messageSource, String tous, String oui, String non){
        List<SimpleCouple> select = Stream.of(
                new SimpleCouple(null, messageSource.getMessage(tous, null, Locale.FRENCH)),
                new SimpleCouple("oui", messageSource.getMessage(oui, null, Locale.FRENCH)),
                new SimpleCouple("non", messageSource.getMessage(non, null, Locale.FRENCH)))
                .collect(Collectors.toList());
        return select;
    }


    /**
     * Imprime une stack trace dans une seule chaîne de texte
     * @param exception
     * @return
     */
    public static String stackTraceToString(Exception exception) {
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter( writer );
        exception.printStackTrace( printWriter );
        printWriter.flush();
        return writer.toString();
    }

}
