package fr.paris.epm.global.coordination.mapper;

import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

/**
 * Created by nty on 08/01/19.
 */
@Mapper(componentModel = "spring")
public abstract class DirectoryMapper {

    private NoyauProxy noyauProxy;

    private AdministrationServiceSecurise administrationService;

    @Autowired
    public void setNoyauProxy(NoyauProxy noyauProxy) {
        this.noyauProxy = noyauProxy;
    }

    @Autowired
    public void setAdministrationService(AdministrationServiceSecurise administrationService) {
        this.administrationService = administrationService;
    }



    public int id( EpmTRef referentiel) {
        if (referentiel == null)
            return 0;
        return referentiel.getId();
    }

    public String label( EpmTRef referentiel) {
        if (referentiel == null)
            return null;
        return referentiel.getLibelle();
    }



}
