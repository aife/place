package fr.paris.epm.global.commun.securite;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTHabilitation;
import fr.paris.epm.noyau.persistance.EpmTProfil;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Cheikh Diop
 * @version $Revision: 96 $, $Date: 2007-07-30 $, $Author: DIOP $
 */
public class RememberUtilisateurDetailService implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(RememberUtilisateurDetailService.class);

    private String guidDefaut;

    private String authoritiesByUsernameQuery;

    private String rolePrefix = "";

    private String usersByUsernameQuery;

    private boolean usernameBasedPrimaryKey = true;

    private AdministrationServiceSecurise administrationService;

    /**
     * 
     * 
     */
    public RememberUtilisateurDetailService() {

    }

    /**
     * methode pour l'authentification via un cookie.
     */
    protected void addCustomAuthorities(String username, Collection authorities) {
    }

    public String getAuthoritiesByUsernameQuery() {
        return authoritiesByUsernameQuery;
    }

    public String getRolePrefix() {
        return rolePrefix;
    }

    public String getUsersByUsernameQuery() {
        return usersByUsernameQuery;
    }

    /**
     * Extension point to allow other MappingSqlQuery objects to be substituted
     * in a subclass
     */

    public boolean isUsernameBasedPrimaryKey() {
        return usernameBasedPrimaryKey;
    }

    /**
     * @param guid
     * @return
     * @throws UsernameNotFoundException
     * @throws DataAccessException
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserByUsername(String guid) {
        EpmTUtilisateur utilisateur = new EpmTUtilisateur();
        List<GrantedAuthority> dbAuths = new ArrayList<GrantedAuthority>();

        if (administrationService != null) {
            try {
                if (guidDefaut != null && !guidDefaut.equals("")) {
                    guid = guidDefaut;
                }
                if ("".equals(guid)) {
                	guid = null;
                }
                if(guid != null) {
                	utilisateur = administrationService.chargerUtilisateur(guid);
                }
            } catch (TechnicalNoyauException e) {
                // TODO Auto-generated catch block
                LOG.error(e.getMessage(), e.fillInStackTrace());
            }
            if (utilisateur != null) {
                Collection<EpmTProfil> profilsUtilisateur = utilisateur.getProfil();

                if (profilsUtilisateur != null) {

                    for (EpmTProfil profil : profilsUtilisateur) {
                        Collection<EpmTHabilitation> listehabilitation = profil.getHabilitationAssocies();
                        for (EpmTHabilitation habilitation : listehabilitation) {
                            String[] roleTab = habilitation.getRole().split(",");
                            for (String s : roleTab) {
                                dbAuths.add(new SimpleGrantedAuthority(s));
                            }
                        }
                    }
                }
            }
            else {
                throw new UsernameNotFoundException("User not found");
            }
        }
        return new User(utilisateur.getGuidSso(), utilisateur.getMotDePasse(), utilisateur.isActif(), true, true, true, dbAuths);
    }

    /**
     * setter de authoritiesByUsernameQuery
     */
    public void setAuthoritiesByUsernameQuery(String queryString) {
        authoritiesByUsernameQuery = queryString;
    }

    /**
     * setter de rolePrefix
     */
    public void setRolePrefix(String rolePrefix) {
        this.rolePrefix = rolePrefix;
    }

    /**
     * setter de usernameBasedPrimaryKey
     */
    public void setUsernameBasedPrimaryKey(boolean usernameBasedPrimaryKey) {
        this.usernameBasedPrimaryKey = usernameBasedPrimaryKey;
    }

    /**
     * setter de usersByUsernameQuery
     */
    public void setUsersByUsernameQuery(String usersByUsernameQueryString) {
        this.usersByUsernameQuery = usersByUsernameQueryString;
    }

    /**
     * requete objet qui recherche les droits des utilisateur.
     */
    protected class AuthoritiesByUsernameMapping extends MappingSqlQuery {
        protected AuthoritiesByUsernameMapping(DataSource ds) {
            super(ds, authoritiesByUsernameQuery);
            declareParameter(new SqlParameter(Types.VARCHAR));
            compile();
        }

        protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
            String roleName = rolePrefix + rs.getString(2);
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(roleName);

            return authority;
        }
    }

    /**
     * recherche d'un utilisateur
     */
    protected class UsersByUsernameMapping extends MappingSqlQuery {
        protected UsersByUsernameMapping(DataSource ds) {
            super(ds, usersByUsernameQuery);
            declareParameter(new SqlParameter(Types.VARCHAR));
            compile();
        }

        protected Object mapRow(ResultSet rs, int rownum) throws SQLException {
            String username = rs.getString(1);
            boolean enabled = rs.getBoolean(2);
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("HOLDER"));
            UserDetails user = new User(username, "", enabled, true, true, true, authorities);
            return user;
        }
    }

    public AdministrationServiceSecurise getAdministrationService() {
        return administrationService;
    }

    public void setAdministrationService(
            AdministrationServiceSecurise administrationService) {
        this.administrationService = administrationService;
    }

    public String getGuidDefaut() {
        return guidDefaut;
    }

    public void setGuidDefaut(String guidDefaut) {
        this.guidDefaut = guidDefaut;
    }

}
