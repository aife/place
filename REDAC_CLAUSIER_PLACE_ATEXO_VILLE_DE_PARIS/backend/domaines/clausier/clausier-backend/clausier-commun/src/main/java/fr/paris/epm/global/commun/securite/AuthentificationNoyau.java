package fr.paris.epm.global.commun.securite;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author JOR
 */
public class AuthentificationNoyau {
    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthentificationNoyau.class);
    /**
     * info du client JAAS.
     */
    private InfoClientAuth infoClientAuth;
    /**
     * l'objet loginContext du CLient JAAS.
     */
    private LoginContext loginContext;
    /**
     * algorithme utilisé pour chifrement du password du Client JAAS.
     */
    private final String algorithmeChifrement = "DESede";
    /**
     * clé privée du chifrement.
     */
    private final String cleSecret = "123456781234567812345678";

    /**
     * Constructeur par default.
     */
    public AuthentificationNoyau() {
    }

    /**
     * Constructeur avec l'info du Client fournite.
     * @param infoClient objet POJO avec info du client JAAS
     */
    public AuthentificationNoyau(final InfoClientAuth infoClient) {
        this.infoClientAuth = infoClient;
    }

    /**
     * service d'authentification au pres serveur EJB Noyau.
     * @return resultat du method login du LoginContext
     */
    public final boolean authentication() {
        try {
            if (infoClientAuth != null && infoClientAuth.getLogin() != null
                    && infoClientAuth.getPassword() != null
                    && infoClientAuth.getLoginModuleJNDIReference() != null) {
                SecretKey key2 =
                        new SecretKeySpec(cleSecret.getBytes(),
                            algorithmeChifrement);
                Cipher ecipher = Cipher.getInstance(algorithmeChifrement);
                ecipher.init(Cipher.DECRYPT_MODE, key2);
                byte[] enc =
                        ecipher.doFinal(Base64.decodeBase64(infoClientAuth
                                .getPassword().getBytes()));
                String passwordDechifre = new String(enc);
                loginContext = null;
                AppCallbackHandler handler =
                        new AppCallbackHandler(infoClientAuth.getLogin(),
                            passwordDechifre.toCharArray());
                loginContext =
                        new LoginContext(infoClientAuth
                                .getLoginModuleJNDIReference(), handler);
                LOG.debug("Created LoginContext");
                loginContext.login();
                LOG.debug("Module Client Authentifie. Login="
                        + infoClientAuth.getLogin());
                return true;
            }
        } catch (InvalidKeyException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (NoSuchAlgorithmException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (NoSuchPaddingException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (IllegalBlockSizeException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (BadPaddingException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        return false;
    }

    /**
     * Service pour logout le client JAAS authentifié.
     */
    public final void logout() {
        try {
            if (loginContext != null) {
                loginContext.logout();
                LOG.debug("Login Context closed");
            }
        } catch (LoginException e) {
            LOG.error("Probleme logout", e.fillInStackTrace());
        }
    }
/**
 * @return loginContext
 */
    public LoginContext getLoginContext() {
        return loginContext;
    }
}
