package fr.paris.epm.global.commun.doc;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.Util;
import org.apache.poi.hssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Stack;

/**
 * Classe de factorisation pour les documents Excel .
 * @author David ABAD GARCIA
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractXLSBuilder implements DocBuilder {

    /**
     * Loggeur.
     */
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractXLSBuilder.class);

    private static final String[] ALPHABET = {"A", "B", "C", "D", "E", "F", "G",
        "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U"};

    /**
     * Formateur de dates.
     */
    private static final SimpleDateFormat SDF = new SimpleDateFormat(
        "dd/MM/yyyy");

    /**
     * chemin contenant fichier temporaire
     */
    protected String dossierTmp;
    

    /**
     * structure de fichier Excel manipulée par POI .
     */
    private HSSFWorkbook wb;

    /**
     * Attributs fichier/inputstream.
     */
    private InputStream fluxModel;

    /**
     * Pile des fichiers générés.
     */
    private Stack fichiersGeneres = new Stack();

    /**
     * méthode définie pour chaque type de document Excel à générer.
     * @throws TechnicalException
     */
    public abstract void build(String path);

    /**
     * @param flux dans lequel le fichier généré est écrit.
     * @throws TechnicalException
     */
    public final void ecrireDansFlux(final OutputStream flux) {
        try {
            wb.write(flux);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    /**
     * @return caractère correpsondant
     */
    protected String lettre(final int code) {
        return ALPHABET[code];
    }

    /**
     * @return flux du fichier généré
     * @throws TechnicalException
     */
    public final InputStream getFlux() {
        final File fichierTmp = Util.creeFichierTmp(dossierTmp);
        
        try {
            FileOutputStream fos = new FileOutputStream(fichierTmp);
            ecrireDansFlux(fos);
            FileInputStream fis = new FileInputStream(fichierTmp);
            fichiersGeneres.add(fichierTmp);
            return fis;
        } catch (FileNotFoundException e) {
        	LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    } 
    
    /**
     * @return flux du fichier généré
     * @throws TechnicalException
     */
    public final FileTmp getFichierTmp() {
        final FileTmp fichierTmp = Util.creeFichierTmp(dossierTmp);
        
        try {
            FileOutputStream fos = new FileOutputStream(fichierTmp);
            ecrireDansFlux(fos);
            return fichierTmp;
        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException(e);
        }
    }

    /**
     * @throws TechnicalException
     */
    public void nettoyer() {
        while (!fichiersGeneres.isEmpty()) {
            File fichier = (File) fichiersGeneres.pop();
            fichier.delete();
        }
    }


    /**
     * Rempli la cellule avec la chaîne.
     * @param page page où se situe la cellule
     * @param cs style de la cellule
     * @param position coordonnées de la cellule
     * @param chaine texte à mettre dans la cellule
     */
    protected void remplir(final HSSFSheet page, final HSSFCellStyle cs,
            final short[] position, final String chaine) {
        HSSFRow ligne = page.getRow(position[0]);
        if (ligne == null) {
            ligne = page.createRow(position[0]);
        }
        HSSFCell cellule = ligne.getCell(position[1]);
        if (cellule == null) {
            cellule = ligne.createCell(position[1]);
        }
        cellule.setCellStyle(cs);
        cellule.setCellValue(
            new HSSFRichTextString(chaine == null ? "" : chaine));
    }


    /**
     * Rempli la cellule avec la chaîne.
     * @param page page où se situe la cellule
     * @param position coordonnées de la cellule
     * @param chaine texte à mettre dans la cellule
     */
    protected void completer(final HSSFSheet page, final short[] position,
            final String chaine) {

//        if (chaine == null) {
//            return;
//        }

        HSSFRow ligne = page.getRow(position[0]);
        if (ligne == null) {
            ligne = page.createRow(position[0]);
        }

        HSSFCell cellule = ligne.getCell(position[1]);
        if (cellule == null) {
            cellule = ligne.createCell(position[1]);
        }

        HSSFRichTextString contenu = cellule.getRichStringCellValue();
        if (contenu == null) {
            LOG.error("Cellule sans contenu : (" + position + ")");
            return;
        }
        cellule.setCellValue(new HSSFRichTextString(contenu
            + (chaine == null? "" : chaine)));
    }

    /**
     * Rempli la cellule avec la date; ne fait rien si date nulle.
     * @param page page où se situe la cellule
     * @param cs style de la cellule
     * @param position coordonnées de la cellule
     * @param date date à remplir
     */
    protected void remplir(final HSSFSheet page, final HSSFCellStyle cs,
            final short[] position, final Date date) {
        if (date != null) {
            remplir(page, cs, position, SDF.format(date));
        } else {
            remplir(page, cs, position, "");
        }
    }

    /**
     * Rempli la cellule avec la date; ne fait rien si date nulle.
     * @param page page où se situe la cellule
     * @param cs style de la cellule
     * @param position coordonnées de la cellule
     * @param date date à remplir
     * @param sdf formateur de dates
     */
    protected void remplir(final HSSFSheet page, final HSSFCellStyle cs,
            final short[] position, final Date date,
            final SimpleDateFormat sdf) {
        if (date != null) {
            remplir(page, cs, position, sdf.format(date));
        } else {
            remplir(page, cs, position, "");
        }
    }
    
    /**
     * @param moyens liste de chaînes à séparer par des virgules
     * @return chaîne résultante
     */
    protected String separer(final List moyens) {
        String chaine = "";
        for (int i = 0; i < moyens.size(); i++) {
            if (i != 0) {
                StringBuffer strBuf = new StringBuffer();
                strBuf.append(chaine);
                strBuf.append(", ");
                strBuf.append(moyens.get(i));
                chaine = strBuf.toString();                
            } else {
                StringBuffer strBuf = new StringBuffer();
                strBuf.append(chaine);
                strBuf.append(moyens.get(i));
                chaine = strBuf.toString();                
            }
        }

        return chaine;
    }

    /**
     * Rempli la cellule avec la chaîne.
     * @param page page où se situe la cellule
     * @param position coordonnées de la cellule
     * @param date date à mettre dans la cellule
     * @param sdf formateur de dates
     */
    protected void completer(final HSSFSheet page, final short[] position,
            final Date date, final SimpleDateFormat sdf) {

        if (date != null) {
            completer(page, position, sdf.format(date));
        }
    }

    /**
     * Rempli la cellule avec la chaîne.
     * @param page page où se situe la cellule
     * @param position coordonnées de la cellule
     * @param date date à mettre dans la cellule
     */
    protected void completer(final HSSFSheet page, final short[] position,
            final Date date) {
        if (date != null) {
            completer(page, position, SDF.format(date));
        }
    }

    protected InputStream getFluxModel() {
        return fluxModel;
    }

    protected void setFluxModel(InputStream valeur) {
        this.fluxModel = valeur;
    }

    protected HSSFWorkbook getWb() {
        return wb;
    }

    protected void setWb(HSSFWorkbook wb) {
        this.wb = wb;
    }
}
