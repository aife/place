/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author Roger Iss
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class UserTag extends TagSupport {

    /**
     * Prénom de l'utilisateur
     */
    public static final String NOM_UTILISATEUR = "nom";

    /**
     * Prénom de l'utilisateur
     */
    public static final String PRENOM_UTILISATEUR = "prenom";

    /**
     * Services auxquel appartient l'utilisateur
     */
    public static final String SERVICES = "service";

    /**
     * Valeur par défaut en cas de type inconnu
     */
    public static final String ATTRIBUT_INCONNU = "non disponible";

    private static final long serialVersionUID = 1L;

    private String type = null;

    private String key = null;

    /**
     * Pour chaque valeur autorisée du paramètre "type", cette méthode inscrit
     * l'attribut de l'UserBean correspondant à l'emplacement de l'usertag.
     * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
     */
    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext
                    .getRequest();
            EpmTUtilisateur utilisateur = null;
            EpmTRefDirectionService direction = null;
            if (request.getAttribute(key) == null) {
                pageContext.getOut().println(ATTRIBUT_INCONNU);
                return SKIP_BODY;
            }
            utilisateur = (EpmTUtilisateur) request.getAttribute(key);
            direction = (EpmTRefDirectionService) request.getAttribute("direction");
            if (this.type == null) {
                this.type = "ATTRIBUT NULL";
            }

            if (NOM_UTILISATEUR.equals(this.type)) {
                pageContext.getOut().println(utilisateur.getNom());
            }
            // type = "prenom"
            else if (PRENOM_UTILISATEUR.equals(this.type)) {
                pageContext.getOut().println(utilisateur.getPrenom());
            }
            // type = "services"
            else if (SERVICES.equals(this.type)) {             
                pageContext.getOut().println(direction.getLibelle());
            }
            // type = any other value 
            else {
                pageContext.getOut().println(ATTRIBUT_INCONNU);
            }

        } catch (IOException e) {
            throw new JspException("I/O Error", e);
        }

        return SKIP_BODY;
    }


    public void release() {
        key = null;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setType(String string) {
        this.type = string;
    }
    
}
