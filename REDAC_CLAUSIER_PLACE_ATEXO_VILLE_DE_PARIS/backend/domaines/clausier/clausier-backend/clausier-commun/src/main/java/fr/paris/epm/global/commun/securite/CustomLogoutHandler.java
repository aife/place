package fr.paris.epm.global.commun.securite;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filtre Déclenché par spring lors de la deconnexion du module (logout)
 * @author MGA
 */
public class CustomLogoutHandler implements LogoutHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CustomLogoutHandler.class);

    /**
     * Acces aux objets persistants (injection Spring).
     */
    private AdministrationServiceSecurise administrationService;

    /**
     * Indique s'il faut utiliser les tokens pour detecter qu'une meme authentification est utilisee depuis differents navigateurs ou postes
     * ou qu'il y a eu un vol de cookie et deconnecter les utilisateurs.
     */
    private Boolean interdirMultiAuthentification;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            HttpSession session = request.getSession();

            // on supprime le certificat de la session :utilisé dans le cas d'authentification par certificat
            if (session.getAttribute("javax.servlet.request.X509Certificate") != null)
                session.removeAttribute("javax.servlet.request.X509Certificate");

            session.removeAttribute(ConstantesGlobales.UTILISATEUR);
            session.removeAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION);

            SecurityContextHolder.getContext().setAuthentication(null);

            // Suppresion du cookie remember me
            response.setContentType("text/html");
            Cookie cookie = new Cookie(AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY, "");
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);

            // Supression de la signature d'authentification en BDD
            String username = null;
            if (authentication != null) {
                UserDetails user = (UserDetails) authentication.getPrincipal();
                username = user.getUsername();
            }
            if (interdirMultiAuthentification == null) {
                interdirMultiAuthentification = RememberMeFilter.INTERDIR_MULTI_AUTHENTIFICATION_DEFAUT;
            }
            // On ne supprime les informations d'authentification que si on interdit a plusieur utilisateur d'utiliser le meme compte
            if (username != null && interdirMultiAuthentification) {
                EpmTUtilisateur utilisateurBdd = administrationService.chargerUtilisateur(username);
                administrationService.supprimerAuthentificationToken(utilisateurBdd.getId(), username);
            }

        } catch (TechnicalNoyauException e) {
            LOG.error("Erreur lors de la recherche de l'utilisateur en base " + e.getMessage(), e);
        }
    }

    public void setAdministrationService(final AdministrationServiceSecurise valeur) {
        this.administrationService = valeur;
    }

    public final void setInterdirMultiAuthentification(final Boolean valeur) {
        this.interdirMultiAuthentification = valeur;
    }

}
