package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.BiclefFile;
import org.springframework.web.multipart.MultipartFile;

import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.IOException;

/**
 * Created by nty on 05/04/17.
 */
public class BiclefFileConstraintValidator implements ConstraintValidator<BiclefFile, MultipartFile> {

    @Override
    public void initialize(BiclefFile biclef) {

    }

    @Override
    public boolean isValid(MultipartFile field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {
            try {
                X509Certificate.getInstance(field.getInputStream());
            } catch (IOException | CertificateException ex) {
                return false;
            }
        }
        return true;
    }

}
