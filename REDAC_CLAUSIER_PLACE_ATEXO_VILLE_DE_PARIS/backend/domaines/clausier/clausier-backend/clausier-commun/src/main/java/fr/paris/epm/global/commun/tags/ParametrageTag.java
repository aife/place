package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Collection;

/**
 * Permet d'interagir avec la table epm__t_ref_parametrage qui contient le
 * parametrage de l'application propre a la version cliente. Si
 * testEgal/testDifferent sont renseignes alors le corps de la balise est
 * affiche si la valeur de la clef correspond/differe. Si testEgal/testDifferent
 * ne sont pas renseignes, alors la balise est remplacee par la valeur de la
 * clef.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class ParametrageTag extends TagSupport {

    /**
     * Identifiant de serialisation.
     */
    private static final long serialVersionUID = 6021268112150816013L;

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ParametrageTag.class);

    /**
     * Affichage du corps de la balise si la valeur est egale a testEgal.
     */
    private String testEgal;

    /**
     * Affichage du corps de la balise si la valeur est differente de testDifferent.
     */
    private String testDifferent;

    /**
     * Acces au referentiel (injection Spring).
     */
    private static NoyauProxy noyauProxy;

    /**
     * Identifie un parametrage.
     */
    private String clef;
    
    /**
     * Identifiant de la procedure
     */
    private Integer idProcedure;

    @Override
    public int doStartTag() throws JspException {

        String valeurBdd;

        if (clef == null) {
            LOG.error("La clef doit être renseignée.");
            return EVAL_BODY_INCLUDE;
        }
        if (testEgal != null && testDifferent != null) {
            LOG.error("Impossible de tester à la fois testEgal et testDifferent.");
            return EVAL_BODY_INCLUDE;
        }

        // Recherche du parametrage
        EpmTRefParametrage refParametrage = null;

        try {
            if (idProcedure!=null) {
                // Recherche du parametrage par rapport à une procedure
                Collection<EpmTRefProcedure> refProcedures = noyauProxy.getReferentiels().getRefProcedure();
                EpmTRefProcedure refProcedure = refProcedures.stream()
                        .filter(procedure -> procedure.getId() == idProcedure)
                        .findFirst()
                        .orElse(null);

                if (refProcedure != null && refProcedure.getEpmTRefParametrageSet() != null)
                    refParametrage = refProcedure.getEpmTRefParametrageSet().stream()
                            .filter(parametre -> parametre.getClef().equals(clef))
                            .findFirst()
                            .orElse(null);
            }
            else{
                EpmTRef referentiel = noyauProxy.getReferentielByReference(clef, TypeEpmTRefObject.PARAMETRAGE);
                if (referentiel != null)
                    refParametrage = (EpmTRefParametrage) referentiel;
            }
        } catch (TechnicalException e) {
            LOG.error("Erreur lors de la recuperation du parametrage " + clef + " en base.", e);
            return EVAL_BODY_INCLUDE;
        }
        if (refParametrage == null) {
            LOG.debug("Parametrage " + clef + " absent en base.");
            return SKIP_BODY;
        }
        valeurBdd = refParametrage.getValeur();

        if (testEgal != null) {
            if (testEgal.equals(valeurBdd))
                return EVAL_BODY_INCLUDE;
            else
                return SKIP_BODY;
        }

        if (testDifferent != null) {
            if (testDifferent.equals(valeurBdd))
                return SKIP_BODY;
            else
                return EVAL_BODY_INCLUDE;
        }

        try {
            pageContext.getOut().print(valeurBdd);
        } catch (IOException e) {
            LOG.error("Erreur lors de l'ecriture de la valeur " + valeurBdd + " dans la jsp.", e);
        }

        return EVAL_BODY_INCLUDE;
    }

    @Override
    public void release() {
        super.release();
        testEgal = testDifferent = clef = null;
    }

    /**
     * @param valeur Identifie un parametrage.
     */
    public final void setClef(final String valeur) {
        this.clef = valeur;
    }

    /**
     * @param valeur Affichage du corps de la balise si la valeur est egale a testEgal.
     */
    public final void setTestEgal(final String valeur) {
        this.testEgal = valeur;
    }

    /**
     * @param valeur Affichage du corps de la balise si la valeur est differente de testDifferent.
     */
    public final void setTestDifferent(final String valeur) {
        this.testDifferent = valeur;
    }

    /**
     * @param valeur Acces au referentiel (injection Spring).
     */
    @Autowired
    public final void setNoyauProxy(final NoyauProxy valeur) {
        noyauProxy = valeur;
    }

    /**
     * @param valeur the idProcedure to set
     */
    public final void setIdProcedure(final Integer valeur) {
        this.idProcedure = valeur;
    }
    
}
