package fr.paris.epm.global.commun.securite;

import fr.paris.epm.global.commun.ConstantesGlobales;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Appelee pour chaque requete d'accès au module Rédaction en mode service.
 * Vérifie que l'identifiant passé en paramètre est bien présent en mémoire
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public class ExterneRechercheSecurityFilter extends RequestHeaderAuthenticationFilter {

	@Override
	protected Object getPreAuthenticatedPrincipal(
			HttpServletRequest request) {
		String signature = request.getParameter(ConstantesGlobales.IDENTIFIANT_SAAS);
		return signature;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(
			HttpServletRequest paramHttpServletRequest) {
		return "N/A";
	}
}
