/**
 * 
 */
package fr.paris.epm.global.commun.generationdocument.builder;

/**
 * @author Mounthei
 */
public abstract class AbstractGenerationDocumentBuilder {

    /**
     * @param consultation
     */
    public abstract void buildAbstractGenerationDocumentBean();

}
