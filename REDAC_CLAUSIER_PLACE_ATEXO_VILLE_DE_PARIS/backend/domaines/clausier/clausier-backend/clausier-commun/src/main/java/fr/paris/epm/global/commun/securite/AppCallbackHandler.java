package fr.paris.epm.global.commun.securite;

import javax.security.auth.callback.*;

/**
 * Class CallbackHandler pour gerer l'envoi de l'info Client au serveur JAAS.
 * @author JOR
 */
public class AppCallbackHandler implements CallbackHandler {
    /**
     * login du client JAAS.
     */
    private String username;
    /**
     * password du client JAAS.
     */
    private char[] password;

    /**
     * Constructeur.
     * @param username login du Client JAAS
     * @param password password du Client JAAS
     */
    public AppCallbackHandler(final String username, final char[] password) {
        this.username = username;
        this.password = password;
    }

    /**
     * methode implementé de l'interface CallbackHandler.
     * @params callbacks callbacks avec info du client JAAS
     * @thows UnsupportedCallbackException exception du handler
     */
    public final void handle(final Callback[] callbacks)
            throws UnsupportedCallbackException {
        for (int i = 0; i < callbacks.length; i++) {
            if (callbacks[i] instanceof NameCallback) {
                NameCallback nc = (NameCallback) callbacks[i];
                nc.setName(username);
            } else if (callbacks[i] instanceof PasswordCallback) {
                PasswordCallback pc = (PasswordCallback) callbacks[i];
                pc.setPassword(password);
            } else {
                throw new UnsupportedCallbackException(callbacks[i],
                    "Unrecognized Callback");
            }
        }
    }
}
