package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.Util;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class ValeurBCTag extends TagSupport {



    /**
     * 
     */
    private static final long serialVersionUID = 704020531374573609L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;
    
    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ValeurBCTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           null);
            if (texte == null) {
                return Tag.SKIP_BODY;
            }
            String resultat = "";
            if (texte instanceof String) {
                String contenu = (String) texte;
                if (ConstantesGlobales.BC_SANS_VALEUR
                        .equalsIgnoreCase(contenu)
                        || ConstantesGlobales.BC_SANS_VALEUR_TO_STRING
                                .equalsIgnoreCase(contenu)) {
                    contenu = ConstantesGlobales.BC_SANS;
                }
                resultat = contenu;
            } else if (texte instanceof Double) {
                Double valeur = (Double) texte;
                resultat = Util.doubleToStringOuSANS(valeur);
            }
            pageContext.getOut().print(resultat);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
    }
    
    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        name = valeur;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }
}
