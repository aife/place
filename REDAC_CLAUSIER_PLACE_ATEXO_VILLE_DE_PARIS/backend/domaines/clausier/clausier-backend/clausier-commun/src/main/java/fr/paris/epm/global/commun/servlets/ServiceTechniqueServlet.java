package fr.paris.epm.global.commun.servlets;

import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet de test de connection entre un module et le noyau.
 * @author Léon Barsamian
 *
 */
public class ServiceTechniqueServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ServiceTechniqueServlet.class);
    
    /**
     * Le service technique (sauvegarde de fichier, etc ...).
     * Injecté par spring.
     */
    private static ServiceTechniqueSecurise serviceTechniqueService;

    public void init() throws ServletException {
        // Put your code here
    }

    /**
     * Destruction of the servlet. <br>
     */
    public void destroy() {
        super.destroy(); // Just puts "destroy" string in log
        // Put your code here
    }
    
    /**
     * @param request {@HttpServletRequest}
     * @param response {@HttpServletResponse}
     * @throws ServletException
     */
    public void doGet(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException {
        response.setContentType("text/html");
        PrintWriter out;
        String resultat = "";
        try {
            resultat = serviceTechniqueService.ping();
        } catch (IllegalArgumentException e1) {
            LOG.error(e1.getMessage(), e1.fillInStackTrace());
            resultat = e1.toString();
        }
        try {
            out = response.getWriter();
            out.println(resultat);
            out.close();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        

    }

    /**
     * @param serviceTechnique the serviceTechnique to set
     */
    public final void setServiceTechniqueService(
            ServiceTechniqueSecurise valeur) {
        ServiceTechniqueServlet.serviceTechniqueService = valeur;
    }

}
