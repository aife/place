package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by nty on 16/05/17.
 */
public interface SafelyReferentielFacade {

    static final Logger log = LoggerFactory.getLogger(SafelyReferentielFacade.class);

    public Referentiels getReferentiels();

    public SafelyNoyauInvokator getSafelyInvokator();

    default <T extends EpmTRef> List<T> safelyListReferentiels( Supplier<Collection<T>> getter) {
        return getSafelyInvokator().safelyInvoke(() -> (List<T>) getter.get());
    }

    default <T extends EpmTRef> List<Directory> safelyListDirectories( Supplier<Collection<T>> getter) {

        return safelyListReferentiels(getter).stream()
                .map(s -> new Directory(s.getId(), s.getLibelle()))
                .collect(Collectors.toList());
    }

    default <T extends EpmTRef> T safelyReferentiel( Supplier<Collection<T>> getter, int id) {
        return safelyListReferentiels(getter).stream()
                .filter(s -> s.getId() == id)
                .findFirst().orElse(null);
    }

    default Directory safelyDirectory( Supplier<Collection<EpmTRef>> getter, int id) {

        EpmTRef s = safelyReferentiel(getter, id);
        return new Directory(s.getId(), s.getLibelle());
    }

}
