package fr.paris.epm.global.coordination.facade;
import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;

import java.util.List;

/**
 * Interface pour toutes les facades
 * Created by nty on 03/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface Facade<B extends Bean> {


    /**
     * Cherche un bean par son Id
     * @param id id du bean
     * @return bean
     */
    public B findById(int id, EpmTRefOrganisme epmTRefOrganisme) ;


    public List<B> find(String field, Object value, EpmTRefOrganisme epmTRefOrganisme) ;

    public List<B> findStrict(String field, String value, EpmTRefOrganisme epmTRefOrganisme) ;

    /**
     * Enregistre / crée un bean
     * @param bean entité à modifier / créer.
     * @return le nouveau bean
     */
    public B save(final B bean, EpmTRefOrganisme epmTRefOrganisme) ;

    /**
     * Suppression d'un bean
     * @param id id du bean à supprimer
     */
    public boolean remove(final int id);

}
