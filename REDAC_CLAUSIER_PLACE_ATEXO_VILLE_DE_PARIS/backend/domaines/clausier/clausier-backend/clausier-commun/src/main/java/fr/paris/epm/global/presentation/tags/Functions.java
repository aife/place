package fr.paris.epm.global.presentation.tags;

import fr.paris.epm.global.commun.Config;
import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.global.coordination.bean.Parametrage;
import fr.paris.epm.global.coordination.facade.DirectoryFacade;
import fr.paris.epm.noyau.persistance.EpmTHabilitation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by nty on 20/02/17.
 */
@Component
public class Functions {

    private static DirectoryFacade directoryFacade;

    @Autowired
    public final void setDirectoryFacade(final DirectoryFacade directoryFacade) {
        Functions.directoryFacade = directoryFacade;
    }

    public static boolean contains(Collection collection, Object obj) {
        return collection.contains(obj);
    }

    public static boolean containsId(Collection<Bean> collection, Bean bean) {
        return collection.stream().anyMatch(b -> bean.getId() == b.getId());
    }

    public static boolean filtreSecurite(EpmTUtilisateur utilisateur, String role, Boolean condition) {

        if (utilisateur == null || utilisateur.getProfil() == null)
            return false;

        Optional<String> o = utilisateur.getProfil().stream()
                .flatMap(p -> p.getHabilitationAssocies().stream())
                .map(EpmTHabilitation::getRole)
                .filter(Objects::nonNull)
                .map(r -> r.split(","))
                .flatMap(Arrays::stream)
                .filter(r -> r.equalsIgnoreCase(role))
                .findFirst();

        return o.isPresent() ? condition : !condition;
            // si on trouve la role alors retour est selon la condition
            //      si on attend TRUE (presence) et on l'a vu alors        => TRUE
            //      sinon ->    FALSE (absence)  alors                     => FALSE
            // sinon inverse
            //      si on attend TRUE (presence) et on ne l'a pas vu alors => FALSE
            //      sinon ->    FALSE (absence)  alors                     => TRUE
    }

    public static boolean parametrage(String reference, Boolean condition) {

        Parametrage parametrage = directoryFacade.findParametrage(reference);

        if (parametrage == null)
            return false;

        return parametrage.getValeur().equals(condition.toString());
    }

    public static String parametre(String reference) {

        Parametrage parametrage = directoryFacade.findParametrage(reference);

        if (parametrage == null)
            return null;

        return parametrage.getValeur();
    }

    public static String propriete(String clef) {
        return Config.getProperties().getProperty(clef);
    }

}
