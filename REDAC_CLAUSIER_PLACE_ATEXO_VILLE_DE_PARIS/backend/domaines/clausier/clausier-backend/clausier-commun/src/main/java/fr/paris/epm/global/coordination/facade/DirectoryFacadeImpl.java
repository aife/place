package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.global.coordination.bean.Parametrage;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefCcag;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.ReferentielsServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Facade de gestion des Annuaires
 * - types marchés
 * - boamp sites
 * - boamp organismes
 * - departements
 * - utilisateurs
 * - directions/services
 * - pouvoirs adjudicateur
 * - natures
 * - types budget
 * - status de demande d'achat
 * - types de besoin de demande d'achat
 * - CCAG
 * - types marchés-attributions
 * - opérations travaux / unités fonctionnelles
 * - codes d'achat
 * - imputations budgetaires
 * - procedures
 * - écrans
 * - destinataires
 * - parametrage
 * Created by nty on 14/03/17.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class DirectoryFacadeImpl extends AbstractFacade implements DirectoryFacade, SafelyReferentielFacade {

    private static final Logger log = LoggerFactory.getLogger(DirectoryFacadeImpl.class);
    private ReferentielsServiceSecurise referentielsService;

    @Override
    public Referentiels getReferentiels() {
        return getNoyauProxy().getReferentiels();
    }


    @Override
    public List<Directory> findListNatures() {
        return safelyListDirectories(getReferentiels()::getRefNature);
    }



    @Override
    public List<Directory> findListCcag() {
        Collection<EpmTRefCcag> epmTRefCcags = getSafelyInvokator().safelyInvoke(() -> getNoyauProxy().getReferentiels().getRefCcagTous());

        return epmTRefCcags.stream()
                .map(c -> new Directory(c.getId(), c.getLibelle()))
                .sorted(Comparator.comparing(Directory::getLabel))
                .collect(Collectors.toList());
    }


    @Override
    public List<Directory> findListProcedures() {
        return safelyListDirectories(getReferentiels()::getRefProcedureEcriture);
    }

    @Override
    public List<Directory> findListProceduresByOrganisme(Integer idOrganisme) {
        Collection<EpmTRefProcedure> epmTRefProcedures = referentielsService.getProcedureByIdOrganisme(idOrganisme);
        return epmTRefProcedures.stream().map(s -> new Directory(s.getId(), s.getLibelle()))
                .collect(Collectors.toList());
    }
    @Override
    public Parametrage findParametrage(String reference) {
        EpmTRefParametrage epmTRefParametrage = getSafelyInvokator().safelyInvoke(() ->
                (EpmTRefParametrage) getNoyauProxy().getReferentielByReference(reference, TypeEpmTRefObject.PARAMETRAGE));

        if (epmTRefParametrage == null)
            return null;

        Parametrage parametrage = new Parametrage();
        parametrage.setId(epmTRefParametrage.getId());
        parametrage.setLabel(epmTRefParametrage.getLibelle());
        parametrage.setClef(epmTRefParametrage.getClef());
        parametrage.setValeur(epmTRefParametrage.getValeur());
        return parametrage;
    }

}
