package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.ZipCode;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nty on 21/03/17.
 */
public class ZipCodeConstraintValidator implements ConstraintValidator<ZipCode, String> {

    @Override
    public void initialize(ZipCode zipCode) {

    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {
            if (!field.matches("[0-9]+") || field.length() != 5)
                return false;
        }
        return true;
    }

}