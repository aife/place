package fr.paris.epm.global.coordination.bean;

/**
 * Bean Directory.
 * Created by nty on 08/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class Directory extends AbstractBean {

    private String uid;

    private String label;

    private boolean actif;

    private String shortLabel;

    private String externalCode;

    public Directory() {
        this(0, "");
    }

    public Directory(int id, String label) {
        super();
        setId(id);
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

	public String getUid() {
		return uid;
	}

	public void setUid( String uid ) {
		this.uid = uid;
	}
}
