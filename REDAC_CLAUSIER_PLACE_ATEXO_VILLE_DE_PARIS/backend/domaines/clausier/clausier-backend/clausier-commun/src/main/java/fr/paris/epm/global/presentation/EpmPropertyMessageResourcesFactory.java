package fr.paris.epm.global.presentation;

import fr.paris.epm.global.commun.Config;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.PropertyMessageResources;
import org.apache.struts.util.PropertyMessageResourcesFactory;

import java.util.Properties;

public class EpmPropertyMessageResourcesFactory extends PropertyMessageResourcesFactory {

    public MessageResources createResources(String config) {

        Properties properties = Config.getProperties();

        PropertyMessageResources messagesResourcesDefautCommun = new PropertyMessageResources(
                this, "ApplicationResources-commun",
                returnNull);

	    PropertyMessageResources messagesResourcesDefaut = new PropertyMessageResources(
		    this, "ApplicationResources",
		    returnNull);

        String messages = properties.getProperty("messages.bundle.externe");

        if (null == messages) {
	        messages = "epm/libelle/ApplicationResources";
        }

        return new EpmPropertyMessageResources(
                this, messages,
                returnNull, messagesResourcesDefaut, messagesResourcesDefautCommun);
    }

}
