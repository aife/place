/**
 * $Id$
 */
package fr.paris.epm.global.commun.servlets;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * Listener pour afficher le log si l'utilisateur dans la session est ajouté, modifié ou supprimé. 
 * @author GAO Xuesong
 */
public final class UtilisateurSessionListener implements HttpSessionAttributeListener {

	/**
	 * Loggueur.
	 */
	private static final Logger LOG = LoggerFactory
			.getLogger(UtilisateurSessionListener.class);

	@Override
	public void attributeAdded(HttpSessionBindingEvent se) {
		String evenementSession = "ajouté";
		afficherMessage(se, evenementSession);
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent se) {
		String evenementSession = "supprimé";
		afficherMessage(se, evenementSession);
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent se) {
		String evenementSession = "modifié";
		afficherMessage(se, evenementSession);
	}
	
	/**
	 * Si l'utilisateur dans la session est ajouté, modifié ou supprimé. Cette
	 * méthode va afficher le log indiquant le profil et les habilitations de
	 * cet utilisateur.
	 * 
	 * @param se HttpSessionBindingEvent
	 * @param evenement ajouté, modifié ou supprimé
	 */
	private void afficherMessage(HttpSessionBindingEvent se, String evenement){
		try {
			String nomAttribut = se.getName();
			if(ConstantesGlobales.UTILISATEUR.equals(nomAttribut)){
				LOG.info("Utilisateur dans la session a été " + evenement);
				EpmTUtilisateur utilisateur = (EpmTUtilisateur) se.getValue();
				LOG.info(utilisateur.afficherListeHabilitation());
			}
		} catch (Exception e) {
			LOG.error("Erreur pendant la récupération de l'utilisateur " + evenement +" dans la sesssion");
		}
	}

}
