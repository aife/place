package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.Superior;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SuperiorConstraintValidator implements ConstraintValidator<Superior, Object> {

    String superiorOfField;

    @Override
    public void initialize(Superior constraintAnnotation) {
        superiorOfField = constraintAnnotation.superiorOfField();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return true;
    }

}
