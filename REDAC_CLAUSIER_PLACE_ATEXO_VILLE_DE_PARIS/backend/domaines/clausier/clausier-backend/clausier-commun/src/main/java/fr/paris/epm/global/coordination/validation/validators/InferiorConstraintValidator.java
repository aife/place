package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.Inferior;

import javax.validation.ConstraintValidatorContext;

public class InferiorConstraintValidator extends AbstractConstraintValidator<Inferior, Object> {

    String inferiorOfField;

    @Override
    public void initialize(Inferior constraintAnnotation) {
        inferiorOfField = constraintAnnotation.inferiorOfField();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return true;
    }

}
