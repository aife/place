package fr.paris.epm.global.presentation;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commons.exception.UtilisateurException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.commun.Util;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.global.coordination.objetValeur.Utilisateur;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.persistance.EpmTProfil;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * @author Guillaume Beraudo
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractAction extends Action {

    /**
     * Le LOG.
     */
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractAction.class);

    /**
     * Proxy vers le noyau permettant d'obtenir les objets en base quasi
     * immuables (referentiels, selects).
     */
    protected static NoyauProxy noyauProxy;

    /**
     * Mapper (injection Spring)
     */
    protected static DozerBeanMapper conversionService;

    /**
     * Code du "mapping forward" en cas de succes.
     */
    public static final String SUCCES = "succes";

    /**
     * Code du bouton valider.
     */
    public static final String VALIDER = "valider";

    /**
     * Code du "mapping forward" dans le cas valide.
     */
    public static final String VALIDE = "valide";

    /**
     * Code du "mapping forward" dans le cas retour.
     */
    public static final String RETOUR = "retour";

    /**
     * Code du "mapping forward" dans le cas de l'annulation.
     */
    public static final String ANNULER = "annuler";

    /**
     * Code du bouton valider.
     */
    public static final String ENREGISTRER = "enregistrer";

    /**
     * Code du "mapping forward" en cas de controle.
     */
    public static final String INIT = "init";

    /**
     * Code du "mapping forward" en cas de probleme.
     */
    public static final String ERREUR = "erreur";

    /**
     * Code du "mapping forward" en cas de collection de consultations vides.
     */
    public static final String VIDE = "vide";

    /**
     * Code du "mapping forward" en cas de tri.
     */
    public static final String TRI = "tri";

    /**
     * Code du "mapping forward" en cas de generation de document.
     */
    public static final String GENERER_DOCUMENT = "GENERER_DOCUMENT";

    /**
     * Le nom du param pour indiquer s'il faut créer la session
     */
    public static final String NOM_PARAM_INITIALISATION_SESSION = "initialisationSession";

    /**
     * Cas d'un "submit" correspondant a  une validation.
     */
    public static final int ENVOI_VALIDATION = 4;

    /**
     * Cas d'un "submit" correspondant a  un enregistrement.
     */
    public static final int ENVOI_ENREGISTREMENT = 5;

    /**
     * Cas d'un "submit" correspondant a  une annulation.
     */
    public static final int ENVOI_ANNULATION = 6;

    /**
     * creer la session si elle n'existe pas
     */
    private boolean initialisationSession = false;

    /**
     * nettoie la session si true
     */
    private boolean nettoyerSession = false;

    /**
     * nettoie la session si true
     */
    private boolean nettoyerSessionSaufForm = false;

    /**
     * Acces aux fichiers de libelle (injection Spring).
     */
    protected ResourceBundleMessageSource messageSource;

    protected ActionForward gestionSession(SessionManagerGlobal sessionManager,
                                           ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {
        // vérifier si la propriété initialisation session est renseigné dans la requete
        String initialisationSessionParamStr = request.getParameter(NOM_PARAM_INITIALISATION_SESSION);
        boolean initialisationSessionParam = false;
        if (initialisationSessionParamStr != null) {
            try {
                initialisationSessionParam = Boolean.parseBoolean(initialisationSessionParamStr);
            } catch (Exception e) {
                LOG.debug("Erreur lors de la parse de param initialisationSession");
            }
        }

        HttpSession session = request.getSession(false);
        if (initialisationSession || initialisationSessionParam) {
            session = request.getSession(true);
            sessionManager.setSessionValide(session, true);
        } else {
            session = sessionManager.getSession(request);
        }

        if (nettoyerSession) {
            sessionManager.nettoyerTous(session);
        } else {
            sessionManager.ajouterForm(session, mapping.getName());
            if (nettoyerSessionSaufForm)
                sessionManager.nettoyerTous(session, mapping.getName());
        }

        String titreEcran = request.getParameter(ConstantesGlobales.S_TITRE_ECRAN);
        if (titreEcran != null && !titreEcran.isEmpty())
            sessionManager.ajouterElement(session, ConstantesGlobales.S_TITRE_ECRAN, titreEcran);
        else
            sessionManager.ajouterElement(session, ConstantesGlobales.S_TITRE_ECRAN, "");
        
        return execute(sessionManager, mapping, form, request, response);
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        SessionManagerGlobal sessionManager = SessionManagerGlobal.getInstance();
        return gestionSession(sessionManager, mapping, form, request, response);
    }

    public abstract ActionForward execute(SessionManagerGlobal sessionManager, ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception;

    /**
     * @param request requete HTTP
     * @return utilisateur courant
     * @throws UtilisateurException utilisateur inconnu
     */
    public final Utilisateur getUtilisateur(final HttpServletRequest request) throws UtilisateurException {
        Utilisateur utilisateur = (Utilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION);
        if (utilisateur == null) {
            String msg = "Il n'y a aucun utilisateur en session";
            throw new UtilisateurException(msg);
        }
        return utilisateur;
    }

    /**
     * @param request requete HTTP
     * @return direction/service de l'utilisateur courant
     * @throws UtilisateurException utilisateur inconnu
     * @throws TechnicalException   erreur technique
     */
    public final DirectionService getDirectionUtilisateur(final HttpServletRequest request) throws UtilisateurException {
        final DirectionService ds;
        final Utilisateur util;
        final int idDs;

        util = getUtilisateur(request);
        idDs = util.getDirectionService().getId();
        ds = noyauProxy.getDirService(idDs);
        return ds;
    }

    /**
     * @param request requete HTTP
     * @return utilisateur courant
     * @throws UtilisateurException utilisateur inconnu
     */
    public final EpmTUtilisateur getUtilisateurProfil(final HttpServletRequest request) throws UtilisateurException {
        EpmTUtilisateur utilisateur = (EpmTUtilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR);
        if (utilisateur == null) {
            String msg = "Il n'y a aucun utilisateur en session";
            throw new UtilisateurException(msg);
        }
        return utilisateur;
    }

    public boolean verificationHabilitation(final String nomRole, Collection<EpmTProfil> profils) {
        return Util.verificationHabilitation(nomRole, profils);
    }


    /**
     * @param valeur proxy vers le noyau
     */
    public void setNoyauProxy(final NoyauProxy valeur) {
        noyauProxy = valeur;
    }

    /**
     * @param valeur mappeur de proprietes (injection Spring).
     */
    public void setConversionService(final DozerBeanMapper valeur) {
        conversionService = valeur;
    }

    /**
     * @param valeur mappeur de proprietes (injection Spring).
     */
    public void setInitialisationSession(final boolean valeur) {
        this.initialisationSession = valeur;
    }

    /**
     * @param valeur mappeur de proprietes (injection Spring).
     */
    public void setNettoyerSession(final boolean valeur) {
        this.nettoyerSession = valeur;
    }

    /**
     * @param valeur mappeur de proprietes (injection Spring).
     */
    public void setNettoyerSessionSaufForm(final boolean valeur) {
        this.nettoyerSessionSaufForm = valeur;
    }

    /**
     * @param valeur Acces aux fichiers de libelle (injection Spring).
     */
    public void setMessageSource(final ResourceBundleMessageSource valeur) {
        this.messageSource = valeur;
    }

}
