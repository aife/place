package fr.paris.epm.global.coordination.objetValeur;

import fr.paris.epm.noyau.metier.objetvaleur.Doublet;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;

import java.util.AbstractMap;

/**
 * Class clef-valeur.
 * Created by nty on 25/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class SimplePair extends AbstractMap.SimpleImmutableEntry<Integer, String> {

    public SimplePair(final Integer id, final String libelle) {
        super(id, libelle);
    }

    public SimplePair(final EpmTRef referentiel) {
        this(referentiel.getId(), referentiel.getLibelle());
    }

    @Deprecated // solition temporaire avant de suppression Doublet dans le noyau
    public SimplePair(final Doublet doublet) {
        this(doublet.getId(), doublet.getLibelle());
    }

    // créer pour remplacer les anciennes classes Paire, SelectItem, Doublet et LabelValueBean
    public Integer getId() {
        return super.getKey();
    }

    // créer pour remplacer les anciennes classes Paire, SelectItem, Doublet et LabelValueBean
    public String getLibelle() {
        return super.getValue();
    }

    @Deprecated // à supprimer (créer pour remplacer les anciennes classes Paire et LabelValueBean)
    public final String getValue() {
        return String.valueOf(super.getKey());
    }

    @Deprecated // à supprimer (créer pour remplacer les anciennes classes Paire et LabelValueBean)
    public final String getLabel() {
        return super.getValue();
    }

    @Override
    public String toString() {
        return this.getId() + " -> " + this.getLibelle();
    }

}
