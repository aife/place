package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.commun.Util;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefFichePratique;
import org.apache.struts.Globals;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class FichePratiqueTag extends TagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    private static NoyauProxy noyauProxy;
    /**
     * Référence de la fiche pratique
     */
    private String reference;
    
    /**
     * Clé du ressource bundle
     */
    private String key;

    /**
     * Fichier de bundle
     */
    private String bundle;

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(FichePratiqueTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {

            EpmTRefFichePratique fichePratique = null;

            try {
                if (reference != null && !reference.isEmpty()) {
                    EpmTRef referentiel = noyauProxy.getReferentielByReference(reference, TypeEpmTRefObject.FICHE_PRATIQUE);
                    if (referentiel != null)
                        fichePratique = (EpmTRefFichePratique) referentiel;
                }
            } catch (TechnicalException e) {
                LOG.error("Erreur lors de la récupération de la fiche pratique dont la référence est " + reference, e);
            }
            String message = TagUtils.getInstance().message(pageContext, bundle, Globals.LOCALE_KEY, key);
            if (fichePratique != null) {
                if (!fichePratique.isActif()) {
                    return Tag.SKIP_BODY;
                }
                pageContext.getOut().print(creationScriptHTML(fichePratique.getUrl(), message));
            } else {
                pageContext.getOut().print(creationScriptHTML(null, message));
            }

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    private String creationScriptHTML(String url, final String message) {
        if (url != null && !url.isEmpty()) {
            if(url.startsWith("www.")) {
                url = "http://" + url;
            }
            return "<a class=\"fiche-pratique\" href=\"" + Util.echapperQuotes(url) + "\" target=\"_blank\">" + message + "</a>";
        } 
        return "";
    }

    /*
     * (non-Javadoc) @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        reference = null;
    }

    /**
     * @param valeur noyauProxy
     */
    public void setNoyauProxy(final NoyauProxy valeur) {
        FichePratiqueTag.noyauProxy = valeur;
    }

    /**
     * @param valeur reference
     */
    public void setReference(final String valeur) {
        this.reference = valeur;
    }
    
    public void setKey(final String key) {
        this.key = key;
    }
    
    public void setBundle(final String bundle) {
        this.bundle = bundle;
    }

}
