package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.InferiorConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { InferiorConstraintValidator.class })
public @interface Inferior {

    String inferiorOfField();

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
