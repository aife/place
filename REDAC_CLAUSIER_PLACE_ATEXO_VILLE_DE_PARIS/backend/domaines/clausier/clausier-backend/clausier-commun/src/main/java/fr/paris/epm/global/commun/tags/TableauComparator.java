package fr.paris.epm.global.commun.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

public class TableauComparator<T> implements Comparator<T> {

    private static final Logger LOG = LoggerFactory.getLogger(TableauComparator.class);

    private String propriete;

    public int compare(T objet1, T objet2) {
        try {
            if (propriete == null) {
                return -1;
            }
            String[] methodes = propriete.split("\\.");

            Object result1 = objet1;
            Object result2 = objet2;

            for (String m : methodes) {
                String methode = creationMethode(m);
                Class[] clazz1 = new Class[1];
                clazz1[0] = result1.getClass();
                Method method1 = clazz1[0].getMethod(methode);

                Class[] clazz2 = new Class[1];
                clazz2[0] = result2.getClass();
                Method method2 = clazz2[0].getMethod(methode);

                result1 = method1.invoke(result1);
                if (result1 == null)
                    return -1;
                result2 = method2.invoke(result2);
                if (result2 == null)
                    return 1;
            }

            if (result1 instanceof String && result2 instanceof String) {
                return ((String) result1).compareToIgnoreCase((String) result2);

            } else if (result1 instanceof Integer && result2 instanceof Integer) {
                return ((Integer) result1 - (Integer) result2);

            } else if (result1 instanceof Date && result2 instanceof Date) {
                int result = ((Date) result1).compareTo((Date) result2);
                if (result > 0)
                    return -1;
                if (result < 0)
                    return 1;
                return result;

            } else if (result1 instanceof Calendar && result2 instanceof Calendar) {
                int result = ((Calendar) result1).getTime().compareTo(((Calendar) result2).getTime());
                if (result > 0)
                    return -1;
                if (result < 0)
                    return 1;
                return result;
            }

        } catch (NoSuchMethodException e) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Le tri n'a pas abouti car l'attribut " + propriete + " n'a pas été trouvé pour l'objet :" + objet1.getClass().toString());
                LOG.debug(e.getMessage());
            }
        } catch (InvocationTargetException | IllegalAccessException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        return 0;
    }

    public void setPropertieComparer(String propriete) {
        this.propriete = propriete;
    }

    private static String creationMethode(String methode) {
        if (methode == null || methode.length() == 0)
            return "";
        return "get" + methode.substring(0, 1).toUpperCase() + methode.substring(1);
    }

}
