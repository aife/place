package fr.paris.epm.global.commun.tags;

import org.apache.struts.taglib.html.CheckboxTag;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import java.util.List;

public class CheckBoxTag extends CheckboxTag {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = -8361412087198784995L;

	/**
     * Marqueur du fichier journal.
     */
    private List<String> identifiants = null;
    
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        identifiants = (List<String>)session.getAttribute("listeIdentifiants");
        int resultat = super.doStartTag();
        return resultat;
        
    }
    
    /**
     * Determines if the checkbox should be checked.
     *
     * @return true if checked="checked" should be rendered.
     * @throws JspException
     * @since Struts 1.2
     */
    @Override
    protected boolean isChecked()
        throws JspException {
        boolean checked = false;
		if (identifiants != null) {
			if (identifiants.contains(value)) {
				checked = true;
			}
		}
        return checked;
    }
}
