package fr.paris.epm.global.coordination.objetValeur;

import java.io.Serializable;
import java.util.List;

/**
 * Bean d'un courriel.
 * @author Léon Barsamian
 * @version $Revision$, $Date$, $Author$
 */
public class CourrielGeneriqueBean  implements Serializable {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -4932943783931854378L;

    /**
     * Id du courriel en base.
     */
    private int id;

    /**
     * Destinataire du courriel.
     */
    private String destinataire;

    /**
     * Expéditeur du courriel.
     */
    private String expediteur;

    /**
     * Message du courriel.
     */
    private String message;

    /**
     * Objet du courriel.
     */
    private String objet;
    
    /**
     * Liste des piéces jointes.
     */
    private List<PieceJointeCourrielGeneriqueBean> listePiecesJointes;

    /**
     * Taille des piéces jointes du mail pour validation.
     */
    private long tailleTotalPiecesJointe;
    /**
     * @return Id du courriel en base.
     */
    public final int getId() {
        return id;
    }

    /**
     * @param valeur Id du courriel en base.
     */
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return destinataire
     */
    public final String getDestinataire() {
        return destinataire;
    }

    /**
     * @param valeur le destinataire
     */
    public final void setDestinataire(final String valeur) {
        this.destinataire = valeur;
    }

    /**
     * @return l'expediteur
     */
    public final String getExpediteur() {
        return expediteur;
    }

    /**
     * @param valeur l'expediteur
     */
    public final void setExpediteur(final String valeur) {
        this.expediteur = valeur;
    }

    /**
     * @return le message
     */
    public final String getMessage() {
        return message;
    }

    /**
     * @param valeur le message
     */
    public final void setMessage(final String valeur) {
        this.message = valeur;
    }

    /**
     * @return objet du message.
     */
    public final String getObjet() {
        return objet;
    }

    /**
     * @param valeur objet du message.
     */
    public final void setObjet(final String valeur) {
        this.objet = valeur;
    }

    /**
     * @return Liste des piéces jointes.
     */
    public final List<PieceJointeCourrielGeneriqueBean> getListePiecesJointes() {
        return listePiecesJointes;
    }

    /**
     * @param valeur Liste des piéces jointes.
     */
    public final void setListePiecesJointes(final List<PieceJointeCourrielGeneriqueBean> valeur) {
        this.listePiecesJointes = valeur;
    }

    /**
     * @return Taille des piéces jointes du mail pour validation.
     */
    public final long getTailleTotalPiecesJointe() {
        return tailleTotalPiecesJointe;
    }

    /**
     * @param valeur Taille des piéces jointes du mail pour validation.
     */
    public final void setTailleTotalPiecesJointe(final long valeur) {
        this.tailleTotalPiecesJointe = valeur;
    }
}
