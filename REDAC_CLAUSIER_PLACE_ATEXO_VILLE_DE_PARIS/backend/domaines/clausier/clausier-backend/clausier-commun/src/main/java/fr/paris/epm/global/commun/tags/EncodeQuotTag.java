/**
 * 
 */
package fr.paris.epm.global.commun.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant replacer une "'" par "&quot".
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class EncodeQuotTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String texte;

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EncodeQuotTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            if (texte == null) {
                return Tag.SKIP_BODY;
            }

            pageContext.getOut()
                    .print(texte.replaceAll("'", "&#146;"));

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        texte = null;
    }

  

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setTexte(final String valeur) {
        texte = valeur;
    }

}
