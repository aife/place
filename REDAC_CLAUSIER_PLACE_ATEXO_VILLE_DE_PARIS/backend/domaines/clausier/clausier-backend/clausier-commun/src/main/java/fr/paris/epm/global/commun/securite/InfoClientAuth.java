package fr.paris.epm.global.commun.securite;

/**
 * Class avec l'info du client JAAS.
 * @author JOR
 */
public class InfoClientAuth {
    /**
     * le login du client JAAS.
     */
    private String login;
    /**
     * le password du client JAAS.
     */
    private String password;
    /**
     * la reference JNDI du Domain securisé.
     */
    private String loginModuleJNDIReference;

    /**
     * Constructeur par default.
     */
    public InfoClientAuth() {
    }

    /**
     * injection Spring.
     * @param valeur valeur.
     */
    public final void setLogin(final String valeur) {
        this.login = valeur;
    }

    /**
     * injection Spring.
     * @param valeur password.
     */
    public final void setPassword(final String valeur) {
        this.password = valeur;
    }

    /**
     * injection Spring.
     * @param valeur la reference JNDI du Domain securisé.
     */
    public final void setLoginModuleJNDIReference(final String valeur) {
        this.loginModuleJNDIReference = valeur;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getLoginModuleJNDIReference() {
        return loginModuleJNDIReference;
    }

}
