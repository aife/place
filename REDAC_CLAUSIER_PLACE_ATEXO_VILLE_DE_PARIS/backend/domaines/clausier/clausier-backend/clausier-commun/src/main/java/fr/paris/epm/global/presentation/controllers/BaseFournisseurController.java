package fr.paris.epm.global.presentation.controllers;

import fr.paris.epm.global.coordination.BaseFournisseurProvider;
import fr.paris.epm.noyau.configuration.UtilisateurHandler;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;

/**
 * Controller permettant de se connecter à la base fournisseur
 * Created by sta on 19/05/16.
 */
@Controller
public class BaseFournisseurController {

    @Autowired
    AdministrationServiceSecurise administrationService;

    @Autowired
    BaseFournisseurProvider baseFournisseurService;

    @RequestMapping(value = {"/baseFournisseur"}, method = RequestMethod.GET)
    public ModelAndView redirectBaseFournisseur(@UtilisateurHandler EpmTUtilisateur utilisateur,
                                                @RequestParam(required = false, value = "motCles") String motCles,
                                                @RequestParam(required = false, value = "selectionContact") boolean selectionContact,
                                                @RequestParam(required = false, value = "callBack") String callBack,
                                                @RequestParam(required = false, value = "contrats") String contrats) {

        String tokenSso = administrationService.genererTokenUnique(utilisateur);

        if (motCles != null && !motCles.isEmpty()) {
            try {
                motCles = java.net.URLEncoder.encode(motCles, "UTF-8").replace("+", "%20");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        ModelAndView baseFournisseur = new ModelAndView("redirect:" + baseFournisseurService.fetchBaseFournisseurUrl(tokenSso, motCles, selectionContact, callBack, contrats));
        return baseFournisseur;
    }

}
