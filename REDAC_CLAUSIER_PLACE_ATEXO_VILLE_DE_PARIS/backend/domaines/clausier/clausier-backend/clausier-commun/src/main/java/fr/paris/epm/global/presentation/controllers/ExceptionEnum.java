package fr.paris.epm.global.presentation.controllers;

import lombok.AllArgsConstructor;
import lombok.Getter;

public enum ExceptionEnum {
    NOT_FOUND(404, "Non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement"),
    DELETE_ERROR(500, "Erreur lors de la suppression"),
    NOT_AUTHORIZED(401, "Non autorisé"),
    FORBIDDEN(403, "Accès refusé"),
    MANDATORY(400, "Champs obligatoires"),
    DATA(400, "Champs de type différent"),
    NO_RESULT(500, "Aucun résultat"),
    TECHNICAL_ERROR(500, "Erreur technique");

    private final int code;
    private final String type;

    ExceptionEnum(int code, String type) {
        this.code = code;
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }
}
