package fr.paris.epm.global.commun;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

/**
 * Permet de trier n'importe quel collection d'objet comparable selon un champ
 * fournit en parametre.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class SimpleGenericComparator implements Comparator<Object> {

    public static enum OrdreEnum {CROISSANT, DECROISSANT};
    
    private static final Logger LOG = LoggerFactory.getLogger(SimpleGenericComparator.class);

    private String property;
    
    private OrdreEnum ordre = OrdreEnum.CROISSANT;

    public SimpleGenericComparator(final String property) {
        this.property = property;
    }
    
    public SimpleGenericComparator(final String property, final OrdreEnum ordre) {
        this.property = property;
        this.ordre = ordre;
    }

    @Override
    public int compare(final Object o1, final Object o2) {

        int res;
        Object obj1, obj2;
        Comparable<Object> obj1Cmp, obj2Cmp;

        obj1 = obj2 = null;

        try {
            obj1 = PropertyUtils.getProperty(o1, property);
        } catch (IllegalAccessException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (InvocationTargetException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (NoSuchMethodException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        try {
            obj2 = PropertyUtils.getProperty(o2, property);
        } catch (IllegalAccessException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (InvocationTargetException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        } catch (NoSuchMethodException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }

        if ((obj1 == null) || (obj2 == null)) {
            LOG.error("Un des objets de la liste trier par GenericComparator n'a pas de champ "
                    + property);
            return -1;
        }

        if (!(obj1 instanceof Comparable) || !(obj2 instanceof Comparable)) {
            LOG.error("Un des objets de la liste trier par GenericComparator a un champ "
                    + property + " qui n'est pas comparable.");
            return -1;
        }

        obj1Cmp = (Comparable<Object>) obj1;
        obj2Cmp = (Comparable<Object>) obj2;

        if (ordre == OrdreEnum.CROISSANT) {
            res =  obj1Cmp.compareTo(obj2Cmp);
        } else {
            res =  obj2Cmp.compareTo(obj1Cmp);
        }
        
        return res;
    }

}
