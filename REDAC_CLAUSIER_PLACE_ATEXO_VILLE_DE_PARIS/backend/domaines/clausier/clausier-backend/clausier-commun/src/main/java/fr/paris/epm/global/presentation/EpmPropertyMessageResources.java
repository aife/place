package fr.paris.epm.global.presentation;

import org.apache.struts.util.MessageResourcesFactory;
import org.apache.struts.util.PropertyMessageResources;

import java.util.Locale;

/**
 * Surcharge de methodes utilisees par Struts pour permettre la recherche des libelles specifiques avant ceux par defaut.
 * @author Remi Ville
 * @version 2010/06/06
 */
public class EpmPropertyMessageResources extends PropertyMessageResources {

    /**
     * Identifiant de serialisation.
     */
    private static final long serialVersionUID = -8770979912822426088L;

    /**
     * Objet de recherche des libelles par defaut.
     */
    private PropertyMessageResources messagesResourcesDefaut;

    /**
     * Objet de recherche des libelles communs par defaut.
     */
    private PropertyMessageResources messagesResourcesDefautCommun;

    public EpmPropertyMessageResources(MessageResourcesFactory factory, String config, boolean returnNull,
                                       PropertyMessageResources messagesResourcesDefaut,
                                       PropertyMessageResources messagesResourcesDefautCommun) {
        super(factory, config, returnNull);
        this.messagesResourcesDefaut = messagesResourcesDefaut;
        this.messagesResourcesDefautCommun = messagesResourcesDefautCommun;
    }

    /**
     * Retourne le libelle correspondant a la clef key et la locale, en allant chercher
     * d'abord dans le fichier des libelles specifique avant celui par defaut.
     * @param locale langue
     * @param key    clef
     * @return le libelle correspondant a la clef key et la locale
     */
    public String getMessage(Locale locale, String key) {

        String message = null;
        locale = new Locale("fr_FR");

        message = super.getMessage(locale, key);

        if ((message == null) || (message.startsWith("???"))) {
            message = messagesResourcesDefaut.getMessage(locale, key);
            if ((message == null || message.startsWith("???")) && messagesResourcesDefautCommun != null) {
                message = messagesResourcesDefautCommun.getMessage(locale, key);
            }
        }

        return message;
    }
}
