package fr.paris.epm.global.coordination.bean;

import java.io.Serializable;

/**
 * Interface pour toutes les beans
 * Created by nty on 17/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class AbstractBean implements Bean, Serializable {

    /**
     * Identifiant.
     */
    private int id;

    @Override
    public final int getId() {
        return id;
    }

    @Override
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != this.getClass())
            return false;

        Bean b = (Bean) o;
        return this.getId() == b.getId();
    }

}
