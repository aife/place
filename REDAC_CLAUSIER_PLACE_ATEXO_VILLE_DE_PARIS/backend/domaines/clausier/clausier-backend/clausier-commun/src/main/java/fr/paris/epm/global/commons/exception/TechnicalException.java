package fr.paris.epm.global.commons.exception;



/**
 * Classe gérant les erreurs d'ordre technique.
 * @author Régis Menet
 */

public class TechnicalException extends RuntimeException {

    /**
     * Marqueur de sérialization propagé aux enfants.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public TechnicalException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause exception ayant provoqué cette exception
     */
    public TechnicalException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public TechnicalException(final String msg) {
        super(msg);
    }

    /**
     * @param cause exception ayant provoqué cette exception
     */
    public TechnicalException(final Throwable cause) {
        super(cause);
    }
}
