package fr.paris.epm.global.coordination.objetValeur;

import java.util.AbstractMap;

/**
 * Class clef-valeur.
 * Created by nty on 25/01/19.
 * @author Nikolay Tyurin
 * @author nty
 */
public class SimpleCouple extends AbstractMap.SimpleImmutableEntry<String, String>  {

    public SimpleCouple(final String key, final String value) {
        super(key, value);
    }

    // créer pour remplacer les anciennes classes SelectItem et LabelValueBean
    public String getId() {
        return super.getKey();
    }

    // créer pour remplacer les anciennes classes SelectItem et LabelValueBean
    public String getLibelle() {
        return super.getValue();
    }

    @Deprecated // à supprimer (créer pour remplacer l'ancienne classe LabelValueBean)
    public final String getValue() {
        return String.valueOf(super.getKey());
    }

    @Deprecated // à supprimer (créer pour remplacer l'ancienne classe LabelValueBean)
    public final String getLabel() {
        return super.getValue();
    }

    @Override
    public String toString() {
        return this.getId() + " -> " + this.getLibelle();
    }

}