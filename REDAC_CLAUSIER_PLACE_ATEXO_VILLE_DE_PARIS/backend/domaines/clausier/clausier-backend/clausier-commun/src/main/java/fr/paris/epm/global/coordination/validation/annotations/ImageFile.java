package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.ImageFileConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by nty on 11/04/17.
 */
@Documented
@Constraint(validatedBy = ImageFileConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ImageFile {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
