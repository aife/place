package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.coordination.bean.Directory;
import fr.paris.epm.global.coordination.bean.Parametrage;

import java.util.List;

public interface DirectoryFacade {


    List<Directory> findListNatures() ;


    List<Directory> findListCcag() ;

    List<Directory> findListProcedures() ;

    List<Directory> findListProceduresByOrganisme(Integer idOrganisme);


    Parametrage findParametrage(String reference) ;

}
