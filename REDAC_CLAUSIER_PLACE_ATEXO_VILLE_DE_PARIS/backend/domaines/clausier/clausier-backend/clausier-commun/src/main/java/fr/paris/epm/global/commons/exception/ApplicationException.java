package fr.paris.epm.global.commons.exception;


/**
 * Classe gérant les erreurs d'ordre fonctionnel.
 * @author Régis Menet
 */

public class ApplicationException extends RuntimeException {
    /**
     * Marqueur de sérialization
     */
    private static final long serialVersionUID = 1L;

    public ApplicationException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause exception ayant provoqué cette exception
     */
    public ApplicationException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public ApplicationException(final String msg) {
        super(msg);
    }

    /**
     * @param cause exception ayant provoqué cette exception
     */
    public ApplicationException(final Throwable cause) {
        super(cause);
    }
}
