package fr.paris.epm.global.commun.servlets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.Map;

/**
 * Support pour la migration de struts vers Spring
 * les liens .epm sont redirigés vers la page .htm (Spring) si elle existe et vers la page .epm (Struts) sinon
 * Created by qba on 12/05/16.
 */
public class MigrationStrutsFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationStrutsFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        //Ssi on a le paramètre ?migrationFilter=true
        Map parameterMap = servletRequest.getParameterMap();
        if (parameterMap.containsKey("migrationFilter")) {
            //Wrapper d'erreur lors de la requête vers .htm (Spring MVC)
            HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper((HttpServletResponse) servletResponse) {
                @Override
                public void sendError(int sc) throws IOException {
                    //requete vers .epm  (Struts)
                    String requestURI = ((HttpServletRequest) servletRequest).getServletPath().replace(".htm", ".epm");
                    try {
                        servletRequest.getRequestDispatcher(requestURI).forward(request, servletResponse);
                    } catch (ServletException e) {
                        LOG.error("erreur lors de la requete {0}", e, requestURI);
                        super.sendError(sc);
                    }
                }
            };
            //requete vers .htm (Spring MVC)
            filterChain.doFilter(servletRequest, wrapper);
        } else {
            //requete non filtrée
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
