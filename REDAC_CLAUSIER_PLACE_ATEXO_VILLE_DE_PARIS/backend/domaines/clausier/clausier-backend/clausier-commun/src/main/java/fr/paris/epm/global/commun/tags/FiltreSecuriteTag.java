package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.persistance.EpmTHabilitation;
import fr.paris.epm.noyau.persistance.EpmTProfil;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefTypeMarche;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.Iterator;
import java.util.List;

/**
 * Cette classe permet de filtrer le code HTML suivant les droits de
 * l'utilisateur.
 * @author Régis Menet
 * @version $Revision:$, $Date:$, $Author:$
 */
public class FiltreSecuriteTag extends BodyTagSupport {
    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(FiltreSecuriteTag.class);
    private static final long serialVersionUID = 1L;

    private String role;

    private boolean egal;
    
    private static NoyauProxy noyauProxy;

    private int idProcedure = -1;

    public int doStartTag() throws JspException {
        ServletRequest request = (ServletRequest) pageContext.getRequest();
        this.pageContext.getAttribute("utilisateur");
        EpmTUtilisateur utilisateur = (EpmTUtilisateur) request.getAttribute("utilisateur");
        if (utilisateur == null) {
        	LOG.warn("Utilisateur n'est pas présent dans la requête lors de l'exécution de FiltreSecuriteTag");
            return SKIP_BODY;
        }
        boolean isPresent = false;
        if (utilisateur.getProfil() != null) {
            Iterator it = utilisateur.getProfil().iterator();
            while (it.hasNext()) {
                EpmTProfil profil = (EpmTProfil) it.next();
                List<EpmTRefTypeMarche> listTypeMarche = utilisateur.getListeTypeMarches(profil);
                EpmTRefTypeMarche typeMarche = null;
                if (idProcedure != -1) {
                    List list;
                    try {
                        list = (List) noyauProxy.getReferentiels().getRefProcedureLecture();
                    } catch (TechnicalException e) {
                        LOG.error(e.getMessage(), e.fillInStackTrace());
                        throw new JspException("Erreur d'E/S", e);
                    }
                    for(int i = 0; i < list.size(); i++) {
                        EpmTRefProcedure procedure = (EpmTRefProcedure) list.get(i);
                        if (procedure.getId() == idProcedure) {
                            if (procedure.getEpmTRefTypeMarche() != null){
                            	typeMarche = procedure.getEpmTRefTypeMarche();
                                break;
                            }
                        }
                    }
                }
                if (typeMarche == null || (listTypeMarche.contains(typeMarche))) {
                    if (profil.getHabilitationAssocies() != null) {
                        Iterator it2 = profil.getHabilitationAssocies()
                                .iterator();
                        while (it2.hasNext()) {
                            EpmTHabilitation habilitation = (EpmTHabilitation) it2
                                    .next();

                            String[] roleTab = null;
                            if (habilitation.getRole() != null) {
                                roleTab = habilitation.getRole().split(",");
                                for (int i = 0; i < roleTab.length; i++) {

                                	if(role != null && !role.contains(",")){
                                		if (egal) {
                                			if (roleTab[i].trim().equalsIgnoreCase(role)) {
                                				return EVAL_BODY_INCLUDE;
                                			}
                                		} else {
                                			if (roleTab[i].equalsIgnoreCase(role)) {
                                				isPresent = true;
                                				break;
                                			}
                                		}
									} else if (role != null && role.contains(",")) {
										for (String str : role.split(",")) {
											if (egal) {
												if (roleTab[i].trim().equalsIgnoreCase(str)) {
													return EVAL_BODY_INCLUDE;
												}
											} else {
												if (roleTab[i].equalsIgnoreCase(role)) {
													isPresent = true;
													break;
												}
											}
										}
									}
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!isPresent && !egal) {
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    public int doAfterBody() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        role = null;
        egal = true;
        idProcedure = -1;
    }

    /**
     * @param valeur du role
     */
    public void setRole(String valeur) {
        this.role = valeur;
    }

	/**
     * @param valeur du role
     */
    public void setIdProcedure(int valeur) {
        this.idProcedure = valeur;
    }

    /**
     * @param valeur du role
     */
    public void setEgal(boolean valeur) {
        this.egal = valeur;
    }
    
    /**
     * @param valeur injecté par String
     */
    @Autowired
    public void setNoyauProxy(NoyauProxy valeur) {
        noyauProxy = valeur;
    }

}
