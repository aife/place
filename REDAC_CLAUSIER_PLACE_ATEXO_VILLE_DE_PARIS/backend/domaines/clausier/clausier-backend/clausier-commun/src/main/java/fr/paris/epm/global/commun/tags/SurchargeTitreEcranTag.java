package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.ConstantesGlobales;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;


/**
 * Cette classe permet de gerer le nom des ecrans par rapport au lien qui été cliqué sur sommaire.
 *
 * @author Régis Menet
 * @version: $, $Revision: $, $Author: $
 */
public class SurchargeTitreEcranTag extends TagSupport {

    /**
     * identifiant de serialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Valeur de la propriete
     */
    private String libelleParDefault;

    @Override
    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext());

        ResourceBundleMessageSource resourceBundleMessageSource = (ResourceBundleMessageSource) webApplicationContext.getBean("messageSource");

        HttpSession session = request.getSession();
        String titre = (String) session.getAttribute(ConstantesGlobales.S_TITRE_ECRAN);

        if (titre != null && !titre.isEmpty()) {

            titre = resourceBundleMessageSource.getMessage(titre, null, request.getLocale());
        } else {
            if (libelleParDefault != null) {
                titre = resourceBundleMessageSource.getMessage(libelleParDefault, null, request.getLocale());
            }
        }
        try {
            pageContext.getOut().print(titre);
        } catch (IOException e) {
            throw new JspException(e.fillInStackTrace());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        libelleParDefault = null;
    }

    /**
     * @param resourcebundle par defaut
     */
    public void setLibelleParDefault(String valeur) {
        this.libelleParDefault = valeur;
    }
}
