package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.global.commons.exception.ApplicationException;
import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.commun.securite.RememberMeServices;
import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Appelé pour chaque requete ayant le paramètre "identifiantSaaS". Ce paramètre
 * correspond au jeton d'authentification renvoyé par le noyau après
 * identification par webService. Si le jeton est présent en base l'utilisateur
 * est authentifé et les informations d'autentifications sont mises en session.
 * Un attribut est également ajouté à la session pour indiquer que l'application doit s'afficher en mode SaaS
 * Les autres filtres seront alors utilisés.
 * @author MGA
 * @version $Revision$, $Date$, $Author$
 */
public class ExterneRechercheDocumentServiceSecurityInterceptor extends FilterSecurityInterceptor {
    
    private static ServiceTechniqueSecurise serviceTechnique;
    
    private static RememberMeServices rememberMeServices;
    
    
    
    /**
     * Loguer.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExterneRechercheDocumentServiceSecurityInterceptor.class);


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        FilterInvocation fi = new FilterInvocation(request, response, chain);
        logRequest(fi.getHttpRequest());
        try {
			verifieToken(fi.getHttpRequest(), fi.getHttpResponse());
		} catch (ApplicationException e) {
			LOG.error(e.getMessage(), e);
		}
		chain.doFilter(request, response);
    }

    /**
     * Verification que le token fournit en parametre est present en base et
     * n'est pas expire.
     * @throws ApplicationException 
     */
    private void verifieToken(HttpServletRequest request, HttpServletResponse response) {


        SessionManagerGlobal sessionManager = SessionManagerGlobal.getInstance();
        HttpSession session = null;
        try {
            session = sessionManager.getSession(request);
        } catch (SessionExpireeException e) {
        	session = request.getSession(true);
            sessionManager.setSessionValide(session, true);
        }
        
        
        
        // Recuperation de l'identifant utilisateurExterne/consultation
        String identifiantExterne = request.getParameter(ConstantesGlobales.IDENTIFIANT_SAAS);

        if (identifiantExterne != null) {

            // Recuperation utilisateur et consultation

            UtilisateurExterneConsultation utilExteneConsult = serviceTechnique
                    .getUtilisateurExterneConsultation(identifiantExterne);
            if (utilExteneConsult != null) {
                EpmTUtilisateur utilExterne = utilExteneConsult.getUtilisateur();

                if (utilExterne == null) {
                    throw new ApplicationException("Utilisateur absent.");
                }
                request.setAttribute(ConstantesGlobales.UTILISATEUR, utilExterne);
                // initialisation du paramétre pour l'accés externe qui cachera header, footer, menu gauche.
                session.setAttribute(ConstantesGlobales.ACCES_MODE_SAAS, ConstantesGlobales.ACCES_MODE_SAAS);
                Authentication authentication = new UsernamePasswordAuthenticationToken(
                        utilExterne.getGuidSso(), utilExterne.getMotDePasse());
                rememberMeServices.onLoginSuccess(request, response, authentication);
                LOG.info("User authenticated");
            } else {
                throw new ApplicationException("Identifiant incorrect");
            }
        }
    }

    /**
     * Logger la requete entrante.
     */
    private void logRequest(HttpServletRequest request) {

        String queryString = "";
        if (request.getQueryString() != null) {
            queryString = request.getQueryString();
        }

        LOG.info("Requete REST : " + request.getRequestURL() + "?" + queryString);
    }

	public final void setServiceTechnique(final ServiceTechniqueSecurise valeur) {
		ExterneRechercheDocumentServiceSecurityInterceptor.serviceTechnique = valeur;
	}

	public final void setRememberMeServices(final RememberMeServices valeur) {
		ExterneRechercheDocumentServiceSecurityInterceptor.rememberMeServices = valeur ;
	}
    
}
