package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.InEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by nty on 23/03/17.
 */
public class InEnumConstraintValidator implements ConstraintValidator<InEnum, Integer> {

    Class<? extends Enum> enumClass;

    @Override
    public void initialize(InEnum inEnum) {
        enumClass = inEnum.enumClass();
    }

    @Override
    public boolean isValid(Integer field, ConstraintValidatorContext cxt) {
        try {
            Method findById = enumClass.getMethod("findById", int.class);
            return findById.invoke(null, field) != null;
        } catch (NoSuchMethodException | SecurityException |
                InvocationTargetException | IllegalAccessException ex) {
            //log.error("Class " + enumClass + " ne supporte pas methode \"findById\" !", ex);
            throw new RuntimeException("Class " + enumClass + " ne supporte pas methode \"findById\" !", ex);
        }
    }

}
