package fr.paris.epm.global.coordination.objetValeur;

import fr.paris.epm.global.commun.FileTmp;

import java.io.Serializable;

/**
 * Brean de présentation pour les piéces jointes d'un {@link CourrielGeneriqueBean}.
 * @author Léon Barsamian
 *
 */
public class PieceJointeCourrielGeneriqueBean implements Serializable {

    /**
     * SErialisation.
     */
    private static final long serialVersionUID = -3744739624307026412L;

    /**
     * Fichier {@link FileTmp} en session - dans le cas ou la piéce jointe n'est
     * pas encore enregistrée dans le noyau (par exemple les documents oofice
     * générés dynamiquement).
     */
    private FileTmp fichierSession;
    
    /**
     * Identifiant de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    private Integer idPieceJointe;
    
    /**
     * Nom de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    private String nomPieceJointe;
    
    /**
     * Taille de la piéce jointe pour l'affichage (ie 123ko).
     */
    private String taillePieceJointeAffichage;
    
    /**
     * Taille de la piéce jointe pour le calcul.
     */
    private long taillePieceJointeCalcul;
    
    private String url;

    /**
     * @return Fichier {@link FileTmp} en session - dans le cas ou la piéce jointe n'est
     * pas encore enregistrée dans le noyau (par exemple les documents oofice
     * générés dynamiquement).
     */
    public final FileTmp getFichierSession() {
        return fichierSession;
    }

    /**
     * @param valeur Fichier {@link FileTmp} en session - dans le cas ou la piéce jointe n'est
     * pas encore enregistrée dans le noyau (par exemple les documents oofice
     * générés dynamiquement).
     */
    public final void setFichierSession(final FileTmp valeur) {
        this.fichierSession = valeur;
    }

    /**
     * @return Identifiant de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    public final Integer getIdPieceJointe() {
        return idPieceJointe;
    }

    /**
     * @param valeur Identifiant de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    public final void setIdPieceJointe(final Integer valeur) {
        this.idPieceJointe = valeur;
    }

    /**
     * @return Nom de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    public final String getNomPieceJointe() {
        return nomPieceJointe;
    }

    /**
     * @param valeur Nom de la piéce jointe si elle est déjà enregistrée dans le noyau.
     */
    public final void setNomPieceJointe(final String valeur) {
        this.nomPieceJointe = valeur;
    }

    /**
     * @return Taille de la piéce jointe pour l'affichage (ie 123ko).
     */
    public final String getTaillePieceJointeAffichage() {
        return taillePieceJointeAffichage;
    }

    /**
     * @param valeur Taille de la piéce jointe pour l'affichage (ie 123ko).
     */
    public final void setTaillePieceJointeAffichage(final String valeur) {
        this.taillePieceJointeAffichage = valeur;
    }

    /**
     * @return Taille de la piéce jointe pour le calcul.
     */
    public final long getTaillePieceJointeCalcul() {
        return taillePieceJointeCalcul;
    }

    /**
     * @param valeur Taille de la piéce jointe pour le calcul.
     */
    public final void setTaillePieceJointeCalcul(final long valeur) {
        this.taillePieceJointeCalcul = valeur;
    }

    public final String getUrl() {
        return url;
    }

    public final void setUrl(final String valeur) {
        this.url = valeur;
    }

}
