package fr.paris.epm.global.commun.servlets;

import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Permet d'autorsier les requetes CROSS-domaine
 * Utilisé :
 * - Appel de fonction js (dans rsem) à partir du module exec (base fournisseur) : appel de la fonction remplirContact
 */
public class CORSFilter implements Filter {

    private static final String ALLOWED_DOMAINS_REGEXP = ".*";

    private static ServiceTechniqueSecurise serviceTechniqueService;

    private static final Logger logger = LoggerFactory.getLogger(CORSFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        //QBA developpement spécifique paris
       /* String guidSSO = req.getParameter("login");
        String token = req.getParameter("token");
        String username = req.getParameter("j_username");
        String password = req.getParameter("j_password");
        if (guidSSO != null && token != null && username != null && password != null) {
            if (username.equals(guidSSO)) {
                UtilisateurCritere utilisateurCritere = new UtilisateurCritere();
                utilisateurCritere.setIdentifiant(guidSSO);
                EpmTUtilisateur utilisateur = serviceTechniqueService.chercherUniqueEpmTObject(0, utilisateurCritere);
                if (utilisateur != null && utilisateur.getMotDePasse().equals(token)) {
                    utilisateur.setMotDePasse(password);
                    utilisateur.setGuidSso(guidSSO);
                    utilisateur = serviceTechniqueService.modifierEpmTObject(utilisateur);
                    logger.info("L'utilisateur {} a été modifié", utilisateur.getIdentifiant());
                }
            }
        }*/
        //QBA developpement spécifique paris

        String origin = req.getHeader("Origin");
        if (origin != null && origin.matches(ALLOWED_DOMAINS_REGEXP)) {
            resp.addHeader("Access-Control-Allow-Origin", origin);
            if ("options".equalsIgnoreCase(req.getMethod())) {
                resp.setHeader("Allow", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS");

                String headers = req.getHeader("Access-Control-Request-Headers");
                String method = req.getHeader("Access-Control-Request-Method");
                resp.addHeader("Access-Control-Allow-Methods", method);
                resp.addHeader("Access-Control-Allow-Headers", headers);
                // optional, only needed if you want to allow cookies.
                resp.addHeader("Access-Control-Allow-Credentials", "true");
                resp.setContentType("text/plain");

                resp.getWriter().flush();
                return;
            }
        }

        // Fix ios6 caching post requests
        if ("post".equalsIgnoreCase(req.getMethod())) {
            resp.addHeader("Cache-Control", "no-cache");
        }

        if (filterChain != null) {
            filterChain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {

    }

    public void setServiceTechniqueService(ServiceTechniqueSecurise serviceTechniqueService) {
        CORSFilter.serviceTechniqueService = serviceTechniqueService;
    }
}
