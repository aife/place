package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefInfoBulle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant de tronquer un mot.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class InfoBulleTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    /**
     * 
     */
    private static NoyauProxy noyauProxy;
    /**
     * 
     */
    private String reference;

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InfoBulleTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            EpmTRefInfoBulle infoBulle = null;

            try {
                EpmTRef referentiel = noyauProxy.getReferentielByReference(reference, TypeEpmTRefObject.INFO_BULLE);
                if (referentiel != null)
                    infoBulle = (EpmTRefInfoBulle) referentiel;

            } catch (TechnicalException e) {
                LOG.error("Erreur lors de la récupération de l'infobulle dont le code est " + reference, e);
            }
            if (infoBulle != null) {
                if (!infoBulle.isActif()) {
                    return Tag.SKIP_BODY;
                }
                String contenuFixe = fixerContenu(infoBulle.getContenu());
                pageContext.getOut().print(creationScriptHTML(contenuFixe, infoBulle.getFichePratique()));
            } else {
                pageContext.getOut().print("ND");
            }

        } catch (IOException e) {
        	LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    private final String creationScriptHTML(String contenu, String url) {
        StringBuffer script = new StringBuffer();
        script.append("<img src=\"" + HrefTag.getHrefRacine() + "images/picto-info-bulle.png \" onmouseover=\"afficheInfoBulleLien(");
        script.append("'infoBulle','");
        script.append(contenu);
        script.append("','");
        script.append(url);
        script.append("', this)\" onmouseout=\"cacheInfoBulle('infoBulle')\" class=\"picto-info-bulle\" title=\"Info-bulle\">");
        return script.toString();
    }

    private final String fixerContenu(String contenu) {
        String contenuFixe = "";
        int trouve = -1;
        String subChaine = "";
        while ((trouve = contenu.indexOf("'")) != -1) {
            if (contenu.length() > (trouve + 1)) {
                subChaine += contenu.substring(0, trouve) + "\\'";
                contenu = contenu.substring(trouve + 1);
            } else {
                subChaine += contenu.substring(0, trouve) + "\\'";
                contenu = "";
            }
        }
        while ((trouve = contenu.indexOf('"')) != -1) {
            if (contenu.length() > (trouve + 1)) {
                subChaine += contenu.substring(0, trouve) + "&quot;";
                contenu = contenu.substring(trouve + 1);
            } else {
                subChaine += contenu.substring(0, trouve) + "&quot;";
                contenu = "";
            }
        }
        contenuFixe = subChaine + contenu;
        return contenuFixe;
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        reference = null;

    }

    /**
     * @param valeur noyauProxy
     */
    public void setNoyauProxy(NoyauProxy valeur) {
        InfoBulleTag.noyauProxy = valeur;
    }

    /**
     * @param valeur reference
     */
    public void setReference(String valeur) {
        this.reference = valeur;
    }

}
