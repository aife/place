package fr.paris.epm.global.presentation.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controlleur Abstrait de gestion des appeles générique
 * (il ne faut avoir qu'une seule instance de cette classe par module)
 * (module Passation le contient par défaut)
 * Created by nty on 25/09/19.
 * @author Nikolay Tyurin
 * @author nty
 */
@Controller
public class MainController extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @RequestMapping(value = "/errorMultiWindow")
    public final String main() {
        logger.info("/errorMultiWindow.htm");
        return "popinErrorMultiWindowLayout";
    }

}
