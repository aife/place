package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.commons.exception.ApplicationException;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.function.Supplier;

/**
 * Class SafelyNoyauInvokatorImpl
 * Created by nty on 07/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class SafelyNoyauInvokatorImpl implements SafelyNoyauInvokator {

    static final Logger log = LoggerFactory.getLogger(SafelyNoyauInvokatorImpl.class);

    public <E> E safelyInvoke(Supplier<E> supplier) {
        try {
            return supplier.get();
        } catch (TechnicalNoyauException | NonTrouveNoyauException ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException(ex);
        } catch (RemoteConnectFailureException ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException("echec connexion noyau", ex.fillInStackTrace());
        } catch (ApplicationException ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalException(ex);
        }
    }

}
