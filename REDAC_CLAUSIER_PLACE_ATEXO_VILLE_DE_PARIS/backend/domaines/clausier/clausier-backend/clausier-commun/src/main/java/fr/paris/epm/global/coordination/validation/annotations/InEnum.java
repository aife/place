package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.InEnumConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by nty on 23/06/17.
 */
@Documented
@Constraint(validatedBy = InEnumConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface InEnum {

    Class<? extends Enum> enumClass();

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
