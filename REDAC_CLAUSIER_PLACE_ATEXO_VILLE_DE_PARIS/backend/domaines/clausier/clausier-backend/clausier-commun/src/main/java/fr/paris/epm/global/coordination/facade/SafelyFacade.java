package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.service.GeneriqueServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by nty on 14/04/17.
 */
public interface SafelyFacade {

    static final Logger log = LoggerFactory.getLogger(SafelyFacade.class);

    public GeneriqueServiceSecurise getService() ;

    public SafelyNoyauInvokator getSafelyInvokator() ;

    public <K extends Critere> Supplier<K> critereContructor() ;

    default <T extends EpmTObject, K extends Critere> List<T> safelyFind(K critere) {
        return getSafelyInvokator().safelyInvoke(() -> (List<T>) getService().chercherEpmTObject(0, critere));
    }

    default <T extends EpmTObject, K extends Critere> List<T> safelyFind(String sortBy) {
        K critere = (K) critereContructor().get();
        critere.setChercherNombreResultatTotal(false);

        if (sortBy != null && !sortBy.isEmpty()) {
            critere.setProprieteTriee(sortBy);
            critere.setTriCroissant(true);
        }

        return safelyFind(critere);
    }

    default <T extends EpmTObject, K extends Critere> List<T> safelyFind(final String field, final Object value, final Boolean strict) {

        K critere = (K) critereContructor().get();

        String setter = "set" +
                field.substring(0, 1).toUpperCase() +
                field.substring(1);

        try {
            critere.getClass().getMethod(setter, value.getClass()).invoke(critere, value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new TechnicalException("Critere ne supporte pas le methode " + setter, ex);
        }

        if (strict != null) {
            String setterStrict = "set" +
                    field.substring(0, 1).toUpperCase() +
                    field.substring(1) + "Strict";

            try {
                critere.getClass().getMethod(setterStrict, boolean.class).invoke(critere, strict);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                throw new TechnicalException("Critere ne supporte pas le methode " + setterStrict, ex);
            }
        }

        return safelyFind(critere);
    }

    default <T extends EpmTObject, K extends Critere> T safelyUniqueFind(K critere) {
        return getSafelyInvokator().safelyInvoke(() -> (T) getService().chercherUniqueEpmTObject(0, critere));
    }

    default <T extends EpmTObject, K extends Critere> T safelyUniqueFind(int id) {
        return safelyUniqueFind("id", id);
    }

    default <T extends EpmTObject, K extends Critere> T safelyUniqueFind(final String field, final Object value) {

        K critere = (K) critereContructor().get();


        String setter = "set" +
                field.substring(0, 1).toUpperCase() +
                field.substring(1);

        try {
            critere.getClass().getMethod(setter, value.getClass()).invoke(critere, value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            throw new TechnicalException("Critere ne supporte pas le methode " + setter, ex);
        }

        return safelyUniqueFind(critere);
    }

    default <T extends EpmTObject> T safelySave(T epmTObject) {
        return getSafelyInvokator().safelyInvoke(() -> (T) getService().modifierEpmTObject(epmTObject));
    }

    default <T extends EpmTObject> boolean safelyRemove(T epmTObject) {
        return getSafelyInvokator().safelyInvoke(() -> {
            getService().supprimerEpmTObject(0, epmTObject);
            return true;
        });
    }

}
