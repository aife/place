package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Properties;


/**
 * Cette classe permet d'afficher les proprietes se trouvant dans le fichier de configuration.
 * @author Regis Menet
 * @version: $, $Revision: $, $Author: $
 */
public class AfficheProprieteConfigurationTag extends TagSupport {

    private static final Logger LOG = LoggerFactory.getLogger(AfficheProprieteConfigurationTag.class);
    /**
     * identifiant de serialisation
     */
    private static final long serialVersionUID = 1L;

    /**
     * Nom du fichier de configuration 
     */
    private String fichier;

    /**
     * cle de la propriété à afficher
     */
    private String propriete;

    @Override
    public int doStartTag() throws JspException {
        try {
        	Properties properties = Config.getProperties();
            String valeur = properties.getProperty(propriete);
            if (valeur != null) {
                pageContext.getOut().print(valeur);
            } else {
                throw new JspException("propriete '" + propriete
                        + "' inconnu  dans le fichier de configuration: " + fichier);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        propriete = null;
        fichier = null;
    }

    /**
     * 
     * @param valeur  cle de la propriete present dans le fichier de configuration
     */
    public final void setPropriete(final String valeur) {
        this.propriete = valeur;
    }
    
    /**
     * 
     * @param valeur  nom du fichier de configuration.
     */
    public final void setFichier(final String valeur) {
        this.fichier = valeur;
    }    
}
