package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.presentation.AbstractAction;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Declenchee lors de l'echec de l'authentification depuis la page de login.
 * @author Rémi Villé
 * @version @version $Revision$, $Date$, $Author$
 */
public class LoginFailureAction extends AbstractAction {

    private static final Logger LOG = LoggerFactory.getLogger(LoginFailureAction.class);

    public final ActionForward execute(final SessionManagerGlobal sessionManager, final ActionMapping mapping,
            final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws SessionExpireeException {

        HttpSession session = sessionManager.getSession(request);

        Exception failureException = (Exception) session
                .getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        if (failureException instanceof BadCredentialsException) {
            sessionManager.ajouterElement(session, "erreurKey",
                    "login.erreur");
            LOG.warn("Erreur d'authentification de l'utilisateur : " + failureException.getMessage());
        } else {
            LOG.error("Erreur d'authentification générale : " + failureException.getMessage());
        }

        return mapping.findForward("success");
    }
}
