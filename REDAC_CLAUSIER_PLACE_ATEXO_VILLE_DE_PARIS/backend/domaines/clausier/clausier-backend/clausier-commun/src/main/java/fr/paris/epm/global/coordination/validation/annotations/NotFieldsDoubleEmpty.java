package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.NotFieldsDoubleEmptyConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by nty on 06/04/17.
 */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { NotFieldsDoubleEmptyConstraintValidator.class })
@Repeatable(NotFieldsDoubleEmpty.List.class)
public @interface NotFieldsDoubleEmpty {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Name of the first field that will be compared.
     *
     * @return name
     */
    String firstFieldName();

    /**
     * Name of the second field that will be compared.
     *
     * @return name
     */
    String secondFieldName();

    @Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    public @interface List {
        NotFieldsDoubleEmpty[] value();
    }

}
