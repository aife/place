/**
 * 
 */
package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.Util;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant de couper un mot.
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class CouperTexteTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * nombre maximal de caractères.
     */
    private int max = 60;

    /**
     * ajouts de 3 points à  la fin de texte.
     */
    private boolean etc;
    
    private String infoBulleId;

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CouperTexteTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            String texte = (String) tagUtils.lookup(pageContext, name, property,
                                           null);
            if (texte == null) {
                return Tag.SKIP_BODY;
            }

            texte = HtmlUtils.htmlEscape(texte);

            infoBulleId = "infoBulleId-" + System.nanoTime();

            pageContext.getOut()
                    .print(Util.tronquer((String) texte, max, false));
            if (etc && texte.length() > max ) {
                StringBuffer image = new StringBuffer();
                image.append("<img src=\"" + HrefTag.getHrefRacine() + "images/picto-info-suite.gif\" onmouseover=\"afficheBulle('");
                image.append(infoBulleId);
                image.append("', this)\" onmouseout=\"cacheBulle('");
                image.append(infoBulleId);
                image.append("')\" class=\"picto-info-bulle\" alt=\"Info-bulle\" title=\"Info-bulle\">");
                image.append("<div id=\"");
                image.append(infoBulleId);
                image.append("\" class=\"info-bulle\" onmouseover=\"mouseOverBulle();\" onmouseout=\"mouseOutBulle();\"><div>");
                image.append(Util.insererEspace(texte, 24));
                image.append("</div></div>");
                pageContext.getOut().print(image.toString());
            }

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
        max = 60;
        infoBulleId = null;
        etc = false;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        name = valeur;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

    /**
     * @param valeur nombre de caratère maximal.
     */
    public final void setMax(final int valeur) {
        max = valeur;
    }
    
    /**
     * @param valeur true affiche l'image permanttant d'afficher en surimpression le texte complet.
     */
    public final void setEtc(final boolean valeur) {
        etc = valeur;
    }

}
