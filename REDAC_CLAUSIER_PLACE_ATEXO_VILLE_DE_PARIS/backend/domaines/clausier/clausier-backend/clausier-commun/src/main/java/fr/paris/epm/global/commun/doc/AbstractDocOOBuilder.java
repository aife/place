package fr.paris.epm.global.commun.doc;

import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.bridge.UnoUrlResolver;
import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.connection.NoConnectException;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XModel;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.*;
import com.sun.star.style.BreakType;
import com.sun.star.text.*;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.CloseVetoException;
import com.sun.star.util.XCloseable;
import com.sun.star.util.XRefreshable;
import fr.atexo.commun.commande.CommandeConfiguration;
import fr.atexo.commun.commande.CommandeLanceur;
import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.Util;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.IllegalAccessException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;

/**
 * Classe de factorisation pour les documents openoffice. Instancie un document
 * à partir d'un modèle via un serveur openoffice.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractDocOOBuilder implements DocBuilder {

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDocOOBuilder.class);

    /**
     * Temps d'attente en millisecondes avant d'insérer une entrée dans la table
     * des matières.
     */
    private static final long TEMPS_ATTENTE_TABLE_MATIERES = 600L;

    /**
     * Chemin vers le dossier temporaire du système.
     */
    protected String dossierTemp = "/tmp/";

    /**
     * Extension du fichier à générer; par défaut doc.
     */
    private String extension = EXT_DOC;

    /**
     * Attributs du document OpenOffice.
     */
    protected XComponentContext mxRemoteContext = null;

    /**
     * Attributs du document OpenOffice.
     */
    protected XMultiComponentFactory mxRemoteServiceManager;

    /**
     * Attributs du document OpenOffice.
     */
    protected XPropertySet xIndex;

    /**
     * Attributs du document OpenOffice.
     */
    private XTextContent xIndexContent;

    /**
     * Attributs du document OpenOffice.
     */
    protected XComponent xTemplateComponent;

    /**
     * Attributs du document OpenOffice.
     */
    protected XTextDocument xDoc;

    /**
     * Attributs du document OpenOffice.
     */
    protected XMultiServiceFactory mxDocFactory;

    /**
     * Attributs fichier/inputstream.
     */
    private InputStream fis;

    /**
     * propriété du document openOffice.
     */
    protected XText mxDocText;

    /**
     * propriété du document openOffice.
     */
    protected XTextCursor mxDocCursor;

    /**
     * propriété du document openOffice.
     */
    protected XPropertySet xPropertySet;

    /**
     * Utilisé pour spécifier le titre de la table des matiéres. Vide si titre
     * est dans le template (rédaction). Renseigné dans commission. De plus si
     * il est renseigné la construction du document est différente.
     */
    protected String titreTableDesMatieres = "";
    
    
    /**
     * @param cheminModele chemin vers le fichier modèle
     * @param urlServeurOO url vers le serveur OpenOffice
     * @param commandeRelanceServiceOo le uri de fichier de commande pour relancer le service openoffice.
     * @throws TechnicalException erreur technique
     */
    public AbstractDocOOBuilder(final String cheminModele, final String urlServeurOO, final String commandeRelanceServiceOo) {
        // Crée le composant modèle
        xTemplateComponent = componentFromTemplate(cheminModele, urlServeurOO, commandeRelanceServiceOo);
        xDoc = UnoRuntime.queryInterface(XTextDocument.class, xTemplateComponent);
        mxDocText = xDoc.getText();
        mxDocCursor = mxDocText.createTextCursor();
        xPropertySet = UnoRuntime.queryInterface(XPropertySet.class, mxDocCursor);
        mxDocFactory = UnoRuntime.queryInterface(XMultiServiceFactory.class, xDoc);
    }

    /**
     * @param cheminModele chemin vers le fichier modèle
     * @param urlServeurOO url vers le serveur OpenOffice
     * @param bean bean des variables du document à générer
     * @param commandeRelanceServiceOo le uri de fichier de commande pour relancer le service openoffice.
     * @throws TechnicalException erreur technique
     */
    public AbstractDocOOBuilder(final String cheminModele, final String urlServeurOO,
                                final DocBean bean, final String commandeRelanceServiceOo) throws TechnicalException {
        // Crée le composant modèle
        LOG.info("chemin modele : "+cheminModele);
        xTemplateComponent = componentFromTemplate(cheminModele, urlServeurOO, commandeRelanceServiceOo);
        if (bean != null) {
            try {
                remplirAvecBean(bean);
            } catch (TechnicalException e) {
                String msg = "Erreur lors du remplissage du document avec les variables du bean \n" + bean + "\nlancement nettoyage du document";
                nettoyer();
                LOG.error(msg);
                throw new TechnicalException(msg, e.fillInStackTrace());
            }
        }
        xDoc = UnoRuntime.queryInterface(XTextDocument.class, xTemplateComponent);
        mxDocText = xDoc.getText();
        mxDocCursor = mxDocText.createTextCursor();
        xPropertySet = UnoRuntime.queryInterface(XPropertySet.class, mxDocCursor);
        mxDocFactory = UnoRuntime.queryInterface(XMultiServiceFactory.class, xDoc);
    }

    /**
     * Fabrique le document à partir des données. Méthode à étendre par les fils
     * et appelée par la méthode build(bean).
     * @throws TechnicalException erreur technique
     */
    public abstract void build(String path);

    /**
     * @return fichier généré sous forme de flux
     * @throws TechnicalException erreur technique
     */
    public final InputStream getFlux() {
        toInputStream(dossierTemp);
        return fis;
    }

    /**
     * @return fichier généré sous forme de File
     * @throws TechnicalException erreur technique
     */
    public final File getFichier() {
        return toInputStream(dossierTemp);
    }
    
    /**
     * @return fichier généré sous forme de FileTmp
     * @throws TechnicalException erreur technique
     */
    public final FileTmp getFichierTmp() {
        return toInputStream(dossierTemp);
    }

    /**
     * @param flux enrichi le flux avec le "fichier" généré
     * @throws TechnicalException erreur technique
     */
    public final void ecrireDansFlux(final OutputStream flux) {
        toInputStream(dossierTemp);
        try {
            IOUtils.copy(fis, flux);
        } catch (IOException e) {
           throw new TechnicalException(e.fillInStackTrace());
        }
    }

    /**
     * ne Supprime plus les objets sur disque temporaires et ferme le flux.
     * @throws TechnicalException erreur technique
     */
    public final void nettoyer() {
        //effaceTemporaires();

        IOUtils.closeQuietly(fis);
        try {
            // xTemplateComponent.dispose();
            XModel xModel = UnoRuntime.queryInterface(XModel.class, xTemplateComponent);
            if (xModel != null) {
                XCloseable xCloseable;
                xCloseable = UnoRuntime.queryInterface(XCloseable.class, xModel);
                if (xCloseable != null)
                    xCloseable.close(true);
            } else {
                XComponent xDisposeable;
                xDisposeable = UnoRuntime.queryInterface(XComponent.class, xModel);
                if (xDisposeable != null)
                    xDisposeable.dispose();
            }
        } catch (CloseVetoException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
    }

    public final void fermerConnectionOpenOffice() {

        IOUtils.closeQuietly(fis);
        try {
            // xTemplateComponent.dispose();
            XModel xModel = UnoRuntime.queryInterface(XModel.class, xTemplateComponent);
            if (xModel != null) {
                XCloseable xCloseable;
                xCloseable = UnoRuntime.queryInterface(XCloseable.class, xModel);
                if (xCloseable != null)
                    xCloseable.close(true);
            } else {
                XComponent xDisposeable;
                xDisposeable = UnoRuntime.queryInterface(XComponent.class, xModel);
                if (xDisposeable != null)
                    xDisposeable.dispose();
            }
        } catch (CloseVetoException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
    }

    /**
     * @param chiffre chiffre à décomposer
     * @return chaîne de caractères correspondante
     */
    protected static String decomposeChiffre(final Integer chiffre) {
        if (chiffre == null)
            return "";

        StringBuilder retour = new StringBuilder();
        int entier = chiffre.intValue();
        while (entier > 0) {
            String reste = String.valueOf(entier % 1000);
            while (reste.length() < 3) {
                reste = "0" + reste;
                // condition d'arrêt: augmentation stricte longueur
            }
            retour.insert(0, " " + reste);
            entier = entier / 1000;
            // condition d'arrêt: décroissance stricte entier
        }
        while (retour.charAt(0) == ' ' || retour.charAt(0) == '0') {
            retour.deleteCharAt(0);
            // Condition d'arrêt: décroissance stricte longueur
        }

        return retour.toString();
    }

    // Méthodes récupérées de AbstracionGenerationDocument

    /**
     * @param texte texte
     * @param style style (peut etre null)
     * @param mxDocText pointeur
     * @param mxDocCursor pointeur
     * @param xPropertySet pointeur
     * @throws TechnicalException erreur
     */
    protected void ajoutParagraphe(final String texte, final String style, final XText mxDocText, final XTextCursor mxDocCursor, XPropertySet xPropertySet) {
        try {
            mxDocText.insertControlCharacter(mxDocCursor, ControlCharacter.APPEND_PARAGRAPH, false);
        } catch (Exception e) {
            String msg = "Insertion caractère de contrôle paragraphe\n";
            msg += erreur(texte, style, mxDocText, mxDocCursor, xPropertySet);
            LOG.error(msg + "\n" + e, e.fillInStackTrace());
            throw new TechnicalException(msg, e);
        }

        try {
        	if (style != null) {
        		xPropertySet.setPropertyValue("ParaStyleName", style);
        	}
        } catch (Exception e) {
            String msg = "Définition du style : " + style + "\n";
            msg += erreur(texte, style, mxDocText, mxDocCursor, xPropertySet);
            LOG.error(msg + "\n" + e, e.fillInStackTrace());
            throw new TechnicalException(msg, e);
        }

        mxDocText.insertString(mxDocCursor, texte, false);
    }

    protected void ajoutTexte(final String texte, float fontWeight) {
        try {
            xPropertySet.setPropertyValue("CharWeight", new Float(fontWeight));
        } catch (Exception e) {
            String msg = "Définition du style : " + fontWeight + "\n";
            LOG.error(msg + "\n" + e, e.fillInStackTrace());
            throw new TechnicalException(msg, e);
        }
        mxDocText.insertString(mxDocCursor, texte, false);
    }
    
    
    
    /**
     * Insert page break into a writer document.
     *
     * @param xText        text of writer document
     * @param xTextCursor  cursor position inside text
     * @throws Exception   if the cursor position does not belong to the text
     */
    protected void sautPage(XText xText, XTextCursor xTextCursor)
    {
      // Get cursor property set
      XPropertySet xCursorProps = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xTextCursor);
      

		try {
	        // Insert page break		    
            xCursorProps.setPropertyValue("BreakType", BreakType.PAGE_AFTER);
            // Insert paragraph break
            xText.insertControlCharacter(xTextCursor, ControlCharacter.PARAGRAPH_BREAK, false);            
        } catch (Exception e) {
            LOG.debug("Problème lors du page break | paragraphe break");
            e.printStackTrace();
        }
    }
    
    /**
     * Saut de ligne.
     *
     * @param xTextCursor objet cursor ooffice
     */
    protected void sautLigne(final XTextCursor xTextCursor, boolean isParagraphe) {
    	
    	if(isParagraphe) {
    		try {
    			mxDocText.insertControlCharacter(mxDocCursor,
    					ControlCharacter.PARAGRAPH_BREAK, false);
				xPropertySet.setPropertyValue("ParaStyleName", "Standard");
			} catch (Exception e) {
				LOG.debug("Probleme lors d'un saut de ligne (dans paragraphe).");
				e.printStackTrace();
			}
    	} else {
	    	try {
	    		mxDocText.insertControlCharacter(xTextCursor, ControlCharacter.LINE_BREAK, false);
			} catch (Exception e) {
				LOG.debug("Probleme lors d'un saut de ligne.");
				e.printStackTrace();
			}
    	}
    }
    

    /**
     * @param texte texte
     * @param style style
     * @param mxDocText pointeur
     * @param mxDocCursor pointeur
     * @param xPropertySet pointeur
     * @return message d'erreur
     */
    private String erreur(final String texte, final String style, final XText mxDocText, final XTextCursor mxDocCursor, XPropertySet xPropertySet) {
        boolean txt = mxDocText == null;
        boolean curseur = mxDocCursor == null;
        boolean prop = xPropertySet == null;

        String msg = "ajout du Paragraphe \n" + texte + "\n";
        msg += "dont le style vaut: " + style + "\n";
        if (txt)
            msg += "mxDocText nul\n";
        if (curseur)
            msg += "mxDocCursor nul\n";
        if (prop)
            msg += "xPropertySet nul\n";

        return msg;
    }

    /**
     * @param niveau niveau de titre
     * @param titre titre de l'entrée
     * @param mxDocText pointeur texte
     * @param mxDocCursor curseur de texte
     */
    protected void ajoutEntreeTable(final Short niveau, final String titre, final XText mxDocText, final XTextCursor mxDocCursor) {
        try {
            Thread.sleep(TEMPS_ATTENTE_TABLE_MATIERES);
            XDocumentIndex xDocIndex = UnoRuntime.queryInterface(XDocumentIndex.class, xIndex);
            // on crée une entrée dans la table des matières
            mxDocFactory = UnoRuntime.queryInterface(XMultiServiceFactory.class, xDoc);
            XPropertySet xEntry;
            xEntry = UnoRuntime.queryInterface(XPropertySet.class, mxDocFactory.createInstance("com.sun.star.text.ContentIndexMark"));
            xEntry.setPropertyValue("AlternativeText", titre);
            xEntry.setPropertyValue("Level", niveau);
            XTextContent xEntryContent = UnoRuntime.queryInterface(XTextContent.class, xEntry);
            mxDocText.insertTextContent(mxDocCursor, xEntryContent, false);

            xDocIndex.update();
        } catch (Exception | InterruptedException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException(e);
        }
    }

    /**
     * Insère la table des matières.
     * @throws TechnicalException TechnicalException
     */
    protected void insererTableDesMatieres() {


        try {
            xIndex = UnoRuntime.queryInterface(XPropertySet.class, mxDocFactory.createInstance("com.sun.star.text.ContentIndex"));
            xIndex.setPropertyValue("Level", 10);
            xIndex.setPropertyValue("Title", titreTableDesMatieres);
            xIndexContent = UnoRuntime.queryInterface(XTextContent.class, xIndex);

            if ("".equals(titreTableDesMatieres)) {
                mxDocCursor.gotoEnd(false);
            }
            XParagraphCursor xParaCursor = UnoRuntime.queryInterface(XParagraphCursor.class, mxDocCursor);
            if ("".equals(titreTableDesMatieres))
                xParaCursor.gotoPreviousParagraph(false);
            mxDocText.insertTextContent(mxDocCursor, xIndexContent, false);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
        }
    }

    /**
     * Affecte la valeur des champs de texte du document. Attention, ne
     * fonctionne pas avec un DocBean en classe interne.
     * @param bean bean contenant les valeurs à remplir par introspection
     */
    private void remplirAvecBean(final DocBean bean) {

        // récupération des interfaces XTextFieldsSupplier,
        // XBookmarksSupplier
        XTextFieldsSupplier xTextFieldsSupplier = UnoRuntime.queryInterface(XTextFieldsSupplier.class, this.xTemplateComponent);

        // Accès aux TextFields et aux collections du TextFieldMasters
        XNameAccess xNamedFieldMasters = xTextFieldsSupplier.getTextFieldMasters();
        XEnumerationAccess xEnumeratedFields = xTextFieldsSupplier.getTextFields();

        // Récupération des clés de la Hashtable
        // Iterator it = valeur.keySet().iterator();
        Field[] fields = bean.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++)
            if (Modifier.isStatic(fields[i].getModifiers()))
                fields[i] = null;

        for (Field field : fields) {
            // String key = (String) it.next();
            // Accès au field master
            Object fieldMaster;
            String champ = null;
            try {
                if (field == null)
                    continue;

                champ = field.getName();
                fieldMaster = xNamedFieldMasters.getByName("com.sun.star.text.FieldMaster.User." + champ);
                // key
            } catch (NoSuchElementException e) {
                // le champ n'est pas défini dans le document, passage au
                // suivant
                LOG.debug(champ + " : champ non défini dans le document");
                continue;
            } catch (WrappedTargetException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
                nettoyer();
                throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
            }
            XPropertySet xPropertySet = UnoRuntime.queryInterface(XPropertySet.class, fieldMaster);

            // Insertion de la valeur dans le champ
            try {
                field.setAccessible(true);
                String valeur = (String) field.get(bean);
                xPropertySet.setPropertyValue("Content", valeur);
            } catch (UnknownPropertyException | PropertyVetoException | IllegalArgumentException |
                    IllegalAccessException | WrappedTargetException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
                nettoyer();
                throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
            }
        }

        // Refresh des textfields
        XRefreshable xRefreshable = UnoRuntime.queryInterface(XRefreshable.class, xEnumeratedFields);
        xRefreshable.refresh();
    }

    // Fin méthodes récupérées de AbstracionGenerationDocument

    // Méthodes récupérées de GenerationDocument.java
    /**
     * Crée un nouveau document à partir d'un fichier template.
     * @param source url du template
     * @param urlServeur url serveur OO
     * @param commandeRelanceServiceOo le uri de fichier de commande pour relancer le service openoffice.
     * @return {@link XComponent}
     */
    private XComponent componentFromTemplate(final String source, final String urlServeur, final String commandeRelanceServiceOo) {

        // chargement du template
        File sourceFile = new File(source);
        StringBuilder sTemplateFileUrl = new StringBuilder("file:///");
        try {
            sTemplateFileUrl.append(sourceFile.getCanonicalPath().replace('\\', '/'));
            XComponent xTemplateComponent2 = newDocComponentFromTemplate(sTemplateFileUrl.toString(), urlServeur, commandeRelanceServiceOo);
            return xTemplateComponent2;
        } catch (IOException e) {
            LOG.error(e + "\n lancement nettoyage", e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException("Probleme dans la generation du document, ", e.fillInStackTrace());
        }
    }

    /**
     * Récupère le service distant OpenOffice.
     * @param commandeRelanceServiceOo le uri de fichier de commande pour relancer le service openoffice.
     * @return mxRemoteServiceManager global
     * @throws TechnicalException erreur technique
     */
    private XComponent newDocComponentFromTemplate(final String loadUrl, final String urlServeur, final String commandeRelanceServiceOo)
            throws TechnicalException {
        // Récupère le Remote Context d'OpenOffice
        try {
            LOG.debug("getRemoteServiceManager -->");
            mxRemoteServiceManager = this.getRemoteServiceManager(urlServeur, commandeRelanceServiceOo, true);
            LOG.info("open office remote --> " + mxRemoteServiceManager);
            XPropertySet xProperySet = UnoRuntime.queryInterface(XPropertySet.class, mxRemoteServiceManager);

            // Get the default context from the office server.
            Object oDefaultContext = xProperySet.getPropertyValue("DefaultContext");

            // Query for the interface XComponentContext.
            XComponentContext xOfficeComponentContext = UnoRuntime.queryInterface(XComponentContext.class, oDefaultContext);

            // now create the desktop service
            // NOTE: use the office component context here!
            Object oDesktop = mxRemoteServiceManager.createInstanceWithContext("com.sun.star.frame.Desktop", xOfficeComponentContext);
            XComponentLoader xComponentLoader = UnoRuntime.queryInterface(XComponentLoader.class, oDesktop);

            // Définition des propriétés de chargement
            PropertyValue[] loadProps = new PropertyValue[1];
            loadProps[0] = new PropertyValue();
            loadProps[0].Name = "AsTemplate";
            loadProps[0].Value = true;
            // load
            LOG.info("load document by url --> " + loadUrl);
            return xComponentLoader.loadComponentFromURL(loadUrl, "_blank", 0, loadProps);
        } catch (IllegalArgumentException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException("Le modèle de document n'a pas été trouvé", e.fillInStackTrace());
        }
    }

    /**
     * Récupère le service distant OpenOffice.
     * @param url url service
     * @param relance Tentative de relance du service Open Office si true
     * @return mxRemoteServiceManager global
     */
    private XMultiComponentFactory getRemoteServiceManager(final String url, final String commandeRelanceServiceOo, boolean relance) {
        XMultiComponentFactory xOfficeFactory;

        try {
            XComponentContext xcomponentcontext = Bootstrap.createInitialComponentContext(null);
            // create a connector, so that it can contact the office
            XUnoUrlResolver urlResolver = UnoUrlResolver.create(xcomponentcontext);
            Object initialObject = urlResolver.resolve(url);
            xOfficeFactory = UnoRuntime.queryInterface(XMultiComponentFactory.class, initialObject);

            LOG.info("Connecté au service OpenOffice.");
            return xOfficeFactory;
        } catch (NoConnectException connectException) {
            LOG.error("Impossible de se connecter au service Open Office...");
            if (relance) {
                if (commandeRelanceServiceOo != null && commandeRelanceServiceOo.trim().length() != 0) {
                    LOG.error("Tentative de relance du service Open Office avec la commande : " + commandeRelanceServiceOo);
                    CommandeConfiguration commandeConfiguration = new CommandeConfiguration(commandeRelanceServiceOo);
                    try {
                        CommandeLanceur.lancer(commandeConfiguration, false);
                    } catch (IOException e) {
                        LOG.error("Le service Open office n'a pas pu être relancé...");
                        throw new TechnicalException("Service Open Office impossible à relancer avec relance automatique configurée", e);
                    }
                    LOG.info("Le service Open office a été relancé avec succès...");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        LOG.debug("Erreur lors de l'attente de redémarrage open office", e);
                    }
                    return getRemoteServiceManager(url, null, false);
                } else {
                    LOG.error("La commande pour relancer Service Open Office automatique n'est pas configurée");
                    throw new TechnicalException(connectException);
                }
            } else {
                throw new TechnicalException(connectException);
            }
        } catch (java.lang.Exception e) {
            nettoyer();
            throw new TechnicalException(e);
        }
    }

    /**
     * Charge un document à partir de l'URL.
     * @param storeUrl url du document
     * @param extentionFichier extension de fichier
     * @exception TechnicalException TechnicalException
     */
    private void storeDocComponent(final String storeUrl, final String extentionFichier) {

        XStorable xStorable = UnoRuntime.queryInterface(XStorable.class, this.xDoc);
        PropertyValue[] storeProps = new PropertyValue[1];
        storeProps[0] = new PropertyValue();
        storeProps[0].Name = "FilterName";
        storeProps[0].Value = getFormatDoc(extentionFichier);

        try {
            xStorable.storeToURL(storeUrl, storeProps);
        } catch (com.sun.star.io.IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            nettoyer();
            throw new TechnicalException(e);
        }
    }

    /**
     * Configure le générateur pour un type de document donné.
     * @param type type de document
     * @return code Openoffice du format de document
     */
    public final String getFormatDoc(final String type) {
        String codeFormatDoc = "";
        switch (type) {
            case EXT_ODT:
                codeFormatDoc = "";
                break;
            case EXT_PDF:
                codeFormatDoc = "writer_pdf_Export";
                break;
            case EXT_DOC:
                codeFormatDoc = "MS Word 97";
                break;
        }
        return codeFormatDoc;
    }

    /**
     * Génère un fichier temporaire sur le disque dur, et initialise l'attribut
     * FileInputStream avec ce fichier.
     * @param repertoireTemporaire utilisé pour générer aléatoirement le nom du fichier temporaire
     * @throws TechnicalException TechnicalException
     */
    private FileTmp toInputStream(final String repertoireTemporaire) {

        FileTmp sourceFile = Util.creeFichierTmp(repertoireTemporaire);

        try {
            this.storeDocComponent(sourceFile.toURL().toString(), extension);
        } catch (MalformedURLException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
        }

        if (sourceFile.exists()) {
            LOG.debug("Fichier .doc temporaire trouvé");
            try {
                this.fis = new FileInputStream(sourceFile);
            } catch (FileNotFoundException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
                throw new TechnicalException("Probleme dans la generation du document", e.fillInStackTrace());
            }
        } else {
            LOG.debug("Fichier .doc temporaire non trouvé");
        }
        return sourceFile;
    }

    // Fin méthodes récupérées de GenerationDocument.java

    // Accesseurs
    /**
     * Extensions dans l'interface {@link DocBean}.
     * @param valeur type de fichier à générer (extension)
     */
    public final void setExtension(final String valeur) {
        this.extension = valeur;
    }
    
    /**
     * Insert un tableau dans le document Ooffice.
     * 
     * @param nbLigne nombre de ligne du tableau (penser a ajouter +1 pour le header)
     * @param nbColonne nombre de colonne du tableau
     * @return le tableau insere (utiliser insererLigneDansTableau pour ajouter les lignes)
     */
    public final XTextTable insererTableau(int nbLigne, int nbColonne) {

        XTextTable xTTres = null;

        // Creation d'un objet tableau XTextTable
        try {
            Object oInt = mxDocFactory.createInstance("com.sun.star.text.TextTable");

            xTTres = UnoRuntime.queryInterface(XTextTable.class, oInt);
        } catch (Exception e) {
            LOG.error("Couldn't create instance " + e);
            e.printStackTrace(System.err);
        }

        // Initialisation du tableau
        xTTres.initialize(nbLigne, nbColonne);
        
        // insert the table
        try {
        	//xDoc.getText().insertTextContent(xTC, xTTres, true);
        	mxDocText.insertTextContent(mxDocCursor, xTTres, false);

        } catch (Exception e) {
           LOG.error("Couldn't insert the table " + e);
            e.printStackTrace(System.err);
        }
    	
    	return xTTres;
    }
    
    /**
     * Insere une ligne dans un tableau Ooffice.
     * 
     * @param xTT le tableau Ooffice
     * @param ligneTable le tableau contenant les cellules d'une ligne
     * @param numLigne le numero de la ligne dans le tableau a inserer
     */
    public final void insererLigneDansTableau(XTextTable xTT, String[] ligneTable, int numLigne) {

        final String[] colonneIndiceTable = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        int nbCell = ligneTable.length;
        String erreurMsg;
        
        if (nbCell != xTT.getColumns().getCount()) {
        	erreurMsg = "Nombre de cellule dans la ligne a inserer different du nombre de colonne du tableau";
        	LOG.debug(erreurMsg);
        	throw(new TechnicalException(erreurMsg));
        }
        if(nbCell > colonneIndiceTable.length) {
        	erreurMsg = "Nombre de cellule dans la ligne a inserer trop grand pour le nombre maximum de colonne prevu";
        	LOG.debug(erreurMsg);
        	throw(new TechnicalException(erreurMsg));
        }
        
        for(int i=0; i<ligneTable.length; i++) {
        	insertIntoCell(colonneIndiceTable[i] + numLigne, ligneTable[i], xTT);
        }
    }
    
    /**
     * Permet d'inserer une cellule dans un tableau Ooffice (utilise par insererLigneDansTableau).
     * 
     * @param CellName nom de la cellule
     * @param theText texte a inserer dans la cellule
     * @param xTTbl le tableau Ooffice
     */
    private void insertIntoCell(String CellName, String theText, XTextTable xTTbl) {

        XText xTableText = UnoRuntime.queryInterface(XText.class, xTTbl.getCellByName(CellName));
        
        // create a cursor object
        XTextCursor xTC = xTableText.createTextCursor();
        xTableText.insertString(xTC, theText, false);
    }

    /**
     * Ajoute un titre a mettre aussi en entree de la table des matières.
     *
     * @param titreLibelle le titre
     * @param style le style du titre
     * @param niveau niveau du titre dans la table des matiere
     */
    protected final void ajoutTitreEtEntree(final String titreLibelle, final String style, final int niveau) {

        ajoutParagraphe(titreLibelle, style, mxDocText, mxDocCursor, xPropertySet);
        ajoutEntreeTable((short) niveau, titreLibelle, mxDocText, mxDocCursor);
    }

    /**
     * Ajoute un titre a mettre aussi en entree de la table des matières.
     * @param titreLibelleTableMatiere le titre tel qu'il doit apparaitre dans la table des matieres
     * @param titreLibelle le titre tel qu'il doit apparaitre dans le corps du document
     * @param style le style du titre
     * @param niveau niveau du titre dans la table des matiere
     */
    protected final void ajoutTitreEtEntree(final String titreLibelleTableMatiere, final String titreLibelle, final String style, final int niveau) {

        ajoutParagraphe(titreLibelle, style, mxDocText, mxDocCursor, xPropertySet);
        ajoutEntreeTable((short) niveau, titreLibelleTableMatiere, mxDocText, mxDocCursor);
    }

    /**
     * Utilisé pour spécifier le titre de la table des matiéres. Vide si titre
     * est dans le template (rédaction). Renseigné dans commission. De plus si
     * il est renseigné la construction du document est différente.
     * @param valeur le titre affiché dans le document pour la table des matiéres. par défaut vide.
     */
    public final void setTitreTableDesMatieres(final String valeur) {
        this.titreTableDesMatieres = valeur;
    }

    /**
     * Fusion d'un champ dans le document.
     *
     * @param champNom nom du champ dans le document.
     * @param champValeur valeur a fusionner.
     */
    protected final void fusionerChamp(String champNom, String champValeur) {

        String erreurMsg = null;
        Object fieldMaster = null;

        // récupération des interfaces XTextFieldsSupplier
        XTextFieldsSupplier xTextFieldsSupplier = UnoRuntime.queryInterface(XTextFieldsSupplier.class, xTemplateComponent);

        // Accès aux TextFields et aux collections du TextFieldMasters
        XNameAccess xNamedFieldMasters = xTextFieldsSupplier.getTextFieldMasters();
        XEnumerationAccess xEnumeratedFields = xTextFieldsSupplier.getTextFields();

        try {
            fieldMaster = xNamedFieldMasters.getByName("com.sun.star.text.FieldMaster.User." + champNom);
        } catch (NoSuchElementException e) {
            erreurMsg = champNom + " : champ non défini dans le document";
        } catch (WrappedTargetException e) {
            erreurMsg = "Probleme dans la generation du document au moment d'une fusion.";
        }
        if (erreurMsg != null) {
            LOG.debug(erreurMsg);
            return;
        }

        XPropertySet xPropertySet = UnoRuntime.queryInterface(XPropertySet.class, fieldMaster);

        try {
            xPropertySet.setPropertyValue("Content", champValeur);
        } catch (Exception e) {
            erreurMsg = "Probleme dans la generation du document au moment d'une fusion.";
            LOG.debug(erreurMsg);
        }

        // Refresh des textfields
        XRefreshable xRefreshable = UnoRuntime.queryInterface(XRefreshable.class, xEnumeratedFields);
        xRefreshable.refresh();
    }

}
