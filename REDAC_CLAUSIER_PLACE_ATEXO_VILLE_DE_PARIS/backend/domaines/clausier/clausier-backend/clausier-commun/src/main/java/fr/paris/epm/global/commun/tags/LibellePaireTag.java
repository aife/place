package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class LibellePaireTag extends TagSupport {

    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LibellePaireTag.class);

    /**
     *
     */
    private String name;

    /**
     *
     */
    private String property;

    /**
     *
     */
    private String scope;

    /**
     *
     */
    private int value;
    
    public int doStartTag() throws JspException {
        try {
            Collection paires = (Collection) TagUtils.getInstance().lookup(
                    pageContext, name, property, scope);
            String resultat = "";
            for (Iterator iter = paires.iterator(); iter.hasNext();) {
                final Object paire = (Object) iter.next();
                final Integer idPaire = (Integer) lancerMethode(paire, "getId");
                if (idPaire.intValue() == value) {
                    resultat = (String) lancerMethode(paire, "getLibelle");
                    break;
                }
            }
            pageContext.getOut().print(resultat);  
            
        } catch (IOException e) {
            return SKIP_BODY;
        } catch (TechnicalException e) {
            LOG.error("Erreur à la conversion", e.fillInStackTrace());
            return SKIP_BODY;
        }
        return SKIP_BODY;
    }


    /**
     * Uniquement sur les méthodes sans argument.
     * @param obj objet sur lequel la méthode doit être appelée
     * @param nomMethode nom de la méthode à invoquer
     * @return résultat de l'invocation de la méthode
     * @throws TechnicalException erreur technique
     */
    private static Object lancerMethode(final Object obj, final String nomMethode) {

       Method m;
       try {
           m = obj.getClass().getMethod(nomMethode);
           return m.invoke(obj);
       } catch (Exception e) {
           LOG.error(e.getMessage(), e.fillInStackTrace());
           throw new TechnicalException(e);
       }
    }


    // Accesseurs
    /**
     * @param valeur identifiant à chercher
     */
    public final void setValue(final int valeur) {
        this.value = valeur;
    }


    /**
     * @param valeur nom du bean à utiliser
     */
    public final void setName(final String valeur) {
        this.name = valeur;
    }


    /**
     * @param valeur propriété à utiliser
     */
    public final void setProperty(final String valeur) {
        this.property = valeur;
    }


    /**
     * @param valeur portée dans laquelle chercher
     */
    public final void setScope(final String valeur) {
        this.scope = valeur;
    }

}
