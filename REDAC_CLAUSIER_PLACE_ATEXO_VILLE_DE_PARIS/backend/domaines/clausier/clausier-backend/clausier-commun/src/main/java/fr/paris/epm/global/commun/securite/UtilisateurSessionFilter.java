package fr.paris.epm.global.commun.securite;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.commun.exceptions.FirewallException;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.global.coordination.objetValeur.SimplePair;
import fr.paris.epm.global.coordination.objetValeur.Utilisateur;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import fr.paris.epm.noyau.service.technique.ServiceTechniqueSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

/**
 * Classe utilisé par Spring security. ajoute l'utilisateur en session pour qu'il puisse etre utilié par les actions struts
 *
 * @author RME
 */
public class UtilisateurSessionFilter extends GenericFilterBean {


    private static AdministrationServiceSecurise administrationService;

    /**
     * Proxy vers le noyau.
     */
    private static NoyauProxy noyauProxy;

    private static ServiceTechniqueSecurise serviceTechniqueService;

    private static final Logger LOG = LoggerFactory.getLogger(UtilisateurSessionFilter.class);

    private static final List<String> BLACKLIST = List.of(".*class\\.module\\.classLoader.*");

    private boolean forbidden(String pattern) {
        return BLACKLIST.stream().anyMatch(pattern::matches);
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var parameterNames = (Enumeration<String>) request.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            var name = parameterNames.nextElement();

            if (forbidden(name)) {
                throw new FirewallException(name);
            }

            var values = request.getParameterValues(name);

            for (var value : values) {
                if (forbidden(value)) {
                    throw new FirewallException(value);
                }
            }
        }

        try {
            if (null == SecurityContextHolder.getContext().getAuthentication()) {
                LOG.info("Authentication context is null. Seems bad!");
            }

        } catch (final Exception e) {
            LOG.error("Error handling cookies", e);
        }

        try {
            HttpSession session = getSession((HttpServletRequest) request);

            if (session != null) {
                LOG.trace("Session exist, no problem");
                String header = ((HttpServletRequest) request).getHeader(ConstantesGlobales.AUTHORIZATION);
                String identifiantExterne = request.getParameter(ConstantesGlobales.IDENTIFIANT_SAAS);
                logger.info("header : " + header);
                logger.info("identifiantExterne : " + identifiantExterne);
                if (identifiantExterne != null || (header != null && header.contains("Bearer "))) {
                    LOG.trace("Add information to session");
                    ajouterInformationSession(session, header, identifiantExterne);
                }
                if (session.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION) == null) {
                    LOG.trace("Add information to session for coordination");
                    ajouterInformationSession2(session);
                }
                request.setAttribute(ConstantesGlobales.UTILISATEUR, session.getAttribute(ConstantesGlobales.UTILISATEUR));
                request.setAttribute(ConstantesGlobales.DIRECTION, session.getAttribute(ConstantesGlobales.DIRECTION));
                request.setAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION, session.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION));
            } else {
                LOG.trace("Session is always null. Huh?");
            }
        } catch (NonTrouveNoyauException | TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }

        LOG.trace("Before chain filter, authentication context = {}", SecurityContextHolder.getContext().getAuthentication());

        chain.doFilter(request, response);
    }

    private static HttpSession getSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            LOG.trace("Session now must be created. This may be suspect");
            session = request.getSession(true);
        } else {
            LOG.trace("Session already exist. All is ok!");
        }
        return session;
    }


    private void ajouterInformationSession(HttpSession session, String authorization, String identifiantExterne) {


        EpmTUtilisateur utilisateur = null;
        EpmTConsultationContexte consultation = null;
        List<String> habilitations = null;
        UUID contexte = null;
        UUID agent = null;
        String idContexte = null;
        String accessToken = null;
        String plateformeUuid = null;
        String erreur = null;
        String client = null;


        String documentUrl = null;
        if (identifiantExterne == null && authorization != null) {
            identifiantExterne = authorization.replace("Bearer ", "");
        }
        if (identifiantExterne != null) {
            session.setAttribute(ConstantesGlobales.IDENTIFIANT_SAAS, identifiantExterne);
            session.setAttribute(ConstantesGlobales.ACCES_MODE_SAAS, ConstantesGlobales.ACCES_MODE_SAAS);
            SessionManagerGlobal.getInstance().setSessionValide(session, true);
            UtilisateurExterneConsultation utilExteneConsult = serviceTechniqueService.getUtilisateurExterneConsultation(identifiantExterne);
            if (utilExteneConsult != null) {
                utilisateur = utilExteneConsult.getUtilisateur();
                contexte = utilExteneConsult.getContexte();
                agent = utilExteneConsult.getAgent();
                accessToken = utilExteneConsult.getAccessToken();
                client = utilExteneConsult.getClient();
                documentUrl = utilExteneConsult.getDocumentUrl();
                erreur = utilExteneConsult.getErreur();
                consultation = utilExteneConsult.getConsultation();
                habilitations = utilExteneConsult.getHabilitations();
                idContexte = utilExteneConsult.getIdContexte();

            }
        }

        if (utilisateur == null || !utilisateur.isActif())
            throw new BadCredentialsException("Utilisateur inconnu.");

        EpmTRefDirectionService direction = administrationService.chargerDirectionService(utilisateur.getId(), utilisateur.getDirectionService());

        session.setAttribute(ConstantesGlobales.CONTEXTE, contexte);
        session.setAttribute(ConstantesGlobales.AGENT, agent);
        session.setAttribute(ConstantesGlobales.ERREUR_DOCUMENT, erreur);
        session.setAttribute(ConstantesGlobales.ACCESS_TOKEN, accessToken);
        session.setAttribute(ConstantesGlobales.CLIENT, client);
        session.setAttribute(ConstantesGlobales.DOCUMENT_URL, documentUrl);
        session.setAttribute(ConstantesGlobales.REDIRECTION, true);

        session.setAttribute(ConstantesGlobales.UTILISATEUR, utilisateur);
        session.setAttribute(ConstantesGlobales.DIRECTION, direction);
        session.setAttribute(ConstantesGlobales.HABILITATIONS, habilitations);
        session.setAttribute(ConstantesGlobales.CONSULTATION, consultation);
        session.setAttribute(ConstantesGlobales.ID_CONTEXTE_CLAUSIER, idContexte);
        session.setAttribute(ConstantesGlobales.PLATEFORME, plateformeUuid);

        ajouterInformationSession2(session);
    }

    private void ajouterInformationSession2(HttpSession session) {
        EpmTUtilisateur utilisateur = (EpmTUtilisateur) session.getAttribute(ConstantesGlobales.UTILISATEUR);

        if (utilisateur == null)
            return;

        Utilisateur utilisateurPresentation = new Utilisateur();
        utilisateurPresentation.setId(utilisateur.getId());
        utilisateurPresentation.setNom(utilisateur.getNom());
        utilisateurPresentation.setPrenom(utilisateur.getPrenom());
        utilisateurPresentation.setIdOrganisme(utilisateur.getIdOrganisme());
        utilisateurPresentation.setAdresse(utilisateur.getAdresse());
        utilisateurPresentation.setTelephone(utilisateur.getTelephone());
        utilisateurPresentation.setFax(utilisateur.getFax());
        utilisateurPresentation.setCourriel(utilisateur.getCourriel());

        Integer idDirService = utilisateur.getDirectionService();

        EpmTRefDirectionService ds = administrationService.chargerDirectionService(utilisateur.getId(), utilisateur.getDirectionService());

        String libelleDirServiceComplet = ds.getLibelle();
        SimplePair paireDS = new SimplePair(idDirService, libelleDirServiceComplet);
        utilisateurPresentation.setDirectionService(paireDS);

        session.setAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION, utilisateurPresentation);
    }

    public void setAdministrationService(AdministrationServiceSecurise administrationService) {
        UtilisateurSessionFilter.administrationService = administrationService;
    }

    public void setNoyauProxy(NoyauProxy noyauProxy) {
        UtilisateurSessionFilter.noyauProxy = noyauProxy;
    }

    public final void setServiceTechniqueService(final ServiceTechniqueSecurise valeur) {
        UtilisateurSessionFilter.serviceTechniqueService = valeur;
    }

}
