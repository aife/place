package fr.paris.epm.global.commun.securite;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.AuthentificationTokenCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * classe Spring security. recupere les details de l'utilisateur de la base de donnee. cette classe est utilité par le webService
 * @author RME
 *
 */
public class WebServiceUserDetails implements UserDetailsService {

    /**
     * Loguer.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebServiceUserDetails.class);
    
    /**
     * Acces aux objets persistants (injection Spring).
     */
    private static AdministrationServiceSecurise administrationService;

    /**
     * Delai d'inactivite maximum en minute (injection Spring).
     */
    private Integer delaiInactiviteMax;

    @Override
    public UserDetails loadUserByUsername(String username) {
       
        EpmTAuthentificationToken tokenBdd = null;
        long delaiInactiviteMaxMilliSec;

        if (username == null) {
            BadCredentialsException e = new BadCredentialsException("Echec d'authentification : parametre token manquant.");
            LOG.warn(e.getMessage());
            LOG.debug(e.getMessage(), e);
            throw e;
        }

        // Recherche du token en base
        AuthentificationTokenCritere tokenCritere = new AuthentificationTokenCritere();
        tokenCritere.setSignature(username);

        List<EpmTAuthentificationToken> tokenBddList = null;
        try {
            tokenBddList = administrationService.chercherEpmTObject(0, tokenCritere);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage());
        }
        if (tokenBddList == null || tokenBddList.isEmpty()) {
            BadCredentialsException e = new BadCredentialsException("Echec d'authentification : token manquant en base lié avec la signature = " + username + ".");
            LOG.debug(e.getMessage(), e);
            throw e;
        }
        tokenBdd = tokenBddList.get(0);

        // Verification du delai d'inactivite
        delaiInactiviteMaxMilliSec = delaiInactiviteMax * 60 * 60 * 1000;
        Date dateActuelle = new Date();
        Date dateDerniereActivite = tokenBdd.getDateDerniereActivite();
        if (dateActuelle.getTime() - dateDerniereActivite.getTime() > delaiInactiviteMaxMilliSec) {
            BadCredentialsException e = new BadCredentialsException("Echec d'authentification : inactivité supérieur à " + delaiInactiviteMax + " minutes.");
            LOG.warn(e.getMessage());
            LOG.debug(e.getMessage(), e);
            try {
                administrationService.supprimerAuthentificationToken(0, username);
            } catch (TechnicalNoyauException e1) {
                LOG.error(e.getMessage(), e);
                throw new AuthenticationServiceException(e.getMessage());
            }
        }
        tokenBdd.setDateDerniereActivite(dateActuelle);
        try {
            administrationService.modifierAuthentificationToken(0, tokenBdd);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage());
        }
        return new User(tokenBdd.getIdentifiant(), tokenBdd.getSignature(), true, true, true, true, new ArrayList<>());
    }

    public final void setAdministrationService(AdministrationServiceSecurise valeur) {
    	WebServiceUserDetails.administrationService = valeur;
    }

    public final void setDelaiInactiviteMax(final Integer valeur) {
        this.delaiInactiviteMax = valeur;
    }

}
