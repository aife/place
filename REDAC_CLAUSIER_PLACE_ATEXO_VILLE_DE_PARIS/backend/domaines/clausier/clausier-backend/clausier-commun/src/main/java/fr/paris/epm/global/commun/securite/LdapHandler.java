package fr.paris.epm.global.commun.securite;

/**
 * @author Roger Iss
 * @author Younes Eddoughmi
 * @version $Revision: 372 $, $Date: 2009-02-06 08:41:29 +0100 (ven., 06 févr. 2009) $, $Author: menet $
 */

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.LdapContext;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

public class LdapHandler {

    private Hashtable env;
    private String uid;
    private String cn;
    private String sn;
    private String mail;
    
    public static final String[] JOSSO_ATTRIBUT_LDAP = { "cn", "sn", "mail", "uid" };

    public LdapHandler() {
        this.uid = null;
        this.cn = null;
        this.sn = null;
        this.mail = null;
        init();
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * initialise les variables
     * 
     */
    public void init() {

        env = new Hashtable();

        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.STATE_FACTORIES, "PersonStateFactory");
        env.put(Context.OBJECT_FACTORIES, "PersonObjectFactory");

        // SET YOUR SERVER AND STARTING CONTEXT HERE
        env.put(Context.PROVIDER_URL, LdapConf.LDAP_URL
                + LdapConf.PROVIDER_URL_LOG);

        // SET USER THAT CAN SEARCH AND MODIFY FULL NAME HERE
        env.put(Context.SECURITY_PRINCIPAL, LdapConf.SECURITY_PRINCIPAL);// "cn=Manager,dc=fr,dc=atexo");

        // SET PASSWORD HERE
        env.put(Context.SECURITY_CREDENTIALS, LdapConf.SECURITY_CREDENTIALS);// "secret");

        env.put(LdapContext.CONTROL_FACTORIES,
                "com.sun.jndi.ldap.ControlFactory");
    }

    public List getCommunNameByMail() throws NamingException {
        //init();
        env.put(Context.PROVIDER_URL, LdapConf.LDAP_URL + LdapConf.PEOPLE);// "ou=People,dc=fr,dc=atexo");
        DirContext ctx = new InitialDirContext(env);
        // Specify the search filter to match all users with no full name
        String filter = "(&(objectClass=person)";
        if (uid != null) {
            filter += "(uid=" + uid + ")";
        }

        if (cn != null) {
            filter += "(cn=" + cn + ")";
        }
        if (sn != null) {
            filter += "(sn=" + sn + ")";
        }
        if (mail != null) {
            filter += "(mail=" + mail + ")";
        }
        filter += ")";

        // limit returned attributes to those we care about
        // attrIDs = JOSSO_ATTRIBUT_LDAP;
        SearchControls ctls = new SearchControls();
        ctls.setReturningAttributes(JOSSO_ATTRIBUT_LDAP);
        // comment out next line to limit to one container otherwise it'll walk
        // down the tree
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        // Search for objects using filter and controls
        NamingEnumeration answer = ctx.search("", filter, ctls);
        // String communName = null;

        List liste = new ArrayList();
        while (answer.hasMore()) {
            SearchResult sr = (SearchResult) answer.next();
            liste.add(sr.getAttributes());
        }
        // Close the context when we're done
        ctx.close();
        return liste;
    }

    
    /**
     * @param sn
     * @return <b>uid</b> du LDAP correspondant à la valeur <b>sn</b>.
     */
    public String getGuidBySn(String sn) throws NamingException {
        
        String guidToReturn = "";
        StringTokenizer attribut;
        List resultat = getCommunNameByMail();
        java.util.Iterator i = resultat.iterator();
        
        while (i.hasNext()) {
            Attributes users = (Attributes) i.next();
            String guid = "";
            String name = "";
 

            try {      
                // récuperation du guid
                attribut = new StringTokenizer(users.get("uid").toString(), ":"); // str : "uid: guid"
                attribut.nextElement(); // str = "uid"
                guid = ((String)attribut.nextElement()).trim(); // str = "guid"
    
            } catch (NullPointerException ne) {    
            }
             
            try{
                // récuperation du nom
                attribut = new StringTokenizer(users.get("sn").toString(),":");
                attribut.nextElement();
                name = (new String((String)attribut.nextElement())).trim();
                
            }catch(NullPointerException ne){
            }

            if(sn.equals(name)){
                guidToReturn = guid;
            }
        
    }        
        return guidToReturn;
    }

}
