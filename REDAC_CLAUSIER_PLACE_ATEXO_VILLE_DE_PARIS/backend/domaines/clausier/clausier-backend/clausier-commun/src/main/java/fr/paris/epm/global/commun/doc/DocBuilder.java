/**
 * $Id$
 */
package fr.paris.epm.global.commun.doc;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.FileTmp;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface définissant un "buildeur".
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public interface DocBuilder {

    /**
     * document de type open office.
     */
    String EXT_ODT = "odt";

    /**
     * document de type PDF.
     */
    String EXT_PDF = "pdf";

    /**
     * document de type Word.
     */
    String EXT_DOC = "doc";

    /**
     * document de type Excel.
     */
    String EXT_XLS = "xls";
    
    /**
     * Séparateur.
     */
    String SEPARATEUR = "/";


    /**
     * Fabrique le document à partir des données.
     * @throws TechnicalException erreur technique
     */
    void build(String path);

    /**
     * @return fichier généré sous forme de flux
     * @throws TechnicalException erreur technique
     */
    InputStream getFlux();

    
    /**
     * @return fichier généré sous forme de File
     * @throws TechnicalException erreur technique
     */
    FileTmp getFichierTmp();
    
    /**
     * @param flux écrit le fichier généré dans le flux
     * @throws TechnicalException erreur technique
     */
    void ecrireDansFlux(final OutputStream flux);

    /**
     * Supprime les objets sur disque temporaires.
     * @throws TechnicalException erreur technique
     */
    void nettoyer();
}
