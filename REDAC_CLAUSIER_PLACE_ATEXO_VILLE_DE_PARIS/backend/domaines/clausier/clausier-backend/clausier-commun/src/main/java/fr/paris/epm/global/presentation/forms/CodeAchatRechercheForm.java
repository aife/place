package fr.paris.epm.global.presentation.forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Cette classe est utilisé dans le cadre de la recherche des code NAV.
 * 
 * @author Regis Menet
 */
public class CodeAchatRechercheForm extends ActionForm {
    /**
     * ID de serialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * mots cles permettant la recherche des codes nav
     */
    private String motsCles;

    /**
     * Reinitialisation du formulaire.
     */
    public final void reset(ActionMapping mapping, HttpServletRequest request) {
        motsCles = "";
    }

    /**
     * @return mots cles permettant la recherche des codes nav
     */
    public final String getMotsCles() {
        return motsCles;
    }

    /**
     * @param valeur mots cles permettant la recherche des codes nav
     */
    public final void setMotsCles(final String valeur) {
        this.motsCles = valeur;
    }
}
