package fr.paris.epm.global.commun.tags;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;

/**
 * Pour utiliser les ressources localisees par le fichier de configuration.
 * On peut utiliser l'attribut var pour mettre le resultat dans celui ci.
 * La portee de cette variable est limitee au corps si ce dernier est present.
 * @author Rémi Villé
 * @version 2010/06/02
 */
public class HrefTag extends BodyTagSupport {
    /**
     * Identifiant de serialisation.
     */
    private static final long serialVersionUID = -1007678650468379795L;

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(HrefTag.class);

    /**
     * Lien relatif.
     */
    private String href;

    /**
     * Racine du lien provenant du fichier de configuration.
     */
    private static String hrefRacine;

    /**
     * Lien complet resultat.
     */
    private String hrefResult;

    /**
     * Indique si la balise contient un corps (<balise>..</balise> et pas <balise/>)
     */
    private boolean bodyPresent;

    /**
     * Debut du tag.
     */
    public int doStartTag() throws JspException {

        String res;

        try {
            res = hrefRacine + href;
            if (hrefResult == null)
                pageContext.getOut().print(res);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }

        if (hrefResult != null)
            pageContext.setAttribute(hrefResult, res);

        return Tag.EVAL_BODY_INCLUDE;
    }

    /**
     * Appelee des qu'un corps est present (<balise>+</balise>)
     */
    public int doAfterBody() throws JspException {
        bodyPresent = true;
        return super.doAfterBody();
    }

    /**
     * Fin du tag, si un corps est present on limite la portee de la variable a celui ci.
     */
    public int doEndTag() throws JspException {
        if (hrefResult != null && bodyPresent) {
            pageContext.removeAttribute(hrefResult);
            hrefResult = null;
        }
        return Tag.EVAL_PAGE;
    }

    /**
     * Nettoyage.
     */
    public void release() {
        href = null;
        hrefResult = null;
    }

    /**
     * @param valeur Lien relatif.
     */
    public final void setHref(final String valeur) {
        this.href = valeur;
    }

    /**
     * @param valeur Debut du lien provenant du fichier de configuration (injection Spring).
     */
    public final void setHrefRacine(final String valeur) {
        hrefRacine = valeur;
    }

    /**
     * @param valeur Lien complet resultat.
     */
    public final void setVar(String valeur) {
        hrefResult = valeur;
    }

    /**
     * @return valeur Lien complet resultat.
     */
    public static String getHrefRacine() {
        return hrefRacine;
    }

}
