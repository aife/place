package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.FieldsEquality;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

/**
 * Created by nty on 05/04/17.
 */
public class FieldsEqualityConstraintValidator
        extends AbstractDoubleFieldsConstraintValidator<FieldsEquality, Object> {

    @Override
    public void initialize(FieldsEquality constraintAnnotation) {
        setFirstFieldName(constraintAnnotation.firstFieldName());
        setSecondFieldName(constraintAnnotation.secondFieldName());
    }

    @Override
    public boolean isValid(Object instance, ConstraintValidatorContext cxt) {
        if (instance == null)
            return true;

        try {
            Object first = getValueOfFirstField(instance);
            Object second = getValueOfSecondField(instance);

            if (first != null && second != null && !first.equals(second)) {
                ConstraintViolationBuilder cvb = cxt.buildConstraintViolationWithTemplate(cxt.getDefaultConstraintMessageTemplate());
                //cvb.addNode(getFirstFieldName()).addConstraintViolation();
                cvb.addNode(getSecondFieldName()).addConstraintViolation();
                return false;
            }
        } catch (IllegalAccessException ex) {
            //log.error("Cannot validate fileds equality in '" + value + "'!", ex);
            return false;
        }

        return true;
    }

}
