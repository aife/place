package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.noyau.metier.NoteCritere;
import fr.paris.epm.noyau.persistance.EpmTNote;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class NoteTag extends BodyTagSupport {
    /**
     * 
     */
    private static final long serialVersionUID = -684727827878553368L;

    private static final String CONSULTATION_FACADE = "consultationFacade";
    
    private static final String METHODE_FACADE = "chargerNote";
    /**
     * propriété accessible de l'objet.
     */
    private String idConsultation;

    /**
     * nom de l'objet.
     */
    private String idBudLot;
    
    private String idAvenant;

    private WebApplicationContext ctx = null;
    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NoteTag.class);

    public Object getService(String name) {
        if (ctx == null) {
            ctx = WebApplicationContextUtils
                    .getRequiredWebApplicationContext(pageContext
                            .getServletContext());
        }
        if (name != null) {
            return ctx.getBean(name);
        }
        return null;

    }
    
    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext
            .getRequest();
            
            EpmTUtilisateur utilisateur = (EpmTUtilisateur) request
            .getAttribute(ConstantesGlobales.UTILISATEUR);
            
            
            
            NoteCritere noteCritere = new NoteCritere();

            int idLotDissocie = Integer.parseInt(idBudLot);
            noteCritere.setIdBudLot(idLotDissocie);

            int consultation = Integer.parseInt(idConsultation);
            noteCritere.setIdConsultation(consultation);

            int avenant = Integer.parseInt(idAvenant);
            noteCritere.setIdAvenant(avenant);

            // la facade en session
            Object facade = getService(CONSULTATION_FACADE);
            String resultatHTML = "";
            List list = null;
            if (facade != null) {
                Object resultat = null;
                try {
                    Class tableauClasse[] = new Class[2];
                    tableauClasse[0] = int.class;
                    tableauClasse[1] = NoteCritere.class;
                    Method method = facade.getClass()
                            .getMethod(METHODE_FACADE, tableauClasse);
                    Object[] parametres = new Object[2];
                    parametres[0] = utilisateur.getId();
                    parametres[1] = noteCritere;
                    resultat = method.invoke(facade, parametres);
                    list = (List) resultat;
                    String texteNote = traiterResultat(list, request);

                    resultatHTML = creationScriptHTML(texteNote);
                } catch (InvocationTargetException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    pageContext.setAttribute(id, new ArrayList());
                    return EVAL_BODY_INCLUDE;
                } catch (IllegalAccessException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    pageContext.setAttribute(id, new ArrayList());
                    return EVAL_BODY_INCLUDE;
                } catch (NoSuchMethodException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    pageContext.setAttribute(id, new ArrayList());
                    return EVAL_BODY_INCLUDE;
                }
            }
            
            
            pageContext.getOut().print(resultatHTML);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    private final String traiterResultat(List listeResultat, HttpServletRequest request) {
        String texte = "";
        String idNote = "0";
        if (listeResultat.size() == 0) {
            LOG
                    .debug("Note Tag Pas de de note pour la consultation / lot / avenant => idConsultation : "
                            + idConsultation
                            + "idLotDissocie : "
                            + idBudLot + "idAvenant : " + idAvenant);
        } else if (listeResultat.size() == 1) {
            EpmTNote note = (EpmTNote)listeResultat.get(0);
            texte = note.getNote();
            
            idNote = String.valueOf(note.getId());
        } else  {
            LOG.error("Bloc notes : plusieures notes pour la consultation / lot / avenant => idConsultation : "
                            + idConsultation
                            + "idLotDissocie : "
                            + idBudLot + "idAvenant : " + idAvenant);
        }
        request.setAttribute("idNote", idNote);
        request.setAttribute("contenuBlocNotes", texte);
        return texte;
    }
    private final String creationScriptHTML(String contenu) {
        StringBuffer script = new StringBuffer();
        script.append("<a class=\"postit\" href=\"javascript:initialiserBlocNotes();\"><img title=\"Info-bulle\" alt=\"Info-bulle\" class=\"picto-info-bulle\" onmouseout=\"cacheBulle('infos-postit')\" onmouseover=\"afficheBulle('infos-postit', this)\" src=\""+ HrefTag.getHrefRacine() +"images/picto-postit.gif\"/></a>");
        
        return script.toString();
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        idConsultation = null;
        idBudLot = null;
        idAvenant = null;
    }

    public void setIdConsultation(String valeur) {
        this.idConsultation = valeur;
    }

    public void setIdBudLot(String valeur) {
        this.idBudLot = valeur;
    }

    public void setIdAvenant(String valeur) {
        this.idAvenant = valeur;
    }
   
}
