package fr.paris.epm.global.commons.exception;

/**
 * Exception levée lorsque l'accès à la ressource n'est pas autorisée.
 * Par exemple, l'accès à une consultation hors du périmètre d'intervention/
 * vision de l'utilisateur.
 * @author Guillaume Béraudo
 * @version $Revision: 103 $,
 *          $Date: 2007-07-23 19:16:22 +0200 (lun., 23 juil. 2007) $,
 *          $Author: beraudo $
 */
public class NonAutoriseException extends ApplicationException {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public NonAutoriseException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause Exception ayant provoqué cette exception
     */
    public NonAutoriseException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public NonAutoriseException(final String msg) {
        super(msg);
    }

    /**
     * @param cause Exception ayant provoqué cette exception
     */
    public NonAutoriseException(final Throwable cause) {
        super(cause);
    }
}
