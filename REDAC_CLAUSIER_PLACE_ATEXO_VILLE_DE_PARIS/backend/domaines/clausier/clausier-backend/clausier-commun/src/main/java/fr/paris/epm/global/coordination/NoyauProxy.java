package fr.paris.epm.global.coordination;

import fr.paris.epm.global.commun.TypeEpmTRefObject;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.commun.EpmTRef;
import fr.paris.epm.noyau.persistance.commun.EpmTRefImportExport;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;

import java.util.Collection;
import java.util.Optional;

/**
 * Classe proxy entre passation et le noyau pour les accès aux référentiels,
 * sélects, info-bulles. Il est possible de limiter les requêtes vers le noyau
 * au moyen d'un cache/timer.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public interface NoyauProxy {

    DirectionService getDirService(Integer id);

    Referentiels getReferentiels();

	void initialiserReferentiels();


    EpmTRef getReferentielById( final int id, TypeEpmTRefObject typeReferentiel);

	<T extends EpmTRefImportExport> Optional<T> getReferentielExportableById( final int id, TypeEpmTRefObject typeReferentiel);

	<T extends EpmTRefImportExport> Optional<T> getReferentielExportableByUid( String uid, TypeEpmTRefObject nature );


    Collection<EpmTRefProcedure> getProcedureByIdOrganisme(final Integer idOrganisme, final boolean avecTous, final boolean actif);

    EpmTRef getReferentielByReference( final String reference, TypeEpmTRefObject typeReferentiel);

}
