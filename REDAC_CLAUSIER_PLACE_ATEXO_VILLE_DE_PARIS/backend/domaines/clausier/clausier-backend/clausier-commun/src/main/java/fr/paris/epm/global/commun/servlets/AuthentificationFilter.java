package fr.paris.epm.global.commun.servlets;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.securite.AuthentificationNoyau;
import fr.paris.epm.global.commun.securite.InfoClientAuth;
import fr.paris.epm.global.coordination.NoyauProxy;
import fr.paris.epm.global.coordination.objetValeur.SimplePair;
import fr.paris.epm.global.coordination.objetValeur.Utilisateur;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.persistance.EpmTHabilitation;
import fr.paris.epm.noyau.persistance.EpmTProfil;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Classe EncodingFilter permet d'encoder le contenu d'une rêquete .
 * 
 * @author Regis Menet
 * @version $Revision$, $Date$, $Author$
 */

public class AuthentificationFilter implements javax.servlet.Filter {
    /**
     * Le nom du cookie sur le poste client.
     */
    private String cookie = "MDP-WSSO-MDPWSSOGUID";
    private static final String COOKIE_ALFRESCO_NAME = "WSSO-LOGIN-BD";
    private static final String LOGIN_LECTEUR = "guest";
    private static final String LOGIN_ADMIN = "admin";
    private static AdministrationServiceSecurise administrationService;

    /**
     * Proxy vers le noyau.
     */
    private static NoyauProxy noyauProxy;
    /**
     * info du module client JAAS.
     */
    private static InfoClientAuth infoClientAuth;
    private static String guid = null;
    
    /**
     * Mapper.
     */
    private static DozerBeanMapper conversionService;
    
    /**
     * Le LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthentificationFilter.class);

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(final ServletRequest request,
            final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        boolean isAdministrateur = false;
        try {
            Cookie cookies[] = ((HttpServletRequest) request).getCookies();
            Cookie myCookie = null;
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    if ((cookies[i].getName()).equals(cookie)) {
                        myCookie = cookies[i];
                        break;
                    }
                }
            }
            String username = null;            
            // Authentification spring-security
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if ((authentication == null) && ((myCookie != null) || (guid != null && !guid.equals("")))) {
                if (myCookie != null) {
                    username = myCookie.getValue();
                }
                if (guid != null && !guid.equals("")) {
                    username = guid;
                }
            } // Authentification non SSO, recherche de l'authentification par spring-security (BDD ou LDAP)
            else if(authentication != null){
                if(authentication.getPrincipal() instanceof UserDetails){
                    UserDetails user = (UserDetails)authentication.getPrincipal();
                    username = user.getUsername();
                } else{
                    username = (String)authentication.getPrincipal();
                }
            }	
            
            if(username != null) {
            	AuthentificationNoyau auth = null;
            	if (infoClientAuth != null) {
	                auth = new AuthentificationNoyau(infoClientAuth);
	                auth.authentication();
            	}
                EpmTUtilisateur utilisateur = administrationService.chargerUtilisateur(username);
                if (utilisateur == null || !utilisateur.isActif()) {
                	throw new BadCredentialsException("Utilisateur inconnu.");
                }
                EpmTRefDirectionService direction = administrationService.chargerDirectionService(utilisateur.getId(), utilisateur.getDirectionService());
                if (auth != null) {
                	auth.logout();
                }

                request.setAttribute("direction", direction);
                request.setAttribute("utilisateur", utilisateur);
                if (utilisateur.getProfil() != null) {
                    for (EpmTProfil profil : utilisateur.getProfil()) {
                        if (profil.getHabilitationAssocies() != null) {
                            for (EpmTHabilitation habilitation : profil.getHabilitationAssocies()) {
                                String[] roleTab = null;
                                if (habilitation.getRole() != null) {
                                    roleTab = habilitation.getRole().split(",");
                                    for (int i = 0; i < roleTab.length; i++) {
                                        if (roleTab[i].equalsIgnoreCase("ROLE_baseConnaissance")) {
                                            isAdministrateur = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Utilisateur utilisateurPresentation = conversionService.map(utilisateur, Utilisateur.class);
                Integer idDirService = utilisateur.getDirectionService();
                DirectionService ds;
                try {
                    ds = noyauProxy.getDirService(idDirService);
                } catch (TechnicalException e) {
                    LOG.error(e.getMessage(), e.fillInStackTrace());
                    throw new ServletException(e);
                }
                if (ds == null) {
                    throw new ServletException("Direction/service introuvable: " + idDirService);
                }
                String nomDirServiceComplet = ds.libelleComplet();
                SimplePair paireDS = new SimplePair(idDirService, nomDirServiceComplet);
                utilisateurPresentation.setDirectionService(paireDS);
                request.setAttribute("utilisateur2", utilisateurPresentation);
            }

            Cookie cookieAlfresco = null;
            String cookieAlfrescoName = COOKIE_ALFRESCO_NAME;
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    if ((cookies[i].getName()).equals(cookieAlfrescoName)) {
                        cookieAlfresco = cookies[i];
                    }
                }
            }

            /* Si le cookie pour alfresco n'existe pas, on l'ajoute */

                if (isAdministrateur) {
                    cookieAlfresco = new Cookie(cookieAlfrescoName, LOGIN_ADMIN);
                } else {
                    cookieAlfresco = new Cookie(cookieAlfrescoName, LOGIN_LECTEUR);
                }
                cookieAlfresco.setPath("/");
                cookieAlfresco.setMaxAge(1200);
                cookieAlfresco.setValue(isAdministrateur ? LOGIN_ADMIN : LOGIN_LECTEUR);
                ((HttpServletResponse) response).addCookie(cookieAlfresco);
        } catch (TechnicalNoyauException | RemoteConnectFailureException | NonTrouveNoyauException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
        }
        filterChain.doFilter(request, response);
    }

    /**
     * destructeur.
     */
    public void destroy() {
    }

    /**
     * @param valeur the administrationService to set
     */
    public void setAdministrationService(AdministrationServiceSecurise valeur) {
        AuthentificationFilter.administrationService = valeur;
    }

    /**
     * @param valeur the administrationService to set
     */
    public void setGuid(String valeur) {
        guid = valeur;
    }

    /**
     * @param valeur the administrationService to set
     */
    public void setCookie(String valeur) {
        this.cookie = valeur;
    }

    /*
     * (non-Javadoc) @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * @param valeur proxy vers le noyau
     */
    public final void setNoyauProxy(final NoyauProxy valeur) {
        AuthentificationFilter.noyauProxy = valeur;
    }

    /**
     * @param valeur info du Client JAAS.
     */
    public final void setInfoClientAuth(final InfoClientAuth valeur) {
        AuthentificationFilter.infoClientAuth = valeur;
    }

    /**
     * Mappeur de proprietes (injection Spring).
     */
	public final void setConversionService(final DozerBeanMapper valeur) {
		AuthentificationFilter.conversionService = valeur;
	}

}
