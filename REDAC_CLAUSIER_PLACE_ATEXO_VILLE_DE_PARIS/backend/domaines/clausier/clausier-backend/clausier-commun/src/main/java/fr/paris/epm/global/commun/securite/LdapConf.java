package fr.paris.epm.global.commun.securite;

/**
 * @author Roger Iss
 * @author Younes Eddoughmi
 * @version $Revision: 372 $, $Date: 2009-02-06 08:41:29 +0100 (ven., 06 févr. 2009) $, $Author: menet $
 */
public interface LdapConf {

    public String LDAP_URL = "ldap://192.168.0.3:389/"; //configApp.getString("LDAP_URL");
    
    public String SECURITY_PRINCIPAL = "cn=Manager,dc=fr,dc=atexo"; //configApp.getString("SECURITY_PRINCIPAL");

    public String SECURITY_CREDENTIALS = "secret"; //configApp.getString("SECURITY_CREDENTIALS");

    public String PEOPLE = "ou=People,dc=fr,dc=atexo"; //configApp.getString("LDAP_URL");
    
    public String PROVIDER_URL_LOG = "dc=fr,dc=atexo"; //configApp.getString("PROVIDER_URL_LOG");

}
