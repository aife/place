/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class CompteurPageTag extends TagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request =
                    (HttpServletRequest) pageContext.getRequest();
            StringBuffer sb = new StringBuffer();

            int intervalle = 10;
            
            
            
            if (pageContext
                    .getAttribute(TableauTag.INTERVALLE_AFFICHER) != null) {
                intervalle =  Integer.parseInt(((String[]) pageContext
                            .getAttribute(TableauTag.INTERVALLE_AFFICHER))[0]);
            }
            
            if (pageContext.getAttribute("intervalle") != null) {
                intervalle = (Integer)pageContext.getAttribute("intervalle");
            }
            
            String paramIntervalle = request.getParameter("intervalle");
            if (paramIntervalle != null && paramIntervalle.length() != 0) {
                intervalle = Integer.parseInt(paramIntervalle);
            } else {
                paramIntervalle = (String) request.getAttribute("intervalle");
                if (paramIntervalle != null) {
                    intervalle = Integer.parseInt(paramIntervalle);
                }
            }
            String taillePagination =
                    (String) pageContext
                            .getAttribute(TableauPagineTag.NB_RESULT);
            List list = (List) pageContext.getAttribute(TableauTag.LIST);

            int max = 1;
            if (taillePagination != null) {
                max =
                        (int) Math.ceil(Double.parseDouble(taillePagination)
                                / (double) intervalle);
            } else if (list != null) {
                max =
                        (int) Math.ceil(Double.parseDouble(String.valueOf(list
                                .size()))
                                / (double) intervalle);
            }
            int index = 0;
            String paramIndex = request.getParameter("index");
            if (paramIndex != null && paramIndex.length() != 0) {
                index = Integer.parseInt(paramIndex);
            } else {
                paramIndex = (String) request.getAttribute("index");
                if (paramIndex != null) {
                    index = Integer.parseInt(paramIndex);
                }
            }
            sb.append(index + 1);
            sb.append("/");
            if (max == 0) {
                max = 1;
            }
            sb.append(max);
            pageContext.getOut().print(sb.toString());
        } catch (IOException e) {
            throw new JspException("I/O Error", e);
        }
        return SKIP_BODY;
    }

    public void release() {
    }

}
