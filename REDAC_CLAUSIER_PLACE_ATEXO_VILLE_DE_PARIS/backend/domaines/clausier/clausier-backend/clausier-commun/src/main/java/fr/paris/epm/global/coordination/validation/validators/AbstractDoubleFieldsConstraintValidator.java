package fr.paris.epm.global.coordination.validation.validators;

import java.lang.annotation.Annotation;

/**
 * Created by nty on 06/04/17.
 */
public abstract class AbstractDoubleFieldsConstraintValidator<A extends Annotation, T>
        extends AbstractConstraintValidator<A, T> {

    private String firstFieldName;
    private String secondFieldName;

    public String getFirstFieldName() {
        return firstFieldName;
    }

    public void setFirstFieldName(String firstFieldName) {
        this.firstFieldName = firstFieldName;
    }

    public String getSecondFieldName() {
        return secondFieldName;
    }

    public void setSecondFieldName(String secondFieldName) {
        this.secondFieldName = secondFieldName;
    }

    public Object getValueOfFirstField(Object instance) throws IllegalAccessException {
        return getFieldValue(instance, firstFieldName);
    }

    public Object getValueOfSecondField(Object instance) throws IllegalAccessException {
        return getFieldValue(instance, secondFieldName);
    }

}
