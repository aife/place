package fr.paris.epm.global.commun.tags;

import fr.paris.epm.noyau.feature.FeatureUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Tag permettant d'ajouter une liste des features activées dans le DOM.
 * On peut ensuite tester si une feature est activé avec le code Javascript : if(features.myFeature){...}
 * Created by sta on 18/12/15.
 */
public class FeaturesJsonTag extends TagSupport {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(FeaturesJsonTag.class);

    @Override
    public int doStartTag() throws JspException {
        try {
            String features = FeatureUtil.getJsonActivatedFeatures();
            pageContext.getOut().print(String.format("<script type=\"text/javascript\">var features = %s</script>", features));
        } catch (Exception e) {
            LOG.error("Erreur dans l'insertion de la liste des features activées au niveau du DOM : ", e);
        }
        return SKIP_BODY;
    }
}
