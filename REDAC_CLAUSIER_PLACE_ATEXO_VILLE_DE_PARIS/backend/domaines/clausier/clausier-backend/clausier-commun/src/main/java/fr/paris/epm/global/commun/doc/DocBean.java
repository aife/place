/**
 * $Id$
 */
package fr.paris.epm.global.commun.doc;

import java.io.Serializable;

/**
 * Interface à implémenter par tous les "beans" de documents.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public interface DocBean extends Serializable {

}
