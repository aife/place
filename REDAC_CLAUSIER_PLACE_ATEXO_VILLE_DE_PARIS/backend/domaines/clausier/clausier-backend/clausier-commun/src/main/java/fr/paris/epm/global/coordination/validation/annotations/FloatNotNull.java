package fr.paris.epm.global.coordination.validation.annotations;

import fr.paris.epm.global.coordination.validation.validators.FloatNotNullConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by nty on 10/04/17.
 */
@Documented
@Constraint(validatedBy = FloatNotNullConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface FloatNotNull {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
