package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commons.exception.TechnicalException;
import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.TypeRecherche;
import fr.paris.epm.global.coordination.NoyauProxyImpl;
import fr.paris.epm.global.coordination.objetValeur.Utilisateur;
import fr.paris.epm.noyau.metier.AbstractCritere;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class TableauPagineTag extends BodyTagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Marqueur du fichier journal.
     */
    private static final Logger LOG = LoggerFactory.getLogger(TableauPagineTag.class);

    public static final String INTERVALLE_AFFICHER = "INTERVALLE_AFFICHER";
    public static final String LIST = "LIST";
    public static final String URL = "URL";
    public static final String FORM = "FORM";
    public static final String NB_RESULT = "NB_RESULT";
    private TagUtils tagUtils = TagUtils.getInstance();

    private String elementAfficher;

    private List list;

    private String name;

    private String property;

    private String scope;

    private String url;

    private String script;

    private static NoyauProxyImpl noyauProxy;

    private boolean toutAfficher;

    /**
     * Nom du formulaire utilisé pour envoyer les données au serveur.
     */
    private String formulaire;

    private WebApplicationContext ctx = null;

    private Integer nombreResultatAAfficherParDefaut = null;

    public Object getService(String name) {
        if (ctx == null) {
            ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(pageContext.getServletContext());
        }
        if (name != null) {
            return ctx.getBean(name);
        } else {
            return null;

        }
    }

    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        HttpSession session = pageContext.getSession();
        // la facade en session
        Object facade = getService((String) session.getAttribute(ConstantesGlobales.S_FACADE));

        // la méthode à lancer pour lancer la recherche paginée.
        String nomMethode = (String) session.getAttribute(ConstantesGlobales.S_METHODE);
        // les criteres qui permettent la recherche.
        Object critere = session.getAttribute(ConstantesGlobales.S_CRITERES);
        Integer typeRecherche = (Integer) session.getAttribute(ConstantesGlobales.S_TYPE_RECHERCHE);
        String nomResultat = (String) session.getAttribute(ConstantesGlobales.S_NOM_RESULTAT);
        int intervalle = 10;

        if (elementAfficher != null) {
            String[] array = elementAfficher.split(",");
            pageContext.setAttribute(INTERVALLE_AFFICHER, array);
            if (nombreResultatAAfficherParDefaut != null) {
                intervalle = nombreResultatAAfficherParDefaut;
                pageContext.setAttribute("intervalle", intervalle);
            } else {
                intervalle = Integer.parseInt(array[0]);
            }
        }
        try {
            if (formulaire != null) {
                pageContext.getOut().print(getJavascriptSubmit());
            } else {
                pageContext.getOut().print(getJavascriptUrl());
            }
        } catch (IOException e) {
            throw new JspException(e);
        }

        String paramIntervalle = request.getParameter("intervalle");
        if (paramIntervalle != null && paramIntervalle.length() != 0) {
            intervalle = Integer.parseInt(paramIntervalle);
        } else {
            paramIntervalle = (String) request.getAttribute("intervalle");
            if (paramIntervalle != null) {
                intervalle = Integer.parseInt(paramIntervalle);
            }
        }

        int index = 0;
        String paramIndex = request.getParameter("index");
        if (paramIndex != null && paramIndex.length() != 0) {
            index = Integer.parseInt(paramIndex);
        } else {
            paramIndex = (String) request.getAttribute("index");
            if (paramIndex != null) {
                index = Integer.parseInt(paramIndex);
            }
        }
        EpmTUtilisateur utilisateur = (EpmTUtilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR);
        Object[] parametres = remplirParametres(typeRecherche, critere, utilisateur.getId(), intervalle, index, request);
        Class[] classes = remplirClasses(typeRecherche, critere, request);
        if (critere instanceof AbstractCritere) {
            AbstractCritere aCritere = (AbstractCritere) critere;
            if (request.getParameter("triCroissant") != null) {
                aCritere.setTriCroissant(request.getParameter("triCroissant").equalsIgnoreCase("true"));
            }
            if (request.getParameter("property") != null) {
                aCritere.setProprieteTriee(request.getParameter("property"));
            }
        }

        pageContext.setAttribute(URL, url);
        pageContext.setAttribute(FORM, formulaire);
        if (facade != null && nomMethode != null) {
            Object resultat;
            try {
                Method method = facade.getClass().getMethod(nomMethode, classes);
                resultat = method.invoke(facade, parametres);
            } catch (InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                LOG.error(e.getMessage(), e.fillInStackTrace());
                pageContext.setAttribute(id, new ArrayList());
                return EVAL_BODY_INCLUDE;
            }

            list = (List) resultat;
            // Traitement post recherche.
            try {
                filtreResultat(list, utilisateur, typeRecherche);
            } catch (TechnicalException e) {
                pageContext.setAttribute(id, new ArrayList());
                LOG.error(e.getMessage(), e.fillInStackTrace());
                return EVAL_BODY_INCLUDE;
            }

            // Stockage du resultat en session,
            // s'il y a une demande de l'action appelante.
            if (nomResultat != null && !"".equals(nomResultat)) {
                session.setAttribute(nomResultat, list);
            }

            if (critere instanceof AbstractCritere) {
                AbstractCritere aCritere = (AbstractCritere) critere;
                Number taille = 0L;
                if (aCritere.isChercherNombreResultatTotal()) {
                    if (!list.isEmpty() && list.get(0) instanceof Number) {
                        taille = (Number) list.remove(0);
                    }
                    pageContext.setAttribute(NB_RESULT, String.valueOf(taille.intValue()));
                    pageContext.getRequest().setAttribute(NB_RESULT, String.valueOf(taille.intValue()));
                }
            }
        } else if (name != null) { // Cas pas de facade, methode en session.
            pageContext.getRequest().setAttribute(NB_RESULT, "0");
            if (property == null)
                list = (List) tagUtils.lookup(pageContext, name, scope);
            else
                list = (List) tagUtils.lookup(pageContext, name, property, scope);
        }
        if (list == null) {
            pageContext.setAttribute(id, new ArrayList());
            return EVAL_BODY_INCLUDE;
        }

        pageContext.setAttribute(LIST, list);
        pageContext.setAttribute(id, list);

        return EVAL_BODY_INCLUDE;
    }

    private Object[] remplirParametres(Integer typeRecherche, Object critere, int idUtilisateur, int intervalle, int index, HttpServletRequest request) {
        Utilisateur utilisateurCoordination = (Utilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION);
        Object[] res = null;
        if (typeRecherche != null) {
            int idTypeRecerche = typeRecherche.intValue();
            AbstractCritere criteres = null;
            switch (idTypeRecerche) {
            case TypeRecherche.S_RECHERCHE_ADMINISTRATION_PROFIL:
                String codeProfil = (String) critere;
                res = new Object[1];
                res[0] = codeProfil;
                break;
            case TypeRecherche.S_RECHERCHE_CANEVAS:
            case TypeRecherche.S_RECHERCHE_CONSULTATION:
            case TypeRecherche.S_RECHERCHE_CONTRAT:
                criteres = (AbstractCritere) critere;
                if (!toutAfficher) {
                    criteres.setTaillePage(intervalle);
                }
                criteres.setNumeroPage(index);
                res = new Object[2];
                res[0] = idUtilisateur;
                res[1] = criteres;
                break;
            case TypeRecherche.S_RECHERCHE_OPERATION_UNITE_FONCTIONNELLE:
                criteres = (AbstractCritere) critere;
                if (!toutAfficher) {
                    criteres.setTaillePage(intervalle);
                }
                criteres.setNumeroPage(index);
                res = new Object[3];
                res[0] = idUtilisateur;
                res[1] = criteres;
                res[2] = utilisateurCoordination.getIdOrganisme();
                break;
            case TypeRecherche.S_RECHERCHE_REUNION:
            case TypeRecherche.S_RECHERCHE_INSTANCE_COMMISSION:
                criteres = (AbstractCritere) critere;
                if (!toutAfficher) {
                    criteres.setTaillePage(intervalle);
                }
                criteres.setNumeroPage(index);
                res = new Object[2];
                res[0] = utilisateurCoordination;
                res[1] = criteres;
                break;
            case TypeRecherche.S_RECHERCHE_DEFAUT:
                criteres = (AbstractCritere) critere;
                if (!toutAfficher) {
                    criteres.setTaillePage(intervalle);
                }
                criteres.setNumeroPage(index);
                res = new Object[1];
                res[0] = criteres;
                break;
            default:
                break;
            }
        }
        return res;
    }

    public int doAfterBody() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        name = null;
        property = null;
        scope = null;
        elementAfficher = null;
        url = null;
    }

    private Class[] remplirClasses(Integer typeRecherche, Object critere, HttpServletRequest request) {
        Utilisateur utilisateurCoordination = (Utilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION);
        Class[] res = null;
        if (typeRecherche != null) {
            int idTypeRecerche = typeRecherche.intValue();
            AbstractCritere criteres = null;
            switch (idTypeRecerche) {
            case TypeRecherche.S_RECHERCHE_ADMINISTRATION_PROFIL:
                String codeProfil = (String) critere;
                res = new Class[1];
                res[0] = codeProfil.getClass();
                break;
            case TypeRecherche.S_RECHERCHE_CANEVAS:
            case TypeRecherche.S_RECHERCHE_CONTRAT:
            case TypeRecherche.S_RECHERCHE_CONSULTATION:
                criteres = (AbstractCritere) critere;
                res = new Class[2];
                res[0] = int.class;
                res[1] = criteres.getClass();
                break;
            case TypeRecherche.S_RECHERCHE_OPERATION_UNITE_FONCTIONNELLE:
                criteres = (AbstractCritere) critere;
                res = new Class[3];
                res[0] = int.class;
                res[1] = criteres.getClass();
                res[2] = Integer.class;
                break;
            case TypeRecherche.S_RECHERCHE_REUNION:
            case TypeRecherche.S_RECHERCHE_INSTANCE_COMMISSION:
                criteres = (AbstractCritere) critere;
                res = new Class[2];
                res[0] = utilisateurCoordination.getClass();
                res[1] = criteres.getClass();
                break;
            case TypeRecherche.S_RECHERCHE_DEFAUT:
                criteres = (AbstractCritere) critere;
                res = new Class[1];
                res[0] = criteres.getClass();
                break;
            default:
                break;
            }
        }
        return res;
    }

    /**
     * @return fonction javascript, cas du passage par une URL
     */
    private String getJavascriptUrl() {
        StringBuffer sb = new StringBuffer();
        sb.append("<script>");
        sb.append("function sendUrl(url, suivant){");
        sb.append("document.location.href=url + ");
        if (url.indexOf("?") == -1) {
            sb.append("\"?intervalle=\" + ");
        } else {
            sb.append("\"&intervalle=\" + ");
        }
        sb.append("document.getElementById('nbResults').value").append(" + \"&");
        sb.append("index=\" + suivant;");
        sb.append("}");
        sb.append("</script>");
        return sb.toString();
    }

    /**
     * Ajoute aux ConsultPassation les informations de périmétre.
     * Supprime les consultations dans aucun des périmetres.
     * @param consults collection de consultations
     * @param utilisateur utilisateur courant
     * @throws TechnicalException erreur
     */
    protected static void filtreResultat(final Collection consults, final EpmTUtilisateur utilisateur, final Integer typeRecherche) {
        final int idDsUtil;
        final DirectionService dsUtil;
        if (typeRecherche != null) {
            int idTypeRecerche = typeRecherche.intValue();
            switch (idTypeRecerche) {
            case TypeRecherche.S_RECHERCHE_CONSULTATION:
                if (consults == null || noyauProxy == null || utilisateur == null) {
                    String msg = "Contrat non rempli: " + "noyau: " + noyauProxy + "consults: " + consults + "utilisateur: " + utilisateur;
                    throw new TechnicalException(msg);
                }
                idDsUtil = utilisateur.getDirectionService();
                break;
            default:
                break;
            }
        }
    }

    /**
     * @return fonction javascript, cas de l'envoi
     */
    private String getJavascriptSubmit() {
        StringBuffer sb = new StringBuffer();
        sb.append("<script>");
        sb.append("function envoiTableau(typeEnvoi, page, propriete){\n");
        if (script != null) {
            // Script de préparation à appeler avant celui-ci.
            sb.append(script).append("; ");
        }
        sb.append("var champType = document.getElementById(\"typeSubmit\"); ");
        sb.append("champType.value = typeEnvoi; ");

        sb.append("var champNbResults = document.getElementById('nbResults'); ");
        sb.append("if (champNbResults != null) { ");
        sb.append("   var champIntervalle = document.getElementById('intervalleSubmit'); ");
        sb.append("   champIntervalle.value = champNbResults.value; ");
        sb.append("}");

        sb.append("var champPage = document.getElementById(\"pageSubmit\"); ");
        sb.append("champPage.value = page; ");
        sb.append("");
        sb.append("switch (typeEnvoi) { ");
        sb.append("   case 1: ");
        // Changement d'intervalle
        sb.append("   case 2: ");
        // Précédent/suivant
        sb.append("      break;");
        sb.append("   case 3: ");
        // Tri
        sb.append("      var champPropriete = document.getElementById(\"proprieteSubmit\"); ");
        sb.append("      champPropriete.value = propriete; ");
        sb.append("      break;");
        sb.append("   case 4: ");
        // Validation
        sb.append("      break;");
        sb.append("   } ");
        sb.append("document.").append(formulaire).append(".submit(); ");
        sb.append("} ");
        sb.append("</script>");
        return sb.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setElementAfficher(String elementAfficher) {
        this.elementAfficher = elementAfficher;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param form nom du formulaire utilisé pour envoyer au serveur
     */
    public final void setformulaire(final String form) {
        formulaire = form;
    }

    /**
     * @param valeur nom du script à appeler avant les envois au serveur
     */
    public final void setScript(final String valeur) {
        script = valeur;
    }

    /**
     * @param valeur noyauProxy
     */
    public void setNoyauProxy(NoyauProxyImpl valeur) {
        TableauPagineTag.noyauProxy = valeur;
    }

    public final void setToutAfficher(final String valeur) {
        this.toutAfficher = "true".equals(valeur);
    }

    public final void setNombreResultatAAfficherParDefaut(final Integer valeur) {
        this.nombreResultatAAfficherParDefaut = valeur;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
