package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.TemplateFile;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nty on 05/04/17.
 */
public class TemplateFileConstraintValidator implements ConstraintValidator<TemplateFile, MultipartFile> {

    @Override
    public void initialize(TemplateFile template) {

    }

    @Override
    public boolean isValid(MultipartFile field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {
/*
            ConstraintViolationBuilder cvb = cxt.buildConstraintViolationWithTemplate(cxt.getDefaultConstraintMessageTemplate());
            //cvb.addNode(getFirstFieldName()).addConstraintViolation();
            cvb.addNode(getSecondFieldName()).addConstraintViolation();
*/
        }
        return true;
    }

}
