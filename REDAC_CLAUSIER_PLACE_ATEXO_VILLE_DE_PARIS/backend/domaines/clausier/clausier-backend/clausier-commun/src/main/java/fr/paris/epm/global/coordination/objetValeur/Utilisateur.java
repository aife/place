package fr.paris.epm.global.coordination.objetValeur;

import java.io.Serializable;

/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class Utilisateur implements Serializable {

    /**
	 * Serialisation
	 */
	private static final long serialVersionUID = 5219774615218961420L;

	/**
     * Propriétés à ignorer lors de la copie.
     */
    public static final String [] IGNORER = {"directionService"};

    /**
     * identifiant de l'enregistrement.
     */
    private int id;

    /**
     * direction service de l'utilisateur.
     */
    private SimplePair directionService;

    /**
     * nom de l'utilisateur.
     */
    private String nom;

    /**
     * prénom de l'utilisateur.
     */
    private String prenom;

    /**
     * email de l'utilisateur.
     */
    private String courriel;

    /**
     * Adresse de l'utilisateur.
     */
    private String adresse;
    
    private String telephone;
    
    private String fax;

    /**
     * Identifiant de l'organisme
     */
    protected Integer idOrganisme;

    // Méthodes
    /**
     * L'identifiant n'est pas utilisé.
     * @return code de hachage
     */
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        if (courriel != null) {
            result += prime * result + courriel.hashCode();
        }
        if (directionService != null) {
            result += prime * result + directionService.hashCode();
        }
        if (nom != null) {
            result += prime * result + nom.hashCode();
        }
        if (prenom != null) {
            result += prime * result + prenom.hashCode();
        }
        return result;
    }

    /**
     * L'identifiant n'est pas utilisé.
     * @param obj objet à comparer
     * @return résultat
     */
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Utilisateur autre = (Utilisateur) obj;
        if (courriel == null) {
            if (autre.courriel != null) {
                return false;
            }
        } else if (!courriel.equals(autre.courriel)) {
            return false;
        }
        if (directionService == null) {
            if (autre.directionService != null) {
                return false;
            }
        } else if (!directionService
                .equals(autre.directionService)) {
            return false;
        }
        if (nom == null) {
            if (autre.nom != null) {
                return false;
            }
        } else if (!nom.equals(autre.nom)) {
            return false;
        }
        if (prenom == null) {
            if (autre.prenom != null) {
                return false;
            }
        } else if (!prenom.equals(autre.prenom)) {
            return false;
        }
        return true;
    }


    // Accesseurs

    public final int getId() {
        return this.id;
    }

    public final void setId(final int valeur) {
        this.id = valeur;
    }

    public final SimplePair getDirectionService() {
        return this.directionService;
    }

    public final void setDirectionService(final SimplePair valeur) {
        this.directionService = valeur;
    }

    public final String getNom() {
        return this.nom;
    }

    public final void setNom(final String valeur) {
        this.nom = valeur;
    }

    public final String getPrenom() {
        return this.prenom;
    }

    public final void setPrenom(final String valeur) {
        this.prenom = valeur;
    }

    public final String getCourriel() {
        return this.courriel;
    }

    public final void setCourriel(final String valeur) {
        this.courriel = valeur;
    }

    public final String getAdresse() {
        return adresse;
    }

    public final void setAdresse(final String valeur) {
        this.adresse = valeur;
    }

    public final String getTelephone() {
        return telephone;
    }

    public final void setTelephone(final String valeur) {
        this.telephone = valeur;
    }

    public final String getFax() {
        return fax;
    }

    public final void setFax(final String valeur) {
        this.fax = valeur;
    }
    
    public final String getNomPrenom() {
    	return nom + " " + prenom;
    }

    public final Integer getIdOrganisme() {
        return idOrganisme;
    }

    public final void setIdOrganisme(final Integer valeur) {
        this.idOrganisme = valeur;
    }

}