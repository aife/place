package fr.paris.epm.global.commun.securite;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.AuthentificationTokenCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Utilise a chaque requete de l'utilisateur pour verifier l'integrite du cookie d'authentification (signature du cookie)
 * qu'un seul utilisateur a la fois utilise un meme compte (ou vol de cookie), grace au token incremente a chaque requete.
 * Verification du delai d'inactivite. 
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class RememberMeFilter extends GenericFilterBean {
    /**
     * Acces aux objets persistants (injection Spring).
     */
    private AdministrationServiceSecurise administrationService;

    private static final Logger LOG = LoggerFactory.getLogger(RememberMeFilter.class);
    /**
     * Delai d'inactivite maximum en minute (injection Spring).
     */
    private Integer delaiInactiviteMax;
    /**
     * Indique s'il faut utiliser les tokens pour detecter qu'une meme authentification est utilisee depuis differents navigateurs ou postes
     * ou qu'il y a eu un vol de cookie et deconnecter les utilisateurs.
     */
    private Boolean interdirMultiAuthentification;
    /**
     * Delai d'inactivite maximum par defaut en minute.
     */
    public static final int DELAI_INACTIVITE_MAX_DEFAUT = 30;
    /**
     * Par defaut, detection de l'utilisation d'un meme compte par des utilisateurs differents.
     */
    public static final boolean INTERDIR_MULTI_AUTHENTIFICATION_DEFAUT = true;

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        FilterInvocation fi = new FilterInvocation(request, response, chain);
        verifieMiseAJourCookie(fi.getHttpRequest(), fi.getHttpResponse());
        chain.doFilter(request, response);
    }

    private void verifieMiseAJourCookie(HttpServletRequest request, HttpServletResponse response) {
        String signatureBdd = null;
        Integer tokenBdd = null;
        Date dateDerniereActivite = null;
        String rememberMeCookie = extractRememberMeCookie(request);
        long milliSec20Min;
        String errorMsg;
        boolean justeAuthentifier;

        if (delaiInactiviteMax == null) {
            delaiInactiviteMax = DELAI_INACTIVITE_MAX_DEFAUT;
            LOG.warn("Delai d'incativite maximum non renseigné : utilisation de la valeur par defaut (" + DELAI_INACTIVITE_MAX_DEFAUT + ")");
        }
        // En millisecondes
        milliSec20Min = delaiInactiviteMax * 60 * 60 * 1000;

        if (interdirMultiAuthentification == null) {
            interdirMultiAuthentification = INTERDIR_MULTI_AUTHENTIFICATION_DEFAUT;
            LOG.warn("Détection de la multi-authentification activée.");
        }

        try {
            // Recuperation des informations d'authentification de l'utilisateur en BDD
            EpmTAuthentificationToken authTokenBdd;
            if (SecurityContextHolder.getContext().getAuthentication() == null) {
                return;
            }
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            LOG.debug("username :" + username);
            EpmTUtilisateur utilisateurBdd = administrationService.chargerUtilisateur(username);
            if (utilisateurBdd == null) {
                errorMsg = "Utilisateur " + username + " non trouvé.";
                LOG.error(errorMsg);
                throw new InvalidCookieException(errorMsg);
            }
            AuthentificationTokenCritere critere = new AuthentificationTokenCritere();
            critere.setIdentifiant(username);
            List<EpmTAuthentificationToken> authTokenList = administrationService.chercherEpmTObject(utilisateurBdd.getId(), critere);
            if (authTokenList == null || authTokenList.isEmpty() || authTokenList.size() > 1) {
                errorMsg = "Probleme lors de la recuperation de l'authentification de l'utilisateur (aucun ou plus d'un resultat).";
                LOG.error(errorMsg);
                throw new InvalidCookieException(errorMsg);
            }
            authTokenBdd = authTokenList.get(0);
            // Fin recuperation des informations d'authentification

            // Recuperation des informations d'authentification de l'utilisateur a partir du cookie
            if (rememberMeCookie == null) {
                authTokenBdd.setJusteAuthentifier(false);
                return;
            }
            String[] cookieTokens = decodeCookie(rememberMeCookie);
            if (cookieTokens.length != 3) {
                throw new InvalidCookieException("Cookie token did not contain " + 3 +
                        " tokens, but contained '" + Arrays.asList(cookieTokens) + "'");
            }
            username = cookieTokens[0];
            String signatureCookie = cookieTokens[1];
            String tokenCookieStr = cookieTokens[2];
            int tokenCookie = Integer.parseInt(tokenCookieStr);
            // Fin recuperation des informations du cookie
            Date dateActuelle = new Date();

            // L'utilisateur vient-il de s'authentifier ? (login + mot de passe)
            justeAuthentifier = authTokenBdd.isJusteAuthentifier();

            // Tests de signature et token inutiles si l'utilisateur viens juste d'etre authentifie
            signatureBdd = authTokenBdd.getSignature();
            tokenBdd = authTokenBdd.getToken();
            if (!justeAuthentifier) {
                // Comparaison des signature
                if (signatureCookie == null || signatureBdd == null || !signatureCookie.equals(signatureBdd)) {
                    errorMsg = "Les signatures cookie et bdd ne correspondent pas.";
                    LOG.warn(errorMsg);
                    supprimerInfoAuthentification(utilisateurBdd.getId(), authTokenBdd, request, response);
                    throw new InvalidCookieException(errorMsg);
                }
                if (interdirMultiAuthentification) {
                    // Comparaison des token
                    if (tokenBdd == null) {
                        errorMsg = "Token en bdd inexistant.";
                        LOG.error(errorMsg);
                        supprimerInfoAuthentification(utilisateurBdd.getId(), authTokenBdd, request, response);
                        throw new InvalidCookieException(errorMsg);
                    }
                    if ((tokenBdd != tokenCookie)) {
                        errorMsg = "Les token cookie et bdd ne correspondent pas (vol de cookie?).";
                        LOG.warn(errorMsg);
                        // Cookie potentiellement vole, suppression des informations d'authentifications pour desauthentifier l'utilisateur
                        supprimerInfoAuthentification(utilisateurBdd.getId(), authTokenBdd, request, response);
                        throw new InvalidCookieException(errorMsg);
                    }
                }
            }
            authTokenBdd.setJusteAuthentifier(false);
            // Utilisateur authentifie, comparaison des dates d'inactivite
            dateDerniereActivite = authTokenBdd.getDateDerniereActivite();
            if (dateActuelle.getTime() - dateDerniereActivite.getTime() > milliSec20Min) {
                errorMsg = "Inactivité supérieur à " + delaiInactiviteMax + " minutes, authentification requise.";
                LOG.warn(errorMsg);
                // Delai d'inactivite depasse, suppression des informations d'authentifications
                supprimerInfoAuthentification(utilisateurBdd.getId(), authTokenBdd, request, response);
                throw new InvalidCookieException(errorMsg);
            }
            // Utilisateur authentifie par le cookie (mise a jour du token et de la derniere date d'activite)
            tokenBdd++;
            // Mise a jour du cookie
            setCookie(new String[] {username, signatureBdd, tokenBdd.toString()}, -1, request, response);
            // Mise a jour en BDD
            authTokenBdd.setDateDerniereActivite(dateActuelle);
            authTokenBdd.setToken(tokenBdd);
            administrationService.modifierEpmTObject(authTokenBdd);
        } catch (TechnicalNoyauException e) {
            LOG.error("Erreur lors de la recherche de l'utilisateur en base : " + e.getMessage(), e);
        }
    }

    /**
     * Supprime le cookie d'authentification et les informations en BDD.
     * @throws TechnicalNoyauException
     */
    private void supprimerInfoAuthentification(int utilisateurId, EpmTAuthentificationToken authTokenBdd, HttpServletRequest request, HttpServletResponse response) {
        administrationService.supprimerEpmTObject(utilisateurId, authTokenBdd);
        cancelCookie(request, response);
    }

    /**
     * Sets the cookie on the response (cf. EpmRememberMeServices)
     *
     * @param tokens the tokens which will be encoded to make the cookie value.
     * @param maxAge the value passed to {@link Cookie#setMaxAge(int)}
     * @param request the request
     * @param response the response to add the cookie to.
     */
    private void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response) {
        String cookieValue = encodeCookie(tokens);
        Cookie cookie = new Cookie(AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY, cookieValue);
        cookie.setMaxAge(maxAge);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * Sets a "cancel cookie" (with maxAge = 0) on the response to disable persistent logins (cf. EpmRememberMeServices).
     */
    private void cancelCookie(HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Cancelling cookie");
        Cookie cookie = new Cookie(AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY, null);
        cookie.setMaxAge(0);
        cookie.setPath("/");

        response.addCookie(cookie);
    }

    /**
     * Locates the Spring Security remember me cookie in the request (cf. AbstractRememberMeServices).
     *
     * @param request the submitted request which is to be authenticated
     * @return the cookie value (if present), null otherwise.
     */
    private String extractRememberMeCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        if ((cookies == null) || (cookies.length == 0)) {
            return null;
        }

        for (int i = 0; i < cookies.length; i++) {
            if (AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY
                    .equals(cookies[i].getName())) {
                return cookies[i].getValue();
            }
        }

        return null;
    }

    /**
     * Decodes the cookie and splits it into a set of token strings using the ":" delimiter (cf. AbstractRememberMeServices).
     *
     * @param cookieValue the value obtained from the submitted cookie
     * @return the array of tokens.
     * @throws InvalidCookieException if the cookie was not base64 encoded.
     */
    private String[] decodeCookie(String cookieValue) {
        for (int j = 0; j < cookieValue.length() % 4; j++) {
            cookieValue = cookieValue + "=";
        }

        if (!Base64.isArrayByteBase64(cookieValue.getBytes())) {
            throw new InvalidCookieException( "Cookie token was not Base64 encoded; value was '" + cookieValue + "'");
        }

        String cookieAsPlainText = new String(Base64.decodeBase64(cookieValue.getBytes()));

        return StringUtils.delimitedListToStringArray(cookieAsPlainText, ":");
    }

    /**
     * Inverse operation of decodeCookie (cf. AbstractRememberMeServices).
     *
     * @param cookieTokens the tokens to be encoded.
     * @return base64 encoding of the tokens concatenated with the ":" delimiter.
     */
    private String encodeCookie(String[] cookieTokens) {
        StringBuffer sb = new StringBuffer();
        for(int i=0; i < cookieTokens.length; i++) {
            sb.append(cookieTokens[i]);

            if (i < cookieTokens.length - 1) {
                sb.append(":");
            }
        }

        String value = sb.toString();

        sb = new StringBuffer(new String(Base64.encodeBase64(value.getBytes())));

        while (sb.charAt(sb.length() - 1) == '=') {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString();
    }

    /**
     * @param valeur acces aux objets persistants (injection Spring).
     */
    public void setAdministrationService(AdministrationServiceSecurise valeur) {
        this.administrationService = valeur;
    }
    /**
     * @param valeur delai d'inactivite maximum en minute (injection Spring).
     */
    public final void setDelaiInactiviteMax(final Integer valeur) {
        this.delaiInactiviteMax = valeur;
    }
    /**
     * @param valeur (injection Spring)
     */
    public final void setInterdirMultiAuthentification(final Boolean valeur) {
        this.interdirMultiAuthentification = valeur;
    }

}