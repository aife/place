package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.Phone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nty on 21/03/17.
 */
public class PhoneConstraintValidator implements ConstraintValidator<Phone, String> {

    @Override
    public void initialize(Phone phone) {

    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {

            //validate phone numbers of format "1234567890"
            if (field.matches("\\d{10}"))
                return true;

            //validating phone number with -, . or spaces, ex: "12-34-56-78-90"
            else if (field.matches("\\d{2}[-\\.\\s]\\d{2}[-\\.\\s]\\d{2}[-\\.\\s]\\d{2}[-\\.\\s]\\d{2}"))
                return true;

            else if (field.length() >= 10 || field.length() <= 15 || field.matches("(([+][1-9][0-9][0-9]?[ ])?[0-9]+)"))
                return true;

            return false;
        }
        return true;
    }

}