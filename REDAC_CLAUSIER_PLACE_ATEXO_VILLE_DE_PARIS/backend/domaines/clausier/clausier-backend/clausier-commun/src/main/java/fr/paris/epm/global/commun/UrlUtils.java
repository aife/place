package fr.paris.epm.global.commun;

import org.apache.commons.beanutils.BeanMap;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe des utilitaires commun a tout le projet.
 *
 * @author EDDOUGHMI Younes
 * @version $Revision$, $Date$, $Author$
 */
public class UrlUtils {
    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(UrlUtils.class);

    public static final String PATH_PASSATION="/epm.passation";

    public static final String PATH_HTM="%s%s.htm";
    public static final String PATH_EPM="%s%s.epm";
    public static final String AND="&";
    public static final String DEBUT="?";
    public static final String EGAL="=";



    public static final String construireUrlSpring(String base, String urlSpring, Map<?,?> map) {
        StringBuilder sb = new StringBuilder(construireUrlSpring(base, urlSpring));
        return construireUrl(sb,map);
    }

    public static final String construireUrlSpring(String base, String urlSpring) {
        String templateSpringUrl = String.format(PATH_HTM,base,urlSpring);
        return templateSpringUrl;
    }



    public static final String construireUrlLegacy(String base, String urlSpring, Map<?,?> map){
        StringBuilder sb = new StringBuilder(construireUrlLegacy(base, urlSpring));
        return construireUrl(sb,map);
    }

    public static final String construireUrlLegacy(String base, String urlSpring){
        String templateSpringUrl = String.format(PATH_EPM,base,urlSpring);
        return templateSpringUrl;
    }



    protected static final String construireUrl(StringBuilder sb, Map<?,?> map){
        boolean premier =true;
        for (Map.Entry mapentry : map.entrySet()) {
            if(premier) {
                sb.append(DEBUT);
                sb.append(mapentry.getKey());
                sb.append(EGAL);
                sb.append(mapentry.getValue());
                premier=false;
            }else{
                sb.append(AND);
                sb.append(mapentry.getKey());
                sb.append(EGAL);
                sb.append(mapentry.getValue());
            }
        }
        return sb.toString();
    }

    /**
     * Transforme un bean en une chaîne de paramètres sous la forme "key1=value1&key2=value2&..."
     * @param bean
     * @return
     */
    public static String formatParameters(Object bean) {
        List<BasicNameValuePair> parameters = new ArrayList<>(); Map<Object, Object> criteresRecherche = new BeanMap(bean);
        //Permet de réécrire l'url avec les paramètres de recherche
        for (Map.Entry<Object, Object> entry : criteresRecherche.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();
            //On supprime les champs vides et le champs technique "class"
            if (value != null && ! "class".equals(key)) {
                parameters.add(new BasicNameValuePair(String.valueOf(key), String.valueOf(value)));
            }
        }
        //On récupère une chaîne de paramètres sous la forme "key1=value1&key2=value2&..."
        return URLEncodedUtils.format(parameters, "UTF-8");
    }
}
