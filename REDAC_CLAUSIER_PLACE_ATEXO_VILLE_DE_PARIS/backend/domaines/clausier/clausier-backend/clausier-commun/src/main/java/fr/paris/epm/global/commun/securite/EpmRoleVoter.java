package fr.paris.epm.global.commun.securite;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Iterator;

/**
 * Voter utilisé dans le cadre de spring security permet de gerer les roles
 * @author RME
 *
 */
public class EpmRoleVoter extends RoleVoter {

	public int vote(Authentication authentication, Object object, Collection<ConfigAttribute> config) {
        int result = ACCESS_ABSTAIN;
        Iterator<ConfigAttribute> iter = config.iterator();
        Collection<GrantedAuthority> authorities = extractAuthorities(authentication);        

        while (iter.hasNext()) {
            ConfigAttribute attribute = (ConfigAttribute) iter.next();

            if (this.supports(attribute)) {
                result = ACCESS_DENIED;

                // Attempt to find a matching granted authority
                for (GrantedAuthority grantedAuthority : authorities) {
                	String authoritiesStr = grantedAuthority.getAuthority();
                	authoritiesStr = authoritiesStr.replaceAll("\\s+", "");
                	String authoritiesTab[] = authoritiesStr.split(",");
                	for (String authoritiesTabItem : authoritiesTab) {
                		if (attribute.getAttribute().equals(authoritiesTabItem)) {
                            return ACCESS_GRANTED;
                        }
                	}
                }
            }
        }

        return result;
    }
	
	Collection<GrantedAuthority> extractAuthorities(final Authentication authentication) {
    	return (Collection<GrantedAuthority>) authentication.getAuthorities();
    }
}
