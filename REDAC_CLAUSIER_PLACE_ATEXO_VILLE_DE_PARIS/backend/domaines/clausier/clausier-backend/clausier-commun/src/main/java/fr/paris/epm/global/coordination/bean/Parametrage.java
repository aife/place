package fr.paris.epm.global.coordination.bean;

/**
 * Référentiel pour l'affichage des valeurs de parametrages de la version cliente de l'application.
 * Created by nty on 03/05/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class Parametrage extends Directory {

    private String clef;

    private String valeur;

    public String getClef() {
        return clef;
    }

    public void setClef(String clef) {
        this.clef = clef;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

}
