package fr.paris.epm.global.coordination.objetValeur;

import java.io.Serializable;


/**
 * Bean utilisé pour la gestion de l'affiche de la popup d'attente de la génération des documents modéles.
 * @author Léon BARSAMIAN
 *
 */
public class GenerationDocumentModeleBean implements Serializable {

	/**
	 * Serialisation.
	 */
	private static final long serialVersionUID = 2670448582140769556L;

	/**
	 * Token de suivi de génération du document
	 */
	private String token;
	
	/**
	 * Le nom du fichier à générer, sert pour l'affichage du résultat dans la popup d'attente.
	 */
	private String nomFichierAGenerer;

    /**
     * Le chemin physique du fichier à générer.
     */
    private String cheminFichierAGenerer;


    /**
     * @return chemin physique du fichier.
     */
    public String getCheminFichierAGenerer() {
        return cheminFichierAGenerer;
    }

    /**
     * @param cheminFichierAGenerer le chemin du fichier généré.
     */
    public void setCheminFichierAGenerer(String cheminFichierAGenerer) {
        this.cheminFichierAGenerer = cheminFichierAGenerer;
    }

	/**
	 * @return the Token de suivi de génération du document
	 */
	public final String getToken() {
		return token;
	}

	/**
	 * @param valeur Token de suivi de génération du document
	 */
	public final void setToken(String valeur) {
		this.token = valeur;
	}

	/**
	 * @return Le nom du fichier à générer, sert pour l'affichage du résultat dans la popup d'attente.
	 */
	public final String getNomFichierAGenerer() {
		return nomFichierAGenerer;
	}

	/**
	 * @param valeur Le nom du fichier à générer, sert pour l'affichage du résultat dans la popup d'attente.
	 */
	public final void setNomFichierAGenerer(String valeur) {
		this.nomFichierAGenerer = valeur;
	}
}
