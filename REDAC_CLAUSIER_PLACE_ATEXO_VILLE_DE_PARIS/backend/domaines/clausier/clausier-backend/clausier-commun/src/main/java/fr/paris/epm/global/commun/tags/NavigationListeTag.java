/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import org.apache.struts.Globals;
import org.apache.struts.taglib.TagUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class NavigationListeTag extends TagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private String key;

    private String bundle;

    private boolean suivant;

    protected String localeKey = Globals.LOCALE_KEY;

    private enum NavigationListeEnum {PRECEDENT, SUIVANT} ;

    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
            StringBuffer sb = new StringBuffer();

            int intervalle = 10;
            if (pageContext.getAttribute(TableauTag.INTERVALLE_AFFICHER) != null) {
                intervalle =  Integer.parseInt(((String[]) pageContext.getAttribute(TableauTag.INTERVALLE_AFFICHER))[0]);
            }
            if (pageContext.getAttribute("intervalle") != null && (Integer)pageContext.getAttribute("intervalle") != 0) {
                intervalle = (Integer)pageContext.getAttribute("intervalle");
            }
            String paramIntervalle = request.getParameter("intervalle");
            if (paramIntervalle != null && paramIntervalle.length() != 0) {
                intervalle = Integer.parseInt(paramIntervalle);
            } else {
                paramIntervalle = (String) request.getAttribute("intervalle");
                if (paramIntervalle != null) {
                    intervalle = Integer.parseInt(paramIntervalle);
                }
            }
            String taillePagination = (String) pageContext.getAttribute(TableauPagineTag.NB_RESULT);
            List list = (List) pageContext.getAttribute(TableauTag.LIST);
            String url = (String) pageContext.getAttribute(TableauTag.URL);
            String form = (String) pageContext.getAttribute(TableauTag.FORM);
            int max = 0;
            if (taillePagination != null) { // cas d'une pagination
                max = (int) Math.ceil(Double.parseDouble(taillePagination) / (double) intervalle);
            } else if (list != null) { // cas ou il n'y a pas de pagination
                max = (int) Math.ceil(Double.parseDouble(String.valueOf(list.size())) / (double) intervalle);
            }
            int index = 0;
            String paramIndex = request.getParameter("index");
            if (paramIndex != null && paramIndex.length() != 0) {
                index = Integer.parseInt(paramIndex);
            } else {
                paramIndex = (String) request.getAttribute("index");
                if (paramIndex != null) {
                    index = Integer.parseInt(paramIndex);
                }
            }
            String message = TagUtils.getInstance().message(pageContext, bundle, localeKey, key);
            if (suivant) {
                if (index >= max - 1) {
                    sb.append(message);
                } else {
                    if (form != null) {
                        sb.append(createFORM(message, index + 1));
                    } else {
                        sb.append(createURL(message, url, index + 1, NavigationListeEnum.SUIVANT));
                    }

                }
            } else {
                if (index == 0) {
                    sb.append(message);
                } else {
                    if (form != null) {
                        sb.append(createFORM(message, index - 1));
                    } else {
                        sb.append(createURL(message, url, index - 1, NavigationListeEnum.PRECEDENT));
                    }
                }
            }

            pageContext.getOut().print(sb.toString());
        } catch (IOException e) {
            throw new JspException("I/O Error", e);
        }
        return SKIP_BODY;
    }

    public void release() {
        key = null;
        bundle = Globals.MESSAGES_KEY;
        suivant = false;
    }

    private String createFORM(final String value, final int index) {
        return "<a href=\"javascript:envoiTableau(2, " + index + ");\">" + value + "</a>";
    }

    private String createURL(String value, String url, int index, NavigationListeEnum precedentSuivant) {
        return "<a id=\"navigationListe" + precedentSuivant + "\" href=\"javascript:sendUrl('" +
                url + "'," + index + ");\">" + value + "</a>";
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setBundle(String bundle) {
        this.bundle = bundle;
    }

    public void setSuivant(boolean suivant) {
        this.suivant = suivant;
    }

}
