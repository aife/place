package fr.paris.epm.global.presentation.controllers;


public class WsMessage {
    private int code;
    private String message;
    private String type;

    public static WsMessageBuilder builder() {
        return new WsMessageBuilder();
    }

    public WsMessage() {
    }

    public WsMessage(final int code, final String message, final String type) {
        this.code = code;
        this.message = message;
        this.type = type;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getType() {
        return this.type;
    }

    public void setCode(final int code) {
        this.code = code;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public static class WsMessageBuilder {
        private int code;
        private String message;
        private String type;

        WsMessageBuilder() {
        }

        public WsMessageBuilder code(final int code) {
            this.code = code;
            return this;
        }

        public WsMessageBuilder message(final String message) {
            this.message = message;
            return this;
        }

        public WsMessageBuilder type(final String type) {
            this.type = type;
            return this;
        }

        public WsMessage build() {
            return new WsMessage(this.code, this.message, this.type);
        }

        public String toString() {
            return "WsMessage.WsMessageBuilder(code=" + this.code + ", message=" + this.message + ", type=" + this.type + ")";
        }
    }
}
