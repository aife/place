package fr.paris.epm.global.coordination.facade;

import java.util.function.Supplier;

/**
 * Interface SafelyNoyauInvokator
 * Created by nty on 07/03/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public interface SafelyNoyauInvokator {

    public <E> E safelyInvoke(Supplier<E> supplier) ;

}
