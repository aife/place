package fr.paris.epm.global.commons.exception;

/**
 * Exception levée lorsque l'objet cherché n'a pas été trouvé.
 * @author Guillaume Béraudo
 * @version $Revision: 103 $,
 *          $Date: 2007-07-23 19:16:22 +0200 (lun., 23 juil. 2007) $,
 *          $Author: beraudo $
 */
public class NonTrouveException extends RuntimeException {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public NonTrouveException() {
        super();
    }

    /**
     * @param msg message d'erreur
     * @param cause Exception ayant provoqué cette exception
     */
    public NonTrouveException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public NonTrouveException(final String msg) {
        super(msg);
    }

    /**
     * @param cause Exception ayant provoqué cette exception
     */
    public NonTrouveException(final Throwable cause) {
        super(cause);
    }
}
