/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import org.apache.struts.taglib.TagUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class TableauTag extends BodyTagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public static final String INTERVALLE_AFFICHER = "INTERVALLE_AFFICHER";
    public static final String LIST = "LIST";
    public static final String URL = "URL";
    public static final String FORM = "FORM";
    private TagUtils tagUtils = TagUtils.getInstance();

    private String elementAfficher;

    private String name;

    private String property;

    private String scope;

    private String url;

    private String script;

    /**
     * Nom du formulaire utilisé pour envoyer les données au serveur.
     */
    private String formulaire;

    public int doStartTag() throws JspException {
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();
        List list;
        if(property == null){
            list = (List) tagUtils.lookup(pageContext, name, scope);
        }else{
            list = (List) tagUtils.lookup(pageContext, name, property, scope);
        }
        
        pageContext.setAttribute(URL, url);
        pageContext.setAttribute(FORM, formulaire);
        String[] array = elementAfficher.split(",");
        pageContext.setAttribute(INTERVALLE_AFFICHER, array);
        try {
            if (formulaire != null) {
                pageContext.getOut().print(getJavascriptSubmit());
            } else {
                pageContext.getOut().print(getJavascriptUrl());
            }
        } catch (IOException e) {
            throw new JspException(e);
        }
        if (list == null) {
            pageContext.setAttribute(id, new ArrayList());
            return EVAL_BODY_INCLUDE;
        }
        pageContext.setAttribute(LIST, list);
        String paramProperty = request.getParameter("property");
        if (paramProperty != null && paramProperty.length() != 0) {
            String colonne = request.getParameter("property");
            TableauComparator tabComparator = new TableauComparator();
            tabComparator.setPropertieComparer(colonne);
            Collections.sort(list, tabComparator);
        }else{
            paramProperty = (String) request.getAttribute("property");
            if (paramProperty!=null){
                String colonne = paramProperty;
                TableauComparator tabComparator = new TableauComparator();
                tabComparator.setPropertieComparer(colonne);
                Collections.sort(list, tabComparator);
            }
        }

        int intervalle = Integer.parseInt(array[0]);
        String paramIntervalle = request.getParameter("intervalle");
        if (paramIntervalle != null && paramIntervalle.length() != 0) {
            intervalle = Integer.parseInt(paramIntervalle);
        }else{
            paramIntervalle = (String) request.getAttribute("intervalle");
            if (paramIntervalle!=null){
                intervalle = Integer.parseInt(paramIntervalle);
            }
        }

        int index = 0;
        String paramIndex = request.getParameter("index");
        if (paramIndex != null && paramIndex.length() != 0) {
            index = Integer.parseInt(paramIndex);
        }else{
            paramIndex = (String) request.getAttribute("index");
            if (paramIndex!=null){
                index = Integer.parseInt(paramIndex);
            }
        }

        int fromIndex = index * intervalle;
        int toIndex = fromIndex + intervalle;
        
        toIndex = (list.size() > toIndex) ? toIndex : list.size();
        pageContext.setAttribute(id, list.subList(fromIndex, toIndex));
       
        return EVAL_BODY_INCLUDE;
    }

    public int doAfterBody() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        name = null;
        property = null;
        scope = null;
        elementAfficher = null;
        url = null;
    }

    /**
     * @return fonction javascript, cas du passage par une URL
     */
    private String getJavascriptUrl() {
        StringBuffer sb = new StringBuffer();
        sb.append("<script>");
        sb.append("function sendUrl(url, suivant){");
        sb.append("window.location.href=url + ");
        if (url.indexOf("?") == -1){
            sb.append("\"?intervalle=\" + ");
        } else {
            sb.append("\"&intervalle=\" + ");
        }
        sb.append(
                "document.getElementById('nbResults').value").append(" + \"&");
        sb.append("index=\" + suivant;");
        sb.append("}");
        sb.append("</script>");
        return sb.toString();
    }
    
    
    /**
     * @return fonction javascript, cas de l'envoi
     */
    private String getJavascriptSubmit() { 
        StringBuffer sb = new StringBuffer();
        sb.append("<script>");
        sb.append("function envoiTableau(typeEnvoi, page, propriete){\n");
        if (script != null) {
            // Script de préparation à appeler avant celui-ci.
            sb.append(script).append("; ");
        }
        sb.append("var champType = document.getElementById(\"typeSubmit\"); ");
        sb.append("champType.value = typeEnvoi; ");
        sb.append("");

        sb.append("var champNbResults = document.getElementById('nbResults'); ");
        sb.append("if (champNbResults != null) { ");
        sb.append("   var champIntervalle = document.getElementById('intervalleSubmit'); ");
        sb.append("   champIntervalle.value = champNbResults.value; ");
        sb.append("}");
        sb.append("");
        sb.append("var champPage = document.getElementById(\"pageSubmit\"); ");
        sb.append("champPage.value = page; ");
        sb.append("");
        sb.append("switch (typeEnvoi) { ");
        sb.append("   case 1: ");
        // Changement d'intervalle
        sb.append("   case 2: ");
        // Précédent/suivant
        sb.append("      break;");
        sb.append("   case 3: ");
        // Tri
        sb.append("      var champPropriete = document.getElementById(\"proprieteSubmit\"); ");
        sb.append("      champPropriete.value = propriete; ");
        sb.append("      break;");
        sb.append("   case 4: ");
        // Validation
        sb.append("      break;");
        sb.append("   } ");
        sb.append("document." + formulaire + ".submit(); ");
        sb.append("} ");
        sb.append("</script>");
        return sb.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setElementAfficher(String elementAfficher) {
        this.elementAfficher = elementAfficher;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param form nom du formulaire utilisé pour envoyer au serveur
     */
    public final void setformulaire(final String form) {
        formulaire = form;
    }

    /**
     * @param valeur nom du script à appeler avant les envois au serveur
     */
    public final void setScript(final String valeur) {
        script = valeur;
    }
}
