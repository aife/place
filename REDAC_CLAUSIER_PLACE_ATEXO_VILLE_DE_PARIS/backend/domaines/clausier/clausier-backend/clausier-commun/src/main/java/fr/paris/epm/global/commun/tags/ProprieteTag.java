package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.Config;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class ProprieteTag extends TagSupport {

    /**
     * Propriétés de commun.properties.
     */
    private static Properties props;
    
    /**
     * Marqueur de sérialisation.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Clef à utiliser.
     */
    private String clef;

    public int doStartTag() throws JspException {
        try {
            if (props == null) {
            	props = Config.getProperties();
            }
            String resultat = props.getProperty(clef);
            pageContext.getOut().print(resultat);  
        } catch (IOException e) {
            return SKIP_BODY;
        }

        return SKIP_BODY;
    }

    /**
     * @param valeur clef à chercher
     */
    public final void setClef(final String valeur) {
        this.clef = valeur;
    }
}