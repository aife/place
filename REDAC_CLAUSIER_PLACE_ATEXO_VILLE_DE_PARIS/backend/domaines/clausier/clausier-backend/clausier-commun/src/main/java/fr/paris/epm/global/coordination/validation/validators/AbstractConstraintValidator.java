package fr.paris.epm.global.coordination.validation.validators;

import org.springframework.util.ReflectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;

public abstract class AbstractConstraintValidator<A extends Annotation, T> implements ConstraintValidator<A, T> {

    public Object getFieldValue(Object instance, String fieldName) throws IllegalAccessException {
        Class<?> clazz = instance.getClass();

        Field field = ReflectionUtils.findField(clazz, fieldName);
        field.setAccessible(true);
        return field.get(instance);
    }

    boolean checkFieldEmpty(Object fieldValue) {
        if (fieldValue == null)
            return true;

        else if (fieldValue.getClass().equals(String.class))
            return ((String) fieldValue).isEmpty();

        else if (fieldValue.getClass().equals(Integer.class))
            return (Integer) fieldValue == 0;

        else if (fieldValue.getClass().equals(BigDecimal.class))
            return ((BigDecimal) fieldValue).doubleValue() == 0.0;

        else if (fieldValue instanceof MultipartFile)
            return ((MultipartFile) fieldValue).isEmpty();

        return false;
    }

}
