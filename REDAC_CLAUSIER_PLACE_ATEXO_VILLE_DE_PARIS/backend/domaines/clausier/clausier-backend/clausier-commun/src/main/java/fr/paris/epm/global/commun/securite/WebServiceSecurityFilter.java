package fr.paris.epm.global.commun.securite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Appelee pour chaque requete concernant les webservices REST (sauf celle
 * d'authentification). Verifie que le token fournit en parametre est present en
 * base, sinon la requete est refusee.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class WebServiceSecurityFilter extends RequestHeaderAuthenticationFilter {

    
    /**
     * Balise encapsulant un message REST token.
     */
    public static final String XML_QNAME_TOKEN = "ticket";
    
    /**
     * Loguer.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebServiceSecurityFilter.class);


	@Override
	protected Object getPreAuthenticatedPrincipal(
			HttpServletRequest request) {
		String signature = request.getParameter(XML_QNAME_TOKEN);;
		return signature;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(
			HttpServletRequest paramHttpServletRequest) {
		return "N/A";
	}
}
