package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.AuthentificationTokenCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Appelee pour chaque requete concernant les webservices REST (sauf celle
 * d'authentification). Verifie que le token fournit en parametre est present en
 * base, sinon la requete est refusee.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public class EpmRestWebServiceSecurityInterceptor extends FilterSecurityInterceptor {
    /**
     * Acces aux objets persistants (injection Spring).
     */
    private static AdministrationServiceSecurise administrationService;
    
    /**
     * Balise encapsulant un message REST token.
     */
    public static final String XML_QNAME_TOKEN = "ticket";
    
    /**
     * Loguer.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EpmRestWebServiceSecurityInterceptor.class);
    /**
     * Delai d'inactivite maximum en minute (injection Spring).
     */
    private Integer delaiInactiviteMax;

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        FilterInvocation fi = new FilterInvocation(request, response, chain);
        logRequest(fi.getHttpRequest());
        verifieToken(fi.getHttpRequest(), fi.getHttpResponse());
        invoke(fi);
    }

    /**
     * Verification que le token fournit en parametre est present en base et
     * n'est pas expire.
     */
    private void verifieToken(HttpServletRequest request, HttpServletResponse response) {

        String signature = request.getParameter(XML_QNAME_TOKEN);
        EpmTAuthentificationToken tokenBdd = null;
        long delaiInactiviteMaxMilliSec;

        if (signature == null) {
            BadCredentialsException e = new BadCredentialsException(
                    "Echec d'authentification : parametre token manquant.");
            LOG.info(e.getMessage(), e);
            throw e;
        }

        // Recherche du token en base
        AuthentificationTokenCritere tokenCritere = new AuthentificationTokenCritere();
        tokenCritere.setSignature(signature);

        List<EpmTAuthentificationToken> tokenBddList = null;
        try {
            tokenBddList = administrationService.chercherEpmTObject(0, tokenCritere);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage());
        }
        if ((tokenBddList == null) || tokenBddList.isEmpty()) {
            BadCredentialsException e = new BadCredentialsException(
                    "Echec d'authentification : token manquant en base.");
            LOG.info(e.getMessage(), e);
            throw e;
        }
        tokenBdd = tokenBddList.get(0);

        // Verification du delai d'inactivite
        delaiInactiviteMaxMilliSec = delaiInactiviteMax * 60 * 60 * 1000;
        Date dateActuelle = new Date();
        Date dateDerniereActivite = tokenBdd.getDateDerniereActivite();
        if (dateActuelle.getTime() - dateDerniereActivite.getTime() > delaiInactiviteMaxMilliSec) {
            BadCredentialsException e = new BadCredentialsException(
                    "Echec d'authentification : inactivité supérieur à " + delaiInactiviteMax
                            + " minutes.");
            LOG.info(e.getMessage(), e);
            try {
                administrationService.supprimerAuthentificationToken(0, tokenBdd.getIdentifiant());
            } catch (TechnicalNoyauException e1) {
                LOG.error(e.getMessage(), e);
                throw new AuthenticationServiceException(e.getMessage());
            }
        }
        tokenBdd.setDateDerniereActivite(dateActuelle);
        try {
            administrationService.modifierAuthentificationToken(0, tokenBdd);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e);
            throw new AuthenticationServiceException(e.getMessage());
        }
    }

    /**
     * Logger la requete entrante.
     */
    private void logRequest(HttpServletRequest request) {

        String queryString = "";
        if (request.getQueryString() != null) {
            queryString = request.getQueryString();
        }

        LOG.info("Requete REST : " + request.getRequestURL() + "?" + queryString);
    }

    /**
     * @param valeur delai d'inactivite maximum en minute (injection Spring).
     */
    public final void setDelaiInactiviteMax(final Integer valeur) {
        this.delaiInactiviteMax = valeur;
    }

    /**
     * @param valeur Acces aux objets persistants (injection Spring).
     */
    public final void setAdministrationService(AdministrationServiceSecurise valeur) {
        EpmRestWebServiceSecurityInterceptor.administrationService = valeur;
    }
}
