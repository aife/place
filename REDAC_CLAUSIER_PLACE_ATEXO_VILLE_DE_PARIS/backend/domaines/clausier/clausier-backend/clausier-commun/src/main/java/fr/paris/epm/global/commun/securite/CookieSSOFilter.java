package fr.paris.epm.global.commun.securite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtre utilisé par le WebSSO VDP.
 * @author RME
 *
 */
public class CookieSSOFilter extends RequestHeaderAuthenticationFilter {
	/**
	 * Le nom du cookie sur le poste client.
	 */
	private String cookie = "MDP-WSSO-MDPWSSOGUID";

	/**
	 * Le LOG.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CookieSSOFilter.class);
	/**
	 * Guid par défaut injecté par Spring.
	 */
	private String guidDefaut;

	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		String username = "";
		Cookie cookies[] = request.getCookies();
		Cookie myCookie = null;
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if ((cookies[i].getName()).equals(cookie)) {
					myCookie = cookies[i];
					break;
				}
			}
		}
		if (myCookie != null) {
			username = myCookie.getValue();

		}
		if (username == null) {
			username = "";
		}
		if (guidDefaut != null && !guidDefaut.equals("")) {
			username = guidDefaut;
		}
		return username;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(
			HttpServletRequest paramHttpServletRequest) {
		return "N/A";
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	/**
	 * GUID alimenté par le fichier de configuration commun.properties
	 * @param guidDefaut
	 */
	public void setGuidDefaut(String guidDefaut) {
		this.guidDefaut = guidDefaut;
	}
}
