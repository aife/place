package fr.paris.epm.global.coordination.validation.validators;

import fr.paris.epm.global.coordination.validation.annotations.BankAccount;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by nty on 21/03/17.
 */
public class BankAccountConstraintValidator implements ConstraintValidator<BankAccount, String> {

    @Override
    public void initialize(BankAccount bankAccount) {

    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        if (field != null && !field.isEmpty()) {
            if (!field.matches("7[0-2][0-9][0-9][0-9][A-Z]"))
                return false;
        }
        return true;
    }

}