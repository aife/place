package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.presentation.AbstractAction;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Declenchee lorsque de l'accès à une ressource non autorisé (possède pas habilitation).
 * 
 * @author xga
 */
public class AccesRefuseAction extends AbstractAction {

    public final ActionForward execute(final SessionManagerGlobal sessionManager, final ActionMapping mapping,
            final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws SessionExpireeException {

        return mapping.findForward("accesRefuse");
    }
}
