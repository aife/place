/**
 * 
 */
package fr.paris.epm.global.commun.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Régis Menet
 * @version $Revision: $, $Date: $, $Author: $
 */
public class ProtectionUrlTag extends BodyTagSupport {
    private static final long serialVersionUID = 1L;
    private static final Pattern pattern = Pattern.compile("[a-zA-Z]+\\.epm$");
    private String url;

    public int doStartTag() throws JspException {
        Matcher m = pattern.matcher(url);
        if (m.matches()) {
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    public int doAfterBody() throws JspException {
        return EVAL_PAGE;
    }
    
    public void release() {
        url = null;
    }

	public void setUrl(String url) {
		this.url = url;
	}
}
