package fr.paris.epm.global.presentation.forms;

import org.apache.struts.action.ActionForm;

/**
 * @author Léon Barsamian
 *
 */
public abstract class AbstractCourrielGeneriqueForm extends ActionForm {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 673804499226900317L;

    /**
     * Destinataire du courriel.
     */
    private String[] destinataires;

    /**
     * Expéditeur du courriel.
     */
    private String[] expediteurs;

    /**
     * Message du courriel.
     */
    private String[] messages;

    /**
     * Objet du courriel.
     */
    private String[] objets;
    
    private Long tailleMaxAutorisee;

    /**
     * @return Destinataire du courriel.
     */
    public final String[] getDestinataires() {
        return destinataires;
    }

    /**
     * @param valeur Destinataire du courriel.
     */
    public final void setDestinataires(String[] valeur) {
        this.destinataires = valeur;
    }

    /**
     * @return Expéditeur du courriel.
     */
    public final String[] getExpediteurs() {
        return expediteurs;
    }

    /**
     * @param valeur Expéditeur du courriel.
     */
    public final void setExpediteurs(String[] valeur) {
        this.expediteurs = valeur;
    }

    /**
     * @return Message du courriel.
     */
    public final String[] getMessages() {
        return messages;
    }

    /**
     * @param valeur Message du courriel.
     */
    public final void setMessages(String[] valeur) {
        this.messages = valeur;
    }

    /**
     * @return Objet du courriel.
     */
    public final String[] getObjets() {
        return objets;
    }

    /**
     * @param valeur Objet du courriel.
     */
    public final void setObjets(String[] valeur) {
        this.objets = valeur;
    }

    public final Long getTailleMaxAutorisee() {
        return tailleMaxAutorisee;
    }

    public final void setTailleMaxAutorisee(final Long valeur) {
        this.tailleMaxAutorisee = valeur;
    }
}
