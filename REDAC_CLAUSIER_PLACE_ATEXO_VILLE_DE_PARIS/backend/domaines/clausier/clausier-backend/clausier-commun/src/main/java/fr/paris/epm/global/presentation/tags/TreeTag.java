package fr.paris.epm.global.presentation.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;

/**
 * Bean direction / service.
 * Created by nty on 21/02/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public class TreeTag extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String root;

    @Override
    public int doStartTag() throws JspException {
        RecursiveNode node = (RecursiveNode) pageContext.getAttribute(root);
        if (node == null)
            node = (RecursiveNode) pageContext.getRequest().getAttribute(root);
        if (node == null)
            node = (RecursiveNode) pageContext.getSession().getAttribute(root);

        try {
            pageContext.getOut().println("<div id=\"" + id + "\">");
            pageContext.getOut().println("<ul>");
            parseTreeNode(node);
            pageContext.getOut().println("</ul>");
            pageContext.getOut().println("</div>");
        } catch (IOException ex) {
            throw new JspException(ex);
        }

        return SKIP_BODY;
    }

    private void parseTreeNode(RecursiveNode node) throws IOException {

        pageContext.getOut().println("<li id=\"node" + node.getId() + "\" " +
                "idNode=\"" + node.getId() + "\" " +
                "nomNode=\"" + node.getName() + "\">");
        pageContext.getOut().println(node.getName());

        List<RecursiveNode> childs = node.getListChilds();
        if (childs != null && childs.size() > 0) {
            pageContext.getOut().println("<ul>");

            for (RecursiveNode child : childs)
                parseTreeNode(child);

            pageContext.getOut().println("</ul>");
        }

        pageContext.getOut().println("</li>");
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

}
