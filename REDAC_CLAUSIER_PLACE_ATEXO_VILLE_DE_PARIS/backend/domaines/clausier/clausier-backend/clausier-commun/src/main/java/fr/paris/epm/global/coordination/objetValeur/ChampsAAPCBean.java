package fr.paris.epm.global.coordination.objetValeur;

import java.io.Serializable;

/**
 * Bean contenant tous les champs AAPC
 * @author Marouane GAZANAYI
 * @version $Revision$, $Date$, $Author$
 */
public class ChampsAAPCBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Code NUTS
     */
    private String codeNuts;
    /**
     * Autre informations
     */
    private String autreInfos;
    /**
     * Autre conditions
     */
    private String autreConditions;
    /**
     * Cautionnement et garanties exigés
     */
    private String cautionnementGaranties;
    /**
     * Modalités essentielles de financement et de paiement
     */
    private String modalitesFinancement;
    /**
     * Situation juridique - références requises
     */
    private String situationJuridique;
    /**
     * Capacité économique et financière - références requises
     */
    private String capaciteEco;
    /**
     * Référence professionnelle et capacité technique - références requises
     */
    private String refPro;
    /**
     * Renseignements complémentaires - Procédures Adaptée Ouvert
     */
    private String renseignementPAO;
    /**
     * Renseignements complémentaires - Procédures Adaptée Restreint
     */
    private String renseignementPAR;
    /**
     * Renseignements complémentaires - Autre
     */
    private String renseignementAutre;
    /**
     * Nom de l'organisme recours
     */
    private String nomOrganisme;
    /**
     * Adresse recours
     */
    private String adresseRecours;
    /**
     * Code Postal recours
     */
    private String cpRecours;
    /**
     * Ville recours
     */
    private String villeRecours;
    /**
     * Téléphone recours
     */
    private String telRecours;
    /**
     * Télécopieur recours
     */
    private String faxRecours;
    /**
     * Courrier électronique recours
     */
    private String emailRecours;

    /**
     * Champ sans règle de gestion "Introduction du recours"
     */
    private String introductionRecoursPrecisions;
    
    /**
     * @return the codeNuts
     */
    public final String getCodeNuts() {
        return codeNuts;
    }

    /**
     * @param codeNuts the codeNuts to set
     */
    public final void setCodeNuts(final String valeur) {
        this.codeNuts = valeur;
    }

    /**
     * @return the autreInfos
     */
    public final String getAutreInfos() {
        return autreInfos;
    }

    /**
     * @param autreInfos the autreInfos to set
     */
    public final void setAutreInfos(final String valeur) {
        this.autreInfos = valeur;
    }

    /**
     * @return the autreConditions
     */
    public final String getAutreConditions() {
        return autreConditions;
    }

    /**
     * @param autreConditions the autreConditions to set
     */
    public final void setAutreConditions(final String valeur) {
        this.autreConditions = valeur;
    }

    /**
     * @return the cautionnementGaranties
     */
    public final String getCautionnementGaranties() {
        return cautionnementGaranties;
    }

    /**
     * @param cautionnementGaranties the cautionnementGaranties to set
     */
    public final void setCautionnementGaranties(final String valeur) {
        this.cautionnementGaranties = valeur;
    }

    /**
     * @return the modalitesFinancement
     */
    public final String getModalitesFinancement() {
        return modalitesFinancement;
    }

    /**
     * @param modalitesFinancement the modalitesFinancement to set
     */
    public final void setModalitesFinancement(final String valeur) {
        this.modalitesFinancement = valeur;
    }

    /**
     * @return the situationJuridique
     */
    public final String getSituationJuridique() {
        return situationJuridique;
    }

    /**
     * @param situationJuridique the situationJuridique to set
     */
    public final void setSituationJuridique(final String valeur) {
        this.situationJuridique = valeur;
    }

    /**
     * @return the capaciteEco
     */
    public final String getCapaciteEco() {
        return capaciteEco;
    }

    /**
     * @param capaciteEco the capaciteEco to set
     */
    public final void setCapaciteEco(final String valeur) {
        this.capaciteEco = valeur;
    }

    /**
     * @return the refPro
     */
    public final String getRefPro() {
        return refPro;
    }

    /**
     * @param refPro the refPro to set
     */
    public final void setRefPro(final String valeur) {
        this.refPro = valeur;
    }

    /**
     * @return the renseignementPAO
     */
    public final String getRenseignementPAO() {
        return renseignementPAO;
    }

    /**
     * @param renseignementPAO the renseignementPAO to set
     */
    public final void setRenseignementPAO(final String valeur) {
        this.renseignementPAO = valeur;
    }

    /**
     * @return the renseignementPAR
     */
    public final String getRenseignementPAR() {
        return renseignementPAR;
    }

    /**
     * @param renseignementPAR the renseignementPAR to set
     */
    public final void setRenseignementPAR(final String valeur) {
        this.renseignementPAR = valeur;
    }

    /**
     * @return the renseignementAutre
     */
    public final String getRenseignementAutre() {
        return renseignementAutre;
    }

    /**
     * @param renseignementAutre the renseignementAutre to set
     */
    public final void setRenseignementAutre(final String valeur) {
        this.renseignementAutre = valeur;
    }

    /**
     * @return the nomOrganisme
     */
    public final String getNomOrganisme() {
        return nomOrganisme;
    }

    /**
     * @param nomOrganisme the nomOrganisme to set
     */
    public final void setNomOrganisme(final String valeur) {
        this.nomOrganisme = valeur;
    }

    /**
     * @return the adresseRecours
     */
    public final String getAdresseRecours() {
        return adresseRecours;
    }

    /**
     * @param adresseRecours the adresseRecours to set
     */
    public final void setAdresseRecours(final String valeur) {
        this.adresseRecours = valeur;
    }

    /**
     * @return the cpRecours
     */
    public final String getCpRecours() {
        return cpRecours;
    }

    /**
     * @param cpRecours the cpRecours to set
     */
    public final void setCpRecours(final String valeur) {
        this.cpRecours = valeur;
    }

    /**
     * @return the villeRecours
     */
    public final String getVilleRecours() {
        return villeRecours;
    }

    /**
     * @param villeRecours the villeRecours to set
     */
    public final void setVilleRecours(final String valeur) {
        this.villeRecours = valeur;
    }

    /**
     * @return the telRecours
     */
    public final String getTelRecours() {
        return telRecours;
    }

    /**
     * @param telRecours the telRecours to set
     */
    public final void setTelRecours(final String valeur) {
        this.telRecours = valeur;
    }

    /**
     * @return the faxRecours
     */
    public final String getFaxRecours() {
        return faxRecours;
    }

    /**
     * @param faxRecours the faxRecours to set
     */
    public final void setFaxRecours(final String valeur) {
        this.faxRecours = valeur;
    }

    /**
     * @return the emailRecours
     */
    public final String getEmailRecours() {
        return emailRecours;
    }

    /**
     * @param emailRecours the emailRecours to set
     */
    public final void setEmailRecours(final String valeur) {
        this.emailRecours = valeur;
    }

    public String getIntroductionRecoursPrecisions() {
        return introductionRecoursPrecisions;
    }

    public void setIntroductionRecoursPrecisions(String introductionRecoursPrecisions) {
        this.introductionRecoursPrecisions = introductionRecoursPrecisions;
    }
}
