/**
 * $Id$
 */
package fr.paris.epm.global.commun.doc;

/**
 * Informations sur le document.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class DocInfo {

    /**
     * Type mime du document.
     */
    private String typeMime;

    /**
     * Taille du document. Null si non connue.
     */
    private Integer taille;

    /**
     * Nom du document.
     */
    private String nom;

    /**
     * @return nom du document
     */
    public final String getNom() {
        return nom;
    }

    /**
     * @param valeur nom du document
     */
    public final void setNom(final String valeur) {
        this.nom = valeur;
    }

    /**
     * @return taille du document
     */
    public final Integer getTaille() {
        return taille;
    }

    /**
     * @param valeur taille du document
     */
    public final void setTaille(final Integer valeur) {
        this.taille = valeur;
    }

    /**
     * @return type mime du document
     */
    public final String getTypeMime() {
        return typeMime;
    }

    /**
     * @param valeur type mime du document
     */
    public final void setTypeMime(final String valeur) {
        this.typeMime = valeur;
    }
}
