package fr.paris.epm.global.coordination;

import java.util.List;

/**
 * Bean générique pour l'affichage d'une liste de résultats d'une recherche dans un tableau paginé
 * Created by qba on 06/04/16.
 */
public class ResultatRecherchePagineBean<T> {

    private List<T> resultats;
    private Long nbResultats;
    private int nbPages;

    public List<T> getResultats() {
        return resultats;
    }

    public void setResultats(List<T> resultats) {
        this.resultats = resultats;
    }

    public Long getNbResultats() {
        return nbResultats;
    }

    public void setNbResultats(Long nbResultats) {
        this.nbResultats = nbResultats;
    }

    public int getNbPages() {
        return nbPages;
    }

    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }

}