package fr.paris.epm.global.coordination.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TypeField {

    STRING(0x01, "Alphanumérique"),

    DATE(0x02, "Date"),

    INTEGER(0x04, "Nombre entier"),

    DOUBLE(0x08, "Nombre décimal"),

    LIST(0x10, "Liste de sélection");

    private int id;

    private String name;

    private TypeField(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isString() {
        return this == STRING;
    }

    public boolean isDate() {
        return this == DATE;
    }

    public boolean isInteger() {
        return this == INTEGER;
    }

    public boolean isDouble() {
        return this == DOUBLE;
    }

    public boolean isList() {
        return this == LIST;
    }

    public static TypeField findById(int id) {
        if (STRING.id == id)
            return STRING;
        else if (DATE.id == id)
            return DATE;
        else if (INTEGER.id == id)
            return INTEGER;
        else if (DOUBLE.id == id)
            return DOUBLE;
        else if (LIST.id == id)
            return LIST;
        return null;
    }

    public static List<Directory> likeDirectory() {
        return new ArrayList<Directory>() {{
            add(new Directory(STRING.id, STRING.name));
            add(new Directory(DATE.id, DATE.name));
            add(new Directory(INTEGER.id, INTEGER.name));
            add(new Directory(DOUBLE.id, DOUBLE.name));
            add(new Directory(LIST.id, LIST.name));
        }};
    }

    public static Map<Integer, String> likeHashMap() {
        return new HashMap<Integer, String>() {{
            put(STRING.id, STRING.name);
            put(DATE.id, DATE.name);
            put(INTEGER.id, INTEGER.name);
            put(DOUBLE.id, DOUBLE.name);
            put(LIST.id, LIST.name);
        }};
    }

}
