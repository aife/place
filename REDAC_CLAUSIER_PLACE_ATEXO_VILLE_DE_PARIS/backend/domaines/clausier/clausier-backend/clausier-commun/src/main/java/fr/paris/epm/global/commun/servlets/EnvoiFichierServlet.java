/**
 * $Id$
 */
package fr.paris.epm.global.commun.servlets;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.commun.FileTmp;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * chargement du fichiers par la clef passer en parramétre.
 * 
 * @author EDDOUGHMI younes
 * @version $Revision$, $Date$, $Author$
 */
public final class EnvoiFichierServlet extends HttpServlet {

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EnvoiFichierServlet.class);

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param request
     *            {@HttpServletRequest}
     * @param response
     *            {@HttpServletResponse}
     */
    public void doGet(final HttpServletRequest request,
            final HttpServletResponse response) throws ServletException {

        try {
            HttpSession session = request.getSession();

            Map mapFichier = (Map) session
                    .getAttribute(ConstantesGlobales.S_DOCUMENT_LISTE_FICHIERS);

            Long cles = new Long(request.getParameter(ConstantesGlobales.P_CLEF));
            String nomFichier = null;
            if (mapFichier == null || mapFichier.isEmpty() || cles == null) {
                throw new IllegalArgumentException("Aucun fichier en session avec l'id " + cles);
            }
           
            File file = (File) mapFichier.get(cles); 
            if (mapFichier.get(cles) instanceof FileTmp) {
               FileTmp tmp = (FileTmp) mapFichier.get(cles);
               nomFichier = tmp.getNomFichier();
            } else if (mapFichier.get(cles) instanceof File && nomFichier != null) {
                // Le nom du fichier est celui sur disque
                nomFichier = file.getName();
            }
            FileInputStream inputStream = new FileInputStream(file);
            
            // Outils
            String typeContenu = getTypeContenu(nomFichier);
            // Entêtes
            response.setContentType(typeContenu);

            response.setHeader("Content-Disposition", "attachment; filename=\""
                    + nomFichier + "\"");

            OutputStream fluxServlet = response.getOutputStream();
            IOUtils.copyLarge(inputStream, fluxServlet);
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(fluxServlet);
            // Flux de sortie

        } catch (IOException ioe) {
            LOG.error(ioe.getMessage(), ioe.fillInStackTrace());
            return;
        }
    }

    /**
     * @param fichier
     * @return
     */
    private String getTypeContenu(String strFichier) {
        if ("pdf".equals(getExtension(strFichier).toLowerCase())) {
            return "application/pdf";
        } else if ("csv".equals(getExtension(strFichier).toLowerCase())) {
            return "application/vnd.ms-excel";
        } else if ("xls".equals(getExtension(strFichier).toLowerCase())) {
            return "application/vnd.ms-excel";
        } else if ("doc".equals(getExtension(strFichier).toLowerCase())) {
            return "application/msword";
        } else if ("pps".equals(getExtension(strFichier).toLowerCase())) {
            return "application/vnd.ms-powerpoint";
        } else if ("zip".equals(getExtension(strFichier).toLowerCase())) {
            return "multipart/zip";
        }
        return "application/octet-stream";
    }

    private String getExtension(String strFichier) {
        if (strFichier.lastIndexOf(".") != -1) {
            if (strFichier.lastIndexOf(".") + 1 <= strFichier.length()) {
                LOG.info(strFichier.substring(strFichier.lastIndexOf(".") + 1,
                        strFichier.length()));
                return strFichier.substring(strFichier.lastIndexOf(".") + 1,
                        strFichier.length());
            }
        }
        return "";
    }

}
