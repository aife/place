/**
 * 
 */
package fr.paris.epm.global.commun.tags;

import fr.paris.epm.global.commun.Util;
import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;


/**
 * Tag permettant de tronquer un mot.
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class IntituleElementTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * chaine de remplacement des sauts de lignes
     */
    private static final String SAUT_DE_LIGNE = "\r\n";
    
    private static final String ESPACE = " ";
    

    
    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(IntituleElementTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           null);
            if (texte == null) {
                return Tag.SKIP_BODY;
            }

            String valeur = (String) texte;
            valeur = valeur.replaceAll(SAUT_DE_LIGNE, ESPACE);
            valeur = Util.echapperQuotes(valeur);
            pageContext.getOut()
                    .print(valeur);
            
        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        name = valeur;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

}
