/**
 * 
 */
package fr.paris.epm.global.commun.tags;

import org.apache.struts.taglib.TagUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag permettant de remplacerla balise br par un retoure à la ligne .
 * @author Tarik Ramli
 * @version $Revision: $, $Date: $, $Author: $
 */
public class RetoureLigneTag extends TagSupport {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * propriété accessible de l'objet.
     */
    private String property;

    /**
     * nom de l'objet.
     */
    private String name;

    /**
     * chaine remplacement.
     */
    private static final char[] remplacement = { '\n' };

    /**
     * premier chaine a remplcé.
     */
    private static final String param1 = "<br>";

    /**
     * deuxiéme chaine a remplcé.
     */
    private static final String param2 = "<br/>";

    /**
     * tag provenant de struts.
     */
    private TagUtils tagUtils = TagUtils.getInstance();

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(RetoureLigneTag.class);

    /**
     * @return int
     * @exception JspException JspExcption
     */
    public int doStartTag() throws JspException {
        try {
            Object texte = tagUtils.lookup(pageContext, name, property,
                                           null);
            if (texte == null) {
                return Tag.SKIP_BODY;
            }
            String remplaceTmp = new String(remplacement);

            String valeur = (String) texte;
            valeur = valeur.replaceAll(param1, remplaceTmp);
            valeur = valeur.replaceAll(param2, remplaceTmp);

            pageContext.getOut().print(valeur);

        } catch (IOException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new JspException("Erreur d'E/S", e);
        }
        return Tag.SKIP_BODY;
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagSupport#release()
     */
    public void release() {
        property = null;
        name = null;
    }

    /**
     * @param valeur nom de l'objet.
     */
    public final void setName(final String valeur) {
        name = valeur;
    }

    /**
     * @param valeur propriété accessible de l'objet.
     */
    public final void setProperty(final String valeur) {
        property = valeur;
    }

}
