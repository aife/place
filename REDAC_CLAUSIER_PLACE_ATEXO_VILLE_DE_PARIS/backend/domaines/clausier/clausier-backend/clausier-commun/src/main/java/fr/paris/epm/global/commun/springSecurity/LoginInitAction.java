package fr.paris.epm.global.commun.springSecurity;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.global.commun.SessionManagerGlobal;
import fr.paris.epm.global.presentation.AbstractAction;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Declenchee lors de l'acces initial a la page de login.
 * @author Rémi Villé
 * @version @version $Revision$, $Date$, $Author$
 */
public class LoginInitAction extends AbstractAction {

    public final ActionForward execute(final SessionManagerGlobal sessionManager, final ActionMapping mapping,
            final ActionForm form, final HttpServletRequest request,
            final HttpServletResponse response) throws SessionExpireeException {

        request.setAttribute("login", request.getParameter("login"));
        request.setAttribute("token", request.getParameter("token"));

        return mapping.findForward("success");
    }
}
