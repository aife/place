package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nty on 14/04/17.
 */
public interface MapperFacade<T extends Bean, E extends EpmTObject> {

    public T map(E epmTObject, EpmTRefOrganisme epmTRefOrganisme);

    public E map(T bean, EpmTRefOrganisme epmTRefOrganisme);

    default List<T> map(Collection<E> epmTObjects, EpmTRefOrganisme epmTRefOrganisme) {
        return epmTObjects.stream()
                .map(e -> this.map(e, epmTRefOrganisme))
                .collect(Collectors.toList());
    }

}
