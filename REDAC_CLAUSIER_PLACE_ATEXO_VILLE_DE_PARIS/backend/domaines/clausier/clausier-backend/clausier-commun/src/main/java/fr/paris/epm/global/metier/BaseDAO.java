package fr.paris.epm.global.metier;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Interface CRUD générique pour tous les Gestionnaires de Ressources Métier.
 * Created by nty on 29/03/18.
 * @author Nikolay Tyurin
 * @author NTY
 */
public interface BaseDAO {

    static final Logger logger = LoggerFactory.getLogger(BaseDAO.class);

    public Session getCurrentSession();
/*
    public default <T> void create(T entity) {
        getCurrentSession().persist(entity);
        getCurrentSession().flush();
    }

    public default <T> void update(T entity) {
        //getCurrentSession().update(entity);
        getCurrentSession().merge(entity);
    }

    public default <T, K extends Serializable> K save(T entity) {
        //return (K) getCurrentSession().save(entity);
        return getCurrentSession().merge(entity);
    }
*/
    public default <T> void persist(T entity) {
        getCurrentSession().persist(entity);
        getCurrentSession().flush();
    }

    public default <T> T merge(T entity) {
        return (T) getCurrentSession().merge(entity);
    }

    public default <T> void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    public default <T, K extends Serializable> T find(Class<T> klass, K id) {
        return (T) getCurrentSession().get(klass, id);
    }

    public default <T, K extends Serializable> T load(Class<T> klass, K id) {
        return (T) getCurrentSession().load(klass, id);
    }

    public default <T> List<T> findAll(Class<T> klass) {
        return findByQuery("from " + klass.getName());
    }

    public default <T> List<T> findByQuery(String queryString) {
        return findByQueryWithParams(queryString, Collections.emptyMap());
    }

    public default <T> List<T> findByQueryWithParams(String queryString, Map<String, Object> params) {
        //TypedQuery<T> query = getCurrentSession().createQuery(queryString, getEntityClass());
        Query query = getCurrentSession().createQuery(queryString);
        query.setProperties(params);
        return (List<T>) query.list();
    }

    public default <T> T findUniqueByQuery(String queryString) {
        return findUniqueByQueryWithParams(queryString, Collections.emptyMap());
    }

    public default <T> T findUniqueByQueryWithParams(String queryString, Map<String, Object> params) {
        //TypedQuery<T> query = getCurrentSession().createQuery(queryString, getEntityClass());
        Query query = getCurrentSession().createQuery(queryString);
        query.setProperties(params);
        return (T) query.uniqueResult();
    }

    public default void executeUpdate(String queryString) {
        Query query = getCurrentSession().createQuery(queryString);
        query.executeUpdate();
    }

    public default int executeSqlUpdate(String sqlQuery) {
        SQLQuery query = getCurrentSession().createSQLQuery(sqlQuery);
        return query.executeUpdate();
    }

}