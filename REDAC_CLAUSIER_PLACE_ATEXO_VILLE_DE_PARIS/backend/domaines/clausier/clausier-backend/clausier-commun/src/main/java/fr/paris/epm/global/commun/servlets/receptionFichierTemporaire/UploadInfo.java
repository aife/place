package fr.paris.epm.global.commun.servlets.receptionFichierTemporaire;

/**
 * suivi de l'upload des fichiers.
 * Leon BARSAMIAN
 */
public class UploadInfo
{
    private long totalSize = 0;
    private long bytesRead = 0;
    private long elapsedTime = 0;
    private String status = "done";
    private int fileIndex = 0;
    
    /**
     * Identifiant du fichier.
     */
    private String identifiant;
    /**
     * Nom du fichier
     */
    private String nomDuFichier;
    /**
     * Url du fichier pour téléchargement.
     */
    private String url;


    public UploadInfo(int fileIndex, long totalSize, long bytesRead, long elapsedTime,
            String status, final String valeurIdentifiant, final String valeurNomDuFichier, final String valeurUrl) {
        this.fileIndex = fileIndex;
        this.totalSize = totalSize;
        this.bytesRead = bytesRead;
        this.elapsedTime = elapsedTime;
        this.status = status;
        this.identifiant = valeurIdentifiant;
        this.nomDuFichier = valeurNomDuFichier;
        this.url = valeurUrl;
    }

    /**
     * 
     */
    public UploadInfo() {
        // TODO Auto-generated constructor stub
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public long getTotalSize()
    {
        return totalSize;
    }

    public void setTotalSize(long totalSize)
    {
        this.totalSize = totalSize;
    }

    public long getBytesRead()
    {
        return bytesRead;
    }

    public void setBytesRead(long bytesRead)
    {
        this.bytesRead = bytesRead;
    }

    public long getElapsedTime()
    {
        return elapsedTime;
    }

    public void setElapsedTime(long elapsedTime)
    {
        this.elapsedTime = elapsedTime;
    }

    public boolean isInProgress()
    {
        return "progress".equals(status) || "start".equals(status);
    }
    
    public boolean isEnErreur()
    {
        return "erreur".equals(status);
    }

    public int getFileIndex()
    {
        return fileIndex;
    }

    public void setFileIndex(int fileIndex)
    {
        this.fileIndex = fileIndex;
    }

    public final String getIdentifiant() {
        return identifiant;
    }

    public final void setIdentifiant(final String valeur) {
        this.identifiant = valeur;
    }

    public final String getNomDuFichier() {
        return nomDuFichier;
    }

    public final void setNomDuFichier(String valeur) {
        this.nomDuFichier = valeur;
    }

    public final String getUrl() {
        return url;
    }

    public final void setUrl(String valeur) {
        this.url = valeur;
    }
}
