package fr.paris.epm.global.coordination.objetValeur;

import java.io.Serializable;

/**
 * @author Ibrahim LAHLOU
 * @version $Revision: $, $Date:, $Author: $
 */
public class GestionActualite implements Serializable {
    /**
     * Serialisation.
     */
    private static final long serialVersionUID = -2329490050134981967L;
    
    private static final String BALISE_LIEN = "<a href=\"";
    private static final String BALISE_LIEN_MODIFIE = "<a target=\"_blank\" href=\"";
    
    /**
     * l'identifiant de l'actualité.
     */
    private int id;
    /**
     * la date de l'actualité.
     */
    private String date;
    /**
     * le contenu de l'actualité.
     */
    private String contenu;
    
    /**
     * ordre d'affichage de l'actualité.
     */
    private int ordre;

    /**
     * @return contenu
     */
    public final String getContenu() {
        return contenu;
    }

    /**
     * Parse le contenu pour ajouter l'attribut "target="_blank" aux liens html présent dans le contenu.
     * @param valeur contenu.
     */
    public final void setContenu(final String valeur) {
        this.contenu = valeur.replaceAll(BALISE_LIEN, BALISE_LIEN_MODIFIE);
    }

    /**
     * @return date
     */
    public final String getDate() {
        return date;
    }

    /**
     * @param valeur date
     */
    public final void setDate(final String valeur) {
        this.date = valeur;
    }

    /**
     * @return id
     */
    public final int getId() {
        return id;
    }

    /**
     * @param valeur id
     */
    public final void setId(final int valeur) {
        this.id = valeur;
    }

    /**
     * @return the ordre
     */
    public final int getOrdre() {
        return ordre;
    }

    /**
     * @param ordre the ordre to set
     */
    public final void setOrdre(final int valeur) {
        this.ordre = valeur;
    }
}
