package fr.paris.epm.global.commun;

import fr.paris.epm.global.commons.exception.SessionExpireeException;
import fr.paris.epm.noyau.metier.Critere;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
 * Cestte classe gere la vie de l'ensemble des valeurs transitant dans la
 * session.
 * 
 * @author Regis Menet
 */
public class SessionManagerGlobal {
    public static final String SESSION_MANAGER = ConstantesGlobales.SESSION_MANAGER;
    public static final String SESSION_MANAGER_FICHIERS = ConstantesGlobales.SESSION_MANAGER_FICHIERS;
    public static final String SESSION_MANAGER_DOCUMENT = ConstantesGlobales.SESSION_MANAGER_DOCUMENT;
    public static final String SESSION_MANAGER_RECHERCHE_TABLEAU = ConstantesGlobales.SESSION_MANAGER_RECHERCHE_TABLEAU;
    public static final String SESSION_VALIDE = ConstantesGlobales.SESSION_VALIDE;
    
    /**
     * Nombre de fichier qui devront être généré.
     */
    public static final String SESSION_MANAGER_NOMBRE_FICHIERS = "SESSION_MANAGER_NOMBRE_FICHIERS";
    protected static SessionManagerGlobal instance = null;
    protected List sessionManagerList;

    protected static final Logger LOG = LoggerFactory.getLogger(SessionManagerGlobal.class);

    protected SessionManagerGlobal() {
        sessionManagerList = new ArrayList();
        sessionManagerList.add(SESSION_MANAGER);
        sessionManagerList.add(SESSION_MANAGER_RECHERCHE_TABLEAU);
    }

    public static SessionManagerGlobal getInstance() {
        if (instance == null) {
            instance = new SessionManagerGlobal();
        }
        return instance;
    }

    /**
     * @param session
     * @param cle
     *            cle de l'objet session.
     * @param valeur
     *            valeur de l'objet a mettre dans la session.
     */
    public final void ajouterElement(final HttpSession session, final String cle, final Object valeur) {
        ajouterElement(SESSION_MANAGER, session, cle, valeur);
    }

    protected final void ajouterElement(final String sessionManager,
            final HttpSession session, final String cle, final Object valeur) {
        if (cle == null) {
            LOG.debug("la cle n'existe pas ");
            return;
        }
        if (valeur == null) {
            LOG.debug("la valeur est null ");
            return;
        }
        if (!(valeur instanceof Serializable)) {
            LOG.error("L'objet ayant pour cle " + cle + " n'est pas serialize : " + valeur.getClass().getName());
        }
        List cles = (List) session.getAttribute(sessionManager);
        if (cles == null) {
            cles = new ArrayList();
            session.setAttribute(sessionManager, cles);
        }
        LOG.debug("ajout element Session : " + cle);
        session.setAttribute(cle, valeur);
        if (!cles.contains(cle)) {
            cles.add(cle);
        }
    }

    /**
     * @param session
     * @param facade
     *            nom de la classe qui sera utilisé pour la recherche
     * @param methode
     *            nom de la methode de la classe ci dessus
     * @param critere
     *            critere de recherche
     */
    public void ajouterRechercheTableau(final HttpSession session,
            final String facade, final String methode, final Critere critere) {
        ajouterRechercheTableau(session, new TypeRecherche(facade, methode, -1), critere);
    }

    /**
     * @param facade
     *            nom de la classe qui sera utilisé pour la recherche
     * @param session
     * @param typeRecherche
     * @param critere
     *            critere de recherche
     */
    public void ajouterRechercheTableau(final HttpSession session, TypeRecherche typeRecherche, final Critere critere) {
        List<String> cles = (List<String>) session.getAttribute(SESSION_MANAGER_RECHERCHE_TABLEAU);
        if (cles == null) {
            cles = new ArrayList<String>();
            session.setAttribute(SESSION_MANAGER_RECHERCHE_TABLEAU, cles);
        }
        session.setAttribute(ConstantesGlobales.S_FACADE, typeRecherche.getFacade());
        cles.add(ConstantesGlobales.S_FACADE);
        session.setAttribute(ConstantesGlobales.S_METHODE, typeRecherche.getMethode());
        cles.add(ConstantesGlobales.S_METHODE);
        session.setAttribute(ConstantesGlobales.S_CRITERES, critere);
        cles.add(ConstantesGlobales.S_CRITERES);
        session.setAttribute(ConstantesGlobales.S_TYPE_RECHERCHE, typeRecherche.getTypeRecherche());
        cles.add(ConstantesGlobales.S_TYPE_RECHERCHE);
    }

    /**
     * @param session
     * @param cle
     *            ajout de la form pour pouvoir etre supprimé de la session
     */
    public void ajouterForm(final HttpSession session, final String cle) {
        if (cle == null) {
            LOG.warn("la cle n'existe pas ");
            return;
        }
        List cles = (List) session.getAttribute(SESSION_MANAGER);
        if (cles == null) {
            cles = new ArrayList();
            session.setAttribute(SESSION_MANAGER, cles);
        }
        LOG.debug("ajout form Session : " + cle);
        if (!cles.contains(cle)) {
            cles.add(cle);
        }
    }

    public void nettoyerTous(final HttpSession session, final String saufForm) {
        for (int i = 0; i < sessionManagerList.size(); i++) {
            List<String> cleMap = (List<String>) session
                    .getAttribute((String) sessionManagerList.get(i));
            if (cleMap != null) {
                Iterator<String> it = cleMap.iterator();
                while (it.hasNext()) {
                    String cle = (String) it.next();
                    LOG.debug("suppression element Session : " + cle);
                    if (!cle.equals(saufForm) && !cle.equals(ConstantesGlobales.RETOUR_RECHERCHE) && !cle.equals(ConstantesGlobales.LIST_ID_DOCUMENTS)) {
                        session.removeAttribute(cle);
                    }
                }
                cleMap.clear();
            }
        }
    }
    
    /**
     * Suppression des elements en session
     * 
     * @param session
     */
    public void nettoyerTous(final HttpSession session) {
        nettoyerTous(session, "");
    }

    public void nettoyerTousAvecCritereRecherche(final HttpSession session, final String saufForm){
        nettoyerTous(session, saufForm);
        session.removeAttribute(ConstantesGlobales.RETOUR_RECHERCHE);
    }
    
    /**
     * Cette methode permet de sauvegarder les fichiers generé par
     * l'application. ils seront supprimé au moment de l'appel de la méthode
     * nettoyerFichier.
     * 
     * @param session
     * @param fichier
     *            fichier à ajouter
     */
    public void ajouterFichier(final HttpSession session, final File fichier) {
        List fichiers = (List) session.getAttribute(SESSION_MANAGER_FICHIERS);
        if (fichiers == null) {
            LOG.debug("Liste en session des fichiers null : creation");
            fichiers = new ArrayList();
            session.setAttribute(SESSION_MANAGER_FICHIERS, fichiers);
            return;//sinon un NPE est garantie dans l'instruction suivante !!!!!!!!
        }
        LOG.debug("ajout fichier Session : " + fichier.getName());//
        fichiers.add(fichier);
        LOG.debug("taille de la liste de fichier = " + fichiers.size());
    }
    
    /**
     * Cette methode permet de sauvegarder les fichiers generé par
     * l'application. ils seront supprimé au moment de l'appel de la méthode
     * nettoyerFichier.
     * 
     * @param session
     * @param fichier
     *            fichier à ajouter
     */
    public void ajouterFichierTmp(final HttpSession session, final FileTmp fileTmp, boolean generer) {
        
        if (fileTmp == null) {
            throw new IllegalArgumentException("Le fichier a ajouter en session est null.");
        }
        
        ajouterFichier(session, fileTmp);
        Map listFichier = (Map) session.getAttribute(ConstantesGlobales.S_DOCUMENT_LISTE_FICHIERS);
        if (listFichier == null) {
            LOG.debug("Map fichiers session null : creation.");
            listFichier = new HashMap();
        }
        Long cleMap = null;
        do {
            cleMap = new Long(System.nanoTime());
        } while(listFichier.containsKey(cleMap));
        LOG.debug("Ajoute de " + cleMap + " => " + fileTmp.getNomFichier() + " - " + fileTmp.getAbsolutePath());
        listFichier.put(cleMap, fileTmp);
        fileTmp.setUrl("envoiFichier?clef=" + cleMap.longValue());
        ajouterFichier(session, ConstantesGlobales.S_DOCUMENT_LISTE_FICHIERS, listFichier);
        LOG.debug("Map fichiers de taille " + listFichier.size() + " ejoute en session.");
        Integer nombreFichierAttenduGeneration = decrementerNombreDeFichierAGenerer(session);
        
        if (generer && nombreFichierAttenduGeneration == 0) {
            ajouterFichier(session, ConstantesGlobales.S_DOCUMENT_STATUT, ConstantesGlobales.S_DOCUMENT_GENERER);
        }
    }

    /**
     * Ajout des variables utilisé pour la gestion des fichiers generés
     * 
     * @param session
     * @param cle
     * @param valeur
     */
    public final void ajouterFichier(final HttpSession session,
            final String cle, final Object valeur) {
        if (cle == null) {
            LOG.warn("la cle n'existe pas ");
            return;
        }
        
        if (session.getAttribute(SESSION_MANAGER_NOMBRE_FICHIERS) != null && valeur instanceof String) {
            String chaineValeur = (String) valeur;
            if (cle.equals(ConstantesGlobales.S_DOCUMENT_STATUT) && chaineValeur.equals(ConstantesGlobales.ERREUR)) {
        		Integer nombreDocmentRestant = decrementerNombreDeFichierAGenerer(session);
        		if (nombreDocmentRestant == 0) {
        			ajouterFichier(session, ConstantesGlobales.S_DOCUMENT_STATUT, ConstantesGlobales.S_DOCUMENT_GENERER);
        		}
        		return;
        	}
        }
        List documents = (List) session.getAttribute(SESSION_MANAGER_DOCUMENT);
        LOG.debug("ajout element Session : " + cle);
        session.setAttribute(cle, valeur);
        if (documents == null) {
            documents = new ArrayList();
            session.setAttribute(SESSION_MANAGER_DOCUMENT, documents);
        }
        if (!documents.contains(cle)) {
            documents.add(cle);
        }
    }
    
	/**
	 * Gestion du nombre de fichier à générer (à utiliser si plus de 1 fichier à
	 * générer).
	 * 
	 * @param session
	 *            la session
	 * @param nombreFichierAttendu
	 *            le nombre de fichier attendu pour la génération
	 */
	public final void genererationFichierMultiple(final HttpSession session,
			final Integer nombreFichierAttendu) {

		ajouterFichier(session, SESSION_MANAGER_NOMBRE_FICHIERS,
				nombreFichierAttendu);
		LOG.debug("nombre de fichier attendu pour la généreration : " + nombreFichierAttendu);

	}
    
    synchronized private Integer decrementerNombreDeFichierAGenerer(final HttpSession session) {
    	Integer nombreFichierAttenduGeneration = 0;
    	if (session.getAttribute(SESSION_MANAGER_NOMBRE_FICHIERS) != null) {
        	nombreFichierAttenduGeneration = (Integer) session.getAttribute(SESSION_MANAGER_NOMBRE_FICHIERS);
        	genererationFichierMultiple(session, --nombreFichierAttenduGeneration);
        	LOG.debug("nombre de fichier restant pour la généreration : " + nombreFichierAttenduGeneration);
        }
    	return nombreFichierAttenduGeneration;
    }

    /**
     * Methode gerant la suppression des fichiers temporaires.
     * 
     * @param session
     */
    public final void nettoyerFichier(final HttpSession session) {
        List fichiers = (List) session.getAttribute(SESSION_MANAGER_FICHIERS);
        session.removeAttribute(ConstantesGlobales.S_DOCUMENT_STATUT);
        if (fichiers != null) {
            int i = 0;
            while (i < fichiers.size()) {
                File fichier = (File) fichiers.get(i);
                if (fichier.exists()) {
                    boolean resultat = fichier.delete();
                    if (resultat) {
                        LOG.debug("suppression fichier Session : "
                                + ((File) fichiers.get(i)).getName());
                        fichiers.remove(i);
                    } else {
                        LOG.error("impossible de supprimer fichier Session : "
                                + ((File) fichiers.get(i)).getName());
                        i++;
                    }
                } else {
                    fichiers.remove(i);
                }
            }
            List cles = (List) session.getAttribute(SESSION_MANAGER_DOCUMENT);
            if (cles != null) {
                for (int j = 0; j < cles.size(); j++) {
                    session.removeAttribute((String) cles.get(j));
                    LOG.debug("suppression recherche tableau Session : "
                            + (String) cles.get(j));
                }
                session.removeAttribute(SESSION_MANAGER_DOCUMENT);
            }
        }
    }

    /**
     * Methode gerant la suppression des variables utilisé dans le cadre des
     * recherches
     * 
     * @param session
     */
    public final void nettoyerRechercheTableau(final HttpSession session) {
        List cles = (List) session
                .getAttribute(SESSION_MANAGER_RECHERCHE_TABLEAU);
        if (cles != null) {
            for (int i = 0; i < cles.size(); i++) {
                session.removeAttribute((String) cles.get(i));
                LOG.debug("suppression recherche tableau Session : "
                        + (String) cles.get(i));
            }
            session.removeAttribute(SESSION_MANAGER_RECHERCHE_TABLEAU);
        }
    }

    /**
     * @param session
     * @return affiche les variables et fichiers present en session.
     */
    public final String toString(final HttpSession session) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < sessionManagerList.size(); i++) {
            List cles = (List) session.getAttribute((String) sessionManagerList.get(i));
            if (cles != null) {
                sb.append("session [");
                for (int j = 0; j < cles.size(); j++) {
                    sb.append(cles.get(j).toString()).append(", ");
                }
                sb.append("]");
                sb.append("]\n");
            }
        }
        List fichiers = (List) session.getAttribute(SESSION_MANAGER_FICHIERS);
        if (fichiers != null) {
            sb.append("session document [");
            for (int i = 0; i < fichiers.size(); i++) {
                sb.append(fichiers.get(i).toString()).append(", ");
            }
            sb.append("]\n");
        }
        return sb.toString();
    }

    /**
     * @param session
     * @param valeur
     *            true indique que la session est valide
     */
    public final void setSessionValide(final HttpSession session,
            final boolean valeur) {
        session.setAttribute(SESSION_VALIDE, new Boolean(valeur));
    }

    /**
     * @param request
     * @return renvoie l'objet {@link HttpSession} si session valide sinon
     *         declanche une erreur
     * @throws SessionExpireeException
     */
    public final HttpSession getSession(final HttpServletRequest request)
            throws SessionExpireeException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            throw new SessionExpireeException();
        }
        Boolean sessionValide = (Boolean) session.getAttribute(SESSION_VALIDE);
        if (sessionValide == null) {
            throw new SessionExpireeException();
        }
        return session;
    }
}
