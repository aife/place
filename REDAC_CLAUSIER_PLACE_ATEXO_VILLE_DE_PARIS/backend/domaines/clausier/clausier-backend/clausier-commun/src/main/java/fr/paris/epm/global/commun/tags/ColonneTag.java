/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ResourceBundle;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil. 2007) $, $Author: iss $
 */
public class ColonneTag extends TagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    
    private static ResourceBundle resourceBundle;

    private String img;

    private String property;

    private String key;

    private boolean triCroissant = true;

   // protected String localeKey = Globals.LOCALE_KEY;

    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext
                    .getRequest();
            StringBuffer sb = new StringBuffer();

            String intervalle = "10";
            if (pageContext.getAttribute(TableauTag.INTERVALLE_AFFICHER) != null) {
                intervalle = ((String[]) pageContext
                        .getAttribute(TableauTag.INTERVALLE_AFFICHER))[0];
            }
            if (pageContext.getAttribute("intervalle") != null) {
                intervalle = String.valueOf(pageContext.getAttribute("intervalle"));
            }
            if (request.getParameter("intervalle") != null) {
                intervalle = request.getParameter("intervalle");
            }
            String triCroissantParam = request.getParameter("triCroissant");
            String propertyParam = request.getParameter("property");
            triCroissant = ! ("true".equals(triCroissantParam) && property.equals(propertyParam)); //true par défaut puis inverse le paramètre

            String url = (String) pageContext.getAttribute(TableauTag.URL);
            String form = (String) pageContext.getAttribute(TableauTag.FORM);
            if (url == null && form == null) {
                sb.append(img);
            } else if (form != null) {
                sb.append(createFORM());
            } else {
                sb.append(createURL(url, intervalle));
            }
            pageContext.getOut().print(sb.toString());
        } catch (IOException e) {
            throw new JspException("I/O Error", e);
        }
        return SKIP_BODY;
    }

    public void release() {
        img = null;
        property = null;
        triCroissant = true;
    }

    private String createFORM() {
        String lien = "javascript:envoiTableau(3, 0, '" + property + "')";
        return assemble(lien);
    }

    private String createURL(String url, String intervalle) {
        StringBuffer sb = new StringBuffer();
        sb.append(url);
        if (url.indexOf("?") == -1) {
            sb.append("?property=");
        } else {
            sb.append("&property=");
        }
        sb.append(property);
        sb.append("&triCroissant=").append(triCroissant);
        sb.append("&index=0&intervalle=");
        sb.append(intervalle);
        return assemble(sb.toString());
    }

    private String assemble(final String lien) {
        StringBuffer sb = new StringBuffer();
        sb.append("<a href=\"");
        sb.append(lien);
        sb.append("\">");
        if (key != null) {
            if (resourceBundle != null) {
                sb.append(resourceBundle.getString(key));
            }
        }
        sb.append("<img src=\"").append(img).append("\" border=\"0\"/>");
        sb.append("</a>");
        return sb.toString();
    }

    /**
     * 
     * @param valeur propriété de la colonne à trier
     */
    public final void setProperty(final String valeur) {
        this.property = valeur;
    }

    /**
     * 
     * @param valeur true si tri croissant
     */
    public final void setTriCroissant(final boolean valeur) {
        this.triCroissant = valeur;
    }
    
    /**
     * 
     * @param valeur nom du resource bundle.
     */
    public final void setResourceBundle(final String valeur) {
        resourceBundle = ResourceBundle.getBundle(valeur);
    }

    /**
     * 
     * @param valeur image representant l'icone de tri.
     */
    public final void setImg(final String valeur) {
        this.img = valeur;
    }

    /**
     * 
     * @param valeur key du resource bundle.
     */
    public final void setKey(final String valeur) {
        this.key = valeur;
    }
}
