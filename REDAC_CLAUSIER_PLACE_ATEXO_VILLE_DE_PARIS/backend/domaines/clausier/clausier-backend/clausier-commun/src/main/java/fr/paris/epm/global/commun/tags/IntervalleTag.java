/**
 * $Id: UserTag.java 28 2007-07-27 07:27:54Z iss $
 */
package fr.paris.epm.global.commun.tags;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author Regis Menet
 * @version $Revision: 28 $, $Date: 2007-07-27 09:27:54 +0200 (ven., 27 juil.
 *          2007) $, $Author: iss $
 */
public class IntervalleTag extends TagSupport {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;
    
    private String classe;

    public int doStartTag() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest) pageContext
                    .getRequest();
            String url = (String) pageContext.getAttribute(TableauTag.URL);
            String form = (String) pageContext.getAttribute(TableauTag.FORM);
            StringBuffer sb = new StringBuffer();
            String[] array = (String[]) pageContext.getAttribute(TableauTag.INTERVALLE_AFFICHER);
            String intervalle = "10";
            if (array != null) {
                intervalle = array[0];
            }

            String param = "intervalle";
            if (pageContext.getAttribute(param) != null) {
                intervalle = String.valueOf(pageContext.getAttribute(param));
            }
            if (request.getParameter(param) != null) {
                intervalle = request.getParameter(param);
            }else{
                
                if (request.getAttribute(param)!=null){
                    intervalle = (String) request.getAttribute(param);
                }
            }
            if (request.getParameter("classe") != null) {
                classe = request.getParameter("classe");
            }

            if (form != null) {
                sb.append(getSelectFORM(intervalle, form, array, classe));
            } else {
                sb.append(getSelectURL(intervalle, url, array, classe));
            }

            pageContext.getOut().print(sb.toString());
        } catch (IOException e) {
            throw new JspException("I/O Error", e);
        }
        return SKIP_BODY;
    }

    public void release() {
    }

    private String getSelectAssemble(String intervalle, String[] array,
            final String classe, final String script) {
        StringBuffer select = new StringBuffer();
        select.append("<select name=\"nbResults\" id=\"nbResults\" ");
        if (classe != null) {
            select.append("class=\"" + classe + "\"");
        }
        select.append("onChange=\"javascript:");
        select.append("synchronisationAtexoIntervalle(this);");
        select.append(script).append("\">");

        if (array == null) {
            return select.toString();
        }
        for (int i = 0; i < array.length; i++) {
            select.append("<option value=\"").append(array[i]);
            select.append("\"");
            if (array[i].equals(String.valueOf(intervalle))) {
                select.append(" selected ");
            }
            select.append(">");
            select.append(array[i]);
            select.append("</option>");
        }
        select.append("</select>");
        return select.toString();
    }
    
    /**
     * @param intervalle intervalle
     * @param url url
     * @param array options
     * @param classe classe du sélect
     * @return sélect généré pour le cas où on passe par une URL
     */
    private String getSelectURL(String intervalle, String url, String[] array,
            final String classe) {
        String script = "sendUrl('" + url + "', 0)";
        return getSelectAssemble(intervalle, array, classe, script);

    }


    /**
     * @param intervalle intervalle
     * @param form formulaire
     * @param array options
     * @param classe classe du sélect
     * @return sélect généré pour le cas où on passe par un submit
     */
    private String getSelectFORM(String intervalle, String form, String[] array,
            final String classe) {
        // 1 est le code pour le changement d'un intervalle
        String script = "envoiTableau(1, 0)";
        return getSelectAssemble(intervalle, array, classe, script);
    }


    /**
     * @param attribut classe du sélect généré
     */
    public final void setClasse(final String attribut) {
        this.classe = attribut;
    }
}
