package fr.paris.epm.global.commun;

/**
 * Classe permettant de regrouper les différents types de recherche qui existent dans l'application
 * Cette classe permettra de refactoriser les API de recherche sans oublier d'appels par instrospection  de type facade.methode
 */
public class TypeRecherche {

    /**
     * Profil sous administration.
     */
    public static final int S_RECHERCHE_ADMINISTRATION_PROFIL = 0;

    /**
     * Recherche de consultation.
     */
    public static final int S_RECHERCHE_CONSULTATION = 1;

    /**
     * Recherche d'instance de commission.
     */
    public static final int S_RECHERCHE_INSTANCE_COMMISSION = 2;

    /**
     * Recherche de reunion pour modification (module commission).
     */
    public static final int S_RECHERCHE_REUNION = 3;

    /**
     * Recherche de canevas pour le module redaction.
     */
    public static final int S_RECHERCHE_CANEVAS = 4;

    /**
     * Recherche de consultation.
     */
    public static final int S_RECHERCHE_CONTRAT = 5;

    /**
     * Recherche d' opération/unité fonctionnelle.
     */
    public static final int S_RECHERCHE_OPERATION_UNITE_FONCTIONNELLE = 6;

    /**
     * Recherche de note d'opportunité.
     */
    public static final int S_RECHERCHE_NOTE_OPPORTUNITE = 7;
    /**
     * Recherche par defaut.
     */
    public static final int S_RECHERCHE_DEFAUT = -1;

    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_FICHE_PRATIQUE = new TypeRecherche("fichePratiqueFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_CODE_NAV = new TypeRecherche("consultationFacade", "chercherCodeNavParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_COMMISSION = new TypeRecherche("commissionFacade", "chercherConsultationInscrire", S_RECHERCHE_REUNION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_ORGANISME_CIBLE = new TypeRecherche("echangeFacade", "chercherOrganismeCible", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_AVENANT = new TypeRecherche("avenantFacade", "chercherAvenantsEtNombreResultats", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_COMMISSION_CRITERE = new TypeRecherche("commissionFacade", "chercherInstCommParCritere", S_RECHERCHE_INSTANCE_COMMISSION);
    public static final TypeRecherche TYPE_RECHERCHE_ECHANGE_HISTORIQUE = new TypeRecherche("echangeDematFacade", "rechercherHistoriquePublicationEtRegistre", S_RECHERCHE_DEFAUT);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_PAR_LOT = new TypeRecherche("consultationFacade", "rechercheConsultationParLot", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_PROCEDURE = new TypeRecherche("procedureFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_CONTRAT_LOT = new TypeRecherche("contratFacade", "chercherVueLotParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONTRAT_LOT = new TypeRecherche("contratFacade", "recupererContratsParLot", S_RECHERCHE_DEFAUT);
    public static final TypeRecherche TYPE_RECHERCHE_CANEVAS = new TypeRecherche("canevasFacadeGWT", "chercher", S_RECHERCHE_CANEVAS);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_UTILISATEUR = new TypeRecherche("utilisateurFacade", "chercherUtilisateur", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_CONTRAT = new TypeRecherche("contratFacade", "recupererContratBeanAvecNombreResultats", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_PASSATION_CRITERE = new TypeRecherche("consultationFacade", "chercherConsultPassationParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_CPV_CRITERE = new TypeRecherche("correspondanceCpvFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CLAUSE_CRITERE_SANS_HTML = new TypeRecherche("clauseFacadeGWT", "chercherParCriteresSansHTML", S_RECHERCHE_DEFAUT);
    public static final TypeRecherche TYPE_RECHERCHE_CANEVAS_COMMISSION_MEMBRE = new TypeRecherche("commissionFacade", "chercherSeancesParMembre", S_RECHERCHE_CANEVAS);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_ANNUAIRE_CRITERE = new TypeRecherche("annuaireEntrepriseFacadeTarget", "chercherEntrepriseParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_REUNION_COMMISSION_CONSULTATION_MINIMALE_PREINSCRIRE = new TypeRecherche("commissionFacade", "chercherConsultMinimalePreInscrire", S_RECHERCHE_REUNION);
    public static final TypeRecherche TYPE_RECHERCHE_CONTRAT_AVENANT = new TypeRecherche("avenantFacade", "chercherContrat", S_RECHERCHE_CONTRAT);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_SUPPORT_EXTERNE = new TypeRecherche("gestionSupportFacade", "chercherSupportPubExterneEPM", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_NAV_CRITERE = new TypeRecherche("referentielCodeNAVFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_ACTUALITES_CRITERES = new TypeRecherche("messageActualiteFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_NAV_OBJET = new TypeRecherche("referentielCodeNAVFacade", "chercherEpmTObjetParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_OPPORTUNITE = new TypeRecherche("noteOpportuniteFacade", "chercher", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_ACTUALITES_CRITERE = new TypeRecherche("messageActualiteFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CLAUSES_CRITERE = new TypeRecherche("clauseFacadeGWT", "chercherParCriteres", S_RECHERCHE_DEFAUT);
    public static final TypeRecherche TYPE_RECHERCHE_OPERATION = new TypeRecherche("operationUniteFonctionnelleFacade", "chercherOperationUniteFonctionnelle", S_RECHERCHE_OPERATION_UNITE_FONCTIONNELLE);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_HABILITATIONS = new TypeRecherche("habilitationFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_REUNION_COMMISSION_CRITERES = new TypeRecherche("commissionFacade", "chercherReunionParCritere", S_RECHERCHE_REUNION);
    public static final TypeRecherche TYPE_RECHERCHE_REUNIONCOMMISSION_CONSULTATION_INSCRIRE = new TypeRecherche("commissionFacade", "chercherConsultationInscrire", S_RECHERCHE_REUNION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_DOCUMENT_CONSULTATION = new TypeRecherche("documentRedactionFacade", "chercherConsultations", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONTRAT_BEAN = new TypeRecherche("contratFacade", "recupererContratBeanAvecNombreResultats", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_ECHANGE_GO = new TypeRecherche("echangeFacade", "chercherEchangesGo", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_REUNION_COMMISSION_DELIBERATION = new TypeRecherche("commissionFacade", "chercherConsultationDeliberation", S_RECHERCHE_REUNION);
    public static final TypeRecherche TYPE_RECHERCHE_ACHAT_NAV_CRITERE = new TypeRecherche("consultationFacade", "chercherCodeNavParCritere", S_RECHERCHE_CONSULTATION);
    public static final TypeRecherche TYPE_RECHERCHE_CONSULTATION_DOCUMENT_CRITERE = new TypeRecherche("documentModeleFacade", "chercherParCriteres", S_RECHERCHE_CONSULTATION);

    private final String facade;
    private final String methode;
    private final int typeRecherche;

    /**
     * @param facade        nom de la classe qui sera utilisé pour la recherche
     * @param methode       nom de la methode de la classe ci dessus
     * @param typeRecherche
     */
    public TypeRecherche(String facade, String methode, int typeRecherche) {
        this.facade = facade;
        this.methode = methode;
        this.typeRecherche = typeRecherche;
    }

    public String getFacade() {
        return facade;
    }

    public String getMethode() {
        return methode;
    }

    public int getTypeRecherche() {
        return typeRecherche;
    }

}
