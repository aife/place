package fr.paris.epm.global.coordination.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Formulaire {

    DEMANDE_ACHAT(0x1, "Demande Achat"),

    CONSULTATION(0x2, "Consultation");

    private int id;

    private String name;

    private Formulaire(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Formulaire findById(int id) {
        if (DEMANDE_ACHAT.id == id)
            return DEMANDE_ACHAT;
        else if (CONSULTATION.id == id)
            return CONSULTATION;
        return null;
    }

    public static List<Directory> likeDirectory() {
        return new ArrayList<Directory>() {{
            add(new Directory(DEMANDE_ACHAT.id, DEMANDE_ACHAT.name));
            add(new Directory(CONSULTATION.id, CONSULTATION.name));
        }};
    }

    public static Map<Integer, String> likeHashMap() {
        return new HashMap<Integer, String>() {{
            put(DEMANDE_ACHAT.id, DEMANDE_ACHAT.name);
            put(CONSULTATION.id, CONSULTATION.name);
        }};
    }

}
