package fr.paris.epm.global.commun.securite;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SecurityServlet extends HttpServlet {
	
	
	/**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
		// Put your code here
	}

	/**
	 * Constructor of the object.
	 */
	public SecurityServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		/*
		RequestDispatcher rd =getServletContext().getRequestDispatcher("/j_acegi_security_check");	
		rd.forward(request,response);
		*/
		//response.sendRedirect(request.getContextPath() + "/j_acegi_security_check?j_username=" + user + "&j_password=" + user);
		response.sendRedirect(request.getContextPath() + "/j_acegi_security_check");
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */

}
