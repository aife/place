package fr.paris.epm.global.commun.tags;

import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Permet de rendre des morceaux de la JSP uniquement si un profile Spring est actif (ou non avec l'option enabled=false)
 * Created by sta on 15/12/15.
 */
public class FeatureTag extends TagSupport {

    private static final long serialVersionUID = 1L;

    private String profile;

    private String activated = "true";

    private boolean isActivated() {
        return "true".equals(activated);
    }

    @Override
    public int doStartTag() throws JspException {
        String[] activeProfiles = WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getEnvironment().getActiveProfiles();
        for (String activeProfile : activeProfiles) {
            if (activeProfile.equals(profile)) {
                return isActivated() ? EVAL_BODY_INCLUDE : SKIP_BODY;
            }
        }
        return isActivated() ? SKIP_BODY : EVAL_BODY_INCLUDE;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }
}
