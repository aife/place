package fr.paris.epm.global.commun.worker;

import fr.paris.epm.global.commons.exception.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Classe-lanceur des Taches.
 * Created by nty on 12/03/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Component
public class RsemTaskWorker {
    private static final Logger logger = LoggerFactory.getLogger(RsemTaskWorker.class);

    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    private Map<String, RsemFutureResult<?>> results = new HashMap<>();

    public <T> String executeTask(RsemTask<T> task) {
        String idTask = UUID.randomUUID().toString();
        synchronized (this) {
            task.setIdTask(idTask);
            RsemFutureResult<T> rsemResult = new RsemFutureResult<T>(taskExecutor.submit(task), task.getResultHref());
            results.put(idTask, rsemResult);
        }
        return idTask;
    }

    public <T> T getResult(String idTask) throws Exception {
        if (idTask == null) {
            logger.error("The task id is null");
            throw new TechnicalException("The task id is null");
        }
        if (CollectionUtils.isEmpty(results)) {
            logger.error("La liste des taches est vide");
            throw new TechnicalException("La liste des tâches est vide");
        }


        RsemFutureResult<T> rsemResult = (RsemFutureResult<T>) results.get(idTask);
        if (rsemResult == null)
            throw new TechnicalException("The task was removed for the work id : " + idTask);
        if (rsemResult.isDone())
            return rsemResult.get();
        return null;
    }

    public String getResultHref(String idTask) throws Exception {
        RsemFutureResult<?> rsemResult = results.get(idTask);
        if (rsemResult == null)
            throw new TechnicalException("The task was removed for the work id : " + idTask);
        return rsemResult.getResultHref();
    }

}
