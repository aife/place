package fr.paris.epm.global.presentation.controllers;

import fr.paris.epm.global.commun.ConstantesGlobales;
import fr.paris.epm.global.coordination.objetValeur.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controlleur Abstrait de gestion des requetes
 * Created by nty on 14/02/17.
 * @author Nikolay Tyurin
 * @author nty
 */
public abstract class AbstractController {

    private MessageSource messageSource;

    /**
     * @param request
     * @return
     */
    public static Utilisateur getUtilisateur(HttpServletRequest request) {
        return (Utilisateur) request.getAttribute(ConstantesGlobales.UTILISATEUR_COORDINATION);
    }

    public String message(String codeMessage) {
        try {
            return messageSource.getMessage(codeMessage, new Object[]{}, null);
        } catch (NoSuchMessageException ex) {
            return codeMessage;
        }
    }

    public Message message(TypeMessage typeMessage, String codeMessage) {
        return new Message(typeMessage, message(codeMessage));
    }

    public Message messageError(String codeMessade) {
        return message(TypeMessage.ERROR, codeMessade);
    }

    public Message messageError() {
        return messageError("erreur.erreurGenerique");
    }

    public Message messageSucces(String codeMessage) {
        return message(TypeMessage.SUCCESS, codeMessage);
    }

    public List<String> parseErrors(BindingResult bindingResult) {
        List<String> messages = bindingResult.getGlobalErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .map(this::message)
                .collect(Collectors.toList());

        messages.addAll(bindingResult.getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .map(this::message)
                .collect(Collectors.toList()));

        return messages;
    }

    @Autowired
    public void setMessageSource(@Qualifier("messageSource") final MessageSource valeur) {
        this.messageSource = valeur;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public static enum TypeMessage { NULL, ERROR, SUCCESS, WARNING }

    public static class Message {

        private TypeMessage type;
        private String title;
        private List<String> lines;

        public Message(TypeMessage type, String title) {
            this.type = type;
            this.title = title;
            this.lines = new ArrayList<>();
        }

        public TypeMessage getType() {
            return type;
        }

        public String getTitle() {
            return title;
        }

        public List<String> getLines() {
            return lines;
        }

        public Message addErrors(Collection<String> errors) {
            lines.addAll(errors);
            return this;
        }

    }

    public static class Ajax extends HashMap<String, Object> {

        private Ajax() {
            super();
        }

        public static Ajax successResponse(Object object) {
            Ajax response = new Ajax();
            response.put("result", "success");
            response.put("data", object);
            return response;
        }

        public static Ajax emptyResponse() {
            Ajax response = new Ajax();
            response.put("result", "success");
            return response;
        }

        public static Ajax errorResponse(String errorMessage) {
            Ajax response = new Ajax();
            response.put("result", "error");
            response.put("message", errorMessage);
            return response;
        }

    }

}

