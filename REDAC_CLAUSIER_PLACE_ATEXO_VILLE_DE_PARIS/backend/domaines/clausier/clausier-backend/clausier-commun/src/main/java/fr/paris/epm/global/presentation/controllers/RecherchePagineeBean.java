package fr.paris.epm.global.presentation.controllers;

import fr.paris.epm.global.presentation.forms.TableauPagineGeneriqueForm;

/**
 * Created by sta on 29/06/16.
 */
public class RecherchePagineeBean extends TableauPagineGeneriqueForm {

    /*
     temporairement RecherchePagineeBean extends TableauPagineGeneriqueForm
     pour se marier ESR-2016 et ESR-2017
     */

    /** Propriété sur laquelle portera le tri */
    private String triPropriete;
    /** indique si le tri doit être croissant ou non */
    private boolean triCroissant;
    /** Numéro de la page en cours */
    private int numeroPage = 1;
    /** Nombre de résultats par page */
    private int intervalle = 10;

    /**
     * Calcule le nombre de pages de résultats.
     *
     * @param nbResultats
     * @return
     */
    public int calculerNombrePages(Long nbResultats) {
        return (int) Math.ceil(nbResultats.floatValue() / (float) intervalle);
    }

    public String getTriPropriete() {
        return triPropriete;
    }

    public void setTriPropriete(String triPropriete) {
        this.triPropriete = triPropriete;
    }

    public boolean isTriCroissant() {
        return triCroissant;
    }

    public void setTriCroissant(boolean triCroissant) {
        this.triCroissant = triCroissant;
    }

    public int getNumeroPage() {
        return numeroPage;
    }

    public void setNumeroPage(int numeroPage) {
        this.numeroPage = numeroPage;
    }

    public int getIntervalle() {
        return intervalle;
    }

    public void setIntervalle(int intervalle) {
        this.intervalle = intervalle;
    }

}
