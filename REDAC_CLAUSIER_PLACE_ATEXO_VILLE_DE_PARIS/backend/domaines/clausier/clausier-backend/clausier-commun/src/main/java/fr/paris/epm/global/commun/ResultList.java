package fr.paris.epm.global.commun;

import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.global.coordination.facade.MapperFacade;
import fr.paris.epm.global.coordination.facade.SafelyFacade;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class ResultList
 * Created by nty on 03/03/17.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
public class ResultList<B extends Bean, F extends SafelyFacade & MapperFacade, K extends Critere> implements Iterable<B> {

    private List<B> listResults;

    private long count;

    private F facade;

    private K critere;

    public ResultList(F facade, K critere) {
        this.listResults = new ArrayList<B>();
        this.count = 0;

        this.facade = facade;
        this.critere = critere;
    }

    @Override
    public Iterator<B> iterator() {
        return listResults.iterator();
    }

    public long count() {
        return count;
    }

    public long getCount() {
        return count;
    }

    public int countPages() {
        long d = count / critere.getTaillePage();
        long r = count % critere.getTaillePage();
        return (int) (r == 0 ? d : d + 1);
    }

    public int getCountPages() {
        long rest = count % critere.getTaillePage();
        long i = count / critere.getTaillePage();
        return (int) (rest == 0L ? i : i + 1);
    }

    public int pageCurrent() {
        return critere.getNumeroPage() + 1;
    }

    public int getPageCurrent() {
        return critere.getNumeroPage() + 1;
    }

    public int interval() {
        return critere.getTaillePage();
    }

    public int getInterval() {
        return critere.getTaillePage();
    }

    public List<B> listResults() {
        return listResults;
    }

    public List<B> getListResults() {
        return listResults;
    }

    public K critere() {
        return critere;
    }

    public ResultList<B, F, K> find(EpmTRefOrganisme epmTRefOrganisme) {

        List l = facade.safelyFind(critere);
        count = (long) l.remove(0);
        listResults.clear();
        listResults.addAll(facade.map(l, epmTRefOrganisme));

        return this;
    }


}
