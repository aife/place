package fr.paris.epm.global.commun.securite;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.AuthentificationTokenCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.InvalidCookieException;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Utilise par spring-security pour enregistrer et authentifier par un cookie un utilisateur.
 * @author Rémi Villé
 */
public class RememberMeServices extends TokenBasedRememberMeServices  {

    /**
     * Acces aux objets persistants (injection Spring).
     */
    protected AdministrationServiceSecurise administrationService;

    private static final Logger logger = LoggerFactory.getLogger(RememberMeServices.class);

    public RememberMeServices(String key, UserDetailsService userDetailsService) {
        super(key, userDetailsService);
    }

    private String getCookiePath() {
        return "/";
    }

    /**
     * Sets a "cancel cookie" (with maxAge = 0) on the response to disable persistent logins.
     */
    protected void cancelCookie(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Cancelling cookie");
        Cookie cookie = new Cookie(getCookieName(), null);
        cookie.setMaxAge(0);
        cookie.setPath(getCookiePath());

        response.addCookie(cookie);
    }

    /**
     * Sets the cookie on the response
     *
     * @param tokens the tokens which will be encoded to make the cookie value.
     * @param maxAge the value passed to {@link Cookie#setMaxAge(int)}
     * @param request the request
     * @param response the response to add the cookie to.
     */
    protected void setCookie(String[] tokens, int maxAge, HttpServletRequest request, HttpServletResponse response) {
        String cookieValue = encodeCookie(tokens);
        Cookie cookie = new Cookie(getCookieName(), cookieValue);
        cookie.setMaxAge(maxAge);
        cookie.setPath(getCookiePath());
        response.addCookie(cookie);
    }

    /**
     * Juste apres une premiere authentification reussi, sauvegarde d'une signature d'authentification dans le cookie remembre-me
     * et en base (pour authentification BDD et authentification LDAP).
     */
    public void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
                               Authentication successfulAuthentication) {
        String errorMsg;
        String username = retrieveUserName(successfulAuthentication);
        EpmTUtilisateur utilisateurBdd;
        EpmTAuthentificationToken authTokenBdd;
        Date dateDerniereActivite = null;

        // If unable to find a username, just abort as TokenBasedRememberMeServices is
        // unable to construct a valid token in this case.
        if (!StringUtils.hasLength(username))
            return;

        String signatureValue = UUID.randomUUID().toString();

        // Ajout des informations d'authentification en bdd
        try {
            utilisateurBdd  = administrationService.chargerUtilisateur(username);
            // Recuperation des informations d'authentification de l'utilisateur en BDD
            AuthentificationTokenCritere critere = new AuthentificationTokenCritere();
            critere.setIdentifiant(username);
            List<EpmTAuthentificationToken> authTokenList = administrationService.chercherEpmTObject(utilisateurBdd.getId(), critere);
            if (authTokenList != null && authTokenList.size() > 1) {
                errorMsg = "Probleme lors de la recuperation de l'authentification de l'utilisateur (plus d'un resultat).";
                logger.error(errorMsg);
                throw new InvalidCookieException(errorMsg);
            }
            if (authTokenList != null && !authTokenList.isEmpty()) {
                authTokenBdd = authTokenList.get(0);
                // Si deja authentifie (d'un autre browser par exemple, on utilise la meme signature)
                signatureValue = authTokenBdd.getSignature();
                dateDerniereActivite = authTokenBdd.getDateDerniereActivite();
                administrationService.supprimerAuthentificationToken(utilisateurBdd.getId(), username);
            } else {
                authTokenBdd = new EpmTAuthentificationToken();
            }
            // Initialisation des informations d'authentification
            authTokenBdd.setJusteAuthentifier(true);
            authTokenBdd.setIdentifiant(username);
            authTokenBdd.setSignature(signatureValue);
            authTokenBdd.setToken(0);
            // Si on reutilise les informations d'authentification, il ne faut pas actualiser la date de derniere activite
            if (dateDerniereActivite == null) {
                dateDerniereActivite = new Date();
            }
            authTokenBdd.setDateDerniereActivite(dateDerniereActivite);
            administrationService.modifierAuthentificationToken(utilisateurBdd.getId(), authTokenBdd);
        } catch (TechnicalNoyauException e) {
            logger.error("Erreur lors de la recherche de l'utilisateur en base : " + e.getMessage(), e);
        }

        // -1 => Cookie non persistant (supprime a la fermeture de la session ou du navigateur)
        setCookie(new String[] {username, signatureValue, "0"}, -1, request, response);
        if (logger.isDebugEnabled()) {
            logger.debug("Added remember-me cookie for user '" + username);
        }

        request.setAttribute("justeAuthentifier", "true");
    }

    /**
     * Utilise pour l'authentification quand cookie remember-me present (pour authentification BDD).
     */
    public UserDetails processAutoLoginCookie(String[] cookieTokens, HttpServletRequest request, HttpServletResponse response) {
        logger.info("username" + cookieTokens[0]);
        UserDetails userDetails = getUserDetailsService().loadUserByUsername(cookieTokens[0]);
        return userDetails;
    }

    public void setAdministrationService(AdministrationServiceSecurise administrationService) {
        this.administrationService = administrationService;
    }

}