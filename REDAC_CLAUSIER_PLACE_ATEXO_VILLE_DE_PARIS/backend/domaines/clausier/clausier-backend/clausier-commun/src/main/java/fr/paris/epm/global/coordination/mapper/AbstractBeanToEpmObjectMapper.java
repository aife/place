package fr.paris.epm.global.coordination.mapper;

import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.noyau.persistance.EpmTObject;
import org.mapstruct.MappingTarget;

/**
 * Created by nty on 08/01/19.
 */
public interface AbstractBeanToEpmObjectMapper<T extends Bean, E extends EpmTObject> {

    public abstract E toEpmTObject(T bean);

    public abstract void toEpmTObject(T bean, @MappingTarget E epmTObject);

}