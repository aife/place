package fr.paris.epm.global.coordination.facade;

import fr.paris.epm.global.coordination.bean.Bean;
import fr.paris.epm.global.coordination.mapper.AbstractBeanToEpmObjectMapper;
import fr.paris.epm.global.coordination.mapper.AbstractEpmObjectToBeanMapper;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;

import java.util.List;
import java.util.function.Supplier;

/**
 * Created by nty on 18/05/17.
 */
public abstract class AbstractMapperFacade<B extends Bean, E extends EpmTObject>
        extends AbstractFacade
        implements Facade<B>, SafelyFacade, MapperFacade<B, E> {

    public Supplier<? extends Critere> critereContructor;

    @Override
    public <K extends Critere> Supplier<K> critereContructor() {
        return (Supplier<K>) critereContructor;
    }

    protected <K extends Critere> void setCritereContructor(Supplier<K> critereContructor) {
        this.critereContructor = critereContructor;
    }

    private String defaultSortField;

    protected void setDefaultSortField(String defaultSortField) {
        this.defaultSortField = defaultSortField;
    }

    private String defaultIdField;

    protected void setDefaultIdField(String defaultIdField) {
        this.defaultIdField = defaultIdField;
    }

    protected abstract boolean defaultRemoveTest(final int id);

    public abstract AbstractBeanToEpmObjectMapper<B, E> getToEpmObjectMapper();

    @Override
    public E map(B bean, EpmTRefOrganisme epmTRefOrganisme) {
        if (bean.getId() == 0) {
            return getToEpmObjectMapper().toEpmTObject(bean);
        } else {
            E epmTObject = safelyUniqueFind(defaultIdField, bean.getId());
            getToEpmObjectMapper().toEpmTObject(bean, epmTObject);
            return epmTObject;
        }
    }

    @Override
    public B findById(final int id, EpmTRefOrganisme epmTRefOrganisme) {
        E epmTObject = safelyUniqueFind(defaultIdField, id);
        return map(epmTObject, epmTRefOrganisme);
    }

    @Override
    public List<B> find(String field, Object value, EpmTRefOrganisme epmTRefOrganisme) {

        if (value.getClass() == String.class)
            return findStrict(field, (String) value, epmTRefOrganisme);

        List<E> epmTObjects = safelyFind(field, value, null);
        return map(epmTObjects, epmTRefOrganisme);
    }

    @Override
    public List<B> findStrict(String field, String value, EpmTRefOrganisme epmTRefOrganisme) {

        List<E> epmTObjects = safelyFind(field, value, true);
        return map(epmTObjects, epmTRefOrganisme);
    }


    /**
     * Enregistre / crée un bean
     *
     * @param bean entité à modifier / créer.
     * @return le nouveau bean
     */
    @Override
    public B save(final B bean, EpmTRefOrganisme epmTRefOrganisme) {
        E epmTObject = map(bean, epmTRefOrganisme);
        epmTObject = safelySave(epmTObject);
        return map(epmTObject, epmTRefOrganisme);
    }

    /**
     * Suppression d'un bean
     *
     * @param id id du bean à supprimer
     */
    @Override
    public boolean remove(final int id) {

        if (defaultRemoveTest(id)) {
            E epmTObject = safelyUniqueFind(defaultIdField, id);
            return safelyRemove(epmTObject);
        }
        return false;
    }

}
