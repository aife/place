package fr.paris.epm.global.presentation.controllers;

import fr.paris.epm.global.commun.FileTmp;
import fr.paris.epm.global.commun.worker.RsemTaskWorker;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * Controlleur Abstrait de gestion des taches longues
 * Created by nty on 12/03/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Controller
public class WorkerController extends AbstractController {

    private static final Logger logger = LoggerFactory.getLogger(WorkerController.class);

    @Resource
    private RsemTaskWorker rsemTaskWorker;

    /**
     * Verification que la thread id est en finit, en cours ou ko
     */
    @RequestMapping(value = "/verifierRsemTask")
    @ResponseBody
    public Ajax verifierRsemTask(@RequestParam("idTask") String idTask) {
        logger.info("/verifierRsemTask.json -> " + idTask);

        try {
            Object result = rsemTaskWorker.getResult(idTask);
            if (result == null) {
                logger.info("La tache {} est encore en train d'execution", idTask);
                return Ajax.emptyResponse(); // tache n'est pas encore finie
            }
            String infoShowResult = result.toString();
            if (infoShowResult.length() > 100)
                infoShowResult = infoShowResult.substring(0, 90) + " ...";
            logger.info("Result d'execution de la tache {} : {}", idTask, infoShowResult);
            return Ajax.successResponse(rsemTaskWorker.getResultHref(idTask));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return Ajax.errorResponse(ex.getMessage());
        }
    }

    @RequestMapping(value = "/downloadTaskResult")
    public void downloadTaskResult(@RequestParam("idTask") String idTask, HttpServletResponse response) throws Exception {
        logger.info("/downloadTaskResult.htm -> " + idTask);

        Object result = rsemTaskWorker.getResult(idTask);
        FileTmp fileTmp = (FileTmp) result;

        // Entêtes
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileTmp.getNomFichier());
        response.setContentLength((int) fileTmp.length());

        // Flux de sortie
        OutputStream fluxServlet = response.getOutputStream();
        FileInputStream fluxFichier = new FileInputStream(fileTmp);
        IOUtils.copy(fluxFichier, fluxServlet);
        IOUtils.closeQuietly(fluxServlet);
        IOUtils.closeQuietly(fluxFichier);
        response.flushBuffer();
    }

    @RequestMapping(value = "/ajaxTaskResult")
    @ResponseBody
    public Ajax ajaxTaskResult(@RequestParam("idTask") String idTask) throws Exception {
        logger.info("/ajaxTaskResult.json -> " + idTask);

        Map<String, Object> result = rsemTaskWorker.getResult(idTask);
        return (Ajax) result.get("ajax");
    }

}
