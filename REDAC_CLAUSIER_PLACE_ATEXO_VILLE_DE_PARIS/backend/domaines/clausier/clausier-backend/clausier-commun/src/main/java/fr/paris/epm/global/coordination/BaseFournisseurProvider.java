package fr.paris.epm.global.coordination;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

/**
 * Created by sta on 31/05/16.
 */
@Component
public class BaseFournisseurProvider {

    public static final String SSO_MODE_RSEM_TOKEN = "&sso_mode=rsem_token";
    public static final String BASE_FOURNISSEUR_NG2_ANCHOR = "#/fournisseurs?filter=none&motsCles=";
    public static final String BASE_FOURNISSEUR_CONTRAT_NG2_ANCHOR = "#/contrats?perimetre=";
    public static final String SELECTION_CONTACT = "&selectionContact=";
    public static final String CALLBACK = "&callBack=";

    @Value("${base-fournisseur.url}")
    String baseFournisseurUrl;

    public String fetchBaseFournisseurUrl(String tokenSso) {
        return fetchBaseFournisseurUrl(tokenSso, null, false, null, null);
    }

    public String fetchBaseFournisseurUrl(String tokenSso, String motClef, boolean selectionContact, String callBack, String contrats) {

        StringJoiner baseFournisseurJoiner = new StringJoiner("").add(baseFournisseurUrl).add("?sso=").add(tokenSso).add(SSO_MODE_RSEM_TOKEN);

        if (contrats != null && !contrats.isEmpty()) {
            //Redirection vers la page contrats
            baseFournisseurJoiner.add(BASE_FOURNISSEUR_CONTRAT_NG2_ANCHOR).add(contrats);
        } else {
            //Par defaut redirection vers la page fournisseurs
            baseFournisseurJoiner.add(SELECTION_CONTACT).add(String.valueOf(selectionContact)).add(CALLBACK).add(callBack).add(BASE_FOURNISSEUR_NG2_ANCHOR).add(motClef);
        }
        return baseFournisseurJoiner.toString();
    }

}