package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultationServiceHTTPTest {

    @Mock
    private GeneriqueDAO generiqueDAO;
    @InjectMocks
    private ConsultationServiceHTTP consultationServiceHTTP = new ConsultationServiceHTTP();


    @Test
    public void testGetConsultationContexte() throws IOException {
        // Arrange
        consultationServiceHTTP.setGeneriqueDAO(generiqueDAO);
        ConsultationContexteCritere consultationContexteCritere = new ConsultationContexteCritere();
        EpmTConsultationContexte contexte = new EpmTConsultationContexte();
        contexte.setConsultationJson(FileUtils.readFileToString(new File("src/test/resources/data/consultation.json"), "UTF-8"));
        when(generiqueDAO.findUniqueByCritere(consultationContexteCritere)).thenReturn(contexte);
        // Act
        EpmTConsultation result = consultationServiceHTTP.chercherConsultation(consultationContexteCritere);
        // Assert
        Assertions.assertNotNull(result);
    }
}
