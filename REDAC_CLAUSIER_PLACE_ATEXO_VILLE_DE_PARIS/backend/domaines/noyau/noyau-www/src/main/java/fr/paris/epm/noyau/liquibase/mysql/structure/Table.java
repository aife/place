package fr.paris.epm.noyau.liquibase.mysql.structure;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Table {
    private String publicSchema;
    private String schema;
    private String name;
    private String csvFile;
    private int nbRows;
    private LocalDateTime start;
    private LocalDateTime end;
    private List<String> warnings = new ArrayList<>();

    public Table(String publicSchema, String schema, String name) {
        this.publicSchema = publicSchema;
        this.schema = schema;
        this.name = name;
    }

    public String getSchema() {
        return schema;
    }

    public String getName() {
        return name;
    }

    public String getQualifiedTableName() {
        return schema.equals(publicSchema) ? name : schema + "." + name;
    }

    public String getCsvFile() {
        return csvFile;
    }

    public void setCsvFile(String csvFile) {
        this.csvFile = csvFile;
    }

    public int getNbRows() {
        return nbRows;
    }

    public void setNbRows(int nbRows) {
        this.nbRows = nbRows;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }
}
