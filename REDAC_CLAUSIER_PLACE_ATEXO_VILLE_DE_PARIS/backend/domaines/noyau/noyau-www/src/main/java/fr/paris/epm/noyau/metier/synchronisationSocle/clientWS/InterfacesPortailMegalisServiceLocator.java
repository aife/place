/**
 * InterfacesPortailMegalisServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.paris.epm.noyau.metier.synchronisationSocle.clientWS;

public class InterfacesPortailMegalisServiceLocator extends org.apache.axis.client.Service implements InterfacesPortailMegalisService {

    public InterfacesPortailMegalisServiceLocator() {
    }


    public InterfacesPortailMegalisServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public InterfacesPortailMegalisServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for InterfacesPortailMegalisPort
    private java.lang.String InterfacesPortailMegalisPort_address;

    public java.lang.String getInterfacesPortailMegalisPortAddress() {
        return InterfacesPortailMegalisPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String InterfacesPortailMegalisPortWSDDServiceName = "InterfacesPortailMegalisPort";

    public java.lang.String getInterfacesPortailMegalisPortWSDDServiceName() {
        return InterfacesPortailMegalisPortWSDDServiceName;
    }

    public void setInterfacesPortailMegalisPortWSDDServiceName(java.lang.String name) {
        InterfacesPortailMegalisPortWSDDServiceName = name;
    }

    public InterfacesPortailMegalisPortType getInterfacesPortailMegalisPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InterfacesPortailMegalisPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInterfacesPortailMegalisPort(endpoint);
    }

    public InterfacesPortailMegalisPortType getInterfacesPortailMegalisPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            InterfacesPortailMegalisBindingStub _stub = new InterfacesPortailMegalisBindingStub(portAddress, this);
            _stub.setPortName(getInterfacesPortailMegalisPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInterfacesPortailMegalisPortEndpointAddress(java.lang.String address) {
        InterfacesPortailMegalisPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (InterfacesPortailMegalisPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                InterfacesPortailMegalisBindingStub _stub = new InterfacesPortailMegalisBindingStub(new java.net.URL(InterfacesPortailMegalisPort_address), this);
                _stub.setPortName(getInterfacesPortailMegalisPortWSDDServiceName());
                return _stub;
            }
        }
        catch (Exception e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("InterfacesPortailMegalisPort".equals(inputPortName)) {
            return getInterfacesPortailMegalisPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:InterfacesPortailMegaliswsdl", "InterfacesPortailMegalisService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:InterfacesPortailMegaliswsdl", "InterfacesPortailMegalisPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("InterfacesPortailMegalisPort".equals(portName)) {
            setInterfacesPortailMegalisPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
