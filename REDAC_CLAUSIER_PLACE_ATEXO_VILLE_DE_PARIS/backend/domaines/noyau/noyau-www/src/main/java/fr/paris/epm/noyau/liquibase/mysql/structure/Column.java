package fr.paris.epm.noyau.liquibase.mysql.structure;

public class Column {
    private String name;
    private boolean booleanType;

    public Column(String name, boolean booleanType) {
        this.name = name;
        this.booleanType = booleanType;
    }

    public String getName() {
        return name;
    }

    public boolean isBooleanType() {
        return booleanType;
    }
}
