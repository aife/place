package fr.paris.epm.noyau.metier;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Interface CRUD générique pour tous les Gestionnaires de Ressources Métier.
 * Created by nty on 29/03/18.
 *
 * @author Nikolay Tyurin
 * @author NTY
 */
public interface BaseDAO {

    Logger logger = LoggerFactory.getLogger(BaseDAO.class);

    Session getCurrentSession();

    default <T> void persist(T entity) {
        getCurrentSession().persist(entity);
        getCurrentSession().flush();
    }

    default <T> T merge(T entity) {
        try {
            return (T) getCurrentSession().merge(entity);
        } catch (Exception e) {
            logger.error("Error while merging entity {}", entity, e);
            throw e;
        }
    }

    default <T> void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    default <T, K extends Serializable> T find(Class<T> klass, K id) {
        return (T) getCurrentSession().get(klass, id);
    }

    default <T, K extends Serializable> T load(Class<T> klass, K id) {
        return (T) getCurrentSession().load(klass, id);
    }

    default <T> List<T> findAll(Class<T> klass) {
        return findByQuery("from " + klass.getName());
    }

    default <T> List<T> findBySQLQueryWithParams(String queryString, Class<?> clazz) {
        logger.info("Query = {}", queryString);

        Query query = getCurrentSession().createSQLQuery(queryString).addEntity(clazz);
        return (List<T>) query.list();
    }

    default <T> List<T> findByQuery(String queryString, boolean cache) {
        return findByQueryWithParams(queryString, Collections.emptyMap(), cache);
    }

    default <T> List<T> findByQuery(String queryString) {
        return findByQuery(queryString, true);
    }

    default <T> List<T> findByQueryWithParams(String queryString, Map<String, Object> params, boolean cache) {
        logger.debug(cache ? "Caching multiple result query '{}'" : "No cache for multiple result query '{}'", queryString);
        Query query = getCurrentSession().createQuery(queryString);
        query.setProperties(params);
        query.setCacheable(cache);
        query.setFetchSize(1000);
        return (List<T>) query.list();
    }

    default <T> List<T> findByQueryWithParams(String queryString, Map<String, Object> params) {
        return findByQueryWithParams(queryString, params, true);
    }

    default <T> T findUniqueByQuery(String queryString, boolean cache) {
        logger.debug(cache ? "Caching unique query '{}'" : "No cache for unique query '{}'", queryString);
        return findUniqueByQueryWithParams(queryString, Collections.emptyMap(), cache);
    }

    default <T> T findUniqueByQuery(String queryString) {
        return findUniqueByQuery(queryString, true);
    }

    default <T> T findUniqueByQueryWithParams(String queryString, Map<String, Object> params, boolean cache) {
        Query query = getCurrentSession().createQuery(queryString);
        query.setProperties(params);
        query.setCacheable(cache);
        query.setMaxResults(1);
        query.setFetchSize(1);
        return (T) query.uniqueResult();
    }

    default <T> T findUniqueByQueryWithParams(String queryString, Map<String, Object> params) {
        return this.findUniqueByQueryWithParams(queryString, params, true);
    }

    default void executeUpdate(String queryString) {
        Query query = getCurrentSession().createQuery(queryString);
        query.executeUpdate();
    }

    default int executeSqlUpdate(String sqlQuery) {
        SQLQuery query = getCurrentSession().createSQLQuery(sqlQuery);
        return query.executeUpdate();
    }
    default int executeSqlcount(String sqlQuery) {
        SQLQuery query = getCurrentSession().createSQLQuery(sqlQuery);
        return query.list().size();
    }

}
