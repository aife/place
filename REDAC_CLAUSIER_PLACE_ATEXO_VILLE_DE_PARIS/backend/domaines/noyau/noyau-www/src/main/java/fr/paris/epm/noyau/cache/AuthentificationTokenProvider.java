package fr.paris.epm.noyau.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by sta on 23/05/16.
 */
@Component
public class AuthentificationTokenProvider {

    protected Cache<UUID, EpmTUtilisateur> jetonsAuthentification;

    @PostConstruct
    public void init() {
        jetonsAuthentification = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .build();
    }

    /**
     * Génère un token d'authentification pour cet utilisateur
     * @param utilisateur
     * @return
     */
    public UUID generateToken(EpmTUtilisateur utilisateur) {
        UUID token = UUID.randomUUID();
        jetonsAuthentification.put(token, utilisateur);
        return token;
    }

    /**
     * Retourne un utilisateur si le token est valide ou null
     * @param token
     * @param usageUnique -> si vrai, supprime le token du cache
     * @return
     */
    public EpmTUtilisateur retreiveUtilisateur(UUID token, boolean usageUnique) {
        EpmTUtilisateur utilisateur = jetonsAuthentification.getIfPresent(token);
        if (usageUnique) {
            jetonsAuthentification.invalidate(token);
        }
        return utilisateur;
    }

}
