package fr.paris.epm.noyau.metier.documentModele.freemarker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Permet de faire des traitements dans un template FreeMarker (ex : tris sur
 * plusieurs parametres). Doit etre instancie dans le template avec
 * ObjectConstructor pour etre compatible.
 * @author RVI
 */
public class FreemarkerUtil {

    /**
     * Permet de trier la liste paramList par rapport a certains champs des
     * objets qui la compose. Ces champs (et leur priorite de tri) sont definis
     * dans paramCmp.
     */
    public List<Object> tri(List<Object> paramList, String paramCmp[]) {

        GenericComparator comparator = new GenericComparator();
        comparator.setParametreCmp(paramCmp);

        // Creation d'une liste temporaire pour le tri (pour ne faire le sort()
        // directement sur la liste presente dans le template FreeMarker, ce qui
        // declenche une exception)
        List<Object> tmpList = new ArrayList<Object>();
        tmpList.addAll(paramList);

        Collections.sort(tmpList, comparator);

        // Reemcapsulation FreeMarker implicite au moment du return
        return tmpList;
    }
}
