package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.technique.TableStockageEPM;
import fr.paris.epm.noyau.persistance.EpmTObject;

import java.util.ArrayList;
import java.util.List;

public abstract class GeneriqueServiceHTTP implements GeneriqueServiceSecurise {

    /**
     * Serialisation
     */
    private static final long serialVersionUID = 2674567374478753792L;

    protected static GeneriqueDAO generiqueDAO;
    
    protected static TableStockageEPM tableStockageEPM;

    private static ReferentielsServiceLocal referentielsServiceLocal;


    /**
     * L'identifiant de l'objet implementant l'interface EPMTObject passé doit être renseigné et correspondre à
     * un objet de la consultation existant en base.
     * @param objet note de la consultation
     * @return note liée à la consultation ou null si aucune
     */
    public final <T extends EpmTObject> T modifierEpmTObject(final T objet) {
        return generiqueDAO.merge(objet);
    }

    @Override
    public <T extends EpmTObject> void creerEpmTObject(T objet) {
        generiqueDAO.persist(objet);
    }

    /**
     * Recherche d'objet par criteres.
     * @param id id de l'utilisateur.
     * @param criteres les criteres de recherches.
     * @throws NonTrouveNoyauException dans le cas où les élements ne sont pas trouvés.
     * @return {@link List} retourne une liste de documents recherchés.
     */
    @Override
    public <T extends EpmTObject> List<T> chercherEpmTObject(final int id, final Critere criteres) {
        List liste = new ArrayList<>();
        if (criteres.isChercherNombreResultatTotal()) {
            long nb = generiqueDAO.findByCritere(criteres, liste);
            liste.add(0, nb);
        } else {
            liste = generiqueDAO.findByCritere(criteres);
        }
        return liste;
    }
    
    /**
     * Recherche d'objet par criteres et retourne un objet que s'il est unique, retourne null si absent, une exception
     * si plusieurs objets trouvé
     */
    @Override
    public <T extends EpmTObject> T chercherUniqueEpmTObject(final int id, final Critere criteres) {
        T res = generiqueDAO.findUniqueByCritere(criteres);
        return res;
    }

    /**
     * Suppression d'une liste d'objet {@link EpmTObject}
     * @param valeur liste d'objet implementant l'interface {@link EpmTObject} à supprimer.
     * @param id id de l'utilisateur.
     */
    public <T extends EpmTObject> void supprimerEpmTObject(final int id, final List<T> valeur) {
        generiqueDAO.delete(valeur);
    }

    /**
     * Suppression d'un objet {@link EpmTObject}
     * @param valeur objet implement l'interface {@link EpmTObject} à supprimer.
     * @param id id de l'utilisateur.
     */
    public <T extends EpmTObject> void supprimerEpmTObject(final int id, final T valeur) {
        generiqueDAO.delete(valeur);
    }

    /**
     * modifier une liste d'EpmTObject en base
     * 
     * @param id identifiant de l'utilisateur appelant cette méhode
     * @param objets liste de EpmTObject à modifier
     * @return liste de EpmTObject déjà modifié
     */
    @Override
    public <T extends EpmTObject> List<T> modifierEpmTObject(final int id, final List<T> objets) {
        List<T> resultat = new ArrayList<>();
        for (T epmTObject : objets) {
            T tmp = modifierEpmTObject(epmTObject);
            resultat.add(tmp);
        }
        return resultat;
    }

    @Override
    public <T extends EpmTObject> T chercherObject(int id, Class<T> clazz) {
        return (T) generiqueDAO.find(clazz, id);
    }



    public static void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        GeneriqueServiceHTTP.generiqueDAO = generiqueDAO;
    }
}
