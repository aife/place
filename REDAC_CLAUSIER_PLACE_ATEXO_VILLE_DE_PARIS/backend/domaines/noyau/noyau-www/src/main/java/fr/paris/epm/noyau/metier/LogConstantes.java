package fr.paris.epm.noyau.metier;

/**
 * Ensemble des constantes utilisé pour les logs fonctionnels.
 * @author Jose Ortegon
 * @version $Revision$, $Date$, $Author$
 */
public final class LogConstantes {

    /**
     * 
     */
    public static final String NOM = "LOG_FONCTIONNEL";
    public static final String CREER_CONSULTATION = "CREER_CONSULTATION";
    public static final String PLANIFIER_PROCEDURE = "PLANIFIER_PROCEDURE";
    public static final String REDACTION_DE_LAAPC = "REDACTION_DE_L'AAPC";
    public static final String DIFUSION_DE_LAAPC = "DIFUSION_DE_L'AAPC";
    public static final String DEPOT_CANDIDATURES_ET_OFFRES =
            "DEPOT_DES_CANDIDATURES_OFFRES";
    public static final String RETRAIT_DCE = "RETRAIT_DCE_(DEMAT)";
    public static final String DEPOT_CAND_ET_OFFRES_ELECTRONIQUE =
            "DEPOT_DES_CANDIDATURES_OFFRES_(DEMAT)";
    public static final String DIFFUSION_DCE_ELECTRONIQUE =
            "DIFFUSION_DCE_(DEMAT)";
    public static final String GESTION_PROCESSUS_WORKFLOW =
            "GESTION_PROCESSUS_WORKFLOW";
}
