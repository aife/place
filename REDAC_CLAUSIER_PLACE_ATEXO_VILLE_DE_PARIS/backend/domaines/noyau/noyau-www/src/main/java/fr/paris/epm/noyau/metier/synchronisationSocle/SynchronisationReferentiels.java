package fr.paris.epm.noyau.metier.synchronisationSocle;

import com.google.common.base.Strings;
import fr.atexo.mpe.echanges.webservices.client.ClientMpe;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.jaxb.ContratType;
import fr.atexo.mpe.echanges.webservices.client.jaxb.ProcedureType;
import fr.atexo.mpe.echanges.webservices.client.jaxb.ReferentielsType;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.OrganismeCritere;
import fr.paris.epm.noyau.metier.synchronisationSocle.exceptions.SynchronisationException;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeContrat;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SynchronisationReferentiels implements Synchronisable {

    @Resource
    private ClientMpe clientMpe;

    @Value("${mpe.client}")
    private String plateformeUuid;

    @Resource
    private GeneriqueDAO generiqueDAO;

    @Override
    public void synchroniser() throws SynchronisationException {
        try {
            ReferentielsType referentielsType = clientMpe.recupererReferentielsMpe();

            //on commence par la synchro des procedures
            getLogger().info("Début de la synchronisation des procédures");
            Map<String, List<EpmTRefProcedure>> localProceduresByCodeExterne =
                    synchroniserProcedures(referentielsType.getProcedures().getProcedure());
            getLogger().info("Fin de la synchronisation des procédures");

            //ensuite  synchro des contrats
            getLogger().info("Début de la synchronisation des types de contrats");
            synchroniserTypeContrats(referentielsType.getContrats().getContrat(), localProceduresByCodeExterne);
            getLogger().info("Fin de la synchronisation des types de contrats");

        } catch (final ClientMpeException e) {
            throw new SynchronisationException("Une erreur est survenue lors de l'appel du web service pour récupérer la liste des référentiels", e);
        } catch (final Exception e) {
            throw new SynchronisationException("Une erreur imprévue est survenue lors de la synchronisation des référentiels", e);
        }
    }

    /*---------------------------------------- Debut de gestion de synchronisation des procedures RSEM ----------------------------------------*/

    /**
     * methode principale pour la synchronisation des procedures RSEM
     */
    private Map<String, List<EpmTRefProcedure>> synchroniserProcedures(final Collection<ProcedureType> mpeProcedures) {
        getLogger().info("\uD83D\uDEA7 {} procédures provenant de MPE. Voici la liste complète : ", mpeProcedures.size());

        mpeProcedures.forEach(
                procedure -> getLogger().info("✔️ {}", procedure)
        );

        Map<String, List<ProcedureType>> proceduresMPEParCodeExterne = mpeProcedures.stream()
                .filter(procedureType -> null != procedureType.getCodeExterne())
                // On supprime les PA-INF
                .filter(p -> !EpmTRefProcedure.CODE_MAPA_INF_PERSO.equals(p.getAbreviation()) || !EpmTRefProcedure.CODE_MAPA_INF_PERSO.equals(p.getCodeExterne()))
                // On supprime les PA-SUP
                .filter(p -> !EpmTRefProcedure.CODE_MAPA_SUP_PERSO.equals(p.getAbreviation()) || !EpmTRefProcedure.CODE_MAPA_SUP_PERSO.equals(p.getCodeExterne()))
                .collect(Collectors.groupingBy(ProcedureType::getCodeExterne));

        List<ProcedureType> mpeProceduresWithCodeExterneEmpty = proceduresMPEParCodeExterne.get("");
        if (mpeProceduresWithCodeExterneEmpty != null) {
            mpeProceduresWithCodeExterneEmpty.forEach(procedure ->
                    getLogger().warn("La procedure MPE '{}' n'est pas mise à jour car son code externe est vide", procedure));
            proceduresMPEParCodeExterne.remove("");
        } else {
            getLogger().debug("Aucune procédure MPE sans code externe");
        }

        // le procédures personnalisées contiennent soit "PA_INF" soit "PA_SUP" comme code externe
        // mais leurs abreviations sont une valeur unique mais differente "PA_INF" et "PA_SUP"
        Map<String, List<ProcedureType>> proceduresPersonnaliseesMPEParAbreviation = Stream.concat(
                        proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO) != null ?
                                proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO).stream()
                                        .filter(p -> !p.getAbreviation().equals(EpmTRefProcedure.CODE_MAPA_INF_PERSO)) :
                                Stream.empty(),
                        proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO) != null ?
                                proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO).stream()
                                        .filter(p -> !p.getAbreviation().equals(EpmTRefProcedure.CODE_MAPA_SUP_PERSO)) :
                                Stream.empty())
                .collect(Collectors.groupingBy(ProcedureType::getAbreviation));

        // on enlève les procédures personnalisées car on les traite differement
        if (proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO) != null)
            proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO).removeIf(p -> !p.getAbreviation().equals(EpmTRefProcedure.CODE_MAPA_INF_PERSO));
        if (proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO) != null)
            proceduresMPEParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO).removeIf(p -> !p.getAbreviation().equals(EpmTRefProcedure.CODE_MAPA_SUP_PERSO));

        // on récupèrele et prépare les procédure de RSEM
        List<EpmTRefProcedure> localProcedures = generiqueDAO.findAll(EpmTRefProcedure.class);
        Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne = localProcedures.stream()
                .collect(Collectors.groupingBy(EpmTRefProcedure::getCodeExterne));

        // le procédures personnalisées contiennent soit "PA_INF" soit "PA_SUP" comme code externe
        // mais leurs abreviations sont une valeur unique mais differente "PA_INF" et "PA_SUP"
        Map<String, List<EpmTRefProcedure>> proceduresPersonnaliseesLocalesParAbreviation = Stream.concat(
                        proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO) != null ?
                                proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO).stream()
                                        .filter(p -> !p.getLibelleCourt().equals(EpmTRefProcedure.CODE_MAPA_INF_PERSO)) :
                                Stream.empty(),
                        proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO) != null ?
                                proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO).stream()
                                        .filter(p -> !p.getLibelleCourt().equals(EpmTRefProcedure.CODE_MAPA_SUP_PERSO)) :
                                Stream.empty())
                .collect(Collectors.groupingBy(EpmTRefProcedure::getLibelleCourt));

        // on enlève les procédures personnalisées car on les traite differement
        if (proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO) != null)
            proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_INF_PERSO).removeIf(p -> !p.getLibelleCourt().equals(EpmTRefProcedure.CODE_MAPA_INF_PERSO));
        if (proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO) != null)
            proceduresLocalesParCodeExterne.get(EpmTRefProcedure.CODE_MAPA_SUP_PERSO).removeIf(p -> !p.getLibelleCourt().equals(EpmTRefProcedure.CODE_MAPA_SUP_PERSO));

        List<EpmTRefOrganisme> organismes = generiqueDAO.findAll(EpmTRefOrganisme.class);

        synchroniserProcedures(proceduresMPEParCodeExterne, proceduresLocalesParCodeExterne, organismes);
        synchroniserProceduresPersonnalisees(proceduresPersonnaliseesMPEParAbreviation, proceduresPersonnaliseesLocalesParAbreviation, organismes);
        verifierMAPA(localProcedures);
        desactiverProceduresAbsentes(proceduresMPEParCodeExterne, proceduresLocalesParCodeExterne);

        return proceduresLocalesParCodeExterne;
    }

    private void verifierMAPA(List<EpmTRefProcedure> localProcedures) {

        var listeMapaInf = localProcedures.stream()
                .filter(procedure -> EpmTRefProcedure.CODE_MAPA_INF_PERSO.equals(procedure.getLibelleCourt()))
                .filter(procedure -> EpmTRefProcedure.CODE_MAPA_INF_PERSO.equals(procedure.getCodeExterne()))
                .collect(Collectors.toCollection(ArrayDeque::new));

        if (listeMapaInf.size() > 1) {
            getLogger().error("Il existe plus d'une procédure MAPA INF (avec code externe = PA-INF et libelle court / abreviation = PA-INF) côté REDAC . Merci de redresser, en un gardant une seule active");
        } else if (listeMapaInf.isEmpty()) {
            getLogger().error("Aucune procédure MAPA INF (avec code externe = PA-INF et libelle court / abreviation = PA-INF) active côté REDAC. Elle va être crée");

            EpmTRefProcedure epmTRefProcedure = new EpmTRefProcedure();
            epmTRefProcedure.setLibelle("Procédure adaptée < 100 000 Euros HT");
            epmTRefProcedure.setLibelleCourt(EpmTRefProcedure.CODE_MAPA_INF_PERSO);
            epmTRefProcedure.setCodeExterne(EpmTRefProcedure.CODE_MAPA_INF_PERSO);
            epmTRefProcedure.setActif(true);
            epmTRefProcedure.setAttributFormalise("Procédure formalisée");
            epmTRefProcedure.setWorkflow("aoo_2016");
            epmTRefProcedure.setProcedurePersonnalise(false);

            epmTRefProcedure.setEpmTRefOrganismeSet(new HashSet<>(generiqueDAO.findAll(EpmTRefOrganisme.class)));

            try {
                generiqueDAO.persist(epmTRefProcedure);
                getLogger().info("✅ La procédure MAPA INF vient d'être ajouté dans REDACTION (procédure REDACTION = {}).", epmTRefProcedure);
            } catch (Exception ex) {
                getLogger().info("\uD83D\uDCA5 Erreur lors de la création de la procédure MAPA SUP dans REDACTION {}", epmTRefProcedure, ex);
            }
        } else if (!listeMapaInf.getFirst().isActif()) {
            getLogger().error("La procédure MAPA INF (avec code externe = PA-INF et libelle court / abreviation = PA-INF) n'est PAS active côté REDAC. Normal?");
        }

        var listeMapaSup = localProcedures.stream()
                .filter(procedure -> EpmTRefProcedure.CODE_MAPA_SUP_PERSO.equals(procedure.getLibelleCourt()))
                .filter(procedure -> EpmTRefProcedure.CODE_MAPA_SUP_PERSO.equals(procedure.getCodeExterne()))
                .collect(Collectors.toCollection(ArrayDeque::new));

        if (listeMapaSup.size() > 1) {
            getLogger().error("Il existe plus d'une procédure MAPA SUP (avec code externe = PA-SUP et libelle court / abreviation = PA-SUP) côté REDAC . Merci de redresser, en un gardant une seule active");
        } else if (listeMapaSup.isEmpty()) {
            getLogger().error("Aucune procédure MAPA SUP (avec code externe = PA-INF et libelle court / abreviation = PA-SUP) active côté REDAC. Elle va être crée");

            EpmTRefProcedure epmTRefProcedure = new EpmTRefProcedure();
            epmTRefProcedure.setLibelle("Procédure adaptée > 100 000 Euros HT");
            epmTRefProcedure.setLibelleCourt(EpmTRefProcedure.CODE_MAPA_SUP_PERSO);
            epmTRefProcedure.setCodeExterne(EpmTRefProcedure.CODE_MAPA_SUP_PERSO);
            epmTRefProcedure.setActif(true);
            epmTRefProcedure.setAttributFormalise("Procédure formalisée");
            epmTRefProcedure.setWorkflow("aoo_2016");
            epmTRefProcedure.setProcedurePersonnalise(false);

            epmTRefProcedure.setEpmTRefOrganismeSet(new HashSet<>(generiqueDAO.findAll(EpmTRefOrganisme.class)));

            try {
                generiqueDAO.persist(epmTRefProcedure);
                getLogger().info("✅ La procédure MAPA SUP vient d'être ajouté dans REDACTION (procédure REDACTION = {}).", epmTRefProcedure);
            } catch (Exception ex) {
                getLogger().info("\uD83D\uDCA5 Erreur lors de la création de la procédure MAPA SUP dans REDACTION {}", epmTRefProcedure, ex);
            }
        } else if (!listeMapaSup.getFirst().isActif()) {
            getLogger().error("La procédure MAPA INF (avec code externe = PA-INF et libelle court / abreviation = PA-INF) n'est PAS active côté REDAC. Normal?");
        }


    }

    /**
     * Désactive les procédures non communiquées
     *
     * @param proceduresMPEParCodeExterne     procedures communiquées pour mise à jour
     * @param proceduresLocalesParCodeExterne liste complète des procédures locales
     */
    private void desactiverProceduresAbsentes(final Map<String, List<ProcedureType>> proceduresMPEParCodeExterne,
                                              final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne) {

        // On parcourt les procédures REDAC pour désactiver celles qui n'existe plus dans MPE
        for (final Map.Entry<String, List<EpmTRefProcedure>> procedures : proceduresLocalesParCodeExterne.entrySet()) {
            if (!proceduresMPEParCodeExterne.containsKey(procedures.getKey()) || proceduresMPEParCodeExterne.get(procedures.getKey()).size() > 1) {
                procedures.getValue().forEach(procedure -> {
                    if (procedure.isActif()) {
                        getLogger().info("Désactivation de la procédure REDACTION {} qui est active mais non-présente dans MPE", procedure);

                        this.desactiver(procedure);
                    } else {
                        getLogger().info("La procédure REDACTION {} n'est pas présente dans MPE mais est déjà inactive, rien à faire", procedure);
                    }
                });
            }
        }
    }

    /**
     * Désactivation d'une procédure
     *
     * @param procedure la procédure à désactiver
     */
    private void desactiver(final EpmTRefProcedure procedure) {
        procedure.setActif(false);

        try {
            generiqueDAO.merge(procedure);
            getLogger().debug("Procedure REDACTION '{}' correctement désactivée", procedure);
        } catch (final Exception ex) {
            getLogger().debug("Une erreur technique est survenue au moment de la mise à jour de la procedure '{}'", procedure, ex);
        }
    }

    private void synchroniserProcedures(final Map<String, List<ProcedureType>> proceduresMPEParCodeExterne, final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne, final List<EpmTRefOrganisme> organismes) {
        for (Map.Entry<String, List<ProcedureType>> aggregation : proceduresMPEParCodeExterne.entrySet()) {
            if (aggregation.getValue().size() > 1) {
                getLogger().warn("Le code externe '{}' correspond à plusieurs procédures, ce qui n'est pas correct. Celle-ci ne seront pas traitées, le référentiel MPE doit être redressé avant.", aggregation.getKey());

                getLogger().info("\uD83D\uDEA7 {} procédures écartées. Voici la liste : ", aggregation.getValue().size());

                aggregation.getValue().forEach(
                        procedure -> getLogger().info("✖️️ {}", procedure)
                );

                continue;
            } else if (aggregation.getValue().isEmpty()) {
                continue;
            }

            ProcedureType procedureMPE = aggregation.getValue().get(0);

            getLogger().info("\uD83D\uDC49\uD83C\uDFFB Traitement de la procédure MPE {}", procedureMPE);

            List<EpmTRefProcedure> localProcedures = proceduresLocalesParCodeExterne.get(aggregation.getKey());

            if (localProcedures == null) {
                EpmTRefProcedure procedureLocale = creerProcedure(procedureMPE, false);
                if (procedureLocale != null) {
                    proceduresLocalesParCodeExterne.put(aggregation.getKey(), Collections.singletonList(procedureLocale));
                }

                continue;
            }
            if (localProcedures.size() != 1) {
                getLogger().error("Le code externe '{}' n'est pas unique dans REDACTION (présent {} fois). La procedure MPE {} ne sera pas traitée, le référentiel REDACTION doit être redressé avant", aggregation.getKey(), localProcedures.size(), procedureMPE);

                continue;
            }

            EpmTRefProcedure localProcedure = localProcedures.get(0);

            getLogger().info("La procédure MPE {} existe DEJA dans REDACTION.", procedureMPE);

            EpmTRefProcedure epmTRefProcedure = mettreAJourProcedure(procedureMPE, localProcedure, organismes);

            if (epmTRefProcedure != null) {
                proceduresLocalesParCodeExterne.put(aggregation.getKey(), Collections.singletonList(epmTRefProcedure));
            }
        }
    }

    private void synchroniserProceduresPersonnalisees(
            final Map<String, List<ProcedureType>> mpeProceduresPersonnaliseesByAbreviation,
            final Map<String, List<EpmTRefProcedure>> localProceduresPersonnaliseesByAbreviation,
            final List<EpmTRefOrganisme> organismes
    ) {

        for (Map.Entry<String, List<ProcedureType>> aggregate : mpeProceduresPersonnaliseesByAbreviation.entrySet()) {
            if (aggregate.getValue().size() > 1) {
                getLogger().warn("Le code externe '{}' correspond à plusieurs procédures personnalisées, ce qui n'est pas correct. Celle-ci ne seront pas traitées, le référentiel MPE doit être redressé avant.", aggregate.getKey());

                getLogger().info("\uD83D\uDEA7 {} procédures écartées. Voici la liste : ", aggregate.getValue().size());

                aggregate.getValue().forEach(
                        procedure -> getLogger().info("✖️️ {}", procedure)
                );

                continue;
            } else if (aggregate.getValue().isEmpty()) {
                continue;
            }

            ProcedureType mpeProcedure = aggregate.getValue().get(0);

            getLogger().info("\uD83D\uDC49\uD83C\uDFFB Traitement de la procédure personnalisée MPE {}", mpeProcedure);

            List<EpmTRefProcedure> localProcedurePersonnalisee = localProceduresPersonnaliseesByAbreviation.get(aggregate.getKey());
            if (localProcedurePersonnalisee == null) {
                EpmTRefProcedure epmTRefProcedure = creerProcedure(mpeProcedure, true);
                if (epmTRefProcedure != null)
                    localProceduresPersonnaliseesByAbreviation.put(aggregate.getKey(), Collections.singletonList(epmTRefProcedure));
                continue;
            }
            if (localProcedurePersonnalisee.size() != 1) {
                getLogger().error("Le code externe '{}' n'est pas unique dans REDACTION (présent {} fois). La procedure personnalisée MPE {} ne sera pas traitée, le référentiel REDACTION doit être redressé avant", aggregate.getKey(), localProcedurePersonnalisee.size(), mpeProcedure);

                continue;
            }

            getLogger().info("La procédure personnalisée MPE {} existe DEJA dans REDACTION.", mpeProcedure);

            EpmTRefProcedure epmTRefProcedure = mettreAJourProcedure(aggregate.getValue().get(0), localProcedurePersonnalisee.get(0), organismes);
            if (epmTRefProcedure != null) {
                localProceduresPersonnaliseesByAbreviation.put(aggregate.getKey(), Collections.singletonList(epmTRefProcedure));
            }
        }
    }

    /**
     * methode pour create une nouvelle procedure sur la DB RSEM
     */
    private EpmTRefProcedure creerProcedure(final ProcedureType mpeProcedure, boolean procedurePersonnalisee) {
        getLogger().info("La procédure{} MPE {} n'existe pas encore dans REDACTION. Elle doit être créée", procedurePersonnalisee ? "personnalisée" : "", mpeProcedure);

        EpmTRefProcedure epmTRefProcedure = new EpmTRefProcedure();
        epmTRefProcedure.setLibelle(mpeProcedure.getLibelle());
        epmTRefProcedure.setLibelleCourt(mpeProcedure.getAbreviation());
        epmTRefProcedure.setCodeExterne(mpeProcedure.getCodeExterne());
        epmTRefProcedure.setActif(true);
        epmTRefProcedure.setAttributFormalise("Procédure formalisée");
        epmTRefProcedure.setWorkflow("aoo_2016");
        epmTRefProcedure.setProcedurePersonnalise(procedurePersonnalisee);

        OrganismeCritere critere = new OrganismeCritere(plateformeUuid);
        critere.setCodesExternes(mpeProcedure.getAssociationOrganisme().getCodeOrganisme());
        epmTRefProcedure.setEpmTRefOrganismeSet(new HashSet<>(generiqueDAO.findByCritere(critere)));

        try {
            generiqueDAO.persist(epmTRefProcedure);
            getLogger().info("✅ La procédure{} MPE {} vient d'être ajouté dans REDACTION (procédure REDACTION = {}).", procedurePersonnalisee ? "personnalisée" : "", mpeProcedure, epmTRefProcedure);
            return epmTRefProcedure;
        } catch (Exception ex) {
            getLogger().info("\uD83D\uDCA5 Erreur lors de la création de la procédure{} REDACTION {}", procedurePersonnalisee ? "personnalisée" : "", epmTRefProcedure, ex);
            return null;
        }
    }

    /**
     * methode pour mettre à jour une procedure dans la base RSEM
     */
    private EpmTRefProcedure mettreAJourProcedure(final ProcedureType procedureMPE, EpmTRefProcedure procedureLocale, final List<EpmTRefOrganisme> organismes) {
        boolean miseAJourRequise = false;

        //si le libelle est different ===> update
        if (procedureLocale.getLibelle().equals(procedureMPE.getLibelle())) {
            getLogger().debug("Pas de changement du libellé pour la procédure REDACTION {} (le libellé est toujours '{}')", procedureLocale, procedureMPE.getLibelle());
        } else {
            getLogger().debug("Changement du libellé de la procédure REDACTION {} (modification de '{}' à '{}')", procedureLocale, procedureLocale.getLibelle(), procedureMPE.getLibelle());
            procedureLocale.setLibelle(procedureMPE.getLibelle());
            miseAJourRequise = true;
        }
        // si l'abreviation de la procedure systeme est different ==> update
        if (!procedureLocale.isProcedurePersonnalise() && !procedureLocale.getLibelleCourt().equals(procedureMPE.getAbreviation())) {
            getLogger().debug("Changement d'abréviation pour la procédure REDACTION {} (modification de '{}' à '{}')", procedureLocale, procedureLocale.getLibelleCourt(), procedureMPE.getAbreviation());

            procedureLocale.setLibelleCourt(procedureMPE.getAbreviation());
            miseAJourRequise = true;
        } else {
            getLogger().debug("Pas de changement d'abréviation pour la procédure REDACTION {} (le libellé est toujours '{}')", procedureLocale, procedureLocale.getLibelleCourt());
        }
        // l'update des ids des organismes s'il faut
        if (testAssociationOrganismes(procedureMPE, procedureLocale, organismes)) {
            getLogger().debug("Changement des associations des organismes de la procédure REDACTION {} ({} associations coté REDACTION contre {} pour MPE, modification de '{}' à '{}')", procedureLocale, procedureLocale.getEpmTRefOrganismeSet().size(), procedureMPE.getAssociationOrganisme().getCodeOrganisme().size(), procedureLocale.getEpmTRefOrganismeSet().stream().map(EpmTRefOrganisme::getCodeExterne).collect(Collectors.toSet()).stream().sorted().collect(Collectors.toList()),
                    procedureMPE.getAssociationOrganisme().getCodeOrganisme().stream().sorted().collect(Collectors.toList()));

            //si retour vrai on le met pour le flag update
            procedureLocale.getEpmTRefOrganismeSet().clear();
            if (procedureMPE.getAssociationOrganisme() != null && procedureMPE.getAssociationOrganisme().getCodeOrganisme() != null &&
                    !procedureMPE.getAssociationOrganisme().getCodeOrganisme().isEmpty()) {
                OrganismeCritere critere = new OrganismeCritere(plateformeUuid);
                critere.setCodesExternes(procedureMPE.getAssociationOrganisme().getCodeOrganisme());
                procedureLocale.getEpmTRefOrganismeSet().addAll(generiqueDAO.findByCritere(critere));
            }
            miseAJourRequise = true;
        } else {
            getLogger().debug("Pas de changement des associations des organismes de la procédure REDACTION {}", procedureLocale);

        }

        if (miseAJourRequise) {
            getLogger().info("⚠️ La procédure MPE {} doit être mise a jour dans REDACTION ({}).", procedureMPE, procedureLocale);

            try {
                procedureLocale = generiqueDAO.merge(procedureLocale);
                getLogger().info("✅ La procédure MPE {} vient d'être mise a jour dans REDACTION ({}).", procedureMPE, procedureLocale);

                return procedureLocale;
            } catch (Exception ex) {
                getLogger().info("\uD83D\uDCA5 Erreur lors de la mise à jour de la procédure REDACTION {}", procedureLocale, ex);
                return null;
            }
        } else {
            getLogger().info("\uD83D\uDCA4 La procédure MPE {} est DEJA a jour dans REDACTION ({}), rien a faire.", procedureMPE, procedureLocale);

            return null;
        }
    }

    /**
     * verifie la coincidence des association avec les organismes entre la procedure RSEM et la procedure MPE correspondant
     * si ce n'est pas les memes association ==> update des associations de la procedure RSEM avec celles de la procedure mpe
     */
    private boolean testAssociationOrganismes(final ProcedureType mpeProcedure, final EpmTRefProcedure localProcedure, final List<EpmTRefOrganisme> organismes) {
        Set<EpmTRefOrganisme> organismesLocaux = localProcedure.getEpmTRefOrganismeSet();
        ProcedureType.AssociationOrganisme organismesMPE = mpeProcedure.getAssociationOrganisme();
        if (organismesMPE == null || organismesMPE.getCodeOrganisme() == null ||
                organismesMPE.getCodeOrganisme().isEmpty()) {
            if (organismesLocaux == null || organismesLocaux.isEmpty()) {
                // pas d'association recu de MPE et pas d'association trouvé sur RSEM
                // ==> ne rien faire et pas d'update
                return false;
            } else { // pas d'association recu de MPE et presence d'association sur RSEM ==> vider celle ci pourq'elle coincide avec mpe
                organismesLocaux.clear();
                return true;
            }
        } else {
            List<EpmTRefOrganisme> organismesExistant = organismes.stream().filter(organisme -> organismesMPE.getCodeOrganisme().contains(organisme.getCodeExterne())).collect(Collectors.toList());
            Set<String> codeExternesExistants = organismesExistant.stream().map(EpmTRefOrganisme::getCodeExterne).collect(Collectors.toSet());

            boolean result = codeExternesExistants.size() != organismesLocaux.size();

            if (result) {
                getLogger().debug("Les associations des organismes pour la procédure REDACTION {} sont différents en nombre de celles de la procédure MPE après filtrage en base {} ({} associations coté REDACTION contre {} pour MPE)", localProcedure, mpeProcedure, localProcedure.getEpmTRefOrganismeSet().size(), codeExternesExistants.size());

                return true;
            } else {
                List<String> codesOrganismeLocaux = organismesLocaux.stream()
                        .map(EpmTRefOrganisme::getCodeExterne)
                        .collect(Collectors.toList());

                result = !codeExternesExistants.containsAll(codesOrganismeLocaux) || !codesOrganismeLocaux.containsAll(codeExternesExistants);

                if (result) {
                    getLogger().debug("Les associations des organismes pour la procédure REDACTION {} sont différentes de celles de la procédure MPE {} (modification de '{}' à '{}')", localProcedure, mpeProcedure, localProcedure.getEpmTRefOrganismeSet().stream().map(EpmTRefOrganisme::getCodeExterne).collect(Collectors.toSet()).stream().sorted().collect(Collectors.toList()),
                            mpeProcedure.getAssociationOrganisme().getCodeOrganisme().stream().sorted().collect(Collectors.toList()));
                }

                return result;
            }
        }
    }


    /*----------------------------------------- Debut de gestion de synchronisation des type decontrats RSEM -----------------------------------------*/

    /**
     * methode principale pour la synchronisation des contrats
     */
    private void synchroniserTypeContrats(final Collection<ContratType> mpeTypeContrats, final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne) {
        mpeTypeContrats.stream().filter(contrat -> null == contrat.getCodeExterne()).forEach(
                contrat -> getLogger().warn("Le contrat intitule " + contrat.getLibelle() + " n'a pas de code externe")
        );

        Map<String, List<ContratType>> typesContratMPEParCodeExterne = mpeTypeContrats.stream()
                .filter(contrat -> null != contrat.getCodeExterne())
                .collect(Collectors.groupingBy(ContratType::getCodeExterne));

        List<ContratType> typesContratMPESansCodeExterne = typesContratMPEParCodeExterne.get("");
        if (typesContratMPESansCodeExterne != null) {
            typesContratMPESansCodeExterne.forEach(tc ->
                    getLogger().error("Le type de contrat MPE {} n'est pas mis à jour car son code externe est vide", tc));
            typesContratMPEParCodeExterne.remove("");
        } else {
            getLogger().debug("Aucun type de contrat MPE sans code externe");
        }

        List<EpmTRefTypeContrat> typesContratLocaux = generiqueDAO.findAll(EpmTRefTypeContrat.class);
        Map<String, List<EpmTRefTypeContrat>> localTypeContratsByCodeExterne = typesContratLocaux.stream()
                .collect(Collectors.groupingBy(EpmTRefTypeContrat::getCodeExterne));

        synchroniserTypeContrats(typesContratMPEParCodeExterne, localTypeContratsByCodeExterne, proceduresLocalesParCodeExterne);
    }

    private void synchroniserTypeContrats(final Map<String, List<ContratType>> mpeTypeContratsByCodeExterne,
                                          final Map<String, List<EpmTRefTypeContrat>> localTypeContratsByCodeExterne,
                                          final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne) {

        for (Map.Entry<String, List<ContratType>> typesContratMPE : mpeTypeContratsByCodeExterne.entrySet()) {
            if (typesContratMPE.getValue().size() != 1) {
                getLogger().error("Le code externe '{}' correspond à plusieurs types de contrat, ce qui n'est pas correct. Ceux-ci ne seront pas traitées, le référentiel MPE doit être redressé avant.", typesContratMPE.getKey());

                typesContratMPE.getValue().forEach(
                        contrat -> getLogger().info("✖️ {}", contrat)
                );

                continue;
            }


            ContratType typeContratMPE = typesContratMPE.getValue().get(0);

            List<EpmTRefTypeContrat> typesContratLocaux = localTypeContratsByCodeExterne.get(typesContratMPE.getKey());

            var proceduresPersonnaliseesParCodeExterne = generiqueDAO.findAll(EpmTRefProcedure.class)
                    .stream()
                    .filter(EpmTRefProcedure::estProcedurePersonnalisee)
                    .collect(Collectors.groupingBy(EpmTRefProcedure::getCodeExterne));

            getLogger().info("\uD83D\uDC49\uD83C\uDFFB Traitement du type de contrat MPE {}", typeContratMPE);

            try {
                if (null == typesContratLocaux) {
                    getLogger().info("Le type de contrat MPE {} n'existe pas encore dans REDACTION. Il doit être créée", typeContratMPE);

                    ajouterTypeContrat(typeContratMPE, proceduresLocalesParCodeExterne, proceduresPersonnaliseesParCodeExterne);

                    continue;
                }
                if (typesContratLocaux.size() != 1) {
                    getLogger().error("Le code externe '{}' n'est pas unique dans REDACTION (présent {} fois). Le type de contrat MPE {} ne sera pas traité, le référentiel REDACTION doit être redressé avant", typesContratMPE.getKey(), typesContratLocaux.size(), typeContratMPE);

                    continue;
                }

                getLogger().info("Le type de contrat MPE {} existe DEJA dans REDACTION.", typeContratMPE);

                EpmTRefTypeContrat typeContratLocal = mettreAJourTypeContrat(typeContratMPE, typesContratLocaux.get(0), proceduresLocalesParCodeExterne, proceduresPersonnaliseesParCodeExterne);

                if (null != typeContratLocal) {
                    localTypeContratsByCodeExterne.put(typesContratMPE.getKey(), Collections.singletonList(typeContratLocal));
                }

            } catch (Exception ex) {
                getLogger().error("Une erreur imprevue est survenue lors de l'ajout ou de la modification des types contrat", ex);
            }
        }
    }

    /**
     * methode pour create un nouveau type contrat sur la DB RSEM
     */
    private void ajouterTypeContrat(
            final ContratType mpeTypeContrat,
            final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne,
            final Map<String, List<EpmTRefProcedure>> proceduresPersonnaliseesParCodeExterne
    ) {
        getLogger().info("Le type de contrat MPE {} n'existe pas encore dans REDACTION. Il doit être créée", mpeTypeContrat);

        EpmTRefTypeContrat typeContratLocal = new EpmTRefTypeContrat();
        typeContratLocal.setAbreviation(mpeTypeContrat.getAbreviation());
        typeContratLocal.setCodeExterne(mpeTypeContrat.getCodeExterne());
        typeContratLocal.setLibelle(mpeTypeContrat.getLibelle());
        typeContratLocal.setIdsProcedures(associerProcedures(mpeTypeContrat, proceduresLocalesParCodeExterne, proceduresPersonnaliseesParCodeExterne));
        typeContratLocal.setActif(true);

        try {
            generiqueDAO.persist(typeContratLocal);
            getLogger().info("✅ Le type de contrat MPE {} vient d'être ajouté dans REDACTION (procédure REDACTION = {}).", mpeTypeContrat, typeContratLocal);
        } catch (Exception ex) {
            getLogger().info("\uD83D\uDCA5 Erreur lors de la création du type de contrat REDACTION {}", typeContratLocal, ex);
        }
    }

    /**
     * methode pour mettre à jour un type contrat dans la base RSEM
     */
    private EpmTRefTypeContrat mettreAJourTypeContrat(
            final ContratType mpeTypeContrat,
            EpmTRefTypeContrat typeContratLocal,
            final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne,
            final Map<String, List<EpmTRefProcedure>> proceduresPersonnaliseesParCodeExterne

    ) {
        boolean miseAJourRequise = false;

        //si le libelle est different ===> update
        String libelle = mpeTypeContrat.getLibelle();
        if (libelle == null || !libelle.equals(typeContratLocal.getLibelle())) {
            typeContratLocal.setLibelle(libelle);
            miseAJourRequise = true;
        }
        //si l'abreviation est different ===> update
        String abreviation = mpeTypeContrat.getAbreviation();
        if (abreviation == null || !abreviation.equals(typeContratLocal.getAbreviation())) {
            typeContratLocal.setAbreviation(abreviation);
            miseAJourRequise = true;
        }
        // l'update des asociations avec les procedures ici
        if (verifierAssociationProcedures(mpeTypeContrat, typeContratLocal.getIdsProcedures(), proceduresLocalesParCodeExterne, proceduresPersonnaliseesParCodeExterne)) {
            //si retour vrai on le met pour le flag update
            getLogger().info("⚠️ Le type de contrat MPE {} doit être mise a jour dans REDACTION ({}).", mpeTypeContrat, typeContratLocal);
            miseAJourRequise = true;
        }

        if (miseAJourRequise) {
            typeContratLocal = generiqueDAO.merge(typeContratLocal);
            getLogger().info("✅ Le type de contrat MPE {} vient d'être mise a jour dans REDACTION ({}).", mpeTypeContrat, typeContratLocal);
            return typeContratLocal;
        } else {
            return null;
        }
    }

    /**
     * methode pour gerer les association entre con trat et procedures par organisme
     */
    private boolean verifierAssociationProcedures(
            final ContratType contratTypeMPE,
            final Set<Integer> idsProcedures,
            final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne,
            final Map<String, List<EpmTRefProcedure>> proceduresPersonnaliseesParCodeExterne
    ) {

        boolean resultat;

        ContratType.AssociationProcedures associationProceduresMPE = contratTypeMPE.getAssociationProcedures();

        if (associationProceduresMPE == null || associationProceduresMPE.getAssociationProcedure() == null
                || associationProceduresMPE.getAssociationProcedure().isEmpty()) {

            if (idsProcedures == null || idsProcedures.isEmpty()) {
                getLogger().warn("Le type de contrat MPE {} n'est associé à aucune procedure, idem dans redaction, pas de mise à jour", contratTypeMPE);
            } else {
                getLogger().warn("Le type de contrat MPE {} n'est associé à aucune procedure, mais il est associé a des procedures dans REDACTION, elles doivent être supprimées", contratTypeMPE);

                idsProcedures.clear();
            }

            resultat = idsProcedures == null || idsProcedures.isEmpty();
        } else {
            Set<Integer> mpeIdsProcedures = associerProcedures(contratTypeMPE, proceduresLocalesParCodeExterne, proceduresPersonnaliseesParCodeExterne);

            if (mpeIdsProcedures.size() != idsProcedures.size() || !idsProcedures.containsAll(mpeIdsProcedures) || !mpeIdsProcedures.containsAll(idsProcedures)) {
                idsProcedures.clear();
                idsProcedures.addAll(mpeIdsProcedures);
            }

            getLogger().debug("Le type de contrat MPE {} à des procédures associées", contratTypeMPE);

            resultat = true;
        }

        return resultat;
    }

    private Set<Integer> associerProcedures(
            final ContratType typeContratMPE,
            final Map<String, List<EpmTRefProcedure>> proceduresLocalesParCodeExterne,
            final Map<String, List<EpmTRefProcedure>> proceduresPersonnaliseesParCodeExterne
    ) {
        Set<Integer> ids;

        ContratType.AssociationProcedures associationProceduresMPE = typeContratMPE.getAssociationProcedures();

        var existeProcedureLiees = associationProceduresMPE.getAssociationProcedure()
                .stream()
                .map(ContratType.AssociationProcedures.AssociationProcedure::getCodeExterneProcedure)
                .anyMatch(codeExterne -> !Strings.isNullOrEmpty(codeExterne));

        if (existeProcedureLiees) {
            ids = associationProceduresMPE.getAssociationProcedure().stream()
                    .map(ContratType.AssociationProcedures.AssociationProcedure::getCodeExterneProcedure)
                    .distinct()
                    .filter(codeExterne -> !Strings.isNullOrEmpty(codeExterne))
                    .map(codeExterne -> {
                        getLogger().warn("Liste des procédures pour le type de contrat '{}' : ", typeContratMPE.getCodeExterne());

                        var procedures = proceduresLocalesParCodeExterne.get(codeExterne);
                        if (procedures != null)
                            procedures.forEach(
                                    procedure -> getLogger().info("✔️ {}", procedure)
                            );

                        return procedures;
                    })
                    .filter(Objects::nonNull)
                    .flatMap(List::stream)
                    .map(EpmTRefProcedure::getId)
                    .collect(Collectors.toSet());
        } else {
            getLogger().warn("Le code externe est vide pour le type de contrat '{}', association de toutes les procedures", typeContratMPE.getCodeExterne());

            ids = proceduresLocalesParCodeExterne
                    .values()
                    .stream()
                    .flatMap(Collection::stream)
                    .map(EpmTRefProcedure::getId)
                    .collect(Collectors.toSet());
        }

        // Dans tous les cas, on ajoute les procedures personnalisées
        ids.addAll(
                proceduresPersonnaliseesParCodeExterne.values()
                        .stream()
                        .flatMap(Collection::stream)
                        .map(EpmTRefProcedure::getId)
                        .collect(Collectors.toSet())
        );

        return ids;
    }

    public void setClientMpe(ClientMpe clientMpe) {
        this.clientMpe = clientMpe;
    }

    public void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

}
