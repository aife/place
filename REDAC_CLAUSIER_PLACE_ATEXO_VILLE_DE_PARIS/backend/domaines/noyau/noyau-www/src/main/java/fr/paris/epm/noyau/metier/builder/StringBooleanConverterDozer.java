/**
 * 
 */
package fr.paris.epm.noyau.metier.builder;

import org.dozer.CustomConverter;

import fr.paris.epm.noyau.commun.Constantes;

/**
 * Converter utilisé par Dozer. Il permet de convertir entre boolean et String
 * avec 'oui' et 'non'.
 * 
 * @author Xuesong
 * 
 */
public class StringBooleanConverterDozer implements CustomConverter {

	@Override
	public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
		if (source == null) {
			return null;
		}

		if (source instanceof String) {
			String valeur = (String) source;

			// correction des conneries de XGAO, putain (perdu 3 jours merde)
			if (valeur.equals("true"))
				return true;
			else if (valeur.equals("false"))
				return false;

			if (Constantes.OUI.equalsIgnoreCase(valeur)) {
				return Boolean.TRUE;
			} else if (Constantes.NON.equalsIgnoreCase(valeur)) {
				return Boolean.FALSE;
			}
		} else if (source instanceof Boolean) {
			Boolean valeur = (Boolean) source;
			if (Boolean.TRUE.equals(valeur)) {
				return Constantes.OUI;
			} else if (Boolean.FALSE.equals(valeur)) {
				return Constantes.NON;
			}
		} 

		throw new IllegalStateException("La valeur est inconnue");
	}

}

