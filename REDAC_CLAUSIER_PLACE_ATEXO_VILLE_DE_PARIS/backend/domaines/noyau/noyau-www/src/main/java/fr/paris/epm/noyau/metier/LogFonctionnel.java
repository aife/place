package fr.paris.epm.noyau.metier;

/**
 * @author JOR
 */
public class LogFonctionnel {
    /**
     * Service pour construir le message à afficher dans le loggeur
     * @param prenomUtilisateur prenom de l'utilisateur
     * @param nomUtilisateur nom de l'utilisateur
     * @param operation operation en cours
     * @param refConsultation ref de la consultation en cours
     * @return le message à afficher
     */
    public final static String creerMessageLog(
            final String prenomUtilisateur, String nomUtilisateur,
            String operation, String refConsultation, String etapeWorkFlow) {
        StringBuffer buff = new StringBuffer();
        buff.append(prenomUtilisateur);
        buff.append(" ");
        buff.append(nomUtilisateur);
        buff.append(", ");
        buff.append(operation);
        buff.append(", ");
        buff.append(refConsultation);
        if (etapeWorkFlow != null && etapeWorkFlow.length() > 0) {
            buff.append(", ");
            buff.append(etapeWorkFlow);
        }
        return buff.toString();
    }
}
