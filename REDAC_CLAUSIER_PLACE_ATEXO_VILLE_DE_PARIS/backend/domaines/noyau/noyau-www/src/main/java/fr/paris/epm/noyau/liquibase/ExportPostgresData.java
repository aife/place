package fr.paris.epm.noyau.liquibase;

import fr.paris.epm.noyau.liquibase.mysql.structure.Table;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static fr.paris.epm.noyau.liquibase.MysqlMigrationConstants.*;

public class ExportPostgresData implements CustomTaskChange {

    private static final Logger LOG = LoggerFactory.getLogger(ExportPostgresData.class);
    private static final List<Table> postgresTables = new ArrayList<>();
    private String schemaQuery = "SELECT * FROM information_schema.tables where table_name not like 'pgstat%' and table_name not like 'jbpm_%' and table_name not like 'databasechangeloglock%' and table_type='BASE TABLE' and table_schema in ('public','redaction','consultation','commission','echanges','referentiel') order by table_name";

    private void populateQueue(JdbcConnection databaseConnection) {
        try (ResultSet schemaResultSet = databaseConnection.createStatement()
                .executeQuery(schemaQuery)) {
            while (schemaResultSet.next()) {
                String newTableName = "";
                String tableSchema = schemaResultSet.getString("table_schema");
                String tableName = schemaResultSet.getString("table_name");
                Table table = new Table("public", tableSchema, tableName);
                // les tables postgres sont toujours en minuscule
                if (tableSchema.equals("public")) {
                    newTableName = tableName;
                } else {
                    newTableName = tableSchema + "." + tableName;
                }
                String newFilePath = EXPORT_PATH + '/' + newTableName + ".csv";
                table.setCsvFile(newFilePath);
                int nbRows = 0;
                try (ResultSet resultSet = databaseConnection.createStatement()
                        .executeQuery(String.format("SELECT count(*) from %s.%s", tableSchema, tableName))) {
                    while (resultSet.next()) {
                        nbRows = resultSet.getInt(1);
                    }

                }
                table.setNbRows(nbRows);
                postgresTables.add(table);
            }
        } catch (DatabaseException | SQLException e) {
            LOG.error("Erreur lors de l'import", e);
        }

    }

    private Table processTable(JdbcConnection databaseConnection, Table table) throws CustomChangeException {
        table.setStart(LocalDateTime.now());
        String columnsStatement = String.format("SELECT * FROM information_schema.columns WHERE table_schema ='%s' AND table_name = '%s'", table.getSchema(), table.getName());
        List<String> columns = new ArrayList<>();
        try (ResultSet columnsResultSet = databaseConnection.createStatement()
                .executeQuery(columnsStatement)) {
            while (columnsResultSet.next()) {
                String columnName = columnsResultSet.getString("column_name");
                String columnType = columnsResultSet.getString("data_type");
                String columnAlias = columnName;
                if (columnType.equals("boolean")) {
                    columnAlias += ("::int");
                }
                columns.add(columnAlias);
            }
        } catch (DatabaseException | SQLException ex) {
            throw new CustomChangeException("Erreur de récupération des colonnes pour la tables :" + table.getQualifiedTableName(), ex);
        }
        try (Statement dumpStatement = databaseConnection.createStatement()) {
            String statement = String.format("COPY (SELECT %s FROM %s.%s) TO '%s' WITH (FORMAT CSV,delimiter E'\\t',NULL 'NULL', HEADER)", String.join(",", columns), table.getSchema(), table.getName(), table.getCsvFile());
            LOG.info("COPY statement :{}", statement);
            dumpStatement.execute(statement);
        } catch (DatabaseException | SQLException ex) {
            throw new CustomChangeException("Erreur de génération de csv pour la table : " + table.getQualifiedTableName(), ex);
        }

        table.setEnd(LocalDateTime.now());
        return table;
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        Path exPath = Paths.get(EXPORT_PATH);
        if (!exPath.toFile().exists()) {
            LOG.info("Création du répertoire des fichiers CSV : {} pour la base de données : {}", exPath, database.getDefaultSchemaName());
            try {
                Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxrwxrwx");
                Files.createDirectory(exPath, PosixFilePermissions.asFileAttribute(permissions));
                exPath.toFile().setWritable(true, false);
                exPath.toFile().setReadable(true, false);

            } catch (IOException e) {
                throw new CustomChangeException("Impossible de créer le répertoire", e);
            }
        }
        JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();
        populateQueue(databaseConnection);
        ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
        for (final Table singleTable : postgresTables) {
            executor.execute(() -> {
                String threadName = Thread.currentThread()
                        .getName();
                LOG.info("Thread {} -> Génération des fichiers CSV pour la table : {}", threadName, singleTable.getQualifiedTableName());
                try {
                    processTable(databaseConnection, singleTable);
                } catch (CustomChangeException e) {
                    LOG.error("erreur", e);
                }
            });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            Thread.currentThread()
                    .interrupt();
            throw new CustomChangeException("Impossible d'éxécuter l'export des données", e);
        }
        LOG.info("Fin d'exécution de toutes les taches d'export");
        writeCSVResults(postgresTables, POSTGRES_RESULTS_FILE);
    }


    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {
        // setUp
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // set file opener
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
