/**
 * $Id$
 */
package fr.paris.epm.utilitaire;

/**
 * Classe utilitaire représentant le POJO d'une table minimale en base. Cette
 * classe est utile pour lors des casts.
 * @author Guillaume Béraudo
 * @version $Revision$,
 *          $Date$,
 *          $Author$
 */
public class EpmTMinimale implements java.io.Serializable {

    // Propriétés

     /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Un identifiant numérique.
     */
    private int id;

    /**
     * @return identifiant
     */
    public final int getId() {
        return id;
    }

    /**
     * @param identifiant identifiant
     */
    public final void setId(final int identifiant) {
        this.id = identifiant;
    }
}
