package fr.paris.epm.noyau.liquibase;

import liquibase.change.custom.CustomSqlChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import liquibase.statement.SqlStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * on appelle cette class dans le projet RSEM-Client
 */
public class RepriseTokenUtilisateur  implements CustomSqlChange {

    private ResourceAccessor resourceAccessor;

    private static final Logger logger = LoggerFactory.getLogger(RepriseTokenUtilisateur.class);

    private String environnement;

    public String getEnvironnement() {
        return environnement;
    }

    public void setEnvironnement(String environnement) {
        this.environnement = environnement;
    }

    @Override
    public SqlStatement[] generateStatements(Database database) throws CustomChangeException {
        logger.info("Début de l'initialisation des token utilisateur Paris...");
        List<SqlStatement> statements = new ArrayList<>();
        try {
            JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();
            Statement smtSelect = databaseConnection.createStatement();
            ResultSet resultSet = smtSelect.executeQuery("SELECT * FROM consultation.epm__t_utilisateur where actif=true ");
            String urlEPM = "paris-epm-recette.local-trust.com";
            if (environnement.equals("prod")) {
                urlEPM = "m13-epm.apps.paris.fr";
            }
            while (resultSet.next()) {
                int idUtilisateur = resultSet.getInt("id_utilisateur");
                String uuid = UUID.randomUUID().toString();
                Statement smtUpdate = databaseConnection.createStatement();
                smtUpdate.executeUpdate("UPDATE consultation.epm__t_utilisateur SET mot_de_passe = '" + uuid + "' WHERE id_utilisateur = " + idUtilisateur);

                final String mailDestinataireTo = resultSet.getString("courriel");
                final String mailContenu = "Madame, Monsieur, \n\n" +
                        "Dans le cadre de la migration de l'hébergement de la solution EPM, veuillez trouvez ci-dessous le lien vous permettant d'accéder à la nouvelle plateforme.\n" +
                        "Lors du clic sur ce lien, vous accéderez à la page de connexion à l'application avec votre identifiant pré-rempli.\n" +
                        "Nous vous invitons à saisir un mot de passe qui sera votre nouveau mot de passe afin de pouvoir vous connecter à la nouvelle plateforme. Pensez à bien le conserver.\n" +
                        "Seuls les profils administrateur pourront modifier le mot de passe après votre connexion à l'application. \n\n" +
                        "Lien d’accès :\n"+
                        "https://" + urlEPM + "/epm.passation/login.epm?token=" + uuid + "&login=" + resultSet.getString("identifiant")+"\n\n" +
                        "Bien cordialement,\n\n" +
                        "Les équipes ATEXO.";
                final String mailObjet = "Réinitialisation de votre mot de passe EPM";
                final String mailFrom = "noreply@atexo.com";

                JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
                mailSender.setHost("localhost");
                mailSender.setPort(25);


                MimeMessagePreparator preparator = mimeMessage -> {
                    mimeMessage.setHeader("Content-Transfert-Encoding", "8bit");
                    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");

                    if (mailDestinataireTo != null)
                        message.setTo(mailDestinataireTo);
                    message.setFrom(mailFrom);
                    message.setSubject(mailObjet);
                    message.setText(mailContenu);
                };
                mailSender.send(preparator);
            }
        } catch (Exception e) {
            throw new CustomChangeException(e);
        }
        logger.info("Fin de l'initialisation des token utilisateur Paris...");
        return statements.toArray(new SqlStatement[0]);
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        this.resourceAccessor = resourceAccessor;
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }
}
