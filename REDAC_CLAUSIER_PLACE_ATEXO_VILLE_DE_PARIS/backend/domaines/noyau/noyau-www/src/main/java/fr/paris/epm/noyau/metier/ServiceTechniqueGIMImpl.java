package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.metier.technique.TableStockageUtilisateurExterneEPM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implémentation du service technique.
 * @author Mounthei KHAM
 * @author Guillaume BERAUDO
 * @version $Revision$, $Date$, $Author$
 */
public class ServiceTechniqueGIMImpl extends BaseGIMImpl implements ServiceTechniqueGIM {

    /**
     * Le LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ServiceTechniqueGIMImpl.class);


    /**
	 * Table utilisé pour le caclul de la taille du dce d'une consultation. clé : id de la consultation String valeur : taille du dce String.
	 */
	private final TableStockageUtilisateurExterneEPM tableStockageUtilisateurExterneEPM = TableStockageUtilisateurExterneEPM.getInstance();


    public final UtilisateurExterneConsultation getUtilisateurExterneConsultation(final String identifiantExterne) {
        return tableStockageUtilisateurExterneEPM.recuperer(identifiantExterne);
    }


}
