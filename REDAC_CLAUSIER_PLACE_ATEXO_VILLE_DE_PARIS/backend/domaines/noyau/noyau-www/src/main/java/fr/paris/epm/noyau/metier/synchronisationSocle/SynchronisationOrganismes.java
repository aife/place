package fr.paris.epm.noyau.metier.synchronisationSocle;

import fr.atexo.mpe.echanges.webservices.client.ClientMpe;
import fr.atexo.mpe.echanges.webservices.client.beans.OrganismeAvecLogoBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.jaxb.LogoType;
import fr.atexo.mpe.echanges.webservices.client.jaxb.OrganismeType;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.LogoOrganismeCritere;
import fr.paris.epm.noyau.metier.OrganismeCritere;
import fr.paris.epm.noyau.metier.PouvoirAdjudicateurCritere;
import fr.paris.epm.noyau.metier.synchronisationSocle.exceptions.SynchronisationException;
import fr.paris.epm.noyau.persistance.EpmTLogoOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefPouvoirAdjudicateur;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
public class SynchronisationOrganismes implements Synchronisable {

	@Resource
	private ClientMpe clientMpe;

	@Resource
	private GeneriqueDAO generiqueDAO;


	@Value("${mpe.client}")
	private String plateformeUuid;

	/**
	 * Synchroniser les organisme de coté MPE, y compris le logo attaché à l'organisme.
	 */
	@Override
	public void synchroniser() throws SynchronisationException {
		try {
			List<OrganismeType> listeAcronymesOrganismes = clientMpe.recupererListeOrganismes();
			OrganismeCritere critere = new OrganismeCritere(null);
			critere.setWithoutPlateformeUuid(true);
			final List<EpmTRefOrganisme> all = generiqueDAO.findByCritere(critere);

			all.stream().filter(epmTRefOrganisme -> epmTRefOrganisme.getPlateformeUuid() == null).forEach(epmTRefOrganisme -> {
				epmTRefOrganisme.setPlateformeUuid(plateformeUuid);
				generiqueDAO.merge(epmTRefOrganisme);
			});

			if (listeAcronymesOrganismes == null || listeAcronymesOrganismes.isEmpty()) {
				return;
			}

			// enregistrer les organisme un par un
			for (final OrganismeType organismeMpe : listeAcronymesOrganismes) {
				try {

					final EpmTRefOrganisme organismeRedac = enregistrerOrganismeEtLogo(organismeMpe);
					updatePouvoirAdjudicateur(organismeRedac);
				} catch (final RuntimeException e) {
					getLogger().error("Erreur lors de la synchronisation de l'organisme : {}", organismeMpe.getAcronyme(), e);
				}
			}
		} catch (final ClientMpeException e) {
			throw new SynchronisationException("Une erreur est survenue lors de l'appel du web service pour récupérer la liste des organismes", e);
		} catch (final Exception e) {
			throw new SynchronisationException("Une erreur imprévue est survenue lors de la synchronisation des organismes", e);
		}
	}

	private EpmTLogoOrganisme chercherLogoOrganisme(final Integer idOrganisme) {
		final LogoOrganismeCritere critere = new LogoOrganismeCritere();
		critere.setIdOrganisme(idOrganisme);
		final List<EpmTLogoOrganisme> listeEpmTLogoOrganisme = generiqueDAO.findByCritere(critere);
		if (listeEpmTLogoOrganisme != null && !listeEpmTLogoOrganisme.isEmpty()) {
			return listeEpmTLogoOrganisme.get(0);
		}
		return null;
	}

	/**
	 * Enregistrer les données d'un organisme et son logo s'il a été changé.
	 *
	 * @param organismeMpe objet JAXB retourné par WS
	 */
	private EpmTRefOrganisme enregistrerOrganismeEtLogo(final OrganismeType organismeMpe) {
		// mettre à jour ou insérer les données de l'organisme
		final EpmTRefOrganisme organismeEpm = mettreAJourOrganisme(organismeMpe);

		final LogoType logoMpe = organismeMpe.getLogo();
		final EpmTLogoOrganisme logoEpm = chercherLogoOrganisme(organismeEpm.getId());
		if (logoMpe != null) {
			// si le logo a été changé selon checksum, on sauvegarde de nouveau le logo
			if (logoEpm == null || !Objects.equals(logoMpe.getMd5Checksum(), logoEpm.getMd5Checksum())) {
				mettreAJourLogo(organismeEpm.getId(), organismeMpe);
			}
			// si le logo Mpe est null et le logo epm non null, on supprimme le logo epm
		} else if (logoEpm != null) {
			generiqueDAO.delete(logoEpm);
		}
		return organismeEpm;
	}

	/**
	 * Enregistrer un organisme s'il est pas encore sauvegardé ou mettre à jour un organisme s'il
	 * est déjà sauvegardé.
	 *
	 * @param organismeMpe objet JAXB retourné par WS
	 * @return objet persisté
	 */
	private EpmTRefOrganisme mettreAJourOrganisme(final OrganismeType organismeMpe) {
		final String acronymeOrganisme = organismeMpe.getAcronyme();

		final OrganismeCritere organismeCritere = new OrganismeCritere(plateformeUuid);
		organismeCritere.setCodesExternes(acronymeOrganisme);

		final EpmTRefOrganisme epmTRefOrganisme;

		final boolean ajout;
		final List<EpmTRefOrganisme> organismes = generiqueDAO.findByCritere(organismeCritere);
		if (organismes != null && !organismes.isEmpty()) {
			epmTRefOrganisme = organismes.get(0);
			ajout = false;
		} else {
			epmTRefOrganisme = new EpmTRefOrganisme();
			epmTRefOrganisme.setCodeExterne(acronymeOrganisme);
			ajout = true;
		}
		epmTRefOrganisme.setPlateformeUuid(plateformeUuid);
		epmTRefOrganisme.setLibelle(organismeMpe.getDenomination());

		final EpmTRefOrganisme resultat = generiqueDAO.merge(epmTRefOrganisme);

		if (ajout) {
			getLogger().info("L'organisme '{}' (libelle = {}, code externe = {}) vient d'être ajouté avec succès", epmTRefOrganisme.getId(), epmTRefOrganisme.getLibelle(), epmTRefOrganisme.getCodeExterne());
		} else {
			getLogger().info("L'organisme '{}' (libelle = {}, code externe = {}) existe déjà, il vient d'être mis a jour", epmTRefOrganisme.getId(), epmTRefOrganisme.getLibelle(), epmTRefOrganisme.getCodeExterne());
		}

		return resultat;
	}

	/**
	 * Appeler le WS MPE pour récupérer l'image logo, ensuite enregistrer l'image.
	 */
	private void mettreAJourLogo(final int idOrganismeEpm, final OrganismeType organismeMpe) {
		OrganismeAvecLogoBean organismeAvecLogo;
		try {
			organismeAvecLogo = clientMpe.recupererOrganisme(organismeMpe.getAcronyme());
		} catch (final ClientMpeException e) {
			throw new TechnicalNoyauException("Une erreur est survenue lors de l'appel du web service MPE pour récupérer le logo de l'organisme", e);
		}

		if (organismeAvecLogo.getFichierLogo() == null) {
			return;
		}

		final byte[] fichierByteLogo;
		try {
			fichierByteLogo = FileUtils.readFileToByteArray(organismeAvecLogo.getFichierLogo());
		} catch (final IOException e) {
			throw new TechnicalNoyauException(e);
		}
		final LogoType logoMpe = organismeMpe.getLogo();
		final String extension = FilenameUtils.getExtension(logoMpe.getNom());
		final String md5Checksum = logoMpe.getMd5Checksum();

		EpmTLogoOrganisme logoOrganisme = chercherLogoOrganisme(idOrganismeEpm);
		if (logoOrganisme == null) {
			logoOrganisme = new EpmTLogoOrganisme();
			logoOrganisme.setIdOrganisme(idOrganismeEpm);
		}
		logoOrganisme.setFichierLogo(fichierByteLogo);
		logoOrganisme.setTypeFichierLogo(extension);
		logoOrganisme.setMd5Checksum(md5Checksum);

		generiqueDAO.merge(logoOrganisme);
	}

	public void setClientMpe(final ClientMpe valeur) {
		clientMpe = valeur;
	}

	private void updatePouvoirAdjudicateur(final EpmTRefOrganisme organismeRedac) {
		final PouvoirAdjudicateurCritere pouvoirAdjudicateurCritere = new PouvoirAdjudicateurCritere();
		pouvoirAdjudicateurCritere.setCodeExterne(organismeRedac.getCodeExterne());

		final EpmTRefPouvoirAdjudicateur epmTRefPouvoirAdjudicateur;

		final boolean ajout;
		final List<EpmTRefPouvoirAdjudicateur> pouvoirAdjudicateurs = generiqueDAO.findByCritere(pouvoirAdjudicateurCritere);
		if (pouvoirAdjudicateurs != null && !pouvoirAdjudicateurs.isEmpty()) {
			epmTRefPouvoirAdjudicateur = pouvoirAdjudicateurs.get(0);
			ajout = false;
		} else {
			epmTRefPouvoirAdjudicateur = new EpmTRefPouvoirAdjudicateur();
			epmTRefPouvoirAdjudicateur.setCodeExterne(organismeRedac.getCodeExterne());
			ajout = true;
		}
		epmTRefPouvoirAdjudicateur.setLibelle(organismeRedac.getLibelle());
		epmTRefPouvoirAdjudicateur.setLibelleCourt(organismeRedac.getCodeExterne());
		epmTRefPouvoirAdjudicateur.setAcronymeOrganisme(organismeRedac.getCodeExterne());
		epmTRefPouvoirAdjudicateur.setOrganismeMPE(organismeRedac.getCodeExterne());
		epmTRefPouvoirAdjudicateur.setCodeGo(organismeRedac.getCodeExterne());
		epmTRefPouvoirAdjudicateur.setCodeContrat(organismeRedac.getCodeExterne());
		epmTRefPouvoirAdjudicateur.setActif(true);
		epmTRefPouvoirAdjudicateur.setCommission(false);
		epmTRefPouvoirAdjudicateur.setGroupement(false);
		epmTRefPouvoirAdjudicateur.setMembre(false);

		generiqueDAO.merge(epmTRefPouvoirAdjudicateur);

		if (ajout) {
			getLogger().info("Le pouvoir adjudicateur '{}' (libelle = {}, code externe = {}) vient d'être ajouté avec succès", epmTRefPouvoirAdjudicateur.getId(), epmTRefPouvoirAdjudicateur.getLibelle(), epmTRefPouvoirAdjudicateur.getCodeExterne());
		} else {
			getLogger().info("Le pouvoir adjudicateur '{}' (libelle = {}, code externe = {}) existe déjà, il vient d'être mis a jour", epmTRefPouvoirAdjudicateur.getId(), epmTRefPouvoirAdjudicateur.getLibelle(), epmTRefPouvoirAdjudicateur.getCodeExterne());
		}
	}

	public void setGeneriqueDAO(final GeneriqueDAO generiqueDAO) {
		this.generiqueDAO = generiqueDAO;
	}
}
