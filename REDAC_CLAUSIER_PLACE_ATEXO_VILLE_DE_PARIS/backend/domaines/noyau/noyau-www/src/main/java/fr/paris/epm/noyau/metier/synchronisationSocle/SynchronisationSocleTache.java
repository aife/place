package fr.paris.epm.noyau.metier.synchronisationSocle;

import fr.paris.epm.noyau.metier.synchronisationSocle.exceptions.SynchronisationException;
import org.springframework.stereotype.Service;

import java.util.TimerTask;

/**
 * Récupère les utilisateurs de mpe pour les ajouter dans la base rsem Cette
 * classe s'execute régulièrement selon un interval donné.
 *
 * @author MGA
 */
@Service
public class SynchronisationSocleTache extends TimerTask implements Synchronisation, Loggable {

	/**
	 * Pour synchroniser les agents de MPE
	 */
	private Synchronisable synchronisationAgents;

	/**
	 * Pour synchroniser les organismes de MPE
	 */
	private Synchronisable synchronisationOrganismes;

	/**
	 * Pour la synchronisation des procedure et contrats MPE
	 */
	private Synchronisable synchronisationReferentiels;

	/**
	 * Méthode de synchronisation déclenchée régulierement.
	 */
	@Override
	public void run() {
		getLogger().info("------------------------ Debut de la synchronisation REDAC ------------------------");

		try {
			getLogger().info("Début de la synchronisation des ORGANISMES");

			synchronisationOrganismes.synchroniser();
		} catch ( final SynchronisationException e ) {
			getLogger().error("Une erreur est survenue lors de la synchronisation des ORGANISMES", e);
		} finally {
			getLogger().info("Fin de la synchronisation des ORGANISMES");
		}

		try {
			getLogger().info("Début de la synchronisation AGENTS");

			synchronisationAgents.synchroniser();
		} catch ( final SynchronisationException e ) {
			getLogger().error("Une erreur est survenue lors de la synchronisation des AGENTS", e);
		} finally {
			getLogger().info("Fin de la synchronisation des AGENTS");
		}

		try {
			getLogger().info("Début de la synchronisation des REFERENTIELS");

			synchronisationReferentiels.synchroniser();
		} catch ( final SynchronisationException e ) {
			getLogger().error("Une erreur est survenue lors de la synchronisation des REFERENTIELS", e);
		} finally {
			getLogger().info("Fin de la synchronisation des REFERENTIELS");
		}

		getLogger().info("------------------------ Fin de la synchronisation REDAC ------------------------");
	}

	public void setSynchronisationAgents( final Synchronisable synchronisationAgents ) {
		this.synchronisationAgents = synchronisationAgents;
	}

	public void setSynchronisationOrganismes( final Synchronisable synchronisationOrganismes ) {
		this.synchronisationOrganismes = synchronisationOrganismes;
	}

	public void setSynchronisationReferentiels( final Synchronisable synchronisationReferentiels ) {
		this.synchronisationReferentiels = synchronisationReferentiels;
	}

	@Override
	public void synchronize() {
		this.run();
	}
}
