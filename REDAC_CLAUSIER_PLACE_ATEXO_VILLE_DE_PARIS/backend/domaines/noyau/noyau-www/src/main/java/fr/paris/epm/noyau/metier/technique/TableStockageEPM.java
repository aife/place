package fr.paris.epm.noyau.metier.technique;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;

/**
 * Bean dans lequel est stocké des informations temporaires de l'application.
 * notemment : 
 *  - les idConsultation / taille du dce zippé.
 *  - vérification de la mise à jour des référentiels / selects.
 * @author Léon Barsamian
 *
 */
public class TableStockageEPM extends ConcurrentHashMap {

    /**
     * Serialisation.
     */
    private static final long serialVersionUID = 20004061426512022L;

}
