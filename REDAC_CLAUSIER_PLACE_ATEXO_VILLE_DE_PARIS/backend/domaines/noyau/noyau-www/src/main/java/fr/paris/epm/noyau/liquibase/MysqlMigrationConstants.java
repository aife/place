package fr.paris.epm.noyau.liquibase;

import fr.paris.epm.noyau.liquibase.mysql.structure.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

public interface MysqlMigrationConstants {
    Logger LOG = LoggerFactory.getLogger(ImportPostgresData.class);
    String EXPORT_PATH = "/data/migration/";
    String MYSQL_WARNINGS_FILE = EXPORT_PATH + "mysql_warnings.txt";
    String MYSQL_RESULTS_FILE = EXPORT_PATH + "mysql_results.csv";
    String POSTGRES_RESULTS_FILE = EXPORT_PATH + "postgres_results.csv";
    int MAX_THREADS = 10;

    static void writeCSVResults(List<Table> tables, String fileName) {
        String header = "table\tnb_rows\tdurée(secondes)\n";
        StringBuilder content = new StringBuilder(header);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            if (tables != null && !tables.isEmpty()) {
                for (Table t : tables) {
                    String duration = "NA";
                    if (t.getStart() != null && t.getEnd() != null) {
                        duration = Long.toString(t.getStart().until(t.getEnd(), ChronoUnit.SECONDS));
                    }
                    content.append(t.getQualifiedTableName() + "\t" + t.getNbRows() + "\t" + duration + "\n");
                }

            }
            writer.write(content.toString());
        } catch (IOException ex) {
            LOG.error("Impossible de générer le fichier CSV", ex);
        }
    }
}
