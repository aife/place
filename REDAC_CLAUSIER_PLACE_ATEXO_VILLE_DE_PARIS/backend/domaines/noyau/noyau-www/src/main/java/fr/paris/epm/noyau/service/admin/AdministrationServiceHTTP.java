package fr.paris.epm.noyau.service.admin;

import fr.paris.epm.noyau.cache.AuthentificationTokenProvider;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.metier.AdministrationGIM;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import fr.paris.epm.noyau.service.GeneriqueServiceHTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Service d'accès à l'administration en utilisant Spring remoting.
 * @author MGA
 */
public class AdministrationServiceHTTP extends GeneriqueServiceHTTP implements AdministrationServiceSecurise {

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AdministrationServiceHTTP.class);

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private String plateformeUuid;

    private static AdministrationGIM administrationGIM;

    private static AuthentificationTokenProvider authentificationTokenProvider;

    /**
     * le verrouillage pour modifier ou supprimer {@EpmTAuthentificationToken}
     */
    private ReentrantReadWriteLock rwlAuthentificationToken;

    /**
     * Constructeur
     */
    public AdministrationServiceHTTP() {
        rwlAuthentificationToken = new ReentrantReadWriteLock();
    }

    @Override
    public final EpmTUtilisateur chargerUtilisateur(final String guid) {
        UtilisateurCritere critere = new UtilisateurCritere(plateformeUuid);
        critere.setGuidsSso(guid);

        try {
            List<EpmTUtilisateur> liste = generiqueDAO.findByCritere(critere);

            if (liste.isEmpty())
                return null;
            else
                return liste.get(0);
        } catch (NonTrouveNoyauException e) {
            return null;
        }
    }

    /**
     * @param id identifiant de l'utilisateur appelant cette méhode
     * @param directionId de la direction/service à charger
     * @return direction/service possédant cet identifiant en base
     */
    @Override
    public final EpmTRefDirectionService chargerDirectionService(final int id, final int directionId) {
        return generiqueDAO.find(EpmTRefDirectionService.class, directionId);
    }



    @Override
    public String genererTokenUnique(EpmTUtilisateur utilisateur) {
        return authentificationTokenProvider.generateToken(utilisateur).toString();
    }

    @Override
    public EpmTUtilisateur authentifierUtilisateurParToken(String token) {
        return authentificationTokenProvider.retreiveUtilisateur(UUID.fromString(token), true);
    }

    /**
     * supprimer les informations d'authentification
     */
    @Override
    public void supprimerAuthentificationToken(final int id, String username) {
        rwlAuthentificationToken.writeLock().lock();
        try {
            administrationGIM.supprimerAuthentificationToken(username);
        } finally {
            rwlAuthentificationToken.writeLock().unlock();
        }
    }

    /**
     * modifier les informations d'authentification en mettant les valeurs dans param authToken
     * @EpmTAuthentificationToken authToken
     */
    @Override
    public EpmTAuthentificationToken modifierAuthentificationToken(final int id, EpmTAuthentificationToken authToken) {
        rwlAuthentificationToken.writeLock().lock();
        try {
            return administrationGIM.modifierAuthentificationToken(authToken);
        } finally {
            rwlAuthentificationToken.writeLock().unlock();
        }
    }

    /**
     * @param administrationGIM the administrationService to set
     */
    public final void setAdministrationGIM(final AdministrationGIM administrationGIM) {
        AdministrationServiceHTTP.administrationGIM = administrationGIM;
    }

    public void setAuthentificationTokenProvider(AuthentificationTokenProvider authentificationTokenProvider) {
        AdministrationServiceHTTP.authentificationTokenProvider = authentificationTokenProvider;
    }

    public void setPlateformeUuid(@Value("${mpe.client}") String plateformeUuid) {
        this.plateformeUuid = plateformeUuid;
    }

}
