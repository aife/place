package fr.paris.epm.noyau.metier;

import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Interface CRUD générique pour tous les Gestionnaires de Ressources Métier.
 * Created by nty on 30/03/18.
 * @author Nikolay Tyurin
 * @author NTY
 */
public interface GeneriqueDAO extends BaseDAO {

    static final Logger logger = LoggerFactory.getLogger(GeneriqueDAO.class);

    public default <T> T findUniqueByCritere(Critere critere) {
        String requeteHQL = critere.toHQL();
        logger.debug(requeteHQL);
        Query query = getCurrentSession().createQuery(requeteHQL);
        if (critere.getTaillePage() != 0) {
            query.setFirstResult(critere.getNumeroPage() * critere.getTaillePage());
            query.setMaxResults(critere.getTaillePage());
        }

        Map<String, Object> parametres = critere.getParametres();
        if (parametres == null)
            parametres = Collections.emptyMap();
        query.setProperties(parametres);

        return (T) query.uniqueResult();
    }

    public default <T> List<T> findByCritere(Critere critere) {
        String requeteHQL = critere.toHQL();
        logger.debug(requeteHQL);
        Query query = getCurrentSession().createQuery(requeteHQL);
        if (critere.getTaillePage() != 0) {
            query.setFirstResult(critere.getNumeroPage() * critere.getTaillePage());
            query.setMaxResults(critere.getTaillePage());
        }

        Map<String, Object> parametres = critere.getParametres();
        if (parametres == null)
            parametres = Collections.emptyMap();
        query.setProperties(parametres);

        return (List<T>) query.list();
    }

    public default <T> Long findByCritere(Critere critere, List<T> result) {
        result.clear();
        result.addAll(findByCritere(critere));

        if (critere.isChercherNombreResultatTotal()) {
            critere.setProprieteTriee(null);
            String requeteCountHQL = critere.toCountHQL();
            logger.debug(requeteCountHQL);
            Query query = getCurrentSession().createQuery(requeteCountHQL);

            Map<String, Object> parametres = critere.getParametres();
            if (parametres == null)
                parametres = Collections.emptyMap();
            query.setProperties(parametres);

            return (Long) query.uniqueResult();
        }
        return null;
    }
/*
    default <T> void create(Iterable<T> entities){
        entities.forEach(this::create);
    }

    default <T> void update(Iterable<T> entities){
        entities.forEach(this::update);
    }
*/
    default <T> void persist(Iterable<T> entities) {
        entities.forEach(this::persist);
    }

    default <T> Collection<T> merge(Collection<T> entities){
        return entities.stream()
                .map(this::merge)
                .collect(Collectors.toList());
    }

    default <T> void delete(Iterable<T> entities){
        entities.forEach(this::delete);
    }

}
