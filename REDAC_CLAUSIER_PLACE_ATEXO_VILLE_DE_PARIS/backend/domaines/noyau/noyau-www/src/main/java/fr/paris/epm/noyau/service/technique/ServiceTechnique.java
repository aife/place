/**
 * $Id$
 */
package fr.paris.epm.noyau.service.technique;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.jms.MessageJMS;
import fr.paris.epm.noyau.persistance.EpmTAbstractDocument;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTDocument;
import fr.paris.epm.noyau.persistance.EpmTFichierSurDisque;

import java.util.List;

/**
 * @author Mounthei KHAM
 * @author Guillaume BERAUDO
 * @version $Revision$, $Date$, $Author$
 */
public interface ServiceTechnique {

    /**
     * Nom du BEAN pour le service technique.
     */
    String BEAN_ID = "serviceTechnique";

    /**
     * Envoi d'un ensemble de courrier électronique du même type. Le type est
     * défini dans MessageJMS L'ensemble de mail est défini dans MessageJMS
     * @param message message JMS
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException erreur non trouvé
     */
    void envoiCourriel(MessageJMS message);

    /**
     * Dans le cas où le document à sauvegarder est lié à une consultation.
     * Sauvegarde d'un document sur disque (réseau,...) suivant le chemin
     * d'accès spécifié dans le fichier de properties (noyau-serveur.properties)
     * @param consultation consultation
     * @param donneesFichier tableau de bits représentant le fichier
     * @param typeFichier constante de type de fichier
     * @param nomFichier nom du fichier
     * @return fichier créé sur disque
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException erreur non trouvé
     */
    EpmTFichierSurDisque sauvegarderFichier(EpmTConsultation consultation, int typeFichier, String nomFichier, byte[] donneesFichier);

    /**
     * Sauvegarde d'un document sur disque (réseau,...) suivant le chemin
     * d'accès spécifié dans le fichier de properties
     * (noyau-serveur.properties).
     * @param typeFichier type du fichier
     * @param nomFichier nom du fichier
     * @param donneesFichier contenu du fichier
     * @return fichier a créer sur disque
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException erreur introuvable
     */
    EpmTFichierSurDisque sauvegarderFichier(int typeFichier, String nomFichier, byte[] donneesFichier);

    /**
     * Récupérer un fichier sur disque.
     * @param documentSauvegarde document sauvegardé sur disque
     * @return contenu du fichier
     * @throws TechnicalNoyauException erreur technique
     */
    byte[] recupererFichier(EpmTFichierSurDisque documentSauvegarde);

    /**
     * @param id identifiant de l'utilisateur appelant la méthode
     * @param idConsultation identifiant de la consultation
     * @param phase phase du RC à mettre à jour
     * @return renvoi le nouveau RC
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException RC non trouvé
     */
    EpmTDocument miseAJourRC(int idConsultation, int idDocumentRC, String phase);

    /**
     * @param documentSauvegarde document sur disque
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException erreur introuvable
     */
    void supprimerFichier(EpmTFichierSurDisque documentSauvegarde);

    /**
     * Récupère une liste de documents à zipper et le nom du fichier final. Un
     * préfix aléatoire et une extension sont rajoutés au nom du fichier.
     * @param documents liste des documents à zipper
     *            {@link EpmTAbstractDocument}
     * @param nomZip nom du fichier zip généré
     * @return nom du fichier zippé
     * @exception TechnicalNoyauException erreur technique
     * @author Guillaume Béraudo
     * @throws TechnicalNoyauException
     */
    String zipperFichiers(final List documents, final String nomZip);

    /**
     * Duplique un document. les informations de l'objet sont
	 * préservées, le fichier physique est lui aussi dupliqué.
     * @param documentSource document à dupliquer
     * @return un nouveau document.
     * @throws TechnicalNoyauException
     */
    EpmTDocument dupliquerDocument(final EpmTDocument documentSource);
    
    /**
	 * Duplique une liste de document. les informations de l'objet sont
	 * préservées, le fichier physique est lui aussi dupliqué.
	 * 
	 * @param listeDocuments
	 *            liste de EpmTDocument
	 * @return une nouvelle liste de nouveaux EpmTDocument
	 * @throws TechnicalNoyauException
	 */
	List<EpmTDocument> dupliquerListeDocuments(final List<EpmTDocument> listeDocuments);
	
	/**
     * Zip des documents à partir d'une liste d'identifiant.Un préfix aléatoire et une extension sont rajoutés au nom du fichier.
     * 
     * @param idDocuments liste d'identifiant des documents à zipper {@link EpmTAbstractDocument}
     * @param nomZip nom du fichier zip généré
     * @return uri du fichier zippé
     * @exception TechnicalNoyauException erreur technique
     * @author Guillaume Béraudo
     * @throws TechnicalNoyauException
     */
    String zipperFichiersParId(final List<Integer> idDocuments, final String nomZip);
    
    /**
     * Méthode pour tester la connection entre les modules et le noyau.
     * @return OK
     */
    String ping();
    
    /**
     * obtenir le jeton afin de modifier le fichier sur disque en ligne
     * @param idUtilisateur identifiant de l'utilisateur appelant
     * @param idDocumentConcersve identifiant de document conservé
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException erreur non trouvé
     */
    String getUrlJetonPourModificationEnLigneDocumentConserve(final int idUtilisateur, int idDocumentConcersve);
}
