package fr.paris.epm.noyau.metier.synchronisationSocle.exceptions;

public class SynchronisationException extends Exception {

	public SynchronisationException() {
	}

	public SynchronisationException( String message ) {
		super(message);
	}

	public SynchronisationException( String message, Throwable cause ) {
		super(message, cause);
	}

	public SynchronisationException( Throwable cause ) {
		super(cause);
	}

	public SynchronisationException( String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
