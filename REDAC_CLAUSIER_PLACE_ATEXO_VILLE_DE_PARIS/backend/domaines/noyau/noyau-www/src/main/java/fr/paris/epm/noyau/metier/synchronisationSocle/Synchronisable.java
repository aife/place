package fr.paris.epm.noyau.metier.synchronisationSocle;

import fr.paris.epm.noyau.metier.synchronisationSocle.exceptions.SynchronisationException;

public interface Synchronisable extends Loggable {
	void synchroniser() throws SynchronisationException;
}
