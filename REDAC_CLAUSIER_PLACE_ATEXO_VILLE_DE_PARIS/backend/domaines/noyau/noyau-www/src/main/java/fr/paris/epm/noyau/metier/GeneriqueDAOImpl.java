package fr.paris.epm.noyau.metier;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Classe CRUD générique pour tous les Gestionnaires de Ressources Métier.
 * Created by nty on 30/03/18.
 * @author Nikolay Tyurin
 * @author NTY
 */
public class GeneriqueDAOImpl implements GeneriqueDAO {

    SessionFactory sessionFactory;

    @Override
    public final Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
