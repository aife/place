package fr.paris.epm.noyau.liquibase;

import fr.paris.epm.noyau.liquibase.mysql.structure.Column;
import fr.paris.epm.noyau.liquibase.mysql.structure.Table;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static fr.paris.epm.noyau.liquibase.MysqlMigrationConstants.*;
import static java.lang.String.format;


public class ImportPostgresData implements CustomTaskChange {

    private static final Logger LOG = LoggerFactory.getLogger(ImportPostgresData.class);
    private static final List<Table> mysqlTables = new ArrayList<>();
    private static String loadArgs = " FIELDS TERMINATED BY '\\t' OPTIONALLY ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\\n' IGNORE 1 LINES ";

    private void checkFolder() throws CustomChangeException {
        Path exPath = Paths.get(EXPORT_PATH);
        if (!exPath.toFile().exists()) {
            throw new CustomChangeException("Répertoire d'import absent : " + EXPORT_PATH);
        }
    }

    private void populateQueue(JdbcConnection connection) throws DatabaseException {
        String publicSchema = connection.getCatalog();
        List<String> allSchemas = Arrays.asList(publicSchema, "referentiel", "consultation", "redaction", "echanges", "commission");
        for (String schema : allSchemas) {
            String currentSchema = schema;
            // ignorer les liquibase lock et les tables JBPM
            String tablesQuery = "SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' and TABLE_NAME not like 'databasechangeloglock%' and TABLE_NAME not like 'JBPM_%' and TABLE_SCHEMA = '" + currentSchema + "'";
            try (ResultSet schemaResultSet = connection.createStatement().executeQuery(tablesQuery)) {

                while (schemaResultSet.next()) {
                    String tableSchema = schemaResultSet.getString("TABLE_SCHEMA");
                    String table = schemaResultSet.getString("TABLE_NAME");
                    Table entry = new Table(publicSchema, tableSchema, table);
                    Optional<File> correspondingFile = Arrays.stream(new File(EXPORT_PATH).listFiles()).filter(f -> f.isFile() && f.getName().equalsIgnoreCase(entry.getQualifiedTableName() + ".csv")).findFirst();
                    if (!correspondingFile.isPresent()) {
                        LOG.warn("Le fichier CSV {} pour la table {} n'existe pas, on continue avec la table suivante", entry.getCsvFile(), entry.getQualifiedTableName());
                        continue;
                    }
                    String newFilePath = EXPORT_PATH + '/' + correspondingFile.get().getName();
                    entry.setCsvFile(newFilePath);
                    mysqlTables.add(entry);
                }
            } catch (Exception e) {
                LOG.error("Erreur lors de l'import", e);
            }
        }
    }

    private List<Column> getMysqlColumns(JdbcConnection mysqlConnection, String schema, String table) {
        List<Column> mysqlColumns = new ArrayList<>();
        String columnsQuery = format("select COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA ='%s' and table_name ='%s'", schema, table);
        try (ResultSet columnsResultSet = mysqlConnection.createStatement().executeQuery(columnsQuery)) {
            while (columnsResultSet.next()) {

                String columnName = columnsResultSet.getString("COLUMN_NAME");
                String columneType = columnsResultSet.getString("DATA_TYPE");
                Column column = new Column(columnName, "bit".equals(columneType));
                mysqlColumns.add(column);
            }
        } catch (Exception ex) {
            LOG.error("Erreur lors de la récupération des colonnes Mysql", ex);
        }
        return mysqlColumns;
    }

    private List<String> getPostgresColumns(String newFilePath) {
        List<String> postgresColumns = new ArrayList<>();
        try (BufferedReader brTest = new BufferedReader(new FileReader(newFilePath))) {
            String firstLine = brTest.readLine();
            if (firstLine == null || "".equals(firstLine.trim())) {
                LOG.warn("le fichier {} est vide", newFilePath);
            } else {
                postgresColumns = Arrays.asList(firstLine.split("\t"));
                LOG.debug("Colonnes postgres : {}", postgresColumns);
            }
        } catch (IOException ioex) {
            LOG.error("le fichier {} est introuvable", newFilePath);
        }

        return postgresColumns;
    }

    private Table processTable(JdbcConnection mysqlConnection, Table table) {
        table.setStart(LocalDateTime.now());
        List<String> postgresColumns = getPostgresColumns(table.getCsvFile());
        List<Column> allMysqlColumns = getMysqlColumns(mysqlConnection, table.getSchema(), table.getName());
        List<String> mysqlColumns = allMysqlColumns.stream()
                .map(Column::getName)
                .collect(Collectors.toList());
        List<String> booleanColumns = allMysqlColumns.stream()
                .filter(Column::isBooleanType)
                .map(Column::getName)
                .collect(Collectors.toList());

        if (mysqlColumns.size() != postgresColumns.size()) {
            LOG.warn("Attention, les colonnes Postgres et Mysql ne sont pas identiques !\nMysql: {}\nPostgres: {}", mysqlColumns, postgresColumns);
        }
        List<String> mysqlMappedColumns = postgresColumns.stream().map(col -> {
            if (mysqlColumns.stream().anyMatch(col::equalsIgnoreCase)) {
                return col;
            } else {
                LOG.warn("Table : {} la colonne {} est inexistante dans la BDD MySQL", table.getQualifiedTableName(), col);
                return "@" + col;
            }
        }).collect(Collectors.toList());
        String setBooleans = "";
        if (!booleanColumns.isEmpty()) {
            List<String> booleanSets = new ArrayList<>();
            for (String booleanColumn : booleanColumns) {
                int index = mysqlMappedColumns.indexOf(booleanColumn);
                if (index != -1) {
                    mysqlMappedColumns.set(index, "@" + booleanColumn);
                    booleanSets.add(String.format("%s=cast(@%s as signed)", booleanColumn, booleanColumn));
                }
            }
            setBooleans = String.format(" SET %s", String.join(",", booleanSets));
        }
        StringBuilder mysqlLoadContent = new StringBuilder(format("LOAD DATA LOCAL INFILE '%s' IGNORE INTO TABLE %s %s (%s) %s;\n", table.getCsvFile(), table.getQualifiedTableName(), loadArgs, String.join(",", mysqlMappedColumns), setBooleans));
        LOG.info("Import de {} ,Exécution de la commande : {}", table.getQualifiedTableName(), mysqlLoadContent);
        int updatedRows = 0;
        List<String> warnings = new ArrayList<>();
        try (Statement batch = mysqlConnection.createStatement()) {
            batch.execute(mysqlLoadContent.toString());
            updatedRows = batch.getUpdateCount();
            SQLWarning warning = batch.getWarnings();
            if (warning != null) {
                while (warning != null) {
                    LOG.warn("l'import s'est passé avec des alertes : {}", warning.getMessage());
                    warnings.add(warning.toString());
                    warning = warning.getNextWarning();
                }
            }
            LOG.info("load statement : {}", mysqlLoadContent);
        } catch (DatabaseException | SQLException ex) {
            LOG.error("Une erreur est survenue lors du chargement du fichier {} pour la table {}", table.getCsvFile(), table.getQualifiedTableName(), ex);
        }
        table.setWarnings(warnings);
        table.setEnd(LocalDateTime.now());
        table.setNbRows(updatedRows);
        return table;
    }


    @Override
    public void execute(Database database) throws CustomChangeException {
        LOG.info("Début d'mport des fichiers CSV présents dans le répertoire : {}", EXPORT_PATH);
        checkFolder();
        JdbcConnection mysqlConnection = (JdbcConnection) database.getConnection();
        enableFk(mysqlConnection, 0);
        try {
            populateQueue(mysqlConnection);
            ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
            for (final Table singleTable : mysqlTables) {
                executor.execute(() -> {
                    String threadName = Thread.currentThread().getName();
                    LOG.info("Thread d'import : {} je process la table {}", threadName, singleTable.getQualifiedTableName());
                    processTable(mysqlConnection, singleTable);
                });
            }
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);

            LOG.info("Fin d'exécution de toutes les taches d'import");

            // write results
            writeCSVResults(mysqlTables, MYSQL_RESULTS_FILE);
            writeWarnings();

        } catch (Exception ex) {
            Thread.currentThread()
                    .interrupt();
            throw new CustomChangeException("Impossible d'éxécuter l'import des données", ex);
        } finally {
            enableFk(mysqlConnection, 1);
        }
        LOG.info("Fin de l'import");
    }

    private void writeWarnings() {
        //write warnings
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(MYSQL_WARNINGS_FILE))) {
            StringBuilder content = new StringBuilder();
            mysqlTables.stream()
                    .filter(t -> !t.getWarnings()
                            .isEmpty())
                    .forEach(t -> content.append("\n" + t.getQualifiedTableName() + "\n" + String.join("\n", t.getWarnings())));
            writer.write(content.toString());
        } catch (IOException ex) {
            LOG.error("Erreur lors de l'écriture du fichier des warnings", ex);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {
        // setUp
    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        // setFileOpener
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    /**
     * @param flag 1 pour activer les contraintes, 0 pour les désactiver
     * @return
     */
    private void enableFk(JdbcConnection connection, int flag) {

        LOG.info(" {} des contraintes et des clés étrangères", flag == 0 ? "désactivation" : "activation");
        String[] statements = new String[]{format("SET autocommit = %d;\n", flag), format("SET unique_checks = %d;\n", flag), format("SET foreign_key_checks= %d;\n", flag)};
        String[] statementsGlobal = new String[]{format("SET GLOBAL autocommit = %d;\n", flag), format("SET GLOBAL unique_checks = %d;\n", flag), format("SET GLOBAL foreign_key_checks= %d;\n", flag)};
        Statement statement = null;
        try {
            statement = connection.createStatement();
            for (String s : statements) {
                statement.addBatch(s);
            }
            for (String s : statementsGlobal) {
                statement.addBatch(s);
            }
            statement.executeBatch();
        } catch (DatabaseException | SQLException ex) {
            LOG.error("Impossible de désactiver les contraintes", ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    LOG.error("Impossible de ferme le statement", e);
                }

            }
        }

    }
}
