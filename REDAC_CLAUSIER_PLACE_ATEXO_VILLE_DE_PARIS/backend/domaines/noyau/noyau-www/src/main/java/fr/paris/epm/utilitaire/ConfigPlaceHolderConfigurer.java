package fr.paris.epm.utilitaire;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.List;
import java.util.Properties;

/**
 * Attention : cette classe est la meme que celle presente dans commun.lib (dependance cyclique entre noyau et commun.lib).
 *
 * Chargement des proprietes de configuration depuis les fichiers de configuration.
 *
 * @author Remi Ville
 * @version 2010/06/15
 */
public class ConfigPlaceHolderConfigurer extends PropertySourcesPlaceholderConfigurer {

    /**
     * Liste des noms de fichier de configuration.
     */
    private List filename;

    /**
     * Chargement des proprietes de configuration depuis les fichiers de configuration.
     * @throws BeansException
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory valeur) {
        // Creation du conteneur des proprietes de configuration
    	Config.init(filename);
    	Properties props = Config.getProperties();
    	if(props != null) {
    		Properties[] propertiesArray = new Properties[1];
    		propertiesArray[0] = props;
    		setPropertiesArray(propertiesArray);
    	}

        super.postProcessBeanFactory(valeur);
    }
    
    /**
     * @param filename Liste des noms de fichier de configuration.
     */
    public void setFilename(List filename) {
        this.filename = filename;
    }

}