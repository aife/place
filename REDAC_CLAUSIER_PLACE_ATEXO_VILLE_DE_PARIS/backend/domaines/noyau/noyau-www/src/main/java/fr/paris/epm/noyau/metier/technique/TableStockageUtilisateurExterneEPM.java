package fr.paris.epm.noyau.metier.technique;

import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Bean dans lequel est stocké des informations temporaires de l'application.
 * notemment :
 * - les idConsultation / taille du dce zippé.
 * - vérification de la mise à jour des référentiels / selects.
 *
 * @author Léon Barsamian
 */
public class TableStockageUtilisateurExterneEPM {

	/**
	 * Data
	 */
	private final ConcurrentMap<String, UtilisateurExterneConsultation> data = new ConcurrentHashMap<>();

	/** Constructeur privé */
	private TableStockageUtilisateurExterneEPM()
	{}

	/** Holder */
	private static class SingletonHolder
	{
		/** Instance unique non préinitialisée */
		protected static final TableStockageUtilisateurExterneEPM INSTANCE = new TableStockageUtilisateurExterneEPM();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static TableStockageUtilisateurExterneEPM getInstance() {
		return SingletonHolder.INSTANCE;
	}

	/**
	 * Logger
	 */
	private static final Logger LOG = LoggerFactory.getLogger(TableStockageUtilisateurExterneEPM.class);

	/**
	 * Identifiant format
	 */
	public static String IDENTIFIANT_EXTERNE_FORMATTER = "IDENTIFIANT_EXTERNE_{0}";


	/**
	 * Permet de tester si un identifiant externe est déjà utilisé
	 *
	 * @param identifiantExterne l'identifiant externe
	 *
	 * @return vrai s'il existe, faux sinon
	 */
	public boolean existe( final String identifiantExterne ) {
		String key = MessageFormat.format(IDENTIFIANT_EXTERNE_FORMATTER,identifiantExterne);
		final boolean existe = this.data.containsKey(key);

		LOG.warn(existe ? "L'identifiant externe {} existe déjà" : "L'identifiant externe {} n'existe pas encore.", key);

		return existe;
	}

	/**
	 * Permet de récupérer un utilisateur depuis son identifiant externe
	 *
	 * @param identifiantExterne l'identifiant externe de l'utilisateur
	 *
	 * @return l'utilisateur
	 */
	public UtilisateurExterneConsultation recuperer( final String identifiantExterne ) {
		String key = MessageFormat.format(IDENTIFIANT_EXTERNE_FORMATTER,identifiantExterne);

		final UtilisateurExterneConsultation utilisateur = this.data.get(key);

		LOG.debug("Recuperation de l'utilisateur {} avec identifiant externe {}.", utilisateur, key);

		return utilisateur;
	}

	/**
	 * Permet de stocker un utilisateur
	 *
	 * @param identifiantExterne l'identifiant externe
	 * @param utilisateur l'utilisateur
	 */
	public void stocker( final String identifiantExterne, final UtilisateurExterneConsultation utilisateur ) {
		String key = MessageFormat.format(IDENTIFIANT_EXTERNE_FORMATTER,identifiantExterne);

		LOG.debug("Mise en session  de l'utilisateur {} avec identifiant externe {}.", utilisateur, key);

		this.data.put(key, utilisateur);
	}
}
