package fr.paris.epm.noyau.metier;

import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineRacine;
import fr.paris.epm.noyau.persistance.EpmTBudLot;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.documents.EpmTDocumentModele;
import fr.paris.epm.noyau.service.ReferentielsServiceLocal;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Méthodes métiers pour les opérations liées au {@link EpmTDocumentModele}
 * @author Léon Barsamian
 */
public class DocumentModeleGIMImpl extends BaseGIMImpl implements DocumentModeleGIM {

    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DocumentModeleGIMImpl.class);
    /**
     * Mapper.
     */
    private static DozerBeanMapper conversionService;


    private GeneriqueDAO generiqueDAO;



    private ReferentielsServiceLocal referentielsServiceLocal;


    public String genererFreeMarker(final Map<String, Object> freeMarkerMap, final String xml, final EpmTConsultation consultation, final int idLot) {

        String resultat = null;

        DomaineRacine domaineRacine = null;
        if (consultation == null) {
            domaineRacine = new DomaineRacine();
        } else {
            DocumentBuilder builder = new DocumentBuilder();
            builder.setConsultation(consultation);
            if (idLot != -1 && idLot != 0) {
                builder.setIdLot(idLot);
            }
            builder.setGeneriqueDAO(generiqueDAO);
            builder.setConversionService(conversionService);
            builder.setReferentiels(referentielsServiceLocal.getAllReferentiels());
            domaineRacine = builder.genererDomainePairesTypes();
        }
        freeMarkerMap.put("racine", domaineRacine);
        Boolean criteresCommuns = Boolean.FALSE;
        if (consultation != null) {
            List<EpmTBudLot> lots = consultation.trieSetLots();
            if (lots.size() > 1) {
                EpmTBudLot lot = lots.get(0);
                if (lot.getCritereAttribution().getCodeExterne().equals("prix")
                        || lot.getCritereAttribution().getCodeExterne().equals("coutglobal")
                        || lot.getCritereAttribution().getCodeExterne().equals("cahierCharges")
                ) {
                    criteresCommuns = Boolean.TRUE;
                }
            }
        }
        freeMarkerMap.put("criteresCommuns", criteresCommuns.equals(Boolean.TRUE)?"true":"false");

        try {
            resultat = FreeMarkerUtil.executeDefaultScriptFreeMarker(freeMarkerMap, xml);
        } catch (Exception e) {
            LOG.error("Erreur lors de la génération du document modèle", e);
            throw new TechnicalNoyauException(e);
        }
        return resultat;
    }


    public final void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    public final void setConversionService(final DozerBeanMapper valeur) {
        DocumentModeleGIMImpl.conversionService = valeur;
    }


    public final void setReferentielsServiceLocal(final ReferentielsServiceLocal valeur) {
        this.referentielsServiceLocal = valeur;
    }

}
