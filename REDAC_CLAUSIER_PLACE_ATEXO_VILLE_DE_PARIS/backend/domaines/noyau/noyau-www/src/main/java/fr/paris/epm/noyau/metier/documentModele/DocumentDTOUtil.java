package fr.paris.epm.noyau.metier.documentModele;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.TypeChamp;
import fr.atexo.commun.document.generation.util.IntrospectionUtil;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineRacine;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefChampFusion;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefChampFusionComplexe;

/**
 * Classe utilisataire pour les transformations entre objets RSEM et DTO utilisé
 * pour la génération de document avec le module documentType.
 * @author Léon BARSAMIAN
 */
public class DocumentDTOUtil {

    public static List<ChampFusionDTO> creerListeChampFusionDTOPourGeneration(
            final Set<EpmTRefChampFusion> listeChampEPM,
            final DomaineRacine domaine) {

        List<ChampFusionDTO> resultat = new ArrayList<ChampFusionDTO>();
        List<ChampFusionDTO> champComplexe = new ArrayList<ChampFusionDTO>();

        for (EpmTRefChampFusion champEPM : listeChampEPM) {
            ChampFusionDTO nouveau = new ChampFusionDTO();
            TypeChamp type = null;
            if (champEPM instanceof EpmTRefChampFusionComplexe) {
                
            	if(!StringUtils.isEmpty(champEPM.getScriptType())){
            		type = TypeChamp.valueOf(champEPM.getScriptType());
            	}
            	
                nouveau.setGenerationAttributes(champEPM.getLibelle(), type,
                        null, champEPM.getExpression());
                champComplexe.add(nouveau);
            } else {
                nouveau.setGenerationAttributesSansValeur(
                        champEPM.getLibelle(), champEPM.getExpression(), type,
                        null);
                resultat.add(nouveau);
            }

        }

        IntrospectionUtil introspectionUtil = new IntrospectionUtil(domaine);
        introspectionUtil.calculerValeurs(resultat);

        List<ChampFusionDTO> listeChampFusionASupprimer = new ArrayList<ChampFusionDTO>();
        for (ChampFusionDTO champFusionDTO : resultat) {
            if (champFusionDTO.getValeur() == null) {
                listeChampFusionASupprimer.add(champFusionDTO);
            }
        }
        resultat.removeAll(listeChampFusionASupprimer);
        resultat.addAll(champComplexe);
        return resultat;
    }
}
