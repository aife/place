package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.*;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.persistance.referentiel.*;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service d'accès aux consultation en utilisant Spring remoting.
 *
 * @author MGA
 */
public class ReferentielsServiceHTTP extends GeneriqueServiceHTTP implements ReferentielsServiceLocal, ReferentielsServiceSecurise, Serializable {

    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(ReferentielsServiceHTTP.class);

    private static Referentiels referentiels;

    private static DozerBeanMapper conversionService;


    private static String plateformeUuid;
    private static Date dateDerniereMiseAjour;

    private static Date dateDerniereModification = new Date();

    /**
     * Mettre à jour la cache du referentiel chaque une heure
     */
    private static final long DUREE_MISE_A_JOUR_REFERENTIEL = 3600000;

    /**
     * Agrège la totalité des référentiels en base de donnée. Une transaction
     * est effectuée pour chaque récupération de référentiel (une dizaine).
     *
     * @return POJO Referentiels stockant chaque collection de référentiels
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException NonTrouveNoyauException
     */
    @Override
    public final synchronized Referentiels getAllReferentiels() throws TechnicalNoyauException, NonTrouveNoyauException {
        if (!aExpire()) {
            if (logger.isDebugEnabled()) {
                logger.debug("Cache non expiré - Récupération des référentiels");
            }
            return referentiels;
        }

        return rechargerAllReferentiels();
    }

    @Override
    public final Referentiels rechargerAllReferentiels() {
        referentiels = new Referentiels();

        dateDerniereMiseAjour = new Date();

        if (logger.isDebugEnabled()) {
            logger.debug("Mise en cache des référentiels");
        }

        PouvoirAdjudicateurCritere pouvoirAdjudicateurCritere = new PouvoirAdjudicateurCritere();
        pouvoirAdjudicateurCritere.setCommission(false);
        referentiels.setRefPouvoirAdjudicateur(generiqueDAO.findByCritere(pouvoirAdjudicateurCritere));
        referentiels.setRefPouvoirAdjudicateurCommision(generiqueDAO.findAll(EpmTRefPouvoirAdjudicateur.class));

        ProcedureCritere procedureCritere = new ProcedureCritere();
        procedureCritere.setActif(true);
        referentiels.setRefProcedureEcriture(generiqueDAO.findByCritere(procedureCritere));

        procedureCritere = new ProcedureCritere();
        referentiels.setRefProcedureLecture(generiqueDAO.findByCritere(procedureCritere));


        referentiels.setRefArticle(generiqueDAO.findAll(EpmTRefArticle.class));
        referentiels.setRefNature(generiqueDAO.findAll(EpmTRefNature.class));
        referentiels.setRefArrondissement(generiqueDAO.findAll(EpmTRefArrondissement.class));
        referentiels.setRefTypeConsultation(generiqueDAO.findAll(EpmTRefTypeConsultation.class));
        referentiels.setRefTypeDsp(generiqueDAO.findAll(EpmTRefTypeDsp.class));
        referentiels.setRefAutoriteDelegante(generiqueDAO.findAll(EpmTRefAutoriteDelegante.class));

        DirectionServiceCritere critere = new DirectionServiceCritere(plateformeUuid);
        critere.setProprieteTriee("libelle");
        referentiels.setRefDirectionServices(generiqueDAO.findByCritere(critere));
        critere.setActif(true);
        referentiels.setRefDirectionServicesActives(generiqueDAO.findByCritere(critere));
        referentiels.setRefSignatureElectronique(generiqueDAO.findAll(EpmTRefSignatureElectronique.class).stream().sorted(Comparator.comparingInt(BaseEpmTRefReferentiel::getId)).collect(Collectors.toList()));
        referentiels.setRefCcag(generiqueDAO.findAll(EpmTRefCcag.class));
        referentiels.setRefTypeMarche(generiqueDAO.findAll(EpmTRefTypeMarche.class));
        referentiels.setRefDocumentUtilise(generiqueDAO.findAll(EpmTRefDocumentUtilise.class));
        referentiels.setRefModeleEtapesCalendrier(generiqueDAO.findAll(EpmTRefEtapeCalendrier.class));
        referentiels.setRefBeneficiaireMarcheList(generiqueDAO.findAll(EpmTRefBeneficiaireMarche.class));
        referentiels.setRefUnite(generiqueDAO.findAll(EpmTRefUnite.class));
        referentiels.setRefCritereAttribution(generiqueDAO.findAll(EpmTRefCritereAttribution.class));

        referentiels.setRefCodeAchat(generiqueDAO.findAll(EpmTRefCodeAchat.class));
        referentiels.setRefImputationBudgetaire(generiqueDAO.findAll(EpmTRefImputationBudgetaire.class));
        referentiels.setRefChampAdditionnel(generiqueDAO.findAll(EpmTRefChampAdditionnel.class));
        referentiels.setRefNomenclature(generiqueDAO.findAll(EpmTRefNomenclature.class));
        referentiels.setRefTypeContratAvenant(generiqueDAO.findAll(EpmTRefTypeContratAvenant.class));
        referentiels.setRefTypeObjetContrat(generiqueDAO.findAll(EpmTRefTypeObjetContrat.class));

        referentiels.setRefTypeDocumentContrat(generiqueDAO.findAll(EpmTRefTypeDocumentContrat.class));
        referentiels.setRefCategorieDocumentContrat(generiqueDAO.findAll(EpmTRefCategorieDocumentContrat.class));
        referentiels.setRefStatutNotificationContrat(generiqueDAO.findAll(EpmTRefStatutNotificationContrat.class));
        referentiels.setRefTypeEtablissement(generiqueDAO.findAll(EpmTRefTypeEtablissement.class));
        referentiels.setRefTypeAttributaire(generiqueDAO.findAll(EpmTRefTypeAttributaire.class));
        referentiels.setRefRoleJuridique(generiqueDAO.findAll(EpmTRefRoleJuridique.class));

        referentiels.setRefTrancheBudgetaire(generiqueDAO.findAll(EpmTRefTrancheBudgetaire.class));
        referentiels.setRefTypeAvenant(generiqueDAO.findAll(EpmTRefTypeAvenant.class));
        referentiels.setRefClausesSociales(generiqueDAO.findAll(EpmTRefClausesSociales.class));
        referentiels.setRefClausesEnvironnementales(generiqueDAO.findAll(EpmTRefClausesEnvironnementales.class));
        referentiels.setRefStructureSocialeReserves(generiqueDAO.findAll(EpmTRefTypeStructureSociale.class));
        referentiels.setRefDroitProprieteIntellectuelle(generiqueDAO.findAll(EpmTRefDroitProprieteIntellectuelle.class));
        referentiels.setRefLieuExecution(generiqueDAO.findAll(EpmTRefLieuExecution.class));

        referentiels.setRefParametrage(generiqueDAO.findAll(EpmTRefParametrage.class));
        referentiels.setRefInfoBulle(generiqueDAO.findAll(EpmTRefInfoBulle.class));
        referentiels.setRefFichePratique(generiqueDAO.findAll(EpmTRefFichePratique.class));
        referentiels.setTypesEvenementsCalendrier(generiqueDAO.findAll(EpmTRefTypeEvenementCalendrier.class));
        referentiels.setRefReponseElectronique(generiqueDAO.findAll(EpmTRefReponseElectronique.class));
        referentiels.setRefFormesJuridique(generiqueDAO.findAll(EpmTRefFormeJuridique.class));

        Map<Integer, DirectionService> directionService = transformerDS(referentiels.getRefDirectionServices());
        referentiels.setMapDirService(directionService);

        referentiels.setRefOrganisme(generiqueDAO.findAll(EpmTRefOrganisme.class));


        referentiels.setBonQuantite(generiqueDAO.findAll(EpmTRefBonQuantite.class));
        referentiels.setChoixFormePrix(generiqueDAO.findAll(EpmTRefChoixFormePrix.class));
        referentiels.setChoixMoisJour(generiqueDAO.findAll(EpmTRefChoixMoisJour.class));
        referentiels.setDureeDelaiDescription(generiqueDAO.findAll(EpmTRefDureeDelaiDescription.class));
        referentiels.setGroupementAttributaire(generiqueDAO.findAll(EpmTRefGroupementAttributaire.class));
        referentiels.setMinMax(generiqueDAO.findAll(EpmTRefMinMax.class));
        referentiels.setNatureTranche(generiqueDAO.findAll(EpmTRefNatureTranche.class));
        referentiels.setNbCandidatsAdmis(generiqueDAO.findAll(EpmTRefNbCandidatsAdmis.class));
        referentiels.setReservationLot(generiqueDAO.findAll(EpmTRefReservationLot.class));
        referentiels.setTrancheTypePrix(generiqueDAO.findAll(EpmTRefTrancheTypePrix.class));
        referentiels.setTypePrix(generiqueDAO.findAll(EpmTRefTypePrix.class));
        referentiels.setVariation(generiqueDAO.findAll(EpmTRefVariation.class));
        referentiels.setVision(generiqueDAO.findAll(EpmTRefVision.class));
        referentiels.setStatut(generiqueDAO.findAll(EpmTRefStatut.class));
        referentiels.setStatutAvenant(generiqueDAO.findAll(EpmTRefStatutAvenant.class));
        referentiels.setModaliteDeRetrait(generiqueDAO.findAll(EpmTRefModaliteRetrait.class));
        referentiels.setModaliteDepotPli(generiqueDAO.findAll(EpmTRefModaliteDepotPli.class));
        referentiels.setModaliteDepotElement(generiqueDAO.findAll(EpmTRefModaliteDepotElement.class));
        referentiels.setActionsPossibles(generiqueDAO.findAll(EpmTRefActionPossible.class));
        referentiels.setTypesCandidatures(generiqueDAO.findAll(EpmTRefTypeEnveloppe.class));
        referentiels.setStatutsCandidatures(generiqueDAO.findAll(EpmTRefStatutEnveloppe.class));
        referentiels.setCompletudeEnveloppe(generiqueDAO.findAll(EpmTRefCompletudeEnveloppe.class));
        referentiels.setMoyenNotification(generiqueDAO.findAll(EpmTRefMoyenNotification.class));
        referentiels.setSuiteADonner(generiqueDAO.findAll(EpmTRefSuiteADonner.class));
        referentiels.setSuiteNegociations(generiqueDAO.findAll(EpmTRefSuiteNegociation.class));
        referentiels.setAdmissibilite(generiqueDAO.findAll(EpmTRefAdmissibilite.class));
        referentiels.setStatutEntreprise(generiqueDAO.findAll(EpmTRefStatutEntreprise.class));
        referentiels.setListeFormeJuridique(generiqueDAO.findAll(EpmTRefFormeJuridique.class));
        referentiels.setActionConfirmationAttribution(generiqueDAO.findAll(EpmTRefActionConfirmationAttribution.class));
        referentiels.setActionConfirmationAttributionChgtAttributaire(generiqueDAO.findAll(EpmTRefActionConfirmationAttributionChgtAttributaire.class));
        referentiels.setFormatEchange(generiqueDAO.findAll(EpmTRefFormatEchange.class));
        referentiels.setMatinsAprems(generiqueDAO.findAll(EpmTRefMatinAprem.class));
        referentiels.setRoles(generiqueDAO.findAll(EpmTRefRole.class));
        referentiels.setPresences(generiqueDAO.findAll(EpmTRefPresence.class));
        referentiels.setEtatsAttestations(generiqueDAO.findAll(EpmTRefEtatAttestation.class));
        referentiels.setStatutOffreFinanciere(generiqueDAO.findAll(EpmTRefStatutOffreFinanciere.class));
        referentiels.setDepartements(generiqueDAO.findAll(EpmTRefDepartement.class));
        referentiels.setTypesBudget(generiqueDAO.findAll(EpmTRefTypeBudget.class));

        DocumentEmplacementCritere emplacementCritere = new DocumentEmplacementCritere();
        referentiels.setRefDocumentEmplacements(generiqueDAO.findByCritere(emplacementCritere));

        DocumentDestinataireCritere destinataireCritere = new DocumentDestinataireCritere();
        referentiels.setRefDocumentDestinataires(generiqueDAO.findByCritere(destinataireCritere));

        DomaineDonneesCritere domaineCritere = new DomaineDonneesCritere();
        referentiels.setRefDomainesDonnees(generiqueDAO.findByCritere(domaineCritere));

        ChampFusionCritere champFusionCritere = new ChampFusionCritere();
        champFusionCritere.setSimple(true);
        referentiels.setRefChampsFusionSimples(generiqueDAO.findByCritere(champFusionCritere));
        champFusionCritere.setSimple(false);
        referentiels.setRefChampsFusionComplexes(generiqueDAO.findByCritere(champFusionCritere));

        DocumentTypeCritere typeCritere = new DocumentTypeCritere();
        List<EpmTRefDocumentType> documentTypes = generiqueDAO.findByCritere(typeCritere);
        referentiels.setRefDocumentTypesDocument(documentTypes.stream().filter(t -> t.getId() < 4).collect(Collectors.toList()));
        referentiels.setRefDocumentTypesExcel(documentTypes.stream().filter(t -> t.getId() == 4).collect(Collectors.toList()));

        // REDACTION //
        referentiels.setRefActionDocuments(generiqueDAO.findAll(EpmTRefActionDocument.class));
        referentiels.setRefAuteurs(generiqueDAO.findAll(EpmTRefAuteur.class));
        referentiels.setRefExtensionFichiers(generiqueDAO.findAll(EpmTRefExtensionFichier.class));
        referentiels.setRefInterventions(generiqueDAO.findAll(EpmTRefIntervention.class));
        referentiels.setRefPotentiellementConditionnees(generiqueDAO.findAll(EpmTRefPotentiellementConditionnee.class));
        referentiels.setRefStatutRedactionClausiers(generiqueDAO.findAll(EpmTRefStatutRedactionClausier.class));
        referentiels.setRefThemeClauses(generiqueDAO.findAll(EpmTRefThemeClause.class));
        referentiels.setRefTypeClauses(generiqueDAO.findAll(EpmTRefTypeClause.class));
        referentiels.setRefTypeContrats(generiqueDAO.findAll(EpmTRefTypeContrat.class));
        referentiels.setRefTypeDocuments(generiqueDAO.findAll(EpmTRefTypeDocument.class));
        referentiels.setRefTypePageDeGardes(generiqueDAO.findAll(EpmTRefTypePageDeGarde.class));
        referentiels.setRefValeurTypeClauses(generiqueDAO.findAll(EpmTRefValeurTypeClause.class));

        dateDerniereMiseAjour = new Date();

        return referentiels;
    }

    public Collection<EpmTUtilisateur> getAllUtilisateurs() {
        return generiqueDAO.findAll(EpmTUtilisateur.class);
    }

    /**
     * @param epmTRefDirectionServices collection des direction/services en base
     */
    private Map<Integer, DirectionService> transformerDS(final Collection<EpmTRefDirectionService> epmTRefDirectionServices)
            throws TechnicalNoyauException {
        Map<Integer, DirectionService> mapDirService = new HashMap<Integer, DirectionService>();
        DirectionService racine = new DirectionService();
        racine.setLibelle("racine");
        racine.setId(-1);
        DirectionService.setIdsTransverses(new Vector());
        mapDirService.put(Integer.valueOf(racine.getId()), racine);

        boolean dsNiveau0 = false;
        for (EpmTRefDirectionService epmTRefDirectionService : epmTRefDirectionServices) {
            if (EpmTRefDirectionService.NIVEAU_1.equals(epmTRefDirectionService.getTypeEntite())) {
                mapDirService = construitArbre(epmTRefDirectionService, racine, mapDirService);
                dsNiveau0 = true;
            }
        }

        if (!dsNiveau0)
            throw new TechnicalNoyauException("Niveau direction vide");

        Collections.sort(racine.getEnfants());
        return mapDirService;
    }

    /**
     * @param dsBaseCourant direction/service en base à transformer
     * @param parent        direction/service parent déjà transformée
     */
    private Map<Integer, DirectionService> construitArbre(final EpmTRefDirectionService dsBaseCourant, final DirectionService parent,
                                                          Map<Integer, DirectionService> mapDirService) {

        // Transformation de la direction/service
        DirectionService directionService = conversionService.map(dsBaseCourant, DirectionService.class);
        directionService.setNiveau(dsBaseCourant.getTypeEntite());
        directionService.setParent(parent);
        parent.getEnfants().add(directionService);
        mapDirService.put(directionService.getId(), directionService);

        if (directionService.isTransverse())
            DirectionService.ajouterIdTransverse(directionService.getId());

        List<EpmTRefDirectionService> enfantsBase = referentiels.getRefDirectionServices().stream()
                .filter(ds -> ds.getParent() != null && ds.getParent().getId() == dsBaseCourant.getId())
                .collect(Collectors.toList());

        // parcours en profondeur
        for (EpmTRefDirectionService epmTRefDirectionService : enfantsBase) {
            mapDirService = construitArbre(epmTRefDirectionService, directionService, mapDirService);
            Collections.sort(directionService.getEnfants());
        }

        return mapDirService;
    }


    @Override
    public EpmTRefOrganisme getOrganismeById(Integer idOrganisme) {
        return generiqueDAO.find(EpmTRefOrganisme.class, idOrganisme);
    }

    @Override
    public List<EpmTRefProcedure> getProcedureByIdOrganisme(Integer idOrganisme) {

        final EpmTRefOrganisme refOrganisme = this.getOrganismeById(idOrganisme);
        List<EpmTRefProcedure> refProcedures = generiqueDAO.findAll(EpmTRefProcedure.class);
        return refProcedures.stream()
                .filter(p -> p.getEpmTRefOrganismeSet().stream().anyMatch(o -> o.getId().equals(refOrganisme.getId())))
                .collect(Collectors.toList());

    }

    @Override
    public List<EpmTRefProcedure> getAllProcedures() {
        return generiqueDAO.findAll(EpmTRefProcedure.class);
    }

    @Autowired
    public void setConversionService(DozerBeanMapper conversionService) {
        ReferentielsServiceHTTP.conversionService = conversionService;
    }

    @Autowired
    public void setPlateformeUuid(@Value("${mpe.client}") String plateformeUuid) {
        ReferentielsServiceHTTP.plateformeUuid = plateformeUuid;
    }


    private boolean aExpire() {

        if (null == dateDerniereMiseAjour) {
            return true;
        } else {
            Date maintenant = new Date();

            boolean passerDelaiMiseAJourRef = maintenant.getTime() - dateDerniereMiseAjour.getTime() > DUREE_MISE_A_JOUR_REFERENTIEL;
            boolean referentielModifieApresDerniereMiseAJour = dateDerniereMiseAjour.compareTo(dateDerniereModification) < 0;

            return passerDelaiMiseAJourRef || referentielModifieApresDerniereMiseAJour;
        }
    }

    @Override
    public Date getDateDerniereMiseAjour() {
        return dateDerniereMiseAjour;
    }

    @Override
    public Date getDateDerniereModification() {
        return dateDerniereModification;
    }

    public void setDateDerniereMiseAjour(Date dateDerniereMiseAjour) {
        ReferentielsServiceHTTP.dateDerniereMiseAjour = dateDerniereMiseAjour;
    }

    public void setDateDerniereModification(Date dateDerniereModification) {
        ReferentielsServiceHTTP.dateDerniereModification = dateDerniereModification;
    }
}
