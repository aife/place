package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.EtapesDatesCal;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineConsultation;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineRacine;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.*;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.referentiel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Permet de generer l'ensemble des domaines de donnees (avec
 * genererDomaineRacine) correspondant informations necessaires a la generation
 * d'un document.
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class DocumentBuilder extends AbstractDocumentBuilder {
    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DocumentBuilder.class);

    private EtapesDatesCal etapesDatesCal = null;


    private void genererDomaineCaracteristiques(DomaineRacine domaineRacine) {


        DomaineCaracteristiques domaineCaracteristiques = new DomaineCaracteristiques();
        try {
            getConversionService().map(getConsultation(), domaineCaracteristiques);

            Map<String, String> codeLibelleLieuxExecution = new HashMap<String, String>();
            for (EpmTRefLieuExecution lieuExecution : getConsultation().getLieuxExecution())
                codeLibelleLieuxExecution.put(lieuExecution.getCodeExterne(), lieuExecution.getLibelle());

            domaineCaracteristiques.setCodeLibelleLieuxExecution(codeLibelleLieuxExecution);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw new ApplicationNoyauException(e);
        }

        EpmTRefDirectionService epmTRefDirectionService = getConsultation().getEpmTRefDirectionService();
        domaineCaracteristiques.setAdresseDSResponsable(epmTRefDirectionService.getAdresse());
        domaineCaracteristiques.setCodePostalDSResponsable(epmTRefDirectionService.getCodePostal() != null ? epmTRefDirectionService.getCodePostal() : "");
        domaineCaracteristiques.setVilleDSResponsable(epmTRefDirectionService.getVille() != null ? epmTRefDirectionService.getVille() : "");
        domaineCaracteristiques.setCourrielDSResponsable(epmTRefDirectionService.getCourriel());
        domaineCaracteristiques.setLibelleEtape(getConsultation().getEpmTRefStatut().getLibelle());
        domaineCaracteristiques.setResponsableConsultation(getConsultation().getEpmTUtilisateur().getPrenom() + " " + getConsultation().getEpmTUtilisateur().getNom());
        if (getConsultation().getEpmTRefProcedure() != null && getConsultation().getEpmTRefProcedure().getEpmTRefTypeMarche() != null)
            domaineCaracteristiques.setTypeMarche(getConsultation().getEpmTRefProcedure().getEpmTRefTypeMarche().getLibelle());
        domaineCaracteristiques.setAttributFormalise(getConsultation().getEpmTRefProcedure() != null ? getConsultation().getEpmTRefProcedure().getAttributFormalise() : "");

        try {
            EpmTRefDirectionService benef = getGeneriqueDAO().find(EpmTRefDirectionService.class, getConsultation().getDirServiceVision());
            domaineCaracteristiques.setDirectionServiceBeneficiaire(benef.getLibelle());
        } catch (Exception e) {
            logErreurEtLancerException(LOG, "Erreur lors de la récupération de la direction/service id = " + getConsultation().getDirServiceVision() + ".", e);
        }
        creerDomaineConsultationSiNull(domaineRacine);
        domaineRacine.getConsultation().setCaracteristiques(domaineCaracteristiques);
    }

    /**
     * Ajoute au domaine racine le domaine des lots
     */
    private void genererDomaineLot(DomaineRacine domaineRacine) {
        if (getConsultation() == null) {
            logErreurEtLancerException(LOG, "Impossible de générer les données du domaine lot : consultation null", null);
        } else {
            creerDomaineConsultationSiNull(domaineRacine);
            domaineRacine.getConsultation().setLot(null);
            domaineRacine.getConsultation().setListeLot(genererDomaineLot());
        }
    }

    /**
     * @param domaineRacine
     * @throws ApplicationNoyauException
     */
    private void genererDomaineVariantes(DomaineRacine domaineRacine) {
        if (getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des domaine des variantes obligatoires et/ou autorisées, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        DomaineVariantes domaineVariantes = new DomaineVariantes();
        domaineVariantes.setAutorisee(false);

        EpmTBudLotOuConsultation epmTBudLotOuConsultation = getConsultation().getEpmTBudLotOuConsultation();
        if (epmTBudLotOuConsultation.getVariantesAutorisees() != null && epmTBudLotOuConsultation.getVariantesAutorisees().equals(Constantes.OUI))
            domaineVariantes.setAutorisee(true);

        if (epmTBudLotOuConsultation.getDescriptionOptionsImposees() != null)
            domaineVariantes.setDescription(epmTBudLotOuConsultation.getDescriptionOptionsImposees());

        domaineVariantes.setObligatoire(false);
        if (epmTBudLotOuConsultation.getVariantesExigees() != null && epmTBudLotOuConsultation.getVariantesExigees().equals(Constantes.OUI))
            domaineVariantes.setObligatoire(true);

        domaineRacine.getConsultation().setVariantes(domaineVariantes);
    }

    private void genererDomaineClauseSociale(DomaineRacine domaineRacine) throws ApplicationNoyauException {
        if (getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des domaine des variantes obligatoires et/ou autorisées, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        DomaineClauseSociale clauseSociale = new DomaineClauseSociale();
        if ("oui".equals(getConsultation().getClausesSociales())) {
            clauseSociale.setClauseSociale(true);
            clauseSociale = genererDomaineClauseSocialeGlobal(getConsultation().getEpmTBudLotOuConsultation(), clauseSociale);
        }
        domaineRacine.getConsultation().setClauseSociale(clauseSociale);
    }

    private void genererDomaineClauseEnvironnementale(DomaineRacine domaineRacine) throws ApplicationNoyauException {
        if (getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des domaine des variantes obligatoires et/ou autorisées, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        DomaineClauseEnvironnementale clauseEnvironnementale = new DomaineClauseEnvironnementale();
        if ("oui".equals(getConsultation().getClausesEnvironnementales())) {
            clauseEnvironnementale.setClauseEnvironnementale(true);
            clauseEnvironnementale = genererDomaineClauseEnvironnementaleGlobal(getConsultation().getEpmTBudLotOuConsultation(), clauseEnvironnementale);
        }
        domaineRacine.getConsultation().setClauseEnvironnementale(clauseEnvironnementale);
    }

    private void genererDomaineReconduction(DomaineRacine domaineRacine) throws ApplicationNoyauException {

        if (getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des domaine reconduction, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        DomaineReconduction domaineReconduction = new DomaineReconduction();

        EpmTBudLotOuConsultation epmTBudLotOuConsultation = getConsultation().getEpmTBudLotOuConsultation();
        if (epmTBudLotOuConsultation.getModalitesReconduction() != null)
            domaineReconduction.setModalite(epmTBudLotOuConsultation.getModalitesReconduction());

        if (epmTBudLotOuConsultation.getNbReconductions() != null)
            domaineReconduction.setNombre(epmTBudLotOuConsultation.getNbReconductions());

        domaineReconduction.setReconduction(epmTBudLotOuConsultation.getReconductible() != null &&
                epmTBudLotOuConsultation.getReconductible().equals(Constantes.OUI) &&
                domaineReconduction.getModalite() != null && domaineReconduction.getNombre() != null);
        domaineRacine.getConsultation().setReconduction(domaineReconduction);
    }

    /**
     * @param domaineRacine
     * @throws ApplicationNoyauException
     */
    private void genererDomaineLotTechnique(DomaineRacine domaineRacine) {
        if(getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des lots techniques, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        List<DomaineLotTechique> domaineLotTechniqueList = recupererDomaineLotsTechniques(getConsultation().getEpmTBudLotOuConsultation().getEpmTLotTechniques());
        domaineRacine.getConsultation().setListeLotTechnique(domaineLotTechniqueList);
    }

    /**
     * Ajoute au domaine racine le domaine des tranches (idConsultation necessaire).<br>
     * Appelée par réflexion.
     */
    private void genererDomaineTranches(DomaineRacine domaineRacine) {

        if (getConsultation() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine des tranches, idConsultation nécessaire.", null);

        if (Constantes.OUI.equals(getConsultation().getAllotissement()))
            return;

        Set<EpmTBudTranche> trancheSet = getConsultation().getEpmTBudLotOuConsultation().getEpmTBudTranches();
        List<DomaineTranche> domaineTrancheList = null;
        if (domaineRacine.getConsultation() != null)
            domaineTrancheList = domaineRacine.getConsultation().getTranches();
        if ((domaineTrancheList == null) && (trancheSet != null) && !trancheSet.isEmpty())
            domaineTrancheList = new ArrayList<DomaineTranche>();

        StringBuilder txtFormePrixNonAllotieFracStr = new StringBuilder();
        StringBuilder txtEstimNonAllotieFracStr = new StringBuilder();
        StringBuilder TxtNonAllotieFracStr = new StringBuilder();
        for (EpmTBudTranche tranche : trancheSet) {
            txtFormePrixNonAllotieFracStr.setLength(0);
            txtEstimNonAllotieFracStr.setLength(0);
            TxtNonAllotieFracStr.setLength(0);
            txtFormePrixNonAllotieFracStr.append(tranche.getIntituleTranche()).append(" : ");
            TxtNonAllotieFracStr.append(tranche.getIntituleTranche()).append(" : ");
            txtFormePrixNonAllotieFracStr.append(tranche.getEpmTRefNatureTranche().getLibelle()).append(", ");
            txtEstimNonAllotieFracStr.append(tranche.getEpmTRefNatureTranche().getLibelle()).append(" : ");
            TxtNonAllotieFracStr.append(tranche.getEpmTRefNatureTranche().getLibelle());
            if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPm) {
                txtFormePrixNonAllotieFracStr.append("Prix Mixte.");
                txtEstimNonAllotieFracStr.append(((EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix()).getPuEstimationHt()).append(" HT (Prix Unitaire), ");
                txtEstimNonAllotieFracStr.append(((EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix()).getPfEstimationHt()).append(" HT (Prix Forfaitaire).");
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPu) {
                txtFormePrixNonAllotieFracStr.append("Prix Unitaire.");
                txtEstimNonAllotieFracStr.append(((EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix()).getPuEstimationHt()).append(" HT");
            } else if (tranche.getEpmTBudFormePrix() instanceof EpmTBudFormePrixPf) {
                txtFormePrixNonAllotieFracStr.append("Prix Forfaitaire.");
                txtEstimNonAllotieFracStr.append(((EpmTBudFormePrixPf) tranche.getEpmTBudFormePrix()).getPfEstimationHt()).append(" HT");
            }

            StringBuilder txtEstimNonAllotieNonFracStr = new StringBuilder();
            if ((trancheSet == null) || trancheSet.isEmpty()) {
                EpmTBudFormePrix fdpNonFrac = getConsultation().getEpmTBudLotOuConsultation().getEpmTBudFormePrix();
                if (fdpNonFrac != null) {
                    if (fdpNonFrac instanceof EpmTBudFormePrixPm) {
                        txtEstimNonAllotieNonFracStr.append(((EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix()).getPuEstimationHt()).append(" HT (Prix Unitaire), ");
                        txtEstimNonAllotieNonFracStr.append(((EpmTBudFormePrixPm) tranche.getEpmTBudFormePrix()).getPfEstimationHt()).append(" HT (Prix Forfaitaire).");
                    } else if (fdpNonFrac instanceof EpmTBudFormePrixPu) {
                        txtEstimNonAllotieNonFracStr.append(((EpmTBudFormePrixPu) tranche.getEpmTBudFormePrix()).getPuEstimationHt()).append(" HT");
                    } else if (fdpNonFrac instanceof EpmTBudFormePrixPf) {
                        txtEstimNonAllotieNonFracStr.append(((EpmTBudFormePrixPf) tranche.getEpmTBudFormePrix()).getPfEstimationHt()).append(" HT");
                    }
                }
            }

            DomaineTranche domaineTranche = getDomaineTranche(tranche.getId());
            String codeTranche = tranche.getEpmTRefNatureTranche().getLibelle();
            if (tranche.getCodeTranche() != null && !tranche.getCodeTranche().isEmpty())
                codeTranche += "-" + tranche.getCodeTranche();

            domaineTranche.setCodeTranche(codeTranche);
            domaineTranche.setTxtFormePrixNonAllotieFractionnee(txtFormePrixNonAllotieFracStr.toString());
            domaineTranche.setTxtEstimationNonAllotieFractionnee(txtEstimNonAllotieFracStr.toString());
            domaineTranche.setTxtNonAllotieFractionnee(TxtNonAllotieFracStr.toString());
            domaineTranche.setTxtEstimationNonAllotieNonFractionnee(txtEstimNonAllotieNonFracStr.toString());
            domaineTranche.setIntituleTranche(tranche.getIntituleTranche());
            domaineTranche.setNatureTranche(tranche.getEpmTRefNatureTranche().getLibelle());
            if (!domaineTrancheList.contains(domaineTranche))
                domaineTrancheList.add(domaineTranche);
        }

        if (domaineTrancheList != null) {
            creerDomaineConsultationSiNull(domaineRacine);
            domaineRacine.getConsultation().setTranches(domaineTrancheList);
        }
    }


    /**
     * charger le lot ou la consultation.
     * @return lotDissocie : if lot dissocie.
     */
    private boolean chargerLotouConsultation() {
        boolean lotDissocie = false;
        if (getConsultation() == null && getIdLot() == null)
            logErreurEtLancerException(LOG, "Impossible de générer le domaine de données du classement des offres" + ", idConsultation ou idLot (si dissocié) nécessaire(s).", null);

        if (getIdLot() != null) {

            if (getLot() != null && getLot().getEpmTBudLotOuConsultation() != null && getLot().getEpmTBudLotOuConsultation().isLotDissocie())
                lotDissocie = true;
        }
        return lotDissocie;
    }


    private void genererDomaineFormeDePrix(DomaineRacine domaineRacine) {

        DomaineFormeDePrix domFormeDePrix = null;
        EpmTBudLotOuConsultation lotOuConsultation = null;

        if (getConsultation() == null && getIdLot() == null)
            logErreurEtLancerException(LOG, "Impossible de generer le domaine forme de prix : idConsultation " + "ou idLot necessaires.", null);

        if (getIdLot() != null) {
            lotOuConsultation = getLot().getEpmTBudLotOuConsultation();
        } else {
            if (!Constantes.OUI.equals(getConsultation().getAllotissement()))
                lotOuConsultation = getConsultation().getEpmTBudLotOuConsultation();
            else
                domFormeDePrix = new DomaineFormeDePrix(); // Forme de prix vide au niveau consultation pour une consultation allotie
        }

        if (lotOuConsultation != null) {
            EpmTBudFormePrix formePrix = lotOuConsultation.getEpmTBudFormePrix();
            // Forme de prix de la consultation/(lot principal)
            domFormeDePrix = genererDomaineFormeDePrix(formePrix);

            // Forme de prix des tranches de la consultation/(lot principal)
            Set<EpmTBudTranche> trancheSet = lotOuConsultation.getEpmTBudTranches();
            if ((trancheSet != null) && !trancheSet.isEmpty()) {
                for (EpmTBudTranche trancheItem : trancheSet) {
                    DomaineFormeDePrix domFormDePrixTranche = genererDomaineFormeDePrix(trancheItem.getEpmTBudFormePrix());
                    DomaineTranche domTranche = getDomaineTranche(trancheItem.getId());
                    domTranche.setFormeDePrix(domFormDePrixTranche);
                    domTranche.setCodeTranche(trancheItem.getCodeTranche());
                    if (domTranche.getIntituleTranche() == null)
                        domTranche.setIntituleTranche(trancheItem.getIntituleTranche());
                    creerDomaineConsultationSiNull(domaineRacine);
                    List<DomaineTranche> domTrancheList = domaineRacine.getConsultation().getTranches();
                    if (domTrancheList == null) {
                        domTrancheList = new ArrayList<DomaineTranche>();
                        domaineRacine.getConsultation().setTranches(domTrancheList);
                    }
                    if (!domTrancheList.contains(domTranche))
                        domTrancheList.add(domTranche);
                }
            }
        }

        // Forme de prix de chacun des lots de la consultation
        if (getLot() == null && Constantes.OUI.equals(getConsultation().getAllotissement())) {
            for (EpmTBudLot lotItem : getConsultation().getEpmTBudLots()) {
                DomaineFormeDePrix domFormePrixLot = genererDomaineFormeDePrix(lotItem.getEpmTBudLotOuConsultation().getEpmTBudFormePrix());
                DomaineLot domLot = getDomaineLot(lotItem.getNumeroLot());
                domLot.setFormeDePrix(domFormePrixLot);
                creerDomaineConsultationSiNull(domaineRacine);
                List<DomaineLot> domLotList = domaineRacine.getConsultation().getListeLot();
                if (domLotList == null) {
                    domLotList = new ArrayList<DomaineLot>();
                    domaineRacine.getConsultation().setListeLot(domLotList);
                }
                if (!domLotList.contains(domLot))
                    domLotList.add(domLot);

                // Pour chaque lot : Forme de prix de chacune des tranches
                for (EpmTBudTranche trancheItem : (Set<EpmTBudTranche>) lotItem.getEpmTBudLotOuConsultation().getEpmTBudTranches()) {
                    DomaineFormeDePrix domFormDePrixTranche = genererDomaineFormeDePrix(trancheItem.getEpmTBudFormePrix());
                    DomaineTranche domTranche = getDomaineTranche(trancheItem.getId());
                    domTranche.setFormeDePrix(domFormDePrixTranche);
                    creerDomaineConsultationSiNull(domaineRacine);
                    List<DomaineTranche> domTrancheList = domaineRacine.getConsultation().getTranches();
                    if (domTrancheList == null) {
                        domTrancheList = new ArrayList<DomaineTranche>();
                        domaineRacine.getConsultation().setTranches(domTrancheList);
                    }
                    if (!domTrancheList.contains(domTranche))
                        domTrancheList.add(domTranche);
                }
            }
        }
        creerDomaineConsultationSiNull(domaineRacine);
        domaineRacine.getConsultation().setFormeDePrix(domFormeDePrix);
    }

    public DomaineRacine genererDomainePairesTypes() {
        DomaineRacine domaineRacine = new DomaineRacine();

        creerDomaineConsultationSiNull(domaineRacine);
        genererDomaineCaracteristiques(domaineRacine);
        genererDomaineDureeMarche(domaineRacine.getConsultation());

        domaineRacine.getConsultation()
                .setNombreCandidatsFixe(getConsultation().getNombreCandidatsFixe())
                .setNombreCandidatsMin(getConsultation().getNombreCandidatsMin())
                .setNombreCandidatsMax(getConsultation().getNombreCandidatsMax());

        if (!getConsultation().getEpmTBudLots().isEmpty()) {
            //cas d'une consultation allotie
            if (getIdLot() != null) {
                //si un lot a été choisi on ne prend que les données de ce lot
                Set<EpmTBudLot> lots = getConsultation().getEpmTBudLots();
                EpmTBudLot lotChoisi = null;
                // On recherche le lot correspondant à l'idLot
                for (EpmTBudLot lot : lots) {
                    if (lot.getId() == getIdLot()) {
                        lotChoisi = lot;
                        break;
                    }
                }
                if (lotChoisi == null) {
                    logErreurEtLancerException(LOG, "Lot non trouvé pour l'idLot " + getIdLot() + ".", null);
                } else {
                    domaineRacine.getConsultation().setLot(genererDomaineLotUnique(lotChoisi, getConsultation()));
                    DomaineLot domaineLot = domaineRacine.getConsultation().getLot();
                    List<DomaineLot> listeLot = new ArrayList<DomaineLot>();
                    listeLot.add(domaineLot);
                    domaineRacine.getConsultation().setListeLot(listeLot);

                }
            } else {
                //si aucun lot n'a été choisi on prend les données de tous les lots
                genererDomaineLot(domaineRacine);
            }

        } else {
            // consultation non allotie
            DomainePairesTypes pairesTypes = new DomainePairesTypes();
            DomaineConsultation domaineConsultation = domaineRacine.getConsultation();
            domaineConsultation.setPairesTypes(pairesTypes);
            if (getConsultation().getEpmTBudLotOuConsultation() != null && getConsultation().getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternes() != null) {
                for (EpmTValeurConditionnementExterne valeurConditionnementExterne : getConsultation().getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternes()) {
                    DomainePaireType paireType = new DomainePaireType();
                    paireType.setClef(valeurConditionnementExterne.getClef().toUpperCase());
                    paireType.setValeur(valeurConditionnementExterne.getValeur());
                    pairesTypes.getPairesTypes().put(valeurConditionnementExterne.getClef().toUpperCase(), paireType);
                }

                genererDomaineTranches(domaineRacine);
                genererDomaineFormeDePrix(domaineRacine);
                genererDomaineLotTechnique(domaineRacine);
                genererDomaineReconduction(domaineRacine);
                genererDomaineVariantes(domaineRacine);
                genererDomaineClauseSociale(domaineRacine);
                genererDomaineClauseEnvironnementale(domaineRacine);
            }
            if (getIdLot() != null)
                pairesTypes.setIdLot(getIdLot());

            // Recherche des criteres d'attribution
            if (getConsultation().getAllotissement() != null && Constantes.NON.equals(getConsultation().getAllotissement())) {
                List<DomaineCritereConsultation> domaineCritereAttribution = recupererDomaineCritereAttribution(getConsultation().getListeCritereAttributionTriee());
                domaineConsultation.setListeCriteres(domaineCritereAttribution);

                if (getConsultation().getCritereAttribution() != null)
                    domaineConsultation.setTypeCriteresAttribution(getConsultation().getCritereAttribution().getLibelle());

                if (getConsultation().getCritereAttribution() != null)
                    domaineConsultation.setTypeCriteresAttribution(getConsultation().getCritereAttribution().getCodeExterne());
            }
            domaineConsultation.setCritereParLot(!getConsultation().isCritereAppliqueTousLots());

            DomainePairesTypesComplexes pairesTypesComplexes = new DomainePairesTypesComplexes();
            if (getConsultation().getEpmTBudLotOuConsultation() != null && getConsultation().getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternesComplexe() != null) {
                for (EpmTValeurConditionnementExterneComplexe valeurConditionnementExterneComplexe : getConsultation().getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternesComplexe()) {
                    DomainePairesTypes listPairesTypes = new DomainePairesTypes();
                    listPairesTypes.setClef(valeurConditionnementExterneComplexe.getIdObjetComplexe());
                    for (EpmTValeurConditionnementExterne valeurConditionnementExterne : valeurConditionnementExterneComplexe.getEpmTValeurConditionnementExternes()) {
                        DomainePaireType paireType = new DomainePaireType();
                        paireType.setClef(valeurConditionnementExterne.getClef().toUpperCase());
                        paireType.setValeur(valeurConditionnementExterne.getValeur());
                        listPairesTypes.getPairesTypes().put(valeurConditionnementExterne.getClef().toUpperCase(), paireType);
                    }
                    if (getIdLot() != null)
                        listPairesTypes.setIdLot(getIdLot());
                    pairesTypesComplexes.getPairesTypesComplexes().add(listPairesTypes);
                }
            }
            domaineRacine.getConsultation().setPairesTypesComplexes(pairesTypesComplexes);
        }

        return domaineRacine;
    }



}
