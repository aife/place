package fr.paris.epm.noyau.metier.synchronisationSocle;

import fr.atexo.commun.synchronisation.socle.SynchronisationSocleUtil;
import fr.atexo.commun.synchronisation.socle.beans.Transaction;
import fr.atexo.commun.synchronisation.socle.beans.Transaction.Response.Entities;
import fr.atexo.commun.synchronisation.socle.beans.Transaction.Response.Entities.Agent;
import fr.atexo.commun.synchronisation.socle.beans.Transaction.Response.Entities.Entity;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.DirectionServiceCritere;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.OrganismeCritere;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.metier.objetvaleur.Doublet;
import fr.paris.epm.noyau.metier.synchronisationSocle.exceptions.SynchronisationException;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class SynchronisationAgents implements Synchronisable {

    private static final int CODE_DIRECTION_DEFAUT = 0;

    private static final String DOUBLE_COTE = "\"";

    private static final String DOUBLE_COTE_JAVA = "\\\"";

    @Resource
    private GeneriqueDAO generiqueDAO;

    /**
     * Chemin temporaire de génération du fichier contenant la réponse xml
     */
    @Value("${openoffice.fichierTemporaire}")
    private String cheminFichierReponse;

    /**
     * URL du webservice
     */
    @Value("${synchro.agent.url}")
    private String urlWebServiceSocle;

    @Value("${mpe.client}")
    private String plateformeUuid;

    /**
     * Identifiant de la direction service en base epm__ref_direction_service
     */
    @Value("${utilisateur.idDirectionService}")
    private int idDirectionService;

    @Override
    public void synchroniser() throws SynchronisationException {
        try {
            final SynchronisationSocleUtil synchroSocle = new SynchronisationSocleUtil(cheminFichierReponse, urlWebServiceSocle);
            final Transaction transactionRequest = synchroSocle.getTransactionRequestEntities();
            final Transaction transactionResponse = synchroSocle.getTransactionResponse(transactionRequest);

            if (transactionResponse != null && transactionResponse.getResponse() != null && transactionResponse.getResponse().getEntities() != null) {
                final Entities entities = transactionResponse.getResponse().getEntities();

                OrganismeCritere organismeCritere = new OrganismeCritere(plateformeUuid);

                final List<EpmTRefOrganisme> listEpmTRefOrganismes = generiqueDAO.findByCritere(organismeCritere);
                final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne = listEpmTRefOrganismes.stream().collect(Collectors.toMap(EpmTRefOrganisme::getCodeExterne, Function.identity()));

                final List<String> codesExterne = entities.getEntity().stream().map(Entity::getId).map(Objects::toString).collect(Collectors.toList());
                codesExterne.add("0");

                final List<EpmTRefDirectionService> listEpmTRefDirectionService = new ArrayList<>();


                DirectionServiceCritere critere = new DirectionServiceCritere(plateformeUuid);
                critere.setProprieteTriee("libelle");
                final List<EpmTRefDirectionService> all = generiqueDAO.findByCritere(critere);
                all.forEach(epmTRefDirectionService -> {
                    if (codesExterne.contains(epmTRefDirectionService.getCodeExterne())) {
                        listEpmTRefDirectionService.add(epmTRefDirectionService);
                    } else {
                        epmTRefDirectionService.setActif(false);
                        generiqueDAO.merge(epmTRefDirectionService);
                    }
                });
                final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme = listEpmTRefDirectionService.stream().filter(ds -> ds.getCodeExterne() != null && ds.getCodeOrganisme() != null).collect(Collectors.toMap(ds -> new Doublet(Integer.parseInt(ds.getCodeExterne()), ds.getCodeOrganisme()), Function.identity()));

                final Collection<List<Agent>> agents = entities.getAgent().size() > 9999 ? entities.getAgent().stream().collect(Collectors.groupingBy(s -> entities.getAgent().indexOf(s) / 5000)) /* problème des grosses requetes */.values() : new HashSet<>() {{
                    add(entities.getAgent());
                }};

                final List<EpmTUtilisateur> listEpmTUtilisateur = agents.stream().map(sousList -> {
                    final UtilisateurCritere utilisateurCritere = new UtilisateurCritere(plateformeUuid);
                    utilisateurCritere.setGuidsSso(sousList.stream().map(Agent::getLogin).collect(Collectors.toList()));
                    return generiqueDAO.<EpmTUtilisateur>findByCritere(utilisateurCritere);
                }).flatMap(Collection::stream).collect(Collectors.toList());

                final Map<String, EpmTUtilisateur> mapUtilisateurByGuidSso = listEpmTUtilisateur.stream().collect(Collectors.toMap(EpmTUtilisateur::getGuidSso, Function.identity(), (u1, u2) -> u1));

                try {
                    getLogger().info("Début de la synchronisation des directions/services");
                    miseAJourDirectionFictiveOrganisme(mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);
                    miseAJourEntites(entities.getEntity(), mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);
                    getLogger().info("Fin de la synchronisation des directions/services");
                } catch (final NonTrouveNoyauException | TechnicalNoyauException e) {
                    getLogger().error("Erreur lors de la synchronisation des directions/services", e);
                }

                try {
                    getLogger().info("Début de la synchronisation des utilisateurs");
                    miseAJourUtilisateurs(entities.getAgent(), mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme, mapUtilisateurByGuidSso);
                    getLogger().info("Fin de la synchronisation des utilisateurs");
                } catch (final NonTrouveNoyauException | TechnicalNoyauException e) {
                    getLogger().error("Erreur lors de la synchronisation des utilisateurs", e);
                }
            } else {
                if (transactionResponse == null || transactionResponse.getResponse() == null) {
                    getLogger().error("La réponse du web services MPE de récupération des agents à l'URL '{}' est vide.", urlWebServiceSocle);
                } else {
                    getLogger().warn("Aucun agent à synchroniser à l'URL '{}. Repoonse XML = {}", urlWebServiceSocle, transactionResponse);
                }
            }
        } catch (final Exception e) {
            throw new SynchronisationException("Une erreur imprévue est survenue lors de la synchronisation des organismes", e);
        }
    }

    /**
     * Méthode de mise à jour des directions service depuis le xml reçu de MPE
     */
    private void miseAJourEntites(final List<Entity> listeDirections, final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne, final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme) {

        if (listeDirections != null) {
            for (final Entity directionMPE : listeDirections) {
                final Doublet clef = new Doublet(directionMPE.getId(), directionMPE.getAcronymOrganism());
                EpmTRefDirectionService directionServiceRSEM = mapDirectionServiceByCodeAndOrganisme.get(clef);
                if (directionServiceRSEM != null) {
                    final boolean aMerger = miseAJourDonneesDirectionService(directionMPE, directionServiceRSEM, mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);
                    if (aMerger) {
                        try {
                            directionServiceRSEM = generiqueDAO.merge(directionServiceRSEM);
                            mapDirectionServiceByCodeAndOrganisme.put(clef, directionServiceRSEM);

                            getLogger().info("La direction/service '{}' (id = {}, code externe = {}) vient d'être mise à jour avec succès", directionServiceRSEM.getLibelle(), directionServiceRSEM.getId(), directionServiceRSEM.getCodeExterne());
                        } catch (final Exception e) {
                            getLogger().error("Erreur lors de la mise a jour de la direction/service '{}' (id = {}, code externe = {})", directionServiceRSEM.getLibelle(), directionServiceRSEM.getId(), directionServiceRSEM.getCodeExterne(), e);
                            throw new TechnicalNoyauException(e);
                        }
                    }
                } else {
                    directionServiceRSEM = new EpmTRefDirectionService();
                    miseAJourDonneesDirectionService(directionMPE, directionServiceRSEM, mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);
                    try {
                        generiqueDAO.persist(directionServiceRSEM);
                        mapDirectionServiceByCodeAndOrganisme.put(clef, directionServiceRSEM);

                        getLogger().info("La direction/service '{}' (id = {}, code externe = {}) vient d'être ajoutée avec succès", directionServiceRSEM.getLibelle(), directionServiceRSEM.getId(), directionServiceRSEM.getCodeExterne());
                    } catch (final Exception e) {
                        getLogger().error("Erreur lors de création de la direction/service '{}' (id = {}, code externe = {})", directionServiceRSEM.getLibelle(), directionServiceRSEM.getId(), directionServiceRSEM.getCodeExterne(), e);
                        throw new TechnicalNoyauException(e);
                    }
                }
            }
        }
    }

    /**
     * Remplis un objet EpmTRefDirectionService à partir des données venant de mpe (objet Entity)
     *
     * @param directionMPE         Entity venant du webservice de mpe
     * @param directionServiceRSEM EpmTRefDirectionService de rsem à mettre à jour avec les donénes de directionMPE
     */
    private static boolean miseAJourDonneesDirectionService(final Entity directionMPE, final EpmTRefDirectionService directionServiceRSEM, final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne, final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme) {

        boolean aMerger = !directionServiceRSEM.isActif(); // ici -> directionServiceRSEM.isActif() != true; -> alors à merger
        directionServiceRSEM.setActif(true);

        String adresse = StringEscapeUtils.unescapeHtml(directionMPE.getAddress());
        adresse = adresse.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);
        String codePostal = StringEscapeUtils.unescapeHtml(directionMPE.getPostalCode());
        codePostal = codePostal.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);
        String ville = StringEscapeUtils.unescapeHtml(directionMPE.getCity());
        ville = ville.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);
        String pays = StringEscapeUtils.unescapeHtml(directionMPE.getCountry());
        pays = pays.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getCodePostal(), codePostal) || aMerger;
        directionServiceRSEM.setCodePostal(codePostal);

        adresse = adresse + " " + codePostal + " " + ville + " " + pays;
        aMerger = !Objects.equals(directionServiceRSEM.getAdresse(), adresse) || aMerger;
        directionServiceRSEM.setAdresse(adresse);

        String email = StringEscapeUtils.unescapeHtml(directionMPE.getEmail());
        email = email.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getCourriel(), email) || aMerger;
        directionServiceRSEM.setCourriel(email);

        String fax = StringEscapeUtils.unescapeHtml(directionMPE.getFax());
        fax = fax.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getFax(), fax) || aMerger;
        directionServiceRSEM.setFax(fax);

        aMerger = !Objects.equals(directionServiceRSEM.getCodeExterne(), "" + directionMPE.getId()) || aMerger;
        directionServiceRSEM.setCodeExterne("" + directionMPE.getId());

        String acronymeOrganisme = StringEscapeUtils.unescapeHtml(directionMPE.getAcronymOrganism());
        acronymeOrganisme = acronymeOrganisme.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getCodeOrganisme(), acronymeOrganisme) || aMerger;
        directionServiceRSEM.setCodeOrganisme(acronymeOrganisme);

        final EpmTRefOrganisme epmTRefOrganisme = mapOrganismesByCodeExterne.get(acronymeOrganisme);
        directionServiceRSEM.setEpmTRefOrganisme(epmTRefOrganisme);

        final Doublet clefParent = new Doublet(directionMPE.getParentId(), directionMPE.getAcronymOrganism());
        final EpmTRefDirectionService directionServiceParent = mapDirectionServiceByCodeAndOrganisme.get(clefParent);

        if (directionServiceRSEM.getParent() == null) {
            aMerger = directionServiceParent != null || aMerger;
        } else {
            aMerger = directionServiceParent == null || !Objects.equals(directionServiceRSEM.getParent().getId(), directionServiceParent.getId()) || aMerger;
        }
        directionServiceRSEM.setParent(directionServiceParent);

        String telephone = StringEscapeUtils.unescapeHtml(directionMPE.getPhone());
        telephone = telephone.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getTelephone(), telephone) || aMerger;
        directionServiceRSEM.setTelephone(telephone);

        String libelle = StringEscapeUtils.unescapeHtml(directionMPE.getTitle());
        libelle = libelle.replace(DOUBLE_COTE, DOUBLE_COTE_JAVA);

        aMerger = !Objects.equals(directionServiceRSEM.getLibelle(), libelle) || aMerger;
        directionServiceRSEM.setLibelle(libelle);

        aMerger = !Objects.equals(directionServiceRSEM.getLibelleCourt(), libelle) || aMerger;
        directionServiceRSEM.setLibelleCourt(libelle);


        directionServiceRSEM.setTransverse(true);
        directionServiceRSEM.setTypeEntite("1");

        return aMerger;
    }

    /**
     * Méthode de mise à jour des utilisateurs depuis le xml reçu de MPE
     */
    private void miseAJourUtilisateurs(final List<Agent> agents, final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne, final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme, final Map<String, EpmTUtilisateur> mapUtilisateurByGuidSso) {

        if (agents != null) {
            for (final Agent agent : agents) {
                EpmTUtilisateur utilisateursRSEM = mapUtilisateurByGuidSso.get(agent.getLogin());
                if (utilisateursRSEM != null) {
                    final boolean aMerger = miseAJourDonneesUtilisateur(utilisateursRSEM, agent, mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);
                    if (aMerger) {
                        try {
                            utilisateursRSEM = generiqueDAO.merge(utilisateursRSEM);
                            mapUtilisateurByGuidSso.put(utilisateursRSEM.getGuidSso(), utilisateursRSEM);

                            getLogger().info("L'utilisateur '{}' existe déjà, il vient d'être mis a jour avec succès", utilisateursRSEM.getGuidSso());
                        } catch (final Exception e) {
                            getLogger().error("Erreur lors de la mise à jour de l'utilisateur '{}'", utilisateursRSEM.getGuidSso(), e);
                            throw new TechnicalNoyauException(e);
                        }
                    } else {
                        getLogger().info("L'utilisateur '{}' existe déjà mais il n'a pas été modifié, pas de mise a jour", utilisateursRSEM.getGuidSso());
                    }
                } else {
                    utilisateursRSEM = new EpmTUtilisateur();
                    miseAJourDonneesUtilisateur(utilisateursRSEM, agent, mapOrganismesByCodeExterne, mapDirectionServiceByCodeAndOrganisme);

                    try {
                        generiqueDAO.persist(utilisateursRSEM);
                        mapUtilisateurByGuidSso.put(utilisateursRSEM.getGuidSso(), utilisateursRSEM);
                        getLogger().info("L'utilisateur '{}' vient d'être ajouté avec succès", utilisateursRSEM.getGuidSso());
                    } catch (final Exception e) {
                        getLogger().error("Erreur lors de création de l'utilisateur '{}'", utilisateursRSEM.getGuidSso(), e);
                        throw new TechnicalNoyauException(e);
                    }
                }
            }
        }
    }

    private boolean miseAJourDonneesUtilisateur(final EpmTUtilisateur utilisateur, final Agent agent, final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne, final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme) {

        final BiFunction<Object, Object, Boolean> notEquals = (param1, param2) -> (param1 == null && param2 != null) || (param1 != null && !param1.equals(param2));

        // ------------
        // GUID
        // ------------
        boolean aMerger = notEquals.apply(utilisateur.getGuidSso(), agent.getLogin());

        if (notEquals.apply(utilisateur.getGuidSso(), agent.getLogin())) {
            getLogger().info("Le login de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getGuidSso(), agent.getLogin());
        }

        utilisateur.setGuidSso(agent.getLogin());

        // ------------
        // ACTIVATION
        // ------------
        aMerger = (utilisateur.isActif() != agent.isCompteActif()) || aMerger;

        if (utilisateur.isActif() != agent.isCompteActif()) {
            if (utilisateur.isActif()) {
                getLogger().info("L'utilisateur '{}' à été désactivé", utilisateur.getGuidSso());
            } else {
                getLogger().info("L'utilisateur '{}' à été activé", utilisateur.getGuidSso());
            }
        }

        utilisateur.setActif(agent.isCompteActif());

        // ------------
        // ORGANISME
        // ------------
        aMerger = notEquals.apply(utilisateur.getCodeOrganisme(), agent.getAcronymOrganism()) || aMerger;

        if (notEquals.apply(utilisateur.getCodeOrganisme(), agent.getAcronymOrganism())) {
            getLogger().info("Le code organisme de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getCodeOrganisme(), agent.getAcronymOrganism());
        }

        utilisateur.setCodeOrganisme(agent.getAcronymOrganism());

        final EpmTRefOrganisme epmTOrganisme = mapOrganismesByCodeExterne.get(agent.getAcronymOrganism());
        utilisateur.setEpmTRefOrganisme(epmTOrganisme);

        // ------------
        // EMAIL
        // ------------
        aMerger = notEquals.apply(utilisateur.getCourriel(), agent.getEmail()) || aMerger;

        if (notEquals.apply(utilisateur.getCourriel(), agent.getEmail())) {
            getLogger().info("L'email de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getCourriel(), agent.getEmail());
        }

        utilisateur.setCourriel(agent.getEmail());

        // ------------
        // FAX
        // ------------
        aMerger = notEquals.apply(utilisateur.getFax(), agent.getFax()) || aMerger;

        if (notEquals.apply(utilisateur.getFax(), agent.getFax())) {
            getLogger().info("Le fax de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getFax(), agent.getFax());
        }

        utilisateur.setFax(agent.getFax());

        // ------------
        // PRENOM
        // ------------
        aMerger = notEquals.apply(utilisateur.getPrenom(), agent.getFirstname()) || aMerger;

        if (notEquals.apply(utilisateur.getPrenom(), agent.getFirstname())) {
            getLogger().info("Le prenom de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getPrenom(), agent.getFirstname());
        }

        utilisateur.setPrenom(agent.getFirstname());

        // ------------
        // NOM
        // ------------
        aMerger = notEquals.apply(utilisateur.getNom(), agent.getLastname()) || aMerger;

        if (notEquals.apply(utilisateur.getNom(), agent.getLastname())) {
            getLogger().info("Le nom de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getNom(), agent.getLastname());
        }

        utilisateur.setNom(agent.getLastname());

        // ------------
        // MOT DE PASSE
        // ------------
        aMerger = notEquals.apply(utilisateur.getMotDePasse(), agent.getPassword()) || aMerger;

        if (notEquals.apply(utilisateur.getMotDePasse(), agent.getPassword())) {
            getLogger().info("Le mot de passe de l'utilisateur '{}' a changé", utilisateur.getGuidSso());
        }

        utilisateur.setMotDePasse(agent.getPassword());

        // ------------
        // TELEPHONE
        // ------------
        aMerger = notEquals.apply(utilisateur.getTelephone(), agent.getPhone()) || aMerger;

        if (notEquals.apply(utilisateur.getTelephone(), agent.getPhone())) {
            getLogger().info("Le téléphone de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getTelephone(), agent.getPhone());
        }

        utilisateur.setTelephone(agent.getPhone());

        // ------------
        // IDENTIFIANT
        // ------------
        aMerger = notEquals.apply(utilisateur.getIdentifiant(), agent.getLogin()) || aMerger;

        if (notEquals.apply(utilisateur.getIdentifiant(), agent.getLogin())) {
            getLogger().info("L'identifiant de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getIdentifiant(), agent.getLogin());
        }

        utilisateur.setIdentifiant(agent.getLogin());

        final EpmTRefDirectionService direction = mapDirectionServiceByCodeAndOrganisme.get(new Doublet(agent.getEntityId(), agent.getAcronymOrganism()));

        if (direction != null) {
            aMerger = utilisateur.getDirectionService() != direction.getId() || aMerger;

            if (utilisateur.getDirectionService() != direction.getId()) {
                getLogger().info("La direction/service de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getDirectionService(), direction.getId());
            }

            utilisateur.setDirectionService(direction.getId());
        } else {
            aMerger = utilisateur.getDirectionService() != idDirectionService || aMerger;

            if (utilisateur.getDirectionService() != idDirectionService) {
                getLogger().info("La direction/service de l'utilisateur '{}' a changé (de '{}' à '{}')", utilisateur.getGuidSso(), utilisateur.getDirectionService(), idDirectionService);
            }

            utilisateur.setDirectionService(idDirectionService);
        }
        return aMerger;
    }

    /**
     * Dans mpe un utilisateur peut être rataché directement à un organisme sans
     * être lié à une consultation ce qui n'est pas possible dans RSEM. Pour
     * contourner ce cas on va créer une direction qui reprend les infos de
     * l'organisme et ces utilsateurs y seront ratachés.
     * Cette méthode créé une liste d'objet EpmTRefDirectionService à partir des organismes et les persiste.
     */
    private void miseAJourDirectionFictiveOrganisme(final Map<String, EpmTRefOrganisme> mapOrganismesByCodeExterne, final Map<Doublet, EpmTRefDirectionService> mapDirectionServiceByCodeAndOrganisme) {

        for (final EpmTRefOrganisme epmTRefOrganisme : mapOrganismesByCodeExterne.values()) {
            if (!mapDirectionServiceByCodeAndOrganisme.containsKey(new Doublet(CODE_DIRECTION_DEFAUT, epmTRefOrganisme.getCodeExterne()))) {
                final EpmTRefDirectionService nouvelleDirectionFictive = new EpmTRefDirectionService();
                nouvelleDirectionFictive.setActif(true);
                nouvelleDirectionFictive.setTransverse(true);
                nouvelleDirectionFictive.setCodeOrganisme(epmTRefOrganisme.getCodeExterne());
                nouvelleDirectionFictive.setCodeExterne("" + CODE_DIRECTION_DEFAUT);
                nouvelleDirectionFictive.setEpmTRefOrganisme(epmTRefOrganisme);
                nouvelleDirectionFictive.setLibelle(epmTRefOrganisme.getLibelle());
                nouvelleDirectionFictive.setLibelleCourt(epmTRefOrganisme.getLibelle());
                nouvelleDirectionFictive.setTypeEntite("0");
                nouvelleDirectionFictive.setCourriel("");

                try {
                    generiqueDAO.persist(nouvelleDirectionFictive);
                    final Doublet clef = new Doublet(Integer.parseInt(nouvelleDirectionFictive.getCodeExterne()), nouvelleDirectionFictive.getCodeOrganisme());
                    mapDirectionServiceByCodeAndOrganisme.put(clef, nouvelleDirectionFictive);

                    getLogger().info("La direction/service fictive '{}' (id = {}, code externe = {}) vient d'être ajouté avec succès", nouvelleDirectionFictive.getLibelle(), nouvelleDirectionFictive.getId(), nouvelleDirectionFictive.getCodeExterne());
                } catch (final Exception e) {
                    getLogger().error("Erreur lors de création de la direction/service fictive '{}' (id = {}, code externe = {}).", nouvelleDirectionFictive.getLibelle(), nouvelleDirectionFictive.getId(), nouvelleDirectionFictive.getCodeExterne(), e);
                    throw new TechnicalNoyauException(e);
                }
            }
        }
    }

    /**
     * @param valeur the cheminFichierReponse to set
     */
    public final void setCheminFichierReponse(final String valeur) {
        cheminFichierReponse = valeur;
    }

    /**
     * @param valeur the urlWebServiceSocle to set
     */
    public final void setUrlWebServiceSocle(final String valeur) {
        urlWebServiceSocle = valeur;
    }

    public void setIdDirectionService(final int valeur) {
        idDirectionService = valeur;
    }

    public final void setGeneriqueDAO(final GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }
}
