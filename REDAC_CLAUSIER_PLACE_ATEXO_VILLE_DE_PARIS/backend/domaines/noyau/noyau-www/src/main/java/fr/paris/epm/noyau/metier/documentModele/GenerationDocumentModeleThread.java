package fr.paris.epm.noyau.metier.documentModele;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.schlichtherle.io.FileOutputStream;
import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.generation.service.openoffice.OpenOfficeService;
import fr.atexo.commun.document.generation.service.xls.XLSGenerationService;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.SuiviGenerationDocumentModele;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineRacine;
import fr.paris.epm.noyau.persistance.documents.EpmTDocumentModele;
import freemarker.template.utility.ObjectConstructor;

/**
 * Thread de génération du document.
 * 
 * @author Léon Barsamian
 */
public class GenerationDocumentModeleThread extends Thread {

	/**
	 * Loggueur.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(GenerationDocumentModeleThread.class);
	/**
	 * Modéle de document à générer.
	 */
	private EpmTDocumentModele docModele;

	/**
	 * Données .
	 */
	private DomaineRacine domaine;

	/**
	 * Chemin des fichiers temporaire pour la génération.
	 */
	private String cheminFichierTemporaire;

	/**
	 * Url du serveur openoffice (adresse ip).
	 */
	private String urlOO;

	/**
	 * Port du server openoffice.
	 */
	private String portOO;

	/**
	 * Objet de suivi de la génération du document.
	 */
	private SuiviGenerationDocumentModele suivi;

	/**
	 * Le template à remplir.
	 */
	private byte[] fichierTemplate;

	/**
	 * Lance la génération d'un document dans un thread.
	 * 
	 * @param docModeleParam Modéle de document à générer.
	 * @param domaineParam Données.
	 * @param fichierTemporaireParam Chemin des fichiers temporaire pour la génération.
	 * @param urlOOParam Url du serveur openoffice (adresse ip).
	 * @param portOOParam Port du server openoffice.
	 * @param suiviParam Objet de suivi de la génération du document.
	 */
	public GenerationDocumentModeleThread(
			final EpmTDocumentModele docModeleParam,
			final DomaineRacine domaineParam,
			final String fichierTemporaireParam, final String urlOOParam,
			final String portOOParam,
			final SuiviGenerationDocumentModele suiviParam,
			final byte[] fichierTemplateParam) {
		super();
		this.docModele = docModeleParam;
		this.domaine = domaineParam;
		this.cheminFichierTemporaire = fichierTemporaireParam;
		this.urlOO = urlOOParam;
		this.portOO = portOOParam;
		this.suivi = suiviParam;
		this.fichierTemplate = fichierTemplateParam;
	}

	@Override
	public void run() {
        LOG.info("Debut de la tache de la génération du doc.");

		File repertoire = null;
		File copie = null;

		InputStream is = null;
		FileOutputStream fos = null;
		File resultatFreeMarker = null;
		TypeFormat formatExport = null;

		try {
			formatExport = TypeFormat.valueOf(docModele.getType().getLibelle());

			DocumentModeleDTO documentModele = DocumentModeleDTO.getInstancePourGenerationDocument(formatExport);
			
			List<ChampFusionDTO> listeChampDTO = DocumentDTOUtil.creerListeChampFusionDTOPourGeneration(docModele.getChampsDeFusion(), domaine);

			long timeStamp = System.nanoTime();
			
			String nomFichierFinal ="resultat"+timeStamp+"_"+docModele.getId(); 
			DocumentDTO documentDTO = new DocumentDTO(documentModele, nomFichierFinal, listeChampDTO);

			
			
			String repertoireSpecifique = cheminFichierTemporaire + timeStamp + "_" + docModele.getId();
			repertoire = new File(repertoireSpecifique);
			repertoire.mkdir();
			String chemineFichierTravail = repertoireSpecifique + File.separator;
			copie = new File(chemineFichierTravail + docModele.getNomFichierModele());
			FileUtils.writeByteArrayToFile(copie, fichierTemplate);

			Map<String, Object> freeMarkerMap = new HashMap<String, Object>();
			freeMarkerMap.put("racine", domaine);
			freeMarkerMap.put("objectConstructor", new ObjectConstructor());

			String cheminFichierResultat = null;

			FichierDTO fichierDTO = new FichierDTO(copie);

			if (TypeFormat.EXCEL.equals(formatExport)) {
				
				documentModele.setXml(docModele.getContenu());
				XLSGenerationService xlsGenerationService = new XLSGenerationService();
				is = xlsGenerationService.genererDocument(documentDTO, fichierDTO, freeMarkerMap);
                cheminFichierResultat = cheminFichierTemporaire + "xls_" + timeStamp + "_" + docModele.getId();
                resultatFreeMarker = new File(cheminFichierResultat);
				fos = new FileOutputStream(resultatFreeMarker);
				IOUtils.copy(is, fos);
				xlsGenerationService.supprimerFichierTemporaire();
			} else {
				urlOO = "localhost";
				OpenOfficeService openOfficeService = new OpenOfficeService(urlOO, portOO, repertoireSpecifique);
				openOfficeService.setFreeMarkerMapPourGeneration(freeMarkerMap);
				is = openOfficeService.genererDocumentDepuisTemplateFreemarker(documentDTO, fichierDTO);
				
				resultatFreeMarker = new File(cheminFichierTemporaire + "fm_" + timeStamp + "_" + docModele.getId()+".odt");
				fos = new FileOutputStream(resultatFreeMarker);
				IOUtils.copy(is, fos);
				
				openOfficeService.genererDocumentDansRepertoireSpecifique(documentDTO, new FichierDTO(resultatFreeMarker), cheminFichierTemporaire);
				cheminFichierResultat = openOfficeService.getFichierTemporaire().getPath();
			}

			suivi.setNomFichierGenere(cheminFichierResultat);
			suivi.setNomFichierRetour(docModele.getNom());
			suivi.setStatutGeneration("OK");

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			suivi.setNomFichierGenere(null);
			suivi.setStatutGeneration("KO");
			if (repertoire != null) {
				try {
					FileUtils.deleteDirectory(repertoire);
				} catch (IOException e1) {
					LOG.error("impossible de supprimer le repertoire : " + repertoire.getName());
					LOG.error(e1.getMessage(), e1.fillInStackTrace());
				}
			}
		} finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(fos);
			try {
				if (repertoire != null && repertoire.exists())
					FileUtils.deleteDirectory(repertoire);
			} catch (IOException ex) {
				LOG.error("erreur de deleteDirectory");
			}
			try {
                if (!TypeFormat.EXCEL.equals(formatExport) && resultatFreeMarker != null && resultatFreeMarker.exists())
                    resultatFreeMarker.delete();
            } catch (Exception ex) {
                LOG.error("erreur de resultatFreeMarker.delete()");
            }
		}

        LOG.info("Fin de la tache de la génération du doc.");
	}

	/**
	 * Modéle de document à générer.
	 */
	public final void setDocModele(final EpmTDocumentModele valeur) {
		this.docModele = valeur;
	}

	/**
	 * Données.
	 */
	public final void setDomaine(final DomaineRacine valeur) {
		this.domaine = valeur;
	}

	/**
	 * Fichier temporaire pour la génération.
	 */
	public final void setFichierTemporaire(final String valeur) {
		this.cheminFichierTemporaire = valeur;
	}

	/**
	 * Url du serveur openoffice (adresse ip)
	 */
	public final void setUrlOO(final String valeur) {
		this.urlOO = valeur;
	}

	/**
	 * Port du server openoffice.
	 */
	public final void setPortOO(final String valeur) {
		this.portOO = valeur;
	}

	/**
	 * Objet de suivi de la génération du document.
	 */
	public final void setSuivi(final SuiviGenerationDocumentModele valeur) {
		this.suivi = valeur;
	}

}