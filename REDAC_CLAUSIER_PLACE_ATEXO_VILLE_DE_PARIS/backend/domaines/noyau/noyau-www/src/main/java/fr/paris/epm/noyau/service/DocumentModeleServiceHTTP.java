package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.metier.DocumentModeleGIM;
import fr.paris.epm.noyau.persistance.EpmTConsultation;

import java.util.Map;

/**
 * Service d'accès aux document modele en utilisant Spring remoting.
 * @author MGA
 */
public class DocumentModeleServiceHTTP extends GeneriqueServiceHTTP implements DocumentModeleServiceSecurise {

    private static DocumentModeleGIM documentModeleService;


    @Override
    public String genererFreeMarker(final Map<String, Object> freeMarkerMap, final String xml, final EpmTConsultation idConsultation, final int idLot) {
        return documentModeleService.genererFreeMarker(freeMarkerMap, xml, idConsultation, idLot);
    }


    /**
     * @param valeur the documentModeleService to set
     */
    public final void setDocumentModeleService(final DocumentModeleGIM valeur) {
        documentModeleService = valeur;
    }

}
