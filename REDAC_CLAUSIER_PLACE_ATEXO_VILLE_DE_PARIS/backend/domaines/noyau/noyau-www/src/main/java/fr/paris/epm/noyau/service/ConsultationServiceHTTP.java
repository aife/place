package fr.paris.epm.noyau.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.ConsultationContexteCritere;
import fr.paris.epm.noyau.metier.Critere;
import fr.paris.epm.noyau.metier.objetvaleur.EtapeSimple;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTEtapeCal;
import fr.paris.epm.noyau.persistance.EpmTObject;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service d'accès aux consultation en utilisant Spring remoting.
 * @author MGA
 */
public class ConsultationServiceHTTP extends GeneriqueServiceHTTP implements ConsultationServiceSecurise {

    private static final Logger LOG = LoggerFactory.getLogger(ConsultationServiceHTTP.class);
    @Override
    public EtapeSimple chargerEtapeDateCal(EpmTEtapeCal etapeCal) {
        if (etapeCal == null) {
            return null;
        }
        EtapeSimple etapeSimple = new EtapeSimple();
        etapeSimple.setLibelle(etapeCal.getLibelle());
        etapeSimple.setType(etapeCal.getTypeEtape());
        if (etapeCal.getDateReelle() != null) {
            etapeSimple.setDate(etapeCal.getDateReelle());
        } else if (etapeCal.getDateHerite() != null) {
            etapeSimple.setDate(etapeCal.getDateHerite());
        } else {
            etapeSimple.setDate(etapeCal.getDatePrevue());
        }
        return etapeSimple;
    }


    @Override
    public <T> List<T> chercherParCritere(Critere critere) throws TechnicalNoyauException {
        return generiqueDAO.findByCritere(critere);
    }

    @Override
    public <T extends EpmTObject> Collection<T> modifier(final Collection<T> objets) {
        return generiqueDAO.merge(objets);
    }

    @Override
    public <T extends EpmTObject> T modifier(T epmTObject) {
        return generiqueDAO.merge(epmTObject);
    }


    @Override
    public Collection<EpmTConsultation> chercherConsultations(ConsultationContexteCritere critere) {
        List<EpmTConsultationContexte> list = generiqueDAO.findByCritere(critere);
        return list.stream().map(this::getConsultation).collect(Collectors.toList());
    }

    @Override
    public EpmTConsultation chercherConsultation(ConsultationContexteCritere critereConstultation) {
        EpmTConsultationContexte epmTConsultationContexte = generiqueDAO.findUniqueByCritere(critereConstultation);
        return getConsultation(epmTConsultationContexte);

    }

    private EpmTConsultation getConsultation(EpmTConsultationContexte epmTConsultationContexte) {
        String contexte = epmTConsultationContexte.getConsultationJson();
        try {
            return getMapper().readValue(contexte, EpmTConsultation.class);
        } catch (IOException e) {
            throw new TechnicalNoyauException("Erreur lors de la lecture du contexte de la consultation", e);
        }
    }

    public static ObjectMapper getMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return objectMapper;
    }


}
