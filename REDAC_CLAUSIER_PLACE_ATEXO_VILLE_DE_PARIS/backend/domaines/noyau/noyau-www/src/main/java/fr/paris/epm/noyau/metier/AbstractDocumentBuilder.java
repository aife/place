package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineAnalyseDepouillementPlis;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineAttribution;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineConsultation;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.DomaineRacine;
import fr.paris.epm.noyau.metier.objetvaleur.documentModele.domaine.sousDomaine.*;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.referentiel.*;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.*;

/**
 * Methodes utilitaires a la classe DocumentBuilder (genereration des domaines de donnees).
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public abstract class AbstractDocumentBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDocumentBuilder.class);

    /**
     * Acces aux objets persistants (injection Spring).
     */
    private GeneriqueDAO generiqueDAO;

    /**
     * Mapping dozer
     */
    private DozerBeanMapper conversionService;

    private Referentiels referentiels;

    /**
     * Identifiant en base d'un lot. Si != null, le document concerne un lot.
     */
    private Integer idLot;

    private EpmTConsultation consultation;

    private EpmTBudLot lot;

    private EpmTAvenant avenant;

    private Map<Integer, DomaineTranche> epmIdVersDomaineTrancheMap = new HashMap<Integer, DomaineTranche>();;
    /**
     * Associe un lot par son id a son domaine de donnees. Permet de reutiliser le domaine de donnees (DomaineLot) lors de la consutruction de deux domaines/sous-domaines differents.
     */
    private Map<String, DomaineLot> epmIdVersDomaineLotMap = new HashMap<String, DomaineLot>();
    private Map<Integer, EpmTRefChoixFormePrix> mapEpmTRefChoixFormePrixParId = new HashMap<Integer, EpmTRefChoixFormePrix>();


    void logErreurEtLancerException(Logger log, String msg, final Exception exception) {
        if (exception != null)
            log.error(exception.getMessage(), exception);
        ApplicationNoyauException e = new ApplicationNoyauException(msg);
        LOG.error(e.getMessage(), e);
        throw e;
    }

    /**
     * Alloue le domaine consultation si null.
     */
    void creerDomaineConsultationSiNull(DomaineRacine domaineRacine) {
        if (domaineRacine.getConsultation() == null)
            domaineRacine.setConsultation(new DomaineConsultation());
    }

    /**
     * Retourne le domaine de données concernant le lot.
     */
    DomaineLot genererDomaineLotUnique(EpmTBudLot lotParam, EpmTConsultation consultation) {

        if (lotParam == null && idLot == null)
            logErreurEtLancerException(LOG, "Impossible de charger un lot : identifiant ou lot absent.", null);

        DomaineLot res = getDomaineLot(lotParam.getNumeroLot());
        conversionService.map(lotParam, res);

        //set CCAG reference value
        if(lotParam.getEpmTBudLotOuConsultation()!=null && lotParam.getEpmTBudLotOuConsultation().getEpmTRefCcag()!=null){
            res.setCcagDeReference(lotParam.getEpmTBudLotOuConsultation().getEpmTRefCcag().getLibelle());
        }

        // Autres infos sur les lots
        // Lots techniques
        List<DomaineLotTechique> domaineLotTechniqueList = recupererDomaineLotsTechniques(lotParam.getEpmTBudLotOuConsultation().getEpmTLotTechniques());
        Collections.sort(domaineLotTechniqueList);
        res.setListeLotTechnique(domaineLotTechniqueList);

        genererDomaineDureeMarche(res, lotParam);

        // clauses sociale
        DomaineClauseSociale clauseSociale = new DomaineClauseSociale();

        if (lotParam.getEpmTBudLotOuConsultation().getClausesSociales()) {
            clauseSociale.setClauseSociale(true);
            clauseSociale = genererDomaineClauseSocialeGlobal(lotParam.getEpmTBudLotOuConsultation(), clauseSociale);
        }
        res.setClauseSociale(clauseSociale);

        // clauses environnementale
        DomaineClauseEnvironnementale clauseEnvironnementale = new DomaineClauseEnvironnementale();
        if (lotParam.getEpmTBudLotOuConsultation().getClausesEnvironnementales()) {
            clauseEnvironnementale.setClauseEnvironnementale(true);
            clauseEnvironnementale = genererDomaineClauseEnvironnementaleGlobal(lotParam.getEpmTBudLotOuConsultation(), clauseEnvironnementale);
        }
        res.setClauseEnvironnementale(clauseEnvironnementale);

        // Variantes et Reconductions
        DomaineVariantes domaineVariantes = new DomaineVariantes();

        domaineVariantes.setAutorisee(false);
        if (lotParam.getEpmTBudLotOuConsultation().getVariantesAutorisees() != null && lotParam.getEpmTBudLotOuConsultation().getVariantesAutorisees().equals(Constantes.OUI))
            domaineVariantes.setAutorisee(true);

        if (lotParam.getEpmTBudLotOuConsultation().getDescriptionOptionsImposees() != null)
            domaineVariantes.setDescription(lotParam.getEpmTBudLotOuConsultation().getDescriptionOptionsImposees());

        domaineVariantes.setObligatoire(false);
        if (lotParam.getEpmTBudLotOuConsultation().getVariantesExigees() != null && lotParam.getEpmTBudLotOuConsultation().getVariantesExigees().equals(Constantes.OUI))
            domaineVariantes.setObligatoire(true);
        res.setVariantes(domaineVariantes);

        DomaineReconduction domaineReconduction = new DomaineReconduction();
        if (lotParam.getEpmTBudLotOuConsultation().getModalitesReconduction() != null)
            domaineReconduction.setModalite(lotParam.getEpmTBudLotOuConsultation().getModalitesReconduction());

        if (lotParam.getEpmTBudLotOuConsultation().getNbReconductions() != null)
            domaineReconduction.setNombre(lotParam.getEpmTBudLotOuConsultation().getNbReconductions());
        domaineReconduction.setReconduction(domaineReconduction.getModalite() != null && domaineReconduction.getNombre() != null);
        res.setReconduction(domaineReconduction);

        // Récuperer les informations du domaine de critères d'attribution
        List<DomaineCritereConsultation> domaineCritereAttribution = null;
        if (consultation.isCritereAppliqueTousLots()){
            domaineCritereAttribution = recupererDomaineCritereAttribution(getConsultation().getListeCritereAttributionTriee());
        } else {
            domaineCritereAttribution = recupererDomaineCritereAttribution(lotParam.getListeCritereAttributionTriee());
        }
        res.setListeCriteres(domaineCritereAttribution);
        if (consultation.isCritereAppliqueTousLots()) {
            res.setTypeCriteresAttribution(consultation.getCritereAttribution().getCodeExterne());
            res.setLibelleCritereAttribution(consultation.getCritereAttribution().getLibelle());
        } else {
            if (lotParam.getCritereAttribution() != null) {
                res.setTypeCriteresAttribution(lotParam.getCritereAttribution().getCodeExterne());
                res.setLibelleCritereAttribution(lotParam.getCritereAttribution().getLibelle());
            }
        }
        // Informations sur les tranches
        if (mapEpmTRefChoixFormePrixParId.isEmpty())
            initialiserMapEpmTRefChoixFormePrixParId();

        List<DomaineTranche> listeTranche = new ArrayList<DomaineTranche>();

        if (!lotParam.getEpmTBudLotOuConsultation().getEpmTBudTranches().isEmpty()) {
            for (EpmTBudTranche trancheItem : lotParam.getEpmTBudLotOuConsultation().getEpmTBudTranches()) {
                DomaineTranche tranche = new DomaineTranche();
                String codeTranche = trancheItem.getEpmTRefNatureTranche().getLibelle();
                if (trancheItem.getCodeTranche() != null && !trancheItem.getCodeTranche().isEmpty())
                    codeTranche += "-" + trancheItem.getCodeTranche();

                tranche.setCodeTranche(codeTranche);
                tranche.setIntituleTranche(trancheItem.getIntituleTranche());
                tranche.setNatureTranche(trancheItem.getEpmTRefNatureTranche().getLibelle());
                EpmTBudFormePrix formePrix = trancheItem.getEpmTBudFormePrix();
                DomaineFormeDePrix domainePrix = new DomaineFormeDePrix();

                EpmTRefChoixFormePrix epmTRefChoixFormePrix = null;
                if (formePrix instanceof EpmTBudFormePrixPm) {
                    EpmTBudFormePrixPm formePrixPm = (EpmTBudFormePrixPm) formePrix;
                    epmTRefChoixFormePrix = mapEpmTRefChoixFormePrixParId.get(3);

                    domainePrix.setEstimationUnitaireHT(formePrixPm.getPuEstimationHt());
                    domainePrix.setEstimationUnitaireTTC(formePrixPm.getPuEstimationTtc());

                    domainePrix.setEstimationForfaitaireHT(formePrixPm.getPfEstimationHt());
                    domainePrix.setEstimationForfaitaireTTC(formePrixPm.getPfEstimationTtc());

                    domainePrix.setUnitaireBDCMin(formePrixPm.getPuMinHt());
                    domainePrix.setUnitaireBDCMax(formePrixPm.getPuMaxHt());

                    // Partie unitaire
                    if (formePrixPm.getPuEpmTRefBonQuantite() != null) {
                        domainePrix.setPrixUnitaireBdCAutres(formePrixPm.getPuEpmTRefBonQuantite().getLibelle());
                        domainePrix.setIdBonQuantite(formePrixPm.getPuEpmTRefBonQuantite().getId());
                    }
                    if (formePrixPm.getPuEpmTRefMinMax() != null && formePrixPm.getPuMinHt() != null && formePrixPm.getPuMaxHt() != null)
                        domainePrix.setUnitaireBDCMinMaxAvecSans("Min : " + formePrixPm.getPuMinHt() + " / Max : " + formePrixPm.getPuMaxHt() + " EUR HT");

                    if (formePrixPm.getPuDateValeur() != null)
                        domainePrix.setDateValeurUnitaire(formePrixPm.getPuDateValeur().getTime());
                    domainePrix.setTypePrixUnitaire(typePrix(formePrixPm.getPuEpmTRefTypePrix()));

                    Set<EpmTRefVariation> pmEpmTRefVariations = new HashSet<EpmTRefVariation>(formePrixPm.getPuEpmTRefVariations());
                    pmEpmTRefVariations.addAll(formePrixPm.getPuEpmTRefVariations());

                    for (EpmTRefVariation variation : formePrixPm.getPfEpmTRefVariations())
                        if (!pmEpmTRefVariations.contains(variation))
                            pmEpmTRefVariations.add(variation);

                    domainePrix.setVariationPrix(variationPrix(pmEpmTRefVariations));
                    domainePrix.setVariationPrixPMPU(variationPrix(formePrixPm.getPuEpmTRefVariations()));
                    domainePrix.setVariationPrixPMPF(variationPrix(formePrixPm.getPfEpmTRefVariations()));
                } else if (formePrix instanceof EpmTBudFormePrixPf) {
                    epmTRefChoixFormePrix = mapEpmTRefChoixFormePrixParId.get(2);
                    EpmTBudFormePrixPf formePrixPf = (EpmTBudFormePrixPf) formePrix;
                    domainePrix.setEstimationForfaitaireHT(formePrixPf.getPfEstimationHt());
                    domainePrix.setEstimationForfaitaireTTC(formePrixPf.getPfEstimationTtc());

                    Set<EpmTRefVariation> pmEpmTRefVariations = new HashSet<EpmTRefVariation>(formePrixPf.getPfEpmTRefVariations());
                    pmEpmTRefVariations.addAll(formePrixPf.getPfEpmTRefVariations());
                    domainePrix.setVariationPrix(variationPrix(pmEpmTRefVariations));
                } else if (formePrix instanceof EpmTBudFormePrixPu) {
                    EpmTBudFormePrixPu formePrixPu = (EpmTBudFormePrixPu) formePrix;
                    epmTRefChoixFormePrix = mapEpmTRefChoixFormePrixParId.get(1);
                    domainePrix.setEstimationUnitaireHT(formePrixPu.getPuEstimationHt());
                    domainePrix.setEstimationUnitaireTTC(formePrixPu.getPuEstimationTtc());
                    domainePrix.setUnitaireBDCMin(formePrixPu.getPuMinHt());
                    domainePrix.setUnitaireBDCMax(formePrixPu.getPuMaxHt());

                    if (formePrixPu.getEpmTRefBonQuantite() != null) {
                        domainePrix.setPrixUnitaireBdCAutres(formePrixPu.getEpmTRefBonQuantite().getLibelle());
                        domainePrix.setIdBonQuantite(formePrixPu.getEpmTRefBonQuantite().getId());
                    }
                    if (formePrixPu.getEpmTRefMinMax() != null)
                        domainePrix.setUnitaireBDCMinMaxAvecSans(formePrixPu.getEpmTRefMinMax().getLibelle());
                    if (formePrixPu.getEpmTRefTypePrix() != null)
                        domainePrix.setTypePrixUnitaire(typePrix(formePrixPu.getEpmTRefTypePrix()));

                    Set<EpmTRefVariation> pmEpmTRefVariations = new HashSet<EpmTRefVariation>(formePrixPu.getPuEpmTRefVariations());
                    pmEpmTRefVariations.addAll(formePrixPu.getPuEpmTRefVariations());
                    domainePrix.setVariationPrix(variationPrix(pmEpmTRefVariations));
                }

                domainePrix.setFormePrix(epmTRefChoixFormePrix.getLibelle());
                domainePrix.setIdFormePrix(epmTRefChoixFormePrix.getId());
                tranche.setFormeDePrix(domainePrix);
                listeTranche.add(tranche);
            }
        } else {
            EpmTBudFormePrix formeDePrix = lotParam.getEpmTBudLotOuConsultation().getEpmTBudFormePrix();
            DomaineFormeDePrix domaineFormeDePrix = genererDomaineFormeDePrix(formeDePrix);
            res.setFormeDePrix(domaineFormeDePrix);
        }
        // Remplissage des pairesTypes
        DomainePairesTypes pairesTypes = new DomainePairesTypes();
        for (EpmTValeurConditionnementExterne valeurConditionnementExterne : lotParam.getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternes()) {
            DomainePaireType paireType = new DomainePaireType();
            paireType.setClef(valeurConditionnementExterne.getClef());
            paireType.setValeur(valeurConditionnementExterne.getValeur());
            pairesTypes.getPairesTypes().put(valeurConditionnementExterne.getClef(), paireType);
        }

        List<EpmTValeurConditionnementExterne> listExterne = consultation.getEpmTValeurConditionnementExterneList();

        for (EpmTValeurConditionnementExterne epmTValeurConditionnementExterne : listExterne) {
            DomainePaireType paireType = new DomainePaireType();
            paireType.setClef(epmTValeurConditionnementExterne.getClef());
            paireType.setValeur(epmTValeurConditionnementExterne.getValeur());
            pairesTypes.getPairesTypes().put(epmTValeurConditionnementExterne.getClef(), paireType);
        }

        if (idLot != null)
            pairesTypes.setIdLot(idLot);

        res.setPairesTypes(pairesTypes);
        // Remplissage des pairesTypesComplexes
        DomainePairesTypesComplexes pairesTypesComplexes = new DomainePairesTypesComplexes();
        for (EpmTValeurConditionnementExterneComplexe valeurConditionnementExterneComplexe : lotParam.getEpmTBudLotOuConsultation().getEpmTValeurConditionnementExternesComplexe()) {
            DomainePairesTypes listPairesTypes = new DomainePairesTypes();
            listPairesTypes.setClef(valeurConditionnementExterneComplexe.getIdObjetComplexe());
            for (EpmTValeurConditionnementExterne valeurConditionnementExterne : valeurConditionnementExterneComplexe.getEpmTValeurConditionnementExternes()) {
                DomainePaireType paireType = new DomainePaireType();
                paireType.setClef(valeurConditionnementExterne.getClef());
                paireType.setValeur(valeurConditionnementExterne.getValeur());
                listPairesTypes.getPairesTypes().put(valeurConditionnementExterne.getClef(), paireType);
            }
            if (idLot != null)
                listPairesTypes.setIdLot(idLot);
            pairesTypesComplexes.getPairesTypesComplexes().add(listPairesTypes);
        }

        List<EpmTValeurConditionnementExterneComplexe> listeExterneComplexe = consultation.getEpmTValeurConditionnementExterneComplexeList();

        for (EpmTValeurConditionnementExterneComplexe epmTValeurConditionnementExterneComplexe : listeExterneComplexe) {
            DomainePairesTypes listPairesTypes = new DomainePairesTypes();
            listPairesTypes.setClef(epmTValeurConditionnementExterneComplexe.getIdObjetComplexe());
            for (EpmTValeurConditionnementExterne valeurConditionnementExterne : epmTValeurConditionnementExterneComplexe.getEpmTValeurConditionnementExternes()) {
                DomainePaireType paireType = new DomainePaireType();
                paireType.setClef(valeurConditionnementExterne.getClef());
                paireType.setValeur(valeurConditionnementExterne.getValeur());
                listPairesTypes.getPairesTypes().put(valeurConditionnementExterne.getClef(), paireType);
            }
            if (idLot != null)
                listPairesTypes.setIdLot(idLot);
            pairesTypesComplexes.getPairesTypesComplexes().add(listPairesTypes);
        }
        res.setPairesTypesComplexes(pairesTypesComplexes);
        res.setListeTranches(listeTranche);
        res.setIdentifiant(lotParam.getNumeroLot());
        return res;
    }


    List<DomaineCritereConsultation> recupererDomaineCritereAttribution(final List<EpmTCritereAttributionConsultation> critereAttribConsultList) {
        List<DomaineCritereConsultation> listCritere = new ArrayList<DomaineCritereConsultation>();
        if (critereAttribConsultList != null && !critereAttribConsultList.isEmpty()) {
            for (EpmTCritereAttributionConsultation critereAttribConsultItem : critereAttribConsultList) {
                DomaineCritereConsultation critereAttrib = new DomaineCritereConsultation();
                critereAttrib.setNomCritere(critereAttribConsultItem.getEnonce());
                critereAttrib.setPonderationCritere(critereAttribConsultItem.getPonderation());

                List<DomaineSousCritereConsultation> listeSousCriteres = new ArrayList<DomaineSousCritereConsultation>();
                for (EpmTSousCritereAttributionConsultation sousCritere : critereAttribConsultItem.getListeSousCritereAttributionTriee()) {
                    DomaineSousCritereConsultation domaineSousCritere = new DomaineSousCritereConsultation();
                    domaineSousCritere.setNomSousCritere(sousCritere.getEnonce());
                    domaineSousCritere.setPonderationSousCritere(sousCritere.getPonderation());
                    listeSousCriteres.add(domaineSousCritere);
                }
                critereAttrib.setListeSousCriteres(listeSousCriteres);
                listCritere.add(critereAttrib);
            }
        }
        return listCritere;
    }

    List<DomaineLotTechique> recupererDomaineLotsTechniques(Set<EpmTLotTechnique> lotsTechniquesSet) {

        List<DomaineLotTechique> domaineLotTechiques = new ArrayList<DomaineLotTechique>();
        for (EpmTLotTechnique lotTechnique : lotsTechniquesSet) {
            DomaineLotTechique domaineLotTechique = new DomaineLotTechique();
            if (lotTechnique.getNumeroLot() != null)
                domaineLotTechique.setCode(lotTechnique.getNumeroLot());
            if (lotTechnique.getIntituleLot() != null)
                domaineLotTechique.setIntitule(lotTechnique.getIntituleLot());
            if (lotTechnique.getPrincipal() != null)
                domaineLotTechique.setPrincipal(Boolean.parseBoolean(lotTechnique.getPrincipal().replace("oui", "true")));

            List<DomaineTranche> domaineTrancheList = new ArrayList<DomaineTranche>();
            for (EpmTBudTranche tranche : (Set<EpmTBudTranche>) lotTechnique.getEpmTBudTranches()) {
                DomaineTranche domaineTranche = new DomaineTranche();
                domaineTranche.setCodeTranche(tranche.getEpmTRefNatureTranche().getLibelle());
                if (tranche.getCodeTranche() != null && !tranche.getCodeTranche().isEmpty())
                    domaineTranche.setCodeTranche(tranche.getEpmTRefNatureTranche().getLibelle() + "-" + tranche.getCodeTranche());
                if (tranche.getIntituleTranche() != null)
                    domaineTranche.setIntituleTranche(tranche.getIntituleTranche());
                if (tranche.getEpmTRefNatureTranche() != null && tranche.getEpmTRefNatureTranche().getLibelle() != null)
                    domaineTranche.setNatureTranche(tranche.getEpmTRefNatureTranche().getLibelle());
                domaineTrancheList.add(domaineTranche);
            }
            domaineLotTechique.setTranches(domaineTrancheList);
            domaineLotTechiques.add(domaineLotTechique);
        }

        domaineLotTechiques.sort(Comparator.comparing(o -> StringUtils.trimToNull(o.getCode())));
        return domaineLotTechiques;
    }

    /**
     * Retourne la liste des domaines consultation.lot a partir de l'identifiant d'une consultation.
     */
    List<DomaineLot> genererDomaineLot() {


        List<EpmTBudLot> lots = consultation.trieSetLots();

        if (lots == null || lots.isEmpty())
            return null;

        List<DomaineLot> domaineLots = new ArrayList<>();
        for (EpmTBudLot lot : lots) {
            if (!lot.getEpmTBudLotOuConsultation().isLotDissocie())
                domaineLots.add(genererDomaineLotUnique(lot, consultation));
        }
        return domaineLots;
    }


    private void initialiserMapEpmTRefChoixFormePrixParId() {
        Collection<EpmTRefChoixFormePrix> collectionChois = referentiels.getChoixFormePrix();
        for (EpmTRefChoixFormePrix epmTRefChoixFormePrix : collectionChois)
            mapEpmTRefChoixFormePrixParId.put(epmTRefChoixFormePrix.getId(), epmTRefChoixFormePrix);
    }

    /**
     * Retourne la chaine des formes de prix separees par des virgules.
     */
    private String variationPrix(Set<EpmTRefVariation> variationSet) {

        if (variationSet == null)
            return null;

        StringBuilder res = new StringBuilder();
        int i = 1;
        for (EpmTRefVariation variationItem : variationSet) {
            String v = variationItem.getLibelle().substring(5);
            v = v.replaceFirst(v.substring(0, 1), v.substring(0, 1).toUpperCase());
            res.append(v);

            if (variationSet.size() != i)
                res.append(", ");
            i++;
        }
        return res.toString();
    }

    /**
     * Retourne la chaine des types de prix separes par des virgules.
     */
    private String typePrix(Set<EpmTRefTypePrix> typePrixSet) {

        if (typePrixSet == null)
            return null;

        StringBuilder res = new StringBuilder();
        for (EpmTRefTypePrix typePrix : typePrixSet)
            res.append(typePrix.getLibelle()).append(", ");

        if (res.length() != 0)
            res.setLength(res.length() - 2);
        return res.toString();
    }

    /**
     * Retourne le domaine de donnees correspondant a l'identifiant de tranche s'il a deja ete cree. En retourne un nouveau sinon (qui pourra etre reutiliser en reutilisant cette fonction).
     */
    DomaineTranche getDomaineTranche(int idTranche) {

        DomaineTranche domTranche = epmIdVersDomaineTrancheMap.get(idTranche);
        if (domTranche == null) {
            domTranche = new DomaineTranche();
            epmIdVersDomaineTrancheMap.put(idTranche, domTranche);
        }
        return domTranche;
    }

    /**
     * Retourne un domaine de donnees des formes de prix correspondant a l'objet persistant formeDePrix.
     */
    DomaineFormeDePrix genererDomaineFormeDePrix(EpmTBudFormePrix formeDePrix) {

        DomaineFormeDePrix res = new DomaineFormeDePrix();

        if (formeDePrix != null) {
            if (formeDePrix instanceof EpmTBudFormePrixPm) {
                EpmTBudFormePrixPm formePrixPm = (EpmTBudFormePrixPm) formeDePrix;
                res.setTxtFormePrixNonAllotieNonFractionnee("Prix Mixte");
                res.setFormePrix("Prix Mixte");
                res.setIdFormePrix(3);
                // Partie unitaire
                if (formePrixPm.getPuEpmTRefBonQuantite() != null) {
                    res.setPrixUnitaireBdCAutres(formePrixPm.getPuEpmTRefBonQuantite().getLibelle());
                    res.setIdBonQuantite(formePrixPm.getPuEpmTRefBonQuantite().getId());
                }
                if (formePrixPm.getPuEpmTRefMinMax() != null) {
                    res.setUnitaireBDCMinMaxAvecSans(formePrixPm.getPuEpmTRefMinMax().getLibelle());
                }
                res.setUnitaireBDCMin(formePrixPm.getPuMinHt());
                res.setUnitaireBDCMax(formePrixPm.getPuMaxHt());
                res.setEstimationUnitaireHT(formePrixPm.getPuEstimationHt());
                res.setEstimationUnitaireTTC(formePrixPm.getPuEstimationTtc());
                if (formePrixPm.getPuDateValeur() != null) {
                    res.setDateValeurUnitaire(formePrixPm.getPuDateValeur().getTime());
                }
                res.setTypePrixUnitaire(typePrix(formePrixPm.getPuEpmTRefTypePrix()));
                Set<EpmTRefVariation> pmEpmTRefVariations = new HashSet<EpmTRefVariation>(formePrixPm.getPuEpmTRefVariations());
                pmEpmTRefVariations.addAll(formePrixPm.getPfEpmTRefVariations());
                res.setVariationPrix(variationPrix(pmEpmTRefVariations));
                res.setVariationPrixPMPU(variationPrix(formePrixPm.getPuEpmTRefVariations()));
                res.setVariationPrixPMPF(variationPrix(formePrixPm.getPfEpmTRefVariations()));
                // Partie forfaitaire
                initialiserDomaineFormeDePrixForfaitaire(res, formePrixPm);
            } else if (formeDePrix instanceof EpmTBudFormePrixPu) {
                EpmTBudFormePrixPu formePrixPu = (EpmTBudFormePrixPu) formeDePrix;
                res.setTxtFormePrixNonAllotieNonFractionnee("Prix Unitaire");
                res.setFormePrix("Prix Unitaire");
                res.setIdFormePrix(1);
                initialiserDomaineFormeDePrixUnitaire(res, formePrixPu);
                res.setVariationPrix(variationPrix(formePrixPu.getPuEpmTRefVariations()));
            } else if (formeDePrix instanceof EpmTBudFormePrixPf) {
                EpmTBudFormePrixPf formePrixPf = (EpmTBudFormePrixPf) formeDePrix;
                res.setTxtFormePrixNonAllotieNonFractionnee("Prix Forfaitaire");
                res.setIdFormePrix(2);
                res.setFormePrix("Prix Forfaitaire");
                initialiserDomaineFormeDePrixForfaitaire(res, formePrixPf);
                res.setVariationPrix(variationPrix(formePrixPf.getPfEpmTRefVariations()));
            }
        }

        return res;
    }

    /**
     * Initialise le domaine forme de prix fournit en parametre avec les informations de forme de prix unitaire.
     */
    private void initialiserDomaineFormeDePrixUnitaire(DomaineFormeDePrix domFormePrix, final EpmTBudFormePrixPu formePrixPuBdd) {

        if (formePrixPuBdd.getEpmTRefBonQuantite() != null) {
            domFormePrix.setPrixUnitaireBdCAutres(formePrixPuBdd.getEpmTRefBonQuantite().getLibelle());
            domFormePrix.setIdBonQuantite(formePrixPuBdd.getEpmTRefBonQuantite().getId());
        }
        if (formePrixPuBdd.getEpmTRefMinMax() != null)
            domFormePrix.setUnitaireBDCMinMaxAvecSans(formePrixPuBdd.getEpmTRefMinMax().getLibelle());
        domFormePrix.setUnitaireBDCMin(formePrixPuBdd.getPuMinHt());
        domFormePrix.setUnitaireBDCMax(formePrixPuBdd.getPuMaxHt());
        domFormePrix.setEstimationUnitaireHT(formePrixPuBdd.getPuEstimationHt());
        domFormePrix.setEstimationUnitaireTTC(formePrixPuBdd.getPuEstimationTtc());
        if (formePrixPuBdd.getPuDateValeur() != null)
            domFormePrix.setDateValeurUnitaire(formePrixPuBdd.getPuDateValeur().getTime());
        domFormePrix.setTypePrixUnitaire(typePrix(formePrixPuBdd.getEpmTRefTypePrix()));
    }

    /**
     * Initialise le domaine forme de prix fournit en parametre avec les informations de forme de prix forfaitaire.
     */
    private void initialiserDomaineFormeDePrixForfaitaire(DomaineFormeDePrix domFormePrix, final EpmTBudFormePrixPf formePrixPfBdd) {

        domFormePrix.setEstimationForfaitaireHT(formePrixPfBdd.getPfEstimationHt());
        domFormePrix.setEstimationForfaitaireTTC(formePrixPfBdd.getPfEstimationTtc());
        if (formePrixPfBdd.getPfDateValeur() != null)
            domFormePrix.setDateValeurForfaitaire(formePrixPfBdd.getPfDateValeur().getTime());
    }

    DomaineLot getDomaineLot(String idLot) {
        if (!epmIdVersDomaineLotMap.containsKey(idLot))
            epmIdVersDomaineLotMap.put(idLot, new DomaineLot());
        return epmIdVersDomaineLotMap.get(idLot);
    }


    DomaineClauseSociale genererDomaineClauseSocialeGlobal(EpmTBudLotOuConsultation budLotOuConsultation, DomaineClauseSociale clauseSociale) {
        for (EpmTRefClausesSociales refClausesSociales : budLotOuConsultation.getClausesSocialesChoixUtilisateur()) {
            DomaineStructureSociale domaineStructureSociale = genererDomaineStructureSociale(budLotOuConsultation.getListeStructureSocialeReserves(), refClausesSociales);
            switch (refClausesSociales.getCodeExterne()) {
                case EpmTRefClausesSociales.CODE_CLAUSE_RESERVE:
                    clauseSociale.setReserver(domaineStructureSociale);
                    break;
                case EpmTRefClausesSociales.CODE_CLAUSE_EXECUTION:
                    clauseSociale.setExecution(domaineStructureSociale);
                    break;
                case EpmTRefClausesSociales.CODE_CLAUSE_ATTRIBUTION:
                    clauseSociale.setAttribution(domaineStructureSociale);
                    break;
                case EpmTRefClausesSociales.CODE_CLAUSE_SPECIFICATION:
                    clauseSociale.setSpecification(domaineStructureSociale);
                    break;
                case EpmTRefClausesSociales.CODE_CLAUSE_INSERTION:
                    clauseSociale.setInsertion(domaineStructureSociale);
                    break;
            }
        }
        return clauseSociale;
    }

    DomaineStructureSociale genererDomaineStructureSociale(Set<EpmTRefTypeStructureSociale> listRefTypeStructureSociale, EpmTRefClausesSociales refClausesSociales) {
        DomaineStructureSociale domaineStructureSociale = new DomaineStructureSociale();
        for (EpmTRefTypeStructureSociale refTypeStructureSociale : listRefTypeStructureSociale) {
            switch (refTypeStructureSociale.getCodeExterne()) {
                case EpmTRefTypeStructureSociale.CODE_EESS:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setEess(true);
                        domaineStructureSociale.setLibelleEess(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_SIAE:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setSiae(true);
                        domaineStructureSociale.setLibelleSiae(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_ESAT_EA:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setEsat(true);
                        domaineStructureSociale.setLibelleEsat(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_FORMATION_SCOLAIRE:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setFormation(true);
                        domaineStructureSociale.setLibelleFormation(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_ACHATS_ETHIQUES:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setAchat(true);
                        domaineStructureSociale.setLibelleAchat(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_ACITIVITE_ECONOMIQUE:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setActiviteEconomique(true);
                        domaineStructureSociale.setLibelleActiviteEconomique(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_AUTRE:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setAutres(true);
                        domaineStructureSociale.setLibelleAutres(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_COMMERCE_EQUITABLE:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setCommerce(true);
                        domaineStructureSociale.setLibelleCommerce(refTypeStructureSociale.getLibelle());
                    }
                    break;
                case EpmTRefTypeStructureSociale.CODE_LUTE_DISCRIMINATIONS:
                    if (refClausesSociales == refTypeStructureSociale.getEpmTRefClauseSociale()) {
                        domaineStructureSociale.setLutte(true);
                        domaineStructureSociale.setLibelleLutte(refTypeStructureSociale.getLibelle());
                    }
                    break;
                default:
                    break;
            }
        }
        return domaineStructureSociale;
    }


    DomaineClauseEnvironnementale genererDomaineClauseEnvironnementaleGlobal(EpmTBudLotOuConsultation budLotOuConsultation, DomaineClauseEnvironnementale clauseEnvironnementale) {
        for (EpmTRefClausesEnvironnementales refClausesEnvironnementales : budLotOuConsultation.getClausesEnvironnementalesChoixUtilisateur()) {
            switch (refClausesEnvironnementales.getCodeExterne()) {
                case EpmTRefClausesEnvironnementales.CODE_CLAUSE_CONDITIONS:
                    clauseEnvironnementale.setCondition(true);
                    break;
                case EpmTRefClausesEnvironnementales.CODE_CLAUSE_ATTRIBUTION:
                    clauseEnvironnementale.setAttribution(true);
                    break;
                case EpmTRefClausesEnvironnementales.CODE_CLAUSE_SPECIFICATION:
                    clauseEnvironnementale.setSpecification(true);
                    break;
                default:
                    break;
            }
        }
        return clauseEnvironnementale;
    }

    /**
     * Domaine de la durée de marché
     * @param domaineConsultation
     * @throws ApplicationNoyauException
     */
    void genererDomaineDureeMarche(DomaineConsultation domaineConsultation) throws ApplicationNoyauException {
        if (Constantes.OUI.equals(consultation.getAllotissement()))
            return;

        if (consultation.getEpmTRefDureeDelaiDescription() == null)
            return;

        DomaineDureeMarche domaineDureeMarche = new DomaineDureeMarche();
        switch (consultation.getEpmTRefDureeDelaiDescription().getId()) {
            case EpmTRefDureeDelaiDescription.DUREE_MARCHE_EN_MOIS_EN_JOUR:
                domaineDureeMarche.setNombreUnite(consultation.getDureeMarche());
                domaineDureeMarche.setUniteTemps(consultation.getEpmTRefChoixMoisJour().getLibelle());
                break;
            case EpmTRefDureeDelaiDescription.DELAI_EXECUTION_DU_AU:
                domaineDureeMarche.setDateDebut(consultation.getDateExecutionPrestationsDebut().getTime());
                domaineDureeMarche.setDateFin(consultation.getDateExecutionPrestationsFin().getTime());
                break;
            case EpmTRefDureeDelaiDescription.DESCRIPTION_LIBRE:
                domaineDureeMarche.setDescription(consultation.getDescriptionDuree());
                break;
        }
        domaineConsultation.setDureeMarche(domaineDureeMarche);
    }

    /**
     * Domaine de la durée de marché
     * @param domaineLot
     * @throws ApplicationNoyauException
     */
    private void genererDomaineDureeMarche(DomaineLot domaineLot, EpmTBudLot epmTBudLot) throws ApplicationNoyauException {

        if (epmTBudLot.getEpmTRefDureeDelaiDescription() == null)
            return;

        DomaineDureeMarche domaineDureeMarche = new DomaineDureeMarche();
        switch (epmTBudLot.getEpmTRefDureeDelaiDescription().getId()) {
            case EpmTRefDureeDelaiDescription.DUREE_MARCHE_EN_MOIS_EN_JOUR:
                domaineDureeMarche.setNombreUnite(epmTBudLot.getDureeMarche());
                domaineDureeMarche.setUniteTemps(epmTBudLot.getEpmTRefChoixMoisJour().getLibelle());
                break;
            case EpmTRefDureeDelaiDescription.DELAI_EXECUTION_DU_AU:
                domaineDureeMarche.setDateDebut(epmTBudLot.getDateDebut().getTime());
                domaineDureeMarche.setDateFin(epmTBudLot.getDateFin().getTime());
                break;
            case EpmTRefDureeDelaiDescription.DESCRIPTION_LIBRE:
                domaineDureeMarche.setDescription(epmTBudLot.getDescriptionDuree());
                break;
        }
        domaineLot.setDureeMarche(domaineDureeMarche);
    }

    public GeneriqueDAO getGeneriqueDAO() {
        return generiqueDAO;
    }

    public void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    public DozerBeanMapper getConversionService() {
        return conversionService;
    }

    public void setConversionService(DozerBeanMapper conversionService) {
        this.conversionService = conversionService;
    }

    public Referentiels getReferentiels() {
        return referentiels;
    }

    public void setReferentiels(Referentiels referentiels) {
        this.referentiels = referentiels;
    }


    public Integer getIdLot() {
        return idLot;
    }

    public void setIdLot(Integer idLot) {
        this.idLot = idLot;
    }

    public EpmTConsultation getConsultation() {
        return consultation;
    }

    public void setConsultation(EpmTConsultation consultation) {
        this.consultation = consultation;
    }

    public EpmTBudLot getLot() {
        return lot;
    }

    public void setLot(EpmTBudLot lot) {
        this.lot = lot;
    }

    public EpmTAvenant getAvenant() {
        return avenant;
    }

    public void setAvenant(EpmTAvenant avenant) {
        this.avenant = avenant;
    }


}
