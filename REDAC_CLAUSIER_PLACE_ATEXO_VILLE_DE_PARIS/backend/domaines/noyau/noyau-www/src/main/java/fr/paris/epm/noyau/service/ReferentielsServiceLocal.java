package fr.paris.epm.noyau.service;

import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.TypeEpmTRefObject;
import fr.paris.epm.noyau.metier.objetvaleur.DirectionService;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.EpmTObject;

import java.util.Date;
import java.util.Map;

/**
 * Interface du service Référentiels. Ce service est utilisé pour récupérer
 * l'intégralité des référentiels en base.
 * @author $Author$
 * @version $Revision$, $Date$, $Author$
 */
public interface ReferentielsServiceLocal {

    /**
     * Agrège la totalité des référentiels en base de donnée. Une transaction
     * est effectuée pour chaque récupération de référentiel (une dizaine).
     * @return POJO Referentiels stockant chaque collection de référentiels
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException NonTrouveNoyauException
     */
    Referentiels getAllReferentiels();

    /**
     * Recharge la totalité des référentiels.
     *
     * @throws TechnicalNoyauException erreur technique
     * @throws NonTrouveNoyauException NonTrouveNoyauException
     */
    Referentiels rechargerAllReferentiels();

}
