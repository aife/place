package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Gestionnaire d'information métier administration.
 * @author guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class AdministrationGIMImpl extends BaseGIMImpl implements AdministrationGIM {
	
    /**
     * Loggueur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AdministrationGIMImpl.class);

    /**
     * Gestionnaire de Ressources Métier (DAO) fiche pratique.
     */
    private GeneriqueDAO generiqueDAO;


    public final EpmTUtilisateur chargerUtilisateur(final int identifiant) {
        EpmTUtilisateur epmTUtilisateur = generiqueDAO.find(EpmTUtilisateur.class, identifiant);
        return epmTUtilisateur;
    }
    @Override
    public void supprimerAuthentificationToken(String username) {

        LOG.info("Débuter supprimer authentificationToken : " + username);
        AuthentificationTokenCritere critere = new AuthentificationTokenCritere();
        critere.setIdentifiant(username);
        List<EpmTAuthentificationToken> authTokenList = chercherParCritere(critere);
        if ((authTokenList == null) || authTokenList.size() != 1) {
            String errorMsg = "Probleme lors de la recuperation de l'authentification de l'utilisateur (aucun ou plus d'un resultat).";
            LOG.info(errorMsg);
        } else {
            EpmTAuthentificationToken authTokenBdd = authTokenList.get(0);
            supprimer(authTokenBdd);
        }
        LOG.debug("Terminer supprimer authentificationToken : " + username);

    }
    
    /**
     * modifier les informations d'authentification en mettant les valeurs dans param authToken
     */
    @Override
    public EpmTAuthentificationToken modifierAuthentificationToken(EpmTAuthentificationToken authToken) {

        LOG.info("Débuter modifier authentificationToken : "
                + authToken.getIdentifiant());
        AuthentificationTokenCritere critere = new AuthentificationTokenCritere();
        critere.setIdentifiant(authToken.getIdentifiant());
        List<EpmTAuthentificationToken> authTokenList = chercherParCritere(critere);

        if ((authTokenList != null) && (authTokenList.size() > 1)) {
            String errorMsg = "Probleme lors de la recuperation de l'authentification de l'utilisateur (plus d'un resultat).";
            LOG.error(errorMsg);
            throw new TechnicalNoyauException(errorMsg);
        }

        EpmTAuthentificationToken authTokenBdd;
        if ((authTokenList != null) && (!authTokenList.isEmpty())) {
            authTokenBdd = authTokenList.get(0);
        } else {
            authTokenBdd = new EpmTAuthentificationToken();
            authTokenBdd.setIdentifiant(authToken.getIdentifiant());
        }

        authTokenBdd.setSignature(authToken.getSignature());
        authTokenBdd.setToken(authToken.getToken());
        authTokenBdd.setDateDerniereActivite(authToken.getDateDerniereActivite());
        authTokenBdd.setJusteAuthentifier(authToken.isJusteAuthentifier());

        LOG.info("Terminer modifier authentificationToken : " + authToken.getIdentifiant());

        return modifier(authTokenBdd);
    }

    /**
     * @param generiqueDAO
     */
    public final void setGeneriqueDAO(final GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }


}
