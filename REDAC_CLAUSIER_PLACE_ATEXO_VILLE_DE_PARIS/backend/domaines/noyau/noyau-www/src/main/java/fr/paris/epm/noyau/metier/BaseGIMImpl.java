package fr.paris.epm.noyau.metier;

import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTObject;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class BaseGIMImpl implements BaseGIM {

    // Propriétés
    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(BaseGIMImpl.class);
    /**
     * Fabrique de sessions Hibernate.
     */
    private SessionFactory sessionFactory;
    /**
     * Table manipulée par ce gestionnaire.
     */
    private String table;
    /**
     * Objet hibernate représentant la table manipulée par ce gestionnaire.
     */
    private String nomTable;

    // Méthodes

    /**
     * @return nom du POJO associé à la table manipulée par ce GRM
     */
    protected final String getObjetTable() {
        return nomTable;
    }

    /**
     * @param valeur nom de l'objet table, format HQL
     */
    public final void setObjetTable(final String valeur) {
        nomTable = valeur;
    }

    /**
     * @return fabrique de sessions Hibernate
     */
    protected final SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * @param sf fabrique de sessions hibernate
     */
    public final void setSessionFactory(final SessionFactory sf) {
        sessionFactory = sf;
    }

    /**
     * @return nom de la table en base manipulée par ce GRM
     */
    protected final String getTable() {
        return table;
    }

    /**
     * @param valeur nom de la table en base
     */
    public final void setTable(final String valeur) {
        table = valeur;
    }

    /**
     * Attention le critère contient le nom de la table/ propriétés/...
     * @param critere critère de recherche
     * @return liste des objets correspondants
     */
    @Override
    public <T> List<T> chercherParCritere(final Critere critere) {
        Session session = getSessionFactory().getCurrentSession();
        try {
            String requeteHQL = critere.toHQL();
            Query requete = session.createQuery(requeteHQL);
            if (critere.getTaillePage() != 0) {
                requete.setFirstResult(critere.getNumeroPage() * critere.getTaillePage());
                requete.setMaxResults(critere.getTaillePage());
            }
            final Map parametres = critere.getParametres();
            if (parametres != null && parametres.size() > 0) {
                requete.setProperties(parametres);
            }
            List reponse = requete.list();
            if (critere.isChercherNombreResultatTotal()) {
                critere.setProprieteTriee(null);
                String requeteCountHQL = critere.toCountHQL();
                LOG.debug(requeteCountHQL);
                requete = session.createQuery(requeteCountHQL);
                if (parametres != null && parametres.size() > 0) {
                    requete.setProperties(parametres);
                }
                reponse.add(0, requete.uniqueResult());
            }
            return reponse;
        } catch (RuntimeException ex) {
            throw new TechnicalNoyauException(ex.getMessage(), ex);
        }
    }

    @Override
    public <T extends EpmTObject> T chercherUniqueParCritere(final Critere critere) {

        List<T> resList = chercherParCritere(critere);

        if (resList == null || resList.isEmpty())
            return null;

        if (resList.size() > 1) {
            ApplicationNoyauException e = new ApplicationNoyauException("Donnee non unique en base. Critere : " + critere);
            LOG.error(e.getMessage());
            throw e;
        }

        return resList.get(0);
    }
    
    /**
     * 
     * @param valeur sauveagarde en base un objet implement l'interface.
     */
    @Override
    public <T extends EpmTObject> T modifier(T valeur) {
        Session session = getSessionFactory().getCurrentSession();

        T objet = (T) session.merge(valeur);

        session.flush();

        return objet;
    }

    public <T extends EpmTObject> void creer(T valeur) {
        Session session = getSessionFactory().getCurrentSession();
        session.persist(valeur);
        session.flush();
    }
    
    /**
     * Modification d'une liste d'objet {@link EpmTObject}
     * @param valeur liste d'objet implementant l'interface {@link EpmTObject} à modifier
     */
    @Override
    public <T extends EpmTObject> Collection<T> modifier(Collection<T> valeur) {
        return valeur.stream()
                .map(this::modifier)
                .collect(Collectors.toList());
    }

    /**
     * Suppression d'un objet
     * @param valeur objet implement l'interface à supprimer.
     */
    @Override
    public <T extends EpmTObject> void supprimer(T valeur) {
        try {
            Session session = getSessionFactory().getCurrentSession();
            session.delete(valeur);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
            throw new TechnicalNoyauException(ex.getMessage(), ex);
        }
    }

    /**
     * Suppression d'une liste d'objet
     * @param valeur liste d'objet implementant l'interface à supprimer.
     */
    @Override
    public <T extends EpmTObject> void supprimer(List<T> valeur) {
        valeur.forEach(this::supprimer);
    }

    @Override
    public <T extends EpmTObject> T chercherObject(int id, Class<T> clazz) {
        Session session = getSessionFactory().getCurrentSession();
        T epmTObject = (T) session.load(clazz, id);
        Hibernate.initialize(epmTObject);
        return epmTObject;
    }

}
