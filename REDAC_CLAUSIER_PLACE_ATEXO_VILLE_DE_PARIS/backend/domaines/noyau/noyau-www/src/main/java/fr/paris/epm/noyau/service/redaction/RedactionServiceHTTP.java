package fr.paris.epm.noyau.service.redaction;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.metier.redaction.DocumentRedactionCritere;
import fr.paris.epm.noyau.metier.redaction.TemplateVersionCritere;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.redaction.*;
import fr.paris.epm.noyau.service.GeneriqueServiceHTTP;
import fr.paris.epm.noyau.service.RedactionServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;


public class RedactionServiceHTTP extends GeneriqueServiceHTTP implements RedactionServiceSecurise {
    @PostConstruct
    public void init() {
        logger.info("Initialisation de la facade PublicationClausierFacadeImpl");
        this.desactivationAllPublicationEnCours();

    }
    /**
     * Recherche recursive pour les clauses à partir de la référence du canevas
     */
    private static final String RECHERCHE_CLAUSE = "WITH RECURSIVE  chapter AS " +
            "( " +
            "    SELECT has.reference_clause, chapitre.id " +
            "    FROM redaction.epm__t_chapitre chapitre " +
            "    LEFT JOIN redaction.epm__t_canevas canevas on canevas.id = chapitre.id_canevas " +
            "    LEFT JOIN redaction.epm__t_chapitre_has_clause has on has.id_chapitre = chapitre.id  " +
            "    WHERE LOWER(canevas.reference) like LOWER(''%{0}%'') " +
            "     \n" +
            "    UNION ALL \n" +
            "     \n" +
            "    SELECT has.reference_clause, enfant.id " +
            "    FROM chapter " +
            "    INNER JOIN redaction.epm__t_chapitre AS enfant ON enfant.id_parent_chapitre =  chapter.id   " +
            "    LEFT JOIN redaction.epm__t_canevas canevas on canevas.id = enfant.id_canevas " +
            "    LEFT JOIN redaction.epm__t_chapitre_has_clause has on has.id_chapitre = enfant.id  " +
            "    WHERE LOWER(canevas.reference) like LOWER(''%{0}%'')  " +
            ") \n";

    /**
     * Recherche recursive pour les canevas à partir de la référence d'une clause
     */
    private static final String RECHERCHE_CANEVAS = "WITH RECURSIVE  chapter AS\n" +
            "(\n" +
            "    SELECT chapitre.id, chapitre.id_canevas, chapitre.id_parent_chapitre, chapitre.titre, chapitre.numero, chapitre.info_bulle_text, chapitre.info_bulle_url, chapitre.style, chapitre.derogation_article, chapitre.derogation_commentaires, chapitre.derogation_active \n" +
            "    FROM redaction.epm__t_chapitre chapitre \n" +
            "    LEFT JOIN redaction.epm__t_chapitre_has_clause has on has.id_chapitre = chapitre.id \n" +
            "    WHERE LOWER(has.reference_clause) like LOWER(''%{0}%'') \n" +
            "    \n" +
            "    UNION ALL\n" +
            "    \n" +
            "    SELECT enfant.id, enfant.id_canevas, enfant.id_parent_chapitre, enfant.titre, enfant.numero, enfant.info_bulle_text, enfant.info_bulle_url, enfant.style, enfant.derogation_article, enfant.derogation_commentaires, enfant.derogation_active \n" +
            "    FROM chapter\n" +
            "    INNER JOIN redaction.epm__t_chapitre AS enfant ON enfant.id_parent_chapitre =  chapter.id  \n" +
            "    LEFT JOIN redaction.epm__t_chapitre_has_clause has on has.id_chapitre = enfant.id \n" +
            "    WHERE LOWER(has.reference_clause) like LOWER(''%{0}%'') \n" +
            ") \n";

    /**
     * Loggueur.
     */
    private static final Logger logger = LoggerFactory.getLogger(RedactionServiceHTTP.class);

    @Deprecated
    public String chercherLastVersion(Integer idLastVersion) {
        String query = "select version from EpmTPublicationClausier where id = " + idLastVersion;
        return generiqueDAO.findUniqueByQuery(query);
    }

    @Override
    public List<EpmTCanevas> chercherListeCanevasPourClauseInterministerielle(final String referenceClause) {
        List<EpmTCanevas> canevas = generiqueDAO.findBySQLQueryWithParams(
                MessageFormat.format(RECHERCHE_CANEVAS, referenceClause) +
                        " SELECT DISTINCT canevas.id, canevas.id_document_type, canevas.titre, canevas.reference, canevas.date_creation, canevas.date_modification, canevas.id_nature_prestation, canevas.auteur, canevas.etat, canevas.compatible_entite_adjudicatrice, canevas.id_organisme, canevas.canevas_editeur, canevas.id_statut_redaction_clausier, canevas.date_premiere_validation, canevas.date_derniere_validation, canevas.id_ref_ccag, canevas.actif, canevas.id_last_publication " +
                        " FROM redaction.epm__t_canevas canevas \n" +
                        " INNER JOIN chapter on canevas.id = chapter.id_canevas \n" +
                        " WHERE ( canevas.etat <> 2 ) AND ( canevas.canevas_editeur = TRUE ) \n" +
                        " ORDER BY canevas.id DESC;  ", EpmTCanevas.class);

        logger.info("{} canevas trouves", canevas.size());

        return canevas;
    }

    @Override
    public List<EpmTCanevas> chercherListeCanevasPourClauseMinisterielle(final String referenceClause, final Integer organisme) {
        List<EpmTCanevas> canevas = generiqueDAO.findBySQLQueryWithParams(
                MessageFormat.format(RECHERCHE_CANEVAS, referenceClause) +
                        " SELECT DISTINCT canevas.id, canevas.id_document_type, canevas.titre, canevas.reference, canevas.date_creation, canevas.date_modification, canevas.id_nature_prestation, canevas.auteur, canevas.etat, canevas.compatible_entite_adjudicatrice, canevas.id_organisme, canevas.canevas_editeur, canevas.id_statut_redaction_clausier, canevas.date_premiere_validation, canevas.date_derniere_validation, canevas.id_ref_ccag, canevas.actif, canevas.id_last_publication " +
                        " FROM redaction.epm__t_canevas canevas \n" +
                        " INNER JOIN chapter on canevas.id = chapter.id_canevas \n" +
                        " LEFT JOIN redaction.epm__t_canevas_pub canevas_pub on canevas_pub.id_canevas = canevas.id \n" +
                        " WHERE ( canevas.etat <> 2 ) AND ( ( canevas.canevas_editeur = FALSE AND canevas.id_organisme = " + organisme + " ) OR ( canevas.canevas_editeur = TRUE AND canevas_pub.id IS NOT NULL ) ) \n" +
                        " ORDER BY canevas.id DESC;  ", EpmTCanevas.class);

        logger.info("{} canevas trouves", canevas.size());

        return canevas;
    }

    @Override
    public List<EpmTClause> chercherListeClausesPourCanevasMinisterielle(final String referenceCanevas, final Integer organisme) {
        List<EpmTClause> clauses = generiqueDAO.findBySQLQueryWithParams(
                MessageFormat.format(RECHERCHE_CLAUSE, referenceCanevas) +
                        " SELECT DISTINCT clause.id, clause.id_ref_type_document, clause.id_ref_type_clause, clause.id_theme, clause.reference, clause.parametrable_direction, clause.parametrable_agent, clause.texte_fixe_avant, clause.texte_fixe_apres, clause.date_modification, clause.date_creation, clause.auteur, clause.id_nature_prestation, clause.etat, clause.saut_ligne_texte_avant, clause.saut_ligne_texte_apres, clause.parametre_agent, clause.parametre_direction, clause.compatible_entite_adjudicatrice, clause.id_statut_redaction_clausier, clause.id_organisme, clause.clause_editeur, clause.surcharge_actif, clause.nombre_surcharges, clause.date_premiere_validation, clause.date_derniere_validation, clause.id_procedure, clause.actif, clause.formulation_modifiable, clause.id_last_publication, clause.mots_cles, clause.info_bulle_text, clause.info_bulle_url, clause.reference_clause_surchargee, clause.id_clause_origine " +
                        " FROM redaction.epm__t_clause clause \n" +
                        " INNER JOIN chapter on clause.reference = chapter.reference_clause \n" +
                        " LEFT JOIN redaction.epm__t_clause_pub clause_pub on clause_pub.id_clause = clause.id \n" +
                        " WHERE ( clause.etat <> 2 ) AND ( ( clause.clause_editeur = FALSE AND clause.id_organisme = " + organisme + " ) OR ( clause.clause_editeur = TRUE AND clause_pub.id IS NOT NULL ) ) \n" +
                        " ORDER BY clause.id DESC;", EpmTClause.class);

        logger.info("{} clauses trouvees", clauses.size());

        return clauses;
    }

    @Override
    public List<EpmTClause> chercherListeClausesPourCanevasInterministerielle(final String referenceCanevas) {
        List<EpmTClause> clauses = generiqueDAO.findBySQLQueryWithParams(
                MessageFormat.format(RECHERCHE_CLAUSE, referenceCanevas) +
                        " SELECT DISTINCT clause.id, clause.id_ref_type_document, clause.id_ref_type_clause, clause.id_theme, clause.reference, clause.parametrable_direction, clause.parametrable_agent, clause.texte_fixe_avant, clause.texte_fixe_apres, clause.date_modification, clause.date_creation, clause.auteur, clause.id_nature_prestation, clause.etat, clause.saut_ligne_texte_avant, clause.saut_ligne_texte_apres, clause.parametre_agent, clause.parametre_direction, clause.compatible_entite_adjudicatrice, clause.id_statut_redaction_clausier, clause.id_organisme, clause.clause_editeur, clause.surcharge_actif, clause.nombre_surcharges, clause.date_premiere_validation, clause.date_derniere_validation, clause.id_procedure, clause.actif, clause.formulation_modifiable, clause.id_last_publication, clause.mots_cles, clause.info_bulle_text, clause.info_bulle_url, clause.reference_clause_surchargee, clause.id_clause_origine " +
                        " FROM redaction.epm__t_clause clause \n" +
                        " INNER JOIN chapter on clause.reference = chapter.reference_clause \n" +
                        " WHERE ( clause.etat <> 2 ) AND ( clause.clause_editeur = true ) \n" +
                        " ORDER BY clause.id DESC;", EpmTClause.class);

        logger.info("{} clauses trouvees", clauses.size());

        return clauses;
    }

    @Deprecated
    public List<String> chercherDerniereReferenceCanevas(Integer idOrganisme, boolean canevasEditeur) {
        StringBuilder queryString = new StringBuilder("select distinct(canevas.reference) from EpmTCanevas as canevas ");
        if (canevasEditeur) {
            queryString.append(" where canevas.canevasEditeur = true ");
        } else {
            queryString.append(" where canevas.canevasEditeur = false ");
            if (idOrganisme != null) {
                queryString.append(" and canevas.idOrganisme = ");
                queryString.append(idOrganisme);
            }
        }
        return generiqueDAO.findByQuery(queryString.toString());
    }

    @Deprecated
    public List<String> chercherDerniereReferenceClause(Integer idOrganisme, boolean clauseEditeur) {
        StringBuilder queryString = new StringBuilder("select distinct(clause.reference) from EpmTClause as clause ");
        if (clauseEditeur) {
            queryString.append(" where clause.clauseEditeur = true ");
        } else {
            queryString.append(" where clause.clauseEditeur = false and clause.referenceClauseSurchargee is null ");
            if (idOrganisme != null) {
                queryString.append(" and clause.idOrganisme = ");
                queryString.append(idOrganisme);
            }
        }
        return generiqueDAO.findByQuery(queryString.toString());
    }

    @Deprecated
    public EpmTClauseAbstract chercherClauseMemeReferenceGrandIdentifiant(Integer idOrganisme, String reference,
                                                                          Integer idPublication, Boolean isFormeEditeur,
                                                                          boolean bricolage) { // temporairement
        boolean dejaPubliee = idPublication != null && idPublication != 0;
        boolean parOrganisme = idOrganisme != null && idOrganisme != 0;
        boolean estClauseEditeur = Boolean.TRUE.equals(isFormeEditeur);

        String table = getTableName(reference, dejaPubliee, estClauseEditeur);

        logger.info("Chargement de la clause (editeur? {}) avec la référence {} depuis la table/vue {}", isFormeEditeur, reference, table);

        String query = getQuery(reference, table, idOrganisme, idPublication);

        logger.info("Requete initiale pour la clause {} = '{}'", reference, query);

        EpmTClauseAbstract epmTClause = getUniqueByQuery(query, table);

        if (epmTClause == null && parOrganisme) {
            if (dejaPubliee) {
                query = MessageFormat.format("from {0} where reference = ''{1}'' and idPublication = {2,number,#} order by id desc ", table, reference, idPublication);
            } else {
                query = MessageFormat.format("from {0} where reference = ''{1}'' order by id desc", table, reference);
            }

            epmTClause = getUniqueByQuery(query, table);
        }

        if (epmTClause == null && estClauseEditeur) {
            table = "EpmVClause";

            logger.debug("Clause {} éditeur non trouvé, recherche dans la table {}", reference, table);

            if (dejaPubliee) {
                query = MessageFormat.format("from {0} where reference = ''{1}'' and idPublication = {2,number,#} order by id desc ", table, reference, idPublication);
            } else {
                query = MessageFormat.format("from {0} where reference = ''{1}'' order by id desc", table, reference);
            }

            epmTClause = getUniqueByQuery(query, table);
        }

        if (bricolage) {
            if (epmTClause == null) {
                logger.error("ATTENTION : BRICOLAGE -> clause {}", reference);

                query = MessageFormat.format("from {0} where reference = ''{1}'' order by id desc ", table, reference);

                epmTClause = getUniqueByQuery(query, table);
            }

            if (epmTClause == null) {
                logger.error("ATTENTION : BRICOLAGE GRAVE -> clause {}", reference);
                table = "EpmTClause";
                query = MessageFormat.format("from {0} where reference = ''{1}'' order by id desc ", table, reference);

                epmTClause = getUniqueByQuery(query, table);
            }
        }

        logger.debug("Resultat pour la recherche de la clause depuis la référence {} (chargé depuis la table {}) = {}", reference, table, null != epmTClause ? epmTClause.getIdClause() : "inconnue");

        return epmTClause;
    }

    private String getQuery(String reference, String table, Integer idOrganisme, Integer idPublication) {
        boolean dejaPubliee = idPublication != null && idPublication != 0;
        boolean parOrganisme = idOrganisme != null && idOrganisme != 0;

        String query = MessageFormat.format("from {0} where reference = ''{1}''", table, reference);

        if (parOrganisme) {
            query += MessageFormat.format(" and idOrganisme = {0,number,#}", idOrganisme);
        }

        if (dejaPubliee) {
            query += MessageFormat.format(" and idPublication = {0,number,#}", idPublication);
        }

        query += " order by id desc";
        return query;
    }

    private String getTableName(String reference, boolean dejaPubliee, boolean estClauseEditeur) {
        String table;
        if (dejaPubliee) {
            table = "EpmTClausePub";

            logger.debug("Recherche de la clause {} déjà publiée, donc depuis la table {}", reference, table);
        } else {
            if (estClauseEditeur) {
                table = "EpmTClause";
            } else {
                table = "EpmVClause";
            }

            logger.debug("Recherche de la clause {} (editeur?{}) NON publiée, donc depuis la table {}", reference, estClauseEditeur, table);
        }
        return table;
    }

    private EpmTClauseAbstract getUniqueByQuery(String stringQuery, String queryTable) {
        boolean caching = !queryTable.startsWith("EpmV");
        return generiqueDAO.findUniqueByQuery(stringQuery, caching);
    }

    /**
     * @param idPublication id publication à vérifier
     * @return true si cette publication à été déjà activée au moins une fois
     */
    @Override
    public boolean isAlreadyActivated(int idPublication) {
        Long countCanevasPub = generiqueDAO.findUniqueByQuery("SELECT count(*) from EpmTCanevasPub WHERE idPublication = " + idPublication);
        Long countClausesPub = generiqueDAO.findUniqueByQuery("SELECT count(*) from EpmTClausePub WHERE idPublication = " + idPublication);

        return countCanevasPub + countClausesPub > 0;
    }

    @Override
    public boolean canActivate(Integer idOrganisme) {
        Long countPublicationActive = generiqueDAO.findUniqueByQuery("SELECT count(*) from EpmTPublicationClausier WHERE enCoursActivation IS TRUE AND idOrganisme = " + idOrganisme);
        return countPublicationActive == 0;
    }

    @Override
    public void desactivationAllPublication() {
        generiqueDAO.executeUpdate(" UPDATE EpmTPublicationClausier p SET p.actif = FALSE WHERE p.actif IS TRUE");
    }

    @Override
    public void desactivationAllPublicationEnCours() {
        generiqueDAO.executeUpdate(" UPDATE EpmTPublicationClausier p SET p.enCoursActivation = FALSE WHERE p.enCoursActivation IS TRUE");
    }

    @Override
    public void publicationClausierUpdateClausePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id " + "FROM redaction.epm__t_clause " + "WHERE clause_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3");
        if (count > 0) {
            int countClauses = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_clause_pub " + "   (id_clause, id_publication, id_ref_type_document, info_bulle_text, info_bulle_url, id_ref_type_clause, id_theme, " + "    reference, mots_cles, parametrable_direction, parametrable_agent, texte_fixe_avant, texte_fixe_apres, " + "    date_modification, date_creation, auteur, actif, id_nature_prestation, id_procedure, " + "    etat, formulation_modifiable, saut_ligne_texte_avant, saut_ligne_texte_apres, id_statut_redaction_clausier, " + "    compatible_entite_adjudicatrice, id_organisme, clause_editeur, date_premiere_validation, date_derniere_validation) " + "(SELECT id, " + idPublication + "  as id_publication, id_ref_type_document, info_bulle_text, info_bulle_url, id_ref_type_clause, id_theme, " + "    reference, mots_cles, parametrable_direction, parametrable_agent, texte_fixe_avant, texte_fixe_apres, " + "    date_modification, date_creation, auteur, actif, id_nature_prestation, id_procedure, " + "    etat, formulation_modifiable, saut_ligne_texte_avant, saut_ligne_texte_apres, id_statut_redaction_clausier, " + "   (CASE WHEN compatible_entite_adjudicatrice is null THEN true ELSE compatible_entite_adjudicatrice END) as compatible_entite_adjudicatrice, " + "    id_organisme, clause_editeur, date_premiere_validation, date_derniere_validation " + "FROM redaction.epm__t_clause " + "WHERE clause_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3" + ");");
            logger.info("Clauses publiées : " + countClauses);
        } else {
            logger.info("Aucune clause à publier");
        }
    }

    @Override
    public void updateClausePub(int id, int idClause) {
        int countClauses = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_clause_pub " +
                        "SET id_clause = " + idClause + " " +
                        "WHERE id = " + id);
        logger.info("Clause pub mise à jour : " + countClauses);
    }

    @Override
    public void deleteContratPub(int idContrat, int idClause, int idPublication) {
        generiqueDAO.executeSqlUpdate(
                "DELETE from redaction.epm__t_clause_has_ref_type_contrat_pub " +
                        "WHERE id_clause = " + idClause + " AND id_publication = " + idPublication + " AND id_type_contrat = " + idContrat);
        logger.info("Clause reset Contrat Pub mise à jour");
    }


    @Override
    public void updateClientClauseAndCanevas() {
        generiqueDAO.executeSqlUpdate("truncate table redaction.epm__v_clause");
        generiqueDAO.executeSqlUpdate("truncate table redaction.epm__v_canevas");
        generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__v_clause (id,\n" +
                "                  id_clause,\n" +
                "                  id_clause_publication,\n" +
                "                  id_publication,\n" +
                "                  id_ref_type_document,\n" +
                "                  info_bulle_text,\n" +
                "                  info_bulle_url,\n" +
                "                  id_ref_type_clause,\n" +
                "                  id_theme,\n" +
                "                  reference,\n" +
                "                  mots_cles,\n" +
                "                  parametrable_direction,\n" +
                "                  parametrable_agent,\n" +
                "                  texte_fixe_avant,\n" +
                "                  texte_fixe_apres,\n" +
                "                  date_modification,\n" +
                "                  date_creation,\n" +
                "                  auteur,\n" +
                "                  actif,\n" +
                "                  id_nature_prestation,\n" +
                "                  id_procedure,\n" +
                "                  etat,\n" +
                "                  formulation_modifiable,\n" +
                "                  saut_ligne_texte_avant,\n" +
                "                  saut_ligne_texte_apres,\n" +
                "                  id_statut_redaction_clausier,\n" +
                "                  compatible_entite_adjudicatrice,\n" +
                "                  id_organisme,\n" +
                "                  clause_editeur,\n" +
                "                  date_premiere_validation,\n" +
                "                  date_derniere_validation)\n" +
                "SELECT NULL,\n" +
                "       NULL,\n" +
                "       clause_pub.id_clause,\n" +
                "       clause_pub.id_publication,\n" +
                "       clause_pub.id_ref_type_document,\n" +
                "       clause_pub.info_bulle_text,\n" +
                "       clause_pub.info_bulle_url,\n" +
                "       clause_pub.id_ref_type_clause,\n" +
                "       clause_pub.id_theme,\n" +
                "       clause_pub.reference,\n" +
                "       clause_pub.mots_cles,\n" +
                "       clause_pub.parametrable_direction,\n" +
                "       clause_pub.parametrable_agent,\n" +
                "       clause_pub.texte_fixe_avant,\n" +
                "       clause_pub.texte_fixe_apres,\n" +
                "       clause_pub.date_modification,\n" +
                "       clause_pub.date_creation,\n" +
                "       clause_pub.auteur,\n" +
                "       clause_pub.actif,\n" +
                "       clause_pub.id_nature_prestation,\n" +
                "       clause_pub.id_procedure,\n" +
                "       clause_pub.etat,\n" +
                "       clause_pub.formulation_modifiable,\n" +
                "       clause_pub.saut_ligne_texte_avant,\n" +
                "       clause_pub.saut_ligne_texte_apres,\n" +
                "       clause_pub.id_statut_redaction_clausier,\n" +
                "       clause_pub.compatible_entite_adjudicatrice,\n" +
                "       clause_pub.id_organisme,\n" +
                "       clause_pub.clause_editeur,\n" +
                "       clause_pub.date_premiere_validation,\n" +
                "       clause_pub.date_derniere_validation\n" +
                "\n" +
                "from redaction.epm__t_clause_pub clause_pub\n" +
                "where clause_pub.actif is true\n" +
                "  and clause_pub.clause_editeur is true\n" +
                "  and clause_pub.id_publication in (select p.id\n" +
                "                                    from redaction.epm__t_publication_clausier p\n" +
                "                                    where actif is true);");
        generiqueDAO.executeSqlUpdate("INSERT\n" +
                "INTO redaction.epm__v_canevas (id,\n" +
                "                               id_canevas,\n" +
                "                               id_canevas_publication,\n" +
                "                               id_publication,\n" +
                "                               id_document_type,\n" +
                "                               titre,\n" +
                "                               reference,\n" +
                "                               date_creation,\n" +
                "                               date_modification,\n" +
                "                               id_nature_prestation,\n" +
                "                               auteur,\n" +
                "                               etat,\n" +
                "                               id_statut_redaction_clausier,\n" +
                "                               compatible_entite_adjudicatrice,\n" +
                "                               id_organisme,\n" +
                "                               canevas_editeur,\n" +
                "                               date_premiere_validation,\n" +
                "                               date_derniere_validation,\n" +
                "                               id_ref_ccag,\n" +
                "                               actif)\n" +
                "SELECT NULL,\n" +
                "       NULL,\n" +
                "       canevas_pub.id_canevas,\n" +
                "       canevas_pub.id_publication,\n" +
                "       canevas_pub.id_document_type,\n" +
                "       canevas_pub.titre,\n" +
                "       canevas_pub.reference,\n" +
                "       canevas_pub.date_creation,\n" +
                "       canevas_pub.date_modification,\n" +
                "       canevas_pub.id_nature_prestation,\n" +
                "       canevas_pub.auteur,\n" +
                "       canevas_pub.etat,\n" +
                "       canevas_pub.id_statut_redaction_clausier,\n" +
                "       canevas_pub.compatible_entite_adjudicatrice,\n" +
                "       canevas_pub.id_organisme,\n" +
                "       canevas_pub.canevas_editeur,\n" +
                "       canevas_pub.date_premiere_validation,\n" +
                "       canevas_pub.date_derniere_validation,\n" +
                "       canevas_pub.id_ref_ccag,\n" +
                "       canevas_pub.actif\n" +
                "FROM redaction.epm__t_canevas_pub canevas_pub\n" +
                "WHERE canevas_pub.actif is true\n" +
                "  and canevas_pub.canevas_editeur is true\n" +
                "  and canevas_pub.id_publication in (select p.id\n" +
                "                                     from redaction.epm__t_publication_clausier p\n" +
                "                                     where actif is true);");
        generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__v_clause (id,\n" +
                "                         id_clause,\n" +
                "                         id_ref_type_document,\n" +
                "                         info_bulle_text,\n" +
                "                         info_bulle_url,\n" +
                "                         id_ref_type_clause,\n" +
                "                         id_theme,\n" +
                "                         reference,\n" +
                "                         mots_cles,\n" +
                "                         parametrable_direction,\n" +
                "                         parametrable_agent,\n" +
                "                         texte_fixe_avant,\n" +
                "                         texte_fixe_apres,\n" +
                "                         date_modification,\n" +
                "                         date_creation,\n" +
                "                         auteur,\n" +
                "                         actif,\n" +
                "                         id_nature_prestation,\n" +
                "                         id_procedure,\n" +
                "                         etat,\n" +
                "                         formulation_modifiable,\n" +
                "                         saut_ligne_texte_avant,\n" +
                "                         saut_ligne_texte_apres,\n" +
                "                         id_statut_redaction_clausier,\n" +
                "                         compatible_entite_adjudicatrice,\n" +
                "                         id_organisme,\n" +
                "                         clause_editeur,\n" +
                "                         date_premiere_validation,\n" +
                "                         date_derniere_validation)\n" +
                "SELECT NULL,\n" +
                "       clause_client.id,\n" +
                "       clause_client.id_ref_type_document,\n" +
                "       clause_client.info_bulle_text,\n" +
                "       clause_client.info_bulle_url,\n" +
                "       clause_client.id_ref_type_clause,\n" +
                "       clause_client.id_theme,\n" +
                "       clause_client.reference,\n" +
                "       clause_client.mots_cles,\n" +
                "       clause_client.parametrable_direction,\n" +
                "       clause_client.parametrable_agent,\n" +
                "       clause_client.texte_fixe_avant,\n" +
                "       clause_client.texte_fixe_apres,\n" +
                "       clause_client.date_modification,\n" +
                "       clause_client.date_creation,\n" +
                "       clause_client.auteur,\n" +
                "       clause_client.actif,\n" +
                "       clause_client.id_nature_prestation,\n" +
                "       clause_client.id_procedure,\n" +
                "       clause_client.etat,\n" +
                "       clause_client.formulation_modifiable,\n" +
                "       clause_client.saut_ligne_texte_avant,\n" +
                "       clause_client.saut_ligne_texte_apres,\n" +
                "       clause_client.id_statut_redaction_clausier,\n" +
                "       clause_client.compatible_entite_adjudicatrice,\n" +
                "       clause_client.id_organisme,\n" +
                "       clause_client.clause_editeur,\n" +
                "       clause_client.date_premiere_validation,\n" +
                "       clause_client.date_derniere_validation\n" +
                "FROM redaction.epm__t_clause clause_client\n" +
                "WHERE clause_client.clause_editeur is false;");
        generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__v_canevas (id,\n" +
                "                                      id_canevas,\n" +
                "                                      id_canevas_publication,\n" +
                "                                      id_publication,\n" +
                "                                      id_document_type,\n" +
                "                                      titre,\n" +
                "                                      reference,\n" +
                "                                      date_creation,\n" +
                "                                      date_modification,\n" +
                "                                      id_nature_prestation,\n" +
                "                                      auteur,\n" +
                "                                      etat,\n" +
                "                                      id_statut_redaction_clausier,\n" +
                "                                      compatible_entite_adjudicatrice,\n" +
                "                                      id_organisme,\n" +
                "                                      canevas_editeur,\n" +
                "                                      date_premiere_validation,\n" +
                "                                      date_derniere_validation,\n" +
                "                                      id_ref_ccag,\n" +
                "                                      actif)\n" +
                "SELECT NULL,\n" +
                "       canevas_client.id,\n" +
                "       NULL,\n" +
                "       NULL,\n" +
                "       canevas_client.id_document_type,\n" +
                "       canevas_client.titre,\n" +
                "       canevas_client.reference,\n" +
                "       canevas_client.date_creation,\n" +
                "       canevas_client.date_modification,\n" +
                "       canevas_client.id_nature_prestation,\n" +
                "       canevas_client.auteur,\n" +
                "       canevas_client.etat,\n" +
                "       canevas_client.id_statut_redaction_clausier,\n" +
                "       canevas_client.compatible_entite_adjudicatrice,\n" +
                "       canevas_client.id_organisme,\n" +
                "       canevas_client.canevas_editeur,\n" +
                "       canevas_client.date_premiere_validation,\n" +
                "       canevas_client.date_derniere_validation,\n" +
                "       canevas_client.id_ref_ccag,\n" +
                "       canevas_client.actif\n" +
                "FROM redaction.epm__t_canevas canevas_client\n" +
                "WHERE canevas_client.canevas_editeur is false;");
    }

    @Override
    public void updateRoleClausePub(int id, int idClause, int idRoleClause) {
        int countClauses = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_role_clause_pub " +
                        "SET id_clause = " + idClause + " , id_role_clause = " + idRoleClause +
                        " WHERE id = " + id);
        logger.info("Role clause pub mise à jour : " + countClauses);
    }

    @Override
    public void publicationClausierUpdateClause(int idPublication) {
        int countClauses = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_clause " +
                        "SET id_last_publication = " + idPublication + " " +
                        "WHERE clause_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3");
        logger.info("Clauses mises à jours : " + countClauses);
    }

    @Override
    public void publicationClausierUpdateClause(int idPublication, List<Integer> idClauses) {
        int countClauses = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_clause " +
                        "SET id_last_publication = " + idPublication + " , " +
                        "etat = 1 ," +
                        "actif = true ," +
                        "id_statut_redaction_clausier = 3 " +
                        "WHERE id in (" + idClauses.stream()
                        .map(String::valueOf).collect(Collectors.joining(",")) + ")");
        logger.info("Clauses mises à jours : " + countClauses);
    }

    @Override
    public void publicationClausierUpdateRoleClausePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id " + "FROM redaction.epm__t_role_clause " + "WHERE id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ");");
        if (count > 0) {
            int countRolesClause = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_role_clause_pub " + "   (id_role_clause, id_publication, id_clause, id_direction_service, id_utilisateur, valeur_defaut, precochee, " + "    etat, id_preference, nombre_carateres_max, champ_obligatoire, valeur_heritee_modifiable, num_formulation) " + "(SELECT id, " + idPublication + " as id_publication, id_clause, id_direction_service, id_utilisateur, valeur_defaut, precochee, " + "    etat, id_preference, nombre_carateres_max, champ_obligatoire, valeur_heritee_modifiable, num_formulation " + "FROM redaction.epm__t_role_clause " + "WHERE id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ")" + ");");
            logger.info("Roles-clause publiées : " + countRolesClause);
        } else {
            logger.info("Aucun role-clause publiées");
        }
    }

    @Override
    public void publicationClausierUpdateClauseHasTypeContratPub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id_clause, id_type_contrat FROM redaction.epm__t_clause_has_ref_type_contrat " + "WHERE id_type_contrat is not null and id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ");");
        if (count > 0) {
            int countClauseContrat = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_clause_has_ref_type_contrat_pub " + "   (id_clause, id_type_contrat, id_publication) " + "(SELECT id_clause, id_type_contrat, " + idPublication + " as id_publication " + "FROM redaction.epm__t_clause_has_ref_type_contrat " + "WHERE id_type_contrat is not null and id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ")" + ");");
            logger.info("Types contrat des clause publiés : " + countClauseContrat);
        } else {
            logger.info("Aucun type contrat des clause publiés");
        }
    }

    @Override
    public void publicationClausierUpdateClauseHasPotentiellementConditionneePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT  id " + "FROM redaction.epm__t_clause_has_valeur_potentiellement_conditionnee " + "WHERE id_clause_potentiellement_conditionnee IN " + "   (SELECT id_clause_potentiellement_conditionnee FROM redaction.epm__t_clause_has_potentiellement_conditionnee_pub " + "    WHERE id_publication = " + idPublication + ");");
        if (count > 0) {
            int countConditions = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_clause_has_potentiellement_conditionnee_pub " + "   (id_clause_potentiellement_conditionnee, id_publication, id_clause, id_ref_potentiellement_conditionnee) " + "(SELECT id, " + idPublication + " as id_publication, id_clause, id_ref_potentiellement_conditionnee " + "FROM redaction.epm__t_clause_has_potentiellement_conditionnee " + "WHERE id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ")" + ");");
            logger.info("Conditions des clause publiées : " + countConditions);
        } else {
            logger.info("Aucune condition des clause publiées");
        }
    }

    @Override
    public void publicationClausierUpdateClauseHasValeurPotentiellementConditionneePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id " + "FROM redaction.epm__t_clause_has_potentiellement_conditionnee " + "WHERE id_clause IN (SELECT id_clause FROM redaction.epm__t_clause_pub WHERE id_publication = " + idPublication + ");");
        if (count > 0) {
            int countValeursCondition = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_clause_has_valeur_potentiellement_conditionnee_pub " + "   (id_clause_valeur_potentiellement_conditionnee, id_publication, id_clause_potentiellement_conditionnee, valeur) " + "(SELECT  id, " + idPublication + " as id_publication, id_clause_potentiellement_conditionnee, valeur " + "FROM redaction.epm__t_clause_has_valeur_potentiellement_conditionnee " + "WHERE id_clause_potentiellement_conditionnee IN " + "   (SELECT id_clause_potentiellement_conditionnee FROM redaction.epm__t_clause_has_potentiellement_conditionnee_pub " + "    WHERE id_publication = " + idPublication + ")" + ");");
            logger.info("Valeur de conditions des clause publiées : " + countValeursCondition);
        } else {
            logger.info("Aucune valeur de condition des clause publiées");
        }
    }

    @Override
    public void publicationClausierUpdateCanevas(int idPublication) {
        int countCanevas = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_canevas " +
                        "SET id_last_publication = " + idPublication + " " +
                        "WHERE canevas_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3");
        logger.info("Canevas mis à jours : " + countCanevas);
    }

    @Override
    public void publicationClausierUpdateCanevas(int idPublication, List<Integer> idCanevas) {
        int countCanevas = generiqueDAO.executeSqlUpdate(
                "UPDATE redaction.epm__t_canevas " +
                        "SET id_last_publication = " + idPublication + " , " +
                        "etat = 1 ," +
                        "actif = true ," +
                        "id_statut_redaction_clausier = 3 " +
                        "WHERE  id in (" + idCanevas.stream()
                        .map(String::valueOf).collect(Collectors.joining(",")) + ")");
        logger.info("Canevas mis à jours : " + countCanevas);
    }

    @Override
    public void publicationClausierUpdateCanevasPub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id " + "FROM redaction.epm__t_canevas " + "WHERE canevas_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3");
        if (count > 0) {
            int countCanevas = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_canevas_pub " + "   (id_canevas, id_publication, id_document_type, titre, reference, date_creation, date_modification, " + "    actif, id_nature_prestation, id_ref_ccag, auteur, etat, compatible_entite_adjudicatrice, id_organisme, " + "    canevas_editeur, id_statut_redaction_clausier, date_premiere_validation, date_derniere_validation) " + "(SELECT id, " + idPublication + " as id_publication, id_document_type, titre, reference, date_creation, date_modification, " + "   actif, id_nature_prestation, id_ref_ccag, auteur, etat, compatible_entite_adjudicatrice, id_organisme, " + "   canevas_editeur, id_statut_redaction_clausier, date_premiere_validation, date_derniere_validation " + "FROM redaction.epm__t_canevas " + "WHERE canevas_editeur = true AND actif = true AND etat <> '2' AND id_statut_redaction_clausier = 3" + ")");
            logger.info("Canevas publiés : " + countCanevas);
        } else {
            logger.info("Aucun canevas publiés");
        }
    }

    @Override
    public void publicationClausierUpdateCanevasHasProcedurePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT distinct id_canevas, id_procedure " + "FROM redaction.epm__t_canevas_has_ref_procedure_passation " + "WHERE id_canevas IN (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")");
        if (count > 0) {
            int countCanevasProcedure = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_canevas_has_ref_procedure_passation_pub" + "   (id_canevas, id_procedure, id_publication) " + "(SELECT distinct id_canevas, id_procedure, " + idPublication + " as id_publication " + "FROM redaction.epm__t_canevas_has_ref_procedure_passation " + "WHERE id_canevas IN (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")" + ")");
            logger.info("Procedures des canevas publiées : " + countCanevasProcedure);
        } else {
            logger.info("Aucune procedure des canevas publiées");
        }
    }

    @Override
    public void publicationClausierUpdateCanevasHasTypeContratPub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT distinct id_canevas, id_type_contrat " + "FROM redaction.epm__t_canevas_has_ref_type_contrat " + "WHERE id_canevas IN (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")");
        if (count > 0) {
            int countCanevasContrat = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_canevas_has_ref_type_contrat_pub " + "   (id_canevas, id_type_contrat, id_publication) " + "(SELECT id_canevas, id_type_contrat, " + idPublication + " as id_publication " + "FROM redaction.epm__t_canevas_has_ref_type_contrat " + "WHERE id_canevas IN (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")" + ")");
            logger.info("Types contrat des canevas publiés : " + countCanevasContrat);
        } else {
            logger.info("Aucun type contrat des canevas publiés");
        }
    }

    @Override
    public void publicationClausierUpdateChapitrePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id " + "FROM redaction.epm__t_chapitre " + "WHERE id IN (" + "   WITH RECURSIVE chapitrePub(id_chapitre, id_parent) AS (" + "       SELECT id, id_canevas FROM redaction.epm__t_chapitre " + "       WHERE id_canevas in (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")" + "           UNION ALL " + "       SELECT ch.id, ch.id_parent_chapitre FROM redaction.epm__t_chapitre ch " + "       INNER JOIN chapitrePub ch_pub ON ch.id_parent_chapitre = ch_pub.id_chapitre " + "    ) SELECT id_chapitre FROM chapitrePub)");
        if (count > 0) {
            int countChapitres = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_chapitre_pub " + "   (id_chapitre, id_publication, id_canevas, id_parent_chapitre, titre, numero, info_bulle_text, " + "    info_bulle_url, style, derogation_article, derogation_commentaires, derogation_active) " + "(SELECT id, " + idPublication + " as id_publication, id_canevas, id_parent_chapitre, titre, numero, info_bulle_text, " + "   info_bulle_url, style, derogation_article, derogation_commentaires, derogation_active " + "FROM redaction.epm__t_chapitre " + "WHERE id IN (" + "   WITH RECURSIVE chapitrePub(id_chapitre, id_parent) AS (" + "       SELECT id, id_canevas FROM redaction.epm__t_chapitre " + "       WHERE id_canevas in (SELECT id_canevas FROM redaction.epm__t_canevas_pub WHERE id_publication = " + idPublication + ")" + "           UNION ALL " + "       SELECT ch.id, ch.id_parent_chapitre FROM redaction.epm__t_chapitre ch " + "       INNER JOIN chapitrePub ch_pub ON ch.id_parent_chapitre = ch_pub.id_chapitre " + "    ) SELECT id_chapitre FROM chapitrePub)" + ")");
            logger.info("Chapitres publiés : " + countChapitres);
        } else {
            logger.info("Aucun chapitre publiés");
        }
    }

    @Override
    public void publicationClausierUpdateChapitreHasClausePub(int idPublication) {
        int count = generiqueDAO.executeSqlcount("SELECT id_chapitre " + "FROM redaction.epm__t_chapitre_has_clause " + "WHERE id_chapitre IN (SELECT id_chapitre FROM redaction.epm__t_chapitre_pub WHERE id_publication = " + idPublication + ")");
        if (count > 0) {
            int countChapitresClauses = generiqueDAO.executeSqlUpdate("INSERT INTO redaction.epm__t_chapitre_has_clause_pub " + "   (id_chapitre, reference_clause, num_order, id_publication) " + "(SELECT id_chapitre, reference_clause, num_order, " + idPublication + " as id_publication " + "FROM redaction.epm__t_chapitre_has_clause " + "WHERE id_chapitre IN (SELECT id_chapitre FROM redaction.epm__t_chapitre_pub WHERE id_publication = " + idPublication + ")" + ")");
            logger.info("Clauses des chapitres publiés : " + countChapitresClauses);
        } else {
            logger.info("Aucune clause des chapitres publiés");
        }
    }

    @Override
    public void rollbackPublicationClausier(int idPublication) {
        generiqueDAO.executeUpdate("DELETE FROM EpmTClauseValeurPotentiellementConditionneePub WHERE idPublication = " + idPublication);
        generiqueDAO.executeUpdate("DELETE FROM EpmTClausePotentiellementConditionneePub WHERE idPublication = " + idPublication);
        generiqueDAO.executeUpdate("DELETE FROM EpmTRoleClausePub WHERE idPublication = " + idPublication);
        generiqueDAO.executeUpdate("DELETE FROM EpmTClausePub WHERE idPublication = " + idPublication);
        generiqueDAO.executeUpdate("DELETE FROM EpmTCanevasPub WHERE idPublication = " + idPublication);
        generiqueDAO.executeUpdate("DELETE FROM EpmTPublicationClausier WHERE id = " + idPublication);
    }

    @Deprecated
    public final List<EpmTPublicationClausier> chercherPublicationsParClause(final int idClause) {
        List<EpmTPublicationClausier> resultat = generiqueDAO.findByQuery(" from EpmTPublicationClausier p where p.id in " +
                "( select distinct clause.idPublication from EpmTClausePub as clause  WHERE clause.etat != '2' AND clause.idClause = " + idClause + " )");
        Collections.sort(resultat);
        return resultat;
    }

    @Deprecated
    public final EpmTDocumentRedaction modifierDocumentRedaction(final EpmTDocumentRedaction epmTDocumentInstance,
                                                                 final boolean nouveauRegistreDocVersion) {
        epmTDocumentInstance.setDateModification(Calendar.getInstance().getTime());
        if (nouveauRegistreDocVersion)
            epmTDocumentInstance.setVersion(epmTDocumentInstance.getVersion() + 1);

        DocumentRedactionCritere critere = new DocumentRedactionCritere();
        critere.setDernierDocumentCree(true);
        critere.setReference(epmTDocumentInstance.getReference());
        critere.setIdConsultation(epmTDocumentInstance.getIdConsultation());

        // MAJ de la variable dernierDocumentCree
        List<EpmTDocumentRedaction> documents = generiqueDAO.findByCritere(critere);
        for (EpmTDocumentRedaction elemDoc : documents) {
            elemDoc.setDernierDocumentCree(false);
            generiqueDAO.merge(elemDoc);
        }

        epmTDocumentInstance.setDernierDocumentCree(true);
        return generiqueDAO.merge(epmTDocumentInstance);
    }

    /**
     * Génére une référence automatique pour le prochain document.
     *
     * @return la référence du prochain document à créer
     */
    @Deprecated
    public String getReferenceDocumentSuivante() {
        logger.debug("Constuire une référence pour le nouvel document");
        try {
            String refSuiv = "DOC" + UUID.randomUUID();
            logger.debug("Référence document construite avec succès");
            return refSuiv;
        } catch (RuntimeException re) {
            logger.error("Echec de construction de la référence du document", re);
            throw new TechnicalNoyauException(re);
        }
    }

    @Override
    public EpmTDocumentRedaction chargerDernierParReference(String query) {
        return generiqueDAO.findUniqueByQuery(query);
    }

    /**
     * Permet de rechercher un template versionné par critere.
     *
     * @param critere objet rassemblant l'ensembles des critères de recherche
     * @return liste des template
     */
    @Override
    public EpmTTemplateVersion rechercherTemplateVersionUnique(TemplateVersionCritere critere) {
        List liste = generiqueDAO.findByCritere(critere);
        List<EpmTTemplateVersion> result = new ArrayList<>();
        for (Object o : liste) {
            if (o.getClass().equals(Object[].class)) {
                Object[] obj = (Object[]) o;
                EpmTTemplateVersion tv = new EpmTTemplateVersion();
                tv.setId((Integer) obj[0]);
                tv.setIdIntervention((Integer) obj[1]);
                tv.setIdRepresentant((Integer) obj[2]);
                tv.setIdPageGarde((Integer) obj[3]);
                tv.setIdTypeDocument((Integer) obj[4]);
                tv.setNomFichier((String) obj[5]);
                tv.setDateCreation((Timestamp) obj[6]);
                result.add(tv);
            } else if (o.getClass().equals(EpmTTemplateVersion.class)) {
                result.add((EpmTTemplateVersion) o);
            }
        }

        // TEMPORAIREMENT -> MAX(DATE) ne marche pas ALORS
        if (critere.getDernier() != null && critere.getDernier() && !result.isEmpty()) {
            EpmTTemplateVersion max = result.get(0);
            for (EpmTTemplateVersion tv : result)
                if (tv.getDateCreation().after(max.getDateCreation()))
                    max = tv;
            result.clear();
            result.add(max);
        }
        // TEMPORAIREMENT

        if (result.size() != 1) {
            logger.info("L'objet TEMPLATE VERSION n'est pas trouvé ou n'est pas unique");
            return null;
        }
        return result.get(0);
    }

    @Override
    public List<EpmTUtilisateur> chercher(UtilisateurCritere critere) {
        return generiqueDAO.findByCritere(critere);
    }
}
