package fr.paris.epm.noyau.service.technique;

import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.LogoOrganismeCritere;
import fr.paris.epm.noyau.metier.ParametrageCritere;
import fr.paris.epm.noyau.metier.ServiceTechniqueGIM;
import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.persistance.EpmTLogoOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.service.GeneriqueServiceHTTP;

import java.util.List;

/**
 * Service d'accès aux avenants en utilisant Spring remoting.
 * @author MGA
 */
public class ServiceTechniqueServiceHTTP extends GeneriqueServiceHTTP implements ServiceTechniqueSecurise {
    /**
     * Marqueur de sérialization.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Référence vers le POJO de service associé à cet EJB.
     */
    private static ServiceTechniqueGIM serviceTechnique;

    public final String ping() {
        return "OK";
    }


    public final UtilisateurExterneConsultation getUtilisateurExterneConsultation(
            final String identifiantExterne) {
        return serviceTechnique.getUtilisateurExterneConsultation(identifiantExterne);
    }

    @Override
    public void modifierLogoOrganisme(byte[] fichierLogo, String typeFichierLogo, Integer idOrganisme) {
        EpmTLogoOrganisme logoOrganisme;
        if (idOrganisme == null) {
            logoOrganisme = chercherLogoDefault();
            if (logoOrganisme == null) {
                logoOrganisme = new EpmTLogoOrganisme();
                ParametrageCritere paramCritere = new ParametrageCritere();
                paramCritere.setClef(Constantes.LOGO_DEFAULT);
                EpmTRefParametrage refParametrage = generiqueDAO.findUniqueByCritere(paramCritere);
                if (refParametrage != null)
                    logoOrganisme.setId(Integer.parseInt(refParametrage.getValeur()));
            }
        } else {
            logoOrganisme = chercherLogoOrganisme(idOrganisme);
            if (logoOrganisme == null) {
                logoOrganisme = new EpmTLogoOrganisme();
                logoOrganisme.setIdOrganisme(idOrganisme);
            }
        }
        logoOrganisme.setFichierLogo(fichierLogo);
        logoOrganisme.setTypeFichierLogo(typeFichierLogo);
        logoOrganisme.setMd5Checksum(null);

        logoOrganisme = generiqueDAO.merge(logoOrganisme);
        logoOrganisme.getId();
    }

    /**
     * Permet de rechercher le logo en base selon id d'organisme passé en paramétre.
     * @param idOrganisme organisme auquel appartient le logo
     * @return le fichier image du logo
     * @throws TechnicalNoyauException
     */
    @Override
    public EpmTLogoOrganisme chercherLogoDefault() {
        ParametrageCritere paramCritere = new ParametrageCritere();
        paramCritere.setClef(Constantes.LOGO_DEFAULT);
        EpmTRefParametrage refParametrage = generiqueDAO.findUniqueByCritere(paramCritere);
        if ( refParametrage != null ) {
            LogoOrganismeCritere logoCritere = new LogoOrganismeCritere();
            logoCritere.setId(Integer.parseInt(refParametrage.getValeur()));
            EpmTLogoOrganisme logoOrganisme = generiqueDAO.findUniqueByCritere(logoCritere);
            if (logoOrganisme != null)
                return logoOrganisme;
        }
        return null;
    }

    /**
     * Permet de rechercher le logo en base selon id d'organisme passé en paramétre.
     * @param idOrganisme organisme auquel appartient le logo
     * @return le fichier image du logo
     * @throws TechnicalNoyauException
     */
    @Override
    public EpmTLogoOrganisme chercherLogoOrganisme(Integer idOrganisme) {
        LogoOrganismeCritere critere = new LogoOrganismeCritere();
        critere.setIdOrganisme(idOrganisme);
        List<EpmTLogoOrganisme> listeEpmTLogoOrganisme = generiqueDAO.findByCritere(critere);
        if (listeEpmTLogoOrganisme != null && !listeEpmTLogoOrganisme.isEmpty()) {
            EpmTLogoOrganisme logoOrganisme = listeEpmTLogoOrganisme.get(0);
            return logoOrganisme;
        }
        return null;
    }

    /**
     * Supprimer un logo dans la base s'il existe
     * @param idOrganisme organisme auquel appartient le logo
     * @throws TechnicalNoyauException
     */
    @Override
    public void supprimerLogoOrganisme(Integer idOrganisme) {
        EpmTLogoOrganisme logoOrganisme = chercherLogoOrganisme(idOrganisme);
        if (logoOrganisme != null) {
            generiqueDAO.delete(logoOrganisme);
        }
    }

    /**
     * @param valeur the serviceTechnique to set
     */
    public final void setServiceTechnique(final ServiceTechniqueGIM valeur) {
        serviceTechnique = valeur;
    }

}
