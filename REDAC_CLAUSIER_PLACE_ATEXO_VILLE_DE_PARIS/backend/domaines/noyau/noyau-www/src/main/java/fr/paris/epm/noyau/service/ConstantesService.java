/**
 * $Id$
 */
package fr.paris.epm.noyau.service;

/**
 * Classe statique comportant les constantes utilisées par les services.
 * @author Guillaume Béraudo
 * @version $Revision$, $Date$, $Author$
 */
public class ConstantesService {

    /**
     * Contexte primaire Spring.
     */
    public static final String ID_CONTEXTE_PRIMAIRE = "fabriqueBeanMetier";

    /**
     * Identifiant de l'utilisateur de test.
     */
    public static final int ID_UTILISATEUR_TEST = 1;

    /**
     * Identifiant du profil de test.
     */
    public static final int ID_PROFIL_TEST = 1;

    /**
     * Identifiant de l'hablitation de test.
     */
    public static final int ID_HABILITATION_TEST = 1;

    /**
     * Attribute de la requet HTTP. Service Noyau Web recupererFichier.
     */
    public static final String WEB_REQUEST_ID_FICHIER = "idFichier";

    /**
     * Attribute de la requet HTTP. Service Noyau Web recupererFichier
     * temporaire
     */
    public static final String WEB_REQUEST_ZIP_FICHIER = "zipFichier";

    /**
     * Attribute de la requet HTTP. Service Noyau Web recupererFichier.
     */
    public static final String WEB_REQUEST_ID_UTILISATEUR = "idUtilisateur";
    
}
