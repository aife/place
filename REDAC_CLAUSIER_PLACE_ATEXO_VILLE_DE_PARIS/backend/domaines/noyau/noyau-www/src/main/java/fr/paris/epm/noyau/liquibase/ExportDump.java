package fr.paris.epm.noyau.liquibase;

import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.io.*;

public class ExportDump implements CustomTaskChange {

    private ResourceAccessor resourceAccessor;


    private static final Logger logger = LoggerFactory.getLogger(ExportDump.class);
    private static DataSource dataSource;

    private static String jndiName;

    private static String repertoireTemporaire;

    private String host;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void execute(Database database) throws CustomChangeException {
        logger.info("export dump redaction START");
        String password = null;
        try {
            if (host == null)
                host = "localhost";
            Context ct = new InitialContext();
            BasicDataSource dataSource = (BasicDataSource) ct.lookup(jndiName);
            password = dataSource.getPassword();

            JdbcConnection databaseConnection = (JdbcConnection) database.getConnection();
            Runtime r = Runtime.getRuntime();
            String[] cmd1= {"/bin/sh", "-c", "export PGPASSWORD=" + password + " 2>" + repertoireTemporaire + "variablePasswordError.txt"};
            String[] cmd2= {"/bin/sh", "-c", "pg_dump --no-owner -h " + host + " -U " + databaseConnection.getConnectionUserName() +
                    " " + databaseConnection.getCatalog().replace("noyau", "redaction")
                    + " > " + repertoireTemporaire + "dumpRedaction.sql 2>" + repertoireTemporaire + "dumpRedactionError.txt"};

            Process p1 = r.exec(cmd1);
            p1.waitFor();
            Process p2 = r.exec(cmd2);
            p2.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("" + repertoireTemporaire + "dumpRedaction.sql"))));
            FileWriter writer = new FileWriter("" + repertoireTemporaire + "dumpRedactionReplace.sql");
            String str;
            while ((str = reader.readLine()) != null) {
                if (!str.contains("databasechangelog"))
                    if (str.contains("public."))
                        str = str.replace("public.", "redaction.");     // replace character at a time
                    else if (!str.contains("CONSTRAINT"))
                        str = str.replace("epm__t_", "redaction.epm__t_");
                    else if (str.contains("REFERENCES epm__t_"))
                        str = str.replace("REFERENCES epm__t_", "REFERENCES redaction.epm__t_");
                writer.append(str);
                writer.append("\n");
            }
            writer.close();
            reader.close();

            String[] cmd3= {"/bin/sh", "-c", "psql -h " + host + " -U " + databaseConnection.getConnectionUserName() + " "
                    + databaseConnection.getCatalog() + " < " + repertoireTemporaire + "dumpRedactionReplace.sql 2>" + repertoireTemporaire + "importRedactionError.txt"};
            Process p3 = r.exec(cmd3);
            p3.waitFor();


        } catch (Exception e) {
            throw new CustomChangeException(e);
        }
    }

    @Override
    public String getConfirmationMessage() {
        return null;
    }

    @Override
    public void setUp() throws SetupException {

    }

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {
        this.resourceAccessor = resourceAccessor;
    }

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public String getJndiName() {
        return jndiName;
    }

    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }

    public void setRepertoireTemporaire(String repertoireTemporaire) {
        ExportDump.repertoireTemporaire = repertoireTemporaire;
    }
}
