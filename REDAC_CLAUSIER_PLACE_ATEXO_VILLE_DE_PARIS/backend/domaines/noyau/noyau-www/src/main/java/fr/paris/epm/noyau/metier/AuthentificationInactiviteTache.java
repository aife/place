package fr.paris.epm.noyau.metier;

import java.util.List;
import java.util.TimerTask;

import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Recherche des utilisateurs ayant depasser la periode maximale d'inactivite,
 * afin de les deconnecter.
 *
 * @author Rémi Villé
 * @version $Revision$, $Date$, $Author$
 */
public class AuthentificationInactiviteTache extends TimerTask {

    /**
     * Loggeur.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthentificationInactiviteTache.class);

    /**
     * Acces aux objets persistants (injection Spring).
     */
    private GeneriqueDAO generiqueDAO;

    /**
     * Delai d'inactivite maximum en minute (injection Spring).
     */
    private int delaiInactiviteMax;

    public final void run() {
        String errMsg;
        List<EpmTAuthentificationToken> authentificationTokenBddList;

        LOG.debug("Recherche des utilisateurs ayant depasser la periode maximale d'inactivite.");

        AuthentificationTokenCritere critere = new AuthentificationTokenCritere();
        critere.setDelaiInactiviteMax(delaiInactiviteMax);
        try {
            authentificationTokenBddList = generiqueDAO.findByCritere(critere);
            if (authentificationTokenBddList == null) {
                errMsg = "Probleme lors de la recuperation des informations d'authentifiaction.";
                LOG.error(errMsg);
                throw new TechnicalNoyauException(errMsg);
            }
            generiqueDAO.delete(authentificationTokenBddList);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage());
        }
    }

    public final void setGeneriqueDAO(final GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    public final void setDelaiInactiviteMax(final Integer valeur) {
        this.delaiInactiviteMax = valeur;
    }

}