<schema xmlns:atexo="http://www.atexo.com/rsem" xmlns="http://www.w3.org/2001/XMLSchema"
		targetNamespace="http://www.atexo.com/rsem">
	
	<include schemaLocation="types.xsd" />
	
	<complexType name="Consultation">
		<sequence>
			<element name="mps" type="boolean"/>
			<element name="justificationNonAllotissement" type="string" minOccurs="0">
				<annotation>
					<documentation>Justification de non allotissement</documentation>
				</annotation>
			</element>
			<element name="url" type="string">
				<annotation>
					<documentation>URL de la consultation</documentation>
				</annotation>
			</element>
			<element name="numeroConsultation" type="string">
				<annotation>
					<documentation>Numéro unique de la consultation</documentation>
				</annotation>
			</element>
			<element name="lieuExecutions" type="atexo:LieuExecutionsType" minOccurs="0">
				<annotation>
					<documentation>Lieux d'execution de la consultation</documentation>
				</annotation>
			</element>
			<element name="referenceExterne" type="string">
				<annotation>
					<documentation>Référence externe de la consultation
					</documentation>
				</annotation>
			</element>
			<element name="intitule" type="string">
				<annotation>
					<documentation>Intitulé de la consultation</documentation>
				</annotation>
			</element>
			<element name="objet" type="string">
				<annotation>
					<documentation>Objet de la consultation</documentation>
				</annotation>
			</element>
			<element name="typeContrat" type="atexo:TypeContratType">
				<annotation>
					<documentation>Type de contrat de la consultation</documentation>
				</annotation>
			</element>
			<element name="typeProcedure" type="string">
				<annotation>
					<documentation>Type de procédure de passation</documentation>
				</annotation>
			</element>
			<element name="dateRemisePlis" type="dateTime">
				<annotation>
					<documentation>Date et heure de remise des plis</documentation>
				</annotation>
			</element>
			<element name="dateMiseLigne" type="dateTime" minOccurs="0">
				<annotation>
					<documentation>Date et heure de la mise en ligne</documentation>
				</annotation>
			</element>
			<element name="procedure" type="atexo:ProcedureType" minOccurs="0">
				<annotation>
					<documentation>Procédure de la consultation</documentation>
				</annotation>
			</element>
			<element name="code_externe_procedure" type="string" minOccurs="0">
				<annotation>
					<documentation>Code externe procédure</documentation>
				</annotation>
			</element>
			<element name="reponseElectronique" type="string">
				<annotation>
					<documentation>True si la réponse est éléctronique</documentation>
				</annotation>
			</element>
			<element name="signatureElectronique" type="short">
				<annotation>
					<documentation>0 = signature électronique non requise, 1 = requise, 2 = autorisée
					</documentation>
				</annotation>
			</element>
			<element name="chiffrementPlis" type="boolean">
				<annotation>
					<documentation>True si chiffrement de plis.</documentation>
				</annotation>
			</element>
			<element name="enveloppeUnique" type="boolean">
				<annotation>
					<documentation>True si l'enveloppe est unique</documentation>
				</annotation>
			</element>
			<element name="naturePrestation" type="atexo:naturePrestation"
			>
				<annotation>
					<documentation>Code externe du type nature des prestations
						permettant de faire le lien entre la nature de prestation RSEM et
						de l’application appelante.
					</documentation>
				</annotation>
			</element>
			
			<element name="libelle_long_pouvoir_adjudicateur" type="string" >
				<annotation>
					<documentation>Libelle long du pouvoir adjudicateur</documentation>
				</annotation>
			</element>
			<element name="pouvoirAdjudicateur" type="string" >
				<annotation>
					<documentation>Code externe du type de pouvoir adjudicateur
						permettant de faire le lien entre la nature de prestation RSEM et
						de l’application appelante
					</documentation>
				</annotation>
			</element>
			<element name="article" type="string" >
				<annotation>
					<documentation>L'article de la consultation</documentation>
				</annotation>
			</element>
			<element name="directionService" type="string" >
				<annotation>
					<documentation>La direction service de la consultation
					</documentation>
				</annotation>
			</element>
			<choice>
				<sequence>
					<element name="lots" type="atexo:listLotType">
						<annotation>
							<documentation>Liste de lots de la consultation</documentation>
						</annotation>
					</element>
				</sequence>
				<sequence>
					<element name="informationsComplementaires"
							 type="atexo:InformationsCommunesLotEtConsultationType"/>
				</sequence>
			</choice>
			<element name="pairesType" type="atexo:listPaireType"
					 minOccurs="0">
				<annotation>
					<documentation>Champ gérant les propriété de la consultation non
						definie dans RSEM
					</documentation>
				</annotation>
			</element>
			<element name="pairesTypeComplexe" type="atexo:listPaireTypeComplexe"
					 minOccurs="0">
				<annotation>
					<documentation>Liste des paires type complexe</documentation>
				</annotation>
			</element>
			<element name="statut">
				<annotation>
					<documentation>Statut de la consultation</documentation>
				</annotation>
				<simpleType>
					<annotation>
						<documentation>Référentiel pour la forme attendue du groupement
							attributaire
						</documentation>
					</annotation>
					<restriction base="NMTOKEN">
						<enumeration value="0">
							<annotation>
								<documentation>Desactivé</documentation>
							</annotation>
						</enumeration>
						<enumeration value="1">
							<annotation>
								<documentation>Définition</documentation>
							</annotation>
						</enumeration>
						<enumeration value="2">
							<annotation>
								<documentation>Publicité</documentation>
							</annotation>
						</enumeration>
						<enumeration value="3">
							<annotation>
								<documentation>Consultation</documentation>
							</annotation>
						</enumeration>
						<enumeration value="4">
							<annotation>
								<documentation>Dépouillement</documentation>
							</annotation>
						</enumeration>
						<enumeration value="5">
							<annotation>
								<documentation>Attribution</documentation>
							</annotation>
						</enumeration>
						<enumeration value="6">
							<annotation>
								<documentation>Exécution</documentation>
							</annotation>
						</enumeration>
						<enumeration value="7">
							<annotation>
								<documentation>Clos</documentation>
							</annotation>
						</enumeration>
						<enumeration value="8">
							<annotation>
								<documentation>Sans suite</documentation>
							</annotation>
						</enumeration>
						<enumeration value="9">
							<annotation>
								<documentation>Infructueux</documentation>
							</annotation>
						</enumeration>
					</restriction>
				</simpleType>
			</element>
			<element name="transverse" type="boolean">
				<annotation>
					<documentation>Détermine si la consultation est transverse ou pas
					</documentation>
				</annotation>
			</element>
			<element name="atelierProteges" type="boolean">
				<annotation>
					<documentation>True, si au moins un lot du marché est réservé à
						des ateliers protégés.
					</documentation>
				</annotation>
			</element>
			<element name="emploisProteges" type="boolean">
				<annotation>
					<documentation>True, si au moins un lot du marché est réservé à
						des emplois protégés.
					</documentation>
				</annotation>
			</element>
			<element name="formeGroupementAttributaire">
				<annotation>
					<documentation>Code externe du type de forme de groupement
						attributaire permettant de faire le lien entre la nature de
						prestation RSEM et de l’application appelante
					</documentation>
				</annotation>
				<simpleType>
					<annotation>
						<documentation>Référentiel pour la forme attendue du groupement
							attributaire
						</documentation>
					</annotation>
					<restriction base="NMTOKEN">
						<enumeration value="AU_CHOIX"/>
						<enumeration value="CONJOINT"/>
						<enumeration value="SOLIDAIRE"/>
					</restriction>
				</simpleType>
			</element>
			<element name="dureeMarche" type="atexo:DureeMarcheType"/>
			<element name="dureeValiditeOffreEnJour" type="atexo:CustomTypeShortMax999">
				<annotation>
					<documentation>La durée de validité de l’offre en Jours.
					</documentation>
				</annotation>
			</element>
			<element name="criteresAttribution" type="atexo:CriteresAttributionType"/>
			<element name="nombreCandidats" type="atexo:NombreCandidatsType"/>
			<element name="clausesEnvironnementales" type="atexo:ClausesEnvironnementalesType" />
			<element name="clausesSociales" type="atexo:ClausesSocialesType"/>
			<element name="listeCodeCPV" type="atexo:ListeCPVType">
				<annotation>
					<documentation>Liste de codes et libellés CPV</documentation>
				</annotation>
			</element>
			<element name="compatibleEntiteAdjudicatrice" type="boolean">
				<annotation>
					<documentation>True si la consultation est compatible entité
						adjudicatrice
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
	<complexType name="ProcedureType">
		<sequence>
			<element name="codeExterne" type="string"/>
			<element name="libelleCourt" type="string"/>
			<element name="libelleLong" type="string"/>
			<element name="type" type="string"/>
		</sequence>
	</complexType>
	<complexType name="LieuExecutionsType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="lieuExecution" type="string"/>
		</sequence>
	</complexType>
	<complexType name="listLotType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="lot" type="atexo:LotType"/>
		</sequence>
	</complexType>
	<complexType name="TypeContratType">
		<sequence>
			<element name="id" type="int"/>
			<element name="libelle" type="string"/>
			<element name="abreviation" type="string"/>
			<element name="code_externe" type="string" minOccurs="0"/>
		</sequence>
	</complexType>
	<complexType name="LotType">
		<sequence>
			<element name="numeroLot" type="string">
				<annotation>
					<documentation>Description du lot</documentation>
				</annotation>
			</element>
			<element name="description" type="string">
				<annotation>
					<documentation>Description du lot</documentation>
				</annotation>
			</element>
			<element name="intitule" type="string">
				<annotation>
					<documentation>Intitulé du lot</documentation>
				</annotation>
			</element>
			<element name="naturePrestation" type="atexo:naturePrestation">
				<annotation>
					<documentation>Code externe du type nature des prestations
						permettant de faire le lien entre la nature de prestation RSEM et
						de l’application appelante
					</documentation>
				</annotation>
			</element>
			<element name="pairesType" type="atexo:listPaireType"
					 minOccurs="0">
				<annotation>
					<documentation>Champ gérant les propriété de la consultation non
						definie dans RSEM
					</documentation>
				</annotation>
			</element>
			<element name="pairesTypeComplexe" type="atexo:listPaireTypeComplexe"
					 minOccurs="0"/>
			<element name="dureeMarche" type="atexo:DureeMarcheType"/>
			<element name="informationsComplementaires"
					 type="atexo:InformationsCommunesLotEtConsultationType"/>
			<element name="clausesEnvironnementales" type="atexo:ClausesEnvironnementalesType"
			>
				<annotation>
					<documentation>True, si marché à clauses environnementales.
					</documentation>
				</annotation>
			</element>
			<element name="clausesSociales" type="atexo:ClausesSocialesType"/>
			<element name="listeCodeCPV" type="atexo:ListeCPVType">
				<annotation>
					<documentation>Liste de codes et libellés CPV</documentation>
				</annotation>
			</element>
			<element name="criteresAttribution" type="atexo:CriteresAttributionType"/>
		</sequence>
	</complexType>
	<complexType name="listPaireTypeComplexe">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="paireTypeComplexe" type="atexo:listPaireType"/>
		</sequence>
	</complexType>
	<complexType name="listPaireType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="paireType" type="atexo:PaireType"/>
		</sequence>
		<attribute name="idObjetComplexe" type="string"/>
	</complexType>
	<complexType name="PaireType">
		<annotation>
			<documentation>Ce type permet de gerer les proprietes du formulaire
				amont non géré par RSEM.
			</documentation>
		</annotation>
		<sequence>
			<element name="cle" type="string"/>
			<element name="valeur" type="string"/>
		</sequence>
	</complexType>
	<complexType name="ListeDocumentsType">
		<sequence>
			<element name="document" minOccurs="0" maxOccurs="unbounded"/>
		</sequence>
	</complexType>
	<complexType name="documentType">
		<sequence>
			<choice maxOccurs="unbounded">
				<annotation>
					<documentation>Document associé a une redaction et redigé et
						generer à travers le module de redaction de RSEM
					</documentation>
				</annotation>
				<element name="nom" type="string"/>
				<element name="titre" type="string"/>
				<element name="identifiant" type="string" minOccurs="0"
				/>
				<element name="taille" type="integer"/>
				<element name="statut">
					<simpleType>
						<annotation>
							<documentation>Etat du document</documentation>
						</annotation>
						<restriction base="NMTOKEN">
							<enumeration value="VALIDER"/>
							<enumeration value="EN_ATTENTE_VALIDATION"/>
							<enumeration value="EN_COURS_REDACTION"/>
						</restriction>
					</simpleType>
				</element>
			</choice>
		</sequence>
	</complexType>
	<complexType name="DureeMarcheType">
		<sequence>
			<sequence>
				<element name="nbMois" type="atexo:CustomTypeShortMax999" minOccurs="0">
					<annotation>
						<documentation>Nombre de mois, en cas de durée du marché
						</documentation>
					</annotation>
				</element>
				<element name="nbJours" type="atexo:CustomTypeShortMax999" minOccurs="0">
					<annotation>
						<documentation>Nombre de jours, en cas de durée du marché
						</documentation>
					</annotation>
				</element>
				<!-- en nombre de mois et/ou jours à compter de la notification du marché -->
			</sequence>
			<sequence>
				<annotation>
					<documentation>Dates de début et de fin</documentation>
				</annotation>
				<element name="dateACompterDu" type="dateTime" minOccurs="0">
					<annotation>
						<documentation>Date de début, en cas de délai d’exécution des
							prestations
						</documentation>
					</annotation>
				</element>
				<element name="dateJusquau" type="dateTime" minOccurs="0">
					<annotation>
						<documentation>Date de fin, en cas de délai d’exécution des
							prestations
						</documentation>
					</annotation>
				</element>
			</sequence>
			<element name="descriptionLibre" type="string" minOccurs="0">
				<annotation>
					<documentation>Description livre du délai de la durée
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
	<complexType name="ClausesEnvironnementalesType">
		<sequence>
			<element name="specificationTechnique" type="boolean">
				<annotation>
					<documentation>Spécifications techniques</documentation>
				</annotation>
			</element>
			<element name="conditionExecution" type="boolean">
				<annotation>
					<documentation>Conditions d'execution</documentation>
				</annotation>
			</element>
			<element name="critereAttribution" type="boolean">
				<annotation>
					<documentation>critères d'attribution</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
	<complexType name="CriteresAttributionType">
		<annotation>
			<documentation>Critères de jugement des offres
				Critères énoncés dans
				le cahier des charges
				Liste des critères énoncés par ordre de
				priorité décroissante (ordre 1
				est le plus prioritaire)
			</documentation>
		</annotation>
		<sequence>
			<choice>
				<element name="critereCDC" type="string">
					<annotation>
						<documentation>Critères énoncés dans le cahier des charges. si null = non, si != null -> oui
						</documentation>
					</annotation>
				</element>
				<element name="criterePrix" type="string">
					<annotation>
						<documentation>Offre économiquement la pus avantageuse appréciée en
							fonction du critère unique du prix le plus bas
						</documentation>
					</annotation>
				</element>
				<element name="listeCritereLibre" type="atexo:ListeCritereLibreType" minOccurs="0">
					<annotation>
						<documentation>Offre économiquement la pus avantageuse appréciée en
							fonction des critères énoncés par ordre de priorité décroissante (
							prorite = 1 à la priorité la plus grande)
						</documentation>
					</annotation>
				</element>
				<element name="listeCriterePondere" type="atexo:ListeCriterePondereType" minOccurs="0">
					<annotation>
						<documentation>Offre économiquement la pus avantageuse appréciée en
							fonction des critères énoncés avec leur ponderation
						</documentation>
					</annotation>
				</element>
				<element name="coutglobal" type="string">
					<annotation>
						<documentation>Critère unique du coût déterminé selon une approche globale</documentation>
					</annotation>
				</element>
			</choice>
		</sequence>
	</complexType>
	<complexType name="ListeCritereLibreType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="critereLibre" type="atexo:CritereLibreType"/>
		</sequence>
	</complexType>
	<complexType name="CritereLibreType">
		<choice>
			<element name="enonce" type="string">
				<annotation>
					<documentation>Libellé du critère</documentation>
				</annotation>
			</element>
			<element name="listeSousCriteresAttribution" type="atexo:ListeSousCriteresAttributionType" minOccurs="0">
			</element>
		</choice>
	</complexType>
	<complexType name="ListeCriterePondereType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="criterePondere" type="atexo:CriterePondereType"/>
		</sequence>
	</complexType>
	<complexType name="CriterePondereType">
		<choice>
			<element name="pourcentagePonderation" type="atexo:CustomTypeShortMax99">
				<annotation>
					<documentation>Pourcentage à appliquer au critère</documentation>
				</annotation>
			</element>
			<element name="enonce" type="string">
				<annotation>
					<documentation>Libellé du critère</documentation>
				</annotation>
			</element>
			<element name="listeSousCriteresAttribution" type="atexo:ListeSousCriteresAttributionType" minOccurs="0">
			</element>
		</choice>
	</complexType>
	<complexType name="ListeSousCriteresAttributionType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="sousCriteresAttribution" type="atexo:SousCriteresAttributionType"/>
		</sequence>
	</complexType>
	<complexType name="SousCriteresAttributionType">
		<annotation>
			<documentation>Sous critère contenant énonce et pondération en
				cas
				d'un critère liCritères de jugement des offres
				Critères
				énoncés dans
				le cahier des charges
				Liste des critères énoncés par
				ordre de priorité
				décroissante (ordre
				1 est le plus prioritaire)
			</documentation>
		</annotation>
		<choice>
			<element name="pourcentagePonderation" type="atexo:CustomTypeShortMax99">
				<annotation>
					<documentation>Pourcentage à appliquer au critère</documentation>
				</annotation>
			</element>
			<element name="critere" type="string">
				<annotation>
					<documentation>Libellé du critère pondéré</documentation>
				</annotation>
			</element>
		</choice>
	</complexType>
	<complexType name="NombreCandidatsType">
		<annotation>
			<documentation>Nombre de candidats admis à présenter une offre
				nombreMax doit être supérieur ou égal à nombreMin
			</documentation>
		</annotation>
		<sequence>
			<choice>
				<sequence>
					
					<element name="nombreMax" type="atexo:CustomTypeShortMax999" minOccurs="0">
						<annotation>
							<documentation>Nombre maximal de candidats admis à présenter une
								offre
							</documentation>
						</annotation>
					</element>
					<element name="nombreMin" type="atexo:CustomTypeShortMax999" minOccurs="0">
						<annotation>
							<documentation>Nombre minimal de candidats admis à présenter une
								offre
							</documentation>
						</annotation>
					</element>
				
				</sequence>
				<element name="nombreFixe" type="atexo:CustomTypeShortMax999" minOccurs="0">
					<annotation>
						<documentation>Nombre fixe de candidats admis à présenter une
							offre
						</documentation>
					</annotation>
				</element>
			</choice>
			<element name="reductionProgressive" type="string" minOccurs="0">
				<annotation>
					<documentation>Procédure en phases successives afin de réduire le
						nombre d'offres à négocier. Si null -> non, si != null -> oui
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
	<simpleType name="SousClauseSociale1Type">
		<restriction base="NMTOKEN">
			<enumeration id="INSERTION_ACTIVITE_ECONOMIQUE" value="insertionActiviteEconomique"/>
			<enumeration id="CLAUSE_SOCIALE_FORMATION_SCOLAIRE" value="clauseSocialeFormationScolaire"/>
			<enumeration id="LUTTE_CONTRE_DISCRIMINATIONS" value="lutteContreDiscriminations"/>
			<enumeration id="COMMERCE_EQUITABLE" value="commerceEquitable"/>
			<enumeration id="ACHATS_ETHIQUES_TRACABILITE_SOCIALE" value="achatsEthiquesTracabiliteSociale"/>
			<enumeration id="AUTRE_CLAUSE_SOCIALE" value="autreClauseSociale"/>
		</restriction>
	</simpleType>
	<complexType name="MarchePublicType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="value" type="atexo:SousClauseSociale1Type"/>
		</sequence>
	</complexType>
	<complexType name="SousClauseSociale2Type">
		<sequence>
			<element name="entreprisesAdapteesEtablissementsServicesAide" type="boolean"/>
			<element name="structuresInsertionActiviteEconomique" type="boolean"/>
			<element name="entreprisesEconomieSocialeSolidaire" type="boolean"/>
		</sequence>
	</complexType>
	<complexType name="ClausesSocialesType">
		<sequence>
			<element name="nombreHeure" type="atexo:CustomTypeDecimal"/>
			<element name="pourcentage" type="atexo:CustomTypeShortMax99"/>
			<element name="marcheReserve" type="boolean"/>
			<element name="conditionExecution" type="boolean"/>
			<element name="critereAttribution" type="boolean"/>
			<element name="marchePublicClauseSocialeConditionExecution" type="atexo:MarchePublicType" minOccurs="0"/>
			<element name="marchPublicClauseSocialeSpecificationTechnique" type="atexo:MarchePublicType" minOccurs="0"/>
			<element name="marchePublicCritèreSocialCritereAttribution" type="atexo:MarchePublicType" minOccurs="0"/>
			<element name="marcheReserveADes" type="atexo:SousClauseSociale2Type" minOccurs="0"/>
			<element name="marchePublicObjetEstInsertion" type="boolean"/>
		</sequence>
	</complexType>
	<complexType name="FormeDePrixType">
		<choice>
			<sequence>
				<element name="formeDePrixForfaitaire" type="atexo:FDPPartieForfaitaireType"/>
			</sequence>
			<sequence>
				<element name="formeDePrixUnitaire" type="atexo:FDPPartieUnitaireType"/>
			</sequence>
			<sequence>
				<element name="formeDePrixMixte" type="atexo:FDPMixteType"/>
			</sequence>
		</choice>
	</complexType>
	<complexType name="FDPMixteType">
		<sequence>
			<element name="partieForfaitaire" type="atexo:FDPPartieForfaitaireType"/>
			<element name="partieUnitaire" type="atexo:FDPPartieUnitaireType"/>
		</sequence>
	</complexType>
	<complexType name="FDPPartieUnitaireType">
		<sequence>
			<element name="partieForfaitaire" type="atexo:FDPPartieForfaitaireType"/>
			<choice>
				<sequence>
					<element name="bonDeCommande" type="atexo:BonDeCommandeType"/>
				</sequence>
				<sequence>
					<element name="autresPrixUnitaires" type="boolean"/>
				</sequence>
			</choice>
			<element name="typePrix" type="atexo:TypePrixType"/>
		</sequence>
	</complexType>
	<complexType name="FDPPartieForfaitaireType">
		<sequence>
			<element name="estimationInterneHT" type="decimal"/>
			<element name="estimationInterneTTC" type="decimal"/>
			<element name="dateDeValeur" type="atexo:CustomTypeDate" minOccurs="0"/>
			<element name="variationsPrix" type="atexo:VariationPrixType">
				<annotation>
					<documentation>Variation du prix :
						- PRIX_ACTUALISABLES
						-
						PRIX_REVISABLES
						- PRIX_FERMES
					</documentation>
				</annotation>
			</element>
		</sequence>
	</complexType>
	<complexType name="VariationPrixType">
		<sequence>
			<element name="variationsPrix" maxOccurs="3">
				<simpleType>
					<annotation>
						<documentation>Variation du prix.</documentation>
					</annotation>
					<restriction base="NMTOKEN">
						<enumeration value="PRIX_ACTUALISABLES"/>
						<enumeration value="PRIX_REVISABLES"/>
						<enumeration value="PRIX_FERMES"/>
					</restriction>
				</simpleType>
			</element>
		</sequence>
	</complexType>
	<complexType name="TypePrixType">
		<sequence>
			<element name="typePrix" maxOccurs="3">
				<simpleType>
					<annotation>
						<documentation>Variation du prix.</documentation>
					</annotation>
					<restriction base="NMTOKEN">
						<enumeration value="PRIX_CATALOGUE"/>
						<enumeration value="BORDEREAU_PRIX"/>
						<enumeration value="AUTRE"/>
					</restriction>
				</simpleType>
			</element>
		</sequence>
	</complexType>
	<complexType name="BonDeCommandeType">
		<choice>
			<sequence>
				<element name="sansMiniMaxi" type="boolean"/>
			</sequence>
			<sequence>
				<element name="avecMiniMaxi" type="atexo:MiniMaxiType"/>
			</sequence>
		</choice>
	</complexType>
	<complexType name="MiniMaxiType">
		<sequence>
			<element name="mini" type="decimal"/>
			<element name="maxi" type="decimal"/>
		</sequence>
	</complexType>
	<complexType name="InformationsCommunesLotEtConsultationType">
		<sequence>
			<element name="ccag" type="string">
				<annotation>
					<documentation>CCAG de référence</documentation>
				</annotation>
			</element>
			<element name="droitProprieteIntellectuelle" minOccurs="0">
				<annotation>
					<documentation>
						- Option A – Concession des droits d’utilisation
						- Option B – Cession des droits d’exploitation
						- Dérogation au CCAG (OPTION-DEROG)
					</documentation>
				</annotation>
				<simpleType>
					<restriction base="NMTOKEN">
						<enumeration value="OPTION-A"/>
						<enumeration value="OPTION-B"/>
						<enumeration value="OPTION-DEROG"/>
					</restriction>
				</simpleType>
			</element>
			<element name="varianteAutorisee" type="boolean"/>
			<element name="variantesExigees" type="boolean"/>
			<element name="variantesTechniquesObligatoires" type="string"
					 minOccurs="0"/>
			<element name="marcheReconductible" type="atexo:MarcheReconductibleType"
					 minOccurs="0"/>
			<element name="lotsTechniques" type="atexo:ListeLotTechniqueType"
					 minOccurs="0"/>
			<element name="structuresSocialeReserves" type="atexo:StructuresSocialeReserveType"
					 minOccurs="0"/>
			
			<sequence>
				<element name="formeDePrix" type="atexo:FormeDePrixType"/>
			</sequence>
			<sequence>
				<element name="tranches" type="atexo:ListeTrancheType" minOccurs="0"/>
			</sequence>
		
		</sequence>
	</complexType>
	<complexType name="MarcheReconductibleType">
		<sequence>
			<element name="nombreDeReconduction" type="atexo:CustomTypeShortMax99"/>
			<element name="modalitesDeReconduction" type="string"/>
		</sequence>
	</complexType>
	<complexType name="StructuresSocialeReserveType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="structureSocialeReserves" type="string"/>
		</sequence>
	</complexType>
	<complexType name="ListeLotTechniqueType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="lotTechnique" type="atexo:LotTechniqueType"/>
		</sequence>
	</complexType>
	<complexType name="LotTechniqueType">
		<sequence>
			<element name="identifiant" type="atexo:CustomTypeAlphanumerique">
				<annotation>
					<documentation>Identifiant du lot technique</documentation>
				</annotation>
			</element>
			<element name="intitule" type="string">
				<annotation>
					<documentation>Intitulé du lot technique</documentation>
				</annotation>
			</element>
			<element name="lotPrincipal" type="boolean">
				<annotation>
					<documentation>True si c'est le lot principal</documentation>
				</annotation>
			</element>
			<sequence>
				<element name="tranches" type="atexo:ListCodeTrancheType">
					<annotation>
						<documentation>Liste des identifiants des tranches associés au
							lot technique
						</documentation>
					</annotation>
				</element>
			</sequence>
		</sequence>
	</complexType>
	<complexType name="ListeTrancheType">
		<sequence>
			<sequence maxOccurs="unbounded">
				<element name="tranchesCondionnelle" type="atexo:TrancheConditionnelleType"/>
			</sequence>
			<element name="trancheFixe" type="atexo:TrancheFixeType"/>
		</sequence>
	</complexType>
	<complexType name="TrancheConditionnelleType">
		<sequence>
			<element name="identifiant" type="atexo:CustomTypeShortMax99"/>
			<element name="intitule" type="string"/>
			<element name="formeDePrix" type="atexo:FormeDePrixType"/>
		</sequence>
	</complexType>
	<complexType name="TrancheFixeType">
		<sequence>
			<element name="intitule" type="string"/>
			<element name="formeDePrix" type="atexo:FormeDePrixType"/>
		</sequence>
	</complexType>
	<complexType name="ListeCPVType">
		<sequence>
			<sequence maxOccurs="unbounded">
				<element name="cpv" type="atexo:CPVType"/>
			</sequence>
		</sequence>
	</complexType>
	<complexType name="CPVType">
		<sequence>
			<element name="codeCPV" type="string"/>
			<element name="libelleCPV" type="string"/>
			<element name="principal" type="boolean"/>
		</sequence>
	</complexType>
	<complexType name="ListCodeTrancheType">
		<sequence minOccurs="0" maxOccurs="unbounded">
			<element name="codeTranche" type="string"/>
		</sequence>
	</complexType>
	<simpleType name="naturePrestation">
		<annotation>
			<documentation>Code externe du type nature des prestations permettant
				de faire le lien entre la nature de prestation RSEM et de
				l’application appelante
			</documentation>
		</annotation>
		<restriction base="NMTOKEN">
			<enumeration value="T">
				<annotation>
					<documentation>Travaux</documentation>
				</annotation>
			</enumeration>
			<enumeration value="F">
				<annotation>
					<documentation>Fournitures</documentation>
				</annotation>
			</enumeration>
			<enumeration value="S">
				<annotation>
					<documentation>Services</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>
</schema>
