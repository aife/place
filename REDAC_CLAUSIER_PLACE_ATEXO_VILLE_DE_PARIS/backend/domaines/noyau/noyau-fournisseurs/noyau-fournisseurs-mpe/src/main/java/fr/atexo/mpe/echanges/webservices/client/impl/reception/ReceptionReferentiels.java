package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;
import fr.atexo.mpe.echanges.webservices.client.jaxb.ReferentielsType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReceptionReferentiels {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour appeler le WS MPE
     */
    private final BaseReception reception;

    public ReceptionReferentiels(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger MPE et récupérer  les referentiels.
     * @return la liste de tous les organismes.
     */
    public ReferentielsType recupererListeReferentiels(final String pathWs) throws ClientMpeException {
        logger.info("Début de la récupération de la liste des referentiels mpe. Url = {}", pathWs);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs);

        logger.info("Fin de la récupération de la liste des referentiels.\n");

        return reponseDemat.getReponse().getReferentiels();
    }

}
