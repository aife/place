package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>Java class for DepotType complex type.* <p>The following schema fragment specifies the expected content contained within this class.* <pre>
 * &lt;complexType name="DepotType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="contact" type="{http://www.atexo.com/epm/xml}ContactType"/>
 *         &lt;element name="entreprise" type="{http://www.atexo.com/epm/xml}EntrepriseType"/>
 *         &lt;element name="enveloppes" type="{http://www.atexo.com/epm/xml}EnveloppesType"/>
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="nombrePlis" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="marchePublicSimplifie" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="statutOffres" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>* 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepotType", propOrder = {

})
public class DepotType {

    protected int id;
    @XmlElement(required = true)
    protected String numero;
    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String reponseAnnonceBase64;
    @XmlElement(required = true)
    protected ContactType contact;
    @XmlElement(required = true)
    protected EntrepriseType entreprise;
    @XmlElement(required = true)
    protected EnveloppesType enveloppes;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar date;
    protected int nombrePlis;
    protected Boolean marchePublicSimplifie;
    protected String statutOffres;

    /**
     * Gets the value of the id property.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the numero property.
     * @return possible object is {@link String }
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Sets the value of the numero property.
     * @param value allowed object is {@link String }
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the type property.
     * @return possible object is {@link String }
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * @param value allowed object is {@link String }
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the contact property.
     * @return possible object is {@link ContactType }
     */
    public ContactType getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * @param value allowed object is {@link ContactType }
     */
    public void setContact(ContactType value) {
        this.contact = value;
    }

    /**
     * Gets the value of the entreprise property.
     * @return possible object is {@link EntrepriseType }
     */
    public EntrepriseType getEntreprise() {
        return entreprise;
    }

    /**
     * Sets the value of the entreprise property.
     * @param value allowed object is {@link EntrepriseType }
     */
    public void setEntreprise(EntrepriseType value) {
        this.entreprise = value;
    }

    /**
     * Gets the value of the enveloppes property.
     * @return possible object is {@link EnveloppesType }
     */
    public EnveloppesType getEnveloppes() {
        return enveloppes;
    }

    /**
     * Sets the value of the enveloppes property.
     * @param value allowed object is {@link EnveloppesType }
     */
    public void setEnveloppes(EnveloppesType value) {
        this.enveloppes = value;
    }

    /**
     * Gets the value of the date property.
     * @return possible object is {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * @param value allowed object is {@link XMLGregorianCalendar }
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Gets the value of the nombrePlis property.
     */
    public int getNombrePlis() {
        return nombrePlis;
    }

    /**
     * Sets the value of the nombrePlis property.
     */
    public void setNombrePlis(int value) {
        this.nombrePlis = value;
    }

    /**
     * Gets the value of the marchePublicSimplifie property.
     * @return possible object is {@link Boolean }
     */
    public Boolean getMarchePublicSimplifie() {
        return marchePublicSimplifie;
    }

    /**
     * Sets the value of the marchePublicSimplifie property.
     * @param value allowed object is {@link Boolean }
     */
    public void setMarchePublicSimplifie(Boolean value) {
        this.marchePublicSimplifie = value;
    }

    public String getReponseAnnonceBase64() {
        return reponseAnnonceBase64;
    }

    public void setReponseAnnonceBase64(String reponseAnnonceBase64) {
        this.reponseAnnonceBase64 = reponseAnnonceBase64;
    }

    public String getStatutOffres() {
        return statutOffres;
    }

    public void setStatutOffres(String statutOffres) {
        this.statutOffres = statutOffres;
    }

}