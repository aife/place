
package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reponse"
})
@XmlRootElement(name = "mpe", namespace = "http://www.atexo.com/epm/xml")
public class MpeReferentiel {

    @XmlElement(namespace = "http://www.atexo.com/epm/xml")
    protected MpeReferentiel.Reponse reponse;

    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "messageErreur",
        "message",
        "referentiels"
    })
    public static class Reponse {

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected String messageErreur;
        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected String message;

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected ReferentielsType referentiels;

        /**
         * Obtient la valeur de la propriété message.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessage() {
            return message;
        }

        /**
         * Définit la valeur de la propriété message.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessage(String value) {
            this.message = value;
        }

        /**
         * Obtient la valeur de la propriété referentiels.
         * 
         * @return
         *     possible object is
         *     {@link ReferentielsType }
         *     
         */
        public ReferentielsType getReferentiels() {
            return referentiels;
        }

        /**
         * Définit la valeur de la propriété referentiels.
         * 
         * @param value
         *     allowed object is
         *     {@link ReferentielsType }
         *     
         */
        public void setReferentiels(ReferentielsType value) {
            this.referentiels = value;
        }

    }

    public Reponse getReponse() {
        return reponse;
    }

    public void setReponse(Reponse reponse) {
        this.reponse = reponse;
    }
}
