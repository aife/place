package fr.atexo.mpe.echanges.webservices.client.beans;

import fr.atexo.mpe.echanges.webservices.client.jaxb.OrganismeType;

import java.io.File;

/**
 * Le bean présentant l'organisme MPE. Il contient l'objet Jaxb et le fichier logo.
 * @author Xuesong
 */
public class OrganismeAvecLogoBean {

    /**
     * objet organisme reçu du MPE
     */
    private OrganismeType OrganismeMpe;

    /**
     * fichier logo pour cet organisme
     */
    private File fichierLogo;

    public OrganismeType getOrganismeMpe() {
        return OrganismeMpe;
    }

    public void setOrganismeMpe(OrganismeType valeur) {
        OrganismeMpe = valeur;
    }

    public File getFichierLogo() {
        return fichierLogo;
    }

    public void setFichierLogo(File valeur) {
        this.fichierLogo = valeur;
    }

}
