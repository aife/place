package fr.atexo.mpe.echanges.webservices.client.impl;

import fr.atexo.mpe.echanges.webservices.client.ClientMpe;
import fr.atexo.mpe.echanges.webservices.client.beans.OrganismeAvecLogoBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.reception.*;
import fr.atexo.mpe.echanges.webservices.client.jaxb.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * La classe qui implemente l'interface {@link ClientMpe}. Elle est la façade pour effectuer tous les types d'appels du webservices demat.
 * @author Xuesong
 */
public class ClientMpeImpl implements ClientMpe {

    private static final Logger logger = LoggerFactory.getLogger(ClientMpeImpl.class);

    /**
     * Chemin des fichiers temporaires
     */
    private String cheminFichierTemporaire;

	/**
	 * L'URL du WS d'authentification
	 */
	private String urlAuthentification;

	/**
	 * L'URL du WS des procedures et contrats
	 */
	private String urlReferentiels;

	/**
	 * L'URL du WS des organismes
	 */
	private String urlOrganismes;

	/**
	 * L'URL du WS d'un organisme
	 */
	private String urlOrganisme;

    /**
     * login pour l'accès au web service de la pf demat
     */
    private String login;

    /**
     * mot de passe pour l'accès au web service de la pf demat
     */
    private String password;

    @Override
    public List<OrganismeType> recupererListeOrganismes() throws ClientMpeException {
        ReceptionListeOrganismes receptionListeAcronymeOrganismes = new ReceptionListeOrganismes(creerBaseReception());
        return receptionListeAcronymeOrganismes.recupererListeOrganismes(urlOrganismes);
    }

    @Override
    public OrganismeAvecLogoBean recupererOrganisme(String acronymeOrganisme) throws ClientMpeException {
        ReceptionOrganisme receptionOrganisme = new ReceptionOrganisme(creerBaseReception());
        return receptionOrganisme.recupererOrganisme(urlOrganisme, acronymeOrganisme);
    }

    @Override
    public ReferentielsType recupererReferentielsMpe() throws ClientMpeException {
        ReceptionReferentiels receptionReferentiels = new ReceptionReferentiels(creerBaseReception());
        return receptionReferentiels.recupererListeReferentiels(urlReferentiels);
    }

    private BaseReception creerBaseReception() {
        verifierStatutConfig();
        return new BaseReception(urlAuthentification, login, password, cheminFichierTemporaire);
    }

    private void verifierStatutConfig() {
    	if ( login == null || login.isEmpty())
            throw new IllegalArgumentException("Le login pour l'accès au web service de la pf demat n'est pas paramétré");
        if ( password == null || password.isEmpty())
            throw new IllegalArgumentException("Le mot de passe pour l'accès au web service de la pf demat n'est pas paramétré");
        if (cheminFichierTemporaire == null || cheminFichierTemporaire.isEmpty())
            throw new IllegalArgumentException("Le chemin des fichiers temporaires n'est pas paramétré");
    }

    /**
     * Paramétrer le chemin des fichiers temporaires. Le path doit finir par un "/").
     * Les fichieres temporaires générés sont sauvegardés dans ce path.
     * @param cheminFichierTemporaire Chemin des fichiers temporaires
     */
    public void setCheminFichierTemporaire(final String cheminFichierTemporaire) {
        this.cheminFichierTemporaire = cheminFichierTemporaire;
    }

	public void setUrlAuthentification( String urlAuthentification ) {
		this.urlAuthentification = urlAuthentification;
	}


    public void setLogin( final String login ) {
        this.login = login;
    }

    public void setPassword( final String password ) {
        this.password = password;
    }

	public void setUrlReferentiels( String urlReferentiels ) {
		this.urlReferentiels = urlReferentiels;
	}

	public void setUrlOrganismes( String urlOrganismes ) {
		this.urlOrganismes = urlOrganismes;
	}

	public void setUrlOrganisme( String urlOrganisme ) {
		this.urlOrganisme = urlOrganisme;
	}
}
