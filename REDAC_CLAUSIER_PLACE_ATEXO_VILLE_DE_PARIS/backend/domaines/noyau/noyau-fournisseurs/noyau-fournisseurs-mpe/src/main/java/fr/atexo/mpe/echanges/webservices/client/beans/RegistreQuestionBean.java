package fr.atexo.mpe.echanges.webservices.client.beans;

import fr.atexo.mpe.echanges.webservices.client.jaxb.QuestionType;

import java.io.File;

/**
 * Le bean présentant le registre de question/réponse. Il contient l'objet mapping Jaxb
 * pour le registre de question retourné par Demat et le fichier joint.
 * @author Xuesong
 */
public class RegistreQuestionBean {

    /**
     * objet registre question reçu du demat
     */
    private QuestionType questionDemat;

    /**
     * fichier accompagné avec le registre
     */
    private File fichierRegistreQuestion;

    public QuestionType getQuestionDemat() {
        return questionDemat;
    }

    public void setQuestionDemat(QuestionType valeur) {
        this.questionDemat = valeur;
    }

    public File getFichierRegistreQuestion() {
        return fichierRegistreQuestion;
    }

    public void setFichierRegistreQuestion(File valeur) {
        this.fichierRegistreQuestion = valeur;
    }

}
