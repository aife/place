package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;
import fr.atexo.mpe.echanges.webservices.client.jaxb.Mpe;
import fr.atexo.mpe.echanges.webservices.client.jaxb.OrganismeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe qui permet de récupérer les organismes
 * @author Xuesong GAO
 */
public class ReceptionListeOrganismes {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour appeler le WS MPE
     */
    private final BaseReception reception;

    public ReceptionListeOrganismes(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger MPE et récupérer tous les organismes.
     * @return la liste de tous les organismes.
     */
    public List<OrganismeType> recupererListeOrganismes(final String url) throws ClientMpeException {
        logger.info("Débuter à récupérer la liste des organisme");

        ReponseDematBean reponseDemat = reception.recuperer(url, "withLogo=true");

        List<OrganismeType> listeOrganismes = new ArrayList<OrganismeType>();
        Mpe.Reponse.Organismes organismes = reponseDemat.getReponse().getOrganismes();
        if (organismes != null)
            listeOrganismes = organismes.getOrganisme();

        logger.info("La récupération de la liste des organisme est terminée.\n");
        return listeOrganismes;
    }

}
