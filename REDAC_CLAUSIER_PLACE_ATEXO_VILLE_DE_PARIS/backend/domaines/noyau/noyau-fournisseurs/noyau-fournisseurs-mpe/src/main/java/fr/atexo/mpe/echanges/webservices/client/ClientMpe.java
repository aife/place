package fr.atexo.mpe.echanges.webservices.client;

import fr.atexo.mpe.echanges.webservices.client.beans.BlocPliBean;
import fr.atexo.mpe.echanges.webservices.client.beans.DocumentMpsBean;
import fr.atexo.mpe.echanges.webservices.client.beans.OrganismeAvecLogoBean;
import fr.atexo.mpe.echanges.webservices.client.beans.RegistreQuestionBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.jaxb.*;

import java.io.File;
import java.util.List;

/**
 * L'interface représentant tous les services fournis par webservices demat.
 * 
 * @author Xuesong GAO
 */
public interface ClientMpe {

	/**
     * Méthode pour interroger MPE et récupérer tous les organismes.
	 * @return la liste de tous les organismes MPE
	 * @throws ClientMpeException si erreur coté MPE
	 */
    List<OrganismeType> recupererListeOrganismes() throws ClientMpeException;
	
    /**
     * Méthode pour interroger MPE et récupérer les données d'un organisme.
     * @param acronymeOrganisme pour préciser quel organisme à récupérer
     * @return les données d'un organisme, y compris le fichier logo
     * @throws ClientMpeException si erreur coté MPE
     */
    OrganismeAvecLogoBean recupererOrganisme(String acronymeOrganisme) throws ClientMpeException;

    /**
     * methode pour interroger MPE et recuperer
     * @return les données referentiels
     */
    ReferentielsType recupererReferentielsMpe()throws ClientMpeException;


}
