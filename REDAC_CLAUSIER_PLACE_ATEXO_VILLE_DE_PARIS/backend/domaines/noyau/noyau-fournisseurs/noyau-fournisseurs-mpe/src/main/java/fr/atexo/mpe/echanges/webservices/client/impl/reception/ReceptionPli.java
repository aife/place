package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.beans.BlocPliBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * La classe qui permet de récupérer le registre pli
 * @author Xuesong GAO
 */
public class ReceptionPli {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    private static final String PREFIX_NOM_BLOC_PLI = "bloc_";

    /**
     * La classe basique qui implemente le processus commun pour récupérer le registre au demat
     */
    private final BaseReception reception;

    public ReceptionPli(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger MPE et récupérer une série des blocs fichiers
     * décomposés d'un fichier pli
     * @param idPli  identifiant pour préciser le pli demat
     * @return la liste de fihcier blocs. Ils sont dans l'ordre afin de pouvoir être recomposé
     */
    public List<BlocPliBean> recupererBlocsPlis(final int idPli, String pathWs) throws ClientMpeException {
        logger.info("Débuter à récupérer le pli demat, idPli: {}", idPli);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs + "/" + idPli);

        List<BlocPliBean> listeBlocPlis = new ArrayList<BlocPliBean>();
        List<File> fichiers = reponseDemat.getFichiers();

        int ordreBloc = 1;
        // la série des blocs du fichier du pli
        for (File fichier : fichiers) {
            BlocPliBean blocPliBean = new BlocPliBean();
            blocPliBean.setNomBloc(PREFIX_NOM_BLOC_PLI + ordreBloc++);
            blocPliBean.setFichierBloc(fichier);
            listeBlocPlis.add(blocPliBean);
        }
        logger.info("La récupération du pli demat est terminée.\n");
        return listeBlocPlis;
    }
}
