
package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour ContratType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ContratType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codeExterne" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="libelle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="abreviation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="associationProcedures">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="associationProcedure" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element name="codeExterneProcedure" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContratType", namespace = "http://www.atexo.com/epm/xml", propOrder = {
    "codeExterne",
    "libelle",
    "abreviation",
    "associationProcedures"
})
public class ContratType {

    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String codeExterne;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String libelle;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String abreviation;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected ContratType.AssociationProcedures associationProcedures;

    /**
     * Obtient la valeur de la propriété codeExterne.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodeExterne() {
        return codeExterne;
    }

    /**
     * Définit la valeur de la propriété codeExterne.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodeExterne(String value) {
        this.codeExterne = value;
    }

    /**
     * Obtient la valeur de la propriété libelle.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Définit la valeur de la propriété libelle.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLibelle(String value) {
        this.libelle = value;
    }

    /**
     * Obtient la valeur de la propriété abreviation.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAbreviation() {
        return abreviation;
    }

    /**
     * Définit la valeur de la propriété abreviation.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAbreviation(String value) {
        this.abreviation = value;
    }

    /**
     * Obtient la valeur de la propriété associationProcedures.
     *
     * @return
     *     possible object is
     *     {@link ContratType.AssociationProcedures }
     *
     */
    public ContratType.AssociationProcedures getAssociationProcedures() {
        return associationProcedures;
    }

    /**
     * Définit la valeur de la propriété associationProcedures.
     *
     * @param value
     *     allowed object is
     *     {@link ContratType.AssociationProcedures }
     *
     */
    public void setAssociationProcedures(ContratType.AssociationProcedures value) {
        this.associationProcedures = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="associationProcedure" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="codeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element name="codeExterneProcedure" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "associationProcedure"
    })
    public static class AssociationProcedures {

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected List<AssociationProcedure> associationProcedure;

        /**
         * Gets the value of the associationProcedure property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the associationProcedure property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAssociationProcedure().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContratType.AssociationProcedures.AssociationProcedure }
         *
         *
         */
        public List<AssociationProcedure> getAssociationProcedure() {
            if (associationProcedure == null) {
                associationProcedure = new ArrayList<AssociationProcedure>();
            }
            return this.associationProcedure;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="codeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="codeExterneProcedure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
                "codeOrganisme",
                "codeExterneProcedure"
        })
        public static class AssociationProcedure {

            protected String codeOrganisme;
            protected String codeExterneProcedure;

            /**
             * Obtient la valeur de la propriété codeOrganisme.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCodeOrganisme() {
                return codeOrganisme;
            }

            /**
             * Définit la valeur de la propriété codeOrganisme.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCodeOrganisme(String value) {
                this.codeOrganisme = value;
            }

            /**
             * Obtient la valeur de la propriété codeExterneProcedure.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getCodeExterneProcedure() {
                return codeExterneProcedure;
            }

            /**
             * Définit la valeur de la propriété codeExterneProcedure.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setCodeExterneProcedure(String value) {
                this.codeExterneProcedure = value;
            }

        }

    }

	@Override
	public String toString() {
		return MessageFormat.format("''{0}''(code externe = {1}, abbreviation = {2})", this.getLibelle(), this.getCodeExterne(), this.getAbreviation());
	}
}
