//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.03.18 at 10:56:52 AM CET 
//


package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for EnveloppeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnveloppeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="typeEnveloppe">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *               &lt;enumeration value="CANDIDATURE"/>
 *               &lt;enumeration value="OFFRE"/>
 *               &lt;enumeration value="ANONYMAT"/>
 *               &lt;enumeration value="OFFRE_TECHNIQUE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="numeroLot" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plis" type="{http://www.atexo.com/epm/xml}PlisType"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnveloppeType", propOrder = {

})
public class EnveloppeType {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String typeEnveloppe;
    @XmlElement(required = true)
    protected String numeroLot;
    @XmlElement(required = true)
    protected PlisType plis;

    /**
     * Gets the value of the typeEnveloppe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeEnveloppe() {
        return typeEnveloppe;
    }

    /**
     * Sets the value of the typeEnveloppe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeEnveloppe(String value) {
        this.typeEnveloppe = value;
    }

    /**
     * Gets the value of the numeroLot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLot() {
        return numeroLot;
    }

    /**
     * Sets the value of the numeroLot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLot(String value) {
        this.numeroLot = value;
    }

    /**
     * Gets the value of the plis property.
     * 
     * @return
     *     possible object is
     *     {@link PlisType }
     *     
     */
    public PlisType getPlis() {
        return plis;
    }

    /**
     * Sets the value of the plis property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlisType }
     *     
     */
    public void setPlis(PlisType value) {
        this.plis = value;
    }

}
