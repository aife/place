package fr.atexo.mpe.echanges.webservices.client.impl.commun;

import fr.atexo.mpe.echanges.webservices.client.jaxb.Mpe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean pour interpréter les données reçues du demat
 * @author Xuesong GAO
 */
public class ReponseDematBean {

    /**
     * l'objet jaxb mapping xml contenant des informations de la réponse
     */
    private Mpe.Reponse reponse;

    /**
     * les fichiers dans la réponse s'ils existent
     */
    private List<File> fichiers;

    /**
     * @return les fichiers dans la réponse s'ils existent, ils sont dans
     * l'ordre comme ils sont dans la requête http
     */
    public List<File> getFichiers() {
        if (fichiers == null)
            fichiers = new ArrayList<File>(); // on doit garder l'ordre des fichiers comme ils sont dans la requête http
        return this.fichiers;
    }

    /**
     * @return l'objet jaxb mapping xml contenant des informations de la réponse mpe
     */
    public Mpe.Reponse getReponse() {
        return reponse;
    }

    /**
     * @param reponse l'objet jaxb mapping xml contenant des informations de la réponse mpe
     */
    public void setReponse(Mpe.Reponse reponse) {
        this.reponse = reponse;
    }

}
