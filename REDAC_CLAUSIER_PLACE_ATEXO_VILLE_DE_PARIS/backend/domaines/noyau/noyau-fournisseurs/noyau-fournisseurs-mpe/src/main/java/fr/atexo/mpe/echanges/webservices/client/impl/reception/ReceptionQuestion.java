package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.beans.RegistreQuestionBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * La classe qui permet de récupérer le registre question/reponse
 * @author Xuesong GAO
 */
public class ReceptionQuestion {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour récupérer le registre au demat
     */
    private final BaseReception reception;

    public ReceptionQuestion(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger Demat et récupérer le registre question demat
     * @param idRegistreQuestion indiquer quel registre question à récupérer
     * @return {@link RegistreQuestionBean} un bean qui contient un objet pojo
     * présentant la question et un fichier joint (si existe)
     */
    public RegistreQuestionBean recupererRegistreQuestion(final int idRegistreQuestion, String pathWs) throws ClientMpeException {
        logger.info("Débuter à récupérer la question demat, idRegistreQuestion: {}", idRegistreQuestion);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs + "/" + idRegistreQuestion);

        RegistreQuestionBean registreQuestionBean = new RegistreQuestionBean();
        registreQuestionBean.setQuestionDemat(reponseDemat.getReponse().getRegistreQuestion());

        List<File> fichiers = reponseDemat.getFichiers();
        if (!fichiers.isEmpty())
            registreQuestionBean.setFichierRegistreQuestion(fichiers.get(0)); // il y a maximum un fichier joint pour le registre question

        logger.info("La récupération de la question demat est terminée.\n");
        return registreQuestionBean;
    }

}
