package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.EchangeDematTemplate;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * La classe basique qui implemente le processus commun pour la reception du
 * registre demat. Des responsabilités supplémentaires peuvent être attachés en
 * créant une nouvelle classe décorateur afin de récupérer le retrait, question
 * ou depot.
 * @author Xuesong GAO
 */
public class BaseReception {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * class qui permet de communiquer le webservice demat et envoyer la requete
     */
    private final EchangeDematTemplate template;

    /**
     * @param urlAuthentification     recuperation du ticket
     * @param login            login pour l'accès au web service de la pf demat
     * @param password              mot de passe pour l'accès au web service de la pf demat
     * @param cheminFichierTemporaire Chemin des fichiers temporaires. Le path
     *                                doit finir par un "/"). Les fichieres temporaires générés sont
     *                                sauvegardés dans ce path.
     */
    public BaseReception(String urlAuthentification, String login, String password, String cheminFichierTemporaire) {
        this.template = new EchangeDematTemplate(urlAuthentification, login, password, cheminFichierTemporaire, logger);
    }

    /**
     * La méthode générique pour récupérer le registre au demat.
     * @param url la location du resource du registre
     * @return le bean représentant la réponse du webservice demat
     */
    public ReponseDematBean recuperer(final String url) throws ClientMpeException {
        return recuperer(url, null);
    }

    /**
     * La méthode générique pour récupérer le registre au demat.
     *
     * @param endpoint endpoint la location du resource du registre
     * @param params query paramètres à ajouter
     * @return le bean représentant la réponse du webservice demat
     */
    public ReponseDematBean recuperer(final String endpoint, final String params) throws ClientMpeException {

        try {
            return template.appelerWsEtRecupererReponse(endpoint, (url) -> {

                StringBuilder sb = new StringBuilder();
                sb.append(url);
                if (params != null)
                    sb.append("&").append(params);

                HttpGet request = new HttpGet(sb.toString());
                request.setHeader("Accept", "application/xml");

                logger.info("Appeler web service HTTP GET: " + request.getURI());
                HttpClient client = HttpClientBuilder.create().build();
                HttpResponse response;
                try {
                    response = client.execute(request);
                } catch (IOException ex) {
                    logger.error("Une erreur est survenue lors de la réception du message demat : {}", ex.getMessage());
                    throw new ClientMpeException("Une erreur est survenue lors de la réception du message demat : ", ex);
                }
                logger.info("HTTP Response est reçu : {}", response.toString());

                return response;
            });

        } catch (Exception e) {
            logger.error("Une erreur est survenue lors de la réception du message demat");
            throw new ClientMpeException(e);
        }
    }

}
