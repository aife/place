package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by qba on 08/03/17.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FichierType", propOrder = {

})
public class FichierType {

    protected String nom;

    protected Integer id;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
