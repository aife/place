package fr.atexo.mpe.echanges.webservices.client.beans;

import fr.atexo.mpe.echanges.webservices.client.jaxb.DocumentType;

import java.io.File;

public class DocumentMpsBean {

    private DocumentType documentType;

    private File fichierDocumentMps;

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType valeur) {
        this.documentType = valeur;
    }

    public File getFichierDocumentMps() {
        return fichierDocumentMps;
    }

    public void setFichierDocumentMps(File valeur) {
        this.fichierDocumentMps = valeur;
    }

}
