package fr.atexo.mpe.echanges.webservices.client.beans;

import java.io.File;

/**
 * Le bean présentant le bloc du pli sachant que le fichier d'un pli peut être
 * décomposé par une série de bloc
 * @author Xuesong
 */
public class BlocPliBean {

    /**
     * fichier contenant le donnés du bloc
     */
    private File fichierBloc;

    /**
     * le nom du bloc
     */
    private String nomBloc;

    public File getFichierBloc() {
        return fichierBloc;
    }

    public void setFichierBloc(File valeur) {
        this.fichierBloc = valeur;
    }

    public String getNomBloc() {
        return nomBloc;
    }

    public void setNomBloc(String valeur) {
        this.nomBloc = valeur;
    }

}
