package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;
import fr.atexo.mpe.echanges.webservices.client.jaxb.DepotType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * La classe qui permet de récupérer le registre depot
 * @author Xuesong GAO
 */
public class ReceptionDepot {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour récupérer le registre au demat
     */
    private final BaseReception reception;

    public ReceptionDepot(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger Demat et récupérer le registre depot demat
     * @param idRegistreDepot identifiant pour indiquer le registre dépôt
     * @param pathWs          la location du depot
     * @return {@link DepotType} objet pojo présentant le depot
     */
    public DepotType recupererRegistreDepot(final int idRegistreDepot, String pathWs) throws ClientMpeException {
        logger.info("Débuter à récupérer le dépôt demat, idRegistreDepot: {}", idRegistreDepot);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs + "/" + idRegistreDepot);
        DepotType depotRecu = reponseDemat.getReponse().getRegistreDepot();
        logger.info("La récupération du dépôt demat est terminée.\n");
        return depotRecu;
    }

}
