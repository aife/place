package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for ClauseSocialeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClauseSocialeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="conditionExecution" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="insertion" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="marcheReserve" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="ESAT_EA" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="SIAE" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="EESS" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClauseSocialeType")
public class ClauseSocialeType {

    @XmlAttribute(name = "conditionExecution", required = true)
    protected boolean conditionExecution;
    @XmlAttribute(name = "insertion", required = true)
    protected boolean insertion;
    @XmlAttribute(name = "marcheReserve", required = true)
    protected boolean marcheReserve;
    @XmlAttribute(name = "ESAT_EA", required = true)
    protected boolean esatea;
    @XmlAttribute(name = "SIAE", required = true)
    protected boolean siae;
    @XmlAttribute(name = "EESS", required = true)
    protected boolean eess;

    /**
     * Gets the value of the conditionExecution property.
     */
    public boolean isConditionExecution() {
        return conditionExecution;
    }

    /**
     * Sets the value of the conditionExecution property.
     */
    public void setConditionExecution(boolean value) {
        this.conditionExecution = value;
    }

    /**
     * Gets the value of the insertion property.
     */
    public boolean isInsertion() {
        return insertion;
    }

    /**
     * Sets the value of the insertion property.
     */
    public void setInsertion(boolean value) {
        this.insertion = value;
    }

    /**
     * Gets the value of the marcheReserve property.
     */
    public boolean isMarcheReserve() {
        return marcheReserve;
    }

    /**
     * Sets the value of the marcheReserve property.
     */
    public void setMarcheReserve(boolean value) {
        this.marcheReserve = value;
    }

    /**
     * Gets the value of the esatea property.
     */
    public boolean isEsatea() {
        return esatea;
    }

    /**
     * Sets the value of the esatea property.
     */
    public void setEsatea(boolean value) {
        this.esatea = value;
    }

    /**
     * Gets the value of the siae property.
     */
    public boolean isSiae() {
        return siae;
    }

    /**
     * Sets the value of the siae property.
     */
    public void setSiae(boolean value) {
        this.siae = value;
    }

    /**
     * Gets the value of the eess property.
     */
    public boolean isEess() {
        return eess;
    }

    /**
     * Sets the value of the eess property.
     */
    public void setEess(boolean value) {
        this.eess = value;
    }

}
