package fr.atexo.mpe.echanges.webservices.client.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour AnnonceType complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="AnnonceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="intitule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="objet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="details" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *               &lt;enumeration value="INFORMATION"/>
 *               &lt;enumeration value="ATTRIBUTION"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="naturePrestation" type="{http://www.atexo.com/epm/xml}NaturePrestationType"/>
 *         &lt;element name="typeProcedure" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cpv" type="{http://www.atexo.com/epm/xml}CPVType" minOccurs="0"/>
 *         &lt;element name="dateFinAffichage" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="lieuExecutions" type="{http://www.atexo.com/epm/xml}LieuExecutionsType" minOccurs="0"/>
 *         &lt;element name="statut">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *               &lt;enumeration value="ATTENTE_VALIDATION"/>
 *               &lt;enumeration value="VALIDER"/>
 *               &lt;enumeration value="HORS_LIGNE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="fichiersPublicite" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fichier" type="{http://www.atexo.com/epm/xml}FichierType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="idCreateur" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="organisme" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idDirectionService" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="urlExterne" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnonceType", propOrder = {

})
public class AnnonceType {

    protected Integer id;
    @XmlElement(required = true)
    protected String reference;
    protected String intitule;
    @XmlElement(required = true)
    protected String objet;
    protected String details;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String type;
    @XmlElement(required = true)
    protected NaturePrestationType naturePrestation;
    @XmlElement(required = true)
    protected String typeProcedure;
    protected CPVType cpv;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateFinAffichage;
    protected LieuExecutionsType lieuExecutions;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String statut;
    protected AnnonceType.FichiersPublicite fichiersPublicite;
    protected Integer idCreateur;
    @XmlElement(required = true)
    protected String organisme;
    protected int idDirectionService;
    protected String urlExterne;

    /**
     * Obtient la valeur de la propriété id.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getId() {
        return id;
    }

    /**
     * Définit la valeur de la propriété id.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propriété reference.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getReference() {
        return reference;
    }

    /**
     * Définit la valeur de la propriété reference.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * Obtient la valeur de la propriété intitule.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Définit la valeur de la propriété intitule.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIntitule(String value) {
        this.intitule = value;
    }

    /**
     * Obtient la valeur de la propriété objet.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getObjet() {
        return objet;
    }

    /**
     * Définit la valeur de la propriété objet.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setObjet(String value) {
        this.objet = value;
    }

    /**
     * Obtient la valeur de la propriété details.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDetails() {
        return details;
    }

    /**
     * Définit la valeur de la propriété details.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDetails(String value) {
        this.details = value;
    }

    /**
     * Obtient la valeur de la propriété type.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Définit la valeur de la propriété type.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtient la valeur de la propriété naturePrestation.
     *
     * @return
     *     possible object is
     *     {@link NaturePrestationType }
     *
     */
    public NaturePrestationType getNaturePrestation() {
        return naturePrestation;
    }

    /**
     * Définit la valeur de la propriété naturePrestation.
     *
     * @param value
     *     allowed object is
     *     {@link NaturePrestationType }
     *
     */
    public void setNaturePrestation(NaturePrestationType value) {
        this.naturePrestation = value;
    }

    /**
     * Obtient la valeur de la propriété typeProcedure.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTypeProcedure() {
        return typeProcedure;
    }

    /**
     * Définit la valeur de la propriété typeProcedure.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTypeProcedure(String value) {
        this.typeProcedure = value;
    }

    /**
     * Obtient la valeur de la propriété cpv.
     *
     * @return
     *     possible object is
     *     {@link CPVType }
     *
     */
    public CPVType getCpv() {
        return cpv;
    }

    /**
     * Définit la valeur de la propriété cpv.
     *
     * @param value
     *     allowed object is
     *     {@link CPVType }
     *
     */
    public void setCpv(CPVType value) {
        this.cpv = value;
    }

    /**
     * Obtient la valeur de la propriété dateFinAffichage.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDateFinAffichage() {
        return dateFinAffichage;
    }

    /**
     * Définit la valeur de la propriété dateFinAffichage.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDateFinAffichage(XMLGregorianCalendar value) {
        this.dateFinAffichage = value;
    }

    /**
     * Obtient la valeur de la propriété lieuExecutions.
     *
     * @return
     *     possible object is
     *     {@link LieuExecutionsType }
     *
     */
    public LieuExecutionsType getLieuExecutions() {
        return lieuExecutions;
    }

    /**
     * Définit la valeur de la propriété lieuExecutions.
     *
     * @param value
     *     allowed object is
     *     {@link LieuExecutionsType }
     *
     */
    public void setLieuExecutions(LieuExecutionsType value) {
        this.lieuExecutions = value;
    }

    /**
     * Obtient la valeur de la propriété statut.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStatut() {
        return statut;
    }

    /**
     * Définit la valeur de la propriété statut.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStatut(String value) {
        this.statut = value;
    }

    /**
     * Obtient la valeur de la propriété fichiersPublicite.
     *
     * @return
     *     possible object is
     *     {@link AnnonceType.FichiersPublicite }
     *
     */
    public AnnonceType.FichiersPublicite getFichiersPublicite() {
        return fichiersPublicite;
    }

    /**
     * Définit la valeur de la propriété fichiersPublicite.
     *
     * @param value
     *     allowed object is
     *     {@link AnnonceType.FichiersPublicite }
     *
     */
    public void setFichiersPublicite(AnnonceType.FichiersPublicite value) {
        this.fichiersPublicite = value;
    }

    /**
     * Obtient la valeur de la propriété idCreateur.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getIdCreateur() {
        return idCreateur;
    }

    /**
     * Définit la valeur de la propriété idCreateur.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setIdCreateur(Integer value) {
        this.idCreateur = value;
    }

    /**
     * Obtient la valeur de la propriété organisme.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrganisme() {
        return organisme;
    }

    /**
     * Définit la valeur de la propriété organisme.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrganisme(String value) {
        this.organisme = value;
    }

    /**
     * Obtient la valeur de la propriété idDirectionService.
     *
     */
    public int getIdDirectionService() {
        return idDirectionService;
    }

    /**
     * Définit la valeur de la propriété idDirectionService.
     *
     */
    public void setIdDirectionService(int value) {
        this.idDirectionService = value;
    }

    /**
     * Obtient la valeur de la propriété urlExterne.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUrlExterne() {
        return urlExterne;
    }

    /**
     * Définit la valeur de la propriété urlExterne.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUrlExterne(String value) {
        this.urlExterne = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fichier" type="{http://www.atexo.com/epm/xml}FichierType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "fichier"
    })
    public static class FichiersPublicite {

        @XmlElement(required = true)
        protected List<FichierType> fichier;

        /**
         * Gets the value of the fichier property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fichier property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFichier().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FichierType }
         *
         *
         */
        public List<FichierType> getFichier() {
            if (fichier == null) {
                fichier = new ArrayList<FichierType>();
            }
            return this.fichier;
        }

    }

}