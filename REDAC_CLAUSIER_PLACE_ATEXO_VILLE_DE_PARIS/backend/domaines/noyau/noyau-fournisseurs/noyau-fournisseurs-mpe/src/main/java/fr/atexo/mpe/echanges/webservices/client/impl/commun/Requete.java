package fr.atexo.mpe.echanges.webservices.client.impl.commun;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import org.apache.http.HttpResponse;

/**
 * Callback interface utilisée par la méthode appelerWsEtRecupererReponse du
 * EnvoiReceptionTemplate. Souvent implementation doit être faite par anonymous
 * classes.
 * @author Xuesong GAO
 */
@FunctionalInterface
public interface Requete {

    /**
     * La méthode doit comuniquer avec le fournisseur du webservices, puis
     * envoyer la requete (par POST ou GET) et finallement retourner la HttpResponse
     * @param url Ticketed déjà initialisé avec url serveur demat et le ticket authentification,
     *                    prêt à utiliser pour envoyer la requete
     * @return la réponse retournée après envoyer la requete
     */
    HttpResponse envoyerRequete(String url) throws ClientMpeException;

}
