package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * La classe qui permet de récupérer tous les IDs de registres pour une consultation
 * @author Xuesong GAO
 */
public class ReceptionListeIdRegistre {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour récupérer le registre au demat
     */
    private final BaseReception reception;

    public ReceptionListeIdRegistre(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger Demat et récupérer une liste id de registre
     * retrait disponible pour une consultation
     * @param idConsultationDemat indiquer que l'on récupère la liste de quelle consultation.
     *                            Il doit être l'identifiant de consultation coté demat
     * @return liste id de retrait dispobible pour cette consultation
     */
    public List<Integer> recupererListeIdRegistre(final int idConsultationDemat, String pathWs) throws ClientMpeException {
        logger.info("Débuter à récupérer la liste ids registre, idConsultationDemat: {}", idConsultationDemat);

        ReponseDematBean reponseDematBean = reception.recuperer(pathWs + "/" + idConsultationDemat);
        List<Integer> listeIdRegistre = reponseDematBean.getReponse().getListeIdRegistre().getId();

        logger.info("La récupération de la liste ids registre est terminée.\n");
        return listeIdRegistre;
    }

}
