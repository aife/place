
package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour ReferentielsType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ReferentielsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="procedures">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="procedure" type="{http://www.atexo.com/epm/xml}ProcedureType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="contrats">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="contrat" type="{http://www.atexo.com/epm/xml}ContratType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferentielsType", namespace = "http://www.atexo.com/epm/xml", propOrder = {
    "procedures",
    "contrats"
})
public class ReferentielsType {

    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected ReferentielsType.Procedures procedures;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected ReferentielsType.Contrats contrats;

    /**
     * Obtient la valeur de la propriété procedures.
     *
     * @return
     *     possible object is
     *     {@link ReferentielsType.Procedures }
     *
     */
    public ReferentielsType.Procedures getProcedures() {
        return procedures;
    }

    /**
     * Définit la valeur de la propriété procedures.
     *
     * @param value
     *     allowed object is
     *     {@link ReferentielsType.Procedures }
     *
     */
    public void setProcedures(ReferentielsType.Procedures value) {
        this.procedures = value;
    }

    /**
     * Obtient la valeur de la propriété contrats.
     *
     * @return
     *     possible object is
     *     {@link ReferentielsType.Contrats }
     *
     */
    public ReferentielsType.Contrats getContrats() {
        return contrats;
    }

    /**
     * Définit la valeur de la propriété contrats.
     *
     * @param value
     *     allowed object is
     *     {@link ReferentielsType.Contrats }
     *
     */
    public void setContrats(ReferentielsType.Contrats value) {
        this.contrats = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="contrat" type="{http://www.atexo.com/epm/xml}ContratType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contrat"
    })
    public static class Contrats {

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected List<ContratType> contrat;

        /**
         * Gets the value of the contrat property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contrat property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContrat().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContratType }
         * 
         * 
         */
        public List<ContratType> getContrat() {
            if (contrat == null) {
                contrat = new ArrayList<ContratType>();
            }
            return this.contrat;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="procedure" type="{http://www.atexo.com/epm/xml}ProcedureType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "procedure"
    })
    public static class Procedures {

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected List<ProcedureType> procedure;

        /**
         * Gets the value of the procedure property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the procedure property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProcedure().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcedureType }
         * 
         * 
         */
        public List<ProcedureType> getProcedure() {
            if (procedure == null) {
                procedure = new ArrayList<ProcedureType>();
            }
            return this.procedure;
        }

    }

}
