
package fr.atexo.mpe.echanges.webservices.client.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour ProcedureType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ProcedureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codeExterne" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="libelle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="abreviation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="associationOrganisme">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="codeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcedureType", namespace = "http://www.atexo.com/epm/xml", propOrder = {
    "codeExterne",
    "libelle",
    "abreviation",
    "associationOrganisme"
})
public class ProcedureType {

    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String codeExterne;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String libelle;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected String abreviation;
    @XmlElement(namespace = "http://www.atexo.com/epm/xml", required = true)
    protected ProcedureType.AssociationOrganisme associationOrganisme;

    /**
     * Obtient la valeur de la propriété codeExterne.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodeExterne() {
        return codeExterne;
    }

    /**
     * Définit la valeur de la propriété codeExterne.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodeExterne(String value) {
        this.codeExterne = value;
    }

    /**
     * Obtient la valeur de la propriété libelle.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Définit la valeur de la propriété libelle.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setLibelle(String value) {
        this.libelle = value;
    }

    /**
     * Obtient la valeur de la propriété abreviation.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAbreviation() {
        return abreviation;
    }

    /**
     * Définit la valeur de la propriété abreviation.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAbreviation(String value) {
        this.abreviation = value;
    }

    /**
     * Obtient la valeur de la propriété associationOrganisme.
     *
     * @return
     *     possible object is
     *     {@link ProcedureType.AssociationOrganisme }
     *
     */
    public ProcedureType.AssociationOrganisme getAssociationOrganisme() {
        return associationOrganisme;
    }

    /**
     * Définit la valeur de la propriété associationOrganisme.
     *
     * @param value
     *     allowed object is
     *     {@link ProcedureType.AssociationOrganisme }
     *
     */
    public void setAssociationOrganisme(ProcedureType.AssociationOrganisme value) {
        this.associationOrganisme = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="codeOrganisme" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codeOrganisme"
    })
    public static class AssociationOrganisme {

        @XmlElement(namespace = "http://www.atexo.com/epm/xml")
        protected List<String> codeOrganisme;

        /**
         * Gets the value of the codeOrganisme property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the codeOrganisme property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCodeOrganisme().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getCodeOrganisme() {
            if (codeOrganisme == null) {
                codeOrganisme = new ArrayList<String>();
            }
            return this.codeOrganisme;
        }

    }

	@Override
	public String toString() {
		return MessageFormat.format("''{0}'' (abréviation = ''{1}'', code externe = ''{2}'')", this.getLibelle(), this.getAbreviation(), this.getCodeExterne());
	}
}
