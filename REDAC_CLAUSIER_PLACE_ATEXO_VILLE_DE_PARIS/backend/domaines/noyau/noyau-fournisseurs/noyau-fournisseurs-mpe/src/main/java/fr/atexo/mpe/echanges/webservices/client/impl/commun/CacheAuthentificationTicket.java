package fr.atexo.mpe.echanges.webservices.client.impl.commun;

import java.util.Date;

/**
 * La class utilisé pour sauvegarder le ticket une fois authentifier auprès du
 * service Demat pour que ce ticket puisse être re-utilisé après. Cela peut
 * réduire le nombre de communication avec le serveur demat et donc augmenter la
 * performance.
 * @author Xuesong
 */
public class CacheAuthentificationTicket {

    /**
     * Une heure de la durée de validité du ticket en millisecondes
     */
    private static final long DUREE_VALIDITE_TICKET_MILLISECONDS = 3600000;

    /**
     * L'horodatage timestamp quand on demande le ticket en communiquant avec le
     * serveur demat
     */
    public static Date dateDerniereMiseAJour;

    /**
     * Le champ statique pour sauvegarder le ticket
     */
    public static String ticket;

    /**
     * Retourner le ticket en cache.
     * @return Il est null si le ticket est déjà expiré ou si le ticket n'est jamais setté.
     */
    public static String getTicket() {
        Date dateCourante = new Date();
        if (dateDerniereMiseAJour != null && dateCourante.getTime() - dateDerniereMiseAJour.getTime() < DUREE_VALIDITE_TICKET_MILLISECONDS)
            return ticket;
        return null;
    }

    /**
     * Mettre à jour le ticket en cache
     * @param valeur le nouveau ticket
     */
    public static void setTicket(String valeur) {
        CacheAuthentificationTicket.ticket = valeur;
        CacheAuthentificationTicket.dateDerniereMiseAJour = new Date();
    }

}
