package fr.atexo.mpe.echanges.webservices.client.exception;

/**
 * Exception levée lorsque d'un échec l'appel du webservice demat
 * @author Xuesong
 */
public class ClientMpeException extends Exception {

    /**
     * Marqueur de sérialization propagé aux enfants.
     */
    private static final long serialVersionUID = 1L;

    public ClientMpeException() {
        super();
    }

    /**
     * @param msg   message d'erreur
     * @param cause exception ayant provoqué cette exception
     */
    public ClientMpeException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    /**
     * @param msg message d'erreur
     */
    public ClientMpeException(final String msg) {
        super(msg);
    }

    /**
     * @param cause exception ayant provoqué cette exception
     */
    public ClientMpeException(final Throwable cause) {
        super(cause);
    }

}
