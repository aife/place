package fr.atexo.mpe.echanges.webservices.client.impl.commun;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.jaxb.Mpe;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

/**
 * classe pour regrouper les méthodes communes utilisées par classes envoi et reception
 * @author Xuesong GAO
 */
public class EchangeDematTemplate {

    /**
     * Logger pour enregistrer les infos d'échanges
     */
    private final Logger logger;

    /**
     * nom du param de la méthode GET pour envoyer le ticket authentification
     */
    private static final String TOKEN = "token";

    /**
     * relative URI path pour l'authentification
     */
    private static final String URL_AUTHENTIFICATION = "/ws/authentification/connexion";

    /**
     * login pour l'accès au web service de la pf demat
     */
    private final String login;

    /**
     * mot de passe pour l'accès au web service de la pf demat
     */
    private final String password;

	/**
	 * Permet de recuperer le ticket
	 */
	private final String urlAuthentification;

    /**
     * Chemin des fichiers temporaires
     */
    private final String cheminFichierTemporaire;

    /**
     * @param urlAuthentification     recuperation du ticket
     * @param login            login pour l'accès au web service de la pf demat
     * @param password              mot de passe pour l'accès au web service de la pf demat
     * @param cheminFichierTemporaire Chemin des fichiers temporaires. Le path
     *                                doit finir par un "/"). Les fichieres temporaires générés sont
     *                                sauvegardés dans ce path.
     * @param logger                  passer un logger pour sortir les logs
     */
    public EchangeDematTemplate( String urlAuthentification, String login, String password,
                                 String cheminFichierTemporaire, Logger logger) {
        this.urlAuthentification = urlAuthentification;
        this.login = login;
        this.password = password;
        this.cheminFichierTemporaire = cheminFichierTemporaire;
        this.logger = logger;
    }

    /**
     * Authentifier auprès du service Demat en utilisant le login et mot de
     * passe configué par Spring. Un ticket va être obtenu si l'authentification
     * est réussi.
     * @return ticket obtenu après s’authentifier auprès du service. Il doit être fourni à chaque requête.
     */
    private String recupererTicket( final String authentification, boolean forceMiseAJour) throws ClientMpeException {
        logger.info("Préparer le ticket d'authentification");

        String ticket = CacheAuthentificationTicket.getTicket();
        if (ticket != null && !forceMiseAJour) {
            logger.info("Utiliser le ticket {} déjà récupéré lors de la dernière authentification", ticket);
            return ticket;
        }

        HttpClient client = HttpClientBuilder.create().build();
	    HttpGet request = new HttpGet(MessageFormat.format(authentification, login, password));
        request.setHeader("Accept", "application/xml");
        logger.info("Appeler web service: " + request.getURI());

        HttpResponse response;
        try {
            response = client.execute(request);
        } catch (IOException ex) {
            throw new ClientMpeException("Erreur lors de la récuperation du ticket", ex);
        }

        if (response.getStatusLine().getStatusCode() != 200) { // "OK"
            logger.error("Le serveur demat répond avec code http : {}. Le contenu du réponse http: {}",
                    response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());

            throw new ClientMpeException("Le serveur demat répond avec code http : " + response.getStatusLine().getStatusCode() + ". " +
                    "Le contenu du réponse http: " + response.getStatusLine().getReasonPhrase() + ". ");
        }

        try (InputStream inputStream = response.getEntity().getContent()) {
            JAXBContext jaxbContext = JAXBContext.newInstance(String.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            JAXBElement<String> element = jaxbUnmarshaller.unmarshal(new StreamSource(inputStream), String.class);
            ticket = element.getValue();
        } catch (JAXBException | IOException e) {
            throw new ClientMpeException("Erreur lors de la récuperation du ticket: impossible d'extraire le ticket de la requete.");
        }

        if (ticket == null || ticket.isEmpty())
            throw new ClientMpeException("Erreur lors de la récuperation du ticket: le ticket reçu est vide (p-e l'user n'existe pas dans MPE)");

        logger.info("Le ticket reçu est: " + ticket);
        CacheAuthentificationTicket.setTicket(ticket);

        return ticket;
    }

    /**
     * Appeler le webservices et vérifier la réponse http.
     * @param requete contient l'URI du WS et le contenu à envoyer
     */
    private HttpResponse appelerWebservices(String url, Requete requete) throws ClientMpeException {
        String ticket = recupererTicket(urlAuthentification,false);

        HttpResponse response = requete.envoyerRequete(MessageFormat.format(url, ticket));

        // si le ticket est invalide, on ré-authentifie et récupère le ticket de nouveau, puis appelle le webservices avec le nouveau ticket.
        if (response.getStatusLine().getStatusCode() == 401) { // "Unauthorized"
            logger.info("Le ticket en cache est invalide, ré-authentifie et ré-interroger le webservices");
            ticket = recupererTicket(urlAuthentification,true);
            response = requete.envoyerRequete(MessageFormat.format(url, ticket));
        }

        if (response.getStatusLine().getStatusCode() != 200) { // "OK"
            StringBuilder messageException = new StringBuilder();
            messageException.append("Le serveur demat répond avec code http : ")
                    .append(response.getStatusLine().getStatusCode())
                    .append(". ")
                    .append("La raison est: ")
                    .append(response.getStatusLine().getReasonPhrase())
                    .append("\n");

            if (response.getEntity().getContentType().getValue().equals("application/xml")) {
                messageException.append("Contenu de la reponse :\n");
                try {
                    String resultXml = EntityUtils.toString(response.getEntity(), "UTF-8");
                    messageException.append(resultXml);
                } catch (IOException ex) {
                    messageException.append("Content de la reponse est erronée");
                }
            }
            throw new ClientMpeException(messageException.toString());
        }

        return response;
    }

    /**
     * Méthode utilisée pour interroger le webservice demat, envoyer la requete
     * et récupérer la réponse.
     * @param requete contient l'URI du WS et le contenu à envoyer
     * @return un bean représentant les données reçues du demat
     */
    public ReponseDematBean appelerWsEtRecupererReponse(String url, Requete requete) throws ClientMpeException {
        HttpResponse response = appelerWebservices(url, requete);

        ReponseDematBean reponseDematBean = new ReponseDematBean();
        if (response.getEntity().getContentType().getValue().equals("application/xml")) {

            try {
                String resultXml = EntityUtils.toString(response.getEntity(), "UTF-8");
                logger.info("Le flux XML reçu du demat:\n{}", resultXml);

                JAXBContext jaxbContext = JAXBContext.newInstance(Mpe.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                Mpe retourMpe = (Mpe) jaxbUnmarshaller.unmarshal(new StringReader(resultXml));
                reponseDematBean.setReponse(retourMpe.getReponse());

            } catch (JAXBException | IOException e) {
	            logger.error("Erreur lors de la conversion de la réponse XML provenant de MPE", e);
                throw new ClientMpeException("Erreur lors de la conversion de la réponse XML provenant de MPE", e);
            }

        } else if (response.getEntity().getContentType().getValue().startsWith("multipart/form-data")) {

            try {
                DataSource dataSource = new DataSource() {

                    @Override
                    public InputStream getInputStream() throws IOException {
                        return response.getEntity().getContent();
                    }

                    @Override
                    public OutputStream getOutputStream() {
                        throw new UnsupportedOperationException("Not implemented");
                    }

                    @Override
                    public String getContentType() {
                        return response.getEntity().getContentType().getValue();
                    }

                    @Override
                    public String getName() {
                        return "InputStreamDataSource";
                    }

                };
                MimeMultipart multipart = new MimeMultipart(dataSource);
                int count = multipart.getCount();
                logger.debug("count " + count);
                for (int i = 0; i < count; i++) {
                    BodyPart bodyPart = multipart.getBodyPart(i);
                    if (bodyPart.getContentType().equals("application/xml")) {
                        logger.info("application/xml " + bodyPart.getContentType());

                        JAXBContext jaxbContext = JAXBContext.newInstance(Mpe.class);
                        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                        Mpe retourMpe = (Mpe) jaxbUnmarshaller.unmarshal(bodyPart.getInputStream());
                        reponseDematBean.setReponse(retourMpe.getReponse());

                    } else /*if (bodyPart.getContentType().equals("application/octet-stream"))*/ {
                        /* avant MPE nous a envoyé "application/octet-stream"
                           à partir d'une version 2019 c'est le type de fichier */
                        logger.info("application/octet-stream " + bodyPart.getContentType());

                        try (InputStream inputStream = bodyPart.getInputStream()) {
                            InputStream resultStream = inputStream;

                            Optional<String> headerBase64 = Arrays.stream(bodyPart.getHeader("Content-Transfer-Encoding"))
                                    .filter(header -> header.contains("base64"))
                                    .findFirst();
                            if (headerBase64.isPresent())
                                resultStream = new ByteArrayInputStream(Base64.encodeBase64(IOUtils.toByteArray(inputStream)));

                            File fichier = enregistrerFichierTemporaire(resultStream, cheminFichierTemporaire);
                            inputStream.close();
                            resultStream.close();
                            logger.info("Reçu un fichier de {} octet", fichier.length());
                            reponseDematBean.getFichiers().add(fichier);
                        } catch (IOException e) {
                            logger.error("Erreur lor de l'enregistrement du fichier dans la rêquete réponse http", e);
                            throw new ClientMpeException("Erreur lor de l'enregistrement du fichier dans la rêquete réponse http", e);
                        }
                    }/* else {
                        logger.warn("default " + bodyPart.getContentType());
                    }*/
                }
            } catch (MessagingException | JAXBException | IOException ex) {
                throw new ClientMpeException("Dans l'objet MPE retourné, il y a pas objet reponse");
            }
        } else {
            throw new ClientMpeException("MIME media type de la réponse http demat est " + response.getEntity().getContentType().getValue() +
                    ". Les types acceptés sont: multipart/form-data et application/xml ");
        }
        return reponseDematBean;
    }

    /**
     * Méthode pour transformer l'objet Jaxb en XML sous format String
     * @param objet Jaxb à transformer en XML
     * @return XML transformé
     */
    public static String objetVersXml(Object objet) {
        String resultat = "";
        if (objet == null)
            return resultat;

        try {
            JAXBContext context = JAXBContext.newInstance(objet.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            marshaller.marshal(objet, os);
            resultat = os.toString();

        } catch (JAXBException e) {
            return "Erreur quand transformer l'objet en XML";
        }

        return resultat.trim();
    }

    /**
     * Sortir les données dans les InputStream vers un fichiers temporaire
     * @param input le stream entrant
     * @param cheminFichierTemporaire le chemin des fichiers temporaires. Le path doit finir par un "/").
     * @return le fichier créé
     */
    static File enregistrerFichierTemporaire(InputStream input, final String cheminFichierTemporaire) throws IOException {

        String nomRepertoire = cheminFichierTemporaire + System.currentTimeMillis();
        File repertoireTemp = new File(nomRepertoire);
        if (!repertoireTemp.exists())
            if (!repertoireTemp.mkdirs())
                throw new IOException("Erreur pendant la création du repertoire TEMP : " + nomRepertoire);

        Random generateur = new Random();
        final int entier = generateur.nextInt(9999999);

        File fichier = new File(nomRepertoire + File.separator + entier);
        if (!fichier.exists())
            if (!fichier.createNewFile())
                throw new IOException("Erreur pendant la création du fichier TEMP : " + fichier.getName());

        try (OutputStream output = new FileOutputStream(fichier)) {
            IOUtils.copy(input, output);
            output.flush();
        }

        return fichier;
    }

}
