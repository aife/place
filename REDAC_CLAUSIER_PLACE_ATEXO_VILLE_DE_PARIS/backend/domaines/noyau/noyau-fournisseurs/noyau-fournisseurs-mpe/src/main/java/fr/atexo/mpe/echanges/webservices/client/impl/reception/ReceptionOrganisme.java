package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.beans.OrganismeAvecLogoBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;
import fr.atexo.mpe.echanges.webservices.client.jaxb.OrganismeType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;

/**
 * La classe qui permet de récupérer les données d'un organisme
 * @author Xuesong GAO
 */
public class ReceptionOrganisme {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour appeler le WS MPE
     */
    private final BaseReception reception;

    public ReceptionOrganisme(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger MPE et récupérer les données d'un organisme.
     * @return les données d'un organisme, y compris le fichier logo
     */
    public OrganismeAvecLogoBean recupererOrganisme(final String url, final String acronymeOrganisme) throws ClientMpeException {
        logger.info("Débuter à récupérer les données de l'organisme {}", acronymeOrganisme);

        ReponseDematBean reponseDemat = reception.recuperer(MessageFormat.format(url, acronymeOrganisme));

        OrganismeAvecLogoBean organismeAvecLogoBean = new OrganismeAvecLogoBean();
        OrganismeType organismeType = reponseDemat.getReponse().getOrganisme();
        organismeAvecLogoBean.setOrganismeMpe(organismeType);

        List<File> fichiers = reponseDemat.getFichiers();
        if (!fichiers.isEmpty())
            organismeAvecLogoBean.setFichierLogo(fichiers.get(0)); // il y a maximum un fichier logo pour chaque organisme

        logger.info("La récupération des données de l'organisme {} est terminée.\n", acronymeOrganisme);
        return organismeAvecLogoBean;
    }

}
