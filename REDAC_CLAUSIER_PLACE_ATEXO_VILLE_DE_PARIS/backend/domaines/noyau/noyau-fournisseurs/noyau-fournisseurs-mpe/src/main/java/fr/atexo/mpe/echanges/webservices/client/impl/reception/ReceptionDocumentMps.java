package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.beans.DocumentMpsBean;
import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

public class ReceptionDocumentMps {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour appeler le WS MPE
     */
    private final BaseReception reception;

    public ReceptionDocumentMps(BaseReception reception) {
        this.reception = reception;
    }

    public DocumentMpsBean recupererDocumentMps(final int idDepot, String pathWs) throws ClientMpeException {

        logger.info("Débuter à récupérer le document MPS avec idDepot: {}", idDepot);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs + "/" + idDepot);

        DocumentMpsBean documentMpsBean = new DocumentMpsBean();
        documentMpsBean.setDocumentType(reponseDemat.getReponse().getDocument());

        List<File> fichiers = reponseDemat.getFichiers();
        if (!fichiers.isEmpty())
            documentMpsBean.setFichierDocumentMps(fichiers.get(0)); // il y a un seul document MPS

        logger.info("La récupération du document MPS est terminée.\n");
        return documentMpsBean;
    }

}
