package fr.atexo.mpe.echanges.webservices.client.impl.reception;

import fr.atexo.mpe.echanges.webservices.client.exception.ClientMpeException;
import fr.atexo.mpe.echanges.webservices.client.impl.commun.ReponseDematBean;
import fr.atexo.mpe.echanges.webservices.client.jaxb.RetraitDCEType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * La classe qui permet de récupérer le registre retrait
 * @author Xuesong GAO
 */
public class ReceptionRetrait {

    private static final Logger logger = LoggerFactory.getLogger("logSynchronisationRedaction");

    /**
     * La classe basique qui implemente le processus commun pour récupérer le registre au demat
     */
    private final BaseReception reception;

    public ReceptionRetrait(BaseReception reception) {
        this.reception = reception;
    }

    /**
     * Méthode pour interroger demat et récupérer le registre retrait demat
     * @param idRegistreRetrait identifiant pour préciser le registre retrait
     * @return {@link RetraitDCEType} objet pojo présentant le retrait
     */
    public RetraitDCEType recupererRegistreRetrait(final int idRegistreRetrait, String pathWs) throws ClientMpeException {
        logger.info("Débuter à récupérer du retrait demat, idRegistreRetrait: {}", idRegistreRetrait);

        ReponseDematBean reponseDemat = reception.recuperer(pathWs + "/" + idRegistreRetrait);
        RetraitDCEType retraitDceRecu = reponseDemat.getReponse().getRegistreRetrait();

        logger.info("La récupération du retrait demat est terminée.\n");
        return retraitDceRecu;
    }

}
