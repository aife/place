package fr.atexo.rsem.noyau.ws.rest;

import fr.atexo.rsem.noyau.ws.beans.ErreurEnum;
import fr.atexo.rsem.noyau.ws.rest.constantes.ConstantesRestWebService;
import fr.atexo.rsem.noyau.ws.util.ResponseError;
import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.AuthentificationTokenCritere;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.ParametrageCritere;
import fr.paris.epm.noyau.metier.UtilisateurCritere;
import fr.paris.epm.noyau.persistance.EpmTAuthentificationToken;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Web Service REST d'authentification.
 *
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
@Controller
@RequestMapping("/authentification")
public class AuthentificationRestWebService {
    /**
     * Loger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthentificationRestWebService.class);

    @Value("${mpe.client}")
    private String plateformeUuid;
    /**
     * Délai d'inactivité max, au delà un nouveau ticket est créé
     */
    @Value("${authentification.delaiInactiviteMax:60}")
    private long delaiInactiviteMax;

    /**
     * Clé duparamètre SSO
     */
    @Value("${authentification.cleParametreSSO:sso.actif}")
    private String cleParametreSSO;

    /**
     * clé login technique
     */
    @Value("${authentification.cleLoginTechnique:utilisateurTechnique.login}")
    private String cleLoginTechnique;

    /**
     * clé Mot de passe technique
     */
    @Value("${authentification.cleMotDePasseTechnique:utilisateurTechnique.motDePasse}")
    private String cleMotDePasseTechnique;

    /**
     * référentiel GIM
     */
    @Autowired
    private GeneriqueDAO generiqueDAO;

    /**
     * Défini une nouvelle erreur
     *
     * @param error   le type erreurM
     * @param message le message
     * @return les elements XML
     */
    public static ResponseEntity<String> error(ErreurEnum error, String message) {
        ResponseError bean = new ResponseError();
        bean.setCodeMessage(error, message);
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(bean.toString());
    }

    /**
     * AuthentificationTokenProvider
     *
     * @return un token a reutiliser en parametre des autres appels REST
     * (http://...?token=...).
     */
    @GetMapping(value = "/connexion/**", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> connexion( HttpServletRequest request) {
        var url = request.getRequestURL().toString();

        var matcher = Pattern.compile("^.*/connexion/(?<utilisateur>[^/]*)/(?<motDePasse>.*)$").matcher(url);

        if (!matcher.find( )) {
            throw new IllegalArgumentException("Merci de fournir l'utilisateur et le mot de passe");
        }

        var utilisateur = matcher.group("utilisateur");
        var motDePasse = matcher.group("motDePasse");

        if (null==motDePasse || motDePasse.isEmpty()) {
            EpmTRefParametrage parametrage;
            try {
                parametrage = chercherParametre(cleParametreSSO);
            } catch (TechnicalNoyauException e1) {
                LOG.error("Erreur lors de la recherche du parametre SSO ACTIF");
                return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche du parametre SSO ACTIF");
            } catch (ApplicationNoyauException e1) {
                LOG.error("Erreur lors de la recherche du parametre SSO ACTIF");
                return error(ErreurEnum.AUTRE, "Probleme lors de la recherche du parametre SSO ACTIF");
            }

            if (parametrage == null || !Constantes.TRUE.equals(parametrage.getValeur())) {
                LOG.error("Authentification SSO inactive pour la plateforme");
                return error(ErreurEnum.ENTITE_NON_TROUVEE, "Authentification SSO inactive pour la plateforme");
            }
        }

        UtilisateurCritere critere = new UtilisateurCritere(plateformeUuid);
        critere.setGuidsSso(utilisateur);

        EpmTRefParametrage loginTechnique;
        boolean estUtilisateurTechnique = false;

        if (cleLoginTechnique != null) {
            try {
                loginTechnique = chercherParametre(cleLoginTechnique);
            } catch (TechnicalNoyauException e1) {
                LOG.error("Erreur lors de la recherche du parametre LOGIN TECHNIQUE");
                return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche du parametre LOGIN TECHNIQUE");
            } catch (ApplicationNoyauException e1) {
                LOG.error("Erreur lors de la recherche du parametre LOGIN TECHNIQUE");
                return error(ErreurEnum.AUTRE, "Probleme lors de la recherche du parametre LOGIN TECHNIQUE");
            }

            EpmTRefParametrage motDePasseTechnique;
            try {
                motDePasseTechnique = chercherParametre(cleMotDePasseTechnique);
            } catch (TechnicalNoyauException e1) {
                LOG.error("Erreur technique lors de la recherche du parametre MDP TECHNIQUE");
                return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche du parametre MDP TECHNIQUE");
            } catch (ApplicationNoyauException e1) {
                LOG.error("Erreur lors de la recherche du parametrage MDP TECHNIQUE");
                return error(ErreurEnum.AUTRE, "Probleme lors de la recherche du parametre MDP TECHNIQUE");
            }

            if (motDePasseTechnique != null && motDePasseTechnique.getValeur() != null && loginTechnique != null && loginTechnique.getValeur() != null && motDePasseTechnique.getValeur().equals(motDePasse)
                    && loginTechnique.getValeur().equals(utilisateur)) {
                estUtilisateurTechnique = true;
            }
        }

        if (!estUtilisateurTechnique) {
            EpmTUtilisateur utilisateurBDD;
            List<EpmTUtilisateur> utilisateurBddList;
            try {
                utilisateurBddList = generiqueDAO.findByCritere(critere);
            } catch (TechnicalNoyauException e) {
                LOG.error("Probleme lors de la recherche de l'utilisateur {}", utilisateur);
                return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche de l'utilisateur " + utilisateur);
            }

            if ((utilisateurBddList == null) || (utilisateurBddList.isEmpty())) {
                LOG.error("L'utilisateur avec le login {} n'existe pas", utilisateur);
                return error(ErreurEnum.ENTITE_NON_TROUVEE, "Utilisateur " + utilisateur + " inexistant.");
            } else {
                utilisateurBDD = utilisateurBddList.get(0);
            }

            if (!Objects.equals(motDePasse, utilisateurBDD.getMotDePasse())) {
                LOG.warn("L'utilisateur avec le login {} existe pas mais le mdp ne correspond pas", utilisateur);
                return error(ErreurEnum.AUTHENTIFICATION, "Mot de passe de l'utilisateur " + utilisateur + " incorrect.");
            }
        }

        try {
            EpmTAuthentificationToken tokenBdd = processAuthentication(utilisateur);

            return ResponseEntity.ok(tokenBdd.toString());
        } catch (TechnicalNoyauException e) {
            LOG.error("Probleme lors de la recherche de du token de l'utilisateur {}", utilisateur);
            return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche de du token de l'utilisateur " + utilisateur);
        }
    }

    private EpmTAuthentificationToken processAuthentication(String utilisateur) {
        EpmTAuthentificationToken tokenBdd;
        AuthentificationTokenCritere authCritere = new AuthentificationTokenCritere();
        authCritere.setIdentifiant(utilisateur);
        List<EpmTAuthentificationToken> tokenBddList = generiqueDAO.findByCritere(authCritere);
        if ((tokenBddList != null) && (!tokenBddList.isEmpty())) {
            tokenBdd = tokenBddList.get(0);
            long dureeInactivite = new Date().getTime() - tokenBdd.getDateDerniereActivite().getTime();
            if (TimeUnit.MILLISECONDS.toMinutes(dureeInactivite) >= delaiInactiviteMax) {
                //tokenBdd.setDateDerniereActivite(new Date()); --> Mise à jour via RememberMeFilter.java
                generiqueDAO.merge(tokenBdd);
                return tokenBdd;
            }
        }
        //Création d'un nouveau ticket
        tokenBdd = new EpmTAuthentificationToken();
        tokenBdd.setDateDerniereActivite(new Date());
        tokenBdd.setIdentifiant(utilisateur);
        tokenBdd.setSignature(signature());

        return generiqueDAO.merge(tokenBdd);
    }

    /**
     * Supprime le token utiliser pour l'authentification Web Service.
     *
     * @param token fournit en parametre de l'url
     * @return balise deconnexion si ok, balise erreur sinon
     */
    @RequestMapping(value = "/deconnexion", method = RequestMethod.GET, consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity deconnexion(@RequestParam(ConstantesRestWebService.XML_QNAME_TOKEN) String token) {
        AuthentificationTokenCritere tokenCritere = new AuthentificationTokenCritere();
        tokenCritere.setSignature(token);

        List<EpmTAuthentificationToken> tokenBddList;
        try {
            tokenBddList = generiqueDAO.findByCritere(tokenCritere);
        } catch (TechnicalNoyauException e) {
            return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la recherche du token " + token);
        }
        if ((tokenBddList == null) || tokenBddList.isEmpty()) {
            return error(ErreurEnum.ENTITE_NON_TROUVEE, "Token " + token + " absent en base.");
        }
        if (tokenBddList.size() > 1) {
            LOG.warn("Plus d'un token REST present en base (suppression de tous).");
        }
        try {
            generiqueDAO.delete(tokenBddList);
        } catch (TechnicalNoyauException e) {
            return error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, "Probleme lors de la suppression du token " + token + " en base.");
        }

        return ResponseEntity.ok().body(token);
    }

    /**
     * Retourne une chaine ascii aleatoire.
     */
    public static String signature() {
        return new String(Base64.encodeBase64URLSafe(UUID.randomUUID().toString().getBytes()));
    }

    /**
     * chercher un parametre depuis epmtREFParametre cette methode est utilisé
     * pour chercher le login et le mot de passe techeniques
     */
    private EpmTRefParametrage chercherParametre(String clef) {
        ParametrageCritere paramCritere = new ParametrageCritere();
        paramCritere.setClef(clef);
        return (EpmTRefParametrage) generiqueDAO.findUniqueByCritere(paramCritere);
    }

    /**
     * @param delaiInactiviteMax the delaiInactiviteMax to set
     */
    public final void setDelaiInactiviteMax(final long delaiInactiviteMax) {
        this.delaiInactiviteMax = delaiInactiviteMax;
    }

    /**
     * @param cleLoginTechnique the cleLoginTechnique to set
     */
    public final void setCleLoginTechnique(String cleLoginTechnique) {
        this.cleLoginTechnique = cleLoginTechnique;
    }

    /**
     * @param cleParametreSSO la clé du parametre SSO
     */
    public void setCleParametreSSO(String cleParametreSSO) {
        this.cleParametreSSO = cleParametreSSO;
    }

    /**
     * @param cleMotDePasseTechnique the cleMotDePasseTechnique to set
     */
    public final void setCleMotDePasseTechnique(String cleMotDePasseTechnique) {
        this.cleMotDePasseTechnique = cleMotDePasseTechnique;
    }

    /**
     * @param generiqueDAO the generiqueDAO to set
     */
    public final void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

}
