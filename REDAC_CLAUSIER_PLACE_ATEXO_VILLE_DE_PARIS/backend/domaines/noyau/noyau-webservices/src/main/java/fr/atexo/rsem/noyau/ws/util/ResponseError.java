package fr.atexo.rsem.noyau.ws.util;

import fr.atexo.rsem.noyau.ws.beans.ErreurEnum;
import fr.atexo.rsem.noyau.ws.beans.ResponseBean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.MessageFormat;

@XmlRootElement(
	name = "resultat"
)
public class ResponseError extends ResponseBean {
	@XmlElement(name = "code")
	private int code;
	private String message;

	public ResponseError() {
	}

	public void setCodeMessage( ErreurEnum valeurCode, String valeurMessage) {
		this.code = valeurCode.getValeur();
		this.message = valeurMessage;
	}

	public int getCode() {
		return this.code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String valeur) {
		this.message = valeur;
	}

	@Override
	public String toString() {
		return MessageFormat.format("<erreur><code>{0}</code><message>{1}</message></erreur>", code, message);
	}

}
