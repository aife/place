/**
 * 
 */
package fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions;

/**
 * @author KBE
 * 
 */
public class GlobaleException extends Throwable {

    /**
     * @param cause
     */
    public GlobaleException(Throwable cause) {
	super(cause);
	// TODO Auto-generated constructor stub
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    protected String message;

    /**
     * @return the message
     */
    public String getMessage() {
	return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
	this.message = message;
    }

    public GlobaleException() {

    }

    public GlobaleException(String message) {
	this.message = message;
    }

    public GlobaleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause);
	// TODO Auto-generated constructor stub
    }

    public GlobaleException(Throwable cause, String message) {
	super(cause);
	this.message = message;
	// TODO Auto-generated constructor stub
    }

}
