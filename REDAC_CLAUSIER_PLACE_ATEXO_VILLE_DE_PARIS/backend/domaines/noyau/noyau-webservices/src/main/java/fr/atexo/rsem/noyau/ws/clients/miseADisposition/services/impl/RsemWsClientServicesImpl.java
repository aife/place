package fr.atexo.rsem.noyau.ws.clients.miseADisposition.services.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBException;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions.RsemWsClientGlobaleException;
import fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions.RsemWsClientIterationException;
import fr.atexo.rsem.noyau.ws.clients.miseADisposition.services.RsemWsClientServices;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.DocumentMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.TokenBean;

/**
 * @author KHALED BENARI
 * @since 29/07/2013
 */
public class RsemWsClientServicesImpl extends RsemWsClientServices {

    private static final String REST = "rest";
    private static final String TICKET = "ticket";
    private static Logger logger = LoggerFactory.getLogger(RsemWsClientServicesImpl.class);

    // injectés par spring
    protected String repertoireFichiersTemporaires;
    protected String baseUri;
    protected static final String urlMiseAdisposition = "miseAdisposition";
    protected static final String LISTE_MISE_A_DISPOSITION_WEB_METHOD = "listeMisADisposition";
    protected static final String WEB_SERVICE_AUTHENTIFICATION = "authentification";
    protected static final String LISTE_DOCUMENT_MISE_A_DISPOSITION_WEB_METHOD = "archive";
    protected static final String CONNEXION = "connexion";
    protected static final String DECONNEXION = "deconnexion";

    /**
     * appele un url du genre
     * http://localhost:8080/epm.noyau/rest/miseAdisposition
     * /listemiseadisposition?ticket=token
     */
    @Override
    public ListMiseAdispositionBean getMiseADispositionList(String token, String applicationTiers) throws RsemWsClientGlobaleException {
        ListMiseAdispositionBean result = null;
        try {
            String xmlString = getResource().path(REST).path(urlMiseAdisposition).path(LISTE_MISE_A_DISPOSITION_WEB_METHOD).queryParam(TICKET, token).queryParam("applicationTiers", applicationTiers)
                    .accept(MediaType.APPLICATION_XML).get(String.class);
            if (xmlString == null || xmlString.isEmpty()) {
                throw new RsemWsClientGlobaleException(" le message renvoyer par le webservices Rest est null");
            }
            ByteArrayInputStream input = new ByteArrayInputStream(xmlString.getBytes());
            result = RsemWsClientUtils.parse(input, ListMiseAdispositionBean.class);
        } catch (JAXBException e) {
            RsemWsClientGlobaleException rsemWsClientGlobaleException = new RsemWsClientGlobaleException(e.fillInStackTrace(),
                    "Lors du creation de l'objet JAXB ListMiseAdispositionBean à partir du webservices REST");
            throw rsemWsClientGlobaleException;
        } catch (UniformInterfaceException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        }
        return result;

    }

    /**
     * appele un url du genre
     * http://localhost:8080/epm.noyau/rest/miseAdisposition/archive/<numero
     * mise a disposition>
     */
    @Override
    public ListDocumentsMiseAdispositionBean getDocumentsDeLaMiseADisposition(String miseAdisposition, String token) throws RsemWsClientIterationException {
        ClientResponse response = null;
        try {
            response = getResource().path(REST).path(urlMiseAdisposition).path(LISTE_DOCUMENT_MISE_A_DISPOSITION_WEB_METHOD).path(miseAdisposition).queryParam(TICKET, token).get(ClientResponse.class);
        } catch (UniformInterfaceException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientIterationException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        }

        if (response.getStatus() != 200) {
            try {
                throw new RsemWsClientIterationException("Erreur de connection avec le serveur RSEM " + " " + response.toString() + " " + IOUtils.toString(response.getEntityInputStream()));
            } catch (IOException e) {
            }
        } else {
            logger.info("recuperer la liste des document depuis :{} ", getResource().path(REST).path(urlMiseAdisposition).path(LISTE_DOCUMENT_MISE_A_DISPOSITION_WEB_METHOD).path(miseAdisposition)
                    .toString());
        }
        // copier le fichier temporaire
        File archive;
        try {
            archive = copierArchiveFichierTemporaire(response.getEntityInputStream(), miseAdisposition);
        } catch (FileNotFoundException e) {
            logger.error("impossible de creer ou d'acceder le fichier temporaire pour stocker l'archive pour la mise à disposition " + miseAdisposition, e.fillInStackTrace());
            throw new RsemWsClientIterationException(e, "impossible de creer ou d'acceder le fichier temporaire pour stocker l'archive pour la mise à disposition " + miseAdisposition);

        } catch (IOException e) {
            logger.error("impossible de copier le flux de l'archive dans le fichier temporaire  pour la mise à disposition " + miseAdisposition, e.fillInStackTrace());
            throw new RsemWsClientIterationException(e, "impossible de copier le flux de l'archive dans le fichier temporaire  pour la mise à disposition " + miseAdisposition);
        } catch (Exception e) {
            logger.error("le fichier zip n'a pas pu etre lu pour la mise à disposition " + miseAdisposition, e.fillInStackTrace());
            throw new RsemWsClientIterationException(e, "le fichier zip n'a pas pu etre lu pour la mise à disposition " + miseAdisposition);
        }
        ListDocumentsMiseAdispositionBean listDocumentDeLaMisedispositionBean;

        try {
            logger.info("archive Path :{}", archive);

            listDocumentDeLaMisedispositionBean = unzip(archive, miseAdisposition);
        } catch (IOException e) {
            throw new RsemWsClientIterationException(e, "impossible de creer le fichier temporaire du xml de description pour la mise à disposition " + miseAdisposition);
        } catch (JAXBException e) {
            throw new RsemWsClientIterationException(e, "erreur lors de la creation de l'objet ListDocumentDeLaMisedispositionBean");
        } catch (Exception e) {
            logger.error("le fichier zip n'a pas pu etre lu pour la mise à disposition " + miseAdisposition, e.fillInStackTrace());
            throw new RsemWsClientIterationException(e, "le fichier zip n'a pas pu etre lu pour la mise à disposition " + miseAdisposition);
        }

        listDocumentDeLaMisedispositionBean.setArchive(archive);
        return listDocumentDeLaMisedispositionBean;
    }

    /**
     * cette methode construit le bean list ListDocumentDeLaMisedispositionBean
     * à partir du fichier XML inclus dans le zip
     */
    public ListDocumentsMiseAdispositionBean unzip(File fichierZip, String miseAdisposition) throws Exception {
        ListDocumentsMiseAdispositionBean listDocumentDeLaMisedispositionBean = null;
        ZipFile zipFile = null;
        File xmlFile = null;
        boolean zipCorrompu = true;
        Map<String, File> fichierMap = new HashMap<String, File>();
        try {

            zipFile = new ZipFile(fichierZip, "cp437");
            File repertoireDestination = new File(repertoireFichiersTemporaires);
            if (!repertoireDestination.exists()) {
                repertoireDestination.mkdirs();
            }

            Enumeration<ZipArchiveEntry> fichiers = zipFile.getEntries();
            while (fichiers.hasMoreElements()) {
                zipCorrompu = false;
                ZipArchiveEntry zipArchiveEntry = fichiers.nextElement();
                String nomFichier = zipArchiveEntry.getName();
                File fichierDestination = new File(repertoireDestination, nomFichier);

                FileOutputStream fichierDestinationOutputStream = null;
                InputStream contenuFichierZip = null;
                try {
                    fichierDestinationOutputStream = new FileOutputStream(fichierDestination);
                    contenuFichierZip = zipFile.getInputStream(zipArchiveEntry);
                    IOUtils.copy(contenuFichierZip, fichierDestinationOutputStream);
                } finally {
                    IOUtils.closeQuietly(contenuFichierZip);
                    IOUtils.closeQuietly(fichierDestinationOutputStream);
                }
                if (nomFichier.endsWith("_ListDocuments.xml")) {
                    xmlFile = fichierDestination;
                 } else {
                    fichierMap.put(nomFichier, fichierDestination);
                }
            }
            if (zipCorrompu) {
                throw new Exception("le fichier zip est corrompu miseAdisposition id " + miseAdisposition);
            }
            if (xmlFile == null) {
                throw new Exception("le fichier XML est non present");
            } else {
                listDocumentDeLaMisedispositionBean = RsemWsClientUtils.parse(new FileInputStream(xmlFile), ListDocumentsMiseAdispositionBean.class);
                for (DocumentMiseAdispositionBean document : listDocumentDeLaMisedispositionBean.getDocumentsList()) {
                    document.setPathFichier(fichierMap.get(document.getNomDeFichier()));
                }
            }
            return listDocumentDeLaMisedispositionBean;
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * cette méthode va copier le zip depuis l'url et le mettre dans un fichier
     * zip sous: <fichier temporraire>/miseadisposition_<random number>.zip
     */
    private File copierArchiveFichierTemporaire(InputStream inputstream, String miseAdisposition) throws FileNotFoundException, IOException {
        final long entier = Calendar.getInstance().getTimeInMillis();
        File tempFile = new File(repertoireFichiersTemporaires + "/" + miseAdisposition + "_" + entier + ".zip");
        if(!tempFile.exists()) {
            logger.info("Repertoire invalide : " + repertoireFichiersTemporaires + "/" + miseAdisposition + "_" + entier + ".zip");
        }
        OutputStream out = null;

        try {
            out = new FileOutputStream(tempFile);
            IOUtils.copy(inputstream, out);
        } finally {
            out.close();
            inputstream.close();
        }
        return tempFile;
    }

    /**
     * acquitte une mise a disposition au niveau de RSEM(non-Javadoc)
     */
    public AcquitementMiseAdispositionBean acquiteMiseADisposition(AcquitementMiseAdispositionBean acquitementMiseAdispositionBean, String token) throws RsemWsClientIterationException,
            RsemWsClientGlobaleException {
        AcquitementMiseAdispositionBean result = null;
        try {
            String xmlString = getResource().path(REST).path(urlMiseAdisposition).path(acquitementMiseAdispositionBean.getMiseAdisposition()).path(acquitementMiseAdispositionBean.getStatut())
                    .queryParam(TICKET, token).accept(MediaType.TEXT_XML).get(String.class);
            if (xmlString == null || xmlString.isEmpty()) {
                throw new RsemWsClientGlobaleException(" le message renvoyer par le webservices Rest est null");
            }
            ByteArrayInputStream input = new ByteArrayInputStream(xmlString.getBytes());
            result = RsemWsClientUtils.parse(input, AcquitementMiseAdispositionBean.class);
        } catch (JAXBException e) {
            RsemWsClientGlobaleException rsemWsClientGlobaleException = new RsemWsClientGlobaleException(e.fillInStackTrace(),
                    "Lors du creation de l'objet JAXB ListMiseAdispositionBean à partir du webservices REST");
            throw rsemWsClientGlobaleException;
        } catch (UniformInterfaceException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        }
        return result;
    }

    @Override
    public TokenBean connexion(String loginUtilisateur, String motDePasse) throws RsemWsClientGlobaleException {
        try {
            String xmlString = getResource().path(REST).path(WEB_SERVICE_AUTHENTIFICATION).path(CONNEXION).path(loginUtilisateur).path(motDePasse).accept(MediaType.APPLICATION_XML).get(String.class);

            ByteArrayInputStream input = new ByteArrayInputStream(xmlString.getBytes());
            if (xmlString.contains("ticket")) {
                return RsemWsClientUtils.parse(input, TokenBean.class);
            } else {
                logger.error("Erreur de connection avec le serveur RSEM :" + xmlString);
                RsemWsClientGlobaleException rsemWsClientGlobaleException = new RsemWsClientGlobaleException("Erreur de connection avec le serveur RSEM");
                throw rsemWsClientGlobaleException;
            }
        } catch (JAXBException e) {
            RsemWsClientGlobaleException rsemWsClientGlobaleException = new RsemWsClientGlobaleException(e.fillInStackTrace(),
                    "Lors du creation de l'objet JAXB TokenBean à partir du webservices REST");
            throw rsemWsClientGlobaleException;
        } catch (UniformInterfaceException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        } catch (ClientHandlerException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        }

    }

    @Override
    public String deconnexion(String token) throws RsemWsClientGlobaleException {
        try {
            String xmlString = getResource().path(REST).path(WEB_SERVICE_AUTHENTIFICATION).path(DECONNEXION).queryParam(TICKET, token).accept(MediaType.APPLICATION_XML).get(String.class);
            return xmlString;
        } catch (UniformInterfaceException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        } catch (ClientHandlerException e) {
            logger.error("Erreur de connection avec le serveur RSEM", e.fillInStackTrace());
            throw new RsemWsClientGlobaleException(e.fillInStackTrace(), "Erreur de connection avec le serveur RSEM");
        }
    }

    /**
     * recuperer la base url exemple http://host_wb:port/nom_appli
     */
    protected WebResource getResource() {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        URI uri = UriBuilder.fromUri(baseUri).build();

        return client.resource(uri);
    }

    public final void setBaseUri(String value) {
        baseUri = value;
    }

    public void setRepertoireFichiersTemporaires(String value) {
        this.repertoireFichiersTemporaires = value;
    }

}