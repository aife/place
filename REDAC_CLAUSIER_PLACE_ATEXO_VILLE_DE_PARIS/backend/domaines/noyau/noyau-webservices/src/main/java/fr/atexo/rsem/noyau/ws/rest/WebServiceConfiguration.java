package fr.atexo.rsem.noyau.ws.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("rest")
public class WebServiceConfiguration extends Application {
}
