package fr.atexo.rsem.noyau.ws.rest.metier.mpe;

import com.atexo.redaction.domaines.document.mpe.v2.RedactionType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "redaction")
public class Redaction extends RedactionType {
}
