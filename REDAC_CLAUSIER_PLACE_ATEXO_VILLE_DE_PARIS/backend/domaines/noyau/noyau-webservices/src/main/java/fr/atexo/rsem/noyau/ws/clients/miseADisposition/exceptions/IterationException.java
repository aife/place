/**
 * 
 */
package fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions;

/**
 * @author KBE
 * 
 */
public class IterationException extends Throwable {

	protected String message;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
     * 
     */
	public IterationException() {

	}

	/**
	 * @param message
	 * @param cause
	 */
	public IterationException(Throwable cause, String message) {
		super(cause);
		this.setMessage(message);
	}

	/**
	 * @param message
	 */
	public IterationException(String message) {
		this.setMessage(message);
	}

	/**
	 * @param cause
	 */
	public IterationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
