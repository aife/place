package fr.atexo.rsem.noyau.ws.rest;

import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.synchronisationSocle.Synchronisation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Web service REST de recuperation des depots et pour la récuperation des
 * marchés
 *
 * @author RVI, RDA
 * @version $Revision$, $Date$, $Author$
 */
@Controller
@RequestMapping("/synchronisation")
public class SynchroRestWebService {

	private final Logger LOG = LoggerFactory.getLogger(SynchroRestWebService.class);

	@Autowired
	private Synchronisation synchronisation;

	/**
	 * Synchronisation
	 *
	 * @return la réṕonse
	 *
	 * @throws TechnicalNoyauException en cas d'erreur technique
	 * @throws ApplicationNoyauException en cas d'erreur métier
	 */
	@GetMapping(value =  {"/start", "/restart"}, consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> restart( )
		throws TechnicalNoyauException, ApplicationNoyauException {

		synchronisation.synchronize();

		return ResponseEntity.ok("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
				"<synchronisation>OK</synchronisation>");
	}
}
