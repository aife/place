package fr.atexo.rsem.noyau.ws.rest;

import fr.paris.epm.utilitaire.Config;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.List;
import java.util.Properties;

public class ConfigurationWebServices extends PropertySourcesPlaceholderConfigurer {

	/**
	 * Liste des noms de fichier de configuration.
	 */
	private static List<String> filename;

	/**
	 * Chargement des proprietes de configuration depuis les fichiers de configuration.
	 * @throws BeansException
	 */
	public void postProcessBeanFactory( ConfigurableListableBeanFactory valeur) {
		// Creation du conteneur des proprietes de configuration
		Config.init(filename);
		Properties props = Config.getProperties();
		if(props != null) {
			Properties[] propertiesArray = new Properties[1];
			propertiesArray[0] = props;
			setPropertiesArray(propertiesArray);
		}

		super.postProcessBeanFactory(valeur);
	}

	/**
	 * @param filename Liste des noms de fichier de configuration.
	 */
	public void setFilename(List filename) {
		this.filename = filename;
	}
}
