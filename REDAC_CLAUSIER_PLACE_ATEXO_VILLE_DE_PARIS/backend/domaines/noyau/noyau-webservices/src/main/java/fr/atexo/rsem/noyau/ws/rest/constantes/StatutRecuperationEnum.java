package fr.atexo.rsem.noyau.ws.rest.constantes;

public enum StatutRecuperationEnum {

    ATTENTE(1),ACQUITTE(2),ECHOUE(3);

    private int code;

    private StatutRecuperationEnum(int code){
        this.code=code;
    }

    public int getCode(){
        return this.code;
    }

    public static int getCode(String statut) {
        int result = 0;
        for (StatutRecuperationEnum value: StatutRecuperationEnum.values()){
            if (value.name().equalsIgnoreCase(statut)){
                result= value.getCode();
                break;
            }
        }
        return result;
    }

}