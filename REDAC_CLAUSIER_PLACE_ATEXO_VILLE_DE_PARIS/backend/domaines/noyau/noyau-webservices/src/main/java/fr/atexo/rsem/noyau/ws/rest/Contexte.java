package fr.atexo.rsem.noyau.ws.rest;

import java.util.UUID;

public class Contexte {
	private UUID uuid;
	private String url;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid( UUID uuid ) {
		this.uuid = uuid;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl( String url ) {
		this.url = url;
	}
}
