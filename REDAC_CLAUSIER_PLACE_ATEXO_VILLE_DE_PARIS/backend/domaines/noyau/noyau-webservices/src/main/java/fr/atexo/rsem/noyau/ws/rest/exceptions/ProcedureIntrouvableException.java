package fr.atexo.rsem.noyau.ws.rest.exceptions;

import org.slf4j.helpers.MessageFormatter;

public class ProcedureIntrouvableException extends RuntimeException {

	public ProcedureIntrouvableException( String code, String type ) {
		super(MessageFormatter.format("La procedure avec le code externe '{}' et le libellé court '{}' est introuvable dans le module de redaction", code, type).getMessage());
	}
}
