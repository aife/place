package fr.atexo.rsem.noyau.ws.util;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



public class MultipartRequest {
    public static final String DASH_DASH = "--";
    public static final String BOUNDARY = "7d22bb3b890472";
    public static final String DASH_BOUNDARY = DASH_DASH + BOUNDARY;
    private static final String CONTENT_DISPOSITION = "Content-Disposition: form-data; name=\"";
    private static final String FIN_FICHIER = "\"";
    private static final String LIGNE = "\r\n";
    private static final String FIN = DASH_BOUNDARY + "--";

    private OutputStream outputStream;

    public MultipartRequest(OutputStream outputStream) throws IOException {
        this.outputStream = outputStream;
        outputStream.write(LIGNE.getBytes());
    }
    
    /**
     * Ajoute un fichier afin de pouvoir l'envoyer.
     *
     * @param nom     le nom du paramètre
     * @param fichier le fichier à ajouter
     * @throws IOException
     */
    public void ajouterFichier(String nom, File fichier) throws IOException {
        preparerMetadonneesFichier(nom, nom);
        outputStream.write(LIGNE.getBytes());
        IOUtils.copy(new FileInputStream(fichier), outputStream);
        outputStream.write(LIGNE.getBytes());
        outputStream.write(LIGNE.getBytes());

    }

    /**
     * Prépare les méta-données pour l'envoi de fichier.
     *
     * @param nom        le nom du paramétre
     * @param nomFichier le nom du fichier
     * @throws IOException
     */
    private void preparerMetadonneesFichier(String nom, String nomFichier) throws IOException {
        outputStream.write(DASH_BOUNDARY.getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write((CONTENT_DISPOSITION + nom + FIN_FICHIER).getBytes());
        outputStream.write(("; filename=\"" + nomFichier + "\"" + LIGNE).getBytes());
        outputStream.write(("Content-Type: application/octet-stream" + LIGNE).getBytes());
        outputStream.write(("Content-Transfer-Encoding: base64" + LIGNE).getBytes());
//      outputStream.write(LIGNE.getBytes());
    }

    /**
     * Prépare les méta-données pour l'envoi d'informations.
     *
     * @param nom    le nom du paramétre
     * @param valeur la valeur de ce paramétre
     * @throws IOException
     */
    public void ajouterContenu(String nom, String valeur) throws IOException {
        outputStream.write(LIGNE.getBytes());
        outputStream.write(FIN.getBytes());
        outputStream.write(BOUNDARY.getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write((CONTENT_DISPOSITION + nom + FIN_FICHIER).getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write(valeur.getBytes());
        outputStream.write(LIGNE.getBytes());
    }

    public void ajouterContenu(String nom, Integer valeur) throws IOException {
        ajouterContenu(nom, valeur != null ? String.valueOf(valeur) : null);
    }

    /**
     * Permet d'envoyer la requête au serveur
     *
     * @return le retour de la reponse serveur
     * @throws IOException
     */
    public void fin() throws IOException {
        outputStream.write(FIN.getBytes());
        outputStream.flush();
        outputStream.close();
    }
}

