package fr.atexo.rsem.noyau.ws.clients.miseADisposition.services;

import fr.atexo.rsem.noyau.ws.beans.miseADisposition.AcquitementMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListDocumentsMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.ListMiseAdispositionBean;
import fr.atexo.rsem.noyau.ws.beans.miseADisposition.TokenBean;
import fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions.RsemWsClientGlobaleException;
import fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions.RsemWsClientIterationException;

public abstract class RsemWsClientServices {
   
   
    // les méthodes abstraits
  
    /**
     * cette méthode va nous permettre à se connecter au webservices de rsem et d'obtenir un token
     * @param idUtilisateur
     * @param motDePAsse
     * @return TokenBean le token  {@link fr.atexo.rsem.noyau.ws.beans.miseADisposition.atexo.connecteur.rsemwsclient.ws.bean.TokenBean}
     * @throws RsemWsClientIterationException
     */
    public abstract TokenBean connexion(String loginUtilisateur,String motDePasse)throws RsemWsClientGlobaleException;
    
    /**
     * cette méthode va nous permettre de se deconnecter de RSEM 
     * @param token
     * @throws RsemWsClientIterationException
     * @return
     */
     
    public abstract String deconnexion(String token)throws RsemWsClientGlobaleException;
    
    /**
     * cette méthode va appeler la web méthode qui liste les mises à disposition disponibles  pour le transfert
     * @return
     * @throws RsemWsClientGlobaleException
     */
    public abstract ListMiseAdispositionBean getMiseADispositionList(String token, String applicationTiers)
            throws RsemWsClientGlobaleException;
    /**
     * cette méthode va appeler la web méthode qui génere l'archive des documents d'une mise à disposition à transferer 
     * @param miseAdisposition
     * @return
     * @throws RsemWsClientIterationException
     */
    public abstract ListDocumentsMiseAdispositionBean getDocumentsDeLaMiseADisposition(
            String miseAdisposition, String token) throws RsemWsClientIterationException;

    /**
     * cette méthode va rendre la reponse transfert  à rsem 
     * @param acquitementMiseAdispositionBean
     * @return
     * @throws RsemWsClientIterationException
     */
    public abstract AcquitementMiseAdispositionBean acquiteMiseADisposition(
            AcquitementMiseAdispositionBean acquitementMiseAdispositionBean, String token)throws RsemWsClientIterationException, RsemWsClientGlobaleException;

  
    
}
