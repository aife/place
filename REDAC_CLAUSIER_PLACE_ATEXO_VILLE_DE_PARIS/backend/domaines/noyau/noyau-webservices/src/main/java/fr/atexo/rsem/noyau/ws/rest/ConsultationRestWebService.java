package fr.atexo.rsem.noyau.ws.rest;

import com.atexo.redaction.domaines.document.mpe.v2.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.atexo.rsem.noyau.ws.beans.ErreurEnum;
import fr.atexo.rsem.noyau.ws.rest.constantes.ConstantesRestWebService;
import fr.atexo.rsem.noyau.ws.rest.exceptions.ProcedureIntrouvableException;
import fr.atexo.rsem.noyau.ws.rest.metier.InitialisationConsultationWebServiceGIM;
import fr.atexo.rsem.noyau.ws.rest.metier.mpe.*;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.*;
import fr.paris.epm.noyau.metier.objetvaleur.UtilisateurExterneConsultation;
import fr.paris.epm.noyau.metier.technique.TableStockageUtilisateurExterneEPM;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.redaction.EpmTConsultationContexte;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefDirectionService;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefOrganisme;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefParametrage;
import fr.paris.epm.noyau.persistance.referentiel.EpmTRefProcedure;
import fr.paris.epm.noyau.service.technique.Credentials;
import fr.paris.epm.noyau.service.technique.OauthAccessTokenHolder;
import fr.paris.epm.noyau.service.technique.RestService;
import org.hibernate.NonUniqueResultException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Web service REST de recuperation des depots et pour la récuperation des
 * marchés
 *
 * @author RVI, RDA
 * @version $Revision$, $Date$, $Author$
 */
@Controller
@RequestMapping("/consultation")
public class ConsultationRestWebService {

    private final Logger LOG = LoggerFactory.getLogger(ConsultationRestWebService.class);

    @Autowired
    private GeneriqueDAO generiqueDAO;

    private final TableStockageUtilisateurExterneEPM tableStockageUtilisateurExterneEPM = TableStockageUtilisateurExterneEPM.getInstance();

    @Bean
    public static PropertySourcesPlaceholderConfigurer load() {
        return new ConfigurationWebServices();
    }

    @Autowired
    private InitialisationConsultationWebServiceGIM initialisationConsultationWebServiceGIM;

    @Value("${document.www.url}")
    private String documentUrl;

    @Value("${document.contexte.creation.url}")
    private String documentContexteUrl;

    @Value("${document.agent.creation.url}")
    private String documentAgentUrl;

    @Value("${plateforme}")
    private String plateforme;

    @Value("${mpe.client}")
    private String mpePlateformeUuid;

    @Value("${client}")
    private String client;

    @Value("${document.canevas.generation.url}")
    private String urlGenerationCanevas;

    @Value("${document.contexte.enregistrement.url}")
    private String urlEnregistrementContexte;

    @Value("${document.consultation.recuperation.url}")
    private String urlRecuperationConsultation;

    @Value("${document.canevas.recherche.url}")
    private String urlRechercheCanevas;

    @Value("${document.agent.recherche.url}")
    private String urlRechercheAgents;

    @Value("${document.visualisation.consultation}")
    private String urlVisualisationConsultation;
    @Value("${synchro.agent.url}")
    private String urlMpe;
    @Value("${clausier.agent.url}")
    private String apiAgentUrl;
    @Value("${synchro.login}")
    private String login;
    @Value("${synchro.password}")
    private String password;

    @Value("${version}")
    private String version;

    @Autowired
    private OauthAccessTokenHolder oauthAccessTokenHolder;

    @Autowired
    private RestService restService;

    public static Object unmarshallXmlString(String xmlString, Class<?> clazz) {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(xmlString);
            return unmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ObjectMapper getMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        //   objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        return objectMapper;
    }

    /**
     * Initialisation de la consultation
     *
     * @param token le jeton de sécurité
     * @return la réṕonse
     * @throws TechnicalNoyauException   en cas d'erreur technique
     * @throws ApplicationNoyauException en cas d'erreur métier
     */
    @PutMapping(value = "/initialisation", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> initialisation(@RequestBody final String initialisation, @RequestParam(ConstantesRestWebService.XML_QNAME_TOKEN) final String token) throws TechnicalNoyauException, ApplicationNoyauException {
        var redactionType = (Redaction) unmarshallXmlString(initialisation, Redaction.class);
        LOG.info("Flux envoyé par MPE = {}", toJSON(redactionType));

        final ResponseEntity<String> verification = verificationToken(token);

        if (verification.getStatusCode() != HttpStatus.OK) {
            return verification;
        }

        final com.atexo.redaction.domaines.document.mpe.v2.InitialisationType initialisationType = redactionType.getInitialisation();
        final com.atexo.redaction.domaines.document.mpe.v2.Utilisateur utilisateurExterne = initialisationType.getUtilisateur();
        final com.atexo.redaction.domaines.document.mpe.v2.Consultation consultation = initialisationType.getConsultation();
        String base = null;
        try {
            URL url = new URL(urlMpe);
            base = url.getProtocol() + "://" + url.getHost();

        } catch (MalformedURLException e) {
            LOG.error("Erreur lors de la construction de l'url du serveur MPE", e);
        }

        final UtilisateurExterneConsultation utilisateurExterneConsultation = new UtilisateurExterneConsultation();

        //ajouter plateforme_uuid au contexte
        String plateformeUuid = initialisationType.getUtilisateur().getPlateformeUuid();
        if (plateformeUuid == null) {
            plateformeUuid = mpePlateformeUuid;
        }


        // Creation de l'utilisateur temporaire
        final EpmTUtilisateur utilisateur = rechercheUtilisateurExterne(utilisateurExterne, plateformeUuid);

        if (utilisateur == null) {
            final String messageErreur = "Utilisateur " + utilisateurExterne.getNom() + " inconnu.";
            LOG.error(messageErreur);
            return AuthentificationRestWebService.error(ErreurEnum.ENTITE_NON_TROUVEE, messageErreur);
        }

        Contexte contexte = null;
        Agent agent = null;
        // Mise a jour ou creation de la consultation associee
        Credentials credentials = renouvellerJetonOAuth(utilisateurExterneConsultation);
        String accessToken = credentials.getAccessToken();
        agent = envoyerAgentDocument(utilisateurExterneConsultation, accessToken, redactionType, utilisateur, plateformeUuid, base);

        if (null == agent) {
            LOG.info("Tentative de renvoi de l'agent. Regénération du jeton");

            credentials = renouvellerJetonOAuth(utilisateurExterneConsultation);

            agent = envoyerAgentDocument(utilisateurExterneConsultation, accessToken, redactionType, utilisateur, plateformeUuid, base);
        }

        try {
            contexte = envoyerConsultation(utilisateurExterneConsultation, accessToken, redactionType, contexte, plateformeUuid, base);
        } catch (ProcedureIntrouvableException pie) {
            return AuthentificationRestWebService.error(ErreurEnum.ENTITE_NON_TROUVEE, pie.getMessage());
        }
        if (consultation != null) {
            if (contexte == null) {
                return AuthentificationRestWebService.error(ErreurEnum.ENTITE_NON_TROUVEE, "Impossible de joindre redacdocument. Merci de contacter votre support");
            }
            try {
                EpmTConsultationContexte epmTConsultationContexte = addConsultationContexte(plateformeUuid, consultation, utilisateur, initialisation);
                utilisateurExterneConsultation.setConsultation(epmTConsultationContexte);
                LOG.info("Contexte de consultation enregistré en base de données pour la consultation {} / {}", epmTConsultationContexte.getReference(), epmTConsultationContexte.getPlateforme());
            } catch (Exception e) {
                LOG.error("Erreur lors de la sauvegarde du contexte de consultation", e);
                final String messageErreur = "Probleme lors de la creation ou mise a jour de la consultation (Reference externe : " + consultation.getReferenceExterne() + " / numero de consultation " + consultation.getNumeroConsultation() + "). " + e.getMessage();
                return AuthentificationRestWebService.error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, messageErreur);
            }
        }



        //Les plateformes MPE et RSEM doivent être paramétrés de la même facon editeur/administration
        final ParametrageCritere parametrageCritere = new ParametrageCritere();
        parametrageCritere.setClef("plateforme.editeur");
        final EpmTRefParametrage refParametrage = generiqueDAO.findUniqueByCritere(parametrageCritere);
        final String plateformeEditeur;
        if (refParametrage != null && refParametrage.getValeur().equals("1")) {
            plateformeEditeur = "true";
        } else if (refParametrage != null && refParametrage.getValeur().equals("0")) {
            plateformeEditeur = "false";
        } else {
            plateformeEditeur = "";
        }

        if (!utilisateurExterne.getConfiguration().getPlateformeEditeur().equals(plateformeEditeur)) {
            final String messageErreur = "Les plateformes MPE et RSEM sont paramétrées différemment, " + "elles doivent concorder sur le mode editeur (plateforme.editeur = 1), " + "administratif (plateforme.editeur = 0) ou aucun (plateforme.editeur = 2/null) ";
            LOG.error(messageErreur);
            return AuthentificationRestWebService.error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, messageErreur);
        }


        try {
            if (null != utilisateur.getProfil()) {
                utilisateur.getProfil().clear();
            }



            // creation du profil
            final EpmTProfil profileTemporaire = new EpmTProfil();
            profileTemporaire.setNom("ProfilTemporaireAccesExterneRedaction");
            if (utilisateurExterne.getHabilitations() != null) {
                final Collection<EpmTHabilitation> habilitations = generiqueDAO.findAll(EpmTHabilitation.class);
                Set<EpmTHabilitation> habilitationAssocies = utilisateurExterne.getHabilitations().getHabilitation().stream().map(habilitationInitialisation -> habilitations.stream().filter(epmTHabilitation -> epmTHabilitation.getRole().contains(habilitationInitialisation)).findFirst().orElse(null)).filter(Objects::nonNull).collect(Collectors.toSet());
                profileTemporaire.setHabilitationAssocies(habilitationAssocies);
            }

            final EpmTProfilUtilisateur profilUtilisateur = new EpmTProfilUtilisateur();
            profilUtilisateur.setProfil(profileTemporaire);

            final Map<EpmTProfil, EpmTProfilUtilisateur> map = new HashMap<>();
            map.put(profileTemporaire, profilUtilisateur);
            utilisateur.setProfilUtilisateurMap(map);
        } catch (final TechnicalNoyauException | NonTrouveNoyauException e) {
            LOG.error(e.getMessage(), e);
        }

        utilisateurExterneConsultation.setUtilisateur(utilisateur);
        utilisateurExterneConsultation.setHabilitations(utilisateurExterne.getHabilitations().getHabilitation());

        if (null != credentials) {
            utilisateurExterneConsultation.setAccessToken(accessToken);
        }

        if (null != client) {
            utilisateurExterneConsultation.setClient(client);
        }


        if (null != contexte) {
            utilisateurExterneConsultation.setContexte(contexte.getUuid());
            utilisateurExterneConsultation.setDocumentUrl(documentUrl);
        }

        if (null != agent) {
            utilisateurExterneConsultation.setAgent(agent.getUuid());
        }

        if (plateformeUuid != null) {
            utilisateurExterneConsultation.setPlateformeUuid(plateformeUuid);
        }
        List<ProduitType> produits = new ArrayList<>();

        //ajouter les produits au contexte de l'utilisateur
        if (initialisationType.getProduits() != null && !CollectionUtils.isEmpty(initialisationType.getProduits().getProduit())) {
            produits = initialisationType.getProduits().getProduit();
        } else {
            ProduitType mpe = new ProduitType();
            mpe.setNom("MPE");
            mpe.setVersion(initialisationType.getVersion());
            mpe.setUrl(base);
            produits.add(mpe);

        }

        String refreshToken = credentials.getRefreshToken();
        utilisateurExterneConsultation.setIdContexte(createContext(base, utilisateurExterne.getApi(), initialisationType.getPlateforme(), accessToken, refreshToken, utilisateurExterneConsultation, produits));
        // Association identifiant externe au couple utilisateur externe /
        // consultation et sauvegarde en memoire
        tableStockageUtilisateurExterneEPM.stocker(accessToken, utilisateurExterneConsultation);

        return ResponseEntity.ok(MessageFormat.format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + "<identifiant>{0}</identifiant>", accessToken));
    }

    private EpmTConsultationContexte addConsultationContexte(String plateformeUuid, Consultation consultation, EpmTUtilisateur utilisateur, String initialisation) throws JsonProcessingException {
        ConsultationContexteCritere consultationContexteCritere = new ConsultationContexteCritere();
        consultationContexteCritere.setPlateforme(plateformeUuid);
        String referenceRSEM = consultation.getReferenceExterne().trim();
        consultationContexteCritere.setReference(referenceRSEM);

        EpmTConsultationContexte epmTConsultationContexte = generiqueDAO.findUniqueByCritere(consultationContexteCritere);
        if (epmTConsultationContexte == null) {
            epmTConsultationContexte = new EpmTConsultationContexte();
            epmTConsultationContexte.setContexte(getMapper().writeValueAsString(consultation));
            epmTConsultationContexte.setConsultationJson(getMapper().writeValueAsString(initialisationConsultationWebServiceGIM.getEpmTConsultation(consultation, utilisateur)));
            epmTConsultationContexte.setReference(referenceRSEM);
            epmTConsultationContexte.setIdUtilisateur(utilisateur.getId());
            epmTConsultationContexte.setXmlMpe(initialisation);
            epmTConsultationContexte.setPlateforme(plateformeUuid);
            epmTConsultationContexte = generiqueDAO.merge(epmTConsultationContexte);
        } else {
            epmTConsultationContexte.setContexte(getMapper().writeValueAsString(consultation));
            epmTConsultationContexte.setConsultationJson(getMapper().writeValueAsString(initialisationConsultationWebServiceGIM.getEpmTConsultation(consultation, utilisateur)));
            epmTConsultationContexte.setXmlMpe(initialisation);
            epmTConsultationContexte = generiqueDAO.merge(epmTConsultationContexte);
        }
        return epmTConsultationContexte;
    }

    private Contexte envoyerConsultation(UtilisateurExterneConsultation utilisateurExterneConsultation, String jeton, com.atexo.redaction.domaines.document.mpe.v2.RedactionType redactionType, Contexte contexte, String plateformeUuid, String mpeUrl) {
        try {
            contexte = envoyerConsultation(jeton, redactionType, plateformeUuid, mpeUrl);
        } catch (final HttpClientErrorException exception) {
            LOG.error("Erreur HTTP {} lors de l'envoi de l'agent vers le module document a l'URL '{}' avec le jeton '{}'", exception.getStatusCode(), documentContexteUrl, jeton);

            switch (exception.getStatusCode()) {
                case NOT_ACCEPTABLE:
                    utilisateurExterneConsultation.setErreur("Incompatibilité de version avec redacdocument: " + exception.getMessage());
                    break;
                case NOT_FOUND:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. L'url semble incorrecte");
                    break;
                case UNAUTHORIZED:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. Le jeton d'authentification semble KO");
                    break;
                case INTERNAL_SERVER_ERROR:
                    utilisateurExterneConsultation.setErreur("Une erreur s'est produite dans redacdocument lors de l'envoi de la consultation");
                    break;
                default:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument");
                    break;
            }

        }
        return contexte;
    }

    private Agent envoyerAgentDocument(UtilisateurExterneConsultation utilisateurExterneConsultation, String jeton, com.atexo.redaction.domaines.document.mpe.v2.RedactionType redactionType, EpmTUtilisateur utilisateur, String plateformeUuid, String mpeUrl) {
        Agent result = null;
        try {
            result = envoyerAgentDocument(jeton, redactionType.getInitialisation().getUtilisateur(), utilisateur, plateformeUuid, mpeUrl);
        } catch (final HttpClientErrorException exception) {
            LOG.error("Erreur HTTP {} lors de l'envoi de l'agent vers redacdocument a l'URL '{}' avec le jeton '{}'", exception.getStatusCode(), documentAgentUrl, jeton);

            switch (exception.getStatusCode()) {
                case NOT_ACCEPTABLE:
                    utilisateurExterneConsultation.setErreur("Incompatibilité de version avec redacdocument. Merci de contacter votre support");
                    break;
                case NOT_FOUND:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. L'url semble incorrecte. Merci de contacter votre support");
                    break;
                case UNAUTHORIZED:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. Le jeton d'authentification semble KO. Merci de contacter votre support");
                    break;
                case INTERNAL_SERVER_ERROR:
                    utilisateurExterneConsultation.setErreur("Une erreur s'est produite dans redacdocument lors de l'envoi de l'agent. Merci de contacter votre support");
                    break;
                default:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. Merci de contacter votre support");
                    break;
            }

        } catch (Exception e) {
            LOG.error("Erreur {} lors de l'envoi de l'agent vers redacdocument a l'URL '{}' avec le jeton '{}'", e.getMessage(), documentAgentUrl, jeton);
            utilisateurExterneConsultation.setErreur("Impossible de joindre redacdocument. Merci de contacter votre support");
        }
        return result;
    }

    private Credentials renouvellerJetonOAuth(final UtilisateurExterneConsultation utilisateurExterneConsultation) {
        Credentials result = null;
        try {
            result = oauthAccessTokenHolder.renew();
        } catch (final HttpClientErrorException exception) {
            LOG.error("Erreur HTTP {} lors du renouvellement du jeton oauth depuis le serveur d'autorisations (keycloak)", exception.getStatusCode());

            switch (exception.getStatusCode()) {
                case NOT_FOUND:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre le serveur d'autorisations (keycloak). L'url semble incorrecte. Merci de contacter votre support");
                    break;
                case UNAUTHORIZED:
                    utilisateurExterneConsultation.setErreur("Le login ou le mot de passe du serveur d'autorisations (keycloak) est incorrect. Merci de contacter votre support");
                    break;
                default:
                    utilisateurExterneConsultation.setErreur("Impossible de joindre  le serveur d'autorisations (keycloak). Merci de contacter votre support");
                    break;
            }

        }
        return result;
    }

    @GetMapping(value = "/creation")
    public ResponseEntity<String> creation(final String input) throws TechnicalNoyauException, ApplicationNoyauException {

        final ObjectMapper mapper = new ObjectMapper();

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        com.atexo.redaction.domaines.document.mpe.v2.RedactionType redactionType;
        try {
            redactionType = mapper.readValue(input, com.atexo.redaction.domaines.document.mpe.v2.RedactionType.class);
        } catch (final IOException e) {
            final String messageErreur = "Impossible de lire le xml du contexte";

            return AuthentificationRestWebService.error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, messageErreur);
        }

        final com.atexo.redaction.domaines.document.mpe.v2.InitialisationType initialisationType = redactionType.getInitialisation();
        final com.atexo.redaction.domaines.document.mpe.v2.Utilisateur utilisateurExterne = initialisationType.getUtilisateur();

        // Creation de l'utilisateur temporaire
        // Creation de l'utilisateur temporaire
        final EpmTUtilisateur utilisateur = rechercheUtilisateurExterne(utilisateurExterne, mpePlateformeUuid);
        if (utilisateur == null) {
            LOG.error("Utilisateur {} inconnu", utilisateurExterne.getNom());
            final String messageErreur = "Utilisateur " + utilisateurExterne.getNom() + " inconnu.";
            return AuthentificationRestWebService.error(ErreurEnum.ENTITE_NON_TROUVEE, messageErreur);
        }

        //Les plateformes MPE et RSEM doivent être paramétrés de la même facon editeur/administration
        final ParametrageCritere parametrageCritere = new ParametrageCritere();
        parametrageCritere.setClef("plateforme.editeur");
        final EpmTRefParametrage refParametrage = generiqueDAO.findUniqueByCritere(parametrageCritere);
        final String plateformeEditeur;
        if (refParametrage != null && refParametrage.getValeur().equals("1")) {
            plateformeEditeur = "true";
        } else if (refParametrage != null && refParametrage.getValeur().equals("0")) {
            plateformeEditeur = "false";
        } else {
            plateformeEditeur = "";
        }

        if (!utilisateurExterne.getConfiguration().getPlateformeEditeur().equals(plateformeEditeur)) {
            final String messageErreur = "Les plateformes MPE et RSEM sont paramétrées différemment, " + "elles doivent concorder sur le mode editeur (plateforme.editeur = 1), " + "administratif (plateforme.editeur = 0) ou aucun (plateforme.editeur = 2/null) ";
            LOG.error(messageErreur);
            return AuthentificationRestWebService.error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, messageErreur);
        }

        // Mise a jour ou creation de la consultation associee
        final com.atexo.redaction.domaines.document.mpe.v2.Consultation consultation = initialisationType.getConsultation();
        if (consultation != null) {
            try {
                EpmTConsultationContexte epmTConsultationContexte = addConsultationContexte(mpePlateformeUuid, consultation, utilisateur, input);
                LOG.info("Contexte de consultation enregistré en base de données pour la consultation {} / {}", epmTConsultationContexte.getReference(), epmTConsultationContexte.getPlateforme());
            } catch (Exception e) {
                LOG.error("Erreur lors de la sauvegarde du contexte de consultation", e);
                final String messageErreur = "Probleme lors de la creation ou mise a jour de la consultation (Reference externe : " + consultation.getReferenceExterne() + " / numero de consultation " + consultation.getNumeroConsultation() + "). " + e.getMessage();
                return AuthentificationRestWebService.error(ErreurEnum.TECHNIQUE_BASE_DE_DONNEES, messageErreur);
            }
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Envoi de l'utilisateur au nouveau REDAC
     *
     * @param utilisateur l'utilisateur
     */
    private Agent envoyerAgentDocument(final String jeton, final com.atexo.redaction.domaines.document.mpe.v2.Utilisateur utilisateurDTO, final EpmTUtilisateur utilisateur, String plateformeUuid, String mpeUrl) {
        final var agentDTO = new com.atexo.redaction.domaines.document.mpe.v2.Agent();

        if (null == utilisateur) {
            LOG.error("Aucun utilisateur trouvé donc impossible de l'envoyer au nouveau REDAC");

            throw new RuntimeException("Aucun utilisateur trouvé donc impossible de l'envoyer au nouveau REDAC");
        }
        // -------------
        // Environnement
        // -------------
        var environnementDTO = new com.atexo.redaction.domaines.document.mpe.v2.Environnement();
        environnementDTO.setVersion(version);
        environnementDTO.setPlateforme(plateforme);
        environnementDTO.setClient(client);
        environnementDTO.setMpeUuid(plateformeUuid);
        environnementDTO.setMpeUrl(mpeUrl);
        agentDTO.setEnvironnement(environnementDTO);

        final EpmTRefDirectionService directionService = getEpmTRefDirectionService(utilisateur, plateformeUuid);
        // ---------
        // Organisme
        // ---------
        EpmTRefOrganisme organisme = null;
        if (utilisateur.getIdOrganisme() != null) {
            final OrganismeCritere organismeCritere = new OrganismeCritere(plateformeUuid);
            organismeCritere.setIdOrganisme(utilisateur.getIdOrganisme());
            organisme = generiqueDAO.findUniqueByCritere(organismeCritere);
        }
        if (organisme == null) {
            organisme = directionService.getEpmTRefOrganisme();
        }
        if (null == organisme) {
            LOG.error("Impossible de trouver l'organisme pour l'id {}", utilisateur.getIdOrganisme());
            LOG.error("Envoi de l'organisme par le service {}", utilisateur.getDirectionService());
            final var organismeDTO = new Organisme();
            organismeDTO.setId(directionService.getId());
            organismeDTO.setAcronyme(directionService.getCodeExterne());
            organismeDTO.setLibelleCourt(directionService.getLibelleCourt());
            organismeDTO.setLibelleLong(directionService.getLibelle());
            agentDTO.setOrganisme(organismeDTO);
        } else {
            LOG.info("Organisme {} correctement récupéré", utilisateur.getIdOrganisme());
            agentDTO.setOrganisme(map(organisme));
        }


        agentDTO.setService(map(directionService));

        // -----------
        // Utilisateur
        // -----------
        agentDTO.setNom(utilisateurDTO.getNom());
        agentDTO.setPrenom(utilisateurDTO.getPrenom());
        agentDTO.setCourriel(utilisateurDTO.getCourriel());
        agentDTO.setIdentifiantExterne(utilisateurDTO.getIdentifiantExterne());
        agentDTO.setHabilitations(utilisateurDTO.getHabilitations());
        agentDTO.setConfiguration(utilisateurDTO.getConfiguration());

        // header keycloak
        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
        headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");
        final HttpEntity<com.atexo.redaction.domaines.document.mpe.v2.Agent> entity = new HttpEntity<>(agentDTO, headers);

        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(agentDTO);
        } catch (final JsonProcessingException e) {
            LOG.error("Impossible de convertir le payload en json", e);
        }

        LOG.debug("Envoi de l'utilisateur '{}' vers le nouveau REDAC a l'adresse {}", json, documentAgentUrl);

        var result = restService.getRestTemplate().postForEntity(documentAgentUrl, entity, Agent.class).getBody();

        LOG.info("Agent '{}' créé avec succès dans le module document avec l'identifiant {}", json, result.getUuid());

        return result;
    }

    private EpmTRefDirectionService getEpmTRefDirectionService(EpmTUtilisateur utilisateur, String plateformeUuid) {
        // -------
        // Service
        // -------
        final DirectionServiceCritere directionServiceCritere = new DirectionServiceCritere(plateformeUuid);
        directionServiceCritere.setIdDirectionsService(utilisateur.getDirectionService());
        final EpmTRefDirectionService directionService = generiqueDAO.findUniqueByCritere(directionServiceCritere);

        if (null == directionService) {
            LOG.error("Impossible de trouver le service pour l'id {}", utilisateur.getDirectionService());

            throw new RuntimeException("Impossible de trouver le service pour l'id " + utilisateur.getDirectionService());
        } else {
            LOG.info("Service {} correctement récupéré", utilisateur.getDirectionService());
        }
        return directionService;
    }

    private Organisme map(final EpmTRefOrganisme organisme) {
        final var organismeDTO = new Organisme();
        organismeDTO.setId(organisme.getId());
        organismeDTO.setAcronyme(organisme.getCodeExterne());
        organismeDTO.setLibelleCourt(organisme.getLibelleCourt());
        organismeDTO.setLibelleLong(organisme.getLibelle());
        return organismeDTO;
    }

    private Service map(final EpmTRefDirectionService directionService) {
        final var serviceDTO = new Service();
        serviceDTO.setId(directionService.getId());
        if (null != directionService.getEpmTRefOrganisme()) {
            serviceDTO.setIdOrganisme(directionService.getEpmTRefOrganisme().getId());
        }
        serviceDTO.setLibelleLong(directionService.getLibelle());
        serviceDTO.setLibelleCourt(directionService.getLibelleCourt());

        if (null != directionService.getParent()) {
            serviceDTO.setParent(this.map(directionService.getParent()));
        }

        return serviceDTO;
    }

    /**
     * Envoi du contexte au nouveau REDAC
     *
     * @param redactionType le contexte
     */
    private Contexte envoyerConsultation(final String jeton, final com.atexo.redaction.domaines.document.mpe.v2.RedactionType redactionType, String plateformeUuid, String mpeUrl) {
        final var contexteDTO = new com.atexo.redaction.domaines.document.mpe.v2.Contexte();
        final var environnementDTO = new com.atexo.redaction.domaines.document.mpe.v2.Environnement();
        environnementDTO.setPlateforme(plateforme);
        environnementDTO.setClient(client);
        environnementDTO.setVersion(version);
        environnementDTO.setMpeUuid(plateformeUuid);
        environnementDTO.setMpeUrl(mpeUrl);
        contexteDTO.setEnvironnement(environnementDTO);

        final var accesDTO = new com.atexo.redaction.domaines.document.mpe.v2.Acces();
        accesDTO.setUrlGenerationCanevas(urlGenerationCanevas);
        accesDTO.setUrlEnregistrementContexte(urlEnregistrementContexte);
        accesDTO.setUrlRecuperationConsultation(urlRecuperationConsultation);
        accesDTO.setUrlRechercheCanevas(urlRechercheCanevas);
        accesDTO.setUrlRechercheAgents(urlRechercheAgents);
        accesDTO.setUrlVisualisationConsultation(urlVisualisationConsultation);
        contexteDTO.setUrls(accesDTO);

        final var consultationMPE = redactionType.getInitialisation().getConsultation();

        contexteDTO.setConsultation(consultationMPE);

        if (null != consultationMPE) {
            LOG.debug("Recherche de la procedure avec code externe = '{}' et libelle court = '{}'", consultationMPE.getCodeExterneProcedure(), consultationMPE.getTypeProcedure());

            final var procedureCritere = new ProcedureCritere();
            procedureCritere.setCodeExterne(consultationMPE.getCodeExterneProcedure());
            procedureCritere.setLibelleCourt(consultationMPE.getTypeProcedure());
            final EpmTRefProcedure procedure = generiqueDAO.findUniqueByCritere(procedureCritere);

            if (null == procedure) {
                LOG.error("La procedure avec code externe = '{}' et libelle court = '{}' est introuvable!", consultationMPE.getCodeExterneProcedure(), consultationMPE.getTypeProcedure());

                throw new ProcedureIntrouvableException(consultationMPE.getCodeExterneProcedure(), consultationMPE.getTypeProcedure());
            }

            final var procedureDTO = new com.atexo.redaction.domaines.document.mpe.v2.ProcedureType();
            procedureDTO.setCodeExterne(procedure.getCodeExterne());
            procedureDTO.setLibelleCourt(procedure.getLibelleCourt());
            procedureDTO.setLibelleLong(procedure.getLibelle());
            procedureDTO.setType(procedure.getTypeProcedure());

            consultationMPE.setProcedure(procedureDTO);
        }

        // header keycloak
        final var headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, MessageFormat.format("Bearer {0}", jeton));
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.set(HttpHeaders.ACCEPT, "application/atexo.redacdocument.v2+json");
        final HttpEntity<com.atexo.redaction.domaines.document.mpe.v2.Contexte> entity = new HttpEntity<>(contexteDTO, headers);

        String json = toJSON(redactionType);

        LOG.debug("Envoi de la consultation '{}' vers le module document a l'adresse '{}'", json, documentContexteUrl);

        Contexte result = null;

        try {
            result = restService.getRestTemplate().postForEntity(documentContexteUrl, entity, Contexte.class).getBody();

            LOG.info("Consultation '{}' créée avec succès dans le module document avec l'identifiant {}", json, result.getUuid());
        } catch (final HttpClientErrorException e) {
            LOG.error("Erreur HTTP {} lors de l'envoi de la consultation vers le module document a l'URL '{}' avec le jeton '{}' et le payload = '{}'", e.getStatusCode(), documentContexteUrl, jeton, json);
        }

        return result;
    }

    private String toJSON(com.atexo.redaction.domaines.document.mpe.v2.RedactionType redactionType) {
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(redactionType);
        } catch (final JsonProcessingException e) {
            LOG.error("Impossible de convertir le payload en json", e);
        }
        return json;
    }

    /**
     * Retourne l'utilisateur externe correspondant a celui indique dans le flux
     * XML en parametre, null si utilisateur inexistant en base.
     */
    private EpmTUtilisateur rechercheUtilisateurExterne(final Utilisateur utilisateurExterne, String plateformeUuid) {

        final Integer idOrganisme = rechercheIdOrganisme(utilisateurExterne.getAcronymeOrganisme(), plateformeUuid);
        final UtilisateurCritere utilisateurCritere = new UtilisateurCritere(plateformeUuid);
        utilisateurCritere.setGuidsSso(utilisateurExterne.getIdentifiantExterne());
        utilisateurCritere.setIdOrganisme(idOrganisme);

        try {
            final EpmTUtilisateur epmTUtilisateur = generiqueDAO.findUniqueByCritere(utilisateurCritere);
            if (epmTUtilisateur == null) {
                throw new TechnicalNoyauException("EpmTUtiliisateur " + utilisateurExterne.getNom() + " n'est pas trouvé");
            }
            return epmTUtilisateur;
        } catch (final NonUniqueResultException ex) {
            throw new TechnicalNoyauException("EpmTUtilisateur " + utilisateurExterne.getNom() + " n'est pas unique");
        } catch (final TechnicalNoyauException | ApplicationNoyauException ex) {
            LOG.error(ex.getMessage(), ex.fillInStackTrace());
        }
        return null;
    }

    /**
     * Recherche de l'idOrganisme correspondant au acronyme envoyé par MPE
     *
     * @return l'identifiant de l'organisme
     */
    private Integer rechercheIdOrganisme(final String acronyme, String plateformeUuid) {
        // Recherche du idOrganisme
        Integer idOrganisme = null;

        if (acronyme != null && !acronyme.isEmpty()) {
            try {
                final OrganismeCritere orgCritere = new OrganismeCritere(plateformeUuid);
                orgCritere.setCodesExternes(acronyme);
                final List<EpmTRefOrganisme> listOrganisme = generiqueDAO.findByCritere(orgCritere);
                if (listOrganisme != null && !listOrganisme.isEmpty()) {
                    idOrganisme = listOrganisme.get(0).getId();
                }
            } catch (final TechnicalNoyauException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return idOrganisme;
    }

    /**
     * Recherche de l'organisme correspondant au acronyme envoyé par MPE
     *
     * @return l'identifiant de l'organisme
     */
    private EpmTRefOrganisme rechercheOrganisme(final String acronyme, String plateformeUuid) {
        // Recherche du idOrganisme
        EpmTRefOrganisme organisme = null;

        if (acronyme != null && !acronyme.isEmpty()) {
            try {
                final OrganismeCritere orgCritere = new OrganismeCritere(plateformeUuid);
                orgCritere.setCodesExternes(acronyme);
                final List<EpmTRefOrganisme> listOrganisme = generiqueDAO.findByCritere(orgCritere);
                if (listOrganisme != null && !listOrganisme.isEmpty()) {
                    organisme = listOrganisme.get(0);
                }
            } catch (final TechnicalNoyauException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return organisme;
    }

    /**
     * Permet la vérification du token afin de savoir si il est toujours valide
     *
     * @param token le jeton
     * @return la réponse
     */
    private ResponseEntity<String> verificationToken(String token) {
        if (token == null || token.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Le token non fourni ou vide ");
        }
        AuthentificationTokenCritere tokenCritere = new AuthentificationTokenCritere();
        tokenCritere.setSignature(token);

        List<EpmTAuthentificationToken> tokenBddList;
        try {
            tokenBddList = generiqueDAO.findByCritere(tokenCritere);
        } catch (TechnicalNoyauException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Probleme lors de la recherche du token " + token);
        }
        if ((tokenBddList == null) || tokenBddList.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Token " + token + " absent en base.");
        }
        if (tokenBddList.size() > 1) {
            LOG.warn("Plus d'un token REST present en base (suppression de tous).");
        }

        return ResponseEntity.ok().body(token);
    }

    public String createContext(String urlMpe, ServeurApiType serveurApi, String plateforme, String token, String refreshToken, UtilisateurExterneConsultation agent, List<ProduitType> produits) {
        String idContexte = this.createAgent(token, refreshToken, getMpe(urlMpe, serveurApi, plateforme, agent, produits));
        if (idContexte == null) {
            throw new TechnicalNoyauException("Impossible de créer le contexte pour l'agent " + agent.getUtilisateur().getIdentifiant());
        }
        return idContexte;

    }

    public String createAgent(String token, String refreshToken, Mpe mpe) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            URI uri = new URI(this.apiAgentUrl);
            //HEADER
            HttpHeaders headers = new HttpHeaders();
            String authHeader = "Bearer " + token;
            headers.set("Authorization", authHeader);
            headers.set("RefreshToken", refreshToken);
            headers.setContentType(MediaType.APPLICATION_JSON);
            //BODY
            //init
            HttpEntity<Mpe> request = new HttpEntity<>(mpe, headers);
            ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
            return result.getBody();

        } catch (Exception e) {
            throw new TechnicalNoyauException("Impossible de créer l'agent " + mpe.getEnvoi().getAgent().getIdentifiant());
        }
    }

    private Mpe getMpe(String urlMpe, ServeurApiType serveurApi, String plateformeUuid, UtilisateurExterneConsultation agent, List<ProduitType> produits) {
        Mpe mpe = new Mpe();
        mpe.setEnvoi(new Mpe.Envoi());
        mpe.getEnvoi().setAgent(new AgentType());
        EpmTUtilisateur utilisateur = agent.getUtilisateur();
        mpe.getEnvoi().getAgent().setIdentifiant(utilisateur.getIdentifiant());
        mpe.getEnvoi().getAgent().setNom(utilisateur.getNom());
        mpe.getEnvoi().getAgent().setPrenom(utilisateur.getPrenom());
        mpe.getEnvoi().getAgent().setPlateforme(plateformeUuid);
        EpmTRefOrganisme epmTRefOrganisme = utilisateur.getEpmTRefOrganisme();
        OrganismeType organisme = new OrganismeType();
        final EpmTRefDirectionService directionService = getEpmTRefDirectionService(utilisateur, plateformeUuid);
        if (epmTRefOrganisme == null) {
            epmTRefOrganisme = directionService.getEpmTRefOrganisme();
        }
        if (epmTRefOrganisme != null) {
            mpe.getEnvoi().getAgent().setAcronymeOrganisme(epmTRefOrganisme.getCodeExterne());
            mpe.getEnvoi().getAgent().setNomCourantAcheteurPublic(epmTRefOrganisme.getLibelle());
            organisme.setAcronyme(epmTRefOrganisme.getCodeExterne());
            organisme.setDenominationOrganisme(epmTRefOrganisme.getLibelle());
        } else {
            mpe.getEnvoi().getAgent().setAcronymeOrganisme(directionService.getCodeExterne());
            mpe.getEnvoi().getAgent().setNomCourantAcheteurPublic(directionService.getLibelle());
            organisme.setAcronyme(directionService.getCodeExterne());
            organisme.setDenominationOrganisme(directionService.getLibelle());
        }
        mpe.getEnvoi().getAgent().setOrganisme(organisme);
        ServiceType service = new ServiceType();
        service.setLibelle(directionService.getLibelle());
        service.setId(directionService.getId());
        mpe.getEnvoi().getAgent().setService(service);
        mpe.getEnvoi().getAgent().setProduits(new ListeProduitsType());
        mpe.getEnvoi().getAgent().getProduits().getProduit().addAll(produits);
        if (serveurApi == null && urlMpe != null) {
            serveurApi = new ServeurApiType();
            MpeToken mpeToken = getAuthentification(urlMpe, login, password);
            serveurApi.setRefreshToken(mpeToken.getRefreshToken());
            serveurApi.setToken(mpeToken.getToken());
            serveurApi.setUrl(urlMpe);
        }
        mpe.getEnvoi().getAgent().setApi(serveurApi);
        final Habilitations habilitations = new Habilitations();
        habilitations.getHabilitation().addAll(agent.getHabilitations());
        mpe.getEnvoi().getAgent().setHabilitations(habilitations);
        return mpe;
    }

    private MpeToken getAuthentification(String urlServeurUpload, String login, String password) {
        String url = urlServeurUpload + "/api/v2/token";
        LOG.info("Demande d'authentification: " + url);

        AuthenitificationRequest authenitificationRequest = new AuthenitificationRequest();
        authenitificationRequest.setLogin(login);
        authenitificationRequest.setPassword(password);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<AuthenitificationRequest> request = new HttpEntity<>(authenitificationRequest, headers);
        return new RestTemplate().postForObject(url, request, MpeToken.class);


    }

    public void setGeneriqueDAO(final GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }

    private Integer getIntValueByParametrage(final String clef) throws TechnicalNoyauException, ApplicationNoyauException {

        final ParametrageCritere paramCritere = new ParametrageCritere();
        paramCritere.setClef(clef);
        final EpmTRefParametrage parametrage = generiqueDAO.findUniqueByCritere(paramCritere);

        try {
            return Integer.parseInt(parametrage.getValeur());
        } catch (final Exception ex) {
            LOG.error("Impossible de parser la valeur int du parametrage : {}", clef);
        }

        return null;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getDocumentContexteUrl() {
        return documentContexteUrl;
    }

    public void setDocumentContexteUrl(String documentContexteUrl) {
        this.documentContexteUrl = documentContexteUrl;
    }

    public String getDocumentAgentUrl() {
        return documentAgentUrl;
    }

    public void setDocumentAgentUrl(String documentAgentUrl) {
        this.documentAgentUrl = documentAgentUrl;
    }

    public String getPlateforme() {
        return plateforme;
    }

    public void setPlateforme(final String plateforme) {
        this.plateforme = plateforme;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUrlGenerationCanevas() {
        return urlGenerationCanevas;
    }

    public void setUrlGenerationCanevas(String urlGenerationCanevas) {
        this.urlGenerationCanevas = urlGenerationCanevas;
    }

    public String getUrlEnregistrementContexte() {
        return urlEnregistrementContexte;
    }

    public void setUrlEnregistrementContexte(String urlEnregistrementContexte) {
        this.urlEnregistrementContexte = urlEnregistrementContexte;
    }

    public String getUrlRecuperationConsultation() {
        return urlRecuperationConsultation;
    }

    public void setUrlRecuperationConsultation(String urlRecuperationConsultation) {
        this.urlRecuperationConsultation = urlRecuperationConsultation;
    }

    public String getUrlRechercheCanevas() {
        return urlRechercheCanevas;
    }

    public void setUrlRechercheCanevas(String urlRechercheCanevas) {
        this.urlRechercheCanevas = urlRechercheCanevas;
    }
}
