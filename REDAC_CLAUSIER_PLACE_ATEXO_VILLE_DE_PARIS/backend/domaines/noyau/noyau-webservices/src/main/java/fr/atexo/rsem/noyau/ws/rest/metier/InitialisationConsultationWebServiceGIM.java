package fr.atexo.rsem.noyau.ws.rest.metier;

import com.atexo.redaction.domaines.document.mpe.v2.Consultation;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.persistance.EpmTConsultation;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;

public interface InitialisationConsultationWebServiceGIM {

    EpmTConsultation getEpmTConsultation(Consultation consultation, EpmTUtilisateur idUtilisateur) throws TechnicalNoyauException, ApplicationNoyauException;
}
