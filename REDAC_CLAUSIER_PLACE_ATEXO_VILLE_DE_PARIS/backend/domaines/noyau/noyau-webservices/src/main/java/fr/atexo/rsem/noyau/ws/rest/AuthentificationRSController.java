package fr.atexo.rsem.noyau.ws.rest;

import com.atexo.execution.common.dto.UtilisateurDTO;
import fr.paris.epm.noyau.persistance.EpmTUtilisateur;
import fr.paris.epm.noyau.service.AdministrationServiceSecurise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Servlet d'authentification pour le module EXEC
 * Created by sta on 23/05/16.
 */
@Controller
public class AuthentificationRSController {

    private static final Logger LOG = LoggerFactory.getLogger(AuthentificationRSController.class);

    @Autowired
    AdministrationServiceSecurise administrationService;

    /**
     * Retourne un UtilisateurDTO pour le module EXEC
     * @param token
     * @return
     */
    @RequestMapping(value = { "/authenticate/user/{token}" }, method = RequestMethod.GET)
    public ResponseEntity<UtilisateurDTO> authenticateUser(@PathVariable("token") String token) {
        EpmTUtilisateur utilisateur = administrationService.authentifierUtilisateurParToken(token);
        if (utilisateur == null) {
            return new ResponseEntity<UtilisateurDTO>(HttpStatus.FORBIDDEN);
        }
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();
        utilisateurDTO.setIdentifiant( utilisateur.getIdentifiant() );
        return new ResponseEntity<UtilisateurDTO>(utilisateurDTO, HttpStatus.OK);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> catchRuntimeException(HttpServletRequest request, Exception e) {
        LOG.error("Erreur lors de la requete " + request.getRequestURI(), e);
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}