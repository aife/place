package fr.atexo.rsem.noyau.ws.rest.metier;

import com.atexo.redaction.domaines.document.mpe.v2.*;
import fr.paris.epm.noyau.commun.Constantes;
import fr.paris.epm.noyau.commun.exception.ApplicationNoyauException;
import fr.paris.epm.noyau.commun.exception.NonTrouveNoyauException;
import fr.paris.epm.noyau.commun.exception.TechnicalNoyauException;
import fr.paris.epm.noyau.metier.GeneriqueDAO;
import fr.paris.epm.noyau.metier.ProcedureCritere;
import fr.paris.epm.noyau.metier.objetvaleur.EtapeSimple;
import fr.paris.epm.noyau.metier.objetvaleur.Referentiels;
import fr.paris.epm.noyau.persistance.*;
import fr.paris.epm.noyau.persistance.redaction.EpmTRefTypeContrat;
import fr.paris.epm.noyau.persistance.referentiel.*;
import fr.paris.epm.noyau.service.ReferentielsServiceLocal;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe Métier qui fournit la méthode pour la mise à jour de consultation
 */
@Component
public class InitialisationConsultationWebServiceGIMImpl implements InitialisationConsultationWebServiceGIM {

    private static final Logger LOG = LoggerFactory.getLogger(InitialisationConsultationWebServiceGIMImpl.class);
    private static final String IDENTIFIANT_TRANCHE_FIXE = "0";
    private DozerBeanMapper conversionService;
    private GeneriqueDAO generiqueDAO;
    private ReferentielsServiceLocal referentielsServiceLocal;

    /**
     * Rempli les paireTypeComplexe dans l'objet
     * epmTBudLot depuis le flux dans le cas d'une consultation allotie.
     */
    private static void remplirPaireTypeComplexePourLot(LotType lot, EpmTBudLot epmTBudLot, EpmTConsultation epmTConsultation) {

        EpmTBudLotOuConsultation epmTBudLotOuConsultation = null;
        if (epmTBudLot.getEpmTBudLotOuConsultation() != null) {
            epmTBudLotOuConsultation = epmTBudLot.getEpmTBudLotOuConsultation();
        }

        if (epmTBudLotOuConsultation != null) {
            Set<EpmTValeurConditionnementExterneComplexe> conditionnementExterneComplexe = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe();

            if (conditionnementExterneComplexe == null) {
                conditionnementExterneComplexe = new HashSet<>();
                epmTBudLotOuConsultation.setEpmTValeurConditionnementExternesComplexe(conditionnementExterneComplexe);
            }

            Set<EpmTValeurConditionnementExterne> conditionnementExternes = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes();

            if (conditionnementExternes == null) {
                conditionnementExternes = new HashSet<>();
                epmTBudLotOuConsultation.setEpmTValeurConditionnementExternes(conditionnementExternes);
            }

            conditionnementExternes = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes();
            conditionnementExternes.clear();

            ListPaireTypeComplexe pairesTypeComplexe = lot.getPairesTypeComplexe();
            if (pairesTypeComplexe != null) {
                if (epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe() == null) {
                    epmTConsultation.setEpmTBudLotOuConsultation(epmTBudLotOuConsultation);
                }

                conditionnementExterneComplexe = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe();
                conditionnementExterneComplexe.clear();

                List<ListPaireType> listPaireTypeComplexe = pairesTypeComplexe.getPaireTypeComplexe();
                for (ListPaireType paireTypeComplexe : listPaireTypeComplexe) {
                    List<PaireType> listPaireType = paireTypeComplexe.getPaireType();
                    if (listPaireType != null) {

                        Set<EpmTValeurConditionnementExterne> setValeurConditionnementExterne = new HashSet<>();
                        for (PaireType paireType : listPaireType) {
                            epmTConsultation.getEpmTValeurConditionnementExterneList().stream().filter(epmTValeurConditionnementExterne -> epmTValeurConditionnementExterne.getValeur().equals(paireType.getValeur())).forEach(setValeurConditionnementExterne::add);

                        }
                        EpmTValeurConditionnementExterneComplexe valeurConditionnementExterneComplexe = new EpmTValeurConditionnementExterneComplexe();
                        valeurConditionnementExterneComplexe.setEpmTValeurConditionnementExternes(setValeurConditionnementExterne);
                        valeurConditionnementExterneComplexe.setIdObjetComplexe(paireTypeComplexe.getIdObjetComplexe());
                        conditionnementExterneComplexe.add(valeurConditionnementExterneComplexe);
                    }
                }
            }
            epmTBudLotOuConsultation.setEpmTValeurConditionnementExternesComplexe(conditionnementExterneComplexe);

            ListPaireType listPairesType = lot.getPairesType();
            if (listPairesType != null) {
                for (PaireType paireType : listPairesType.getPaireType()) {
                    EpmTValeurConditionnementExterne valeurConditionnementExterne = new EpmTValeurConditionnementExterne();
                    valeurConditionnementExterne.setClef(paireType.getCle());
                    valeurConditionnementExterne.setValeur(paireType.getValeur());
                    conditionnementExternes.add(valeurConditionnementExterne);
                }
            }
            epmTConsultation.setEpmTValeurConditionnementExterneList(new ArrayList<>(conditionnementExternes));
        }
    }

    /**
     * Rempli les paireType dans le cas d'une consultation non-allotie Crée un
     * objet EpmTBudLotOuConsultation et lui associe un ensemble
     * EpmTValeurConditionnementExterneComplexe
     */
    private static List<EpmTValeurConditionnementExterneComplexe> remplirPaireTypeComplexeConsultation(Consultation consultation) {

        List<EpmTValeurConditionnementExterneComplexe> conditionnementExterneComplexe = new ArrayList<>();

        ListPaireTypeComplexe pairesTypeComplexe = consultation.getPairesTypeComplexe();
        if (pairesTypeComplexe != null) {

            List<ListPaireType> listPaireTypeComplexe = pairesTypeComplexe.getPaireTypeComplexe();
            for (ListPaireType paireTypeComplexe : listPaireTypeComplexe) {
                List<PaireType> listPaireType = paireTypeComplexe.getPaireType();
                if (listPaireType != null) {

                    Set<EpmTValeurConditionnementExterne> setValeurConditionnementExterne = new HashSet<EpmTValeurConditionnementExterne>();
                    for (PaireType paireType : listPaireType) {
                        EpmTValeurConditionnementExterne valeurConditionnementExterne = new EpmTValeurConditionnementExterne();
                        valeurConditionnementExterne.setClef(paireType.getCle());
                        valeurConditionnementExterne.setValeur(paireType.getValeur());
                        setValeurConditionnementExterne.add(valeurConditionnementExterne);
                    }
                    EpmTValeurConditionnementExterneComplexe valeurConditionnementExterneComplexe = new EpmTValeurConditionnementExterneComplexe();
                    valeurConditionnementExterneComplexe.setEpmTValeurConditionnementExternes(setValeurConditionnementExterne);
                    valeurConditionnementExterneComplexe.setIdObjetComplexe(paireTypeComplexe.getIdObjetComplexe());
                    conditionnementExterneComplexe.add(valeurConditionnementExterneComplexe);
                }
            }
        }
        return conditionnementExterneComplexe;
    }

    /**
     * Rempli les paireType dans le cas d'une consultation non-allotie Crée un
     * objet EpmTBudLotOuConsultation et lui associe un ensemble
     * EpmTValeurConditionnementExterne
     */
    private static List<EpmTValeurConditionnementExterne> remplirPaireTypeConsultation(Consultation consultation) {
        List<EpmTValeurConditionnementExterne> conditionnementExternes = new ArrayList<EpmTValeurConditionnementExterne>();

        ListPaireType pairesType = consultation.getPairesType();
        if (pairesType != null) {
            List<PaireType> listPaireType = pairesType.getPaireType();
            if (listPaireType != null) {
                for (PaireType paireType : listPaireType) {
                    EpmTValeurConditionnementExterne valeurConditionnementExterne = new EpmTValeurConditionnementExterne();
                    valeurConditionnementExterne.setClef(paireType.getCle());
                    valeurConditionnementExterne.setValeur(paireType.getValeur());
                    conditionnementExternes.add(valeurConditionnementExterne);
                }
            }
        }
        return conditionnementExternes;
    }

    /**
     * Dans le cas ou obligatoire == false, on renvoi le premier objet qu'on
     * récupère juste pour faire passer la sauvegarde en base de données
     *
     * @return EpmTRefNature
     */
    private static EpmTRefNature getEpmTRefNature(final Referentiels referentiels, final Consultation consultation, final boolean obligatoire) {

        for (EpmTRefNature nature : referentiels.getRefNature()) {
            if (obligatoire) {
                if (nature.getCodeGo() != null && consultation.getNaturePrestation() != null && nature.getCodeGo().equals(consultation.getNaturePrestation().value())) {
                    return nature;
                }
            } else {
                return nature;
            }
        }
        return null;
    }

    /**
     * @param obligatoire Dans le cas ou obligatoire == false, on renvoi le premier
     *                    objet qu'on récupère juste pour faire passer la sauvegarde en
     *                    base de données
     * @return EpmTRefStatut
     */
    private static EpmTRefStatut getEpmTRefStatut(final Referentiels referentiels, final Consultation consultation, final boolean obligatoire) {

        for (EpmTRefStatut statut : referentiels.getStatut()) {
            if (obligatoire) {
                if (statut.getCodeGo() != null && consultation.getStatut() != null && consultation.getStatut().equals(statut.getCodeGo())) {
                    return statut;
                }
            } else {
                return statut;
            }
        }
        return null;
    }

    private static EpmTRefClausesSociales getEpmTRefClausesSociales(final Referentiels referentiels, String code) {
        for (EpmTRefClausesSociales refClausesSociales : referentiels.getRefClausesSociales())
            if (refClausesSociales.getCodeExterne() != null && code.equals(refClausesSociales.getCodeExterne()))
                return refClausesSociales;
        return null;
    }

    private static EpmTRefClausesEnvironnementales getEpmTRefClausesEnvironnementales(final Referentiels referentiels, String code) {

        for (EpmTRefClausesEnvironnementales refClausesEnvironnementales : referentiels.getRefClausesEnvironnementales())
            if (refClausesEnvironnementales.getCodeExterne() != null && code.equals(refClausesEnvironnementales.getCodeExterne()))
                return refClausesEnvironnementales;
        return null;
    }

    /**
     * @param obligatoire Dans le cas ou obligatoire == false, on renvoi le premier
     *                    objet qu'on récupère juste pour faire passer la sauvegarde en
     *                    base de données
     * @return EpmTRefArticle
     */
    private static EpmTRefArticle getEpmTRefArticle(Referentiels referentiels, Consultation consultation, final boolean obligatoire) {

        for (EpmTRefArticle article : referentiels.getRefArticle()) {
            if (obligatoire) {
                if (article.getCodeExterne() != null && consultation.getArticle() != null && consultation.getArticle().equals(article.getCodeExterne())) {
                    return article;
                }
            } else {
                return article;
            }
        }
        return null;
    }

    /**
     * @param obligatoire Dans le cas ou obligatoire == false, on renvoi le premier
     *                    objet qu'on récupère juste pour faire passer la sauvegarde en
     *                    base de données
     * @return EpmTRefPouvoirAdjudicateur
     */
    private static EpmTRefPouvoirAdjudicateur getEpmTRefPouvoirAdjudicateur(final Referentiels referentiels, final Consultation consultation, final boolean obligatoire) {

        for (EpmTRefPouvoirAdjudicateur pouvoirAdjudicateur : referentiels.getRefPouvoirAdjudicateur()) {
            if (obligatoire) {
                if (pouvoirAdjudicateur.getCodeGo() != null && consultation.getPouvoirAdjudicateur() != null && pouvoirAdjudicateur.getCodeGo().equals(consultation.getPouvoirAdjudicateur())) {
                    return pouvoirAdjudicateur;
                }
            } else {
                return pouvoirAdjudicateur;
            }
        }
        return null;
    }

    /**
     * @param obligatoire Dans le cas ou obligatoire == false, on renvoi le premier
     *                    objet qu'on récupère juste pour faire passer la sauvegarde en
     *                    base de données
     * @return EpmTRefChoixMoisJour
     */
    private static EpmTRefChoixMoisJour getEpmTRefChoixMoisJour(final Referentiels referentiels, DureeMarcheType dureeMarcheType, final boolean obligatoire) {

        EpmTRefChoixMoisJour defautValue = null;
        for (EpmTRefChoixMoisJour choixMoisJour : referentiels.getChoixMoisJour()) {
            if (obligatoire || dureeMarcheType != null) {
                if (dureeMarcheType.getNbJours() != null && choixMoisJour.getId() == EpmTRefChoixMoisJour.EN_JOUR) {
                    return choixMoisJour;
                } else if (dureeMarcheType.getNbMois() != null && choixMoisJour.getId() == EpmTRefChoixMoisJour.EN_MOIS) {
                    return choixMoisJour;
                }
            }

            if (choixMoisJour.getId() == EpmTRefChoixMoisJour.EN_JOUR) defautValue = choixMoisJour;
        }
        return defautValue;
    }

    /**
     * Recherche dans le referentiel le CCAG correspondant à celui envoyé par le flux MPE.
     * Si pas de correspondance, on renvoi le dernier objet qu'on
     * récupère juste pour faire passer la sauvegarde en base de données
     */
    private static EpmTRefCcag getEpmTRefCcag(final Referentiels referentiels, final String ccag) {

        for (EpmTRefCcag epmTRefCcag : referentiels.getRefCcag())
            if (epmTRefCcag.getCodeExterne().equals(ccag)) return epmTRefCcag;
        return null;
    }

    private static Set<EpmTRefVariation> getEpmTRefVariation(Referentiels referentiels, VariationPrixType variationsPrixType) {

        Set<EpmTRefVariation> epmTRefVariations = new HashSet<>();

        if (variationsPrixType != null && variationsPrixType.getVariationsPrix() != null) {
            List<String> libellesVariationPrix = variationsPrixType.getVariationsPrix();
            for (EpmTRefVariation variation : referentiels.getVariationTous())
                if (libellesVariationPrix.contains(variation.getCodeExterne())) epmTRefVariations.add(variation);
        }
        return epmTRefVariations;
    }

    /**
     * A partir d'une consultation externe, mise a jour ou création de la consultation associée
     *
     * @param consultation consultation externe passée par flux XML
     */
    @Override
    public EpmTConsultation getEpmTConsultation(final Consultation consultation, EpmTUtilisateur epmTUtilisateur) throws TechnicalNoyauException, ApplicationNoyauException {
        Referentiels referentiels = referentielsServiceLocal.getAllReferentiels();
        EpmTConsultation epmTConsultation = conversionService.map(consultation, EpmTConsultation.class);
        if (epmTUtilisateur != null) {
            epmTConsultation.setEpmTUtilisateur(epmTUtilisateur);
            EpmTRefOrganisme epmTRefOrganisme = epmTUtilisateur.getEpmTRefOrganisme();
            if (epmTRefOrganisme != null) {
                epmTConsultation.setIdOrganisme(epmTRefOrganisme.getId());
            }
        }
        // DLRO
        if (null != consultation.getDateRemisePlis()) {
            epmTConsultation.setDateRemisePlis(consultation.getDateRemisePlis().toGregorianCalendar());
        }
        epmTConsultation.setNumeroConsultation(consultation.getNumeroConsultation());
        epmTConsultation.setNumeroConsultationExterne(consultation.getReferenceExterne());
        // Autre opération
        epmTConsultation.setAutreOperation("Autre opération");

        epmTConsultation.setDateModification(Calendar.getInstance());

        if (consultation.getClausesSociales() != null) epmTConsultation.setClausesSociales("oui");
        else epmTConsultation.setClausesSociales("non");

        if (consultation.getClausesEnvironnementales() != null) epmTConsultation.setClausesEnvironnementales("oui");
        else epmTConsultation.setClausesEnvironnementales("non");

        initialiserValeurConsultation(epmTConsultation, consultation, referentiels);

        //mise à jour de la date de modification en cas d'une consultation déjà existante

        epmTConsultation.setNumeroConsultationExterne(consultation.getReferenceExterne());

        // Date de remise de plis
        if (consultation.getDateRemisePlis() != null) {
            Date dateRemisePlis = consultation.getDateRemisePlis().toGregorianCalendar().getTime();
            epmTConsultation.addCalendrier(sauvegardeCalendrier(dateRemisePlis, EpmTEtapeCal.RECEPTION_CANDIDATURES));
        }

        // Date de mise en ligne
        if (consultation.getDateMiseLigne() != null) {
            Date dateMiseEnLigne = consultation.getDateMiseLigne().toGregorianCalendar().getTime();
            epmTConsultation.addCalendrier(sauvegardeCalendrier(dateMiseEnLigne, EpmTEtapeCal.NOTIFICATION));
        }

        epmTConsultation.setSignature(consultation.getSignatureElectronique());
        epmTConsultation.setChiffrement(consultation.isChiffrementPlis());
        epmTConsultation.setEnveloppeUniqueReponse(consultation.isEnveloppeUnique() ? Constantes.OUI : Constantes.NON);

        if (consultation.getLots() == null || consultation.getLots().getLot() == null || consultation.getLots().getLot().isEmpty()) {
            epmTConsultation.setAllotissement(Constantes.NON);

            remplirPaireTypeComplexe(consultation, epmTConsultation, referentiels);
            //initialiser les valeurs des lots si la consultation est allotissement
        } else {
            epmTConsultation.setAllotissement(Constantes.OUI);
            epmTConsultation.setCritereAppliqueTousLots(false);
            initialiserValeurLots(consultation, epmTConsultation);
        }
        // Sauvegarde de la consultation
        try {
            List<EpmTValeurConditionnementExterne> listExterne = remplirPaireTypeConsultation(consultation);
            epmTConsultation.setEpmTValeurConditionnementExterneList(listExterne);

            List<EpmTValeurConditionnementExterneComplexe> listeExterneComplexe = remplirPaireTypeComplexeConsultation(consultation);
            epmTConsultation.setEpmTValeurConditionnementExterneComplexeList(listeExterneComplexe);
        } catch (TechnicalNoyauException e) {
            LOG.error(e.getMessage(), e.fillInStackTrace());
            throw e;
        }

        return epmTConsultation;
    }

    private void initialiserValeurConsultation(EpmTConsultation epmTConsultation, Consultation consultation, Referentiels referentiels) throws ApplicationNoyauException {

        epmTConsultation.setJustificationNonAllotissement(consultation.getJustificationNonAllotissement());
        epmTConsultation.setTransverse((consultation.isTransverse() ? Constantes.OUI : Constantes.NON));
        epmTConsultation.setRefReponseElectronique(getEpmTRefReponseElectronique(referentiels, consultation));

        String messageErreur = " n'existe pas.";
        // Intitulé de la consultation
        if (consultation.getIntitule() != null && !"".equals(consultation.getIntitule())) {
            epmTConsultation.setIntituleConsultation(consultation.getIntitule());
        } else {
            ApplicationNoyauException e = new ApplicationNoyauException("L'intitulé de la consultation doit être renseigné");
            LOG.error(e.getMessage());
            throw e;
        }

        // Objet de la consultation
        if (consultation.getObjet() != null && !"".equals(consultation.getObjet())) {
            epmTConsultation.setObjet(consultation.getObjet());
        } else {
            ApplicationNoyauException e = new ApplicationNoyauException("L'objet de la consultation doit être renseigné");
            LOG.error(e.getMessage());
            throw e;
        }

        // Procedure
        try {
            epmTConsultation.setEpmTRefProcedure(getEpmTRefProcedure(consultation, epmTConsultation.getIdOrganisme()));
            if (epmTConsultation.getEpmTRefProcedure() == null) {
                ApplicationNoyauException ex = new ApplicationNoyauException("Le type de de procedure '" + consultation.getTypeProcedure() + "' avec le code externe " + consultation.getCodeExterneProcedure() + messageErreur);
                LOG.error(ex.getMessage());
                throw ex;
            }
        } catch (TechnicalNoyauException ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }

        // Nature prestation
        epmTConsultation.setEpmTRefNature(getEpmTRefNature(referentiels, consultation, true));
        if (epmTConsultation.getEpmTRefNature() == null) {
            ApplicationNoyauException e = new ApplicationNoyauException("La nature de prestation " + consultation.getNaturePrestation() + messageErreur);
            LOG.error(e.getMessage());
            throw e;
        }

        // Pouvoir adjudicateur
        epmTConsultation.setEpmTRefPouvoirAdjudicateur(getEpmTRefPouvoirAdjudicateur(referentiels, consultation, true));
        if (epmTConsultation.getEpmTRefPouvoirAdjudicateur() == null) {
            ApplicationNoyauException e = new ApplicationNoyauException("Le pouvoir adjudicateur " + consultation.getPouvoirAdjudicateur() + messageErreur);
            LOG.error(e.getMessage());
            throw e;
        }

        // Article
        epmTConsultation.setEpmTRefArticle(getEpmTRefArticle(referentiels, consultation, true));

        // Direction service
        LOG.info("Recherche de la direction/service");
        EpmTRefDirectionService epmTRefDirectionService = getEpmTRefDirectionService(referentiels, consultation, true, epmTConsultation.getIdOrganisme());
        if (epmTRefDirectionService != null) {
            LOG.info("Direction/service correpondante = {}", epmTRefDirectionService.getId());

            epmTConsultation.setEpmTRefDirectionService(epmTRefDirectionService);
            epmTConsultation.setDirServiceVision(epmTRefDirectionService.getId());
        } else {
            ApplicationNoyauException e = new ApplicationNoyauException("La direction service " + consultation.getDirectionService() + messageErreur);
            LOG.error(e.getMessage());
            throw e;
        }

        // Type Contrat
        EpmTRefTypeContrat typeContrat = null;
        EpmTRefTypeContrat marche = null;
        TypeContratType typeContratConsultation = consultation.getTypeContrat();
        if (typeContratConsultation != null) {
            try {
                List<EpmTRefTypeContrat> refs = generiqueDAO.findAll(EpmTRefTypeContrat.class);
                for (EpmTRefTypeContrat ref : refs) {
                    if (StringUtils.isNotBlank(ref.getCodeExterne()) && ref.getCodeExterne().equalsIgnoreCase("mar")) {
                        marche = ref;
                    }
                    String codeExterne = typeContratConsultation.getCodeExterne();
                    if (codeExterne != null) {
                        if (StringUtils.isNotBlank(codeExterne) && codeExterne.equals(ref.getCodeExterne())) { //Probleme : MPE n'envoie pas le code externe
                            typeContrat = ref;
                        }
                    } else {
                        if (StringUtils.isNotBlank(typeContratConsultation.getLibelle()) && typeContratConsultation.getLibelle().equals(ref.getLibelle())) { //se baser sur le libelle en attendant code_externe?
                            typeContrat = ref;
                        }
                    }

                }
            } catch (TechnicalNoyauException e) {
                LOG.error("Le Type d'attribution avec l'id : " + typeContratConsultation + " est introuvable dans le noyau");
            }
        }
        if (typeContrat != null) {
            epmTConsultation.setEpmTRefTypeContrat(typeContrat);
        } else {
            epmTConsultation.setEpmTRefTypeContrat(marche);
        }

        // Lieu d'execution
        if (consultation.getLieuExecutions() != null) {
            Set<EpmTRefLieuExecution> epmTRefLieuExecutions = getListEpmTLieuExecutions(referentiels, consultation.getLieuExecutions().getLieuExecution());
            epmTConsultation.setLieuxExecution(epmTRefLieuExecutions);
        }

        // Statut
        epmTConsultation.setEpmTRefStatut(getEpmTRefStatut(referentiels, consultation, true));
        if (epmTConsultation.getEpmTRefStatut() == null) {
            ApplicationNoyauException e = new ApplicationNoyauException("Le statut " + consultation.getStatut() + messageErreur);
            LOG.error(e.getMessage());
            throw e;
        }

        //Codes CPV
        Set<EpmTCpv> cpvSet = epmTConsultation.getCpvs();
        if (consultation.getListeCodeCPV() != null && !consultation.getListeCodeCPV().getCpv().isEmpty()) {
            if (cpvSet == null) {
                cpvSet = new HashSet<>();
                epmTConsultation.setCpvs(cpvSet);
            } else {
                cpvSet.clear();
            }

            for (CPVType cpvType : consultation.getListeCodeCPV().getCpv()) {
                EpmTCpv epmTCpv = new EpmTCpv();

                epmTCpv.setPrincipal(cpvType.isPrincipal());

                epmTCpv.setCodeCpv(cpvType.getCodeCPV().trim());

                if (null != cpvType.getLibelleCPV()) {
                    epmTCpv.setLibelleCpv(cpvType.getLibelleCPV().trim());
                } else {
                    epmTCpv.setLibelleCpv("");
                }
                cpvSet.add(epmTCpv);
            }
        }
        epmTConsultation.setCpvs(cpvSet);

        // Mharche Public Simplifie
        epmTConsultation.setMarchePublicSimplifie(consultation.isMps());

        // Forme groupement attributaire
        EpmTRefGroupementAttributaire epmTRefGroupementAttributaire = getEpmTRefGroupementAttributaire(referentiels, consultation.getFormeGroupementAttributaire());
        epmTConsultation.setEpmTRefGroupementAttributaire(epmTRefGroupementAttributaire);

        //Durée du marché
        DureeMarcheType dureeMarcheType = consultation.getDureeMarche();
        EpmTRefChoixMoisJour choixMJ = getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false);
        epmTConsultation.setEpmTRefChoixMoisJour(choixMJ);

        if (dureeMarcheType != null && choixMJ != null) {
            EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription = getEmTRefDureeDelaiDescription(referentiels, dureeMarcheType);
            if (epmTRefDureeDelaiDescription != null) {
                epmTConsultation.setEpmTRefDureeDelaiDescription(epmTRefDureeDelaiDescription);
                if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DUREE_MARCHE_EN_MOIS_EN_JOUR) {
                    if (choixMJ.getId() == EpmTRefChoixMoisJour.EN_JOUR)
                        epmTConsultation.setDureeMarche(Integer.valueOf(dureeMarcheType.getNbJours()));
                    else epmTConsultation.setDureeMarche(Integer.valueOf(dureeMarcheType.getNbMois()));
                    epmTConsultation.setDescriptionDuree(null);
                    epmTConsultation.setDateExecutionPrestationsDebut(null);
                    epmTConsultation.setDateExecutionPrestationsFin(null);
                } else if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DESCRIPTION_LIBRE) {
                    epmTConsultation.setDescriptionDuree(dureeMarcheType.getDescriptionLibre());
                    epmTConsultation.setDureeMarche(null);
                    epmTConsultation.setDateExecutionPrestationsDebut(null);
                    epmTConsultation.setDateExecutionPrestationsFin(null);
                    epmTConsultation.setEpmTRefChoixMoisJour(getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false));
                } else if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DELAI_EXECUTION_DU_AU) {
                    epmTConsultation.setDureeMarche(null);
                    epmTConsultation.setDescriptionDuree(null);
                    Calendar calendarDateDu = convertGregorianCalendarToCalendar(dureeMarcheType.getDateACompterDu());
                    epmTConsultation.setDateExecutionPrestationsDebut(calendarDateDu);
                    Calendar calendarDateJusquau = convertGregorianCalendarToCalendar(dureeMarcheType.getDateJusquau());
                    epmTConsultation.setDateExecutionPrestationsFin(calendarDateJusquau);
                    epmTConsultation.setEpmTRefChoixMoisJour(getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false));
                }
            }
        }

        //Delai de validité des offres
        epmTConsultation.setDelaiValiditeOffres((int) consultation.getDureeValiditeOffreEnJour());

        // Criteres d'attribution
        if (consultation.getCriteresAttribution() != null) {
            EpmTRefCritereAttribution epmTRefCritereAttribution = getEpmTRefCritereAttribution(referentiels, consultation.getCriteresAttribution());
            if (epmTRefCritereAttribution != null && epmTConsultation.getEpmTBudLotOuConsultation() != null) {
                epmTConsultation.setCritereAttribution(epmTRefCritereAttribution);
            }

            // Liste des critéres d'attribution liés à la consultation
            Set<EpmTCritereAttributionConsultation> listeCritereAttributionConsultation = getEpmTCritereAttributionConsultation(referentiels, consultation.getCriteresAttribution());
            //si non alloti
            if (listeCritereAttributionConsultation != null && epmTConsultation.getEpmTBudLotOuConsultation() != null && epmTConsultation.getListeCritereAttribution() != null) {
                epmTConsultation.getListeCritereAttribution().clear();
                epmTConsultation.getListeCritereAttribution().addAll(listeCritereAttributionConsultation);
            } else if (listeCritereAttributionConsultation != null && epmTConsultation.getEpmTBudLotOuConsultation() != null && epmTConsultation.getListeCritereAttribution() == null) {
                epmTConsultation.setListeCritereAttribution(listeCritereAttributionConsultation);
            }
        }

        //Nombre de candidats admis
        NombreCandidatsType nombreCandidatsType = consultation.getNombreCandidats();
        EpmTRefNbCandidatsAdmis refNombreCandidatAdmis = null;
        Integer idRefNbCandidat = null;
        if (nombreCandidatsType != null) {
            if (nombreCandidatsType.getNombreFixe() != null && nombreCandidatsType.getNombreFixe() != 0) {
                epmTConsultation.setNombreCandidatsFixe(nombreCandidatsType.getNombreFixe().intValue());
                epmTConsultation.setNombreCandidatsMax(null);
                epmTConsultation.setNombreCandidatsMin(null);
                idRefNbCandidat = EpmTRefNbCandidatsAdmis.ID_FIXE;
            } else {
                epmTConsultation.setNombreCandidatsFixe(null);
            }

            if (nombreCandidatsType.getNombreMax() != null && nombreCandidatsType.getNombreMax() != 0) {
                epmTConsultation.setNombreCandidatsMax(nombreCandidatsType.getNombreMax().intValue());
                idRefNbCandidat = EpmTRefNbCandidatsAdmis.ID_FOURCHETTE;
            } else {
                epmTConsultation.setNombreCandidatsMax(null);
            }

            if (nombreCandidatsType.getNombreMin() != null && nombreCandidatsType.getNombreMin() != 0) {
                epmTConsultation.setNombreCandidatsMin(nombreCandidatsType.getNombreMin().intValue());
                idRefNbCandidat = EpmTRefNbCandidatsAdmis.ID_FOURCHETTE;
            } else {
                epmTConsultation.setNombreCandidatsMin(null);
            }

            if (nombreCandidatsType.getReductionProgressive() != null) {
                epmTConsultation.setEnPhasesSuccessives(Constantes.OUI);
            } else {
                epmTConsultation.setEnPhasesSuccessives(Constantes.NON);
            }
        } else {
            epmTConsultation.setNombreCandidatsFixe(null);
            epmTConsultation.setNombreCandidatsMax(null);
            epmTConsultation.setNombreCandidatsMin(null);
            epmTConsultation.setEnPhasesSuccessives(Constantes.NON);
        }

        if (idRefNbCandidat != null) {
            refNombreCandidatAdmis = getEpmTRefNbCandidatsAdmis(referentiels, idRefNbCandidat);
        }

        epmTConsultation.setEpmTRefNbCandidatsAdmis(refNombreCandidatAdmis);

        initialiserValeurConsultationInformationsComplementaires(epmTConsultation.getEpmTBudLotOuConsultation(), consultation.getInformationsComplementaires(), referentiels);
    }

    /**
     * Rempli les paireTypeComplexe depuis le flux dans le cas d'une
     * consultation non-allotie Crée un objet EpmTBudLotOuConsultation et lui
     * associe un ensemble EpmTValeurConditionnementExterneComplexe
     */
    private void remplirPaireTypeComplexe(Consultation consultation, EpmTConsultation epmTConsultation, Referentiels referentiels) {

        EpmTBudLotOuConsultation epmTBudLotOuConsultation = null;
        if (epmTConsultation.getEpmTBudLotOuConsultation() == null) {
            epmTBudLotOuConsultation = new EpmTBudLotOuConsultation();
            epmTConsultation.setEpmTBudLotOuConsultation(epmTBudLotOuConsultation);
        }
        epmTBudLotOuConsultation = epmTConsultation.getEpmTBudLotOuConsultation();

        Set<EpmTValeurConditionnementExterneComplexe> conditionnementExterneComplexe = epmTBudLotOuConsultation.getEpmTValeurConditionnementExternesComplexe();
        if (conditionnementExterneComplexe == null) {
            conditionnementExterneComplexe = new HashSet<EpmTValeurConditionnementExterneComplexe>();
            epmTBudLotOuConsultation.setEpmTValeurConditionnementExternesComplexe(conditionnementExterneComplexe);
        }

        // Clauses Sociales
        ClausesSocialesType clausesSocialesType = consultation.getClausesSociales();
        if (clausesSocialesType != null) {
            epmTConsultation.setClausesSociales(Constantes.OUI);
            gerererClausesSociales(clausesSocialesType, epmTConsultation.getEpmTBudLotOuConsultation(), referentiels);
        } else {
            epmTConsultation.setClausesSociales(Constantes.NON);
            epmTConsultation.getEpmTBudLotOuConsultation().setClausesSociales(false);
        }

        // Clauses Environnamentales
        ClausesEnvironnementalesType clausesEnvironnementales = consultation.getClausesEnvironnementales();
        if (clausesEnvironnementales != null) {
            epmTConsultation.setClausesEnvironnementales(Constantes.OUI);
            epmTConsultation.getEpmTBudLotOuConsultation().setClausesEnvironnementales(true);

            Set<EpmTRefClausesEnvironnementales> refClausesEnvironnementales = new HashSet<>();
            if (clausesEnvironnementales.isSpecificationTechnique())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_SPECIFICATION));

            if (clausesEnvironnementales.isConditionExecution())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_CONDITIONS));

            if (clausesEnvironnementales.isCritereAttribution())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_ATTRIBUTION));

            epmTConsultation.getEpmTBudLotOuConsultation().setClausesEnvironnementalesChoixUtilisateur(refClausesEnvironnementales);
        } else {
            epmTConsultation.setClausesEnvironnementales(Constantes.NON);
            epmTConsultation.getEpmTBudLotOuConsultation().setClausesEnvironnementales(false);
        }

        conditionnementExterneComplexe.clear();

        ListPaireTypeComplexe pairesTypeComplexe = consultation.getPairesTypeComplexe();
        if (pairesTypeComplexe != null) {

            List<ListPaireType> listPaireTypeComplexe = pairesTypeComplexe.getPaireTypeComplexe();
            for (ListPaireType paireTypeComplexe : listPaireTypeComplexe) {
                List<PaireType> listPaireType = paireTypeComplexe.getPaireType();
                if (listPaireType != null) {

                    Set<EpmTValeurConditionnementExterne> setValeurConditionnementExterne = new HashSet<EpmTValeurConditionnementExterne>();
                    for (PaireType paireType : listPaireType) {
                        EpmTValeurConditionnementExterne valeurConditionnementExterne = new EpmTValeurConditionnementExterne();
                        valeurConditionnementExterne.setClef(paireType.getCle());
                        valeurConditionnementExterne.setValeur(paireType.getValeur());
                        setValeurConditionnementExterne.add(valeurConditionnementExterne);
                    }
                    EpmTValeurConditionnementExterneComplexe valeurConditionnementExterneComplexe = new EpmTValeurConditionnementExterneComplexe();
                    valeurConditionnementExterneComplexe.setEpmTValeurConditionnementExternes(setValeurConditionnementExterne);
                    valeurConditionnementExterneComplexe.setIdObjetComplexe(paireTypeComplexe.getIdObjetComplexe());
                    conditionnementExterneComplexe.add(valeurConditionnementExterneComplexe);
                }
            }
        }
        epmTBudLotOuConsultation.setEpmTValeurConditionnementExternesComplexe(conditionnementExterneComplexe);
        epmTConsultation.setEpmTValeurConditionnementExterneList(new ArrayList<>(epmTBudLotOuConsultation.getEpmTValeurConditionnementExternes()));
    }

    private void gerererClausesSociales(final ClausesSocialesType clausesSocialesType, final EpmTBudLotOuConsultation epmTBudLotOuConsultation, final Referentiels referentiels) {

        epmTBudLotOuConsultation.setClausesSociales(true);
        Set<EpmTRefClausesSociales> refClausesSociales = new HashSet<>();
        Set<EpmTRefTypeStructureSociale> refTypeStructureSociales = new HashSet<>();
        if (clausesSocialesType.isMarcheReserve()) {
            refClausesSociales.add(getEpmTRefClausesSociales(referentiels, EpmTRefClausesSociales.CODE_CLAUSE_RESERVE));
        }
        if (clausesSocialesType.isConditionExecution()) {
            refClausesSociales.add(getEpmTRefClausesSociales(referentiels, EpmTRefClausesSociales.CODE_CLAUSE_EXECUTION));
        }
        if (clausesSocialesType.isCritereAttribution()) {
            if (clausesSocialesType.isCritereAttribution()) {
                epmTBudLotOuConsultation.setClausesSocialesPresentCriteresAttribution(true);
            }
        }
        MarchePublicType marchePublicType = null;
        if (clausesSocialesType.getMarchePublicClauseSocialeConditionExecution() != null) {
            marchePublicType = clausesSocialesType.getMarchePublicClauseSocialeConditionExecution();
            ajouterMarchePublicClauseSociales(marchePublicType, referentiels, EpmTRefClausesSociales.CODE_CLAUSE_EXECUTION, refClausesSociales, refTypeStructureSociales);
        }
        if (clausesSocialesType.getMarchePublicCritèreSocialCritereAttribution() != null) {
            marchePublicType = clausesSocialesType.getMarchePublicCritèreSocialCritereAttribution();
            ajouterMarchePublicClauseSociales(marchePublicType, referentiels, EpmTRefClausesSociales.CODE_CLAUSE_ATTRIBUTION, refClausesSociales, refTypeStructureSociales);
        }
        if (clausesSocialesType.getMarchPublicClauseSocialeSpecificationTechnique() != null) {
            marchePublicType = clausesSocialesType.getMarchPublicClauseSocialeSpecificationTechnique();
            ajouterMarchePublicClauseSociales(marchePublicType, referentiels, EpmTRefClausesSociales.CODE_CLAUSE_SPECIFICATION, refClausesSociales, refTypeStructureSociales);
        }
        if (clausesSocialesType.getMarcheReserveADes() != null) {
            EpmTRefClausesSociales refClauseSociale = getEpmTRefClausesSociales(referentiels, EpmTRefClausesSociales.CODE_CLAUSE_RESERVE);
            refClausesSociales.add(refClauseSociale);
            EpmTRefTypeStructureSociale refTypeStructureSociale = null;
            if (Boolean.TRUE.equals(clausesSocialesType.getMarcheReserveADes().isEntreprisesAdapteesEtablissementsServicesAide())) {
                refTypeStructureSociale = getStructureSocialeRefByClauseSocialeRefId(referentiels, EpmTRefTypeStructureSociale.CODE_ESAT_EA, refClauseSociale.getId());
            } else if (Boolean.TRUE.equals(clausesSocialesType.getMarcheReserveADes().isEntreprisesEconomieSocialeSolidaire())) {
                refTypeStructureSociale = getStructureSocialeRefByClauseSocialeRefId(referentiels, EpmTRefTypeStructureSociale.CODE_EESS, refClauseSociale.getId());
            } else if (Boolean.TRUE.equals(clausesSocialesType.getMarcheReserveADes().isStructuresInsertionActiviteEconomique())) {
                refTypeStructureSociale = getStructureSocialeRefByClauseSocialeRefId(referentiels, EpmTRefTypeStructureSociale.CODE_SIAE, refClauseSociale.getId());
            }
            if (refTypeStructureSociale != null) {
                refTypeStructureSociales.add(refTypeStructureSociale);
            }
        }
        if (clausesSocialesType.isMarchePublicObjetEstInsertion()) {
            refClausesSociales.add(getEpmTRefClausesSociales(referentiels, EpmTRefClausesSociales.CODE_CLAUSE_INSERTION));
        }

        epmTBudLotOuConsultation.setClausesSocialesChoixUtilisateur(refClausesSociales);
        epmTBudLotOuConsultation.setListeStructureSocialeReserves(refTypeStructureSociales);
    }

    private void ajouterMarchePublicClauseSociales(MarchePublicType marchePublicType, final Referentiels referentiels, String codeClause, final Set<EpmTRefClausesSociales> refClausesSociales, final Set<EpmTRefTypeStructureSociale> refTypeStructureSociales) {
        if (!marchePublicType.getValue().isEmpty()) {
            EpmTRefClausesSociales refClauseSociale = getEpmTRefClausesSociales(referentiels, codeClause);
            refClausesSociales.add(refClauseSociale);
            for (SousClauseSociale1Type sc1t : marchePublicType.getValue()) {
                EpmTRefTypeStructureSociale refTypeStructureSociale = getStructureSocialeRefByClauseSocialeRefId(referentiels, sc1t.value(), refClauseSociale.getId());
                if (refTypeStructureSociale != null) {
                    refTypeStructureSociales.add(refTypeStructureSociale);
                }
            }
        }
    }

    /**
     * Charge les valeurss de chaque lot depuis le flux pour les
     * ajouter dans les lots de l'EpmTConsultation
     *
     * @param consultation
     * @param epmTConsultation
     * @throws NonTrouveNoyauException
     * @throws TechnicalNoyauException
     */
    private void initialiserValeurLots(Consultation consultation, EpmTConsultation epmTConsultation) throws NonTrouveNoyauException, TechnicalNoyauException {

        int compte = 1;
        for (LotType lotNouveau : consultation.getLots().getLot()) {
            // Si le numero de lot dans consultation est null, affecter un numero à partir de 1.
            if (lotNouveau.getNumeroLot() == null || lotNouveau.getNumeroLot().isEmpty()) {
                lotNouveau.setNumeroLot(String.valueOf(compte++));
            }
        }

        if (epmTConsultation.getEpmTBudLots() == null) {
            epmTConsultation.setEpmTBudLots(new HashSet<EpmTBudLot>());
        }

        Set<EpmTBudLot> listLotAncienASupprime = new HashSet<EpmTBudLot>();
        List<LotType> listLotNouveauPourMiseAJour = new ArrayList<LotType>();

        for (Iterator<EpmTBudLot> iter = epmTConsultation.getEpmTBudLots().iterator(); iter.hasNext(); ) {
            EpmTBudLot lotAncien = iter.next();
            int nombrePresentDanslistLotNouveau = 0;
            for (int i = 0; i < consultation.getLots().getLot().size(); i++) {
                LotType lotNouveau = consultation.getLots().getLot().get(i);

                // mise à jour le lot s'il est dans epmTConsultation et consultation (1)
                if ((lotAncien.getNumeroLot()).equals(lotNouveau.getNumeroLot())) {

                    miseAJourEpmTBudLot(lotNouveau, lotAncien, consultation, epmTConsultation, i + 1);
                    listLotNouveauPourMiseAJour.add(lotNouveau);
                    continue;
                } else {
                    nombrePresentDanslistLotNouveau++;
                }
                // si le lot n'est pas dans epmTConsultation, on va le supprimer
                if (nombrePresentDanslistLotNouveau == consultation.getLots().getLot().size()) {
                    listLotAncienASupprime.add(lotAncien);
                }
            }
        }

        // supprimer les lots dans epmTConsultation, ces lots ne sont pas
        // présents dans consultation (2)
        epmTConsultation.getEpmTBudLots().removeAll(listLotAncienASupprime);

        // ajouter les lots dans epmTConsultation qui sont présents dans
        // consultation mais pas dans epmTConsultation (3)
        List<LotType> listlotNouveauAAjouter = new ArrayList<LotType>();
        listlotNouveauAAjouter.addAll(consultation.getLots().getLot());
        listlotNouveauAAjouter.removeAll(listLotNouveauPourMiseAJour);
        for (int i = 0; i < listlotNouveauAAjouter.size(); i++) {
            LotType lotNouveauAAjouter = listlotNouveauAAjouter.get(i);
            epmTConsultation.getEpmTBudLots().add(miseAJourEpmTBudLot(lotNouveauAAjouter, null, consultation, epmTConsultation, i + 1));
        }
    }

    /**
     * la méthode peut mettre à jour l'objet 'epmTBudLot' en reprenant les
     * valeurs de l'objet 'lot', si 'epmTBudLot' est null, cette méthode va
     * créer un nouveau objet de classe 'EpmTBudLot' en reprenant les
     * valeurs de l'objet 'lot'.
     *
     * @param lotNouveau       objet qui apporte les valeurs pour la mise à jour
     * @param lotAMettreAJour  objet à mettre à jour. S'il est null, un nouveau objet de classe 'EpmTBudLot' va être créé
     * @param consultation     la cosultation associé avec 'lot'
     * @param epmTConsultation la cosultation associé avec 'epmTBudLot'
     * @return objet déjà mis à jour ou objet créé
     */
    private EpmTBudLot miseAJourEpmTBudLot(LotType lotNouveau, EpmTBudLot lotAMettreAJour, Consultation consultation, EpmTConsultation epmTConsultation, int id) throws NonTrouveNoyauException, TechnicalNoyauException {

        Referentiels referentiels = referentielsServiceLocal.getAllReferentiels();

        EpmTBudLot epmTBudLot;

        // on met à jour le lot si le lotAMettreAJour est pas null, sinon on crée un nouveau objet du lot
        if (lotAMettreAJour == null) epmTBudLot = new EpmTBudLot();
        else epmTBudLot = lotAMettreAJour;
        epmTBudLot.setId(id);

        epmTBudLot.setDescriptionSuccinte(lotNouveau.getDescription());
        epmTBudLot.setIntituleLot(lotNouveau.getIntitule());

        if (lotNouveau.getNumeroLot() != null && !lotNouveau.getNumeroLot().isEmpty())
            epmTBudLot.setNumeroLot(lotNouveau.getNumeroLot());

        // Nature de la prestation
        epmTBudLot.setEpmTRefNature(getEpmTRefNature(referentiels, consultation, true));

        // Durée du marché
        DureeMarcheType dureeMarcheType = lotNouveau.getDureeMarche();
        EpmTRefChoixMoisJour choixMJ = getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false);
        epmTBudLot.setEpmTRefChoixMoisJour(choixMJ);

        if (dureeMarcheType != null && choixMJ != null) {
            EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription = getEmTRefDureeDelaiDescription(referentiels, dureeMarcheType);
            if (epmTRefDureeDelaiDescription != null) {
                epmTBudLot.setEpmTRefDureeDelaiDescription(epmTRefDureeDelaiDescription);
                if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DUREE_MARCHE_EN_MOIS_EN_JOUR) {
                    if (choixMJ.getId() == EpmTRefChoixMoisJour.EN_JOUR)
                        epmTBudLot.setDureeMarche(Integer.valueOf(dureeMarcheType.getNbJours()));
                    else epmTBudLot.setDureeMarche(Integer.valueOf(dureeMarcheType.getNbMois()));
                    epmTBudLot.setDescriptionDuree(null);
                    epmTBudLot.setDateDebut(null);
                    epmTBudLot.setDateFin(null);
                } else if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DESCRIPTION_LIBRE) {
                    epmTBudLot.setDescriptionDuree(dureeMarcheType.getDescriptionLibre());
                    epmTBudLot.setDureeMarche(null);
                    epmTBudLot.setDateDebut(null);
                    epmTBudLot.setDateFin(null);
                    epmTBudLot.setEpmTRefChoixMoisJour(getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false));
                } else if (epmTRefDureeDelaiDescription.getId() == EpmTRefDureeDelaiDescription.DELAI_EXECUTION_DU_AU) {
                    epmTBudLot.setDureeMarche(null);
                    epmTBudLot.setDescriptionDuree(null);
                    Calendar calendarDateDu = convertGregorianCalendarToCalendar(dureeMarcheType.getDateACompterDu());
                    epmTBudLot.setDateDebut(calendarDateDu);
                    Calendar calendarDateJusquau = convertGregorianCalendarToCalendar(dureeMarcheType.getDateJusquau());
                    epmTBudLot.setDateFin(calendarDateJusquau);
                    epmTBudLot.setEpmTRefChoixMoisJour(getEpmTRefChoixMoisJour(referentiels, dureeMarcheType, false));
                }
            }
        }

        //Codes CPV
        Set<EpmTCpv> cpvSet = epmTBudLot.getCpvs();
        if (lotNouveau.getListeCodeCPV() != null && !lotNouveau.getListeCodeCPV().getCpv().isEmpty()) {
            if (cpvSet == null) {
                cpvSet = new HashSet<>();
                epmTBudLot.setCpvs(cpvSet);
            } else {
                cpvSet.clear();
            }

            for (CPVType cpvType : lotNouveau.getListeCodeCPV().getCpv()) {
                EpmTCpv epmTCpv = new EpmTCpv();

                epmTCpv.setPrincipal(false);
                if (cpvType.isPrincipal()) epmTCpv.setPrincipal(true);

                epmTCpv.setCodeCpv(cpvType.getCodeCPV().trim());

                if (null != cpvType.getLibelleCPV()) {
                    epmTCpv.setLibelleCpv(cpvType.getLibelleCPV().trim());
                } else {
                    epmTCpv.setLibelleCpv("");
                }

                cpvSet.add(epmTCpv);
            }
        }
        epmTBudLot.setCpvs(cpvSet);

        EpmTBudLotOuConsultation epmTBudLotOuConsultation = new EpmTBudLotOuConsultation();
        epmTBudLotOuConsultation = initialiserValeurConsultationInformationsComplementaires(epmTBudLotOuConsultation, lotNouveau.getInformationsComplementaires(), referentiels);

        // Clauses Sociales
        ClausesSocialesType clausesSocialesType = lotNouveau.getClausesSociales();
        if (clausesSocialesType != null) {
            gerererClausesSociales(clausesSocialesType, epmTBudLotOuConsultation, referentiels);
        } else {
            epmTBudLotOuConsultation.setClausesSociales(false);
        }

        // Clauses Environnamentales
        epmTBudLotOuConsultation.setClausesEnvironnementales(false);
        ClausesEnvironnementalesType clausesEnvironnementales = lotNouveau.getClausesEnvironnementales();
        if (clausesEnvironnementales != null) {
            Set<EpmTRefClausesEnvironnementales> refClausesEnvironnementales = new HashSet<>();
            EpmTRefClausesEnvironnementales valeur = new EpmTRefClausesEnvironnementales();
            if (clausesEnvironnementales.isSpecificationTechnique())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_SPECIFICATION));
            if (clausesEnvironnementales.isConditionExecution())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_CONDITIONS));
            if (clausesEnvironnementales.isCritereAttribution())
                refClausesEnvironnementales.add(getEpmTRefClausesEnvironnementales(referentiels, EpmTRefClausesEnvironnementales.CODE_CLAUSE_ATTRIBUTION));
            epmTBudLotOuConsultation.setClausesEnvironnementalesChoixUtilisateur(refClausesEnvironnementales);
            epmTBudLotOuConsultation.setClausesEnvironnementales(true);
        }

        //Criteres d'attribution
        if (lotNouveau.getCriteresAttribution() != null) {
            CriteresAttributionType criteresAttribution = lotNouveau.getCriteresAttribution();

            EpmTRefCritereAttribution refCritereOrganisme = getEpmTRefCritereAttribution(referentiels, criteresAttribution);
            epmTBudLot.setCritereAttribution(refCritereOrganisme);

            Set<EpmTCritereAttributionConsultation> criteresAttributionConsultation = getEpmTCritereAttributionConsultation(referentiels, criteresAttribution);
            epmTBudLot.setListeCritereAttribution(criteresAttributionConsultation);
        }

        epmTBudLot.setEpmTBudLotOuConsultation(epmTBudLotOuConsultation);

        remplirPaireTypeComplexePourLot(lotNouveau, epmTBudLot, epmTConsultation);

        return epmTBudLot;
    }

    private EpmTBudLotOuConsultation initialiserValeurConsultationInformationsComplementaires(EpmTBudLotOuConsultation epmTBudLotOuConsultation, InformationsCommunesLotEtConsultationType informationsCommunesLotEtConsultation, Referentiels referentiels) {

        if (informationsCommunesLotEtConsultation != null) {
            //Variantes autorisees
            boolean isVarianteAutorisee = informationsCommunesLotEtConsultation.isVarianteAutorisee();
            epmTBudLotOuConsultation.setVariantesAutorisees(isVarianteAutorisee ? Constantes.OUI : Constantes.NON);

            //Variantes techniques
            String valeurVarianteTechnique = informationsCommunesLotEtConsultation.getVariantesTechniquesObligatoires();
            epmTBudLotOuConsultation.setOptionsTechniquesImposees(Constantes.NON);
            if (valeurVarianteTechnique != null && !valeurVarianteTechnique.isEmpty()) {
                epmTBudLotOuConsultation.setOptionsTechniquesImposees(Constantes.OUI);
                epmTBudLotOuConsultation.setDescriptionOptionsImposees(valeurVarianteTechnique);
            }

            // Variantes obligatoires
            boolean isVariantesExigees = informationsCommunesLotEtConsultation.isVariantesExigees();
            epmTBudLotOuConsultation.setVariantesExigees(isVariantesExigees ? Constantes.OUI : Constantes.NON);

            //CCAG
            EpmTRefCcag epmTRefCcag = getEpmTRefCcag(referentiels, informationsCommunesLotEtConsultation.getCcag());
            epmTBudLotOuConsultation.setEpmTRefCcag(epmTRefCcag);

            // Droits de propriété intellectuelle
            EpmTRefDroitProprieteIntellectuelle epmTRefDroitProprieteIntellectuelle = getEpmTRefDroitProprieteIntellectuelle(referentiels, informationsCommunesLotEtConsultation.getDroitProprieteIntellectuelle());
            epmTBudLotOuConsultation.setDroitProprieteIntellectuelle(epmTRefDroitProprieteIntellectuelle);

            //Marche reconductible
            if (informationsCommunesLotEtConsultation.getMarcheReconductible() != null) {
                epmTBudLotOuConsultation.setReconductible(Constantes.OUI);
                if (informationsCommunesLotEtConsultation.getMarcheReconductible() != null) {
                    epmTBudLotOuConsultation.setNbReconductions(Integer.valueOf(informationsCommunesLotEtConsultation.getMarcheReconductible().getNombreDeReconduction()));
                }
                epmTBudLotOuConsultation.setModalitesReconduction(informationsCommunesLotEtConsultation.getMarcheReconductible().getModalitesDeReconduction());
            } else {
                epmTBudLotOuConsultation.setReconductible(Constantes.NON);
            }

            //Tranches
            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null && !epmTBudLotOuConsultation.getEpmTBudTranches().isEmpty())
                epmTBudLotOuConsultation.getEpmTBudTranches().clear();

            Set<EpmTBudTranche> epmTBudTranches = getListeEpmTBudTranche(referentiels, informationsCommunesLotEtConsultation.getTranches());

            if (epmTBudLotOuConsultation.getEpmTBudTranches() != null && epmTBudTranches != null)
                epmTBudLotOuConsultation.getEpmTBudTranches().addAll(epmTBudTranches);
            else if (epmTBudTranches != null) epmTBudLotOuConsultation.setEpmTBudTranches(epmTBudTranches);

            Set<EpmTRefTypeStructureSociale> structureSocialeReserves = getListeStructuresSocialeReserves(referentiels, informationsCommunesLotEtConsultation.getStructuresSocialeReserves());
            epmTBudLotOuConsultation.setListeStructureSocialeReserves(structureSocialeReserves);

            //Lots techniques
            if (informationsCommunesLotEtConsultation.getLotsTechniques() != null) {
                List<LotTechniqueType> lotsTechniques = informationsCommunesLotEtConsultation.getLotsTechniques().getLotTechnique();
                Set<EpmTLotTechnique> epmTLotsTechniques = new HashSet<EpmTLotTechnique>();
                for (LotTechniqueType lotTechnique : lotsTechniques) {
                    EpmTLotTechnique epmTLotTechnique = conversionService.map(lotTechnique, EpmTLotTechnique.class);
                    epmTLotTechnique.setPrincipal(lotTechnique.isLotPrincipal() ? Constantes.OUI : Constantes.NON);

                    if (epmTBudLotOuConsultation.getEpmTBudTranches() != null && !epmTBudLotOuConsultation.getEpmTBudTranches().isEmpty()) {
                        Set<EpmTBudTranche> tranchesAssocies = getEpmTBudTranchesAssocies(lotTechnique.getTranches(), epmTBudLotOuConsultation.getEpmTBudTranches());
                        if (epmTLotTechnique.getEpmTBudTranches() != null) {
                            epmTLotTechnique.getEpmTBudTranches().clear();
                            epmTLotTechnique.getEpmTBudTranches().addAll(tranchesAssocies);
                        } else {
                            epmTLotTechnique.setEpmTBudTranches(tranchesAssocies);
                        }
                    }
                    epmTLotsTechniques.add(epmTLotTechnique);
                }
                if (epmTBudLotOuConsultation.getEpmTLotTechniques() != null) {
                    epmTBudLotOuConsultation.getEpmTLotTechniques().clear();
                    epmTBudLotOuConsultation.getEpmTLotTechniques().addAll(epmTLotsTechniques);
                } else {
                    epmTBudLotOuConsultation.setEpmTLotTechniques(epmTLotsTechniques);
                }

                epmTBudLotOuConsultation.setDecompositionLotsTechniques(Constantes.OUI);
            } else {
                epmTBudLotOuConsultation.setDecompositionLotsTechniques(Constantes.NON);
                if (epmTBudLotOuConsultation.getEpmTLotTechniques() != null) {
                    epmTBudLotOuConsultation.getEpmTLotTechniques().clear();
                }
            }

            epmTBudLotOuConsultation.setEpmTBudFormePrix(getEpmTBudFormePrix(referentiels, informationsCommunesLotEtConsultation.getFormeDePrix()));
        }
        return epmTBudLotOuConsultation;
    }

    //TODO
    private EpmTBudFormePrix getEpmTBudFormePrix(Referentiels referentiels, FormeDePrixType formePrixType) {
        EpmTBudFormePrix epmTBudFormePrix = null;

        if (formePrixType != null) {
            FDPPartieForfaitaireType fdpPartieForfaitaireType = formePrixType.getFormeDePrixForfaitaire();
            FDPPartieUnitaireType fdpPartieUnitaireType = formePrixType.getFormeDePrixUnitaire();
            FDPMixteType fdpPartieMixteType = formePrixType.getFormeDePrixMixte();

            if (fdpPartieMixteType != null) {
                EpmTBudFormePrixPm pm = new EpmTBudFormePrixPm();
                EpmTBudFormePrixPf pf = getEpmTBudFormePrixPf(referentiels, fdpPartieMixteType.getPartieForfaitaire());
                pm.setPfDateValeur(pf.getPfDateValeur());
                pm.setPfEpmTRefVariations(pf.getPfEpmTRefVariations());
                pm.setPfEstimationHt(pf.getPfEstimationHt());
                pm.setPfEstimationTtc(pf.getPfEstimationTtc());

                EpmTBudFormePrixPu pu = getEpmTBudFormePrixPu(referentiels, fdpPartieMixteType.getPartieUnitaire());
                pm.setPuDateValeur(pu.getPuDateValeur());
                pm.setPuEpmTRefVariations(pu.getPuEpmTRefVariations());
                pm.setPuEstimationHt(pu.getPuEstimationHt());
                pm.setPuEstimationTtc(pu.getPuEstimationTtc());
                pm.setPuEpmTRefBonQuantite(pu.getEpmTRefBonQuantite());
                pm.setPuEpmTRefMinMax(pu.getEpmTRefMinMax());
                pm.setPuEpmTRefTypePrix(pu.getEpmTRefTypePrix());

                pm.setPuMaxHt(pu.getPuMaxHt());
                pm.setPuMinHt(pu.getPuMinHt());

                epmTBudFormePrix = pm;
            } else if (fdpPartieForfaitaireType != null) {
                epmTBudFormePrix = getEpmTBudFormePrixPf(referentiels, fdpPartieForfaitaireType);
            } else if (fdpPartieUnitaireType != null) {
                epmTBudFormePrix = getEpmTBudFormePrixPu(referentiels, fdpPartieUnitaireType);
            }
        }
        return epmTBudFormePrix;
    }

    /**
     * @param obligatoire Dans le cas ou obligatoire == false, on renvoi le premier
     *                    objet qu'on récupère juste pour faire passer la sauvegarde en
     *                    base de données
     * @return EpmTRefDirectionService
     */
    private EpmTRefDirectionService getEpmTRefDirectionService(final Referentiels referentiels, final Consultation consultation, final boolean obligatoire, Integer idOrganisme) {

        List<EpmTRefDirectionService> directionsServices = filterReferentielByIdOrganisme(referentiels.getRefDirectionServices(), idOrganisme);

        if (!directionsServices.isEmpty()) {
            LOG.info("Liste des directions/services pour l'organisme {} = {}", idOrganisme, directionsServices.stream().map(EpmTRefDirectionService::getId).map(Object::toString).collect(Collectors.joining(",")));
        } else {
            LOG.info("Aucune direction/service pour l'organisme {} ", idOrganisme);
        }

        for (EpmTRefDirectionService directionService : directionsServices) {
            LOG.info("Analyse de la direction/service {} ", directionService);

            if (obligatoire) {
                LOG.info("Code externe de la direction/service en base = {}, de la consultation = {}", directionService.getCodeExterne(), consultation.getDirectionService());

                if (directionService.getCodeExterne() != null && consultation.getDirectionService() != null && consultation.getDirectionService().equals(directionService.getCodeExterne())) {
                    LOG.info("La direction/service {} (code externe = {}) correspond", directionService.getId(), directionService.getCodeExterne());
                    return directionService;
                } else {
                    LOG.info("La direction/service {} (code externe = {}) ne correspond pas", directionService.getId(), directionService.getCodeExterne());
                }
            } else {
                LOG.info("Pas de correspondance obligatoire, direction/service = {}", directionService.getId());

                return directionService;
            }
        }
        return null;
    }

    private EpmTRefTypeStructureSociale getStructureSocialeRefByClauseSocialeRefId(final Referentiels referentiels, String code, int clauseSocialeId) {
        for (EpmTRefTypeStructureSociale refStructureSociale : referentiels.getRefStructureSocialeReserves()) {
            if (refStructureSociale.getCodeExterne() != null && refStructureSociale.getCodeExterne().equalsIgnoreCase(code) && refStructureSociale.getEpmTRefClauseSociale() != null && refStructureSociale.getEpmTRefClauseSociale().getId() == clauseSocialeId)
                return refStructureSociale;
        }
        return null;
    }

    /**
     * @return EpmTRefProcedure
     */
    private EpmTRefProcedure getEpmTRefProcedure(final Consultation consultation, Integer idOrganisme) {

        if (consultation.getCodeExterneProcedure() == null) return null;

        ProcedureCritere critere = new ProcedureCritere();
        critere.setCodeExterne(consultation.getCodeExterneProcedure());
        critere.setLibelleCourt(consultation.getTypeProcedure());

        List<EpmTRefProcedure> procedures = generiqueDAO.findByCritere(critere);
        if (procedures == null || procedures.size() == 0) return null;
        else if (procedures.size() == 1) return procedures.get(0);
        else
            throw new TechnicalNoyauException("ERREUR INTERNE : Couple (code externe, abreviation) ('" + consultation.getCodeExterneProcedure() + "', '" + consultation.getTypeProcedure() + "' n'est pas unique dans la base de données.");
    }

    private Set<EpmTBudTranche> getListeEpmTBudTranche(Referentiels referentiels, ListeTrancheType listeTrancheType) {
        if (listeTrancheType != null && listeTrancheType.getTrancheFixe() != null && listeTrancheType.getTranchesCondionnelle() != null && !listeTrancheType.getTranchesCondionnelle().isEmpty()) {
            Set<EpmTBudTranche> tranches = new HashSet<>();
            EpmTBudTranche epmTBudTrancheFixe = new EpmTBudTranche();
            epmTBudTrancheFixe.setCodeTranche("");
            epmTBudTrancheFixe.setIntituleTranche(listeTrancheType.getTrancheFixe().getIntitule());
            epmTBudTrancheFixe.setEpmTRefNatureTranche(getEpmTRefNatureTranche(referentiels, EpmTRefNatureTranche.TRANCHE_FIXE));
            epmTBudTrancheFixe.setEpmTBudFormePrix(getEpmTBudFormePrix(referentiels, listeTrancheType.getTrancheFixe().getFormeDePrix()));
            tranches.add(epmTBudTrancheFixe);

            EpmTRefNatureTranche natureTrancheConditionnelle = getEpmTRefNatureTranche(referentiels, EpmTRefNatureTranche.TRANCHE_CONDITIONNELLE);
            for (TrancheConditionnelleType trancheConditionnelle : listeTrancheType.getTranchesCondionnelle()) {
                EpmTBudTranche epmTBudTranche = conversionService.map(trancheConditionnelle, EpmTBudTranche.class);
                epmTBudTranche.setEpmTRefNatureTranche(natureTrancheConditionnelle);
                EpmTBudFormePrix fdp = getEpmTBudFormePrix(referentiels, trancheConditionnelle.getFormeDePrix());
                if (fdp != null) {
                    epmTBudTranche.setEpmTBudFormePrix(fdp);
                }
                tranches.add(epmTBudTranche);
            }
            return tranches;
        }

        return null;
    }

    private EpmTBudFormePrixPu getEpmTBudFormePrixPu(Referentiels referentiels, FDPPartieUnitaireType fdpPartieUnitaireType) {
        EpmTBudFormePrixPu pu = new EpmTBudFormePrixPu();

        //infos défauts fdp
        EpmTBudFormePrixPf pf = getEpmTBudFormePrixPf(referentiels, fdpPartieUnitaireType.getPartieForfaitaire());
        pu.setPuDateValeur(pf.getPfDateValeur());
        pu.setPuEpmTRefVariations(pf.getPfEpmTRefVariations());
        pu.setPuEstimationHt(pf.getPfEstimationHt());
        pu.setPuEstimationTtc(pf.getPfEstimationTtc());

        // bon de commande et mini maxi
        EpmTRefBonQuantite epmTRefBonQuantite = getEpmTRefBonQuantite(referentiels, fdpPartieUnitaireType);
        if (epmTRefBonQuantite != null) {
            pu.setEpmTRefBonQuantite(epmTRefBonQuantite);
            if (epmTRefBonQuantite.getId() == EpmTRefBonQuantite.ID_BON_COMMANDE) {
                EpmTRefMinMax epmTRefMinMax = getEpmTRefMinMax(referentiels, fdpPartieUnitaireType.getBonDeCommande());
                if (epmTRefMinMax != null) {
                    pu.setEpmTRefMinMax(epmTRefMinMax);
                    if (epmTRefMinMax.getId() == 1) {
                        MiniMaxiType miniMaxiType = fdpPartieUnitaireType.getBonDeCommande().getAvecMiniMaxi();
                        pu.setPuMinHt(miniMaxiType.getMini().doubleValue());
                        pu.setPuMaxHt(miniMaxiType.getMaxi().doubleValue());
                    }
                }
            }
        }

        //type de prix
        Set<EpmTRefTypePrix> epmTRefTypePrix = getEpmTRefTypePrix(referentiels, fdpPartieUnitaireType.getTypePrix());
        pu.setEpmTRefTypePrix(epmTRefTypePrix);

        return pu;
    }

    private EpmTBudFormePrixPf getEpmTBudFormePrixPf(Referentiels referentiels, FDPPartieForfaitaireType fdpPartieForfaitaireType) {
        EpmTBudFormePrixPf pf = new EpmTBudFormePrixPf();

        if (fdpPartieForfaitaireType != null) {
            Calendar pfDateValeur = convertStringDateToCalendar(fdpPartieForfaitaireType.getDateDeValeur());
            if (pfDateValeur != null) pf.setPfDateValeur(pfDateValeur);

            if (fdpPartieForfaitaireType.getEstimationInterneHT() != null)
                pf.setPfEstimationHt(fdpPartieForfaitaireType.getEstimationInterneHT().doubleValue());

            if (fdpPartieForfaitaireType.getEstimationInterneTTC() != null)
                pf.setPfEstimationTtc(fdpPartieForfaitaireType.getEstimationInterneTTC().doubleValue());

            pf.setPfEpmTRefVariations(getEpmTRefVariation(referentiels, fdpPartieForfaitaireType.getVariationsPrix()));
        }

        return pf;
    }

    private Set<EpmTRefTypePrix> getEpmTRefTypePrix(Referentiels referentiels, TypePrixType typePrixType) {

        Set<EpmTRefTypePrix> epmTRefTypePrix = new HashSet<>();

        if (typePrixType != null && typePrixType.getTypePrix() != null) {
            List<String> libellesTypePrix = typePrixType.getTypePrix();
            for (EpmTRefTypePrix refTypePrix : referentiels.getTypePrix())
                if (libellesTypePrix.contains(refTypePrix.getCodeExterne())) epmTRefTypePrix.add(refTypePrix);
        }
        return epmTRefTypePrix;
    }

    private EpmTRefMinMax getEpmTRefMinMax(Referentiels referentiels, BonDeCommandeType bonDeCommande) {

        if (bonDeCommande != null) {
            for (EpmTRefMinMax epmTRefMinMax : referentiels.getMinMax()) {
                if (bonDeCommande.getAvecMiniMaxi() != null && epmTRefMinMax.getId() == 1) return epmTRefMinMax;
                if (bonDeCommande.isSansMiniMaxi() != null && bonDeCommande.isSansMiniMaxi() && epmTRefMinMax.getId() == 2)
                    return epmTRefMinMax;
            }
        }
        return null;
    }

    private EpmTRefBonQuantite getEpmTRefBonQuantite(Referentiels referentiels, FDPPartieUnitaireType partieUnitaire) {

        if (partieUnitaire != null) {
            for (EpmTRefBonQuantite refBonCommande : referentiels.getBonQuantite()) {
                if (partieUnitaire.getBonDeCommande() != null && refBonCommande.getId() == EpmTRefBonQuantite.ID_BON_COMMANDE)
                    return refBonCommande;
                else if (partieUnitaire.isAutresPrixUnitaires() != null && partieUnitaire.isAutresPrixUnitaires() && refBonCommande.getId() != EpmTRefBonQuantite.ID_BON_COMMANDE)
                    return refBonCommande;
            }
        }
        return null;
    }

    private EpmTRefGroupementAttributaire getEpmTRefGroupementAttributaire(Referentiels referentiels, String formeGroupementAttributaire) {

        if (formeGroupementAttributaire != null) {
            for (EpmTRefGroupementAttributaire refGroupementAttributaire : referentiels.getGroupementAttributaire())
                if (formeGroupementAttributaire.equals(refGroupementAttributaire.getCodeExterne()))
                    return refGroupementAttributaire;
        }
        return null;
    }

    private EpmTRefCritereAttribution getEpmTRefCritereAttribution(Referentiels referentiels, CriteresAttributionType criteresAttribution) {

        if (criteresAttribution != null) {
            for (EpmTRefCritereAttribution epmTRefCritereAttribution : referentiels.getRefCritereAttribution()) {
                //si critere du type "énoncé dans le cahier des charges
                if (criteresAttribution.getCritereCDC() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CAHIER_CHARGES)) {
                    return epmTRefCritereAttribution;
                    //si critere du type "unique du prix plus bas"
                } else if (criteresAttribution.getCriterePrix() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_PRIX)) {
                    return epmTRefCritereAttribution;
                    //si critere du type "unique du cout plus bas"
                } else if (criteresAttribution.getCoutglobal() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_COUT)) {
                    return epmTRefCritereAttribution;
                    //si critere du type "énoncés ci après" => libre
                } else if (criteresAttribution.getListeCritereLibre() != null && criteresAttribution.getListeCritereLibre().getCritereLibre() != null && !criteresAttribution.getListeCritereLibre().getCritereLibre().isEmpty() && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_ORDRE)) {
                    return epmTRefCritereAttribution;
                } else if (criteresAttribution.getListeCriterePondere() != null && criteresAttribution.getListeCriterePondere().getCriterePondere() != null && !criteresAttribution.getListeCriterePondere().getCriterePondere().isEmpty() && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_PONDERE)) {
                    return epmTRefCritereAttribution;
                }
            }
        }
        return null;
    }

    private EpmTRefDureeDelaiDescription getEmTRefDureeDelaiDescription(Referentiels referentiels, DureeMarcheType dureeMarcheType) {

        for (EpmTRefDureeDelaiDescription epmTRefDureeDelaiDescription : referentiels.getDureeDelaiDescription()) {
            if ((dureeMarcheType.getNbJours() != null || dureeMarcheType.getNbMois() != null) && EpmTRefDureeDelaiDescription.DUREE_MARCHE_EN_MOIS_EN_JOUR == epmTRefDureeDelaiDescription.getId()) {
                return epmTRefDureeDelaiDescription;
            } else if (dureeMarcheType.getDescriptionLibre() != null && EpmTRefDureeDelaiDescription.DESCRIPTION_LIBRE == epmTRefDureeDelaiDescription.getId()) {
                return epmTRefDureeDelaiDescription;
            } else if (dureeMarcheType.getDateACompterDu() != null && dureeMarcheType.getDateJusquau() != null && EpmTRefDureeDelaiDescription.DELAI_EXECUTION_DU_AU == epmTRefDureeDelaiDescription.getId()) {
                return epmTRefDureeDelaiDescription;
            }
        }
        return null;
    }

    private EpmTRefNatureTranche getEpmTRefNatureTranche(Referentiels referentiels, int idTranche) {

        for (EpmTRefNatureTranche epmTRefNatureTranche : referentiels.getNatureTranche())
            if (epmTRefNatureTranche.getId() == idTranche) return epmTRefNatureTranche;
        return null;
    }

    private Calendar convertGregorianCalendarToCalendar(XMLGregorianCalendar dateACompterDuXML) {
        Date dateDu = dateACompterDuXML.toGregorianCalendar().getTime();
        Calendar calendarDateDu = Calendar.getInstance();
        calendarDateDu.setTime(dateDu);
        return calendarDateDu;
    }

    private Calendar convertStringDateToCalendar(String dateDeValeur) {
        try {
            if (dateDeValeur != null && !dateDeValeur.isEmpty()) {
                DateFormat pfDateValeurFormat = new SimpleDateFormat("MM/yyyy");
                Date pfDateValeur = pfDateValeurFormat.parse(dateDeValeur);
                Calendar calendarPfDateValeur = Calendar.getInstance();
                calendarPfDateValeur.setTime(pfDateValeur);
                return calendarPfDateValeur;
            }
        } catch (ParseException e) {
            LOG.debug(e.getMessage(), e.fillInStackTrace());
        }

        return null;
    }

    private <T extends EpmTReferentielByOrganisme> List<T> filterReferentielByIdOrganisme(Collection<T> refs, Integer idOrganisme) {
        List<T> resultatOrganisme = new ArrayList<T>();
        List<T> resultatDefault = new ArrayList<T>();

        LOG.info("Recherche de l'organisme {}", idOrganisme);

        for (T referentiel : refs) {
            if (referentiel.getEpmTRefOrganisme() == null) {
                resultatDefault.add(referentiel);
            } else if (referentiel.getEpmTRefOrganisme() != null && referentiel.getEpmTRefOrganisme().getId().equals(idOrganisme)) {
                LOG.info("La direction/service avec id {} correspond a l'organisme {}", referentiel.getId(), referentiel.getEpmTRefOrganisme().getId());

                resultatOrganisme.add(referentiel);
            }
        }

        if (resultatOrganisme.isEmpty()) {
            LOG.info("Aucune direction/service ne correspond a l'organisme {}. Récupération de celles par défaut = {}", idOrganisme, resultatDefault);

            return resultatDefault;
        }

        return resultatOrganisme;
    }

    private EpmTRefNbCandidatsAdmis getEpmTRefNbCandidatsAdmis(Referentiels referentiels, int idNbCandidatAdmin) {

        for (EpmTRefNbCandidatsAdmis epmTRefNbCandidat : referentiels.getNbCandidatsAdmis())
            if (epmTRefNbCandidat.getId() == idNbCandidatAdmin) return epmTRefNbCandidat;
        return null;
    }

    /**
     * la méthode peut créer une liste de EpmTCritereAttributionConsultation en récupérant un objet de CriteresAttributionType
     *
     * @param referentiels        les referentiels
     * @param criteresAttribution les criteres d'attribution
     * @return la liste de EpmTCritereAttributionConsultation
     */
    private Set<EpmTCritereAttributionConsultation> getEpmTCritereAttributionConsultation(Referentiels referentiels, CriteresAttributionType criteresAttribution) {

        Set<EpmTCritereAttributionConsultation> listeCritereAttributionConsultation = new HashSet<EpmTCritereAttributionConsultation>();

        if (criteresAttribution != null) {
            for (EpmTRefCritereAttribution epmTRefCritereAttribution : referentiels.getRefCritereAttribution()) {

                //si critere du type "énoncé dans le cahier des charges
                if (criteresAttribution.getCritereCDC() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CAHIER_CHARGES)) {
                    EpmTCritereAttributionConsultation epmTCritereAttributionConsultation = new EpmTCritereAttributionConsultation();
                    epmTCritereAttributionConsultation.setId(0);
                    epmTCritereAttributionConsultation.setEnonce(epmTRefCritereAttribution.getLibelle());
                    epmTCritereAttributionConsultation.setOrdre(0);
                    listeCritereAttributionConsultation.add(epmTCritereAttributionConsultation);

                    return listeCritereAttributionConsultation;

                    //si critere du type "unique du prix plus bas"
                } else if (criteresAttribution.getCriterePrix() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_PRIX)) {
                    EpmTCritereAttributionConsultation epmTCritereAttributionConsultation = new EpmTCritereAttributionConsultation();
                    epmTCritereAttributionConsultation.setId(0);
                    epmTCritereAttributionConsultation.setEnonce(epmTRefCritereAttribution.getLibelle());
                    epmTCritereAttributionConsultation.setOrdre(0);
                    listeCritereAttributionConsultation.add(epmTCritereAttributionConsultation);

                    return listeCritereAttributionConsultation;

                    //si critere du type "unique du cout plus bas"
                } else if (criteresAttribution.getCoutglobal() != null && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_COUT)) {
                    EpmTCritereAttributionConsultation epmTCritereAttributionConsultation = new EpmTCritereAttributionConsultation();
                    epmTCritereAttributionConsultation.setId(0);
                    epmTCritereAttributionConsultation.setEnonce(epmTRefCritereAttribution.getLibelle());
                    epmTCritereAttributionConsultation.setOrdre(0);
                    listeCritereAttributionConsultation.add(epmTCritereAttributionConsultation);

                    return listeCritereAttributionConsultation;

                    //si critere du type "énoncés ci après" => libre
                } else if (criteresAttribution.getListeCritereLibre() != null && criteresAttribution.getListeCritereLibre().getCritereLibre() != null && !criteresAttribution.getListeCritereLibre().getCritereLibre().isEmpty() && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_ORDRE)) {

                    int compte = 0;
                    for (CritereLibreType critereLibre : criteresAttribution.getListeCritereLibre().getCritereLibre()) {

                        EpmTCritereAttributionConsultation epmTCritereAttributionConsultation = new EpmTCritereAttributionConsultation();
                        epmTCritereAttributionConsultation.setId(0);
                        epmTCritereAttributionConsultation.setEnonce(critereLibre.getEnonce().trim());
                        epmTCritereAttributionConsultation.setOrdre(compte++);
                        if (critereLibre.getListeSousCriteresAttribution() != null && critereLibre.getListeSousCriteresAttribution().getSousCriteresAttribution() != null) {
                            Set<EpmTSousCritereAttributionConsultation> listeSousCriteres = new HashSet<EpmTSousCritereAttributionConsultation>();

                            int ordreSC = 0;
                            for (SousCriteresAttributionType sousCriteresAttribution : critereLibre.getListeSousCriteresAttribution().getSousCriteresAttribution()) {
                                EpmTSousCritereAttributionConsultation epmTSousCritere = new EpmTSousCritereAttributionConsultation();
                                epmTSousCritere.setId(0);
                                epmTSousCritere.setEnonce(sousCriteresAttribution.getCritere());
                                epmTSousCritere.setOrdre(ordreSC++);
                                listeSousCriteres.add(epmTSousCritere);
                            }
                            epmTCritereAttributionConsultation.setSousCritere(listeSousCriteres);
                        }
                        listeCritereAttributionConsultation.add(epmTCritereAttributionConsultation);
                    }

                    return listeCritereAttributionConsultation;
                } else if (criteresAttribution.getListeCriterePondere() != null && criteresAttribution.getListeCriterePondere().getCriterePondere() != null && !criteresAttribution.getListeCriterePondere().getCriterePondere().isEmpty() && epmTRefCritereAttribution.getCodeExterne().equals(EpmTRefCritereAttribution.TYPE_CRITERE_PONDERE)) {

                    int compte = 0;
                    for (CriterePondereType criterePondere : criteresAttribution.getListeCriterePondere().getCriterePondere()) {
                        EpmTCritereAttributionConsultation epmTCritereAttributionConsultation = new EpmTCritereAttributionConsultation();
                        epmTCritereAttributionConsultation.setId(0);
                        epmTCritereAttributionConsultation.setEnonce(criterePondere.getEnonce().trim());
                        epmTCritereAttributionConsultation.setOrdre(compte++);
                        Double ponderation = Double.valueOf(criterePondere.getPourcentagePonderation());
                        epmTCritereAttributionConsultation.setPonderation(ponderation);
                        if (criterePondere.getListeSousCriteresAttribution() != null && criterePondere.getListeSousCriteresAttribution().getSousCriteresAttribution() != null) {
                            Set<EpmTSousCritereAttributionConsultation> listeSousCriteres = new HashSet<EpmTSousCritereAttributionConsultation>();

                            int ordreSC = 0;
                            for (SousCriteresAttributionType sousCriteresAttribution : criterePondere.getListeSousCriteresAttribution().getSousCriteresAttribution()) {
                                EpmTSousCritereAttributionConsultation epmTSousCritere = new EpmTSousCritereAttributionConsultation();
                                epmTSousCritere.setId(0);
                                epmTSousCritere.setEnonce(sousCriteresAttribution.getCritere());
                                epmTSousCritere.setPonderation(Double.valueOf(sousCriteresAttribution.getPourcentagePonderation()));
                                epmTSousCritere.setOrdre(ordreSC++);
                                listeSousCriteres.add(epmTSousCritere);
                            }
                            epmTCritereAttributionConsultation.setSousCritere(listeSousCriteres);
                        }
                        listeCritereAttributionConsultation.add(epmTCritereAttributionConsultation);
                    }

                    return listeCritereAttributionConsultation;
                }
            }
        }
        return listeCritereAttributionConsultation;
    }

    private Set<EpmTBudTranche> getEpmTBudTranchesAssocies(ListCodeTrancheType listCodesTranches, Set<EpmTBudTranche> tranches) {
        Set<EpmTBudTranche> tranchesAssocies = new HashSet<EpmTBudTranche>();

        if (listCodesTranches != null && listCodesTranches.getCodeTranche() != null && !listCodesTranches.getCodeTranche().isEmpty()) {
            for (EpmTBudTranche tranche : tranches) {
                if (listCodesTranches.getCodeTranche().contains(tranche.getCodeTranche()) || (tranche.getEpmTRefNatureTranche().getId() == EpmTRefNatureTranche.TRANCHE_FIXE && (listCodesTranches.getCodeTranche().contains(IDENTIFIANT_TRANCHE_FIXE)))) {
                    tranchesAssocies.add(tranche);
                }
            }
        }
        return tranchesAssocies;
    }

    private EpmTRefReponseElectronique getEpmTRefReponseElectronique(Referentiels referentiels, Consultation consultation) {
        for (EpmTRefReponseElectronique epmTRefReponseElectronique : referentiels.getRefReponseElectronique())
            if (epmTRefReponseElectronique.getCodeExterne() != null && consultation.getReponseElectronique() != null && consultation.getReponseElectronique().equals(epmTRefReponseElectronique.getCodeExterne()))
                return epmTRefReponseElectronique;
        return null;
    }

    /**
     * Recherche dans le referentiel Droits de propriété intellectuelle correspondant à celui envoyé par le flux MPE.
     */
    private EpmTRefDroitProprieteIntellectuelle getEpmTRefDroitProprieteIntellectuelle(final Referentiels referentiels, final String droitProprieteIntellectuelle) {
        for (EpmTRefDroitProprieteIntellectuelle epmTRefDroitProprieteIntellectuelle : referentiels.getRefDroitProprieteIntellectuelle())
            if (epmTRefDroitProprieteIntellectuelle.getCodeExterne() != null && epmTRefDroitProprieteIntellectuelle.getCodeExterne().equals(droitProprieteIntellectuelle))
                return epmTRefDroitProprieteIntellectuelle;
        return null;
    }

    private Set<EpmTRefTypeStructureSociale> getListeStructuresSocialeReserves(Referentiels referentiels, StructuresSocialeReserveType structuresSocialeReserveType) {
        Set<EpmTRefTypeStructureSociale> resultat = new HashSet<>();

        if (structuresSocialeReserveType != null) {
            List<String> structureSocialeReserves = structuresSocialeReserveType.getStructureSocialeReserves();
            for (String code : structureSocialeReserves) {
                for (EpmTRefTypeStructureSociale epmTRefTypeStructureSociale : referentiels.getRefStructureSocialeReserves()) {
                    if (epmTRefTypeStructureSociale.getCodeExterne().equals(code))
                        resultat.add(epmTRefTypeStructureSociale);
                }
            }
        }
        return resultat;
    }

    private Set<EpmTRefLieuExecution> getListEpmTLieuExecutions(Referentiels referentiels, List<String> listCodeLieuExecutions) {
        Set<EpmTRefLieuExecution> result = new HashSet<EpmTRefLieuExecution>();

        List<EpmTRefLieuExecution> epmTRefLieuExecutions = referentiels.getRefLieuExecution();
        for (String codeLieuExecution : listCodeLieuExecutions)
            for (EpmTRefLieuExecution lieuExecution : epmTRefLieuExecutions)
                if (lieuExecution.getCodeExterne().equals(codeLieuExecution)) result.add(lieuExecution);
        return result;
    }

    /**
     * sauvegarder ou modifier la date d'une étape de calendrier
     *
     * @return
     */
    private EpmTCalendrier sauvegardeCalendrier(Date date, int typeEtape) throws TechnicalNoyauException, ApplicationNoyauException {

        // Date de remise de plis
        EtapeSimple etape = null;


        etape = new EtapeSimple();
        etape.setDate(date);

        // Sauvegarde des dates de calendrier
        EpmTEtapeCal etapeCal;
        etapeCal = new EpmTEtapeCal();
        etapeCal.setTypeEtape(typeEtape);
        etapeCal.setDateHerite(etape.getDate());

        EpmTCalendrier epmTCalendrier = null;

        epmTCalendrier = new EpmTCalendrier();

        if (epmTCalendrier.getEtapes() == null) {
            epmTCalendrier.setEtapes(new ArrayList<EpmTEtapeCal>());
        }

        epmTCalendrier.getEtapes().add(etapeCal);
        return epmTCalendrier;
    }

    @Autowired
    public final void setConversionService(final DozerBeanMapper valeur) {
        this.conversionService = valeur;
    }

    @Autowired
    public final void setReferentielsServiceLocal(ReferentielsServiceLocal referentielsServiceLocal) {
        this.referentielsServiceLocal = referentielsServiceLocal;
    }

    @Autowired
    public final void setGeneriqueDAO(GeneriqueDAO generiqueDAO) {
        this.generiqueDAO = generiqueDAO;
    }
}
