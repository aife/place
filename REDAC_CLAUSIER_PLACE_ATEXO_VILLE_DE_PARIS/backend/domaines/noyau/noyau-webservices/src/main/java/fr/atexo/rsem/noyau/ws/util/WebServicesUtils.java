package fr.atexo.rsem.noyau.ws.util;

public class WebServicesUtils {
/**
 * @author KBE
 */
private String repertoireTemporaire;

/**
 * @return
 */


public String getRepertoireTemporaire() {
    
    return repertoireTemporaire;
}

public WebServicesUtils() {
    
}

public WebServicesUtils(String repertoireTemporaire) {
    
    this.repertoireTemporaire = repertoireTemporaire;
}

/**
 * @param repertoireTemporaire
 */
public void setRepertoireTemporaire(String repertoireTemporaire) {
    this.repertoireTemporaire = repertoireTemporaire;
}
}
