/**
 * 
 */
package fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions;


/**
 * @author KBE
 *
 */
public class RsemWsClientGlobaleException extends GlobaleException {

    

    public RsemWsClientGlobaleException() {
        super();
        
    }

    /**
     * @param cause
     */
    public RsemWsClientGlobaleException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public RsemWsClientGlobaleException(String message) {
        super(message);
     }

    
    public RsemWsClientGlobaleException(Throwable cause,String message) {
        super(cause);
        this.setMessage(message);
        // TODO Auto-generated constructor stub
    }
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
