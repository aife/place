package fr.atexo.rsem.noyau.ws.rest;

import java.util.UUID;

public class Agent {
	private UUID uuid;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid( UUID uuid ) {
		this.uuid = uuid;
	}
}
