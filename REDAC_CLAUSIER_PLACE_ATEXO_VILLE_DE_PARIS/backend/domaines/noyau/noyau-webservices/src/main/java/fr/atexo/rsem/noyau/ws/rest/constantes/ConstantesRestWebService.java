package fr.atexo.rsem.noyau.ws.rest.constantes;

/**
 * Constantes utilisees dans le cadre des Web Services REST.
 * @author RVI
 * @version $Revision$, $Date$, $Author$
 */
public final class ConstantesRestWebService {
    /**
     * Nombre de digit pour la creation du token des webservices REST.
     */
    public static final int TOKEN_SIGNATURE_WS_REST_NB_DIGIT = 15;
    /**
     * Balise encapsulant un message REST token.
     */
    public static final String XML_QNAME_TOKEN = "ticket";
    /**
     * Balise encapsulant un message REST erreur.
     */
    public static final String XML_QNAME_ERREUR = "erreur";
    /**
     * Balise encapsulant un message REST consultation.
     */
    public static final String XML_QNAME_CONSULTATION = "consultation";
    /**
     * Balise encapsulant un message REST qui contient un ensemble de consultations.
     */
    public static final String XML_QNAME_CONSULTATIONS = "consultations";
    
    public static final String XML_QNAME_RESULTAT = "resultat";
    /**
     * Balise encapsulant un message REST deconnexion.
     */
    public static final String XML_QNAME_DECONNEXION = "deconnexion";
    
    public static final String XML_QNAME_IDENTIFIANT = "identifiant";
    
    public static final String XML_QNAME_APPLICATION_TIERS = "applicationTiers";

}
