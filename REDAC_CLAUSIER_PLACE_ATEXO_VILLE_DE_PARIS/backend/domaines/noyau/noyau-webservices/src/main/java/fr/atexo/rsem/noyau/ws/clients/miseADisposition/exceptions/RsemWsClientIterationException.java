/**
 * 
 */
package fr.atexo.rsem.noyau.ws.clients.miseADisposition.exceptions;


/**
 * @author KBE
 *
 */
public class RsemWsClientIterationException extends IterationException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public RsemWsClientIterationException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public RsemWsClientIterationException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     * @param message
     */
    public RsemWsClientIterationException(Throwable cause, String message) {
        super(cause, message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public RsemWsClientIterationException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
