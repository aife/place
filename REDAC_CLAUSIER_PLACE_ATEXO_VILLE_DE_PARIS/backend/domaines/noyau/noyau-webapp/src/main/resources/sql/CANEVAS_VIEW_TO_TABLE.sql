DROP VIEW IF EXISTS redaction.epm__v_canevas;

CREATE OR REPLACE TABLE redaction.epm__v_canevas AS 
select 
       null AS id,
       view_canevas.id_canevas AS id_canevas,
       view_canevas.id_canevas_publication AS id_canevas_publication,
       view_canevas.id_publication AS id_publication,
       view_canevas.id_document_type AS id_document_type,
       view_canevas.titre AS titre,
       view_canevas.reference AS reference,
       view_canevas.date_creation AS date_creation,
       view_canevas.date_modification AS date_modification,
       view_canevas.id_nature_prestation AS id_nature_prestation,
       view_canevas.auteur AS auteur,
       view_canevas.etat AS etat,
       view_canevas.id_statut_redaction_clausier AS id_statut_redaction_clausier,
       view_canevas.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
       view_canevas.id_organisme AS id_organisme,
       view_canevas.canevas_editeur AS canevas_editeur,
       view_canevas.date_premiere_validation AS date_premiere_validation,
       view_canevas.date_derniere_validation AS date_derniere_validation,
       view_canevas.id_ref_ccag AS id_ref_ccag,
       view_canevas.actif AS actif 
FROM (
       SELECT 
              NULL AS id_canevas,
              canevas.id_canevas AS id_canevas_publication,
              canevas.id_publication AS id_publication,
              canevas.id_document_type AS id_document_type,
              canevas.titre AS titre,
              canevas.reference AS reference,
              canevas.date_creation AS date_creation,
              canevas.date_modification AS date_modification,
              canevas.id_nature_prestation AS id_nature_prestation,
              canevas.auteur AS auteur,
              canevas.etat AS etat,
              canevas.id_statut_redaction_clausier AS id_statut_redaction_clausier,
              canevas.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
              canevas.id_organisme AS id_organisme,
              canevas.canevas_editeur AS canevas_editeur,
              canevas.date_premiere_validation AS date_premiere_validation,
              canevas.date_derniere_validation AS date_derniere_validation,
              canevas.id_ref_ccag AS id_ref_ccag,
              canevas.actif AS actif 
       FROM redaction.epm__t_canevas_pub canevas 
       LEFT JOIN redaction.epm__t_publication_clausier publication ON canevas.id_publication = publication.id 
       WHERE publication.actif = 1 
              
       UNION 
       
       SELECT 
              canevas.id AS id_canevas,
              NULL AS id_canevas_publication,
              NULL AS id_publication,
              canevas.id_document_type AS id_document_type,
              canevas.titre AS titre,
              canevas.reference AS reference,
              canevas.date_creation AS date_creation,
              canevas.date_modification AS date_modification,
              canevas.id_nature_prestation AS id_nature_prestation,
              canevas.auteur AS auteur,
              canevas.etat AS etat,
              canevas.id_statut_redaction_clausier AS id_statut_redaction_clausier,
              canevas.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
              canevas.id_organisme AS id_organisme,
              canevas.canevas_editeur AS canevas_editeur,
              canevas.date_premiere_validation AS date_premiere_validation,
              canevas.date_derniere_validation AS date_derniere_validation,
              canevas.id_ref_ccag AS id_ref_ccag,
              canevas.actif AS actif 
       FROM redaction.epm__t_canevas canevas 
       WHERE canevas.canevas_editeur = 0
) view_canevas;


ALTER TABLE redaction.epm__v_canevas MODIFY id BIGINT(21) NOT NULL AUTO_INCREMENT PRIMARY KEY;

DROP INDEX IF EXISTS index_canevas_pub_reference ON redaction.epm__t_canevas_pub;
DROP INDEX IF EXISTS index_canevas_reference  ON redaction.epm__t_canevas;
DROP INDEX IF EXISTS index_canevas_editeur ON redaction.epm__t_canevas;
DROP INDEX IF EXISTS index_canevas_vue_reference  ON redaction.epm__v_canevas;

CREATE INDEX index_canevas_pub_reference on redaction.epm__t_canevas_pub(reference) using BTREE;
CREATE INDEX index_canevas_reference on redaction.epm__t_canevas(reference) using BTREE;
CREATE INDEX index_canevas_editeur on redaction.epm__t_canevas(canevas_editeur) using BTREE;
CREATE INDEX index_canevas_vue_reference on redaction.epm__v_canevas(reference) using BTREE;
