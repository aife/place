DROP VIEW IF EXISTS redaction.epm__v_clause ;

CREATE OR REPLACE TABLE redaction.epm__v_clause 
AS 
SELECT 
       null AS id,
       view_clause.id_clause AS id_clause,
       view_clause.id_clause_publication AS id_clause_publication,
       view_clause.id_publication AS id_publication,
       view_clause.id_ref_type_document AS id_ref_type_document,
       view_clause.info_bulle_text AS info_bulle_text,
       view_clause.info_bulle_url AS info_bulle_url,
       view_clause.id_ref_type_clause AS id_ref_type_clause,
       view_clause.id_theme AS id_theme,
       view_clause.reference AS reference,
       view_clause.mots_cles AS mots_cles,
       view_clause.parametrable_direction AS parametrable_direction,
       view_clause.parametrable_agent AS parametrable_agent,
       view_clause.texte_fixe_avant AS texte_fixe_avant,
       view_clause.texte_fixe_apres AS texte_fixe_apres,
       view_clause.date_modification AS date_modification,
       view_clause.date_creation AS date_creation,
       view_clause.auteur AS auteur,
       view_clause.actif AS actif,
       view_clause.id_nature_prestation AS id_nature_prestation,
       view_clause.id_procedure AS id_procedure,
       view_clause.etat AS etat,
       view_clause.formulation_modifiable AS formulation_modifiable,
       view_clause.saut_ligne_texte_avant AS saut_ligne_texte_avant,
       view_clause.saut_ligne_texte_apres AS saut_ligne_texte_apres,
       view_clause.id_statut_redaction_clausier AS id_statut_redaction_clausier,
       view_clause.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
       view_clause.id_organisme AS id_organisme,
       view_clause.clause_editeur AS clause_editeur,
       view_clause.date_premiere_validation AS date_premiere_validation,
       view_clause.date_derniere_validation AS date_derniere_validation 
       FROM 
       (
              SELECT NULL AS id_clause,
                     clause.id_clause AS id_clause_publication,
                     clause.id_publication AS id_publication,
                     clause.id_ref_type_document AS id_ref_type_document,
                     clause.info_bulle_text AS info_bulle_text,
                     clause.info_bulle_url AS info_bulle_url,
                     clause.id_ref_type_clause AS id_ref_type_clause,
                     clause.id_theme AS id_theme,
                     clause.reference AS reference,
                     clause.mots_cles AS mots_cles,
                     clause.parametrable_direction AS parametrable_direction,
                     clause.parametrable_agent AS parametrable_agent,
                     clause.texte_fixe_avant AS texte_fixe_avant,
                     clause.texte_fixe_apres AS texte_fixe_apres,
                     clause.date_modification AS date_modification,
                     clause.date_creation AS date_creation,
                     clause.auteur AS auteur,
                     clause.actif AS actif,
                     clause.id_nature_prestation AS id_nature_prestation,
                     clause.id_procedure AS id_procedure,
                     clause.etat AS etat,
                     clause.formulation_modifiable AS formulation_modifiable,
                     clause.saut_ligne_texte_avant AS saut_ligne_texte_avant,
                     clause.saut_ligne_texte_apres AS saut_ligne_texte_apres,
                     clause.id_statut_redaction_clausier AS id_statut_redaction_clausier,
                     clause.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
                     clause.id_organisme AS id_organisme,
                     clause.clause_editeur AS clause_editeur,
                     clause.date_premiere_validation AS date_premiere_validation,
                     clause.date_derniere_validation AS date_derniere_validation 
              FROM  redaction.epm__t_clause_pub clause 
              LEFT JOIN redaction.epm__t_publication_clausier publication ON clause.id_publication = publication.id 
              WHERE  publication.actif = 1 

              UNION 
              
              SELECT 
                     clause.id AS id_clause,
                     NULL AS id_clause_publication,
                     NULL AS id_publication,
                     clause.id_ref_type_document AS id_ref_type_document,
                     clause.info_bulle_text AS info_bulle_text,
                     clause.info_bulle_url AS info_bulle_url,
                     clause.id_ref_type_clause AS id_ref_type_clause,
                     clause.id_theme AS id_theme,
                     clause.reference AS reference,
                     clause.mots_cles AS mots_cles,
                     clause.parametrable_direction AS parametrable_direction,
                     clause.parametrable_agent AS parametrable_agent,
                     clause.texte_fixe_avant AS texte_fixe_avant,
                     clause.texte_fixe_apres AS texte_fixe_apres,
                     clause.date_modification AS date_modification,
                     clause.date_creation AS date_creation,
                     clause.auteur AS auteur,
                     clause.actif AS actif,
                     clause.id_nature_prestation AS id_nature_prestation,
                     clause.id_procedure AS id_procedure,
                     clause.etat AS etat,
                     clause.formulation_modifiable AS formulation_modifiable,
                     clause.saut_ligne_texte_avant AS saut_ligne_texte_avant,
                     clause.saut_ligne_texte_apres AS saut_ligne_texte_apres,
                     clause.id_statut_redaction_clausier AS id_statut_redaction_clausier,
                     clause.compatible_entite_adjudicatrice AS compatible_entite_adjudicatrice,
                     clause.id_organisme AS id_organisme,
                     clause.clause_editeur AS clause_editeur,
                     clause.date_premiere_validation AS date_premiere_validation,
                     clause.date_derniere_validation AS date_derniere_validation 
              FROM redaction.epm__t_clause clause 
              WHERE clause.clause_editeur = 0
       ) 
view_clause ;

ALTER TABLE redaction.epm__v_clause MODIFY id BIGINT(21) NOT NULL AUTO_INCREMENT PRIMARY KEY;

DROP INDEX IF EXISTS index_actif_clausier ON redaction.epm__t_publication_clausier;
DROP INDEX IF EXISTS index_clause_editeur ON redaction.epm__t_clause;
DROP INDEX IF EXISTS index_clause_pub_reference ON redaction.epm__t_clause_pub;
DROP INDEX IF EXISTS index_clause_reference  ON redaction.epm__t_clause;
DROP INDEX IF EXISTS index_clause_vue_reference  ON redaction.epm__v_clause;


CREATE INDEX index_actif_clausier on redaction.epm__t_publication_clausier(actif) using BTREE;
CREATE INDEX index_clause_editeur on redaction.epm__t_clause(clause_editeur) using BTREE;
CREATE INDEX index_clause_pub_reference on redaction.epm__t_clause_pub(reference) using BTREE;
CREATE INDEX index_clause_reference on redaction.epm__t_clause(reference) using BTREE;
CREATE INDEX index_clause_vue_reference on redaction.epm__v_clause(reference) using BTREE;
