DROP TRIGGER IF EXISTS redaction.CANEVAS_INSERT_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CANEVAS_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CANEVAS_DELETE_TRIGGER;
/*DROP TRIGGER IF EXISTS redaction.CANEVAS_PUB_INSERT_TRIGGER;*/
DROP TRIGGER IF EXISTS redaction.CANEVAS_PUB_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CANEVAS_PUB_DELETE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CANEVAS_PUBLICATION_CLAUSIER_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CANEVAS_PUBLICATION_CLAUSIER_DELETE_TRIGGER;

CREATE OR REPLACE TRIGGER redaction.CANEVAS_INSERT_TRIGGER AFTER INSERT ON redaction.epm__t_canevas
FOR EACH ROW
    INSERT INTO redaction.epm__v_canevas (
        id,
        id_canevas,
        id_canevas_publication,
        id_publication,
        id_document_type,
        titre,
        reference,
        date_creation,
        date_modification,
        id_nature_prestation,
        auteur,
        etat,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        canevas_editeur,
        date_premiere_validation,
        date_derniere_validation,
        id_ref_ccag,
        actif 
    )
    VALUES
    (
        NULL,
        new.id,
        NULL,
        NULL,
        new.id_document_type,
        new.titre,
        new.reference,
        new.date_creation,
        new.date_modification,
        new.id_nature_prestation,
        new.auteur,
        new.etat,
        new.id_statut_redaction_clausier,
        new.compatible_entite_adjudicatrice,
        new.id_organisme,
        new.canevas_editeur,
        new.date_premiere_validation,
        new.date_derniere_validation,
        new.id_ref_ccag,
        new.actif 
   );

CREATE OR REPLACE TRIGGER redaction.CANEVAS_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_canevas
FOR EACH ROW
    UPDATE redaction.epm__v_canevas
    SET
        id_canevas = new.id,
        id_canevas_publication = NULL,
        id_document_type = new.id_document_type,
        titre = new.titre,
        reference = new.reference,
        date_creation = new.date_creation,
        date_modification = new.date_modification,
        id_nature_prestation = new.id_nature_prestation,
        auteur = new.auteur,
        etat = new.etat,
        id_statut_redaction_clausier = new.id_statut_redaction_clausier,
        compatible_entite_adjudicatrice = new.compatible_entite_adjudicatrice,
        id_organisme = new.id_organisme,
        canevas_editeur = new.canevas_editeur,
        date_premiere_validation = new.date_premiere_validation,
        date_derniere_validation = new.date_derniere_validation,
        id_ref_ccag = new.id_ref_ccag,
        actif = new.actif 
    WHERE id_canevas = new.id;

CREATE OR REPLACE TRIGGER redaction.CANEVAS_DELETE_TRIGGER BEFORE DELETE ON redaction.epm__t_canevas
FOR EACH ROW
    DELETE FROM redaction.epm__v_canevas WHERE id_canevas = old.id;
/*
CREATE OR REPLACE TRIGGER redaction.CANEVAS_PUB_INSERT_TRIGGER AFTER INSERT ON redaction.epm__t_canevas_pub
FOR EACH ROW
    INSERT INTO redaction.epm__v_canevas (
        id,
        id_canevas,
        id_canevas_publication,
        id_publication,
        id_document_type,
        titre,
        reference,
        date_creation,
        date_modification,
        id_nature_prestation,
        auteur,
        etat,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        canevas_editeur,
        date_premiere_validation,
        date_derniere_validation,
        id_ref_ccag,
        actif 
    )
    SELECT
    (
        NULL,
        NULL,
        new.id_canevas,
        new.id_publication,
        new.id_document_type,
        new.titre,
        new.reference,
        new.date_creation,
        new.date_modification,
        new.id_nature_prestation,
        new.auteur,
        new.etat,
        new.id_statut_redaction_clausier,
        new.compatible_entite_adjudicatrice,
        new.id_organisme,
        new.canevas_editeur,
        new.date_premiere_validation,
        new.date_derniere_validation,
        new.id_ref_ccag,
        new.actif 
    )
    FROM redaction.epm__t_publication_clausier publication
    WHERE new.id_publication = publication.id AND publication.actif = 1;
*/
CREATE OR REPLACE TRIGGER redaction.CANEVAS_PUB_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_canevas_pub
FOR EACH ROW
    UPDATE redaction.epm__v_canevas
    SET
        id_canevas = NULL,
        id_canevas_publication = new.id_canevas,
        id_document_type = new.id_document_type,
        titre = new.titre,
        reference = new.reference,
        date_creation = new.date_creation,
        date_modification = new.date_modification,
        id_nature_prestation = new.id_nature_prestation,
        auteur = new.auteur,
        etat = new.etat,
        id_statut_redaction_clausier = new.id_statut_redaction_clausier,
        compatible_entite_adjudicatrice = new.compatible_entite_adjudicatrice,
        id_organisme = new.id_organisme,
        canevas_editeur = new.canevas_editeur,
        date_premiere_validation = new.date_premiere_validation,
        date_derniere_validation = new.date_derniere_validation,
        id_ref_ccag = new.id_ref_ccag,
        actif = new.actif 
    WHERE id_canevas = new.id;

CREATE OR REPLACE TRIGGER redaction.CANEVAS_PUB_DELETE_TRIGGER BEFORE DELETE ON redaction.epm__t_canevas_pub
FOR EACH ROW 
    DELETE FROM redaction.epm__v_canevas WHERE id_canevas_publication = old.id_canevas;

CREATE OR REPLACE TRIGGER redaction.CANEVAS_PUBLICATION_CLAUSIER_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_publication_clausier
FOR EACH ROW 
    INSERT INTO redaction.epm__v_canevas (
        id,
        id_canevas,
        id_canevas_publication,
        id_publication,
        id_document_type,
        titre,
        reference,
        date_creation,
        date_modification,
        id_nature_prestation,
        auteur,
        etat,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        canevas_editeur,
        date_premiere_validation,
        date_derniere_validation,
        id_ref_ccag,
        actif 
    )
    SELECT
        NULL,
        NULL,
        canevas.id_canevas,
        new.id,
        canevas.id_document_type,
        canevas.titre,
        canevas.reference,
        canevas.date_creation,
        canevas.date_modification,
        canevas.id_nature_prestation,
        canevas.auteur,
        canevas.etat,
        canevas.id_statut_redaction_clausier,
        canevas.compatible_entite_adjudicatrice,
        canevas.id_organisme,
        canevas.canevas_editeur,
        canevas.date_premiere_validation,
        canevas.date_derniere_validation,
        canevas.id_ref_ccag,
        canevas.actif 
    FROM redaction.epm__t_canevas_pub canevas
    WHERE new.id = canevas.id_publication and new.actif = 1;

CREATE OR REPLACE TRIGGER redaction.CANEVAS_PUBLICATION_CLAUSIER_DELETE_TRIGGER BEFORE UPDATE ON redaction.epm__t_publication_clausier
FOR EACH ROW
    DELETE FROM redaction.epm__v_canevas WHERE id_canevas_publication IS NOT NULL and id_canevas_publication <> old.id;
