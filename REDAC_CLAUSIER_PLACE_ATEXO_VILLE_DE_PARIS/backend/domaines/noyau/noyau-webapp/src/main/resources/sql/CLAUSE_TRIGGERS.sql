DROP TRIGGER IF EXISTS redaction.CLAUSE_INSERT_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CLAUSE_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CLAUSE_DELETE_TRIGGER;
/*DROP TRIGGER IF EXISTS redaction.CLAUSE_PUB_INSERT_TRIGGER;*/
DROP TRIGGER IF EXISTS redaction.CLAUSE_PUB_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CLAUSE_PUB_DELETE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CLAUSE_PUBLICATION_CLAUSIER_UPDATE_TRIGGER;
DROP TRIGGER IF EXISTS redaction.CLAUSE_PUBLICATION_CLAUSIER_DELETE_TRIGGER;

CREATE OR REPLACE TRIGGER redaction.CLAUSE_INSERT_TRIGGER AFTER INSERT ON redaction.epm__t_clause
FOR EACH ROW
    INSERT INTO redaction.epm__v_clause (
        id,
        id_clause,
        id_ref_type_document,
        info_bulle_text,
        info_bulle_url,
        id_ref_type_clause,
        id_theme,
        reference,
        mots_cles,
        parametrable_direction,
        parametrable_agent,
        texte_fixe_avant,
        texte_fixe_apres,
        date_modification,
        date_creation,
        auteur,
        actif,
        id_nature_prestation,
        id_procedure,
        etat,
        formulation_modifiable,
        saut_ligne_texte_avant,
        saut_ligne_texte_apres,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        clause_editeur,
        date_premiere_validation,
        date_derniere_validation
    )
    SELECT
        NULL,
        new.id,
        new.id_ref_type_document,
        new.info_bulle_text,
        new.info_bulle_url,
        new.id_ref_type_clause,
        new.id_theme,
        new.reference,
        new.mots_cles,
        new.parametrable_direction,
        new.parametrable_agent,
        new.texte_fixe_avant,
        new.texte_fixe_apres,
        new.date_modification,
        new.date_creation,
        new.auteur,
        new.actif,
        new.id_nature_prestation,
        new.id_procedure,
        new.etat,
        new.formulation_modifiable,
        new.saut_ligne_texte_avant,
        new.saut_ligne_texte_apres,
        new.id_statut_redaction_clausier,
        new.compatible_entite_adjudicatrice,
        new.id_organisme,
        new.clause_editeur,
        new.date_premiere_validation,
        new.date_derniere_validation
    FROM redaction.epm__t_clause clause
    WHERE new.id = clause.id and new.clause_editeur = 0;


CREATE OR REPLACE TRIGGER redaction.CLAUSE_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_clause
FOR EACH ROW
    UPDATE redaction.epm__v_clause
    SET
        id_ref_type_document = new.id_ref_type_document,
        info_bulle_text = new.info_bulle_text,
        info_bulle_url = new.info_bulle_url,
        id_ref_type_clause = new.id_ref_type_clause,
        id_theme = new.id_theme,
        reference = new.reference,
        mots_cles = new.mots_cles,
        parametrable_direction = new.parametrable_direction,
        parametrable_agent = new.parametrable_agent,
        texte_fixe_avant = new.texte_fixe_avant,
        texte_fixe_apres = new.texte_fixe_apres,
        date_modification = new.date_modification,
        date_creation = new.date_creation,
        auteur = new.auteur,
        actif = new.actif,
        id_nature_prestation = new.id_nature_prestation,
        id_procedure = new.id_procedure,
        etat = new.etat,
        formulation_modifiable = new.formulation_modifiable,
        saut_ligne_texte_avant = new.saut_ligne_texte_avant,
        saut_ligne_texte_apres = new.saut_ligne_texte_apres,
        id_statut_redaction_clausier = new.id_statut_redaction_clausier,
        compatible_entite_adjudicatrice = new.compatible_entite_adjudicatrice,
        id_organisme = new.id_organisme,
        clause_editeur = new.clause_editeur,
        date_premiere_validation = new.date_premiere_validation,
        date_derniere_validation = new.date_derniere_validation
    WHERE id_clause = new.id;

CREATE OR REPLACE TRIGGER redaction.CLAUSE_DELETE_TRIGGER BEFORE DELETE ON redaction.epm__t_clause
FOR EACH ROW
    DELETE FROM redaction.epm__v_clause WHERE id_clause = old.id;

/*
CREATE OR REPLACE TRIGGER redaction.CLAUSE_PUB_INSERT_TRIGGER AFTER INSERT ON redaction.epm__t_clause_pub
FOR EACH ROW
    INSERT INTO redaction.epm__v_clause (
        id,
        id_clause,
        id_clause_publication,
        id_ref_type_document,
        info_bulle_text,
        info_bulle_url,
        id_ref_type_clause,
        id_theme,
        reference,
        mots_cles,
        parametrable_direction,
        parametrable_agent,
        texte_fixe_avant,
        texte_fixe_apres,
        date_modification,
        date_creation,
        auteur,
        actif,
        id_nature_prestation,
        id_procedure,
        etat,
        formulation_modifiable,
        saut_ligne_texte_avant,
        saut_ligne_texte_apres,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        clause_editeur,
        date_premiere_validation,
        date_derniere_validation
    )
    VALUES
        (
            NULL,
            NULL,
            new.id_clause,
            new.id_ref_type_document,
            new.info_bulle_text,
            new.info_bulle_url,
            new.id_ref_type_clause,
            new.id_theme,
            new.reference,
            new.mots_cles,
            new.parametrable_direction,
            new.parametrable_agent,
            new.texte_fixe_avant,
            new.texte_fixe_apres,
            new.date_modification,
            new.date_creation,
            new.auteur,
            new.actif,
            new.id_nature_prestation,
            new.id_procedure,
            new.etat,
            new.formulation_modifiable,
            new.saut_ligne_texte_avant,
            new.saut_ligne_texte_apres,
            new.id_statut_redaction_clausier,
            new.compatible_entite_adjudicatrice,
            new.id_organisme,
            new.clause_editeur,
            new.date_premiere_validation,
            new.date_derniere_validation
        );
*/
CREATE OR REPLACE TRIGGER redaction.CLAUSE_PUB_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_clause_pub
FOR EACH ROW
    UPDATE redaction.epm__v_clause
    SET
        id_publication = new.id_publication,
        id_ref_type_document = new.id_ref_type_document,
        info_bulle_text = new.info_bulle_text,
        info_bulle_url = new.info_bulle_url,
        id_ref_type_clause = new.id_ref_type_clause,
        id_theme = new.id_theme,
        reference = new.reference,
        mots_cles = new.mots_cles,
        parametrable_direction = new.parametrable_direction,
        parametrable_agent = new.parametrable_agent,
        texte_fixe_avant = new.texte_fixe_avant,
        texte_fixe_apres = new.texte_fixe_apres,
        date_modification = new.date_modification,
        date_creation = new.date_creation,
        auteur = new.auteur,
        actif = new.actif,
        id_nature_prestation = new.id_nature_prestation,
        id_procedure = new.id_procedure,
        etat = new.etat,
        formulation_modifiable = new.formulation_modifiable,
        saut_ligne_texte_avant = new.saut_ligne_texte_avant,
        saut_ligne_texte_apres = new.saut_ligne_texte_apres,
        id_statut_redaction_clausier = new.id_statut_redaction_clausier,
        compatible_entite_adjudicatrice = new.compatible_entite_adjudicatrice,
        id_organisme = new.id_organisme,
        clause_editeur = new.clause_editeur,
        date_premiere_validation = new.date_premiere_validation,
        date_derniere_validation = new.date_derniere_validation
    WHERE id_clause_publication = new.id_clause;

CREATE OR REPLACE TRIGGER redaction.CLAUSE_PUB_DELETE_TRIGGER BEFORE DELETE ON redaction.epm__t_clause_pub
FOR EACH ROW
    DELETE FROM redaction.epm__v_clause WHERE id_clause_publication = old.id_clause;

CREATE OR REPLACE TRIGGER redaction.CLAUSE_PUBLICATION_CLAUSIER_UPDATE_TRIGGER AFTER UPDATE ON redaction.epm__t_publication_clausier
FOR EACH ROW 
    INSERT INTO redaction.epm__v_clause (
        id,
        id_clause,
        id_clause_publication,
        id_publication,
        id_ref_type_document,
        info_bulle_text,
        info_bulle_url,
        id_ref_type_clause,
        id_theme,
        reference,
        mots_cles,
        parametrable_direction,
        parametrable_agent,
        texte_fixe_avant,
        texte_fixe_apres,
        date_modification,
        date_creation,
        auteur,
        actif,
        id_nature_prestation,
        id_procedure,
        etat,
        formulation_modifiable,
        saut_ligne_texte_avant,
        saut_ligne_texte_apres,
        id_statut_redaction_clausier,
        compatible_entite_adjudicatrice,
        id_organisme,
        clause_editeur,
        date_premiere_validation,
        date_derniere_validation
    )
    SELECT
        NULL,
        NULL,
        clause.id_clause,
        new.id,
        clause.id_ref_type_document,
        clause.info_bulle_text,
        clause.info_bulle_url,
        clause.id_ref_type_clause,
        clause.id_theme,
        clause.reference,
        clause.mots_cles,
        clause.parametrable_direction,
        clause.parametrable_agent,
        clause.texte_fixe_avant,
        clause.texte_fixe_apres,
        clause.date_modification,
        clause.date_creation,
        clause.auteur,
        clause.actif,
        clause.id_nature_prestation,
        clause.id_procedure,
        clause.etat,
        clause.formulation_modifiable,
        clause.saut_ligne_texte_avant,
        clause.saut_ligne_texte_apres,
        clause.id_statut_redaction_clausier,
        clause.compatible_entite_adjudicatrice,
        clause.id_organisme,
        clause.clause_editeur,
        clause.date_premiere_validation,
        clause.date_derniere_validation
    FROM redaction.epm__t_clause_pub clause
    WHERE new.id = clause.id_publication and new.actif = 1;

CREATE OR REPLACE TRIGGER redaction.CLAUSE_PUBLICATION_CLAUSIER_DELETE_TRIGGER BEFORE UPDATE ON redaction.epm__t_publication_clausier
FOR EACH ROW
    DELETE FROM redaction.epm__v_clause WHERE id_publication IS NOT NULL and id_publication <> old.id;
