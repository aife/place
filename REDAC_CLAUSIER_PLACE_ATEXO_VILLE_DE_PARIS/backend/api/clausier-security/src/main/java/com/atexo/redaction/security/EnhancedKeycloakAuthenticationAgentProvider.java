package com.atexo.redaction.security;

import com.atexo.redaction.config.ConstantesGlobales;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import com.atexo.redaction.dao.agent.repository.SessionAgentRepository;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

@ConditionalOnProperty(value = "keycloak.enabled", matchIfMissing = true)
@Configuration
@Slf4j
public class EnhancedKeycloakAuthenticationAgentProvider extends KeycloakAuthenticationProvider {
    public static final String ID_CONTEXTE = "ID_CONTEXTE";


    private final SessionAgentRepository sessionAgentRepository;
    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;

    public EnhancedKeycloakAuthenticationAgentProvider(SessionAgentRepository sessionAgentRepository, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        this.sessionAgentRepository = sessionAgentRepository;
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
    }


    @Override
    @Transactional
    public Authentication authenticate(final Authentication authentication) {
        var authenticationToken = (KeycloakAuthenticationToken) authentication;
        Account account = new Account(authenticationToken.getAccount(), authenticationToken.isInteractive(), authenticationToken.getAuthorities());

        String tokenString = authenticationToken.getAccount().getKeycloakSecurityContext().getTokenString();
        SessionAgentEntity sessionAgent = sessionAgentRepository.findByToken(tokenString);
        if (sessionAgent != null) {
            AgentEntity agent = sessionAgent.getAgent();
            account.setIdAccount(agent.getId());
            MDC.put("org", agent.getOrganisme().getCodeExterne());
            MDC.put("utilisateur", agent.getPrenom() + " " + agent.getNom());

            HttpSession session = getSession(httpServletRequest);
            if (session != null) {
                session.setAttribute(ConstantesGlobales.AGENT, agent);
                session.setAttribute(ConstantesGlobales.SESSION_VALIDE, sessionAgent);
            }
        }
        return account;
    }

    @Override
    public boolean supports(final Class<?> aClass) {
        return KeycloakAuthenticationToken.class.isAssignableFrom(aClass);
    }

    private HttpSession getSession(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession(false);
        if (session == null) {
            log.trace("Session now must be created. This may be suspect");
            session = httpServletRequest.getSession(true);
        } else {
            log.trace("Session already exist. All is ok!");
        }
        return session;
    }

    private String normalize(String path) {
        if (null != path && path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        if (null == path) {
            path = "/";
        }
        return path;
    }
}
