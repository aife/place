package com.atexo.redaction.security;

import org.keycloak.adapters.spi.KeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class Account extends KeycloakAuthenticationToken {
    private static final long serialVersionUID = 4942399809603746232L;

    private String idAccount;

    public Account(final KeycloakAccount account, final boolean interactive) {
        super(account, interactive);
    }

    public Account(final KeycloakAccount account, final boolean interactive, final Collection<? extends GrantedAuthority> authorities) {
        super(account, interactive, authorities);
    }

    public String getIdentifiant() {
        return getAccount().getPrincipal().getName();
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

}
