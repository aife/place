package com.atexo.redaction.config;

import org.testcontainers.containers.MariaDBContainer;

public class ITCustomMariaDBContainer extends MariaDBContainer<ITCustomMariaDBContainer> {
    private static ITCustomMariaDBContainer ITCustomMariaDBContainer;

    private ITCustomMariaDBContainer() {
        super();
    }


    public static ITCustomMariaDBContainer getInstanceRedaction() {
        if (ITCustomMariaDBContainer != null) {
            ITCustomMariaDBContainer.stop();
        }
        ITCustomMariaDBContainer = new ITCustomMariaDBContainer()
                .withPassword("password")
                .withUsername("user")
                .withDatabaseName("redaction");

        return ITCustomMariaDBContainer;
    }

    @Override
    public void start() {
        super.start();

    }

    @Override
    public void stop() {

    }
}
