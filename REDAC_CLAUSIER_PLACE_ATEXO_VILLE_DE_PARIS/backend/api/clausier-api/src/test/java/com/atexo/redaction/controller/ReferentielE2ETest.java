package com.atexo.redaction.controller;

import com.atexo.redaction.config.E2EAbstractResource;
import com.atexo.redaction.config.MariaDbExtension;
import com.atexo.redaction.config.WsMessage;
import com.atexo.redaction.ws.mpe.model.Referentiel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ReferentielE2ETest extends E2EAbstractResource {

    @RegisterExtension
    static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension();

    private final static String PATH = "/clausier-api/referentiels/";

    /* *******************************************************
     *  vérification de la protection des services
     * *******************************************************
     */

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        setAgentContexte();
    }


    @ParameterizedTest
    @ValueSource(strings = {"ref-directions-services", "ref-procedures", "ref-types-contrats"})
    void mustReturnUnauthorizedWhenGetRefDirectionService(String api) {

        ResponseEntity<WsMessage> result = testRestTemplate.getForEntity(BASE_URL + port + PATH + api, WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }


    @DisplayName("GET référentiels MPE")
    @ParameterizedTest
    @ValueSource(strings = {"ref-directions-services", "ref-procedures", "ref-types-contrats"})
    void Given_idContexte_When_getDirectionService_Then_return_liste_Referentiel_DIRECTION_SERVICE(String api) {
        // given
        String requestUrl = BASE_URL + port + PATH + api;
        HttpHeaders headers = getHeadersWithAuthenticationAndValidIdContexte();

        // when
        ResponseEntity<Referentiel[]> responseEntity = testRestTemplate.exchange(
                requestUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Referentiel[].class);

        // then
        Referentiel[] referentiels = responseEntity.getBody();
        assertThat(referentiels).isNotEmpty();
    }


}
