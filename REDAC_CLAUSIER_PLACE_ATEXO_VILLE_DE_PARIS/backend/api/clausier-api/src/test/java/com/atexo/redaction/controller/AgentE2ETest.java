package com.atexo.redaction.controller;

import com.atexo.redaction.config.E2EAbstractResource;
import com.atexo.redaction.config.MariaDbExtension;
import com.atexo.redaction.config.WsMessage;
import com.atexo.redaction.domain.agent.exception.AgentExceptionEnum;
import com.atexo.redaction.domain.agent.model.Agent;
import com.atexo.redaction.domain.agent.model.AgentType;
import com.atexo.redaction.domain.agent.model.Mpe;
import com.atexo.redaction.domain.agent.model.ServeurApiType;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.http.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AgentE2ETest extends E2EAbstractResource {

    @RegisterExtension
    static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension();

    private final static String PATH = "/clausier-api/agents/";

    @Test
    @Order(1)
    void Given_NoToken_When_SaveAgent_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.postForEntity(BASE_URL + port + PATH, Agent.builder()
                .build(), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(2)
    void Given_EmptyAgent_When_SaveAgent_Then_ReturnMandatoryFields() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(WsMessage.builder().build(), headers), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
    }

    @Test
    @Order(3)
    void Given_InvalidEnvoie_When_SaveAgent_Then_ReturnRequiredException() {
        final Mpe contexteAgent = new Mpe();
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        WsMessage wsMessage = result.getBody();
        assertNotNull(wsMessage);
        assertThat(wsMessage.getCode()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        assertThat(wsMessage.getMessage()).isEqualTo("Envoie ne peut pas être null");
        assertThat(wsMessage.getType()).isEqualTo(AgentExceptionEnum.MANDATORY.getType());
    }

    @Test
    @Order(4)
    void Given_InvalidAgent_When_SaveAgent_Then_ReturnRequiredException() {
        final Mpe contexteAgent = new Mpe();
        contexteAgent.setEnvoi(new Mpe.Envoi());
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        WsMessage wsMessage = result.getBody();
        assertNotNull(wsMessage);
        assertThat(wsMessage.getCode()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());

        assertThat(wsMessage.getMessage()).isEqualTo("Agent ne peut pas être null");
        assertThat(wsMessage.getType()).isEqualTo(AgentExceptionEnum.MANDATORY.getType());
    }

    @Test
    @Order(5)
    void Given_InvalidAgentContexte_When_SaveAgent_Then_ReturnRequiredException() {
        final Mpe contexteAgent = new Mpe();
        contexteAgent.setEnvoi(new Mpe.Envoi());
        contexteAgent.getEnvoi().setAgent(new AgentType());
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        WsMessage wsMessage = result.getBody();
        assertNotNull(wsMessage);
        assertThat(wsMessage.getCode()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());

        assertThat(wsMessage.getMessage()).isEqualTo("L'acheteur public est obligatoire, L'identifiant est obligatoire, L'organisme est obligatoire, La plateforme est obligatoire, Le nom est obligatoire, Le prénom est obligatoire, l'objet api ne peut pas être null");
        assertThat(wsMessage.getType()).isEqualTo(AgentExceptionEnum.MANDATORY.getType());
    }

    @Test
    @Order(6)
    void Given_InvalidAgentContexteWithEmptyApi_When_SaveAgent_Then_ReturnRequiredOrganismeException() {
        final Mpe contexteAgent = getMpe("mpe");
        contexteAgent.getEnvoi().getAgent().setApi(new ServeurApiType());
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        WsMessage wsMessage = result.getBody();
        assertNotNull(wsMessage);
        assertThat(wsMessage.getCode()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());

        assertThat(wsMessage.getMessage()).isEqualTo("L'url du serveur est obligatoire");
        assertThat(wsMessage.getType()).isEqualTo(AgentExceptionEnum.MANDATORY.getType());
    }

    @Test
    @Order(7)
    void Given_InvalidAgentContexteWithMalformedApiUrl_When_SaveAgent_Then_ReturnRequiredOrganismeException() {
        final Mpe contexteAgent = getMpe("mpe");
        contexteAgent.getEnvoi().getAgent().setApi(new ServeurApiType());
        contexteAgent.getEnvoi().getAgent().getApi().setUrl("wrongUrl");
        contexteAgent.getEnvoi().getAgent().getApi().setToken("login");
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());
        WsMessage wsMessage = result.getBody();
        assertNotNull(wsMessage);
        assertThat(wsMessage.getCode()).isEqualTo(AgentExceptionEnum.MANDATORY.getCode());

        assertThat(wsMessage.getMessage()).isEqualTo("L'url du serveur doit être sous le format http:// ou https://");
        assertThat(wsMessage.getType()).isEqualTo(AgentExceptionEnum.MANDATORY.getType());
    }

    @Test
    @Order(8)
    void Given_AgentContexte_When_SaveAgent_Then_ReturnSaved() {
        final Mpe contexteAgent = getMpe("mpe");
        //When
        ResponseEntity<String> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(contexteAgent, getBearerHeader()), String.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        idAgent = result.getBody();
        assertNotNull(idAgent);
    }

    @Test
    @Order(8)
    void Given_AgentContexte_When_SaveAgentXml_Then_ReturnSaved() throws JAXBException {
        final Mpe contexteAgent = getMpe("mpe");
        //When
        HttpHeaders headers = getBearerHeader();
        headers.setContentType(MediaType.APPLICATION_XML);
        JAXBContext jaxbContextTed = JAXBContext.newInstance(Mpe.class);
        Marshaller jaxbMarshaller = jaxbContextTed.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(contexteAgent, sw);
        ResponseEntity<String> result = testRestTemplate.exchange(BASE_URL + port + PATH, HttpMethod.POST,
                new HttpEntity<>(sw.toString(), headers), String.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.OK.value());
        idAgent = result.getBody();
        assertNotNull(idAgent);
    }

    @Test
    @Order(9)
    void Given_NoToken_When_GetAgentById_Then_ReturnUnauthorized() {
        ResponseEntity<WsMessage> result = testRestTemplate.getForEntity(BASE_URL + port + PATH + "whoami", WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }


    @Test
    @Order(10)
    void Given_ValidId_When_GetAgentById_Then_ReturnAgent() {
        final Mpe contexteAgent = getMpe("mpe");
        //When
        ResponseEntity<Agent> result = testRestTemplate.exchange(BASE_URL + port + PATH + "whoami", HttpMethod.GET,
                new HttpEntity<>(getBearerHeader()), Agent.class);
        assertNotNull(result);
        Agent agent = result.getBody();
        assertNotNull(agent);
        assertEquals(agent.getIdentifiant(), contexteAgent.getEnvoi().getAgent().getIdentifiant());
        assertEquals(agent.getAcheteurPublic(), contexteAgent.getEnvoi().getAgent().getNomCourantAcheteurPublic());
    }


}
