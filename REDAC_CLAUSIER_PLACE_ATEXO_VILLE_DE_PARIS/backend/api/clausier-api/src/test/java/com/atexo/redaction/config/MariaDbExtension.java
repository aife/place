package com.atexo.redaction.config;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class MariaDbExtension implements BeforeAllCallback, AfterAllCallback {
    public static ITCustomMariaDBContainer MARIA_DB_CONTAINER_REDACTION;

    public MariaDbExtension() {
        MARIA_DB_CONTAINER_REDACTION = ITCustomMariaDBContainer.getInstanceRedaction();

    }

    @Override
    public void beforeAll(ExtensionContext context) {
        MARIA_DB_CONTAINER_REDACTION.start();
        System.setProperty("DB_URL", MARIA_DB_CONTAINER_REDACTION.getJdbcUrl());
        System.setProperty("DB_USERNAME", "root");
        System.setProperty("DB_PASSWORD", MARIA_DB_CONTAINER_REDACTION.getPassword());
    }

    @Override
    public void afterAll(ExtensionContext context) {
        MARIA_DB_CONTAINER_REDACTION.stop();
    }
}
