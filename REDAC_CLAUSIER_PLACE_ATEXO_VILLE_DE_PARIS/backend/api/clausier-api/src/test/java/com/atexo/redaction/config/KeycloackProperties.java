package com.atexo.redaction.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "clausier.auth")
@Getter
@Setter
public class KeycloackProperties {
	private String url;
	private String clientId;
	private String username;
	private String password;
	private String grantType;
}
