package com.atexo.redaction.controller;

import com.atexo.redaction.config.E2EAbstractResource;
import com.atexo.redaction.config.MariaDbExtension;
import com.atexo.redaction.config.WsMessage;
import com.atexo.redaction.domain.document.model.DocumentAdministrable;
import com.atexo.redaction.domain.document.model.EditionRequest;
import com.atexo.redaction.domain.document.model.Produit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DocumentsAdministrablesE2ETest extends E2EAbstractResource {

    @RegisterExtension
    static final MariaDbExtension MARIA_DB_EXTENSION = new MariaDbExtension();

    private final static String PATH = "/clausier-api/documents-administrables/";
    private static DocumentAdministrable savedDocument = null;


    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        setAgentContexte();
    }

    @Test
    @Order(1)
    @DisplayName("1- Vérifier que le ws de sauvegarde de document administrable est sécurisé")
    void Given_NoToken_When_SaveDocumentAdministrable_Then_ReturnUnauthorized() {
        DocumentAdministrable doc = DocumentAdministrable.builder()
                .libelle("libelle1")
                .code("code1")
                .produit("Mpe")
                .champFusion(false)
                .plateformeUuid("testUuid")
                .build();
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        Path pathToFile = Paths.get("src/test/resources/testStyle.docx");
        bodyMap.add("fichier",
                new FileSystemResource(pathToFile.toAbsolutePath().toFile()));
        bodyMap.add("documentAdministrable", doc);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        //When
        final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, httpHeaders);
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH,
                HttpMethod.POST,
                requestEntity, DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(2)
    @DisplayName("2- Vérifier que le ws d'ajout de document administrable est NOK sans fichier joint")
    void Given_NoFile_When_SaveDocumentAdministrable_Then_ThrowError() {
        DocumentAdministrable doc = DocumentAdministrable.builder()
                .libelle("libelle1")
                .code("code1")
                .produit("Mpe")
                .champFusion(false)
                .plateformeUuid("testUuid")
                .build();
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("documentAdministrable", doc);
        HttpHeaders httpHeaders = getBearerHeader();
        httpHeaders.add("idContexte", idAgent);
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        //When
        final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, httpHeaders);
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH,
                HttpMethod.POST,
                requestEntity, WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);

    }

    @Test
    @Order(3)
    @DisplayName("3- Vérifier que le ws d'ajout de document administrable est NOK sans données du document")
    void Given_NoDoc_When_SaveDocumentAdministrable_Then_ThrowError() {
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        Path pathToFile = Paths.get("src/test/resources/testStyle.docx");
        bodyMap.add("fichier",
                new FileSystemResource(pathToFile.toAbsolutePath().toFile()));
        HttpHeaders httpHeaders = getBearerHeader();
        httpHeaders.add("idContexte", idAgent);
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        //When
        final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, httpHeaders);
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH,
                HttpMethod.POST,
                requestEntity, DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }


    @Test
    @Order(4)
    @DisplayName("4- Vérifier que le ws d'ajout de document administrable est OK")
    void Given_FileAndDoc_When_SaveDocumentAdministrable_Then_ReturnDocumentAdministrable() {
        DocumentAdministrable doc = DocumentAdministrable.builder()
                .libelle("libelle1")
                .code("code1")
                .produit("Mpe")
                .champFusion(false)
                .plateformeUuid("testUuid")
                .build();
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        Path pathToFile = Paths.get("src/test/resources/testStyle.docx");
        bodyMap.add("fichier",
                new FileSystemResource(pathToFile.toAbsolutePath().toFile()));
        bodyMap.add("documentAdministrable", doc);
        HttpHeaders httpHeaders = getBearerHeader();
        httpHeaders.add("idContexte", idAgent);
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        //When
        final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, httpHeaders);
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH,
                HttpMethod.POST,
                requestEntity, DocumentAdministrable.class);
        //Then
        assertNotNull(result);
        assertNotNull(result.getBody());
        savedDocument = result.getBody();
        assertThat(savedDocument.getPlateformeUuid()).isEqualTo("testUuid");
    }

    @Test
    @Order(5)
    @DisplayName("5- Vérifier que le ws de récupération de documents administrables est sécurisé")
    void Given_NoToken_When_ListDocumentsAdministrables_Then_ReturnUnauthorized() {
        ResponseEntity<DocumentAdministrable[]> result = testRestTemplate.exchange(BASE_URL + port + PATH,
                HttpMethod.GET,
                null,
                DocumentAdministrable[].class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(6)
    @DisplayName("6- Vérifier que le ws de récupération de documents administrables est NOK sans booleen champFusion")
    void Given_NoChampFusion_When_ListDocumentsAdministrables_Then_ThrowError() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH)
                .queryParam("plateformeUuid", "fakeUuid")
                .queryParam("produits", "Mpe");
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(7)
    @DisplayName("7- Vérifier que le ws de récupération de documents administrables est NOK sans plateformeUuid")
    void Given_NoPlateformeUuid_When_ListDocumentsAdministrables_Then_ThrowError() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH)
                .queryParam("champFusion", false)
                .queryParam("produits", "Mpe");
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(8)
    @DisplayName("8- Vérifier que le ws de récupération de documents administrables est NOK sans produits")
    void Given_NoProduits_When_ListDocumentsAdministrables_Then_ThrowError() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH)
                .queryParam("champFusion", false)
                .queryParam("plateformeUuid", "fakeUuid");
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.GET,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(9)
    @DisplayName("9- Vérifier que le ws de désactivation de documents administrables est sécurisé")
    void Given_NoToken_When_DisableDocument_Then_ReturnUnauthorized() {
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH + savedDocument.getId() + "/desactiver",
                HttpMethod.PATCH,
                null,
                DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(10)
    @DisplayName("10- Vérifier que le ws de désactivation de documents administrables est NOK si id document invalide")
    void Given_InvalidId_When_DisableDocument_Then_ThrowError() {
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(BASE_URL + port + PATH + 5000L + "/desactiver",
                HttpMethod.PATCH,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(404);
        assertThat(result.getBody().getMessage()).isEqualTo("Le document administrable n'existe pas");

    }

    @Test
    @Order(11)
    @DisplayName("11- Vérifier que le ws de désactivation de documents administrables est OK")
    void Given_TokenAndValidId_When_DisableDocument_Then_Disable() {
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH + savedDocument.getId() + "/desactiver",
                HttpMethod.PATCH,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                DocumentAdministrable.class);
        assertNotNull(result);
        assertNotNull(result.getBody());
        assertFalse(result.getBody().isActif());
    }

    @Test
    @Order(12)
    @DisplayName("12- Vérifier que le ws de récupération de l'url d'édition est sécurisé")
    void Given_NoToken_When_EditDocument_Then_ReturnUnauthorized() {
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH + savedDocument.getId() + "/desactiver",
                HttpMethod.PATCH,
                null,
                DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    @Order(13)
    @DisplayName("13- Vérifier que le ws de récupération de l'url d'édition est NOK sans plateformeUuid")
    void Given_NoPlateformeUuid_When_EditDocument_Then_ThrowError() {
        Produit produit = Produit.builder()
                .nom("Mpe")
                .url("fakeUrl")
                .build();
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(BASE_URL + port + PATH + savedDocument.getId() + "/document-edit",
                HttpMethod.POST,
                new HttpEntity<>(produit, getHeadersWithAuthenticationAndValidIdContexte()),
                DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(14)
    @DisplayName("14- Vérifier que le ws de récupération de l'url d'édition est NOK sans produit")
    void Given_NoProduit_When_EditDocument_Then_ThrowError() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH + savedDocument.getId() + "/document-edit")
                .queryParam("plateformeUuid", "fakeUuid");
        ResponseEntity<DocumentAdministrable> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(getHeadersWithAuthenticationAndValidIdContexte()),
                DocumentAdministrable.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    @Order(15)
    @DisplayName("15- Vérifier que le ws de récupération de l'url d'édition est NOK si id document invalide")
    void Given_InvalidId_When_EditDocument_Then_ThrowError() {
        Produit produit = Produit.builder()
                .nom("Mpe")
                .url("fakeUrl")
                .build();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH + 5000L + "/document-edit")
                .queryParam("plateformeUuid", "fakeUuid");
        ResponseEntity<WsMessage> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(produit, getHeadersWithAuthenticationAndValidIdContexte()),
                WsMessage.class);
        assertNotNull(result);
        assertThat(result.getStatusCodeValue()).isEqualTo(404);
        assertThat(result.getBody().getMessage()).isEqualTo("Le document administrable n'existe pas");
    }


    @Test
    @Order(16)
    @DisplayName("16- Vérifier que le ws de récupération de l'url d'édition est OK")
    void Given_TokenAndValidId_When_EditDocument_Then_ReturnUrl() {
        Produit produit = Produit.builder()
                .nom("Mpe")
                .url("fakeUrl")
                .build();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(BASE_URL + port + PATH + savedDocument.getId() + "/document-edit")
                .queryParam("plateformeUuid", "fakeUuid");
        ResponseEntity<EditionRequest> result = testRestTemplate.exchange(builder.toUriString(),
                HttpMethod.POST,
                new HttpEntity<>(produit, getHeadersWithAuthenticationAndValidIdContexte()),
                EditionRequest.class);
        assertNotNull(result);
        assertNotNull(result.getBody());
        assertNotNull(result.getBody().getUrl());
    }




}
