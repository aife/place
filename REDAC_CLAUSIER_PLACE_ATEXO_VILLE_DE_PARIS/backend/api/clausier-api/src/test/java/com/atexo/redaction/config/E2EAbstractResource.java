package com.atexo.redaction.config;

import com.atexo.redaction.config.bdd.DatabaseConfiguration;
import com.atexo.redaction.domain.agent.model.*;
import com.atexo.redaction.ws.mpe.model.AuthenitificationRequest;
import com.atexo.redaction.ws.mpe.model.MpeToken;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.JsonExpectationsHelper;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = {E2EScanConfig.class, DatabaseConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("e2e")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext
public abstract class E2EAbstractResource {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private KeycloackProperties keycloackProperties;

    @Autowired
    protected TestRestTemplate testRestTemplate;
    protected String token;
    protected String refreshToken;
    static protected String idAgent = "testID";
    protected Agent agent = null;


    @LocalServerPort
    protected int port;
    protected static final String JSON_PATH = "src/test/resources/json/";
    private static final String STUB_PATH = "src/test/resources/json/expected/";
    private static final String STUB_EXTENSION = ".json";
    protected String BASE_URL = "http://localhost:";
    protected final ObjectMapper objectMapper = new ObjectMapper();


    protected static final String SQL_PATH = "src/test/resources/sql/";
    protected final JsonExpectationsHelper jsonHelper = new JsonExpectationsHelper();
    private static final String SQL_EXTENSION = ".sql";


    @BeforeEach
    public void setUp() throws Exception {
        this.initToken();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

    }

    public void initToken() throws Exception {
        if (this.token != null) {
            return;
        }
        var restTemplate = new RestTemplate();
        var uri = new URI(keycloackProperties.getUrl());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", keycloackProperties.getClientId());
        map.add("username", keycloackProperties.getUsername());
        map.add("password", keycloackProperties.getPassword());
        map.add("grant_type", keycloackProperties.getGrantType());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<ITToken> result = restTemplate.postForEntity(uri, request, ITToken.class);
        final ITToken body = result.getBody();
        if (body == null) {
            throw new Exception();
        }
        this.token = body.getAccessToken();
        this.refreshToken = body.getRefreshToken();

    }

    protected HttpHeaders getBearerHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    protected HttpHeaders getHeadersWithAuthenticationAndValidIdContexte() {
        HttpHeaders headers = getBearerHeader();
        headers.set("idContexte", idAgent);
        return headers;
    }

    protected HttpHeaders getHeadersWithAuthenticationAndBadIdContexte() {
        HttpHeaders headers = getBearerHeader();
        headers.set("idContexte", "bad_id_context");
        return headers;
    }



    protected Mpe getMpe(String platform) {
        Mpe mpe = new Mpe();
        mpe.setEnvoi(new Mpe.Envoi());
        mpe.getEnvoi().setAgent(new AgentType());
        mpe.getEnvoi().getAgent().setOrganisme(new OrganismeType());
        mpe.getEnvoi().getAgent().getOrganisme().setId(1);
        mpe.getEnvoi().getAgent().getOrganisme().setAcronyme("pmi-min-1");
        mpe.getEnvoi().getAgent().getOrganisme().setDenominationOrganisme("nomCourantAcheteurPublic");
        mpe.getEnvoi().getAgent().setId("2");
        mpe.getEnvoi().getAgent().setIdentifiant("identifiant");
        mpe.getEnvoi().getAgent().setNom("nom");
        mpe.getEnvoi().getAgent().setPrenom("prenom");
        mpe.getEnvoi().getAgent().setPlateforme(platform);
        mpe.getEnvoi().getAgent().setAcronymeOrganisme("pmi-min-1");
        mpe.getEnvoi().getAgent().setSigleUrl("sigleUrl");
        mpe.getEnvoi().getAgent().setPhotoUrl("photoUrl");
        mpe.getEnvoi().getAgent().setNomCourantAcheteurPublic("nomCourantAcheteurPublic");
        ServiceType service = new ServiceType();
        service.setComplement("Complément");
        service.setLibelle("Service Test");
        service.setId(1);
        mpe.getEnvoi().getAgent().setService(service);
        final ServeurApiType serveurApi = new ServeurApiType();
        serveurApi.setUrl("https://mpe-release.local-trust.com");
        serveurApi.setToken(getAuthentificationV2(serveurApi.getUrl()));
        mpe.getEnvoi().getAgent().setApi(serveurApi);
        final AgentType.Habilitations habilitations = new AgentType.Habilitations();
        habilitations.getHabilitation().addAll(Arrays.asList("roles1", "roles2"));
        mpe.getEnvoi().getAgent().setHabilitations(habilitations);
        return mpe;
    }

    private String getAuthentificationV2(String urlServeurUpload) {
        String url = urlServeurUpload + "/api/v2/token";
        MpeToken ticket = testRestTemplate.postForObject(url,
                AuthenitificationRequest.builder().login("adminorme").password("@@ORME34").build(), MpeToken.class);
        return ticket.getToken();

    }

    protected void setAgentContexte() {
        if (agent == null) {
            ResponseEntity<String> result = testRestTemplate.postForEntity(BASE_URL + port + "/clausier-api/agents/",
                    new HttpEntity<>(getMpe("mpe"), getBearerHeader()), String.class);
            assertNotNull(result);
            idAgent = result.getBody();

            ResponseEntity<Agent> resultAgent = testRestTemplate.exchange(BASE_URL + port + "/clausier-api/agents/" + idAgent, HttpMethod.GET,
                    new HttpEntity<>(getBearerHeader()), Agent.class);
            agent = resultAgent.getBody();
        }
    }

    protected static String readStub(String stubId) throws IOException {
        Path pathSource = Paths.get(STUB_PATH + stubId + STUB_EXTENSION).toAbsolutePath();
        List<String> lines = Files.readAllLines(pathSource, Charset.defaultCharset());
        return lines.stream().reduce((result, concatWith) -> (result + concatWith.trim()).replace("\": ", "\":")).orElse(null);
    }

    protected <T> void assertJsonEqual(String expectedStubId, T actualObject) throws Exception {

        jsonHelper.assertJsonEqual(readStub(expectedStubId + "_expected"), objectMapper.writeValueAsString(actualObject), false);
    }


    protected <T> T getObject(String jsonId, Class<T> tClass) throws IOException {
        Path pathSource = Paths.get(JSON_PATH + jsonId + STUB_EXTENSION).toAbsolutePath();
        return objectMapper.readValue(pathSource.toFile(), tClass);
    }


    protected void initSql(String sqlId) throws Exception {
        Path pathSource = Paths.get(SQL_PATH + sqlId + SQL_EXTENSION).toAbsolutePath();
        try (FileReader reader = new FileReader(pathSource.toFile());
             Connection connection = dataSource.getConnection()) {
            ScriptRunner sr = new ScriptRunner(connection, true, true);

            sr.runScript(reader);

        }
    }
}
