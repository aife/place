package com.atexo.redaction.controllers;

import com.atexo.redaction.config.ConstantesGlobales;
import com.atexo.redaction.config.http_logging.LogEntryExit;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import com.atexo.redaction.domain.agent.model.Mpe;
import com.atexo.redaction.domain.agent.service.AgentService;
import com.atexo.redaction.ws.keycloak.model.Token;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/agents")
public class AgentsController {

    private final AgentService agentService;

    public AgentsController(AgentService agentService) {
        this.agentService = agentService;
    }


    @PostMapping
    @LogEntryExit(showArgs = true, showResult = true, unit = ChronoUnit.MILLIS)
    public String saveAgent(@RequestBody @Valid Mpe agent, @RequestHeader(value = "RefreshToken", required = false) String refreshToken,
                            @RequestHeader(value = "Authorization", required = false) String token) {
        String idContexte = agentService.saveAgentIfNotExist(agent, token.replace("Bearer ", ""));
        if (StringUtils.isNotBlank(refreshToken))
            return refreshToken + "/" + idContexte;
        return idContexte;
    }

    @GetMapping("/whoami")
    @Transactional
    public com.atexo.redaction.domain.agent.model.Agent getAgentById(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent,
                                                                     @SessionAttribute(ConstantesGlobales.SESSION_VALIDE) SessionAgentEntity sessionAgentEntity) {
        return agentService.getAgent(agent, sessionAgentEntity);
    }


}
