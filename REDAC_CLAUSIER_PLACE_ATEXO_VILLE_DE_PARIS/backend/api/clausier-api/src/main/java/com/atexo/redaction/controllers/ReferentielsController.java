package com.atexo.redaction.controllers;


import com.atexo.redaction.config.ConstantesGlobales;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.domain.referentiel.model.RefSurchargeLibelle;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;
import com.atexo.redaction.domain.referentiel.service.RedactionReferentielService;
import com.atexo.redaction.domain.referentiel.service.ReferentielMpeService;
import com.atexo.redaction.domain.referentiel.service.ReferentielService;
import com.atexo.redaction.ws.mpe.model.Referentiel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/referentiels")
public class ReferentielsController {

    private final ReferentielService referentielService;
    private final ReferentielMpeService referentielMpeService;
    private final RedactionReferentielService redactionReferentielService;

    public ReferentielsController(ReferentielService referentielService, ReferentielMpeService referentielMpeService, RedactionReferentielService redactionReferentielService) {
        this.referentielService = referentielService;
        this.referentielMpeService = referentielMpeService;
        this.redactionReferentielService = redactionReferentielService;
    }


    @GetMapping("type-auteurs-canevas")
    public List<ReferentielRepresentation> allTypeAuteursCanevas() {
        return redactionReferentielService.findListTypesAuteurCanevas();
    }

    @GetMapping("type-auteurs-clause")
    public List<ReferentielRepresentation> allTypeAuteursClauses() {
        return redactionReferentielService.findListTypesAuteurClauses();
    }

    @GetMapping("statuts-redaction-clausier")
    public List<ReferentielRepresentation> allStatut() {
        return redactionReferentielService.findListStatutsRedactionClausier();
    }

    @GetMapping("types-document")
    public List<ReferentielRepresentation> allTypesDocument() {
        return redactionReferentielService.findListTypesDocument();
    }

    @GetMapping("types-contrat")
    public List<ReferentielRepresentation> allTypesContrat() {
        return redactionReferentielService.findListTypesContrat();
    }

    @GetMapping("theme-clause")
    public List<ReferentielRepresentation> allThemesClause() {
        return redactionReferentielService.findListThemesClause();
    }

    @GetMapping("types-clause")
    public List<ReferentielRepresentation> allTypesClause() {
        return redactionReferentielService.findListTypesClause();
    }

    @GetMapping("valeur-heritee")
    public List<ReferentielRepresentation> allValeursHeritees() {
        return redactionReferentielService.findListReferenceHeritee();
    }

    @GetMapping("ccag")
    public List<ReferentielRepresentation> allCcag() {
        return referentielService.findListRefCCAG();
    }

    @GetMapping("natures")
    public List<ReferentielRepresentation> allNatures() {
        return referentielService.findListNatures();
    }

    @GetMapping("procedures")
    public List<ReferentielRepresentation> allProcedures(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        return referentielService.findListProcedures(agent.getOrganisme().getId());
    }

    @GetMapping("ref-directions-services")
    public List<Referentiel> getDirectionService(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        return referentielMpeService.getDirectionService(agent);
    }

    @GetMapping("ref-procedures")
    public List<Referentiel> getProcedure(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        return referentielMpeService.getProcedure(agent);
    }


    @GetMapping("ref-surcharge-libelle")
    public Map<String, String> getSurchargesLibelles() {
        return referentielService.getSurchargesLibelles();
    }
}
