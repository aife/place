package com.atexo.redaction.controllers;

import com.atexo.redaction.config.ConstantesGlobales;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/themes")
@Slf4j
public class ThemesController {

    private final RestTemplate atexoRestTemplate;

    public ThemesController(RestTemplate atexoRestTemplate) {
        this.atexoRestTemplate = atexoRestTemplate;
    }


    @GetMapping("/css/mpe-new-client.css")
    public String getMpeCss(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        try {

            return atexoRestTemplate.getForObject(agent.getMpeUrl() + "/themes/css/mpe-new-client.css", String.class);
        } catch (Exception e) {
            log.error("getMpeCss {}", e.getMessage());
            return null;
        }
    }


}
