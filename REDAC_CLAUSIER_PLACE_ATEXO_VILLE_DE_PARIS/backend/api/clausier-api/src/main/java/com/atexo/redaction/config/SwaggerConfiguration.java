package com.atexo.redaction.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

@Configuration
@OpenAPIDefinition(servers = {@Server(url = "/clausier-api", description = "Default Server URL")})
public class SwaggerConfiguration {


    @Bean
    public OpenAPI customOpenAPI(@Value("${clausier.name}") String name,
                                 @Value("${clausier.version}") String version) {
        final String securitySchemeName = "bearerAuth";
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .components(
                        new Components()
                                .addSecuritySchemes(securitySchemeName,
                                        new SecurityScheme()
                                                .name(securitySchemeName)
                                                .type(SecurityScheme.Type.HTTP)
                                                .scheme("bearer")
                                                .bearerFormat("JWT")
                                )
                )
                .info(new Info()
                        .title(name)
                        .version(version)
                        .description("Partie Clausier")
                        .termsOfService("http://www.atexo.com/")
                        .license(new License().name("Atexo " + LocalDate.now().getYear()).url("http://www.atexo.com/")));
    }
}
