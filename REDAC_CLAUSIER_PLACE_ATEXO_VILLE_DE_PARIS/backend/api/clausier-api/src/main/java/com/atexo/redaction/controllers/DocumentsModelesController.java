package com.atexo.redaction.controllers;

import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableException;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableExceptionEnum;
import com.atexo.redaction.domain.document.model.ReferentielDocumentAdministrable;
import com.atexo.redaction.domain.document.service.DocumentAdministrableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/documents-modeles")
@Slf4j
public class DocumentsModelesController {

    private final DocumentAdministrableService documentAdministrableService;

    public DocumentsModelesController(DocumentAdministrableService documentAdministrableService) {

        this.documentAdministrableService = documentAdministrableService;
    }

    /**
     * WS exposé pour la récupération des documents modèles depuis l'espace documentaire MPE
     */
    @GetMapping("/")
    public List<ReferentielDocumentAdministrable> listDocumentsAdministrables(
            @RequestParam(value = "produit") List<String> produit,
            @RequestParam(value = "plateforme") String plateforme,
            @RequestParam(value = "champsFusion", required = false) Boolean champsFusion,
            @RequestParam(value = "service", required = false) String service,
            @RequestParam(value = "organisme", required = false) String organisme) {
        log.info("/liste Documents administrables pour MPE");
        return documentAdministrableService.getListDocumentsAdministrablesMPE(produit, plateforme, organisme, service, Boolean.TRUE.equals(champsFusion));
    }

    /**
     * WS exposé pour le téléchargement des documents modèles depuis l'espace documentaire MPE
     */
    @GetMapping("/{sigle}/download")
    public ResponseEntity<ByteArrayResource> downloadTemplate(@PathVariable String sigle,
                                                              @RequestParam(value = "plateforme") String plateforme,
                                                              @RequestParam(value = "produit") String produit,
                                                              @RequestParam(value = "service", required = false) String service,
                                                              @RequestParam(value = "champsFusion", required = false) Boolean champsFusion,
                                                              @RequestParam(value = "organisme", required = false) String organisme) {
        log.info("/téléchargement d'un document administrable pour MPE");
        DocumentAdministrableEntity documentAdministrableEntity = documentAdministrableService.getDocumentByCodeAndPlateforme(sigle, produit, plateforme, organisme, service, champsFusion);
        if (documentAdministrableEntity.getTemplate() != null) {
            String name = documentAdministrableEntity.getFichier().getNom();
            ByteArrayResource inputStreamResource = documentAdministrableService.getFile(documentAdministrableEntity.getTemplate().getChemin());
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + name)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(inputStreamResource);
        } else {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_FOUND, "Le template du document est null.");
        }


    }


}
