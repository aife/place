package com.atexo.redaction.controllers;

import com.atexo.redaction.config.PageRepresentation;
import com.atexo.redaction.dao.redaction.specification.RechercheCanevasSpecification;
import com.atexo.redaction.domain.canevas.model.CanevasResumeRepresentation;
import com.atexo.redaction.domain.canevas.service.CanevasService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

@RestController
@RequestMapping("/canevas")
@Slf4j
public class CanevasController {

    private final CanevasService canevasService;

    public CanevasController(CanevasService canevasService) {
        this.canevasService = canevasService;
    }


    @GetMapping(params = {"page", "size"})
    public PageRepresentation<CanevasResumeRepresentation> listCanevas(final RechercheCanevasSpecification search, Pageable pageable, @SessionAttribute("organisme") int organisme) {
        log.info("/liste Canevas");
        search.setIdOrganisme(organisme);
        return canevasService.search(search, pageable);
    }


}
