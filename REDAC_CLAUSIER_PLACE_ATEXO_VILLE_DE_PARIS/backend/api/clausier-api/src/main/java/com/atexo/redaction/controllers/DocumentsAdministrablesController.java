package com.atexo.redaction.controllers;

import com.atexo.redaction.config.ConstantesGlobales;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.domain.document.model.DocumentAdministrable;
import com.atexo.redaction.domain.document.model.EditionRequest;
import com.atexo.redaction.domain.document.model.Produit;
import com.atexo.redaction.domain.document.model.TrackDocumentResponse;
import com.atexo.redaction.domain.document.service.DocumentAdministrableService;
import com.atexo.redaction.ws.docgen.model.FileStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/documents-administrables")
@Slf4j
public class DocumentsAdministrablesController {

    private final DocumentAdministrableService documentAdministrableService;

    public DocumentsAdministrablesController(DocumentAdministrableService documentAdministrableService) {

        this.documentAdministrableService = documentAdministrableService;
    }


    @GetMapping("/")
    public List<DocumentAdministrable> listDocumentsAdministrables(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent,
                                                                   @RequestParam(value = "champFusion") boolean champFusion,
                                                                   @RequestParam(value = "produits") List<String> produits) {

        return documentAdministrableService.getDocumentsAdministrables(produits, agent.getOrganisme().getPlateformeUuid(), agent.getOrganisme(), agent.getService(), champFusion);


    }

    @GetMapping("/{documentId}/download")
    public ResponseEntity<ByteArrayResource> downloadTemplate(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent, @PathVariable Long documentId, @RequestHeader(value = "Authorization") String token) {
        DocumentAdministrableEntity documentAdministrableEntity = documentAdministrableService.getDocumentById(documentId);
        String extension = FilenameUtils.getExtension(documentAdministrableEntity.getTemplate().getNom());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + documentId + "." + extension)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(documentAdministrableService.getTemplate(agent, documentAdministrableEntity, token));

    }


    @PostMapping("/")
    public DocumentAdministrable saveDocumentAdministrable(@SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent,
                                                           @RequestHeader(value = "Authorization") String token,
                                                           @RequestPart(required = false) MultipartFile fichier,
                                                           @RequestParam(required = false) Boolean surchargeOrganisme,
                                                           @RequestParam(required = false) Boolean surchargeService,
                                                           @RequestParam(required = false) String extension,
                                                           @RequestPart @NotNull DocumentAdministrable documentAdministrable) throws IOException {

        return documentAdministrableService.saveDocumentAdministrable(documentAdministrable, agent, fichier, surchargeOrganisme, surchargeService, token, extension);
    }


    @PostMapping("/{documentId}/document-edit")
    public EditionRequest editDocument(@PathVariable Long documentId,
                                       @SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent,
                                       @SessionAttribute(ConstantesGlobales.SESSION_VALIDE) SessionAgentEntity sessionAgentEntity,
                                       @RequestParam(required = false) Boolean surchargeOrganisme,
                                       @RequestParam(required = false) Boolean surchargeService,
                                       @RequestBody Produit produit) {
        log.info("/édition d'un document administrable");
        return documentAdministrableService.editDocument(sessionAgentEntity, documentId, agent, produit, surchargeOrganisme, surchargeService);

    }

    @PatchMapping("/{documentId}/desactiver")
    public Boolean disableDocument(@PathVariable Long documentId, @SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        log.info("/désactivation d'un document administrable");
        return documentAdministrableService.deleteDocument(documentId, agent);

    }

    @PatchMapping("/{documentId}/reinitialiser")
    public DocumentAdministrable reinitialiserDocument(@PathVariable Long documentId, @SessionAttribute(ConstantesGlobales.AGENT) AgentEntity agent) {
        log.info("reinitialiser d'un document administrable");
        return documentAdministrableService.reinitialiserDocument(documentId, agent);

    }

    @PostMapping("/{code}/editeur-en-ligne/callback")
    public TrackDocumentResponse trackDocument(@SessionAttribute(value = ConstantesGlobales.AGENT, required = false) AgentEntity agent,
                                               @PathVariable String code, @RequestParam String plateforme,
                                               @RequestParam boolean champFusion,
                                               @RequestParam String produit,
                                               @RequestParam(value = "service", required = false) String service,
                                               @RequestParam(value = "organisme", required = false) String organisme,
                                               @RequestBody FileStatus status) {
        return documentAdministrableService.getDocumentCallback(produit, code, plateforme, organisme, service, champFusion, status, agent);
    }


}
