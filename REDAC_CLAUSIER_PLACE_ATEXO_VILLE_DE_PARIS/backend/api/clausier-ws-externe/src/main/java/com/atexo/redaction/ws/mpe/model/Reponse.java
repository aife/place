

package com.atexo.redaction.ws.mpe.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Reponse {

    private String statutReponse;

    private List<Service> services;

    private MpeReferentiels referentiels;

}
