package com.atexo.redaction.ws.docgen.model;


import lombok.*;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InfoBulle {
    private String description;
    private Lien lien;
    private Boolean actif;

}
