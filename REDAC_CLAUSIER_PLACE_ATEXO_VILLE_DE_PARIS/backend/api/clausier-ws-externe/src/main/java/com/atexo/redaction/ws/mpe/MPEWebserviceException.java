package com.atexo.redaction.ws.mpe;

public class MPEWebserviceException extends RuntimeException {
    

    public MPEWebserviceException(String message) {
        super(message);
    }

    public MPEWebserviceException(String message, Throwable cause) {
        super(message, cause);
    }
}
