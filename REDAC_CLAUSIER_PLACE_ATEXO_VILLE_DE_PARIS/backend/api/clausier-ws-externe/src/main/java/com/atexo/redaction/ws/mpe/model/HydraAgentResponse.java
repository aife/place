package com.atexo.redaction.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class HydraAgentResponse {

    @JsonProperty("hydra:member")
    private List<AgentMpeResponse> list;
}
