package com.atexo.redaction.ws.docgen;

import com.atexo.redaction.ws.docgen.model.DocGenException;
import com.atexo.redaction.ws.docgen.model.DocumentEditorRequest;
import com.atexo.redaction.ws.docgen.model.FileStatus;
import com.atexo.redaction.ws.docgen.model.FileStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class DocgenWS {

    private final RestTemplate atexoRestTemplate;


    private final String docgenBaseUrl;


    private final String editionTemplateRequestWs;
    private final String getTemplateWs;


    private final String editionEnLigneBaseUrl;

    private final String documentStatusWs;

    private static final String MESSAGE = "Le fichier ne peut pas être null";

    public DocgenWS(RestTemplate atexoRestTemplate,
                    @Value("${external-apis.doc-gen.base-path}") String docgenBaseUrl,
                    @Value("${external-apis.doc-gen.ws.edition-template-request}") String editionTemplateRequestWs,
                    @Value("${external-apis.doc-gen.ws.get-template}") String getTemplateWs,
                    @Value("${external-apis.edition-en-ligne.base-path}") String editionEnLigneBaseUrl,
                    @Value("${external-apis.doc-gen.ws.document-status}") String documentStatusWs) {
        this.atexoRestTemplate = atexoRestTemplate;
        this.docgenBaseUrl = docgenBaseUrl;
        this.editionTemplateRequestWs = editionTemplateRequestWs;
        this.getTemplateWs = getTemplateWs;
        this.editionEnLigneBaseUrl = editionEnLigneBaseUrl;
        this.documentStatusWs = documentStatusWs;
    }

    public String getEditionToken(File document, DocumentEditorRequest editorRequest) throws IOException {
        if (editorRequest == null) {
            throw new DocGenException("Les parametres d'édition en ligne ne doivent pas être null");
        }

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        File tmp = File.createTempFile("edit-", "." + "docx");
        Files.deleteIfExists(tmp.toPath());
        if (document != null) {
            Files.copy(document.toPath(), tmp.toPath());
        }
        bodyMap.add("template", new FileSystemResource(tmp));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        bodyMap.add("editorRequest", new HttpEntity<>(editorRequest, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> body = new HttpEntity<>(bodyMap, headers);
        String result = atexoRestTemplate.postForObject(docgenBaseUrl + editionTemplateRequestWs, body, String.class);
        Files.deleteIfExists(tmp.toPath());
        return result;
    }

    public String getEditionUrl(String token) {
        return editionEnLigneBaseUrl + "?token=" + token;
    }

    public FileStatus getDocumentStatus(String token) {
        ResponseEntity<Resource> resource = this.getDocument(token);
        if (resource == null) {
            return null;
        }
        FileStatus status = new FileStatus();

        HttpHeaders headers = resource.getHeaders();
        if (headers.containsKey("document-status") && !CollectionUtils.isEmpty(headers.get("document-status"))) {
            status.setStatus(FileStatusEnum.valueOf(headers.get("document-status").get(0)));
        }

        if (headers.containsKey("document-version") && !CollectionUtils.isEmpty(headers.get("document-version"))) {
            status.setVersion(Integer.valueOf(headers.get("document-version").get(0)));
        }

        if (headers.containsKey("redac-status") && !CollectionUtils.isEmpty(headers.get("redac-status"))) {
            status.setRedacStatus(Integer.valueOf(headers.get("redac-status").get(0)));
        }
        return status;
    }

    public ResponseEntity<Resource> getDocument(String token) {
        if (token == null) {
            throw new DocGenException("Le token ne peut pas être null");
        }
        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put("token", token);
        return atexoRestTemplate.getForEntity(docgenBaseUrl + documentStatusWs, Resource.class, urlParameters);
    }

    public Resource getTemplate(File modele, String authorizationHeader) throws IOException {
        if (authorizationHeader == null) {
            throw new DocGenException("Le token ne peut pas être null");
        }
        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        File tmp = File.createTempFile("edit-", "." + "docx");
        Files.deleteIfExists(tmp.toPath());
        if (modele != null) {
            Files.copy(modele.toPath(), tmp.toPath());
        }
        bodyMap.add("modele", new FileSystemResource(tmp));
        final HttpHeaders theJsonHeader = new HttpHeaders();
        theJsonHeader.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> headersMap = new HashMap<>();
        headersMap.put("Authorization", authorizationHeader);
        bodyMap.add("headers", new HttpEntity<>(headersMap, theJsonHeader));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> body = new HttpEntity<>(bodyMap, headers);
        Resource result =  atexoRestTemplate.postForObject(docgenBaseUrl + getTemplateWs, body, Resource.class);
        Files.deleteIfExists(tmp.toPath());
        return result;
    }
}
