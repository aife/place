package com.atexo.redaction.ws.mpe.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatutConsultationEnum {
    CONSULTATION;

}
