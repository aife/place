package com.atexo.redaction.ws.mpe.model;


import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeContrats {
    private List<MpeReferentiel> contrat;

}
