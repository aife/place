package com.atexo.redaction.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class MpeConsultationV2 {

    @JsonProperty("@id")
    private String idExterne;
    private Integer id;
    private String directionService;
    private String organisme;
    private String naturePrestation;
    private String typeProcedure;
    private String typeContrat;
    private Double valeurEstimee;
    private String reference;
    private String intitule;
    private String objet;
    private String commentaireInterne;
    private List<Integer> lieuxExecution;
    private String codeCpvPrincipal;
    private String codeCpvSecondaire1;
    private String codeCpvSecondaire2;
    private String codeCpvSecondaire3;
    private boolean alloti = false;
}
