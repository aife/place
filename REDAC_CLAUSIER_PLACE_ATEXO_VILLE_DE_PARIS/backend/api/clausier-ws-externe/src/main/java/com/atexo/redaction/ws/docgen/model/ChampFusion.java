package com.atexo.redaction.ws.docgen.model;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChampFusion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String libelle;
    private String champsFusion;
    private String blocUrl;
    private String type;
    private InfoBulle infoBulle;
    private boolean simple;
    private boolean collection;

}
