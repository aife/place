package com.atexo.redaction.config;

import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

public interface WsMapper<R, M> {


    M mapToModel(R reponse);


    default List<M> mapToModels(Collection<R> reponses) {
        if (isEmpty(reponses)) {
            return emptyList();
        }
        return reponses.stream()
                .map(this::mapToModel)
                .collect(toList());
    }

}
