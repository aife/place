package com.atexo.redaction.ws.mpe.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeConsultation {

    private String organisme;
    private String reference;
    private String intitule;
    private String objet;
    private String typeProcedure;
    private MpeCpv cpv;
    private StatutConsultationEnum statut;
    private NatureEnum naturePrestation;

}
