package com.atexo.redaction.ws.docgen.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DonneeTest {
    private String libelle;
    private List<KeyValueRequest> requests;
}
