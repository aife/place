package com.atexo.redaction.ws.mpe.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Service {

    private String libelle;
    private String sigle;
    @JsonProperty("@id")
    private String idExterne;

}
