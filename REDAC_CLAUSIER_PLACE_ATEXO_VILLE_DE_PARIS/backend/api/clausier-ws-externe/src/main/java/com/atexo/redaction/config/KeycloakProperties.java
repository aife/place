package com.atexo.redaction.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeycloakProperties {
    private String basePath;
    private String identiteUrl;
    private String clientId;
    private String username;
    private String password;
    private String grantType;

}
