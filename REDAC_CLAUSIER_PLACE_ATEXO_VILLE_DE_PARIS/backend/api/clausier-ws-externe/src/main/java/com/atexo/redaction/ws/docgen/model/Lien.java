package com.atexo.redaction.ws.docgen.model;


import lombok.*;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Lien {
    private String url;
    private String description;

}
