package com.atexo.redaction.ws.docgen.model;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @NotEmpty
    private String id;
    @NotEmpty
    private String name;

    private List<String> roles = new ArrayList<>();


}
