package com.atexo.redaction.config;

import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

@Slf4j
public final class TrustAllManager implements X509TrustManager {

    public static final TrustAllManager THE_INSTANCE = new TrustAllManager();

    private TrustAllManager() {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
        log.info("checkClientTrusted {}", s);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
        log.info("checkServerTrusted {}", s);
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }
}

