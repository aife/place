package com.atexo.redaction.ws.mpe.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AgentMpeResponse {
    @JsonProperty("@id")
    private String idExterne;
    private Long id;
    private String login;
    private String email;
    private String nom;
    private String prenom;
    private String organisme;
    private String telephone;
    private String service;
    private Boolean actif;
}
