package com.atexo.redaction.ws.mpe.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Referentiel {
    protected Integer id;

    protected String code;

    protected String libelle;

    protected String type;

    protected String idExterne;

}
