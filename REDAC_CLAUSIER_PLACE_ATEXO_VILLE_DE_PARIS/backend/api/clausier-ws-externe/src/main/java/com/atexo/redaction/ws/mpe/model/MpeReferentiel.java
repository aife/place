package com.atexo.redaction.ws.mpe.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeReferentiel {

    @JsonProperty("@id")
    private String idExterne;
    private String libelle;
    private String abreviation;


}
