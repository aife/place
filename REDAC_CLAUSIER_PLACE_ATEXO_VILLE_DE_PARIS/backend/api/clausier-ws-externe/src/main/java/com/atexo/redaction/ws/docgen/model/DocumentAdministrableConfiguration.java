package com.atexo.redaction.ws.docgen.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DocumentAdministrableConfiguration {

    private List<ChampFusion> champsFusions;

    private List<DonneeTest> donneesTests;

}
