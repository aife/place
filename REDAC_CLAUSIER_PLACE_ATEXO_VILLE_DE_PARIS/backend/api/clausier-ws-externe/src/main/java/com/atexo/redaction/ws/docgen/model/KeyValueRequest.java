package com.atexo.redaction.ws.docgen.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class KeyValueRequest {
    @NotEmpty
    private String key;
    private Object value;
}
