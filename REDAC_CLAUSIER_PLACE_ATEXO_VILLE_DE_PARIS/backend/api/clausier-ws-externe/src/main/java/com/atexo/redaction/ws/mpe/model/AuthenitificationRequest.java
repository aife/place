package com.atexo.redaction.ws.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenitificationRequest {
    private String login;
    private String password;
}
