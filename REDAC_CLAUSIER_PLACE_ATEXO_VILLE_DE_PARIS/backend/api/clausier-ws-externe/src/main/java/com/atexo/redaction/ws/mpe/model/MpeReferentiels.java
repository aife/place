package com.atexo.redaction.ws.mpe.model;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeReferentiels {

    private MpeProcedures procedures;
    private MpeContrats contrats;

}
