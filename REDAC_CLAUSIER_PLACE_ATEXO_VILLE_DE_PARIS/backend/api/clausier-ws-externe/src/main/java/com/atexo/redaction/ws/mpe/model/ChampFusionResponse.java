package com.atexo.redaction.ws.mpe.model;

import com.atexo.redaction.ws.docgen.model.InfoBulle;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ChampFusionResponse {
    private String libelle;
    private String champFusion;
    private String blocUrl;
    private String type;
    private InfoBulle infoBulle;
    private Boolean simple;
    private Boolean collection;

}
