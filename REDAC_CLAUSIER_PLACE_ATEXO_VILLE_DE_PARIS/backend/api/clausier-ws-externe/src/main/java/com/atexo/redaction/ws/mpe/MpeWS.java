
package com.atexo.redaction.ws.mpe;


import com.atexo.redaction.ws.mpe.model.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class MpeWS {

    private final RestTemplate atexoRestTemplate;
    private static final String MESSAGE = "Token ne peut pas être null";
    private static final String ACRONYME_ORGANISME_PARAM = "acronymeOrganisme";
    private static final String PAGINATION_PARAM = "pagination";
    private static final String ACTIF_PARAM = "actif";
    private static final String TICKET_PARAM = "ticket";

    public MpeWS(RestTemplate atexoRestTemplate) {
        this.atexoRestTemplate = atexoRestTemplate;
    }

    public MpeApiResponse getServiceByOrganisme(String urlServeurUpload, String organisme, String ticket) {
        if (ticket == null) {
            throw new MPEWebserviceException(MESSAGE);
        }
        if (ticket.length() <= 100)
            return geMpetWsV1(urlServeurUpload, organisme, ticket, "/api/v1/services.json?ticket={ticket}&acronymeOrganisme={acronymeOrganisme}");
        else
            return getServiceByOrganismeV2(urlServeurUpload, organisme, ticket);
    }


    public MpeResponse getProcedureByOrganisme(String urlServeurUpload, String organisme, String ticket) {
        if (ticket == null)
            throw new MPEWebserviceException(MESSAGE);
        if (ticket.length() > 100) {
            return getProcedureByOrganismeV2(urlServeurUpload, organisme, ticket);

        } else {
            return getProcedureByOrganismeV1(urlServeurUpload, organisme, ticket).getMpe();
        }


    }

    public MpeResponse getContratsByOrganisme(String urlServeurUpload, String organisme, String ticket) {
        if (ticket == null)
            throw new MPEWebserviceException(MESSAGE);
        if (ticket.length() > 100) {
            return getContratByOrganismeV2(urlServeurUpload, organisme, ticket);
        } else {
            return getContratByOrganismeV1(urlServeurUpload, organisme, ticket).getMpe();
        }


    }

    public MpeApiResponse getServiceByOrganismeV2(String urlServeurUpload, String organisme, String token) {
        String url = urlServeurUpload + "/api/v2/referentiels/services?pagination={pagination}&organisme={acronymeOrganisme}";

        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put(ACRONYME_ORGANISME_PARAM, organisme);
        urlParameters.put(PAGINATION_PARAM, Boolean.FALSE.toString());
        MpeResponse mpe = new MpeResponse();
        Reponse reponse = new Reponse();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        com.atexo.redaction.ws.mpe.model.HydraServiceResponse services = atexoRestTemplate.exchange(url, HttpMethod.GET,
                new HttpEntity<>(headers), HydraServiceResponse.class, urlParameters).getBody();

        reponse.setServices(services.getList());
        mpe.setReponse(reponse);
        return MpeApiResponse.builder().mpe(mpe).build();

    }


    public MpeApiResponse getProcedureByOrganismeV1(String urlServeurUpload, String organisme, String token) {
        return getMpeApiResponse(urlServeurUpload, organisme, token);
    }

    public MpeApiResponse getContratByOrganismeV1(String urlServeurUpload, String organisme, String token) {
        return getMpeApiResponse(urlServeurUpload, organisme, token);
    }

    public MpeApiResponse getMpeApiResponse(String urlServeurUpload, String organisme, String token) {
        String url = urlServeurUpload + "/api/v1/referentiels/procedureContrats?ticket={ticket}&organisme={acronymeOrganisme}";
        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put(TICKET_PARAM, token);
        urlParameters.put(ACRONYME_ORGANISME_PARAM, organisme);
        log.info("get procedures/contrat from {}", url);
        String response = atexoRestTemplate.getForObject(url, String.class, urlParameters);
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return MpeApiResponse.builder().mpe(xmlMapper.readValue(response, MpeResponse.class))
                    .build();
        } catch (IOException e) {
            throw new MPEWebserviceException(e.getMessage(), e);
        }
    }

    public MpeResponse getProcedureByOrganismeV2(String urlServeurUpload, String organisme, String token) {
        String url = urlServeurUpload + "/api/v2/referentiels/procedures?pagination={pagination}&organisme={acronymeOrganisme}";

        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put(ACRONYME_ORGANISME_PARAM, organisme);
        urlParameters.put(PAGINATION_PARAM, Boolean.FALSE.toString());
        MpeResponse mpe = new MpeResponse();
        Reponse reponse = new Reponse();
        MpeReferentiels referentiels = new MpeReferentiels();
        MpeProcedures procedures = new MpeProcedures();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HydraReferentielResponse procedureTypes = atexoRestTemplate.exchange(url, HttpMethod.GET,
                new HttpEntity<>(headers), HydraReferentielResponse.class, urlParameters).getBody();

        procedures.setProcedure(procedureTypes.getList());
        referentiels.setProcedures(procedures);
        reponse.setReferentiels(referentiels);
        mpe.setReponse(reponse);
        return mpe;

    }

    public MpeResponse getContratByOrganismeV2(String urlServeurUpload, String organisme, String token) {
        String url = urlServeurUpload + "/api/v2/referentiels/contrats?pagination={pagination}&organisme={acronymeOrganisme}";

        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put(ACRONYME_ORGANISME_PARAM, organisme);
        urlParameters.put(PAGINATION_PARAM, Boolean.FALSE.toString());
        MpeResponse mpe = new MpeResponse();
        Reponse reponse = new Reponse();
        MpeReferentiels referentiels = new MpeReferentiels();
        MpeContrats contrats = new MpeContrats();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HydraReferentielResponse contratsTypes = atexoRestTemplate.exchange(url, HttpMethod.GET,
                new HttpEntity<>(headers), HydraReferentielResponse.class, urlParameters).getBody();

        contrats.setContrat(contratsTypes.getList());
        referentiels.setContrats(contrats);
        reponse.setReferentiels(referentiels);
        mpe.setReponse(reponse);
        return mpe;

    }

    public List<ChampFusionResponse> getChampsFusionV2(String urlServeurUpload, String token, String endpoint) {
        String url = urlServeurUpload + endpoint;
        HttpHeaders headers = new HttpHeaders();
        if (token != null)
            headers.setBearerAuth(token);
        Map<String, String> urlParameters = new HashMap<>();
        headers.add("accept", "application/json");
        ResponseEntity<ChampFusionResponse[]> exchange = atexoRestTemplate.exchange(url, HttpMethod.GET,
                new HttpEntity<>(headers), ChampFusionResponse[].class, urlParameters);
        return List.of(exchange.getBody());

    }


    private MpeApiResponse geMpetWsV1(String urlServeurUpload, String organisme, String token, String endpoint) {
        String url = urlServeurUpload + endpoint;


        Map<String, String> urlParameters = new HashMap<>();
        urlParameters.put(TICKET_PARAM, token);
        urlParameters.put(ACRONYME_ORGANISME_PARAM, organisme);

        return atexoRestTemplate.getForObject(url, MpeApiResponse.class, urlParameters);
    }


}
