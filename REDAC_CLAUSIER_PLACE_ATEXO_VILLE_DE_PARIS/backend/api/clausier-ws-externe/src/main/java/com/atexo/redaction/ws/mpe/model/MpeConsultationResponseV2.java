package com.atexo.redaction.ws.mpe.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MpeConsultationResponseV2 {

    private Integer id;

}
