package com.atexo.redaction.ws.docgen.model;


import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DocumentEditorRequest {
    @NotEmpty
    @NotNull
    private String documentId;
    @NotEmpty
    @NotNull
    private String plateformeId;
    private Integer version;
    @NotEmpty
    @NotNull
    private String documentTitle;
    @NotEmpty
    @NotNull
    private String callback;
    private Map<String, String> headersCallback;
    private String mode;
    @NotNull
    private User user;

    private DocumentConfiguration configuration;

    private DocumentAdministrableConfiguration documentAdministrableConfiguration;

}
