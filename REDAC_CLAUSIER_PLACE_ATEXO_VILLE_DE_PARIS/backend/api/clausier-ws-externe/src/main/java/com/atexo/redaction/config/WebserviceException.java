package com.atexo.redaction.config;

public class WebserviceException extends RuntimeException {


    public WebserviceException(String message) {
        super(message);
    }

    public WebserviceException(String message, Throwable cause) {
        super(message, cause);
    }
}
