package com.atexo.redaction.ws.keycloak;

import com.atexo.redaction.config.KeycloakProperties;
import com.atexo.redaction.config.WebserviceException;
import com.atexo.redaction.ws.keycloak.model.Token;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class KeycloackWS {

    private final RestTemplate atexoRestTemplate;


    public KeycloackWS(RestTemplate atexoRestTemplate) {
        this.atexoRestTemplate = atexoRestTemplate;

    }


    public Token refreshToken(String identiteUrl, String clientId, Token token) {
        Instant now = Instant.now();
        Instant instantToken = token.getExpiresTime().minus(2, ChronoUnit.MINUTES);
        Instant instantRefreshToken = token.getRefreshExpiresTime().minus(2, ChronoUnit.MINUTES);
        if (now.isBefore(instantToken) && now.isBefore(instantRefreshToken)) {
            return null;
        }
        return getTokenFromRefresh(identiteUrl, clientId, token);
    }

    public Token getTokenFromRefresh(String identiteUrl, String clientId, Token token) {
        try {
            URI uri = new URI(identiteUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("client_id", clientId);
            map.add("grant_type", "refresh_token");
            map.add("refresh_token", token.getRefreshToken());

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

            Token result = atexoRestTemplate.postForObject(uri, request, Token.class);
            if (result == null) {
                return token;
            }
            token.setAccessToken(result.getAccessToken());
            token.setRefreshToken(result.getRefreshToken());
            log.info("Token refreshed {} : {}", clientId, result.getAccessToken());
            return token;
        } catch (Exception e) {
            throw new WebserviceException("TECHNICAL_ERROR", e);
        }
    }

    public void verifToken(KeycloakProperties keycloakProperties, Token token) {

        Instant now = Instant.now();
        Instant instantToken = token.getExpiresTime();
        if (now.isBefore(instantToken)) {
            return;
        }

        Token tokenGenerated = this.generateToken(keycloakProperties);
        token.setAccessToken(tokenGenerated.getAccessToken());
        token.setRefreshToken(tokenGenerated.getRefreshToken());

    }

    public Token generateToken(KeycloakProperties execProperties) {
        try {
            URI uri = new URI(execProperties.getIdentiteUrl());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("client_id", execProperties.getClientId());
            map.add("username", execProperties.getUsername());
            map.add("password", execProperties.getPassword());
            map.add("grant_type", execProperties.getGrantType());

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

            return atexoRestTemplate.postForObject(uri, request, Token.class);

        } catch (Exception e) {
            throw new WebserviceException("TECHNICAL_ERROR", e);
        }
    }
}
