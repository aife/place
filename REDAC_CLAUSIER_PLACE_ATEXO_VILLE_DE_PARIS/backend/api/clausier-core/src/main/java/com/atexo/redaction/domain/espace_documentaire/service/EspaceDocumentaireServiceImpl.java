package com.atexo.redaction.domain.espace_documentaire.service;


import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import com.atexo.redaction.dao.redaction.repository.DocumentAdministrableRepository;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableException;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableExceptionEnum;
import com.atexo.redaction.domain.document.model.EditionRequest;
import com.atexo.redaction.domain.fichierDisque.mapper.FichierDisqueMapper;
import com.atexo.redaction.domain.fichierDisque.model.FichierDisque;
import com.atexo.redaction.domain.fichierDisque.service.FichierDisqueService;
import com.atexo.redaction.ws.docgen.DocgenWS;
import com.atexo.redaction.ws.docgen.model.DocumentEditorRequest;
import com.atexo.redaction.ws.docgen.model.FileStatus;
import com.atexo.redaction.ws.docgen.model.FileStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


@Slf4j
@Component
public class EspaceDocumentaireServiceImpl implements EspaceDocumentaireService {

    private final DocgenWS docgenWS;
    private final FichierDisqueService fichierDisqueService;
    private final FichierDisqueMapper fichierDisqueMapper;
    private final DocumentAdministrableRepository documentAdministrableRepository;

    public EspaceDocumentaireServiceImpl(DocgenWS docgenWS, FichierDisqueService fichierDisqueService, FichierDisqueMapper fichierDisqueMapper, DocumentAdministrableRepository documentAdministrableRepository) {
        this.docgenWS = docgenWS;
        this.fichierDisqueService = fichierDisqueService;
        this.fichierDisqueMapper = fichierDisqueMapper;
        this.documentAdministrableRepository = documentAdministrableRepository;
    }

    @Override
    public EditionRequest getUrlEdition(String editionToken) {
        return EditionRequest.builder().url(docgenWS.getEditionUrl(editionToken))
                .build();
    }

    @Override
    public String getEditionToken(File file, DocumentEditorRequest request) {
        try {
            return docgenWS.getEditionToken(file, request);
        } catch (IOException e) {
            log.error("Erreur lors de la récupération du token docgen : {}", e.getMessage());
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.API_ERROR, "Erreur lors de la récupération du token docgen");
        }

    }

    @Override
    public DocumentAdministrableEntity updateDocumentEdition(FileStatus status, DocumentAdministrableEntity documentAdministrable) {
        documentAdministrable.setStatutEdition(status.getStatus().name());
            try {
                ResponseEntity<Resource> reponse = docgenWS.getDocument(documentAdministrable.getTokenEdition());
                InputStream dossierZip = Objects.requireNonNull(reponse.getBody()).getInputStream();
                List<FichierDisqueEntity> list = new ArrayList<>();
                try (ZipInputStream zipInputStream = new ZipInputStream(dossierZip)) {
                    ZipEntry entry;
                    while ((entry = zipInputStream.getNextEntry()) != null) {
                        String entryName = entry.getName();
                        if (entryName.endsWith("_template.docx")) {
                            log.info("Récupération du template {}", entry.getName());
                            FichierDisqueEntity saved = documentAdministrable.getTemplate();
                            File tmpFile = File.createTempFile("tmp", ".docx");
                            String md5;
                            Files.copy(zipInputStream, tmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                            try (var stream = new FileInputStream(tmpFile); var toCopy = new FileInputStream(tmpFile)) {
                                md5 = org.springframework.util.DigestUtils.md5DigestAsHex(stream);
                                log.info("Récupération du document {} avec le hash {} pour le document administrable {}/{}", documentAdministrable.getLibelle(), md5, documentAdministrable.getId(), documentAdministrable.getCode());
                                if (saved == null || !md5.equals(saved.getMd5())) {
                                    String name = entry.getName();
                                    long size = entry.getSize();
                                    FichierDisque templateSaved = createFichierDisque(toCopy, name, size, md5);
                                    documentAdministrable.setTemplate(fichierDisqueMapper.mapToEntity(templateSaved));
                                    if (saved != null) {
                                        list.add(saved);
                                    }
                                }


                            } catch (Exception e) {
                                log.error("Erreur lors de la récupération du document : {}", e.getMessage());
                            } finally {
                                tmpFile.deleteOnExit();
                            }
                        } else if (entryName.endsWith(documentAdministrable.getId() + ".docx")) {
                            log.info("Récupération du fichier {}", entry.getName());
                            FichierDisqueEntity saved = documentAdministrable.getFichier();
                            File tmpFile = File.createTempFile("tmp", ".docx");
                            String md5;
                            Files.copy(zipInputStream, tmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                            try (var stream = new FileInputStream(tmpFile); var toCopy = new FileInputStream(tmpFile)) {
                                md5 = org.springframework.util.DigestUtils.md5DigestAsHex(stream);
                                log.info("Récupération du document {} avec le hash {} pour le document administrable {}/{}", documentAdministrable.getLibelle(), md5, documentAdministrable.getId(), documentAdministrable.getCode());
                                if (saved == null || !md5.equals(saved.getMd5())) {
                                    String name = entry.getName();
                                    long size = entry.getSize();
                                    FichierDisque templateSaved = createFichierDisque(toCopy, name, size, md5);
                                    documentAdministrable.setFichier(fichierDisqueMapper.mapToEntity(templateSaved));
                                    if (saved != null) {
                                        list.add(saved);
                                    }
                                }


                            } catch (Exception e) {
                                log.error("Erreur lors de la récupération du document : {}", e.getMessage());
                            } finally {
                                tmpFile.deleteOnExit();
                            }
                        }
                        documentAdministrable = documentAdministrableRepository.save(documentAdministrable);
                        zipInputStream.closeEntry();
                    }
                } catch (IOException e) {
                    throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.API_ERROR, "Erreur lors de la récupération des fichiers du zip");
                }
            } catch (IOException e) {
                throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.API_ERROR, "Erreur lors de la récupération du zip");
            }

        return documentAdministrable;
    }

    private FichierDisque createFichierDisque(InputStream inputStream, String name, long size, String md5) {
        log.info("Récupération du document {} avec le hash {}", name, md5);
        Date now = new Date();
        FichierDisque fichierToSave = FichierDisque.builder().nom(name).dateSauvegarde(now).taille(size).md5(md5)
                .build();
        return fichierDisqueService.save(fichierToSave, inputStream);
    }

    @Override
    public DocumentAdministrableEntity suiviDocument(DocumentAdministrableEntity documentAdministrable) {
        if (documentAdministrable.getTokenEdition() == null) {
            documentAdministrable.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING.name());
            return documentAdministrable;
        }
        try {


            FileStatus status = docgenWS.getDocumentStatus(documentAdministrable.getTokenEdition());
            if (status == null || status.getStatus() == null) {
                documentAdministrable.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING.name());
                return documentAdministrable;
            }

            return updateDocumentEdition(status, documentAdministrable);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération du statut du document : {}", e.getMessage());
            documentAdministrable.setStatutEdition(FileStatusEnum.CLOSED_WITHOUT_EDITING.name());
            return documentAdministrable;
        }
    }
}
