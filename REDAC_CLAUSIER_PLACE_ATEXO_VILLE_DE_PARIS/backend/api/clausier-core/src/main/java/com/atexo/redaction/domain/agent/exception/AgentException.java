package com.atexo.redaction.domain.agent.exception;

import com.atexo.redaction.config.exception.AtexoException;
import lombok.Getter;

@Getter
public class AgentException extends AtexoException {

    public AgentException(AgentExceptionEnum agentExceptionEnum, String errorMessage, Throwable err) {
        super(errorMessage, agentExceptionEnum.getCode(), agentExceptionEnum.getType(), err);

    }

    public AgentException(AgentExceptionEnum agentExceptionEnum, String errorMessage) {
        super(errorMessage, agentExceptionEnum.getCode(), agentExceptionEnum.getType());

    }
}
