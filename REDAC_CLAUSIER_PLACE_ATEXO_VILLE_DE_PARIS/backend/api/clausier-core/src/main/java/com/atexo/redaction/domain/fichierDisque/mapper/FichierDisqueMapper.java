package com.atexo.redaction.domain.fichierDisque.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import com.atexo.redaction.domain.fichierDisque.model.FichierDisque;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FichierDisqueMapper extends DaoMapper<FichierDisqueEntity, FichierDisque> {

    @Override
    FichierDisque mapToModel(FichierDisqueEntity entity);

    @Override
    FichierDisqueEntity mapToEntity(FichierDisque model);

}
