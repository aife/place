package com.atexo.redaction.config;

import org.mapstruct.MappingTarget;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

public interface DaoMapper<E, M> {

    E mapToEntity(M model);

    M mapToModel(E entity);

    void copyToEntity(M model, @MappingTarget E entity);

    void copyToModel(E entity, @MappingTarget M model);

    default List<E> mapToEntities(Collection<M> models) {
        if (isEmpty(models)) {
            return emptyList();
        }
        return models.stream()
                .map(this::mapToEntity)
                .collect(toList());
    }

    default List<M> mapToModels(Collection<E> entities) {
        if (isEmpty(entities)) {
            return emptyList();
        }
        return entities.stream()
                .map(this::mapToModel)
                .collect(toList());
    }

    default PageRepresentation<M> mapToPageModel(Page<E> responses) {
        if (Objects.isNull(responses)) {
            return null;
        }
        return PageRepresentation.<M>builder()
                .content(this.mapToModels(responses.getContent()))
                .first(responses.isFirst())
                .last(responses.isLast())
                .numberOfElements(responses.getNumberOfElements())
                .size(responses.getSize())
                .totalElements(responses.getTotalElements())
                .totalPages(responses.getTotalPages())
                .build();
    }
}
