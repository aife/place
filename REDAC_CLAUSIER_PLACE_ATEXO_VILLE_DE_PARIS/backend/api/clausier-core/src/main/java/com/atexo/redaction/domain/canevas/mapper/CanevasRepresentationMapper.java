package com.atexo.redaction.domain.canevas.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.redaction.entity.CanevasEntity;
import com.atexo.redaction.domain.canevas.model.CanevasResumeRepresentation;
import com.atexo.redaction.domain.referentiel.mapper.NatureReferentielMapper;
import com.atexo.redaction.domain.referentiel.mapper.ReferentialMpeMapper;
import org.mapstruct.Mapper;

/**
 * Canevas Mapper
 * Created by nty on 04/07/18.
 *
 * @author Nikolay Tyurin
 * @author nty
 */
@Mapper(componentModel = "spring", uses = {ReferentialMpeMapper.class, NatureReferentielMapper.class})
public interface CanevasRepresentationMapper extends DaoMapper<CanevasEntity, CanevasResumeRepresentation> {


    @Override
    CanevasResumeRepresentation mapToModel(CanevasEntity epmTCanevas);

    @Override
    CanevasEntity mapToEntity(CanevasResumeRepresentation canevasBean);


}
