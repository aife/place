package com.atexo.redaction.domain.document.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrackDocumentResponse {
    private String error;
}
