package com.atexo.redaction.domain.document.exception;

import com.atexo.redaction.config.exception.AtexoException;
import lombok.Getter;

@Getter
public class DocumentAdministrableException extends AtexoException {

    public DocumentAdministrableException(DocumentAdministrableExceptionEnum documentAdministrableExceptionEnum, String errorMessage, Throwable err) {
        super(errorMessage, documentAdministrableExceptionEnum.getCode(), documentAdministrableExceptionEnum.getType(), err);

    }

    public DocumentAdministrableException(DocumentAdministrableExceptionEnum documentAdministrableExceptionEnum, String errorMessage) {
        super(errorMessage, documentAdministrableExceptionEnum.getCode(), documentAdministrableExceptionEnum.getType());

    }
}
