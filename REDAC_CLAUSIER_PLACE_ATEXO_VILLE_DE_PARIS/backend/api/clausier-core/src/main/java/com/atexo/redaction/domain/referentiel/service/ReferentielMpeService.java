package com.atexo.redaction.domain.referentiel.service;

import com.atexo.redaction.config.DaoAdapter;
import com.atexo.redaction.config.exception.AtexoException;
import com.atexo.redaction.config.exception.ExceptionEnum;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import com.atexo.redaction.dao.referentiel.repository.ReferentielMpeRepository;
import com.atexo.redaction.domain.referentiel.mapper.ProcedureMpeMapper;
import com.atexo.redaction.domain.referentiel.mapper.ReferentialMpeMapper;
import com.atexo.redaction.domain.referentiel.mapper.ServiceMpeMapper;
import com.atexo.redaction.domain.referentiel.mapper.TypeContratMpeMapper;
import com.atexo.redaction.ws.mpe.MpeWS;
import com.atexo.redaction.ws.mpe.model.MpeApiResponse;
import com.atexo.redaction.ws.mpe.model.MpeResponse;
import com.atexo.redaction.ws.mpe.model.Referentiel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ReferentielMpeService extends DaoAdapter<Integer, ReferentielMpeEntity, Referentiel, ReferentielMpeRepository, ReferentialMpeMapper> {


    private final MpeWS mpeWS;
    private final ServiceMpeMapper serviceMpeMapper;
    private final TypeContratMpeMapper typeContratMpeMapper;
    private final ProcedureMpeMapper procedureMpeMapper;

    public ReferentielMpeService(ReferentialMpeMapper mapper, ReferentielMpeRepository repository, MpeWS mpeWS, ServiceMpeMapper serviceMpeMapper, TypeContratMpeMapper typeContratMpeMapper, ProcedureMpeMapper procedureMpeMapper) {
        super(mapper, repository);
        this.mpeWS = mpeWS;
        this.serviceMpeMapper = serviceMpeMapper;
        this.typeContratMpeMapper = typeContratMpeMapper;
        this.procedureMpeMapper = procedureMpeMapper;
    }


    public List<Referentiel> getDirectionService(AgentEntity agent) {
        String organisme = agent.getOrganisme().getCodeExterne();
        log.info("Récupération direction service : url serveur = {}, organisme = {}",
                agent.getMpeUrl(), organisme);
        final MpeApiResponse serviceByOrganisme = this.mpeWS.getServiceByOrganisme(agent.getMpeUrl(), organisme, agent.getMpeToken());
        if (null != serviceByOrganisme && null != serviceByOrganisme.getMpe() && null != serviceByOrganisme.getMpe().getReponse() &&
                !CollectionUtils.isEmpty(serviceByOrganisme.getMpe().getReponse().getServices()))
            return serviceMpeMapper.mapToModels(serviceByOrganisme.getMpe().getReponse().getServices());
        return new ArrayList<>();
    }


    public List<Referentiel> getProcedure(AgentEntity agent) {
        String organisme = agent.getOrganisme().getCodeExterne();
        log.info("Récupération procedure : url serveur = {}, organisme = {}",
                agent.getMpeUrl(), organisme);
        final MpeResponse procedureByOrganisme = this.mpeWS.getProcedureByOrganisme(agent.getMpeUrl(), organisme, agent.getMpeToken());
        if (null != procedureByOrganisme && null != procedureByOrganisme.getReponse()
                && null != procedureByOrganisme.getReponse().getReferentiels()
                && null != procedureByOrganisme.getReponse().getReferentiels().getProcedures()
                && !CollectionUtils.isEmpty(procedureByOrganisme.getReponse().getReferentiels().getProcedures().getProcedure()))
            return procedureMpeMapper.mapToModels(procedureByOrganisme.getReponse().getReferentiels().getProcedures().getProcedure()
                    .stream().filter(mpeReferentiel -> mpeReferentiel.getIdExterne() == null || mpeReferentiel.getIdExterne().contains("organisme=" + organisme)).collect(Collectors.toList())
            );
        return new ArrayList<>();
    }


    private ReferentielMpeEntity setReferential(ReferentielMpeEntity referential) {
        if (referential == null) {
            return null;
        }
        if (referential.getOrganisme() == null) {
            throw new AtexoException("L'organisme du référentiel est obligatoire", ExceptionEnum.MANDATORY.getCode(), ExceptionEnum.MANDATORY.getType());
        }
        log.info("Récupération référentiel : code = {}, type = {}", referential.getCode(), referential.getType());

        ReferentielMpeEntity savedReferential = repository.findByCodeAndTypeAndOrganismeId(referential.getCode(), referential.getType(), referential.getOrganisme().getId()).orElse(null);
        return null != savedReferential ? savedReferential : repository.save(referential);
    }

    public ReferentielMpeEntity getReferential(ReferentielMpeEntity referential) {
        if (referential == null) {
            return null;
        }
        return setReferential(referential);
    }


    public List<Referentiel> getTypeContrat(AgentEntity agent) {
        String organisme = agent.getOrganisme().getCodeExterne();
        log.info("Récupération type contrat : url serveur = {},  organisme = {}",
                agent.getMpeUrl(), organisme);
        final MpeResponse procedureByOrganisme = this.mpeWS.getContratsByOrganisme(agent.getMpeUrl(), organisme, agent.getMpeToken());
        if (null != procedureByOrganisme && null != procedureByOrganisme.getReponse()
                && null != procedureByOrganisme.getReponse().getReferentiels()
                && null != procedureByOrganisme.getReponse().getReferentiels().getContrats()
                && !CollectionUtils.isEmpty(procedureByOrganisme.getReponse().getReferentiels().getContrats().getContrat()))
            return typeContratMpeMapper.mapToModels(procedureByOrganisme.getReponse().getReferentiels().getContrats().getContrat());
        return new ArrayList<>();
    }


}
