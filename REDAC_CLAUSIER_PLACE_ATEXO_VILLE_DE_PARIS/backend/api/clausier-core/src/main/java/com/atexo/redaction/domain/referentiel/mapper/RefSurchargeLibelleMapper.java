package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.referentiel.entity.RefSurchargeLibelleEntity;
import com.atexo.redaction.domain.referentiel.model.RefSurchargeLibelle;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RefSurchargeLibelleMapper extends DaoMapper<RefSurchargeLibelleEntity, RefSurchargeLibelle> {

    @Override
    RefSurchargeLibelle mapToModel(RefSurchargeLibelleEntity entity);
}
