package com.atexo.redaction.domain.agent.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TechnicalAgent extends Agent {
    


    private String mpeToken;

    private String mpeRefreshToken;


    private String envoiMpe;

}
