package com.atexo.redaction.config;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WsMessage {

    private int code;
    private String message;
    private String type;

}
