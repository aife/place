package com.atexo.redaction.domain.agent.model;

import lombok.*;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class CapaciteUpdates {

    private Double capacite;

    private String idAgent;

    private int mois;

    private int annee;

}
