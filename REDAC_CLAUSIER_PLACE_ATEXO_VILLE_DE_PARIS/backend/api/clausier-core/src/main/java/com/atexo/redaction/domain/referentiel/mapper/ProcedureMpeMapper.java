package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.config.WsMapper;
import com.atexo.redaction.ws.mpe.model.MpeReferentiel;
import com.atexo.redaction.ws.mpe.model.Referentiel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProcedureMpeMapper extends WsMapper<MpeReferentiel, Referentiel> {

    @Mapping(source = "abreviation", target = "code")
    @Mapping(source = "libelle", target = "libelle")
    @Mapping(target = "type", constant = "PROCEDURE")
    @Override
    Referentiel mapToModel(MpeReferentiel mpeProcedure);

}
