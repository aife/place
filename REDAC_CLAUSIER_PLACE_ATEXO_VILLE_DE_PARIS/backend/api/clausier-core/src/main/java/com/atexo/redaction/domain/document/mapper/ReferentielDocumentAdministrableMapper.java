package com.atexo.redaction.domain.document.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.domain.agent.mapper.StringAndListStringMapper;
import com.atexo.redaction.domain.document.model.ReferentielDocumentAdministrable;
import com.atexo.redaction.domain.organisme.mapper.OrganismeMapper;
import com.atexo.redaction.domain.referentiel.mapper.ReferentialMpeMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class, OrganismeMapper.class, ReferentialMpeMapper.class})
public interface ReferentielDocumentAdministrableMapper extends DaoMapper<DocumentAdministrableEntity, ReferentielDocumentAdministrable> {

    @Mapping(target = "extension", source = "entity", qualifiedByName = "toExtension")
    @Override
    ReferentielDocumentAdministrable mapToModel(DocumentAdministrableEntity entity);

    @Override
    DocumentAdministrableEntity mapToEntity(ReferentielDocumentAdministrable model);


    @Named("toExtension")
    default String toExtension(DocumentAdministrableEntity model) {
        return model == null || model.getFichier() == null ? null : getExtension(model.getFichier().getNom());
    }

    private String getExtension(String strFichier) {
        if (strFichier.lastIndexOf(".") != -1) {
            if (strFichier.lastIndexOf(".") + 1 <= strFichier.length()) {
                return strFichier.substring(strFichier.lastIndexOf(".") + 1);
            }
        }
        return "";
    }
}
