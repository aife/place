package com.atexo.redaction.domain.fichierDisque.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FichierDisqueExceptionEnum {
    NOT_FOUND(404, "Fichier non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement du fichier"),
    MANDATORY(400, "Champs obligatoires"), FOLDER_ERROR(500, "Erreur lors de la création du dossier"), NOT_AUTHORIZED(401, "Utilisateur non autorisé");


    private final int code;
    private final String type;

}
