package com.atexo.redaction.domain.document.model;

import com.atexo.redaction.ws.docgen.model.FileStatusEnum;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DocumentAdministrable {

    private Long id;

    private String libelle;

    private String code;

    private String produit;

    private String agentId;
    private String creeParId;
    private String modifieParId;

    private Long documentParentId;

    private boolean actif;

    private Integer serviceId;

    private Integer organismeId;

    private Integer fichierId;

    private Integer templateId;
    private String extension;

    private boolean champFusion;

    private FileStatusEnum statutEdition;

    private String tokenEdition;

    private String plateformeUuid;

}
