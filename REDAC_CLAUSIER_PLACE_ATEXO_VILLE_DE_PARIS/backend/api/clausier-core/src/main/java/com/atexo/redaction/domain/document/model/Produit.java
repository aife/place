package com.atexo.redaction.domain.document.model;

import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    private String nom;
    private String url;



}
