package com.atexo.redaction.domain.produit.exception;

import com.atexo.redaction.config.exception.AtexoException;
import lombok.Getter;

@Getter
public class ProduitException extends AtexoException {

    public ProduitException(ProduitExceptionEnum produitExceptionEnum, String errorMessage, Throwable err) {
        super(errorMessage, produitExceptionEnum.getCode(),produitExceptionEnum.getType(), err);

    }

    public ProduitException(ProduitExceptionEnum produitExceptionEnum, String errorMessage) {
        super(errorMessage, produitExceptionEnum.getCode(), produitExceptionEnum.getType());

    }
}
