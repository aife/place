package com.atexo.redaction.domain.organisme.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Organisme {

    private Integer id;

    private String libelle;

    private String codeExterne;

    private String plateformeUuid;

}
