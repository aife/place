package com.atexo.redaction.domain.produit.service;

import com.atexo.redaction.dao.redaction.entity.ProduitEntity;
import com.atexo.redaction.dao.redaction.repository.ProduitRepository;
import com.atexo.redaction.domain.produit.exception.ProduitException;
import com.atexo.redaction.domain.produit.exception.ProduitExceptionEnum;
import com.atexo.redaction.domain.produit.mapper.ProduitMapper;
import com.atexo.redaction.domain.produit.model.Produit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ProduitService {

    private final ProduitRepository produitRepository;
    private final ProduitMapper produitMapper;


    public ProduitService(ProduitRepository produitRepository, ProduitMapper produitMapper) {


        this.produitRepository = produitRepository;
        this.produitMapper = produitMapper;
    }

    public List<String> getNomsProduitsActifs() {
        try {
            return produitRepository.findAllByActifIsTrue().stream().map(ProduitEntity::getNom).map(String::toUpperCase).collect(Collectors.toList());

        } catch (Exception e) {
            throw new ProduitException(ProduitExceptionEnum.FUNCTIONAL_ERROR, "Erreur pendant la récupération des produits actifs");
        }

    }

    public Produit getProduitBddByNom(String nom) {
        ProduitEntity produit = produitRepository.findByNomAndActifIsTrue(nom.toUpperCase());
        if (produit != null) {
            return produitMapper.mapToModel(produit);
        }
        else {
            throw new ProduitException(ProduitExceptionEnum.NOT_FOUND, "Pas de produit actif trouvé avec le nom " + nom);
        }
    }



}
