package com.atexo.redaction.domain.document.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ReferentielDocumentAdministrable {


    private String libelle;

    private String code;

    private String produit;

    private String extension;

}
