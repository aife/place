package com.atexo.redaction.domain.fichierDisque.model;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class FichierDisque {

    private int idFichier;

    private String nom;

    private String chemin;

    private String md5;

    private Date dateSauvegarde;

    private Long taille;

}
