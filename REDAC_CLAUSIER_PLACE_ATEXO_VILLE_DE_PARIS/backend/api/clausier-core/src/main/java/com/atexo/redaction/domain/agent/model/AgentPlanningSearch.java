package com.atexo.redaction.domain.agent.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AgentPlanningSearch {
    @NotNull
    private int moisMin;
    @NotNull
    private int moisMax;
    @NotNull
    private int anneeMin;
    @NotNull
    private int anneeMax;
    private List<String> agent;
    private List<String> projets;
    private List<Integer> services;

}
