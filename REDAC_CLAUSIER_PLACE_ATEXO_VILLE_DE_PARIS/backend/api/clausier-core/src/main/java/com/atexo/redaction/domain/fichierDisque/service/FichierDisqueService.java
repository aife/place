package com.atexo.redaction.domain.fichierDisque.service;

import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import com.atexo.redaction.dao.redaction.repository.FichierDisqueRepository;
import com.atexo.redaction.domain.fichierDisque.exception.FichierDisqueException;
import com.atexo.redaction.domain.fichierDisque.exception.FichierDisqueExceptionEnum;
import com.atexo.redaction.domain.fichierDisque.mapper.FichierDisqueMapper;
import com.atexo.redaction.domain.fichierDisque.model.FichierDisque;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Slf4j
@Component
public class FichierDisqueService {


    private final String path;

    private final FichierDisqueRepository fichierDisqueRepository;
    private final FichierDisqueMapper fichierDisqueMapper;
    private static final String ERROR_MESSAGE = "le fichier n'existe pas";
    private static final String ERROR_MESSAGE_PARAMS = "les parametres du fichier ne doivent pas être null";


    public FichierDisqueService(@Value("${repertoire.fichiers}") String path, FichierDisqueRepository fichierDisqueRepository, FichierDisqueMapper fichierDisqueMapper) {
        this.path = path;
        this.fichierDisqueRepository = fichierDisqueRepository;
        this.fichierDisqueMapper = fichierDisqueMapper;
    }

    public boolean deleteById(Integer id) {
        log.info("Suppression fichier : id {}", id);
        this.fichierDisqueRepository.deleteById(id);
        return true;
    }

    public FichierDisqueEntity getById(Integer id) {
        log.info("Récupération fichier : id {}", id);
        return this.fichierDisqueRepository.findById(id).orElse(null);
    }

    public ByteArrayResource loadFileAsResource(String chemin) {
        try {
            Path pathResource = Paths.get(this.path, chemin);
            if (pathResource.toFile().exists()) {
                return new ByteArrayResource(Files.readAllBytes(pathResource));
            } else {
                throw new FichierDisqueException(FichierDisqueExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
            }
        } catch (IOException ex) {
            throw new FichierDisqueException(FichierDisqueExceptionEnum.NOT_FOUND, ERROR_MESSAGE);
        }
    }

    public File loadFile(String chemin) {
        return Paths.get(this.path, chemin).toFile();

    }

    public boolean filesExists(String chemin) {
        return Paths.get(this.path, chemin).toFile().exists();

    }


    public FichierDisque save(FichierDisque fichierDisque, InputStream inputStream) {
        if (fichierDisque == null) {
            throw new FichierDisqueException(FichierDisqueExceptionEnum.MANDATORY, ERROR_MESSAGE_PARAMS);
        }
        String relativePath = this.createPathToUpload();
        return getFichier(fichierDisque, inputStream, relativePath);
    }


    public FichierDisque getFichier(FichierDisque fichierDisque, InputStream inputStream, String relativePath) {
        File file = new File(path, relativePath);
        try {
            Files.createDirectories(file.toPath());
        } catch (IOException e) {
            throw new FichierDisqueException(FichierDisqueExceptionEnum.FOLDER_ERROR, e.getMessage());
        }
        try {
            this.storageFile(inputStream, file.getAbsolutePath());
            fichierDisque.setChemin(relativePath);
            fichierDisque.setTaille(file.length());
            log.info("Sauvegarde fichier : name = {}, path = {}", fichierDisque.getNom(), fichierDisque.getChemin());

            return fichierDisqueMapper.mapToModel(fichierDisqueRepository.save(fichierDisqueMapper.mapToEntity(fichierDisque)));
        } catch (Exception e) {
            this.cleanUpFile(file.getAbsolutePath());
            throw new FichierDisqueException(FichierDisqueExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    private void storageFile(InputStream inputStream, String pathFile) {
        if (inputStream == null) {
            throw new FichierDisqueException(FichierDisqueExceptionEnum.MANDATORY, "le fichier ne doit pas être null");
        }

        try {
            Path of = Path.of(pathFile);
            Path pathResource = of.getParent();
            if (!pathResource.toFile().exists()) {
                boolean created = pathResource.toFile().mkdirs();
                if (!created)
                    throw new FichierDisqueException(FichierDisqueExceptionEnum.FOLDER_ERROR, "Erreur lors de la création du dossier parent");
            }
            Files.copy(inputStream, of, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new FichierDisqueException(FichierDisqueExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }

    private String createPathToUpload() {
        StringBuilder result = new StringBuilder();
        LocalDate now = LocalDate.now();
        result.append(now.getYear());
        result.append(File.separator);
        result.append(now.getMonthValue());
        result.append(File.separator);
        result.append(now.getDayOfMonth());
        result.append(File.separator);
        UUID uuid = UUID.randomUUID();
        result.append(uuid);
        return result.toString();
    }

    private void cleanUpFile(String path) {
        Path pathFileToDelete = new File(path).toPath();
        if (Files.exists(pathFileToDelete)) {
            try {
                Files.delete(pathFileToDelete);
            } catch (IOException e) {
                throw new FichierDisqueException(FichierDisqueExceptionEnum.FOLDER_ERROR, e.getMessage());
            }
        }
    }


    public void deleteAll(List<FichierDisqueEntity> list) {
        log.info("Suppression de {} fichiers", list.size());
        this.fichierDisqueRepository.deleteAll(list);
    }
}
