package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.redaction.entity.RefTypeContratEditeurEntity;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RefTypeContratEditeurMapper extends DaoMapper<RefTypeContratEditeurEntity, ReferentielRepresentation> {

    @Override
    @Mapping(source = "libelle", target = "label")
    @Mapping(source = "codeExterne", target = "uid")
    @Mapping(source = "codeExterne", target = "externalCode")
    ReferentielRepresentation mapToModel(RefTypeContratEditeurEntity entity);
}
