package com.atexo.redaction.domain.agent.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "envoi"
})
@XmlRootElement(name = "mpe")
public class Mpe {
    @NotNull(message = "Envoie ne peut pas être null")
    @Valid
    protected Envoi envoi;


    /**
     * Obtient la valeur de la propriété envoi.
     *
     * @return possible object is
     * {@link Envoi }
     */
    public Envoi getEnvoi() {
        return envoi;
    }

    /**
     * Définit la valeur de la propriété envoi.
     *
     * @param value allowed object is
     *              {@link Envoi }
     */
    public void setEnvoi(Envoi value) {
        this.envoi = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="annonce" type="{http://www.atexo.com/epm/xml}AnnonceType"/&gt;
     *           &lt;element name="consultation" type="{http://www.atexo.com/epm/xml}ConsultationType"/&gt;
     *           &lt;element name="offre" type="{http://www.atexo.com/epm/xml}OffreType"/&gt;
     *           &lt;element name="enveloppe" type="{http://www.atexo.com/epm/xml}EnveloppeObjectType"/&gt;
     *           &lt;element name="agent" type="{http://www.atexo.com/epm/xml}AgentType" minOccurs="0"/&gt;
     *           &lt;element name="service" type="{http://www.atexo.com/epm/xml}ServiceType" minOccurs="0"/&gt;
     *           &lt;element name="modification" type="{http://www.atexo.com/epm/xml}ModificationType" minOccurs="0"/&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "agent"
    })
    public static class Envoi {


        @Valid
        @NotNull(message = "Agent ne peut pas être null")
        protected AgentType agent;


        /**
         * Obtient la valeur de la propriété agent.
         *
         * @return possible object is
         * {@link AgentType }
         */
        public AgentType getAgent() {
            return agent;
        }

        /**
         * Définit la valeur de la propriété agent.
         *
         * @param value allowed object is
         *              {@link AgentType }
         */
        public void setAgent(AgentType value) {
            this.agent = value;
        }


    }


}
