package com.atexo.redaction.domain.canevas.service;

import com.atexo.redaction.config.PageRepresentation;
import com.atexo.redaction.domain.canevas.model.CanevasResumeRepresentation;
import com.atexo.redaction.dao.redaction.specification.RechercheCanevasSpecification;
import org.springframework.data.domain.Pageable;

public interface CanevasService {
    PageRepresentation<CanevasResumeRepresentation> search(RechercheCanevasSpecification search, Pageable pageable);
}
