package com.atexo.redaction.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "keycloak")
@Getter
@Setter
public class ClausierKeycloackProperties {
    private String authServerUrl;
    private String realm;
    private String resource;
}
