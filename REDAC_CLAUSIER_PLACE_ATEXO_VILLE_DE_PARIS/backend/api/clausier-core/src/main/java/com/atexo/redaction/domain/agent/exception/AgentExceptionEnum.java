package com.atexo.redaction.domain.agent.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AgentExceptionEnum {
    NOT_FOUND(404, "Utilisateur non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement de l'agent"),
    NOT_AUTHORIZED(401, "Utilisateur non autorisé"),
    MANDATORY(400, "Champs obligatoires"),
    MPE_ENVOI(500, "Erreur lors de la récupération de agent MPE");

    private final int code;
    private final String type;

}
