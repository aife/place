package com.atexo.redaction.domain.canevas.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CanevasDetailRepresentation {

    private long serialVersionUID = 1L;

    private int idCanevas;

    private Integer idPublication;

    private String lastVersion;

    private String ref;

    private String titre;

    private List<Integer> procedurePassationList;

    private List<Integer> typeContratList;

    private int typeDocument;

    private int naturePrestation;

    private boolean statut;

    private String auteur;

    private String message; //toujours null , correspond à quoi ?

    private boolean compatibleEntiteAdjudicatrice = true;

    private Integer idOrganisme;

    private boolean canevasEditeur;

    private Boolean editeur; //toujours null , correspond à quoi ?

    private Integer idStatutRedactionClausier;

    private String creeLe;

    private String modifieLe;

    private Integer idRefCCAG;

    /**
     * Déterminer si on peut gérer les dérogations sur ce canevas. C'est le cas
     * pour le type CCAP, CCAP-AE, CCP et CCP-AE.
     */
    private boolean derogationActive;

    /**
     * True si la valeur précédente du champ 'Type de document' pour lequel
     * l'option Dérogation CCAG est activée(CCAP, CCAP-AE, CCP ou CCP-AE) ET si
     * la nouvelle valeur est différente de CCAP, CCAP-AE, CCP ou CCP-AE
     * (c'est-à-dire la dérogation devient inactif pour ce Canevas)
     */
    private boolean derogationDevenirInactif;

    /**
     * renvoie l'id du type de document du cote de passation , cet ID est recupere à partir du TypeDocument dans EpmTCanevas
     *
     * */
    private Integer idTypePassation;









}
