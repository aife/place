package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.config.WsMapper;
import com.atexo.redaction.ws.mpe.model.Referentiel;
import com.atexo.redaction.ws.mpe.model.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ServiceMpeMapper extends WsMapper<Service, Referentiel> {

    @Mapping(source = "sigle", target = "code")
    @Mapping(source = "libelle", target = "libelle")
    @Mapping(target = "type", constant = "DIRECTION_SERVICE")
    @Override
    Referentiel mapToModel(Service service);

}
