package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.ws.mpe.model.Referentiel;
import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ReferentialMpeMapper extends DaoMapper<ReferentielMpeEntity, Referentiel> {


    @Override
    Referentiel mapToModel(ReferentielMpeEntity entity);


}
