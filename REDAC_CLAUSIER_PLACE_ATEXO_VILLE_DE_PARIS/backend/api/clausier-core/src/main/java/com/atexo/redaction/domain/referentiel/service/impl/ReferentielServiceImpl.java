package com.atexo.redaction.domain.referentiel.service.impl;

import com.atexo.redaction.dao.organisme.repository.OrganismeRepository;
import com.atexo.redaction.dao.referentiel.entity.RefSurchargeLibelleEntity;
import com.atexo.redaction.dao.referentiel.repository.RefCcagRepository;
import com.atexo.redaction.dao.referentiel.repository.RefNatureRepository;
import com.atexo.redaction.dao.referentiel.repository.RefSurchargeLibelleRepository;
import com.atexo.redaction.domain.referentiel.mapper.CcagReferentielMapper;
import com.atexo.redaction.domain.referentiel.mapper.NatureReferentielMapper;
import com.atexo.redaction.domain.referentiel.mapper.ProcedureReferentielMapper;
import com.atexo.redaction.domain.referentiel.mapper.RefSurchargeLibelleMapper;
import com.atexo.redaction.domain.referentiel.model.RefSurchargeLibelle;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;
import com.atexo.redaction.domain.referentiel.service.ReferentielService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReferentielServiceImpl implements ReferentielService {

    private final RefCcagRepository refCcagRepository;
    private final CcagReferentielMapper ccagReferentielMapper;
    private final RefSurchargeLibelleMapper refSurchargeLibelleMapper;
    private final RefNatureRepository refNatureRepository;
    private final NatureReferentielMapper natureReferentielMapper;
    private final OrganismeRepository organismeRepository;
    private final RefSurchargeLibelleRepository refSurchargeLibelleRepository;
    private final ProcedureReferentielMapper procedureReferentielMapper;

    public ReferentielServiceImpl(RefCcagRepository refCcagRepository, CcagReferentielMapper ccagReferentielMapper, RefSurchargeLibelleMapper refSurchargeLibelleMapper, RefNatureRepository refNatureRepository, NatureReferentielMapper natureReferentielMapper, OrganismeRepository organismeRepository, RefSurchargeLibelleRepository refSurchargeLibelleRepository, ProcedureReferentielMapper procedureReferentielMapper) {
        this.refCcagRepository = refCcagRepository;
        this.ccagReferentielMapper = ccagReferentielMapper;
        this.refSurchargeLibelleMapper = refSurchargeLibelleMapper;
        this.refNatureRepository = refNatureRepository;
        this.natureReferentielMapper = natureReferentielMapper;
        this.organismeRepository = organismeRepository;
        this.refSurchargeLibelleRepository = refSurchargeLibelleRepository;
        this.procedureReferentielMapper = procedureReferentielMapper;
    }


    @Override
    public List<ReferentielRepresentation> findListNatures() {
        return natureReferentielMapper.mapToModels(refNatureRepository.findAll());
    }


    @Override
    public List<ReferentielRepresentation> findListRefCCAG() {
        return ccagReferentielMapper.mapToModels(refCcagRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListProcedures(Integer idOrganisme) {
        var organisme = organismeRepository.findById(idOrganisme).orElseThrow();
        return procedureReferentielMapper.mapToModels(organisme.getEpmTRefProcedureSet());
    }

    @Override
    public Map<String, String> getSurchargesLibelles() {
        Map<String, String> map = new HashMap<>();
        refSurchargeLibelleRepository.findAll().forEach(refSurchargeLibelleEntity -> map.put(refSurchargeLibelleEntity.getCode(), refSurchargeLibelleEntity.getLibelle()));
        return map;
    }

}
