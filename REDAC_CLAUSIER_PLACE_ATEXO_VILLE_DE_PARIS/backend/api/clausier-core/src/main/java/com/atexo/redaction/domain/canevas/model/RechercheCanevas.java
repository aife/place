package com.atexo.redaction.domain.canevas.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RechercheCanevas {
    private String referenceCanevas;
    private String referenceClause;
    private int idNaturePrestation;
    private int typeAuteur;
    private int idStatutRedactionClausier;
    private int idTypeDocument;
    private Integer idTypeContrat;
    private Integer idProcedure;
    private Integer idOrganisme;
    private Boolean actif;
    private Date dateModificationMin;
    private Date dateModificationMax;
    private boolean editeur;


}
