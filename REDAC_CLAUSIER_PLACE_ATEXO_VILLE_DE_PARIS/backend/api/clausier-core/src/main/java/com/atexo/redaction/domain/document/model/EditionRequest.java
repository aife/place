package com.atexo.redaction.domain.document.model;


import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class EditionRequest {
    private String url;
}
