package com.atexo.redaction.domain.referentiel.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RefSurchargeLibelle {

    private String code;

    private String libelle;


}
