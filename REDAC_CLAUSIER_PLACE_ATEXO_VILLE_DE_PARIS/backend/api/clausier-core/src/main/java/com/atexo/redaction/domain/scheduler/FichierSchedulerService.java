
package com.atexo.redaction.domain.scheduler;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@Slf4j
public class FichierSchedulerService {


    private static final long DELAI_SUPPRESION_FICHIER_TMP = 1000L * 60L * 60L * 24L;

    @Scheduled(cron = "${clausier.scheduler.cron.delete-tmp}")
    public void cleanTmpFiles() {
        File[] files = FileUtils.getTempDirectory().listFiles();
        long date = System.currentTimeMillis() - DELAI_SUPPRESION_FICHIER_TMP;
        for (File file : files) {
            if (file.getName().equals("javamelody") || file.getName().equals("noyau")) {
                continue;
            }
            if (FileUtils.isFileOlder(file, date)) {
                log.info("fichier {} supprime", file.getAbsolutePath());
                FileUtils.deleteQuietly(file);
            }
        }

    }

}
