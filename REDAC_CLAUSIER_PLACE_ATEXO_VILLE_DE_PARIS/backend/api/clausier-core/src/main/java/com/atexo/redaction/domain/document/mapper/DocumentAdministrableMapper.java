package com.atexo.redaction.domain.document.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import com.atexo.redaction.domain.agent.mapper.StringAndListStringMapper;
import com.atexo.redaction.domain.document.model.DocumentAdministrable;
import com.atexo.redaction.domain.organisme.mapper.OrganismeMapper;
import com.atexo.redaction.domain.referentiel.mapper.ReferentialMpeMapper;
import org.apache.commons.io.FilenameUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class, OrganismeMapper.class, ReferentialMpeMapper.class})
public interface DocumentAdministrableMapper extends DaoMapper<DocumentAdministrableEntity, DocumentAdministrable> {
    @Mapping(target = "agentId", source = "entity", qualifiedByName = "toAgentId")
    @Mapping(target = "creeParId", source = "entity", qualifiedByName = "toCreeParId")
    @Mapping(target = "modifieParId", source = "entity", qualifiedByName = "toModifieParId")
    @Mapping(target = "serviceId", source = "entity", qualifiedByName = "toServiceId")
    @Mapping(target = "organismeId", source = "entity", qualifiedByName = "toOrganismeId")
    @Mapping(target = "fichierId", source = "entity", qualifiedByName = "toFichierId")
    @Mapping(target = "templateId", source = "entity", qualifiedByName = "toTemplateId")
    @Mapping(target = "extension", source = "entity", qualifiedByName = "toExtension")
    @Mapping(target = "documentParentId", source = "entity", qualifiedByName = "toDocumentParentId")
    @Override
    DocumentAdministrable mapToModel(DocumentAdministrableEntity entity);

    @Mapping(target = "agent", source = "model", qualifiedByName = "toAgent")
    @Mapping(target = "creePar", source = "model", qualifiedByName = "toCreePar")
    @Mapping(target = "modifiePar", source = "model", qualifiedByName = "toModifiePar")
    @Mapping(target = "service", source = "model", qualifiedByName = "toService")
    @Mapping(target = "organisme", source = "model", qualifiedByName = "toOrganisme")
    @Mapping(target = "fichier", source = "model", qualifiedByName = "toFichier")
    @Mapping(target = "template", source = "model", qualifiedByName = "toTemplate")
    @Mapping(target = "documentParent", source = "model", qualifiedByName = "toDocumentParent")
    @Override
    DocumentAdministrableEntity mapToEntity(DocumentAdministrable model);

    @Named("toAgentId")
    default String toAgentId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getAgent() == null ? null : entity.getAgent().getId();
    }

    @Named("toAgent")
    default AgentEntity toAgent(DocumentAdministrable model) {
        return model == null || model.getAgentId() == null ? null : AgentEntity.builder().id(model.getAgentId()).build();
    }

    @Named("toCreeParId")
    default String toCreeParId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getCreePar() == null ? null : entity.getCreePar().getId();
    }

    @Named("toCreePar")
    default AgentEntity toCreePar(DocumentAdministrable model) {
        return model == null || model.getCreeParId() == null ? null : AgentEntity.builder().id(model.getCreeParId()).build();
    }

    @Named("toModifieParId")
    default String toModifieParId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getModifiePar() == null ? null : entity.getModifiePar().getId();
    }

    @Named("toModifiePar")
    default AgentEntity toModifiePar(DocumentAdministrable model) {
        return model == null || model.getModifieParId() == null ? null : AgentEntity.builder().id(model.getModifieParId()).build();
    }

    @Named("toServiceId")
    default Integer toServiceId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getService() == null ? null : entity.getService().getId();
    }

    @Named("toService")
    default ReferentielMpeEntity toService(DocumentAdministrable model) {
        return model == null || model.getServiceId() == null ? null : ReferentielMpeEntity.builder().id(model.getServiceId()).build();
    }

    @Named("toOrganismeId")
    default Integer toOrganismeId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getOrganisme() == null ? null : entity.getOrganisme().getId();
    }

    @Named("toOrganisme")
    default EpmTRefOrganisme toOrganisme(DocumentAdministrable model) {
        return model == null || model.getOrganismeId() == null ? null : EpmTRefOrganisme.builder().id(model.getOrganismeId()).build();
    }

    @Named("toFichierId")
    default Integer toFichierId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getFichier() == null ? null : entity.getFichier().getIdFichier();
    }

    @Named("toTemplateId")
    default Integer toTemplateId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getTemplate() == null ? null : entity.getTemplate().getIdFichier();
    }

    @Named("toExtension")
    default String toExtension(DocumentAdministrableEntity entity) {
        return entity == null || entity.getTemplate() == null ? null : FilenameUtils.getExtension(entity.getTemplate().getNom());
    }

    @Named("toFichier")
    default FichierDisqueEntity toFichier(DocumentAdministrable model) {
        return model == null || model.getFichierId() == null ? null : FichierDisqueEntity.builder().idFichier(model.getFichierId()).build();
    }

    @Named("toTemplate")
    default FichierDisqueEntity toTemplate(DocumentAdministrable model) {
        return model == null || model.getTemplateId() == null ? null : FichierDisqueEntity.builder().idFichier(model.getTemplateId()).build();
    }

    @Named("toDocumentParentId")
    default Long toDocumentParentId(DocumentAdministrableEntity entity) {
        return entity == null || entity.getDocumentParent() == null ? null : entity.getDocumentParent().getId();
    }

    @Named("toDocumentParent")
    default DocumentAdministrableEntity toDocumentParent(DocumentAdministrable model) {
        return model == null || model.getDocumentParentId() == null ? null : DocumentAdministrableEntity.builder().id(model.getDocumentParentId()).build();
    }
}
