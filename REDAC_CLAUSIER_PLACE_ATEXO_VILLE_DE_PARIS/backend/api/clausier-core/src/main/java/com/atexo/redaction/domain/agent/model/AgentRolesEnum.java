package com.atexo.redaction.domain.agent.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AgentRolesEnum {
    CONSULTATION("consultation"), CREATION("creation"), MODIFICATION("modification"),
    VALIDATION("validation");

    private final String value;

}
