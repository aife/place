package com.atexo.redaction.domain.referentiel.service.impl;

import com.atexo.redaction.dao.redaction.repository.*;
import com.atexo.redaction.domain.referentiel.mapper.*;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;
import com.atexo.redaction.domain.referentiel.service.RedactionReferentielService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RedactionReferentielServiceImpl implements RedactionReferentielService {

    private final RefStatutRedactionClausierRepository refStatutRedactionClausierRepository;
    private final RefThemeClauseRepository refThemeClauseRepository;
    private final RefTypeClauseRepository refTypeClauseRepository;
    private final StatutRedactionReferentielMapper statutRedactionReferentielMapper;
    private final ThemeClauseReferentielMapper themeClauseReferentielMapper;
    private final TypeClauseReferentielMapper typeClauseReferentielMapper;
    private final RefTypeDocumentRepository refTypeDocumentRepository;
    private final TypeDocumentReferentielMapper typeDocumentReferentielMapper;
    private final ValeurTypeClauseReferentielMapper valeurTypeClauseReferentielMapper;
    private final RefAuteurRepository refAuteurRepository;
    private final ActeurReferentielMapper acteurReferentielMapper;
    private final RefTypeContratRepository refTypeContratRepository;
    private final RefTypeContratEditeurMapper refTypeContratEditeurMapper;
    private final RefValeurTypeClauseRepository refValeurTypeClauseRepository;


    public RedactionReferentielServiceImpl(RefStatutRedactionClausierRepository refStatutRedactionClausierRepository, RefThemeClauseRepository refThemeClauseRepository, RefTypeClauseRepository refTypeClauseRepository, StatutRedactionReferentielMapper statutRedactionReferentielMapper, ThemeClauseReferentielMapper themeClauseReferentielMapper, TypeClauseReferentielMapper typeClauseReferentielMapper, RefTypeDocumentRepository refTypeDocumentRepository, TypeDocumentReferentielMapper typeDocumentReferentielMapper, ValeurTypeClauseReferentielMapper valeurTypeClauseReferentielMapper, RefAuteurRepository refAuteurRepository, ActeurReferentielMapper acteurReferentielMapper, RefTypeContratRepository refTypeContratRepository, RefTypeContratEditeurMapper refTypeContratEditeurMapper, RefValeurTypeClauseRepository refValeurTypeClauseRepository) {
        this.refStatutRedactionClausierRepository = refStatutRedactionClausierRepository;
        this.refThemeClauseRepository = refThemeClauseRepository;
        this.refTypeClauseRepository = refTypeClauseRepository;
        this.statutRedactionReferentielMapper = statutRedactionReferentielMapper;
        this.themeClauseReferentielMapper = themeClauseReferentielMapper;
        this.typeClauseReferentielMapper = typeClauseReferentielMapper;
        this.refTypeDocumentRepository = refTypeDocumentRepository;
        this.typeDocumentReferentielMapper = typeDocumentReferentielMapper;
        this.valeurTypeClauseReferentielMapper = valeurTypeClauseReferentielMapper;
        this.refAuteurRepository = refAuteurRepository;
        this.acteurReferentielMapper = acteurReferentielMapper;
        this.refTypeContratRepository = refTypeContratRepository;
        this.refTypeContratEditeurMapper = refTypeContratEditeurMapper;
        this.refValeurTypeClauseRepository = refValeurTypeClauseRepository;
    }

    @Override
    public List<ReferentielRepresentation> findListStatutsRedactionClausier() {
        return statutRedactionReferentielMapper.mapToModels(refStatutRedactionClausierRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListTypesDocument() {
        return typeDocumentReferentielMapper.mapToModels(refTypeDocumentRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListTypesAuteurCanevas() {
        return acteurReferentielMapper.mapToModels(refAuteurRepository.findAll()
                .stream().filter(refAuteurEntity -> !"EDITEUR_SURCHARGE".equals(refAuteurEntity.getCodeExterne()))
                .collect(Collectors.toList())
        );
    }

    @Override
    public List<ReferentielRepresentation> findListTypesAuteurClauses() {
        return acteurReferentielMapper.mapToModels(refAuteurRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListTypesContrat() {
        return refTypeContratEditeurMapper.mapToModels(refTypeContratRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListThemesClause() {
        return themeClauseReferentielMapper.mapToModels(refThemeClauseRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListTypesClause() {
        return typeClauseReferentielMapper.mapToModels(refTypeClauseRepository.findAll());
    }

    @Override
    public List<ReferentielRepresentation> findListReferenceHeritee() {
        return valeurTypeClauseReferentielMapper.mapToModels(refValeurTypeClauseRepository.findAll());
    }
}
