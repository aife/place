package com.atexo.redaction.domain.agent.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.domain.agent.model.Agent;
import com.atexo.redaction.domain.organisme.mapper.OrganismeMapper;
import com.atexo.redaction.domain.referentiel.mapper.ReferentialMpeMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class, OrganismeMapper.class, ReferentialMpeMapper.class})
public interface AgentMapper extends DaoMapper<AgentEntity, Agent> {


}
