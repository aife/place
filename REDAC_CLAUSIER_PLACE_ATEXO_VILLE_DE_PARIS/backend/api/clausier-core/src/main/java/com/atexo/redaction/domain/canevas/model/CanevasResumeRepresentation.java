package com.atexo.redaction.domain.canevas.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CanevasResumeRepresentation {

    private int id;

    private int idCanevas;

    private Integer idPublication;

    private Integer idLastPublication;

    private Integer idOrganisme;

    private String referenceCanevas;

    private String referenceClause;

    private String titre;

    private String lastVersion;

    private int idTypeDocument;

    private String labelTypeDocument;

    private List<Integer> idsTypeContrats;

    private List<String> labelsTypeContrats;

    private List<Integer> idsProcedures;

    private List<String> labelsProcedures;

    private int idNaturePrestation;

    private String labelNaturePrestation;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateCreation;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date dateModification;

    private Boolean actif;

    private int etat;

    private int idRefCCAG;

    private int idStatutRedactionClausier;

    private int typeAuteur;

    private String labelTypeAuteur;

    private boolean canevasEditeur;

    private boolean compatibleEntiteAdjudicatrice;


}
