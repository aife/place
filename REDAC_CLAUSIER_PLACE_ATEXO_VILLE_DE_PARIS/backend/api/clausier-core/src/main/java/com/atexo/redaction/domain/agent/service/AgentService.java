package com.atexo.redaction.domain.agent.service;


import com.atexo.redaction.config.DaoAdapter;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import com.atexo.redaction.dao.agent.repository.AgentRepository;
import com.atexo.redaction.dao.agent.repository.SessionAgentRepository;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.redaction.entity.ProduitEntity;
import com.atexo.redaction.dao.redaction.repository.ProduitRepository;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import com.atexo.redaction.domain.agent.exception.AgentException;
import com.atexo.redaction.domain.agent.exception.AgentExceptionEnum;
import com.atexo.redaction.domain.agent.mapper.AgentMapper;
import com.atexo.redaction.domain.agent.mapper.AgentMpeMapper;
import com.atexo.redaction.domain.agent.mapper.StringAndListStringMapper;
import com.atexo.redaction.domain.agent.model.*;
import com.atexo.redaction.domain.document.model.Produit;
import com.atexo.redaction.domain.organisme.service.OrganismeService;
import com.atexo.redaction.domain.referentiel.service.ReferentielMpeService;
import com.atexo.redaction.ws.keycloak.model.Token;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@Component
public class AgentService extends DaoAdapter<String, AgentEntity, Agent, AgentRepository, AgentMapper> {

    private final OrganismeService organismeService;
    private final SessionAgentRepository sessionAgentRepository;
    private final ProduitRepository produitRepository;
    private final ReferentielMpeService referentielMpeService;
    private final AgentMpeMapper agentMpeMapper;
    private final StringAndListStringMapper stringAndListStringMapper;

    private String format = "L'utilisateur avec id [%s] n'existe pas";

    public AgentService(AgentMapper mapper, AgentRepository repository, OrganismeService organismeService, SessionAgentRepository sessionAgentRepository, ProduitRepository produitRepository, ReferentielMpeService referentielMpeService, AgentMpeMapper agentMpeMapper, StringAndListStringMapper stringAndListStringMapper) {
        super(mapper, repository);
        this.organismeService = organismeService;
        this.sessionAgentRepository = sessionAgentRepository;
        this.produitRepository = produitRepository;
        this.referentielMpeService = referentielMpeService;
        this.agentMpeMapper = agentMpeMapper;
        this.stringAndListStringMapper = stringAndListStringMapper;
    }


    public String saveAgentIfNotExist(Mpe mpe, String token) throws AgentException {
        try {

            if (mpe == null || mpe.getEnvoi() == null || mpe.getEnvoi().getAgent() == null || mpe.getEnvoi().getAgent().getAcronymeOrganisme() == null || mpe.getEnvoi().getAgent().getOrganisme() == null || mpe.getEnvoi().getAgent().getPlateforme() == null) {
                throw new AgentException(AgentExceptionEnum.SAVE_ERROR, "La plateforme/organisme ne doivent pas être null");
            }

            AgentType agentType = mpe.getEnvoi().getAgent();


            EpmTRefOrganisme organisme = EpmTRefOrganisme.builder().codeExterne(agentType.getOrganisme().getAcronyme()).libelle(agentType.getOrganisme().getDenominationOrganisme()).actif(true).logoActif(false).plateformeUuid(agentType.getPlateforme()).build();
            organisme = organismeService.savePlateformeOrganismeIfNotExist(organisme);

            var identifiant = agentType.getIdentifiant();
            var idOrganisme = organisme.getId();
            log.info("Récupération agent : identifiant = {}, id organisme = {}", identifiant, idOrganisme);
            AgentEntity savedAgent = repository.findByIdentifiantAndOrganismeId(identifiant, idOrganisme).orElse(null);

            if (agentType.getOrganisme() == null) {
                agentType.setOrganisme(new OrganismeType());
            }
            agentType.getOrganisme().setId(idOrganisme);

            if (savedAgent != null) {
                agentType.setIdContexte(savedAgent.getId());
            } else {
                savedAgent = agentMpeMapper.mapToEntity(mpe);
                savedAgent.setOrganisme(organisme);
            }

            ReferentielMpeEntity service = null;
            ServiceType typeService = agentType.getService();
            if (typeService != null) {
                ReferentielMpeEntity service1 = ReferentielMpeEntity.builder().libelle(typeService.getLibelle()).type("SERVICE").organisme(organisme).code(String.valueOf(typeService.getId())).build();
                service = referentielMpeService.getReferential(service1);

            }
            try {
                savedAgent.setService(service);
                savedAgent.setEnvoiMpe(new ObjectMapper().writeValueAsString(mpe));
                savedAgent.setRoles(stringAndListStringMapper.mapToEntity(agentMpeEnvoiAgentHabilitationsHabilitation(mpe)));
                ServeurApiType api = mpe.getEnvoi().getAgent().getApi();
                if (api != null) {
                    savedAgent.setMpeToken(api.getToken());
                    savedAgent.setMpeUrl(api.getUrl());
                    savedAgent.setMpeRefreshToken(api.getRefreshToken());
                }

            } catch (JsonProcessingException e) {
                throw new AgentException(AgentExceptionEnum.MPE_ENVOI, e.getMessage());
            }

            savedAgent = repository.save(savedAgent);
            if (CollectionUtils.isEmpty(savedAgent.getSessions()) || savedAgent.getSessions().stream().noneMatch(sessionAgentEntity -> sessionAgentEntity.getToken().equals(token))) {
                String produits = null;
                if (mpe.getEnvoi().getAgent().getProduits() != null) {
                    List<Produit> produit = mpe.getEnvoi().getAgent().getProduits().getProduit();
                    if (CollectionUtils.isEmpty(produit)) {
                        produit = new ArrayList<>();
                    }
                    List<ProduitEntity> allByActifIsTrue = produitRepository.findAllByActifIsTrueAndAlwaysShowIsTrue();
                    if (!CollectionUtils.isEmpty(allByActifIsTrue)) {
                        List<Produit> finalProduit = produit;
                        produit.addAll(allByActifIsTrue.stream().filter(produitEntity -> finalProduit.stream()
                                .noneMatch(produit1 -> produit1.getNom().equals(produitEntity.getNom()))).map(produitEntity -> {
                            Produit produitToAdd = new Produit();
                            produitToAdd.setNom(produitEntity.getNom());
                            produitToAdd.setUrl(produitEntity.getDefaultUrl());
                            return produitToAdd;
                        }).collect(Collectors.toList()));
                    }
                    produits = new ObjectMapper().writeValueAsString(produit);
                }
                sessionAgentRepository.save(SessionAgentEntity.builder().agent(savedAgent).produits(produits).token(token).build());
            }

            return savedAgent.getId();

        } catch (Exception e) {
            throw new AgentException(AgentExceptionEnum.SAVE_ERROR, e.getMessage(), e);
        }
    }


    public Agent getAgent(AgentEntity agent, SessionAgentEntity sessionAgentEntity) {
        Agent model = mapper.mapToModel(agent);
        if (model == null) {
            throw new AgentException(AgentExceptionEnum.NOT_FOUND, String.format(format, agent.getId()));
        }
        if (sessionAgentEntity != null && sessionAgentEntity.getProduits() != null) {
            try {
                model.setProduits(List.of(new ObjectMapper().readValue(sessionAgentEntity.getProduits(), Produit[].class)));
            } catch (Exception e) {
                log.error("Erreur lors de la récupération des produits", e);
            }
        }
        return model;
    }

    @Scheduled(cron = "0 0 * * * *")
    public void refreshToken() {
        Pageable page = Pageable.ofSize(100).withPage(0);
        Page<SessionAgentEntity> all;
        do {
            all = sessionAgentRepository.findAll(page);
            all.forEach(sessionAgentEntity -> {
                Token token = Token.builder().accessToken(sessionAgentEntity.getToken()).build();
                Instant now = Instant.now();
                if (now.isAfter(token.getExpiresTime())) {
                    sessionAgentRepository.delete(sessionAgentEntity);
                }

            });
            page = page.next();
        } while (!all.isLast());
    }

    private List<String> agentMpeEnvoiAgentHabilitationsHabilitation(Mpe mpe) {
        if (mpe == null) {
            return null;
        }
        Mpe.Envoi envoi = mpe.getEnvoi();
        if (envoi == null) {
            return null;
        }
        AgentType agent = envoi.getAgent();
        if (agent == null) {
            return null;
        }
        AgentType.Habilitations habilitations = agent.getHabilitations();
        if (habilitations == null) {
            return null;
        }
        List<String> habilitation = habilitations.getHabilitation();
        if (habilitation == null) {
            return null;
        }
        return habilitation;
    }
}

