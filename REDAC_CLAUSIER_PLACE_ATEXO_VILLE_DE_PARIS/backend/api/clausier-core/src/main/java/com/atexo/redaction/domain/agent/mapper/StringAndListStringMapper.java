package com.atexo.redaction.domain.agent.mapper;

import org.mapstruct.Mapper;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Mapper(componentModel = "spring")
public interface StringAndListStringMapper {

    default String mapToEntity(List<String> model) {
        return Objects.nonNull(model) ? String.join(",", model) : "";
    }

    default List<String> mapToModel(String entity) {
        return StringUtils.hasText(entity) ? Arrays.asList(entity.split(",")) : Collections.emptyList();
    }
}
