package com.atexo.redaction.domain.referentiel.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReferentielRepresentation {

    private int id;

    private String uid;

    private String label;

    private boolean actif;

    private String shortLabel;

    private String externalCode;
}
