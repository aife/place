package com.atexo.redaction.domain.produit.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Produit {

    private Integer id;

    private String libelle;

    private String code;

    private String defaultUrl;

    private String nom;

    private String wsChampsFusion;

    private String version;

    private Boolean actif;

}
