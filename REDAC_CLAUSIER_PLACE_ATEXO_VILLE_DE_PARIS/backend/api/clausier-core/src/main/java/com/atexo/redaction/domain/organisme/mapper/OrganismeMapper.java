package com.atexo.redaction.domain.organisme.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.domain.organisme.model.Organisme;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrganismeMapper extends DaoMapper<EpmTRefOrganisme, Organisme> {

}
