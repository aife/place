package com.atexo.redaction.domain.espace_documentaire.service;


import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.domain.document.model.EditionRequest;
import com.atexo.redaction.ws.docgen.model.DocumentEditorRequest;
import com.atexo.redaction.ws.docgen.model.FileStatus;

import java.io.File;

public interface EspaceDocumentaireService {

    EditionRequest getUrlEdition(String editionToken);
    String getEditionToken(File file, DocumentEditorRequest request);

    DocumentAdministrableEntity updateDocumentEdition(FileStatus status, DocumentAdministrableEntity documentAdministrable);

    DocumentAdministrableEntity suiviDocument(DocumentAdministrableEntity documentAdministrable);
}
