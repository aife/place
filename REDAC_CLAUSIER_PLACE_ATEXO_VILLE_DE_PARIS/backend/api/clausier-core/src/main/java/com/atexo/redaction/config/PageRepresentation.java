package com.atexo.redaction.config;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public final class PageRepresentation<T> {
    private boolean first;
    private boolean last;
    private int totalPages;
    private long totalElements;
    private int numberOfElements;
    private int size;
    private int number;
    private List<T> content;

}
