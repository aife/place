package com.atexo.redaction.domain.agent.mapper;

import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.domain.agent.model.Mpe;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {StringAndListStringMapper.class})
public interface AgentMpeMapper {

    @Mapping(target = "id", source = "envoi.agent.idContexte")
    @Mapping(target = "identifiant", source = "envoi.agent.identifiant")
    @Mapping(target = "nom", source = "envoi.agent.nom")
    @Mapping(target = "prenom", source = "envoi.agent.prenom")
    @Mapping(target = "acheteurPublic", source = "envoi.agent.nomCourantAcheteurPublic")
    @Mapping(target = "roles", source = "envoi.agent.habilitations.habilitation")
    @Mapping(target = "organisme.codeExterne", source = "envoi.agent.organisme.acronyme")
    @Mapping(target = "organisme.libelle", source = "envoi.agent.organisme.denominationOrganisme")
    @Mapping(target = "mpeUrl", source = "envoi.agent.api.url")
    @Mapping(target = "mpeToken", source = "envoi.agent.api.token")
    @Mapping(target = "mpeRefreshToken", source = "envoi.agent.api.refreshToken")
    AgentEntity mapToEntity(Mpe agentMpe);
}
