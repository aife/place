package com.atexo.redaction.domain.referentiel.service;

import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;

import java.util.List;

public interface RedactionReferentielService {


    List<ReferentielRepresentation> findListStatutsRedactionClausier();

    List<ReferentielRepresentation> findListTypesDocument();

    List<ReferentielRepresentation> findListTypesAuteurCanevas();
    List<ReferentielRepresentation> findListTypesAuteurClauses();

    List<ReferentielRepresentation> findListTypesContrat();

    List<ReferentielRepresentation> findListThemesClause();

    List<ReferentielRepresentation> findListTypesClause();

    List<ReferentielRepresentation> findListReferenceHeritee();
}
