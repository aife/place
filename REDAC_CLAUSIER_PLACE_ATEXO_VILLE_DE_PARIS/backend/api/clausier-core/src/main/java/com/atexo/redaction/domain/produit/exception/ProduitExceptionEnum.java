package com.atexo.redaction.domain.produit.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ProduitExceptionEnum {
    NOT_FOUND(404, "Produit non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement du produit"),
    MANDATORY(400, "Champs obligatoires"), NOT_AUTHORIZED(401, "Utilisateur non autorisé"), FUNCTIONAL_ERROR(500, "Erreur");


    private final int code;
    private final String type;

}
