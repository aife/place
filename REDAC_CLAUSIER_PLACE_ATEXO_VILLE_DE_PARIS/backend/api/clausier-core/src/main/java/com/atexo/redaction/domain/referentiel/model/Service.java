package com.atexo.redaction.domain.referentiel.model;

import com.atexo.redaction.domain.organisme.model.Organisme;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Service {

    private Service parent;

    private Organisme organisme;

    private Number identifiantExterne;

    private String libelle;

    private String codeExterne;


}
