package com.atexo.redaction.config;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.config.querybean.SearchCriteriaQuery;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DaoAdapter<I, E, M, R extends DaoRepository<E, I>, D extends DaoMapper<E, M>> {

    protected D mapper;
    protected R repository;

    public DaoAdapter(D mapper, R repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public M findById(I idEntity) {
        return mapper.mapToModel(repository.findById(idEntity).orElse(null));
    }

    public E save(E entity) {
        return entity == null ? null : repository.save(entity);
    }

    public List<E> saveAll(List<E> list) {
        final List<E> models = new ArrayList<>();
        repository.saveAll(list).forEach(models::add);
        return models;
    }

    public List<M> findAllByCriteria(SearchCriteriaQuery searchCriteria) {
        return mapper.
                mapToModels(repository.findAllByCriteria(searchCriteria));

    }

    public List<M> findAll() {
        return mapper.
                mapToModels((Collection<E>) repository.findAll());

    }

    public PageRepresentation<M> findAllByCriteria(Pageable pageRequest, SearchCriteriaQuery searchCriteria) {
        return mapper.mapToPageModel(
                repository.findAllByCriteria(searchCriteria, pageRequest));
    }

    public boolean deleteById(I id) {
        if (this.repository.existsById(id))
            this.repository.deleteById(id);
        return true;
    }
}
