package com.atexo.redaction.domain.agent.model;

import com.atexo.redaction.domain.document.model.Produit;
import com.atexo.redaction.domain.organisme.model.Organisme;
import com.atexo.redaction.domain.referentiel.model.Service;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Agent {
    private String id;

    private String identifiant;

    private String nom;

    private String prenom;

    private Organisme organisme;

    private String acheteurPublic;

    private List<String> roles;

    private List<Produit> produits;

    private ZonedDateTime datePremierAcces;

    private ZonedDateTime dateDernierAcces;

    private String mpeUrl;

    private Service service;

}
