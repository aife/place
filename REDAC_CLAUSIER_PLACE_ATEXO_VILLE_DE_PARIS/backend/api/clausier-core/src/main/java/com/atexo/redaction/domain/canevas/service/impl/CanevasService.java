package com.atexo.redaction.domain.canevas.service.impl;

import com.atexo.redaction.config.PageRepresentation;
import com.atexo.redaction.dao.redaction.entity.CanevasEntity;
import com.atexo.redaction.dao.redaction.repository.CanevasRepository;
import com.atexo.redaction.dao.redaction.specification.RechercheCanevasSpecification;
import com.atexo.redaction.domain.canevas.mapper.CanevasRepresentationMapper;
import com.atexo.redaction.domain.canevas.model.CanevasResumeRepresentation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class CanevasService implements com.atexo.redaction.domain.canevas.service.CanevasService {

    private final CanevasRepository canevasRepository;
    private final CanevasRepresentationMapper canevasRepresentationMapper;

    public CanevasService(CanevasRepository canevasRepository, CanevasRepresentationMapper canevasRepresentationMapper) {
        this.canevasRepository = canevasRepository;
        this.canevasRepresentationMapper = canevasRepresentationMapper;
    }

    @Override
    public PageRepresentation<CanevasResumeRepresentation> search(RechercheCanevasSpecification search, Pageable pageable) {
        Page<CanevasEntity> page = canevasRepository.findAll(search, pageable);
        return canevasRepresentationMapper.mapToPageModel(page);
    }
}
