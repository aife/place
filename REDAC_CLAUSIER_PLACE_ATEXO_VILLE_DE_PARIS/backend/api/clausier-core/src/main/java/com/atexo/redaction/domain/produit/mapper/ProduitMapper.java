package com.atexo.redaction.domain.produit.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.redaction.entity.ProduitEntity;
import com.atexo.redaction.domain.produit.model.Produit;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProduitMapper extends DaoMapper<ProduitEntity, Produit> {

}
