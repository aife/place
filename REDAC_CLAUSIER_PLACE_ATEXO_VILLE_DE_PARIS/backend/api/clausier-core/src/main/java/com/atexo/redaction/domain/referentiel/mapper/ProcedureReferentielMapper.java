package com.atexo.redaction.domain.referentiel.mapper;

import com.atexo.redaction.config.DaoMapper;
import com.atexo.redaction.dao.referentiel.entity.RefCcagEntity;
import com.atexo.redaction.dao.referentiel.entity.RefProcedureEntity;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProcedureReferentielMapper extends DaoMapper<RefProcedureEntity, ReferentielRepresentation> {

    @Override
    @Mapping(source = "libelle", target = "label")
    @Mapping(source = "libelleCourt", target = "shortLabel")
    @Mapping(source = "codeExterne", target = "uid")
    @Mapping(source = "codeExterne", target = "externalCode")
    ReferentielRepresentation mapToModel(RefProcedureEntity entity);
}
