package com.atexo.redaction.domain.referentiel.service;

import com.atexo.redaction.domain.referentiel.model.RefSurchargeLibelle;
import com.atexo.redaction.domain.referentiel.model.ReferentielRepresentation;

import java.util.List;
import java.util.Map;

public interface ReferentielService {
    List<ReferentielRepresentation> findListNatures();

    List<ReferentielRepresentation> findListRefCCAG();

    List<ReferentielRepresentation> findListProcedures(Integer idOrganisme);

    Map<String, String>  getSurchargesLibelles();

}
