package com.atexo.redaction.domain.organisme.service;


import com.atexo.redaction.config.DaoAdapter;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.organisme.repository.OrganismeRepository;
import com.atexo.redaction.domain.organisme.mapper.OrganismeMapper;
import com.atexo.redaction.domain.organisme.model.Organisme;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class OrganismeService extends DaoAdapter<Integer, EpmTRefOrganisme, Organisme, OrganismeRepository, OrganismeMapper> {

    public OrganismeService(OrganismeMapper organismeMapper, OrganismeRepository organismeRepository) {
        super(organismeMapper, organismeRepository);

    }


    public EpmTRefOrganisme savePlateformeOrganismeIfNotExist(EpmTRefOrganisme organisme) {
        if (organisme == null) {
            return null;
        }

        String codeExterne = organisme.getCodeExterne();
        String plateformeUuid = organisme.getPlateformeUuid();
        log.info("Récupération organisme : organisme = {} / plateformeUuid {}", codeExterne, plateformeUuid);

        Optional<EpmTRefOrganisme> savedOrganismeOptional = repository.findByCodeExterneAndPlateformeUuid(codeExterne, plateformeUuid);
        return savedOrganismeOptional.orElseGet(() -> {
            List<EpmTRefOrganisme> organismes = repository.findByCodeExterneAndPlateformeUuidIsNull(codeExterne);
            if (CollectionUtils.isEmpty(organismes))
                return this.save(organisme);
            var epmTRefOrganisme = organismes.get(0);
            epmTRefOrganisme.setPlateformeUuid(plateformeUuid);
            epmTRefOrganisme.setActif(true);
            return this.save(epmTRefOrganisme);
        });
    }

    public EpmTRefOrganisme getOrganisme(String codeExterne, String plateformeUuid) {
        if (codeExterne == null || plateformeUuid == null) {
            return null;
        }

        log.info("Récupération organisme : organisme = {} / plateformeUuid {}", codeExterne, plateformeUuid);

        Optional<EpmTRefOrganisme> savedOrganismeOptional = repository.findByCodeExterneAndPlateformeUuid(codeExterne, plateformeUuid);
        return savedOrganismeOptional.orElseGet(() -> {
            List<EpmTRefOrganisme> organismes = repository.findByCodeExterneAndPlateformeUuidIsNull(codeExterne);
            if (CollectionUtils.isEmpty(organismes))
                return null;
            var epmTRefOrganisme = organismes.get(0);
            epmTRefOrganisme.setPlateformeUuid(plateformeUuid);
            epmTRefOrganisme.setActif(true);
            return this.save(epmTRefOrganisme);
        });
    }

}
