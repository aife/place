package com.atexo.redaction.domain.document.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DocumentAdministrableExceptionEnum {
    NOT_FOUND(404, "Document administrable non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement du Document administrable"),
    MANDATORY(400, "Champs obligatoires"), NOT_AUTHORIZED(401, "Utilisateur non autorisé"), API_ERROR(500, "Erreur lors de l'appel à docgen"), FUNCTIONAL_ERROR(500, "Erreur");


    private final int code;
    private final String type;

}
