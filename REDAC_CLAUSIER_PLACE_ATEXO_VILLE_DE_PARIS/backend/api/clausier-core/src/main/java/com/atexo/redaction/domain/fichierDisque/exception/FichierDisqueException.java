package com.atexo.redaction.domain.fichierDisque.exception;

import com.atexo.redaction.config.exception.AtexoException;
import lombok.Getter;

@Getter
public class FichierDisqueException extends AtexoException {

    public FichierDisqueException(FichierDisqueExceptionEnum fichierDisqueExceptionEnum, String errorMessage, Throwable err) {
        super(errorMessage, fichierDisqueExceptionEnum.getCode(), fichierDisqueExceptionEnum.getType(), err);

    }

    public FichierDisqueException(FichierDisqueExceptionEnum fichierDisqueExceptionEnum, String errorMessage) {
        super(errorMessage, fichierDisqueExceptionEnum.getCode(), fichierDisqueExceptionEnum.getType());

    }
}
