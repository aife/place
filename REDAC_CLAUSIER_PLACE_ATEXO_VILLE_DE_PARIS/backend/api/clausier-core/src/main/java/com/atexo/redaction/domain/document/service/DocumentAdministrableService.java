package com.atexo.redaction.domain.document.service;

import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import com.atexo.redaction.dao.redaction.repository.DocumentAdministrableRepository;
import com.atexo.redaction.dao.redaction.repository.FichierDisqueRepository;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import com.atexo.redaction.dao.referentiel.repository.ReferentielMpeRepository;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableException;
import com.atexo.redaction.domain.document.exception.DocumentAdministrableExceptionEnum;
import com.atexo.redaction.domain.document.mapper.DocumentAdministrableMapper;
import com.atexo.redaction.domain.document.mapper.ReferentielDocumentAdministrableMapper;
import com.atexo.redaction.domain.document.model.*;
import com.atexo.redaction.domain.espace_documentaire.service.EspaceDocumentaireService;
import com.atexo.redaction.domain.fichierDisque.mapper.FichierDisqueMapper;
import com.atexo.redaction.domain.fichierDisque.model.FichierDisque;
import com.atexo.redaction.domain.fichierDisque.service.FichierDisqueService;
import com.atexo.redaction.domain.organisme.service.OrganismeService;
import com.atexo.redaction.domain.produit.service.ProduitService;
import com.atexo.redaction.ws.docgen.DocgenWS;
import com.atexo.redaction.ws.docgen.model.*;
import com.atexo.redaction.ws.mpe.MpeWS;
import com.atexo.redaction.ws.mpe.model.ChampFusionResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DocumentAdministrableService {

    private final DocumentAdministrableRepository documentAdministrableRepository;
    private final DocumentAdministrableMapper documentAdministrableMapper;
    private final ReferentielDocumentAdministrableMapper referentielDocumentAdministrableMapper;
    private final FichierDisqueMapper fichierDisqueMapper;
    private final OrganismeService organismeService;
    private final ReferentielMpeRepository referentielMpeRepository;
    private final FichierDisqueService fichierDisqueService;
    private final ProduitService produitService;
    private final MpeWS mpeWS;
    private final DocgenWS docgenWS;
    private final EspaceDocumentaireService espaceDocumentaireService;
    private final String publicUrl;
    private final String plateformeName;

    public DocumentAdministrableService(DocumentAdministrableRepository documentAdministrableRepository, DocumentAdministrableMapper documentAdministrableMapper, ReferentielDocumentAdministrableMapper referentielDocumentAdministrableMapper, FichierDisqueMapper fichierDisqueMapper, FichierDisqueRepository fichierDisqueRepository, OrganismeService organismeService, ReferentielMpeRepository referentielMpeRepository, FichierDisqueService fichierDisqueService, ProduitService produitService, MpeWS mpeWS, DocgenWS docgenWS, EspaceDocumentaireService espaceDocumentaireService, @Value("${public-url}") String publicUrl, @Value("${plateforme}") String plateformeName) {

        this.documentAdministrableRepository = documentAdministrableRepository;
        this.documentAdministrableMapper = documentAdministrableMapper;
        this.referentielDocumentAdministrableMapper = referentielDocumentAdministrableMapper;
        this.fichierDisqueMapper = fichierDisqueMapper;
        this.organismeService = organismeService;
        this.referentielMpeRepository = referentielMpeRepository;
        this.fichierDisqueService = fichierDisqueService;
        this.produitService = produitService;
        this.mpeWS = mpeWS;
        this.docgenWS = docgenWS;
        this.espaceDocumentaireService = espaceDocumentaireService;
        this.publicUrl = publicUrl;
        this.plateformeName = plateformeName;

    }

    public List<DocumentAdministrable> getDocumentsAdministrables(List<String> produits, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service, boolean champsFusion) {
        return documentAdministrableMapper.mapToModels(this.getListDocumentsAdministrables(produits, plateformeUuid, organisme, service, champsFusion))
                .stream().filter(DocumentAdministrable::isActif).collect(Collectors.toList());
    }

    public List<DocumentAdministrableEntity> getListDocumentsAdministrables(List<String> produits, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service, boolean champsFusion) {
        try {
            List<String> produitsActifs = new ArrayList<>();
            List<String> produitsInBdd = produitService.getNomsProduitsActifs();
            if (!CollectionUtils.isEmpty(produits)) {
                for (String produit : produits) {
                    if (produitsInBdd.contains(produit.toUpperCase()) && !produitsActifs.contains(produit.toUpperCase())) {
                        produitsActifs.add(produit);
                    }
                }
            }
            if (produitsActifs.isEmpty()) {
                produitsActifs.addAll(produitsInBdd);
            }
            List<DocumentAdministrableEntity> completeList = new ArrayList<>();
            for (String produit : produitsActifs) {
                //récupération des documents plateformes et des documents atexo surchargés

                completeList.addAll(getDocumentProduit(plateformeUuid, organisme, service, champsFusion, produit));
            }

            return completeList.stream().sorted(Comparator.comparing(DocumentAdministrableEntity::getLibelle)).collect(Collectors.toList());
        } catch (Exception e) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.FUNCTIONAL_ERROR, "Erreur pendant la récupération des documents administrables");
        }

    }

    private List<DocumentAdministrableEntity> getDocumentProduit(String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service, boolean champsFusion, String produit) {
        List<DocumentAdministrableEntity> documentList = new ArrayList<>();

        if (service != null) {
            documentList = documentAdministrableRepository.findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeAndService(champsFusion, produit, organisme.getPlateformeUuid(), organisme, service);
        }

        if (organisme != null) {
            List<DocumentAdministrableEntity> documentOrganismes = documentAdministrableRepository.findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeAndServiceIsNull(champsFusion, produit, organisme.getPlateformeUuid(), organisme);
            for (DocumentAdministrableEntity documentAdministrable : documentOrganismes) {
                if (documentList.stream().noneMatch(documentAdministrableEntity -> documentAdministrableEntity.getCode().equals(documentAdministrable.getCode()))) {
                    documentList.add(documentAdministrable);
                }
            }

        }
        if (plateformeUuid != null) {
            List<DocumentAdministrableEntity> documentPlateformeList = documentAdministrableRepository.findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeIsNullAndServiceIsNull(champsFusion, produit, plateformeUuid);
            for (DocumentAdministrableEntity documentAdministrable : documentPlateformeList) {
                if (documentList.stream().noneMatch(documentAdministrableEntity -> documentAdministrableEntity.getCode().equals(documentAdministrable.getCode()))) {
                    documentList.add(documentAdministrable);
                }
            }
        }
        //récupération des documents atexo pas encore surchargés
        List<DocumentAdministrableEntity> documentAtexoList = documentAdministrableRepository.findAllByChampFusionAndProduitAndPlateformeUuidIsNull(champsFusion, produit);
        for (DocumentAdministrableEntity documentAdministrable : documentAtexoList) {
            if (documentList.stream().noneMatch(documentAdministrableEntity -> documentAdministrableEntity.getCode().equals(documentAdministrable.getCode()))) {
                documentList.add(documentAdministrable);
            }
        }
        return documentList.stream().sorted(Comparator.comparing(DocumentAdministrableEntity::getLibelle)).collect(Collectors.toList());
    }

    public DocumentAdministrableEntity getSurchargeDocumentAdministrable(String code, String produit, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service, boolean champsFusion) {
        try {
            Optional<DocumentAdministrableEntity> optional = Optional.empty();

            if (service != null) {
                optional = documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndService(code, champsFusion, produit, organisme.getPlateformeUuid(), organisme, service);
            }

            if (organisme != null && optional.isEmpty()) {
                optional = documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndServiceIsNull(code, champsFusion, produit, organisme.getPlateformeUuid(), organisme);

            }
            if (plateformeUuid != null && optional.isEmpty()) {
                optional = documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeIsNullAndServiceIsNull(code, champsFusion, produit, plateformeUuid);
            }
            if (optional.isEmpty()) {
                optional = documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidIsNullAndOrganismeIsNullAndServiceIsNull(code, champsFusion, produit);
            }

            return optional.orElse(null);
        } catch (Exception e) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.FUNCTIONAL_ERROR, "Erreur pendant la récupération du document administrable");
        }

    }

    public List<DocumentAdministrableEntity> getAllSurchargeDocumentsAdministrables(String code, String produit, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service, boolean champsFusion) {
        try {
            List<DocumentAdministrableEntity> list = new ArrayList<>();
            if (service != null && organisme != null) {
                documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndService(code, champsFusion, produit, organisme.getPlateformeUuid(), organisme, service)
                        .ifPresent(list::add);
            }

            if (organisme != null) {
                documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndServiceIsNull(code, champsFusion, produit, organisme.getPlateformeUuid(), organisme)
                        .ifPresent(list::add);
            }
            if (plateformeUuid != null) {
                documentAdministrableRepository.findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeIsNullAndServiceIsNull(code, champsFusion, produit, plateformeUuid)
                        .ifPresent(list::add);
            }


            return list;
        } catch (Exception e) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.FUNCTIONAL_ERROR, "Erreur pendant la récupération des documents administrables");
        }

    }


    public EditionRequest editDocument(SessionAgentEntity sessionAgentEntity, Long documentId, AgentEntity agent, Produit produit, Boolean surchargeOrganisme,
                                       Boolean surchargeService) {

        DocumentAdministrableEntity documentAdministrableEntity = documentAdministrableRepository.findById(documentId).orElse(null);
        if (documentAdministrableEntity == null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_FOUND, "Le document administrable n'existe pas");
        }
        EpmTRefOrganisme epmTRefOrganisme = null;
        ReferentielMpeEntity serviceMpe = null;
        String plateforme = agent.getOrganisme().getPlateformeUuid();
        if (Boolean.TRUE.equals(surchargeOrganisme)) {
            epmTRefOrganisme = agent.getOrganisme();
            if (Boolean.TRUE.equals(surchargeService)) serviceMpe = agent.getService();
        }


        DocumentAdministrableEntity toSave;
        if (documentAdministrableEntity.getPlateformeUuid() == null || (documentAdministrableEntity.getOrganisme() == null && epmTRefOrganisme != null) || (documentAdministrableEntity.getService() == null && serviceMpe != null)) {
            toSave = DocumentAdministrableEntity.builder().documentParent(documentAdministrableEntity).libelle(documentAdministrableEntity.getLibelle()).actif(true).agent(agent).service(serviceMpe).template(documentAdministrableEntity.getTemplate()).champFusion(documentAdministrableEntity.isChampFusion()).code(documentAdministrableEntity.getCode()).produit(documentAdministrableEntity.getProduit()).organisme(epmTRefOrganisme).fichier(documentAdministrableEntity.getFichier()).plateformeUuid(plateforme).build();
        } else {
            toSave = documentAdministrableEntity;
        }
        toSave = documentAdministrableRepository.save(toSave);
        return editPlateformeDocument(sessionAgentEntity, toSave, agent, produit, surchargeOrganisme, surchargeService);

    }

    private EditionRequest editPlateformeDocument(SessionAgentEntity sessionAgentEntity, DocumentAdministrableEntity documentAdministrableEntity, AgentEntity agent, Produit produit, Boolean surchargeOrganisme, Boolean surchargeService) {
        //récupération du fichier à éditer
        FichierDisqueEntity fichierDisque = documentAdministrableEntity.getFichier();
        File file = fichierDisqueService.loadFile(fichierDisque.getChemin());
        String endpoint = publicUrl + "/clausier-api/documents-administrables/" + documentAdministrableEntity.getCode() + "/editeur-en-ligne/callback?plateforme=" + agent.getOrganisme().getPlateformeUuid() +
                "&champFusion=" + documentAdministrableEntity.isChampFusion() + "&produit=" + produit.getNom();
        if (Boolean.TRUE.equals(surchargeOrganisme)) {
            endpoint += "&organisme=" + agent.getOrganisme().getCodeExterne();
        }
        if (Boolean.TRUE.equals(surchargeService)) {
            endpoint += "&organisme=" + agent.getOrganisme().getCodeExterne() + "&service=" + agent.getService().getCode();
        }
        //préparation de la requête pour récupérer le token d'édition
        DocumentEditorRequest request = getDocumentEditorRequest(sessionAgentEntity, agent, produit, documentAdministrableEntity, endpoint, fichierDisque);
        request.setDocumentId(documentAdministrableEntity.getId().toString() + documentAdministrableEntity.getFichier().getIdFichier());
        try {
            String editionToken = espaceDocumentaireService.getEditionToken(file, request);
            documentAdministrableEntity.setTokenEdition(editionToken);
            documentAdministrableEntity.setModifiePar(agent);
            documentAdministrableEntity.setStatutEdition(FileStatusEnum.REQUEST_TO_OPEN.name());
            documentAdministrableRepository.save(documentAdministrableEntity);
            return espaceDocumentaireService.getUrlEdition(editionToken);

        } catch (Exception e) {
            log.error("Erreur lors de l'appel à docgen : {}", e.getMessage());
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.API_ERROR, "Erreur lors d'un appel à docgen");
        }
    }


    private DocumentEditorRequest getDocumentEditorRequest(SessionAgentEntity sessionAgentEntity, AgentEntity agent, Produit produit, DocumentAdministrableEntity documentAdministrableEntity, String endpoint, FichierDisqueEntity fichierDisque) {
        User user = User.builder()
                .name(agent.getPrenom() + " " + agent.getNom())
                .roles(List.of("modification"))
                .id(agent.getId()).build();
        List<ChampFusion> champsFusions = new ArrayList<>();
        com.atexo.redaction.domain.produit.model.Produit produitBdd = produitService.getProduitBddByNom(produit.getNom());
        List<ChampFusionResponse> reponse;
        try {
            String token = null;
            if (produit.getNom().equalsIgnoreCase("MPE")) {
                token = agent.getMpeToken();
            }
            String url = produit.getUrl();
            if (!StringUtils.hasText(url)) {
                url = produitBdd.getDefaultUrl();
            }
            reponse = this.mpeWS.getChampsFusionV2(url, token, produitBdd.getWsChampsFusion());
            champsFusions.addAll(reponse.stream().filter(champFusionResponse -> StringUtils.hasText(champFusionResponse.getChampFusion()) && StringUtils.hasText(champFusionResponse.getLibelle())).map(champFusionResponse -> ChampFusion.builder().simple(champFusionResponse.getSimple() == null || Boolean.TRUE.equals(champFusionResponse.getSimple())).libelle(champFusionResponse.getLibelle()).infoBulle(champFusionResponse.getInfoBulle()).type(champFusionResponse.getType()).collection(champFusionResponse.getCollection() == null || Boolean.TRUE.equals(champFusionResponse.getCollection())).champsFusion(champFusionResponse.getChampFusion()).build()).collect(Collectors.toList()));
        } catch (Exception e) {
            log.error("Erreur lors de la récupération des champs fusion : {}", e.getMessage());
        }

        //cas d'un document administrable: ajouter les champs de fusions complexes
        if (!documentAdministrableEntity.isChampFusion()) {
            champsFusions.addAll(addChampsComplexes(produit, agent));
        }
        DocumentAdministrableConfiguration config = DocumentAdministrableConfiguration.builder()
                .champsFusions(champsFusions)
                .build();

        return DocumentEditorRequest.builder()
                .mode("template")
                .documentTitle(documentAdministrableEntity.getLibelle())
                .plateformeId(agent.getOrganisme().getPlateformeUuid() + "@" + plateformeName)
                .callback(endpoint)
                .headersCallback(Map.of("Authorization", "Bearer " + sessionAgentEntity.getToken()))
                .user(user)
                .documentAdministrableConfiguration(config).build();
    }

    private List<ChampFusion> addChampsComplexes(Produit produit, AgentEntity agent) {
        List<DocumentAdministrableEntity> complexes = this.getListDocumentsAdministrables(List.of(produit.getNom()), agent.getOrganisme().getPlateformeUuid(), agent.getOrganisme(), agent.getService(), true);
        return complexes.stream().filter(DocumentAdministrableEntity::isActif).map(document -> {
            String blocUrl = publicUrl + "/clausier-api/documents-modeles/" + document.getCode() + "/download?produit=" + produit.getNom() + "&champsFusion=true";
            blocUrl += "&plateforme=" + agent.getOrganisme().getPlateformeUuid();

            if (document.getOrganisme() != null) {
                blocUrl += "&organisme=" + document.getOrganisme().getCodeExterne();
            }
            if (document.getService() != null) {
                blocUrl += "&service=" + document.getService().getCode();
            }
            return ChampFusion.builder()
                    .simple(false)
                    .blocUrl(blocUrl)
                    .libelle(document.getLibelle())
                    .build();

        }).collect(Collectors.toList());
    }


    public DocumentAdministrable saveDocumentAdministrable(DocumentAdministrable documentAdministrable, AgentEntity agent, MultipartFile file, Boolean surchargeOrganisme, Boolean surchargeService, String token, String extension) throws IOException {
        if (extension == null)
            extension = "docx";
        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            extension = FilenameUtils.getExtension(originalFilename);
        }
        if (!extension.equals("docx") && !extension.equals("xlsx")) {
            log.error("Erreur lors de l'enregistrement du document administrable : le fichier doit être un docx ou un xlsx");
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Le fichier doit être un docx ou un xlsx");
        }
        Date now = new Date();
        boolean isFileVide;
        FichierDisque fichierToSave = null;
        InputStream inputStream = null;
        if (file != null) {
            isFileVide = false;
            fichierToSave = FichierDisque.builder()
                    .nom(file.getOriginalFilename())
                    .dateSauvegarde(now)
                    .taille(file.getSize())
                    .build();
            inputStream = file.getInputStream();
        } else {
            //création d'un docx
            isFileVide = true;
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            if (extension.equals("docx")) {
                try (XWPFDocument doc = new XWPFDocument()) {
                    doc.write(b);
                }

            } else {
                try (XSSFWorkbook workbook = new XSSFWorkbook()) {
                    workbook.write(b);
                }
            }
            inputStream = new ByteArrayInputStream(b.toByteArray());
            fichierToSave = FichierDisque.builder()
                    .nom(documentAdministrable.getCode() + "." + extension)
                    .dateSauvegarde(now)
                    .taille((long) b.size())
                    .build();
            fichierToSave = fichierDisqueService.save(fichierToSave, inputStream);
        }

        EpmTRefOrganisme organisme = agent.getOrganisme();
        String plateformeUuid = organisme.getPlateformeUuid();
        ReferentielMpeEntity service = agent.getService();
        if (Boolean.TRUE.equals(surchargeService) && service == null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "L'agent n'a pas de service");
        }
        DocumentAdministrableEntity saved = getSurchargeDocumentAdministrable(documentAdministrable.getCode(), documentAdministrable.getProduit(), plateformeUuid, organisme, service, documentAdministrable.isChampFusion());

        if (saved != null && saved.isActif()) {
            showErrorDocAlreadyExist(surchargeOrganisme, surchargeService, saved);
        }

        FichierDisque fichierDisque = fichierDisqueService.save(fichierToSave, inputStream);
        documentAdministrable.setFichierId(fichierDisque.getIdFichier());

        documentAdministrable.setPlateformeUuid(plateformeUuid);
        if (Boolean.TRUE.equals(surchargeOrganisme)) {
            documentAdministrable.setOrganismeId(organisme.getId());
        }
        if (Boolean.TRUE.equals(surchargeService)) {
            documentAdministrable.setOrganismeId(organisme.getId());
            documentAdministrable.setServiceId(service.getId());
        }
        if (saved != null && !saved.isActif()) {
            reactiveDoc(documentAdministrable, agent, saved, fichierDisque, token, now, isFileVide);
            try {
                return documentAdministrableMapper.mapToModel(documentAdministrableRepository.save(saved));
            } catch (Exception e) {
                throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Erreur pendant la réactivation du document");
            }
        } else {

            DocumentAdministrableEntity toSave = prepareNewDoc(documentAdministrable, agent);
            FichierDisqueEntity fichierDisqueEntity = fichierDisqueMapper.mapToEntity(fichierDisque);

            if (file != null && extension.equals("docx")) {
                //récupérer un template de docgen à partir du fichier joint
                setDocgenTemplate(token, now, toSave, fichierDisqueEntity);
            } else {
                //pas de fichier joint, mettre en template un docx vide
                toSave.setTemplate(fichierDisqueEntity);
            }
            try {
                return documentAdministrableMapper.mapToModel(documentAdministrableRepository.save(toSave));
            } catch (Exception e) {
                throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Erreur pendant l'enregistrement du nouveau document");
            }

        }


    }

    private void setDocgenTemplate(String token, Date now, DocumentAdministrableEntity toSave, FichierDisqueEntity fichierDisqueEntity) throws IOException {
        File fileSaved = fichierDisqueService.loadFile(fichierDisqueEntity.getChemin());
        Resource template = null;
        try {
            template = docgenWS.getTemplate(fileSaved, token);
        } catch (Exception e) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Erreur pendant l'appel à docgen pour creation du template");
        }

        String nomModele = "template.docx";
        if (fichierDisqueEntity.getNom() != null && fichierDisqueEntity.getNom().contains(".")) {
            String[] tab = fichierDisqueEntity.getNom().split("\\.");
            nomModele = tab[0] + "_template.docx";
        }
        FichierDisque templateToSave = FichierDisque.builder()
                .nom(nomModele)
                .dateSauvegarde(now)
                .taille(template.contentLength())
                .build();
        FichierDisque templateSaved = fichierDisqueService.save(templateToSave, template.getInputStream());
        toSave.setTemplate(fichierDisqueMapper.mapToEntity(templateSaved));
    }

    private static void showErrorDocAlreadyExist(Boolean surchargeOrganisme, Boolean surchargeService, DocumentAdministrableEntity saved) {
        if (Boolean.TRUE.equals(surchargeService) && saved.getService() != null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Le document administrable est déjà surchargé par un service");
        }
        if (Boolean.TRUE.equals(surchargeOrganisme) && saved.getOrganisme() != null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Le document administrable est déjà surchargé par un organisme");
        }

        if (saved.getPlateformeUuid() != null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Le document administrable est déjà existant pour cette plateforme");
        }
    }

    private DocumentAdministrableEntity prepareNewDoc(DocumentAdministrable documentAdministrable, AgentEntity agent) {
        DocumentAdministrableEntity toSave = documentAdministrableMapper.mapToEntity(documentAdministrable);
        toSave.setProduit(documentAdministrable.getProduit().toUpperCase());
        toSave.setCreePar(agent);
        toSave.setModifiePar(agent);
        toSave.setActif(true);
        return toSave;
    }

    private void reactiveDoc(DocumentAdministrable documentAdministrable, AgentEntity agent, DocumentAdministrableEntity saved, FichierDisque fichierDisque, String token, Date now, boolean isFileVide) throws IOException {
        //reactiver le document enregistré et le mettre à jour
        saved.setActif(true);
        saved.setLibelle(documentAdministrable.getLibelle());
        saved.setFichier(fichierDisqueMapper.mapToEntity(fichierDisque));
        saved.setModifiePar(agent);
        saved.setTokenEdition(null);
        saved.setStatutEdition(null);
        FichierDisqueEntity fichierDisqueEntity = fichierDisqueMapper.mapToEntity(fichierDisque);
        if (!isFileVide && fichierDisqueEntity.getNom().endsWith(".docx")) {
            //récupérer un template de docgen à partir du fichier joint
            setDocgenTemplate(token, now, saved, fichierDisqueEntity);
        } else {
            //pas de fichier joint, mettre en template un docx/xlsx vide
            saved.setTemplate(fichierDisqueEntity);
        }
        //todo quelle gestion pour les éventuelles surcharges ?
    }


    public TrackDocumentResponse getDocumentCallback(String produit, String code, String plateforme, String organisme, String service, boolean champFusion, FileStatus status,
                                                     AgentEntity agent) {
        log.info("Réception du statut {} du document avec code {} / plateforme {} / organisme {} / service  {}", status.getStatus(), code, plateforme, organisme, service);
        EpmTRefOrganisme epmTRefOrganisme = null;
        ReferentielMpeEntity serviceMpe = null;
        if (organisme != null) {
            epmTRefOrganisme = organismeService.getOrganisme(organisme, plateforme);
            if (service != null && epmTRefOrganisme != null)
                serviceMpe = referentielMpeRepository.findByCodeAndTypeAndOrganismeId(service, "SERVICE", epmTRefOrganisme.getId()).orElse(null);
        }
        DocumentAdministrableEntity documentAdministrableEntity = getSurchargeDocumentAdministrable(code, produit, plateforme, epmTRefOrganisme, serviceMpe, champFusion);
        if (documentAdministrableEntity == null) {
            return TrackDocumentResponse.builder().error("1").build();
        }

        documentAdministrableEntity = espaceDocumentaireService.updateDocumentEdition(status, documentAdministrableEntity);
        documentAdministrableRepository.save(documentAdministrableEntity);
        return TrackDocumentResponse.builder().error("0").build();
    }


    @Scheduled(cron = "${external-apis.doc-gen.cron}")
    public void checkDocumentStatus() {
        List<String> status = List.of(FileStatusEnum.REQUEST_TO_OPEN.name(), FileStatusEnum.OPENED.name(), FileStatusEnum.EDITED_AND_SAVED.name());
        List<DocumentAdministrableEntity> documentAdministrableEntityList = documentAdministrableRepository.findByStatutEditionIn(status);
        documentAdministrableEntityList.forEach(documentAdministrable -> {
            DocumentAdministrableEntity entity = espaceDocumentaireService.suiviDocument(documentAdministrable);
            if (entity != null) {
                entity = documentAdministrableRepository.save(entity);
            }
        });

    }

    public DocumentAdministrableEntity getDocumentById(Long documentId) {
        return documentAdministrableRepository.findById(documentId).orElseThrow(() -> new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_FOUND, "Le document administrable n'existe pas"));
    }


    public ByteArrayResource getFile(String chemin) {
        return fichierDisqueService.loadFileAsResource(chemin);
    }


    public List<ReferentielDocumentAdministrable> getListDocumentsAdministrablesMPE(List<String> produits, String plateformeUuid, String organisme, String service, boolean champsFusion) {
        EpmTRefOrganisme epmTRefOrganisme = null;
        ReferentielMpeEntity serviceMpe = null;
        if (organisme != null) {
            epmTRefOrganisme = organismeService.getOrganisme(organisme, plateformeUuid);
            if (service != null && epmTRefOrganisme != null)
                serviceMpe = referentielMpeRepository.findByCodeAndTypeAndOrganismeId(service, "SERVICE", epmTRefOrganisme.getId()).orElse(null);
        }


        List<DocumentAdministrableEntity> listDocumentsAdministrables = getListDocumentsAdministrables(produits, plateformeUuid, epmTRefOrganisme, serviceMpe, champsFusion);

        return referentielDocumentAdministrableMapper.mapToModels(listDocumentsAdministrables.stream()
                .filter(documentAdministrable -> documentAdministrable.getTemplate() != null && documentAdministrable.isActif())
                .collect(Collectors.toList()));

    }

    public boolean deleteDocument(Long documentId, AgentEntity agent) {
        DocumentAdministrableEntity documentAdministrableEntity = documentAdministrableRepository.findById(documentId).orElseThrow(() -> new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_FOUND, "Le document administrable n'existe pas"));
        EpmTRefOrganisme organisme = agent.getOrganisme();
        List<DocumentAdministrableEntity> allSurchargeDocumentsAdministrables = getAllSurchargeDocumentsAdministrables(documentAdministrableEntity.getCode(), documentAdministrableEntity.getProduit(), organisme.getPlateformeUuid(), organisme, agent.getService(), documentAdministrableEntity.isChampFusion());
        if (allSurchargeDocumentsAdministrables.isEmpty()) {
            DocumentAdministrableEntity toSave = DocumentAdministrableEntity.builder()
                    .documentParent(documentAdministrableEntity)
                    .libelle(documentAdministrableEntity.getLibelle())
                    .actif(false)
                    .agent(agent)
                    .service(documentAdministrableEntity.getService())
                    .champFusion(documentAdministrableEntity.isChampFusion())
                    .code(documentAdministrableEntity.getCode())
                    .produit(documentAdministrableEntity.getProduit())
                    .organisme(documentAdministrableEntity.getOrganisme())
                    .fichier(documentAdministrableEntity.getFichier())
                    .plateformeUuid(organisme.getPlateformeUuid())
                    .creePar(agent)
                    .modifiePar(agent)
                    .build();
            allSurchargeDocumentsAdministrables.add(toSave);
        }

        allSurchargeDocumentsAdministrables
                .forEach(documentAdministrable -> {
                    documentAdministrableEntity.setModifiePar(agent);
                    documentAdministrable.setActif(false);
                });
        documentAdministrableRepository.saveAll(allSurchargeDocumentsAdministrables);
        return true;
    }

    public DocumentAdministrableEntity getDocumentByCodeAndPlateforme(String sigle, String produit, String plateformeUuid, String organisme, String service, Boolean champsFusion) {
        EpmTRefOrganisme epmTRefOrganisme = null;
        ReferentielMpeEntity serviceMpe = null;
        if (organisme != null) {
            epmTRefOrganisme = organismeService.getOrganisme(organisme, plateformeUuid);
            if (service != null && epmTRefOrganisme != null)
                serviceMpe = referentielMpeRepository.findByCodeAndTypeAndOrganismeId(service, "SERVICE", epmTRefOrganisme.getId()).orElse(null);
        }
        return getSurchargeDocumentAdministrable(sigle, produit, plateformeUuid, epmTRefOrganisme, serviceMpe, Boolean.TRUE.equals(champsFusion));
    }


    public ByteArrayResource getTemplate(AgentEntity agent, DocumentAdministrableEntity documentAdministrableEntity, String token) {
        if (documentAdministrableEntity.getCreePar() != null) {
            EpmTRefOrganisme organisme = agent.getOrganisme();
            EpmTRefOrganisme organismeCreation = documentAdministrableEntity.getCreePar().getOrganisme();
            if (organisme.getId() != organismeCreation.getId()) {
                throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_AUTHORIZED, "L'agent n'a pas le droit de télécharger le document.");
            }
        }

        if (documentAdministrableEntity.getTemplate() == null || !fichierDisqueService.filesExists(documentAdministrableEntity.getTemplate().getChemin())) {
            //récupérer template de docgen
            FichierDisqueEntity fichierDisqueEntity = documentAdministrableEntity.getFichier();
            File fileSaved = fichierDisqueService.loadFile(fichierDisqueEntity.getChemin());
            Resource template = null;
            try {
                template = docgenWS.getTemplate(fileSaved, token);

                String nomModele = "template.docx";
                if (fichierDisqueEntity.getNom() != null && fichierDisqueEntity.getNom().contains(".")) {
                    String[] tab = fichierDisqueEntity.getNom().split("\\.");
                    nomModele = tab[0] + "_template.docx";
                }
                Date now = new Date();
                FichierDisque templateToSave = FichierDisque.builder()
                        .nom(nomModele)
                        .dateSauvegarde(now)
                        .taille(template.contentLength())
                        .build();
                FichierDisque templateSaved = fichierDisqueService.save(templateToSave, template.getInputStream());
                documentAdministrableEntity.setTemplate(fichierDisqueMapper.mapToEntity(templateSaved));
                documentAdministrableEntity = documentAdministrableRepository.save(documentAdministrableEntity);
            } catch (Exception e) {
                throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.SAVE_ERROR, "Erreur pendant l'appel à docgen pour creation du template");
            }
        }

        return this.getFile(documentAdministrableEntity.getTemplate().getChemin());

    }

    public DocumentAdministrable reinitialiserDocument(Long documentId, AgentEntity agent) {
        DocumentAdministrableEntity documentAdministrableEntity = documentAdministrableRepository.findById(documentId).orElseThrow(() -> new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_FOUND, "Le document administrable n'existe pas"));

        DocumentAdministrableEntity documentParent = documentAdministrableEntity.getDocumentParent();
        if (documentAdministrableEntity.getPlateformeUuid() == null || documentParent == null) {
            throw new DocumentAdministrableException(DocumentAdministrableExceptionEnum.NOT_AUTHORIZED, "Le document administrable ne peut pas être réinitialisé");
        }
        documentAdministrableRepository.delete(documentAdministrableEntity);
        return documentAdministrableMapper.mapToModel(documentParent);
    }
}
