package com.atexo.redaction.dao.redaction.entity;

import com.atexo.redaction.dao.referentiel.entity.RefProcedureEntity;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "epm__t_canevas", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Audited
public class CanevasEntity {

    /**
     * Cet attribut mappe la valeur de la colonne de clé primaire.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Identifiant de la dernière publication pour chaque canevas
     */
    @Column(name = "id_last_publication")
    private Integer idLastPublication;

    /*@ManyToOne
    @JoinColumn(name = "id_last_publication")
    private PublicationClausierEntity lastPublication;*/


    @OneToMany(targetEntity = ChapitreEntity.class,
            mappedBy = "epmTCanevas", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    @NotAudited
    private List<ChapitreEntity> epmTChapitres;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(catalog = "redaction", name = "epm__t_canevas_has_ref_type_contrat",
            joinColumns = @JoinColumn(name = "id_canevas", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_type_contrat", referencedColumnName = "id"))
    @NotAudited
    private Set<RefTypeContratEditeurEntity> epmTRefTypeContrats;

    /**
     * Liste des procedures d'editeur
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(catalog = "redaction", name = "epm__t_canevas_has_ref_procedure_passation",
            joinColumns = @JoinColumn(name = "id_canevas"),
            inverseJoinColumns = @JoinColumn(name = "id_procedure"))
    @NotAudited
    private Set<RefProcedureEntity> epmTRefProcedures;


    /**
     * l'objet EpmTRefTypeDocument.
     */
    @ManyToOne
    @JoinColumn(name = "id_document_type", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private RefTypeDocumentEntity epmTRefTypeDocument;

    /**
     * titre du canevas.
     */
    @Column(nullable = false)
    private String titre;

    /**
     * Réference du canevas.
     */
    @Column(nullable = false)
    private String reference;

    /**
     * la date création canevas.
     */
    @Column(name = "date_creation", nullable = false)
    private Date dateCreation;

    /**
     * la date modification canevas.
     */
    @Column(name = "date_modification", nullable = false)
    private Date dateModification;

    /**
     * la date de premiere validation du canevas.
     */
    @Column(name = "date_premiere_validation")
    private Date datePremiereValidation;

    /**
     * la date de derniere validation du canevas.
     */
    @Column(name = "date_derniere_validation")
    private Date dateDerniereValidation;

    /**
     * l'attribut actif.
     */
    @Column(nullable = false)
    private boolean actif;

    /**
     * idNaturePrestation permet de sauvgarder l'id de la nature de prestation
     * choisie.
     */
    @Column(name = "id_nature_prestation", nullable = false)
    private int idNaturePrestation;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String auteur;


    @Column(nullable = false)
    private int etat = 0;


    @Column(name = "compatible_entite_adjudicatrice")
    private boolean compatibleEntiteAdjudicatrice = true;

    /**
     * L'identifiant de l'organisme auquel la consultation est liée
     */
    @Column(name = "id_organisme")
    private Integer idOrganisme;

    /**
     * Détermine si le canevas est de type éditeur ou client
     */
    @Column(name = "canevas_editeur")
    private boolean canevasEditeur;

    @ManyToOne
    @JoinColumn(name = "id_statut_redaction_clausier", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private RefStatutRedactionClausierEntity epmTRefStatutRedactionClausier;

    /**
     * CCAG
     */
    @Column(name = "id_ref_ccag")
    private Integer idRefCCAG;

}
