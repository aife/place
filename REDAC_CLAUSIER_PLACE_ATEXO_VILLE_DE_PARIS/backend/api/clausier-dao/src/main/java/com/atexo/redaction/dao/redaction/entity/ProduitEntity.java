package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_produit", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProduitEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "libelle", nullable = false)
    private String libelle;

    @Column(name = "default_url", nullable = false)
    private String defaultUrl;

    @Column(name = "WS_champs_fusion", nullable = false)
    private String wsChampsFusion;

    @Column(name = "actif", nullable = false)
    private boolean actif;

    @Column(name = "always_show", nullable = false)
    private boolean alwaysShow;

    @Column(name = "version", nullable = false)
    private String version;


}
