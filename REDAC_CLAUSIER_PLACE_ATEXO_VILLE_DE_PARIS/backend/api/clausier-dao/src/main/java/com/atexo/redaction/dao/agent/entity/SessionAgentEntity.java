package com.atexo.redaction.dao.agent.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "epm__t_session_agent", catalog = "redaction")
public class SessionAgentEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_agent", referencedColumnName = "id")
    private AgentEntity agent;

    @Lob
    @Column(name = "token", nullable = false)
    private String token;

    @Lob
    @Column(name = "produits")
    private String produits;


}
