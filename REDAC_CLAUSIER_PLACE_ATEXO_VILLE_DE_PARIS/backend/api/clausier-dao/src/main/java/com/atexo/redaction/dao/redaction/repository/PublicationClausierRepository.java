package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.PublicationClausierEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublicationClausierRepository extends DaoRepository<PublicationClausierEntity, Integer> {

    List<PublicationClausierEntity> findAllByEnCoursActivationIsTrue();
}
