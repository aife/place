package com.atexo.redaction.dao.redaction.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_publication_clausier_file", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class PublicationClausierFileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * nom du fichier referentiel ou du fichier contenant le clausier
     */
    @Column(name="file_name")
    private String fileName;

    /**
     * soit fichier contenant les référentiels lors de cette publication (json)
     * soit fichier contenant le clausier de cette publication (json)
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "file_clausier", columnDefinition = "longblob")
    private byte[] fileClausier;

}
