package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_ref_type_document", catalog= "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RefTypeDocumentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "actif")
    protected boolean actif;

    @Column(name = "libelle", columnDefinition = "TEXT")
    protected String libelle;

    @Column(name = "libelle_court")
    protected String libelleCourt;

    @Column(name = "code_externe")
    protected String codeExterne;

    @Column(name = "extension_fichier")
    private String extensionFichier;

    @Column(name = "template", columnDefinition = "TEXT")
    private String template;

    @Column(name = "sommaire")
    private boolean sommaire;

    @Column(name = "template_tableau_derogation",columnDefinition = "TEXT")
    private String templateTableauDerogation;

    @Column(name = "activer_derogation")
    private boolean activerDerogation;

    @Column(name = "id_type_passation")
    private Integer idTypePassation;

}
