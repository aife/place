package com.atexo.redaction.dao.consultation.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "epm__t_consultation_contexte", catalog = "redaction")
public class ConsultationContexteEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private Integer id;

    @Column(name = "PLATEFORME")
    private String plateforme;

    @Column(name = "REFERENCE")
    private String reference;

    @Lob
    @Column(name = "XML_MPE", nullable = false)
    private String xmlMpe;

    @Lob
    @Column(name = "CONTEXTE", nullable = false)
    private String contexte;

    @Lob
    @Column(name = "CONSULTATION_JSON", nullable = false)
    private String consultationJson;


}
