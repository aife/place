package com.atexo.redaction.dao.redaction.specification;

import com.atexo.redaction.dao.redaction.entity.CanevasEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;

@Getter
@Setter
public class RechercheCanevasSpecification implements Specification<CanevasEntity> {
    private String referenceCanevas;
    private String referenceClause;
    private int idNaturePrestation;
    private int typeAuteur;
    private int idStatutRedactionClausier;
    private int idTypeDocument;
    private Integer idTypeContrat;
    private Integer idProcedure;
    private Integer idOrganisme;
    private Boolean actif;
    private Date dateModificationMin;
    private Date dateModificationMax;
    private boolean editeur;

    @Override
    public Predicate toPredicate(Root<CanevasEntity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        criteriaQuery.distinct(true);
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("dateModification")));
        Predicate predicate = criteriaBuilder.notEqual(root.get("etat"), 2);
        if (actif != null) {
            predicate = addEqualFilter(root, criteriaBuilder, predicate, "actif", actif);
        }
        predicate = addEqualFilter(root, criteriaBuilder, predicate, "canevasEditeur", editeur);
        //appel côté editeur
        if (!editeur && idOrganisme != null) {
            predicate = addEqualFilter(root, criteriaBuilder, predicate, "idOrganisme", idOrganisme);
        }
        //appel côté client
        else {
            Predicate publicationPredict = criteriaBuilder.isNotNull(root.get("lastPublication.id"));
            predicate = criteriaBuilder.and(predicate, publicationPredict);
        }
        return predicate;
    }

    private Predicate addEqualFilter(Root<CanevasEntity> root, CriteriaBuilder criteriaBuilder,
                                     Predicate predicate, String key, Object value) {

        Predicate datePredict = criteriaBuilder.equal(root.get(key), value);
        return criteriaBuilder.and(predicate, datePredict);
    }
}
