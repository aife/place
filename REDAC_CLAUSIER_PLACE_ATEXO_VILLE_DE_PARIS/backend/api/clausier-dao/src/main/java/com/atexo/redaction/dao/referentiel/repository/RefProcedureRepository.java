package com.atexo.redaction.dao.referentiel.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.referentiel.entity.RefProcedureEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefProcedureRepository extends DaoRepository<RefProcedureEntity, Integer> {

}
