package com.atexo.redaction.config.bdd;

import com.atexo.redaction.dao.agent.entity.AgentEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Configuration
@Slf4j
public class AtexoAuditorAware implements AuditorAware<AgentEntity> {

    private final HttpServletRequest httpServletRequest;

    public AtexoAuditorAware(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }


    @Override
    public Optional<AgentEntity> getCurrentAuditor() {
        HttpSession session = getSession(httpServletRequest);
        if (session != null) {
            var agent = session.getAttribute("agent");
            if (agent != null)
                return Optional.of((AgentEntity) agent);
        }
        return Optional.empty();
    }

    private HttpSession getSession(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession(false);
        if (session == null) {
            log.trace("Session now must be created. This may be suspect");
            session = httpServletRequest.getSession(true);
        } else {
            log.trace("Session already exist. All is ok!");
        }
        return session;
    }
}
