package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.CanevasEntity;
import com.atexo.redaction.dao.redaction.entity.RefAuteurEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface CanevasRepository extends DaoRepository<CanevasEntity, Integer> {

}
