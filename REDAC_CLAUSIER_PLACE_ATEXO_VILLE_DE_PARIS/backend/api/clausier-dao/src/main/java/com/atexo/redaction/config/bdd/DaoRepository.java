package com.atexo.redaction.config.bdd;


import com.atexo.redaction.config.querybean.CriterionValue;
import com.atexo.redaction.config.querybean.DatabaseSpecification;
import com.atexo.redaction.config.querybean.LogicOperatorEnum;
import com.atexo.redaction.config.querybean.SearchCriteriaQuery;
import com.atexo.redaction.dao.redaction.entity.CanevasEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.util.CollectionUtils;

import java.util.List;

public interface DaoRepository<T, I> extends PagingAndSortingRepository<T, I>,
        JpaSpecificationExecutor<T> {

    @Override
    List<T> findAll();

    default List<T> findAllByCriteria(SearchCriteriaQuery searchCriteria) {
        Specification<T> specification = getSpecification(searchCriteria);
        return this.findAll(specification);
    }

    default Page<T> findAllByCriteria(SearchCriteriaQuery searchCriteria, Pageable pageable) {
        Specification<T> specification = getSpecification(searchCriteria);

        return this.findAll(specification, pageable);
    }

    private Specification<T> getSpecification(SearchCriteriaQuery searchCriteria) {
        Specification<T> specification = null;
        if (!CollectionUtils.isEmpty(searchCriteria.getValues())) {
            for (CriterionValue criterionValue : searchCriteria.getValues()) {

                if (specification == null) {
                    specification = initSpecification(criterionValue);
                } else {
                    specification = LogicOperatorEnum.AND.equals(searchCriteria.getOperator()) ?
                            specification.and(DatabaseSpecification.<T>builder()
                                    .criteria(criterionValue)
                                    .build())
                            : specification.or(DatabaseSpecification.<T>builder()
                            .criteria(criterionValue)
                            .build());
                }
            }
        }
        specification = getClauses(searchCriteria, specification);
        return specification;
    }

    private Specification<T> getClauses(SearchCriteriaQuery searchCriteria, Specification<T> specification) {
        if (!CollectionUtils.isEmpty(searchCriteria.getClauses())) {
            for (SearchCriteriaQuery clause : searchCriteria.getClauses()) {
                if (specification == null) {
                    specification = Specification.where(getSpecification(clause));
                } else {
                    specification = LogicOperatorEnum.AND.equals(searchCriteria.getOperator()) ?
                            specification.and(getSpecification(clause)) : specification.or(getSpecification(clause));

                }
            }
        }
        return specification;
    }

    private Specification<T> initSpecification(CriterionValue criterionValue) {
        Specification<T> specification;
        specification = Specification.where(
                DatabaseSpecification.<T>builder()
                        .criteria(criterionValue)
                        .build());
        return specification;
    }

}
