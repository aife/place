package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefTypeDocumentEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefTypeDocumentRepository extends DaoRepository<RefTypeDocumentEntity, Integer> {

}
