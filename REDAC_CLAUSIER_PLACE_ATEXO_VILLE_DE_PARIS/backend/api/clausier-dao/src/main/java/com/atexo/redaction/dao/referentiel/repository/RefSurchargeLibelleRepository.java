package com.atexo.redaction.dao.referentiel.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.referentiel.entity.RefSurchargeLibelleEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefSurchargeLibelleRepository extends DaoRepository<RefSurchargeLibelleEntity, Integer> {

}
