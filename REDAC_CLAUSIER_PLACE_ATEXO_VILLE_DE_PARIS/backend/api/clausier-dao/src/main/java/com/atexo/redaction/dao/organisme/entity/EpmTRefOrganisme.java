package com.atexo.redaction.dao.organisme.entity;

import com.atexo.redaction.dao.referentiel.entity.RefProcedureEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "epm__t_ref_organisme", catalog = "referentiel")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
public class EpmTRefOrganisme {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_organisme")
    private int id;

    @Column(name = "actif", nullable = false)
    private boolean actif;

    @Column(name = "libelle", columnDefinition = "TEXT", nullable = false)
    private String libelle;

    @Column(name = "code_externe", nullable = false)
    private String codeExterne;

    @Column(name = "plateforme_uuid")
    private String plateformeUuid;

    @Column(name = "logo_actif")
    private boolean logoActif;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(catalog = "referentiel", name = "epm__t_ref_procedure_has_organisme",
            joinColumns = @JoinColumn(name = "id_organisme", referencedColumnName = "id_organisme"),
            inverseJoinColumns = @JoinColumn(name = "id_procedure"))
    private Set<RefProcedureEntity> epmTRefProcedureSet;
}
