package com.atexo.redaction.dao.referentiel.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.referentiel.entity.RefCcagEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefCcagRepository extends DaoRepository<RefCcagEntity, Integer> {

}
