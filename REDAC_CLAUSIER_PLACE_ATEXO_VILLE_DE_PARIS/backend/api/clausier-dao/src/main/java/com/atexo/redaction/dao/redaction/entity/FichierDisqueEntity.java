package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "epm__t_fichier_disque", catalog = "consultation")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class FichierDisqueEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fichier")
    private int idFichier;

    @Column(name = "nom_fichier", nullable = false)
    private String nom;

    @Column(name = "chemin_fichier", nullable = false)
    private String chemin;


    @Column(name = "date_sauvegarde", nullable = false)
    private Date dateSauvegarde;


    @Column(name = "taille",nullable = false)
    private Long taille;

    @Column(name = "md5")
    private String md5;

}
