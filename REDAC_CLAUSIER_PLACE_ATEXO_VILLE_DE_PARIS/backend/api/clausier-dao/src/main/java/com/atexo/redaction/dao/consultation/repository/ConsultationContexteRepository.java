package com.atexo.redaction.dao.consultation.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.consultation.entity.ConsultationContexteEntity;

public interface ConsultationContexteRepository extends DaoRepository<ConsultationContexteEntity, Integer> {

}
