package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefStatutRedactionClausierEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefStatutRedactionClausierRepository extends DaoRepository<RefStatutRedactionClausierEntity, Integer> {

}
