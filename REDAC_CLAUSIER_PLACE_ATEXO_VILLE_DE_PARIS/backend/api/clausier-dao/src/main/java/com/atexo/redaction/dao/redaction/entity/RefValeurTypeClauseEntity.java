package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_ref_valeur_type_clause", catalog= "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RefValeurTypeClauseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "actif")
    protected boolean actif;

    @Column(name = "libelle", columnDefinition = "TEXT")
    protected String libelle;

    @Column(name = "libelle_court")
    protected String libelleCourt;

    @Column(name = "code_externe")
    protected String codeExterne;

    @Column(name = "expression", columnDefinition = "TEXT")
    protected String expression;

    @Column(name = "simple")
    protected boolean simple;

    /**
     * L'identifiant du type de clause auquel la valeur est liée
     */
    @Column(name = "id_ref_type_clause")
    private Integer idRefTypeClause;

}
