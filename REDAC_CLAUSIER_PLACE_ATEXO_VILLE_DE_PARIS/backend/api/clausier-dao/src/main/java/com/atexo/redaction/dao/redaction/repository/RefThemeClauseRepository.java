package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefThemeClauseEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefThemeClauseRepository extends DaoRepository<RefThemeClauseEntity, Integer> {

}
