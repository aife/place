package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_ref_theme_clause", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RefThemeClauseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "actif")
    protected boolean actif;

    @Column(name = "libelle", columnDefinition = "TEXT")
    protected String libelle;

    @Column(name = "libelle_court")
    protected String libelleCourt;

    @Column(name = "code_externe")
    protected String codeExterne;

}
