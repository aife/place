package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.redaction.entity.DocumentAdministrableEntity;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentAdministrableRepository extends DaoRepository<DocumentAdministrableEntity, Long> {

    List<DocumentAdministrableEntity> findByStatutEditionIn(@Param("status") List<String> status);

    List<DocumentAdministrableEntity> findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeAndService(boolean champsFusion, String produit, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service);

    List<DocumentAdministrableEntity> findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeAndServiceIsNull(boolean champsFusion, String produit, String plateformeUuid, EpmTRefOrganisme organisme);

    List<DocumentAdministrableEntity> findAllByChampFusionAndProduitAndPlateformeUuidAndOrganismeIsNullAndServiceIsNull(boolean champsFusion, String produit, String plateformeUuid);

    List<DocumentAdministrableEntity> findAllByChampFusionAndProduitAndPlateformeUuidIsNull(boolean champFusion, String nomProduit);


    Optional<DocumentAdministrableEntity> findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndService(String code, boolean champsFusion, String produit, String plateformeUuid, EpmTRefOrganisme organisme, ReferentielMpeEntity service);

    Optional<DocumentAdministrableEntity> findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeAndServiceIsNull(String code, boolean champsFusion, String produit, String plateformeUuid, EpmTRefOrganisme organisme);

    Optional<DocumentAdministrableEntity> findByCodeAndChampFusionAndProduitAndPlateformeUuidAndOrganismeIsNullAndServiceIsNull(String code, boolean champsFusion, String produit, String plateformeUuid);

    Optional<DocumentAdministrableEntity> findByCodeAndChampFusionAndProduitAndPlateformeUuidIsNullAndOrganismeIsNullAndServiceIsNull(String code, boolean champFusion, String nomProduit);
}
