package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.ChapitreEntity;
import com.atexo.redaction.dao.redaction.entity.PublicationClausierEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface ChapitreRepository extends DaoRepository<ChapitreEntity, Integer> {

}
