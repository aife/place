package com.atexo.redaction.dao.redaction.entity;

import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "epm__t_chapitre",catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ChapitreEntity {

    /**
     * Clé primaire et id_clause en meme temps
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_canevas", nullable = true)
    private CanevasEntity epmTCanevas;

    @ManyToOne
    @JoinColumn(name = "id_parent_chapitre", nullable = true)
    private ChapitreEntity epmTParentChapitre;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(catalog= "redaction", name = "epm__t_chapitre_has_clause",
            joinColumns = @JoinColumn(name = "id_chapitre"))
    @MapKeyColumn(name = "num_order")
    @Column(name = "reference_clause")
    private Map<Integer, String> clauses;

    @OneToMany(targetEntity = ChapitreEntity.class,
            mappedBy = "epmTParentChapitre", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderBy("numero asc")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ChapitreEntity> epmTSousChapitres;


    @Column(name = "titre", nullable = false)
    private String titre;

    @Column(name = "numero", nullable = false)
    private String numero;

    /**
     * texte contenu dans l'info-bull .
     */
    @Column(name = "info_bulle_text", columnDefinition = "TEXT")
    private String infoBulleText;

    /**
     * Attribut Lien .
     */
    @Column(name = "info_bulle_url", columnDefinition = "TEXT")
    private String infoBulleUrl;

    @Column(name = "style")
    private String style;

    @Column(name = "derogation_article")
    private String derogationArticle;

    @Column(name = "derogation_commentaires",columnDefinition = "TEXT")
    private String derogationCommentaires;

    @Column(name = "derogation_active")
    private Boolean derogationActive;

    @Column(name = "afficher_derogation")
    private Boolean afficherMessageDerogation;


    @Override
    public ChapitreEntity clone() throws CloneNotSupportedException {
        ChapitreEntity epmTChapitre = (ChapitreEntity) super.clone();
        epmTChapitre.id = 0;
        return epmTChapitre;
    }

}
