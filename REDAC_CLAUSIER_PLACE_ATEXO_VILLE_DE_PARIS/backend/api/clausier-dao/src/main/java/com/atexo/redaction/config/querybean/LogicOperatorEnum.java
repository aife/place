package com.atexo.redaction.config.querybean;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LogicOperatorEnum {
    AND, OR;
}
