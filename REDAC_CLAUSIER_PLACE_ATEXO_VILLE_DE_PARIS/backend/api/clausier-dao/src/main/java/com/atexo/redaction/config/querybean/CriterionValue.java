package com.atexo.redaction.config.querybean;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class CriterionValue {
    @NotNull
    private FieldSearchOperatorEnum operator;

    private boolean not = false;

    private boolean join = false;

    @NotNull
    private Object value;
    @NotNull
    private String field;

}
