package com.atexo.redaction.config.querybean;

import com.atexo.redaction.config.exception.AtexoException;
import com.atexo.redaction.config.exception.ExceptionEnum;
import com.atexo.redaction.config.querybean.CriterionValue;
import lombok.*;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DatabaseSpecification<T> implements Specification<T> {


    private CriterionValue criteria;


    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
        final List<String> key = Arrays.asList(criteria.getField().split("\\."));

        Path<String> path;
        if (criteria.isJoin()) {
            path = root.join(key.get(0));
        } else {
            path = root.get(key.get(0));
        }
        for (int i = 1; i < key.size(); i++) {
            path = path.get(key.get(i));
        }
        Predicate predict = getPredicate(root, builder, path, root.get(key.get(0)));
        return criteria.isNot() ? predict.not() : predict;
    }

    public Predicate getPredicate(Root<T> root, CriteriaBuilder builder, Path<String> path, Expression expression) {
        switch (criteria.getOperator()) {
            case GREATER_THAN_DATE:
                Path<ZonedDateTime> entityDate = root.get(criteria.getField());
                return builder.greaterThanOrEqualTo(entityDate, (ZonedDateTime) criteria.getValue());
            case LESS_THAN_DATE:
                entityDate = root.get(criteria.getField());
                return builder.lessThanOrEqualTo(entityDate, (ZonedDateTime) criteria.getValue());
            case IS_NULL:
                return builder.isNull(path);
            case IS_NOT_NULL:
                return builder.isNotNull(path);
            case START_WITH:
                return builder.like(path, criteria.getValue() + "%");
            case IS_EMPTY:
                return builder.isNotEmpty(expression);
            case IN:
                if (criteria.getValue() instanceof List<?>) {
                    return path
                            .in(((List<?>) criteria.getValue()).toArray());
                } else {
                    throw new AtexoException("Une liste est obligatoire", ExceptionEnum.MANDATORY.getCode(), ExceptionEnum.MANDATORY.getType());
                }
            case GREATER_THAN:
                return builder.greaterThanOrEqualTo(path, criteria.getValue().toString());
            case LESS_THAN:
                return builder.lessThanOrEqualTo(path, criteria.getValue().toString());
            case EQUAL:
            default:
                return builder.equal(path, criteria.getValue());
        }
    }
}
