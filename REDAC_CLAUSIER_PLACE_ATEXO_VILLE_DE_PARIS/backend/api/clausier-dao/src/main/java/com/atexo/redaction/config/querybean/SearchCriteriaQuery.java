package com.atexo.redaction.config.querybean;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class SearchCriteriaQuery {

    @NotNull
    private LogicOperatorEnum operator = LogicOperatorEnum.AND;

    private List<CriterionValue> values;

    private List<SearchCriteriaQuery> clauses;

}
