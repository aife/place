package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefTypeClauseEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefTypeClauseRepository extends DaoRepository<RefTypeClauseEntity, Integer> {

}
