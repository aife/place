package com.atexo.redaction.dao.organisme.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrganismeRepository extends DaoRepository<EpmTRefOrganisme, Integer> {

    List<EpmTRefOrganisme> findByCodeExterneAndPlateformeUuidIsNull(String codeExterne);

    Optional<EpmTRefOrganisme> findByCodeExterneAndPlateformeUuid(String codeExterne, String plateformeUuid);

}
