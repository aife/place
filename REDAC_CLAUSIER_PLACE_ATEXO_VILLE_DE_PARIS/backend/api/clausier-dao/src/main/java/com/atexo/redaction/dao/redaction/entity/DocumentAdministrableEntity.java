package com.atexo.redaction.dao.redaction.entity;

import com.atexo.redaction.dao.agent.entity.AgentEntity;
import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_document_administrable", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class DocumentAdministrableEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "libelle", nullable = false)
    private String libelle;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "produit", nullable = false)
    private String produit;

    @ManyToOne
    @JoinColumn(name = "id_agent")
    private AgentEntity agent;

    @ManyToOne
    @JoinColumn(name = "id_document_parent")
    private DocumentAdministrableEntity documentParent;

    @Column(name = "actif", nullable = false)
    private boolean actif;

    @ManyToOne
    @JoinColumn(name = "id_service")
    private ReferentielMpeEntity service;

    @ManyToOne
    @JoinColumn(name = "id_organisme")
    private EpmTRefOrganisme organisme;


    @Column(name = "plateforme_uuid", length = 100)
    private String plateformeUuid;

    @ManyToOne
    @JoinColumn(name = "id_fichier")
    private FichierDisqueEntity fichier;

    @ManyToOne
    @JoinColumn(name = "id_template")
    private FichierDisqueEntity template;

    @Column(name = "champ_fusion", nullable = false)
    private boolean champFusion;

    @Lob
    @Column(name = "token_edition")
    private String tokenEdition;

    @Column(name = "statut_edition")
    private String statutEdition;

    @ManyToOne
    @JoinColumn(name = "id_agent_cree_par")
    @CreatedBy
    private AgentEntity creePar;

    @ManyToOne
    @JoinColumn(name = "id_agent_modifie_par")
    @LastModifiedBy
    private AgentEntity modifiePar;


}
