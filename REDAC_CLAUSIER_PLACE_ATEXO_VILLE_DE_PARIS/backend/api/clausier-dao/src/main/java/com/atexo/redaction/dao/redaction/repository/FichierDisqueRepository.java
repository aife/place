package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.FichierDisqueEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface FichierDisqueRepository extends DaoRepository<FichierDisqueEntity, Integer> {

}
