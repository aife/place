package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefTypeContratEditeurEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefTypeContratRepository extends DaoRepository<RefTypeContratEditeurEntity, Integer> {

}
