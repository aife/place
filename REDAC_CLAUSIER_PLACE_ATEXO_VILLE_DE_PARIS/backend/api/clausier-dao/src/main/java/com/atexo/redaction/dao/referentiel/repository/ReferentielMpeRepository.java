package com.atexo.redaction.dao.referentiel.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReferentielMpeRepository extends DaoRepository<ReferentielMpeEntity, Integer> {

    Optional<ReferentielMpeEntity> findByCodeAndTypeAndOrganismeId(String code, String type, Integer idOrganisme);

    List<ReferentielMpeEntity> findAllByType(String type);
}
