package com.atexo.redaction.dao.referentiel.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.referentiel.entity.RefNatureEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefNatureRepository extends DaoRepository<RefNatureEntity, Integer> {

}
