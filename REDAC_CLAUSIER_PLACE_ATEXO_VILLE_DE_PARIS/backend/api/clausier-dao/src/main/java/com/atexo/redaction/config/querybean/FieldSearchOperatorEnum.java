package com.atexo.redaction.config.querybean;

public enum FieldSearchOperatorEnum {
    EQUAL,
    GREATER_THAN,
    LESS_THAN,
    IS_NULL,
    IS_NOT_NULL,
    START_WITH,
    IS_EMPTY,
    GREATER_THAN_DATE,
    IN,
    LESS_THAN_DATE;
}
