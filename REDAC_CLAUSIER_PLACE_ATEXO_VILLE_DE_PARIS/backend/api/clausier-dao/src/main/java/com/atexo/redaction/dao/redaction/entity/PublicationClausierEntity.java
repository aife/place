package com.atexo.redaction.dao.redaction.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "epm__t_publication_clausier", catalog = "redaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class PublicationClausierEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "version", columnDefinition = "TEXT")
    private String version;

    @Column(name = "id_utilisateur")
    private Integer idUtilisateur;

    @Column(name = "nom_complet_utilisateur", columnDefinition = "TEXT")
    private String nomCompletUtilisateur;

    @Column(name = "id_organisme")
    private Integer idOrganisme;

    @Column(name = "commentaire", columnDefinition = "TEXT")
    private String commentaire;

    @Column(name = "date_publication")
    private Date datePublication;

    /**
     * on ne peut avoir qu'une seule version active en même temps)
     */
    @Column(name = "actif")
    private boolean actif;

    @Column(name = "en_cours_activation")
    private boolean enCoursActivation;

    @Column(name = "editeur")
    private String editeur;

    @Column(name = "date_integration")
    private Date dateIntegration;

    @Column(name = "date_activation")
    private Date dateActivation;

    @Column(name = "gestion_local")
    private boolean gestionLocal;

    /**
     * fichier contenant les référentiels lors de cette publication (exel)
     */
    @ManyToOne(targetEntity = PublicationClausierFileEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_file_referentiel")
    private PublicationClausierFileEntity fichierReferentiel;


    /**
     * fichier contenant le clausier de cette publication (exel)
     */
    @ManyToOne(targetEntity = PublicationClausierFileEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_file_clausier")
    private PublicationClausierFileEntity fichierClausier;



}
