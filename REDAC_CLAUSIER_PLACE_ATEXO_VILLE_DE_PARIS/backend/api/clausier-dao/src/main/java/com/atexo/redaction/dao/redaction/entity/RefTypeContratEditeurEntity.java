package com.atexo.redaction.dao.redaction.entity;

import com.atexo.redaction.dao.referentiel.entity.RefProcedureEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "epm__t_ref_type_contrat_editeur", catalog = "redaction")
@Cacheable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RefTypeContratEditeurEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;

    @Column(name = "actif")
    protected boolean actif;

    @Column(name = "libelle", columnDefinition = "TEXT")
    protected String libelle;

    @Column(name = "libelle_court")
    protected String libelleCourt;

    @Column(name = "code_externe")
    protected String codeExterne;

    @Column(name = "avenant_active")
    private boolean avenantActive;

    @Column(name = "abreviation_type_contrat")
    private String abreviation;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            catalog = "redaction",
            name = "epm__t_type_contrat_has_procedure",
            inverseJoinColumns = @JoinColumn(name = "id_procedure"),
            joinColumns = @JoinColumn(name = "id_type_contrat"))
    private List<RefProcedureEntity> procedures;

}
