package com.atexo.redaction.dao.agent.entity;

import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import com.atexo.redaction.dao.referentiel.entity.ReferentielMpeEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Set;

@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "epm__t_agent", catalog = "redaction", uniqueConstraints = {@UniqueConstraint(columnNames = {"identifiant", "id_organisme"})},
        indexes = {@Index(name = "agent_identifiant_organisme_plateforme", columnList = "identifiant,id_organisme"),
                @Index(name = "agent_id_index", columnList = "id")})
public class AgentEntity implements Serializable {


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid",
            strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private String id;

    @Column(name = "identifiant")
    private String identifiant;

    @Lob
    @Column(name = "acheteur_public")
    private String acheteurPublic;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "nom")
    private String nom;

    @Lob
    @Column(name = "roles", nullable = false)
    private String roles;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_organisme", referencedColumnName = "id_organisme")
    private EpmTRefOrganisme organisme;

    @Column(name = "premier_acces")
    private ZonedDateTime datePremierAcces;

    @Column(name = "dernier_acces")
    private ZonedDateTime dateDernierAcces;

    @Column(name = "mpe_url", length = 100)
    private String mpeUrl;

    @Lob
    @Column(name = "envoi_mpe")
    private String envoiMpe;

    @Lob
    @Column(name = "mpe_refresh_token")
    private String mpeRefreshToken;

    @Lob
    @Column(name = "mpe_token", length = 100)
    private String mpeToken;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_service")
    @Where(clause = "type = 'SERVICE'")
    private ReferentielMpeEntity service;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "agent")
    private Set<SessionAgentEntity> sessions;


}
