package com.atexo.redaction.config.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionEnum {
    NOT_FOUND(404, "Non existant"), SAVE_ERROR(500, "Erreur lors de l'enregistrement de l'élément"),
    NOT_AUTHORIZED(401, "Non autorisé"),
    MANDATORY(400, "Champs obligatoires"),
    DATA(400, "Champs de type différent");
    private final int code;
    private final String type;

}
