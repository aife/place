package com.atexo.redaction.dao.agent.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.agent.entity.SessionAgentEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionAgentRepository extends DaoRepository<SessionAgentEntity, Integer> {
    SessionAgentEntity findByToken(String token);
}
