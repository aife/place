package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.ProduitEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProduitRepository extends DaoRepository<ProduitEntity, Integer> {

    List<ProduitEntity> findAllByActifIsTrue();

    ProduitEntity findByNomAndActifIsTrue(String nom);

    List<ProduitEntity> findAllByActifIsTrueAndAlwaysShowIsTrue();

}
