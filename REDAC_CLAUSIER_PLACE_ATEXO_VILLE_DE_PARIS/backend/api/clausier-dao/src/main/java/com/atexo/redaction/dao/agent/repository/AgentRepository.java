package com.atexo.redaction.dao.agent.repository;

import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.agent.entity.AgentEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AgentRepository extends DaoRepository<AgentEntity, String> {

    Optional<AgentEntity> findByIdentifiantAndOrganismeId(String identifiant, Integer idOrganisme);

    List<AgentEntity> findByOrganismeIsNullAndRolesIsLike(String roleSuffix);

    List<AgentEntity> findByOrganismeIdAndRolesIsLike(Integer idOrganisme, String roles);

    List<AgentEntity> findAllByOrganismeId(Integer idOrganisme);

    @Query("SELECT agent.envoiMpe FROM AgentEntity agent WHERE agent.id =:id")
    String findEnvoiMpeById(@Param("id") String id);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query("UPDATE AgentEntity agent SET agent.datePremierAcces = :datePremierAcces , agent.dateDernierAcces =:dateDernierAcces  WHERE agent.id = :id")
    void update(String id, ZonedDateTime datePremierAcces, ZonedDateTime dateDernierAcces);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query("UPDATE AgentEntity agent SET agent.mpeRefreshToken = :mpeRefreshToken , agent.mpeToken =:mpeToken  WHERE agent.id = :id")
    void updateMpeToken(String id, String mpeToken, String mpeRefreshToken);
}
