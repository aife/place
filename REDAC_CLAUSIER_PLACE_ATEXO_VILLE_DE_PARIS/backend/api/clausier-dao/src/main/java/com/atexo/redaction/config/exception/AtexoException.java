package com.atexo.redaction.config.exception;

import lombok.Getter;

@Getter
public class AtexoException extends RuntimeException {
    protected final String message;
    protected final int code;
    protected final String type;

    public AtexoException(String errorMessage, int code, String type, Throwable err) {
        super(errorMessage, err);
        this.message = errorMessage;
        this.code = code;
        this.type = type;
    }

    public AtexoException(String errorMessage, int code, String type) {
        super(errorMessage);
        this.message = errorMessage;
        this.code = code;
        this.type = type;
    }
}
