package com.atexo.redaction.dao.referentiel.entity;


import com.atexo.redaction.dao.organisme.entity.EpmTRefOrganisme;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_referential_mpe", catalog = "redaction", uniqueConstraints = {@UniqueConstraint(columnNames = {"type", "code", "id_organisme"})},
        indexes = {@Index(name = "referentiel_mpe_type_code_index", columnList = "type,code,id_organisme")})
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor

public class ReferentielMpeEntity extends GenericReferentielEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_organisme", referencedColumnName = "id_organisme")
    private EpmTRefOrganisme organisme;


}
