package com.atexo.redaction.dao.referentiel.entity;

import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "epm__t_ref_type_marche", catalog = "referentiel")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "cacheReferentielSelect")
public class RefTypeMarche {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type_marche")
    private Integer id;

    @Column(name = "libelle", columnDefinition = "TEXT", nullable = false)
    private String libelle;

    @Column(name = "actif", nullable = false)
    private boolean actif;

    @Column(name = "simplifie", nullable = false)
    private boolean simplifie;

}
