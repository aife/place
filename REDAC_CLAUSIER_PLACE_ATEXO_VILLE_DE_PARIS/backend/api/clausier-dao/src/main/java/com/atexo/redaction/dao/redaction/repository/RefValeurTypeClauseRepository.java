package com.atexo.redaction.dao.redaction.repository;


import com.atexo.redaction.config.bdd.DaoRepository;
import com.atexo.redaction.dao.redaction.entity.RefValeurTypeClauseEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface RefValeurTypeClauseRepository extends DaoRepository<RefValeurTypeClauseEntity, Integer> {

}
