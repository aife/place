package com.atexo.redaction.dao.referentiel.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "epm__t_ref_surcharge_libelle",  catalog = "referentiel")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RefSurchargeLibelleEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    protected Integer id;


    @Column(name = "code")
    protected String code;

    @Column(name = "libelle")
    protected String libelle;


}
