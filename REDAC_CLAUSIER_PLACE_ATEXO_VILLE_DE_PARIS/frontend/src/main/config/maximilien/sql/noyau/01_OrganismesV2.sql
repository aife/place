-- Liste des organismes --

DELETE FROM referentiel.epm__t_ref_organisme;

insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (1, 'Organisme de paramétrage de référence', 'a1a');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (2, 'Organisme de test ATEXO', 'a2z');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (3, 'Organisme de Formation', 'e3r');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (4, 'Conseil Régional d''Ile-de-France', 't5y');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (5, 'Organisme de Recette de la Reprise de Données', 'u6i');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (8, 'Organisme de test pour les entreprises', 'o8p');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (9, 'Agence régionale pour la Nature et la Biodiversité en Île-de-France', 'q1s');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (10, 'Ville de Paris - lien externe', 'v-paris-old');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (14, 'Conseil Général du Val d''Oise', 'cg-95');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (26, 'BOAMP', 'boamp');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (29, 'Département / Conseil départemental de Seine-Saint-Denis - lien externe', 'cg-93');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (30, 'Département / Conseil départemental des Hauts-de-Seine', 'cg-92');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (31, 'Conseil Général du Val-de-Marne', 'cg-94');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (32, 'Département / Conseil départemental de l''Essonne', 'cg-91');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (33, 'Plate-forme des Achats de l''Etat', 'place');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (35, 'GIP Maximilien', 'd2f');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (37, 'Département / Conseil départemental du Val d''Oise', 'i9o');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (39, 'Département / Conseil départemental de Seine-et-Marne', 'g3h');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (43, 'Ville d''Aubervilliers', 'j4k');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (45, 'Ville de Carrières-sous-Poissy', 'm5w');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (47, 'Département / Conseil départemental de l''Essonne', 'x6c');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (49, 'Département de Seine-Saint-Denis', 'v5b');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (51, 'département du Val-de-Marne', 'n2a');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (53, 'Ville de Paris', 'r8t');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (55, 'CONFLUENCE HABITAT  - OPH de Montereau', 'z5e');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (57, 'Communauté d''agglomération de Plaine Commune', 'p2q');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (59, 'Le Kremlin-Bicêtre', 's3d');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (61, 'Conseil général des Hauts-de-Seine', 'f4g');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (63, 'Ville d'' Épinay-sur-Seine', 'h5j');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (65, 'Ville de Meudon', 'y6u');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (67, 'Syndicat des transports d''Ile de France', 'b3t');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (69, 'SAEM Noisy-le-Sec Habitat', 'b3w');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (71, 'Organisme de test de génération d''un nouvel organisme', 'b4r');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (73, 'SAERP', 'b3v');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (77, 'CCAS de Meudon', 'b4b');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (79, 'Maison Départementale des Personnes Handicapées du Val d''Oise', 'b4c');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (81, 'Ville de Saint-Denis', 'b4f');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (83, 'Office public communautaire de Plaine Commune', 'b4e');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (85, 'Ville de Neuilly-sur-Seine', 'b4a');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (91, 'Département / Conseil départemental du Val-de-Marne', 'a8z');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (93, 'Agence des espaces verts de la région d''Ile-de-France', 'b4g');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (95, 'Ville de Villetaneuse', 'b4j');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (99, 'Mairie de L''Île-Saint-Denis', 'd5d');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (103, 'SEM PLAINE COMMUNE DEVELOPPEMENT', 'b4k');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (113, 'VILLE D''ALFORTVILLE', 'd5e');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (119, 'Ville de Saint-Ouen', 'd5j');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (121, 'Centre d''Action Sociale de la Ville de Paris ', 'd5b');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (127, 'Centre communal d''action sociale de Neuilly-sur-Seine', 'd5m');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (129, 'CCAS D''ALFORTVILLE', 'd5n');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (131, 'Société de Coordination pour l''Habitat', 'd5o');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (133, 'Lycée Jean-Baptiste Say-groupement de commande', 'd5p');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (135, 'Lycée Voltaire', 'd5r');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (137, 'Centre d’Action Sociale de la Ville de Paris ', 'casvp');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (139, 'Autres collectivités de plateformes partenaires', 'autre-93');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (141, 'Conseil régional d''Ile-de-France - Formation professionnelle', 'aof');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (143, 'GIP BULAC', 'd5q');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (145, 'Ville de Saintry-sur-Seine', 'd5s');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (149, 'Société d’Économie Mixte - Essonne Aménagement', 'sem91');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (153, 'Ville de Paris / Mairie de Paris', 'v-paris');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (155, 'Préfecture de Police de Paris', 'pref-paris');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (157, 'Département de Paris', 'cg-75');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (159, 'Mairie de Grez sur Loing', 'd5v');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (161, 'Syndicat des Eaux d''Ile de France', 'sedif');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (163, 'La Ville d''Antony', 'd5w');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (167, 'Syndicat Mixte Val d''Oise Numérique', 'd5y');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (173, 'Ville de Montreuil', 'd6a');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (175, 'APIS DEVELOPPEMENT PEPINIERE D''ENTREPRISES', 'd6b');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (179, 'Syndicat des Eaux d''Ile de France - lien externe', 'sedif_imp');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (183, 'Syndicat des Eaux de la Presqu''Ile de Gennevilliers', 'd6f');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (185, 'Département / Département de Seine-Saint-Denis', 'cd-93-EPM');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (187, 'Département / Conseil départemental de Seine-Saint-Denis', 'cd-93-rsem');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (75, 'OPH DE MALAKOFF', 'b3x');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (97, 'Office public de l''habitat d''Aubervilliers', 'b4i');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (101, 'Ville de Stains', 'b4l');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (105, 'Ville de Pierrefitte-sur-Seine', 'b4q');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (107, 'CCAS de Pierrefitte-sur-Seine', 'b4p');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (109, 'SPL PLAINE COMMUNE DEVELOPPEMENT', 'd5g');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (111, 'Ville de Fresnes', 'd5f');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (115, 'Ville de La Courneuve', 'd5c');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (117, 'Association Communauté CapDémat', 'd5i');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (123, 'Mairie d''Arcueil', 'd5k');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (125, 'Syndicat Seine et Marne Numérique', 'd5l');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (147, 'OFFICE PUBLIC DE L''HABITAT DE VILLENEUVE SAINT GEORGES', 'd5t');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (151, 'VILLE DE COURDIMANCHE', 'd5u');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (165, 'Communauté d''Agglomération Rambouillet Territoires', 'd5x');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (169, 'Lycée Maximilien Sorre', 'd5z');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (177, 'CCAS DE LA VILLE D''ANTONY', 'd6d');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (181, 'Association d''étude ambition olympique et paralympique', 'd6e');
insert into referentiel.epm__t_ref_organisme( id_organisme, libelle, acronyme)  values (189, 'OPH VAL DU LOING HABITAT', 'd6g');


















