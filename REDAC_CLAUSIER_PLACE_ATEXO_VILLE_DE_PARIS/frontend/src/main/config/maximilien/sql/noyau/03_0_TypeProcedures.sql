-- TYPES DE PROCEDURE --

-- Suppression des relations types de procedures / articles CMP --
DELETE FROM referentiel.epm__t_ref_procedure_has_epm__t_ref_article;

-- Suppression des articles CMP --
-- Mise à jour de la séquence de numérotation des articles CMP --
DELETE FROM referentiel.epm__t_ref_article; 
SELECT setval('referentiel.epm__t_ref_article_id_article_seq', 1, false);

-- Suppression des types de marchés --
-- Mise à jour de la sequence de numérotation des types de marchés --
DELETE FROM referentiel.epm__t_ref_type_marche;
SELECT setval('referentiel.epm_t_ref_type_marche_id_type_marche_seq', 1, false);

-- Suppression type de contrat et etapes calendrier --
DELETE FROM referentiel.epm__t_ref_type_contrat;
DELETE FROM referentiel.epm__t_ref_etape_calendrier;

-- Suppression de toutes les procédures --
DELETE FROM referentiel.epm__t_ref_procedure; 
-- Mise à jour de la séquence de numérotation des types de procédures--
DELETE FROM referentiel.epm__t_ref_type_consultation; 
SELECT setval('referentiel.epm__t_ref_procedure_id_procedure_seq', 1, false);

-- Création des types de marchés --
INSERT INTO referentiel.epm__t_ref_type_marche(id_type_marche, libelle, id_procedure, actif)VALUES (nextval('referentiel.epm_t_ref_type_marche_id_type_marche_seq'), 'Marchés formalisés', (SELECT id_procedure FROM referentiel.epm__t_ref_procedure WHERE acronyme='AV-DSP'), true);
INSERT INTO referentiel.epm__t_ref_type_marche(id_type_marche, libelle, id_procedure, actif)VALUES (nextval('referentiel.epm_t_ref_type_marche_id_type_marche_seq'), 'MAPA avec publicité', (SELECT id_procedure FROM referentiel.epm__t_ref_procedure WHERE acronyme='AV'), true);
INSERT INTO referentiel.epm__t_ref_type_marche(id_type_marche, libelle, id_procedure, actif)VALUES (nextval('referentiel.epm_t_ref_type_marche_id_type_marche_seq'), 'MAPA sans publicité', (SELECT id_procedure FROM referentiel.epm__t_ref_procedure WHERE acronyme='AV'), true);

-- Création des articles CMP --
INSERT INTO referentiel.epm__t_ref_article (id_article, acronyme, libelle, code_externe) VALUES (nextval('referentiel.epm__t_ref_article_id_article_seq'), '28', '28', '1' );
