-- Paramétrage des documents pour ALPI -- 


-- Effacement des documents existants --
DELETE FROM epm__t_ref_type_document WHERE  id !=1 ;

-- Mise en place du paramétrage client --


INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (2, 'RC', 'pdf', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', FALSE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (3, 'CCAP', 'pdf', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', TRUE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (4, 'AE', 'doc', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', FALSE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (5, 'CCAP-AE', 'doc', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', TRUE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (6, 'CCP', 'pdf', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', TRUE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (7, 'CCP-AE', 'doc', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', TRUE);
INSERT INTO epm__t_ref_type_document(id, libelle, extension_fichier, template, sommaire, template_tableau_derogation, activer_derogation) VALUES (8, 'CCTP', 'pdf', 'RedactionTemplateRSEM.odt', TRUE, 'tableauDerogations.xml', FALSE);
