module.exports = function () {
    var config = {
        app: 'RSEM',
        dist: 'dist/',
        src: 'src/',
        sass: {
            src: './charte/src/sass/',
            input: './charte/src/sass/theme/default/style.scss',
            output: './src/main/config/commun/charte_graphique/commune/css/',
            watch: ['./charte/src/sass/**/*.sass', './charte/src/sass/**/*.scss']
        },
        js: {
            src: './charte/src/js/',
            input: './charte/src/js/**/*.js',
            output: './src/main/config/commun/charte_graphique/commune/js/',
            watch: ['./charte/src/js/**/*.js']
        },
        img: {
            src: './charte/src/img/',
            input: './charte/src/img/**/*.*',
            output: './src/main/config/commun/charte_graphique/commune/img/',
            watch: ['./charte/src/img/*']
        },
        lib: [
            {
                src: 'bower_components/jquery/dist/jquery.min.js',
                dist: './src/main/config/commun/charte_graphique/commune/lib/jquery/'
            },
            {
                src: 'bower_components/bootstrap/dist/js/bootstrap.min.js',
                dist: './src/main/config/commun/charte_graphique/commune/lib/bootstrap/'
            },
            {
                src: 'bower_components/font-awesome/fonts/*',
                dist: './src/main/config/commun/charte_graphique/commune/fonts/awesome/'
            },
            {
                src: 'bower_components/moment/min/moment.min.js',
                dist: './src/main/config/commun/charte_graphique/commune/lib/moment/'
            },
            {
                src: 'bower_components/moment/locale/fr.js',
                dist: './src/main/config/commun/charte_graphique/commune/lib/moment/locale/'
            },
            {
                src: 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                dist: './src/main/config/commun/charte_graphique/commune/lib/datetimepicker/'
            },
            {
                src: 'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                dist: './src/main/config/commun/charte_graphique/commune/lib/datetimepicker/'
            },
            {
                src: 'bower_components/bootstrap-select/js/bootstrap-select.js',
                dist: 'src/js/plugins/'
            },

            {
                src: 'bower_components/moment/min/moment.min.js',
                dist: './charte/src/js/lib/',
                basename: '01-moment.min'
            },
            {
                src: 'bower_components/moment/locale/fr.js',
                dist: './charte/src/js/lib/',
                basename: '02-moment.locale.fr'
            },
            {
                src: 'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                dist: './charte/src/js/lib/',
                basename: '03-bootstrap-datetimepicker.min'
            },
            {
                src: 'bower_components/multiselect/js/jquery.multi-select.js',
                dist: './charte/src/js/lib/',
                basename: '04-jquery.multi-select'
            }
        ]
    };
    return config;
}