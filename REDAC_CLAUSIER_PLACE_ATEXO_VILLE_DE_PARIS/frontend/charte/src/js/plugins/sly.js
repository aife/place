(function ($) {


    var ATXSLIDE = {

        fn: {
            toggleBtnNav: function ($this, obj) {
                var currentIndex = parseInt($this.attr('atx-slide-index'));

                $this.removeClass('first last');

                if (currentIndex >= (obj.nth - obj.slidesToShow)) {
                    $this.addClass('last');
                }
                if (currentIndex <= 0) {
                    $this.addClass('first');
                }

            },
            slider: function ($this, obj) {

                ATXSLIDE.fn.toggleBtnNav($this, obj);


                $this.find('.btn-nav').on('click', function (event) {
                    event.preventDefault();

                    var $btn = $(this),
                        currentIndex = parseInt($this.attr('atx-slide-index')),
                        nextIndex = 0,
                        marginLeft = 0;

                    if ($btn.hasClass('btn-next')) {
                        nextIndex = currentIndex + 1;
                        if (nextIndex > (obj.nth - obj.slidesToShow)) {
                            nextIndex = obj.nth - obj.slidesToShow;
                            $this.removeClass('first').addClass('last');
                        }
                    } else {
                        nextIndex = currentIndex - 1;
                        if (nextIndex < 0) {
                            nextIndex = 0;
                            $this.removeClass('last').addClass('first');
                        }
                    }

                    $this.attr('atx-slide-index', nextIndex);

                    marginLeft = nextIndex * obj.itemWidth;

                    ATXSLIDE.fn.toggleBtnNav($this, obj);

                    obj.ul.css("marginLeft", function (index) {
                        return -(nextIndex * obj.itemWidth);
                    });

                    return false;
                });
            },
            init: function (selector) {

                $(selector).each(function () {

                    var $this = $(this),
                        $ul = $this.find('.items'),
                        borderSize = 2,
                        nth = $this.find('.atx-lot_status').length,
                        itemWidth = $this.find('.atx-lot_status:eq(0)').innerWidth() + borderSize,
                        slidesToShow = Math.floor($this.parents('.atx-table_header-cols_status').width() / itemWidth);

                    var obj = {
                        ul: $ul,
                        nth: nth,
                        itemWidth: itemWidth,
                        slidesToShow: slidesToShow,
                        currentIndex: 0
                    };

                    // init current index
                    $this.attr('atx-slide-index', '0');

                    // init container width
                    $this.width((slidesToShow * itemWidth) - borderSize - slidesToShow);

                    // init items list width
                    $ul.width((itemWidth * nth) - borderSize);

                    // if need to show slide
                    if (nth > slidesToShow) {
                        // init items list width
                        $this.find('.btn-nav').addClass('show');
                    } else {
                        $this.find('.btn-nav').removeClass('show');
                    }

                    ATXSLIDE.fn.slider($this, obj);

                });


            }
        },

        resize: function (selector) {
            ATXSLIDE.fn.init(selector);
            $(window).resize(function () {
                ATXSLIDE.fn.init(selector);
            });
        },

        ready: function () {
            $(document).ready(function () {

                ATXSLIDE.resize('.atx-js-list_pli-status');

            });

        }
    }

    ATXSLIDE.ready();


})(jQuery);