package fr.atexo.commun.util;

import fr.atexo.commun.exception.AtexoRuntimeException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;

/**
 * Classe utilitaire permettant de gérer les fichiers temporaires (basé sur la classe venant de l'application Alfresco)
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class FichierTemporaireUtil {
    /**
     * sous répertoire dans lequel sera stocké les fichiers temporaires
     */
    public static final String ATEXO_REPERTOIRE_TEMPORAIRE = "atexo";

    /**
     * la propriété système pour récupérer le répertoire temporaire de stockage système
     */
    public static final String SYSTEM_REPERTOIRE_TEMPORAIRE = "java.io.tmpdir";

    private static final Log logger = LogFactory.getLog(FichierTemporaireUtil.class);

    /**
     * Classe statique uniquement
     */
    private FichierTemporaireUtil() {
    }

    /**
     * Récupère le répertoire java temporaire : java.io.tempdir
     *
     * @return Retourne le répertoire système temporaire. <code>isDir == true</code>
     */
    public static File getRepertoireSystemeTemporaire() {
        String systemTempDirPath = System.getProperty(SYSTEM_REPERTOIRE_TEMPORAIRE);
        if (systemTempDirPath == null) {
            throw new AtexoRuntimeException("Propriété système non disponible : " + SYSTEM_REPERTOIRE_TEMPORAIRE);
        }
        File systemTempDir = new File(systemTempDirPath);
        return systemTempDir;
    }

    /**
     * Récupère le répertoire temporaire atexo, par defaut %java.io.tempdir%/atexo.
     * Le répertoire sera crée automatiquement si celui-ci n'existe pas déjà.
     *
     * @return Retourne un répertoire temporaire. <code>isDir == true</code>
     */
    public static File getRepertoireTemporaire() {
        return getRepertoireTemporaire(ATEXO_REPERTOIRE_TEMPORAIRE, true);
    }

    /**
     * Récupère le répertoire temporaire spécifié.Si repertoireSysteme est vrai alors le répertoire sera crée dans %java.io.tempdir% ou  %java.io.tempdir%/atexo.
     * Le répertoire sera crée automatiquement si celui-ci n'existe pas déjà.
     *
     * @return Retourne un répertoire temporaire. <code>isDir == true</code>
     */
    public static File getRepertoireTemporaire(String nomRepertoire, boolean repertoireSysteme) {
        File systemTempDir = repertoireSysteme ? getRepertoireSystemeTemporaire() : getRepertoireTemporaire();
        // contruction du chemin vers le répertoire temporaire
        File tempDir = new File(systemTempDir, nomRepertoire);
        // s'assure que le fichier temporaire existe bien
        if (tempDir.exists()) {
            // rien a faire
        } else {
            // n'existe pas encore
            if (!tempDir.mkdirs()) {
                throw new AtexoRuntimeException("Le répertoire temporaire n'a pu être crée : " + tempDir);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Creation du répertoire temporaire : " + tempDir);
            }
        }
        // ok
        return tempDir;
    }

    /**
     * Crée un fichier temporaire dans le répertoire temporaire atexo.
     *
     * @return Retourne un <code>File</code> temporaire qui sera localisé dans le sous-répertoire <b>atexo</b> du répertoire temporaire par défaut.
     * @see #ATEXO_REPERTOIRE_TEMPORAIRE
     * @see File#createTempFile(java.lang.String, java.lang.String)
     */
    public static File creerFichierTemporaire(String prefixe, String suffixe) {
        File tempDir = FichierTemporaireUtil.getRepertoireTemporaire();

        return creerFichierTemporaire(prefixe, suffixe, tempDir);
    }

    /**
     * @return Retourne un <code>File</code> temporaire qui sera localisé dans le répertoire spécifié.
     * @see #ATEXO_REPERTOIRE_TEMPORAIRE
     * @see File#createTempFile(java.lang.String, java.lang.String)
     */
    public static File creerFichierTemporaire(String prefixe, String suffixe, File repertoire) {
        try {
            File tempFile = File.createTempFile(prefixe, suffixe, repertoire);
            return tempFile;
        } catch (IOException e) {
            throw new AtexoRuntimeException("Erreur lors de la création du fichier temporaire : \n" +
                    "   prefixe : " + prefixe + "\n"
                    + "   suffixe : " + suffixe + "\n" +
                    "   répertoire : " + repertoire,
                    e);
        }
    }

    /**
     * Supprime tous les fichiers temporaires localisés dans le répertoires temporaire <code>atexo</code>.
     * La suppression sera effectué en cascade.
     *
     * @return Retourne le nombre de fichiers supprimés
     */
    public static int supprimerFichierTemporaires() {
        File tempDir = FichierTemporaireUtil.getRepertoireTemporaire();
        return supprimerFichiers(tempDir, false);
    }

    /**
     * Supprime en cascade tous les fichiers se trouvant dans le répertoire spécifié ainsi que ses sous-répertoires et leurs contenu.
     *
     * @param repertoire          Le répertoire à néttoyer. Peut être supprimer en option.
     * @param supprimerRepertoire true si le répertoires doit aussi être supprimé sinon false.
     * @return Retourne le nombre de fichiers supprimés
     */
    private static int supprimerFichiers(File repertoire, boolean supprimerRepertoire) {

        if (!repertoire.isDirectory()) {
            throw new IllegalArgumentException("Un répertoire était attendu en argument de la fonction : " + repertoire);
        }
        // vérification
        if (!repertoire.exists()) {
            return 0;
        }
        // liste des fichiers
        File[] files = repertoire.listFiles();
        int count = 0;
        for (File file : files) {

            // vérification fichier / répertoire
            if (file.isDirectory()) {
                // on boucle
                supprimerFichiers(file, true);

            } else {
                // si c'est un fichier alors on le supprime
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Suppresion du fichier temporaire : " + file);
                    }
                    file.delete();
                    count++;
                }
                catch (Throwable e) {
                    logger.info("Erreur lors de la tentative de suppression du fichier temporaire : " + file);
                }
            }
        }

        // suppression du répertoire temporaire racine ?
        if (supprimerRepertoire) {
            // suppression si vide
            try {
                File[] listing = repertoire.listFiles();
                if (listing != null && listing.length == 0) {
                    // le répertoire est vide
                    repertoire.delete();
                }
            }
            catch (Throwable e) {
                logger.info("Erreur lors de la tentative de suppression du répertoire temporaire : " + repertoire, e);
            }
        }
        // done
        return count;
    }

}