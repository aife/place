package fr.atexo.commun.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class MessageResourceBundle {

    private static final String RESOURCE_PAR_DEFAUT = "messages";

    private static ResourceBundle resourceBundle;

    public static ResourceBundle getInstance(String resource, Locale locale) {

        if (resourceBundle == null || resourceBundle.getLocale().equals(locale)) {
            resourceBundle = ResourceBundle.getBundle(resource, locale);
        }

        return resourceBundle;
    }

    public static ResourceBundle getInstance(Locale locale) {

        return getInstance(RESOURCE_PAR_DEFAUT, locale);
    }

    public static ResourceBundle getInstance() {

        return getInstance(RESOURCE_PAR_DEFAUT, Locale.getDefault());
    }
}
