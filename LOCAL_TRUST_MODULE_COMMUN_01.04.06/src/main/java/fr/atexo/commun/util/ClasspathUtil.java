/**
 * $Id$
 */
package fr.atexo.commun.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public class ClasspathUtil {
    
    private static final Log LOG = LogFactory.getLog(ClasspathUtil.class);

    public static List<InputStream> searchResources(String resource) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        List<InputStream> inputStreams = new ArrayList<InputStream>();
        Enumeration<URL> enumeration = loader.getResources(resource);
        while (enumeration.hasMoreElements()) {
            InputStream i = getInputStreamFormUrl(enumeration.nextElement());
            if (i != null) {
                inputStreams.add(i);
            }
        }
        return inputStreams;
    }

    private static InputStream getInputStreamFormUrl(URL url) {
        if (url != null) {

            try {
                URLConnection connection = url.openConnection();
                return connection.getInputStream();
            } catch (IOException e) {
                //should not happen
                LOG.error(e);
            }
        }
        return null;
    }

    public static URL searchURL(String resource) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        URL url = loader.getResource(resource);
        if (url == null) {
            loader = ClassLoader.getSystemClassLoader();
            url = loader.getResource(resource);
        }
        return url;
    }

    public static InputStream searchResource(String resource) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        InputStream result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        loader = ClassLoader.getSystemClassLoader();

        result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        throw new IOException("Aucun class loader n'a été trouvé pour charger la resource nommé : " + resource);
    }
}
