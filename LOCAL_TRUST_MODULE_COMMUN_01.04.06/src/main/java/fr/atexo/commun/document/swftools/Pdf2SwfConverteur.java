package fr.atexo.commun.document.swftools;

import fr.atexo.commun.exception.ConfigurationException;
import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class Pdf2SwfConverteur {

    private static String SYNTAXE_COMMANDE = "${swf.exe} -f -t -G -T ${flashVersion} ${source} -o ${destination}";

    private static String SYNTAXE_COMMANDE_VERSION = "${swf.exe} -V";

    public static ExecutionCommande.ExecutionResultat convertir(Pdf2SwfConfiguration pdf2SwfConfiguration, File fichierSource, File fichierDestination) throws IOException {

        ExecutionCommande.ExecutionResultat resultat = null;

        if (pdf2SwfConfiguration != null && pdf2SwfConfiguration.getPdf2swf() != null && pdf2SwfConfiguration.getVersionFlash() >= 9) {

            if (fichierSource != null && fichierSource.exists()) {

                //TODO vérifier le fichier Source
                ExecutionCommande commande = new ExecutionCommande();
                commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE});

                Map<String, String> proprietesParDefaut = new HashMap<String, String>();
                proprietesParDefaut.put("swf.exe", pdf2SwfConfiguration.getPdf2swf());
                proprietesParDefaut.put("flashVersion", String.valueOf(pdf2SwfConfiguration.getVersionFlash()));

                proprietesParDefaut.put("source", fichierSource.getAbsolutePath());
                proprietesParDefaut.put("destination", fichierDestination.getAbsolutePath());

                // exécution de la commande
                try {
                    resultat = commande.executer(proprietesParDefaut);
                }
                catch (Throwable e) {
                    throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
                }

                // vérification
                if (!resultat.isExecuterAvecSucces()) {
                    throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
                }

                // vérification si le fichier a bien été crée
                if (!fichierDestination.exists()) {
                    throw new ContenuIOException("Erreur lors de la transformation - le fichier de destination n'existe pas : \n" + resultat);
                }
            } else {
                throw new ContenuIOException("Erreur avec le fichier source : Celui-ci n'existe pas");
            }

        } else {
            throw new ConfigurationException("Erreur de configuration du service pdf2Swf : " + pdf2SwfConfiguration.toString());
        }

        return resultat;
    }


    public static ExecutionCommande.ExecutionResultat getVersion(Pdf2SwfConfiguration pdf2SwfConfiguration) throws IOException {

        ExecutionCommande.ExecutionResultat resultat = null;

        if (pdf2SwfConfiguration != null && pdf2SwfConfiguration.getPdf2swf() != null) {

            ExecutionCommande commande = new ExecutionCommande();
            commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE_VERSION});

            Map<String, String> proprietesParDefaut = new HashMap<String, String>();
            proprietesParDefaut.put("swf.exe", pdf2SwfConfiguration.getPdf2swf());

            // exécution de la commande
            try {
                resultat = commande.executer(proprietesParDefaut);
            }
            catch (Throwable e) {
                throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
            }

            // vérification
            if (!resultat.isExecuterAvecSucces()) {
                throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
            }

        } else {
            throw new ConfigurationException("Erreur de configuration du service pdf2Swf : " + pdf2SwfConfiguration.toString());
        }

        return resultat;
    }

}
