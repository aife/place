package fr.atexo.commun.document.gstools;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.atexo.commun.exception.ConfigurationException;
import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;

/**
 * Created by IntelliJ IDEA.
 * User: bruno
 * Date: 26 oct. 2010
 * Time: 15:39:57
 * To change this template use File | Settings | File Templates.
 */
public class Pdf2GsConverteur {

    /**
     * -q Initialization silencieuse: suppression des messages d'initialization de l'application. Fait la fonctionnalité de dQUIET.
     * -rnumero1xnumero2 Resolution pour le dispositif de sortie
     * -sDEVICE Dispositif pour la sortie. Valeurs possibles: epson, jpeg png16m
     * -dTextAlphaBits -dGraphicsAlphaBits Ces paramètres sont recommandés pour obtenir une bonne qualité pour la rasterization. Valeurs: 1,2,4
     * -dDEVICEWIDTHPOINTS=w   Largeur de la feiulle imprimé
     * -dDEVICEHEIGHTPOINTS=h Hauteur de  la feiulle imprimé
     * -sPAPERSIZE Taille de la feuille. Valeurs: a4, legal
     * <p/>
     * Example de ligne de commande:
     * gs -q -dQUIET -dPARANOIDSAFER -dBATCH -dNOPAUSE -dNOPROMPT -dFirstPage=4 -dLastPage=2 -sDEVICE=png16m -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r300x300 -sOutputFile=out%d.png in.pdf
     */
    private static String SYNTAXE_COMMANDE = "${gs.exe} -q -dQUIET -dPARANOIDSAFER -dBATCH -dNOPAUSE -dNOPROMPT -dFirstPage=${start} -dLastPage=${stop} -sDEVICE=${sdevice} -dTextAlphaBits=${textalphabits} -dGraphicsAlphaBits=${graphicalphabits} -sOutputFile=${destination} ${source}";

    private static String SYNTAXE_COMMANDE_VERSION = "${gs.exe} -v";

    public Pdf2GsConverteur() {
    }

    public ExecutionCommande.ExecutionResultat convertir(Pdf2GsConfiguration pdf2GsConfiguration, File fichierSource, String nomFichierDestination) throws IOException {

        if (pdf2GsConfiguration != null && pdf2GsConfiguration.getPdf2gs() != null) {

            if (fichierSource != null && nomFichierDestination != null) {

                //TODO vérifier le fichier Source
                ExecutionCommande commande = new ExecutionCommande();
                commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE});

                Map<String, String> proprietesParDefaut = new HashMap<String, String>();
                proprietesParDefaut.put("gs.exe", pdf2GsConfiguration.getPdf2gs());
                proprietesParDefaut.put("start", String.valueOf(pdf2GsConfiguration.getPageDebut()));
                proprietesParDefaut.put("stop", String.valueOf(pdf2GsConfiguration.getPageFin()));
                proprietesParDefaut.put("sdevice", String.valueOf(pdf2GsConfiguration.getsDevice()));
                proprietesParDefaut.put("textalphabits", String.valueOf(pdf2GsConfiguration.getTextAlphaBits()));
                proprietesParDefaut.put("graphicalphabits", String.valueOf(pdf2GsConfiguration.getGraphicAlphaBits()));

                proprietesParDefaut.put("source", fichierSource.getAbsolutePath());
                proprietesParDefaut.put("destination", nomFichierDestination);

                // exécution de la commande
                ExecutionCommande.ExecutionResultat resultat = null;
                try {
                    resultat = commande.executer(proprietesParDefaut);

                    // vérification
                    if (!resultat.isExecuterAvecSucces()) {
                        throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
                    }

                    return resultat;

                }
                catch (Throwable e) {
                    throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
                }

            } else {
                throw new ContenuIOException("Erreur avec le fichier source : Celui-ci n'existe pas");
            }

        } else {
            throw new ConfigurationException("Erreur de configuration du service pdf2Gs : " + pdf2GsConfiguration.toString());
        }


    }


    public static ExecutionCommande.ExecutionResultat getVersion(Pdf2GsConfiguration pdf2GsConfiguration) throws IOException {

        ExecutionCommande.ExecutionResultat resultat = null;

        if (pdf2GsConfiguration != null && pdf2GsConfiguration.getPdf2gs() != null) {

            ExecutionCommande commande = new ExecutionCommande();
            commande.setCommandes(new String[]{ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE_VERSION});

            Map<String, String> proprietesParDefaut = new HashMap<String, String>();
            proprietesParDefaut.put("gs.exe", pdf2GsConfiguration.getPdf2gs());

            // exécution de la commande
            try {
                resultat = commande.executer(proprietesParDefaut);
            }
            catch (Throwable e) {
                throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
            }

            // vérification
            if (!resultat.isExecuterAvecSucces()) {
                throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
            }

        } else {
            throw new ConfigurationException("Erreur de configuration du service pdf2Gs : " + pdf2GsConfiguration.toString());
        }

        return resultat;
    }


}
