package fr.atexo.commun.document.gstools;

/**
 * Created by IntelliJ IDEA.
 * User: bruno
 * Date: 26 oct. 2010
 * Time: 15:40:34
 * To change this template use File | Settings | File Templates.
 */
public class Pdf2GsConfiguration {

    private String pdf2gs;
    private int pageDebut;
    private int pageFin;
    private String sDevice;
    private String textAlphaBits;
    private String graphicAlphaBits;

    public Pdf2GsConfiguration(String pdf2gs, int pageDebut, int pageFin, String sDevice, String textAlphaBits, String graphicAlphaBits) {
        this.pdf2gs = pdf2gs;
        this.pageDebut = pageDebut;
        this.pageFin = pageFin;
        this.sDevice = sDevice;
        this.textAlphaBits= textAlphaBits;
        this.graphicAlphaBits = graphicAlphaBits;
    }

    public Pdf2GsConfiguration() {
    }
    
    public String getPdf2gs() {
        return pdf2gs;
    }

    public void setPdf2gs(String pdf2gs) {
        this.pdf2gs = pdf2gs;
    }

    public int getPageDebut() {
        return pageDebut;
    }

    public void setPageDebut(int pageDebut) {
        this.pageDebut = pageDebut;
    }

    public int getPageFin() {
        return pageFin;
    }

    public void setPageFin(int pageFin) {
        this.pageFin = pageFin;
    }
    
    public String getsDevice() {
        return sDevice;
    }

    public void setsDevice(String sDevice) {
        this.sDevice = sDevice;
    }

    public String getTextAlphaBits() {
        return textAlphaBits;
    }

    public void setTextAlphaBits(String textAlphaBits) {
        this.textAlphaBits = textAlphaBits;
    }

    public String getGraphicAlphaBits() {
        return graphicAlphaBits;
    }

    public void setGraphicAlphaBits(String graphicAlphaBits) {
        this.graphicAlphaBits = graphicAlphaBits;
    }

    @Override
    public String toString() {
        return "pdf2swf=" + this.pdf2gs + " / Page Début=" + this.pageDebut + " / Page Fin=" + this.pageFin + " / sDevice=" + this.sDevice + " / textAlphaBits=" + this.textAlphaBits + " / graphicAlphaBits=" + this.graphicAlphaBits;
    }
}
