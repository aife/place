package fr.atexo.commun.document.wkhtmltopdftools;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

import fr.atexo.commun.exception.ConfigurationException;
import fr.atexo.commun.exception.ContenuIOException;
import fr.atexo.commun.util.exec.ExecutionCommande;

public class WkhtmltopdfConverteur {

  private static String SYNTAXE_COMMANDE = "${wkhtmltopdf.exe} --outline-depth 0 --stop-slow-scripts ${source} ${destination}";

  public static ExecutionCommande.ExecutionResultat convertir(WkhtmltopdfConfiguration wkhtmltopdfConfiguration, File fichierSource, File fichierDestination) throws IOException {

    ExecutionCommande.ExecutionResultat resultat = null;

    if (wkhtmltopdfConfiguration != null && wkhtmltopdfConfiguration.getWkhtmltopdf() != null) {

      // contrainte wkhtmltopdf
      if (!FilenameUtils.getExtension(fichierSource.getName()).toLowerCase().contains("htm")) {
        throw new ContenuIOException("L'extension du fichier source doit être .html");
      }

      if (fichierSource != null && fichierSource.exists()) {

        // TODO vérifier le fichier Source
        ExecutionCommande commande = new ExecutionCommande();
        commande.setCommandes(new String[] {ExecutionCommande.DIRECTIVE_COUPURE + SYNTAXE_COMMANDE});

        Map<String, String> proprietesParDefaut = new HashMap<String, String>();
        proprietesParDefaut.put("wkhtmltopdf.exe", wkhtmltopdfConfiguration.getWkhtmltopdf());

        proprietesParDefaut.put("source", fichierSource.getAbsolutePath());
        proprietesParDefaut.put("destination", fichierDestination.getAbsolutePath());

        // exécution de la commande
        try {
          resultat = commande.executer(proprietesParDefaut);
        } catch (Throwable e) {
          throw new ContenuIOException("Erreur lors de l'exécution de la commande : \n" + commande, e);
        }

        // vérification
        if (!resultat.isExecuterAvecSucces()) {
          throw new ContenuIOException("Erreur lors de la transformation - statut d'erreur : \n" + resultat);
        }

        // vérification si le fichier a bien été crée
        if (!fichierDestination.exists()) {
          throw new ContenuIOException("Erreur lors de la transformation - le fichier de destination n'existe pas : \n" + resultat);
        }
      } else {
        throw new ContenuIOException("Erreur avec le fichier source : Celui-ci n'existe pas");
      }

    } else {
      throw new ConfigurationException("Erreur de configuration du service wkhtmltopdf : " + wkhtmltopdfConfiguration.toString());
    }

    return resultat;
  }

}
