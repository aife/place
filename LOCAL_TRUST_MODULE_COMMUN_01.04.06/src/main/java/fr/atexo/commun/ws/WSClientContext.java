package fr.atexo.commun.ws;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Classe permettant de definir les attribut d'un context client
 * L'application utilisant le WsClientManager doit implementer le context WsClientContext pour initialiser le manager.
 * L'implementation de la classe WsClientContext doit initialiser tous les parametres definis dans WsClientContext
 */
public abstract class WSClientContext {

    /**
     * url du web service
     */
    private String baseUrl;
    /**
     * login utilisé pour l'authentification lors de l'appel du web service. le login sera passé dans le parametre login
     * de l'url du webservice
     */
    private String login;
    /**
     * mot de passe utilisé pour l'authentification lors de l'appel du web service. le mot de passe sera passé dans le
     * parametre motpasse de l'url du webservice
     */
    private String password;


    public final URI getBaseURI() {
        return UriBuilder.fromUri(baseUrl).build();
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    protected void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getLogin() {
        return login;
    }

    protected void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    protected void setPassword(String password) {
        this.password = password;
    }

}
