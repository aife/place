/**
 * $Id$
 */
package fr.atexo.commun.jaxb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Commentaire
 *
 * @version $Revision$ $Date$
 */
public final class JAXBService implements Serializable {

    private static final long serialVersionUID = 986110064372345330L;

    private static final Log LOG = LogFactory.getLog(JAXBService.class);

    private static final String ENCODING = "UTF-8";

    private static final JAXBService INSTANCE = new JAXBService();

    private Map<String, JAXBContext> contextMap;
    private Map<String, Queue<Unmarshaller>> unmarshallerMap;
    private Map<String, Queue<Marshaller>> marshallerMap;

    public static JAXBService instance() {
        return INSTANCE;
    }

    private JAXBService() {
        contextMap = new ConcurrentHashMap<String, JAXBContext>();
        unmarshallerMap = new ConcurrentHashMap<String, Queue<Unmarshaller>>();
        marshallerMap = new ConcurrentHashMap<String, Queue<Marshaller>>();
    }

    private JAXBContext getContext(String contextPath) {
        JAXBContext context = contextMap.get(contextPath);
        if (context == null) {
            try {
                context = JAXBContext.newInstance(contextPath);
                contextMap.put(contextPath, context);
            } catch (JAXBException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return context;
    }

    private Marshaller getMarshaller(String contextPath) {
        Queue<Marshaller> queue = marshallerMap.get(contextPath);
        if (queue == null) {
            queue = new ConcurrentLinkedQueue<Marshaller>();
            marshallerMap.put(contextPath, queue);
        }
        Marshaller marshaller = queue.poll();
        if (marshaller == null) {
            marshaller = createMarshaller(contextPath);
        }
        return marshaller;
    }

    private Marshaller createMarshaller(String contextPath) {
        JAXBContext context = getContext(contextPath);
        try {
            Marshaller marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_ENCODING, ENCODING);
            return marshaller;
        } catch (JAXBException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    private void retreiveMarshaller(String contextPath, Marshaller marshaller) {
        if (marshaller != null) {
            Queue<Marshaller> queue = marshallerMap.get(contextPath);
            if (queue != null) {
                queue.add(marshaller);
            }
        }
    }

    private Unmarshaller getUnmarshaller(String contextPath) {
        Queue<Unmarshaller> queue = unmarshallerMap.get(contextPath);
        if (queue == null) {
            queue = new ConcurrentLinkedQueue<Unmarshaller>();
            unmarshallerMap.put(contextPath, queue);
        }
        Unmarshaller unmarshaller = queue.poll();
        if (unmarshaller == null) {
            unmarshaller = createUnMarshaller(contextPath);
        }
        return unmarshaller;
    }

    private Unmarshaller createUnMarshaller(String contextPath) {
        JAXBContext context = getContext(contextPath);
        try {
            return context.createUnmarshaller();
        } catch (JAXBException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    private void retreiveUnmarshaller(String contextPath, Unmarshaller unmarshaller) {
        if (unmarshaller != null) {
            Queue<Unmarshaller> queue = unmarshallerMap.get(contextPath);
            if (queue != null) {
                queue.add(unmarshaller);
            }
        }
    }

    public Object getAsObject(String xmlAsString, String contextPath) {
        Unmarshaller unmarshaller = getUnmarshaller(contextPath);

        try {
            ByteArrayInputStream in = new ByteArrayInputStream(xmlAsString.getBytes(ENCODING));
            return unmarshaller.unmarshal(in);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return null;
        } finally {
            retreiveUnmarshaller(contextPath, unmarshaller);
        }
    }

    public String getAsString(Object object, String contextPath) {
        Marshaller marshaller = getMarshaller(contextPath);
        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            marshaller.marshal(object, out);

            return new String(out.toByteArray(), ENCODING);

        } catch (JAXBException e) {
            LOG.error(e.getMessage(), e);
            return null;
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            return null;
        } finally {
            retreiveMarshaller(contextPath, marshaller);
        }
    }


}
