/**
 * $Id$
 */
package fr.atexo.commun.ldap;

/**
 * Classe immuable représentant l'identifiant unique d'un compte.
 * Pour plus de détail, consulter <a href="http://www.commentcamarche.net/contents/ldap/ldapnomm.php3">cette référence</a>.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public final class IdentifiantCompte {

    private final String cheminComplet;
    private final String login;

    /**
     * Permet de créer un identifiant à partir uniquement de son chemin complet
     *
     * @param cheminComplet le chemin complet du compte
     */
    public IdentifiantCompte(String valeurCheminComplet) {
        this.cheminComplet = echaperCheminComplet(valeurCheminComplet);
        this.login = cheminComplet.substring(cheminComplet.indexOf('=') + 1, cheminComplet.indexOf(','));
    }

    /**
     * Permet de créer un identifiant de compte
     *
     * @param cheminComplet exemple : {@literal cn=toto,ou=users,dc=ldaptest,dc=fr,dc=atexo}
     * @param login         le login du compte - exemple : {@literal toto}
     */
    public IdentifiantCompte(String valeurCheminComplet, String login) {
        this.cheminComplet = echaperCheminComplet(valeurCheminComplet);;
        this.login = login;
    }
    
    /**
     * @param valeurCheminComplet le chemin complet.
     * @return le chemin complet prefixé et suffixé par des doublequotes (").
     */
    private String echaperCheminComplet(String valeurCheminComplet){
        final String sequenceEchappement = "\"";
        StringBuffer resultat = new StringBuffer(sequenceEchappement);
        resultat.append(valeurCheminComplet).append(sequenceEchappement);
        return resultat.toString();
        
    }

    /**
     * Pour créer un identifiant au premier niveau du répertoire des comptes.
     *
     * @param parametresCompte les paramètres des comptes
     * @param login            le login du compte
     */
    public IdentifiantCompte(LDAPParametresCompte parametresCompte, String login) {
        this(parametresCompte.getChampIdentifiantCompte() + "=" + login + "," + parametresCompte.getRepertoireCompte(), login);
    }

    /**
     * Le chemin unique (dn) du compte échapé pour ldap. Le chemin est préfixé
     * et suffixé par des doublequotes (") pour gérer le cas des caractères
     * spéciaux dans le chemin, par exemple /.
     * @return le chemin complet
     */
    public String getCheminComplet() {
        return cheminComplet;
    }

    /**
     * Le login du compte
     *
     * @return le login
     */
    public String getLogin() {
        return login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IdentifiantCompte that = (IdentifiantCompte) o;

        if (!cheminComplet.equals(that.cheminComplet)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return cheminComplet.hashCode();
    }

    @Override
    public String toString() {
        return "IdentifiantCompte{" + "cheminComplet='" + cheminComplet + '\'' + '}';
    }
}
