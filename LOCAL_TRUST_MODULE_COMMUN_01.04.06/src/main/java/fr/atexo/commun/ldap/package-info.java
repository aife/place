/**
 * <p>
 * Procure les classes et méthodes permettant d'interagir avec un annuaire LDAP.
 * </p>
 * <p>
 *  L'interface principale est {@link fr.atexo.commun.ldap.LDAPService} qui permet d'accéder aux fonctions d'identification, de modification
 * et de récupération d'information à partir de l'annuaire LDAP.
 * </p>
 * <p>
 *  Des exemples d'utilisation sont disponibles en consultant les tests unitaires, que ce soit en utilisant Spring ou java SE.
 * </p>
 * <p>
 * Pour plus de détails à propos de l'implémentation du dialogue avec l'annuaire LDAP, consultez <a target="_blank" href="http://java.sun.com/products/jndi/tutorial/ldap/index.html">http://java.sun.com/products/jndi/tutorial/ldap/index.html</a>
 * </p>
 */
package fr.atexo.commun.ldap;

