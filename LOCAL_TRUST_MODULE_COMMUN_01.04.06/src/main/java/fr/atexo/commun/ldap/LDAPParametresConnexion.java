/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.io.Serializable;

/**
 * Permet de configurer les paramètres de connexion au LDAP.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public class LDAPParametresConnexion implements Serializable {

    private static final long serialVersionUID = -3330392149990420418L;

    //obligatoires
    private String url;
    private String securityPrincipal;
    private String securityCredentials;

    //optionnels
    private String initialContextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
    private String securityAuthentification = "simple";
    private String connectPool = "true";
    private boolean ssl;
    private boolean ad;
    private boolean ignorerCertificat;

    /**
     * Crée une nouvelle instance de paramètres de connexion.
     *
     * @param url                 l'url de connexion au LDAP - exemple : {@literal ldap://linuxserv3:389/}
     * @param securityPrincipal   le dn du compte d'authentification administrateur - exemple : {@literal cn=Manager,dc=fr,dc=atexo}
     * @param securityCredentials le mot de passe administrateur - exemple : {@literal secret}
     */
    public LDAPParametresConnexion(String url, String securityPrincipal, String securityCredentials) {
        this.url = url;
        this.securityPrincipal = securityPrincipal;
        this.securityCredentials = securityCredentials;
    }

    /**
     * Retourne le mot de passe du compte d'authentification.
     *
     * @return le dn du compte d'authentification
     */
    public String getSecurityCredentials() {
        return securityCredentials;
    }

    /**
     * Modifie le mot de passe du compte d'authentification
     *
     * @param securityCredentials le nouveau mot de passe du compte d'authentification
     */
    public void setSecurityCredentials(String securityCredentials) {
        this.securityCredentials = securityCredentials;
    }

    /**
     * Retourne le dn du compte d'authentification.
     *
     * @return le dn du compte d'authentification
     */
    public String getSecurityPrincipal() {
        return securityPrincipal;
    }

    /**
     * Modifie le dn du compte d'authentification
     *
     * @param securityPrincipal le nouveau dn du compte d'authentification
     */
    public void setSecurityPrincipal(String securityPrincipal) {
        this.securityPrincipal = securityPrincipal;
    }

    /**
     * Retourne l'url de connexion au LDAP
     *
     * @return l'url de connexion au LDAP
     */
    public String getUrl() {
        return url;
    }

    /**
     * Modifie l'url de connexion au LDAP
     *
     * @param url la nouvelle url de connexion au LDAP
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Indique si un pool de connexion est utilisé.
     *
     * @return {@code "true"} ou {@code "false"}, {@code "true"} par défaut
     */
    public String getConnectPool() {
        return connectPool;
    }

    /**
     * Modifie l'utilisation d'un pool de connexion.
     *
     * @param connectPool {@code "true"} ou {@code "false"}
     */
    public void setConnectPool(String connectPool) {
        this.connectPool = connectPool;
    }

    /**
     * Retourne l'{@literal initialContextFactory} utilisé pour se connecter au LDAP.
     *
     * @return l'{@literal initialContextFactory} utilisé pour se connecter au LDAP,
     *         {@literal "com.sun.jndi.ldap.LdapCtxFactory"} par défaut
     */
    public String getInitialContextFactory() {
        return initialContextFactory;
    }

    /**
     * Modifie l'{@literal initialContextFactory} utilisé pour se connecter au LDAP.
     *
     * @param initialContextFactory le nouveau {@literal initialContextFactory} utilisé pour se connecter au LDAP
     */
    public void setInitialContextFactory(String initialContextFactory) {
        this.initialContextFactory = initialContextFactory;
    }

    /**
     * Retourne le mécanisme d'authentification au LDAP.
     *
     * @return le mécanisme d'authentification au LDAP, {@literal "simple"} par défaut
     */
    public String getSecurityAuthentification() {
        return securityAuthentification;
    }

    /**
     * Modifie le mécanisme d'authentification au LDAP.
     *
     * @param securityAuthentification le nouveau mécanisme d'authentification au LDAP
     */
    public void setSecurityAuthentification(String securityAuthentification) {
        this.securityAuthentification = securityAuthentification;
    }

    /**
     * Indique si le serveur ldap est de type AD ou non
     *
     * @return {@code true} si le ldap est un AD, {@code false} sinon
     */
    public boolean isAd() {
        return ad;
    }

    /**
     * Modifie le type du serveur ldap.
     *
     * @param ad le type du serveur ldap (ad ou non)
     */
    public void setAd(boolean ad) {
        this.ad = ad;
    }

    /**
     * Indique si la connexion est de type ssl
     *
     * @return {@code true} si la connexion est de type ssl, {@code false} sinon
     */
    public boolean isSsl() {
        return ssl;
    }

    /**
     * Modifie le type de connexion (ssl ou non)
     *
     * @param ssl {@code true} si la connexion est de type ssl, {@code false} sinon
     */
    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    /**
     * Indique si la connexion ssl doit éviter le certificat.
     *
     * @return {@code true} si la connexion de type ssl doit ignorer la validation par certificat, {@code false} sinon
     */
    public boolean isIgnorerCertificat() {
        return ignorerCertificat;
    }

    /**
     * Modifie le fait d'éviter ou non le certificat en mode ssl.
     *
     * @param ignorerCertificat {@code true} si la connexion de type ssl doit ignorer la validation par certificat, {@code false} sinon
     */
    public void setIgnorerCertificat(boolean ignorerCertificat) {
        this.ignorerCertificat = ignorerCertificat;
    }

    @Override
    public String toString() {
        return "LDAPParametresConnexion{" + "connectPool='" + connectPool + '\'' + ", url='" + url + '\'' + ", securityPrincipal='"
                + securityPrincipal + '\'' + ", securityCredentials='" + securityCredentials + '\'' + ", initialContextFactory='"
                + initialContextFactory + '\'' + ", securityAuthentification='" + securityAuthentification + '\'' + ", ad=" + ad + '\'' + ", ssl=" + ssl + '}';
    }
}
