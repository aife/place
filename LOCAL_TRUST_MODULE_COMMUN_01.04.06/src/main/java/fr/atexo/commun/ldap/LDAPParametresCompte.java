/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.directory.SearchControls;

/**
 * Permet de configurer les paramètres de création, modification et récupération d'informations des comptes.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public class LDAPParametresCompte implements Serializable, Cloneable {

    private static final long serialVersionUID = -5700921162480027179L;

    private static final List<String> OBJECT_CLASSES_DEFAUT = Arrays.asList("top", "person", "organizationalPerson", "inetOrgPerson");

    //obligatoire
    private String repertoireCompte;
    private String repertoireRole;

    //optionnels
    private List<String> objectClasses;
    private String filtreCompte = "(cn=*)";
    private String champIdentifiantCompte = "cn";
    private String champIdentifiantRole = "cn";
    private String filtreRole = "(cn=*)";
    private String champCompteObjetRole = "member";
    private String champRoleObjetCompte = "memberOf";
    private String champObjectClass = "objectClass";
    private String champMotDePasse = "userPassword";
    private List<String> attributComptesRecuperes;
    private int profondeurRecherche = SearchControls.SUBTREE_SCOPE;

    /**
     * Crée une nouvelle instance de paramètres des comptes
     *
     * @param repertoireCompte le répertoire par défaut des comptes - par exemple : ou=users,dc=ldaptest,dc=fr,dc=atexo
     * @param repertoireRole   le répertoire qui contient les rôles - par exemple : ou=roles,dc=ldaptest,dc=fr,dc=atexo
     */
    public LDAPParametresCompte(String repertoireCompte, String repertoireRole) {
        this.repertoireCompte = repertoireCompte;
        this.repertoireRole = repertoireRole;
        attributComptesRecuperes = new ArrayList<String>();
    }

    /**
     * Retourne la profondeur de recherche.
     * Trois valeurs sont possibles :
     * <ul>
     * <li>{@link SearchControls#OBJECT_SCOPE} : 0</li>
     * <li>{@link SearchControls#ONELEVEL_SCOPE} : 1</li>
     * <li>{@link SearchControls#SUBTREE_SCOPE} : 2</li>
     * </ul>
     * <p/>
     * {@link SearchControls#SUBTREE_SCOPE} est la valeur par défaut.
     *
     * @return la profondeur de recherche
     */
    public int getProfondeurRecherche() {
        return profondeurRecherche;
    }

    /**
     * Modifie la profondeur de recherche
     *
     * @param profondeurRecherche la nouvelle profondeur de recherche
     */
    public void setProfondeurRecherche(int profondeurRecherche) {
        this.profondeurRecherche = profondeurRecherche;
    }

    /**
     * Modifie la liste des attributs des comptes récupérés.
     *
     * @param attributComptesRecuperes la nouvelle liste des attributs des comptes récupérés
     */
    public void setAttributComptesRecuperes(List<String> attributComptesRecuperes) {
        this.attributComptesRecuperes = attributComptesRecuperes;
    }

    /**
     * Retourne la liste des attributs des comptes récupérés - vide par défaut.
     *
     * @return la liste des attributs des comptes récupérés
     */
    public List<String> getAttributComptesRecuperes() {
        return attributComptesRecuperes;
    }

    /**
     * Retourne le répertoire par défaut des comptes.
     *
     * @return le répertoire par défaut des comptes
     */
    public String getRepertoireCompte() {
        return repertoireCompte;
    }

    /**
     * Retourne le répertoire des rôles.
     *
     * @return le répertoire des rôles
     */
    public String getRepertoireRole() {
        return repertoireRole;
    }

    /**
     * Modifie le répertoire par défaut des comptes.
     *
     * @param repertoireCompte le nouveau répertoire du compte
     */
    public void setRepertoireCompte(String repertoireCompte) {
        this.repertoireCompte = repertoireCompte;
    }

    /**
     * Modifie le répertoire par défaut des rôles.
     *
     * @param repertoireRole les nouveau répertoire des rôles
     */
    public void setRepertoireRole(String repertoireRole) {
        this.repertoireRole = repertoireRole;
    }

    /**
     * Retourne le champ d'identifiant des comptes - par défaut : {@literal "cn"}
     *
     * @return le champ d'identifiant des comptes
     */
    public String getChampIdentifiantCompte() {
        return champIdentifiantCompte;
    }

    /**
     * Modifie le champ d'identifiant des comptes.
     *
     * @param champIdentifiantCompte le nouveau champ d'identifiant des comptes
     */
    public void setChampIdentifiantCompte(String champIdentifiantCompte) {
        this.champIdentifiantCompte = champIdentifiantCompte;
    }

    /**
     * Récupère le nom du champ contenant les comptes reliés aux rôles - par défaut : {@literal "member"}.
     *
     * @return le nom du champ contenant les comptes reliés aux rôles
     */
    public String getChampCompteObjetRole() {
        return champCompteObjetRole;
    }

    /**
     * Modifie le champ contenant les comptes reliés aux rôles.
     *
     * @param champCompteObjetRole le nouveau nom du champ contenant les comptes reliés aux rôles
     */
    public void setChampCompteObjetRole(String champCompteObjetRole) {
        this.champCompteObjetRole = champCompteObjetRole;
    }

    /**
     * Récupère le nom du champ contenant les rôles reliés aux comptes - par défaut : {@literal "memberOf"}.
     * Ne fonctionne que pour Active Directory - Open LDAP n'utilise pas ce champ par défaut.
     *
     * @return le nom du champ contenant les rôles reliés aux comptes
     */
    public String getChampRoleObjetCompte() {
        return champRoleObjetCompte;
    }

    /**
     * Modifie le nom du champ contenant les rôles reliés aux comptes;
     *
     * @param champRoleObjetCompte le nouveau nom du champ contenant les rôles reliés aux comptes
     */
    public void setChampRoleObjetCompte(String champRoleObjetCompte) {
        this.champRoleObjetCompte = champRoleObjetCompte;
    }

    /**
     * Retourne le filtre utilisé pour retourner les comptes - par défaut : {@literal "(cn=*)"}.
     *
     * @return filtre utilisé pour retourner les comptes
     */
    public String getFiltreCompte() {
        return filtreCompte;
    }

    /**
     * Modifie le filtre utilisé pour retourner les comptes.
     *
     * @param filtreCompte le nouveau filtre utilisé pour retourner les comptes
     */
    public void setFiltreCompte(String filtreCompte) {
        this.filtreCompte = filtreCompte;
    }

    /**
     * Retourne la liste des classes ajoutées lors de la création d'un nouveau compte LDAP.
     * Par défaut :
     * <ul>
     * <li>"top"</li>
     * <li>"person"</li>
     * <li>"organizationalPerson"</li>
     * <li>"inetOrgPerson"</li>
     * </ul>
     *
     * @return la liste des classes ajoutées lors de la création d'un nouveau compte LDAP
     */
    public List<String> getObjectClasses() {
        return objectClasses == null || objectClasses.isEmpty() ? OBJECT_CLASSES_DEFAUT : objectClasses;
    }

    /**
     * Modifie la liste des classes ajoutées lors de la création d'un nouveau compte LDAP.
     *
     * @param objectClasses le nouvelle liste des classes ajoutées lors de la création d'un nouveau compte LDAP
     */
    public void setObjectClasses(List<String> objectClasses) {
        this.objectClasses = objectClasses;
    }

    /**
     * Retourne l'identifiant du mot de passe - par défaut : {@literal "userPassword"}.
     *
     * @return l'identifiant du mot de passe
     */
    public String getChampMotDePasse() {
        return champMotDePasse;
    }

    /**
     * Modifie l'identifiant du mot de passe.
     *
     * @param champMotDePasse le nouvel identifiant du mot de passe
     */
    public void setChampMotDePasse(String champMotDePasse) {
        this.champMotDePasse = champMotDePasse;
    }

    /**
     * Retourne l'identifiant du champ {@literal "objectClass"} - par défaut : {@literal ""objectClass""}
     *
     * @return l'identifiant du champ {@literal "objectClass"}
     */
    public String getChampObjectClass() {
        return champObjectClass;
    }

    /**
     * Modifie l'identifiant du champ {@literal "objectClass"}.
     *
     * @param champObjectClass le nouvel identifiant du champ {@literal "objectClass"}
     */
    public void setChampObjectClass(String champObjectClass) {
        this.champObjectClass = champObjectClass;
    }

    /**
     * Retourne l'identifiant de l'objet rôle - par défaut : {@literal "cn"}
     *
     * @return l'identifiant de l'objet rôle
     */
    public String getChampIdentifiantRole() {
        return champIdentifiantRole;
    }

    /**
     * Modifie l'identifiant de l'objet rôle.
     *
     * @param champIdentifiantRole le nouvel identifiant de l'objet rôle
     */
    public void setChampIdentifiantRole(String champIdentifiantRole) {
        this.champIdentifiantRole = champIdentifiantRole;
    }

    /**
     * Retourne le filtre utilisé pour récupérer les rôles - par défaut : {@literal "(cn=*)"}
     *
     * @return le filtre utilisé pour récupérer les rôles
     */
    public String getFiltreRole() {
        return filtreRole;
    }

    /**
     * Modifie le filtre utilisé pour récupérer les rôles.
     *
     * @param filtreRole le nouveau filtre utilisé pour récupérer les rôles
     */
    public void setFiltreRole(String filtreRole) {
        this.filtreRole = filtreRole;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LDAPParametresCompte clone() {
        try {
            LDAPParametresCompte l = (LDAPParametresCompte) super.clone();
            if (objectClasses != null) {
                l.objectClasses = new ArrayList<String>(objectClasses);
            }
            if (attributComptesRecuperes != null) {
                l.attributComptesRecuperes = new ArrayList<String>(attributComptesRecuperes);
            }
            return l;
        } catch (CloneNotSupportedException e) {
            //should not happen
            return null;
        }
    }

    @Override
    public String toString() {
        return "LDAPParametresCompte{" + "attributComptesRecuperes=" + attributComptesRecuperes + ", repertoireCompte='" + repertoireCompte + '\''
                + ", repertoireRole='" + repertoireRole + '\'' + ", objectClasses=" + objectClasses
                + ", filtreCompte='" + filtreCompte + '\'' + ", champIdentifiantCompte='" + champIdentifiantCompte + '\''
                + ", champIdentifiantRole='" + champIdentifiantRole + '\'' + ", filtreRole='" + filtreRole + '\''
                + ", champCompteObjetRole='" + champCompteObjetRole + '\'' + ", champRoleObjetCompte='" + champRoleObjetCompte + '\''
                + ", champObjectClass='" + champObjectClass + '\'' + ", champMotDePasse='" + champMotDePasse + '\''
                + ", profondeurRecherche=" + profondeurRecherche + '}';
    }
}
