/**
 * $Id$
 */
package fr.atexo.commun.excel;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.RichTextString;

import fr.atexo.commun.util.GeneriqueUtil;

/**
 * Commentaire
 * 
 * @version $Revision$ $Date$
 */
public class ExcelManager implements Serializable {

  private static final long serialVersionUID = 5758216167257106075L;

  // déclaration du log
  private static final Log LOG = LogFactory.getLog(ExcelManager.class);

  private HSSFWorkbook workbook;
  private HSSFCellStyle headerStyle;
  private HSSFCellStyle standardStyle;
  private HSSFCellStyle emptyStyle;

  public void read(InputStream inputStream) {
    try {
      workbook = new HSSFWorkbook(inputStream);
    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw new ExcelException(e);
    }
  }

  public HSSFWorkbook getWorkbook() {
    return workbook;
  }

  public String getSheetName(HSSFSheet sheet) {
    for (int i = 0; i < getWorkbook().getNumberOfSheets(); i++) {
      if (sheet.equals(getWorkbook().getSheetAt(i))) {
        return getWorkbook().getSheetName(i);
      }
    }
    return null;
  }

  public boolean getBoolean(HSSFRow row, int cellIndex) {
    String value = getString(row, cellIndex);
    return "true".equals(value) || "x".equalsIgnoreCase(value) || "Oui".equalsIgnoreCase(value) || "1".equals(value);
  }

  public String getString(HSSFRow row, int cellIndex) {
    if (row == null) {
      return null;
    }
    HSSFCell cell = row.getCell((short) cellIndex);
    if (cell == null) {
      return null;
    }
    try {
      switch (cell.getCellType()) {
        case HSSFCell.CELL_TYPE_STRING:
          String s = cell.getRichStringCellValue().getString();
          return s == null ? null : s.trim();
        case HSSFCell.CELL_TYPE_NUMERIC:
          return GeneriqueUtil.formatImport(cell.getNumericCellValue());
        case HSSFCell.CELL_TYPE_BLANK:
          return "";
        default:
          throw new UnsupportedOperationException();
      }
    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw new ExcelException(e);
    }
  }

  public Double getDouble(HSSFRow row, int cellIndex) {
    HSSFCell cell = row.getCell((short) cellIndex);
    if (cell == null) {
      return null;
    }
    try {
      switch (cell.getCellType()) {
        case HSSFCell.CELL_TYPE_STRING:
          return GeneriqueUtil.parseDouble(cell.getRichStringCellValue().getString());
        case HSSFCell.CELL_TYPE_NUMERIC:
          return cell.getNumericCellValue();
        case HSSFCell.CELL_TYPE_BLANK:
          return null;
        default:
          throw new UnsupportedOperationException();
      }
    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw new ExcelException(e);
    }
  }

  public Date getDate(HSSFRow row, int cellIndex) {
    HSSFCell cell = row.getCell((short) cellIndex);
    if (cell == null) {
      return null;
    }
    try {
      switch (cell.getCellType()) {
        case HSSFCell.CELL_TYPE_STRING:
          return GeneriqueUtil.parseSimpleDate(cell.getRichStringCellValue().getString());
        case HSSFCell.CELL_TYPE_BLANK:
          return null;
        case HSSFCell.CELL_TYPE_ERROR:
          return null;
        default:
          return cell.getDateCellValue();
      }

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw new ExcelException(e);
    }
  }

  public void setValue(HSSFRow row, int cellIndex, Object value) {
    HSSFCell cell = row.getCell((short) cellIndex);
    setValue(cell, value);
  }

  public void setValue(HSSFCell cell, Object value) {
    try {
      if (value == null) {
        RichTextString richTextString = new HSSFRichTextString();
        cell.setCellValue(richTextString);
      } else if (value instanceof String) {
        RichTextString richTextString = new HSSFRichTextString((String) value);
        cell.setCellValue(richTextString);
      } else if (value instanceof Enum) {
        RichTextString richTextString = new HSSFRichTextString(((Enum) value).name());
        cell.setCellValue(richTextString);
      } else if (value instanceof Double) {
        cell.setCellValue((Double) value);
      } else if (value instanceof Boolean) {
        cell.setCellValue((Boolean) value);
      } else if (value instanceof Date) {
        cell.setCellValue((Date) value);
      } else {
        RichTextString richTextString = new HSSFRichTextString(String.valueOf(value));
        cell.setCellValue(richTextString);
      }
    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw new ExcelException(e);
    }
  }


  public HSSFCellStyle createHeaderStyle() {
    HSSFCellStyle headerStyle = createCellStyle();
    headerStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    headerStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    headerStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
    headerStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
    headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

    headerStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
    headerStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
    HSSFFont lFont = workbook.createFont();
    lFont.setColor(HSSFFont.COLOR_NORMAL);
    lFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
    headerStyle.setFont(lFont);
    return headerStyle;
  }

  public HSSFCellStyle createStandardStyle() {
    HSSFCellStyle standardStyle = createCellStyle();
    standardStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    standardStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    standardStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
    standardStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
    standardStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
    standardStyle.setFillForegroundColor(HSSFColor.WHITE.index);
    return standardStyle;
  }

  public HSSFCellStyle createEmptyStyle() {
    HSSFCellStyle emptyStyle = createCellStyle();
    emptyStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
    emptyStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
    emptyStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
    emptyStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
    emptyStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
    emptyStyle.setFillForegroundColor(HSSFColor.AQUA.index);
    return emptyStyle;
  }

  public HSSFCellStyle createCellStyle() {
    return workbook.createCellStyle();
  }

  public HSSFSheet createWorkbook(String firstSheetName, Object... titles) {
    workbook = new HSSFWorkbook();

    // set header style
    headerStyle = createHeaderStyle();
    // set standard style
    standardStyle = createStandardStyle();
    // set empty style
    emptyStyle = createEmptyStyle();

    return createSheetWithTitles(firstSheetName, titles);
  }

  public void createWorkbook(String[] sheetsNames, Object... titles) {
    workbook = new HSSFWorkbook();

    // set header style
    headerStyle = createHeaderStyle();
    // set standard style
    standardStyle = createStandardStyle();
    // set empty style
    emptyStyle = createEmptyStyle();

    createSheetWithTitles(sheetsNames, titles);
  }


  public HSSFCell createCell(HSSFRow row, int col, CellStyle cellStyle) {
    HSSFCell cell = row.createCell((short) col);
    cell.setCellStyle(cellStyle);
    return cell;
  }

  public HSSFCell createCell(HSSFRow row, int col) {
    return createCell(row, col, standardStyle);
  }

  public void createSheetWithTitles(String[] sheetsNames, Object... titles) {

    for (String sheetName : sheetsNames) {

      HSSFSheet sheet = createSheet(sheetName);

      // Create a row for the header and put some cells in it. Rows are 0
      // based.
      HSSFRow row = sheet.createRow((short) 0);

      // Set the columns to repeat from row 0 to 0 on the first sheet
      workbook.setRepeatingRowsAndColumns(0, -1, -1, 0, 0);
      // Freeze just one row
      sheet.createFreezePane(0, 1, 0, 1);

      // title row
      // key column
      for (int i = 0; i < titles.length; i++) {
        HSSFCell cell = createCell(row, i, headerStyle);

        setValue(cell, titles[i]);
      }

    }

  }

  public HSSFSheet createSheetWithTitles(String sheetName, Object... titles) {
    HSSFSheet sheet = createSheet(sheetName);

    // Create a row for the header and put some cells in it. Rows are 0
    // based.
    HSSFRow row = sheet.createRow((short) 0);

    // Set the columns to repeat from row 0 to 0 on the first sheet
    workbook.setRepeatingRowsAndColumns(0, -1, -1, 0, 0);
    // Freeze just one row
    sheet.createFreezePane(0, 1, 0, 1);

    // title row
    // key column
    for (int i = 0; i < titles.length; i++) {
      HSSFCell cell = createCell(row, i, headerStyle);

      setValue(cell, titles[i]);
    }
    return sheet;
  }

  public HSSFSheet createSheet(String sheetName) {
    return workbook.createSheet(sheetName);
  }

  public HSSFRow createRow(HSSFSheet sheet, int i) {
    return sheet.createRow(i + 1);
  }

  public InputStream getInputStream() {
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      BufferedOutputStream bos = new BufferedOutputStream(baos);
      workbook.write(bos);
      bos.close();
      return new ByteArrayInputStream(baos.toByteArray());

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
    }
    return null;
  }

  public HSSFCellStyle getHeaderStyle() {
    return headerStyle;
  }
}
