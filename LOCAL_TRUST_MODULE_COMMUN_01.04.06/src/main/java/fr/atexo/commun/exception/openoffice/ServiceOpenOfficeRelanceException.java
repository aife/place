package fr.atexo.commun.exception.openoffice;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ServiceOpenOfficeRelanceException extends ServiceOpenOfficeException {

    public ServiceOpenOfficeRelanceException(String message) {
        super(message);
    }

    public ServiceOpenOfficeRelanceException(String message, Throwable cause) {
        super(message, cause);
    }
}
