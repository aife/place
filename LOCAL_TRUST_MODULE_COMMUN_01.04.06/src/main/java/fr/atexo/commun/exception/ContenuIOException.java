package fr.atexo.commun.exception;

/**
 * Exception lancé par l'application ayant lieu lors de l'écriture / lecture de contenu.
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ContenuIOException extends AtexoRuntimeException
{
    private static final long serialVersionUID = 3258130249983276087L;

    public ContenuIOException(String msgId) {
        super(msgId);
    }

    public ContenuIOException(String msgId, Object[] msgParams) {
        super(msgId, msgParams);
    }

    public ContenuIOException(String msgId, Throwable cause) {
        super(msgId, cause);
    }

    public ContenuIOException(String msgId, Object[] msgParams, Throwable cause) {
        super(msgId, msgParams, cause);
    }
}
