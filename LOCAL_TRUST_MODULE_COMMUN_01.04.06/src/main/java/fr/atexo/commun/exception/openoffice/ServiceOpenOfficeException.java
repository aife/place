package fr.atexo.commun.exception.openoffice;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ServiceOpenOfficeException extends Exception {

    public ServiceOpenOfficeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceOpenOfficeException(String message) {
        super(message);
    }
}
