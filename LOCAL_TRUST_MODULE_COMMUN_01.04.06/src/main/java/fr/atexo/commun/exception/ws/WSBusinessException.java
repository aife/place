package fr.atexo.commun.exception.ws;

/**
 * Exception envoyé lors de l'appel d'un web service dans le cas ou une erreur fonctionnelle est survenu
 */
public class WSBusinessException extends WSException {


    public WSBusinessException(String error) {
        super(error);
    }

    public WSBusinessException(String error, Throwable e) {
        super(error, e);
    }
}