package fr.atexo.commun.exception;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class ConfigurationException extends AtexoRuntimeException {

    public ConfigurationException(String msgId) {
        super(msgId);
    }

    public ConfigurationException(String msgId, Object[] msgParams) {
        super(msgId, msgParams);
    }

    public ConfigurationException(String msgId, Throwable cause) {
        super(msgId, cause);
    }

    public ConfigurationException(String msgId, Object[] msgParams, Throwable cause) {
        super(msgId, msgParams, cause);
    }
}
