/**
 * $Id$
 */
package fr.atexo.commun.ldap.spring;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.atexo.commun.ldap.AbstractTestInstanceLDAPServiceTest;
import fr.atexo.commun.ldap.LDAPParametresCompte;
import fr.atexo.commun.ldap.LDAPParametresConnexion;
import fr.atexo.commun.ldap.LDAPServiceImpl;

/**
 * Pour tester l'instance "Test" + Spring.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/atexo/commun/ldap/spring/ldap_test-services.xml")
public class TestInstanceSpringLDAPServiceTest extends AbstractTestInstanceLDAPServiceTest {

    @Autowired
    LDAPParametresCompte ldapParametresCompteTest;

    @Autowired
    LDAPParametresConnexion ldapParametresConnexionTest;

    @Autowired
    LDAPServiceImpl ldapServiceTest;

    protected LDAPParametresCompte getParametresCompte() {
        return ldapParametresCompteTest;
    }

    protected LDAPParametresConnexion getParametresConnexion() {
        return ldapParametresConnexionTest;
    }

    protected LDAPServiceImpl getService() {
        return ldapServiceTest;
    }
}
