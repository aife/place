/**
 * $Id$
 */
package fr.atexo.commun.ldap;

import java.util.List;

import javax.naming.NamingException;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * La classe qui contient tous les tests LDAP.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
public abstract class AbstractLDAPServiceTest extends TestCase {

    /** Récupération des services **/

    /**
     * @return les paramètres de connexion
     */
    protected abstract LDAPParametresConnexion getParametresConnexion();

    protected abstract LDAPParametresCompte getParametresCompte();

    protected abstract LDAPService getService();

    /**
     * Début des tests *
     */


    /**
     * @throws NamingException en cas de problème de connexion LDAP
     */
    @Test
    public void testCreerModifierLoginSupprimerCompte() throws NamingException {
        LDAPService service = getService();
        IdentifiantCompte toto = new IdentifiantCompte(getParametresCompte(), "toto");

        assertFalse(service.login(toto, "ceci_est_un_mot_de_passe"));

        try {
            service.creerCompte(toto, "ceci_est_un_mot_de_passe",
                    new AttributLDAP("cn", "toto"), new AttributLDAP("mail", "toto@toto.org"), new AttributLDAP("sn", "Mon nom est Toto!"),
                    new AttributLDAP("uid", "toto"));
        } catch (CompteExistantException e) {
            assertTrue(false);
        }

        //vérifie qu'il n'est pas possible de créer un compte existant
        boolean exCreation = false;
        try {
            service.creerCompte(toto, "ceci_est_un_mot_de_passe",
                    new AttributLDAP("cn", "toto"), new AttributLDAP("mail", "toto@toto.org"), new AttributLDAP("sn", "Mon nom est Toto!"),
                    new AttributLDAP("uid", "toto"));
        } catch (CompteExistantException e) {
            exCreation = true;
        }
        assertTrue(exCreation);

        assertTrue(service.login(toto, "ceci_est_un_mot_de_passe"));
        assertFalse(service.login(toto, "bof"));

        //modification du compte
        try {
            service.modifierCompte(toto, "bof",
                    new AttributLDAP("sn", "Mon nom est Titi!"));
        } catch (CompteNonExistantException e) {
            assertTrue(false);
        }

        try {
            service.supprimerCompte(toto);
        } catch (CompteNonExistantException e) {
            assertTrue(false);
        }

        //vérfie qu'il n'est pas possible de supprimer un compte déjà supprimé
        boolean exSuppression = false;
        try {
            service.supprimerCompte(toto);
        } catch (CompteNonExistantException e) {
            exSuppression = true;
        }
        assertTrue(exSuppression);

        //vérifie qu'il n'est pas possible de modifier un compte non existant
        boolean exModification = false;
        try {
            service.modifierCompte(toto, "ceci_est_un_mot_de_passe",
                    new AttributLDAP("cn", "toto"), new AttributLDAP("mail", "toto@toto.org"), new AttributLDAP("sn", "Mon nom est Toto!"),
                    new AttributLDAP("uid", "toto"));
        } catch (CompteNonExistantException e) {
            exModification = true;
        }
        assertTrue(exModification);
    }

    /**
     * Définition des données du test
     */

    /**
     * Le nombre total d'identifiants
     *
     * @return le nombre total d'identifiants
     */
    protected abstract int getNombreTotalIdentifiants();

    protected abstract String getLoginIdentifiantExistant();

    @Test
    public void testGetIdentifiants() {
        List<IdentifiantCompte> ids = getService().getIdentifiants();
        assertEquals(getNombreTotalIdentifiants(), ids.size());
        assertTrue(ids.contains(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant())));
    }

    protected abstract String[] getProfilsIdentifiantsAvecProfils();

    protected abstract int getNombreIdentifiantsAvecProfils();

    protected abstract String[] getProfilsIdentifiantsAvecProfils2();

    protected abstract int getNombreIdentifiantsAvecProfils2();

    @Test
    public void testGetIdentifiantsAvecProfils() {
        List<IdentifiantCompte> ids = getService().getIdentifiantsRestreintsAuxProfils(getProfilsIdentifiantsAvecProfils());
        assertEquals(getNombreIdentifiantsAvecProfils(), ids.size());
        assertTrue(ids.contains(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant())));
        ids = getService().getIdentifiantsRestreintsAuxProfils(getProfilsIdentifiantsAvecProfils2());
        assertEquals(getNombreIdentifiantsAvecProfils2(), ids.size());
        assertTrue(ids.contains(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant())));
    }

    protected abstract String[] getProfilsIdentifiantExistant();

    protected abstract String getNomAttribut();

    protected abstract String getValeurAttribut();

    protected abstract int getNombreProfilsIdendifiantExistant();

    @Test
    public void testGetDetailsComptes() {
        List<DetailsCompte> details = getService().getDetailsComptes();
        assertEquals(getNombreTotalIdentifiants(), details.size());
        boolean joeTrouve = false;
        for (DetailsCompte d : details) {
            if (d.getIdentifiantCompte().equals(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant()))) {
                joeTrouve = true;
                for (String s : getProfilsIdentifiantExistant()) {
                    assertTrue(d.getProfils().contains(s));
                }
                assertEquals(getValeurAttribut(), d.getAttribut(getNomAttribut()));
            }
        }
        assertTrue(joeTrouve);
    }

    @Test
    public void testGetDetailsComptesRestreintsAuProfil() {
        List<DetailsCompte> comptes = getService().getDetailsRestreintsAuxProfils(getProfilsIdentifiantsAvecProfils());
        assertEquals(getNombreIdentifiantsAvecProfils(), comptes.size());
        assertTrue(comptes.contains(new DetailsCompte(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant()))));
        boolean joeTrouve = false;
        for (DetailsCompte d : comptes) {
            if (d.getIdentifiantCompte().equals(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant()))) {
                joeTrouve = true;
                for (String s : getProfilsIdentifiantExistant()) {
                    assertTrue(d.getProfils().contains(s));
                }
                assertEquals(getValeurAttribut(), d.getAttribut(getNomAttribut()));
            }
        }
        assertTrue(joeTrouve);

        comptes = getService().getDetailsRestreintsAuxProfils(getProfilsIdentifiantsAvecProfils2());
        assertEquals(getNombreIdentifiantsAvecProfils2(), comptes.size());
        assertTrue(comptes.contains(new DetailsCompte(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant()))));
    }

    @Test
    public void testGetDetailsCompte() {
        DetailsCompte dc = getService().getDetailsCompte(new IdentifiantCompte(getParametresCompte(), getLoginIdentifiantExistant()));
        assertEquals(getNombreProfilsIdendifiantExistant(), dc.getProfils().size());
        for (String s : getProfilsIdentifiantExistant()) {
            assertTrue(dc.getProfils().contains(s));
        }
        assertEquals(getValeurAttribut(), dc.getAttribut(getNomAttribut()));
    }
}
