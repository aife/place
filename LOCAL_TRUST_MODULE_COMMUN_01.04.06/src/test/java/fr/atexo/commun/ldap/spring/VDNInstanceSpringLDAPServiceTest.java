/**
 * $Id$
 */
package fr.atexo.commun.ldap.spring;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.atexo.commun.ldap.AbstractVDNInstanceLDAPServiceTest;
import fr.atexo.commun.ldap.LDAPParametresCompte;
import fr.atexo.commun.ldap.LDAPParametresConnexion;
import fr.atexo.commun.ldap.LDAPServiceImpl;

/**
 * Pour tester l'instance "VDN" + Spring.
 *
 * @author Julien Buret
 * @version $Revision$ $Date$
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/atexo/commun/ldap/spring/ldap_vdn-services.xml")
public class VDNInstanceSpringLDAPServiceTest extends AbstractVDNInstanceLDAPServiceTest {

    @Autowired
    LDAPParametresCompte ldapParametresCompteVDN;

    @Autowired
    LDAPParametresConnexion ldapParametresConnexionVDN;

    @Autowired
    LDAPServiceImpl ldapServiceVDN;

    protected LDAPParametresCompte getParametresCompte() {
        return ldapParametresCompteVDN;
    }

    protected LDAPParametresConnexion getParametresConnexion() {
        return ldapParametresConnexionVDN;
    }

    protected LDAPServiceImpl getService() {
        return ldapServiceVDN;
    }

}
