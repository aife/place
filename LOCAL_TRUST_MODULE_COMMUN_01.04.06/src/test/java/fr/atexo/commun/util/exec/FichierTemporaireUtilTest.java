package fr.atexo.commun.util.exec;

import fr.atexo.commun.util.FichierTemporaireUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
public class FichierTemporaireUtilTest extends TestCase {

    @Test
    public void testCreationRepertoire() {

        File repertoireAtexoTemporaire = FichierTemporaireUtil.getRepertoireTemporaire("temporaire", false);
        assertTrue(repertoireAtexoTemporaire.isDirectory());

        File repertoireSystemeTemporaire = FichierTemporaireUtil.getRepertoireTemporaire("temporaire", true);
        assertTrue(repertoireSystemeTemporaire.isDirectory());

        repertoireAtexoTemporaire.delete();
        assertTrue(!repertoireAtexoTemporaire.exists());

        repertoireSystemeTemporaire.delete();
        assertTrue(!repertoireSystemeTemporaire.exists());

        File repertoireAtexo = FichierTemporaireUtil.getRepertoireTemporaire();
        assertTrue(repertoireAtexo.isDirectory());
    }


}
