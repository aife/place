package fr.atexo.commun.document.wkhtmltopdftools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.commun.util.ClasspathUtil;
import fr.atexo.commun.util.FichierTemporaireUtil;
import fr.atexo.commun.util.PropertiesUtil;

@Ignore
public class WkhtmltopdfConverteurTest {
  private static String CLEF_COMMANDE = "wkhtmltopdf.command";

  @Test
  public void testConvertir() throws IOException {

    InputStream fichierProprietes = ClasspathUtil.searchResource("fr/atexo/commun/document/wkhtmltopdftools/wkhtmltopdf.properties");
    Properties proprietes = PropertiesUtil.getProprietes(fichierProprietes);

    URL urlSource = ClasspathUtil.searchURL("fr/atexo/commun/document/wkhtmltopdf/htmlComplex.html");
    File fichierSource = null;
    try {
      fichierSource = new File(urlSource.toURI());
    } catch (URISyntaxException e) {
      e.printStackTrace();
      fichierSource = new File(urlSource.getPath());
    }

    File fichierDestination = new File(FichierTemporaireUtil.getRepertoireTemporaire() + "/file.pdf");

    WkhtmltopdfConfiguration wkhtmltopdfConfiguration = new WkhtmltopdfConfiguration();
    wkhtmltopdfConfiguration.setWkhtmltopdf(proprietes.getProperty(CLEF_COMMANDE));

    WkhtmltopdfConverteur.convertir(wkhtmltopdfConfiguration, fichierSource, fichierDestination);

    Assert.assertTrue(fichierDestination.exists());

    fichierDestination.delete();
    Assert.assertTrue(!fichierDestination.exists());
  }
}
