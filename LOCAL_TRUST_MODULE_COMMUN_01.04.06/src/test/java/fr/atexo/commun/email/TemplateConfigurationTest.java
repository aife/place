package fr.atexo.commun.email;

import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.atexo.commun.freemarker.TemplateConfiguration;
import fr.atexo.commun.util.ClasspathUtil;
import freemarker.template.*;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.EmailException;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

/**
 * Commentaire
 *
 * @author Louis Champion
 * @version $Revision$ $Date$
 */
@Ignore
public class TemplateConfigurationTest extends TestCase {

    private static final String TEMPLATE_TXT_PAR_DEFAUT = "template_txt.ftl";
    private static final String TEMPLATE_HTML_PAR_DEFAUT = "template_html.ftl";
    private static final String TEMPLATE_HTML_FR = "template_html_fr.ftl";
    private static final String TEMPLATE_HTML_FR_FR = "template_html_fr_FR.ftl";
    private static final String TEMPLATE_HTML_EN = "template_html_en.ftl";
    private static final String TEMPLATE_HTML_EN_US = "template_html_en_US.ftl";

    @Test
    public void testInitialiseConfiguration() {
        try {

            URL url = ClasspathUtil.searchURL("fr/atexo/commun/email/");
            File repertoireTemplate = new File(url.toURI());
            assertTrue(repertoireTemplate.isDirectory());

            Configuration configuration = TemplateConfiguration.getConfiguration(repertoireTemplate);
            configuration.setObjectWrapper(ObjectWrapper.DEFAULT_WRAPPER);

            Template templateTxt = configuration.getTemplate(TEMPLATE_TXT_PAR_DEFAUT);
            assertNotNull(templateTxt);

            Template templateHtml = configuration.getTemplate(TEMPLATE_HTML_PAR_DEFAUT);
            assertNotNull(templateHtml);

            EmailBean emailBean = new EmailBean("Régis", "Menet", new Date(), "Mr", "Louis", "Champion");

            Writer out = new OutputStreamWriter(System.out);

            SimpleHash root = new SimpleHash();
            root.put("email", emailBean);


            templateTxt.process(root, out);
            templateHtml.process(root, out);

        } catch (URISyntaxException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (TemplateException e) {
            e.printStackTrace();
            assertTrue(false);
        }


    }

    @Test
    public void testExecuteTransformationLocalisee() throws URISyntaxException {

        URL url = ClasspathUtil.searchURL("fr/atexo/commun/email/");
        File repertoireTemplate = new File(url.toURI());
        assertTrue(repertoireTemplate.isDirectory());

        String cheminVersRepertoire = repertoireTemplate.getAbsolutePath();

        EmailBean emailBean = new EmailBean("Régis", "Menet", new Date(), "Mr", "Louis", "Champion");

        Map<String, Object> parametres = new HashMap<String, Object>();
        parametres.put("email", emailBean);


        try {
            File fichier = FreeMarkerUtil.executeTransformation(cheminVersRepertoire + "/", TEMPLATE_HTML_PAR_DEFAUT, Locale.FRANCE, parametres);
            String contenuFichier = FileUtils.readFileToString(fichier);
            assertNotNull(contenuFichier);

            fichier = FreeMarkerUtil.executeTransformation(cheminVersRepertoire + "/", TEMPLATE_HTML_PAR_DEFAUT, Locale.FRENCH, parametres);
            contenuFichier = FileUtils.readFileToString(fichier);
            assertNotNull(contenuFichier);

            fichier = FreeMarkerUtil.executeTransformation(cheminVersRepertoire + "/", TEMPLATE_HTML_PAR_DEFAUT, Locale.UK, parametres);
            contenuFichier = FileUtils.readFileToString(fichier);
            assertNotNull(contenuFichier);

            fichier = FreeMarkerUtil.executeTransformation(cheminVersRepertoire + "/", TEMPLATE_HTML_PAR_DEFAUT, Locale.US, parametres);
            contenuFichier = FileUtils.readFileToString(fichier);
            assertNotNull(contenuFichier);

        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (TemplateException e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }

    @Test
    public void testEnvoyerEmail() {

        EmailServiceImpl emailService = new EmailServiceImpl();

        ServeurSmtpConnexion serveurSmtpConnexion = new ServeurSmtpConnexion("smtp.free.fr", 25);
        serveurSmtpConnexion.setDebug(true);

        Set<String> destinataires = new HashSet<String>();
        destinataires.add("louis.champion@atexo.com");

        Email email = new Email("louis.champion@atexo.com", destinataires, "Test Envoi Email", "Message TXT", "<i>Message HTML italique</i>");

        try {
            emailService.envoyerEmails(serveurSmtpConnexion, email);
            assertTrue(true);

        } catch (EmailException e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }

    @Test
    public void testEnvoyerEmailAvecFreemarker() {


        try {

            URL url = ClasspathUtil.searchURL("fr/atexo/commun/email/");
            File repertoireTemplate = new File(url.toURI());
            assertTrue(repertoireTemplate.isDirectory());

            String cheminVersRepertoire = repertoireTemplate.getAbsolutePath();

            EmailBean emailBean = new EmailBean("Régis", "Menet", new Date(), "Mr", "Louis", "Champion");

            Map<String, Object> parametres = new HashMap<String, Object>();
            parametres.put("email", emailBean);

            EmailServiceImpl emailService = new EmailServiceImpl();

            File fichier = FreeMarkerUtil.executeTransformation(cheminVersRepertoire + "/", TEMPLATE_TXT_PAR_DEFAUT, Locale.FRANCE, parametres);
            String contenuFichier = FileUtils.readFileToString(fichier);
            fichier.deleteOnExit();

            ServeurSmtpConnexion serveurSmtpConnexion = new ServeurSmtpConnexion("smtp.free.fr", 25);
            serveurSmtpConnexion.setDebug(true);

            Set<String> destinataires = new HashSet<String>();
            destinataires.add("louis.champion@atexo.com");

            Email email = new Email("louis.champion@atexo.com", destinataires, "Test Envoi Email", contenuFichier, null);


            emailService.envoyerEmails(serveurSmtpConnexion, email);
            assertTrue(true);

        } catch (EmailException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (TemplateException e) {
            e.printStackTrace();
            assertTrue(false);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }
}
