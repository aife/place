package fr.atexo.editionEnLigne.serveur;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe contenant les correspondances entre les jetons et les nom des fichiers
 * mis a disposition pour webdav.
 * 
 * @author BTE
 * 
 */
public class JetonMapExecutorTask {
	private static final int DUREE = 10 * 60 * 1000;
	private static final Log LOG = LogFactory.getLog(JetonMapExecutorTask.class);
	private JetonMap jetonMap;

	public void cleanMap() {
		Set<String> cles = jetonMap.keySet();
		for (String cle : cles) {
			FichierMetadonnees fichier = jetonMap.get(cle);
			if (fichier.getDateCreation() + DUREE < System.currentTimeMillis()) {
				LOG.info("suppresion document " + fichier.toString());
				jetonMap.remove(cle);
			}
		}
	}

	public void setJetonMap(JetonMap jetonMap) {
		this.jetonMap = jetonMap;
	}

}
