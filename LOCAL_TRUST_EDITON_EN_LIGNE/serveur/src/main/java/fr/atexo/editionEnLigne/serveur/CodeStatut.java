package fr.atexo.editionEnLigne.serveur;

public enum CodeStatut {

	OK(200, "OK"), nonAuthorise(403, "Non Authorisé"), erreurServeur(500, "Erreur Interne du serveur");
	
	private String message; 
	private int code; 
	
	private CodeStatut(int code, String message){
		this.message = message; 
		this.code = code; 
	}
	
	public String getMessage(){
		return this.message; 
	}

	public int getCode() {
		return code;
	}
	
	
}
