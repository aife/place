package fr.atexo.editionEnLigne.serveur;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.util.DigestUtils;

import fr.atexo.editionEnLigne.commun.TypeDepot;
import fr.atexo.editionEnLigne.commun.EditionEnLigne.Fichier;

/**
 * Classe contenant l'ensemble des information sur un fichier permettant de
 * gerer son edition en ligne
 * 
 * @author RME
 * 
 */
public class FichierMetadonnees implements Serializable {

	private TypeDepot typeAcess;
	private long dateCreation;
	private String uri;
	private String login = "";
	private String motDePasse = "";
	private String contentType;
	private String nom;
	private String id;
	private String identifiant;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public TypeDepot getTypeAcess() {
		return typeAcess;
	}

	public void setTypeAcess(TypeDepot typeAcess) {
		this.typeAcess = TypeDepot.DISQUE;
	}

	public String getLogin() {
		return login;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getIdenfiantWebDav() {
		StringBuilder tmp = new StringBuilder();
		tmp.append(uri);
		tmp.append(id);
		tmp.append(login);
		tmp.append(motDePasse);
		tmp.append(typeAcess.name());
		return DigestUtils.md5DigestAsHex(tmp.toString().getBytes());
	}

	public long getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}
	
}
