package fr.atexo.editionEnLigne.serveur.depots;

import fr.atexo.service.alfresco.enums.ProprieteDocumentParapheur;
import fr.atexo.service.alfresco.util.Propriete;

@Deprecated
public class DepotFichierAlfrescoDocumentImpl extends AbstractDepotFichierAlfresco {

	protected Propriete getProprieteAlfresco(){
		return ProprieteDocumentParapheur.PIECE_CONTENU_PRINCIPALE; 
	}

}
