package fr.atexo.editionEnLigne.serveur;
import javax.jcr.RepositoryException;

import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.lock.SimpleLockManager;


public class LockManager extends SimpleLockManager {
	
	public synchronized void releaseLock(String lockToken, DavResource resource)
			throws DavException {
		super.releaseLock(lockToken, resource);
	}
}
