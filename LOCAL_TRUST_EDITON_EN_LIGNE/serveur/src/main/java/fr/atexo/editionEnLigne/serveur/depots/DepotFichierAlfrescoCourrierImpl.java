package fr.atexo.editionEnLigne.serveur.depots;

import fr.atexo.service.alfresco.enums.ProprieteCourrier;
import fr.atexo.service.alfresco.util.Propriete;

@Deprecated
public class DepotFichierAlfrescoCourrierImpl extends AbstractDepotFichierAlfresco {

	protected Propriete getProprieteAlfresco(){
		return ProprieteCourrier.PIECE_CONTENU_PRINCIPALE; 
	}

}
