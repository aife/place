package fr.atexo.editionEnLigne.serveur;

import java.util.Hashtable;

/**
 * Classe contenant les correspondances entre les jetons et les nom des fichiers
 * mis a disposition pour webdav.
 * 
 * @author BTE
 * 
 */
@SuppressWarnings("serial")
public class JetonMap extends Hashtable<String, FichierMetadonnees> {

	public String ajouterNouveauJeton(FichierMetadonnees fichierMetadonnees) {
		String jeton = fichierMetadonnees.getIdenfiantWebDav();
		this.put(jeton, fichierMetadonnees);
		return jeton;
	}
}
