package fr.atexo.editionEnLigne.serveur;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.editionEnLigne.commun.EditionEnLigne;
import fr.atexo.editionEnLigne.commun.TypeDepot;
import fr.atexo.editionEnLigne.commun.EditionEnLigne.Fichier;
import fr.atexo.editionEnLigne.commun.EditionEnLigne.Statut;

/**
 * Web Service pour les actions sur les documents.
 * 
 * @author Bruno Teles
 */
@Path("/WebService")
public class EditionEnLigneWebService {

	private static final Log LOG = LogFactory.getLog(EditionEnLigneWebService.class);

	private static JetonMap jetonMap;

	private static final String CHEMIN_REPOSITORY = "repository/";

	public void setJetonMap(JetonMap jetonMap) {
		EditionEnLigneWebService.jetonMap = jetonMap;
	}

	/**
	 * Permet de récupérer le chemin du fichier éditable dans le repository avec
	 * un identifiant unique.
	 * 
	 * @param uri
	 * @param type
	 * 
	 * @return une réponse contenant une url relative qui indique le chemin vers
	 *         le fichier éditable dans le repository.
	 */
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	@Path("RecupererUuid")
	public EditionEnLigne recupererUuid(EditionEnLigne editionEnLigne) {

		EditionEnLigne editionEnLigneReponse = new EditionEnLigne();
		Statut statutReponse = new Statut();
		editionEnLigneReponse.setStatut(statutReponse);

		// verification le fichier envoyé est valide
		Fichier fichier = editionEnLigne.getFichier();

		if (editionEnLigne == null || fichier == null) {
			statutReponse.setMessage(CodeStatut.nonAuthorise.getMessage());
			statutReponse.setCode(String.valueOf(CodeStatut.nonAuthorise.getCode()));
			return editionEnLigneReponse;
		}

		try {

			FichierMetadonnees fichierMetadonnees = new FichierMetadonnees();
			fichierMetadonnees.setNom(fichier.getNom());
			fichierMetadonnees.setId(fichier.getId());
			fichierMetadonnees.setDateCreation(System.currentTimeMillis());
			fichierMetadonnees.setIdentifiant(fichier.getIdentifiant());
			fichierMetadonnees.setLogin(fichier.getLogin());
			fichierMetadonnees.setMotDePasse(fichier.getMotdepasse());
			fichierMetadonnees.setTypeAcess(TypeDepot.valueOf(fichier.getTypeAcces()));
			fichierMetadonnees.setUri(fichier.getUri());
			String jeton = jetonMap.ajouterNouveauJeton(fichierMetadonnees);

			String urlJeton = buildUrlJeton(jeton, fichier.getNom());
			editionEnLigneReponse.setUrlJeton(urlJeton);

			statutReponse.setMessage(CodeStatut.OK.getMessage());
			statutReponse.setCode(String.valueOf(CodeStatut.OK.getCode()));

		} catch (Exception e) {
			statutReponse.setMessage(CodeStatut.erreurServeur.getMessage());
			statutReponse.setCode(String.valueOf(CodeStatut.erreurServeur.getCode()));
		}

		return editionEnLigneReponse;
	}

	public static String buildUrlJeton(String jeton, String nomFichier) {
		StringBuilder urlJeton = new StringBuilder();
		urlJeton.append(CHEMIN_REPOSITORY);
		urlJeton.append(jeton);
		if (nomFichier.indexOf('.') != -1) {
			urlJeton.append(nomFichier.substring(nomFichier.lastIndexOf('.'), nomFichier.length()));
		} else {
			urlJeton.append(nomFichier);
		}
		return urlJeton.toString();
	}

}
