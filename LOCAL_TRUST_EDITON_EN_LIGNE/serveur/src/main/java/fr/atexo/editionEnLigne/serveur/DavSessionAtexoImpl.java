package fr.atexo.editionEnLigne.serveur;

import java.util.HashSet;

import org.apache.jackrabbit.webdav.DavSession;

public class DavSessionAtexoImpl implements DavSession {

	/** the lock tokens of this session */
	private final HashSet<String> lockTokens = new HashSet<String>();

	/**
	 * @see DavSession#addReference(Object)
	 */
	public void addReference(Object reference) {
		throw new UnsupportedOperationException("No yet implemented.");
	}

	/**
	 * @see DavSession#removeReference(Object)
	 */
	public void removeReference(Object reference) {
		throw new UnsupportedOperationException("No yet implemented.");
	}

	/**
	 * @see DavSession#addLockToken(String)
	 */
	@Override
	public void addLockToken(String token) {
		lockTokens.add(token);
	}

	/**
	 * @see DavSession#getLockTokens()
	 */
	@Override
	public String[] getLockTokens() {
		return lockTokens.toArray(new String[lockTokens.size()]);
	}

	/**
	 * @see DavSession#removeLockToken(String)
	 */
	@Override
	public void removeLockToken(String token) {
		lockTokens.remove(token);
	}

}
