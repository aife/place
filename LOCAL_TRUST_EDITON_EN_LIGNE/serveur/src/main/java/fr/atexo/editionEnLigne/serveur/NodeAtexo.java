package fr.atexo.editionEnLigne.serveur;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.jcr.AccessDeniedException;
import javax.jcr.Binary;
import javax.jcr.InvalidItemStateException;
import javax.jcr.InvalidLifecycleTransitionException;
import javax.jcr.Item;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.ItemVisitor;
import javax.jcr.MergeException;
import javax.jcr.NoSuchWorkspaceException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.Lock;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.nodetype.NodeDefinition;
import javax.jcr.nodetype.NodeType;
import javax.jcr.version.ActivityViolationException;
import javax.jcr.version.Version;
import javax.jcr.version.VersionException;
import javax.jcr.version.VersionHistory;

import fr.atexo.depotfichier.bean.Fichier;

public class NodeAtexo implements Node {

	private String type;
	private FichierMetadonnees fichier;

	public NodeAtexo(String type) {
		this.type = type;
	}

	public NodeAtexo(String type, FichierMetadonnees fichier) {
		this.type = type;
		this.fichier = fichier;
	}

	@Override
	public String getPath() throws RepositoryException {
		if (fichier == null) {
			return fichier.getNom();
		} else {
			return "PATH";
		}
	}

	@Override
	public String getName() throws RepositoryException {
		// TODO Auto-generated method stub
		if (fichier != null) {
			return fichier.getNom();
		}
		return null;
	}

	@Override
	public Item getAncestor(int paramInt) throws ItemNotFoundException,
			AccessDeniedException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getParent() throws ItemNotFoundException,
			AccessDeniedException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getDepth() throws RepositoryException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Session getSession() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNode() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isModified() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSame(Item paramItem) throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void accept(ItemVisitor paramItemVisitor) throws RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void save() throws AccessDeniedException, ItemExistsException,
			ConstraintViolationException, InvalidItemStateException,
			ReferentialIntegrityException, VersionException, LockException,
			NoSuchNodeTypeException, RepositoryException {
		System.out.println("node save");
	}

	@Override
	public void refresh(boolean paramBoolean) throws InvalidItemStateException,
			RepositoryException {
		System.out.println("test refresh");

	}

	@Override
	public void remove() throws VersionException, LockException,
			ConstraintViolationException, AccessDeniedException,
			RepositoryException {
		System.out.println("remove");

	}

	@Override
	public Node addNode(String paramString) throws ItemExistsException,
			PathNotFoundException, VersionException,
			ConstraintViolationException, LockException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node addNode(String paramString1, String paramString2)
			throws ItemExistsException, PathNotFoundException,
			NoSuchNodeTypeException, LockException, VersionException,
			ConstraintViolationException, RepositoryException {
		return null;
	}

	@Override
	public void orderBefore(String paramString1, String paramString2)
			throws UnsupportedRepositoryOperationException, VersionException,
			ConstraintViolationException, ItemNotFoundException, LockException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public Property setProperty(String paramString, Value paramValue)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Value paramValue,
			int paramInt) throws ValueFormatException, VersionException,
			LockException, ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Value[] paramArrayOfValue)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Value[] paramArrayOfValue,
			int paramInt) throws ValueFormatException, VersionException,
			LockException, ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, String[] paramArrayOfString)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString,
			String[] paramArrayOfString, int paramInt)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString1, String paramString2)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString1, String paramString2,
			int paramInt) throws ValueFormatException, VersionException,
			LockException, ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, InputStream paramInputStream)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Binary paramBinary)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, boolean paramBoolean)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, double paramDouble)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, BigDecimal paramBigDecimal)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, long paramLong)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Calendar paramCalendar)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property setProperty(String paramString, Node paramNode)
			throws ValueFormatException, VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getNode(String paramString) throws PathNotFoundException,
			RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodeIterator getNodes() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodeIterator getNodes(String paramString) throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodeIterator getNodes(String[] paramArrayOfString)
			throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Property getProperty(String paramString)
			throws PathNotFoundException, RepositoryException {
		return null;
	}

	@Override
	public PropertyIterator getProperties() throws RepositoryException {
		PropertyIterator it = new PropertyIterator() {
			
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Object next() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void skip(long arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getPosition() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Property nextProperty() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		return it;
	}

	@Override
	public PropertyIterator getProperties(String paramString)
			throws RepositoryException {
		PropertyIterator it = new PropertyIterator() {
			
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Object next() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void skip(long arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getPosition() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Property nextProperty() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		return it;
	}

	@Override
	public PropertyIterator getProperties(String[] paramArrayOfString)
			throws RepositoryException {
		PropertyIterator it = new PropertyIterator() {
			
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Object next() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void skip(long arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getPosition() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Property nextProperty() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		return it;
	}

	@Override
	public Item getPrimaryItem() throws ItemNotFoundException,
			RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUUID() throws UnsupportedRepositoryOperationException,
			RepositoryException {
		if (fichier != null) {
			return fichier.getIdentifiant();
		} else {
			return null;
		}
	}

	@Override
	public String getIdentifier() throws RepositoryException {
		// TODO Auto-generated method stub
		return fichier.getIdenfiantWebDav();
	}

	@Override
	public int getIndex() throws RepositoryException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PropertyIterator getReferences() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PropertyIterator getReferences(String paramString)
			throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PropertyIterator getWeakReferences() throws RepositoryException {
		PropertyIterator it = new PropertyIterator() {
			
			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public Object next() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void skip(long arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getPosition() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Property nextProperty() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		return it;
	}

	@Override
	public PropertyIterator getWeakReferences(String paramString)
			throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasNode(String paramString) throws RepositoryException {
		// TODO Auto-generated method stub
		return paramString != null && paramString.equals("ATEXO");
	}

	@Override
	public boolean hasProperty(String paramString) throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasNodes() throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasProperties() throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public NodeType getPrimaryNodeType() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodeType[] getMixinNodeTypes() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNodeType(String paramString) throws RepositoryException {
		return type.equals(paramString);
	}

	@Override
	public void setPrimaryType(String paramString)
			throws NoSuchNodeTypeException, VersionException,
			ConstraintViolationException, LockException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void addMixin(String paramString) throws NoSuchNodeTypeException,
			VersionException, ConstraintViolationException, LockException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeMixin(String paramString) throws NoSuchNodeTypeException,
			VersionException, ConstraintViolationException, LockException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean canAddMixin(String paramString)
			throws NoSuchNodeTypeException, RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public NodeDefinition getDefinition() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Version checkin() throws VersionException,
			UnsupportedRepositoryOperationException, InvalidItemStateException,
			LockException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkout() throws UnsupportedRepositoryOperationException,
			LockException, ActivityViolationException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doneMerge(Version paramVersion) throws VersionException,
			InvalidItemStateException, UnsupportedRepositoryOperationException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancelMerge(Version paramVersion) throws VersionException,
			InvalidItemStateException, UnsupportedRepositoryOperationException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(String paramString) throws NoSuchWorkspaceException,
			AccessDeniedException, LockException, InvalidItemStateException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public NodeIterator merge(String paramString, boolean paramBoolean)
			throws NoSuchWorkspaceException, AccessDeniedException,
			MergeException, LockException, InvalidItemStateException,
			RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCorrespondingNodePath(String paramString)
			throws ItemNotFoundException, NoSuchWorkspaceException,
			AccessDeniedException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NodeIterator getSharedSet() throws RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeSharedSet() throws VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeShare() throws VersionException, LockException,
			ConstraintViolationException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isCheckedOut() throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void restore(String paramString, boolean paramBoolean)
			throws VersionException, ItemExistsException,
			UnsupportedRepositoryOperationException, LockException,
			InvalidItemStateException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void restore(Version paramVersion, boolean paramBoolean)
			throws VersionException, ItemExistsException,
			InvalidItemStateException, UnsupportedRepositoryOperationException,
			LockException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void restore(Version paramVersion, String paramString,
			boolean paramBoolean) throws PathNotFoundException,
			ItemExistsException, VersionException,
			ConstraintViolationException,
			UnsupportedRepositoryOperationException, LockException,
			InvalidItemStateException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public void restoreByLabel(String paramString, boolean paramBoolean)
			throws VersionException, ItemExistsException,
			UnsupportedRepositoryOperationException, LockException,
			InvalidItemStateException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public VersionHistory getVersionHistory()
			throws UnsupportedRepositoryOperationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Version getBaseVersion()
			throws UnsupportedRepositoryOperationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Lock lock(boolean paramBoolean1, boolean paramBoolean2)
			throws UnsupportedRepositoryOperationException, LockException,
			AccessDeniedException, InvalidItemStateException,
			RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Lock getLock() throws UnsupportedRepositoryOperationException,
			LockException, AccessDeniedException, RepositoryException {
		// TODO Auto-generated method stub
		return new LockAtexo(this);
	}

	@Override
	public void unlock() throws UnsupportedRepositoryOperationException,
			LockException, AccessDeniedException, InvalidItemStateException,
			RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean holdsLock() throws RepositoryException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLocked() throws RepositoryException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void followLifecycleTransition(String paramString)
			throws UnsupportedRepositoryOperationException,
			InvalidLifecycleTransitionException, RepositoryException {
		// TODO Auto-generated method stub

	}

	@Override
	public String[] getAllowedLifecycleTransistions()
			throws UnsupportedRepositoryOperationException, RepositoryException {
		// TODO Auto-generated method stub
		return null;
	}

}
