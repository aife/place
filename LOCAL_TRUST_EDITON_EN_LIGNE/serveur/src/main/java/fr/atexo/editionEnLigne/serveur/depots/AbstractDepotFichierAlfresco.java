package fr.atexo.editionEnLigne.serveur.depots;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil;
import fr.atexo.depotfichier.bean.Fichier;
import fr.atexo.depotfichier.bean.FichierModification;
import fr.atexo.depotfichier.depotdisque.DepotFichier;
import fr.atexo.depotfichier.depotdisque.DepotFichierException;
import fr.atexo.depotfichier.depotdisque.FichierNonTrouveException;
import fr.atexo.service.alfresco.exceptions.ErreurInterneException;
import fr.atexo.service.alfresco.exceptions.SessionInvalideException;
import fr.atexo.service.alfresco.gestionFichiers.GestionDocumentsImpl;
import fr.atexo.service.alfresco.recherche.RequeteBuilderAlfresco;
import fr.atexo.service.alfresco.util.AlfrescoSession;
import fr.atexo.service.alfresco.util.Noeud;
import fr.atexo.service.alfresco.util.Propriete;

@Deprecated
public abstract class AbstractDepotFichierAlfresco extends DepotFichier {

	private static Logger log = LoggerFactory.getLogger(AbstractDepotFichierAlfresco.class);

	private AlfrescoSession session;
	private GestionDocumentsImpl gestionDocuments;

	@Override
	public void init(String url, String login, String motDePasse, String path) {
		try {
			AlfrescoSession localSession = new AlfrescoSession(url, login, motDePasse);
			String strTicket = localSession.getTicket();
			log.debug("Ticket: " + strTicket);
			session = new AlfrescoSession(url, login, motDePasse);
			gestionDocuments = new GestionDocumentsImpl(session);
			session = localSession;
			RequeteBuilderAlfresco builder = new RequeteBuilderAlfresco();
			builder.addRequeteChemin(path);
		} catch (SessionInvalideException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public Fichier ajouter(File arg0) throws DepotFichierException {
		return null;
	}

	@Override
	public Fichier ajouter(InputStream arg0, String arg1) throws DepotFichierException {
		return null;
	}

    @Override
    public Fichier ajouter(File fichier, String noeudOucheminParent) throws DepotFichierException {
        return null;
    }

    @Override
    public Fichier ajouter(InputStream inputStream, String nomFichier, String noeudOucheminParent) throws DepotFichierException {
        return null;
    }

    @Override
    public InputStream charger(String s, boolean b) throws FichierNonTrouveException {
        return null;
    }

    @Override
	public InputStream charger(String reference) throws FichierNonTrouveException {
		InputStream is = null;
		URLConnection connection = null; 
		URL url;
		try {
			url = new URL(buildUrlFichierAlfresco(reference));
			connection = url.openConnection();
			is = connection.getInputStream();
			
			if (is == null) {
				log.error("La référence " + reference + " n'existe pas");
				throw new FichierNonTrouveException("La référence " + reference + " n'existe pas");
			}
		
		} catch (MalformedURLException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		
		return is;

	}
	
	private String buildUrlFichierAlfresco(String reference){
		StringBuilder url = new StringBuilder();
		// on considere que l'urlBase finit par un slash
		url.append(session.getUrlBase());
		url.append("/d/d/workspace/SpacesStore/");
		url.append(reference);
		url.append("/file.bin?property=");
		url.append(getProprieteAlfresco().getPropriete());
		url.append("&ticket=");
		url.append(session.getTicket());
		return url.toString(); 
	}

	@Override
	public Fichier modifier(File fichier, String reference) throws DepotFichierException {

		log.debug("Mise à jour d'un fichier dans Alfresco Courrier...");

		Fichier fichierResultat = null;

		try {

			FileInputStream fileInputStream = new FileInputStream(fichier);
			long tailleFichier = (long) fileInputStream.available();
			String contentTypeFichier = null;
			@SuppressWarnings("unchecked")
			Collection<MimeType> resultatMime = MimeUtil.getMimeTypes(fichier.getName());
			if (!resultatMime.isEmpty()) {
				Iterator<MimeType> it = resultatMime.iterator();
				contentTypeFichier = it.next().toString();
			}

			String referenceExterneFichier = gestionDocuments.miseAJourContenuDocument(new Noeud(reference), fichier.getName(), fileInputStream, getProprieteAlfresco(), contentTypeFichier, tailleFichier, true);

			fichierResultat = new Fichier();
			fichierResultat.setContentType(contentTypeFichier);
			fichierResultat.setDateModification(new Date());
			fichierResultat.setIdentifiant(referenceExterneFichier);
			fichierResultat.setNom(fichier.getName());
			fichierResultat.setTaille(tailleFichier);

		} catch (FileNotFoundException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		} catch (IOException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		} catch (ErreurInterneException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		}

		return fichierResultat;
	}

	@Override
	public Fichier modifier(InputStream is, String nomFichier, String reference) throws DepotFichierException {
		log.debug("Mise à jour d'un fichier dans Alfresco Courrier...");

		Fichier fichierResultat = null;

		try {

			long tailleFichier = (long) is.available();
			String contentTypeFichier = null;
			@SuppressWarnings("unchecked")
			Collection<MimeType> resultatMime = MimeUtil.getMimeTypes(nomFichier);
			if (!resultatMime.isEmpty()) {
				Iterator<MimeType> it = resultatMime.iterator();
				contentTypeFichier = it.next().toString();
			}

			String referenceExterneFichier = gestionDocuments.miseAJourContenuDocument(new Noeud(reference), nomFichier, is, getProprieteAlfresco(), contentTypeFichier, tailleFichier, true);

			fichierResultat = new Fichier();
			fichierResultat.setContentType(contentTypeFichier);
			fichierResultat.setDateModification(new Date());
			fichierResultat.setIdentifiant(referenceExterneFichier);
			fichierResultat.setNom(nomFichier);
			fichierResultat.setTaille(tailleFichier);

		} catch (FileNotFoundException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		} catch (IOException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		} catch (ErreurInterneException e) {
			log.error("Echec lor du mise à jour du fichier : " + e.getMessage(), e);
		}

		return fichierResultat;	}

	@Override
	public boolean supprimer(String arg0) {
		return false;
	}

    @Override
    public FichierModification modifierVersionner(File input, String identifiant) throws DepotFichierException {
        return null;
    }

    @Override
    public FichierModification modifierVersionner(InputStream inputStream, String nomFichier, String identifiant) throws DepotFichierException {
        return null;
    }

    /**
	 * Propriete Alfresco concernant le contenu du fichier.
	 * 
	 * @return
	 */
	protected abstract Propriete getProprieteAlfresco();

}
