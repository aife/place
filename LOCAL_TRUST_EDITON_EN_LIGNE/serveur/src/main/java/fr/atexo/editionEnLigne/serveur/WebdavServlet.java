/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.atexo.editionEnLigne.serveur;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.jackrabbit.webdav.DavLocatorFactory;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavSessionProvider;
import org.apache.jackrabbit.webdav.WebdavRequest;
import org.apache.jackrabbit.webdav.server.AbstractWebdavServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WebdavServlet provides WebDAV support (level
 * {@link org.apache.jackrabbit.webdav.DavCompliance#_1_ 1},
 * {@link org.apache.jackrabbit.webdav.DavCompliance#_2_ 2},
 * {@link org.apache.jackrabbit.webdav.DavCompliance#_3_ 3} and
 * {@link org.apache.jackrabbit.webdav.DavCompliance#BIND bind} compliant) for
 * repository resources.
 * <p>
 * Implementations of this abstract class must implement the
 * {@link #getRepository()} method to access the repository.
 */
@SuppressWarnings("serial")
public class WebdavServlet extends AbstractWebdavServlet {

	/**
	 * the default logger
	 */
	private static final Logger log = LoggerFactory.getLogger(WebdavServlet.class);

	/**
	 * init param name of the repository prefix
	 */
	public static final String INIT_PARAM_RESOURCE_PATH_PREFIX = "resource-path-prefix";

	/**
	 * Name of the parameter that specifies the servlet resource path of a
	 * custom &lt;mime-info/&gt; configuration file. The default setting is to
	 * use the MIME media type database included in Apache Tika.
	 */
	public static final String INIT_PARAM_MIME_INFO = "mime-info";

	/**
	 * Servlet context attribute used to store the path prefix instead of having
	 * a static field with this servlet. The latter causes problems when running
	 * multiple
	 */
	public static final String CTX_ATTR_RESOURCE_PATH_PREFIX = "jackrabbit.webdav.simple.resourcepath";

	/**
	 * the resource path prefix
	 */
	private String resourcePathPrefix;

	/**
	 * the resource factory
	 */
	private static DavResourceFactory resourceFactory;

	/**
	 * the locator factory
	 */
	private static DavLocatorFactory locatorFactory;
	
	/**
	 * the webdav session provider
	 */
	private static DavSessionProvider davSessionProvider;

	/**
	 * Init this servlet
	 * 
	 * @throws ServletException
	 */
	@Override
	public void init() throws ServletException {
		super.init();

		resourcePathPrefix = getInitParameter(INIT_PARAM_RESOURCE_PATH_PREFIX);
		if (resourcePathPrefix == null) {
			log.debug("Missing path prefix > setting to empty string.");
			resourcePathPrefix = "";
		} else if (resourcePathPrefix.endsWith("/")) {
			log.debug("Path prefix ends with '/' > removing trailing slash.");
			resourcePathPrefix = resourcePathPrefix.substring(0, resourcePathPrefix.length() - 1);
		}
		getServletContext().setAttribute(CTX_ATTR_RESOURCE_PATH_PREFIX, resourcePathPrefix);
		log.info(INIT_PARAM_RESOURCE_PATH_PREFIX + " = '" + resourcePathPrefix + "'");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean isPreconditionValid(WebdavRequest request, DavResource resource) {
		return !resource.exists() || request.matchesIfHeader(resource);
	}

	/**
	 * Returns the configured path prefix
	 * 
	 * @return resourcePathPrefix
	 * @see #INIT_PARAM_RESOURCE_PATH_PREFIX
	 */
	public String getPathPrefix() {
		return resourcePathPrefix;
	}

	/**
	 * Returns the configured path prefix
	 * 
	 * @param ctx
	 *            The servlet context.
	 * @return resourcePathPrefix
	 * @see #INIT_PARAM_RESOURCE_PATH_PREFIX
	 */
	public static String getPathPrefix(ServletContext ctx) {
		return (String) ctx.getAttribute(CTX_ATTR_RESOURCE_PATH_PREFIX);
	}

	/**
	 * Returns the <code>DavLocatorFactory</code>. If no locator factory has
	 * been set or created a new instance of
	 * {@link org.apache.jackrabbit.webdav.simple.LocatorFactoryImpl} is
	 * returned.
	 * 
	 * @return the locator factory
	 * @see AbstractWebdavServlet#getLocatorFactory()
	 */
	@Override
	public DavLocatorFactory getLocatorFactory() {
		return locatorFactory;
	}

	/**
	 * Sets the <code>DavLocatorFactory</code>.
	 * 
	 * @param locatorFactory
	 *            The <code>DavLocatorFactory</code> to use.
	 * @see AbstractWebdavServlet#setLocatorFactory(DavLocatorFactory)
	 */
	@Override
	public void setLocatorFactory(DavLocatorFactory locatorFactory) {
		WebdavServlet.locatorFactory = locatorFactory;
	}

	/**
	 * Returns the <code>DavResourceFactory</code>. If no request factory has
	 * been set or created a new instance of {@link ResourceFactoryAtexoImpl} is
	 * returned.
	 * 
	 * @return the resource factory
	 * @see AbstractWebdavServlet#getResourceFactory()
	 */
	@Override
	public DavResourceFactory getResourceFactory() {
		return resourceFactory;
	}

	/**
	 * Sets the <code>DavResourceFactory</code>.
	 * 
	 * @param resourceFactory
	 *            The <code>DavResourceFactory</code> to use.
	 * @see AbstractWebdavServlet#setResourceFactory(org.apache.jackrabbit.webdav.DavResourceFactory)
	 */
	@Override
	public void setResourceFactory(DavResourceFactory resourceFactory) {
		WebdavServlet.resourceFactory = resourceFactory;
	}

	/**
	 * Returns the <code>DavSessionProvider</code>. If no session provider has
	 * been set or created a new instance of {@link DavSessionProviderImpl} is
	 * returned.
	 * 
	 * @return the session provider
	 * @see AbstractWebdavServlet#getDavSessionProvider()
	 */
	@Override
	public synchronized DavSessionProvider getDavSessionProvider() {
		return davSessionProvider;
	}

	/**
	 * Sets the <code>DavSessionProvider</code>.
	 * 
	 * @param sessionProvider
	 *            The <code>DavSessionProvider</code> to use.
	 * @see AbstractWebdavServlet#setDavSessionProvider(org.apache.jackrabbit.webdav.DavSessionProvider)
	 */
	@Override
	public synchronized void setDavSessionProvider(DavSessionProvider sessionProvider) {
		WebdavServlet.davSessionProvider = sessionProvider;
	}
}
