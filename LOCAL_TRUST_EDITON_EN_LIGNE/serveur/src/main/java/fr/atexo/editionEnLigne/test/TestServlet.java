package fr.atexo.editionEnLigne.test;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.atexo.editionEnLigne.commun.TypeDepot;
import fr.atexo.editionEnLigne.serveur.FichierMetadonnees;
import fr.atexo.editionEnLigne.serveur.JetonMap;

@SuppressWarnings("serial")
public class TestServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(TestServlet.class);
    private static JetonMap fichierMap;

 
    /**
     * Surcharge de la requete HTTP GET Connexion a Alfresco a partir de l'id
     * d'un document Creation de la signature via l'applet Envoi de la signature
     * a Alfresco
     * 
     * @param HttpServletRequest
     * @param HttpServletResponse
     * @throws IOException
     */
    public void service(HttpServletRequest request, HttpServletResponse reponse)
            throws ServletException {
        reponse.setHeader("Cache-Control",
                "no-cache, must-revalidate, proxy-revalidate");
        reponse.setHeader("Pragma", "no-cache");
        reponse.setDateHeader("Expires", 0);
        FichierMetadonnees fichier = new FichierMetadonnees();
        fichier.setIdentifiant(request.getParameter("uuid").substring(0, request.getParameter("uuid").indexOf('.')));
        fichier.setNom("test2.doc");
        fichier.setContentType("application/msword");
    	fichier.setTypeAcess(TypeDepot.DISQUE);
    	fichier.setUri("http://alfresco-demo/alfresco/");
    	fichier.setLogin("admin");
    	fichier.setMotDePasse("admin");
        fichierMap.put(request.getParameter("uuid").substring(0, request.getParameter("uuid").indexOf('.')), fichier);
        try {
        	RequestDispatcher rd;
        	rd = getServletContext().getRequestDispatcher("/jsp/upload.jsp");
        	rd.include(request, reponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }


	public void setFichierMap(JetonMap fichierMap) {
		TestServlet.fichierMap = fichierMap;
	}

}
