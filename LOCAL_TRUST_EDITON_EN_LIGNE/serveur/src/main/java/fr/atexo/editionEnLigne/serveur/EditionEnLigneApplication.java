package fr.atexo.editionEnLigne.serveur;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class EditionEnLigneApplication extends Application {
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(EditionEnLigneWebService.class);
		return s;
	}
}
