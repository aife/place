package fr.atexo.editionEnLigne.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestToken extends HttpServlet{

	  public void init() {
	  }
	  public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		  
		  res.getOutputStream().print("http://127.0.0.1:8080/editionEnLigne/repository/" + req.getParameter("uuid")); 
	  }
	
}
