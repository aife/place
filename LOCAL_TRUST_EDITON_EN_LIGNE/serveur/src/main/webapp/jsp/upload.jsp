<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>
<body id="page">
	<div class="popup-moyen" id="container">
		<!--Debut Bloc-->
		<div class="form-field">
			<div class="top">
				<span class="left">&nbsp;</span><span class="right">&nbsp;</span>
			</div>

			<div class="content">
				<h1>Edition en ligne</h1>
				<div class="applet-selection-fichier">
					<applet width='20px' height='20px' name='EditionEnLigneAppletFichier'
						id='EditionEnLigneAppletFichier'
						code='fr.atexo.editionEnLigne.applet.Main'
						archive="upload-applet.jar,jna.jar,platform.jar"
						code="fr.atexo.editionEnLigne.applet.Main.class"
						alt="Java Runtime Environment is not working on your system"
						name="EditionEnLigneAppletFichierCourrier" mayscript="true">

						<param name='urlImage' value='http://127.0.0.1:8080/editionEnLigne-serveur/jsp/images/edition.png' />
						<!--   <param name='methodeJavascriptErreur' value='executerJsErreur()' /> -->
						<!--   <param name='initAutomatique' value='false' /> -->
						<!--   <param name='methodeJavascriptDebut' value='executerJsDebut()' /> -->
						<!--   <param name='methodeJavascriptFin' value='executerJsFin()' /> -->
						<!--   <param name='methodeJavascriptEditionEnCours' -->
						<!--    value='executerJsEditionDejaEnCours()' /> -->
						<param name="urlClient" value="http://127.0.0.1:8080/editionEnLigne-serveur/testToken?uuid=<%= request.getParameter("uuid") %>" />
						<param name="initAutomatique" value="false" />
						<param name='applet_name' value='EditionEnLigneAppletFichier' />
						<param name='codebase_lookup' value='false' />
					</applet>
				</div>
				<div class="spacer"></div>
				<p class="bottom-note">Test</p>
				<!--Debut line boutons-->
				<div class="boutons-line">
					<input type="button" class="bouton-moyen float-right"
						value="Executer"
						onclick="javascript:window.document.EditionEnLigneAppletFichier.executer('http://127.0.0.1:8080/editionEnLigne-serveur/testToken?uuid=${param.uuid}')" />
				</div>

				<!--Fin line boutons-->
				<div class="breaker"></div>
			</div>
			<div class="bottom">
				<span class="left">&nbsp;</span><span class="right">&nbsp;</span>
			</div>
		</div>
		<!--Fin Bloc-->
	</div>
</body>
</html>