package fr.atexo.editionEnLigne;

import java.io.InputStream;

import org.junit.Test;
import org.junit.Ignore;
import junit.framework.TestCase;
import fr.atexo.depotfichier.depotdisque.FichierNonTrouveException;
import fr.atexo.editionEnLigne.serveur.depots.DepotFichierAlfrescoCourrierImpl;
@Ignore
public class DepotAlfrescoFichier extends TestCase {

	@Ignore
	public void testAlfresco() {
		DepotFichierAlfrescoCourrierImpl depotFichier = new DepotFichierAlfrescoCourrierImpl();
		depotFichier.init("http://alfresco-demo/alfresco/", "admin", "admin", null);
		try {
			InputStream in = depotFichier.charger("");
			assertNotNull(in);
		} catch (FichierNonTrouveException e) {
			fail(e.getMessage());
		}
		
	}
	
}
