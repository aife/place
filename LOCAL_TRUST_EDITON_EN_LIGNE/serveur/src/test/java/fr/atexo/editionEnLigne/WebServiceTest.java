package fr.atexo.editionEnLigne;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import fr.atexo.editionEnLigne.commun.EditionEnLigne;
import fr.atexo.editionEnLigne.commun.TypeDepot;
import fr.atexo.editionEnLigne.commun.EditionEnLigne.Fichier;
@Ignore
public class WebServiceTest extends TestCase {

	private static final Log LOG = LogFactory.getLog(WebServiceTest.class);
	private static final String URL_WEBSERVICE_EDITION_EN_LIGNE = "http://127.0.0.1:8080/editionEnLigne-serveur/rest/WebService/";

	private static final String GESTIONNAIRE_EXTERNE_URL = "C:/tmp/";
	private static final String GESTIONNAIRE_EXTERNE_LOGIN = "admin";
	private static final String GESTIONNAIRE_EXTERNE_MOT_DE_PASSE = "admin";

	public void testAppelWebService() {

		WebResource webResourceEditionEnLigne = getWebResourceEditionEnLigne();
		EditionEnLigne editionEnLigneEnvoi = new EditionEnLigne();
		Fichier fichier = new Fichier();

		String versionFichier = "1";
		String nomFichier = "1234.doc";
		String identifiantFichier = "1234";
		String contentTypeFichier = "application/doc";
		long tailleFichier = 1;
		String id = "00000001";

		// metadonnees du fichier
		fichier.setTypeAcces(TypeDepot.DISQUE.name());
		fichier.setId(id);
		fichier.setNom(nomFichier);
		fichier.setIdentifiant(identifiantFichier);
		fichier.setContentType(contentTypeFichier);
		fichier.setTaille(tailleFichier);
		fichier.setVersion(new BigInteger(versionFichier));
		fichier.setUri(GESTIONNAIRE_EXTERNE_URL);
		fichier.setLogin(GESTIONNAIRE_EXTERNE_LOGIN);
		fichier.setMotdepasse(GESTIONNAIRE_EXTERNE_MOT_DE_PASSE);

		try {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			XMLGregorianCalendar dateXML;

			dateXML = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			// TODO récupérer la bonne date
			fichier.setDateModification(dateXML);

		} catch (DatatypeConfigurationException e) {
			LOG.error(e.getMessage(), e);
		}

		editionEnLigneEnvoi.setFichier(fichier);
		EditionEnLigne editionEnLigneReponse = null;

		try {
			editionEnLigneReponse = webResourceEditionEnLigne
					.path("RecupererUuid").accept(MediaType.APPLICATION_XML)
					.entity(editionEnLigneEnvoi, MediaType.APPLICATION_XML)
					.type(MediaType.APPLICATION_XML).post(EditionEnLigne.class);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		if (editionEnLigneReponse != null) {
			assertNotNull(editionEnLigneReponse.getUrlJeton());
			System.out.println(editionEnLigneReponse.getUrlJeton());
		} else {
			assertTrue(false);
		}

	}

	private WebResource getWebResourceEditionEnLigne() {
		Client client = Client.create();
		WebResource webResource = client
				.resource(URL_WEBSERVICE_EDITION_EN_LIGNE);
		return webResource;
	}

}
