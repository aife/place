package fr.atexo.editionEnLigne.applet;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;

import netscape.javascript.JSObject;
import fr.atexo.editionEnLigne.util.Util;

public class Main extends JApplet {

	/**
	 * URL relative du fichier de configuration de cette applet.
	 */
	private final static String CONFIGURATION_PAR_DEFAUT = "os.properties";

	private static final String PARAM_INIT_AUTOMATIQUE = "initAutomatique";

	private static final String PARAM_URL_CLIENT = "urlClient";

	private static final String PARAM_URL_IMAGE = "urlImage";

	private static final String PARAM_URL_CONFIGURATION_CLIENT = "urlConfiguration";

	private static final String PARAM_METHODE_JAVASCRIPT_CLICK = "methodeJavascriptClick";

	private static final String PARAM_METHODE_JAVASCRIPT_DEBUT = "methodeJavascriptDebut";

	private static final String PARAM_METHODE_JAVASCRIPT_FIN = "methodeJavascriptFin";

	private static final String PARAM_METHODE_JAVASCRIPT_ERREUR = "methodeJavascriptErreur";

	private static final String PARAM_METHODE_JAVASCRIPT_EDITION_EN_COURS = "methodeJavascriptEditionEnCours";

	private boolean initAutomatique = true;

	private String methodeJavascriptClick;

	private String methodeJavascriptDebut;

	private String methodeJavascriptFin;

	private String methodeJavascriptErreur;

	private String methodeJavascriptEditionEnCours;

	private String urlClient;

	private String urlConfigurationClient;

	private URL urlImage;

	private boolean initialisee;

	protected Thread listenerInitialisation;

	/**
	 * Indique si la applet a bien été executé.
	 */
	private boolean executionEdition = true;

	@Override
	public void init() {

		String initAutomatique = getParameter(PARAM_INIT_AUTOMATIQUE);
		if (initAutomatique != null) {
			this.initAutomatique = Boolean.valueOf(initAutomatique);
		}
		System.out.println("[DEBUG] Param initAutomatique : " + initAutomatique);

		urlConfigurationClient = getParameter(PARAM_URL_CONFIGURATION_CLIENT);
		System.out.println("[DEBUG] Param Url Configuration Client : " + urlConfigurationClient);

		String urlImage = getParameter(PARAM_URL_IMAGE);
		System.out.println("[DEBUG] Param urlImage : " + urlImage);

		urlClient = getParameter(PARAM_URL_CLIENT);
		System.out.println("[DEBUG] Param urlCient : " + urlClient);

		methodeJavascriptClick = getParameter(PARAM_METHODE_JAVASCRIPT_CLICK);
		System.out.println("[DEBUG] Methode Js Click : " + methodeJavascriptClick);

		methodeJavascriptDebut = getParameter(PARAM_METHODE_JAVASCRIPT_DEBUT);
		System.out.println("[DEBUG] Methode Js Debut : " + methodeJavascriptDebut);

		methodeJavascriptFin = getParameter(PARAM_METHODE_JAVASCRIPT_FIN);
		System.out.println("[DEBUG] Methode Js Fin : " + methodeJavascriptFin);

		methodeJavascriptErreur = getParameter(PARAM_METHODE_JAVASCRIPT_ERREUR);
		System.out.println("[DEBUG] Methode Js Erreur : " + methodeJavascriptErreur);

		methodeJavascriptEditionEnCours = getParameter(PARAM_METHODE_JAVASCRIPT_EDITION_EN_COURS);
		System.out.println("[DEBUG] Methode Js Edition en Cours : " + methodeJavascriptEditionEnCours);

		if (!this.initAutomatique) {

			if (urlClient != null && urlImage != null) {
				try {
					System.out.println("[DEBUG] L'url de l'image : " + urlImage);
					this.urlImage = new URL(urlImage);
					ImageIcon icon = new ImageIcon(this.urlImage);

					final JLabel bouton = new JLabel();
					bouton.setIcon(icon);
					bouton.addMouseListener(new MouseListener() {

						@Override
						public void mouseClicked(MouseEvent e) {
							try {
								if (methodeJavascriptClick != null && methodeJavascriptClick.length() > 0) {
									Boolean execute = (Boolean) appellerMethodeJavascript(methodeJavascriptClick);
									if (execute != null && execute) {
										executer();
									} else {
										System.out.println("[DEBUG] Methode Js Click a retourné " + execute + ". l'éxecution est interrompue.");
									}
								} else {
									executer();
								}
							} catch (Exception exception) {
								exception.printStackTrace();
							}
						}

						@Override
						public void mouseEntered(MouseEvent e) {
							bouton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
						}

						@Override
						public void mouseExited(MouseEvent e) {
							bouton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						}

						@Override
						public void mousePressed(MouseEvent e) {
						}

						@Override
						public void mouseReleased(MouseEvent e) {
						}
					});
					add(bouton);

				} catch (MalformedURLException e) {
					System.out.println("[DEBUG] L'url de l'image pour le bouton est incorrecte...");
					e.printStackTrace();
				}
			} else {
				System.out.println("[DEBUG] Aucune url client ou image n'est définie => Initialisation décorélée par le biais d'un Timer");
				listenerInitialisation = new Thread() {
					@Override
					public void run() {
						while (true) {
							try {
								if (initialisee) {
									executer();
									initialisee = false;
								}
								sleep(500);
							} catch (Throwable e) {
								e.printStackTrace();
								initialisee = false;
							}
						}
					}
				};
				listenerInitialisation.start();

			}

		} else {
			executer();
		}
	}

	@Override
	public void destroy() {
		if (listenerInitialisation != null && listenerInitialisation.isAlive()) {
			listenerInitialisation.interrupt();
		}
	}

	public void executer() {
		executer(this.urlClient);
	}

	public void initialiser(String urlClient) {
		System.out.println("[DEBUG] Url Client : " + urlClient);
		this.urlClient = urlClient;
		this.initialisee = true;
	}

	protected void executer(String urlClient) {
		System.out.println("[DEBUG] Url Client : " + urlClient);
		this.urlClient = urlClient;
		appellerMethodeJavascript(methodeJavascriptDebut);
		String urlFichier = initialiseJeton(urlClient);
		System.out.println("[DEBUG] Jeton : " + urlFichier);
		executerEditionEnLigne(urlFichier);
		if (executionEdition) {
			appellerMethodeJavascript(methodeJavascriptFin);
		} else {
			appellerMethodeJavascript(methodeJavascriptErreur);
		}
	}

	private String initialiseJeton(String urlClient) {
		String jeton = null;
		if (urlClient != null) {

			try {
				URL url = new URL(urlClient);
				URLConnection connection = url.openConnection();
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);
				connection.setDefaultUseCaches(false);

				ByteArrayOutputStream output = new ByteArrayOutputStream();
				InputStream openStream = url.openStream();
				byte[] buffer = new byte[1024];
				int size = 0;
				while ((size = openStream.read(buffer)) != -1) {
					output.write(buffer, 0, size);
				}

				jeton = output.toString("UTF-8");

				openStream.close();
				output.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return jeton;
	}

	protected void executerEditionEnLigne(String urlFichier) {

		if (urlFichier != null) {
			// nom du fichier récupéré en paramètre de l'applet
			System.out.println("[DEBUG] Nom du fichier complet: " + urlFichier);
			Properties propertiesConfigSystemesExploitation = chargerProprietesDefautSystemeExploitation();
			if (propertiesConfigSystemesExploitation != null && urlFichier != null && urlFichier.lastIndexOf(".") >= 0) {
				String extension = urlFichier.substring(urlFichier.lastIndexOf("."));
				String nomProprieteIdentifiantApplication = propertiesConfigSystemesExploitation.getProperty("extension" + extension);
				urlFichier = "\"" + urlFichier + "\"";
				executionEdition = Util.executerApplication(urlFichier, nomProprieteIdentifiantApplication.split("\\|"));
				System.out.println("[DEBUG] L'application a été bien démarrée.");
			} else {
				System.out.println("[DEBUG] Les propriétés du fichier de configuration n'ont pas pu être chargées.");
			}
		} else {
			appellerMethodeJavascript(methodeJavascriptEditionEnCours);
		}
	}

	protected Object appellerMethodeJavascript(String methodeJavascript, String... parametres) {
		return appellerMethodeJavascript(methodeJavascript, true, parametres);
	}

	/**
	 * Permet d'appeler une méthode Javascript avec des paramétres.
	 * 
	 * @param methodeJavascript
	 *            la méthode Javascript à appeler
	 * @param parametres
	 *            la liste des paramétres de la méthode.
	 */
	protected Object appellerMethodeJavascript(String methodeJavascript, boolean verbeux, String... parametres) {
		if (methodeJavascript != null) {
			if (parametres != null && parametres.length != 0) {
				return appellerMethodeJs(methodeJavascript, verbeux, parametres);
			} else {
				return appellerMethodeJs(methodeJavascript, verbeux, new Object[] {});
			}
		}
		return null;
	}

	/**
	 * Permet d'appeler la méthode Javascript native alert afin d'afficher un
	 * message.
	 * 
	 * @param message
	 *            le message à afficher lors de l'appel de la méthode alert
	 *            javascript.
	 */
	protected void appellerMethodeJavascriptAlert(String message) {
		appellerMethodeJavascript("alert", false, message);
	}

	/**
	 * Permet d'appeler une méthode Javascript avec des paramétres.
	 * 
	 * @param methode
	 *            la méthode Javascript à appeler
	 * @param parametres
	 *            la liste des paramétres de la méthode.
	 */
	protected Object appellerMethodeJs(String methode, boolean verbeux, Object[] parametres) {
		if (methode != null && methode.length() > 0) {

			if (verbeux) {

				System.out.println("[DEBUG] Appel de la méthode Js : " + methode);
				System.out.println("[DEBUG] Liste des paramètres de la méthode Js :");
				for (int i = 0; i < parametres.length; i++) {
					Object parametre = parametres[i];
					if (parametre instanceof String[][]) {
						System.out.println("[DEBUG] Paramètre " + i + "  : " + affichierContenuTableauDoubleDimension((String[][]) parametre));
					} else {
						System.out.println("[DEBUG] Paramètre " + i + "  : " + parametre);
					}
				}
			}

			try {
				JSObject window = JSObject.getWindow(this);
				return window.call(methode, parametres);
			} catch (Throwable t) {
				try {
					getAppletContext().showDocument(new URL("javascript:" + methode + ";"));
					return null;
				} catch (MalformedURLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * @param tableau
	 * @return
	 */
	private String affichierContenuTableauDoubleDimension(Object[][] tableau) {
		StringBuffer results = new StringBuffer();
		String separator = ",";

		for (int i = 0; i < tableau.length; ++i) {
			results.append('[');
			for (int j = 0; j < tableau[i].length; ++j)
				if (j > 0)
					results.append(tableau[i][j]);
				else
					results.append(tableau[i][j]).append(separator);
			results.append(']');
		}

		return results.toString();
	}

	/**
	 * Charge le fichier de propriétés qui contient la configuration de la base
	 * de registre pour chaque os et chaque type de fichier
	 *
	 * @return
	 */
	private Properties chargerProprietesDefautSystemeExploitation() {
		Properties properties = null;

		if (urlConfigurationClient != null) {
			try {
				System.out.println("[DEBUG] Chargement des propriétés depuis l'url de configuration client");
				URL url = new URL(urlConfigurationClient);
				properties = new Properties();
				InputStream inputStream = Util.getInputStreamFormUrl(url);
				properties.load(inputStream);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					System.out.println("[DEBUG] L'url de configation client ne peut être chargé...Du coup on charge celui inclu dans le jar de l'applet");
					properties = new Properties();
					InputStream inputStream = Util.getInputStreanFromClasspath(CONFIGURATION_PAR_DEFAUT);
					properties.load(inputStream);
				} catch (Exception e2) {
					e2.printStackTrace();
				}

			}

		} else {
			System.out.println("[DEBUG] Aucune url de configation client trouvé...Du coup on charge celui inclu dans le jar de l'applet");
			try {
				properties = new Properties();
				InputStream inputStream = Util.getInputStreanFromClasspath(CONFIGURATION_PAR_DEFAUT);
				properties.load(inputStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return properties;
	}

	/**
	 * Vérifie si l'applet est initialisée.
	 *
	 * @return
	 */
	public boolean isInitialisee() {
		return executionEdition;
	}

}