package fr.atexo.editionEnLigne.util;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 */
public class Util {

    /**
     * Prefix du nom de la propriété qui correspond au chemin de
     * l'application en fonction du système d'exploitation.
     */
    private final static String PREFIX_PROPRIETE_CHEMIN_OS = "pathApp.";

    public static InputStream getInputStreanFromClasspath(String resource) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        InputStream result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        loader = ClassLoader.getSystemClassLoader();

        return getInputStreamFormUrl(loader.getResource(resource));
    }

    public static InputStream getInputStreamFormUrl(URL url) {
        if (url != null) {

            try {
                URLConnection connection = url.openConnection();
                return connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }



    /**
     * Construction du nom de la propriété qui correspond au chemin de
     * l'application en fonction du système d'exploitation.
     *
     * @param nomSystemeExploitation
     * @return
     */
    public static String buildNomPropriete(String nomSystemeExploitation) {
        StringBuilder nomPropriete = new StringBuilder(PREFIX_PROPRIETE_CHEMIN_OS);

        // 1) Traitement du nom du système d'exploitation : remplace tous les
        // espaces par des underscores et l'ajoute au nom de la propriété

        if (nomSystemeExploitation.contains(" ")) {
            nomPropriete.append(nomSystemeExploitation.replaceAll(" ", "_"));
        } else {
            nomPropriete.append(nomSystemeExploitation);
        }

        return nomPropriete.toString();
    }

    /**
     * Lance l'application souhaitée après avoir récupéré ses propriétés de
     * lancement.
     *
     * @param identifiantProgramme nom de l'application telle que elle est définie dans la base
     *                             de registre.
     * @param cheminRegistre       chemin additionel de l'application selon l'os
     * @return succes ou échec de 'léxécution de l'application
     */

	public static boolean executerApplication(String cheminFichier, String[] identifiantProgramme) {

        // les paramètres d'entrée sont obligatoires
        if (identifiantProgramme != null) {
            for (int i = 0; i < identifiantProgramme.length; i++) {
                System.out.println("[DEBUG] Nom de l'identifiant du programme : " + identifiantProgramme[i]);

                boolean exist = Advapi32Util.registryKeyExists(WinReg.HKEY_CLASSES_ROOT, identifiantProgramme[i]);
                if (!exist) {
                    System.out.println("[DEBUG] ECHEC lors de la recherche de l'identifiant du programme '" + identifiantProgramme[i] + "' dans la base de registre : aucune valeur correspondante.");
                    continue;
                }
                String cleRegistre = Advapi32Util.registryGetStringValue(WinReg.HKEY_CLASSES_ROOT, identifiantProgramme[i], "");
                System.out.println("[DEBUG] Clé de Registre de l'identifiant '" + identifiantProgramme[i] + "' : " + cleRegistre);

                String cheminCompletApplication = null;
                String cheminExecutable = null;
            	cheminCompletApplication = cleRegistre + "\\shell\\open\\command";
                exist = Advapi32Util.registryKeyExists(WinReg.HKEY_CLASSES_ROOT, cheminCompletApplication);
                if (exist) {
                	cheminExecutable = Advapi32Util.registryGetStringValue(WinReg.HKEY_CLASSES_ROOT, cheminCompletApplication, "");
                } else {
                    System.out.println("[DEBUG] ECHEC de l'ouverture de l'application : " + cheminCompletApplication);
                    continue;
                }
                System.out.println("[DEBUG] Chemin complet de l'éxécutable : " + cheminExecutable);
                try {
                    String executable;
                    executable = cheminExecutable.substring(cheminExecutable.lastIndexOf('\\') + 1);

                    System.out.println("[DEBUG] Nom de l'éxécutable : " + executable);
                    String[] cmd = new String[executable.split(" ").length];
                    for (int j = 0; j < executable.split(" ").length; j++) {
                        if (j == 0) {
                            cmd[j] = cheminExecutable.substring(0, cheminExecutable.lastIndexOf('\\') + 1) + executable.split(" ")[j];
                        } else {
                        	String element = executable.split(" ")[j];
                        	if (element.equals("\"%1\"") || element.equals("/dde") ) {
                        		element = cheminFichier;
                        	}
                            cmd[j] = element;
                        }
                        System.out.println("[DEBUG] Ligne de commande : " + cmd[j]);
                    }
                    Runtime.getRuntime().exec(cmd);

                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;

    }
}
