package fr.atexo.editionEnLigne.commun;

/**
 * Commentaire
 *
 * @author Bruno Teles
 * @version
 */
public enum TypeDepot {

	ALFRESCO, DISQUE, ALFRESCO_PARAPH, ALFRESCO_COURRIER;
	
}
