![MINEF](minef.png?raw=true)

# Plateforme des Marchés Publics de l'État

## Présentation

Ce dépôt contient le code sources des modules de la plateforme [PLACE](https://www.marches-publics.gouv.fr/).

## Licence

Les différents modules composants la plateforme PLACE sont sous licence libre CeCILL v2.1. Un fichier `LICENCE.md` est
présent dans chaque sous-répertoire.

## Contenu

Le code contenu dans les différents répertoires correspond à l'état de la plateforme à la date du 23 mai 2024. Le code
peut contenir des résiduts de paramétrages internes comme des URLs de plateformes d'intégration.

## Compilation

La compilation de chacun des modules est réalisée via [maven](https://maven.apache.org/), que le code soit développée en
Java ou non. L'ensemble des dépendances de développement sont récupérées automatiquement par la commande 
d'installation maven. Des fichiers `BUILD.md` présents dans chaque sous-répertoires précisent les pré-requis particulier 
du module, et la commande de compilation à lancer.

En prérequis, il est nécessaire de configurer maven pour utiliser les dépendances externes nécessaires au build. Deux 
solutions sont possibles : 
1. Intégrer dans votre dépôt maven les librairies mise à disposition dans le dossier `libs`
2. Configurer maven pour utiliser le dépôt public hébergeant ces dépendances. La configuration de référence est :
```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                              http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <mirrors>
    <!-- Mirror for adullact-place -->
    <mirror>
      <id>adullact-place</id>
      <mirrorOf>adullact-place</mirrorOf> <!-- Only mirror adullact-place repository -->
      <url>https://nexus.local-trust.com/repository/adullact-place/</url>
    </mirror>
  </mirrors>

  <profiles>
    <profile>
      <id>adullact-place-profile</id>
      <repositories>
        <repository>
          <id>adullact-place</id>
          <url>https://nexus.local-trust.com/repository/adullact-place/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
        </repository>
        <repository>
          <id>central</id>
          <url>https://repo.maven.apache.org/maven2</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
        </repository>
        <repository>
          <id>releases.java.net</id>
          <url>https://maven.java.net/content/repositories/releases/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <blocked>false</blocked>
        </repository>
        <repository>
          <id>jvnet-nexus-staging</id>
          <url>https://maven.java.net/content/repositories/staging/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <blocked>false</blocked>
        </repository>
        <repository>
          <id>eclipse</id>
          <url>https://download.eclipse.org/rt/eclipselink/maven.repo</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <blocked>false</blocked>
        </repository>
        <repository>
          <id>jvnet-nexus-snapshots</id>
          <url>https://maven.java.net/content/repositories/snapshots/</url>
          <releases>
            <enabled>false</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
          <blocked>false</blocked>
        </repository>
        <repository>
          <id>repository jboss</id>
          <url>https://repository.jboss.org/nexus/content/groups/public/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
        </repository>
        <repository>
					<id>cefdigital</id>
					<name>CEF Digital</name>
					<url>https://ec.europa.eu/cefdigital/artifact/content/repositories/esignaturedss/</url>
        </repository>
      </repositories>
    </profile>
  </profiles>

  <activeProfiles>
    <activeProfile>adullact-place-profile</activeProfile>
  </activeProfiles>
</settings>
```

## Mise à disposition des sources

Une fois les sources compilées, elles doivent être mise à disposition sur une dépôt maven, permettant aux scripts 
d'installation Ansible de récupérer ces sources pour installation.

## Installation

L'installation des différents modules est réalisée via Ansible. L'ensemble du code des rôles et playbook permettant le 
déploiement de la solution est disponible dans le dossier `installation`