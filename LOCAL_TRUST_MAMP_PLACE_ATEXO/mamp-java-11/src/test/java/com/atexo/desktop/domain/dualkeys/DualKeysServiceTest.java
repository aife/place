package com.atexo.desktop.domain.dualkeys;

import com.atexo.desktop.config.DualKeyProperties;
import com.atexo.desktop.config.KeyStoreProperties;
import com.atexo.desktop.config.RestTemplateConfig;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dualkeys.exceptions.DualKeyGenerationException;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import com.atexo.desktop.utilitaire.Util;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs11SignatureToken;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.Base64;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class DualKeysServiceTest {
    @InjectMocks
    private DualKeysService dualKeysService;
    @Mock
    private KeyStoreService keyStoreService;
    @Mock
    private RestTemplateConfig restTemplateConfig;
    @Mock
    private RestTemplate restTemplate;
    private KeyStore keyStore;
    @Mock
    private DualKeyProperties props;
    @Mock
    KeyStoreProperties keyStoreProperties;

    @TempDir
    File tempDir;
    private Certificate publicKeyCert;

    @BeforeEach
    void setUp() throws Exception {
        String keystorePath = "src/test/resources/certs/java/cacerts";
        String keystorePassword = "changeit";
        String certificateAlias = "test";
        keyStore = KeyStore.getInstance("JKS");
        FileInputStream fis = new FileInputStream(keystorePath);
        keyStore.load(fis, keystorePassword.toCharArray());
        publicKeyCert = keyStore.getCertificate(certificateAlias);
    }

    @Test
    @DisplayName("exportFromExistingCert should call the getCertificateFrom the store")
    void exportFromExistingCert() throws RecuperationCertificatException, KeyStoreException, DualKeyGenerationException, IOException {
        when(keyStoreService.getKeyStore()).thenReturn(keyStore);
        DualKeyFormDTO dkf = DualKeyFormDTO.builder().certificateId("dummy").build();
        dualKeysService.exportFromExistingCert(dkf);

    }

    @Test
    @DisplayName("exportFromExistingCert should throw DualKeyGenerationException if it cannot get the certificate")
    void exportFromExistingCertException() throws RecuperationCertificatException {
        when(keyStoreService.getKeyStore()).thenThrow(new RecuperationCertificatException(""));
        DualKeyFormDTO dkf = DualKeyFormDTO.builder().certificateId("dummy").build();
        Assertions.assertThrows(DualKeyGenerationException.class, () -> dualKeysService.exportFromExistingCert(dkf));
    }

    @Test
    @DisplayName("generate should store the certificate")
    void generate() throws RecuperationCertificatException, IOException, DualKeyGenerationException {

        try (MockedStatic<Util> utilMocked = mockStatic(Util.class)) {
            utilMocked.when(Util::determinerProvider).thenReturn(TypeProviderEnum.JKS);

            when(keyStoreService.getKeyStore()).thenReturn(keyStore);
            when(restTemplateConfig.getRestTemplate(anyString())).thenReturn(restTemplate);
            when(restTemplate.postForObject(anyString(), any(), any())).thenReturn(Base64.getEncoder().encodeToString(publicKeyCert.getEncoded()));

            when(props.getValidity()).thenReturn(72000);

            DualKeyFormDTO dkf = DualKeyFormDTO.builder().alias("dummy").cnCleChiffrement("dummy").certificateId("id").password("password").mpeUrl("http://exemple.com").build();
            Certificate result = dualKeysService.generatex509Cert(dkf);
            assertNotNull(result);
        } catch (CertificateEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Disabled
    public void main() {

        String PIN = "2019";

        // -Djava.security.debug = sunpkcs11

        // 32b
        // Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Windows\\SysWOW64\\onepin-opensc-pkcs11.dll");

        // 64b
        // Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Windows\\System32\\beidpkcs11.dll");
        // Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Windows\\System32\\beidpkcs11.dll",
        // (PasswordInputCallback) null, 3)

        // Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Windows\\System32\\onepin-opensc-pkcs11.dll",
        // new PasswordProtection(PIN.toCharArray()), 1)
        String alias;
        try (Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Program Files\\ChamberSign\\HashLogic\\bin\\idoPKCS.dll", new KeyStore.PasswordProtection(PIN.toCharArray()), -1)) {

            List<DSSPrivateKeyEntry> keys = token.getKeys();
            for (DSSPrivateKeyEntry entry : keys) {
                System.out.println(entry.getCertificate().getCertificate());
            }

            alias = ((KSPrivateKeyEntry) keys.get(0)).getAlias();

            // ToBeSigned toBeSigned = new ToBeSigned("Hello world".getBytes());
            // SignatureValue signatureValue = token.sign(toBeSigned, DigestAlgorithm.SHA256, dssPrivateKeyEntry);
            // System.out.println("Signature value : " +
            // DatatypeConverter.printBase64Binary(signatureValue.getValue()));
        }

        try (Pkcs11SignatureToken token = new Pkcs11SignatureToken("C:\\Program Files\\ChamberSign\\HashLogic\\bin\\idoPKCS.dll", new KeyStore.PasswordProtection(PIN.toCharArray()), -1)) {

            DSSPrivateKeyEntry key = token.getKey(alias, new KeyStore.PasswordProtection(PIN.toCharArray()));

            ToBeSigned toBeSigned = new ToBeSigned("Hello world".getBytes());
            SignatureValue signatureValue = token.sign(toBeSigned, DigestAlgorithm.SHA256, key);

            System.out.println("Signature value : " + Base64.getEncoder().encodeToString(signatureValue.getValue()));
        }
    }
}
