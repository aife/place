package com.atexo.desktop.domain.keystore.jks;

import com.atexo.desktop.domain.dialog.DialogEnum;
import com.atexo.desktop.domain.dialog.DialogService;
import com.atexo.desktop.domain.dialog.dtos.DialogResponseDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@SpringBootTest
class JksStoreServiceTest {
    public static final String ALIAS = "dummy";
    public static final String ALIAS_2 = "dummy_2";
    @InjectMocks
    private JksStoreService jksStoreService;

    @Mock
    private DialogService dialogService;

    @Test
    @DisplayName("Should call the dialog service to require the password if no password is already set for that alias")
    void getPasswordFromUser() {
        DialogResponseDTO dialogResult = new DialogResponseDTO();
        dialogResult.setDismissed(false);
        dialogResult.setDenied(false);
        dialogResult.setValue("pwd");

        when(dialogService.executeDialogRequest(DialogEnum.JKS_PASSWORD, ALIAS)).thenReturn(dialogResult);

        jksStoreService.getPassword(ALIAS);

        verify(dialogService, times(1)).executeDialogRequest(DialogEnum.JKS_PASSWORD, ALIAS);
    }

    @Test
    @DisplayName("Shouldn't call the dialog enum more than once, but return the stored password instead")
    void getStoredPassword() {
        DialogResponseDTO dialogResult = new DialogResponseDTO();
        dialogResult.setDismissed(false);
        dialogResult.setDenied(false);
        dialogResult.setValue("pwd");

        when(dialogService.executeDialogRequest(DialogEnum.JKS_PASSWORD, ALIAS_2)).thenReturn(dialogResult);

        assertThat(jksStoreService.getPassword(ALIAS_2)).containsExactly('p', 'w', 'd');
        assertThat(jksStoreService.getPassword(ALIAS_2)).containsExactly('p', 'w', 'd');

        verify(dialogService, times(1)).executeDialogRequest(DialogEnum.JKS_PASSWORD, ALIAS_2);
    }

}
