package com.atexo.desktop.domain.dechiffrement;

import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dechiffrement.dtos.KeyPair;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.EnveloppeJNLP;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.KeysIvJNLP;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.OffreCertificat;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.Offres;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.boot.test.context.SpringBootTest;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.security.PrivateKey;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@SpringBootTest
class AESCryptographieServiceTest {
    @InjectMocks
    private AESCryptographieService cryptographieService;
    @Mock
    private KeyStoreService keyStoreService;
    @Mock
    private KeyPair keyPair;
    @Mock
    private PrivateKey privateKey;
    @Mock
    private Cipher cipher;

    @Test
    @DisplayName("should return an empty set when fed with no offers")
    void noOffers() {
        Offres offres = new Offres();
        offres.setOffre(Collections.emptyList());
        offres.setOrganisme("orgranisme");
        offres.setReference("reference");
        offres.setTotalEnveloppe(0);
        assertThat(cryptographieService.processOffers(offres)).isEmpty();
    }

    @Test
    @DisplayName("should accept both CMs and other protocols")
    void CmsProtocol() throws RecuperationCertificatException, BadPaddingException, IllegalBlockSizeException {
        try (MockedStatic<Cipher> mocked = mockStatic(Cipher.class)) {
            mocked.when(() -> Cipher.getInstance(anyString())).thenReturn(cipher);
            byte[] b = new byte[20];
            new Random().nextBytes(b);
            when(cipher.doFinal(any())).thenReturn(b);

            when(keyStoreService.getKeyPair(any(), any(), anyString())).thenReturn(keyPair);
            when(keyPair.getPrivateKey()).thenReturn(privateKey);

            Offres offres = new Offres();
            offres.setOffre(Arrays.asList(givenanOffer("CMS"), givenanOffer("OTHER")));
            offres.setOrganisme("orgranisme");
            offres.setReference("reference");
            offres.setTotalEnveloppe(0);
            assertThat(cryptographieService.processOffers(offres)).isEmpty();
        }
    }

    @Test
    @DisplayName("should return the certificate with no keypar associated as impossible (return list)")
    void noAssociatedKey() throws RecuperationCertificatException, BadPaddingException, IllegalBlockSizeException {
        try (MockedStatic<Cipher> mocked = mockStatic(Cipher.class)) {
            mocked.when(() -> Cipher.getInstance(anyString())).thenReturn(cipher);
            byte[] b = new byte[20];
            new Random().nextBytes(b);
            when(cipher.doFinal(any())).thenReturn(b);

            when(keyStoreService.getKeyPair(any(), any(), anyString())).thenReturn(null);
            when(keyPair.getPrivateKey()).thenReturn(privateKey);

            Offres offres = new Offres();
            offres.setOffre(Arrays.asList(givenanOffer("CMS")));
            offres.setOrganisme("orgranisme");
            offres.setReference("reference");
            offres.setTotalEnveloppe(0);
            assertThat(cryptographieService.processOffers(offres)).hasSize(1);
        }
    }

    OffreCertificat givenanOffer(String protocol) {
        KeysIvJNLP keysIvJNLP = new KeysIvJNLP();
        keysIvJNLP.setCertificate("MIIDRzCCAi+gAwIBAgIEQA6xtjANBgkqhkiG9w0BAQsFADBVMQswCQYDVQQGEwJGUjEMMAoGA1UECgwDTVBFMSYwJAYDVQQLDB1NQVJDSEVTIFBVQkxJQ1MgRUxFQ1RST05JUVVFUzEQMA4GA1UEAwwHTVBFIE1QRTAeFw0xODA4MTAwOTM4MTNaFw0yMDA4MDkwOTM4MTNaMBoxGDAWBgNVBAMMD0JJLUNMRSBBVVJFTElFTjCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAnu9Gtp9bZZlMojb/C/p8immr1c1jaTfbQKhmSRzThp85ffJ7aRzeSS+sq8Ijnws0Zp71QFEpKTvNVFnXicSSji4jE1va+9PqaRVAWvQlvsvYwqnMfCiqB/2SQufvtRQ93bPftuCJ77p4vRMUxic0VSPSQ9egXAqgoLomCzNxYN0CAwEAAaOB3TCB2jAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF4DAdBgNVHQ4EFgQUrNrhkMtvtcncgtfND7oFfeaDuDkwgYEGA1UdIwR6MHiAFB76qw5xCYoeNNL1Jjr68tNeOhovoV2kWzBZMQswCQYDVQQGEwJGUjEMMAoGA1UECgwDTVBFMSYwJAYDVQQLDB1NQVJDSEVTIFBVQkxJQ1MgRUxFQ1RST05JUVVFUzEUMBIGA1UEAwwLTVBFIFJPT1QgQ0GCAQIwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMA0GCSqGSIb3DQEBCwUAA4IBAQBvPCwKRD8b7sod/FHJzQoBOrIebe0OV8icIPC2nPt6ii+R/Q36LZrn+lEDWUQMC8C3D2qUWMEZdgPx8b7kdcm18crWdlKSh4Jd6t9/74mCqj6gAGE653hWOUCwny5ZIbPdqwjDNWCT5rTqHTNDIcKYxfkUweNrYFTcVTvGjlrT1sA+kTouca/Dl0vT5cPajAWGQciIsI8zG3GEA9Flap3s/R5o26t/VtewKwAZvIhsQNfYX7bLiiq+2gLJSmOvGsEHVsuur2HFHbXC7HGTUv8zj0xTqKm1QrcmRhpMesWeBMqVzvGZfhOgf4AJ1AsqdA2nw/8oUOKhnnqX5kEqOu+F");
        keysIvJNLP.setKey("key");
        keysIvJNLP.setIv("IV");

        EnveloppeJNLP envelope = new EnveloppeJNLP();
        envelope.setIdEnveloppe(1);
        envelope.setNumeroLot(1);
        envelope.setTypeEnveloppe("typeEnvelope");
        envelope.setCertificat(new LinkedList<>(List.of(keysIvJNLP)));

        OffreCertificat offreCertificat = new OffreCertificat();
        offreCertificat.setProtocol(protocol);
        offreCertificat.setEnveloppe(new LinkedList<>(List.of(envelope)));

        return offreCertificat;
    }

}
