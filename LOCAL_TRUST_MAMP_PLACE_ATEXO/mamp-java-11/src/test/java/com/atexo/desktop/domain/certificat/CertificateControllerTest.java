package com.atexo.desktop.domain.certificat;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.utilitaire.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

class CertificateControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @InjectMocks
    private CertificateController certificateController;

    private final static String BASE_URL = "http://localhost:";

    @ParameterizedTest
    @DisplayName("getProvider should return the proper provider depending on the call util")
    @EnumSource(value = TypeProviderEnum.class)
    void getProvider(TypeProviderEnum typeProvider) {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            mocked
                    .when(Util::determinerProvider)
                    .thenReturn(typeProvider);

            assertThat(certificateController.getProvider()).isEqualTo(typeProvider);
            mocked.verify(Util::determinerProvider, times(1));
        }
    }

    @Test
    @DisplayName("the route GET /certificat/provider should be defined")
    void getProviderEndpointDefinition() {
        var response = restTemplate.getForEntity(BASE_URL + port + "/certificat/provider/", TypeProviderEnum.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isOfAnyClassIn(TypeProviderEnum.class);
    }

    @Test
    @DisplayName("the route GET /certificat/signatureCertificats should be defined")
    void getSignatureCertificatsEndpointDefinition() {
        var response = restTemplate.getForEntity(BASE_URL + port + "/certificat/signatureCertificats", CertificateItem[].class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isOfAnyClassIn(CertificateItem[].class);
    }

    @Test
    @DisplayName("the route GET /certificat should be defined")
    void getAllCertificatsEndpointDefinition() {
        var response = restTemplate.getForEntity(BASE_URL + port + "/certificat/chiffrementCertificats", CertificateItem[].class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isOfAnyClassIn(CertificateItem[].class);
    }
}
