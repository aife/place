package com.atexo.desktop.domain.keystore;

import com.atexo.desktop.config.KeyStoreProperties;
import com.atexo.desktop.config.propertiesDTO.LinuxKeyStoreProps;
import com.atexo.desktop.domain.certificat.enums.TypeOsEnum;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.keystore.jks.JksStoreService;
import com.atexo.desktop.domain.keystore.pkcs11.Pkcs11KeyStoreService;
import com.atexo.desktop.utilitaire.MultiKeyStore;
import com.atexo.desktop.utilitaire.Util;
import eu.europa.esig.dss.token.JKSSignatureToken;
import eu.europa.esig.dss.token.MSCAPISignatureToken;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class KeyStoreServiceTest {
    @InjectMocks
    KeyStoreService keyStoreService;

    @Mock
    KeyStoreProperties keyStoreProperties;

    @Mock
    KeyStore keyStore;
    @Mock
    MultiKeyStore multiKeyStore;

    @Mock
    Pkcs11KeyStoreService pkcs11KeyStoreService;

    @Mock
    MSCAPISignatureToken mscapiSignatureToken;

    @Mock
    JKSSignatureToken jksSignatureToken;
    @Mock
    JksStoreService jksStoreService;

    @Test
    @DisplayName("GetKeyStore for Apple Provider")
    public void getKeyStoreForApple() throws RecuperationCertificatException, CertificateException, NoSuchAlgorithmException, IOException {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, TypeProviderEnum.APPLE, TypeOsEnum.MacOs);

            try (MockedStatic<KeyStore> keyStoreMocked = mockStatic(KeyStore.class)) {
                keyStoreMocked
                        .when(() -> KeyStore.getInstance(anyString(), anyString()))
                        .thenReturn(keyStore);

                keyStoreService.getKeyStore();
                verify(keyStore, times(1)).load(null, null);
                keyStoreMocked.verify(() -> KeyStore.getInstance(TypeProviderEnum.APPLE.getType(), TypeProviderEnum.APPLE.getNom()), times(1));
            }
        }
    }

    @ParameterizedTest
    @EnumSource(value = TypeProviderEnum.class, names = {"MSCAPI", "JKS", "PKCS12"})
    @DisplayName("GetKeyStore for MSCAPI, JKS and PKCS12 provider")
    public void getKeyStoreForMSCAPIAndJKSAndPKCS12(TypeProviderEnum providerEnum) throws RecuperationCertificatException, CertificateException, NoSuchAlgorithmException, IOException {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, providerEnum, TypeOsEnum.MacOs);

            try (MockedStatic<KeyStore> keyStoreMocked = mockStatic(KeyStore.class)) {
                keyStoreMocked
                        .when(() -> KeyStore.getInstance(anyString()))
                        .thenReturn(keyStore);

                keyStoreService.getKeyStore();

                verify(keyStore, times(1)).load(null, null);
                keyStoreMocked.verify(() -> KeyStore.getInstance(providerEnum.getType()), times(1));
            }
        }
    }

    @Test
    @DisplayName("GetKeyStore for PKCS11 Provider")
    public void getKeyStoreForPKCS11() throws RecuperationCertificatException, CertificateException, NoSuchAlgorithmException, IOException {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, TypeProviderEnum.PKCS11, TypeOsEnum.MacOs);

            when(pkcs11KeyStoreService.getKeyStore()).thenReturn(multiKeyStore);

            keyStoreService.getKeyStore();

            verify(multiKeyStore, times(1)).load(null, null);
            verify(pkcs11KeyStoreService, times(1)).getKeyStore();

        }
    }

    @Test
    @DisplayName("GetKeyStore for BC Provider shouldn't be implemented")
    public void getKeyStoreForBC() {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, TypeProviderEnum.BC, TypeOsEnum.MacOs);

            Assertions.assertThrows(NotImplementedException.class, () -> {
                keyStoreService.getKeyStore();
            });
        }
    }

    @Test
    @DisplayName("GetKeyStore for Linux OS should load it with proper ")
    public void getKeyStoreForLinux() throws RecuperationCertificatException, CertificateException, NoSuchAlgorithmException, IOException {
        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, TypeProviderEnum.APPLE, TypeOsEnum.Linux);

            try (MockedStatic<KeyStore> keyStoreMocked = mockStatic(KeyStore.class)) {
                keyStoreMocked
                        .when(() -> KeyStore.getInstance(anyString(), anyString()))
                        .thenReturn(keyStore);

                when(keyStoreProperties.getLinux()).thenReturn(givenaLinuxKeyStoreProps("changeit", "/etc/ssl/certs/java/cacerts"));

                keyStoreService.getKeyStore();
                verify(keyStore, times(1)).load(any(), any());
                verify(keyStoreProperties, times(2)).getLinux();
                keyStoreMocked.verify(() -> KeyStore.getInstance(TypeProviderEnum.APPLE.getType(), TypeProviderEnum.APPLE.getNom()), times(1));
            }
        }
    }

    @ParameterizedTest
    @EnumSource(value = TypeOsEnum.class, names = {"Windows", "MacOs", "Indetermine"})
    @DisplayName("GetKeyStore for OS other than linux should load the store with null args")
    public void getKeyStoreForNotLinux(TypeOsEnum typeOsEnum) throws RecuperationCertificatException, CertificateException, NoSuchAlgorithmException, IOException {

        try (MockedStatic<Util> mocked = mockStatic(Util.class)) {
            settingUtilMock(mocked, TypeProviderEnum.APPLE, typeOsEnum);

            try (MockedStatic<KeyStore> keyStoreMocked = mockStatic(KeyStore.class)) {
                keyStoreMocked
                        .when(() -> KeyStore.getInstance(anyString(), anyString()))
                        .thenReturn(keyStore);

                keyStoreService.getKeyStore();

                verify(keyStore, times(1)).load(null, null);
                keyStoreMocked.verify(() -> KeyStore.getInstance(TypeProviderEnum.APPLE.getType(), TypeProviderEnum.APPLE.getNom()), times(1));
            }
        }
    }

    @ParameterizedTest
    @EnumSource(value = TypeProviderEnum.class, names = {"PKCS12", "BC"})
    @DisplayName("getPrivateKeyEntry for PKCS12 and BC returns null")
    public void getPrivateKeyEntryForPKCS12AndBC(TypeProviderEnum typeProviderEnum) throws RecuperationCertificatException, IOException {

        assertThat(keyStoreService.getPrivateKeyEntry(typeProviderEnum, "dummy")).isNull();
    }

    @Test
    @DisplayName("getPrivateKeyEntry for PKCS11 should call the PKCS11 service")
    public void getPrivateKeyEntryForPKCS11() throws RecuperationCertificatException, IOException {
        when(pkcs11KeyStoreService.getPrivateKeyEntry(anyString())).thenReturn(null);

        keyStoreService.getPrivateKeyEntry(TypeProviderEnum.PKCS11, "dummy");

        verify(pkcs11KeyStoreService, times(1)).getPrivateKeyEntry("dummy");
    }

    @Test
    @DisplayName("isAccessible should return true if no exception is raised")
    public void isAccessibleTrue() throws RecuperationCertificatException, IOException {
        KeyStoreService mock = mock(KeyStoreService.class);
        when(mock.getKeyStore()).thenReturn(null);
        when(mock.isStoreAccessible()).thenCallRealMethod();
        assertThat(mock.isStoreAccessible()).isTrue();
    }

    @Test
    @DisplayName("isAccessible should return false if an exception is raised")
    public void isAccessibleFalse() throws RecuperationCertificatException, IOException {
        KeyStoreService mock = mock(KeyStoreService.class);
        when(mock.getKeyStore()).thenThrow(new RecuperationCertificatException(""));
        when(mock.isStoreAccessible()).thenCallRealMethod();
        assertThat(mock.isStoreAccessible()).isFalse();
    }

    private void settingUtilMock(MockedStatic<Util> mocked, TypeProviderEnum typeProvider, TypeOsEnum typeOs) {
        mocked
                .when(Util::determinerProvider)
                .thenReturn(typeProvider);
        mocked
                .when(Util::determinerOs)
                .thenReturn(typeOs);
    }

    private LinuxKeyStoreProps givenaLinuxKeyStoreProps(String password, String path) {
        LinuxKeyStoreProps result = new LinuxKeyStoreProps();
        result.setPassword(password);
        result.setPath(path);
        return result;
    }
}
