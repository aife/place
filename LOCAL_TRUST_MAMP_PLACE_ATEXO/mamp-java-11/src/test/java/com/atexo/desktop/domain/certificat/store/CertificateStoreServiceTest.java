package com.atexo.desktop.domain.certificat.store;

import com.atexo.desktop.domain.certificat.CertificateItem;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import com.atexo.desktop.domain.keystore.pkcs11.Pkcs11KeyStoreService;
import com.atexo.desktop.utilitaire.MultiKeyStore;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
class CertificateStoreServiceTest {

    @InjectMocks
    private CertificateStoreService certificateStoreService;

    @Mock
    private KeyStoreService keyStoreService;

    @Mock
    private KeyStore keyStore;

    @Mock
    private Pkcs11KeyStoreService pkcs11KeyStoreService;

    @Mock
    private MultiKeyStore multiKeyStore;

    @ParameterizedTest
    @EnumSource(value = CertificateFilterEnum.class)
    @DisplayName("An empty store and pkcs11store should return no certificate items")
    void getCertificateFromEmptyStore(CertificateFilterEnum certificateFilter) throws RecuperationCertificatException, KeyStoreException, IOException {
        settingStoresStubs(Collections.emptyList(), Collections.emptyList());

        assertThat(certificateStoreService.getAllCertificates(certificateFilter)).isEmpty();
    }

    @Test
    @DisplayName("getAllCertificate with a DUAL_KEY filter should return only proper certificate")
    void getCertificateWithDualKeyFilter() throws RecuperationCertificatException, OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        List<ImmutableTriple<String, Boolean, X509Certificate>> storeStubData = Arrays.asList(
                givenaKeyStoreEntry("dummy1", true, "issuer1", new Date(), new Date(), "subject1", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.dataEncipherment)),
                givenaKeyStoreEntry("dummy2", true, "issuer2", new Date(), new Date(), "subject2", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.decipherOnly)),
                givenaKeyStoreEntry("dummy3", true, "issuer3", new Date(), new Date(), "subject3", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.keyEncipherment))
        );
        settingStoresStubs(storeStubData, Collections.emptyList());

        Set<CertificateItem> certificatesToTest = certificateStoreService.getAllCertificates(CertificateFilterEnum.DUAL_KEY);

        assertThat(certificatesToTest).hasSize(2);

        assertThat(certificatesToTest.stream().filter(c -> c.getId().equals("dummy1")).findFirst().get())
                .hasFieldOrPropertyWithValue("id", "dummy1");

        assertThat(certificatesToTest.stream().filter(c -> c.getId().equals("dummy3")).findFirst().get())
                .hasFieldOrPropertyWithValue("id", "dummy3");
    }

    @Test
    @DisplayName("getAllCertificate with a RGS filter should return only proper certificate")
    void getCertificateWithRGSFilter() throws RecuperationCertificatException, OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, +1);
        Date validEndingDate = calendar.getTime();
        calendar.add(Calendar.DATE, -2);
        Date invalidEndingDate = calendar.getTime();
        List<ImmutableTriple<String, Boolean, X509Certificate>> storeStubData = Arrays.asList(
                givenaKeyStoreEntry("dummy1", true, "issuer1", new Date(), validEndingDate, "subject1", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.dataEncipherment)),
                givenaKeyStoreEntry("dummy2", true, "issuer2", new Date(), validEndingDate, "subject2", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.digitalSignature)),
                givenaKeyStoreEntry("dummy3", true, "issuer3", new Date(), validEndingDate, "subject3", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.nonRepudiation)),
                givenaKeyStoreEntry("dummy4", true, "issuer4", new Date(), validEndingDate, "subject4", TypeProviderEnum.JKS, getAllUsage()),
                givenaKeyStoreEntry("dummy5", true, "issuer5", new Date(), invalidEndingDate, "subject5", TypeProviderEnum.JKS, Collections.singletonList(KeyUsage.nonRepudiation))
        );

        settingStoresStubs(storeStubData, Collections.emptyList());

        Set<CertificateItem> certificatesToTest = certificateStoreService.getAllCertificates(CertificateFilterEnum.RGS);

        assertThat(certificatesToTest).hasSize(1);

        assertThat(certificatesToTest.stream().filter(c -> c.getId().equals("dummy3")).findFirst().get())
                .hasFieldOrPropertyWithValue("id", "dummy3");
    }

    @Test
    @DisplayName("getAllCertificate should merge certificate from both store and pkcs11 store")
    void getCertificateFromBothStores() throws RecuperationCertificatException, OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        List<ImmutableTriple<String, Boolean, X509Certificate>> storeStubData = Collections.singletonList(
                givenaKeyStoreEntry("dummy1", true, "issuer1", new Date(), new Date(), "subject1", TypeProviderEnum.JKS, getAllUsage())
        );

        List<ImmutableTriple<String, Boolean, X509Certificate>> pkcs11StubData = Collections.singletonList(
                givenaKeyStoreEntry("dummy2", true, "issuer2", new Date(), new Date(), "subject2", TypeProviderEnum.PKCS11, getAllUsage())
        );
        settingStoresStubs(storeStubData, pkcs11StubData);

        Set<CertificateItem> certificatesToTest = certificateStoreService.getAllCertificates(CertificateFilterEnum.DUAL_KEY);

        assertThat(certificatesToTest).hasSize(2);
    }

    @Test
    @DisplayName("getAllCertificate should ignore non entry")
    void getCertificateEntries() throws RecuperationCertificatException, OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        List<ImmutableTriple<String, Boolean, X509Certificate>> storeStubData = Arrays.asList(
                givenaKeyStoreEntry("dummy1", true, "issuer1", new Date(), new Date(), "subject1", TypeProviderEnum.JKS, getAllUsage()),
                givenaKeyStoreEntry("dummy2", false, "issuer2", new Date(), new Date(), "subject2", TypeProviderEnum.JKS, getAllUsage())
        );

        settingStoresStubs(storeStubData, Collections.emptyList());

        Set<CertificateItem> certificatesToTest = certificateStoreService.getAllCertificates(CertificateFilterEnum.DUAL_KEY);

        assertThat(certificatesToTest).hasSize(1);
    }

    @Test
    @DisplayName("getAllCertificate should ignore certificates with no usage")
    void getCertificateWithNoUsage() throws RecuperationCertificatException, OperatorCreationException, CertificateException, IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        List<ImmutableTriple<String, Boolean, X509Certificate>> storeStubData = Arrays.asList(
                givenaKeyStoreEntry("dummy1", true, "issuer1", new Date(), new Date(), "subject1", TypeProviderEnum.JKS, getAllUsage()),
                givenaKeyStoreEntry("dummy2", true, "issuer2", new Date(), new Date(), "subject2", TypeProviderEnum.JKS, Collections.emptyList())
        );

        settingStoresStubs(storeStubData, Collections.emptyList());

        Set<CertificateItem> certificatesToTest = certificateStoreService.getAllCertificates(CertificateFilterEnum.DUAL_KEY);

        assertThat(certificatesToTest).hasSize(1);
    }


    private List<Integer> getAllUsage() {
        return Arrays.asList(
                KeyUsage.digitalSignature,
                KeyUsage.nonRepudiation,
                KeyUsage.keyEncipherment,
                KeyUsage.dataEncipherment,
                KeyUsage.keyAgreement,
                KeyUsage.keyCertSign,
                KeyUsage.cRLSign,
                KeyUsage.encipherOnly,
                KeyUsage.decipherOnly
        );
    }

    private void settingStoresStubs(List<ImmutableTriple<String, Boolean, X509Certificate>> storeAliases, List<ImmutableTriple<String, Boolean, X509Certificate>> pkcs11Aliases) throws RecuperationCertificatException, KeyStoreException, IOException {
        when(keyStoreService.getKeyStore()).thenReturn(keyStore);
        when(keyStore.aliases()).thenReturn(new Vector<>(storeAliases.stream().map(ImmutableTriple::getLeft).collect(Collectors.toList())).elements());
        for (ImmutableTriple<String, Boolean, X509Certificate> triple : storeAliases) {
            when(keyStore.isKeyEntry(triple.getLeft())).thenReturn(triple.getMiddle());
            when(keyStore.getCertificate(triple.getLeft())).thenReturn(triple.getRight());
        }

        when(pkcs11KeyStoreService.getKeyStore()).thenReturn(multiKeyStore);
        when(multiKeyStore.aliases()).thenReturn(new Vector<>(pkcs11Aliases.stream().map(ImmutableTriple::getLeft).collect(Collectors.toList())).elements());
        for (ImmutableTriple<String, Boolean, X509Certificate> triple : pkcs11Aliases) {
            when(multiKeyStore.isKeyEntry(triple.getLeft())).thenReturn(triple.getMiddle());
            when(multiKeyStore.getCertificate(triple.getLeft())).thenReturn(triple.getRight());
        }
    }

    private ImmutableTriple<String, Boolean, X509Certificate> givenaKeyStoreEntry(String alias, Boolean isEntry, String issuer, Date beginningDate, Date EndingDate, String subject, TypeProviderEnum typeProvider, List<Integer> keyUsage) throws NoSuchAlgorithmException, CertificateException, CertIOException, NoSuchProviderException, OperatorCreationException {
        return new ImmutableTriple<>(alias, isEntry, givenaX509Cert(issuer, beginningDate, EndingDate, subject, typeProvider, keyUsage));
    }

    private X509Certificate givenaX509Cert(String issuer, Date beginningDate, Date EndingDate, String subject, TypeProviderEnum typeProvider, List<Integer> keyUsage) throws NoSuchProviderException, NoSuchAlgorithmException, CertificateException, OperatorCreationException, CertIOException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(TypeProviderEnum.JKS.getKeyAlgorithm(), TypeProviderEnum.JKS.getNom());
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        X509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(
                new X500Principal("CN=" + issuer), BigInteger.valueOf(System.currentTimeMillis()), beginningDate, EndingDate, new X500Principal("CN=" + subject), keyPair.getPublic());

        builder.addExtension(X509Extensions.KeyUsage, true, new KeyUsage(keyUsage.stream().mapToInt(Integer::intValue).sum()));

        ContentSigner signer = new JcaContentSignerBuilder(TypeProviderEnum.JKS.getSignatureAlgorithm()).setProvider(TypeProviderEnum.JKS.getNom()).build(keyPair.getPrivate());
        final X509CertificateHolder holder = builder.build(signer);

        return new JcaX509CertificateConverter().setProvider(TypeProviderEnum.JKS.getNom()).getCertificate(holder);
    }

}
