package com.atexo.desktop.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.test.util.JsonExpectationsHelper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public abstract class TestConfig {


    protected final ObjectMapper objectMapper = new ObjectMapper();
    protected final JsonExpectationsHelper jsonHelper = new JsonExpectationsHelper();

    protected static final String JSON_PATH = "src/test/resources/json/";
    private static final String STUB_PATH = "src/test/resources/json/expected/";
    private static final String STUB_EXTENSION = ".json";

    @BeforeEach
    public void setUp() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());
    }


    protected static String readStub(String stubId) throws IOException {
        Path pathSource = Paths.get(STUB_PATH + stubId + STUB_EXTENSION).toAbsolutePath();
        List<String> lines = Files.readAllLines(pathSource, Charset.defaultCharset());
        return lines.stream().reduce((result, concatWith) -> (result + concatWith.trim()).replace("\": ", "\":")).orElse(null);
    }

    protected <T> void assertJsonEqual(String expectedStubId, T actualObject) throws Exception {

        jsonHelper.assertJsonEqual(readStub(expectedStubId + "_expected"), objectMapper.writeValueAsString(actualObject), false);
    }


    protected <T> T getObject(String jsonId, Class<T> tClass) throws IOException {
        Path pathSource = Paths.get(JSON_PATH + jsonId + STUB_EXTENSION).toAbsolutePath();
        return objectMapper.readValue(pathSource.toFile(), tClass);
    }

}
