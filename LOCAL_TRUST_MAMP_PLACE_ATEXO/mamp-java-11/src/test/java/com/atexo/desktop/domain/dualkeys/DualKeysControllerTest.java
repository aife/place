package com.atexo.desktop.domain.dualkeys;

import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dualkeys.exceptions.DualKeyGenerationException;
import com.atexo.desktop.domain.dualkeys.exceptions.DuplicateAliasException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class DualKeysControllerTest {
    @InjectMocks
    private DualKeysController dualKeysController;

    @Mock
    private DualKeysService dualKeysService;
    private Certificate publicKeyCert;

    @BeforeEach
    void setUp() throws Exception {

        String keystorePath = "src/test/resources/certs/java/cacerts";
        String keystorePassword = "changeit";
        String certificateAlias = "test";

        KeyStore keystore = KeyStore.getInstance("JKS");
        FileInputStream fis = new FileInputStream(keystorePath);
        keystore.load(fis, keystorePassword.toCharArray());

        publicKeyCert = keystore.getCertificate(certificateAlias);


    }

    @Test
    @DisplayName("should call the proper method from service then send request to the callback")
    void generateFromExisting() throws DualKeyGenerationException, CertificateEncodingException {
        DualKeyFormDTO dkf = DualKeyFormDTO.builder()
                .caUrl("callbackUrl")
                .alias("alias").mpeUrl("mpeUrl").sessionId("ticket").existingCert(true).build();


        when(dualKeysService.exportFromExistingCert(any())).thenReturn(publicKeyCert);
        when(dualKeysService.sendDataToCallbackURL(anyString(), anyString(), any())).thenReturn(true);


        GenerationResultDTO actual = dualKeysController.generateDualKeys(dkf);
        assertThat(actual).isEqualToComparingFieldByField(GenerationResultDTO.builder().generationExecuted(false).generationStatus(false).sendingMpeStatus(true).build());

        verify(dualKeysService, times(1)).exportFromExistingCert(dkf);
        verify(dualKeysService, times(1)).sendDataToCallbackURL(eq("callbackUrl"), eq("ticket"), any());
    }

    @Test
    @DisplayName("should call the proper method from service then send request to the callback")
    void generate() throws DualKeyGenerationException, CertificateEncodingException, DuplicateAliasException, RecuperationCertificatException {
        DualKeyFormDTO dkf = DualKeyFormDTO.builder().alias("alias").mpeUrl("mpeUrl").sessionId("ticket")
                .caUrl("callbackUrl")
                .existingCert(false).build();


        when(dualKeysService.generatex509Cert(any())).thenReturn(publicKeyCert);
        when(dualKeysService.sendDataToCallbackURL(anyString(), anyString(), any())).thenReturn(false);
        assertThat(dualKeysController.generateDualKeys(dkf)).isEqualToComparingFieldByField(GenerationResultDTO.builder().generationExecuted(true).generationStatus(true).sendingMpeStatus(false).build());

        verify(dualKeysService, times(1)).generatex509Cert(dkf);
        verify(dualKeysService, times(1)).sendDataToCallbackURL(eq("callbackUrl"), eq("ticket"), any());
    }
}
