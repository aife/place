package com.atexo.desktop.domain.certificat;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.utilitaire.CertificatUtil;
import com.atexo.desktop.utilitaire.Util;
import lombok.*;

import java.security.cert.X509Certificate;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class CertificateItem {

    private String id;
    private String nom;
    private String nomEmetteur;
    private String strDateExpiration;
    private Date dateExpiration;
    private String utilisationCle;
    private boolean smartCard;
    private String hashCode;
    private TypeProviderEnum typeProvider;
    private String conformite;

    public CertificateItem(String id, String nom, String nomEmetteur, String strDateExpiration, Date dateExpiration, String utilisationCle, boolean smartCard, String hashCode, TypeProviderEnum typeProvider) {
        this.id = id;
        this.nom = nom;
        this.nomEmetteur = nomEmetteur;
        this.strDateExpiration = strDateExpiration;
        this.dateExpiration = dateExpiration;
        this.utilisationCle = utilisationCle;
        this.smartCard = smartCard;
        this.hashCode = hashCode;
        this.typeProvider = typeProvider;
    }

    public CertificateItem(String alias, X509Certificate cert, TypeProviderEnum provider) {
        this.id = alias;
        this.nom = CertificatUtil.getCN(cert.getSubjectDN());
        this.nomEmetteur = CertificatUtil.getCN(cert.getIssuerDN());
        this.strDateExpiration = Util.formaterDate(cert.getNotAfter());
        this.dateExpiration = cert.getNotAfter();
        this.utilisationCle = CertificatUtil.getUtilisablePour(cert, true);
        this.smartCard = provider == TypeProviderEnum.PKCS11;
        this.hashCode = String.valueOf(cert.hashCode());
        this.typeProvider = provider;
        if (CertificatUtil.isConformite(cert)) {
            this.setConformite("Adapt\u00E9e");
        } else {
            this.setConformite("Tol\u00E9r\u00E9e jusqu'en septembre 2017");
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomEmetteur() {
        return nomEmetteur;
    }

    public void setNomEmetteur(String nomEmetteur) {
        this.nomEmetteur = nomEmetteur;
    }

    public String getStrDateExpiration() {
        return strDateExpiration;
    }

    public void setStrDateExpiration(String dateExpiration) {
        this.strDateExpiration = dateExpiration;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public boolean isSmartCard() {
        return smartCard;
    }

    public TypeProviderEnum getTypeProvider() {
        return typeProvider;
    }

    public String getConformite() {
        return conformite;
    }

    public void setConformite(String conformite) {
        this.conformite = conformite;
    }

    @Override
    public String toString() {
        return "CertificateItem{" + "id='" + id + '\'' + ", nom='" + nom + '\'' + ", smartCard='" + smartCard + '\'' + ", nomEmetteur='" + nomEmetteur + '\'' + ", strDateExpiration='" + strDateExpiration + '\'' + ", utilisationCle='" + utilisationCle
                + '\'' + ", typeProvider='" + typeProvider + '\'' + ", hashCode='" + hashCode + '\'' + '}';
    }

}
