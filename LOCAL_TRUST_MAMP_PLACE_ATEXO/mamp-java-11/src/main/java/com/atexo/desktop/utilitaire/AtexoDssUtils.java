package com.atexo.desktop.utilitaire;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import java.io.File;
import java.util.List;

/**
 * Utility class that contains various method to handle conversion to DSS
 * document format.
 */
public class AtexoDssUtils {

	private AtexoDssUtils(){}

	/**
	 * This method will convert a String hexa to it's representation as an array of
	 * Bytes.
	 *
	 * @param hex
	 * @return {@link String} representation of the Hexa.
	 */
	public static byte[] hexToBytes(final String hex) {
		int len = hex.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * This method will load all certification files in the defined path.
	 *
	 * @return List of All files in the trusted store path.
	 */
	public static List<File> loadAllFile(final String pathToBeLoaded) {
		return (List<File>) FileUtils.listFiles(new File(pathToBeLoaded), new RegexFileFilter("^(.*?)"), DirectoryFileFilter.DIRECTORY);
	}


}
