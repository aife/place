package com.atexo.desktop.domain.dechiffrement.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OffreProgressResponse {
	private int totalEnveloppe;
	private int progressEnveloppe;
	private int totalFichier;
	private int progressFichier;
	private String status;
	private String type;
}
