package com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce;


import com.atexo.desktop.utilitaire.JAXBService;

import java.util.*;

/**
 * Classe d'utilitaire permettant de créer une ReponsesAnnonce XML.
 */
public abstract class JaxbReponsesAnnonceUtil {
    private final static ObjectFactory factory = new ObjectFactory();

    public static ReponseAnnonce getReponseAnnonce(String reponseAnnonceXML) {
        String context = ReponseAnnonce.class.getPackage().getName();
        Object reponseAnnonceObject = JAXBService.instance().getAsObject(reponseAnnonceXML, context);
        return (ReponseAnnonce) reponseAnnonceObject;
    }

    public static Map<Integer, List<FichierBloc>> extraireFichierBlocChiffrements(ReponseAnnonce reponseAnnonceType, Set<Integer> idEnveloppes) {

        Map<Integer, List<FichierBloc>> map = new LinkedHashMap<>();

        ReponseAnnonceType.Annonce annonce = reponseAnnonceType.getAnnonce();
        ReponseAnnonceType.OperateurEconomique operateurEconomique = reponseAnnonceType.getOperateurEconomique();

        for (ReponseAnnonceType.Enveloppes.Enveloppe enveloppe : reponseAnnonceType.getEnveloppes().getEnveloppe()) {

            if (idEnveloppes == null || idEnveloppes.contains(enveloppe.getIdEnveloppe())) {

                List<FichierBloc> fichierBlocs = map.computeIfAbsent(enveloppe.getIdEnveloppe(), k -> new ArrayList<>());

                List<FichierBloc> fichierBlocsTemporaire = extraireFichierBlocChiffrements(annonce, enveloppe, operateurEconomique, 1);
                if (!fichierBlocsTemporaire.isEmpty()) {
                    fichierBlocs.addAll(fichierBlocsTemporaire);
                }
            }
        }

        return map;
    }  

    private static List<FichierBloc> extraireFichierBlocChiffrements(ReponseAnnonceType.Annonce annonce, ReponseAnnonceType.Enveloppes.Enveloppe enveloppe,
                                                                     ReponseAnnonceType.OperateurEconomique operateurEconomique, int index) {

        List<FichierBloc> liste = new ArrayList<>();

        String nomRepertoireEnveloppe = "El" + index + "_" + getTypeEnvNomRepertoire(enveloppe.getType(), enveloppe.getNumLot());

        for (ReponseAnnonceType.Enveloppes.Enveloppe.Fichier fichier : enveloppe.getFichier()) {

            List<FichierBloc.Bloc> blocs = new ArrayList<FichierBloc.Bloc>();

            for (ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement blocChiffrement : fichier.getBlocsChiffrement().getBlocChiffrement()) {
                Integer numero = blocChiffrement.getNumOrdre();
                String nomBloc = blocChiffrement.getNomBloc();
                Long taille = blocChiffrement.getTailleApresChiffrement();
                boolean chiffre = ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE.equals(blocChiffrement.getStatutChiffrement());
                FichierBloc.Bloc bloc = new FichierBloc.Bloc(nomBloc, numero, taille, chiffre, null, null);
                blocs.add(bloc);
            }

            String informationsUtilisateur = annonce != null ? annonce.getReferenceUtilisateur() : null;
            FichierBloc fichierBloc = new FichierBloc(fichier.getIdFichier(), enveloppe.getType(), enveloppe.getNumLot(), fichier.getNom(), blocs, informationsUtilisateur,
                    fichier.getTailleEnClair(), fichier.getEmpreinte());
            fichierBloc.setNomEnveloppe(nomRepertoireEnveloppe);
            if (operateurEconomique != null && operateurEconomique.getNom() != null && !operateurEconomique.getNom().isEmpty()) {
                fichierBloc.setNomOperateurEconomique(operateurEconomique.getNom());
            }
            liste.add(fichierBloc);
        }

        return liste;
    }


    private static String getTypeEnvNomRepertoire(int typeEnveloppe, int numeroLot) {
        if (typeEnveloppe == 1) {
            return "Candidature";
        } else if (typeEnveloppe == 2 && numeroLot == 0) {
            return "Offre";
        } else if (typeEnveloppe == 2 && numeroLot > 0) {
            return "Offre_Lot_" + numeroLot;
        } else if (typeEnveloppe == 3 && numeroLot == 0) {
            return "Anonymat";
        } else if (typeEnveloppe == 3 && numeroLot > 0) {
            return "Anonymat_Lot_" + numeroLot;
        } else if (typeEnveloppe == 4 && numeroLot == 0) {
            return "OffreTechnique";
        } else if (typeEnveloppe == 4 && numeroLot > 0) {
            return "OffreTechnique_Lot" + numeroLot;
        }
        return "Indetermine";
    }
}
