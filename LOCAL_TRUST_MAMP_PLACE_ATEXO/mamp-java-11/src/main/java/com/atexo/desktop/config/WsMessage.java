package com.atexo.desktop.config;

import lombok.*;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WsMessage implements Serializable {
	private static final long serialVersionUID = -2973241346680842427L;
	private int code;
	private String message;
	private String type;

}
