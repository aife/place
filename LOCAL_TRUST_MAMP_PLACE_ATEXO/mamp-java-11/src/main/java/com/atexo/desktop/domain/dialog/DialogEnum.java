package com.atexo.desktop.domain.dialog;

import com.atexo.desktop.domain.dialog.dtos.DialogConfigDTO;

/**
 * Enum to define all dialog (prompt) displayed on electron side
 */
public enum DialogEnum {
	SYSTEM_PASSWORD {
		@Override
		public DialogConfigDTO getDialogConfigDTO(String alias) {
			return DialogConfigDTO.builder()
					.title("Mot de passe utilisateur" + alias)
					.input("password")
					.inputLabel("Entrez votre mot de passe")
					.cancelButton(true)
					.backdrop(false)
					.build();
		}
	},
	PKCSS11_PASSWORD {
		@Override
		public DialogConfigDTO getDialogConfigDTO(String alias) {
			return DialogConfigDTO.builder()
					.title("Mot de passe pour PKCS11" + alias)
					.input("password")
					.inputLabel("Entrez votre mot de passe")
					.cancelButton(true)
					.backdrop(false)
					.build();
		}
	},
	JKS_PASSWORD {
		@Override
		public DialogConfigDTO getDialogConfigDTO(String alias) {
			return DialogConfigDTO.builder()
					.title("Mot de passe JKS : " + alias)
					.input("password")
					.inputLabel("Entrez votre mot de passe")
					.cancelButton(true)
					.backdrop(false)
					.build();
		}
	};


	public abstract DialogConfigDTO getDialogConfigDTO(String alias);
}
