package com.atexo.desktop.config;

import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.Offres;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.ReponseAnnonce;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CryptographieClient {

	@Bean
	Map<String, Offres> getOffresMaps() {
		return new HashMap<>();
	}

	@Bean
	Map<String, Map<String, ReponseAnnonce>> getSessionReponsesAnnonces() {
		return new HashMap<>();
	}

	@Bean
	Map<String, StringBuilder> getSessionReponsesAnnoncesXml() {
		return new HashMap<>();
	}

}
