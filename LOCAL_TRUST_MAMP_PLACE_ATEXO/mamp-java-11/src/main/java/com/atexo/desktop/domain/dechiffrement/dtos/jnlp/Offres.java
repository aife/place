package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
@XmlRootElement(name = "Offres")
public class Offres implements Serializable {
    private String reference;
    private String organisme;
    private List<OffreCertificat> offre;
	private int totalEnveloppe;

    
	public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

	public List<OffreCertificat> getOffre() {
		return offre;
	}

	public void setOffre(List<OffreCertificat> offre) {
		this.offre = offre;
	}

	public int getTotalEnveloppe() {
		return totalEnveloppe;
	}

	public void setTotalEnveloppe(int totalEnveloppe) {
		this.totalEnveloppe = totalEnveloppe;
	}
}
