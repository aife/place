package com.atexo.desktop.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
@ConfigurationProperties(prefix = "pkcs11libs")
@Getter
@Setter
public class Pkcs11LibsProperties {
    private List<String> gemalto = Collections.emptyList();
    private List<String> oberthur = Collections.emptyList();
    private List<String> safeNet = Collections.emptyList();
    private List<String> sagem = Collections.emptyList();

    public Map<String, List<File>> getPkcs11Providers() {
        Map<String, List<File>> mapProviders = new HashMap<>();
        mapProviders.put("Gemalto", getListFile(gemalto));
        mapProviders.put("Oberthur", getListFile(oberthur));
        mapProviders.put("SafeNet", getListFile(safeNet));
        mapProviders.put("Sagem", getListFile(sagem));
        return mapProviders;
    }

    private List<File> getListFile(List<String> provider) {
        return provider.stream()
                .map(File::new)
                .filter(File::exists)
                .collect(Collectors.toList());
    }
}
