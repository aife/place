package com.atexo.desktop.utilitaire;

import com.atexo.desktop.domain.certificat.enums.TypeOsEnum;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * Classe d'utilitaire.
 */
public abstract class Util {

    private Util() {
    }

    public static final String DATE_PATTERN_SLASH = "dd/MM/yyyy";


    public static String formaterDate(Date date) {
        return formaterDate(date, DATE_PATTERN_SLASH);
    }

    public static String formaterDate(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static TypeProviderEnum determinerProvider() {
        String nomOs = System.getProperty("os.name");
        return determinerProvider(nomOs);
    }

    public static TypeProviderEnum determinerProvider(String nomOs) {

        if (nomOs.startsWith("Windows")) {
            return TypeProviderEnum.MSCAPI;
        } else if (nomOs.startsWith("Mac")) {
            return TypeProviderEnum.APPLE;
        } else if (nomOs.startsWith("Linux")) {
            return TypeProviderEnum.JKS;
        } else {
            return TypeProviderEnum.PKCS12;
        }
    }

    public static TypeOsEnum determinerOs() {
        String nomOs = System.getProperty("os.name");
        if (nomOs.startsWith("Windows")) {
            return TypeOsEnum.Windows;
        } else if (nomOs.startsWith("Mac")) {
            return TypeOsEnum.MacOs;
        } else if (nomOs.startsWith("Linux")) {
            return TypeOsEnum.Linux;
        } else {
            return TypeOsEnum.Indetermine;
        }
    }

    public static <T> Stream<T> enumerationAsStream(Enumeration<T> e) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(new Iterator<T>() {
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return e.nextElement();
            }

            public boolean hasNext() {
                return e.hasMoreElements();
            }

            public void forEachRemaining(Consumer<? super T> action) {
                while (e.hasMoreElements()) action.accept(e.nextElement());
            }
        }, Spliterator.ORDERED), false);
    }

}
