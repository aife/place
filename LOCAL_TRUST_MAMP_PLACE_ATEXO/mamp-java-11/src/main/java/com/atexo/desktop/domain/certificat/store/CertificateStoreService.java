package com.atexo.desktop.domain.certificat.store;

import com.atexo.desktop.domain.certificat.CertificateItem;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import com.atexo.desktop.domain.keystore.pkcs11.Pkcs11KeyStoreService;
import com.atexo.desktop.utilitaire.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class CertificateStoreService {

    public static final TypeProviderEnum PROVIDER = Util.determinerProvider();
    private KeyStoreService keyStoreService;
    private Pkcs11KeyStoreService pkcs11KeyStoreService;

    public CertificateStoreService(KeyStoreService keyStoreService, Pkcs11KeyStoreService pkcs11KeyStoreService) {
        this.keyStoreService = keyStoreService;
        this.pkcs11KeyStoreService = pkcs11KeyStoreService;
    }

    /**
     * retrieve all certificates Pkcs11 + certificates from the system store depending on the CertificateFilterEnum
     *
     * @param certificateFilterEnum
     * @return all certificates without duplicates
     * @throws RecuperationCertificatException
     */
    public Set<CertificateItem> getAllCertificates(CertificateFilterEnum certificateFilterEnum) throws RecuperationCertificatException, IOException {
        Set<CertificateItem> allPkcs11Certificates = getAllPkcs11Certificate(certificateFilterEnum);
        KeyStore keyStore = keyStoreService.getKeyStore();
        Set<CertificateItem> certificateItemsFromStore = getCertificateItemsFromStore(certificateFilterEnum, keyStore, PROVIDER);

        return Stream.of(allPkcs11Certificates, certificateItemsFromStore)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private Set<CertificateItem> getCertificateItemsFromStore(CertificateFilterEnum certificateFilterEnum, KeyStore keyStore, TypeProviderEnum provider) throws RecuperationCertificatException {
        try {
            return Util.enumerationAsStream(keyStore.aliases())
                    .filter(alias -> isKeyEntry(keyStore, alias))
                    .map(alias -> new ImmutablePair<>(alias, getCertificateFromStore(keyStore, alias)))
                    .filter(pair -> pair.getRight() != null && pair.getRight().getKeyUsage() != null)
                    .filter(pair -> certificateFilterEnum.getFilterFunction().apply(pair.getRight()))
                    .map(pair -> new CertificateItem(pair.getLeft(), pair.getRight(), provider))
                    .collect(Collectors.toSet());

        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new RecuperationCertificatException("Erreur lors de la récupération  des certificats se trouvant dans le key store du provider " + PROVIDER, ex);
        }
    }

    private Set<CertificateItem> getAllPkcs11Certificate(CertificateFilterEnum certificateFilterEnum) throws RecuperationCertificatException {
        KeyStore store = pkcs11KeyStoreService.getKeyStore();

        return getCertificateItemsFromStore(certificateFilterEnum, store, TypeProviderEnum.PKCS11);
    }

    private boolean isKeyEntry(KeyStore keyStore, String alias) {
        try {
            return keyStore.isKeyEntry(alias);
        } catch (KeyStoreException e) {
            throw new RuntimeException();
        }
    }

    private X509Certificate getCertificateFromStore(KeyStore keyStore, String alias) {
        try {
            return (X509Certificate) keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            throw new RuntimeException();
        }
    }

}
