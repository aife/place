package com.atexo.desktop.domain.dialog.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collections;

@Getter
@Setter
@Builder
public class DialogConfigDTO {
    private String title;
    private String input;
    private String inputLabel;
    private boolean cancelButton;
    private boolean backdrop;

    public MultiValueMap<String, String> toParams() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("title", Collections.singletonList(title));
        params.put("input", Collections.singletonList(input));
        params.put("inputLabel", Collections.singletonList(inputLabel));
        params.put("showCancelButton", Collections.singletonList(Boolean.toString(cancelButton)));
        params.put("backdrop", Collections.singletonList(Boolean.toString(backdrop)));
        return params;
    }
}
