package com.atexo.desktop.domain.certificat.store;

import java.security.cert.X509Certificate;
import java.util.function.Function;

/**
 * Enum defining the rules to filter the certificates
 * The lambda represents the filtering logic
 */
public enum CertificateFilterEnum implements Function<X509Certificate, Boolean> {
    STANDARD() {
        @Override
        public Function<X509Certificate, Boolean> getFilterFunction() {
            return cert -> CertificateConstants.isUsableForEncryption.test(cert)
                    || CertificateConstants.isUsableForElectronicSignature.test(cert);
        }
    },
    DUAL_KEY() {
        @Override
        public Function<X509Certificate, Boolean> getFilterFunction() {
            return CertificateConstants.isUsableForEncryption::test;
        }
    },
    RGS {
        @Override
        public Function<X509Certificate, Boolean> getFilterFunction() {
            return cert -> !CertificateConstants.isUsableForEncryption.test(cert)
                    && CertificateConstants.isUsableForElectronicSignature.test(cert)
                    && CertificateConstants.hasValidDate.test(cert)
                    && CertificateConstants.isConform.test(cert);
        }
    };

    public abstract Function<X509Certificate, Boolean> getFilterFunction();

    @Override
    public Boolean apply(X509Certificate cert) {
        return getFilterFunction().apply(cert);
    }

}
