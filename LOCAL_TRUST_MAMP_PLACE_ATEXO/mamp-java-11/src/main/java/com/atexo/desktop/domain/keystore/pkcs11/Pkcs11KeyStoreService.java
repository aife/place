package com.atexo.desktop.domain.keystore.pkcs11;

import com.atexo.desktop.config.Pkcs11LibsProperties;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dialog.DialogEnum;
import com.atexo.desktop.domain.dialog.DialogService;
import com.atexo.desktop.domain.dialog.dtos.DialogResponseDTO;
import com.atexo.desktop.utilitaire.MultiKeyStore;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs11SignatureToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class Pkcs11KeyStoreService {

    public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";

    private static Map<String, char[]> password = new HashMap<>();
    private final DialogService dialogService;
    private final Pkcs11LibsProperties pkcs11LibsProperties;

    public Pkcs11KeyStoreService(DialogService dialogService, Pkcs11LibsProperties pkcs11LibsProperties) {
        this.pkcs11LibsProperties = pkcs11LibsProperties;
        this.dialogService = dialogService;
    }

    public MultiKeyStore getKeyStore() throws RecuperationCertificatException {
        Map<String, List<File>> pkcs11Providers = pkcs11LibsProperties.getPkcs11Providers();

        List<ImmutablePair<String, File>> flattenProvidersWithLibs =
                pkcs11Providers.entrySet().stream()
                        .flatMap(e -> e.getValue().stream().map(v -> new ImmutablePair<>(e.getKey(), v)))
                        .collect(Collectors.toList());

        Map<String, KeyStore> keyStores = flattenProvidersWithLibs.stream()
                .filter(p -> p.getRight().exists())
                .map(p -> new ImmutablePair<>(p.getLeft(), getAuthProvider(p.getLeft(), p.getRight())))
                .filter(p -> p.getRight() != null)
                .peek(this::removeOldProvider)
                .map(p -> new ImmutablePair<>(p.getLeft(), isProviderValid(p.getRight())))
                .filter(p -> p.getRight() != null && p.getLeft() != null)
                .collect(Collectors.toMap(Pair::getLeft, Pair::getRight, (o1, o2) -> o1));

        MultiKeyStore keyStore = new MultiKeyStore(keyStores);

        try {
            keyStore.load(null, null);
            return keyStore;
        } catch (NoSuchAlgorithmException | CertificateException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + TypeProviderEnum.PKCS11, e);
        } catch (IOException e) {
            if (e.getCause() instanceof FailedLoginException) {
                throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider  " + TypeProviderEnum.PKCS11 + " car le pin est incorrect", e);
            } else {
                throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider" + TypeProviderEnum.PKCS11, e);
            }
        }

    }


    public DSSPrivateKeyEntry getPrivateKeyEntry(String alias) throws RecuperationCertificatException {
        MultiKeyStore keyStore = getKeyStore();

        String providerName = keyStore.getProviderName(alias).replace("SunPKCS11-", "");

        try {
            // parcourt de l'ensemble des occurances contenus dans le KeyStore et récupération du bit-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {

                String aliasKey = aliases.nextElement();
                if (aliasKey.equals(alias)) {
                    return this.getDSSPrivateKeyEntry(keyStore, alias, new KeyStore.PasswordProtection(password.get(providerName)));
                }
            }

        } catch (Exception e) {
            password.remove(providerName);
            throw new RecuperationCertificatException("Erreur lors de la récupération des alias se trouvant dans le key store du provider " + TypeProviderEnum.PKCS11, e);
        }
        return null;
    }

    private DSSPrivateKeyEntry getDSSPrivateKeyEntry(KeyStore keyStore, String alias, KeyStore.PasswordProtection passwordProtection) {
        try {
            if (keyStore.isKeyEntry(alias)) {
                KeyStore.Entry entry = keyStore.getEntry(alias, passwordProtection);
                if (entry instanceof KeyStore.PrivateKeyEntry) {
                    KeyStore.PrivateKeyEntry pke = (KeyStore.PrivateKeyEntry) entry;
                    return new KSPrivateKeyEntry(alias, pke);
                }

                log.warn("Skipped entry (unsupported class : {})", entry.getClass().getSimpleName());
            } else {
                log.debug("No related/supported key found for alias '{}'", alias);
            }

            return null;
        } catch (GeneralSecurityException var6) {
            throw new DSSException("Unable to retrieve key from keystore", var6);
        }
    }

    private KeyStore isProviderValid(AuthProvider provider) {
        try {
            return KeyStore.getInstance(TypeProviderEnum.PKCS11.getType(), provider);
        } catch (Exception ex) {
            log.warn(ex.getMessage());
            return null;
        }
    }

    private void removeOldProvider(ImmutablePair<String, AuthProvider> pair) {
        AuthProvider ancienProvider = (AuthProvider) Security.getProvider(pair.getRight().getName());
        if (ancienProvider != null) {
            log.info("Suppression du provider : " + ancienProvider.getName());
            try {
                ancienProvider.logout();
            } catch (LoginException e) {
                log.error(e.getMessage(), e);
                password.remove(pair.getLeft());
            }
            Security.removeProvider(ancienProvider.getName());
        }
    }

    private AuthProvider getAuthProvider(String name, File library) {
        AuthProvider provider;
        try {
            File file = getPkcs11TempConfigFile(name, library);
            provider = (AuthProvider) Security.getProvider("SunPKCS11");
            provider = (AuthProvider) provider.configure(file.getAbsolutePath());
            //java.nio.file.Files.deleteIfExists(file.toPath());
        } catch (ProviderException | IOException e) {
            password.remove(name);
            log.error("Provider error {}", e.getMessage());
            return null;
        }
        CallbackHandler callbackHandler = new PasswordCallbackHandler(name, dialogService);
        provider.setCallbackHandler(callbackHandler);
        return provider;
    }

    private File getPkcs11TempConfigFile(String name, File library) throws IOException {
        String tmpdir = System.getProperty(JAVA_IO_TMPDIR);

        String pkcs11ConfigSettings = "name=" + name + "\n" + "library=" + library + "\n" + "attributes=compatibility";

        File file = new File(tmpdir + "/pkcs11-" + name + ".cfg");

        Files.write(Path.of(file.getPath()), pkcs11ConfigSettings.getBytes());

        return file;
    }

    public DSSPrivateKeyEntry getSignaturePKCS11Params(String alias) {
        List<ImmutablePair<String, File>> flattenProvidersWithLibs =
                pkcs11LibsProperties.getPkcs11Providers().entrySet().stream()
                        .flatMap(e -> e.getValue().stream().map(v -> new ImmutablePair<>(e.getKey(), v)))
                        .collect(Collectors.toList());

        List<ImmutablePair<char[], Pkcs11SignatureToken>> passwordTokens = flattenProvidersWithLibs.stream()
                .filter(p -> p.getRight().exists() && password.get(p.getLeft()) != null)
                .map(p -> new ImmutablePair<>(password.get(p.getLeft()), new Pkcs11SignatureToken(p.getRight().getAbsolutePath(),
                        new KeyStore.PasswordProtection(password.get(p.getLeft())), -1, "attributes=compatibility")))
                .collect(Collectors.toList());


        if (alias != null && !CollectionUtils.isEmpty(passwordTokens)) {
            return passwordTokens.stream().map(passwordToken -> {
                List<DSSPrivateKeyEntry> keys = passwordToken.getRight().getKeys();

                String aliasToken = ((KSPrivateKeyEntry) keys.get(0)).getAlias();
                if (aliasToken == null || !alias.contains(aliasToken))
                    return null;

                return passwordToken.getRight().getKey(aliasToken, new KeyStore.PasswordProtection(passwordToken.getLeft()));
            }).filter(Objects::nonNull).findFirst().orElse(null);

        }
        return null;
    }


    public static class PasswordCallbackHandler implements CallbackHandler {
        private final String name;
        private final DialogService dialogService;

        public PasswordCallbackHandler(String name, DialogService dialogService) {
            this.name = name;
            this.dialogService = dialogService;
        }

        public void handle(Callback[] callbacks)
                throws IOException, UnsupportedCallbackException {
            if (!(callbacks[0] instanceof PasswordCallback)) {
                throw new UnsupportedCallbackException(callbacks[0]);
            }
            if (password.get(name) == null) {
                DialogResponseDTO dialogResponse = dialogService.executeDialogRequest(DialogEnum.PKCSS11_PASSWORD, name);
                if (!dialogResponse.isDenied() && !dialogResponse.isDismissed()) {
                    password.put(name, dialogResponse.getValue().toCharArray());
                }
            }
            PasswordCallback pc = (PasswordCallback) callbacks[0];
            pc.setPassword(password.get(name));
        }
    }
}
