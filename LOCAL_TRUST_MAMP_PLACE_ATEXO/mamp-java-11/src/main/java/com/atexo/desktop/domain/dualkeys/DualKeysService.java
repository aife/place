package com.atexo.desktop.domain.dualkeys;

import com.atexo.desktop.config.RestTemplateConfig;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dualkeys.exceptions.DualKeyGenerationException;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import javax.net.ssl.HttpsURLConnection;
import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DualKeysService {

    private final static String SEPERATOR = ",";
    private final static List<String> CERTIFICATE_ISSUER_EXTENSIONS = Arrays.asList("L", "C");
    private final static List<String> CERTIFICATE_SUBJECT_EXTENSIONS = Arrays.asList("O", "OU", "L", "C");
    private final RestTemplateConfig restTemplateConfig;
    private final KeyStoreService keyStoreService;

    public DualKeysService(RestTemplateConfig restTemplateConfig, KeyStoreService keyStoreService) {
        this.restTemplateConfig = restTemplateConfig;
        this.keyStoreService = keyStoreService;
    }


    public Certificate generatex509Cert(DualKeyFormDTO dualKeyFormDTO) throws DualKeyGenerationException {
        try {
            final BouncyCastleProvider provider = new BouncyCastleProvider();
            Security.addProvider(provider);
            X509Certificate serverCertificate = getServerCertificate(dualKeyFormDTO.getMpeUrl());
            X500Principal principalDN = new X500Principal(buildSubject(serverCertificate, dualKeyFormDTO.getCnCleChiffrement()));
            X500Principal issuerDN = new X500Principal(buildIssuer(serverCertificate));
            KeyPair keyPair = generateKeyPair(TypeProviderEnum.BC);
            ContentSigner signer = new JcaContentSignerBuilder(TypeProviderEnum.BC.getSignatureAlgorithm()).setProvider(TypeProviderEnum.BC.getNom())
                    .build(keyPair.getPrivate());
            X509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(
                    issuerDN, BigInteger.valueOf(System.currentTimeMillis()), generateBeginningDate(), generateEndingDate(), principalDN, keyPair.getPublic());


            final X509CertificateHolder holder = builder.build(signer);

            X509Certificate cert = new JcaX509CertificateConverter().setProvider(TypeProviderEnum.BC.getNom()).getCertificate(holder);

            X509Certificate signedCertificate = this.generateFromCsr(cert, dualKeyFormDTO);

            writingPrivateKeyInStore(dualKeyFormDTO.getCnCleChiffrement(), signedCertificate, keyPair.getPrivate(), dualKeyFormDTO.getPassword());

            return signedCertificate;
        } catch (Exception ex) {
            log.error("Erreur lors de la génération du certificat : {}", ex.getMessage());
            throw new DualKeyGenerationException(ex.getMessage(), ex);
        }
    }

    public Certificate exportFromExistingCert(DualKeyFormDTO dualKeyFormDTO) throws DualKeyGenerationException {
        try {
            KeyStore keyStore = keyStoreService.getKeyStore();
            return keyStore.getCertificate(dualKeyFormDTO.getCertificateId());
        } catch (Exception ex) {
            log.error("Erreur lors de la récupération du certificat : {}", ex.getMessage());
            throw new DualKeyGenerationException();
        }
    }

    public boolean sendDataToCallbackURL(String url, String sessionId, DualKeyMpePayloadDTO payload) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url + "/cryptographie/dual-key/send-mpe")
                .queryParam("sessionId", sessionId);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        HttpEntity<DualKeyMpePayloadDTO> request = new HttpEntity<>(payload, headers);

        String callbackUrl = builder.build().toUriString();
        try {
            ResponseEntity<Boolean> response = restTemplateConfig.getRestTemplate(callbackUrl).postForEntity(callbackUrl, request, Boolean.class);
            return Boolean.TRUE.equals(response.getBody());
        } catch (Exception ex) {
            log.error("Erreur lors du envoi du certificat vers MPE : {}", ex.getMessage());
            return false;
        }
    }

    private X509Certificate generateFromCsr(X509Certificate aPublic, DualKeyFormDTO dualKeyFormDTO) throws DualKeyGenerationException {
        try {

            String certificateString = Base64.getMimeEncoder().encodeToString(aPublic.getEncoded());
            String mpeUrl = dualKeyFormDTO.getMpeUrl();
            DualKeyCsrDTO payload = DualKeyCsrDTO.builder()
                    .mpeUrl(mpeUrl)
                    .certificateString(certificateString)
                    .build();
            log.info("generation de {} | {}", mpeUrl, certificateString);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(dualKeyFormDTO.getCaUrl() + "/cryptographie/dual-key/sign")
                    .queryParam("sessionId", dualKeyFormDTO.getSessionId());

            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.add("Content-Type", "application/json");
            HttpEntity<DualKeyCsrDTO> request = new HttpEntity<>(payload, headers);

            String caUrl = builder.build().toUriString();
            String response = restTemplateConfig.getRestTemplate(caUrl).postForObject(caUrl, request, String.class);
            byte[] bytes = Base64.getMimeDecoder().decode(response);
            InputStream in = new ByteArrayInputStream(bytes);
            CertificateFactory certFactory;
            certFactory = CertificateFactory.getInstance("X.509");
            return (X509Certificate) certFactory.generateCertificate(in);

        } catch (Exception ex) {
            throw new DualKeyGenerationException(ex.getMessage(), ex);
        }

    }


    private KeyPair generateKeyPair(TypeProviderEnum provider) throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(provider.getKeyAlgorithm(), provider.getNom());
        keyPairGenerator.initialize(2048);

        return keyPairGenerator.generateKeyPair();
    }

    private void writingPrivateKeyInStore(String cn, X509Certificate cert, PrivateKey privateKey, String password) throws KeyStoreException, IOException, RecuperationCertificatException {
        KeyStore keyStore = keyStoreService.getKeyStore();
        keyStore.setKeyEntry(cn, privateKey, password != null ? password.toCharArray() : "".toCharArray(), new Certificate[]{cert});
        keyStoreService.store(keyStore);
    }

    private String buildIssuer(X509Certificate x509Certificate) {
        List<String> params = new ArrayList<>();
        params.add("CN=MPE-MAMP");
        params.add("OU=MAMP");
        params.add("O=ATEXO");
        if (x509Certificate != null) {
            params.addAll(Arrays.stream(x509Certificate.getSubjectX500Principal().getName().split(SEPERATOR))
                    .filter(s -> {
                        String extension = s.split("=")[0];
                        return CERTIFICATE_ISSUER_EXTENSIONS.contains(extension);
                    })
                    .collect(Collectors.toList()));
        }
        return String.join(SEPERATOR, params);
    }

    private String buildSubject(X509Certificate x509Certificate, String cn) {
        List<String> params = new ArrayList<>();
        params.add("CN=" + cn);
        if (x509Certificate != null) {
            params.addAll(Arrays.stream(x509Certificate.getSubjectX500Principal().getName().split(SEPERATOR))
                    .filter(s -> {
                        String extension = s.split("=")[0];
                        return CERTIFICATE_SUBJECT_EXTENSIONS.contains(extension);
                    })
                    .collect(Collectors.toList()));
        }
        return String.join(SEPERATOR, params);
    }

    private Date generateBeginningDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    private Date generateEndingDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    private X509Certificate getServerCertificate(String mpeUrl) {
        try {

            if (mpeUrl.startsWith("https://")) {
                URL destinationURL = new URL(mpeUrl);
                HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection(restTemplateConfig.getProxy(mpeUrl));
                conn.setHostnameVerifier(NoopHostnameVerifier.INSTANCE);
                conn.setSSLSocketFactory(restTemplateConfig.createSSLContext().getSocketFactory());
                conn.connect();
                java.security.cert.Certificate[] certs = conn.getServerCertificates();
                if (certs.length > 0 && certs[0] instanceof X509Certificate) {
                    try {
                        final X509Certificate cert = (X509Certificate) certs[0];
                        cert.checkValidity();
                        return cert;

                    } catch (CertificateExpiredException cee) {
                        log.error("Certificate is expired");
                    }
                } else {
                    log.error("Unknown certificate type: {}", certs[0]);
                }
            }

        } catch (Exception e) {
            log.error("Erreur lors de la récupération du certificat du serveur {} => {}", mpeUrl, e.getMessage());
        }
        return null;
    }
}
