package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;


import com.atexo.desktop.domain.dechiffrement.dtos.KeyPair;

/**
 *
 */
public class InfosDechiffrement {

    private KeyPair keyPair;

    private ClefIV clefIV;

    public InfosDechiffrement(KeyPair keyPair, ClefIV clefIV) {
        this.keyPair = keyPair;
        this.clefIV = clefIV;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public ClefIV getClefIV() {
        return clefIV;
    }

}
