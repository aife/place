package com.atexo.desktop.domain.dualkeys.exceptions;

public class DuplicateAliasException  extends Exception {
    public DuplicateAliasException() {
        super("An alias with the same name already exists in your system");
    }
}
