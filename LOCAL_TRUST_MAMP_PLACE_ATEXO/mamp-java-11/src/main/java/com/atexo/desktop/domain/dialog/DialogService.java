package com.atexo.desktop.domain.dialog;

import com.atexo.desktop.config.FrontEndProperties;
import com.atexo.desktop.domain.dialog.dtos.DialogResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;

/**
 * Service executing dialog (prompt) on electron side.
 */
@Service
public class DialogService {
    private final FrontEndProperties frontEndProperties;

    public DialogService(FrontEndProperties frontEndProperties) {
        this.frontEndProperties = frontEndProperties;
    }

    public DialogResponseDTO executeDialogRequest(DialogEnum dialogEnum, String title) {
        final MultiValueMap<String, String> params = dialogEnum.getDialogConfigDTO(title).toParams();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(frontEndProperties.getUrl() + "/dialog")
                .queryParams(params);

        final URI uri = builder.buildAndExpand(params).toUri();
        ResponseEntity<DialogResponseDTO> response = new RestTemplate().getForEntity(uri, DialogResponseDTO.class);
        return response.getBody();
    }

    public String getProxy(String url) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.put("url", Collections.singletonList(url));
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(frontEndProperties.getUrl() + "/proxy")
                .queryParams(params);

        final URI uri = builder.buildAndExpand(params).toUri();
        ResponseEntity<String> response = new RestTemplate().getForEntity(uri, String.class);
        return response.getBody();
    }
}
