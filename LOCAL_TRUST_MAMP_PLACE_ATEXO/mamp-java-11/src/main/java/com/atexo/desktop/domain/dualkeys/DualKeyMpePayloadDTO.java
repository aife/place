package com.atexo.desktop.domain.dualkeys;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DualKeyMpePayloadDTO {
    private String nomCleChiffrement;
    private String cnCleChiffrement;
    private boolean estCleChiffrementSecours;
    private String certificat;
}
