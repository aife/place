package com.atexo.desktop.domain.certificat.exceptions;

/**
 * Exception pour signaler un souci lors de la manipulation des certificats.
 */
public class ManipulationCertificatException extends Exception {

    public ManipulationCertificatException(String message) {
        super(message);
    }

    public ManipulationCertificatException(String message, Throwable cause) {
        super(message, cause);
    }
}
