package com.atexo.desktop.domain.dualkeys;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class DualKeyCsrDTO {
	private String mpeUrl;
	private String certificateString;
}
