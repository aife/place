package com.atexo.desktop.domain.dechiffrement;

import com.atexo.desktop.config.RestTemplateConfig;
import com.atexo.desktop.domain.certificat.exceptions.ErreurTechniqueException;
import com.atexo.desktop.domain.dechiffrement.dtos.OffreProgressResponse;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.Offres;
import com.atexo.desktop.domain.dechiffrement.exceptions.DechiffrementExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.CredentialExpiredException;
import java.util.*;


@RequestMapping("/dechiffrement")
@RestController
@Slf4j
public class ApplicationDechiffrementController {
    private static final String URL_DECHIFFREMENT_CLES_SYMPETRIQUE = "/dechiffrementClesSymetriques";
    private static final String URL_DECHIFFREMENT_STATUS = "/dechiffrement-status";
    private final RestTemplateConfig restTemplateConfig;

    private final AESCryptographieService cryptographieService;

    public ApplicationDechiffrementController(RestTemplateConfig restTemplateConfig, AESCryptographieService cryptographieService) {
        this.restTemplateConfig = restTemplateConfig;
        this.cryptographieService = cryptographieService;
    }

    @GetMapping("offres/{sessionId}")
    public Offres getOffresADechiffrer(@RequestParam String urlServeurUpload, @PathVariable String sessionId) throws ErreurTechniqueException, CredentialExpiredException, DechiffrementExecutionException {
        MDC.put("pf", "OUVERTURE_EN_LIGNE");
        MDC.put("org", sessionId);
        Offres offres = getOffres(urlServeurUpload, sessionId);
        if (offres == null || offres.getOffre() == null || offres.getOffre().isEmpty()) {
            log.error("Pas d'offre a dechiffrer");
            throw new DechiffrementExecutionException("Aucune offre à déchiffrer");
        }
        return offres;
    }

    @PostMapping("offres/cle-privee/{sessionId}")
    public List<Integer> dechiffrementCleSecrete(@RequestBody Offres offres, @RequestParam String urlServeurUpload, @PathVariable String sessionId) throws ErreurTechniqueException, DechiffrementExecutionException {
        MDC.put("pf", "OUVERTURE_EN_LIGNE");
        MDC.put("org", sessionId);
        if (offres == null || offres.getOffre() == null || offres.getOffre().isEmpty()) {
            log.error("Pas d'offre a dechiffrer");
            throw new DechiffrementExecutionException("Aucune offre à déchiffrer");
        }

        Set<String> dechiffrementImpossibleEnv = new HashSet<>(cryptographieService.processOffers(offres));

        if (!dechiffrementImpossibleEnv.isEmpty()) {
            throw new DechiffrementExecutionException(String.join(",", dechiffrementImpossibleEnv));
        }
        final String url = urlServeurUpload + URL_DECHIFFREMENT_CLES_SYMPETRIQUE + "/" + sessionId;
        log.info("URL renvoie des cles dechiffrees : {}", url);
        return Arrays.asList(Objects.requireNonNull(restTemplateConfig.getRestTemplate(url).postForObject(url, offres, Integer[].class)));
    }

    @PostMapping("enveloppes/progress")
    public OffreProgressResponse envoyerEnveloppesInformations(@RequestParam String urlServeurUpload, @RequestBody List<String> idOffreQueue) throws ErreurTechniqueException {
        MDC.put("pf", "OUVERTURE_EN_LIGNE");
        log.info("URL renvoie des cles dechiffrees : {}", urlServeurUpload + URL_DECHIFFREMENT_CLES_SYMPETRIQUE);
        final String url = urlServeurUpload + URL_DECHIFFREMENT_STATUS;
        return restTemplateConfig.getRestTemplate(url).postForObject(url, idOffreQueue, OffreProgressResponse.class);
    }

    private Offres getOffres(String urlServeurUpload, String sessionId) throws CredentialExpiredException, ErreurTechniqueException {
        String url = urlServeurUpload + URL_DECHIFFREMENT_CLES_SYMPETRIQUE + "/" + sessionId;
        ResponseEntity<Offres> result = restTemplateConfig.getRestTemplate(url).getForEntity(url, Offres.class);
        if (result.getStatusCodeValue() == 403) {
            log.error("Jeton session n'existe pas : {}", sessionId);
            throw new CredentialExpiredException();
        } else if (result.getStatusCodeValue() != 200) {
            throw new ErreurTechniqueException();
        }
        return result.getBody();
    }
}
