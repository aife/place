package com.atexo.desktop.utilitaire;

import eu.europa.esig.dss.spi.x509.revocation.crl.OfflineCRLSource;

import java.io.InputStream;

/**
 * This class if a basic skeleton that is able to retrieve needed CRL data from
 * the contained list. The child need to retrieve the list of wrapped CRLs.
 */
public class AtexoCrlSource extends OfflineCRLSource {

    private static final long serialVersionUID = -985602836642741439L;

    public void addCRLToken(final InputStream inputStream) {
            addCRLToken(inputStream);
    }
}
