package com.atexo.desktop.utilitaire;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * Classe d'utilitaire d'extraction / manipulation des informations d'un
 * certificat.
 */
@SuppressWarnings("restriction")
public abstract class CertificatUtil {
    protected static final String[] KEY_USAGE_FLAG_NAMES = new String[]{"digitalSignature", "nonRepudiation", "keyEncipherment", "dataEncipherment", "keyAgreement", "keyCertSign", "cRLSign", "encipherOnly", "decipherOnly"};

    public enum TypeCertificat {
        SignatureElectronique("SIGNATURE_NUMERIQUE"), Chiffrement("CHIFFREMENT"), Authentification("AUTHENTIFICATION");

        private TypeCertificat(String labelKey) {
            this.labelKey = labelKey;
        }

        private String labelKey;

        public String getLabelKey() {
            return labelKey;
        }
    }


    /**
     * Récupére l'attribut CN d'un certificat
     *
     * @param principal le certificat
     * @return la valeur du CN
     */
    public static String getCN(Principal principal) {

        if (principal != null) {
            String[] principalDecompose = principal.toString().split(",");

            for (String valeur : principalDecompose) {
                String valeurTrim = valeur.trim();
                if (valeurTrim.startsWith("CN=")) {
                    return valeurTrim.substring(3);
                }
            }
        }

        return null;
    }

    public static boolean isValideDate(X509Certificate x509Certificate) {
        return x509Certificate.getNotAfter().after(new Date());
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de l'authentification.
     * on se base sur les flags digitalSignature ou nonRepudiation
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de
     * l'authentification, sinon false.
     */
    public static boolean isUtilisablePourAuthentification(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[0] || x509Certificate.getKeyUsage()[1]);
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de la signature
     * électronique.
     * <p>
     * on se base sur les flags digitalSignature ou nonRepudiation
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de la signature
     * électronique, sinon false.
     */
    public static boolean isUtilisablePourSignatureElectronique(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[0] || x509Certificate.getKeyUsage()[1]);
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de le chiffrement on
     * se base sur les flags keyEncipherment ou dataEncipherment
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de le chiffrement,
     * sinon false.
     */
    public static boolean isUtilisablePourChiffrement(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[2] || x509Certificate.getKeyUsage()[3]);

    }

    public static boolean isConformite(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && x509Certificate.getKeyUsage()[1] && !x509Certificate.getKeyUsage()[2]
                && !x509Certificate.getKeyUsage()[3] && !x509Certificate.getKeyUsage()[4] && !x509Certificate.getKeyUsage()[5]
                && !x509Certificate.getKeyUsage()[6] && !x509Certificate.getKeyUsage()[7] && !x509Certificate.getKeyUsage()[8];

    }

    /**
     * @param x509Certificate
     * @return
     */
    public static String getUtilisablePour(X509Certificate x509Certificate, boolean traduction) {
        // on détermine à quel usage peut être utilisé le certificat
        StringBuilder utilisationPossible = new StringBuilder();
        if (x509Certificate.getKeyUsage() != null) {
            boolean[] keyUsage = x509Certificate.getKeyUsage();
            for (int i = 0; i < keyUsage.length; i++) {
                if (keyUsage[i]) {
                    if (utilisationPossible.length() != 0) {
                        utilisationPossible.append(", ");
                    }
                    if (traduction) {
                        utilisationPossible.append(I18nUtil.get("keyUsage_" + KEY_USAGE_FLAG_NAMES[i]));
                    } else {
                        utilisationPossible.append(KEY_USAGE_FLAG_NAMES[i]);
                    }
                }
            }
        }
        return utilisationPossible.toString();
    }

}

