package com.atexo.desktop.domain.system;


import com.atexo.desktop.config.RestTemplateConfig;
import com.atexo.desktop.domain.certificat.enums.TypeOsEnum;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import com.atexo.desktop.utilitaire.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.slf4j.MDC;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


@RequestMapping("configuration")
@RestController
@Slf4j
public class SystemConfigurationController {

    private final RestTemplateConfig restTemplateConfig;
    private final KeyStoreService keyStoreService;

    public SystemConfigurationController(RestTemplateConfig restTemplateConfig, KeyStoreService keyStoreService) {
        this.restTemplateConfig = restTemplateConfig;
        this.keyStoreService = keyStoreService;
    }

    @GetMapping("system")
    public ConfigurationResponse getSystemConfiguration() throws IOException {
        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "system");
        TypeOsEnum typeOs = Util.determinerOs();
        String osName = System.getProperty("os.name");
        String osVersion = System.getProperty("os.version");
        if (typeOs.equals(TypeOsEnum.Linux)) {
            osName = (new BufferedReader(new InputStreamReader((new ProcessBuilder("lsb_release", "-rsa")).start().getInputStream()))).readLine();
            osVersion = (new BufferedReader(new InputStreamReader((new ProcessBuilder("lsb_release", "-rs")).start().getInputStream()))).readLine();
            osName += " " + osVersion;
        }
        log.info("System {} avec la version {}", osName, osVersion);


        DefaultArtifactVersion minVersion = new DefaultArtifactVersion(typeOs.getVersion());
        log.info("System minimun est  {} ", minVersion.toString());
        DefaultArtifactVersion actualVersion = new DefaultArtifactVersion(osVersion);
        log.info("System actuel est  {} ", actualVersion.toString());
        return ConfigurationResponse.builder()
                .label(osName)
                .isValid(actualVersion.compareTo(minVersion) >= 0)
                .build();
    }

    @GetMapping("application")
    public ConfigurationResponse getApplicationConfiguration() {
        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "application");
        return ConfigurationResponse.builder()
                .isValid(true)
                .build();
    }

    @GetMapping("sequestre")
    public ConfigurationResponse getSequestreConfiguration() {
        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "sequestre");
        return ConfigurationResponse.builder()
                .isValid(keyStoreService.isStoreAccessible())
                .build();
    }


    @GetMapping("web")
    public ConfigurationResponse getWebConfiguration(@RequestParam String browserName, @RequestParam String browserVersion) {

        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "web");
        TypeNavigatorEnum typeNavigator = TypeNavigatorEnum.get(browserName);

        DefaultArtifactVersion actualVersion = new DefaultArtifactVersion(browserVersion);
        DefaultArtifactVersion minVersion = new DefaultArtifactVersion(typeNavigator.getVersion());
        return ConfigurationResponse.builder()
                .label(typeNavigator.name() + " " + browserVersion)
                .isValid(actualVersion.compareTo(minVersion) >= 0)
                .build();
    }

    @GetMapping("server")
    public ConfigurationResponse getServerConfiguration(@RequestParam String serverUrl) {

        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "server");
        final String url = serverUrl + "/signature/urlvie";
        ResponseEntity<String> result = restTemplateConfig.getRestTemplate(url).getForEntity(url, String.class);
        return ConfigurationResponse.builder()
                .isValid(result.getStatusCodeValue() == 200)
                .build();
    }


    @GetMapping("version")
    public ResponseEntity<String> getApplicationVersion(@RequestParam String ressourcesUrl) {
        MDC.put("pf", "APPLICATION_TEST_CONFIGURATION");
        MDC.put("org", "version");
        return restTemplateConfig.getRestTemplate(ressourcesUrl).getForEntity(ressourcesUrl, String.class);

    }
}
