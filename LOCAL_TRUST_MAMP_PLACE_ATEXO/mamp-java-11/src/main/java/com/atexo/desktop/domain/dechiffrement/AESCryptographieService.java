package com.atexo.desktop.domain.dechiffrement;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dechiffrement.dtos.KeyPair;
import com.atexo.desktop.domain.dechiffrement.dtos.jnlp.*;
import com.atexo.desktop.domain.dechiffrement.exceptions.DechiffrementExecutionException;
import com.atexo.desktop.domain.keystore.KeyStoreService;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.ReponseAnnonceType;
import com.atexo.desktop.utilitaire.Util;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;


@Service
@Slf4j
public class AESCryptographieService {

	private KeyStoreService keyStoreService;

	public AESCryptographieService(KeyStoreService keyStoreService) {
		this.keyStoreService = keyStoreService;
	}

	// dechiffrement clef symetrique
	public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre dechiffrementCleSymetrique(ClefIV clefIv, PrivateKey privateKey) throws DechiffrementExecutionException {
		try {
			String transformation = "RSA/ECB/PKCS1Padding";
			Cipher decipher = Cipher.getInstance(transformation);
			decipher.init(Cipher.DECRYPT_MODE, privateKey);
			ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keysIv = new ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre();
			keysIv.setKey(new String(Base64.encode(decipher.doFinal(clefIv.getClef()))));
			keysIv.setIv(new String(Base64.encode(decipher.doFinal(clefIv.getIv()))));
			return keysIv;
		} catch (Exception e) {
			throw new DechiffrementExecutionException(e.getMessage(), e);
		}
	}

	// dechiffrement clef asymetrique clef => password, iv => privateKey
	public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre dechiffrementCleAsymetrique(ClefIV clefIv, PrivateKey privateKey) throws DechiffrementExecutionException {
		try {
			String transformation = "RSA/ECB/PKCS1Padding";
			Cipher decipher = Cipher.getInstance(transformation);
			decipher.init(Cipher.DECRYPT_MODE, privateKey);
			ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keysIv = new ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre();
			byte[] passwordChar = decipher.doFinal(clefIv.getClef());
			keysIv.setKey(new String(Base64.encode(passwordChar)));
			keysIv.setIv(new String(Base64.encode(clefIv.getIv())));
			return keysIv;
		} catch (Exception e) {
			throw new DechiffrementExecutionException(e.getMessage(), e);
		}
	}


	public InfosDechiffrement getInfosDechiffrementDepuisMagasin(TypeProviderEnum typeProvider, List<ClefIV> clefIVs) throws RecuperationCertificatException {
		for (ClefIV clefIV : clefIVs) {
			X509Certificate certificat = clefIV.getCertificat();
			if (certificat != null) {
				BigInteger serial = certificat.getSerialNumber();
				String signataire = certificat.getSubjectDN().toString();
				String emetteur = certificat.getIssuerDN().toString();

				log.info("Recherche dans le magasin de certificats du signataire " + signataire + " à partir de son emetteur  " + emetteur + " et son serial " + serial, this.getClass());
				KeyPair keyPair = keyStoreService.getKeyPair(typeProvider, serial, emetteur);
				log.info("Le certificat a été trouvé dans le magasin et utilisé pour effectuer le déchiffrement");

				if (keyPair != null && keyPair.getPrivateKey() != null) {
					return new InfosDechiffrement(keyPair, clefIV);
				}
			}
		}
		log.warn("Le certificat (couple clé public/clé privée) n'a pas été trouvé dans le magasin et utilisé");
		throw new RecuperationCertificatException("Le certificat (couple clé public/clé privée) n'a pas été trouvé dans le magasin et utilisé");
	}

	public Set<String> processOffers(Offres offres) {
		Set<String> result = new HashSet<>();
		for (OffreCertificat offre : offres.getOffre()) {
			for (EnveloppeJNLP enveloppe : offre.getEnveloppe()) {
				processEnvelope(offre, enveloppe).ifPresent(result::add);
			}
		}
		return result;
	}

	/**
	 * Note : Care about side effect treatment {@link #replaceKeys(EnveloppeJNLP, ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre)}
	 */
	private Optional<String> processEnvelope(OffreCertificat offre, EnveloppeJNLP enveloppe) {
		TypeProviderEnum typeProvider = Util.determinerProvider();

		try {
			if (enveloppe.getCertificat() == null) {
				log.info("certificat absent pour l'enveloppe {}", enveloppe.getTypeEnveloppe());
				return Optional.empty();
			}
			List<ClefIV> clefIVs = getClefIVsFromCertificate(enveloppe.getCertificat());
			InfosDechiffrement infos = getInfosDechiffrementDepuisMagasin(typeProvider, clefIVs);
			if (infos.getKeyPair() != null) {
				ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keyIvDechiffre;

				if (offre.getProtocol().equalsIgnoreCase("CMS")) {
					keyIvDechiffre = dechiffrementCleAsymetrique(infos.getClefIV(), infos.getKeyPair().getPrivateKey());
				} else {
					keyIvDechiffre = dechiffrementCleSymetrique(infos.getClefIV(), infos.getKeyPair().getPrivateKey());
				}
				replaceKeys(enveloppe, keyIvDechiffre);

			} else {
				log.error("Clé privée non présente");
				return Optional.ofNullable(enveloppe.getTypeEnveloppe());
			}
			for (FichierJNLP fichierJNLP : enveloppe.getFichiersBlocs()) {

				List<KeysIvJNLP> certificatBlocs = fichierJNLP.getCertificat();

				if (certificatBlocs == null) {
					log.info("certificat absent pour l'enveloppe {}", enveloppe.getTypeEnveloppe());
					continue;
				}
				List<ClefIV> clefIVsfichierJNLP = getClefIVsFromCertificate(fichierJNLP.getCertificat());
				InfosDechiffrement infosfichierJNLP = getInfosDechiffrementDepuisMagasin(typeProvider, clefIVsfichierJNLP);
				if (infosfichierJNLP.getKeyPair() != null) {
					ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keyIvDechiffre;

					if (offre.getProtocol().equalsIgnoreCase("CMS")) {
						keyIvDechiffre = dechiffrementCleAsymetrique(infosfichierJNLP.getClefIV(), infosfichierJNLP.getKeyPair().getPrivateKey());
					} else {
						keyIvDechiffre = dechiffrementCleSymetrique(infosfichierJNLP.getClefIV(), infosfichierJNLP.getKeyPair().getPrivateKey());
					}
					replaceKeys(fichierJNLP, keyIvDechiffre);

				}

			}
		} catch (DechiffrementExecutionException | CertificateException | RecuperationCertificatException e) {
			log.error("Erreur au cours du dechiffrement des cles secrete", e.fillInStackTrace());
			enveloppe.getCertificat().clear();
			return Optional.ofNullable(enveloppe.getTypeEnveloppe());
		}

		return Optional.empty();
	}


	private void replaceKeys(EnveloppeJNLP enveloppe, ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keyIvDechiffre) {
		enveloppe.getCertificat().clear();
		KeysIvJNLP clefIV = new KeysIvJNLP();
		clefIV.setKey(keyIvDechiffre.getKey());
		clefIV.setIv(keyIvDechiffre.getIv());
		enveloppe.getCertificat().add(clefIV);
	}

	private void replaceKeys(FichierJNLP enveloppe, ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keyIvDechiffre) {
		enveloppe.getCertificat().clear();
		KeysIvJNLP clefIV = new KeysIvJNLP();
		clefIV.setKey(keyIvDechiffre.getKey());
		clefIV.setIv(keyIvDechiffre.getIv());
		enveloppe.getCertificat().add(clefIV);
	}

	private List<ClefIV> getClefIVsFromCertificate(List<KeysIvJNLP> keysIvJNLPS) throws CertificateException {
		List<ClefIV> clefIVs = new ArrayList<>();
		for (KeysIvJNLP keysIv : keysIvJNLPS) {
			X509Certificate certificat;
			ClefIV keyIv;
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
			InputStream in = new ByteArrayInputStream(java.util.Base64.getDecoder().decode(keysIv.getCertificate()));
			certificat = (X509Certificate) certFactory.generateCertificate(in);
			keyIv = new ClefIV(java.util.Base64.getDecoder().decode(keysIv.getKey()), java.util.Base64.getDecoder().decode(keysIv.getIv()), certificat);
			clefIVs.add(keyIv);
		}
		return clefIVs;
	}
}
