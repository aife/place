package com.atexo.desktop.domain.tele_pli_par_pli.dtos;

import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.FichierBloc;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TelechargementPliParPliRequestProcess {
	private String initialDirectory;
	private String urlServeurUpload;
	private FichierBloc fichierBloc;
}
