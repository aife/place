package com.atexo.desktop.domain.dualkeys;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class GenerationResultDTO {
    private boolean generationExecuted = false;
    private boolean generationStatus = false;
    private boolean sendingMpeStatus = false;
}
