package com.atexo.desktop.domain.certificat.enums;

/**
 * Le type de provider possible pour la récupération du certificat client.
 */
public enum TypeProviderEnum {


    MSCAPI("SunMSCAPI", "Windows-MY", "sun.security.mscapi.KeyStore$MY", "SHA256withRSA", "RSA"),
    APPLE("Apple", "KeychainStore", null, "com.apple.crypto.provider.KeychainStore", "RSA"),
    PKCS11(null,"PKCS11",null, "", ""),
    PKCS12(null, "PKCS12", null, "SHA256withRSA", "RSA"),
    JKS("SUN", "JKS", null, "SHA256withDSA", "DSA"),
    BC("BC", "BC", null, "SHA256withRSA", "RSA");

    private final String nom;

    private final String type;

    private final String keyStoreVeritable;

    private final String signatureAlgorithm;

    private final String keyAlgorithm;

    TypeProviderEnum(String nom, String type, String keyStoreVeritable, String signatureAlgorithm, String keyAlgorithm) {
        this.nom = nom;
        this.type = type;
        this.keyStoreVeritable = keyStoreVeritable;
        this.signatureAlgorithm = signatureAlgorithm;
        this.keyAlgorithm = keyAlgorithm;
    }

    public String getKeyAlgorithm() {
        return keyAlgorithm;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    public String getKeyStoreVeritable() {
        return keyStoreVeritable;
    }

    public String getNom() {
        return nom;
    }

    public String getType() {
        return type;
    }

    public static TypeProviderEnum get(String nom) {

        for (TypeProviderEnum typeProvider : values()) {
            if (typeProvider.name().equals(nom)) {
                return typeProvider;
            }
        }

        return null;
    }
}
