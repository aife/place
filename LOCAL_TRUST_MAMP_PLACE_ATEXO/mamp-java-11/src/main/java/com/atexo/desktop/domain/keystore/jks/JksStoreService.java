package com.atexo.desktop.domain.keystore.jks;

import com.atexo.desktop.domain.dialog.DialogEnum;
import com.atexo.desktop.domain.dialog.DialogService;
import com.atexo.desktop.domain.dialog.dtos.DialogResponseDTO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Service responsible to store and provide JKS keyStore passwords.
 * If the password isn't known, a dialog is displayed on electron side (prompt) through the dialogService
 */
@Service
public class JksStoreService {

    private DialogService dialogService;
    private static Map<String, char[]> password = new HashMap<>();

    public JksStoreService(DialogService dialogService) {
        this.dialogService = dialogService;
    }

    public char[] getPassword(String alias) {
        char[] storedPassword = password.get(alias);
        if (storedPassword == null) {
            DialogResponseDTO dialogResponse = dialogService.executeDialogRequest(DialogEnum.JKS_PASSWORD, alias);
            if (!dialogResponse.isDenied() && !dialogResponse.isDismissed()) {
                password.put(alias, dialogResponse.getValue().toCharArray());
            }
        }
        return password.get(alias);
    }

    public void removePassword(String alias) {
        char[] storedPassword = password.get(alias);
        if (storedPassword != null) {
            password.remove(alias);
        }
    }
}
