package com.atexo.desktop.config.propertiesDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LinuxKeyStoreProps {
    private String path;
    private String password;
}