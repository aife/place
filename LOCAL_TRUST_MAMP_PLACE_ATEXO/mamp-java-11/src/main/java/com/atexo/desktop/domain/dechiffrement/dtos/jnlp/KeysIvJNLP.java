package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KeysIvJNLP implements Serializable {

    protected String key;
    protected String iv;
    protected String certificate;


    public String getKey() {
        return key;
    }

    public void setKey(String value) {
        this.key = value;
    }


    public String getIv() {
        return iv;
    }


    public void setIv(String value) {
        this.iv = value;
    }


    public String getCertificate() {
        return certificate;
    }


    public void setCertificate(String value) {
        this.certificate = value;
    }
}
