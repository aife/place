package com.atexo.desktop.domain.tele_pli_par_pli.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TelechargementPliParPliRequestFinish {
	private String initialDirectory;
}
