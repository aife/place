package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
@XmlType(name = "offre")
public class OffreCertificat implements Serializable {
    private int idOffre;
    private List<EnveloppeJNLP> enveloppe;
    private String protocol;

    public void setEnveloppe(List<EnveloppeJNLP> enveloppe) {
        this.enveloppe = enveloppe;
    }

    public List<EnveloppeJNLP> getEnveloppe() {
        return enveloppe;
    }

    public int getIdOffre() {
        return idOffre;
    }

    public void setIdOffre(int idOffre) {
        this.idOffre = idOffre;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
