package com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour ReponseAnnonceType complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ReponseAnnonceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperateurEconomique" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Annonce" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Intitule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Objet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TypesEnveloppe">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="typeProcedure" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="nombreLots" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="entiteAchat" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="referenceUtilisateur" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="chiffrement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="signature" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="allotissement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="separationSignatureAe" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="dateLimite" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Enveloppes">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Enveloppe" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Fichier" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BlocsChiffrement">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="BlocChiffrement" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="keysIvsChiffre">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="keysIv" maxOccurs="unbounded">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                                                                               &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                                                                               &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="keyIvDechiffre">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                                                                     &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute ref="{}statutChiffrement"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="idFichier" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="typeFichier" use="required">
 *                                       &lt;simpleType>
 *                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                           &lt;enumeration value="PRI"/>
 *                                           &lt;enumeration value="ACE"/>
 *                                           &lt;enumeration value="SEC"/>
 *                                         &lt;/restriction>
 *                                       &lt;/simpleType>
 *                                     &lt;/attribute>
 *                                     &lt;attribute name="origineFichier" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="hash" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                     &lt;attribute ref="{}statutChiffrement"/>
 *                                     &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="verificationCertificat" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="type" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                                 &lt;enumeration value="1"/>
 *                                 &lt;enumeration value="2"/>
 *                                 &lt;enumeration value="3"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="statutOuverture" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="OUV"/>
 *                                 &lt;enumeration value="FER"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute name="statutAdmissibilite" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="ATR"/>
 *                                 &lt;enumeration value="ADM"/>
 *                                 &lt;enumeration value="NAD"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute ref="{}statutChiffrement"/>
 *                           &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="idReponseAnnonce" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="indexReponse" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="horodatageDepot" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="typeSignature" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReponseAnnonceType", propOrder = {
    "operateurEconomique",
    "annonce",
    "enveloppes"
})
@XmlSeeAlso({
    ReponseAnnonce.class
})
public class ReponseAnnonceType implements Serializable {
    @XmlAttribute(name = "protocol")
    protected String protocol;
    @XmlElement(name = "OperateurEconomique")
    protected OperateurEconomique operateurEconomique;
    @XmlElement(name = "Annonce")
    protected Annonce annonce;
    @XmlElement(name = "Enveloppes", required = true)
    protected Enveloppes enveloppes;
    @XmlAttribute(name = "idReponseAnnonce", required = true)
    protected int idReponseAnnonce;
    @XmlAttribute(name = "idCandidature", required = false)
    protected int idCandidature;
    @XmlAttribute(name = "organisme")
    protected String organisme;
    @XmlAttribute(name = "indexReponse")
    protected Integer indexReponse;
    @XmlAttribute(name = "reference")
    protected String reference;
    @XmlAttribute(name = "horodatageDepot")
    protected String horodatageDepot;
    @XmlAttribute(name = "typeSignature")
    protected String typeSignature;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    /**
     * Obtient la valeur de la propri�t� operateurEconomique.
     *
     * @return
     *     possible object is
     *     {@link OperateurEconomique }
     *
     */
    public OperateurEconomique getOperateurEconomique() {
        return operateurEconomique;
    }

    /**
     * D�finit la valeur de la propri�t� operateurEconomique.
     *
     * @param value
     *     allowed object is
     *     {@link OperateurEconomique }
     *
     */
    public void setOperateurEconomique(OperateurEconomique value) {
        this.operateurEconomique = value;
    }

    /**
     * Obtient la valeur de la propri�t� annonce.
     *
     * @return
     *     possible object is
     *     {@link Annonce }
     *
     */
    public Annonce getAnnonce() {
        return annonce;
    }

    /**
     * D�finit la valeur de la propri�t� annonce.
     *
     * @param value
     *     allowed object is
     *     {@link Annonce }
     *
     */
    public void setAnnonce(Annonce value) {
        this.annonce = value;
    }

    /**
     * Obtient la valeur de la propri�t� enveloppes.
     *
     * @return
     *     possible object is
     *     {@link Enveloppes }
     *
     */
    public Enveloppes getEnveloppes() {
        return enveloppes;
    }

    /**
     * D�finit la valeur de la propri�t� enveloppes.
     *
     * @param value
     *     allowed object is
     *     {@link Enveloppes }
     *
     */
    public void setEnveloppes(Enveloppes value) {
        this.enveloppes = value;
    }

    /**
     * Obtient la valeur de la propriete idReponseAnnonce.
     *
     */
    public int getIdReponseAnnonce() {
        return idReponseAnnonce;
    }

    /**
     * Definit la valeur de la propriete idReponseAnnonce.
     *
     */
    public void setIdReponseAnnonce(int value) {
        this.idReponseAnnonce = value;
    }

    /**
     * Obtient la valeur de la propriete idReponseAnnonce.
     *
     */
    public int getIdCandidature() {
        return idCandidature;
    }

    /**
     * Definit la valeur de la propriete idReponseAnnonce.
     *
     */
    public void setIdCandidature(int value) {
        this.idCandidature = value;
    }

    /**
     * Obtient la valeur de la propri�t� organisme.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOrganisme() {
        return organisme;
    }

    /**
     * D�finit la valeur de la propri�t� organisme.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOrganisme(String value) {
        this.organisme = value;
    }

    /**
     * Obtient la valeur de la propri�t� indexReponse.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getIndexReponse() {
        return indexReponse;
    }

    /**
     * D�finit la valeur de la propri�t� indexReponse.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setIndexReponse(Integer value) {
        this.indexReponse = value;
    }

    /**
     * Obtient la valeur de la propri�t� reference.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getReference() {
        return reference;
    }

    /**
     * D�finit la valeur de la propri�t� reference.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setReference(String value) {
        this.reference = value;
    }

    /**
     * Obtient la valeur de la propri�t� horodatageDepot.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getHorodatageDepot() {
        return horodatageDepot;
    }

    /**
     * D�finit la valeur de la propri�t� horodatageDepot.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setHorodatageDepot(String value) {
        this.horodatageDepot = value;
    }

    /**
     * Obtient la valeur de la propri�t� typeSignature.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTypeSignature() {
        return typeSignature;
    }

    /**
     * D�finit la valeur de la propri�t� typeSignature.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTypeSignature(String value) {
        this.typeSignature = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Intitule" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Objet" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TypesEnveloppe">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="typeProcedure" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="nombreLots" type="{http://www.w3.org/2001/XMLSchema}int" />
     *       &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="entiteAchat" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="referenceUtilisateur" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="chiffrement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="signature" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="allotissement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="separationSignatureAe" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *       &lt;attribute name="dateLimite" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "intitule",
        "objet",
        "typesEnveloppe"
    })
    public static class Annonce implements Serializable {

        @XmlElement(name = "Intitule", required = true)
        protected String intitule;
        @XmlElement(name = "Objet", required = true)
        protected String objet;
        @XmlElement(name = "TypesEnveloppe", required = true)
        protected TypesEnveloppe typesEnveloppe;
        @XmlAttribute(name = "typeProcedure")
        protected String typeProcedure;
        @XmlAttribute(name = "nombreLots")
        protected Integer nombreLots;
        @XmlAttribute(name = "organisme")
        protected String organisme;
        @XmlAttribute(name = "entiteAchat")
        protected String entiteAchat;
        @XmlAttribute(name = "reference")
        protected String reference;
        @XmlAttribute(name = "referenceUtilisateur")
        protected String referenceUtilisateur;
        @XmlAttribute(name = "chiffrement")
        protected Boolean chiffrement;
        @XmlAttribute(name = "signature")
        protected Boolean signature;
        @XmlAttribute(name = "allotissement")
        protected Boolean allotissement;
        @XmlAttribute(name = "separationSignatureAe")
        protected Boolean separationSignatureAe;
        @XmlAttribute(name = "dateLimite")
        protected String dateLimite;

        /**
         * Obtient la valeur de la propri�t� intitule.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getIntitule() {
            return intitule;
        }

        /**
         * D�finit la valeur de la propri�t� intitule.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setIntitule(String value) {
            this.intitule = value;
        }

        /**
         * Obtient la valeur de la propri�t� objet.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getObjet() {
            return objet;
        }

        /**
         * D�finit la valeur de la propri�t� objet.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setObjet(String value) {
            this.objet = value;
        }

        /**
         * Obtient la valeur de la propri�t� typesEnveloppe.
         *
         * @return
         *     possible object is
         *     {@link TypesEnveloppe }
         *
         */
        public TypesEnveloppe getTypesEnveloppe() {
            return typesEnveloppe;
        }

        /**
         * D�finit la valeur de la propri�t� typesEnveloppe.
         *
         * @param value
         *     allowed object is
         *     {@link TypesEnveloppe }
         *
         */
        public void setTypesEnveloppe(TypesEnveloppe value) {
            this.typesEnveloppe = value;
        }

        /**
         * Obtient la valeur de la propri�t� typeProcedure.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTypeProcedure() {
            return typeProcedure;
        }

        /**
         * D�finit la valeur de la propri�t� typeProcedure.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTypeProcedure(String value) {
            this.typeProcedure = value;
        }

        /**
         * Obtient la valeur de la propri�t� nombreLots.
         *
         * @return
         *     possible object is
         *     {@link Integer }
         *
         */
        public Integer getNombreLots() {
            return nombreLots;
        }

        /**
         * D�finit la valeur de la propri�t� nombreLots.
         *
         * @param value
         *     allowed object is
         *     {@link Integer }
         *
         */
        public void setNombreLots(Integer value) {
            this.nombreLots = value;
        }

        /**
         * Obtient la valeur de la propri�t� organisme.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOrganisme() {
            return organisme;
        }

        /**
         * D�finit la valeur de la propri�t� organisme.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOrganisme(String value) {
            this.organisme = value;
        }

        /**
         * Obtient la valeur de la propri�t� entiteAchat.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getEntiteAchat() {
            return entiteAchat;
        }

        /**
         * D�finit la valeur de la propri�t� entiteAchat.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setEntiteAchat(String value) {
            this.entiteAchat = value;
        }

        /**
         * Obtient la valeur de la propri�t� reference.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getReference() {
            return reference;
        }

        /**
         * D�finit la valeur de la propri�t� reference.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setReference(String value) {
            this.reference = value;
        }

        /**
         * Obtient la valeur de la propri�t� referenceUtilisateur.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getReferenceUtilisateur() {
            return referenceUtilisateur;
        }

        /**
         * D�finit la valeur de la propri�t� referenceUtilisateur.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setReferenceUtilisateur(String value) {
            this.referenceUtilisateur = value;
        }

        /**
         * Obtient la valeur de la propri�t� chiffrement.
         *
         * @return
         *     possible object is
         *     {@link Boolean }
         *
         */
        public Boolean isChiffrement() {
            return chiffrement;
        }

        /**
         * D�finit la valeur de la propri�t� chiffrement.
         *
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *
         */
        public void setChiffrement(Boolean value) {
            this.chiffrement = value;
        }

        /**
         * Obtient la valeur de la propri�t� signature.
         *
         * @return
         *     possible object is
         *     {@link Boolean }
         *
         */
        public Boolean isSignature() {
            return signature;
        }

        /**
         * D�finit la valeur de la propri�t� signature.
         *
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *
         */
        public void setSignature(Boolean value) {
            this.signature = value;
        }

        /**
         * Obtient la valeur de la propri�t� allotissement.
         *
         * @return
         *     possible object is
         *     {@link Boolean }
         *
         */
        public Boolean isAllotissement() {
            return allotissement;
        }

        /**
         * D�finit la valeur de la propri�t� allotissement.
         *
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *
         */
        public void setAllotissement(Boolean value) {
            this.allotissement = value;
        }

        /**
         * Obtient la valeur de la propri�t� separationSignatureAe.
         *
         * @return
         *     possible object is
         *     {@link Boolean }
         *
         */
        public Boolean isSeparationSignatureAe() {
            return separationSignatureAe;
        }

        /**
         * D�finit la valeur de la propri�t� separationSignatureAe.
         *
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *
         */
        public void setSeparationSignatureAe(Boolean value) {
            this.separationSignatureAe = value;
        }

        /**
         * Obtient la valeur de la propri�t� dateLimite.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDateLimite() {
            return dateLimite;
        }

        /**
         * D�finit la valeur de la propri�t� dateLimite.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDateLimite(String value) {
            this.dateLimite = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "typeEnveloppe"
        })
        public static class TypesEnveloppe implements Serializable {

            @XmlElement(name = "TypeEnveloppe", required = true)
            protected List<TypeEnveloppe> typeEnveloppe;

            /**
             * Gets the value of the typeEnveloppe property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the typeEnveloppe property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTypeEnveloppe().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TypeEnveloppe }
             *
             *
             */
            public List<TypeEnveloppe> getTypeEnveloppe() {
                if (typeEnveloppe == null) {
                    typeEnveloppe = new ArrayList<>();
                }
                return this.typeEnveloppe;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "certificatChiffrement"
            })
            public static class TypeEnveloppe implements Serializable {

                @XmlElement(name = "CertificatChiffrement")
                protected String certificatChiffrement;
                @XmlAttribute(name = "type", required = true)
                protected int type;
                @XmlAttribute(name = "numLot", required = true)
                protected int numLot;

                /**
                 * Obtient la valeur de la propri�t� certificatChiffrement.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCertificatChiffrement() {
                    return certificatChiffrement;
                }

                /**
                 * D�finit la valeur de la propri�t� certificatChiffrement.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCertificatChiffrement(String value) {
                    this.certificatChiffrement = value;
                }

                /**
                 * Obtient la valeur de la propri�t� type.
                 *
                 */
                public int getType() {
                    return type;
                }

                /**
                 * D�finit la valeur de la propri�t� type.
                 *
                 */
                public void setType(int value) {
                    this.type = value;
                }

                /**
                 * Obtient la valeur de la propri�t� numLot.
                 *
                 */
                public int getNumLot() {
                    return numLot;
                }

                /**
                 * D�finit la valeur de la propri�t� numLot.
                 *
                 */
                public void setNumLot(int value) {
                    this.numLot = value;
                }

            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Enveloppe" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Fichier" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="BlocsChiffrement">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="BlocChiffrement" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="keysIvsChiffre">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="keysIv" maxOccurs="unbounded">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *                                                                     &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *                                                                     &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="keyIvDechiffre">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *                                                           &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                               &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                               &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                               &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute ref="{}statutChiffrement"/>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="idFichier" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="typeFichier" use="required">
     *                             &lt;simpleType>
     *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                 &lt;enumeration value="PRI"/>
     *                                 &lt;enumeration value="ACE"/>
     *                                 &lt;enumeration value="SEC"/>
     *                               &lt;/restriction>
     *                             &lt;/simpleType>
     *                           &lt;/attribute>
     *                           &lt;attribute name="origineFichier" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="hash" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                           &lt;attribute ref="{}statutChiffrement"/>
     *                           &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                           &lt;attribute name="verificationCertificat" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *                 &lt;attribute name="type" use="required">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *                       &lt;enumeration value="1"/>
     *                       &lt;enumeration value="2"/>
     *                       &lt;enumeration value="3"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *                 &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *                 &lt;attribute name="statutOuverture" use="required">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                       &lt;enumeration value="OUV"/>
     *                       &lt;enumeration value="FER"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *                 &lt;attribute name="statutAdmissibilite" use="required">
     *                   &lt;simpleType>
     *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                       &lt;enumeration value="ATR"/>
     *                       &lt;enumeration value="ADM"/>
     *                       &lt;enumeration value="NAD"/>
     *                     &lt;/restriction>
     *                   &lt;/simpleType>
     *                 &lt;/attribute>
     *                 &lt;attribute ref="{}statutChiffrement"/>
     *                 &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enveloppe"
    })
    public static class Enveloppes implements Serializable {

        @XmlElement(name = "Enveloppe", required = true)
        protected List<Enveloppe> enveloppe;

        /**
         * Gets the value of the enveloppe property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the enveloppe property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnveloppe().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Enveloppe }
         *
         *
         */
        public List<Enveloppe> getEnveloppe() {
            if (enveloppe == null) {
                enveloppe = new ArrayList<>();
            }
            return this.enveloppe;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Fichier" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="BlocsChiffrement">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="BlocChiffrement" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="keysIvsChiffre">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="keysIv" maxOccurs="unbounded">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
         *                                                           &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
         *                                                           &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="keyIvDechiffre">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
         *                                                 &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                                     &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute ref="{}statutChiffrement"/>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="idFichier" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="typeFichier" use="required">
         *                   &lt;simpleType>
         *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                       &lt;enumeration value="PRI"/>
         *                       &lt;enumeration value="ACE"/>
         *                       &lt;enumeration value="SEC"/>
         *                     &lt;/restriction>
         *                   &lt;/simpleType>
         *                 &lt;/attribute>
         *                 &lt;attribute name="origineFichier" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="hash" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                 &lt;attribute ref="{}statutChiffrement"/>
         *                 &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
         *                 &lt;attribute name="verificationCertificat" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *       &lt;attribute name="type" use="required">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
         *             &lt;enumeration value="1"/>
         *             &lt;enumeration value="2"/>
         *             &lt;enumeration value="3"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *       &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
         *       &lt;attribute name="statutOuverture" use="required">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *             &lt;enumeration value="OUV"/>
         *             &lt;enumeration value="FER"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *       &lt;attribute name="statutAdmissibilite" use="required">
         *         &lt;simpleType>
         *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *             &lt;enumeration value="ATR"/>
         *             &lt;enumeration value="ADM"/>
         *             &lt;enumeration value="NAD"/>
         *           &lt;/restriction>
         *         &lt;/simpleType>
         *       &lt;/attribute>
         *       &lt;attribute ref="{}statutChiffrement"/>
         *       &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "keysIvsChiffre",
            "keyIvDechiffre",
            "fichier"
        })
        public static class Enveloppe implements Serializable {
            @XmlElement(required = true)
            protected KeysIvsChiffre keysIvsChiffre;
            @XmlElement(required = true)
            protected Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre keyIvDechiffre;
            @XmlElement(name = "Fichier", required = true)
            protected List<Fichier> fichier;
            @XmlAttribute(name = "type", required = true)
            protected int type;
            @XmlAttribute(name = "numLot", required = true)
            protected int numLot;
            @XmlAttribute(name = "statutOuverture", required = true)
            protected String statutOuverture;
            @XmlAttribute(name = "statutAdmissibilite", required = true)
            protected String statutAdmissibilite;
            @XmlAttribute(name = "statutChiffrement")
            protected String statutChiffrement;
            @XmlAttribute(name = "idEnveloppe", required = true)
            protected int idEnveloppe;
            @XmlAttribute(name = "integrite", required = true)
            protected Boolean integrite;
            @XmlAttribute(name = "idAgent", required = true)
            protected Integer idAgent;

            public Integer getIdAgent() {
                return idAgent;
            }

            public void setIdAgent(Integer idAgent) {
                this.idAgent = idAgent;
            }

            /**
             * Gets the value of the fichier property.
             *
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fichier property.
             *
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFichier().add(newItem);
             * </pre>
             *
             *
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Fichier }
             *
             *
             */
            public List<Fichier> getFichier() {
                if (fichier == null) {
                    fichier = new ArrayList<>();
                }
                return this.fichier;
            }

            /**
             * Obtient la valeur de la propri�t� type.
             *
             */
            public int getType() {
                return type;
            }

            /**
             * D�finit la valeur de la propri�t� type.
             *
             */
            public void setType(int value) {
                this.type = value;
            }

            /**
             * Obtient la valeur de la propri�t� numLot.
             *
             */
            public int getNumLot() {
                return numLot;
            }

            /**
             * D�finit la valeur de la propri�t� numLot.
             *
             */
            public void setNumLot(int value) {
                this.numLot = value;
            }

            /**
             * Obtient la valeur de la propri�t� statutOuverture.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStatutOuverture() {
                return statutOuverture;
            }

            /**
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setIntegrite(Boolean value) {
                this.integrite = value;
            }

            public Boolean getIntegrite() {
                return integrite;
            }

            /**
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStatutOuverture(String value) {
                this.statutOuverture = value;
            }

            /**
             * Obtient la valeur de la propri�t� statutAdmissibilite.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStatutAdmissibilite() {
                return statutAdmissibilite;
            }

            /**
             * D�finit la valeur de la propri�t� statutAdmissibilite.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStatutAdmissibilite(String value) {
                this.statutAdmissibilite = value;
            }

            /**
             * Obtient la valeur de la propri�t� statutChiffrement.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getStatutChiffrement() {
                return statutChiffrement;
            }

            /**
             * D�finit la valeur de la propri�t� statutChiffrement.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setStatutChiffrement(String value) {
                this.statutChiffrement = value;
            }

            /**
             * Obtient la valeur de la propri�t� idEnveloppe.
             *
             */
            public int getIdEnveloppe() {
                return idEnveloppe;
            }

            /**
             * D�finit la valeur de la propri�t� idEnveloppe.
             *
             */
            public void setIdEnveloppe(int value) {
                this.idEnveloppe = value;
            }

            /**
             * Obtient la valeur de la propri�t� keysIvsChiffre.
             *
             * @return
             *     possible object is
             *     {@link Fichier.BlocsChiffrement.BlocChiffrement.KeysIvsChiffre }
             *
             */
            public KeysIvsChiffre getKeysIvsChiffre() {
                return keysIvsChiffre;
            }

            /**
             * D�finit la valeur de la propri�t� keysIvsChiffre.
             *
             * @param value
             *     allowed object is
             *     {@link Fichier.BlocsChiffrement.BlocChiffrement.KeysIvsChiffre }
             *
             */
            public void setKeysIvsChiffre(KeysIvsChiffre value) {
                this.keysIvsChiffre = value;
            }

            /**
             * Obtient la valeur de la propri�t� keyIvDechiffre.
             *
             * @return
             *     possible object is
             *     {@link Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre }
             *
             */
            public Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre getKeyIvDechiffre() {
                return keyIvDechiffre;
            }

            /**
             * D�finit la valeur de la propri�t� keyIvDechiffre.
             *
             * @param value
             *     allowed object is
             *     {@link Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre }
             *
             */
            public void setKeyIvDechiffre(Fichier.BlocsChiffrement.BlocChiffrement.KeyIvDechiffre value) {
                this.keyIvDechiffre = value;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "keysIv"
            })
            public static class KeysIvsChiffre implements Serializable {

                @XmlElement(required = true)
                protected List<KeysIv> keysIv;

                /**
                 * Gets the value of the keysIv property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the keysIv property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getKeysIv().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Fichier.BlocsChiffrement.BlocChiffrement.KeysIvsChiffre.KeysIv }
                 *
                 *
                 */
                public List<KeysIv> getKeysIv() {
                    if (keysIv == null) {
                        keysIv = new ArrayList<>();
                    }
                    return this.keysIv;
                }


                /**
                 * <p>Classe Java pour anonymous complex type.
                 *
                 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
                 *
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *         &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *         &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "key",
                    "iv",
                    "certificate"
                })
                public static class KeysIv implements Serializable {

                    @XmlElement(required = true)
                    protected String key;
                    @XmlElement(required = true)
                    protected String iv;
                    @XmlElement(required = true)
                    protected String certificate;

                    /**
                     * Obtient la valeur de la propri�t� key.
                     *
                     * @return
                     *     possible object is
                     *     byte[]
                     */
                    public String getKey() {
                        return key;
                    }

                    /**
                     * D�finit la valeur de la propri�t� key.
                     *
                     * @param value
                     *     allowed object is
                     *     byte[]
                     */
                    public void setKey(String value) {
                        this.key = value;
                    }

                    /**
                     * Obtient la valeur de la propri�t� iv.
                     *
                     * @return
                     *     possible object is
                     *     byte[]
                     */
                    public String getIv() {
                        return iv;
                    }

                    /**
                     * D�finit la valeur de la propri�t� iv.
                     *
                     * @param value
                     *     allowed object is
                     *     byte[]
                     */
                    public void setIv(String value) {
                        this.iv = value;
                    }

                    /**
                     * Obtient la valeur de la propri�t� certificate.
                     *
                     * @return
                     *     possible object is
                     *     byte[]
                     */
                    public String getCertificate() {
                        return certificate;
                    }

                    /**
                     * D�finit la valeur de la propri�t� certificate.
                     *
                     * @param value
                     *     allowed object is
                     *     byte[]
                     */
                    public void setCertificate(String value) {
                        this.certificate = value;
                    }

                }

            }



            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="BlocsChiffrement">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="BlocChiffrement" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="keysIvsChiffre">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="keysIv" maxOccurs="unbounded">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
             *                                                 &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
             *                                                 &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="keyIvDechiffre">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
             *                                       &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                           &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
             *                           &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute ref="{}statutChiffrement"/>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="idFichier" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="typeFichier" use="required">
             *         &lt;simpleType>
             *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *             &lt;enumeration value="PRI"/>
             *             &lt;enumeration value="ACE"/>
             *             &lt;enumeration value="SEC"/>
             *           &lt;/restriction>
             *         &lt;/simpleType>
             *       &lt;/attribute>
             *       &lt;attribute name="origineFichier" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="hash" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *       &lt;attribute ref="{}statutChiffrement"/>
             *       &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
             *       &lt;attribute name="verificationCertificat" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nom",
                "signature",
                "empreinte",
                "blocsChiffrement"
            })
            public static class Fichier implements Serializable {

                @XmlElement(name = "Nom", required = true)
                protected String nom;
                @XmlElement(name = "Signature")
                protected String signature;
                @XmlElement(name = "Empreinte", required = true)
                protected String empreinte;
                @XmlElement(name = "BlocsChiffrement", required = true)
                protected BlocsChiffrement blocsChiffrement;
                @XmlAttribute(name = "idFichier")
                protected Integer idFichier;
                @XmlAttribute(name = "typeFichier", required = true)
                protected String typeFichier;
                @XmlAttribute(name = "origineFichier")
                protected String origineFichier;
                @XmlAttribute(name = "numOrdre")
                protected Integer numOrdre;
                @XmlAttribute(name = "tailleEnClair")
                protected Long tailleEnClair;
                @XmlAttribute(name = "hash")
                protected Boolean hash;
                @XmlAttribute(name = "statutChiffrement")
                protected String statutChiffrement;
                @XmlAttribute(name = "nbrBlocsChiffrement")
                protected Integer nbrBlocsChiffrement;
                @XmlAttribute(name = "verificationCertificat")
                protected String verificationCertificat;
                @XmlAttribute(name = "integrite")
                protected Boolean integrite;

                public Boolean getIntegrite() {
                    return integrite;
                }

                public void setIntegrite(Boolean integrite) {
                    this.integrite = integrite;
                }

                /**
                 * Obtient la valeur de la propri�t� nom.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getNom() {
                    return nom;
                }

                /**
                 * D�finit la valeur de la propri�t� nom.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setNom(String value) {
                    this.nom = value;
                }

                /**
                 * Obtient la valeur de la propri�t� signature.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSignature() {
                    return signature;
                }

                /**
                 * D�finit la valeur de la propri�t� signature.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSignature(String value) {
                    this.signature = value;
                }

                /**
                 * Obtient la valeur de la propri�t� empreinte.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getEmpreinte() {
                    return empreinte;
                }

                /**
                 * D�finit la valeur de la propri�t� empreinte.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setEmpreinte(String value) {
                    this.empreinte = value;
                }

                /**
                 * Obtient la valeur de la propri�t� blocsChiffrement.
                 *
                 * @return
                 *     possible object is
                 *     {@link BlocsChiffrement }
                 *
                 */
                public BlocsChiffrement getBlocsChiffrement() {
                    return blocsChiffrement;
                }

                /**
                 * D�finit la valeur de la propri�t� blocsChiffrement.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link BlocsChiffrement }
                 *
                 */
                public void setBlocsChiffrement(BlocsChiffrement value) {
                    this.blocsChiffrement = value;
                }

                /**
                 * Obtient la valeur de la propri�t� idFichier.
                 *
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *
                 */
                public Integer getIdFichier() {
                    return idFichier;
                }

                /**
                 * D�finit la valeur de la propri�t� idFichier.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *
                 */
                public void setIdFichier(Integer value) {
                    this.idFichier = value;
                }

                /**
                 * Obtient la valeur de la propri�t� typeFichier.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getTypeFichier() {
                    return typeFichier;
                }

                /**
                 * D�finit la valeur de la propri�t� typeFichier.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setTypeFichier(String value) {
                    this.typeFichier = value;
                }

                /**
                 * Obtient la valeur de la propri�t� origineFichier.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getOrigineFichier() {
                    return origineFichier;
                }

                /**
                 * D�finit la valeur de la propri�t� origineFichier.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setOrigineFichier(String value) {
                    this.origineFichier = value;
                }

                /**
                 * Obtient la valeur de la propri�t� numOrdre.
                 *
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *
                 */
                public Integer getNumOrdre() {
                    return numOrdre;
                }

                /**
                 * D�finit la valeur de la propri�t� numOrdre.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *
                 */
                public void setNumOrdre(Integer value) {
                    this.numOrdre = value;
                }

                /**
                 * Obtient la valeur de la propri�t� tailleEnClair.
                 *
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *
                 */
                public Long getTailleEnClair() {
                    return tailleEnClair;
                }

                /**
                 * D�finit la valeur de la propri�t� tailleEnClair.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *
                 */
                public void setTailleEnClair(Long value) {
                    this.tailleEnClair = value;
                }

                /**
                 * Obtient la valeur de la propri�t� hash.
                 *
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *
                 */
                public Boolean isHash() {
                    return hash;
                }

                /**
                 * D�finit la valeur de la propri�t� hash.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *
                 */
                public void setHash(Boolean value) {
                    this.hash = value;
                }

                /**
                 * Obtient la valeur de la propri�t� statutChiffrement.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getStatutChiffrement() {
                    return statutChiffrement;
                }

                /**
                 * D�finit la valeur de la propri�t� statutChiffrement.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setStatutChiffrement(String value) {
                    this.statutChiffrement = value;
                }

                /**
                 * Obtient la valeur de la propri�t� nbrBlocsChiffrement.
                 *
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *
                 */
                public Integer getNbrBlocsChiffrement() {
                    return nbrBlocsChiffrement;
                }

                /**
                 * D�finit la valeur de la propri�t� nbrBlocsChiffrement.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *
                 */
                public void setNbrBlocsChiffrement(Integer value) {
                    this.nbrBlocsChiffrement = value;
                }

                /**
                 * Obtient la valeur de la propri�t� verificationCertificat.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getVerificationCertificat() {
                    return verificationCertificat;
                }

                /**
                 * D�finit la valeur de la propri�t� verificationCertificat.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setVerificationCertificat(String value) {
                    this.verificationCertificat = value;
                }


                /**
                 * <p>Classe Java pour anonymous complex type.
                 *
                 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
                 *
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="BlocChiffrement" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="keysIvsChiffre">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="keysIv" maxOccurs="unbounded">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *                                       &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *                                       &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="keyIvDechiffre">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *                             &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *                 &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute ref="{}statutChiffrement"/>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 *
                 *
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "blocChiffrement"
                })
                public static class BlocsChiffrement implements Serializable {

                    @XmlElement(name = "BlocChiffrement", required = true)
                    protected List<BlocChiffrement> blocChiffrement;
                    @XmlAttribute(name = "nbrBlocs")
                    protected Integer nbrBlocs;

                    /**
                     * Gets the value of the blocChiffrement property.
                     *
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the blocChiffrement property.
                     *
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getBlocChiffrement().add(newItem);
                     * </pre>
                     *
                     *
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link BlocChiffrement }
                     *
                     *
                     */
                    public List<BlocChiffrement> getBlocChiffrement() {
                        if (blocChiffrement == null) {
                            blocChiffrement = new ArrayList<>();
                        }
                        return this.blocChiffrement;
                    }

                    /**
                     * Obtient la valeur de la propri�t� nbrBlocs.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Integer }
                     *     
                     */
                    public Integer getNbrBlocs() {
                        return nbrBlocs;
                    }

                    /**
                     * D�finit la valeur de la propri�t� nbrBlocs.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Integer }
                     *     
                     */
                    public void setNbrBlocs(Integer value) {
                        this.nbrBlocs = value;
                    }


                    /**
                     * <p>Classe Java pour anonymous complex type.
                     * 
                     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="keysIvsChiffre">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="keysIv" maxOccurs="unbounded">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                     *                             &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                     *                             &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="keyIvDechiffre">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                     *                   &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *       &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
                     *       &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute ref="{}statutChiffrement"/>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    public static class BlocChiffrement implements Serializable {

                        @XmlAttribute(name = "tailleEnClair")
                        protected Long tailleEnClair;
                        @XmlAttribute(name = "tailleApresChiffrement")
                        protected Long tailleApresChiffrement;
                        @XmlAttribute(name = "numOrdre")
                        protected Integer numOrdre;
                        @XmlAttribute(name = "empreinte")
                        protected String empreinte;
                        @XmlAttribute(name = "nomBloc")
                        protected String nomBloc;
                        @XmlAttribute(name = "statutChiffrement")
                        protected String statutChiffrement;

                        /**
                         * Obtient la valeur de la propri�t� tailleEnClair.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Long getTailleEnClair() {
                            return tailleEnClair;
                        }

                        /**
                         * D�finit la valeur de la propri�t� tailleEnClair.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setTailleEnClair(Long value) {
                            this.tailleEnClair = value;
                        }

                        /**
                         * Obtient la valeur de la propri�t� tailleApresChiffrement.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Long getTailleApresChiffrement() {
                            return tailleApresChiffrement;
                        }

                        /**
                         * D�finit la valeur de la propri�t� tailleApresChiffrement.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setTailleApresChiffrement(Long value) {
                            this.tailleApresChiffrement = value;
                        }

                        /**
                         * Obtient la valeur de la propri�t� numOrdre.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Integer }
                         *     
                         */
                        public Integer getNumOrdre() {
                            return numOrdre;
                        }

                        /**
                         * D�finit la valeur de la propri�t� numOrdre.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Integer }
                         *     
                         */
                        public void setNumOrdre(Integer value) {
                            this.numOrdre = value;
                        }

                        /**
                         * Obtient la valeur de la propri�t� empreinte.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEmpreinte() {
                            return empreinte;
                        }

                        /**
                         * D�finit la valeur de la propri�t� empreinte.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmpreinte(String value) {
                            this.empreinte = value;
                        }

                        /**
                         * Obtient la valeur de la propri�t� nomBloc.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNomBloc() {
                            return nomBloc;
                        }

                        /**
                         * D�finit la valeur de la propri�t� nomBloc.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNomBloc(String value) {
                            this.nomBloc = value;
                        }

                        /**
                         * Obtient la valeur de la propri�t� statutChiffrement.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getStatutChiffrement() {
                            return statutChiffrement;
                        }

                        /**
                         * D�finit la valeur de la propri�t� statutChiffrement.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setStatutChiffrement(String value) {
                            this.statutChiffrement = value;
                        }


                        /**
                         * <p>Classe Java pour anonymous complex type.
                         * 
                         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                         *         &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "key",
                            "iv"
                        })
                        public static class KeyIvDechiffre implements Serializable {

                            @XmlElement(required = true)
                            protected String key;
                            @XmlElement(required = true)
                            protected String iv;

                            /**
                             * Obtient la valeur de la propri�t� key.
                             * 
                             * @return
                             *     possible object is
                             *     byte[]
                             */
                            public String getKey() {
                                return key;
                            }

                            /**
                             * D�finit la valeur de la propri�t� key.
                             * 
                             * @param value
                             *     allowed object is
                             *     byte[]
                             */
                            public void setKey(String value) {
                                this.key = value;
                            }

                            /**
                             * Obtient la valeur de la propri�t� iv.
                             * 
                             * @return
                             *     possible object is
                             *     byte[]
                             */
                            public String getIv() {
                                return iv;
                            }

                            /**
                             * D�finit la valeur de la propri�t� iv.
                             * 
                             * @param value
                             *     allowed object is
                             *     byte[]
                             */
                            public void setIv(String value) {
                                this.iv = value;
                            }

                        }


                        /**
                         * <p>Classe Java pour anonymous complex type.
                         * 
                         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="keysIv" maxOccurs="unbounded">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                         *                   &lt;element name="iv" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                         *                   &lt;element name="certificate" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                    }

                }

            }

        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nom"
    })
    public static class OperateurEconomique implements Serializable {

        @XmlElement(name = "Nom", required = true)
        protected String nom;

        /**
         * Obtient la valeur de la propri�t� nom.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNom() {
            return nom;
        }

        /**
         * D�finit la valeur de la propri�t� nom.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNom(String value) {
            this.nom = value;
        }

    }

}
