package com.atexo.desktop.utilitaire;

import com.atexo.desktop.config.RestTemplateConfig;

public class AtexoTimestampDataLoader extends AtexoDataLoader {
	public static final String TIMESTAMP_QUERY_CONTENT_TYPE = "application/timestamp-query";
	public AtexoTimestampDataLoader(RestTemplateConfig restTemplateConfig) {
		super(restTemplateConfig, TIMESTAMP_QUERY_CONTENT_TYPE);
	}

	@Override
	public void setContentType(String contentType) {
	}
}
