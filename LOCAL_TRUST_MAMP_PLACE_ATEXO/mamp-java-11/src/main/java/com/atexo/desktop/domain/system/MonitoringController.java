package com.atexo.desktop.domain.system;

import com.atexo.desktop.config.RestTemplateHeaderModifierInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RequestMapping("/monitoring")
@RestController
@Slf4j
public class MonitoringController {

    @GetMapping("health")
    public String initSession() {
        return "OK";
    }


    @PostMapping("navigateur")
    public String initUserAgent(@RequestBody Map<String, String> headers) {
        if (headers != null) {
            headers.keySet().forEach(header -> {
                if (header.equalsIgnoreCase("user-agent") || header.equalsIgnoreCase("userAgent")) {
                    String value = headers.get(header);
                    RestTemplateHeaderModifierInterceptor.userAgent = value;
                    System.setProperty("http.agent", value);
                    log.info("Mise à jour de user agent par {}", value);
                }
            });
        }
        return "OK";
    }

}
