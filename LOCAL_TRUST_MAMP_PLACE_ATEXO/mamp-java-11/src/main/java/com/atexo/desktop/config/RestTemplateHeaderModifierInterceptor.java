package com.atexo.desktop.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Slf4j
@Configuration
public class RestTemplateHeaderModifierInterceptor
        implements ClientHttpRequestInterceptor {
    public static String userAgent;
    static {
        userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0";
    }

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {
        if (userAgent != null) {
            request.getHeaders().add("user-agent", userAgent);
        }
        String user = System.getProperty("http.proxyUser");
        String password = System.getProperty("http.proxyPassword");
        if (StringUtils.hasText(user) && StringUtils.hasText(password)) {
            String userpass = user + ":" + password;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
            log.info("Setting proxy authentification {} ", basicAuth);
            request.getHeaders().add("Proxy-Authorization", basicAuth);
        }
        return execution.execute(request, body);
    }
}