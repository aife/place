package com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour anonymous complex type.
 * 
 * <p>
 * Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette
 * classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{}ReponseAnnonceType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "ReponseAnnonce")
public class ReponseAnnonce extends ReponseAnnonceType {

    //uniquement utilisé pour le dechiffrement pour RSEM
    private String statutPhase;

    public String getStatutPhase() {
        return statutPhase;
    }

    public void setStatutPhase(String statutPhase) {
        this.statutPhase = statutPhase;
    }
}
