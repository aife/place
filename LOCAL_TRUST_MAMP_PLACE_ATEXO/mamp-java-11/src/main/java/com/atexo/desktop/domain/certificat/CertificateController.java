package com.atexo.desktop.domain.certificat;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.certificat.store.CertificateFilterEnum;
import com.atexo.desktop.domain.certificat.store.CertificateStoreService;
import com.atexo.desktop.utilitaire.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Set;


@RequestMapping("certificat")
@RestController
@Slf4j
public class CertificateController {


    private final CertificateStoreService certificateStoreService;

    public CertificateController(CertificateStoreService certificateStoreService) {
        this.certificateStoreService = certificateStoreService;
    }

    @GetMapping("chiffrementCertificats")
    public Set<CertificateItem> findDualKeyCertificats() throws RecuperationCertificatException, IOException {
        return certificateStoreService.getAllCertificates(CertificateFilterEnum.DUAL_KEY);
    }

    @GetMapping("signatureCertificats")
    public Set<CertificateItem> findAllSignatureCertificats() throws RecuperationCertificatException, IOException {
        return certificateStoreService.getAllCertificates(CertificateFilterEnum.RGS);
    }

    @GetMapping("/provider")
    public TypeProviderEnum getProvider() {
        return Util.determinerProvider();
    }
}
