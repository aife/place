package com.atexo.desktop.domain.dualkeys.exceptions;

public class DualKeyGenerationException extends Exception {
    public DualKeyGenerationException() {
        super("The generation of dual keys or certificate failed");
    }

    public DualKeyGenerationException(String msg) {
        super(msg);
    }

    public DualKeyGenerationException(String msg, Throwable e) {
        super(msg, e);
    }
}
