package com.atexo.desktop.domain.system;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigurationResponse {
	private String label;
	private boolean isValid;
}
