package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@XmlType(name = "FichierJNLP")

public class FichierJNLP implements Serializable {
    private List<KeysIvJNLP> keysIvJNLP;
    private int idFichier;
    private int numBloc;

    public List<KeysIvJNLP> getCertificat() {
        if (keysIvJNLP == null) {
            keysIvJNLP = new ArrayList<>();
        }
        return keysIvJNLP;
    }

    public void setCertificat(List<KeysIvJNLP> keysIvJNLP) {
        this.keysIvJNLP = keysIvJNLP;
    }

    public int getIdFichier() {
        return idFichier;
    }

    public void setIdFichier(int idFichier) {
        this.idFichier = idFichier;
    }

    public int getNumBloc() {
        return numBloc;
    }

    public void setNumBloc(int numBloc) {
        this.numBloc = numBloc;
    }
}
