package com.atexo.desktop.domain.dualkeys;

import com.atexo.desktop.domain.dualkeys.exceptions.DualKeyGenerationException;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Base64;

@RestController
@RequestMapping("/dualKeys")
@Slf4j
public class DualKeysController {

    private final DualKeysService dualKeysService;

    public DualKeysController(DualKeysService dualKeysService) {
        this.dualKeysService = dualKeysService;
    }

    @PostMapping("/generate")
    public GenerationResultDTO generateDualKeys(@RequestBody DualKeyFormDTO dualKeyFormDTO) throws DualKeyGenerationException, CertificateEncodingException {
        MDC.put("pf", "DUAL_KEY");
        Certificate publicKeyCert;
        boolean hasBeenGenerated = !dualKeyFormDTO.isExistingCert();

        if (dualKeyFormDTO.isExistingCert()) {
            publicKeyCert = dualKeysService.exportFromExistingCert(dualKeyFormDTO);
        } else {
            try {
                publicKeyCert = dualKeysService.generatex509Cert(dualKeyFormDTO);
            } catch (Exception ex) {
                log.error(ex.getMessage());
                return GenerationResultDTO.builder()
                        .generationExecuted(hasBeenGenerated).generationStatus(false)
                        .sendingMpeStatus(false).build();
            }
        }

        DualKeyMpePayloadDTO payload = new DualKeyMpePayloadDTO();
        payload.setCertificat(Base64.getMimeEncoder().encodeToString(publicKeyCert.getEncoded()));
        payload.setEstCleChiffrementSecours(dualKeyFormDTO.isEstCleChiffrementSecours());
        payload.setNomCleChiffrement(dualKeyFormDTO.getAlias());


        X500Name x500name = new JcaX509CertificateHolder((X509Certificate) publicKeyCert).getSubject();
        RDN cn = x500name.getRDNs(BCStyle.CN)[0];

        payload.setCnCleChiffrement(IETFUtils.valueToString(cn.getFirst().getValue()));

        //Sending request
        boolean requestResult = dualKeysService.sendDataToCallbackURL(dualKeyFormDTO.getCaUrl(), dualKeyFormDTO.getSessionId(), payload);
        return GenerationResultDTO.builder()
                .generationExecuted(hasBeenGenerated).generationStatus(!dualKeyFormDTO.isExistingCert())
                .sendingMpeStatus(requestResult).build();
    }
}
