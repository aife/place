package com.atexo.desktop.utilitaire;

import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;
import java.util.Arrays;

@Slf4j
public final class TrustAllManager implements X509TrustManager {

    public static final TrustAllManager THE_INSTANCE = new TrustAllManager();

    private TrustAllManager() {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
        // DO nothing
        log.info("checkClientTrusted {}", Arrays.stream(x509Certificates).map(X509Certificate::getSubjectDN).toArray());
    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
        // DO nothing
        log.info("checkServerTrusted {}", Arrays.stream(x509Certificates).map(X509Certificate::getSubjectDN).toArray());
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}

