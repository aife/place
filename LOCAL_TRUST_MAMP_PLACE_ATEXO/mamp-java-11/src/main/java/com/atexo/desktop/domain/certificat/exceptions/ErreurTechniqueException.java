package com.atexo.desktop.domain.certificat.exceptions;

/**
 * Exception pour signaler un souci lors de la manipulation des certificats.
 */
public class ErreurTechniqueException extends Exception {

	public ErreurTechniqueException() {
		super();
	}

	public ErreurTechniqueException(String message) {
		super(message);
	}
}
