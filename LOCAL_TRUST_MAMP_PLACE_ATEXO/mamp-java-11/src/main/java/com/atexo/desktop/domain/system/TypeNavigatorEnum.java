package com.atexo.desktop.domain.system;

/**
 * Le type d'os
 */
public enum TypeNavigatorEnum {
    Chrome("75"), IE("9"), ESR("75"), Firefox("75"), EDGE("0"), Indetermine("0");
    private final String version;

    TypeNavigatorEnum(String version) {
        this.version = version;

    }

    public String getVersion() {
        return version;
    }

    public static TypeNavigatorEnum get(String nom) {

        for (TypeNavigatorEnum typeNavigatorEnum : values()) {
            if (nom.toUpperCase().startsWith(typeNavigatorEnum.name().toUpperCase())) {
                return typeNavigatorEnum;
            }
        }

        return Indetermine;
    }
}
