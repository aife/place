package com.atexo.desktop.domain.keystore;

import com.atexo.desktop.config.KeyStoreProperties;
import com.atexo.desktop.domain.certificat.enums.TypeOsEnum;
import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import com.atexo.desktop.domain.certificat.exceptions.RecuperationCertificatException;
import com.atexo.desktop.domain.dechiffrement.dtos.KeyPair;
import com.atexo.desktop.domain.dialog.DialogEnum;
import com.atexo.desktop.domain.dialog.DialogService;
import com.atexo.desktop.domain.dialog.dtos.DialogResponseDTO;
import com.atexo.desktop.domain.keystore.jks.JksStoreService;
import com.atexo.desktop.domain.keystore.pkcs11.Pkcs11KeyStoreService;
import com.atexo.desktop.utilitaire.Util;
import eu.europa.esig.dss.token.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

@Service
@Slf4j
public class KeyStoreService {

    private final KeyStoreProperties keyStoreProperties;
    private final Pkcs11KeyStoreService pkcs11KeyStoreService;
    private final JksStoreService jksStoreService;
    private final DialogService dialogService;

    public KeyStoreService(KeyStoreProperties keyStoreProperties, Pkcs11KeyStoreService pkcs11KeyStoreService, JksStoreService getJKSPrivateKey, DialogService dialogService) {
        this.keyStoreProperties = keyStoreProperties;
        this.pkcs11KeyStoreService = pkcs11KeyStoreService;
        this.jksStoreService = getJKSPrivateKey;
        this.dialogService = dialogService;
    }

    //TODO check repareralias on windows
    public KeyStore getKeyStore() throws RecuperationCertificatException {
        TypeProviderEnum provider = Util.determinerProvider();
        TypeOsEnum os = Util.determinerOs();

        KeyStore keyStore;

        try {
            switch (provider) {
                case JKS:
                case MSCAPI:
                case PKCS12:
                    keyStore = KeyStore.getInstance(provider.getType());
                    break;
                case APPLE:
                    keyStore = KeyStore.getInstance(provider.getType(), provider.getNom());
                    break;
                case PKCS11:
                    keyStore = pkcs11KeyStoreService.getKeyStore();
                    break;
                default:
                    throw new NotImplementedException();
            }

            if (TypeOsEnum.Linux == os) {

                try (FileInputStream stream = new FileInputStream(keyStoreProperties.getLinux().getPath())) {
                    keyStore.load(stream, keyStoreProperties.getLinux().getPassword().toCharArray());
                }
            } else keyStore.load(null, null);
            return keyStore;
        } catch (NoSuchAlgorithmException | CertificateException | IOException | NoSuchProviderException |
                 KeyStoreException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + provider, e);
        }
    }

    public KeyStore store(KeyStore keyStore) throws RecuperationCertificatException, IOException {
        TypeProviderEnum provider = Util.determinerProvider();
        TypeOsEnum os = Util.determinerOs();

        try {

            if (TypeOsEnum.Linux == os) {
                try (FileOutputStream stream = new FileOutputStream(keyStoreProperties.getLinux().getPath())) {
                    keyStore.store(stream, keyStoreProperties.getLinux().getPassword().toCharArray());
                }
            } else keyStore.load(null, null);
            return keyStore;
        } catch (NoSuchAlgorithmException | CertificateException | IOException | KeyStoreException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + provider, e);
        }
    }

    public DSSPrivateKeyEntry getPrivateKeyEntry(TypeProviderEnum typeProvider, String alias) throws RecuperationCertificatException, FileNotFoundException {
        try {
            switch (typeProvider) {
                case PKCS11:
                    return pkcs11KeyStoreService.getPrivateKeyEntry(alias);
                case JKS:
                    try (AbstractKeyStoreTokenConnection token = new JKSSignatureToken(new FileInputStream(keyStoreProperties.getLinux().getPath()), new KeyStore.PasswordProtection(keyStoreProperties.getLinux().getPassword().toCharArray()))) {
                        char[] password = jksStoreService.getPassword(alias);
                        return token.getKey(alias, new KeyStore.PasswordProtection(password));
                    }
                case MSCAPI:
                    try (AbstractKeyStoreTokenConnection token = new MSCAPISignatureToken()) {
                        return token.getKey(alias);
                    }
                case APPLE:
                    try (AbstractKeyStoreTokenConnection token = new AppleSignatureToken()) {

                        DialogResponseDTO dialogResponse = dialogService.executeDialogRequest(DialogEnum.SYSTEM_PASSWORD, "");
                        if (dialogResponse == null) return null;
                        return token.getKey(alias, new KeyStore.PasswordProtection(dialogResponse.getValue().toCharArray()));
                    }
                default:
                    return null;
            }
        } catch (Exception e) {
            jksStoreService.removePassword(alias);
            throw e;
        }
    }

    public KeyPair getKeyPair(TypeProviderEnum typeProvider, BigInteger serial, String emetteur) throws RecuperationCertificatException {
        try {
            // chargement depuis le KeyStore
            KeyStore keyStore = getKeyStore();

            // parcourt de l'ensemble des occurances contenus dans le KeyStore
            // et récupération du bi-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
                BigInteger certificatSerial = certificat.getSerialNumber();
                String certificatEmetteur = certificat.getIssuerDN().toString();
                boolean serialsEgaux = certificatSerial.equals(serial);
                boolean emetteursEgaux = certificatEmetteur.equals(emetteur);
                log.info("Alias : " + aliasKey);
                log.info("Emetteur : " + certificatEmetteur + " => Emetteurs égaux : " + emetteursEgaux);
                log.info("Serial : " + certificatSerial + " => Serials égaux : " + serialsEgaux);
                if (serialsEgaux && emetteursEgaux) {
                    PrivateKey privateKey;
                    if (typeProvider.equals(TypeProviderEnum.JKS)) {
                        privateKey = getJKSPrivateKey(keyStore, aliasKey);
                    } else {
                        privateKey = (PrivateKey) keyStore.getKey(aliasKey, null);

                    }
                    return new KeyPair(certificat, privateKey, typeProvider);
                }
            }
        } catch (Exception e) {
            throw new RecuperationCertificatException("Erreur de recupération du certificat personnel avec comme Serial " + serial + " et comme Issuer " + emetteur, e);
        }

        return null;
    }

    public boolean isStoreAccessible() {
        try {
            getKeyStore();
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    private PrivateKey getJKSPrivateKey(KeyStore keyStore, String aliasKey) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        try {
            char[] passwordChars = jksStoreService.getPassword(aliasKey);
            if (passwordChars == null) {
                throw new UnrecoverableKeyException();
            }
            return (PrivateKey) keyStore.getKey(aliasKey, passwordChars);
        } catch (Exception e) {
            jksStoreService.removePassword(aliasKey);
            throw e;
        }
    }
}
