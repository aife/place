package com.atexo.desktop.domain.certificat.store;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.function.Predicate;

public class CertificateConstants {

    private CertificateConstants(){}

    public static final Predicate<X509Certificate> isUsableForEncryption = cert -> cert.getKeyUsage() != null && (cert.getKeyUsage()[2] || cert.getKeyUsage()[3]);
    public static final Predicate<X509Certificate> isUsableForElectronicSignature = cert -> cert.getKeyUsage() != null && (cert.getKeyUsage()[0] || cert.getKeyUsage()[1]);
    public static final Predicate<X509Certificate> hasValidDate = cert -> cert.getNotAfter().after(new Date());
    public static final Predicate<X509Certificate> isConform = cert -> cert.getKeyUsage() != null && cert.getKeyUsage()[1] && !cert.getKeyUsage()[2]
            && !cert.getKeyUsage()[3] && !cert.getKeyUsage()[4] && !cert.getKeyUsage()[5]
            && !cert.getKeyUsage()[6] && !cert.getKeyUsage()[7] && !cert.getKeyUsage()[8];
}
