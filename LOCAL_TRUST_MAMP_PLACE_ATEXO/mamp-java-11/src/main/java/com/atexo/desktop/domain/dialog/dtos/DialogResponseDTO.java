package com.atexo.desktop.domain.dialog.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DialogResponseDTO  {
    private boolean isDenied;
    private boolean isDismissed;
    private String value;
    private DismissReasonEnum dismiss;

    public enum DismissReasonEnum {
        cancel, backdrop, close, esc, timer
    }
}

