package com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FichierBloc implements Serializable {

    protected int typeEnveloppe;
    protected int numeroLot;
    private String nomEnveloppe;
    private String nomOperateurEconomique;
    private final String referenceUtilisateur;
    private final String nomFichier;
    private final String empreinte;
    private List<Bloc> blocs = new ArrayList<Bloc>();
    private final long taille;

    private int idFichier;

    public static class Bloc implements Serializable {

        private final Integer numero;

        private final long tailleChiffre;

        private String nomBloc;

        private final boolean chiffre;

        // constitue cle symetrique dechiffre
        private final String key;

        private final String iv;

        public Bloc(String nomBloc, Integer numero, long tailleChiffre, boolean chiffre, String key, String iv) {
            this.nomBloc = nomBloc;
            this.numero = numero;
            this.tailleChiffre = tailleChiffre;
            this.chiffre = chiffre;
            this.key = key;
            this.iv = iv;
        }

        public String getIdBloc() {
            return getNomBloc() != null ? getNomBloc() + "-0"
                    : getNumero().toString() + "-0";
        }


        public String getNomBloc() {
            return nomBloc;
        }

        public void setNomBloc(String nomBloc) {
            this.nomBloc = nomBloc;
        }


        public long getTailleChiffre() {
            return tailleChiffre;
        }

        public boolean isChiffre() {
            return chiffre;
        }

        public Integer getNumero() {
            return numero;
        }
    }

    public FichierBloc(int idFichier, int typeEnveloppe, int numeroLot, String nomFichier, List<Bloc> blocs, String referenceUtilisateur, long taille, String enpreinte) {
        this.typeEnveloppe = typeEnveloppe;
        this.numeroLot = numeroLot;
        this.nomFichier = nomFichier;
        this.empreinte = enpreinte;
        this.blocs = blocs;
        this.referenceUtilisateur = referenceUtilisateur;
        this.taille = taille;
        this.idFichier = idFichier;
    }

    public int getTypeEnveloppe() {
        return typeEnveloppe;
    }

    public int getNumeroLot() {
        return numeroLot;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public List<Bloc> getBlocs() {
        return blocs;
    }

    public String getReferenceUtilisateur() {
        return referenceUtilisateur;
    }

    public long getTaille() {
        return taille;
    }

    public String getNomEnveloppe() {
        return nomEnveloppe;
    }

    public void setNomEnveloppe(String nomEnveloppe) {
        this.nomEnveloppe = nomEnveloppe;
    }

    public String getNomOperateurEconomique() {
        return nomOperateurEconomique;
    }

    public void setNomOperateurEconomique(String nomOperateurEconomique) {
        this.nomOperateurEconomique = nomOperateurEconomique;
    }

    public int getIdFichier() {
        return idFichier;
    }

    public void setIdFichier(int idFichier) {
        this.idFichier = idFichier;
    }

    public String getEmpreinte() {
        return empreinte;
    }
}
