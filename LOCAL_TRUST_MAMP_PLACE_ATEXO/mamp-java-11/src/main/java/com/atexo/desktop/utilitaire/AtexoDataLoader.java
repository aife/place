package com.atexo.desktop.utilitaire;

import com.atexo.desktop.config.RestTemplateConfig;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

import static com.atexo.desktop.config.RestTemplateHeaderModifierInterceptor.userAgent;

@Service
public class AtexoDataLoader extends CommonsDataLoader {
    public static final String AUTHORIZATION = "Authorization";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(AtexoDataLoader.class);
    protected final RestTemplateConfig restTemplateConfig;

    public AtexoDataLoader(RestTemplateConfig restTemplateConfig, String contentType) {
        this.restTemplateConfig = restTemplateConfig;
        this.contentType = contentType;
    }

    public AtexoDataLoader(RestTemplateConfig restTemplateConfig) {
        this.restTemplateConfig = restTemplateConfig;
    }

    public AtexoDataLoader() {
        this.restTemplateConfig = null;
    }

    @Override
    public byte[] post(final String url, final byte[] content) throws DSSException {

        logger.debug("URL  " + url);
        HttpPost httpRequest = null;
        CloseableHttpResponse httpResponse = null;
        CloseableHttpClient client = null;
        try {
            final URI uri = URI.create(url.trim());
            httpRequest = new HttpPost(uri);
            final ByteArrayInputStream bis = new ByteArrayInputStream(content);
            final HttpEntity httpEntity = new InputStreamEntity(bis, content.length);
            final HttpEntity requestEntity = new BufferedHttpEntity(httpEntity);
            httpRequest.setEntity(requestEntity);
            if (contentType != null) {
                httpRequest.setHeader(CONTENT_TYPE, contentType);
            }
            if (userAgent != null) {
                httpRequest.setHeader("user-agent", userAgent);
            }

            client = getHttpClient(url);
            httpResponse = getHttpResponse(client, httpRequest);
            return readHttpResponse(httpResponse);
        } catch (IOException e) {
            throw new DSSException(e);
        } finally {
            try {
                if (httpRequest != null) {
                    httpRequest.releaseConnection();
                }
                if (httpResponse != null) {
                    EntityUtils.consumeQuietly(httpResponse.getEntity());
                }
            } finally {
                close(client);
            }
        }
    }

    private void close(CloseableHttpClient httpClient) {
        if (httpClient != null) {
            try {
                httpClient.close();
            } catch (Exception ex) {
                logger.warn("Could not close client", ex);
            }
        }
    }

    @Override
    protected synchronized CloseableHttpClient getHttpClient(String url) {
        logger.info("get Http Client {}", url);
        return restTemplateConfig.createHttpClient(url);
    }

}
