package com.atexo.desktop.utilitaire;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
public final class JAXBService implements Serializable {

    private static final long serialVersionUID = 986110064372345330L;

    private static final JAXBService INSTANCE = new JAXBService();
    public static final String ENCODING_UTF_8 = "UTF-8";

    private final Map<String, JAXBContext> contextMap;
    private final Map<String, Queue<Unmarshaller>> unmarshallerMap;

    public static JAXBService instance() {
        return INSTANCE;
    }

    private JAXBService() {
        contextMap = new ConcurrentHashMap<>();
        unmarshallerMap = new ConcurrentHashMap<>();
    }

    private JAXBContext getContext(String contextPath) {
        JAXBContext context = contextMap.get(contextPath);
        if (context == null) {
            try {
                context = JAXBContext.newInstance(contextPath);
                contextMap.put(contextPath, context);
            } catch (JAXBException e) {
                log.error(e.getMessage(), e);
            }
        }
        return context;
    }


    private Unmarshaller getUnmarshaller(String contextPath) {
        Queue<Unmarshaller> queue = unmarshallerMap.get(contextPath);
        if (queue == null) {
            queue = new ConcurrentLinkedQueue<>();
            unmarshallerMap.put(contextPath, queue);
        }
        Unmarshaller unmarshaller = queue.poll();
        if (unmarshaller == null) {
            unmarshaller = createUnMarshaller(contextPath);
        }
        return unmarshaller;
    }

    private Unmarshaller createUnMarshaller(String contextPath) {
        JAXBContext context = getContext(contextPath);
        if (context == null) {
            throw new RuntimeException("Impossible de créer le unmarshaller pour le contextPath " + contextPath);
        }
        try {
            return context.createUnmarshaller();
        } catch (JAXBException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    private void retreiveUnmarshaller(String contextPath, Unmarshaller unmarshaller) {
        if (unmarshaller != null) {
            Queue<Unmarshaller> queue = unmarshallerMap.get(contextPath);
            if (queue != null) {
                queue.add(unmarshaller);
            }
        }
    }

    public Object getAsObject(String xmlAsString, String contextPath) {
        return getAsObject(xmlAsString, contextPath, ENCODING_UTF_8);
    }

    public Object getAsObject(String xmlAsString, String contextPath, String encoding) {
        Unmarshaller unmarshaller = getUnmarshaller(contextPath);
        if (unmarshaller == null) {
            throw new RuntimeException("Impossible de créer le unmarshaller pour le contextPath " + contextPath);
        }

        try (ByteArrayInputStream in = new ByteArrayInputStream(xmlAsString.getBytes(encoding))) {
            return unmarshaller.unmarshal(in);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        } finally {
            retreiveUnmarshaller(contextPath, unmarshaller);
        }
    }


}
