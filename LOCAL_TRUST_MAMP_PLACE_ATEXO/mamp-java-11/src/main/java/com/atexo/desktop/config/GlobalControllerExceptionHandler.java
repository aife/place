package com.atexo.desktop.config;

import com.atexo.desktop.domain.dualkeys.exceptions.DualKeyGenerationException;
import com.atexo.desktop.domain.dualkeys.exceptions.DuplicateAliasException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import java.util.stream.Collectors;

import static java.lang.String.format;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {


    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<WsMessage> handleHttpClientErrorException(HttpClientErrorException ex) {
        log.error("handleHttpClientErrorException {}", ex.getMessage());
        WsMessage wsMessage = WsMessage.builder()
                .message(ex.getMessage()).code(ex.getRawStatusCode())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(ex.getRawStatusCode()));
    }


    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<WsMessage> handleResourceNotFoundException(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .sorted((s, anotherString) -> s != null ? s.compareTo(anotherString) : 0)
                .collect(Collectors.joining(", "));
        log.error("handleResourceNotFoundException {}", message);
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .type(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @ExceptionHandler(value = {MissingRequestHeaderException.class})
    public ResponseEntity<WsMessage> handleResourceMissingRequestHeaderException(MissingRequestHeaderException e) {

        final String message = e.getHeaderName() + " est manquant";
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .type(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
        log.error("handleResourceMissingRequestHeaderException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @ExceptionHandler(value = {UnsatisfiedServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {

        final String message = String.join(", ", e.getParamConditions()) + " sont manquant(e)s";
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .type(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<WsMessage> handleResourceUnsatisfiedServletRequestParameterException(MissingServletRequestParameterException e) {

        final String message = format("Le paramètre '%s' est obligatoire", e.getParameterName());
        WsMessage wsMessage = WsMessage.builder()
                .message(message)
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .type(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(wsMessage, HttpStatus.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @ExceptionHandler(value
            = {DualKeyGenerationException.class})
    protected ResponseEntity<Object> handleDualKeyGeneration(
            RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return new ResponseEntity<>(bodyOfResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value
            = {DuplicateAliasException.class})
    protected ResponseEntity<Object> handleDuplicateAlias(
            RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return new ResponseEntity<>(bodyOfResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<WsMessage> handleException(RuntimeException ex) {
        log.error("handleException {}", ex.getMessage(), ex);
        WsMessage wsMessage = WsMessage.builder()
                .message("Veuillez contacter un administrateur")
                .code(HttpStatus.BAD_REQUEST.value())
                .type(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();
        return new ResponseEntity<>(wsMessage, HttpStatus.BAD_REQUEST);
    }

}
