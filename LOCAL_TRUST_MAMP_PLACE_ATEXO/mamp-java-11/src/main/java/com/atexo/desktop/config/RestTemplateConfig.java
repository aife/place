package com.atexo.desktop.config;

import com.atexo.desktop.domain.dialog.DialogService;
import com.atexo.desktop.utilitaire.TrustAllManager;
import eu.europa.esig.dss.service.http.proxy.ProxyConfig;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import eu.europa.esig.dss.spi.client.http.Protocol;
import eu.europa.esig.dss.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Slf4j
@Service
public class RestTemplateConfig {
    private static final String SSL_PROTOCOL = "TLS";
    private final DialogService dialogService;

    public RestTemplateConfig(DialogService dialogService) {
        this.dialogService = dialogService;
    }

    public RestTemplate getRestTemplate(String url) {
        int HTTP_READ_TIMEOUT = 15000;
        int HTTP_CONNECT_TIMEOUT = 600000;
        return this.getRestTemplate(url, HTTP_CONNECT_TIMEOUT, HTTP_READ_TIMEOUT);
    }

    public RestTemplate getRestTemplate(String url, int httpConnectTimeout, int httpReadTimeout) {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(httpConnectTimeout);
        requestFactory.setReadTimeout(httpReadTimeout);
        requestFactory.setHttpClient(createHttpClient(url));

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        List<ClientHttpRequestInterceptor> interceptors
                = restTemplate.getInterceptors();
        if (CollectionUtils.isEmpty(interceptors)) {
            interceptors = new ArrayList<>();
        }
        interceptors.add(new RestTemplateHeaderModifierInterceptor());
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }

    public CloseableHttpClient createHttpClient(String url) {
        HttpClientBuilder httpClientBuilder;
        String useSystemProxies = System.getProperty("java.net.useSystemProxies");
        if ("true".equals(useSystemProxies)) {
            log.info("le parametre useSystemProxies est à {}", useSystemProxies);
            httpClientBuilder = HttpClientBuilder.create().useSystemProperties();
            httpClientBuilder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);

            String user = System.getProperty("http.proxyUser");
            String password = System.getProperty("http.proxyPassword");
            if (StringUtils.hasText(user) && StringUtils.hasText(password)) {
                String userpass = user + ":" + password;
                String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
                log.info("Setting proxy authentification {} ", basicAuth);
                Header httpHeaders = new BasicHeader("Proxy-Authorization", basicAuth);
                httpClientBuilder.setDefaultHeaders(List.of(httpHeaders));
            }

        } else {
            log.info("le parametre useSystemProxies est à {}", useSystemProxies);
            httpClientBuilder = createHttpClientWithProxy(url);
        }
        if (httpClientBuilder == null) {
            throw new RuntimeException("Impossible de créer le client HTTP");
        }

        httpClientBuilder.setSSLContext(createSSLContext());

        if (RestTemplateHeaderModifierInterceptor.userAgent != null) {
            httpClientBuilder.setUserAgent(RestTemplateHeaderModifierInterceptor.userAgent);
        }
        return httpClientBuilder.build();
    }

    private HttpClientBuilder createHttpClientWithProxy(String url) {

        final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        try {
            final SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (x509CertChain, authType) -> true)
                    .build();

            httpClientBuilder.setSSLContext(sslContext)
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .setConnectionManager(
                            new PoolingHttpClientConnectionManager(
                                    RegistryBuilder.<ConnectionSocketFactory>create()
                                            .register("http", PlainConnectionSocketFactory.INSTANCE)
                                            .register("https", new SSLConnectionSocketFactory(sslContext,
                                                    NoopHostnameVerifier.INSTANCE))
                                            .build()
                            ));
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            log.error("Erreur lors de la création du client HTTP avec proxy {}", e.getMessage());
        }

        return configureProxy(httpClientBuilder, url);
    }

    private String getProtocol(String url) {
        return getURL(url).getProtocol();
    }

    public SSLContext createSSLContext() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance(SSL_PROTOCOL);
            sslContext.init(null, new TrustManager[]{TrustAllManager.THE_INSTANCE}, new SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            log.error(e.getMessage());
        }
        return sslContext;
    }

    private ProxyConfig getProxyConfig(String url) {
        setProxyFromNavigator(url);
        ProxyConfig proxyConfig = new ProxyConfig();
        String user = System.getProperty("http.proxyUser");
        String password = System.getProperty("http.proxyPassword");
        if (System.getProperty("http.proxyHost") != null) {
            ProxyProperties proxyProperties = new ProxyProperties();
            proxyProperties.setHost(System.getProperty("http.proxyHost"));
            proxyProperties.setPort(Integer.parseInt(System.getProperty("http.proxyPort", "80")));
            proxyProperties.setUser(user);
            proxyProperties.setPassword(password);
            proxyProperties.setExcludedHosts(System.getProperty("http.nonProxyHosts"));
            proxyConfig.setHttpProperties(proxyProperties);
        }
        if (System.getProperty("https.proxyHost") != null) {
            ProxyProperties proxyProperties = new ProxyProperties();
            proxyProperties.setHost(System.getProperty("https.proxyHost"));
            proxyProperties.setPort(Integer.parseInt(System.getProperty("https.proxyPort", "80")));
            proxyProperties.setUser(user);
            proxyProperties.setPassword(password);
            proxyProperties.setExcludedHosts(System.getProperty("http.nonProxyHosts"));
            proxyConfig.setHttpsProperties(proxyProperties);
        }


        if (proxyConfig.getHttpProperties() == null && proxyConfig.getHttpsProperties() == null) {
            return null;
        }
        return proxyConfig;
    }

    public Proxy getProxy(String url) {
        setProxyFromNavigator(url);
        String proxyExcludedHosts = System.getProperty("http.nonProxyHosts");
        if (Utils.isStringNotEmpty(proxyExcludedHosts)) {
            final String[] hosts = proxyExcludedHosts.split("[,;| ]");
            for (String host : hosts) {
                try {
                    if (url.matches(host)) {
                        return Proxy.NO_PROXY;
                    }
                } catch (Exception e) {
                    log.error("Erreur regex {}: {}", host, e.getMessage());
                }
            }
        }

        if (System.getProperty("http.proxyHost") != null) {
            return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(System.getProperty("http.proxyHost"),
                    Integer.parseInt(System.getProperty("http.proxyPort", "80"))));
        }
        if (System.getProperty("https.proxyHost") != null) {
            return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(System.getProperty("https.proxyHost"),
                    Integer.parseInt(System.getProperty("https.proxyPort", "80"))));
        }
        return Proxy.NO_PROXY;

    }

    private void setProxyFromNavigator(String url) {
        if (System.getProperty("http.fromProxyPac") != null) {
            String proxy = dialogService.getProxy(url);
            List<String> urlProxy = Arrays.asList(proxy.split(";"));
            String protocol = getProtocol(url);
            if (Protocol.isHttp(protocol))
                setHttpProxy(urlProxy);
            if (Protocol.isHttps(protocol))
                setHttpsProxy(urlProxy);
        }
    }

    public void setHttpProxy(List<String> proxyList) {
        for (String proxy : proxyList) {
            proxy = proxy.replaceAll(" ", "");
            if ("DIRECT".equals(proxy)) {
                System.clearProperty("http.proxyHost");
                System.clearProperty("http.proxyPort");
                System.clearProperty("http.nonProxyHosts");
            } else if (proxy.contains("PROXY")) {
                List<String> list = Arrays.asList(proxy.replaceAll("PROXY", "").split(";"));
                if (list.isEmpty()) {
                    System.clearProperty("http.proxyHost");
                    System.clearProperty("http.proxyPort");
                    System.clearProperty("http.nonProxyHosts");
                } else {
                    List<String> proxyParams = Arrays.asList(list.get(0).split(":"));
                    System.setProperty("http.proxyHost", proxyParams.get(0));
                    System.setProperty("http.proxyPort", proxyParams.size() == 2 ? proxyParams.get(1) : "80");
                    log.info("Le proxy HTTP configuré est \"{}\":\"{}\"", System.getProperty("http.proxyHost"),
                            System.getProperty("http.proxyPort"));
                }
            }

        }

    }

    public void setHttpsProxy(List<String> proxyList) {
        for (String proxy : proxyList) {
            proxy = proxy.replaceAll(" ", "");
            if ("DIRECT".equals(proxy)) {
                System.clearProperty("https.proxyHost");
                System.clearProperty("https.proxyPort");
            } else if (proxy.contains("PROXY")) {
                List<String> list = Arrays.asList(proxy.replaceAll("PROXY", "").split(";"));
                if (list.isEmpty()) {
                    System.clearProperty("https.proxyHost");
                    System.clearProperty("https.proxyPort");
                } else {
                    List<String> proxyParams = Arrays.asList(list.get(0).split(":"));
                    System.setProperty("https.proxyHost", proxyParams.get(0));
                    System.setProperty("https.proxyPort", proxyParams.size() == 2 ? proxyParams.get(1) : "80");
                    log.info("Le proxy HTTPS configuré est \"{}\":\"{}\"", System.getProperty("https.proxyHost"),
                            System.getProperty("https.proxyPort"));
                }

            }

        }

    }

    private HttpClientBuilder configureProxy(HttpClientBuilder httpClientBuilder, String url) {
        ProxyConfig proxyConfig = getProxyConfig(url);
        if (proxyConfig == null) {
            return httpClientBuilder;
        }

        final String protocol = getURL(url).getProtocol();
        final boolean proxyHTTPS = Protocol.isHttps(protocol) && (proxyConfig.getHttpsProperties() != null);
        final boolean proxyHTTP = Protocol.isHttp(protocol) && (proxyConfig.getHttpProperties() != null);

        ProxyProperties proxyProps = null;
        if (proxyHTTPS) {
            log.debug("Use proxy https parameters");
            proxyProps = proxyConfig.getHttpsProperties();
        } else if (proxyHTTP) {
            log.debug("Use proxy http parameters");
            proxyProps = proxyConfig.getHttpProperties();
        } else {
            return null;
        }

        String proxyHost = proxyProps.getHost();
        int proxyPort = proxyProps.getPort();
        String proxyUser = proxyProps.getUser();
        String proxyPassword = proxyProps.getPassword();
        String proxyExcludedHosts = proxyProps.getExcludedHosts();

        if (Utils.isStringNotEmpty(proxyUser) && Utils.isStringNotEmpty(proxyPassword)) {
            AuthScope proxyAuth = new AuthScope(proxyHost, proxyPort);
            UsernamePasswordCredentials proxyCredentials = new UsernamePasswordCredentials(proxyUser, proxyPassword);
            final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(proxyAuth, proxyCredentials);
            httpClientBuilder = httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        }

        log.debug("proxy host/port: {}:{}", proxyHost, proxyPort);
        // TODO SSL peer shut down incorrectly when protocol is https
        final HttpHost proxy = new HttpHost(proxyHost, proxyPort, Protocol.HTTP.getName());

        if (Utils.isStringNotEmpty(proxyExcludedHosts)) {
            final String[] hosts = proxyExcludedHosts.split("[,;| ]");

            HttpRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy) {
                @Override
                public HttpRoute determineRoute(final HttpHost host, final HttpRequest request, final HttpContext context) throws HttpException {

                    String hostname = (host != null ? host.getHostName() : null);

                    if ((hosts != null) && (hostname != null)) {
                        for (String h : hosts) {
                            try {
                                if (hostname.matches(h)) {
                                    log.info("bypass proxy for that hostname {}", h);
                                    return new HttpRoute(host);
                                }
                            } catch (Exception e) {
                                log.error("Erreur regex {}: {}", h, e.getMessage());
                            }
                        }
                    }
                    return super.determineRoute(host, request, context);
                }
            };

            httpClientBuilder.setRoutePlanner(routePlanner);
        }

        return httpClientBuilder.setProxy(proxy);
    }

    private URL getURL(String urlString) {
        try {
            return new URL(urlString);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to create URL instance", e);
        }
    }

}
