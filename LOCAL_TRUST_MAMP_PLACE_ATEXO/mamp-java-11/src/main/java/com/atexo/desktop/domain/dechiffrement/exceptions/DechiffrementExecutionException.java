package com.atexo.desktop.domain.dechiffrement.exceptions;

import java.util.concurrent.ExecutionException;

/**
 * Exception pour signaler un souci lors du déchiffrement.
 */
@SuppressWarnings("serial")
public class DechiffrementExecutionException extends ExecutionException {

    public DechiffrementExecutionException(String message) {
        super(message);
    }

    public DechiffrementExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
