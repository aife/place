package com.atexo.desktop.domain.dechiffrement.dtos.jnlp;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@XmlType(name = "EnveloppeJNLP")
public class EnveloppeJNLP implements Serializable {
	private List<KeysIvJNLP> keysIvJNLP;

	private List<FichierJNLP> fichiersBlocs;
	private int idEnveloppe;
	private int numeroLot;
	private String typeEnveloppe;

	public List<KeysIvJNLP> getCertificat() {
		if (keysIvJNLP == null) {
			keysIvJNLP = new ArrayList<>();
		}
		return keysIvJNLP;
	}

	public void setCertificat(List<KeysIvJNLP> keysIvJNLP) {
		this.keysIvJNLP = keysIvJNLP;
	}

	public int getIdEnveloppe() {
		return idEnveloppe;
	}

	public void setIdEnveloppe(int idEnveloppe) {
		this.idEnveloppe = idEnveloppe;
	}

	public String getTypeEnveloppe() {
		return typeEnveloppe;
	}

	public void setTypeEnveloppe(String typeEnveloppe) {
		this.typeEnveloppe = typeEnveloppe;
	}

	public int getNumeroLot() {
		return numeroLot;
	}

	public void setNumeroLot(int numeroLot) {
		this.numeroLot = numeroLot;
	}

	public List<FichierJNLP> getFichiersBlocs() {
		if (fichiersBlocs == null) {
			fichiersBlocs = new ArrayList<>();
		}
		return fichiersBlocs;
	}

	public void setFichiersBlocs(List<FichierJNLP> fichiers) {
		this.fichiersBlocs = fichiers;
	}
}
