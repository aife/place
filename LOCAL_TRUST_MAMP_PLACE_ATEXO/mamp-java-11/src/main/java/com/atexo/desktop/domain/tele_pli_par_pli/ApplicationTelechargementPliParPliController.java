package com.atexo.desktop.domain.tele_pli_par_pli;

import com.atexo.desktop.config.RestTemplateConfig;
import com.atexo.desktop.domain.certificat.exceptions.ErreurTechniqueException;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.TelechargementPliParPliRequestFinish;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.TelechargementPliParPliRequestInit;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.TelechargementPliParPliRequestProcess;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.FichierBloc;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.JaxbReponsesAnnonceUtil;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.ReponseAnnonce;
import com.atexo.desktop.domain.tele_pli_par_pli.dtos.reponse_annonce.ReponseAnnonceConstantes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.MDC;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;


@RequestMapping("/telechargement-plis-par-pli")
@RestController
@Slf4j
public class ApplicationTelechargementPliParPliController {

    private final Map<String, Map<String, ReponseAnnonce>> sessionReponsesAnnonces;
    private final Map<String, StringBuilder> sessionReponsesAnnoncesXml;
    private final RestTemplateConfig restTemplateConfig;

    public ApplicationTelechargementPliParPliController(Map<String, Map<String, ReponseAnnonce>> sessionReponsesAnnonces, Map<String, StringBuilder> sessionReponsesAnnoncesXml, RestTemplateConfig restTemplateConfig) {
        this.sessionReponsesAnnonces = sessionReponsesAnnonces;
        this.sessionReponsesAnnoncesXml = sessionReponsesAnnoncesXml;
        this.restTemplateConfig = restTemplateConfig;
    }

    @PostMapping("{sessionId}/init/{reponseAnnonceId}")
    public Map<Integer, List<FichierBloc>> telechargement(@PathVariable String sessionId, @PathVariable String reponseAnnonceId, @RequestBody TelechargementPliParPliRequestInit request) throws Exception {
        MDC.put("pf", "TELECHARGEMENT_PLI_PAR_PLI");
        MDC.put("org", sessionId);
        MDC.put("utilisateur", reponseAnnonceId);
        log.info("Vérification de l'ensemble des parametres");
        String reponseAnnonceXML;
        try {
            reponseAnnonceXML = getReponseAnnonceXML(sessionId, reponseAnnonceId, request);
        } catch (Exception e) {
            log.error("impossible de recuperer le reponseAnnonceXML", e.fillInStackTrace());
            throw new Exception("impossible de recuperer le reponseAnnonceXML");
        }
        ReponseAnnonce reponseAnnonce = JaxbReponsesAnnonceUtil.getReponseAnnonce(reponseAnnonceXML);
        StringBuilder xml = sessionReponsesAnnoncesXml.get(sessionId);

        if (xml == null) {
            xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?><ReponsesAnnonce>").append(reponseAnnonceXML);
        } else {
            xml.append(reponseAnnonceXML);
        }
        sessionReponsesAnnoncesXml.put(sessionId, xml);

        Set<Integer> idEnveloppes = new HashSet<>();

        log.info("Extraction des blocs à traiter en limitant aux enveloppes ayant comme identifiants : " + Arrays.toString(idEnveloppes.toArray(new Integer[idEnveloppes.size()])));
        Map<Integer, List<FichierBloc>> map = JaxbReponsesAnnonceUtil.extraireFichierBlocChiffrements(reponseAnnonce, idEnveloppes.isEmpty() ? null : idEnveloppes);
        sessionReponsesAnnonces.computeIfAbsent(sessionId, k -> new HashMap<>());
        Map<String, ReponseAnnonce> allResponsesAnnonces = sessionReponsesAnnonces.get(sessionId);
        allResponsesAnnonces.put(reponseAnnonceId, reponseAnnonce);
        sessionReponsesAnnonces.put(reponseAnnonceId, allResponsesAnnonces);
        return map;
    }

    @PostMapping("{sessionId}/process/{reponseAnnonceId}")
    public String process(@PathVariable String sessionId, @PathVariable String reponseAnnonceId, @RequestBody TelechargementPliParPliRequestProcess request) throws Exception {
        MDC.put("pf", "TELECHARGEMENT_PLI_PAR_PLI");
        MDC.put("org", sessionId);
        MDC.put("utilisateur", reponseAnnonceId);
        FichierBloc fichierBloc = request.getFichierBloc();
        log.info("Traitement du fichier  " + fichierBloc.getNomFichier() + " composé de " + fichierBloc.getBlocs().size() + " blocs à déchiffré");
        ReponseAnnonce reponseAnnonce = sessionReponsesAnnonces.get(sessionId).get(reponseAnnonceId);
        String filePath = request.getInitialDirectory();
        File tmpDirectory = new File(filePath);
        tmpDirectory.mkdir();
        for (FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {
            bloc.setNomBloc(bloc.getNomBloc());
            File fichierPart = getFileFromProtocol(reponseAnnonce.getProtocol(), fichierBloc, bloc, tmpDirectory.getAbsolutePath());
            getDownloadedBlocInputStream(sessionId, reponseAnnonce, fichierBloc, bloc, request, fichierPart);
        }
        return "OK";
    }


    @PostMapping("{sessionId}/finish")
    public String createReponseAnnonceIfExist(@PathVariable String sessionId, @RequestBody TelechargementPliParPliRequestFinish request) throws IOException, ErreurTechniqueException {
        MDC.put("pf", "TELECHARGEMENT_PLI_PAR_PLI");
        MDC.put("org", sessionId);
        MDC.remove("utilisateur");
        StringBuilder reponsesAnnoncesXML = sessionReponsesAnnoncesXml.get(sessionId);
        sessionReponsesAnnoncesXml.remove(sessionId);
        sessionReponsesAnnonces.remove(sessionId);
        if (reponsesAnnoncesXML != null) {
            File tmpDirectory = new File(request.getInitialDirectory());
            File fichierReponseAnnonceXML = new File(tmpDirectory, ReponseAnnonceConstantes.NOM_FICHIER_REPONSES_ANNONCE);
            log.info("Ecriture locale du fichier reponse annonce xml : " + fichierReponseAnnonceXML.getAbsolutePath());
            reponsesAnnoncesXML.append("</ReponsesAnnonce>");
            FileUtils.writeStringToFile(fichierReponseAnnonceXML, reponsesAnnoncesXML.toString(), StandardCharsets.ISO_8859_1);
        }
        return "OK";

    }


    private File getFileFromProtocol(String protocol, FichierBloc fichierBloc, FichierBloc.Bloc bloc, String tmpDirectory) {
        File fichierPart;
        if ("CMS".equalsIgnoreCase(protocol)) {
            File dossierArborescence = getDossierArborescence(fichierBloc, tmpDirectory);
            fichierPart = new File(dossierArborescence, fichierBloc.getNomFichier() + ".p7m");
        } else {
            fichierPart = new File(tmpDirectory, bloc.getNomBloc());

        }
        return fichierPart;
    }

    private File getDossierArborescence(FichierBloc fichierBloc, String tmpDirectory) {
        String dossierArborescenceString = fichierBloc.getReferenceUtilisateur() + "/"
                + fichierBloc.getNomOperateurEconomique() + "/" + fichierBloc.getNomEnveloppe() + "/";
        File dossier = new File(tmpDirectory, dossierArborescenceString);
        dossier.mkdirs();
        return dossier;
    }


    private void getDownloadedBlocInputStream(String sessionId, ReponseAnnonce reponseAnnonce, FichierBloc fichierBloc, FichierBloc.Bloc bloc, TelechargementPliParPliRequestProcess request,
                                              File fichierPart) {
        log.info("Téléchargement du bloc ayant comme nom systeme : " + bloc.getIdBloc() + ". Ce bloc est-il chiffré : " + bloc.isChiffre());
        String urlTelechargementComplet = request.getUrlServeurUpload() + "/telechargementBLoc" + "/" + sessionId + "/" + reponseAnnonce.getAnnonce().getOrganisme() + "/" + reponseAnnonce.getAnnonce().getReference() + "/"
                + fichierBloc.getIdFichier() + "/" + bloc.getIdBloc();
        log.info("Url de téléchargement : " + urlTelechargementComplet);
        restTemplateConfig.getRestTemplate(urlTelechargementComplet).execute(urlTelechargementComplet,
                HttpMethod.GET,
                null,
                clientHttpResponse -> {
                    try (FileOutputStream out = new FileOutputStream(fichierPart)) {
                        StreamUtils.copy(clientHttpResponse.getBody(), out);
                    }
                    return fichierPart;
                });
    }

    private String getReponseAnnonceXML(String sessionId, String reponseAnnonceId, TelechargementPliParPliRequestInit request) throws IOException {
        String result = null;
        String url = request.getUrlServeurUpload() + "/reponseAnnonceXML/" + reponseAnnonceId + "/" + sessionId;
        ResponseEntity<Resource> responseEntity =
                restTemplateConfig.getRestTemplate(url).exchange(url, HttpMethod.GET,
                        null, Resource.class);

        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode == HttpStatus.FORBIDDEN)
            throw new IOException("Votre session a expire.");
        else if (statusCode != HttpStatus.OK)
            throw new IOException("Probleme connexion avec serveur " + statusCode);


        InputStream responseInputStream = responseEntity.getBody().getInputStream();
        result = new String(Base64.getDecoder().decode(IOUtils.toByteArray(responseInputStream)));
        result = result.replaceFirst("<\\?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"\\?>", "")
                .replaceFirst("<\\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"\\?>", "");

        return result;
    }
}
