package com.atexo.desktop.domain.dualkeys;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DualKeyFormDTO {
    private String cnCleChiffrement;
    private String password;
    private String alias;
    private boolean estCleChiffrementSecours;
    private boolean existingCert;
    private String certificateId;
    private String mpeUrl;
    private String caUrl;
    private String sessionId;
}
