package com.atexo.desktop.domain.dechiffrement.dtos;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * Clef stockant le certificat / la clef privé / le type de provider.
 */
public class KeyPair {

    private X509Certificate certificate = null;
    private PrivateKey privateKey = null;
    private TypeProviderEnum provider;
    private String nomProvider;

    public KeyPair(X509Certificate certificate, PrivateKey privateKey, TypeProviderEnum provider) {
        this.certificate = certificate;
        this.privateKey = privateKey;
        this.provider = provider;
        this.nomProvider = provider.getNom();
    }

    public KeyPair(X509Certificate certificate, PrivateKey privateKey, TypeProviderEnum provider, String nomProvider) {
        this.certificate = certificate;
        this.privateKey = privateKey;
        this.provider = provider;
        this.nomProvider = nomProvider;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public TypeProviderEnum getProvider() {
        return provider;
    }

    public String getNomProvider() {
        return nomProvider;
    }

    public void setNomProvider(String nomProvider) {
        this.nomProvider = nomProvider;
    }
}
