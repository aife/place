package com.atexo.desktop.domain.certificat.enums;

/**
 * Le type d'os
 */
public enum TypeOsEnum {
    Windows("6.1"), MacOs("10.6"), Linux("16"), Indetermine("0");
    private final String version;

    TypeOsEnum(String version) {
        this.version = version;

    }

    public String getVersion() {
        return version;
    }

    public static TypeOsEnum get(String nom) {

        for (TypeOsEnum typeOsEnum : values()) {
            if (typeOsEnum.name().equals(nom)) {
                return typeOsEnum;
            }
        }

        return Indetermine;
    }
}
