package com.atexo.desktop.domain.certificat.exceptions;


/**
 * Exception pour signaler un souci lors de la récupération d'un certificat.
 */
public class RecuperationCertificatException extends ManipulationCertificatException {

    public RecuperationCertificatException(String message) {
        super(message);
    }

    public RecuperationCertificatException(String message, Throwable cause) {
        super(message, cause);
    }
}

