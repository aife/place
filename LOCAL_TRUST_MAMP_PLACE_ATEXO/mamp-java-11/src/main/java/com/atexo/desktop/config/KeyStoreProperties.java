package com.atexo.desktop.config;

import com.atexo.desktop.config.propertiesDTO.LinuxKeyStoreProps;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "keystore")
@Getter
@Setter
public class KeyStoreProperties {
    private LinuxKeyStoreProps linux;
}
