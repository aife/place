package eu.europa.esig.dss.token;

import com.atexo.desktop.domain.certificat.enums.TypeProviderEnum;
import eu.europa.esig.dss.model.DSSException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
@Slf4j
public class AppleSignatureToken extends AbstractKeyStoreTokenConnection {

    public AppleSignatureToken() {
    }


    KeyStore getKeyStore() throws DSSException {
        KeyStore keyStore = null;

        try {
            keyStore = KeyStore.getInstance(TypeProviderEnum.APPLE.getType(), TypeProviderEnum.APPLE.getNom());
            keyStore.load(null, null);
            return keyStore;
        } catch (GeneralSecurityException | IOException e) {
            log.error("Unable to load Apple Store keystore");
            throw new DSSException("Unable to load Apple Store keystore", e);
        }
    }

    @Override
    KeyStore.PasswordProtection getKeyProtectionParameter() {
        return null;
    }


    public void close() {
    }
}
