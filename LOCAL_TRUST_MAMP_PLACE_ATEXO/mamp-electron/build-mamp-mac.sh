mvn clean deploy -DskipTests -P env_place_dev,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_rec,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_ecole,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_pre_prod,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_prod,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_securisation,system_mac,place || true
mvn clean deploy -DskipTests -P env_place_test,system_mac,place || true

