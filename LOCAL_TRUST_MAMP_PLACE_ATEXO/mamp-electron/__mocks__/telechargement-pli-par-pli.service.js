const data = require('./telechargement-pli-par-pli-data/getEnveloppesPerPli.json')

export const mockGetEnveloppesPerPli = jest.fn().mockImplementation(
  function() {
    if (this.getEnveloppesPerPli.mock.calls.length < 3) return new Promise(resolve => resolve({data}))
    else return new Promise((resolve, reject) => reject('Test error'))
  })
export const mockProcessTelechargementFichier = jest.fn().mockImplementation(
  function() {
    if (this.processTelechargementFichier.mock.calls.length < 8) return new Promise(resolve => resolve({data}))
    else return new Promise((resolve, reject) => reject('Test error'))
  })

export const mockFinish = jest.fn().mockResolvedValueOnce().mockRejectedValueOnce().mockResolvedValue()

const mock = jest.fn().mockImplementation(() => {
  return {
    getEnveloppesPerPli: mockGetEnveloppesPerPli,
    processTelechargementFichier: mockProcessTelechargementFichier,
    finish: mockFinish
  };
});

export default mock;