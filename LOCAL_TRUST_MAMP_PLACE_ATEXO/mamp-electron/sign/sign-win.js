exports.default = async function (configuration) {
    sign("preprod");
    sign("securisation");
    sign("ecole");
    sign("rec");
    sign("dev");
    sign("test");
    sign(null);
};

function sign(env) {
    let FILE_TO_SIGN = "./dist/" + require("../package.json").name;
    if (!!env) FILE_TO_SIGN += "-" + env;
    FILE_TO_SIGN += ".exe";
    const envBuild = "linux";
    const KEYSTORE_NAME = "./sign/eToken-" + envBuild + ".cfg";
    const KEYSTORE_PASSWORD = "54444noisy";
    const KEYSTORE_ALIAS = "te-a273d40a-622e-4764-9e60-653966cc743d";

    if (require("fs").existsSync(FILE_TO_SIGN)) {
        require("child_process").execSync(
            `java -jar ./sign/jsign.jar '${FILE_TO_SIGN}' --keystore ${KEYSTORE_NAME} --storepass ${KEYSTORE_PASSWORD} --storetype PKCS11 --tsaurl http://timestamp.digicert.com --alias "${KEYSTORE_ALIAS}"`,
            {
                stdio: "inherit",
            }
        );
        return true;
    }
    return false;
}
