exports.default = async function (configuration) {
    // do not include passwords or other sensitive data in the file
    // rather create environment variables with sensitive data
    sign("8");
    sign("11");

};

function sign(version) {
    let FILE_TO_SIGN = `../src/assets/jar/application-externe-${version}.jar`;
    const KEYSTORE_NAME = "./sign/eToken-linux.cfg";
    const KEYSTORE_PASSWORD = "54444noisy";
    const KEYSTORE_ALIAS = "te-a273d40a-622e-4764-9e60-653966cc743d";
    if (require("fs").existsSync(FILE_TO_SIGN)) {
        require("child_process").execSync(
            `java -jar ./sign/jsign.jar '${FILE_TO_SIGN}' --keystore ${KEYSTORE_NAME} --storepass ${KEYSTORE_PASSWORD} --storetype PKCS11 --tsaurl http://timestamp.digicert.com --alias "${KEYSTORE_ALIAS}"`,
            {
                stdio: "inherit",
            }
        );
    }
}
