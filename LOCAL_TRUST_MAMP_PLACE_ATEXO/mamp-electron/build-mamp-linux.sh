mvn clean deploy -DskipTests -P env_place_dev,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_rec,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_ecole,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_pre_prod,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_prod,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_securisation,system_linux,place || true
mvn clean deploy -DskipTests -P env_place_test,system_linux,place || true

