const fs = require('fs');
const path = require('path');
const dist = './dist/';

const args = process.argv.slice(2); // Extract command-line arguments excluding 'node' and the script file name
const system = args[0];
const env = args[1];
let monAssistantMarchePublic = 'mon-assistant-marche-public';
let installeur = monAssistantMarchePublic;

let confFiles = []
let installeursFiles = []
switch (env) {
    case 'rec':
        installeur += '-rec';
        break;
    case 'int':
        installeur += '-int';
        break;
}
switch (system) {
    case 'win':
        installeursFiles.push(installeur + ".exe");
        installeursFiles.push(installeur + ".exe.blockmap");
        confFiles.push("latest.yml");
        break;
    case 'mac':
        installeursFiles.push(installeur + ".dmg");
        confFiles.push("latest-mac.yml");
        break;
    case 'linux':
        installeursFiles.push(installeur + ".AppImage", installeur + ".deb");
        confFiles.push("latest-linux.yml");
}

// Print the arguments
console.log("env:", env);
console.log("system:", system);
console.log("confFiles :", confFiles);
console.log("installeursFiles :", confFiles);
deleteAllFilesAndFoldersFromDistIfNotContainsInList = async () => {

    fs.readdir(dist, (err, files) => {
        if (err) throw err;
        for (const file of files) {
            if (!confFiles.includes(file) && !installeursFiles.includes(file)) {
                let path = dist + file;
                fs.stat(path, (err, stats) => {
                    if (err) {
                        console.log('Path does not exist.');
                    } else {
                        if (stats.isDirectory()) {
                            fs.rmdirSync(path, {recursive: true});
                        } else if (stats.isFile()) {
                            fs.unlink(path, err => {
                                if (err) throw err;
                                console.log("Given file " + file + " is deleted");
                            });
                        } else {
                            console.log('Path is neither a file nor a folder.');
                        }
                    }
                });


            }
        }
    });

}
deleteAllFilesAndFoldersFromDistIfNotContainsInList();
