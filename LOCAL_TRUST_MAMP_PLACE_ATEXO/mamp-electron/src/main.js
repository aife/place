function guid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
        let r = (Math.random() * 16) | 0, v = c === "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}

// Modules to control application life and create native browser window
const {
    app,
    dialog,
    BrowserWindow,
    Tray,
    Menu,
    Notification,
} = require("electron");
const path = require("path");
const axios = require("axios");
let pjson = require("../package.json");
process.env.environnement = pjson.env;
process.env.ressourcesUrl = pjson.updateServer;
process.env.version = pjson.version;
var AutoLaunch = require("auto-launch");
const {ipcMain, globalShortcut} = require("electron");
let isQuiting;
let tray;
let {userStore, userParam} = require("./app/store/user.store");
const killPort = require("kill-port");
let deeplinkingUrl;
const log = require("electron-log");
const {autoUpdater} = require("electron-updater");
require('@electron/remote/main').initialize();
if (!userStore.get(userParam.uuid) || userStore.get(userParam.uuid) === 'undefined' || userStore.get(userParam.uuid) === 'null') {
    userStore.set(userParam.uuid, guid())
}
process.env.uuid = userStore.get(userParam.uuid)

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = "info";
log.info("App starting...");
//const isDev = import("electron-is-dev");
const isDev = pjson.env === "local";
const os = require("os");
process.env.username = os.userInfo().username
var mainWindow;
var ready = false;
var launchWindow = "src/index.html";
let autoLaunch = new AutoLaunch({
    name: pjson.name,
    path: app.getPath("exe"),
});
let canDownload = true;
let hidden = false;
let downloadProgress = false;
let showMessageBox = false;


function checkIfUserUpdateEnabled() {
    if (downloadProgress) {
        sendStatusToWindow("Download in progress...")
        return;
    }
    if (showMessageBox) {
        sendStatusToWindow("La boite de dialogue est affichée...")
        return;
    }
    autoUpdater.checkForUpdates();

}


function createWindow(windowPath) {
    const windowOptions = {
        width: 1366,
        height: 768,
        show: false,
        title: pjson.description + " (" + pjson.version + ")",
        icon: path.join(__dirname, "assets/app-icon/png/128.png"),
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
            nodeIntegration: true, contextIsolation: false, enableRemoteModule: true,
        },
    };
    // Create the browser window.
    mainWindow = new BrowserWindow(windowOptions);
    require('@electron/remote/main').enable(mainWindow.webContents)

    // and load the index.html of the app.
    mainWindow.loadFile(windowPath);
    mainWindow.on("closed", function () {
        mainWindow = null;
    });

    mainWindow.webContents.on("did-finish-load", function () {
        if (!isDev) {
            checkIfUserUpdateEnabled();
            setInterval(() => {
                checkIfUserUpdateEnabled()
            }, 60 * 60 * 1000)
        }
    });
    mainWindow.on("close", function (event) {
        if (!isQuiting) {
            event.preventDefault();
            mainWindow.hide();
            event.returnValue = false;
        }
    });
    mainWindow.removeMenu();
    ready = true;
    // Protocol handler for win32
    if (process.platform !== 'darwin') {
        // Keep only command line / deep linked arguments
        deeplinkingUrl = process.argv.slice(1)
    }
    sendStatusToWindow('createWindow# ' + deeplinkingUrl);
    runApplication(deeplinkingUrl)
    mainWindow.on('unresponsive', () => {
        if (!isQuiting) {
            mainWindow.hide();
        } else {
            isQuiting = true;
            killServers();
            app.quit();
        }
    });
    mainWindow.on('hide', () => {
        hidden = true;
    });
    mainWindow.on('show', () => {
        hidden = false;
    });
}

async function killServers() {

    try {
        const response = await axios.get("http://localhost:11992/user", {timeout: 2000});
        const os = require('os');
        const username = os.userInfo().username;
        if (response && response.data.username === username && pjson.env === response.data.env) {
            await killPort(11992, "tcp");
            await killPort(11973, "tcp");
        }
    } catch (e) {
        log.error("Erreur lors de l'arret des serveurs locaux", e);
        return false;
    }
    return true;
}

app.whenReady().then(() => {
    /** Check if single instance, if not, simply quit new instance */
    let isSingleInstance = app.requestSingleInstanceLock();
    log.info("App whenReady...");
    if (!isSingleInstance) {
        let message = "L'application est déjà lancée";

        const notification = {
            title: pjson.description,
            body: message,
            icon: path.join(__dirname, "./assets/app-icon/png/128.png"),
        };
        new Notification(notification).show();
        app.quit();
    } else {
        log.info("App killServers...");
        killServers().then(() => {
            log.info("App then...");
            createWindow(launchWindow);
            sendStatusToWindow("URL DOWNLOAD : " + pjson.updateServer);
            mainWindow.maximize();
            mainWindow.show();
            mainWindow.setAlwaysOnTop(true);
            mainWindow.setAlwaysOnTop(false);
            app.on("activate", function () {
                // On macOS it's common to re-create a window in the app when the
                // dock icon is clicked and there are no other windows open.
                if (BrowserWindow.getAllWindows().length === 0)
                    createWindow(launchWindow);
            });

            try {
                switchAutoStart(userStore.get(userParam.autoStart));
            } catch (e) {
                log.error("Erreur lors du paramétrage de l'auto-lancement")
            }
            let message = "L'application est lancée";

            const notification = {
                title: pjson.description,
                body: message,
                icon: path.join(__dirname, "./assets/app-icon/png/128.png"),
            };
            const myNotification = new Notification(notification);
            myNotification.onclick = () => {
                mainWindow.show();
            };
            myNotification.show();

        }, (err) => log.info("App then...", err));


    }
    try {
        globalShortcut.register("Alt+Shift+I", () => {
            if (mainWindow) mainWindow.webContents.openDevTools();
        });
    } catch (e) {
        log.error("Erreur lors du paramétrage du raccourcis")
    }

});

function addTray() {
    app.setAppUserModelId(pjson.description);

    let template = [
        {
            label: pjson.description,
            click: function () {
                mainWindow.show();
            },
        },
        {
            type: "separator",
        },
        {
            type: "checkbox",
            label: "Lancer au démarrage du poste",
            checked: userStore.get(userParam.autoStart),
            click: function () {
                userStore.set(userParam.autoStart, !userStore.get(userParam.autoStart));
                switchAutoStart(userStore.get(userParam.autoStart));
            },
        },
        {
            type: "separator",
        },
        {
            label: "Quitter",
            click: function () {
                isQuiting = true;
                killServers();
                app.quit();
            },
        },
    ];
    tray = new Tray(path.join(__dirname, "assets/app-icon/tray/tray-circle.png"));
    tray.setToolTip(pjson.description);
    tray.setContextMenu(Menu.buildFromTemplate(template));
}

app.on("ready", () => {
    try {
        let isSingleInstance = app.requestSingleInstanceLock();
        if (isSingleInstance) {
            addTray();
        }
    } catch (e) {
        log.error("Erreur lors de l'ajout de l'icone raccourcis")
    }
});

app.on("before-quit", function () {
    isQuiting = true;
});

// Behaviour on second instance for parent process- Pretty much optional
app.on("second-instance", (event, argv, cwd) => {
    if (process.platform !== 'darwin') {
        // Keep only command line / deep linked arguments
        deeplinkingUrl = argv.slice(1)
    }
    sendStatusToWindow('app.makeSingleInstance# ' + deeplinkingUrl);
    runApplication(deeplinkingUrl);
    if (mainWindow) {
        if (mainWindow.isMinimized()) {
            mainWindow.restore();
        }
        mainWindow.maximize();
        mainWindow.show();
        mainWindow.setAlwaysOnTop(true);
        mainWindow.setAlwaysOnTop(false);
    }
});
let protocol = 'mamp';
if (pjson.env !== 'prod') {
    protocol += pjson.env;
}
if (!app.isDefaultProtocolClient(protocol)) {
    // Define custom protocol handler. Deep linking works on packaged versions of the application!
    app.setAsDefaultProtocolClient(protocol)
}

app.on('will-finish-launching', function () {
    // Protocol handler for osx
    app.on('open-url', function (event, url) {
        event.preventDefault()
        deeplinkingUrl = url
        sendStatusToWindow('open-url# ' + deeplinkingUrl)
        runApplication(deeplinkingUrl)

    })
})


function runApplication(deeplinkingUrl) {
    if (mainWindow)
        mainWindow.webContents.send("run-application", deeplinkingUrl);
}


function switchAutoStart(isAutoStartEnabled) {
    isAutoStartEnabled ? autoLaunch.enable() : autoLaunch.disable();
}

const updateChannel = {
    url: process.env.ressourcesUrl,
    provider: "generic",
};

autoUpdater.setFeedURL(updateChannel);
autoUpdater.autoInstallOnAppQuit = false;
autoUpdater.autoDownload = false;
autoUpdater.allowDowngrade = true;

function sendStatusToWindow(text) {
    log.info("message", text);
    if (mainWindow)
        mainWindow.webContents.send("message", text);
}

let releaseDate;
autoUpdater.on("checking-for-update", () => {
    sendStatusToWindow("Checking for update...");
});
autoUpdater.on("update-available", (ev, info) => {
    if (showMessageBox || downloadProgress) {
        return;
    }
    let message = "La nouvelle version " + ev.version + " est disponible.";
    console.log(ev, info)
    const dialogOpts = {
        type: 'info',
        buttons: ['Télécharger', 'Reporter'],
        title: 'Mise à jour de l\'application',
        message: message
    }
    showMessageBox = true;
    dialog.showMessageBox(dialogOpts).then((returnValue) => {
        if (returnValue.response === 0) {
            releaseDate = ev.releaseDate;
            autoUpdater.downloadUpdate();
            downloadProgress = true;
        } else {
            showMessageBox = false;
        }

    })
});
autoUpdater.on("update-not-available", (ev, info) => {
    console.log(ev, info)
    downloadProgress = false;
    sendStatusToWindow("Update not available.");
    if (!userStore.get(userParam.releaseDate)) {
        userStore.set(userParam.releaseDate, ev.releaseDate)
    } else {
        let savedDateRelease = new Date(userStore.get(userParam.releaseDate));
        let newDateRelease = new Date(ev.releaseDate);
        if (savedDateRelease.getTime() < newDateRelease.getTime()) {
            userStore.set(userParam.releaseDate, ev.releaseDate);
            process.env.releaseDate = ev.releaseDate;
        }
    }
});
autoUpdater.on("error", (ev, err) => {
    sendStatusToWindow("Error in auto-updater. " + JSON.stringify(err));
    downloadProgress = false;
});
autoUpdater.on("download-progress", (ev, progressObj) => {
    sendStatusToWindow("Download progress... : " + ev.percent + "%");
    downloadProgress = true;
});
autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
    downloadProgress = false
    console.log(event, releaseName, releaseNotes)
    const dialogOpts = {
        type: 'info',
        buttons: ['Redémarrer'],
        title: 'Mise à jour de l\'application',
        message: process.platform === 'win32' ? releaseNotes : releaseName,
        detail:
            'Une nouvelle version a été téléchargée. Redémarrez l\'application pour appliquer les mises à jour.',
    }
    showMessageBox = true;
    dialog.showMessageBox(dialogOpts).then((returnValue) => {
        if (returnValue.response === 0) {
            userStore.set(userParam.releaseDate, releaseDate);
            autoUpdater.quitAndInstall();
        } else {
            showMessageBox = false;
        }
    })
})

autoUpdater.on('error', (message) => {
    sendStatusToWindow('There was a problem updating the application')
    sendStatusToWindow(message)
})
//hold the array of directory paths selected by user

let dir;

ipcMain.on("selectDirectory", function () {
    dir = dialog
        .showOpenDialog(mainWindow, {
            properties: ["openDirectory"],
        })
        .then((response) => {
            if (!!response && !response.canceled) {
                mainWindow.webContents.send(
                    "close-folder-dialog",
                    response.filePaths[0]
                );
            }
        });
});
