const fr = require("./assets/i18n/fr.json");

const messages = {
  fr: fr,
};

const i18n = new VueI18n({
  locale: "fr",
  messages,
});

export default i18n;
