import AtxHomePage from "./app/pages/home.page.js";
import AtxConfigurationPage from "./app/pages/configuration.page.js";
import AtxOuvertureEnLignePage from "./app/pages/ouverture-en-ligne/ouverture-en-ligne.page.js";
import AtxTelechargementPliParPliPage from "./app/pages/telechargement-pli-par-pli/telechargement-pli-par-pli.page.js";
import AtxDualKeyPage from "./app/pages/dual-key/dual-key.page.js";
import AtxParameterPage from "./app/pages/parameter.page.js";
import AtxTestConfigurationPage from "./app/pages/test-configuration/test-configuration.page.js";

const router = new VueRouter({
    routes: [
        {
            path: "/",
            redirect: {name: "atx-home"},
        },
        {
            path: "/atx-home",
            name: "atx-home",
            component: AtxHomePage,
        }, {
            path: "/atx-configuration",
            name: "atx-configuration",
            component: AtxConfigurationPage,
        },
        {
            path: "/atx-parameter",
            name: "atx-parameter",
            component: AtxParameterPage,
        },
        {
            path: "/atx-ouverture-en-ligne/:sessionId",
            name: "atx-ouverture-en-ligne",
            component: AtxOuvertureEnLignePage,
        },
        {
            path: "/atx-telechargement-pli-par-pli/:sessionId",
            name: "atx-telechargement-pli-par-pli",
            component: AtxTelechargementPliParPliPage,
        },
        {
            path: "/atx-dual-key/:sessionId",
            name: "atx-dual-key",
            component: AtxDualKeyPage,
        },
        {
            path: "/atx-test-configuration",
            name: "atx-test-configuration",
            component: AtxTestConfigurationPage,
        }
    ],
    scrollBehavior() {
        return {x: 0, y: 0};
    },
});

export default router;
