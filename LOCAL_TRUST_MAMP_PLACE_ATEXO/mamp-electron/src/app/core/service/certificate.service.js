const axios = require("axios");
const LOCAL_SERVER_URL = "http://localhost:11973/";
const CERTIFICATE_URL = "certificat/";
const DUAL_KEYS_URL = "dualKeys/";

class CertificateService {
  getSignatureCertificatesList() {
    return axios
      .get(LOCAL_SERVER_URL + CERTIFICATE_URL + "signatureCertificats")
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }

  getAllCertificatesList() {
    return axios
      .get(LOCAL_SERVER_URL + CERTIFICATE_URL + "chiffrementCertificats")
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }

  getProviderName() {
    return axios
      .get(LOCAL_SERVER_URL + CERTIFICATE_URL + "provider")
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }

  postDualKeys(certificateId, mpeUrl, caUrl, sessionId, formData) {
    return axios.post(
      LOCAL_SERVER_URL + DUAL_KEYS_URL + "generate",
      {
        cnCleChiffrement: formData.cnCleChiffrement,
        password: formData.password,
        alias: formData.alias,
        estCleChiffrementSecours: formData.estCleChiffrementSecours,
        existingCert: formData.existingCert,
        certificateId: certificateId,
        mpeUrl: mpeUrl,
        caUrl: caUrl,
        sessionId: sessionId,
      },
      null
    );
  }
}

export default new CertificateService();
