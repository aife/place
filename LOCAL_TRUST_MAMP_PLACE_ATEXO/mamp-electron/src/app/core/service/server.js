/*------------------------- Server --------------------------*/
const axios = require("axios");
const pjson = require("../../../../package.json");

const AppId = require("../enum/app-id.enum").AppId;
var url = require("url");
const {BrowserWindow} = require('@electron/remote');
let port = 11992;
const swal = require("sweetalert2");


require("http").createServer(onRequest).listen(port);

function onRequest(request, response) {
    if (
        !request.headers.host.startsWith("localhost:") &&
        !request.headers.host.startsWith("mon-assistant.local-trust.com:")
    ) {
        response.writeHead(403, {"Content-Type": "text/plain"});
        response.write("Accès de " + request.headers.host + " est interdit");
        response.end();
        return;
    }
    const pathname = url.parse(request.url).pathname;
    setCorsResponse(response);
    switch (request.method.toUpperCase()) {
        case "GET":
            handleGetRequest(request, response);
            break;
        case "OPTIONS":
            handleOptionsRequest(response);
            break;
        case "POST":
            handlePostRequest(pathname, response, request);
            break;
    }
}

async function handleGetRequest(request, response) {
    let pathname = url.parse(request.url).pathname;
    if (pathname === "/health") {
        await axios
            .get("http://localhost:11973/monitoring/health")
            .then(() => {
                response.writeHead(200, {"Content-Type": "text/plain"});
                response.write("OK");
                response.end();
            })
            .catch(() => {
                response.writeHead(404, {"Content-Type": "text/plain"});
                response.write(pathname + " Not Found");
                response.end();
            });
    } else if (pathname === "/proxy") {
        let mainWindow = BrowserWindow.getAllWindows()[0];
        const ses = mainWindow.webContents.session;
        let search = url.parse(request.url).search;
        if (!search) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write("URL est obligatoire");
            response.end();
        } else if (!search.startsWith("?url=")) {
            response.writeHead(500, {"Content-Type": "text/plain"});
            response.write("URL n'est pas conforme");
            response.end();
        } else {
            ses.resolveProxy(search.replace('?url=', '')).then((proxy) => {
                // do whatever you want with proxy string, that contains proxy-setting
                response.writeHead(200, {"Content-Type": "text/plain"});
                response.write(proxy);
                response.end();
            });
        }

    } else if (pathname === "/user") {
        const os = require('os');
        const username = os.userInfo();
        username.env = pjson.env
        response.writeHead(200, {"Content-Type": "application/json"});
        response.write(JSON.stringify(username));
        response.end();
    } else if (pathname === "/dialog") {
        let query = url.parse(request.url, true).query;
        swal
            .fire({
                title: query.title,
                input: query.input,
                inputLabel: query.inputLabel,
                showCancelButton: query.cancelButton === "true",
                backdrop: query.backdrop === "true"
            })
            .then((result) => {
                response.writeHead(200, {"Content-Type": "application/json"});
                response.write(JSON.stringify(result));
                response.end();
            });
    } else {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write(pathname + " Not Found");
        response.end();
    }
}

function runApplication(data, request, response) {
    BrowserWindow.getAllWindows()[0].show();

    BrowserWindow.getAllWindows()[0].setAlwaysOnTop(true);
    BrowserWindow.getAllWindows()[0].setAlwaysOnTop(false);
    const appParam = AppId[data.appId];
    window.dispatchEvent(
        new CustomEvent("run-local-application", {
            detail: {appParam, data, headers: request.headers},
        })
    );
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("OK");
    response.end();
}

function handlePostRequest(pathname, response, request) {
    var bodyString = "";
    request.on("data", function (data) {
        bodyString += data;
    });
    request.on("end", async function () {
        const body = JSON.parse(bodyString);
        const data = body.data ? body.data : body;
        if (pathname === "/run-local-application") {
            if (data) {
                axios.post("http://localhost:11973/monitoring/navigateur", request.headers)
                    .then(() => {
                        runApplication(data, request, response);
                    }, () => {
                        runApplication(data, request, response);
                    })


            } else {
                response.writeHead(500, {"Content-Type": "text/plain"});
                response.write("Aucun parametre est envoyé");
                response.end();
            }
        } else {
            response.writeHead(404, {"Content-Type": "text/plain"});
            response.write(pathname + " Not Found");
            response.end();
        }
    });
}

function handleOptionsRequest(response) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("OK");
    response.end();
}

function setCorsResponse(response) {
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Request-Method", "*");
    response.setHeader("Access-Control-Allow-Methods", "*");
    response.setHeader(
        "Access-Control-Allow-Headers",
        "host,accept,authorization,cache-control,pragma,access-control-allow-credentials,content-type,x-http-method-override"
    );
}

console.log("Server has started.");
