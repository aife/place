const axios = require("axios");
const LOCAL_SERVER_URL = "http://localhost:11973/monitoring/";
const LOCAL_VUE_SERVER_URL = "http://localhost:11992/";

class MonitoringService {
    health() {
        return axios
            .get(LOCAL_SERVER_URL + "health")
            .then(() => {
                return "OK";
            })
            .catch(() => {
                return null;
            });
    }

    session() {
        return axios
            .get(LOCAL_VUE_SERVER_URL + "user")
            .then((data) => {
                return data;
            })
            .catch(() => {
                return null;
            });
    }
}

export default new MonitoringService();
