const axios = require("axios");
const LOCAL_SERVER_URL = "http://localhost:11973/configuration/";

class SystemConfigurationService {
    getConfiguration(key, urlPubliqueServeurCrypto, browserName, browserVersion) {
        return axios
            .get(LOCAL_SERVER_URL + key, {
                params: {
                    serverUrl: urlPubliqueServeurCrypto,
                    browserName,
                    browserVersion,
                },
            })
            .then((response) => {
                return response.data;
            })
            .catch((reason) => {
                return null;
            });
    }

    getVersion(ressourceUrl, env, system) {
        let url = `${ressourceUrl}`
        url += '/latest'
        switch (system) {
            case 'darwin':
                url += '-mac'
                break;
            case 'linux':
                url += '-linux'
                break;

        }
        return axios
            .get(LOCAL_SERVER_URL + 'version', {
                timeout: 5000,
                params: {
                    ressourcesUrl: url + '.yml'
                },
            })
            .then(response => {
                return response.data;
            });
    }
}

export default new SystemConfigurationService();
