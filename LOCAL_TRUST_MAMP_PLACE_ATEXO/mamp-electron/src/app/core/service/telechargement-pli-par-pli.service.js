
const axios = require("axios");
const LOCAL_SERVER_URL = "http://localhost:11973/telechargement-plis-par-pli";

export default class TelechargementPliParPliService {
    
  async getEnveloppesPerPli(urlServeurUpload, sessionId, indexPli) {
    return axios.post(`${LOCAL_SERVER_URL}/${sessionId}/init/${indexPli}`, {
      urlServeurUpload,
    });
  }

  async processTelechargementFichier(
    urlServeurUpload,
    initialDirectory,
    sessionId,
    indexPli,
    fichierBloc
  ) {
    const payload = {
      fichierBloc,
      initialDirectory,
      urlServeurUpload,
    };
    return axios.post(
      `${LOCAL_SERVER_URL}/${sessionId}/process/${indexPli}`,
      payload
    );
  }

  // atx_todo: better name
  async finish(sessionId, initialDirectory) {
    const payload = {
      initialDirectory,
    };
    return axios.post(`${LOCAL_SERVER_URL}/${sessionId}/finish`, payload);
  }
}
