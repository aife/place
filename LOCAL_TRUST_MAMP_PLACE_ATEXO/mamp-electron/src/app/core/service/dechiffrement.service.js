const axios = require("axios");
const LOCAL_SERVER_URL = "http://localhost:11973/dechiffrement/";

class DechiffrementService {
  getOffres(sessionId, urlServeurUpload) {
    return axios
      .get(LOCAL_SERVER_URL + "offres/" + sessionId, {
        params: {
          urlServeurUpload,
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }

  getIdOffreQueue(offres, sessionId, urlServeurUpload) {
    return axios
      .post(LOCAL_SERVER_URL + "offres/cle-privee/" + sessionId, offres, {
        params: { urlServeurUpload },
      })
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }

  getEnveloppesProgress(idOffreQueue, urlServeurUpload) {
    return axios
      .post(LOCAL_SERVER_URL + "enveloppes/progress", idOffreQueue, {
        params: { urlServeurUpload },
      })
      .then((response) => {
        return response.data;
      })
      .catch((reason) => {
        return null;
      });
  }
}

export default new DechiffrementService();
