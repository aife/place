export const AppId = {
    OUVERTURE_EN_LIGNE: {
        run: "atx-ouverture-en-ligne",
        isActiveItem: true,
        title: "main-application.items.dechiffrement",
        icon: "lock_open",
    },
    APPLICATION_TEST_CONFIGURATION: {
        run: "atx-test-configuration",
        isActiveItem: true,
        title: "main-application.items.test",
        icon: "desktop_windows",
    },
    TELECHARGEMENT_PLI_PAR_PLI: {
        run: "atx-telechargement-pli-par-pli",
        isActiveItem: true,
        title: "main-application.items.plis",
        icon: "system_update_alt",
    },
    DUAL_KEY: {
        run: "atx-dual-key",
        isActiveItem: true,
        title: "main-application.items.dual-key",
        icon: "vpn_key",
    }
};
