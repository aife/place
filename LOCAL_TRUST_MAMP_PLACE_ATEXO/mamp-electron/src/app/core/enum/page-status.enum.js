export const TELECHARGEMENT_STATUS = {
  PENDING: "pending",
  LOADING: "loading",
  FAILED: "failed",
  SUCCESSFUL: "successful",
  CLOSED: "closed",
};

export const TELECHARGEMENT_STATUS_PARAMS = {
  PENDING: {
    type: "more_horiz",
    color: "#AAAAAA",
  },
  FAILED: {
    type: "priority_high",
    color: "deep-orange",
  },
  SUCCESSFUL: {
    type: "check",
    color: "green",
  },
  DEFAULT: {
    type: "",
    color: "",
  },
};
