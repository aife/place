const pathToRoot = "../../../../";
const pjson = require(pathToRoot + "package.json");
const isDev = pjson.env === "local";

const path = require("path");
const os = require("os");
const {createConfigurationFolderIfNotExist} = require("./folder.utils");
exports.getDefaultLogPath = function () {
    return getAppDataPath("logs");
};
exports.getDefaultUserPath = function () {
    return getAppDataPath("user");
};
exports.getDefaultApplicationPath = function () {
    return getAppDataPath("application");
};

exports.getDefaultDownloadPath = function () {
    return getAppDataPath("application");
};

exports.getResourcesPath = function () {
    if (isDev) {
        switch (process.platform) {
            case "darwin":
                return path.join(
                    __dirname,
                    pathToRoot +
                    "dist/mac/" +
                    pjson.description +
                    ".app/Contents/Resources"
                );
            case "linux":
                return path.join(
                    __dirname,
                    pathToRoot + "dist/linux-unpacked/resources"
                );
            case "win32":
                return path.join(
                    __dirname,
                    pathToRoot + "/dist/win-ia32-unpacked/resources"
                );
        }
    }
    return process.resourcesPath;
};

exports.getSpringProfiles = function () {
    switch (process.platform) {
        case "darwin":
            return "mac";
        case "linux":
            return "linux";
        default:
            return "win";
    }
};

exports.getDefaultJavaExec = function (useSystem) {
    if (isDev && !useSystem) {
        switch (process.platform) {
            case "darwin":
                return path.join(
                    __dirname,
                    pathToRoot +
                    "dist/mac/" +
                    pjson.description +
                    ".app/Contents/Resources/launch",
                    "java-runtime",
                    "bin",
                    "java"
                );
            case "linux":
                return path.join(
                    __dirname,
                    pathToRoot + "dist/linux-unpacked/resources/launch",
                    "java-runtime",
                    "bin",
                    "java"
                );
            case "win32":
                return path.join(
                    __dirname,
                    pathToRoot + "dist/win-ia32-unpacked/resources/launch",
                    "java-runtime",
                    "bin",
                    "javaw"
                );
        }
    }
    return useSystem ? 'java' : path.join(
        process.resourcesPath,
        "launch",
        "java-runtime",
        "bin",
        "java"
    );

};


const getAppDataPath = function (folder) {
    var result = "";
    if (isDev) {
        switch (process.platform) {
            case "darwin":
                result = path.join(
                    __dirname,
                    pathToRoot +
                    "dist/mac/" +
                    pjson.description +
                    ".app/Contents/Resources",
                    folder
                );
                break;
            case "linux":
                result = path.join(
                    __dirname,
                    pathToRoot + "dist/linux-unpacked/resources",
                    folder
                );
                break;
            case "win32":
                result = path.join(
                    __dirname,
                    pathToRoot + "dist/win-ia32-unpacked/resources",
                    folder
                );
                break;
        }
    } else {
        switch (process.platform) {
            case "darwin": {
                result = path.join(
                    process.env.HOME,
                    "Library",
                    "Application Support",
                    pjson.description,
                    folder
                );
                break;
            }
            case "win32": {
                result = path.join(process.env.APPDATA, pjson.name, folder);
                break;
            }
            case "linux": {
                result = path.join(process.env.HOME, "." + pjson.name, folder);
                break;
            }
            default: {
                result = path.join(os.tmpdir(), pjson.name, folder);
            }
        }
    }
    createConfigurationFolderIfNotExist(result);
    return result;
};


exports.javaVersion = function (useSystem) {
    return new Promise(resolve => {
        try {
            let interval = setTimeout(() => resolve(null), 5000)
            const command = this.getDefaultJavaExec(useSystem);
            const spawn = require('child_process').spawn(command, ['-version']);
            spawn.on('error', function (err) {
                clearTimeout(interval)
                resolve(null);
            })
            spawn.stderr.on('data', function (data) {
                data = data.toString().split('\n')[0];
                let javaVersion = new RegExp('openjdk version').test(data) ? data.split(' ')[2].replace(/"/g, '') : false;
                if (!javaVersion) {
                    javaVersion = new RegExp('java version').test(data) ? data.split(' ')[2].replace(/"/g, '') : false;
                }
                clearTimeout(interval)
                if (javaVersion) {
                    resolve(javaVersion);
                } else {
                    resolve(null);
                }
            });
        } catch (e) {
            console.error(e)
            resolve(null);
        }
    });
}
