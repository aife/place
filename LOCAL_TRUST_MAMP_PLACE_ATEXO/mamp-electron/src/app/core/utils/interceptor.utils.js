const axios = require("axios");

export default function setup(store) {
    axios.interceptors.request.use(function (config) {
        let staticHeadersParams = store.state.application.staticHeadersParams;
        if (staticHeadersParams) {
            Object.keys(staticHeadersParams).forEach(value => {
                config.headers[value] = staticHeadersParams[value];
            })
        }
        return config;
    }, function (err) {
        return Promise.reject(err);
    });
}