const fs = require("fs");
exports.createConfigurationFolderIfNotExist = function (installationPath) {
    // Create appDataDir if not exist
    if (!fs.existsSync(installationPath)) {
        try {
            fs.mkdirSync(installationPath, {recursive: true});
        } catch (error) {
        }
    }
    return true;
};
