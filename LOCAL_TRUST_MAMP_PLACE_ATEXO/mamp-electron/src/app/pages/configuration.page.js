import AtxTitleComponent from "../shared/components/title.component.js";
import AtxPreviousNextFinishComponent from "../shared/components/next-previous-finish.component.js";
import AtxFileSelector from "../shared/components/file-selector.component.js";

export default {
    components: {
        "atx-file-selector": AtxFileSelector,
        "atx-title": AtxTitleComponent,
        "atx-previous-next-finish": AtxPreviousNextFinishComponent,
    },
    props: {
        proxy: Object
    },
    computed: {
        savedProxy() {
            return this.$store.state.application.proxy;
        },
    },
    methods: {
        save() {
            this.$store.dispatch("application/setProxy", JSON.parse(JSON.stringify(this.formData)));
        }, reset() {
            this.formData = JSON.parse(JSON.stringify(this.savedProxy));
        },
        proxyNotDefined() {
            if (!this.formData.isActivated || this.formData.proxyChoice === 1) {
                return false;
            }
            if (this.formData.proxyChoice === 3) {
                return this.isEmpty(this.formData.proxyPacHost);
            }

            return !this.formData || !this.formData.http || this.isEmpty(this.formData.http.host) || this.isEmpty(this.formData.http.port)
                || !this.formData.https || this.isEmpty(this.formData.https.host) || this.isEmpty(this.formData.https.port);
        },
        isEmpty(string) {
            return !string || string === '';
        },
        proxyNotChanged() {
            return JSON.stringify(this.savedProxy) === JSON.stringify(this.formData)
        }
    },
    data() {
        return {
            formData: {
                proxyChoice: 1,
                credentials: {username: null, password: null},
                https: {
                    host: null, port: null
                },
                http: {
                    host: null, port: null
                }, isActivated: true, proxyPacHost: null,
            }
        }
    },
    mounted() {
        if (this.savedProxy) {
            this.formData = JSON.parse(JSON.stringify(this.savedProxy))
        }
    },
    template: `
        <div class="page" style="height: 96vh">
            <atx-title :title="$t('configuration.title')"
                       :description="$t('configuration.description')"></atx-title>

            <v-form v-if="formData" class="mt-1">
                <v-card class="mx-auto mt-1" max-width="95%" outlined  style="border: none">
                    <v-checkbox v-model="formData.isActivated" id="activateProxy"
                                v-bind:label="$t('configuration.proxy.activate')"
                                color="primary" v-bind:value="true"
                                hide-details></v-checkbox>
                </v-card>
                <v-card class="mx-auto" max-width="95%" outlined
                        style="margin-bottom:20px;">
                    <v-list-item three-line>
                        <v-list-item-content>
                            <div>
                                <v-alert icon="info" prominent text type="info"
                                     style="margin-bottom:20px;">
                                    <div v-html="$t('configuration.proxy.infos')"></div>
                                </v-alert>
                                <v-radio-group v-model="formData.proxyChoice" style="margin-top:0;"
                                               v-if="formData.isActivated">
                                    <v-radio v-bind:value="1" id="system"
                                             v-bind:label="$t('configuration.proxy.system')"></v-radio>
                                    <v-radio v-bind:value="2" id="manual"
                                             v-bind:label="$t('configuration.proxy.manuel')"></v-radio>
                                    <v-radio v-bind:value="3" id="fromProxyPac" 
                                             v-bind:label="$t('configuration.proxy.script')"></v-radio>
                                </v-radio-group>
                                <v-text-field v-if="formData.isActivated && formData.proxyChoice === 3" v-model="formData.proxyPacHost"
                                              v-bind:hint="$t('configuration.proxy.script-placeholder')"
                                              v-bind:label="$t('configuration.proxy.script-pac')"
                                              outlined dense></v-text-field>
                            </div>
                        </v-list-item-content>
                    </v-list-item>
                </v-card>
                <v-card class="mx-auto" max-width="95%" outlined v-if=" formData.isActivated">
                    <v-list-item three-line>
                        <v-list-item-content class="configuration">
                            <div class="overline mb-4">
                                {{$t('configuration.proxy.credentials.title')}}
                            </div>
                            <v-row>

                                <v-col
                                        cols="12"
                                        sm="12"
                                        md="6"
                                >
                                    <v-text-field v-model="formData.credentials.username"
                                                  v-bind:hint="$t('configuration.proxy.username-placeholder')"
                                                  v-bind:label="$t('configuration.proxy.username')"
                                                  outlined dense></v-text-field>
                                </v-col>
                                <v-col
                                        cols="12"
                                        sm="12"
                                        md="6"
                                >
                                    <v-text-field v-model="formData.credentials.password"
                                                  v-bind:type="'password'"
                                                  v-bind:label="$t('configuration.proxy.password')"
                                                  outlined dense></v-text-field>
                                </v-col>
                            </v-row>
                        </v-list-item-content>
                    </v-list-item>

                </v-card>
                <div v-if="formData.proxyChoice ===2 && formData.isActivated">

                    <v-card class="mx-auto mt-1" max-width="95%" outlined>
                        <v-list-item three-line>
                            <v-list-item-content class="configuration">
                                <div class="overline mb-4">
                                    {{$t('configuration.proxy.http.title')}}
                                </div>
                                <v-row>
                                    <v-col
                                            cols="12"
                                            sm="12"
                                            md="6"
                                    >
                                        <v-text-field v-model="formData.http.host" :disabled="!formData.isActivated"
                                                      v-bind:hint="$t('configuration.proxy.host-placeholder')"
                                                      v-bind:label="$t('configuration.proxy.host')"
                                                      required
                                                      outlined dense></v-text-field>
                                    </v-col>
                                    <v-col
                                            cols="12"
                                            sm="12"
                                            md="6"
                                    >
                                        <v-text-field v-model="formData.http.port" :disabled="!formData.isActivated"
                                                      type="number" required
                                                      v-bind:hint="$t('configuration.proxy.port-placeholder')"
                                                      v-bind:label="$t('configuration.proxy.port')"
                                                      outlined dense></v-text-field>
                                    </v-col>
                                </v-row>
                            </v-list-item-content>
                        </v-list-item>
                    </v-card>
                    <v-card class="mx-auto mt-1" max-width="95%" outlined>
                        <v-list-item three-line>
                            <v-list-item-content class="configuration">
                                <div class="overline mb-4">
                                    {{$t('configuration.proxy.https.title')}}

                                </div>
                                <v-row>
                                    <v-col
                                            cols="12"
                                            sm="12"
                                            md="6"
                                    >
                                        <v-text-field v-model="formData.https.host" :disabled="!formData.isActivated"
                                                      v-bind:hint="$t('configuration.proxy.host-placeholder')"
                                                      v-bind:label="$t('configuration.proxy.host')"
                                                      required
                                                      outlined dense></v-text-field>
                                    </v-col>
                                    <v-col
                                            cols="12"
                                            sm="12"
                                            md="6"
                                    >
                                        <v-text-field v-model="formData.https.port" :disabled="!formData.isActivated"
                                                      type="number" required
                                                      v-bind:hint="$t('configuration.proxy.port-placeholder')"
                                                      v-bind:label="$t('configuration.proxy.port')"
                                                      outlined dense></v-text-field>
                                    </v-col>
                                </v-row>

                            </v-list-item-content>
                        </v-list-item>
                    </v-card>
                </div>
            </v-form>
      
            <div style="bottom: 35px;
    position: absolute;
    right: 35px;">
       <v-btn small color="primary" v-if="!!savedProxy && !proxyNotChanged()"
                       @click="reset()"
                       class="text--bold  next-btn">
                    <v-icon dark left>loop</v-icon>
                    {{$t('configuration.reset')}}
                </v-btn>
                <v-btn small color="primary" :disabled="proxyNotDefined() || proxyNotChanged()"
                       @click="save()"
                       class="text--bold  next-btn">
                    <v-icon dark left>save</v-icon>
                    {{$t('configuration.relaunch')}}
                </v-btn>
               
            </div>
        </div>`,
};
