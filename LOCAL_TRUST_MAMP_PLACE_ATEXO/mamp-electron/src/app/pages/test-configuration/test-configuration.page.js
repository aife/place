import AtxTitleComponent from "../../shared/components/title.component.js";
import AtxConfigurationResultComponent from "./components/configuration-result.component.js";
import AtxConfigurationTableComponent from "./components/configuration-table.component.js";
import getFormatDate from "../../shared/pipe/date-time.pipe.js";

export default {
  components: {
    "atx-title": AtxTitleComponent,
    "atx-configuration-table": AtxConfigurationTableComponent,
    "atx-configuration-result": AtxConfigurationResultComponent,
  },
  filters: {
    formatDate(date) {
      return getFormatDate(date);
    },
  },
  computed: {
    startAt() {
      let app = this.$store.state.application.itemsActiveMap.get(
        "atx-test-configuration"
      );
      return !!app ? app.startAt : null;
    },
    params() {
      return this.$store.state.application.itemsActiveMap.get(
        "atx-test-configuration"
      ).data;
    },
    configurations() {
      return this.$store.state.systemConfiguration.configurations;
    },
    isLoaded() {
      return this.configurations.filter((item) => !item.isLoaded).length === 0;
    },
  },
  async mounted() {
    if (!this.isLoaded) {
      const browser = require("detect-browser").detect(this.params.useragent);
      this.$store
        .dispatch("systemConfiguration/initConfiguration")
        .then(async () => {
          for (const config of this.configurations) {
            await this.$store.dispatch("systemConfiguration/getConfiguration", {
              key: config.key,
              urlPubliqueServeurCrypto: this.params.urlPubliqueServeurCrypto,
              browserName: browser.name,
              browserVersion: browser.version,
            });
          }
        });
    }
  },
  methods: {
    closeTestConfiguration() {
      this.$store.dispatch("application/closeItem", "atx-test-configuration");
      this.$store.dispatch("systemConfiguration/initConfiguration");
      this.$router.push({
        name: "atx-home",
      });
      this.$destroy();
    },
  },
  template: `
        <div class="page">
            <div class="step--test-configuration">
                <div style="margin-top:20px;margin-left:20px;margin-right: 20px;">
                    <v-card class="elevation-2" color="#FAFAFA">
                        <v-card-title
                                class="headline-5">{{ $t('test-configuration.title') }}</v-card-title>
                        <v-card-text>

                            <strong>{{ $t('test-configuration.launch-time') }}</strong>
                            {{ startAt | formatDate }}<br/>
                        </v-card-text>
                    </v-card>
                    <v-timeline dense alignTop>
                        <atx-configuration-table></atx-configuration-table>
                        <atx-configuration-result
                                @close="closeTestConfiguration()"></atx-configuration-result>
                    </v-timeline>
                </div>
            </div>
        </div>
    `,
};
