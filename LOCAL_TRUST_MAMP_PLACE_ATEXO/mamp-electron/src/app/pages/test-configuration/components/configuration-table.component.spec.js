import Vue from "vue";
import AtxConfigurationTableComponent from "./configuration-table.component.js";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";
import Vuetify from "vuetify";

Vue.use(Vuetify);
let localVue = createLocalVue();
localVue.use(Vuex);

describe("AtxConfigurationTableComponent", () => {
  let wrapper;
  let store;
  let actions;
  let state;

  beforeAll(() => {
    state = {
      configurations: [
        {
          key: "system",
          isValid: true,
          isLoaded: true,
        },
      ],
    };

    actions = {
      initConfiguration: jest.fn(),
      getConfiguration: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        application: {
          namespaced: true,
          actions: { closeItem: jest.fn() },
        },
        systemConfiguration: {
          state,
          actions,
          namespaced: true,
        },
      },
    });
    const AtxConfigurationTableTestComponent = {
      components: { AtxConfigurationTableComponent },
      template:
        '<v-timeline><atx-configuration-table-component ref="testRef"></atx-configuration-table-component></v-timeline>',
    };

    wrapper = mount(AtxConfigurationTableTestComponent, {
      localVue,
      vuetify: new Vuetify(),
      store,
      mocks: {
        $t: (msg) => msg,
      },
    });
  });

  it("should create", () => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-timeline-item")).toBeDefined();
    expect(wrapper.vm.$refs.testRef.isLoaded).toBeTruthy();
    expect(wrapper.vm.$refs.testRef.configurations).toBe(state.configurations);
  });
});
