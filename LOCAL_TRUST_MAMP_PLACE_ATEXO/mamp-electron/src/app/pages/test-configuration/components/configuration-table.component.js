export default {
  computed: {
    configurations() {
      return this.$store.state.systemConfiguration.configurations;
    },
    isLoaded() {
      return this.configurations.filter((item) => !item.isLoaded).length === 0;
    },
  },
  template: `
        <div>
            <v-timeline-item :color="isLoaded?'green':''"
                             :icon="isLoaded?'check':''">
                <template v-slot:icon v-if="!isLoaded">
                    <v-progress-circular :size="25" :width="3"
                                         color="#2660A4"
                                         indeterminate></v-progress-circular>
                </template>
                <v-card class="elevation-2"  color="#FAFAFA">
                    <v-card-title
                            class="headline-5"> {{$t('test-configuration.technical-analyse')}}
                    </v-card-title>
                    <v-card-text>
                        <v-simple-table class="elevation-2">
                            <template v-slot:default>
                                <thead class="configuration">
                                <tr>
                                    <th class="text-left" style="width: 50%">
                                        {{$t('test-configuration.system-header')}}
                                    </th>
                                    <th class="text-left">
                                        {{$t('test-configuration.result')}}
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="configuration">
                                <tr v-for="item in configurations"
                                    :key="item.key">
                                    <td style="width: 50%">{{ $t('test-configuration.' + item.key) }} </td>
                                    <td>
                                        <v-chip v-if="item.isLoaded"
                                                :color="item.isValid? 'green': 'red'"
                                                dark>
                                            <v-icon left>
                                                {{ item.isValid ? 'check_circle_outline' : 'error_outline' }}
                                            </v-icon>
                                            <span v-if="item.label">
                                                        {{ item.label }}
                                                    </span>
                                            <span v-else-if="item.isValid">
                                                        {{$t('test-configuration.' + item.key + '-success')}}
                                                    </span>
                                            <span v-else>
                                                        {{$t('test-configuration.' + item.key + '-error')}}
                                                    </span>
                                        </v-chip>
                                        <v-chip v-else
                                                color="info"
                                                dark>
                                            <v-icon left>
                                                more_horiz
                                            </v-icon>
                                            <span>
                                                        {{$t('test-configuration.loading-test')}}
                                                    </span>
                                        </v-chip>
                                    </td>
                                </tr>
                                </tbody>
                            </template>
                        </v-simple-table>
                    </v-card-text>
                </v-card>
            </v-timeline-item>
        </div>`,
};
