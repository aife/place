export default {
  computed: {
    configurations() {
      return this.$store.state.systemConfiguration.configurations;
    },
    isLoaded() {
      return this.configurations.filter((item) => !item.isLoaded).length === 0;
    },
    isValid() {
      return this.configurations.filter((item) => !item.isValid).length === 0;
    },
  },
  methods: {
    closeItem() {
      this.$emit("close");
    },
  },
  watch: {
    isLoaded(val) {
      if (val) {
        this.$store.dispatch("application/setItemStatus", {
          status: this.isValid ? "success" : "error",
          id: "atx-test-configuration",
        });
      }
    },
  },
  template: `
        <div>
            <v-timeline-item v-if="!isLoaded" color="#AAAAAA"
                             :icon="'more_horiz'"
                             :icon-color="''">
                <template v-slot:icon>
                </template>
                <v-card class="elevation-2"  color="#FAFAFA">
                    <v-card-title
                            class="headline-4">{{$t('test-configuration.result')}}
                    </v-card-title>
                    <v-card-text>
                        {{$t('test-configuration.loading')}}
                    </v-card-text>
                </v-card>
            </v-timeline-item>
            <v-timeline-item v-else-if="isValid" color="green"
                             icon="check">
                <template v-slot:icon>
                </template>
                <v-card class="elevation-2"  color="#FAFAFA">
                    <v-card-title
                            class="headline-4">{{$t('test-configuration.result')}}
                    </v-card-title>
                    <v-card-text>
                        {{$t('test-configuration.success')}}

                        <v-divider class="my-4"></v-divider>

                        <v-btn color="#2660A4" @click="closeItem"><span
                                style="color:white;">{{$t('finish-and-close')}}</span>
                        </v-btn>
                    </v-card-text>
                </v-card>
            </v-timeline-item>

            <v-timeline-item v-else color="red" icon="priority_high">
                <template v-slot:icon>

                </template>
                <v-card class="elevation-2"  color="#FAFAFA">
                    <v-card-title
                            class="headline-4">{{$t('test-configuration.result')}}
                    </v-card-title>
                    <v-card-text style="color:red">
                        {{$t('test-configuration.error')}}

                        <v-divider class="my-4"></v-divider>

                        <v-btn color="#2660A4" @click="closeItem"><span
                                style="color:white;">{{$t('finish-and-close')}}</span>
                        </v-btn>
                    </v-card-text>
                </v-card>
            </v-timeline-item>
        </div>`,
};
