import Vue from "vue";
import AtxConfigurationResultComponent from "./configuration-result.component.js";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";
import Vuetify from "vuetify";

Vue.use(Vuetify);
let localVue = createLocalVue();
localVue.use(Vuex);

describe("AtxConfigurationResultComponent", () => {
  let wrapper;
  let store;
  let actions;
  let state;

  beforeAll(() => {
    state = {
      configurations: [
        {
          key: "system",
          isValid: true,
          isLoaded: true,
        },
      ],
    };

    actions = {
      initConfiguration: jest.fn(),
      getConfiguration: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        application: {
          namespaced: true,
          actions: { closeItem: jest.fn() },
        },
        systemConfiguration: {
          state,
          actions,
          namespaced: true,
        },
      },
    });
    const AtxConfigurationResultTestComponent = {
      components: { AtxConfigurationResultComponent },
      template:
        '<v-timeline><atx-configuration-result-component ref="testRef"></atx-configuration-result-component></v-timeline>',
    };

    wrapper = mount(AtxConfigurationResultTestComponent, {
      localVue,
      vuetify: new Vuetify(),
      store,
      mocks: {
        $t: (msg) => msg,
      },
    });
  });

  it("should create", () => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-timeline-item")).toBeDefined();
    expect(wrapper.vm.$refs.testRef.isLoaded).toBeTruthy();
    expect(wrapper.vm.$refs.testRef.isValid).toBeTruthy();
    expect(wrapper.vm.$refs.testRef.configurations).toBe(state.configurations);
  });
});
