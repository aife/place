import Vue from "vue";
import AtxTestConfigurationPage from "./test-configuration.page.js";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";
import Vuetify from "vuetify";

Vue.use(Vuetify);
let localVue = createLocalVue();
localVue.use(Vuex);

describe("AtxTestConfigurationPage", () => {
  let wrapper;
  let store;
  let actions;
  let state;

  beforeAll(() => {
    state = {
      configurations: [
        {
          key: "system",
          isValid: false,
          isLoaded: false,
        },
      ],
    };

    actions = {
      initConfiguration: jest.fn(),
      getConfiguration: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        application: {
          namespaced: true,
          state: {
            itemsActiveMap: new Map().set("atx-test-configuration", {
              data: "test",
            }),
          },
        },
        systemConfiguration: {
          state,
          actions,
          namespaced: true,
        },
      },
    });

    wrapper = mount(AtxTestConfigurationPage, {
      localVue,
      store,
      vuetify: new Vuetify(),
      mocks: {
        $t: (msg) => msg,
      },
    });
  });

  it("should create", () => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("atx-configuration-table")).toBeDefined();
    expect(wrapper.find("atx-configuration-result")).toBeDefined();
    expect(wrapper.vm.params).toBe("test");
    expect(wrapper.vm.configurations).toBe(state.configurations);
  });
});
