import Vue from "vue";
import Vuetify from "vuetify";
import Vuex from "vuex";
import AtxRecuperationOffreComponent from "./step1-recuperation-offres.component.js";
import { createLocalVue, mount } from "@vue/test-utils";

Vue.use(Vuetify);
let localVue = createLocalVue();
localVue.use(Vuex);

describe("RecuperationOffreComponent", () => {
  let wrapper, store;

  beforeAll(() => {
    const sessionId = "123456";
    const sessionData = { data: "" };
    const itemsActiveMap = new Map();
    itemsActiveMap.set(sessionId, sessionData);
    store = new Vuex.Store({
      modules: {
        application: {
          state: {
            itemsActiveMap,
          },
          namespaced: true,
        },
        dechiffrement: {
          actions: {
            getOffres: () => new Promise(() => {}),
          },
          namespaced: true,
        },
      },
    });
    wrapper = mount(AtxRecuperationOffreComponent, {
      localVue,
      vuetify: new Vuetify(),
      propsData: {
        sessionId,
      },
      store,
      mocks: {
        $t: (msg) => msg,
      },
      stubs: ["v-timeline-item"],
    });
  });

  it("RecuperationOffre component initialized", () => {
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-timeline-item")).toBeDefined();
    expect(wrapper.find("v-progress-circular")).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
  });
});
