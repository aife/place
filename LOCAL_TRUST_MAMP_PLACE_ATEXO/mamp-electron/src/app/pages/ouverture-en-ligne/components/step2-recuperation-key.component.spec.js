import Vue from "vue";
import Vuetify from "vuetify";
import AtxRecuperationKeyComponent from "./step2-recuperation-key.component.js";
import { createLocalVue, mount } from "@vue/test-utils";

Vue.use(Vuetify);
let localVue = createLocalVue();

describe("RecuperationEnveloppesComponent", () => {
  let wrapper;

  beforeAll(() => {
    wrapper = mount(AtxRecuperationKeyComponent, {
      localVue,
      vuetify: new Vuetify(),
      mocks: {
        $t: (msg) => msg,
      },
      stubs: ["v-timeline-item"],
    });
  });

  it("RecuperationEnveloppes component initialized", () => {
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-timeline-item")).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
  });
});
