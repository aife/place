export default {
  props: ["sessionId", "start"],
  data() {
    return {
      isValid: false,
      isLoading: false,
    };
  },
  methods: {
    getKeys() {
      this.isLoading = true;
      let ouvertureEnLigne = this.$store.state.dechiffrement.ouvertureEnLigne;
      this.$store
        .dispatch("dechiffrement/getIdOffreQueue", {
          offres: ouvertureEnLigne.get(this.sessionId).offres,
          sessionId: this.params.sessionId,
          urlServeurUpload: this.params.urlServeurUpload,
        })
        .then((response) => {
          if (response) {
            this.isLoading = false;
            this.isValid = true;
            this.$emit("finish", "success");
          } else {
            this.isLoading = false;
            this.isValid = false;
            this.$emit("finish", "error");
            this.$store.dispatch("application/setItemStatus", {
              status: "error",
              id: this.params.sessionId,
            });
          }
        });
    },
    closeItem() {
      this.$emit("close");
    },
  },
  watch: {
    start(val) {
      if (val) {
        this.getKeys();
      }
    },
  },
  computed: {
    params() {
      return this.$store.state.application.itemsActiveMap.get(this.sessionId)
        .data;
    },
    iconColor() {
      if (!this.start) {
        return "#AAAAAA";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "green";

      return "deep-orange";
    },
    iconType() {
      if (!this.start) {
        return "more_horiz";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "check";

      return "priority_high";
    },
  },
  template: `
      <v-timeline-item
          :color="iconColor"
          :icon="iconType"
      >
      <template v-slot:icon v-if="isLoading">
        <v-progress-circular :size="25" :width="3" color="#2660A4"
                             indeterminate></v-progress-circular>
      </template>
      <v-card class="elevation-2"  color="#FAFAFA">
        <v-card-title class="headline-4">
          {{ $t('ouverture-en-ligne.step2.fetch-private-key') }}
        </v-card-title>
        <v-card-text v-if="!start">
          {{ $t('ouverture-en-ligne.awaiting') }}
        </v-card-text>
        <v-card-text v-else-if="isLoading">
          {{ $t('ouverture-en-ligne.step2.search-private-key') }}
        </v-card-text>
        <v-card-text v-else-if="isValid">
          {{ $t('ouverture-en-ligne.step2.fetch-private-key-correct') }}
        </v-card-text>

        <v-card-text style="color:red" v-else>
          {{ $t('ouverture-en-ligne.step2.decrypt-error') }}<br>
          {{ $t('ouverture-en-ligne.step2.decrypt-at-least-one') }}
          <v-divider class="my-4"></v-divider>
          <v-btn color="#2660A4" @click="closeItem">
                <span style="color:white;">
                  {{ $t('finish-and-close') }}
                </span>
          </v-btn>
        </v-card-text>

      </v-card>
      </v-timeline-item>
    `,
};
