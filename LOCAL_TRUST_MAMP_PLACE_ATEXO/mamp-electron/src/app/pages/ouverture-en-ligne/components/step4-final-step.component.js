export default {
  props: ["sessionId", "start"],
  data() {
    return {
      isLoaded: false,
    };
  },
  methods: {
    checkFinalStep() {
      this.isLoaded = true;
      this.$store.dispatch("application/setItemStatus", {
        status: "success",
        id: this.sessionId,
      });
    },
    closeItem() {
      this.$emit("close");
    },
  },
  watch: {
    start(val) {
      if (val) {
        this.checkFinalStep();
      }
    },
  },
  computed: {
    iconColor() {
      switch (this.isLoaded) {
        case true:
          return "green";
        case false:
          return "grey";
      }
    },
    iconType() {
      switch (this.isLoaded) {
        case true:
          return "check";
        case false:
          return "more_horiz";
      }
    },
  },
  template: `
        <v-timeline-item
                :color="iconColor"
                :icon="iconType"
        >
            <v-card class="elevation-2"  color="#FAFAFA">
                <v-card-title class="headline-5">
                    {{$t('ouverture-en-ligne.step4.title')}}
                </v-card-title>
                <v-card-text v-if="!isLoaded">
                    {{$t('ouverture-en-ligne.awaiting')}}
                </v-card-text>
                <v-card-text v-else>
                    {{$t('ouverture-en-ligne.step4.complete')}}
                    <v-divider class="my-4"></v-divider>
                    <v-btn color="#2660A4" @click="closeItem">
                          <span style="color:white;">
                            {{$t('finish-and-close')}}
                          </span>
                    </v-btn>
                </v-card-text>
            </v-card>
        </v-timeline-item>
    `,
};
