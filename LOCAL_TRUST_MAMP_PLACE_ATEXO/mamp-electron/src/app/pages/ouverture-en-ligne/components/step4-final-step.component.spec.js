import Vue from "vue";
import AtxFinalStepComponent from "./step4-final-step.component.js";
import Vuex from "vuex";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuetify from "vuetify";

Vue.use(Vuetify);

let localVue = createLocalVue();
localVue.use(Vuex);

const systemConfigurationState = {
  configurations: [
    {
      key: "system",
      isValid: true,
      isLoaded: true,
    },
  ],
};

const systemConfigurationActions = {
  initConfiguration: jest.fn(),
  getConfiguration: jest.fn(),
};

// atx_todo: test store
const store = new Vuex.Store({
  modules: {
    application: {
      namespaced: true,
      actions: { closeItem: jest.fn() },
    },
    systemConfiguration: {
      state: systemConfigurationState,
      actions: systemConfigurationActions,
      namespaced: true,
    },
  },
});

describe("FinalStepComponent", () => {
  let wrapper;

  beforeAll(() => {
    wrapper = mount(AtxFinalStepComponent, {
      localVue,
      vuetify: new Vuetify(),
      mocks: {
        $t: (msg) => msg,
      },
      stubs: ["v-timeline-item"],
    });
  });

  it("FinalStep component initialized", () => {
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-btn")).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
  });
});
