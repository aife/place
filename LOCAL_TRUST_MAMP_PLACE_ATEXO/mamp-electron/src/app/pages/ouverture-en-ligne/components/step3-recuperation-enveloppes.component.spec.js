import AtxRecuperationEnveloppesComponent from "./step3-recuperation-enveloppes.component.js";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuetify from "vuetify";
import Vue from "vue";

Vue.use(Vuetify);
let localVue = createLocalVue();

describe("RecuperationEnveloppesComponent", () => {
  let wrapper;

  beforeAll(() => {
    wrapper = mount(AtxRecuperationEnveloppesComponent, {
      localVue,
      vuetify: new Vuetify(),
      mocks: {
        $t: (msg) => msg,
      },
      stubs: ["v-timeline-item"],
    });
  });

  it("RecuperationEnveloppes component initialized", () => {
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find("v-timeline-item")).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
  });
});
