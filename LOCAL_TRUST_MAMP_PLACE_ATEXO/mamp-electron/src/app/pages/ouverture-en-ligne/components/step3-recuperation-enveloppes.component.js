export default {
  props: ["sessionId", "start"],
  data() {
    return {
      progress: null,
      isValid: false,
      isLoading: false,
    };
  },
  methods: {
    getEnveloppes() {
      this.isLoading = true;

      const data = {
        idOffreQueue: this.$store.state.dechiffrement.ouvertureEnLigne.get(
          this.sessionId
        ).idOffreQueue,
        urlServeurUpload: this.params.urlServeurUpload,
        sessionId: this.sessionId,
      };
      this.$store
        .dispatch("dechiffrement/getEnveloppesProgress", data)
        .then((response) => {
          this.progress = response;
          if (!response) {
            clearInterval(this.interval);
            this.isLoading = false;
            this.isValid = false;
            this.$emit("finish", "error");
            this.$store.dispatch("application/setItemStatus", {
              status: "error",
              id: this.sessionId,
            });
          } else if (response.status === "FINI") {
            clearInterval(this.interval);
            this.isLoading = false;
            this.isValid = true;
            this.$emit("finish", "success");
          } else if (response.status === "ERREUR") {
            clearInterval(this.interval);
            this.isLoading = false;
            this.isValid = false;
            this.$emit("finish", "error");
            this.$store.dispatch("application/setItemStatus", {
              status: "error",
              id: this.sessionId,
            });
          }
        });
    },
    closeItem() {
      this.$emit("close");
    },
  },
  watch: {
    start(val) {
      if (val) {
        this.getEnveloppes();
        this.interval = setInterval(() => this.getEnveloppes(), 1000);
      }
    },
  },
  computed: {
    value() {
      if (!this.progress) return 0;
      return (this.progress.progressFichier / this.progress.totalFichier) * 100;
    },
    params() {
      return this.$store.state.application.itemsActiveMap.get(this.sessionId)
        .data;
    },
    iconColor() {
      if (!this.start) {
        return "#AAAAAA";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "green";

      return "deep-orange";
    },
    iconType() {
      if (!this.start) {
        return "more_horiz";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "check";

      return "priority_high";
    },
  },
  template: `
        <v-timeline-item
                :color="iconColor"
                :icon="iconType"
        >
            <template v-slot:icon v-if="isLoading">
                <v-progress-circular :size="25" :width="3" color="#2660A4"
                                     indeterminate></v-progress-circular>
            </template>

            <v-card class="elevation-2"  color="#FAFAFA">
                <v-card-title class="headline-5"
                              v-if="$store?.state?.dechiffrement?.ouvertureEnLigne?.get(sessionId)?.offres?.totalEnveloppe">
                    {{$t('ouverture-en-ligne.step3.decrypt-enveloppe') + ' ' + (progress?.progressEnveloppe ? progress.progressEnveloppe : 0) +
                ' ' + $t('ouverture-en-ligne.step3.of') + ' ' + $store?.state?.dechiffrement?.ouvertureEnLigne?.get(sessionId)?.offres?.totalEnveloppe}}
                </v-card-title>
                <v-card-title class="headline-5" v-else>
                    {{ $t('ouverture-en-ligne.step3.decrypt-enveloppe') }}

                </v-card-title>
                <v-card-text v-if="!start">
                    {{ $t('ouverture-en-ligne.awaiting') }}
                </v-card-text>
                <v-card-text v-else-if="isLoading || isValid">
                    <v-progress-linear height="25" color="#2660A4"
                                       :value="value">
                            <span style="color:white"><strong>{{ value.toFixed(2) }}
                                %</strong></span>
                    </v-progress-linear>
                </v-card-text>
                <v-card-text style="color:red" v-else>
                    {{ $t('ouverture-en-ligne.step1.fetch-files-error') }}
                    <v-divider class="my-4"></v-divider>
                    <v-btn color="#2660A4" @click="closeItem">
                        <span style="color:white;">
                          {{ $t('finish-and-close') }}
                        </span>
                    </v-btn>
                </v-card-text>
            </v-card>
        </v-timeline-item>
    `,
};
