export default {
  props: ["sessionId", "start"],
  data() {
    return {
      isValid: false,
      isLoading: false,
    };
  },
  methods: {
    getOffres() {
      this.isLoading = true;
      this.$store
        .dispatch("dechiffrement/getOffres", this.params)
        .then((response) => {
          if (response) {
            this.isLoading = false;
            this.isValid = true;
            this.$emit("finish", "success");
          } else {
            this.isLoading = false;
            this.isValid = false;
            this.$emit("finish", "error");
            this.$store.dispatch("application/setItemStatus", {
              status: "error",
              id: this.sessionId,
            });
          }
        });
    },
    closeItem() {
      this.$emit("close");
    },
  },
  mounted() {
    this.getOffres();
  },
  computed: {
    params() {
      return this.$store.state.application.itemsActiveMap.get(this.sessionId)
        .data;
    },
    iconColor() {
      if (!this.start) {
        return "#AAAAAA";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "green";

      return "deep-orange";
    },
    iconType() {
      if (!this.start) {
        return "more_horiz";
      }
      if (this.isLoading) {
        return "";
      }
      if (this.isValid) return "check";

      return "priority_high";
    },
  },
  template: `
        <v-timeline-item
                :color="iconColor"
                :icon="iconType"
        >
            <template v-slot:icon v-if="isLoading">
                <v-progress-circular :size="25" :width="3" color="#2660A4"
                                     indeterminate></v-progress-circular>
            </template>

            <v-card class="elevation-2"  color="#FAFAFA">
                <v-card-title class="headline-5">
                    {{ $t('ouverture-en-ligne.step1.fetch-files') }}
                </v-card-title>
                <v-card-text v-if="!start">
                    {{ $t('ouverture-en-ligne.awaiting') }}
                </v-card-text>
                <v-card-text v-else-if="isLoading">
                    {{ $t('ouverture-en-ligne.step1.search-files') }}
                </v-card-text>
                <v-card-text v-else-if="isValid">
                    {{ $t('ouverture-en-ligne.step1.fetch-files-correct') }}
                </v-card-text>
                <v-card-text v-else style="color:red"
                >
                    {{ $t('ouverture-en-ligne.step1.fetch-files-error') }}
                    <v-divider class="my-4"></v-divider>
                    <v-btn color="#2660A4" @click="closeItem">
                    <span style="color:white;">
                      {{ $t('finish-and-close') }}
                    </span>
                    </v-btn>
                </v-card-text>
                <v-card-text v-else>
                    {{ $t('ouverture-en-ligne.step1.search-fetch-files') }}
                </v-card-text>
            </v-card>
        </v-timeline-item>
    `,
};
