import AtxTitleComponent from "../../shared/components/title.component.js";
import AtxRecuperationOffresComponent from "./components/step1-recuperation-offres.component.js";
import AtxRecuperationKeyComponent from "./components/step2-recuperation-key.component.js";
import AtxRecuperationEnveloppesComponent from "./components/step3-recuperation-enveloppes.component.js";
import AtxFinalStepComponent from "./components/step4-final-step.component.js";
import getFormatDate from "../../shared/pipe/date-time.pipe.js";

export default {
  components: {
    "atx-title": AtxTitleComponent,
    "atx-recuperation-offres": AtxRecuperationOffresComponent,
    "atx-recuperation-key": AtxRecuperationKeyComponent,
    "atx-recuperation-enveloppes": AtxRecuperationEnveloppesComponent,
    "atx-final-step": AtxFinalStepComponent,
  },
  filters: {
    formatDate(date) {
      return getFormatDate(date);
    },
  },
  data() {
    return {
      sessionId: null,
      startStep1: false,
      startStep2: false,
      startStep3: false,
      startStep4: false,
    };
  },
  computed: {
    startAt() {
      let app = this.$store.state.application.itemsActiveMap.get(
        this.sessionId
      );
      return !!app ? app.startAt : null;
    },
    isLoaded() {
      let status = this.$store.state.application.itemsActiveMap.get(
        this.sessionId
      ).status;
      return status === "error" || status === "success";
    },
    reference() {
      let offre = this.$store.state.dechiffrement.ouvertureEnLigne.get(
        this.sessionId
      );
      return !!offre && !!offre.offres ? offre.offres.reference : null;
    },
  },
  mounted() {
    this.sessionId = this.$route.params.sessionId;
    if (!this.isLoaded) {
      this.$store.dispatch("application/setItemStatus", {
        status: null,
        id: this.sessionId,
      });
      this.startStep1 = true;
    }
  },

  methods: {
    finishStep1(status) {
      if (status === "success") {
        this.startStep2 = true;
      }
    },
    finishStep2(status) {
      if (status === "success") {
        this.startStep3 = true;
      }
    },
    finishStep3(status) {
      if (status === "success") {
        this.startStep4 = true;
      }
    },
    closeOuvertureEnLigne() {
      this.$store.dispatch("application/closeItem", this.sessionId);
      this.$router.push({
        name: "atx-home",
      });
      this.$destroy();
    },
  },
  template: `
      <div class="page">
      <div class="step--ouverture-en-ligne" v-if="!!sessionId">
        <div style="margin-top:20px;margin-left:20px;margin-right: 20px;">
          <v-card class="elevation-2" color="#FAFAFA">
            <v-card-title
                class="headline-5">{{ $t('ouverture-en-ligne.title') }}</v-card-title>
            <v-card-text>

              <strong>{{ $t('ouverture-en-ligne.launch-time') }}</strong>
              {{ startAt | formatDate }}<br/>
              <strong>{{ $t('ouverture-en-ligne.reference') }}</strong>
              {{ $store?.state?.dechiffrement?.ouvertureEnLigne?.get(sessionId)?.offres?.reference }}
              <br/>
              <strong>{{ $t('ouverture-en-ligne.total-enveloppes') }}</strong>
              {{ $store?.state?.dechiffrement?.ouvertureEnLigne?.get(sessionId)?.offres?.totalEnveloppe }}

            </v-card-text>
          </v-card>
          <v-timeline dense align-top>
            <atx-recuperation-offres @finish="finishStep1($event)" @close="closeOuvertureEnLigne()"
                                     :sessionId="sessionId"
                                     :start="startStep1"></atx-recuperation-offres>
            <atx-recuperation-key @finish="finishStep2($event)" @close="closeOuvertureEnLigne()"
                                  :sessionId="sessionId"
                                  :start="startStep2"></atx-recuperation-key>
            <atx-recuperation-enveloppes
                @finish="finishStep3($event)" @close="closeOuvertureEnLigne()"
                :sessionId="sessionId"
                :start="startStep3"></atx-recuperation-enveloppes>
            <atx-final-step :start="startStep4"
                            :sessionId="sessionId" @close="closeOuvertureEnLigne()"></atx-final-step>
          </v-timeline>
        </div>
      </div>
      </div>`,
};
