export default {
    data() {
        return {
            env: null,
            releaseDate: null,
            ressourcesParams: null,
            error: false,
            loading: true
        };
    },
    mounted() {
        this.env = process.env.environnement;
        this.releaseDate = process.env.releaseDate;
        console.log("URL de téléchargements", process.env.ressourcesUrl);
        this.verifConnexion();
    }, computed: {
        params() {
            return this.$store.state.application.params;
        },
    },
    methods: {
        verifConnexion() {
            this.loading = true;
            this.$store.dispatch("systemConfiguration/version", {
                url: process.env.ressourcesUrl,
                env: this.env,
                system: process.platform
            })
                .then(value => {
                    const yaml = require('js-yaml');
                    this.loading = false;
                    this.ressourcesParams = yaml.load(value)
                }, (error) => {
                    this.loading = false;
                    this.error = true;
                    console.log(error)
                })
        },
        download() {
            this.$store.dispatch('application/download', process.env.ressourcesUrl + '/' + this.ressourcesParams.files[0].url)
        }
    },
    template: `
<div class="page home">
    <div class="welcome">
     <div style="display: flex" class="margin-top-20-px">
                    <img src="assets/img/home.png" class="welcome-image" width="500">
                </div>
        <div style="padding:20px;" v-if="loading">
             <v-progress-linear height="25" color="#2660A4" indeterminate>
                 <span style="color:white"><strong>{{$t('main-application.test-progress')}}</strong></span>
            </v-progress-linear>
        </div>
        <div style="padding:20px;" v-else>
            <v-alert icon="error" prominent text type="error" v-if="!ressourcesParams">
                <div style="display: flex; justify-content: space-between">
                    <div v-html="$t('main-application.ko')"></div>
                    <div>
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="verifConnexion()"
                                        color="error"
                                >
                                    <v-icon>replay</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.replay')}}</span>
                        </v-tooltip>
                    </div>
                </div>
            </v-alert>

            <v-alert icon="warning" prominent text type="warning"
                     v-else-if="params.version !== ressourcesParams.version">
                <div style="display: flex; justify-content: space-between">
                    <div style="margin-top: auto;margin-bottom: auto;"
                         v-html="$t('main-application.old-version') +' <b>'+ ressourcesParams.version+'</b>.'">
                    </div>
                    <div>
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="download()"
                                        color="warning"
                                >
                                    <v-icon>file_download</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.download')}}</span>
                        </v-tooltip>
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="verifConnexion()"
                                        color="warning"
                                >
                                    <v-icon>replay</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.replay')}}</span>
                        </v-tooltip>
                    </div>
                </div>

            </v-alert>
            <v-alert icon="warning" prominent text type="warning"
                     v-else-if="!!releaseDate">
                <div style="display: flex; justify-content: space-between">
                    <div style="margin-top: auto;margin-bottom: auto;"
                         v-html="$t('main-application.old-release') +' <b>'+ releaseDate +'</b>.'">
                    </div>
                    <div>
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="download()"
                                        color="warning"
                                >
                                    <v-icon>file_download</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.download')}}</span>
                        </v-tooltip>
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="verifConnexion()"
                                        color="warning"
                                >
                                    <v-icon>replay</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.replay')}}</span>
                        </v-tooltip>
                    </div>
                </div>

            </v-alert>
            <v-alert icon="check" prominent text type="success" v-else>
                <div style="display: flex; justify-content: space-between">
                    <div style="margin-top: auto;margin-bottom: auto;"
                         v-html="$t('main-application.ok')">
                    </div>
                    <div>
                      
                        <v-tooltip top>
                            <template
                                    v-slot:activator="{ on, attrs }">
                                <v-btn
                                        class="ma-2"
                                        outlined
                                        meduim
                                        fab
                                        v-bind="attrs"
                                        v-on="on"
                                        @click="verifConnexion()"
                                        color="success"
                                >
                                    <v-icon>replay</v-icon>
                                </v-btn>
                            </template>
                            <span>{{$t('main-application.replay')}}</span>
                        </v-tooltip>
                    </div>
                </div>

            </v-alert>
        </div>
        <div
                class="float-right padding-10-px-20-px text-align-right margin-top-20-px">
            <div class="font-size-45-pt ">{{ $t('hello') }}</div>
            <div class="font-size-22-pt"> {{ $t('welcome') }}
                <strong>{{ $t('main-application.title') }}</strong></div>
            <div class="font-size-22-pt">
                <strong v-if="env" style="color: red">{{ $t(env) }}</strong></div>
        </div>
    </div>

</div>
    `,
};
