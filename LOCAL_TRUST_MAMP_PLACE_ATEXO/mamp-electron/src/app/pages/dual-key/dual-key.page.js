import AtxTitleComponent from "../../shared/components/title.component.js";
import AtxDualKeyFormComponent from "../dual-key/components/dual-key-form.component.js";
import AtxDualKeyGenerationComponent from "../dual-key/components/dual-key-generation.component.js";
import AtxDualKeyConfirmationComponent from "../dual-key/components/dual-key-confimation.component.js";

export default {
  components: {
    "atx-title": AtxTitleComponent,
    "atx-dual-key-form": AtxDualKeyFormComponent,
    "atx-dual-key-generation": AtxDualKeyGenerationComponent,
    "atx-dual-key-confirmation": AtxDualKeyConfirmationComponent,
  },
  data() {
    return {
      sessionId: null,
      start: false,
      step: 1,
    };
  },
  methods: {
    launchGeneration() {
      this.nextStep();
      this.$store
        .dispatch("dualKey/postDualKeys", this.sessionId)
        .then((response) => {
          const generationResult = response.data;
          this.$store.dispatch("application/setItemStatus", {
            status:
              generationResult &&
              generationResult.sendingMpeStatus &&
              ((generationResult.generationExecuted &&
                generationResult.generationStatus) ||
                !generationResult.generationExecuted)
                ? "success"
                : "error",
            id: this.sessionId,
          });
          this.nextStep();
        })
        .catch((reason) => {
          this.$store.commit("dualKey/setGenerationResult", {
            sessionId: this.sessionId,
            data: "error",
          });
          this.$store.dispatch("application/setItemStatus", {
            status: "error",
            id: this.sessionId,
          });
          this.nextStep();
        });
    },
    exit() {
      this.$store.commit("application/closeItem", this.sessionId);
      // atx_todo: put the navigation in an action
      this.$router.push({
        name: "atx-home",
      });
      this.$destroy();
    },
    nextStep() {
      this.step++;
    },
  },
  mounted() {
    this.sessionId = this.$route.params.sessionId;
    this.$store.dispatch("dualKey/init", this.sessionId);
    this.start = true;
  },
  template: `
      <div class="page">
      <atx-title :title="$t('dual-key.title')"
                 :description="$t('dual-key.description')"></atx-title>
      <div class="step" v-if="start">
        <v-stepper alt-labels
                   v-model="step">
          <v-stepper-header>
            <v-stepper-step :complete="step > 1"
                            step="1"
            >
            </v-stepper-step>

            <v-divider></v-divider>

            <v-stepper-step :complete="step > 2"
                            step="2"
            >
            </v-stepper-step>

            <v-divider></v-divider>

            <v-stepper-step :complete="step > 3"
                            step="3"
            >
            </v-stepper-step>
          </v-stepper-header>
          <v-stepper-items>
            <v-stepper-content step="1">
              <atx-dual-key-form v-if="step===1"
                                 :sessionId="sessionId"
                                 @validate="launchGeneration"></atx-dual-key-form>
            </v-stepper-content>

            <v-stepper-content step="2">
              <atx-dual-key-generation :sessionId="sessionId" v-if="step===2"
              ></atx-dual-key-generation>
            </v-stepper-content>
            <v-stepper-content step="3">
              <atx-dual-key-confirmation v-if="step===3"
                                         :sessionId="sessionId"
                                         @exit="exit"></atx-dual-key-confirmation>
            </v-stepper-content>
          </v-stepper-items>
        </v-stepper>

      </div>
      </div>
    `,
};
