import AtxStepTitleComponent from "./step-title.component.js";
import { DualKeyFormState } from "../../../store/dual-key.module.js";

export default {
  components: { "atx-step-title": AtxStepTitleComponent },
  props: ["sessionId"],
  data() {
    return {
      formValid: false,
      formData: new DualKeyFormState(),
      aliasRules: [(v) => !!v || "Le nom est obligatoire"],
      certs: [],
      dnRules: [(v) => !!v || "Le nom est obligatoire"],
      singleSelect: true,
      selected: [],
      headers: [
        {
          text: "",
          value: "checked",
          sortable: false,
        },
        {
          text: "Bi-clé(s) disponible(s)",
          align: "start",
          sortable: false,
          value: "name",
        },
      ],
    };
  },
  mounted: function () {
    this.$store.dispatch("dualKey/getCertificatesList");
    this.$store.dispatch("dualKey/getProviderName");
    this.formData = this.getForm();
  },
  computed: {
    getCertificateList() {
      return this.$store.getters["dualKey/certificates"];
    },
    getProviderName() {
      return this.$store.getters["dualKey/provider"];
    },
  },
  methods: {
    isInvalid() {
      return (
        !this.formValid ||
        (this.formData.existingCert && !this.selectedCertificate())
      );
    },
    selectedCertificate() {
      return this.$store.getters["dualKey/selectedCertificate"](this.sessionId);
    },
    getForm() {
      return this.$store.getters["dualKey/form"](this.sessionId);
    },
    saveSelectedCertificate(event) {
      const isItemAlreadySelected =
        event.item.id === this.selectedCertificate();
      this.$store.commit("dualKey/setSelectedCertificate", {
        certificateId: isItemAlreadySelected ? null : event.item.id,
        sessionId: this.sessionId,
      });
      this.$forceUpdate();
    },
    validate() {
      this.$store.commit("dualKey/setFormData", {
        formData: this.formData,
        sessionId: this.sessionId,
      });
      this.$emit("validate");
    },
  },
  template: `
      <div style="overflow: auto; height: 100%;">
      <atx-step-title :title="$t('dual-key.step1.title')"></atx-step-title>
      <template>
        <v-form v-model="formValid" v-if="formData">

          <v-card class="mx-auto" max-width="95%" outlined
                  style="margin-bottom:20px;">
            <v-list-item three-line>
              <v-list-item-content>
                <div class="overline mb-4">
                  {{ $t('dual-key.step1.ident.title') }}
                </div>
                <v-alert icon="info" prominent text type="info"
                         style="margin-bottom:20px;">
                  <div v-html="$t('dual-key.step1.ident.infos')"></div>
                </v-alert>
                <v-text-field v-model="formData.alias"
                              v-bind:label="$t('dual-key.step1.ident.alias-label')"
                              :rules="aliasRules"
                              outlined dense hide-details="auto"></v-text-field>
                <v-checkbox v-model="formData.estCleChiffrementSecours"
                            v-bind:label="$t('dual-key.step1.ident.estCleChiffrementSecours-label')"
                            color="primary" v-bind:value="true"
                            hide-details></v-checkbox>
              </v-list-item-content>
            </v-list-item>
          </v-card>

          <v-card class="mx-auto" max-width="95%" outlined
                  style="margin-bottom:20px;">
            <v-list-item three-line>
              <v-list-item-content>
                <div class="overline mb-4">
                  {{ $t('dual-key.step1.generation.title') }}
                </div>
                <div>
                  <v-radio-group v-model="formData.existingCert" style="margin-top:0px;">
                    <v-radio v-bind:value="false"
                             v-bind:label="$t('dual-key.step1.generation.server-cert-label')"></v-radio>
                    <v-radio v-bind:value="true"
                             v-bind:label="$t('dual-key.step1.generation.existing-cert-label')"></v-radio>
                  
                  </v-radio-group>
                </div>
                <div v-if="formData.existingCert">
                  <hr>
                  <p class="font-weight-regular"
                     style="margin-top:20px;margin-bottom:20px;">
                    {{ $t('dual-key.step1.generation.instructions1') }}
                  </p>
                  <v-data-table
                      :headers="headers"
                      :items="getCertificateList"
                      @item-selected="saveSelectedCertificate"
                      item-key="id"
                      hide-default-footer
                      class="certificate-list-tab"
                      v-if="getCertificateList.length > 0">
                    <template v-slot:body="{ items,select }"
                    >
                      <tbody>
                      <tr
                          v-for="item in items"
                          :key="item.name"
                          @click="select(item)"
                          style="cursor:pointer"
                      >
                        <td>
                          <v-simple-checkbox
                              :value="selectedCertificate() === item.id"
                              @click="select(item)"
                          ></v-simple-checkbox>
                        </td>
                        <td>
                          <strong>
                            {{ item.nom }}
                          </strong>
                          {{ $t('dual-key.step1.transmit') }}
                          : {{ item.nomEmetteur }}
                          <br/>
                          <div style="margin-bottom: 5px">
                            {{ $t('dual-key.step1.end-date') }}
                            : {{ item.strDateExpiration }}
                          </div>
                        </td>
                      </tr>
                      </tbody>
                    </template>
                  </v-data-table>
                </div>
                <div v-else>
                  <v-alert icon="info" prominent text type="info"
                           style="margin-bottom:20px;">
                    <div v-html="$t('dual-key.step1.server')"></div>
                  </v-alert>
                  <hr>
                  <p class="font-weight-regular"
                     style="margin-top:20px;margin-bottom:20px;">
                    {{ $t('dual-key.step1.generation.instructions2') }}
                  </p>
                  <v-text-field v-model="getProviderName" dense outlined
                                disabled></v-text-field>
                  <v-text-field v-model="formData.cnCleChiffrement"
                                style="padding-top: 10px;"
                                v-bind:label="$t('dual-key.step1.generation.cnCleChiffrement-label')"
                                :rules="dnRules" hide-details="auto" outlined
                                dense></v-text-field>
                </div>

              </v-list-item-content>
            </v-list-item>
          </v-card>
          <v-btn
              :disabled="isInvalid()"
              color="#2660A4"
              style="margin:20px;float:right;" v-on:click="validate()"><span
              style="color:white;">
                                {{ $t('dual-key.step1.generation.generate') }}

            </span>
          </v-btn>
        </v-form>
      </template>
      </div>
    `,
};
