import AtxStepTitleComponent from "./step-title.component.js";

export default {
  components: { "atx-step-title": AtxStepTitleComponent },
  props: ["sessionId"],
  template: `
      <div>
      <atx-step-title :title="$t('dual-key.step2.title')"></atx-step-title>

      <v-card class="mx-auto" max-width="95%" outlined
              style="margin-bottom:20px;">
        <v-list-item three-line>
          <v-list-item-content>
            <div class="overline mb-4">
              {{ $t('dual-key.step2.progress') }}
            </div>
            <v-progress-linear indeterminate></v-progress-linear>
          </v-list-item-content>
        </v-list-item>
      </v-card>
      </div>
    `,
};
