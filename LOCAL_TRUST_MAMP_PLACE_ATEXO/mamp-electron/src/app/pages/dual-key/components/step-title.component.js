export default {
  props: ["title", "description"],
  template: `
        <div>
            <div class="atexo-step-title">{{title}}</div>
            <div class="atexo-step-description">{{description}}
            </div>
        </div>`,
};
