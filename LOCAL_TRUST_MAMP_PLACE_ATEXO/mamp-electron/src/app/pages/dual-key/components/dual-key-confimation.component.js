import AtxStepTitleComponent from "./step-title.component.js";

export default {
    components: {"atx-step-title": AtxStepTitleComponent},
    computed: {
        config() {
            return this.$store.state.dualKey.dualsKeys.get(this.sessionId);
        },
        generationResult() {
            return this.config.generationResult;
        },
        getNameCertificat() {
            return this.config.formData.alias;
        },
    },
    props: ["sessionId"],
    methods: {
        closeItem() {
            this.$emit("exit");
        },
    },
    template: `
      <div>
      <atx-step-title :title="$t('dual-key.step3.title')"></atx-step-title>
           <v-card class="mx-auto" max-width="95%" outlined
              style="margin-bottom:20px;"
              v-if="generationResult && generationResult.generationExecuted">
        <v-list-item three-line>
          <v-list-item-content>
            <div class="overline mb-4">
              {{ $t('dual-key.step3.saving.title') }}
            </div>
            <v-alert v-if="generationResult.generationStatus" icon="check"
                     prominent text type="success">
              Le bi-clé de chiffrement <strong>{{ getNameCertificat }}</strong> a bien été créé et enregistré dans le magasin de
              certificats de ce poste.
            </v-alert>
            <v-alert v-else icon="error" prominent text type="error">
              Le bi-clé de chiffrement <strong>{{ getNameCertificat }}</strong>, n'a pas
              été créé.
            </v-alert>
          </v-list-item-content>
        </v-list-item>
      </v-card>
     <v-card class="mx-auto" max-width="95%" outlined
              style="margin-bottom:20px;"
              v-if="generationResult">
        <v-list-item three-line>
          <v-list-item-content>
            <div class="overline mb-4">
              {{ $t('dual-key.step3.generation.title') }}
            </div>
            <v-alert v-if="generationResult.sendingMpeStatus" icon="check"
                     prominent text type="success">
              Le référencement dans la salle des marchés du bi-clé de chiffrement
              <strong>{{ getNameCertificat }}</strong> s'est terminé avec
              succès.
            </v-alert>
            <v-alert v-else icon="error" prominent text type="error">
              L'opération concernant le bi-clé de chiffrement
              <strong>{{ getNameCertificat }}</strong> a échoué.
            </v-alert>
          </v-list-item-content>
        </v-list-item>
      </v-card>


      <v-card class="mx-auto" max-width="95%" outlined
              style="margin-bottom:20px;" v-if="!generationResult">
        <v-list-item three-line>
          <v-list-item-content>
            <div class="overline mb-4">
            </div>
            <v-alert icon="error" prominent text type="error">
              Une erreur s'est produite, veuillez ré-essayer.
            </v-alert>
          </v-list-item-content>
        </v-list-item>
      </v-card>
      <v-btn @click="closeItem()" class="" color="#2660A4"
             style="margin:20px;float:right;"><span style="color:white;">Terminer et fermer</span>
      </v-btn>
      </div>
    `,
};
