import { TELECHARGEMENT_STATUS } from "../../../../core/enum/page-status.enum.js";

export default {
  data() {
    return {
      TELECHARGEMENT_STATUS,
    };
  },
  props: ["sessionId"],
  computed: {
    // atx_todo: use mapGetters
    downloadStatus() {
      return this.$store.getters['telechargementPliParPli/telechargementStatus'](this.sessionId);
    },
    downloadResults() {
      return this.$store.getters['telechargementPliParPli/telechargementResults'](this.sessionId);
    },
    titleTranslationKey() {
      return `telechargement-pli-par-pli.recuperation-enveloppes.download-finished.${
        TELECHARGEMENT_STATUS.FAILED === this.downloadStatus ? "failure" : "success"
      }`;
    },
  },
  methods: {
    closeItem() {
      this.$store.commit("application/closeItem", this.sessionId);
      // atx_todo: put the navigation in an action
      this.$router.push({
        name: "atx-home",
      });
    },
  },
  template: `
      <div>

        <div :style="{color: TELECHARGEMENT_STATUS.FAILED === downloadStatus ? 'red' : 'unset'}">
        {{ $t(titleTranslationKey) }}
        <ul>
          <li>
              {{ $t('telechargement-pli-par-pli.recuperation-enveloppes.download-finished.plis-to-download-number')}}
              {{downloadResults.plisToDownload}}
          </li>
          <li>
              {{ $t('telechargement-pli-par-pli.recuperation-enveloppes.download-finished.successful-plis-downloads-number')}}
              {{downloadResults.successfulPlisDownloads}}
          </li>
        </ul>
      </div>

      <v-divider class="my-4"></v-divider>

        <v-btn @click="closeItem()" color="#2660A4"><span style="color:white;">Terminer et fermer</span></v-btn>

      </div>
    `,
};
