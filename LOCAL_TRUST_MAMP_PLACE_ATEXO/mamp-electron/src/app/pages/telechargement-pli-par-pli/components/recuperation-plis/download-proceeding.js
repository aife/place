export default {
    props: ['sessionId'],
  computed: {
    plisTotalNumber() {
      // atx_todo: make selector
      const sessionData = this.$store.state.application.itemsActiveMap.get(
        this.sessionId
      );
      if (sessionData) return sessionData.data.responsesAnnoncesSize;
      else return null;
    },
    fichiersTotalNumber() {
      // atx_todo: make selector
      const sessionData = this.$store.state.application.itemsActiveMap.get(
        this.sessionId
      );
      if (sessionData) return sessionData.data.nombreTotalFichiers;
      else return 0;
    },
    currentDownload() {
      return this.$store.getters[
        "telechargementPliParPli/currentTelechargement"
      ](this.sessionId);
    },
    percentageFichiersLoaded() {
      if (this.fichiersTotalNumber === 0) return 0;
      return Math.round(
        (this.$store.getters["telechargementPliParPli/fichiersLoaded"](
          this.sessionId
        ) /
          this.fichiersTotalNumber) *
          100
      );
    },
    showLoadingState() {
      // atx_todo: simplify
      return (
        this.currentDownload &&
        this.currentDownload.enveloppe &&
        this.currentDownload.fichier
      );
    },
  },
  methods: {
    getLoadingSpeed() {
      return window.navigator.connection.downlink * 125;
    },
  },
  template: `
    <div>
    <v-card class="mx-auto" outlined>
      <v-list-item three-line>
        <v-list-item-content>
          <div class="overline mb-4">
            {{ $t('telechargement-pli-par-pli.recuperation-enveloppes.advancement') }}
          </div>
          <v-progress-linear height="25" color="#2660A4"
                             :value="percentageFichiersLoaded">
            <span
                style="color:white"><strong>{{ percentageFichiersLoaded }}
              %</strong></span>
          </v-progress-linear>
          <br><br>
          <v-list-item-subtitle>
            <i18n
                path="telechargement-pli-par-pli.recuperation-enveloppes.loading-speed"
                tag="p">
              <template #loadingSpeed>
                <span>{{ getLoadingSpeed() }}</span>
              </template>
            </i18n>
          </v-list-item-subtitle>
        </v-list-item-content>
      </v-list-item>
    </v-card>
    <v-divider class="my-4"></v-divider>
    <div style="display: flex">
      <span
          style="font-weight: bold">{{ $t('telechargement-pli-par-pli.recuperation-enveloppes.loading-element') }}</span>
      <span style="margin-left: 4px">
      <i18n v-if="showLoadingState"
            path="telechargement-pli-par-pli.recuperation-enveloppes.loading-state"
            tag="p">
        <template #pliIndex>
          <span>{{ currentDownload.pliIndex }}</span>
        </template>
        <template #pliTotal>
          <span>{{ plisTotalNumber }}</span>
        </template>
        <template #enveloppeIndex>
          <span>{{ currentDownload.enveloppe ? currentDownload.enveloppe.index : 0 }}</span>
        </template>
        <template #enveloppeTotal>
          <span>{{ currentDownload.enveloppe ? currentDownload.enveloppe.total : 0 }}</span>
        </template>
        <template #fichierIndex>
          <span>{{ currentDownload.fichier ? currentDownload.fichier.index : 0 }}</span>
        </template>
        <template #fichierTotal>
          <span>{{ currentDownload.fichier ? currentDownload.fichier.total : 0 }}</span>
        </template>
      </i18n>
      </span>
    </div>
  </div>
    `,
};
