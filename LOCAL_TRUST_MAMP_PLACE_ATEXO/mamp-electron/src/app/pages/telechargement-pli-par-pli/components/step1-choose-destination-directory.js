// atx_todo: rename to 'choose initial directory'
import AtxFolderSelector from "../../../shared/components/folder-selector.component.js";
import {TELECHARGEMENT_STATUS, TELECHARGEMENT_STATUS_PARAMS,} from "../../../core/enum/page-status.enum.js";

export default {
    props: ["sessionId"],
    computed: {
        stepStatus() {
            const downloadStatus = this.$store.getters[
                "telechargementPliParPli/telechargementStatus"
                ](this.sessionId);
            if (downloadStatus === TELECHARGEMENT_STATUS.PENDING) {
                return "active";
            } else return "done";
        },
        icon() {
            let icon = TELECHARGEMENT_STATUS_PARAMS.DEFAULT;
            if (this.stepStatus === "done") {
                icon = TELECHARGEMENT_STATUS_PARAMS.SUCCESSFUL;
            }
            return icon;
        },
        params() {
            // atx_todo: set as getter
            return this.$store.state.application.itemsActiveMap.get(this.sessionId)
                .data;
        },

        filePath() {
            return this.$store.getters["telechargementPliParPli/sessionPlisData"](
                this.sessionId
            ).initialDirectory;
        },
    },
    components: {
        "atx-folder-selector": AtxFolderSelector,
    },
    methods: {
        registerFolder(event) {
            this.$store.commit("telechargementPliParPli/setInitialDiretory", {
                sessionId: this.sessionId,
                directoryPath: event,
            });
        },
        launchDownload() {
            this.$store.dispatch(
                "telechargementPliParPli/initAndProcessTelechargements",
                {
                    sessionId: this.sessionId,
                    responsesAnnoncesSize: this.params.responsesAnnoncesSize,
                    urlServeurUpload: this.params.urlServeurUpload,
                    initialDirectory: this.filePath, // atx_todo: retrieve this in the store
                }
            );
        },
    },
    template: `
      <v-timeline-item
          :icon="icon.type"
          :color="icon.color"
      >
      <template v-slot:icon v-if="stepStatus === 'active'">
        <v-progress-circular :size="25" :width="3" color="#2660A4"
                             indeterminate></v-progress-circular>
      </template>

      <v-card class="elevation-2" color="#FAFAFA">
        <v-card-title class="headline-5">
          {{ $t('telechargement-pli-par-pli.choose-directory.title') }}
        </v-card-title>
        <v-card-text>
          {{ $t('telechargement-pli-par-pli.choose-directory.description') }}
          <div style="display: flex">
            <v-text-field solo :label="filePath" prepend-inner-icon="folder"
                          :readonly="true"
                          style="margin-right: 10px"></v-text-field>
            <atx-folder-selector
                v-if="stepStatus === 'active'"
                label="telechargement-pli-par-pli.choose-directory.choose-directory-button"
                @changeFolder="registerFolder"
                style="margin-top: 7px">
            </atx-folder-selector>
          </div>
          <div v-if="stepStatus === 'active'">
            <v-divider class="my-4"></v-divider>
            <v-btn @click="launchDownload" color="#2660A4"
                   :disabled="!filePath">
                    <span style="color:white;">
                        {{ $t('telechargement-pli-par-pli.choose-directory.launch-download-button') }}
                    </span>
            </v-btn>
          </div>
        </v-card-text>
      </v-card>
      </v-timeline-item>
    `,
};
