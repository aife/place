import AtxRecuperationPlisDownloadPending from "./recuperation-plis/download-pending.js";
import AtxRecuperationPlisDownloadProceeding from "./recuperation-plis/download-proceeding.js";
import AtxRecuperationPlisDownloadFinished from "./recuperation-plis/download-finished.js";
import {
  TELECHARGEMENT_STATUS,
  TELECHARGEMENT_STATUS_PARAMS,
} from "../../../core/enum/page-status.enum.js";

export default {
  data() {
    return {
      TELECHARGEMENT_STATUS,
    };
  },
  props: ["sessionId"],
  computed: {
    downloadStatus() {
      return this.$store.getters['telechargementPliParPli/telechargementStatus'](this.sessionId);
    },
    icon() {
      switch (this.downloadStatus) {
        case TELECHARGEMENT_STATUS.PENDING:
          return TELECHARGEMENT_STATUS_PARAMS.PENDING;
        case TELECHARGEMENT_STATUS.SUCCESSFUL:
          return TELECHARGEMENT_STATUS_PARAMS.SUCCESSFUL;
        case TELECHARGEMENT_STATUS.FAILED:
          return TELECHARGEMENT_STATUS_PARAMS.FAILED;
        default:
          return TELECHARGEMENT_STATUS_PARAMS.DEFAULT;
      }
    }
  },
  components: {
    "atx-recuperation-plis-download-pending": AtxRecuperationPlisDownloadPending,
    "atx-recuperation-plis-download-proceeding": AtxRecuperationPlisDownloadProceeding,
    "atx-recuperation-plis-download-finished": AtxRecuperationPlisDownloadFinished,
  },
  template: `
      <v-timeline-item
          :icon="icon.type"
          :color="icon.color"
      >
      <template v-slot:icon v-if="downloadStatus === TELECHARGEMENT_STATUS.LOADING">
        <v-progress-circular :size="25" :width="3" color="#2660A4"
                             indeterminate></v-progress-circular>
      </template>
      <v-card class="elevation-2" color="#FAFAFA">
        <v-card-title class="headline-4">
          {{ $t('telechargement-pli-par-pli.recuperation-enveloppes.title') }}
        </v-card-title>
        <v-card-text>
          <atx-recuperation-plis-download-pending 
            v-if="downloadStatus === TELECHARGEMENT_STATUS.PENDING"
            :sessionId="sessionId">
          </atx-recuperation-plis-download-pending>
          <atx-recuperation-plis-download-proceeding
            v-else-if="downloadStatus === TELECHARGEMENT_STATUS.LOADING"
            :sessionId="sessionId">
          </atx-recuperation-plis-download-proceeding>
          <atx-recuperation-plis-download-finished 
            v-else 
            :sessionId="sessionId">
          </atx-recuperation-plis-download-finished>
        </v-card-text>
      </v-card>
      </v-timeline-item>
    `,
};
