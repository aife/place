import AtxChooseDestinationDirectoryComponent from "./components/step1-choose-destination-directory.js";
import AtxRecuperationPlisComponent from "./components/step2-recuperation-plis.js";
import getFormatDate from "../../shared/pipe/date-time.pipe.js";
import { TELECHARGEMENT_STATUS } from "../../core/enum/page-status.enum.js";

export default {
  data() {
    return {
      sessionId: null,
      stepIndex: 0,
    };
  },
  computed: {
    startDate() {
      return getFormatDate(new Date());
    },
    plisNumber() {
      const sessionData = this.$store.state.application.itemsActiveMap.get(
        this.sessionId
      );
      if (sessionData) return sessionData.data.responsesAnnoncesSize;
      else return null;
    },
    storeStatus() {
      this.$store.getters["telechargementPliParPli/telechargementStatus"](
        this.sessionId
      );
    },
  },
  components: {
    "atx-choose-destination-directory": AtxChooseDestinationDirectoryComponent,
    "atx-recuperation-plis": AtxRecuperationPlisComponent,
  },
  mounted() {
    this.sessionId = this.$route.params.sessionId;
  },
  watch: {
    // atx_todo: destroy the component and the route - destroy this component from the store ?
    storeStatus(value) {
      if (value === TELECHARGEMENT_STATUS.CLOSED) {
        this.$destroy();
      }
    },
  },
  template: `
      <div class="page" style="padding: 20px">
      <div class="step--telechargement-pli-par-pli" v-if="!!sessionId">
        <v-card class="elevation-2" color="#FAFAFA">
          <v-card-title class="headline-5">
            {{ $t('telechargement-pli-par-pli.title') }}
          </v-card-title>
          <v-card-text>
            <strong>{{ $t('telechargement-pli-par-pli.start-date') }}</strong>
            {{ startDate }}<br/>
            <strong>{{ $t('telechargement-pli-par-pli.plis-number') }}</strong>
            {{ plisNumber }}<br/>
          </v-card-text>
        </v-card>
        <v-timeline align-top dense>
          <atx-choose-destination-directory
              :sessionId="sessionId"></atx-choose-destination-directory>
          <atx-recuperation-plis :sessionId="sessionId"></atx-recuperation-plis>
        </v-timeline>
      </div>
      </div>
    `,
};
