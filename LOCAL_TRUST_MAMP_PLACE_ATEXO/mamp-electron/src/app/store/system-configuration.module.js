const initialState = {
  configurations: [
    {
      key: "system",
      isValid: false,
      isLoaded: false,
    },
    {
      key: "application",
      isValid: false,
      isLoaded: false,
    },
    {
      key: "sequestre",
      isValid: false,
      isLoaded: false,
    },
    {
      key: "web",
      isValid: false,
      isLoaded: false,
    },
    {
      key: "server",
      isValid: false,
      isLoaded: false,
    },
  ],
};
import SystemConfigurationService from "../core/service/system-configuration.service.js";

export const systemConfiguration = {
  namespaced: true,
  state: initialState,
  actions: {
    initConfiguration({ commit }) {
      commit("initConfiguration");
    },
    version({commit}, params) {
      return SystemConfigurationService.getVersion(params.url, params.env, params.system).then(
          response => {
            return Promise.resolve(response);
          },
          error => {
            return Promise.reject(error);
          }
      );
    },
    getConfiguration({ commit }, params) {
      return SystemConfigurationService.getConfiguration(
        params.key,
        params.urlPubliqueServeurCrypto,
        params.browserName,
        params.browserVersion
      ).then((response) => {
        if (response) {
          commit("updateConfiguration", {
            ...response,
            key: params.key,
          });
        } else
          commit("updateConfiguration", {
            label: null,
            valid: false,
            key: params.key,
          });

        return Promise.resolve(response);
      });
    },
  },
  mutations: {
    initConfiguration(state) {
      state.configurations.forEach((value) => {
        value.isValid = false;
        value.isLoaded = false;
        value.label = null;
      });
    },
    updateConfiguration(state, params) {
      const item = state.configurations.find((item) => item.key === params.key);
      if (!item) {
        return;
      }
      item.isLoaded = true;
      item.label = params.label;
      item.isValid = params.valid;
    },
  },
};
