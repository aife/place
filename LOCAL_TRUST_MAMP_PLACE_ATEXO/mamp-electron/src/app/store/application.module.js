import MonitoringService from "../core/service/monitoring.service.js";

const initialState = {
    itemsActiveMap: new Map(),
    itemsActive: [],
    staticItemParams: new Map(),
    staticHeadersParams: null,
    isReady: false,
    proxy: null,
    userFolder: null,
    systemJava: null,
    params: {}
};

export const application = {
    namespaced: true,
    state: initialState,
    actions: {
        health({commit}) {
            return MonitoringService.health().then((response) => {
                if (response) {
                    commit("setReady", true);
                } else {
                    commit("setReady", false);
                }
                return Promise.resolve(response);
            });
        },
        session({commit}) {
            return MonitoringService.session().then((response) => {
                return Promise.resolve(response);
            });
        },
        addActiveItem({commit}, item) {
            commit("addActiveItem", item);
            return Promise.resolve(item);
        },
        setItemStatus({commit}, param) {
            commit("setItemStatus", param);
            return Promise.resolve(param);
        },
        closeItem({commit}, id) {
            commit("closeItem", id);
            return Promise.resolve(id);
        },
        setStaticItemParams({commit}, params) {
            commit("setStaticItemParams", params);
            return Promise.resolve(params);
        }, setProxy({commit}, params) {
            commit("setProxy", params);
            return Promise.resolve(params);
        }, userFolder({commit}, params) {
            commit("userFolder", params);
            return Promise.resolve(params);
        }, systemJava({commit}, params) {
            commit("systemJava", params);
            return Promise.resolve(params);
        },
        params({commit}, params) {
            commit("params", params);
            return Promise.resolve(params);
        }, download({commit}, url) {
            return window.location.assign(url);
        }

    },
    mutations: {
        addActiveItem(state, result) {
            if (state.itemsActiveMap.get(result.id)) {
                return;
            }
            result.startAt = new Date();
            state.itemsActiveMap.set(result.id, result);
            state.itemsActive = [...state.itemsActiveMap.values()];
        },
        closeItem(state, id) {
            state.itemsActiveMap.delete(id);
            state.itemsActive = [...state.itemsActiveMap.values()];
        },
        setItemStatus(state, param) {
            const item = state.itemsActiveMap.get(param.id);
            if (!item) return;
            item.isLoading = !param.status;
            if (!item.isLoading) {
                item.endAt = new Date();
            }
            item.status = param.status;
            state.itemsActive = [...state.itemsActiveMap.values()];
        },
        setStaticItemParams(state, params) {
            state.staticItemParams.set(params.id, params.data);
            state.staticHeadersParams = params.headers;
        },
        setReady(state, params) {
            state.isReady = params;
        }, setProxy(state, params) {
            state.proxy = params;
        }, userFolder(state, params) {
            state.userFolder = params;
        }, systemJava(state, params) {
            state.systemJava = params;
        }, params(state, params) {
            state.params = params;
        },
    },
};
