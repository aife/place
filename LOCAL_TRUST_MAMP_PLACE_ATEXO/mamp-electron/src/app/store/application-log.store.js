const Store = require("./store");
var pjson = require("../../package.json");

exports.applicationStore = new Store({
  configName: "application-info",
  defaults: {
    version: pjson.version,
    httpLogs: {
      list: [],
      creationDate: Date.now(),
    },
  },
});
