import {systemConfiguration} from "./system-configuration.module.js";
import {dechiffrement} from "./dechiffrement.module.js";
import {application} from "./application.module.js";
import {dualKey} from "./dual-key.module.js";
import {telechargementPliParPli} from "./telechargement-pli-par-pli.module.js";

export default new Vuex.Store({
    modules: {
        dualKey,
        dechiffrement,
        application,
        systemConfiguration,
        telechargementPliParPli
    },
});
