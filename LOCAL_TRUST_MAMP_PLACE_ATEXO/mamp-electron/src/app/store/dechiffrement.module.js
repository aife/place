const initialState = {
  ouvertureEnLigne: new Map(),
};

import DechiffrementService from "../core/service/dechiffrement.service.js";

export const dechiffrement = {
  namespaced: true,
  state: initialState,
  actions: {
    getOffres({ commit }, params) {
      return DechiffrementService.getOffres(
        params.sessionId,
        params.urlServeurUpload
      ).then((response) => {
        commit("setDechiffrementEnLigneParams", {
          sessionId: params.sessionId,
          key: "offres",
          value: response,
        });

        return Promise.resolve(response);
      });
    },
    getIdOffreQueue({ commit }, params) {
      return DechiffrementService.getIdOffreQueue(
        params.offres,
        params.sessionId,
        params.urlServeurUpload
      ).then((response) => {
        commit("setDechiffrementEnLigneParams", {
          sessionId: params.sessionId,
          key: "idOffreQueue",
          value: response,
        });
        return Promise.resolve(response);
      });
    },
    getEnveloppesProgress({ commit }, params) {
      return DechiffrementService.getEnveloppesProgress(
        params.idOffreQueue,
        params.urlServeurUpload
      ).then((response) => {
        commit("setDechiffrementEnLigneParams", {
          sessionId: params.sessionId,
          key: "progress",
          value: response,
        });
        return Promise.resolve(response);
      });
    },
    setParams({ commit }, params) {
      commit("setParams", params);
    },
  },
  mutations: {
    setDechiffrementEnLigneParams(state, result) {
      let dechifferementEnLigne = state.ouvertureEnLigne.get(result.sessionId);
      if (!dechifferementEnLigne) {
        dechifferementEnLigne = {};
        state.ouvertureEnLigne.set(result.sessionId, dechifferementEnLigne);
      }
      dechifferementEnLigne[result.key] = result.value;
    },
  },
};
