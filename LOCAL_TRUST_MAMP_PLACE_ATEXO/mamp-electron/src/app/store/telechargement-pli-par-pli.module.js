import TelechargementPliParPliService from "../core/service/telechargement-pli-par-pli.service.js";
import { TELECHARGEMENT_STATUS } from "../core/enum/page-status.enum.js";

// atx_todo: mutualize with the other one
export const DOWNLOAD_STEPS = {
  PENDING: "pending",
  LOADING: "loading",
  DONE: "done",
  CLOSED: "closed",
};

const registerSession = (state, sessionId) => {
  if (state[sessionId]) return;
  Vue.set(state, sessionId, {
    initialDirectory: null,
    successfulPlisDownloads: 0,
    // atx_todo: not needed !
    failedPlisDownloads: 0,
    fichiersLoaded: 0,
    currentDownload: null,
    downloadStep: DOWNLOAD_STEPS.PENDING,
  });
};

export const telechargementPliParPli = {
  namespaced: true,
  state: () => {},
  actions: {
    async initAndProcessTelechargements(
      { commit, dispatch },
      { sessionId, responsesAnnoncesSize, urlServeurUpload, initialDirectory }
    ) {
      const telechargementPliParPliService = new TelechargementPliParPliService();
      commit("changeDownloadStep", { sessionId, step: DOWNLOAD_STEPS.LOADING });
      commit("resetDownloadState", { sessionId });
      for (let index = 0; index < responsesAnnoncesSize; index++) {
        try {
          commit("initTotalEnveloppeAndFichiersNumber", sessionId);
          commit("setCurrentPliIndex", { sessionId, index: index });
          const enveloppes = (
            await telechargementPliParPliService.getEnveloppesPerPli(
              urlServeurUpload,
              sessionId,
              index
            )
          ).data;
          // atx_todo: put this in component const numberOfEnveloppes = Object.keys(resInit.data)
          // atx_todo: store in store 'au fur et à mesure' informations about 'plis' loading
          commit("setTotalEnveloppesNumber", {
            sessionId,
            total: Object.keys(enveloppes).length,
          });
          // atx_todo: create own 'forEach' ?
          const enveloppesArray = Object.values(enveloppes);
          let failedFichiers = 0;
          for (
            let enveloppeIndex = 0;
            enveloppeIndex < enveloppesArray.length;
            enveloppeIndex++
          ) {
            const fichiers = enveloppesArray[enveloppeIndex];
            commit("setCurrentEnveloppeIndex", {
              sessionId,
              index: enveloppeIndex,
            });
            commit("setTotalFichiersNumber", {
              sessionId,
              total: fichiers.length,
            });
            for (
              let fichierIndex = 0;
              fichierIndex < fichiers.length;
              fichierIndex++
            ) {
              const fichier = fichiers[fichierIndex];
              commit("setCurrentFichierIndex", {
                sessionId,
                index: fichierIndex,
              });
              try {
                await telechargementPliParPliService.processTelechargementFichier(
                  urlServeurUpload,
                  initialDirectory,
                  sessionId,
                  index,
                  fichier
                );
              } catch (e) {
                console.error(e);
                failedFichiers++;
              }
              commit("fichierLoaded", { sessionId });
            }
          }
          commit("pliLoaded", { sessionId, successful: failedFichiers === 0 });
        } catch (e) {
          // atx_todo: debug mode ?
          console.error(e);
          commit("pliLoaded", { sessionId, successful: false });
          // atx_todo: check that the loop keeps on
        }
      }
      dispatch("finishTelechargement", { sessionId, initialDirectory });
    },
    async finishTelechargement(
      { commit, getters },
      { sessionId, initialDirectory }
    ) {
      const telechargementPliParPliService = new TelechargementPliParPliService();
      try {
        // atx_todo: conditionner à la réussite de tous les plis ?
        await telechargementPliParPliService.finish(
          sessionId,
          initialDirectory
        );
      } catch (e) {
        // atx_todo: debug mode ?
        console.error(e);
        commit("changeDownloadStep", { sessionId, step: DOWNLOAD_STEPS.DONE });
        commit(
          "application/setItemStatus",
          {
            status: "error",
            id: sessionId,
          },
          { root: true }
        );
        return;
      }
      commit("changeDownloadStep", { sessionId, step: DOWNLOAD_STEPS.DONE });
      const isDownloadSuccessful =
        getters.telechargementStatus(sessionId) === "successful"
          ? "success"
          : "error";
      commit(
        "application/setItemStatus",
        {
          status: isDownloadSuccessful,
          id: sessionId,
        },
        { root: true }
      );
    },
  },
  mutations: {
    setInitialDiretory(state, { sessionId, directoryPath }) {
      registerSession(state, sessionId);
      state[sessionId].initialDirectory = directoryPath;
    },
    changeDownloadStep(state, { sessionId, step }) {
      registerSession(state, sessionId);
      state[sessionId].downloadStep = step;
    },
    resetDownloadState(state, { sessionId }) {
      registerSession(state, sessionId);
      state[sessionId].successfulPlisDownloads = 0;
      state[sessionId].failedPlisDownloads = 0;
      // atx_todo: use 'Vue.set'
      state[sessionId].currentDownload = {
        pliIndex: 0,
        enveloppe: {
          total: "-",
          index: "-",
        },
        fichier: {
          total: "-",
          index: "-",
        },
      };
    },
    setCurrentPliIndex(state, { sessionId, index }) {
      // atx_todo: add check ?
      state[sessionId].currentDownload.pliIndex = index + 1;
    },
    initTotalEnveloppeAndFichiersNumber(state, sessionId) {
      state[sessionId].currentDownload.enveloppe.total = "-";
      state[sessionId].currentDownload.enveloppe.index = "-";
      state[sessionId].currentDownload.fichier.total = "-";
      state[sessionId].currentDownload.fichier.index = "-";
    },
    setTotalEnveloppesNumber(state, { sessionId, total }) {
      // atx_todo: add check ?
      state[sessionId].currentDownload.enveloppe.total = total;
    },
    setCurrentEnveloppeIndex(state, { sessionId, index }) {
      // atx_todo: add check ?
      state[sessionId].currentDownload.enveloppe.index = index + 1;
    },
    setTotalFichiersNumber(state, { sessionId, total }) {
      // atx_todo: add check ?
      state[sessionId].currentDownload.fichier.total = total;
    },
    setCurrentFichierIndex(state, { sessionId, index }) {
      // atx_todo: add check ?
      state[sessionId].currentDownload.fichier.index = index + 1;
    },
    // atx-todo: rename fichierProceded
    fichierLoaded(state, { sessionId }) {
      state[sessionId].fichiersLoaded++;
    },
    // atx-todo: rename pliProceded
    pliLoaded(state, { sessionId, successful }) {
      // atx_todo: add check ?
      state[sessionId][
        successful ? "successfulPlisDownloads" : "failedPlisDownloads"
      ]++;
    },
    sessionClosed(state, { sessionId }) {
      state[sessionId].downloadStep = DOWNLOAD_STEPS.CLOSED;
    },
  },
  getters: {
    sessionPlisData: (state) => (sessionId) => {
      // atx_todo: fallback return {} or null ?
      return state[sessionId] || {};
    },
    fichiersLoaded: (state) => (sessionId) => {
      const sessionData = state[sessionId];
      if (sessionData) return sessionData.fichiersLoaded;
      else return 0;
    },
    currentTelechargement: (state) => (sessionId) => {
      // atx_todo: use sessionPlisData ?
      const sessionData = state[sessionId];
      if (sessionData) return sessionData.currentDownload;
      else return null;
    },
    telechargementStatus: (state) => (sessionId) => {
      const sessionData = state[sessionId];
      if (!sessionData) return TELECHARGEMENT_STATUS.PENDING;
      const downloadStep = sessionData.downloadStep;
      if (downloadStep === DOWNLOAD_STEPS.DONE) {
        if (sessionData.failedPlisDownloads > 0)
          return TELECHARGEMENT_STATUS.FAILED;
        else return TELECHARGEMENT_STATUS.SUCCESSFUL;
      } else return downloadStep;
    },
    telechargementResults: (state, getters, rootState) => (sessionId) => {
      // atx_todo: change return when download is not finished ?
      const plisData = state[sessionId];
      const successfulPlisDownloads = plisData
        ? plisData.successfulPlisDownloads
        : 0;
      const applicationData = rootState.application.itemsActiveMap.get(
        sessionId
      );
      const plisToDownload = applicationData
        ? applicationData.data.responsesAnnoncesSize
        : 0;
      return {
        successfulPlisDownloads,
        plisToDownload,
        downloadSuccessful: successfulPlisDownloads === plisToDownload,
      };
    },
  },
};
