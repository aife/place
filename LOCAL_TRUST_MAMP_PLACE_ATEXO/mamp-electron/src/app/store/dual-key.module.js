import CertificateService from "../core/service/certificate.service.js";

export class dualKeyState {
  constructor() {
    this.formData = new DualKeyFormState();
    this.selectedCertificate = null;
    this.mpeUrl = "";
    this.caUrl = "";
    this.sessionId = "";

    this.generationResult = null;
  }
}

export class DualKeyFormState {
  constructor() {
    this.alias = null;
    this.cnCleChiffrement = null;
    this.password = null;
    this.estCleChiffrementSecours = false;
    this.existingCert = false;
  }
}

export const dualKey = {
  namespaced: true,
  state: {
    dualsKeys: new Map(),
    provider: null,
    certificatesList: [],
  },
  actions: {
    init({ commit, rootState }, sessionId) {
      const infos = rootState.application.itemsActiveMap.get(sessionId).data;
      commit("init", { sessionId, infos });
    },
    getCertificatesList({ commit }) {
      return CertificateService.getAllCertificatesList().then((response) => {
        if (response) {
          commit("setCertificatesList", response);
        } else {
          commit("setCertificatesList", []);
        }
        return Promise.resolve(response);
      });
    },
    getProviderName({ commit }) {
      return CertificateService.getProviderName().then((response) => {
        if (response) {
          commit("setProviderName", response);
        } else {
          commit("setProviderName", []);
        }
        return Promise.resolve(response);
      });
    },
    postDualKeys({ commit }, sessionId) {
      let request = this.state.dualKey.dualsKeys.get(sessionId);
      return CertificateService.postDualKeys(
        request.selectedCertificate,
        request.mpeUrl,
        request.caUrl,
        request.sessionId,
        request.formData
      ).then((response) => {
        commit("setGenerationResult", {
          sessionId: sessionId,
          data: response.data,
        });
        return Promise.resolve(response);
      });
    },
  },
  mutations: {
    init(state, { sessionId, infos }) {
      const dualState = new dualKeyState();
      dualState.mpeUrl = infos.mpeUrl;
      dualState.caUrl = infos.caUrl ? infos.caUrl : infos.mpeUrl + "/crypto";
      dualState.sessionId = sessionId;
      dualState.formData.estCleChiffrementSecours =
        infos.estCleChiffrementSecours;
      state.dualsKeys.set(sessionId, dualState);
    },
    setCertificatesList(state, certificatesList) {
      state.certificatesList = certificatesList;
    },
    setSelectedCertificate(state, { certificateId, sessionId }) {
      const dualKey = state.dualsKeys.get(sessionId);
      dualKey.selectedCertificate = certificateId;
      state.dualsKeys.set(sessionId, { ...dualKey });
    },
    setProviderName(state, provider) {
      state.provider = provider;
    },
    setFormData(state, { formData, sessionId }) {
      const dualKey = state.dualsKeys.get(sessionId);
      dualKey.formData = formData;
      state.dualsKeys.set(sessionId, { ...dualKey });
    },
    setGenerationResult(state, { data, sessionId }) {
      const dualKey = state.dualsKeys.get(sessionId);
      dualKey.generationResult = data;
      state.dualsKeys.set(sessionId, { ...dualKey });
    },
  },
  getters: {
    selectedCertificate: (state) => (param) => {
      const request = state.dualsKeys.get(param);
      return !!request ? request.selectedCertificate : null;
    },
    form: (state) => (param) => {
      const request = state.dualsKeys.get(param);
      return !!request ? request.formData : null;
    },
    certificates: (state) => {
      return state.certificatesList;
    },
    provider: (state) => {
      return state.provider;
    },
  },
};
