import {
    mockGetEnveloppesPerPli,
    mockProcessTelechargementFichier,
    mockFinish
} from '../core/service/telechargement-pli-par-pli.service.js';
import {DOWNLOAD_STEPS, telechargementPliParPli} from './telechargement-pli-par-pli.module';
jest.mock('../core/service/telechargement-pli-par-pli.service.js')

// atx_todo: harmonize naming
describe('initAndProcessTelechargements action', () => {

    const commit = jest.fn(),
        dispatch = jest.fn()
    const sessionId = "123456",
        responsesAnnoncesSize = 3,
        urlServeurUpload = "url/serveur/upload",
        initialDirectory = "initial/directory"

    beforeAll(async () =>  {
        await telechargementPliParPli.actions.initAndProcessTelechargements(
            {commit, dispatch},
            {sessionId, responsesAnnoncesSize, urlServeurUpload, initialDirectory}
        )
    })
    
    it('it should mark the current step as "loading"', async () => {
        const changeDownloadStepCommits = commit.mock.calls.filter(argsArray => argsArray[0] === "changeDownloadStep")
        expect(changeDownloadStepCommits.length).toBe(1)
        expect(changeDownloadStepCommits[0][1]).toEqual({sessionId, step: DOWNLOAD_STEPS.LOADING})
    })

    it('should fetch enveloppes according to the "responsesAnnoncesSize" parameter', () => {    
        expect(mockGetEnveloppesPerPli).toHaveBeenCalledTimes(3);
        expect(mockGetEnveloppesPerPli.mock.calls[0][0]).toEqual(urlServeurUpload);
        expect(mockGetEnveloppesPerPli.mock.calls[0][1]).toEqual(sessionId);
        expect(mockGetEnveloppesPerPli.mock.calls[1][2]).toEqual(1);   // indexPli
    })

    it('should fetch fichiers according to the enveloppes previously fetch', () => { 
        expect(mockProcessTelechargementFichier).toHaveBeenCalledTimes(12);
        expect(mockProcessTelechargementFichier.mock.calls[0][0]).toEqual(urlServeurUpload);
        expect(mockProcessTelechargementFichier.mock.calls[0][1]).toEqual(initialDirectory);
        expect(mockProcessTelechargementFichier.mock.calls[0][2]).toEqual(sessionId);
        expect(mockProcessTelechargementFichier.mock.calls[5][3]).toEqual(0);   // indexPli
    })

    it('should commit each current pli, enveloppe and fichier when it is proceeded', () => { 
        const commitArgs = commit.mock.calls

        const setCurrentPliIndexCommits = commitArgs.filter(argsArray => argsArray[0] === "setCurrentPliIndex")
        expect(setCurrentPliIndexCommits.length).toBe(3)
        expect(setCurrentPliIndexCommits[1][1]).toEqual({sessionId, index: 1})

        const setCurrentEnveloppeIndexCommits = commitArgs.filter(argsArray => argsArray[0] === "setCurrentEnveloppeIndex")
        expect(setCurrentEnveloppeIndexCommits.length).toBe(6)
        expect(setCurrentEnveloppeIndexCommits[0][1]).toEqual({sessionId, index: 0})
        expect(setCurrentEnveloppeIndexCommits[3][1]).toEqual({sessionId, index: 0})
    })

    it('should commit each fichier once proceeded (successful or failed)', () => { 
        const commitArgs = commit.mock.calls
        const fichierLoadedCommits = commitArgs.filter(argsArray => argsArray[0] === "fichierLoaded")
        expect(fichierLoadedCommits.length).toBe(12)
    })

    it('should commit each pli as "successful" or "failed" once proceeded', () => { 
        const commitArgs = commit.mock.calls
        const pliLoadedCommits = commitArgs.filter(argsArray => argsArray[0] === "pliLoaded")
        expect(pliLoadedCommits.length).toBe(3)
        expect(pliLoadedCommits[0][1]).toEqual({sessionId, successful: true})
        expect(pliLoadedCommits[1][1]).toEqual({sessionId, successful: false})   // this pli has some files in error
        expect(pliLoadedCommits[2][1]).toEqual({sessionId, successful: false})   // an error occured for this pli while fetching the enveloppe
    })

    // should trigger the last action
    it('should dispatch the "finishTelechargement" action', () => {
        expect(dispatch.mock.calls[0][0]).toBe('finishTelechargement')
        expect(dispatch.mock.calls[0][1]).toEqual({sessionId, initialDirectory})
    })

})

describe('finishTelechargements action', () => {
    
    const commit = jest.fn(),
        getters = {
        telechargementStatus: jest.fn()
        }
    const sessionId = "123456",
        initialDirectory = "initial/directory"

    beforeEach(async () =>  {
        commit.mockClear()
        getters.telechargementStatus.mockClear()
    })

    it('should trigger the "finish" request', async () => {
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )
        expect(mockFinish).toBeCalledTimes(1)
        expect(mockFinish.mock.calls[0][0]).toBe(sessionId)
        expect(mockFinish.mock.calls[0][1]).toBe(initialDirectory)
    })
    
    it('if the "finish request" fails, should update the session item stored in the "application" module with the status "error" and set the current step to "done"', async () => {
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )
        const commitArgs = commit.mock.calls

        const changeDownloadStepCommits = commitArgs.filter(argsArray => argsArray[0] === "changeDownloadStep")
        expect(changeDownloadStepCommits.length).toBe(1)
        expect(changeDownloadStepCommits[0][1]).toEqual({sessionId, step: DOWNLOAD_STEPS.DONE})

        const applicationSetItemStatusCommits = commitArgs.filter(argsArray => argsArray[0] === "application/setItemStatus")
        expect(applicationSetItemStatusCommits.length).toBe(1)
        expect(applicationSetItemStatusCommits[0][1]).toEqual({
            status: 'error',
            id: sessionId
        })
    })

    it('should check if all files have been loaded successfully', async () => {
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )
        expect(getters.telechargementStatus).toBeCalledTimes(1)
        expect(getters.telechargementStatus.mock.calls[0][0]).toBe(sessionId)
    })

    it('should update the session item stored in the "application" module according to the previous check result', async () => {
        getters.telechargementStatus = jest.fn().mockReturnValue('successful')
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )        
        let applicationSetItemStatusCommits = commit.mock.calls.filter(argsArray => argsArray[0] === "application/setItemStatus")
        expect(applicationSetItemStatusCommits.length).toBe(1)
        expect(applicationSetItemStatusCommits[0][1]).toEqual({
            status: 'success',
            id: sessionId
        })
        commit.mockClear()
        getters.telechargementStatus = jest.fn().mockReturnValue('error')
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )
        applicationSetItemStatusCommits = commit.mock.calls.filter(argsArray => argsArray[0] === "application/setItemStatus")
        expect(applicationSetItemStatusCommits.length).toBe(1)
        expect(applicationSetItemStatusCommits[0][1]).toEqual({
            status: 'error',
            id: sessionId
        })
    })

    it('it should mark the current step as "done"', async () => {
        await telechargementPliParPli.actions.finishTelechargement(
            {commit, getters}, 
            {sessionId, initialDirectory}
        )
        const changeDownloadStepCommits = commit.mock.calls.filter(argsArray => argsArray[0] === "changeDownloadStep")
        expect(changeDownloadStepCommits.length).toBe(1)
        expect(changeDownloadStepCommits[0][1]).toEqual({sessionId, step: DOWNLOAD_STEPS.DONE})
    })
})