const moment = require("moment");

function getFormatDate() {
  return function (value) {
    if (value) {
      return moment(String(value)).local().format("DD/MM/YYYY - HH:mm:ss");
    }
  };
}

export default getFormatDate();
