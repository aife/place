import AtxSubMenuComponent from "./sub-menu.component.js";
import { createLocalVue, mount } from "@vue/test-utils";

describe("AtxSubMenuComponent", () => {
  let wrapper;
  beforeAll(() => {
    wrapper = mount(AtxSubMenuComponent, {
      localVue: createLocalVue(),
      propsData: {
        title: "test-title",
        description: "test-description",
        noItemMessage: "no-test",
        items: [],
      },
    });
  });
  it("should create", async (done) => {
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.vm.title).toBe("test-title");
    expect(wrapper.vm.description).toBe("test-description");
    done();
  });
});
