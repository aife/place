import AtxTitleComponent from "./title.component.js";
import { config, createLocalVue, mount } from "@vue/test-utils";

describe("TitleComponent", () => {
  let wrapper;
  beforeAll(() => {
    config.showDeprecationWarnings = false;
    wrapper = mount(AtxTitleComponent, createLocalVue());
  });

  it("Title component initialized", async (done) => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.classes("titre")).toBe(true);
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
    done();
  });

  it("should show Test title", async (done) => {
    wrapper.setProps({
      title: "test-title",
      description: "test-description",
    });

    expect(wrapper.vm.title).toBe("test-title");
    expect(wrapper.vm.description).toBe("test-description");
    done();
  });
});
