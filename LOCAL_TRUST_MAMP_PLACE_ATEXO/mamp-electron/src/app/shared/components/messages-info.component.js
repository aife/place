import getFormatDate from "../pipe/date-time.pipe.js";

export default {
    name: "messagesInfo",
    props: {
        messages: Array,
        show: Boolean
    },
    data() {
        return {
            onlyError: false
        }
    },
    filters: {
        formatDate(value) {
            return getFormatDate(value);
        },
    },
    computed: {
        messageList() {
            return !this.onlyError ? this.messages : this.messages.filter(item => item.status === 'red')
        },
        height() {
            return Math.min(300, 40 * (this.messageList.length + 1))
        }
    },
    template: `
          <div v-show="show" style="margin: 10px">
                <v-virtual-scroll
                        :item-height="40"
                        :items="messageList"
                        v-bind:height="height">
                    <template v-slot:default="{ item }">
                        <v-list-item>
                            <v-chip
                                    class="ma-2 font-size-12-pt"
                                    outlined
                                    :color="item.status"
                            >
                                <v-icon left>
                                    {{item.icon}}
                                </v-icon>
                                {{ item.date | formatDate }} |
                                 <strong style="margin-left: 5px;margin-right: 5px;">{{$t('message')}}</strong>
                                {{$t(item.message)}}
                                <span v-if="item.nom" v-bind:title="item.dossier">  <strong style="margin-left: 5px;margin-right: 5px;">{{$t('file')}}</strong>  {{item.nom}} | </span>
                            </v-chip>
                        </v-list-item>
                    </template>
                </v-virtual-scroll>

            </div>`,
};
