export default {
  props: [
    "showPrevious",
    "showNext",
    "showFinish",
    "tooltipMessage",
    "nextDisabled",
  ],
  template: `
      <div class="NextPreviousFinish">
        <div class="row row--bottom">
            <div class="col-lg-6" v-if="showPrevious">
                <v-btn small text @click="$emit('previous')"
                       class="text--bold previous-btn">
                    <v-icon dark>chevron_left</v-icon>
                    {{$t('previous-step')}}

                </v-btn>
            </div>
            <div class="col-lg-6" v-if="showNext">
                <v-btn small v-if="!nextDisabled"
                       color="primary"
                       @click="$emit('next')"
                       class="text--bold next-btn"
                >
                    {{$t('next-step')}}

                    <v-icon dark>chevron_right</v-icon>
                </v-btn>
                <v-tooltip top v-else>
                    <template
                            v-slot:activator="{ on, attrs }">
                        <v-btn small
                               v-bind="attrs"
                               v-on="on"
                               class="text--bold next-btn disabled-btn"
                        >
                            {{$t('next-step')}}

                            <v-icon dark>chevron_right
                            </v-icon>
                        </v-btn>
                    </template>
                    <span>{{tooltipMessage}}</span>
                </v-tooltip>
            </div>
            <div class="col-lg-6" v-if="showFinish">
                <v-btn small color="primary"
                       @click="$emit('finish')"
                       class="text--bold  next-btn">
                    <v-icon dark left>check</v-icon>
                    {{$t('finish-and-close')}}
                </v-btn>
            </div>
        </div>
      </div>
    `,
};
