import AtxNextPreviousFinishComponent from "./next-previous-finish.component.js";
import { createLocalVue, mount, config } from "@vue/test-utils";
import Vuex from "vuex";

let localVue = createLocalVue();
import Vuetify from "vuetify";
localVue.use(Vuex);

describe("NextPreviousFinishComponent", () => {
  let wrapper;
  let actions;
  let state;

  beforeAll(() => {
    state = {
      uploadedFiles: [],
    };

    actions = {
      getCertificateList: jest.fn(),
    };

    wrapper = mount(AtxNextPreviousFinishComponent, { localVue });
  });

  it("NextPreviousFinish component initialized", () => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();
    expect(wrapper.find(".NextPreviousFinish")).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
  });

  it("NextPreviousFinish button defined", () => {
    const button = wrapper.find(".v-btn");
    expect(button).toBeDefined();
  });
});
