const { ipcRenderer } = require("electron");
export default {
  name: "folderSelector",
  props: {
    disabled: Boolean,
    label: String
  },
  data() {
    return {
      isInstanceDialogOpen: false,
    };
  },
  mounted() {
    ipcRenderer.on("close-folder-dialog", (event, selectedFolder) => {
      // this event being triggered globally, this is to scope the event emission to
      // the component instance
      if (this.isInstanceDialogOpen) {
        this.$emit("changeFolder", selectedFolder);
        this.isInstanceDialogOpen = false;
      }
    });
  },
  methods: {
    openDialog() {
      this.isInstanceDialogOpen = true;
      ipcRenderer.send("selectDirectory");
    },
  },
  template: `
      <div>
      <v-btn :disable="disabled" @click="openDialog"><span>{{ $t(label) }}</span></v-btn>
      </div>`,
};
