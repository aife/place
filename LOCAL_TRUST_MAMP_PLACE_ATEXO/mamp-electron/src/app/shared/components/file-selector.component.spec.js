import AtxFileSelectorComponent from "./file-selector.component.js";
import { createLocalVue, mount } from "@vue/test-utils";
import Vuex from "vuex";

let localVue = createLocalVue();
localVue.use(Vuex);

describe("AtxSelectFileToSignComponent", () => {
  let wrapper;
  let actions;
  let state;

  beforeAll(() => {
    state = {
      uploadedFiles: [],
    };

    actions = {
      getCertificateList: jest.fn(),
    };

    wrapper = mount(AtxFileSelectorComponent, { localVue });
  });

  it("File selector component initialized", async (done) => {
    expect(wrapper.element.tagName).toBe("DIV");
    expect(wrapper.html()).toBeDefined();

    const title = wrapper.find('input[type="file"]');
    expect(wrapper.find(".file-select")).toBeDefined();
    expect(title.html()).toBeDefined();
    expect(wrapper.vm.title).toBeUndefined();
    expect(wrapper.vm.description).toBeUndefined();
    done();
  });
});
