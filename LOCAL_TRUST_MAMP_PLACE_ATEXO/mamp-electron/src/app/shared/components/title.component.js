export default {
  props: ["title", "description"],
  template: `
        <div class="titre">
            <div class="atexo-title">{{title}}</div>
            <div class="atexo-description">{{description}}
            </div>

        </div>`,
};
