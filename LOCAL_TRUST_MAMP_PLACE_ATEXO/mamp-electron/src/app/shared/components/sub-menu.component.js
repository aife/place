import AtxTitleComponent from "./title.component.js";
import getFormatDate from "../pipe/date-time.pipe.js";

export default {
    components: {"atx-title": AtxTitleComponent},
    data() {
        return {
            model: -1,
        };
    },
    filters: {
        formatDate(value) {
            return getFormatDate(value);
        },
    },
    props: {
        title: String,
        description: String,
        noItemMessage: String,
        items: Array,
    },
    computed: {
        activeTab: {
            get() {
                return this.model;
            },
            set(val) {
                this.model = val;
            },
        },
    },
    watch: {
        $route(to, from) {
            this.activeTab = this.items.findIndex(
                (item) => this.getLocation(item) === this.$router.currentRoute.path
            );
        },
    },
    mounted() {
        this.activeTab = this.items.findIndex(
            (item) => this.getLocation(item) === this.$router.currentRoute.path
        );
    },
    methods: {
        navigate(event) {
            this.$router.push(this.getLocation(this.items[event]));
        },
        getLocation(item) {
            return !!item && item.id !== item.router
                ? "/" + item.router + "/" + item.id
                : "/" + item.router;
        },
    },
    template: `
        <div class="sub-menu elevation-4">
            <atx-title :title="title"
                       :description="description"></atx-title>
            <div class="liste">

                <v-list v-if="items?.length > 0" dense
                        rounded>
                    <v-list-item-group @change="navigate" v-model="activeTab">
                        <v-list-item
                                v-for="item in items"
                                :key="item.id"
                                link
                        >
                            <v-list-item-icon>
                                <div class="loader-badge">
                                    <v-progress-circular
                                            v-if="item.isLoading && item.withBadge"
                                            indeterminate
                                            color="primary"
                                            :size="18"
                                    ></v-progress-circular>
                                </div>
                                <v-badge
                                        v-if="!item.isLoading && item.withBadge"
                                        bordered
                                        :color="item.status ==='success'? 'green':'red'"
                                        :icon="item.status ==='success'?'check': 'priority_high'"
                                        overlap
                                >
                                    <template v-slot:icon>
                                        <v-progress-circular :size="25"
                                                             :width="3"
                                                             color="#2660A4"
                                                             indeterminate></v-progress-circular>
                                    </template>
                                    <v-icon>{{ item.icon }}</v-icon>
                                </v-badge>
                                <v-icon v-else>{{ item.icon }}</v-icon>

                            </v-list-item-icon>

                            <v-list-item-content>
                                <v-list-item-title>
                                    <strong>{{ $t(item.title) }}</strong>
                                    <v-list-item-subtitle
                                            v-if="item.startAt || item.endAt"
                                            style="margin-top:3px;">
                                        <div v-if="item.startAt">
                                            <i>{{ $t('startAt') }} {{ item.startAt | formatDate }}</i>
                                        </div>
                                        <div v-if="item.endAt">
                                            <i>{{ $t('endAt') }} {{ item.endAt | formatDate }}</i>
                                        </div>
                                    </v-list-item-subtitle>
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                    </v-list-item-group>
                </v-list>
                <div v-else class="atexo-no-item">{{ $t(noItemMessage) }}</div>
            </div>
        </div>`,
    style: `
     #sub-menu {
        background: #ffffff;
        box-shadow: 0 2px 4px 0 hsla(0, 0%, 0%, 0.5);
        margin-right: 0;
        margin-top: 20px;
        margin-left: 20px;
        margin-bottom: 20px;
    }`,
};
