export default {
    name: "fileSelector",
    props: {
        multiple: Boolean,
        disabled: Boolean,
        label: String
    },
    template: `
        <div>
            <label class="file-select"
                   style="margin-top: 10px; margin-bottom: 10px;">
                <div :class="disabled?'disabled-button':'select-button'">
                    <span> <v-icon dark left>note_add</v-icon>
                        {{label}}</span>
                </div>
                <input type="file" :multiple="multiple" :disabled="disabled"
                       @change="$emit('changeFile', $event.target.files)"/>
            </label>
        </div>`,
};
