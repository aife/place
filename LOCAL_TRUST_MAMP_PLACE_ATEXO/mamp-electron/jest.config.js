module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  moduleFileExtensions: ["js", "json"],
  rootDir: "./",
  coverageDirectory: "coverage",
  collectCoverageFrom: ["src/**/*.{js,jsx,ts,tsx}", "!<rootDir>/node_modules/"],
  transform: {
    "^.+\\.js$": "<rootDir>node_modules/babel-jest",
  },
  testMatch: ["**/*.spec.js"],
  testResultsProcessor: "jest-sonar-reporter",
};
