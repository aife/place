mvn clean deploy -DskipTests -P env_place_dev,system_win,place || true
mvn clean deploy -DskipTests -P env_place_rec,system_win,place || true
mvn clean deploy -DskipTests -P env_place_ecole,system_win,place || true
mvn clean deploy -DskipTests -P env_place_pre_prod,system_win,place || true
mvn clean deploy -DskipTests -P env_place_prod,system_win,place || true
mvn clean deploy -DskipTests -P env_place_securisation,system_win,place || true
mvn clean deploy -DskipTests -P env_place_test,system_win,place || true
