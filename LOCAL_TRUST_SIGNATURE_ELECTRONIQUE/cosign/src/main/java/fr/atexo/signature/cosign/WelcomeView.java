/*
 * WelcomeView.java
 *
 * Created on __DATE__, __TIME__
 */

package fr.atexo.signature.cosign;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.Locale;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;
import fr.atexo.signature.commun.util.I18nUtil;

/**
 *
 * @author  __USER__
 */
public class WelcomeView extends javax.swing.JFrame {

	/** Creates new form WelcomeView */
	public WelcomeView() {
		initComponents();
		windowDimension();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		frIcon = new javax.swing.JLabel();
		enIcon = new javax.swing.JLabel();
		esIcon = new javax.swing.JLabel();
		itIcon = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setMinimumSize(new java.awt.Dimension(510, 360));
		//setUndecorated(true);
		setMaximumSize(new java.awt.Dimension(510, 360));
		setResizable(false);
        setTitle(I18nUtil.get("LOCAL_TRUST_COSIGN"));
		setBackground(new java.awt.Color(255, 255, 255));
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("/images/signature.gif")));
		
		frIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/flag-fr.png"))); // NOI18N
		frIcon.setToolTipText(I18nUtil.get("FRENCH"));
		frIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		frIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				frIconMouseClicked(evt);
			}
		});
		getContentPane().add(frIcon);
		frIcon.setBounds(230, 270, 20, 15);

		enIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/flag-en.png"))); // NOI18N
		enIcon.setToolTipText(I18nUtil.get("ENGLISH"));
		enIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		enIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				enIconMouseClicked(evt);
			}
		});
		getContentPane().add(enIcon);
		enIcon.setBounds(250, 270, 20, 15);
		esIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/flag-es.png"))); // NOI18N
		esIcon.setToolTipText(I18nUtil.get("SPANICH"));
		esIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		esIcon.setBackground(new java.awt.Color(255, 255, 255));
		esIcon.setVisible(false);
		esIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				esIconMouseClicked(evt);
			}
		});
		getContentPane().add(esIcon);
		esIcon.setBounds(220, 250, 20, 15);

		itIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/flag-it.png"))); // NOI18N
		itIcon.setToolTipText(I18nUtil.get("ITALIAN"));
		itIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		itIcon.setBackground(new java.awt.Color(255, 255, 255));
		itIcon.setVisible(false);
		itIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				itIconMouseClicked(evt);
			}
		});
		getContentPane().add(itIcon);
		itIcon.setBounds(280, 250, 20, 15);

		jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/header-bk.gif"))); // NOI18N
		jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/images/atexo.gif")));
		getContentPane().add(jLabel7);
		getContentPane().add(jLabel5);
		getContentPane().add(jLabel2);
		getContentPane().add(jLabel3);
		getContentPane().add(jLabel6);
		getContentPane().add(jLabel4);
		
		getContentPane().add(jLabel1);
		
		jLabel2.setBounds(0, 0, 230, 80);
		jLabel3.setBounds(70, 100, 370, 80);
		jLabel3.setText(I18nUtil.get("LOCAL_TRUST_COSIGN"));
		jLabel3.setFont(new Font("Tahoma", Font.PLAIN, 36));
		jLabel3.setForeground(new java.awt.Color(0, 102, 153));
		jLabel4.setBounds(190, 300, 130, 14);
		jLabel4.setText(I18nUtil.get("LOCAL_TRUST_COSIGN_VERSION"));
		jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 11));
		jLabel4.setForeground(new java.awt.Color(0, 102, 153));
		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/images/quitter.gif")));
		jLabel5.setBounds(440, 10, 60, 20);
		jLabel5.setText(I18nUtil.get("FERMER"));
		jLabel5.setFont(new Font("Tahoma", Font.BOLD, 11));
		jLabel5.setForeground(new java.awt.Color(0, 51, 102));
		jLabel6.setFont(new Font("Tahoma", Font.PLAIN, 11));
		jLabel6.setForeground(new java.awt.Color(0, 102, 153));
		jLabel6.setBounds(190, 190, 370, 80);
		jLabel6.setText(I18nUtil.get("WELCOME_MESSAGE_EXPLICATION_1"));
		jLabel7.setFont(new Font("Tahoma", Font.PLAIN, 11));
		jLabel7.setForeground(new java.awt.Color(0, 102, 153));
		jLabel7.setBounds(90, 210, 370, 80);
		jLabel7.setText(I18nUtil.get("WELCOME_MESSAGE_EXPLICATION_2"));
		jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jLabel5MouseClicked(evt);
			}
		});
		jLabel1.setBounds(0, 0, 510, 360);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents
	private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {
		System.exit(0);
	}
	private void itIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in italian");
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
                Locale locale = I18nUtil.toLocale("it");
                Locale.setDefault(locale);
                CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
			}

		});
		this.dispose();
	}

	private void esIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in spanish");
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
                Locale locale = I18nUtil.toLocale("es");
                Locale.setDefault(locale);
                CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
			}

		});
		this.dispose();
	}

	private void enIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in english");
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
                Locale locale = I18nUtil.toLocale("en");
                Locale.setDefault(locale);
                CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
			}

		});
		this.dispose();
	}

	private void frIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in french");
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
                Locale locale = I18nUtil.toLocale("fr");
                Locale.setDefault(locale);
                CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
			}

		});
		this.dispose();
	}
	

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new WelcomeView().setVisible(true);
				try {
                    PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
                    UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
				} catch (UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
			}
			
		});
	}
	
	private void windowDimension() {
		Toolkit kit = this.getToolkit();
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

		Dimension d = kit.getScreenSize();
		int max_width = (d.width - in.left - in.right);
		int max_height = (d.height - in.top - in.bottom);

		this.setSize(Math.min(max_width, 492), Math.min(max_height, 282));//whatever size you want but smaller the insets
		this.setLocation((int) (max_width - this.getWidth()) / 2,
				(int) (max_height - this.getHeight()) / 2);
	}

	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel enIcon;
	private javax.swing.JLabel esIcon;
	private javax.swing.JLabel frIcon;
	private javax.swing.JLabel itIcon;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	// End of variables declaration//GEN-END:variables

}