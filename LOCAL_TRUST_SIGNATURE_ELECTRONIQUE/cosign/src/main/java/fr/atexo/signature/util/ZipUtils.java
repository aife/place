package fr.atexo.signature.util;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Enumeration;
import java.util.List;

public abstract class ZipUtils {

    public static File zip(List<File> fichiers, File repertoire, String nomArchive) throws IOException, ArchiveException {

        File fichierArchiveZip = null;
        if (!repertoire.exists()) {
            repertoire.mkdirs();
        }

        if (fichiers != null && !fichiers.isEmpty()) {

            fichierArchiveZip = new File(repertoire, nomArchive);
            if (fichierArchiveZip.exists()) {
                FileUtils.deleteQuietly(fichierArchiveZip);
            }

            FileOutputStream zipOutputStream = new FileOutputStream(fichierArchiveZip);
            ArchiveOutputStream archiveOutputStream = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, zipOutputStream);

            for (File fichier : fichiers) {
                FileInputStream fileInputStream = null;
                try {
                    ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(fichier, fichier.getName());
                    archiveOutputStream.putArchiveEntry(zipArchiveEntry);
                    fileInputStream = new FileInputStream(fichier);
                    IOUtils.copy(fileInputStream, archiveOutputStream);
                    archiveOutputStream.closeArchiveEntry();
                } finally {
                    IOUtils.closeQuietly(fileInputStream);
                }
            }

            archiveOutputStream.finish();
            zipOutputStream.close();
        }

        return fichierArchiveZip;

    }

    public static File unzip(File fichierZip, File repertoireDestination) throws IOException {

        ZipFile zipFile = null;
        try {

            zipFile = new ZipFile(fichierZip);

            if (!repertoireDestination.exists()) {
                repertoireDestination.mkdirs();
            }

            Enumeration<ZipArchiveEntry> fichiers = zipFile.getEntries();
            while (fichiers.hasMoreElements()) {
                ZipArchiveEntry zipArchiveEntry = fichiers.nextElement();
                String nomFichier = zipArchiveEntry.getName();
                File fichierDestination = new File(repertoireDestination, nomFichier);

                FileOutputStream fichierDestinationOutputStream = null;
                InputStream contenuFichierZip = null;
                try {
                    fichierDestinationOutputStream = new FileOutputStream(fichierDestination);
                    contenuFichierZip = zipFile.getInputStream(zipArchiveEntry);
                    IOUtils.copy(contenuFichierZip, fichierDestinationOutputStream);
                } finally {
                    IOUtils.closeQuietly(contenuFichierZip);
                    IOUtils.closeQuietly(fichierDestinationOutputStream);
                }
            }

            return repertoireDestination;

        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
