package fr.atexo.signature.cosign;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import net.iharder.Base64;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;
import fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.securite.processor.signature.InfosVerificationCertificat;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.securite.provider.pkcs11.Pkcs11Handler;
import fr.atexo.signature.commun.securite.provider.pkcs12.Pkcs12Handler;
import fr.atexo.signature.commun.securite.signature.SignaturePades;
import fr.atexo.signature.commun.securite.signature.SignaturePkcs7;
import fr.atexo.signature.commun.securite.signature.SignatureXades;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.securite.signature.VisualSignatureRectangle;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbPkcs11Util;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.commun.util.io.HashUtil;
import fr.atexo.signature.commun.util.io.XMLUtil;
import fr.atexo.signature.gui.provider.magasin.MagasinCertificateUiService;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateEvent;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateListener;
import fr.atexo.signature.gui.provider.pkcs12.Pkcs12CertificateUiService;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateEvent;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateListener;
import fr.atexo.signature.util.FileManipulation;
import fr.atexo.signature.util.ZipUtils;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;

/**
 * @author __USER__
 */
public class CosignForm extends javax.swing.JFrame implements MagasinCertificateListener, Pkcs12CertificateListener {

	private final TypeOs typeOs;

	private final TypeProvider typeProvider;

	private final TypeAlgorithmHash typeAlgorithmHash = TypeAlgorithmHash.SHA1;

	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

	private static Pkcs11LibsType pkcs11LibsType;

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11LibsType = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String pathfileToSign = "";
	private String nameFileToSign = "";
	private String nameFileToVerSign = "";
	private String pathFileToVerSign = "";
	private String nameFileToVeriSign = "";
	private String absolutePathToCoSign = "";
	private String fileNameOngExport = "";
	private String tmpfileNameOngExport = "";
	private String pathfileToExport = "";
	private String nameFileToExport = "";
	private final String zipCosignName = "";
	private final String zipCosignNameExt = "";
	private String tempDir = "";
	private String fileNameOngCosign = "";
	private String fileNameOngSignPdf = "";
	private String tmpfileNameOngCosign = "";
	private String tmpfileNameOngVer = "";
	private String namePdfToSign = "";
	private String namePdfSign = "";
	private String tmpunzipFiles = "";
	private final javax.swing.table.DefaultTableModel defaultTableModel;
	private String folderExport = "";
	private String lastChosenFileFolder = "";
	protected String urlModuleValidation = "";

	private TypeFonctionnalite fonctionnalite;

	private enum TypeFonctionnalite {
		SignerDocument,
		CosignerDocument,
		SignerDocumentPdfEnPades;
	}

	private final List<InfosComplementairesCertificat> infosComplementairesCertificats = new ArrayList<InfosComplementairesCertificat>();

	/**
	 * Creates new form CosignForm
	 */
	public CosignForm() {

		initComponents();
		windowDimension();

		// os
		this.typeOs = Util.determinerOs();

		// paramétre provider
		this.typeProvider = Util.determinerProvider();

		// begin Tabs Colors
		lesOnglets.setForegroundAt(0, Color.blue);
		ChangeListener changeListener = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent changeEvent) {
				JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
				int index = sourceTabbedPane.getSelectedIndex();
				lesOnglets.setForegroundAt(0, Color.black);
				lesOnglets.setForegroundAt(1, Color.black);
				lesOnglets.setForegroundAt(2, Color.black);
				lesOnglets.setForegroundAt(3, Color.black);
				lesOnglets.setForegroundAt(4, Color.black);

				lesOnglets.setForegroundAt(index, Color.blue);
			}
		};
		lesOnglets.addChangeListener(changeListener);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/signature.gif")));
		// end Tabs Colors

		defaultTableModel = new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null } }, new String[] {
				I18nUtil.get("TEXT_COLUMN_1"), I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4"), I18nUtil.get("TEXT_COLUMN_4") });
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">

	private void initComponents() {

		buttonSignCase = new javax.swing.ButtonGroup();
		buttonVerifyCase = new javax.swing.ButtonGroup();
		typeSignature = new javax.swing.ButtonGroup();
		mainPanel = new javax.swing.JPanel();
		nomProduitEtVersion = new javax.swing.JLabel();
		esIcon = new javax.swing.JLabel();
		esIcon.setVisible(false);
		itIcon = new javax.swing.JLabel();
		itIcon.setVisible(false);
		frIcon = new javax.swing.JLabel();
		enIcon = new javax.swing.JLabel();
		closeIcone = new javax.swing.JLabel();
		lesOnglets = new javax.swing.JTabbedPane();
		tabSignDocXades = new javax.swing.JPanel();
		panelSignDoc = new javax.swing.JPanel();
		texte1 = new javax.swing.JLabel();
		texte2 = new javax.swing.JLabel();
		nomFichier = new javax.swing.JLabel();
		fileNameOngSign = new javax.swing.JTextField();
		btnParcourirO1 = new javax.swing.JButton();
		radioZipCase = new javax.swing.JRadioButton();
		radioDirCase = new javax.swing.JRadioButton();
		mentionZipCase = new javax.swing.JLabel();
		sigXadesCase = new javax.swing.JRadioButton();
		sigXadesCase.setVisible(false);
		sigPkcsCase = new javax.swing.JRadioButton();
		sigPkcsCase.setVisible(false);
		textJetonSignature = new javax.swing.JLabel();
		textJetonSignature.setVisible(false);
		panelDetailSigO1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		detailsSignaturesO1 = new javax.swing.JTable();
		btnSignerO1 = new javax.swing.JButton();
		tabCosignDocXades = new javax.swing.JPanel();
		panelCosignDoc = new javax.swing.JPanel();
		message1 = new javax.swing.JLabel();
		message2 = new javax.swing.JLabel();
		nomZip = new javax.swing.JLabel();
		filePathOngCosign = new javax.swing.JTextField();
		btnParcourirO2 = new javax.swing.JButton();
		btnCosigner = new javax.swing.JButton();
		panelDetailSigO2 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		detailsSignaturesO2 = new javax.swing.JTable();
		tabVerifySigs = new javax.swing.JPanel();
		panelVerifySig = new javax.swing.JPanel();
		nomFichierToVerify = new javax.swing.JLabel();
		fileNameOngVer = new javax.swing.JTextField();
		fileNameOngVer.setText("");
		btnParcourirFichier = new javax.swing.JButton();
		titre1 = new javax.swing.JLabel();
		titre2 = new javax.swing.JLabel();
		nomSignature = new javax.swing.JLabel();
		nomSignature.setVisible(false);
		fileSignNameOngVer = new javax.swing.JTextField();
		fileSignNameOngVer.setVisible(false);
		fileSignNameOngVer.setText("");
		btnParcourirSignature = new javax.swing.JButton();
		btnParcourirSignature.setVisible(false);
		verSignFile = new javax.swing.JRadioButton();
		nomSignature.setVisible(true);
		fileSignNameOngVer.setVisible(true);
		btnParcourirSignature.setVisible(true);
		verSignZip = new javax.swing.JRadioButton();
		verSignPdfFile = new javax.swing.JRadioButton();
		mentionFormesPrisesEnCompte = new javax.swing.JLabel();
		panelDetailSigO3 = new javax.swing.JPanel();
		jScrollPane3 = new javax.swing.JScrollPane();
		detailsSignaturesO3 = new javax.swing.JTable();
		btnVerifier = new javax.swing.JButton();
		tabExportDoc = new javax.swing.JPanel();
		panelExportFile = new javax.swing.JPanel();
		msg1 = new javax.swing.JLabel();
		msg2 = new javax.swing.JLabel();
		cheminArchive = new javax.swing.JLabel();
		fileNameExport = new javax.swing.JTextField();
		btnParcourirFileToExportO4 = new javax.swing.JButton();
		nouvelEmplacement = new javax.swing.JLabel();
		newFileDestination = new javax.swing.JLabel();
		btnEnregistrerO4 = new javax.swing.JButton();
		tabSignDocPades = new javax.swing.JPanel();
		panelSignPades = new javax.swing.JPanel();
		txt1 = new javax.swing.JLabel();
		txt2 = new javax.swing.JLabel();
		cheminPdf = new javax.swing.JLabel();
		pdfPathO5 = new javax.swing.JTextField();
		btnParcourirO5 = new javax.swing.JButton();
		btnSignPadesO5 = new javax.swing.JButton();
		panelDetailSigO5 = new javax.swing.JPanel();
		jScrollPane5 = new javax.swing.JScrollPane();
		detailsSignaturesO5 = new javax.swing.JTable();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle(I18nUtil.get("LOCAL_TRUST_COSIGN"));
		setBackground(new java.awt.Color(255, 255, 255));
		setMinimumSize(new java.awt.Dimension(870, 700));
		setResizable(false);

		mainPanel.setBackground(new java.awt.Color(255, 255, 255));
		mainPanel.setMinimumSize(new java.awt.Dimension(0, 0));
		mainPanel.setName(I18nUtil.get("LOCAL_TRUST_COSIGN"));

		nomProduitEtVersion.setFont(new java.awt.Font("Tahoma", 0, 36));
		nomProduitEtVersion.setForeground(new java.awt.Color(0, 103, 153));
		nomProduitEtVersion.setText(I18nUtil.get("LOCAL_TRUST_COSIGN"));

		esIcon.setBackground(new java.awt.Color(255, 255, 255));
		esIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag-es.png"))); // NOI18N
		esIcon.setToolTipText(I18nUtil.get("SPANICH"));
		esIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		esIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				esIconMouseClicked(evt);
			}
		});

		itIcon.setBackground(new java.awt.Color(255, 255, 255));
		itIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag-it.png"))); // NOI18N
		itIcon.setToolTipText(I18nUtil.get("ITALIAN"));
		itIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		itIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				itIconMouseClicked(evt);
			}
		});

		frIcon.setBackground(new java.awt.Color(255, 255, 255));
		frIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag-fr.png"))); // NOI18N
		frIcon.setToolTipText(I18nUtil.get("FRENCH"));
		frIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		frIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				frIconMouseClicked(evt);
			}
		});

		enIcon.setBackground(new java.awt.Color(255, 255, 255));
		enIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/flag-en.png"))); // NOI18N
		enIcon.setToolTipText(I18nUtil.get("ENGLISH"));
		enIcon.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		enIcon.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				enIconMouseClicked(evt);
			}
		});

		closeIcone.setBackground(new java.awt.Color(255, 255, 255));
		closeIcone.setFont(new java.awt.Font("Tahoma", 1, 11));
		closeIcone.setForeground(new java.awt.Color(5, 76, 112));
		closeIcone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/quitter.gif"))); // NOI18N
		closeIcone.setText(I18nUtil.get("FERMER"));
		closeIcone.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				closeIconeMouseClicked(evt);
			}
		});

		lesOnglets.setBackground(new java.awt.Color(255, 255, 255));
		lesOnglets.setName(I18nUtil.get("LOCAL_TRUST_COSIGN"));

		tabSignDocXades.setBackground(new java.awt.Color(255, 255, 255));
		tabSignDocXades.setName(I18nUtil.get("SIGNER_UN_DOCUMENT") + " " + I18nUtil.get("TYPES_SIGNATURES"));

		panelSignDoc.setBackground(new java.awt.Color(255, 255, 255));
		panelSignDoc.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("SIGNER")));

		texte1.setText(I18nUtil.get("LABEL_UTILISATION_1"));

		texte2.setText(I18nUtil.get("LABEL_UTILISATION_2"));

		nomFichier.setText(I18nUtil.get("NOM_FICHIER"));

		btnParcourirO1.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirO1.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnParcourirO1ActionPerformed(evt);
			}
		});

		radioZipCase.setBackground(new java.awt.Color(255, 255, 255));
		buttonSignCase.add(radioZipCase);
		radioZipCase.setSelected(true);
		radioZipCase.setText(I18nUtil.get("TEXT_RADIO_ZIP_CASE"));
		radioZipCase.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				radioZipCaseActionPerformed(evt);
			}
		});

		radioDirCase.setBackground(new java.awt.Color(255, 255, 255));
		buttonSignCase.add(radioDirCase);
		radioDirCase.setText(I18nUtil.get("TEXT_RADIO_DIR_CASE"));
		radioDirCase.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				radioDirCaseActionPerformed(evt);
			}
		});

		mentionZipCase.setText(I18nUtil.get("MENTION_ZIP_CASE"));

		sigXadesCase.setBackground(new java.awt.Color(255, 255, 255));
		typeSignature.add(sigXadesCase);
		sigXadesCase.setFont(new java.awt.Font("Tahoma", 1, 11));
		sigXadesCase.setSelected(true);
		sigXadesCase.setText(I18nUtil.get("XADES_CASE"));
		sigXadesCase.setLabel(I18nUtil.get("XADES_CASE"));
		sigXadesCase.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sigXadesCaseActionPerformed(evt);
			}
		});

		sigPkcsCase.setBackground(new java.awt.Color(255, 255, 255));
		typeSignature.add(sigPkcsCase);
		sigPkcsCase.setFont(new java.awt.Font("Tahoma", 1, 11));
		sigPkcsCase.setText(I18nUtil.get("PKCS_CASE"));
		sigPkcsCase.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sigPkcsCaseActionPerformed(evt);
			}
		});

		textJetonSignature.setFont(new java.awt.Font("Tahoma", 1, 11));
		textJetonSignature.setText(I18nUtil.get("TEXT_JETON_SIGNATURE"));

		javax.swing.GroupLayout panelSignDocLayout = new javax.swing.GroupLayout(panelSignDoc);
		panelSignDoc.setLayout(panelSignDocLayout);
		panelSignDocLayout.setHorizontalGroup(panelSignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelSignDocLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelSignDocLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												panelSignDocLayout
														.createSequentialGroup()
														.addComponent(nomFichier, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(
																panelSignDocLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																		.addComponent(radioZipCase, javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				panelSignDocLayout.createSequentialGroup().addComponent(fileNameOngSign, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
																						.addComponent(btnParcourirO1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addComponent(radioDirCase, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
																		.addGroup(javax.swing.GroupLayout.Alignment.LEADING,
																				panelSignDocLayout.createSequentialGroup().addGap(21, 21, 21).addComponent(mentionZipCase, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				panelSignDocLayout
																						.createSequentialGroup()
																						.addGap(21, 21, 21)
																						.addGroup(
																								panelSignDocLayout
																										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																										.addComponent(textJetonSignature, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
																										.addGroup(
																												panelSignDocLayout
																														.createSequentialGroup()
																														.addGap(19, 19, 19)
																														.addGroup(
																																panelSignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
																																		.addComponent(sigPkcsCase, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																																		.addComponent(sigXadesCase, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE))))))
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)).addComponent(texte1, javax.swing.GroupLayout.DEFAULT_SIZE, 865, Short.MAX_VALUE).addComponent(texte2, javax.swing.GroupLayout.DEFAULT_SIZE, 865, Short.MAX_VALUE))
						.addContainerGap()));
		panelSignDocLayout.setVerticalGroup(panelSignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelSignDocLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(texte1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(texte2)
						.addGap(26, 26, 26)
						.addGroup(
								panelSignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(fileNameOngSign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(btnParcourirO1)
										.addComponent(nomFichier)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(radioZipCase).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(mentionZipCase)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(radioDirCase).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(textJetonSignature).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(sigXadesCase).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(sigPkcsCase).addContainerGap(18, Short.MAX_VALUE)));

		sigPkcsCase.getAccessibleContext().setAccessibleName(I18nUtil.get("PKCS_CASE"));
		textJetonSignature.getAccessibleContext().setAccessibleName(I18nUtil.get("TEXT_JETON_SIGNATURE"));

		panelDetailSigO1.setBackground(new java.awt.Color(255, 255, 255));
		panelDetailSigO1.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("DETAILS_DES_SIGNATURES")));

		detailsSignaturesO1.setForeground(new java.awt.Color(0, 51, 255));
		detailsSignaturesO1.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { I18nUtil.get("TEXT_COLUMN_1"),
				I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4") }));
		detailsSignaturesO1.setGridColor(new java.awt.Color(204, 204, 204));
		jScrollPane1.setViewportView(detailsSignaturesO1);

		javax.swing.GroupLayout panelDetailSigO1Layout = new javax.swing.GroupLayout(panelDetailSigO1);
		panelDetailSigO1.setLayout(panelDetailSigO1Layout);
		panelDetailSigO1Layout.setHorizontalGroup(panelDetailSigO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO1Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE).addContainerGap()));
		panelDetailSigO1Layout.setVerticalGroup(panelDetailSigO1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO1Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		btnSignerO1.setText(I18nUtil.get("SIGNER"));
		btnSignerO1.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnSignerO1ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout tabSignDocXadesLayout = new javax.swing.GroupLayout(tabSignDocXades);
		tabSignDocXades.setLayout(tabSignDocXadesLayout);
		tabSignDocXadesLayout.setHorizontalGroup(tabSignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabSignDocXadesLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								tabSignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(panelSignDoc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(tabSignDocXadesLayout.createSequentialGroup().addComponent(panelDetailSigO1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(50, 50, 50))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabSignDocXadesLayout.createSequentialGroup().addComponent(btnSignerO1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(211, 211, 211)))));
		tabSignDocXadesLayout.setVerticalGroup(tabSignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabSignDocXadesLayout.createSequentialGroup().addContainerGap().addComponent(panelSignDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(btnSignerO1).addGap(18, 18, 18).addComponent(panelDetailSigO1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(82, Short.MAX_VALUE)));

		lesOnglets.addTab(I18nUtil.get("SIGNER_UN_DOCUMENT") + " " + I18nUtil.get("TYPES_SIGNATURES"), tabSignDocXades);
		tabSignDocXades.getAccessibleContext().setAccessibleName(I18nUtil.get("SIGNER_UN_DOCUMENT") + " " + I18nUtil.get("TYPES_SIGNATURES"));

		tabCosignDocXades.setBackground(new java.awt.Color(255, 255, 255));
		tabCosignDocXades.setName(I18nUtil.get("COSIGNER_UN_DOCUMENT"));

		panelCosignDoc.setBackground(new java.awt.Color(255, 255, 255));
		panelCosignDoc.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("COSIGNER")));

		message1.setText(I18nUtil.get("LABEL_UTILISATION_3"));

		message2.setText(I18nUtil.get("LABEL_UTILISATION_4"));

		nomZip.setText(I18nUtil.get("NOM_ZIP_FILE"));

		btnParcourirO2.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirO2.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnParcourirO2ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout panelCosignDocLayout = new javax.swing.GroupLayout(panelCosignDoc);
		panelCosignDoc.setLayout(panelCosignDocLayout);
		panelCosignDocLayout.setHorizontalGroup(panelCosignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelCosignDocLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelCosignDocLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												panelCosignDocLayout
														.createSequentialGroup()
														.addGroup(
																panelCosignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(message1, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
																		.addComponent(message2, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)).addGap(50, 50, 50))
										.addGroup(
												panelCosignDocLayout.createSequentialGroup().addComponent(nomZip, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(filePathOngCosign, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
														.addComponent(btnParcourirO2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(122, Short.MAX_VALUE)))));
		panelCosignDocLayout.setVerticalGroup(panelCosignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelCosignDocLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(message1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(message2)
						.addGap(11, 11, 11)
						.addGroup(
								panelCosignDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(filePathOngCosign, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(btnParcourirO2)
										.addComponent(nomZip)).addContainerGap(31, Short.MAX_VALUE)));

		btnCosigner.setText(I18nUtil.get("COSIGNER"));
		btnCosigner.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnCosignerActionPerformed(evt);
			}
		});

		panelDetailSigO2.setBackground(new java.awt.Color(255, 255, 255));
		panelDetailSigO2.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("DETAILS_DES_SIGNATURES")));

		detailsSignaturesO2.setForeground(new java.awt.Color(0, 51, 255));
		detailsSignaturesO2.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { I18nUtil.get("TEXT_COLUMN_1"),
				I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4") }));
		detailsSignaturesO2.setGridColor(new java.awt.Color(204, 204, 204));
		jScrollPane2.setViewportView(detailsSignaturesO2);

		javax.swing.GroupLayout panelDetailSigO2Layout = new javax.swing.GroupLayout(panelDetailSigO2);
		panelDetailSigO2.setLayout(panelDetailSigO2Layout);
		panelDetailSigO2Layout.setHorizontalGroup(panelDetailSigO2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO2Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 845, Short.MAX_VALUE).addContainerGap()));
		panelDetailSigO2Layout.setVerticalGroup(panelDetailSigO2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO2Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout tabCosignDocXadesLayout = new javax.swing.GroupLayout(tabCosignDocXades);
		tabCosignDocXades.setLayout(tabCosignDocXadesLayout);
		tabCosignDocXadesLayout.setHorizontalGroup(tabCosignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabCosignDocXadesLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								tabCosignDocXadesLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												tabCosignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(tabCosignDocXadesLayout.createSequentialGroup().addComponent(panelCosignDoc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap())
														.addGroup(tabCosignDocXadesLayout.createSequentialGroup().addComponent(panelDetailSigO2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabCosignDocXadesLayout.createSequentialGroup().addComponent(btnCosigner, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(211, 211, 211)))));
		tabCosignDocXadesLayout.setVerticalGroup(tabCosignDocXadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabCosignDocXadesLayout.createSequentialGroup().addContainerGap().addComponent(panelCosignDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(btnCosigner).addGap(18, 18, 18).addComponent(panelDetailSigO2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(207, Short.MAX_VALUE)));

		lesOnglets.addTab(I18nUtil.get("COSIGNER_UN_DOCUMENT"), tabCosignDocXades);
		tabCosignDocXades.getAccessibleContext().setAccessibleName(I18nUtil.get("COSIGNER_UN_DOCUMENT"));

		tabVerifySigs.setBackground(new java.awt.Color(255, 255, 255));
		tabVerifySigs.setName(I18nUtil.get("VERIFIER_LA_SIGNATURE"));

		panelVerifySig.setBackground(new java.awt.Color(255, 255, 255));
		panelVerifySig.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("VERIFIER")));

		nomFichierToVerify.setText(I18nUtil.get("NOM_FICHIER"));

		btnParcourirFichier.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirFichier.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnParcourirFichierActionPerformed(evt);
			}
		});

		titre1.setText(I18nUtil.get("LABEL_UTILISATION_5"));

		titre2.setFont(new java.awt.Font("Tahoma", 1, 11));
		titre2.setText(I18nUtil.get("LABEL_UTILISATION_7"));

		nomSignature.setText(I18nUtil.get("SIGN_ASSOCIE"));

		btnParcourirSignature.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirSignature.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnParcourirSignatureActionPerformed(evt);
			}
		});

		verSignFile.setBackground(new java.awt.Color(255, 255, 255));
		buttonVerifyCase.add(verSignFile);
		verSignFile.setFont(new java.awt.Font("Tahoma", 1, 11));
		verSignFile.setSelected(true);
		verSignFile.setText(I18nUtil.get("LABEL_UTILISATION_6"));
		verSignFile.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				verSignFileActionPerformed(evt);
			}
		});

		verSignZip.setBackground(new java.awt.Color(255, 255, 255));
		buttonVerifyCase.add(verSignZip);
		verSignZip.setFont(new java.awt.Font("Tahoma", 1, 11));
		verSignZip.setText(I18nUtil.get("LABEL_UTILISATION_9"));
		verSignZip.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				verSignZipActionPerformed(evt);
			}
		});

		verSignPdfFile.setBackground(new java.awt.Color(255, 255, 255));
		buttonVerifyCase.add(verSignPdfFile);
		verSignPdfFile.setFont(new java.awt.Font("Tahoma", 1, 11));
		verSignPdfFile.setText(I18nUtil.get("LABEL_UTILISATION_61"));
		verSignPdfFile.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				verSignPdfFileActionPerformed(evt);
			}
		});

		mentionFormesPrisesEnCompte.setForeground(new java.awt.Color(0, 153, 102));
		mentionFormesPrisesEnCompte.setText(I18nUtil.get("FORMES_PRISES_EN_COMPTE"));

		javax.swing.GroupLayout panelVerifySigLayout = new javax.swing.GroupLayout(panelVerifySig);
		panelVerifySig.setLayout(panelVerifySigLayout);
		panelVerifySigLayout.setHorizontalGroup(panelVerifySigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelVerifySigLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelVerifySigLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												javax.swing.GroupLayout.Alignment.TRAILING,
												panelVerifySigLayout
														.createSequentialGroup()
														.addGroup(
																panelVerifySigLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(verSignPdfFile, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGroup(panelVerifySigLayout.createSequentialGroup().addGap(21, 21, 21).addComponent(titre2, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE))
																		.addGroup(
																				panelVerifySigLayout.createSequentialGroup().addComponent(nomSignature, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(fileSignNameOngVer, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
																						.addComponent(btnParcourirSignature, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGroup(
																				panelVerifySigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
																						.addComponent(titre1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																						.addComponent(verSignFile, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 731, javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addComponent(verSignZip, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGroup(
																				panelVerifySigLayout.createSequentialGroup().addComponent(nomFichierToVerify, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(fileNameOngVer, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
																						.addComponent(btnParcourirFichier, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))).addContainerGap())
										.addGroup(panelVerifySigLayout.createSequentialGroup().addComponent(mentionFormesPrisesEnCompte, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE).addGap(109, 109, 109)))));
		panelVerifySigLayout.setVerticalGroup(panelVerifySigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				panelVerifySigLayout
						.createSequentialGroup()
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(mentionFormesPrisesEnCompte)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(titre1)
						.addGap(7, 7, 7)
						.addComponent(verSignFile)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(titre2)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(verSignZip, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(verSignPdfFile, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(5, 5, 5)
						.addGroup(
								panelVerifySigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(fileNameOngVer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(btnParcourirFichier)
										.addComponent(nomFichierToVerify))
						.addGap(11, 11, 11)
						.addGroup(
								panelVerifySigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(fileSignNameOngVer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(btnParcourirSignature).addComponent(nomSignature)).addGap(40, 40, 40)));

		nomSignature.getAccessibleContext().setAccessibleName("");
		verSignPdfFile.getAccessibleContext().setAccessibleName("");
		mentionFormesPrisesEnCompte.getAccessibleContext().setAccessibleName("");

		panelDetailSigO3.setBackground(new java.awt.Color(255, 255, 255));
		panelDetailSigO3.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("DETAILS_DES_SIGNATURES")));

		detailsSignaturesO3.setForeground(new java.awt.Color(0, 51, 255));
		detailsSignaturesO3.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { I18nUtil.get("TEXT_COLUMN_1"),
				I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4") }));
		detailsSignaturesO3.setGridColor(new java.awt.Color(204, 204, 204));
		jScrollPane3.setViewportView(detailsSignaturesO3);

		javax.swing.GroupLayout panelDetailSigO3Layout = new javax.swing.GroupLayout(panelDetailSigO3);
		panelDetailSigO3.setLayout(panelDetailSigO3Layout);
		panelDetailSigO3Layout.setHorizontalGroup(panelDetailSigO3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO3Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 845, Short.MAX_VALUE).addContainerGap()));
		panelDetailSigO3Layout.setVerticalGroup(panelDetailSigO3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO3Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		btnVerifier.setText(I18nUtil.get("VERIFIER"));
		btnVerifier.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					btnVerifierActionPerformed(evt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		javax.swing.GroupLayout tabVerifySigsLayout = new javax.swing.GroupLayout(tabVerifySigs);
		tabVerifySigs.setLayout(tabVerifySigsLayout);
		tabVerifySigsLayout.setHorizontalGroup(tabVerifySigsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabVerifySigsLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								tabVerifySigsLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												tabVerifySigsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(tabVerifySigsLayout.createSequentialGroup().addComponent(panelVerifySig, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap())
														.addGroup(tabVerifySigsLayout.createSequentialGroup().addComponent(panelDetailSigO3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()))
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabVerifySigsLayout.createSequentialGroup().addComponent(btnVerifier, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(213, 213, 213)))));
		tabVerifySigsLayout.setVerticalGroup(tabVerifySigsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabVerifySigsLayout.createSequentialGroup().addContainerGap().addComponent(panelVerifySig, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18).addComponent(btnVerifier).addGap(18, 18, 18)
						.addComponent(panelDetailSigO3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(81, Short.MAX_VALUE)));

		fileNameOngVer.setText(" ");

		lesOnglets.addTab(I18nUtil.get("VERIFIER_LA_SIGNATURE"), tabVerifySigs);
		tabVerifySigs.getAccessibleContext().setAccessibleName(I18nUtil.get("VERIFIER_LA_SIGNATURE"));

		tabExportDoc.setBackground(new java.awt.Color(255, 255, 255));
		tabExportDoc.setName(I18nUtil.get("EXPORTER_DOCUMENT"));

		panelExportFile.setBackground(new java.awt.Color(255, 255, 255));
		panelExportFile.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("EXPORTER")));

		msg1.setText(I18nUtil.get("LABEL_UTILISATION_10"));

		msg2.setText(I18nUtil.get("LABEL_UTILISATION_11"));

		cheminArchive.setText(I18nUtil.get("NOM_ZIP_FILE"));

		btnParcourirFileToExportO4.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirFileToExportO4.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnParcourirFileToExportO4ActionPerformed(evt);
			}
		});

		nouvelEmplacement.setText(I18nUtil.get("NOUVEAU_EMPLACEMENT"));

		javax.swing.GroupLayout panelExportFileLayout = new javax.swing.GroupLayout(panelExportFile);
		panelExportFile.setLayout(panelExportFileLayout);
		panelExportFileLayout.setHorizontalGroup(panelExportFileLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						panelExportFileLayout
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										panelExportFileLayout
												.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														panelExportFileLayout
																.createSequentialGroup()
																.addGroup(
																		panelExportFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(msg1, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)
																				.addComponent(msg2, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)).addGap(35, 35, 35))
												.addGroup(
														panelExportFileLayout.createSequentialGroup().addComponent(cheminArchive, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addComponent(fileNameExport, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51)
																.addComponent(btnParcourirFileToExportO4, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(122, Short.MAX_VALUE))
												.addGroup(panelExportFileLayout.createSequentialGroup().addComponent(nouvelEmplacement, javax.swing.GroupLayout.PREFERRED_SIZE, 822, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap())))
				.addGroup(panelExportFileLayout.createSequentialGroup().addGap(76, 76, 76).addComponent(newFileDestination, javax.swing.GroupLayout.DEFAULT_SIZE, 789, Short.MAX_VALUE)));
		panelExportFileLayout.setVerticalGroup(panelExportFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelExportFileLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(msg1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(msg2)
						.addGap(11, 11, 11)
						.addGroup(
								panelExportFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(fileNameExport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(btnParcourirFileToExportO4).addComponent(cheminArchive)).addGap(18, 18, 18).addComponent(nouvelEmplacement).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(newFileDestination, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(29, 29, 29)));

		btnEnregistrerO4.setText(I18nUtil.get("ENREGISTRER"));
		btnEnregistrerO4.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnEnregistrerO4ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout tabExportDocLayout = new javax.swing.GroupLayout(tabExportDoc);
		tabExportDoc.setLayout(tabExportDocLayout);
		tabExportDocLayout.setHorizontalGroup(tabExportDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabExportDocLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								tabExportDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(tabExportDocLayout.createSequentialGroup().addComponent(panelExportFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap())
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabExportDocLayout.createSequentialGroup().addComponent(btnEnregistrerO4, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(212, 212, 212)))));
		tabExportDocLayout.setVerticalGroup(tabExportDocLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabExportDocLayout.createSequentialGroup().addContainerGap().addComponent(panelExportFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(btnEnregistrerO4).addContainerGap(343, Short.MAX_VALUE)));

		lesOnglets.addTab(I18nUtil.get("EXPORTER_DOCUMENT"), tabExportDoc);
		tabExportDoc.getAccessibleContext().setAccessibleName(I18nUtil.get("EXPORTER_DOCUMENT"));

		tabSignDocPades.setBackground(new java.awt.Color(255, 255, 255));
		tabSignDocPades.setName(I18nUtil.get("SIGNER_PDF") + " " + I18nUtil.get("PADES"));

		panelSignPades.setBackground(new java.awt.Color(255, 255, 255));
		panelSignPades.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("SIGNER")));

		txt1.setText(I18nUtil.get("LABEL_UTILISATION_3"));

		txt2.setText(I18nUtil.get("LABEL_UTILISATION_14"));

		cheminPdf.setText(I18nUtil.get("FICHIER_NOM"));

		btnParcourirO5.setText(I18nUtil.get("PARCOURIR"));
		btnParcourirO5.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					btnParcourirO5ActionPerformed(evt);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		javax.swing.GroupLayout panelSignPadesLayout = new javax.swing.GroupLayout(panelSignPades);
		panelSignPades.setLayout(panelSignPadesLayout);
		panelSignPadesLayout.setHorizontalGroup(panelSignPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelSignPadesLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								panelSignPadesLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												panelSignPadesLayout
														.createSequentialGroup()
														.addGroup(
																panelSignPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(txt1, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
																		.addComponent(txt2, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)).addGap(50, 50, 50))
										.addGroup(
												panelSignPadesLayout.createSequentialGroup().addComponent(cheminPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(pdfPathO5, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(51, 51, 51).addComponent(btnParcourirO5, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addContainerGap(122, Short.MAX_VALUE)))));
		panelSignPadesLayout.setVerticalGroup(panelSignPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelSignPadesLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(txt1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(txt2)
						.addGap(11, 11, 11)
						.addGroup(
								panelSignPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(pdfPathO5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addComponent(btnParcourirO5)
										.addComponent(cheminPdf)).addContainerGap(31, Short.MAX_VALUE)));

		btnSignPadesO5.setText(I18nUtil.get("SIGNER"));
		btnSignPadesO5.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					btnSignPadesO5ActionPerformed(evt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		panelDetailSigO5.setBackground(new java.awt.Color(255, 255, 255));
		panelDetailSigO5.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("DETAILS_DES_SIGNATURES")));

		detailsSignaturesO5.setForeground(new java.awt.Color(0, 51, 255));
		detailsSignaturesO5.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null } }, new String[] { I18nUtil.get("TEXT_COLUMN_1"),
				I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4") }));
		detailsSignaturesO5.setGridColor(new java.awt.Color(204, 204, 204));
		jScrollPane5.setViewportView(detailsSignaturesO5);

		javax.swing.GroupLayout panelDetailSigO5Layout = new javax.swing.GroupLayout(panelDetailSigO5);
		panelDetailSigO5.setLayout(panelDetailSigO5Layout);
		panelDetailSigO5Layout.setHorizontalGroup(panelDetailSigO5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO5Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 845, Short.MAX_VALUE).addContainerGap()));
		panelDetailSigO5Layout.setVerticalGroup(panelDetailSigO5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				panelDetailSigO5Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

		javax.swing.GroupLayout tabSignDocPadesLayout = new javax.swing.GroupLayout(tabSignDocPades);
		tabSignDocPades.setLayout(tabSignDocPadesLayout);
		tabSignDocPadesLayout.setHorizontalGroup(tabSignDocPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabSignDocPadesLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								tabSignDocPadesLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(
												tabSignDocPadesLayout
														.createSequentialGroup()
														.addGroup(
																tabSignDocPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(panelSignPades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																		.addComponent(panelDetailSigO5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)).addContainerGap())
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tabSignDocPadesLayout.createSequentialGroup().addComponent(btnSignPadesO5, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(212, 212, 212)))));
		tabSignDocPadesLayout.setVerticalGroup(tabSignDocPadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				tabSignDocPadesLayout.createSequentialGroup().addContainerGap().addComponent(panelSignPades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(btnSignPadesO5).addGap(18, 18, 18).addComponent(panelDetailSigO5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap(207, Short.MAX_VALUE)));

		lesOnglets.addTab(I18nUtil.get("SIGNER_PDF") + " " + I18nUtil.get("PADES"), tabSignDocPades);
		tabSignDocPades.getAccessibleContext().setAccessibleName(I18nUtil.get("SIGNER_PDF") + " " + I18nUtil.get("PADES"));

		javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
		mainPanel.setLayout(mainPanelLayout);
		mainPanelLayout.setHorizontalGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				mainPanelLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								mainPanelLayout
										.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(lesOnglets, javax.swing.GroupLayout.DEFAULT_SIZE, 898, Short.MAX_VALUE)
										.addGroup(
												mainPanelLayout.createSequentialGroup().addComponent(nomProduitEtVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 698, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(esIcon).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(itIcon)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(frIcon).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(enIcon)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(closeIcone))).addContainerGap()));
		mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				mainPanelLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(nomProduitEtVersion, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addComponent(enIcon).addComponent(closeIcone).addComponent(frIcon).addComponent(itIcon).addComponent(esIcon)))
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(lesOnglets, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE).addContainerGap()));

		closeIcone.getAccessibleContext().setAccessibleName(I18nUtil.get("FERMER"));
		lesOnglets.getAccessibleContext().setAccessibleName(I18nUtil.get("LOCAL_TRUST_COSIGN"));
		lesOnglets.getAccessibleContext().setAccessibleDescription(I18nUtil.get("LOCAL_TRUST_COSIGN"));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		getAccessibleContext().setAccessibleName(I18nUtil.get("LOCAL_TRUST_COSIGN"));

		pack();
	}// </editor-fold>
		// GEN-END:initComponents

	private void sigPkcsCaseActionPerformed(java.awt.event.ActionEvent evt) {
		detailsSignaturesO1.setForeground(new java.awt.Color(0, 51, 255));
		detailsSignaturesO1.setModel(defaultTableModel);
		detailsSignaturesO1.setGridColor(new java.awt.Color(204, 204, 204));
		jScrollPane1.setViewportView(detailsSignaturesO1);
		detailsSignaturesO1.getColumnModel().getColumn(4).setPreferredWidth(0);
		detailsSignaturesO1.getColumnModel().getColumn(4).setMinWidth(0);
		detailsSignaturesO1.getColumnModel().getColumn(4).setMaxWidth(0);
		detailsSignaturesO1.getTableHeader().setReorderingAllowed(false);
	}

	void sigXadesCaseActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void verSignPdfFileActionPerformed(java.awt.event.ActionEvent evt) {
		if (verSignPdfFile.isSelected()) {
			nomSignature.setVisible(false);
			fileSignNameOngVer.setVisible(false);
			btnParcourirSignature.setVisible(false);
			fileNameOngVer.setText("");
			nomFichierToVerify.setText(I18nUtil.get("PDF_SIGNER"));
			detailsSignaturesO3.setForeground(new java.awt.Color(0, 51, 255));
			detailsSignaturesO3.setModel(defaultTableModel);
			detailsSignaturesO3.setGridColor(new java.awt.Color(204, 204, 204));
			jScrollPane3.setViewportView(detailsSignaturesO3);
			detailsSignaturesO3.getColumnModel().getColumn(4).setPreferredWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMinWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMaxWidth(0);
			detailsSignaturesO3.getTableHeader().setReorderingAllowed(false);

		}
	}

	private void btnSignPadesO5ActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
		if (pdfPathO5.getText().equals("")) {
			JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("LABEL_UTILISATION_14"), JOptionPane.INFORMATION_MESSAGE);
		} else {
			if (!((CosignForm.this.pdfPathO5.getText().substring(CosignForm.this.pdfPathO5.getText().lastIndexOf('.')).equals(".pdf")))) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_PDF"), I18nUtil.get("LABEL_UTILISATION_14"), JOptionPane.INFORMATION_MESSAGE);
				CosignForm.this.pdfPathO5.setText("");
			} else {
				this.namePdfToSign = pdfPathO5.getText();
				String dirChoose = FileManipulation.chooseFolder(I18nUtil.get("OUVRIR_EMPLACEMENT_PDF_SIGNE"), this.namePdfToSign);
				this.namePdfSign = dirChoose + File.separator + this.fileNameOngSignPdf.substring(0, this.fileNameOngSignPdf.lastIndexOf('.')) + "_sign" + ".pdf";
				if (new File(this.namePdfSign).exists()) {
					Object[] options = { I18nUtil.get("OUI"), I18nUtil.get("NON"), };
					int userClic = JOptionPane.showOptionDialog(this, I18nUtil.get("FICHIER_EXISTE_ECRASER"), I18nUtil.get("CONFIRMATION"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);
					if (userClic != JOptionPane.YES_OPTION) {
						JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_AUTRE_EMPLACEMENT"), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
					} else {
						this.namePdfSign = dirChoose + File.separator + this.fileNameOngSignPdf.substring(0, this.fileNameOngSignPdf.lastIndexOf('.')) + "_sign" + ".pdf";

						PDDocument doc = PDDocument.load(this.namePdfToSign.toString());
						List<PDSignature> fields = doc.getSignatureDictionaries();

						if (fields != null && fields.size() != 0) {
							// JOptionPane.showMessageDialog(null,
							// I18nUtil.get("PDF_AVEC_SIGNATURE"),
							// I18nUtil.get("ALERTE"),
							// JOptionPane.INFORMATION_MESSAGE);
							Object[] options2 = { I18nUtil.get("OUI"), I18nUtil.get("NON"), };
							int userClic2 = JOptionPane.showOptionDialog(this, I18nUtil.get("PDF_AVEC_SIGNATURE"), I18nUtil.get("CONFIRMATION"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options2, options2[1]);
							if (userClic2 != JOptionPane.YES_OPTION) {
								JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_AUTRE_PDF"), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
								CosignForm.this.pdfPathO5.setText("");
							} else {
								selectionnerCertificat(this.typeProvider);
							}
						} else {
							selectionnerCertificat(this.typeProvider);
						}
					}
				} else {
					this.namePdfSign = dirChoose + File.separator + this.fileNameOngSignPdf.substring(0, this.fileNameOngSignPdf.lastIndexOf('.')) + "_sign" + ".pdf";
					PDDocument doc = PDDocument.load(this.namePdfToSign.toString());
					List<PDSignature> fields = doc.getSignatureDictionaries();

					if (fields != null && fields.size() != 0) {
						CosignForm.this.fileNameOngVer.setText("");
						Object[] options3 = { I18nUtil.get("OUI"), I18nUtil.get("NON"), };
						int userClic3 = JOptionPane.showOptionDialog(this, I18nUtil.get("PDF_AVEC_SIGNATURE"), I18nUtil.get("CONFIRMATION"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options3, options3[1]);
						if (userClic3 != JOptionPane.YES_OPTION) {
							JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_AUTRE_PDF"), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
							CosignForm.this.pdfPathO5.setText("");
						} else {
							selectionnerCertificat(this.typeProvider);
						}
					} else {
						selectionnerCertificat(this.typeProvider);
					}
				}
			}
		}
	}

	private FilenameFilter getFilenameFilter(final String prefixe, final String extension) {

		FilenameFilter signatureFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.startsWith(prefixe) && lowercaseName.endsWith(extension)) {
					return true;
				} else {
					return false;
				}
			}
		};

		return signatureFilter;
	}

	private List<InfosComplementairesCertificat> getInfosComplementairesCertificats(File fichierSigne, File repertoireSource) {

		List<InfosComplementairesCertificat> infosComplementairesCertificats = new ArrayList<InfosComplementairesCertificat>();

		if ((fichierSigne != null && fichierSigne.exists() && fichierSigne.isFile()) && (repertoireSource != null && repertoireSource.exists() && repertoireSource.isDirectory())) {

			// signature Xades
			FilenameFilter signatureXadesFilter = getFilenameFilter(fichierSigne.getName().toLowerCase(), Constantes.SIGNATURE_FILE_EXTENSION_XADES);
			File[] signaturesXades = repertoireSource.listFiles(signatureXadesFilter);
			for (File signatureXades : signaturesXades) {
				try {
					String contenuFichierSignatureXML = FileUtils.readFileToString(signatureXades, FileUtil.ENCODING_UTF_8);
					InfosComplementairesCertificat infosComplementairesCertificat = SignatureXades.verifier(contenuFichierSignatureXML, fichierSigne);
					infosComplementairesCertificats.add(infosComplementairesCertificat);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// signature pcks7
			FilenameFilter signaturePkcs7Filter = getFilenameFilter(fichierSigne.getName().toLowerCase(), Constantes.SIGNATURE_FILE_EXTENSION_PCKS7);
			File[] signaturesPkcs7 = repertoireSource.listFiles(signaturePkcs7Filter);
			for (File signaturePkcs7 : signaturesPkcs7) {
				try {
					// TODO verification des signatures pkcs7
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		return infosComplementairesCertificats;
	}

	private String verifierContenuArchiveZip(File fichierZip, File repertoireDestination, String clefMessageOrigine) {

		File repertoireDezipe = null;
		try {
			repertoireDezipe = ZipUtils.unzip(fichierZip, repertoireDestination);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, I18nUtil.get("ARCHIVE_EXTRACTION_IMPOSSIBLE"), I18nUtil.get(clefMessageOrigine), JOptionPane.INFORMATION_MESSAGE);
		}
		String[] listeFichiers = repertoireDezipe.list();

		String nomFichierSigne = null;

		if (listeFichiers == null || listeFichiers.length == 0) {
			JOptionPane.showMessageDialog(null, I18nUtil.get("ARCHIVE_VIDE_OU_MAUVAIS_FORMAT"), I18nUtil.get(clefMessageOrigine), JOptionPane.INFORMATION_MESSAGE);
		} else {

			int nbreFichiers = 0;
			int nbreSignature = 0;

			for (int i = 0; i < listeFichiers.length; i++) {
				String nomFichier = listeFichiers[i];
				String extensionFichier = nomFichier.substring(nomFichier.lastIndexOf('.'));
				// Get filename of file or directory
				if (!(extensionFichier.equals(Constantes.SIGNATURE_FILE_EXTENSION_PCKS7)) && !(extensionFichier.equals(Constantes.SIGNATURE_FILE_EXTENSION_XADES))) {
					nomFichierSigne = nomFichier;
					nbreFichiers++;
				} else {
					nbreSignature++;
				}
			}
			if (nbreFichiers > 1) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("ZIP_AVEC_PLUSIEURS_FICHIER"), I18nUtil.get(clefMessageOrigine), JOptionPane.INFORMATION_MESSAGE);
			}
			if (nbreSignature == 0) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("ARCHIVE_SANS_SIGNATURE"), I18nUtil.get(clefMessageOrigine), JOptionPane.INFORMATION_MESSAGE);
			}
		}

		return nomFichierSigne;
	}

	private void btnParcourirFileToExportO4ActionPerformed(java.awt.event.ActionEvent evt) {

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("CHOISIR_ARCHIVE_ZIP"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("ZIP files", "zip");
		chooser.setFileFilter(filter);

		if (!this.absolutePathToCoSign.equals("")) {
			chooser.setSelectedFile(new File(this.absolutePathToCoSign));
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}
		int returnVal = chooser.showOpenDialog(CosignForm.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fileNameExport.setText(chooser.getSelectedFile().getAbsolutePath());
			this.lastChosenFileFolder = chooser.getSelectedFile().getAbsolutePath();
			this.fileNameOngExport = chooser.getSelectedFile().getName();
			if (!((CosignForm.this.fileNameOngExport.substring(CosignForm.this.fileNameOngExport.lastIndexOf('.')).equals(".zip")))) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_ZIP"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
			} else {
				String property = "java.io.tmpdir";
				this.tempDir = System.getProperty(property);
				this.tmpunzipFiles = this.tempDir + File.separator + this.fileNameOngExport.substring(0, this.fileNameOngExport.lastIndexOf('.'));

				File file = new File(tmpunzipFiles);

				if (file.exists()) {
					FileUtils.deleteQuietly(file);
				}

				if (file.mkdir() || file.isDirectory()) {

					File fichierZip = new File(this.fileNameExport.getText());
					File repertoireDestination = new File(this.tempDir + File.separator + this.fileNameOngExport.substring(0, this.fileNameOngExport.lastIndexOf('.')));

					String nomFichier = verifierContenuArchiveZip(fichierZip, repertoireDestination, "CHOIX_FILE_EXPORT");
					if (nomFichier != null) {
						this.tmpfileNameOngExport = this.tempDir + File.separator + this.fileNameOngExport.substring(0, this.fileNameOngExport.lastIndexOf('.')) + File.separator + nomFichier;
						this.pathfileToExport = this.tmpfileNameOngExport;
						this.nameFileToExport = nomFichier;
						btnEnregistrerO4.setVisible(true);
						this.newFileDestination.setText("");
					}
					nouvelEmplacement.setText("");
					btnEnregistrerO4.setText(I18nUtil.get("ENREGISTRER"));
					btnEnregistrerO4.setVisible(true);

				}

			}
		}
	}

	private void btnVerifierActionPerformed(java.awt.event.ActionEvent evt) throws Exception {

		// vérification de la signature d'un fichier (pcks7 ou xades)
		if (verSignFile.isSelected()) {
			if (fileNameOngVer.getText().equals("") || fileSignNameOngVer.getText().equals("")) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
				fileNameOngVer.setText("");
				afficherDetailsSignatures(null, this.detailsSignaturesO3, true);
			} else {
				System.out.println("in verif");

				String cheminFichier = this.fileNameOngVer.getText();
				File fichier = new File(cheminFichier);

				String cheminFichierSignature = this.fileSignNameOngVer.getText();
				File fichierSignature = new File(cheminFichierSignature);

				if (cheminFichierSignature.endsWith(Constantes.SIGNATURE_FILE_EXTENSION_XADES)) {
					String contenuFichierSignatureXML = FileUtils.readFileToString(fichierSignature, FileUtil.ENCODING_UTF_8);
					InfosComplementairesCertificat infosComplementairesCertificat = SignatureXades.verifier(contenuFichierSignatureXML, fichier);
					infosComplementairesCertificats.add(infosComplementairesCertificat);

				} else if (cheminFichierSignature.endsWith(Constantes.SIGNATURE_FILE_EXTENSION_PCKS7)) {
					String contenuFichierSignaturePkcs7 = FileUtils.readFileToString(fichierSignature, FileUtil.ENCODING_UTF_8);
					InfosComplementairesCertificat infosComplementairesCertificat = SignaturePkcs7.verifierHash(fichier, contenuFichierSignaturePkcs7, TypeAlgorithmHash.SHA1);
					infosComplementairesCertificats.add(infosComplementairesCertificat);

				}

				afficherDetailsSignatures(infosComplementairesCertificats, this.detailsSignaturesO3, true);
			}
		}

		// vérification du contenu de la signature d'un fichier pdf
		if (verSignPdfFile.isSelected()) {
			if (fileNameOngVer.getText().equals("")) {
				afficherDetailsSignatures(null, this.detailsSignaturesO3, true);
				JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
				fileNameOngVer.setText("");
			} else {
				if (((this.nameFileToVerSign.substring(this.nameFileToVerSign.lastIndexOf('.')).equals(".pdf")))) {
					File fichierPdf = new File(this.fileNameOngVer.getText());
					List<InfosComplementairesCertificat> infosComplementairesCertificats = SignaturePades.verifier(fichierPdf);
					afficherDetailsSignatures(infosComplementairesCertificats, this.detailsSignaturesO3, true);
				} else {
					afficherDetailsSignatures(null, this.detailsSignaturesO3, true);
					JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_CORRECTE_FILE"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
					fileNameOngVer.setText("");
				}
			}
		}

		// vérification des fichiers de signature se trouvant dans une archive
		// zip
		if (verSignZip.isSelected()) {
			if (fileNameOngVer.getText().equals("")) {
				afficherDetailsSignatures(null, this.detailsSignaturesO3, true);
				JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
				fileNameOngVer.setText("");
			} else {
				if (!((this.nameFileToVerSign.substring(this.nameFileToVerSign.lastIndexOf('.')).equals(".zip")))) {
					afficherDetailsSignatures(null, this.detailsSignaturesO3, true);
					JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_ZIP"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
					fileNameOngVer.setText("");
				} else {
					String property = "java.io.tmpdir";
					this.tempDir = System.getProperty(property);
					File file = new File(this.tempDir + File.separator + this.nameFileToVerSign.substring(0, this.nameFileToVerSign.lastIndexOf('.')));
					if (file.mkdir() || file.isDirectory()) {

						File fichierZip = new File(this.fileNameOngVer.getText());
						File repertoireDestination = new File(this.tempDir + File.separator + this.nameFileToVerSign.substring(0, this.nameFileToVerSign.lastIndexOf('.')));

						String nomFichier = verifierContenuArchiveZip(fichierZip, repertoireDestination, "CHOIX_FILE");
						if (nomFichier != null) {
							this.tmpfileNameOngVer = this.tempDir + File.separator + this.nameFileToVerSign.substring(0, this.nameFileToVerSign.lastIndexOf('.')) + File.separator + nomFichier;
							this.nameFileToVerSign = nomFichier;
							List<InfosComplementairesCertificat> infos = getInfosComplementairesCertificats(new File(repertoireDestination, nomFichier), repertoireDestination);
							infosComplementairesCertificats.addAll(infos);
							afficherDetailsSignatures(infosComplementairesCertificats, this.detailsSignaturesO3, true);
						} else {
							fileNameOngVer.setText("");
						}
					}
				}
			}
		}
	}

	private void verSignZipActionPerformed(java.awt.event.ActionEvent evt) {
		if (verSignZip.isSelected()) {
			nomSignature.setVisible(false);
			fileSignNameOngVer.setVisible(false);
			btnParcourirSignature.setVisible(false);
			fileNameOngVer.setText("");
			nomFichierToVerify.setText(I18nUtil.get("NOM_ZIP_FILE"));
			detailsSignaturesO3.setForeground(new java.awt.Color(0, 51, 255));
			detailsSignaturesO3.setModel(defaultTableModel);
			detailsSignaturesO3.setGridColor(new java.awt.Color(204, 204, 204));
			jScrollPane3.setViewportView(detailsSignaturesO3);
			detailsSignaturesO3.getColumnModel().getColumn(4).setPreferredWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMinWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMaxWidth(0);
			detailsSignaturesO3.getTableHeader().setReorderingAllowed(false);
		}
	}

	private void verSignFileActionPerformed(java.awt.event.ActionEvent evt) {
		if (verSignFile.isSelected()) {
			nomSignature.setVisible(true);
			nomFichierToVerify.setVisible(true);
			nomSignature.setText(I18nUtil.get("SIGN_ASSOCIE"));
			fileSignNameOngVer.setVisible(true);
			btnParcourirSignature.setVisible(true);
			fileNameOngVer.setText("");
			fileSignNameOngVer.setText("");
			nomFichierToVerify.setText(I18nUtil.get("NOM_FICHIER"));
			detailsSignaturesO3.setForeground(new java.awt.Color(0, 51, 255));
			detailsSignaturesO3.setModel(defaultTableModel);
			detailsSignaturesO3.setGridColor(new java.awt.Color(204, 204, 204));
			jScrollPane3.setViewportView(detailsSignaturesO3);
			detailsSignaturesO3.getColumnModel().getColumn(4).setPreferredWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMinWidth(0);
			detailsSignaturesO3.getColumnModel().getColumn(4).setMaxWidth(0);
			detailsSignaturesO3.getTableHeader().setReorderingAllowed(false);
		}
	}

	private void btnParcourirFichierActionPerformed(java.awt.event.ActionEvent evt) {

		final JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("SELECTIONNEZ_LE_FICHIER"));

		if (!this.pathFileToVerSign.equals("")) {
			chooser.setSelectedFile(new File(this.pathFileToVerSign));
		}
		if (!this.pathfileToSign.equals("")) {
			chooser.setSelectedFile(new File(this.pathfileToSign));
		}
		if (!this.absolutePathToCoSign.equals("")) {
			chooser.setSelectedFile(new File(this.absolutePathToCoSign));
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			fileNameOngVer.setText(chooser.getSelectedFile().getAbsolutePath());
			this.lastChosenFileFolder = chooser.getSelectedFile().getAbsolutePath();
			this.nameFileToVerSign = chooser.getSelectedFile().getName();
			this.pathFileToVerSign = chooser.getSelectedFile().getAbsolutePath();
			if (((CosignForm.this.nameFileToVerSign.substring(CosignForm.this.nameFileToVerSign.lastIndexOf('.')).equals(".p7s")))) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_VERIFIER_SIGNATURE"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	private void btnCosignerActionPerformed(java.awt.event.ActionEvent evt) {
		if (filePathOngCosign.getText().equals("")) {
			JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE_COSIGNEE"), JOptionPane.INFORMATION_MESSAGE);
		} else {
			// afficher le details des signatures contenues dans ce zip
			selectionnerCertificat(this.typeProvider);
		}
	}

	private void btnSignerO1ActionPerformed(java.awt.event.ActionEvent evt) {
		// lancement de la fenetre du choix des certificats
		if (fileNameOngSign.getText().equals("")) {
			JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE_SIGNEE"), JOptionPane.INFORMATION_MESSAGE);
		} else {
			selectionnerCertificat(this.typeProvider);
		}
	}

	private void radioDirCaseActionPerformed(java.awt.event.ActionEvent evt) {
		if (radioDirCase.isSelected()) {
			textJetonSignature.setVisible(true);
			sigXadesCase.setVisible(true);
			sigPkcsCase.setVisible(true);

			detailsSignaturesO1.setForeground(new java.awt.Color(0, 51, 255));
			detailsSignaturesO1.setModel(defaultTableModel);
			detailsSignaturesO1.setGridColor(new java.awt.Color(204, 204, 204));
			jScrollPane1.setViewportView(detailsSignaturesO1);
			detailsSignaturesO1.getColumnModel().getColumn(4).setPreferredWidth(0);
			detailsSignaturesO1.getColumnModel().getColumn(4).setMinWidth(0);
			detailsSignaturesO1.getColumnModel().getColumn(4).setMaxWidth(0);
			detailsSignaturesO1.getTableHeader().setReorderingAllowed(false);

		}
	}

	private void radioZipCaseActionPerformed(java.awt.event.ActionEvent evt) {
		if (radioZipCase.isSelected()) {

			textJetonSignature.setVisible(false);
			sigXadesCase.setVisible(false);
			sigPkcsCase.setVisible(false);

			detailsSignaturesO1.setForeground(new java.awt.Color(0, 51, 255));
			detailsSignaturesO1.setModel(defaultTableModel);
			detailsSignaturesO1.setGridColor(new java.awt.Color(204, 204, 204));
			jScrollPane1.setViewportView(detailsSignaturesO1);
			detailsSignaturesO1.getColumnModel().getColumn(4).setPreferredWidth(0);
			detailsSignaturesO1.getColumnModel().getColumn(4).setMinWidth(0);
			detailsSignaturesO1.getColumnModel().getColumn(4).setMaxWidth(0);
			detailsSignaturesO1.getTableHeader().setReorderingAllowed(false);
		}
	}

	private void btnParcourirO1ActionPerformed(java.awt.event.ActionEvent evt) {
		this.fonctionnalite = TypeFonctionnalite.SignerDocument;

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("SELECTIONNEZ_LE_FICHIER"));

		// Memoriser la premiere valeur choisi
		if (!this.pathfileToSign.equals("")) {
			chooser.setSelectedFile(new File(this.pathfileToSign));
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			fileNameOngSign.setText(chooser.getSelectedFile().getAbsolutePath());
			this.pathfileToSign = fileNameOngSign.getText();
			this.nameFileToSign = chooser.getSelectedFile().getName();
			this.lastChosenFileFolder = this.pathfileToSign;
		}
	}

	private void btnParcourirO2ActionPerformed(java.awt.event.ActionEvent evt) {
		this.fonctionnalite = TypeFonctionnalite.CosignerDocument;

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("CHOISIR_ARCHIVE_ZIP"));
		FileNameExtensionFilter filter = new FileNameExtensionFilter("ZIP files", "zip");
		chooser.setFileFilter(filter);

		if (!this.zipCosignNameExt.equals("")) {
			File file = new File(this.zipCosignNameExt);
			chooser.setSelectedFile(file);
		}
		if (!this.absolutePathToCoSign.equals("")) {
			File file = new File(this.absolutePathToCoSign);
			chooser.setSelectedFile(file);
			new File(this.absolutePathToCoSign).setWritable(true);
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}

		int returnVal = chooser.showOpenDialog(CosignForm.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			if (!chooser.getSelectedFile().getAbsolutePath().equals(this.absolutePathToCoSign)) {
				detailsSignaturesO2.setForeground(new java.awt.Color(0, 51, 255));
				detailsSignaturesO2.setModel(defaultTableModel);
				detailsSignaturesO2.setGridColor(new java.awt.Color(204, 204, 204));
				jScrollPane2.setViewportView(detailsSignaturesO2);
				detailsSignaturesO2.getColumnModel().getColumn(4).setPreferredWidth(0);
				detailsSignaturesO2.getColumnModel().getColumn(4).setMinWidth(0);
				detailsSignaturesO2.getColumnModel().getColumn(4).setMaxWidth(0);
				detailsSignaturesO2.getTableHeader().setReorderingAllowed(false);
			}
			filePathOngCosign.setText(chooser.getSelectedFile().getAbsolutePath());
			this.lastChosenFileFolder = chooser.getSelectedFile().getAbsolutePath();
			this.fileNameOngCosign = chooser.getSelectedFile().getName();
			if (!((CosignForm.this.fileNameOngCosign.substring(CosignForm.this.fileNameOngCosign.lastIndexOf('.')).equals(".zip")))) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_ZIP"), I18nUtil.get("CHOIX_FILE_COSIGN"), JOptionPane.INFORMATION_MESSAGE);
				filePathOngCosign.setText("");
			} else {
				this.absolutePathToCoSign = chooser.getSelectedFile().getAbsolutePath();
				// afficher le details des signatures contenues dans ce zip
				String property = "java.io.tmpdir";
				this.tempDir = System.getProperty(property);
				File repertoireDestination = new File(this.tempDir + File.separator + this.fileNameOngCosign.substring(0, this.fileNameOngCosign.lastIndexOf('.')));
				if (repertoireDestination.exists()) {
					FileUtils.deleteQuietly(repertoireDestination);
				}
				if (repertoireDestination.mkdir() || repertoireDestination.isDirectory()) {

					File fichierZip = new File(this.filePathOngCosign.getText());
					String nomFichier = verifierContenuArchiveZip(fichierZip, repertoireDestination, "CHOIX_FILE_COSIGN");
					if (nomFichier != null) {
						this.tmpfileNameOngCosign = this.tempDir + File.separator + this.fileNameOngCosign.substring(0, this.fileNameOngCosign.lastIndexOf('.')) + File.separator + nomFichier;
						File fichierSigne = new File(repertoireDestination, nomFichier);
						List<InfosComplementairesCertificat> infos = getInfosComplementairesCertificats(fichierSigne, repertoireDestination);
						infosComplementairesCertificats.addAll(infos);
						afficherDetailsSignatures(infosComplementairesCertificats, this.detailsSignaturesO2, false);
					} else {
						filePathOngCosign.setText("");
					}
				}
			}
			new File(this.absolutePathToCoSign).setReadOnly();
		}
	}

	private void btnParcourirO5ActionPerformed(java.awt.event.ActionEvent evt) throws Exception {
		this.fonctionnalite = TypeFonctionnalite.SignerDocumentPdfEnPades;

		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("CHOISIR_PDF"));

		if (!this.namePdfToSign.equals("")) {
			chooser.setSelectedFile(new File(this.namePdfToSign));
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}

		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (!pdfPathO5.getText().equals(this.fileNameOngSignPdf)) {
				detailsSignaturesO5.setForeground(new java.awt.Color(0, 51, 255));
				detailsSignaturesO5.setModel(defaultTableModel);
				detailsSignaturesO5.setGridColor(new java.awt.Color(204, 204, 204));
				jScrollPane5.setViewportView(detailsSignaturesO5);
				detailsSignaturesO5.getColumnModel().getColumn(4).setPreferredWidth(0);
				detailsSignaturesO5.getColumnModel().getColumn(4).setMinWidth(0);
				detailsSignaturesO5.getColumnModel().getColumn(4).setMaxWidth(0);
				detailsSignaturesO5.getTableHeader().setReorderingAllowed(false);
			}
			pdfPathO5.setText(chooser.getSelectedFile().getAbsolutePath());
			this.namePdfToSign = chooser.getSelectedFile().getAbsolutePath();
			this.lastChosenFileFolder = chooser.getSelectedFile().getAbsolutePath();
			this.fileNameOngSignPdf = chooser.getSelectedFile().getName();
		}

	}

	private void enIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in english");
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Locale locale = I18nUtil.toLocale("en");
				Locale.setDefault(locale);
				CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
				cosgnFrm.enIcon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 255)));
			}

		});
		this.dispose();
	}

	private void frIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in french");
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Locale locale = I18nUtil.toLocale("fr");
				Locale.setDefault(locale);
				CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
				cosgnFrm.frIcon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 255)));
			}

		});
		this.dispose();
	}

	private void itIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in italian");
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Locale locale = I18nUtil.toLocale("it");
				Locale.setDefault(locale);
				CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
				cosgnFrm.itIcon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 255)));
			}

		});
		this.dispose();
	}

	private void esIconMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("in spanish");
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				Locale locale = I18nUtil.toLocale("es");
				Locale.setDefault(locale);
				CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
				cosgnFrm.esIcon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 255)));
			}

		});
		this.dispose();
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		System.out.println("main frame " + Locale.getDefault().getLanguage());
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				CosignForm cosgnFrm = new CosignForm();
				cosgnFrm.setVisible(true);
				try {
					PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
					UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
				} catch (UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
			}
		});
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton btnCosigner;
	private javax.swing.JButton btnEnregistrerO4;
	private javax.swing.JButton btnParcourirFichier;
	private javax.swing.JButton btnParcourirFileToExportO4;
	private javax.swing.JButton btnParcourirO1;
	private javax.swing.JButton btnParcourirO2;
	private javax.swing.JButton btnParcourirO5;
	private javax.swing.JButton btnParcourirSignature;
	private javax.swing.JButton btnSignPadesO5;
	private javax.swing.JButton btnSignerO1;
	private javax.swing.JButton btnVerifier;
	private javax.swing.ButtonGroup buttonSignCase;
	private javax.swing.ButtonGroup buttonVerifyCase;
	private javax.swing.JLabel cheminArchive;
	private javax.swing.JLabel cheminPdf;
	private javax.swing.JLabel closeIcone;
	private javax.swing.JTable detailsSignaturesO1;
	private javax.swing.JTable detailsSignaturesO2;
	private javax.swing.JTable detailsSignaturesO3;
	private javax.swing.JTable detailsSignaturesO5;
	private javax.swing.JLabel enIcon;
	private javax.swing.JLabel esIcon;
	private javax.swing.JTextField fileNameExport;
	private javax.swing.JTextField fileNameOngSign;
	private javax.swing.JTextField fileNameOngVer;
	private javax.swing.JTextField filePathOngCosign;
	private javax.swing.JTextField fileSignNameOngVer;
	private javax.swing.JLabel frIcon;
	private javax.swing.JLabel itIcon;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JScrollPane jScrollPane5;
	private javax.swing.JTabbedPane lesOnglets;
	private javax.swing.JPanel mainPanel;
	private javax.swing.JLabel mentionFormesPrisesEnCompte;
	private javax.swing.JLabel mentionZipCase;
	private javax.swing.JLabel message1;
	private javax.swing.JLabel message2;
	private javax.swing.JLabel msg1;
	private javax.swing.JLabel msg2;
	private javax.swing.JLabel newFileDestination;
	private javax.swing.JLabel nomFichier;
	private javax.swing.JLabel nomFichierToVerify;
	private javax.swing.JLabel nomProduitEtVersion;
	private javax.swing.JLabel nomSignature;
	private javax.swing.JLabel nomZip;
	private javax.swing.JLabel nouvelEmplacement;
	private javax.swing.JPanel panelCosignDoc;
	private javax.swing.JPanel panelDetailSigO1;
	private javax.swing.JPanel panelDetailSigO2;
	private javax.swing.JPanel panelDetailSigO3;
	private javax.swing.JPanel panelDetailSigO5;
	private javax.swing.JPanel panelExportFile;
	private javax.swing.JPanel panelSignDoc;
	private javax.swing.JPanel panelSignPades;
	private javax.swing.JPanel panelVerifySig;
	private javax.swing.JTextField pdfPathO5;
	private javax.swing.JRadioButton radioDirCase;
	private javax.swing.JRadioButton radioZipCase;
	private javax.swing.JRadioButton sigPkcsCase;
	private javax.swing.JRadioButton sigXadesCase;
	private javax.swing.JPanel tabCosignDocXades;
	private javax.swing.JPanel tabExportDoc;
	private javax.swing.JPanel tabSignDocPades;
	private javax.swing.JPanel tabSignDocXades;
	private javax.swing.JPanel tabVerifySigs;
	private javax.swing.JLabel textJetonSignature;
	private javax.swing.JLabel texte1;
	private javax.swing.JLabel texte2;
	private javax.swing.JLabel titre1;
	private javax.swing.JLabel titre2;
	private javax.swing.JLabel txt1;
	private javax.swing.JLabel txt2;
	private javax.swing.ButtonGroup typeSignature;
	private javax.swing.JRadioButton verSignFile;
	private javax.swing.JRadioButton verSignPdfFile;
	private javax.swing.JRadioButton verSignZip;

	// End of variables declaration//GEN-END:variables

	private void closeIconeMouseClicked(java.awt.event.MouseEvent evt) {
		System.exit(0);
	}

	public javax.swing.JLabel getIconSelected(String lang) {
		if (lang.equals("fr")) {
			return frIcon;
		} else if (lang.equals("en")) {
			return enIcon;
		}
		if (lang.equals("es")) {
			return esIcon;
		}
		if (lang.equals("it")) {
			return itIcon;
		}
		return null;

	}

	private void windowDimension() {
		Toolkit kit = this.getToolkit();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

		Dimension d = kit.getScreenSize();
		int max_width = (d.width - in.left - in.right);
		int max_height = (d.height - in.top - in.bottom);

		this.setSize(Math.min(max_width, 1020), Math.min(max_height, 400));// whatever
		// size you want but smaller the insets
		this.setLocation((max_width - this.getWidth()) / 2, (max_height - this.getHeight()) / 2);
		// Container pane = this.getContentPane();
	}

	/**
	 * Permet d'initialiser l'interface graphique de sélection du certificat en
	 * fonction du provider sélectionné. Permet d'appeler la méthode abstraite
	 * onSelection(KeyPair keyPair) qui doit être implémenté par les classes
	 * héritant de cette classe.
	 * 
	 * @param provider
	 *            le type de provider.
	 */
	protected void selectionnerCertificat(TypeProvider provider) {
		System.out.println("Type de provider : " + provider);
		if (provider != null) {
			switch (provider) {
			case APPLE:
			case PKCS11:
			case MSCAPI:
				MagasinCertificateUiService.getInstance(pkcs11LibsType).initUi(this, typeOs, provider, false);
				break;
			case PKCS12:
				Pkcs12CertificateUiService.getInstance().initUi(this, typeOs, provider, false);
				break;
			}
		}
	}

	@Override
	public void onSelection(final MagasinCertificateEvent event) throws ManipulationCertificatException {
		SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {

			@Override
			public String doInBackground() throws Exception {

				System.out.println("onSelect => Provider d'accès au  Magasin : " + event.getCertificateItem().getTypeProvider());
				String alias = event.getCertificateItem().getId();
				System.out.println("L'alias selectionné est : " + alias);
				boolean smartCard = event.getCertificateItem().isSmartCard();
				System.out.println("SmartCard : " + smartCard);

				KeyPair keyPair = null;
				try {

					if (typeOs != TypeOs.Windows && smartCard) {
						keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
					} else {
						keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, alias);
					}

				} catch (RecuperationCertificatException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
				}

				if (keyPair == null) {
					JOptionPane.showMessageDialog(null, "Aucun certificat avec comme alias " + alias + " n'a été retrouvé dans le magasin", I18nUtil.get("INFORMATION_MESSAGE_ALERTE"), JOptionPane.WARNING_MESSAGE);
				} else {
					System.out.println("Certificat sélectionné depuis le magasin : " + keyPair.getCertificate().getSubjectX500Principal().toString());
					try {
						onSelection(keyPair);
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
					}
				}

				return null;

			}

			@Override
			protected void done() {
				try {
					get();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		};
		sw.execute();
	}

	@Override
	public void onSelection(final Pkcs12CertificateEvent event) throws ManipulationCertificatException {
		SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {

			@Override
			public String doInBackground() throws Exception {

				String cheminFichierP12 = event.getCheminFichierP12();
				String motDePasseFichierP12 = event.getMotDePasseFichierP12();
				KeyPair keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);
				onSelection(keyPair);

				return null;
			}

			@Override
			protected void done() {
				try {
					get();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		};
		sw.execute();
	}

	private String genererSignatureXades(KeyPair keyPair, File fichier, Date dateSignature, TypeAlgorithmHash typeAlgorithmHash) throws SignatureExecutionException {

		byte[] hashBinaireFichier = null;

		try {
			hashBinaireFichier = HashUtil.genererHashShaBinaire(typeAlgorithmHash, fichier);
		} catch (IOException e) {
			throw new SignatureExecutionException("Un problème est survenu lors de la génération du hash sha-256 du fichier : " + fichier.getPath(), e);
		}

		String signatureXadesClient = null;
		try {
			signatureXadesClient = SignatureXades.signer(keyPair, hashBinaireFichier, fichier.toURI(), dateSignature, typeAlgorithmHash);
		} catch (Exception e) {
			throw new SignatureExecutionException("Un problème est survenu lors de la tentative de création de la signature XML Xades", e);
		}
		System.out.println("Contenu de la signature XAdES  : " + signatureXadesClient);
		System.out.println("La date de signature est le  : " + dateSignature);

		return signatureXadesClient;
	}

	private void onSelection(KeyPair keyPair) {
		try {
			switch (fonctionnalite) {

			// onglet signer un document XAdES ou PKCS7
			case SignerDocument:
				// cas de la signature XAdES
				if (radioZipCase.isSelected() || (!radioZipCase.isSelected() && sigXadesCase.isSelected())) {

					// paramétre pour générer la signature XAdES
					File fichier = new File(this.pathfileToSign);
					int indexFichier = FileUtil.getProchaineIndexFichierSignatureXML(fichier.getPath());
					Date dateSignature = new Date();
					String formatDateNomFichier = Util.formaterDate(dateSignature, Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);

					// génération de la signature XAdES
					String signatureXadesClient = genererSignatureXades(keyPair, fichier, dateSignature, typeAlgorithmHash);
					String cheminFichierSignatureXades = XMLUtil.contruireCheminFichierSignatureXML(fichier.getPath(), formatDateNomFichier, indexFichier);
					File fichierSignatureXades = new File(cheminFichierSignatureXades);
					FileUtils.writeStringToFile(fichierSignatureXades, signatureXadesClient, FileUtil.ENCODING_UTF_8);

					// on extrait les informations de la signature afin de les
					// afficher
					InfosComplementairesCertificat infosVerificationCertificat = SignatureXades.verifier(signatureXadesClient, fichier);
					infosComplementairesCertificats.add(infosVerificationCertificat);

					if (radioZipCase.isSelected()) {
						// génération du zip
						File fichierZip = new File(this.zipCosignName);
						if (fichierZip.exists()) {
							FileUtils.deleteQuietly(fichierZip);
						}
						List<File> fichiers = new ArrayList<File>();
						fichiers.add(fichier);
						fichiers.add(fichierSignatureXades);
						fichierZip = ZipUtils.zip(fichiers, fichier.getParentFile(), fichier.getName() + ".zip");
						fichierSignatureXades.delete();

						// on affiche les informations sur le fichier dans le
						// premier onglet
						afficherDetailsSignatures(infosComplementairesCertificats, detailsSignaturesO1, true);
						JOptionPane.showMessageDialog(null, I18nUtil.get("ARCHIVE_ZIP_ENREGISTRER") + fichierZip.getParent(), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);

					} else {
						afficherDetailsSignatures(infosComplementairesCertificats, detailsSignaturesO1, true);
						JOptionPane.showMessageDialog(null, I18nUtil.get("SIGNATURE_ENREGISTER_DANS") + fichier.getParent(), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
					}

				}
				// cas de la signature PKCS7
				else if (!radioZipCase.isSelected() && sigPkcsCase.isSelected()) {

					File fichier = new File(this.pathfileToSign);
					int indexFichier = FileUtil.getProchaineIndexFichierSignaturePkcs7(fichier.getPath());

					String hashFichier = HashUtil.genererHashShaHexadecimal(TypeAlgorithmHash.SHA1, fichier);
					byte[] signatureContenu = SignaturePkcs7.signerHash(keyPair, TypeAlgorithmHash.SHA1, hashFichier);
					String signatureEnBase64 = Base64.encodeBytes(signatureContenu, 0, signatureContenu.length, Base64.DO_BREAK_LINES);
					String cheminFichierSignaturePkcs7 = XMLUtil.contruireCheminFichierSignaturePkcs7(fichier.getPath(), indexFichier);
					File fichierSignaturePkcs7 = new File(cheminFichierSignaturePkcs7);
					FileUtils.writeStringToFile(fichierSignaturePkcs7, signatureEnBase64, FileUtil.ENCODING_UTF_8);

					InfosComplementairesCertificat infosVerificationCertificat = CertificatUtil.extraireInformations(keyPair.getCertificate(), new InfosVerificationCertificat());
					infosVerificationCertificat.setSignatureValide(true);
					infosComplementairesCertificats.add(infosVerificationCertificat);

					// on affiche les informations sur le fichier dans le
					// premier onglet
					afficherDetailsSignatures(infosComplementairesCertificats, detailsSignaturesO1, true);

					JOptionPane.showMessageDialog(null, I18nUtil.get("SIGNATURE_ENREGISTER_DANS") + fichier.getParent(), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
				}
				break;
			case CosignerDocument:

				String extensionFichierZip = this.fileNameOngCosign.substring(this.fileNameOngCosign.lastIndexOf('.'));
				if (!extensionFichierZip.equals(Constantes.SIGNATURE_FILE_EXTENSION_ZIP)) {
					JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_ZIP"), I18nUtil.get("CHOIX_FILE"), JOptionPane.INFORMATION_MESSAGE);
				} else {

					// paramétre pour générer la signature XAdES
					File fichierSigne = new File(this.tmpfileNameOngCosign);
					int indexFichier = FileUtil.getProchaineIndexFichierSignatureXML(fichierSigne.getPath());
					Date dateSignature = new Date();
					String formatDateNomFichier = Util.formaterDate(dateSignature, Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);

					// génération de la signature XAdES
					String signatureXadesClient = genererSignatureXades(keyPair, fichierSigne, dateSignature, typeAlgorithmHash);
					String cheminFichierSignatureXades = XMLUtil.contruireCheminFichierSignatureXML(fichierSigne.getPath(), formatDateNomFichier, indexFichier);
					File fichierSignatureXades = new File(cheminFichierSignatureXades);
					FileUtils.writeStringToFile(fichierSignatureXades, signatureXadesClient, FileUtil.ENCODING_UTF_8);

					// on extrait les informations de la signature afin de les
					// afficher
					InfosComplementairesCertificat infosVerificationCertificat = SignatureXades.verifier(signatureXadesClient, fichierSigne);
					infosComplementairesCertificats.add(infosVerificationCertificat);

					// génération du zip
					File fichierZip = new File(this.zipCosignName);
					if (fichierZip.exists()) {
						FileUtils.deleteQuietly(fichierZip);
					}
					List<File> fichiers = new ArrayList<File>();
					for (File fichier : fichierSigne.getParentFile().listFiles()) {
						fichiers.add(fichier);
					}
					fichierZip = ZipUtils.zip(fichiers, fichierSigne.getParentFile(), fichierSigne.getName() + ".zip");
					File fichierDestinationZip = new File(this.absolutePathToCoSign);
					if (fichierDestinationZip.exists()) {
						fichierDestinationZip.delete();
					}
					FileUtils.moveFile(fichierZip, fichierDestinationZip);
					FileUtils.deleteQuietly(fichierSigne.getParentFile());

					// on affiche les informations sur le fichier dans le
					// premier onglet
					afficherDetailsSignatures(infosComplementairesCertificats, detailsSignaturesO2, true);

					JOptionPane.showMessageDialog(null, I18nUtil.get("ARCHIVE_COSIGNER_DANS") + filePathOngCosign.getText(), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
				}

				break;
			case SignerDocumentPdfEnPades:

				File fichierPdf = new File(this.namePdfToSign);
				FileInputStream fichierPdfInputStream = new FileInputStream(fichierPdf);
				File fichierPdfSigne = new File(this.namePdfSign);
				FileOutputStream fichierPdfSigneOutputStream = new FileOutputStream(fichierPdfSigne);

				PrivateKey privateKey = keyPair.getPrivateKey();
				X509Certificate certificat = keyPair.getCertificate();
				String cn = CertificatUtil.getCN(keyPair.getCertificate().getSubjectX500Principal());
				String signatureTexte = I18nUtil.get("SIGNATURE_PADES_MESSAGE_SIGNATAIRE") + " :\n" + (cn != null ? cn : "");
				VisualSignatureRectangle positionSignature = new VisualSignatureRectangle(10, 10, 210, 60);
				SignaturePades.signerFichierPdf(fichierPdfInputStream, fichierPdfSigneOutputStream, certificat, privateKey, positionSignature, null, null, signatureTexte);

				InfosComplementairesCertificat infosVerificationCertificat = CertificatUtil.extraireInformations(certificat, new InfosVerificationCertificat());
				infosVerificationCertificat.setSignatureValide(true);
				infosComplementairesCertificats.add(infosVerificationCertificat);

				// on affiche les informations sur le fichier dans le premier
				// onglet
				afficherDetailsSignatures(infosComplementairesCertificats, detailsSignaturesO5, true);

				JOptionPane.showMessageDialog(null, I18nUtil.get("PDF_ENREGISTER_DANS") + CosignForm.this.namePdfSign, I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);

				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void btnParcourirSignatureActionPerformed(java.awt.event.ActionEvent evt) {
		// fileSignNameOngVer.setText("");
		final JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle(I18nUtil.get("SELECTIONNEZ_LE_FICHIER"));
		if (!this.pathFileToVerSign.equals("")) {
			chooser.setSelectedFile(new File(this.pathFileToVerSign));
		} else {
			chooser.setSelectedFile(new File(this.lastChosenFileFolder));
		}
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			fileSignNameOngVer.setText(chooser.getSelectedFile().getAbsolutePath());
			this.lastChosenFileFolder = chooser.getSelectedFile().getAbsolutePath();
			this.nameFileToVeriSign = chooser.getSelectedFile().getName();
			if (!((CosignForm.this.nameFileToVeriSign.substring(CosignForm.this.nameFileToVeriSign.lastIndexOf('.')).equals(".p7s"))) && !((CosignForm.this.nameFileToVeriSign.substring(CosignForm.this.nameFileToVeriSign.lastIndexOf('.')).equals(".pdf")))
					&& !((CosignForm.this.nameFileToVeriSign.substring(CosignForm.this.nameFileToVeriSign.lastIndexOf('.')).equals(".xml")))) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_FILE_SIGNE"), I18nUtil.get("CHOIX_FILE_SIGNE"), JOptionPane.INFORMATION_MESSAGE);
			}

		}
	}

	public static int alertZipFileExistance(String theMessage) {
		int result = JOptionPane.showConfirmDialog(null, theMessage, "Warning", JOptionPane.YES_NO_OPTION);

		return result;
	}

	private void btnEnregistrerO4ActionPerformed(java.awt.event.ActionEvent evt) {
		if (btnEnregistrerO4.getText().equals(I18nUtil.get("AFFICHER")) && !folderExport.equals("")) {
			ListExportedFiles listExportedFiles = new ListExportedFiles(folderExport, this.nameFileToExport);
			listExportedFiles.setVisible(true);
		}
		if (btnEnregistrerO4.getText().equals(I18nUtil.get("ENREGISTRER"))) {
			if (fileNameExport.getText().equals("")) {
				JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUN_DOCUMENT_SAISI"), I18nUtil.get("CHOIX_FILE_EXPORTER"), JOptionPane.INFORMATION_MESSAGE);
			} else {
				String dirChoose = FileManipulation.chooseFolder(I18nUtil.get("OUVRIR"), fileNameExport.getText());
				this.folderExport = dirChoose;
				String emplacement = dirChoose + File.separator + this.nameFileToExport;
				this.newFileDestination.setText(emplacement);
				try {
					Object[] options = { I18nUtil.get("OUI"), I18nUtil.get("NON"), };
					// exporter le fichier seul ou bien les signatures aussi
					int exportClic = JOptionPane.showOptionDialog(this, I18nUtil.get("EXPORTER_DOCUMENT_OU_SIGNATURES_AUSSI"), I18nUtil.get("ALERTE"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);
					// enregister le fichier seulement
					if (exportClic != JOptionPane.YES_OPTION) {
						if (new File(emplacement).exists()) {
							int userClic = JOptionPane.showOptionDialog(this, I18nUtil.get("FICHIER_EXISTE_ECRASER"), I18nUtil.get("CONFIRMATION"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);

							if (userClic != JOptionPane.YES_OPTION) {
								this.newFileDestination.setText("");
								JOptionPane.showMessageDialog(null, I18nUtil.get("CHOIX_AUTRE_EMPLACEMENT"), I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
							} else {
								btnEnregistrerO4.setVisible(false);
								FileManipulation.copy(new File(this.pathfileToExport), new File(this.newFileDestination.getText()));
								JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_EXPORTER_ENREGISTRER_DANS") + emplacement, I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
							}
						} else {
							btnEnregistrerO4.setVisible(false);
							FileManipulation.copy(new File(this.pathfileToExport), new File(this.newFileDestination.getText()));
							JOptionPane.showMessageDialog(null, I18nUtil.get("FICHIER_EXPORTER_ENREGISTRER_DANS") + emplacement, I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
						}
					}
					// enregistrer le fichier et les signatures
					else {
						if (FileManipulation.verifyIfFilesFolderExistInAnotherFolder(this.tmpunzipFiles, dirChoose)) {
							int existingFiles = JOptionPane.showOptionDialog(this, I18nUtil.get("FILES_EXIST_ECRASER"), I18nUtil.get("CONFIRMATION"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[1]);
							if (existingFiles != JOptionPane.YES_OPTION) {
								// TODO
							} else {
								// copy files to new folder
								FileManipulation.copyDirectory(new File(this.tmpunzipFiles), new File(dirChoose));
								nouvelEmplacement.setText(I18nUtil.get("DETAIL_EXPORTED"));
								btnEnregistrerO4.setText(I18nUtil.get("AFFICHER"));
								this.newFileDestination.setText("");
							}
						} else {
							// copy files to new folder
							FileManipulation.copyDirectory(new File(this.tmpunzipFiles), new File(dirChoose));
							nouvelEmplacement.setText(I18nUtil.get("DETAIL_EXPORTED"));
							btnEnregistrerO4.setText(I18nUtil.get("AFFICHER"));
							this.newFileDestination.setText("");
						}
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getUrlModuleValidation() {
		return urlModuleValidation;
	}

	public void setParam5(String urlModuleValidation) {
		this.urlModuleValidation = urlModuleValidation;
	}

	private void afficherDetailsSignatures(List<InfosComplementairesCertificat> informations, JTable tableInformationsSignatures, boolean nettoyer) {

		Object[][] data = informations == null || informations.isEmpty() ? null : extraireInformations(informations);
		DefaultTableModel tableModel = new DefaultTableModel(data, new String[] { I18nUtil.get("TEXT_COLUMN_1"), I18nUtil.get("TEXT_COLUMN_2"), I18nUtil.get("TEXT_COLUMN_3"), I18nUtil.get("TEXT_COLUMN_4"), I18nUtil.get("TEXT_COLUMN_4") }) {
			Class[] types = new Class[] { java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class };
			boolean[] canEdit = new boolean[] { false, false, false, false, false };

			@Override
			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return canEdit[columnIndex];
			}
		};
		tableInformationsSignatures.setModel(tableModel);
		tableInformationsSignatures.getColumnModel().getColumn(4).setPreferredWidth(0);
		tableInformationsSignatures.getColumnModel().getColumn(4).setMinWidth(0);
		tableInformationsSignatures.getColumnModel().getColumn(4).setMaxWidth(0);
		tableInformationsSignatures.getTableHeader().setReorderingAllowed(false);

		// on vide le tableau
		if (nettoyer) {
			informations.clear();
		}
	}

	private Object[][] extraireInformations(List<InfosComplementairesCertificat> informations) {
		Object[][] donnees = new Object[20][4];
		for (int i = 0; i < informations.size(); i++) {
			InfosComplementairesCertificat info = informations.get(i);
			// signataire
			donnees[i][0] = info.getSignatairePartiel();
			// emmeteur
			donnees[i][1] = info.getEmetteur();
			// période de validité
			donnees[i][2] = info.getPeriodiciteValide() ? "Valide" : "Expiré";
			// rejeu
			donnees[i][3] = info.getSignatureValide() ? "Valide" : "Invalide";
		}

		return donnees;
	}
}