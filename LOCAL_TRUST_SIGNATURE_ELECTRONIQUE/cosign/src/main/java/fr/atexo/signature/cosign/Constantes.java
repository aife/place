package fr.atexo.signature.cosign;

/**
 */
public class Constantes {

    public static final String SIGNATURE_PKCS7_FILE_EXTENSION_REGEX = " - Signature ([0-9]+)\\.p7s";
    public static final String SIGNATURE_XADES_FILE_EXTENSION_REGEX_DATE = " - ([0-9]+)\\ - Signature ([0-9]+)\\.xml";
    public static final String SIGNATURE_FILE_EXTENSION_PCKS7 = ".p7s";
    public static final String SIGNATURE_FILE_EXTENSION_XADES = ".xml";
    public static final String SIGNATURE_FILE_EXTENSION_ZIP = ".zip";
}
