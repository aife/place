package fr.atexo.signature.util;

import fr.atexo.signature.gui.browser.SelectionRepertoire;

import java.io.*;

@Deprecated
public class FileManipulation {

	private static String lastOpenedDir = null;

	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static String chooseFolder(String dialogTitre, String latDir) {
		SelectionRepertoire dirChooser;
		if (lastOpenedDir != null) {
			dirChooser = new SelectionRepertoire(new File(lastOpenedDir));
		} else {
			dirChooser = new SelectionRepertoire();
		}
		if (latDir != null) {
			dirChooser = new SelectionRepertoire(new File(latDir));
		}
		dirChooser.setDialogTitle(dialogTitre);
		String strShowDialog = dirChooser.showDialog();

		if (dirChooser.getSelectedFile().getAbsolutePath().length() != 0) {
			lastOpenedDir = dirChooser.getSelectedFile().getAbsolutePath();
		}
		return strShowDialog;
	}

	public static boolean verifyIfFilesFolderExistInAnotherFolder(String folderSrc, String folderDest) {
		File file = new File(folderSrc);
		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; i++) {
				if (new File(folderDest + File.separator + children[i]).exists()) {
					return true;
				}
			}
		}
		return false;
	}

	// This Java function demonstrates a method of copying a directory from one location to another. Copying is done from sourcedirectory to targetdirectory.
	public static void copyDirectory(File sourceLocation, File targetLocation) throws IOException {

		if (sourceLocation.isDirectory()) {
			if (!targetLocation.exists()) {
				targetLocation.mkdir();
			}

			String[] children = sourceLocation.list();
			for (int i = 0; i < children.length; i++) {
				copyDirectory(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
			}
		} else {

			InputStream in = new FileInputStream(sourceLocation);
			OutputStream out = new FileOutputStream(targetLocation);

			// Copy the bits from instream to outstream
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}
}
