package fr.atexo;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 */
public abstract class AbstractServlet extends HttpServlet {

    public static final String SYSTEM_REPERTOIRE_TEMPORAIRE = "java.io.tmpdir";

    protected static final File REPERTOIRE_TEMPORAIRE;

    static {
        String systemTempDirPath = System.getProperty(SYSTEM_REPERTOIRE_TEMPORAIRE);
        REPERTOIRE_TEMPORAIRE = new File(systemTempDirPath);
    }

    protected static void renderMessage(HttpServletResponse response, String message, String contentType) throws IOException {

        PrintWriter out = null;

        try {
            response.setContentType(contentType + "; charset=" + CharEncoding.UTF_8);
            response.setCharacterEncoding(CharEncoding.UTF_8);
            out = response.getWriter();
            out.print(message);
            out.flush();
            out.close();
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    protected void renderHtmlMessage(HttpServletResponse response, String message) throws IOException {
        renderMessage(response, message, "text/html");
    }

    protected File creerRepertoireOuFichier(File repertoireParent, String nom, boolean repertoire) throws IOException {
        File nouveau = new File(repertoireParent, nom);
        if (!nouveau.exists()) {
            if (repertoire) {
                nouveau.mkdirs();
            } else {
                nouveau.createNewFile();
            }

        }
        return nouveau;
    }

    protected List<FileItem> getFichiers(HttpServletRequest req) throws FileUploadException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload fileUpload = new ServletFileUpload(factory);
        List<FileItem> fileUploadItems = fileUpload.parseRequest((HttpServletRequest) req);
        return fileUploadItems;
    }
}
