package fr.atexo.module.validation.servlet;

import fr.atexo.AbstractServlet;
import fr.atexo.commun.util.FichierTemporaireUtil;
import fr.atexo.json.reponse.Repertoire;
import fr.atexo.json.reponse.verification.*;
import fr.atexo.signature.commun.securite.certificat.CertificatVerification;
import fr.atexo.signature.commun.securite.certificat.InfosCertificat;
import fr.atexo.signature.commun.securite.processor.signature.InfosVerificationCertificat;
import fr.atexo.signature.commun.securite.signature.SignatureXades;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.securite.signature.XMLConstantes;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.validation.TypeEchange;
import net.iharder.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Servlet du module de validation.
 * Config avec spring.
 */
public class SignatureXadesServlet extends AbstractServlet {

    private static final Logger LOG = LoggerFactory.getLogger(SignatureXadesServlet.class);

    private static final Logger LOG_CSV = LoggerFactory.getLogger(SignatureXadesServlet.class.getName() + ".CVS_LOGGER");

    private static final String REPERTOIRE_ENTREE = "in";

    private static final String REPERTOIRE_SORTIE = "out";

    private static File repertoireTrust;

    private static File repertoireStockageFichierXML = FichierTemporaireUtil.getRepertoireSystemeTemporaire();

    private static File repertoireEntreeStockageFichierXML;

    private static File repertoireSortieStockageFichierXML;

    private static Pattern pattern;

    static {
        // on désactive l'ensemble des logs généré par les classes héritant de cette classe.
        LogManager.getInstance().setActiverLogger(false);
    }

    private File getRepertoire(String cheminRepertoire) {

        if (cheminRepertoire != null) {
            File file = new File(cheminRepertoire);
            if (file.exists() && file.isDirectory()) {
                return file;
            }
        }
        return null;
    }

    public void setCheminRepertoireTrust(String cheminRepertoireTrust) {
        repertoireTrust = getRepertoire(cheminRepertoireTrust);
    }

    public void setCheminRepertoireStockageFichierXML(String cheminRepertoireStockageFichierXML) {
        repertoireStockageFichierXML = getRepertoire(cheminRepertoireStockageFichierXML);

        if (!repertoireStockageFichierXML.exists()) {
            try {
                repertoireStockageFichierXML.mkdir();
            } catch (SecurityException e) {
            }
        }

        if (repertoireStockageFichierXML.exists()) {
            // on crée le répertoire dans lequel sera stocké les fichiers xml de signature entrnat (non enrichi)           
            repertoireEntreeStockageFichierXML = new File(repertoireStockageFichierXML, REPERTOIRE_ENTREE);
            if (!repertoireEntreeStockageFichierXML.exists()) {
                try {
                    repertoireEntreeStockageFichierXML.mkdir();
                } catch (SecurityException e) {
                }
            }
            // on crée le répertoire dans lequel sera stocké les fichiers xml de signature enrichi
            repertoireSortieStockageFichierXML = new File(repertoireStockageFichierXML, REPERTOIRE_SORTIE);
            if (!repertoireSortieStockageFichierXML.exists()) {
                try {
                    repertoireSortieStockageFichierXML.mkdir();
                } catch (SecurityException e) {
                }
            }
        }
    }

    public void setRegexRepertoire(String regexRepertoire) {
        if (regexRepertoire != null) {
            try {
                pattern = Pattern.compile(regexRepertoire);
            } catch (PatternSyntaxException e) {
                LOG.error(e.getMessage(), e);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // paramétres obligatoires
        FileItem fileItemFichierXML = null;
        TypeEchange typeEchange = null;

        // paramétre obligatoire dans le cas TypeEchange.EnrichissementSignature
        TypeAlgorithmHash typeAlgorithmHash = null;
        // paramétre obligatoire dans le cas TypeEchange.VerificationSignatureServeur
        String hashFichier = null;

        // paramétres optionnels
        String originePlateforme = null;
        String origineOrganisme = null;
        String origineItem = null;
        String origineContexteMetier = null;

        try {
            LOG.debug("Appel du module du serveur de validation");
            List<FileItem> fileUploadItems = getFichiers(req);

            for (FileItem fileItem : fileUploadItems) {
                String fieldName = fileItem.getFieldName();
                if (fieldName.equals(TransfertUtil.CONTENU_FICHIER_XML)) {
                    fileItemFichierXML = fileItem;
                } else if (fieldName.endsWith(TransfertUtil.TYPE_ECHANGE)) {
                    typeEchange = TypeEchange.valueOf(fileItem.getString());
                } else if (fieldName.endsWith(TransfertUtil.TYPE_ALGORITHM_HASH)) {
                    typeAlgorithmHash = TypeAlgorithmHash.valueOf(fileItem.getString());
                } else if (fieldName.endsWith(TransfertUtil.HASH_FICHIER)) {
                    hashFichier = fileItem.getString();
                } else if (fieldName.endsWith(TransfertUtil.ORIGINE_PLATEFORME)) {
                    originePlateforme = fileItem.getString();
                } else if (fieldName.endsWith(TransfertUtil.ORIGINE_ORGANISME)) {
                    origineOrganisme = fileItem.getString();
                } else if (fieldName.endsWith(TransfertUtil.ORIGINE_ITEM)) {
                    origineItem = fileItem.getString();
                } else if (fieldName.endsWith(TransfertUtil.ORIGINE_CONTEXTE_METIER)) {
                    origineContexteMetier = fileItem.getString();
                }
            }

            MessageReponse reponse = verifierParametres(fileItemFichierXML, typeEchange, typeAlgorithmHash, hashFichier);

            if (reponse == null) {

                if (typeEchange == TypeEchange.RetourDateServeur) {
                    LOG.info("Type d'échange : " + typeEchange);
                    String dateISO8601 = Util.creerISO8601DateTime(new Date());
                    LOG.info("Date format ISO8601 : " + dateISO8601);
                    reponse = new ReponseDateServeur(CodeReponse.Ok, dateISO8601);
                } else {
                    LOG.info("Origine Plateforme : " + originePlateforme + " | Organisme : " + origineOrganisme + " | Item : " + origineItem + " | Contexte métier : " + origineContexteMetier);
                    LOG.info("Type d'échange : " + typeEchange);
                    LOG.info("Type d'algorith de hash : " + typeAlgorithmHash);
                    LOG.info("Hash du fichier envoyé : " + hashFichier);
                    String contenuFichierXML = new String(Base64.decode(fileItemFichierXML.getString(FileUtil.ENCODING_UTF_8)));
                    X509Certificate certificate = SignatureXades.extraireCertificat(contenuFichierXML);
                    String cnSubject = CertificatUtil.getCN(certificate.getSubjectX500Principal());
                    String cnIssuer = CertificatUtil.getCN(certificate.getIssuerX500Principal());
                    String serialNumber = String.valueOf(certificate.getSerialNumber());
                    String usage = CertificatUtil.getUtilisablePour(certificate);

                    String nomFichier = Util.supprimerNomFichierCaracteresNonSupportes(cnSubject);
                    String nomFichierSignature = Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_YYYMMDDHHMMSSSS) + " - " + nomFichier + ".xml";
                    File fichierEntree = new File(repertoireEntreeStockageFichierXML, nomFichierSignature);
                    LOG.info("Fichier xml en entrée : " + fichierEntree.getAbsolutePath());
                    LOG.info("CN Signataire : " + cnSubject + " | CN Emetteur : " + cnIssuer + " | Serial number : " + serialNumber + " | Utilisable pour : " + usage);

                    FileUtils.writeStringToFile(fichierEntree, contenuFichierXML, FileUtil.ENCODING_UTF_8);
                    String cheminRepertoireTrust = repertoireTrust != null ? repertoireTrust.getAbsolutePath() : null;
                    InfosCertificat infosCertificat = new InfosCertificat(certificate);
                    CertificatVerification certificatVerification = new CertificatVerification(infosCertificat, cheminRepertoireTrust, pattern);
                    RetourVerification retourVerification = certificatVerification.effectuerEnsembleVerifications();
                    LOG.info("Résultat de la vérification : " + retourVerification);
                    Date dateSignature = SignatureXades.getDateSignature(contenuFichierXML);
                    Boolean dateSignatureValide = null;
                    if (dateSignature != null) {
                        dateSignatureValide = Util.isDateCompriseEntre(dateSignature, certificate.getNotBefore(), certificate.getNotAfter());
                    }
                    LOG.info("Date de signature valide : " + dateSignatureValide);

                    String cheminFichierEnrichi = null;

                    switch (typeEchange) {
                        case EnrichissementSignature:
                            contenuFichierXML = SignatureXades.ajouterUnsignedProperties(contenuFichierXML, certificatVerification.getInfosCertificats(), certificatVerification.getInfosCrls(), typeAlgorithmHash);
                            File fichierSortie = new File(repertoireSortieStockageFichierXML, nomFichierSignature);
                            FileUtils.writeStringToFile(fichierSortie, contenuFichierXML, FileUtil.ENCODING_UTF_8);
                            cheminFichierEnrichi = fichierSortie.getAbsolutePath();
                            LOG.info("Fichier xml en sortie : " + cheminFichierEnrichi);
                            reponse = new ReponseEnrichissementSignature(CodeReponse.Ok, retourVerification, dateSignatureValide, Base64.encodeBytes(contenuFichierXML.getBytes(FileUtil.ENCODING_UTF_8)));
                            break;
                        case VerificationSignatureClient:
                            reponse = new ReponseVerificationSignature(CodeReponse.Ok, retourVerification, dateSignatureValide);
                            break;
                        case VerificationSignatureServeur:
                            List<String> hashs = new ArrayList<String>();
                            hashs.add(hashFichier);
                            InfosVerificationCertificat infosVerificationSignature = SignatureXades.verifier(contenuFichierXML, hashs);
                            LOG.info("Résultat de la vérification serveur : " + infosVerificationSignature.getEtat());
                            reponse = new ReponseVerificationSignatureServeur(CodeReponse.Ok, retourVerification, dateSignatureValide, XMLConstantes.VALIDATION_OK == infosVerificationSignature.getEtat());
                            break;
                    }
                    String chaineCsv = construireLigneCSV(new Date(), typeEchange, originePlateforme, origineOrganisme, origineItem, origineContexteMetier, cnSubject, cnIssuer, serialNumber, usage, retourVerification, fichierEntree.getAbsolutePath(), cheminFichierEnrichi);
                    LOG_CSV.error(chaineCsv);
                }
            }
            LOG.info("Reponse à partir duquel sera généré le json : " + reponse);
            JSONObject jsonObject = new JSONObject(reponse, true);
            String jsonString = jsonObject.toString();
            LOG.debug("Retour du module de validation : " + jsonString);
            renderHtmlMessage(resp, jsonString);

        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (fileItemFichierXML != null) {
                fileItemFichierXML.delete();
            }
        }
    }

    private MessageReponse verifierParametres(FileItem fileItemFichierXML, TypeEchange typeEchange, TypeAlgorithmHash typeAlgorithmHash, String hashHexadecimal) {
        MessageReponse reponse = null;

        if (typeEchange == null) {
            reponse = new MessageReponse(CodeReponse.TypeValidationNonPresent);
            return reponse;
        }

        switch (typeEchange) {
            case EnrichissementSignature:
                if (fileItemFichierXML == null) {
                    reponse = new MessageReponse(CodeReponse.ContenuFichierXmlNonPresent);
                }
                if (typeAlgorithmHash == null) {
                    reponse = new MessageReponse(CodeReponse.TypeAlgorithmHashNonPresent);
                }
                break;
            case VerificationSignatureClient:
                if (fileItemFichierXML == null) {
                    reponse = new MessageReponse(CodeReponse.ContenuFichierXmlNonPresent);
                }
                break;
            case VerificationSignatureServeur:
                if (fileItemFichierXML == null) {
                    reponse = new MessageReponse(CodeReponse.ContenuFichierXmlNonPresent);
                }

                if (Util.estVide(hashHexadecimal)) {
                    reponse = new MessageReponse(CodeReponse.HashFichierNonPresent);
                }
                break;
        }

        return reponse;
    }

    private String construireLigneCSV(Date date, TypeEchange typeEchange, String originePlateforme, String origineOrganisme, String origineItem, String origineContexteMetier, String cnSubject, String cnIssuer, String serial, String usage, RetourVerification retourVerification, String cheminFichierXadesOrigine, String cheminFichierXadesEnrichi) {

        StringBuilder builder = new StringBuilder();
        builder.append(Util.formaterDate(date, Util.DATE_TIME_PATTERN_TIRET));
        builder.append(";");
        builder.append(typeEchange.name());
        builder.append(";");
        if (!Util.estVide(originePlateforme)) {
            builder.append(originePlateforme);
        }
        builder.append(";");
        if (!Util.estVide(origineOrganisme)) {
            builder.append(origineOrganisme);
        }
        builder.append(";");
        if (!Util.estVide(origineItem)) {
            builder.append(origineItem);
        }
        builder.append(";");
        if (!Util.estVide(origineContexteMetier)) {
            builder.append(origineContexteMetier);
        }
        builder.append(";");
        builder.append(cnSubject);
        builder.append(";");
        builder.append(cnIssuer);
        builder.append(";");
        builder.append(serial);
        builder.append(";");
        if (!Util.estVide(usage)) {
            builder.append(usage);
        }
        builder.append(";");
        builder.append(retourVerification.getPeriodeValidite());
        builder.append(";");
        builder.append(retourVerification.getChaineCertification());
        builder.append(";");
        builder.append(retourVerification.getRevocation());
        builder.append(";");
        if (retourVerification.getRepertoiresChaineCertification() != null) {
            int i = 1;
            int taille = retourVerification.getRepertoiresChaineCertification().size();
            for (Repertoire repertoire : retourVerification.getRepertoiresChaineCertification()) {
                builder.append(repertoire.getNom());
                if (i != taille) {
                    builder.append("|");
                }
                i++;
            }
        }
        builder.append(";");
        builder.append(cheminFichierXadesOrigine);
        builder.append(";");
        if (!Util.estVide(cheminFichierXadesEnrichi)) {
            builder.append(cheminFichierXadesEnrichi);
        }
        builder.append(";");

        return builder.toString();
    }
}
