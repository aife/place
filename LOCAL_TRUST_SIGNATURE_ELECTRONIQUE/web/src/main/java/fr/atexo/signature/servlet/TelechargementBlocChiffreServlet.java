package fr.atexo.signature.servlet;

import fr.atexo.AbstractServlet;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 */
public class TelechargementBlocChiffreServlet extends AbstractServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uid = req.getParameter("organisme");
        String idBlocDeChiffrement = req.getParameter("idBlob");

        String nomFichier = idBlocDeChiffrement + ".tmp";
        File repertoireUid = creerRepertoireOuFichier(REPERTOIRE_TEMPORAIRE, uid + "_chiffre", true);

        File fichier = null;
        File[] fichiers = repertoireUid.listFiles();
        for (File file : fichiers) {
            if (file.getName().equals(nomFichier)) {
                fichier = file;
                break;
            }
        }

        InputStream inputStream = new FileInputStream(fichier);
        IOUtils.copy(inputStream, resp.getOutputStream());
        IOUtils.closeQuietly(inputStream);
    }
}
