package fr.atexo.signature.servlet;

import fr.atexo.AbstractServlet;
import fr.atexo.signature.util.TransfertUtil;
import net.iharder.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 */
public class EnvoiBlocDechiffreServlet extends AbstractServlet {

    private static final String UID = "organisme";
    private static final String ID_BLOC_DONNEES = "idBlob";
    private static final String BLOC_DONNEES = "blocDechiffre";
    private static final String FIN_ENVOI = "endDecrypt";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            List<FileItem> fileUploadItems =  getFichiers(req);

            String uid = null;
            String blocNumeroOrdre = null;
            InputStream blocDonnees = null;
            FileItem fileItemDonnees = null;
            for (FileItem fileItem : fileUploadItems) {
                String fieldName = fileItem.getFieldName();
                if (fieldName.equals(UID)) {
                    uid = fileItem.getString();
                } else if (fieldName.equals(ID_BLOC_DONNEES)) {
                    blocNumeroOrdre = fileItem.getString();
                } else if (fieldName.endsWith(BLOC_DONNEES)) {
                    blocDonnees = fileItem.getInputStream();
                    fileItemDonnees = fileItem;
                }
            }

            if (blocDonnees != null) {
                File repertoireUid = creerRepertoireOuFichier(REPERTOIRE_TEMPORAIRE, uid + "_dechiffre", true);
                File blocFichier = creerRepertoireOuFichier(repertoireUid, blocNumeroOrdre + ".tmp", false);
                FileUtils.copyInputStreamToFile(blocDonnees, blocFichier);
                byte[] contenuDecode = Base64.decode(FileUtils.readFileToByteArray(blocFichier));
                File blocFichierDecode = creerRepertoireOuFichier(repertoireUid, blocNumeroOrdre + "Decode.tmp", false);
                FileUtils.writeByteArrayToFile(blocFichierDecode, contenuDecode);
                fileItemDonnees.delete();
                blocFichier.delete();
            }

            renderHtmlMessage(resp, TransfertUtil.RESULTAT_RETOUR_UPLOAD_VALIDE);

        } catch (FileUploadException e) {
            e.printStackTrace();
            renderHtmlMessage(resp, TransfertUtil.RESULTAT_RETOUR_UPLOAD_INVALIDE);
        }
    }
}
