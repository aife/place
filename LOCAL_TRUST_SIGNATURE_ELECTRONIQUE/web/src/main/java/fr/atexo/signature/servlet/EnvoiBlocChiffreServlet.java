package fr.atexo.signature.servlet;

import fr.atexo.AbstractServlet;
import fr.atexo.signature.util.TransfertUtil;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 */
public class EnvoiBlocChiffreServlet extends AbstractServlet {

    private static final String UID = "uidOffre";
    private static final String TYPE_ENVELOPPE = "typeEnv";
    private static final String NUMERO_LOT = "sousPli";
    private static final String TYPE_FICHIER = "typeFichier";
    private static final String FICHIER_NUMERO_ORDRE = "numOrdreFichier";
    private static final String BLOC_NUMERO_ORDRE = "numOrdreBloc";
    private static final String BLOC_DONNEES = "bloc";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<FileItem> fileUploadItems = getFichiers(req);

            String uid = null;
            String typeEnveloppe = null;
            String numeroLot = null;
            String typeFichier = null;
            String fichierNumeroOrdre = null;
            String blocNumeroOrdre = null;
            InputStream blocDonnees = null;
            FileItem fileItemDonnees = null;
            for (FileItem fileItem : fileUploadItems) {
                String fieldName = fileItem.getFieldName();
                if (fieldName.equals(UID)) {
                    uid = fileItem.getString();
                } else if (fieldName.endsWith(TYPE_ENVELOPPE)) {
                    typeEnveloppe = fileItem.getString();
                } else if (fieldName.endsWith(NUMERO_LOT)) {
                    numeroLot = fileItem.getString();
                } else if (fieldName.endsWith(TYPE_FICHIER)) {
                    typeFichier = fileItem.getString();
                } else if (fieldName.endsWith(FICHIER_NUMERO_ORDRE)) {
                    fichierNumeroOrdre = fileItem.getString();
                } else if (fieldName.endsWith(BLOC_NUMERO_ORDRE)) {
                    blocNumeroOrdre = fileItem.getString();
                } else if (fieldName.endsWith(BLOC_DONNEES)) {
                    blocDonnees = fileItem.getInputStream();
                    fileItemDonnees = fileItem;
                }
            }

            File repertoireUid = creerRepertoireOuFichier(REPERTOIRE_TEMPORAIRE, uid + "_chiffre", true);
            //File repertoireIndexFichier = creerRepertoireOuFichier(repertoireUid, fichierNumeroOrdre, true);
            File blocFichier = creerRepertoireOuFichier(repertoireUid, blocNumeroOrdre + ".tmp", false);
            FileUtils.copyInputStreamToFile(blocDonnees, blocFichier);
            fileItemDonnees.delete();
            renderHtmlMessage(resp, TransfertUtil.RESULTAT_RETOUR_UPLOAD_VALIDE);

        } catch (FileUploadException e) {
            e.printStackTrace();
            renderHtmlMessage(resp, TransfertUtil.RESULTAT_RETOUR_UPLOAD_INVALIDE);
        }

    }
}
