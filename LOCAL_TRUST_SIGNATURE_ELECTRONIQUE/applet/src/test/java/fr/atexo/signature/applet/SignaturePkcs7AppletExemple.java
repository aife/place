package fr.atexo.signature.applet;

import fr.atexo.signature.commun.util.io.HashUtil;

/**
 *
 */
public class SignaturePkcs7AppletExemple extends SignaturePkcs7Applet {

    private static final String CHAINE = "SignaturePkcs7AppletExemple";

    @Override
    public void init() {
        super.init();
        String hash = HashUtil.genererHashSha1Hexadecimal(CHAINE.getBytes());
        ajouterHash(CHAINE, hash);
        executer();
    }
}
