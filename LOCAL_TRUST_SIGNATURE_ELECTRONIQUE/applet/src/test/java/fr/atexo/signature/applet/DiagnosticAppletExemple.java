package fr.atexo.signature.applet;

/**
 *
 */
public class DiagnosticAppletExemple extends DiagnosticApplet {

    @Override
    public void init() {
        super.init();
        setIdentifiantSystemeExploitation("identifiantSystemeExploitation");
        setIdentifiantAppletDemarre("identifiantAppletDemarre");
        setIdentifiantJavaVersionBit("identifiantJavaVersionBit");
        setIdentifiantJavaVersionNumero("identifiantJavaVersionNumero");
        setIdentifiantTestAccesMagasin("identifiantTestAccesMagasin");
        setIdentifiantTestChiffrement("identifiantTestChiffrement");
        executer();
    }
}
