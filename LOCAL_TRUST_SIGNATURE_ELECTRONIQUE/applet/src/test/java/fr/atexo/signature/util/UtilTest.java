package fr.atexo.signature.util;

import fr.atexo.signature.commun.util.Util;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 */
public class UtilTest extends TestCase {

    private String DATE_NOUVEAU_FORMAT = "2013-06-24T18:58:05+02:00";
    private String DATE_ANCIEN_FORMAT = "2013-06-24 18:58:05";

    @Test
    public void testDate() {

        String dateAncienFormat = extraireDate(DATE_ANCIEN_FORMAT);
        String dateNouveauFormat = extraireDate(DATE_NOUVEAU_FORMAT);
        assertTrue(dateAncienFormat.equals(dateNouveauFormat));

    }

    private String extraireDate(String dateAncienFormat) {
        String date = Util.remplacerPar(dateAncienFormat, new String[]{" ", ":", "-", "T"}, "");
        if (date.length() > 14) {
            date = date.substring(0, 14);
        }

        return date;
    }

}
