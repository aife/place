package fr.atexo.signature.applet;

import java.io.File;
import java.io.IOException;

import net.iharder.Base64;

public class SignaturePadesAppletExemple extends SignaturePadesApplet {

	private static final long serialVersionUID = 1L;

	public SignaturePadesAppletExemple() {
		super();
	}

	@Override
	public void init() {
		super.init();
		this.setLocale("en");
		this.setEcraserJavaPolicy("false");

		String fichierPdfPath = SignaturePadesAppletExemple.class.getClassLoader().getResource("fr/atexo/signature/util/FichierPrincipal.pdf").getFile();

		File fichierPdf = new File(fichierPdfPath);
		try {
			String contenuFichierPdfEnBase64 = Base64.encodeFromFile(fichierPdf.getAbsolutePath());
			initialiserSignatureModeMemoire("identifiantFichierPdfSigne", contenuFichierPdfEnBase64);
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*
		 * this.setOrigineFichierSourceServeur(false);
		 * this.setInfosFichierSource
		 * ("/home/atexo/Bureau/Parapheur/FichierPrincipal.pdf");
		 * this.setOrigineFichierDestinationServeur(false);
		 * 
		 * 
		 * this.setOrigineFichierSourceServeur(true); this.setUrlFichierSource(
		 * "http://paraph-local/parapheur/fr.atexo.parapheur.ParapheurModule/fichier?session=3e37c36ad5964f6bbabe22681581b4ce&id=391&supprimer=false"
		 * ); this.setOrigineFichierDestinationServeur(true);
		 * this.setUrlFichierDestination(
		 * "http://paraph-local/parapheur/fr.atexo.parapheur.ParapheurModule/fichierPrincipalSigne?session=3e37c36ad5964f6bbabe22681581b4ce&applicationType=parapheur&id=391&supprimer=false"
		 * );
		 */
		executer();

	}

}
