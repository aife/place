package fr.atexo.signature.util;

import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 */
@Ignore
public class TransfertUtilTest extends TestCase {

    @Test
    public void testTelechargement() throws TransfertExecutionException {
        try {
            byte[] telechargement = TransfertUtil.telechargerBloc("http://localhost:8080/signature/telechargementBlocChiffre", "uid_00001", "0");
            assertTrue(telechargement != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
