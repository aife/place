package fr.atexo.signature.util;

import fr.atexo.json.reponse.applet.Reponse;
import fr.atexo.json.reponse.applet.ReponseSignatureFichier;
import fr.atexo.json.reponse.applet.ReponseSignatureHash;
import fr.atexo.json.reponse.applet.InfosCertificat;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.HashUtil;
import junit.framework.TestCase;
import net.iharder.Base64;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 */
public class JsonTest extends TestCase {

    @Test
    public void testConversionJson() {

        Reponse reponse = new Reponse();
        reponse.setContenuReponseAnnonceXML(Base64.encodeBytes("ExempleXML".getBytes()));
        reponse.setFichiers(new ArrayList<ReponseSignatureFichier>());
        for (int i = 1; i < 3; i++) {
            ReponseSignatureFichier reponseSignatureFichier = new ReponseSignatureFichier();
            reponseSignatureFichier.setIdentifiant("identifiant_fichier_" + i);
            reponseSignatureFichier.setTypeEnveloppe(1);
            reponseSignatureFichier.setNumeroLot(i);
            reponseSignatureFichier.setIndex(i - 1);
            reponseSignatureFichier.setCheminFichier("chemin/version/fichier" + i + ".pdf");
            reponseSignatureFichier.setCheminSignatureXML("chemin/version/fichierSingature" + i + ".xml");
            reponseSignatureFichier.setSignatureVerifiee(true);
            InfosComplementairesCertificat infosSignature = creerInfosSignature();
            reponseSignatureFichier.setInfosSignature(infosSignature);
            reponse.getFichiers().add(reponseSignatureFichier);
        }

        reponse.setHashs(new ArrayList<ReponseSignatureHash>());
        for (int i = 1; i < 2; i++) {
            ReponseSignatureHash reponseSignatureFichier = new ReponseSignatureHash();
            reponseSignatureFichier.setIdentifiant("identifiant_hash_" + i);
            reponseSignatureFichier.setTypeEnveloppe(1);
            reponseSignatureFichier.setNumeroLot(i);
            reponseSignatureFichier.setIndex(i - 1);
            String hash = HashUtil.genererHashSha1Hexadecimal(reponseSignatureFichier.getIdentifiant().getBytes());
            reponseSignatureFichier.setHashFichier(hash);
            reponseSignatureFichier.setContenuSignatureXML(Base64.encodeBytes(hash.getBytes()));
            reponseSignatureFichier.setSignatureVerifiee(true);
            InfosComplementairesCertificat infosSignature = creerInfosSignature();
            reponseSignatureFichier.setInfosSignature(infosSignature);
            reponse.getHashs().add(reponseSignatureFichier);
        }

        JSONObject jsonObject = new JSONObject(reponse, true);
        String reponseJson = jsonObject.toString();
        System.out.println(reponseJson);
        assertTrue(reponseJson != null);
    }

    private InfosComplementairesCertificat creerInfosSignature() {
        InfosComplementairesCertificat infosSignature = new InfosCertificat();
        String date = Util.creerISO8601DateTime(new Date());
        infosSignature.setDateValiditeAu(date);
        infosSignature.setDateValiditeDu(date);
        infosSignature.setEmetteur("AC emmetrice");
        infosSignature.setSignatairePartiel("Mr Dupont George");
        infosSignature.setChaineDeCertificationValide(true);
        infosSignature.setSignatureValide(true);
        infosSignature.setPeriodiciteValide(true);
        infosSignature.setAbsenceRevocationCRL(true);
        return infosSignature;
    }
}
