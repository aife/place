package fr.atexo.signature;

import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.provider.CertificatItem;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.pkcs11.Pkcs11Handler;
import fr.atexo.signature.commun.util.JaxbPkcs11Util;
import fr.atexo.signature.util.SmartCardUtil;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;
import org.junit.Ignore;

import java.io.File;
import java.math.BigInteger;
import java.net.URL;
import java.security.Signature;
import java.util.*;

@Ignore
public final class Pkcs11ATEXO {

    private static final String URL_MODULE_VALIDATION = "http://clancy:8180/signature/signatureXades";

    private static final String PKCS11_URL = "http://clancy:8180/signature/pkcs11Lib/pcks11Libs.xml";

    private static final String FICHIER_A_SIGNER = "/home/atexo/Bureau/chiffrement/FichierAssocie1_signaturePades.pdf";

    private static Pkcs11LibsType getPkcs11LibsType(URL urlTelechargementXML) {

        Pkcs11LibsType pkcs11Libs = null;
        try {
            byte[] contenuFichierXML = TransfertUtil.telecharger(urlTelechargementXML.toString());
            pkcs11Libs = JaxbPkcs11Util.getPkcs11Libs(new String(contenuFichierXML));
        } catch (TransfertExecutionException e) {
            e.printStackTrace();
        }
        return pkcs11Libs;
    }

    public static final void main(final String[] args) throws Exception {

        TypeOs typeOs = TypeOs.Linux;
        Pkcs11LibsType pkcs11Libs = getPkcs11LibsType(new URL(PKCS11_URL));
        List<CertificatItem> certificatItems = new ArrayList<CertificatItem>();
        Set<String> hashCodes = new HashSet<String>();

        Map<String, List<File>> providers = SmartCardUtil.getPkcs11Providers(pkcs11Libs, typeOs);
        if (providers != null && providers.size() > 0) {
            List<CertificatItem> certificatsPkcs11 = Pkcs11Handler.getInstance().recupererCertificats(providers, hashCodes,false);
            certificatItems.addAll(certificatsPkcs11);
        }

        CertificatItem gemaltoUserSig = certificatItems.get(1);

        for (int i = 0; i < 2; i++) {
            System.out.println("Alias : " + gemaltoUserSig.getId());
            KeyPair keyPair = Pkcs11Handler.getInstance().getKeyPair(gemaltoUserSig.getId());
            final Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initSign(keyPair.getPrivateKey());
            signature.update(new byte[128]);
            System.out.println(new BigInteger(1, signature.sign()).toString(16));

        }
    }
}
