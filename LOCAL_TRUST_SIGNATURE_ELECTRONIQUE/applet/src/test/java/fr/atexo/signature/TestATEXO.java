package fr.atexo.signature;

import com.sun.security.auth.callback.DialogCallbackHandler;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.AuthProvider;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.ProviderException;
import java.security.Security;
import java.security.Signature;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.security.auth.callback.CallbackHandler;

import org.junit.Ignore;
import sun.security.pkcs11.SunPKCS11;

@Ignore
public final class TestATEXO {
    private static final class MetaKeyStore extends KeyStore {
        private static final class MetaKeyStoreSpi extends KeyStoreSpi {
            private final Map<String, KeyStore> keyStores;
            private final Set<String> aliases = new TreeSet<String>();

            private MetaKeyStoreSpi(final Map<String, KeyStore> keyStores) {
                this.keyStores = keyStores;
            }

            @Override
            public final Enumeration<String> engineAliases() {
                return Collections.enumeration(aliases);
            }

            @Override
            public final boolean engineContainsAlias(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).containsAlias(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public final void engineDeleteEntry(final String alias) throws KeyStoreException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                keyStores.get(providerNameAndEntryAlias[0]).deleteEntry(providerNameAndEntryAlias[1]);
                aliases.remove(alias);
            }

            @Override
            public final boolean engineEntryInstanceOf(final String alias, final Class<? extends java.security.KeyStore.Entry> entryClass) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).entryInstanceOf(providerNameAndEntryAlias[1], entryClass);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public final Certificate engineGetCertificate(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).getCertificate(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public final String engineGetCertificateAlias(final Certificate cert) {
                for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
                    try {
                        final String entryAlias = entry.getValue().getCertificateAlias(cert);
                        if (entryAlias == null) {
                            continue;
                        } else {
                            return entry.getKey() + " - " + entryAlias;
                        }
                    }
                    catch (final KeyStoreException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            public final Certificate[] engineGetCertificateChain(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).getCertificateChain(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public final Date engineGetCreationDate(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).getCreationDate(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public final java.security.KeyStore.Entry engineGetEntry(final String alias, final ProtectionParameter protParam) throws KeyStoreException,
                    NoSuchAlgorithmException, UnrecoverableEntryException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                return keyStores.get(providerNameAndEntryAlias[0]).getEntry(providerNameAndEntryAlias[1], protParam);
            }

            @Override
            public final Key engineGetKey(final String alias, final char[] password) throws NoSuchAlgorithmException, UnrecoverableKeyException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).getKey(providerNameAndEntryAlias[1], password);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public final boolean engineIsCertificateEntry(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).isCertificateEntry(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public final boolean engineIsKeyEntry(final String alias) {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                try {
                    return keyStores.get(providerNameAndEntryAlias[0]).isKeyEntry(providerNameAndEntryAlias[1]);
                }
                catch (final KeyStoreException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public final void engineLoad(final InputStream stream, final char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
                for (final KeyStore keyStore : keyStores.values()) {
                    keyStore.load(stream, password);
                }

                for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
                    try {
                        final Enumeration<String> keyStoreAliases = entry.getValue().aliases();
                        while (keyStoreAliases.hasMoreElements()) {
                            aliases.add(entry.getKey() + " - " + keyStoreAliases.nextElement());
                        }
                    }
                    catch (final KeyStoreException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public final void engineLoad(final LoadStoreParameter param) throws IOException, NoSuchAlgorithmException, CertificateException {
                for (final KeyStore keyStore : keyStores.values()) {
                    keyStore.load(param);
                }

                for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
                    try {
                        final Enumeration<String> keyStoreAliases = entry.getValue().aliases();
                        while (keyStoreAliases.hasMoreElements()) {
                            aliases.add(entry.getKey() + " - " + keyStoreAliases.nextElement());
                        }
                    }
                    catch (final KeyStoreException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public final void engineSetCertificateEntry(final String alias, final Certificate cert) throws KeyStoreException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                keyStores.get(providerNameAndEntryAlias[0]).setCertificateEntry(providerNameAndEntryAlias[1], cert);
                aliases.add(alias);
            }

            @Override
            public final void engineSetEntry(final String alias, final java.security.KeyStore.Entry entry, final ProtectionParameter protParam)
                    throws KeyStoreException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                keyStores.get(providerNameAndEntryAlias[0]).setEntry(providerNameAndEntryAlias[1], entry, protParam);
                aliases.add(alias);
            }

            @Override
            public final void engineSetKeyEntry(final String alias, final byte[] key, final Certificate[] chain) throws KeyStoreException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                keyStores.get(providerNameAndEntryAlias[0]).setKeyEntry(providerNameAndEntryAlias[1], key, chain);
                aliases.add(alias);
            }

            @Override
            public final void engineSetKeyEntry(final String alias, final Key key, final char[] password, final Certificate[] chain) throws KeyStoreException {
                final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
                keyStores.get(providerNameAndEntryAlias[0]).setKeyEntry(providerNameAndEntryAlias[1], key, password, chain);
                aliases.add(alias);
            }

            @Override
            public final int engineSize() {
                return aliases.size();
            }

            @Override
            public final void engineStore(final LoadStoreParameter param) throws IOException, NoSuchAlgorithmException, CertificateException {
                for (final KeyStore keyStore : keyStores.values()) {
                    try {
                        keyStore.store(param);
                    }
                    catch (final KeyStoreException e) {
                        throw new IOException(e);
                    }
                }
            }

            @Override
            public final void engineStore(final OutputStream stream, final char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
                for (final KeyStore keyStore : keyStores.values()) {
                    try {
                        keyStore.store(stream, password);
                    }
                    catch (final KeyStoreException e) {
                        throw new IOException(e);
                    }
                }
            }
        }

        protected MetaKeyStore(final Map<String, KeyStore> keyStores) {
            super(new MetaKeyStoreSpi(keyStores), null, null);
        }
    }

    public static final void main(final String[] args) throws Exception {
        final Map<String, String[]> providers = new TreeMap<String, String[]>();
        if (System.getProperty("os.name").startsWith("Windows")) {
            //
        } else if (System.getProperty("os.name").startsWith("Mac OS")) {
            providers.put("Gemalto", new String[]{//
                    "/usr/lib/pkcs11/libgclib.dylib",//
                    "/usr/local/lib/libIASxltCk.dylib"//
            });
            providers.put("Oberthur", new String[]{//
                    "/Library/Security/PKCS11/libOcsCryptoki.dylib",//
                    "/usr/lib/libOcsCryptoki.dylib"//
            });
            providers.put("SafeNet", new String[]{//
                    "/Library/Frameworks/eToken.framework/Versions/Current/libeToken.dylib"//
            });
            providers.put("Sagem", new String[]{//
                    "/usr/local/lib/libCnfPkcs11.dylib"//
            });
        } else {
            providers.put("Gemalto", new String[]{//
                    "/usr/lib/ClassicClient/libgclib.so",
                    "/usr/lib/pkcs11/libgclib.so",//
                    "/usr/lib/libIASxltCk.so"//
            });
            providers.put("Oberthur", new String[]{//
                    "/usr/lib/libOcsCryptoki.so"//
            });
            providers.put("SafeNet", new String[]{//
                    "/usr/lib/libeTPkcs11.so"//
            });
            providers.put("Sagem", new String[]{//
                    "/usr/local/lib/libCnfpkcs11.so"//
            });
        }

        final Map<String, KeyStore> keyStores = new TreeMap<String, KeyStore>();
        final CallbackHandler callbackHandler = new DialogCallbackHandler();
        for (final Entry<String, String[]> entry : providers.entrySet()) {
            final String name = entry.getKey();
            for (final String library : entry.getValue()) {
                if (new File(library).exists()) {
                    final AuthProvider provider;
                    try {
                        provider = new SunPKCS11(new ByteArrayInputStream(("name=" + name + "\n" + "library=" + library + "\n" + "attributes=compatibility").getBytes()));
                    }
                    catch (final ProviderException e) {
                        continue;
                    }
                    provider.setCallbackHandler(callbackHandler);
                    Security.addProvider(provider);
                    try {
                        keyStores.put(name, KeyStore.getInstance("PKCS11", provider));
                    }
                    catch (final KeyStoreException e) {
                        e.printStackTrace();
                        continue;
                    }
                    break;
                }
            }
        }

        if (System.getProperty("os.name").startsWith("Windows")) {
            keyStores.put("Microsoft", KeyStore.getInstance("Windows-MY"));
        } else if (System.getProperty("os.name").startsWith("Mac OS")) {
            keyStores.put("Apple", KeyStore.getInstance("KeychainStore"));
        } else {
            //
        }

        final KeyStore keyStore = new MetaKeyStore(keyStores);
        keyStore.load(null, null);

        final Enumeration<String> aliases = keyStore.aliases();
        while (aliases.hasMoreElements()) {
            final String alias = aliases.nextElement();
            if (!alias.startsWith("Gemalto - User Cert Auth") && keyStore.isKeyEntry(alias)) {
                try {
                    System.out.println(alias);
                    final Signature signature = Signature.getInstance("SHA1withRSA");
                    for (int i = 0; i < 8; i++) {
                        signature.initSign((PrivateKey) keyStore.getKey(alias, " ".toCharArray()));
                        signature.update(new byte[128]);
                        System.out.println(new BigInteger(1, signature.sign()).toString(16));
                    }
                }
                catch (final Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}
