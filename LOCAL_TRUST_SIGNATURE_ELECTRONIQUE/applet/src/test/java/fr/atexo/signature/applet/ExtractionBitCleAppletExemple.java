package fr.atexo.signature.applet;

import fr.atexo.signature.applet.ExtractionBitCleApplet;

import java.net.MalformedURLException;

/**
 *
 */
public class ExtractionBitCleAppletExemple extends ExtractionBitCleApplet {

    @Override
    public void init() {
        super.init();
        try {
            setUrlPkcs11LibRefXml("http://clancy:8180/validation/pkcs11Lib/pcks11Libs.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ajouterRestrictionTypeCertificatChiffrement();
        executer();
    }
}
