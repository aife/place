package fr.atexo.signature.util;

import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.chiffrement.ChiffrementProcessor;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ChiffrementTest extends TestCase {

    private String urlFichier = "fr/atexo/signature/util/FichierPrincipal.pdf";

    private String cheminFichier = "FichierPrincipal.pdf";

    private String cheminFichierChiffre = "FichierPrincipalChiffre.xml";

    private String cheminFichierDechiffre = "FichierPrincipalDechiffre.pdf";

    private String repertoireTemporaire = System.getProperty("java.io.tmpdir");

    @Test
    public void testChiffrement() throws IOException, NoSuchProviderException, InvalidKeySpecException, NoSuchAlgorithmException, CertificateException {

        File systemTempDir = new File(repertoireTemporaire);

        InputStream inputStream = searchResource(urlFichier);
        File fichier = new File(systemTempDir, cheminFichier);
        IOUtils.copy(inputStream, new FileOutputStream(fichier));

        // key pair pour effectuer et tester le chiffrement et déchiffrement
        KeyPair keyPair = CertificatUtil.getKeyPairTestChiffrement();

        List<X509Certificate> certificats = new ArrayList<X509Certificate>();
        certificats.add(keyPair.getCertificate());

        // chiffrement
        ChiffrementProcessor chiffrementProcessor = new StandardChiffrementProcessor(certificats);

        long tailleFichier = fichier.length();
        int tailleBlocChiffrementMax = ReponseAnnonceConstantes.TAILLE_MAXIMALE_BLOC_CHIFFREMENT;
        int tailleBlocChiffrement = 0;

        for (int i = 0; i * tailleBlocChiffrementMax <= tailleFichier; i++) {

            if ((i * tailleBlocChiffrementMax + tailleBlocChiffrementMax) <= tailleFichier) {
                tailleBlocChiffrement = tailleBlocChiffrementMax;
            } else {
                tailleBlocChiffrement = (int) tailleFichier - (i * tailleBlocChiffrementMax);
            }

            try {

                byte[] contenuBloc = FileUtil.lire(fichier, i * tailleBlocChiffrementMax, tailleBlocChiffrement);

                byte[] resultatChiffrement = chiffrementProcessor.chiffrer(contenuBloc);

                File fichierChiffre = new File(systemTempDir, cheminFichierChiffre);
                fichierChiffre.createNewFile();
                FileUtils.writeByteArrayToFile(fichierChiffre, resultatChiffrement);
                assertTrue(fichierChiffre.exists());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testDechiffrement() throws IOException, NoSuchProviderException, InvalidKeySpecException, NoSuchAlgorithmException, CertificateException {

        File systemTempDir = new File(repertoireTemporaire);

        // key pair pour effectuer et tester le chiffrement et déchiffrement
        KeyPair keyPair = CertificatUtil.getKeyPairTestChiffrement();

        try {

            File fichierChiffre = new File(systemTempDir, cheminFichierChiffre);
            byte[] contenuFichierChiffre = FileUtils.readFileToByteArray(fichierChiffre);
            String contenuChiffrement = new String(contenuFichierChiffre);

            // déchiffrement
            DechiffrementProcessor dechiffrementProcessor = new StandardChiffrementProcessor(keyPair.getProvider(), keyPair);
            byte[] resultatDechiffrement = dechiffrementProcessor.dechiffrer(contenuChiffrement);

            File fichierDechiffre = new File(systemTempDir, cheminFichierDechiffre);
            fichierDechiffre.createNewFile();
            FileUtils.writeByteArrayToFile(fichierDechiffre, resultatDechiffrement);

            assertTrue(fichierDechiffre.exists());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static InputStream searchResource(String resource) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        InputStream result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        loader = ClassLoader.getSystemClassLoader();

        result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        throw new IOException("Aucun class loader n'a été trouvé pour charger la resource nommé : " + resource);
    }

    private static InputStream getInputStreamFormUrl(URL url) {
        if (url != null) {

            try {
                URLConnection connection = url.openConnection();
                return connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
