package fr.atexo.signature.barreprogression;

import fr.atexo.signature.gui.barreprogression.BarreDeProgression;
import fr.atexo.signature.gui.provider.pkcs12.ui.MainFrame;
import junit.framework.TestCase;
import org.junit.Ignore;
import org.junit.Test;

/**
 * .
 */
@Ignore
public class SwingTest extends TestCase {

    @Test
    public void testBarreProgression() throws InterruptedException {
        int nombreFichiers = 10;
        int nombreBlocs = 11;
        BarreDeProgression barreDeProgression = new BarreDeProgression("Barre de progression", nombreFichiers);

        for (int i = 0; i < nombreFichiers; i++) {

            barreDeProgression.initialiserFichier((nombreBlocs * 2) + 2);

            // traitement fichier
            String messageTraitementFichier = "Fichier " + (i + 1) + "/" + nombreFichiers + " - Enveloppe des Candidature - FichierExemple.pdf (124Ko)";
            barreDeProgression.setMessageProgressionTraitementFichier(messageTraitementFichier);

            // signature PAdES
            String messageProgressionGlobale = "Signature PAdES en cours de génération...";
            barreDeProgression.setMessageProgressionGlobale(messageProgressionGlobale);
            barreDeProgression.incrementerProgressionGlobale();

            // signature XAdES
            messageProgressionGlobale = "Signature XAdES en cours de génération...";
            barreDeProgression.setMessageProgressionGlobale(messageProgressionGlobale);
            barreDeProgression.incrementerProgressionGlobale();

            for (int j = 0; j < nombreBlocs; j++) {
                String messageBloc = "Bloc " + (j + 1) + "/" + nombreBlocs + " - ";
                // chiffrement
                messageProgressionGlobale = messageBloc + "Chiffrement en cours...";
                barreDeProgression.setMessageProgressionGlobale(messageProgressionGlobale);
                barreDeProgression.incrementerProgressionGlobale();

                // transfert
                messageProgressionGlobale = messageBloc + "Transfert en cours...";
                barreDeProgression.setMessageProgressionGlobale(messageProgressionGlobale);
                barreDeProgression.incrementerProgressionGlobale();
            }

            // passage au prochain fichier
            barreDeProgression.incrementerProgressionTraitementFichier();
            barreDeProgression.reinitialiserProgressionGlobale();
        }
    }
}
