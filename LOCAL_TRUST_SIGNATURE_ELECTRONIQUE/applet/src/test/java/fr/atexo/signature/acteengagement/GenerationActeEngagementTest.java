package fr.atexo.signature.acteengagement;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;

@Ignore
public class GenerationActeEngagementTest {

	private static final String XML_FILE = "fr/atexo/signature/acteengagement/ActeEngagement.xml";

	private Document document;

	@Before
	public void setUp() throws ParserConfigurationException, SAXException, IOException {
		InputStream xmlFileStream = GenerationActeEngagementTest.class.getClassLoader().getResourceAsStream(XML_FILE);
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		InputSource inputSource = new InputSource(xmlFileStream);
		document = documentBuilder.parse(inputSource);
	}

	@Test
	public void testGen() throws TransfertExecutionException, ParserConfigurationException, SAXException, IOException, COSVisitorException {
		GenerationActeEngagement generationActeEngagementPDFBox = new GenerationActeEngagement();

		generationActeEngagementPDFBox.genererActeEngagement(document, new File("c:/Config/test.pdf"), "http://www.newsdegeek.com/wp-content/uploads/2008/12/logo-google-chrome.jpg");

	}
}
