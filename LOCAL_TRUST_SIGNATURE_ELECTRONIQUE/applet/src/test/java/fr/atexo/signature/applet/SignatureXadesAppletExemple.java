package fr.atexo.signature.applet;

import java.net.MalformedURLException;

/**
 *
 */
public class SignatureXadesAppletExemple extends SignatureXadesApplet {

    @Override
    public void init() {
        super.init();
        try {
            setUrlPkcs11LibRefXml("http://localhost:8080/validation/pkcs11Lib/pcks11Libs.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        initialiser("http://localhost:8080/validation/signatureXades");
        ajouterFichier("fichier1", "/home/atexo/Bureau/chiffrement/FichierPrincipal.pdf");
        //ajouterHashFichier("hashFichier1", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf");
        //ajouterHashFichier("hashFichier2", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf");
        //ajouterHashFichier("hashFichier3", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf");
        setOriginePlateforme("CLANCY");
        setOrigineOrganisme("ATEXO-DEV");
        setOrigineItem("4");
        setOrigineContexteMetier("Test-Dev");
        executer();
    }
}
