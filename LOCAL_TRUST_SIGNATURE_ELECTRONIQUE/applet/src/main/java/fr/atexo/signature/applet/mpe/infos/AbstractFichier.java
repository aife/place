package fr.atexo.signature.applet.mpe.infos;

/**
 *
 */
public abstract class AbstractFichier extends AbstractMpeInfos {

    protected String identifiant;

    protected String typeFichier;

    protected String origineFichier;

    protected boolean signatureVerifiee;

    public AbstractFichier(int typeEnveloppe, int numeroLot, int index, String identifiant, String typeFichier, String origineFichier) {
        super(typeEnveloppe, numeroLot, index);
        this.identifiant = identifiant;
        this.typeFichier = typeFichier;
        this.origineFichier = origineFichier;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public String getTypeFichier() {
        return typeFichier;
    }

    public String getOrigineFichier() {
        return origineFichier;
    }

    public boolean isSignatureVerifiee() {
        return signatureVerifiee;
    }

    public void setSignatureVerifiee(boolean signatureVerifiee) {
        this.signatureVerifiee = signatureVerifiee;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("AbstractFichier");
        sb.append("{ typeEnveloppe=").append(typeEnveloppe);
        sb.append(", numeroLot=").append(numeroLot);
        sb.append(", index=").append(index);
        sb.append(", identifiant='").append(identifiant);
        sb.append(", typeFichier='").append(typeFichier);
        sb.append(", origineFichier='").append(origineFichier);
        sb.append(", signatureVerifiee='").append(signatureVerifiee);
        sb.append(" }");
        return sb.toString();
    }
}
