package fr.atexo.signature.applet.mpe;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.iharder.Base64;

import org.apache.commons.io.FileUtils;

import fr.atexo.signature.applet.AbstractCertificatApplet;
import fr.atexo.signature.commun.exception.ParametrageException;
import fr.atexo.signature.commun.exception.execution.CertificatDechiffrementIntrouvableException;
import fr.atexo.signature.commun.exception.execution.DechiffrementExecutionException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.signature.SignatureXades;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbReponsesAnnonceUtil;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.commun.util.io.XMLUtil;
import fr.atexo.signature.gui.barreprogression.BarreDeProgression;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import fr.atexo.signature.util.AffichageUtil;
import fr.atexo.signature.util.FileNameCleaner;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.xml.annonce.ReponsesAnnonce;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;

/**
 * Classe effectuant le déchiffrement d'un fichier.
 */
public class MpeDechiffrementApplet extends AbstractCertificatApplet {

	private static final String NOM_LOGGER = "MpeDechiffrementApplet";

	static {
		LoggerFactory.setNomLogger(NOM_LOGGER);
	}

	private enum TypeDechiffrement {
		EnLigne,
		Mixte;
	}

	private enum TypePhase {
		Telecharger,
		DechiffrerEtEnvoyer;
	}

	private DechiffrementProcessor dechiffrementProcessor;

	private TypeDechiffrement typeDechiffrement;

	private String trigrammeOrganisme;

	private String urlServeurTelechargement;

	private String urlServeurEnvoi;

	private Map<Integer, List<JaxbReponsesAnnonceUtil.FichierBloc>> map;

	private boolean traitementEnCours;

	private String reponseAnnonceXML;

	private Set<Integer> idEnveloppes = new HashSet<Integer>();

	private TypePhase typePhase;

	private String cheminRepertoireTelechargement;

	private BarreDeProgression barreDeProgression;

	@Override
	public void init() {
		super.init();

		// appel de la méthode javascript de fin d'initialisation de l'applet
		appellerMethodeJavascript(methodeJavascriptFinAttente);

		LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
	}

	@Override
	protected void initialiserGui() throws Throwable {

		LogManager.getInstance().afficherMessageInfo("Vérification de l'ensemble des paramétres", this.getClass());

		this.reponseAnnonceXML = new String(Base64.decode(this.reponseAnnonceXML.getBytes()));
		ReponsesAnnonce reponsesAnnonce = JaxbReponsesAnnonceUtil.getReponsesAnnonce(this.reponseAnnonceXML, FileUtil.ENCODING_ISO_8859_1);

		LogManager.getInstance().afficherMessageInfo("Extraction des blocs à traiter en limitant aux enveloppes ayant comme identifiants : " + Arrays.toString(idEnveloppes.toArray(new Integer[idEnveloppes.size()])), this.getClass());
		// on restraint le traitement par rapport à un identifiant d'enveloppe
		map = JaxbReponsesAnnonceUtil.extraireFichierBlocChiffrements(reponsesAnnonce, idEnveloppes.isEmpty() ? null : idEnveloppes);

		// on vérifie que la map contient bien des blocs
		if (map == null || map.isEmpty()) {
			throw new ParametrageException("Aucun bloc à déchiffrer n'a été détecté depuis la liste se trouvant dans le fichier reponse annonce xml");
		}

		// on vérifie si on va devoir télécharger des blocs
		boolean modeTelecharger = typeDechiffrement == TypeDechiffrement.EnLigne || (typePhase == TypePhase.Telecharger);
		if (modeTelecharger && urlServeurTelechargement == null) {
			throw new ParametrageException("Aucune url de téléchargement pour récupérer les bloc n'est configurée");
		}

		// on vérifie que le répertoire de stockage des fichier est bien présent
		// et existe
		boolean modeRepertoireNecessaire = typeDechiffrement != TypeDechiffrement.EnLigne;
		if (modeRepertoireNecessaire && cheminRepertoireTelechargement == null && !(new File(cheminRepertoireTelechargement).isDirectory())) {
			throw new ParametrageException("Le répertoire de stockage des fichiers téléchargé n'existe pas : " + cheminRepertoireTelechargement);
		}

		// on vérifie si on va devoir uploader des blocs
		boolean modeUploader = typeDechiffrement == TypeDechiffrement.EnLigne || (typePhase == TypePhase.DechiffrerEtEnvoyer);
		if (modeUploader && urlServeurEnvoi == null) {
			throw new ParametrageException("Aucune url d'envoi pour envoyer les bloc déchiffrés n'est configurée");
		}

		// on vérifie l'intégrité des blocs avant de lancer le déchiffrement et
		// l'envoi
		boolean modeDechiffrerEtEnvoyer = typeDechiffrement == TypeDechiffrement.Mixte && typePhase == TypePhase.DechiffrerEtEnvoyer;
		if (modeDechiffrerEtEnvoyer) {

			for (Integer idEnveloppe : map.keySet()) {

				LogManager.getInstance().afficherMessageInfo("Traitement des blocs de l'enveloppe ayant comme id : " + idEnveloppe, this.getClass());

				for (JaxbReponsesAnnonceUtil.FichierBloc fichierBloc : map.get(idEnveloppe)) {

					for (JaxbReponsesAnnonceUtil.FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {
						String id = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();
						File repertoire = new File(cheminRepertoireTelechargement);
						File fichier = new File(repertoire, id);
						if (!fichier.exists()) {
							throw new ParametrageException("Le fichier " + id + " ne se trouve pas dans le répertoire " + cheminRepertoireTelechargement);
						}
						if (fichier.length() != bloc.getTailleChiffre()) {
							throw new ParametrageException("Le fichier " + id + " n'est pas de la bonne taille. Taille actuelle [" + fichier.length() + "] => Taille attendue : [" + bloc.getTailleChiffre() + "]");
						}
					}
				}
			}
		} else if (cheminRepertoireTelechargement != null) {
			File repertoire = new File(cheminRepertoireTelechargement);
			File fichierReponseAnnonceXML = new File(repertoire, ReponseAnnonceConstantes.NOM_FICHIER_REPONSES_ANNONCE);
			LogManager.getInstance().afficherMessageInfo("Ecriture locale du fichier reponse annonce xml : " + fichierReponseAnnonceXML.getAbsolutePath(), this.getClass());
			FileUtils.writeStringToFile(fichierReponseAnnonceXML, this.reponseAnnonceXML, FileUtil.ENCODING_ISO_8859_1);
		}

		// on sélectionne le bon provider
		if (provider == TypeProvider.PKCS12 && typePhase != TypePhase.Telecharger) {
			selectionnerCertificat();
		} else {
			KeyPair pair = null;
			onSelection(pair);
		}
	}

	@Override
	protected void onSelection(KeyPair keyPair) throws ExecutionException {

		dechiffrementProcessor = new StandardChiffrementProcessor(provider, keyPair);

		try {
			traitementEnCours = true;
			int indexFichier = 1;

			int nombreTotalFichiers = 0;
			for (List<JaxbReponsesAnnonceUtil.FichierBloc> fichiers : map.values()) {
				nombreTotalFichiers = nombreTotalFichiers + fichiers.size();
			}

			barreDeProgression = new BarreDeProgression("Barre de progression", nombreTotalFichiers);

			for (Integer idEnveloppe : map.keySet()) {
				LogManager.getInstance().afficherMessageInfo("Traitement de l'enveloppe ayant comme identifiant : " + idEnveloppe, this.getClass());

				for (JaxbReponsesAnnonceUtil.FichierBloc fichierBloc : map.get(idEnveloppe)) {

					LogManager.getInstance().afficherMessageInfo("Traitement du fichier  " + fichierBloc.getNomFichier() + " composé de " + fichierBloc.getBlocs().size() + " blocs à déchiffré", this.getClass());

					// mise à jour de la barre de progression (partie haute) de
					// l'avancement du traitement de l'ensemble des fichiers
					String messageTraitementFichier = getMessageBarreProgressionFichier(fichierBloc, indexFichier, nombreTotalFichiers);
					barreDeProgression.setMessageProgressionTraitementFichier(messageTraitementFichier);

					// cas mode en ligne ou cas mode mixte et hors ligne phase
					// téléchargement
					boolean modeTelecharger = typeDechiffrement == TypeDechiffrement.EnLigne || (typePhase == TypePhase.Telecharger);

					int nombreBlocs = fichierBloc.getBlocs().size();
					if (typeDechiffrement == TypeDechiffrement.EnLigne) {
						barreDeProgression.initialiserFichier(true, true, true, false, false, nombreBlocs);
					} else {
						if (typePhase == TypePhase.Telecharger) {
							barreDeProgression.initialiserFichier(false, true, false, false, false, nombreBlocs);
						} else {
							barreDeProgression.initialiserFichier(true, false, true, false, false, nombreBlocs);
						}
					}

					// cas mode mixte et phase déchiffrement
					boolean modeDechiffrerEtEnvoyer = typeDechiffrement == TypeDechiffrement.Mixte && typePhase == TypePhase.DechiffrerEtEnvoyer;

					int indexBloc = 1;
					for (JaxbReponsesAnnonceUtil.FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {
						String idBloc = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();
						String messageAvancementTraitementBloc = "Bloc " + indexBloc + "/" + nombreBlocs + " - ";

						// phase téléchargement
						if (modeTelecharger) {

							LogManager.getInstance().afficherMessageInfo("Téléchargement du bloc ayant comme id : " + idBloc + ". Ce bloc est-il chiffré : " + bloc.isChiffre(), this.getClass());
							barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("TELECHARGEMENT_EN_COURS"));
							String urlTelechargementComplet = TransfertUtil.contruireUrlTelechargement(urlServeurTelechargement, trigrammeOrganisme, idBloc);
							LogManager.getInstance().afficherMessageInfo("Url de téléchargement : " + urlTelechargementComplet, this.getClass());

							byte[] contenuBlocTelechargementEnBase64 = TransfertUtil.telecharger(urlTelechargementComplet);
							byte[] contenuBloc = Base64.decode(contenuBlocTelechargementEnBase64);
							String contenuBlocEnveloppeXML = new String(contenuBloc);
							barreDeProgression.incrementerProgressionGlobale();

							// cas ouverture en ligne (téléchargement +
							// déchiffrement + envoi)
							if (typeDechiffrement == TypeDechiffrement.EnLigne) {
								if (bloc.isChiffre()) {
									LogManager.getInstance().afficherMessageInfo("Déchiffrement du bloc ayant comme id : " + idBloc, this.getClass());
									barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("DECHIFFREMENT_EN_COURS"));
									contenuBloc = dechiffrementProcessor.dechiffrer(contenuBlocEnveloppeXML);
									barreDeProgression.incrementerProgressionGlobale();
								}

								barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("TRANSFERT_EN_COURS"));
								LogManager.getInstance().afficherMessageInfo("Envoi (" + urlServeurEnvoi + ") du bloc ayant comme id : " + idBloc + ". Ce bloc est-il chiffré : " + bloc.isChiffre(), this.getClass());
								String retourEnvoi = TransfertUtil.envoyerBloc(urlServeurEnvoi, trigrammeOrganisme, idBloc, Base64.encodeBytes(contenuBloc));
								LogManager.getInstance().afficherMessageInfo("Resultat de l'envoi : " + retourEnvoi, this.getClass());
								barreDeProgression.incrementerProgressionGlobale();

							}
							// cas ouverture mixte (téléchargement uniquement
							// dans un répertoire)
							else if (typePhase == TypePhase.Telecharger) {
								File repertoire = new File(cheminRepertoireTelechargement);

								File fichierPart = new File(repertoire, idBloc);
								FileUtils.writeByteArrayToFile(fichierPart, contenuBlocTelechargementEnBase64);
								LogManager.getInstance().afficherMessageInfo("Ecriture du bloc en base 64 téléchargé : " + fichierPart.getAbsolutePath(), this.getClass());
							}
						}
						// phase déchiffrement où l'on écrit localement le bloc
						// déchiffré (cas ouverture mixte)
						else if (modeDechiffrerEtEnvoyer) {
							// reconstituation dans des sous-repertoires
							File repertoire = new File(cheminRepertoireTelechargement);
							File fichierPart = new File(repertoire, idBloc);
							byte[] contenuBloc = Base64.decodeFromFile(fichierPart.getAbsolutePath());
							if (bloc.isChiffre()) {
								LogManager.getInstance().afficherMessageInfo("Déchiffrement du bloc ayant comme id : " + idBloc, this.getClass());
								barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("DECHIFFREMENT_EN_COURS"));
								contenuBloc = dechiffrementProcessor.dechiffrer(new String(contenuBloc));
								barreDeProgression.incrementerProgressionGlobale();
							}
							FileUtils.writeByteArrayToFile(fichierPart, contenuBloc);
						}

						indexBloc++;
					}

					indexBloc = 1;
					// on transfert les bloc précédement déchiffré
					if (modeDechiffrerEtEnvoyer) {

						File repertoireSource = new File(cheminRepertoireTelechargement);
						File repertoireDestination = new File(cheminRepertoireTelechargement);

						String nomRepertoireOperateurEconomique = fichierBloc.getNomOperateurEconomique();
						if (nomRepertoireOperateurEconomique != null) {
							nomRepertoireOperateurEconomique = FileNameCleaner.cleanFileName(nomRepertoireOperateurEconomique);
							File repertoireOperateurEconomique = new File(repertoireDestination, nomRepertoireOperateurEconomique);
							repertoireDestination = new File(repertoireOperateurEconomique, fichierBloc.getNomEnveloppe());
						} else {
							repertoireDestination = new File(repertoireDestination, fichierBloc.getNomEnveloppe());
						}

						for (JaxbReponsesAnnonceUtil.FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {
							String messageAvancementTraitementBloc = "Bloc " + indexBloc + "/" + nombreBlocs + " - ";

							File fichier = new File(repertoireDestination, fichierBloc.getNomFichier());
							String idBloc = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();

							File fichierPart = new File(repertoireSource, idBloc);
							byte[] contenuBloc = FileUtils.readFileToByteArray(fichierPart);
							LogManager.getInstance().afficherMessageInfo("Envoi (" + urlServeurEnvoi + ") du contenu du bloc déchiffré : " + fichierPart.getAbsolutePath(), this.getClass());
							barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("TRANSFERT_EN_COURS"));
							String retourEnvoi = TransfertUtil.envoyerBloc(urlServeurEnvoi, trigrammeOrganisme, idBloc, Base64.encodeBytes(contenuBloc));
							LogManager.getInstance().afficherMessageInfo("Resultat de l'envoi : " + retourEnvoi, this.getClass());
							if (!repertoireDestination.exists()) {
								repertoireDestination.mkdirs();
							}
							LogManager.getInstance().afficherMessageInfo("Reconstitution locale du fichier : " + fichier.getAbsolutePath(), this.getClass());
							FileUtils.writeByteArrayToFile(fichier, contenuBloc, true);
							fichierPart.delete();
							barreDeProgression.incrementerProgressionGlobale();
							indexBloc++;
						}

						// création du fichier de signature XAdES
						String signatureXades = fichierBloc.getSignatureXades();
						if (signatureXades != null) {
							signatureXades = new String(Base64.decode(signatureXades));
							String dateSignature = SignatureXades.extraireDate(SignatureXades.getSigningTime(signatureXades));
							String nomFichierSignature = XMLUtil.contruireCheminFichierSignatureXML(fichierBloc.getNomFichier(), dateSignature, 1);
							File fichierSignature = new File(repertoireDestination, nomFichierSignature);
							if (!repertoireDestination.exists()) {
								repertoireDestination.mkdirs();
							}
							LogManager.getInstance().afficherMessageInfo("Ecriture locale du fichier de signature : " + fichierSignature.getAbsolutePath(), this.getClass());
							FileUtils.writeByteArrayToFile(fichierSignature, signatureXades.getBytes());
						}
					}

					indexFichier++;
					barreDeProgression.reinitialiserProgressionGlobale();
				}

				if (typePhase != TypePhase.Telecharger) {
					// on flague pour dire que le traitement de l'ensemble des
					// bloc des fichiers constituant une enveloppe (uniquement
					// lors de l'upload)
					TransfertUtil.TypeTransfert typeTransfert = typeDechiffrement == TypeDechiffrement.EnLigne ? TransfertUtil.TypeTransfert.OuvertureEnLigne : TransfertUtil.TypeTransfert.OuvertureMixte;
					LogManager.getInstance().afficherMessageInfo("Envoi (" + urlServeurEnvoi + ") de l'idEnveloppe pour conclure la fin de l'envoi : " + idEnveloppe, this.getClass());
					String retourEnvoi = TransfertUtil.envoyerInformationsOuvertureEnveloppe(urlServeurEnvoi, trigrammeOrganisme, String.valueOf(idEnveloppe), typeTransfert);
					LogManager.getInstance().afficherMessageInfo("Resultat de l'envoi : " + retourEnvoi, this.getClass());
				}
			}

			map.clear();
			appellerMethodeJavascript(methodeJavascriptRenvoiResultat, Boolean.TRUE.toString());
		} catch (CertificatDechiffrementIntrouvableException e) {
			throw e;
		} catch (ExecutionException e) {
			throw e;
		} catch (Exception e) {
			appellerMethodeJavascript(methodeJavascriptRenvoiResultat, Boolean.FALSE.toString());
			throw new DechiffrementExecutionException("Erreur lors du déchiffrement", e);
		} finally {
			traitementEnCours = false;
			typeDechiffrement = null;
			typePhase = null;
			map.clear();
			reponseAnnonceXML = null;
			barreDeProgression.dispose();
			appellerMethodeJavascript(methodeJavascriptFinAttente);
		}
	}

	private String getMessageBarreProgressionFichier(JaxbReponsesAnnonceUtil.FichierBloc fichierBloc, int numeroFichierEnCours, int nombreTotalFichiers) {
		return "Fichier " + (numeroFichierEnCours) + "/" + nombreTotalFichiers + " - " + AffichageUtil.getMessageTypeEnveloppeEtNumeroLot(fichierBloc) + " : " + fichierBloc.getNomFichier() + " (" + AffichageUtil.getMessageTaille(fichierBloc.getTaille()) + ")";
	}

	/**
	 * Permet d'initialiser l'applet de déchiffrement en mode en ligne.
	 * 
	 * @param reponseAnnonceXMLEnBase64
	 *            la reponse annonce xml encodé en base 64
	 * @param trigrammeOrganisme
	 *            le trigramme de l'organisme
	 * @param urlServeurTelechargement
	 *            l'url du serveur pour télécharger les blocs à déchiffrer
	 * @param urlServeurEnvoi
	 *            l'url du serveur pour envoyer les blocs déchiffrés.
	 */

	public void initialiserEnLigne(String reponseAnnonceXMLEnBase64, String trigrammeOrganisme, String urlServeurTelechargement, String urlServeurEnvoi) {
		initialiser(TypeDechiffrement.EnLigne, null, reponseAnnonceXMLEnBase64, trigrammeOrganisme, urlServeurTelechargement, urlServeurEnvoi, null);
	}

	/**
	 * Permet d'initialiser l'applet de déchiffrement en mode mixte (phase de
	 * téléchargement)
	 * 
	 * @param reponseAnnonceXMLEnBase64
	 *            la reponse annonce xml encodé en base 64
	 * @param trigrammeOrganisme
	 *            le trigramme de l'organisme
	 * @param urlServeurTelechargement
	 *            l'url du serveur pour télécharger les blocs à déchiffrer
	 * @param cheminRepertoireTelechargement
	 *            le chemin vers lequel télécharger les blocs
	 */
	public void initialiserMixtePhaseTelecharger(String reponseAnnonceXMLEnBase64, String trigrammeOrganisme, String urlServeurTelechargement, String cheminRepertoireTelechargement) {
		initialiser(TypeDechiffrement.Mixte, TypePhase.Telecharger, reponseAnnonceXMLEnBase64, trigrammeOrganisme, urlServeurTelechargement, null, cheminRepertoireTelechargement);
	}

	/**
	 * Permet d'initialiser l'applet de déchiffrement en mode mixte (phase de
	 * déchiffrement et d'envoi des blocs déchiffrés)
	 * 
	 * @param reponseAnnonceXMLEnBase64
	 *            la reponse annonce xml encodé en base 64
	 * @param trigrammeOrganisme
	 *            le trigramme de l'organisme
	 * @param urlServeurEnvoi
	 *            l'url du serveur pour envoyer les blocs déchiffrés.
	 * @param cheminRepertoireTelechargement
	 *            le chemin vers lequel les blocs ont été téléchargés
	 */
	public void initialiserMixtePhaseDechiffrerEtEnvoyer(String reponseAnnonceXMLEnBase64, String trigrammeOrganisme, String urlServeurEnvoi, String cheminRepertoireTelechargement) {
		initialiser(TypeDechiffrement.Mixte, TypePhase.DechiffrerEtEnvoyer, reponseAnnonceXMLEnBase64, trigrammeOrganisme, null, urlServeurEnvoi, cheminRepertoireTelechargement);
	}

	/**
	 * Permet d'initialiser l'applet de déchiffrement en mode en ligne.
	 * 
	 * @param typeDechiffrement
	 *            le type de dechiffrement attendu
	 * @param reponseAnnonceXMLEnBase64
	 *            la reponse annonce xml encodé en base 64
	 * @param trigrammeOrganisme
	 *            le trigramme de l'organisme
	 * @param urlServeurTelechargement
	 *            l'url du serveur pour télécharger les blocs à déchiffrer
	 * @param urlServeurEnvoi
	 *            l'url du serveur pour envoyer les blocs déchiffrés.(mode en
	 *            ligne et mixte phase envoyer)
	 * @param cheminRepertoireTelechargement
	 *            le chemin vers lequel télécharger les blocs (mode mixte et
	 *            hors ligne)
	 */
	public void initialiser(TypeDechiffrement typeDechiffrement, TypePhase phase, String reponseAnnonceXMLEnBase64, String trigrammeOrganisme, String urlServeurTelechargement, String urlServeurEnvoi, String cheminRepertoireTelechargement) {
		this.typeDechiffrement = typeDechiffrement;
		this.typePhase = phase;
		this.reponseAnnonceXML = reponseAnnonceXMLEnBase64;
		this.trigrammeOrganisme = trigrammeOrganisme;
		this.urlServeurTelechargement = urlServeurTelechargement;
		this.urlServeurEnvoi = urlServeurEnvoi;
		this.cheminRepertoireTelechargement = cheminRepertoireTelechargement;
		this.idEnveloppes = new HashSet<Integer>();

		// recapitulatif des paramétres
		LogManager.getInstance().afficherMessageInfo("Type de déchiffrement : " + typeDechiffrement, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Type de phase : " + phase, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Trigramme organisme : " + trigrammeOrganisme, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Url de téléchargement des blocs chiffrés : " + urlServeurTelechargement, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Url d'envoi des blocs déchiffrés : " + urlServeurEnvoi, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Chemin repertoire de téléchargement : " + cheminRepertoireTelechargement, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Contenu de la reponse annonce XML (Base 64) : " + reponseAnnonceXMLEnBase64, this.getClass());
	}

	/**
	 * Ajoute un identifiant d'enveloppe à déchiffrer.
	 * 
	 * @param idEnveloppe
	 *            l'identifiant de l'enveloppe à déchiffrer
	 */
	public void ajouterIdEnveloppe(Integer idEnveloppe) {
		if (idEnveloppe != null) {
			LogManager.getInstance().afficherMessageInfo("Identifiant de l'enveloppe : " + idEnveloppe, this.getClass());
			idEnveloppes.add(idEnveloppe);
		}

	}

	public boolean isTraitementEnCours() {
		return traitementEnCours;
	}
}
