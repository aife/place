package fr.atexo.signature.applet;

import fr.atexo.signature.commun.exception.ParametrageException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import netscape.javascript.JSObject;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.applet.Applet;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.Policy;
import java.util.Locale;

/**
 * Classe abstraite permettant d'initialiser une applet devrant communiquer
 * avec les magasins de certificats du client.
 */
public abstract class AbstractApplet extends Applet {

    protected static final String PARAM_LOCALE = "locale";
    protected static final String PARAM_ECRASER_JAVA_POLICY = "ecraserJavaPolicy";
    protected static final String PARAM_METHODE_JAVASCRIPT_DEBUT_ATTENTE = "methodeJavascriptDebutAttente";
    protected static final String PARAM_METHODE_JAVASCRIPT_FIN_ATTENTE = "methodeJavascriptFinAttente";
    private static final String PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT = "methodeJavascriptRenvoiResultat";
    private static final String PARAM_METHODE_JAVASCRIPT_RECUPERER_LOG = "methodeJavascriptRecuperationLog";

    protected static String NOM_FICHIER_JAVA_POLICY = ".java.policy";

    private boolean ecraserJavaPolicy = false;

    /**
     * Le type d'os;
     */
    protected TypeOs os;

    /**
     * La locale devant être utilisée pour localiser les textes à afficher dans
     * l'interface graphique en swing de l'application.
     */
    protected Locale locale = Locale.getDefault();

    /**
     * Liste des méthodes Javascript à appeler.
     */
    protected String methodeJavascriptRenvoiResultat;

    protected String methodeJavascriptDebutAttente;

    protected String methodeJavascriptFinAttente;

    protected String methodeJavascriptRecuperationLog;

    /**
     * Permet de lancer / initialiser l'applet via un thread
     */
    private boolean initialisee;

    protected Thread listenerInitialisation;

    /**
     * Initialise le listener initialisant la partie gui depuis la méthode init afin d'éviter les soucis de sécurié
     * si on appele via une méthode Js la méthode d'initialisation du gui.
     */
    protected void initialiserListenerInitialisationGui() {
        final Class clazz = this.getClass();
        listenerInitialisation = new Thread() {
            public void run() {
                while (true) {
                    try {
                        if (initialisee) {
                            LogManager.getInstance().afficherMessageInfo("Initialisation de l'interface graphique utilisateur...", clazz);
                            initialiserGui();
                            appellerMethodeJavascript(methodeJavascriptFinAttente);
                            initialisee = false;
                        }
                        sleep(500);
                    } catch (Throwable e) {
                        if (!(e instanceof InterruptedException)) {
                            LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                            boolean parametrage = e instanceof ParametrageException;
                            boolean execution = e instanceof ExecutionException;
                            if (parametrage) {
                                JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ALERTE"), JOptionPane.WARNING_MESSAGE);
                            } else if (execution) {
                                JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                            }

                            lireLogs(clazz);
                        }
                        initialisee = false;
                    }
                }
            }
        };
        listenerInitialisation.start();
    }

    protected void lireLogs(final Class clazz) {
        if (methodeJavascriptRecuperationLog != null) {
            try {
                File fichierLog = LoggerFactory.getFichier();
                if (fichierLog != null && fichierLog.exists()) {
                    String contenuLog = FileUtils.readFileToString(fichierLog, FileUtil.ENCODING_UTF_8);
                    appellerMethodeJs(methodeJavascriptRecuperationLog, false, new Object[]{contenuLog});
                } else {
                    LogManager.getInstance().afficherMessageWarning("Aucun fichier de log n'a été trouvé pour envoyer l'erreur", clazz);
                }
            } catch (IOException e1) {
                LogManager.getInstance().afficherMessageErreur(e1.getMessage(), e1, clazz);
            }
        } else {
            LogManager.getInstance().afficherMessageWarning("Aucun ne sera renvoyé au serveur car la méthode methodeJavascriptRecuperationLog n'a pas été définie", clazz);
        }
    }

    @Override
    public void destroy() {
        if (listenerInitialisation.isAlive()) {
            listenerInitialisation.interrupt();
        }
    }

    /**
     * Méthode à executer lors que l'on demande explicitement que l'interface graphique utilisateur
     * soit lancé par l'intermédiaire du thread dormant qui a été initialisé au niveau de la méthode init.
     */
    protected abstract void initialiserGui() throws Throwable;

    /**
     * Permet de demander l'initialisation de l'interface graphique utilisateur de manière détourné et explicitement
     * en dehors de l'applet.
     */
    public void executer() {
        LogManager.getInstance().afficherMessageInfo("Execution de la demande d'initialisation", this.getClass());
        appellerMethodeJavascript(methodeJavascriptDebutAttente);
        initialisee = true;
        LogManager.getInstance().afficherMessageInfo("initialiserInterfaceGraphique : " + initialisee, this.getClass());
    }


    public String getParameterAvecMessage(String name) {
        String parameter = super.getParameter(name);
        LogManager.getInstance().afficherMessageInfo("Le paramètre " + name + " a comme valeur : " + parameter, this.getClass());
        return parameter;
    }

    @Override
    public void init() {
        LogManager.getInstance().afficherMessageInfo("--------------------- DEBUT INIT --------------------------", this.getClass());
        LogManager.getInstance().afficherMessageInfo("Encoding par defaut : " + Charset.defaultCharset(), this.getClass());
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // os
        os = Util.determinerOs();

        // paramètre locale
        String locale = getParameterAvecMessage(PARAM_LOCALE);
        setLocale(locale);

        // paramètre methodeJavascriptDebutAttente
        setMethodeJavascriptDebutAttente(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_DEBUT_ATTENTE));
        // paramètre methodeJavascriptFinAttente
        setMethodeJavascriptFinAttente(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_ATTENTE));
        // paramètre methodeJavascriptRenvoiResultat
        setMethodeJavascriptRenvoiResultat(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT));
        // paramètre methodeJavascriptRecuperationLog
        setMethodeJavascriptRecuperationLog(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_RECUPERER_LOG));

        // paramétre ecraser java policy
        setEcraserJavaPolicy(getParameterAvecMessage(PARAM_ECRASER_JAVA_POLICY));
        if (ecraserJavaPolicy) {
            ecraserJavaPolicy();
        } else {
            LogManager.getInstance().afficherMessageWarning("Le fichier " + NOM_FICHIER_JAVA_POLICY + " n'a pas été écrasé", this.getClass());
        }
        Policy.getPolicy().refresh();

        initialiserListenerInitialisationGui();
    }

    /**
     * Permet d'écraser le fichier java.policy se trouvant dans le répertoire home de l'utilisateur
     * si celui-ci existe.
     */
    protected void ecraserJavaPolicy() {
        LogManager.getInstance().afficherMessageInfo("Ecrasement du contenu du fichier " + NOM_FICHIER_JAVA_POLICY + " se trouvant dans le répertoire home de l'utilisateur", this.getClass());

        try {
            String userHome = System.getProperties().getProperty("user.home");
            LogManager.getInstance().afficherMessageInfo("Répertoire utilisateur : " + userHome, this.getClass());

            File repertoireHome = new File(userHome);
            if (repertoireHome != null && repertoireHome.exists() && repertoireHome.isDirectory()) {

                String cheminJavaPolicy = repertoireHome.getAbsolutePath() + "/" + NOM_FICHIER_JAVA_POLICY;
                LogManager.getInstance().afficherMessageInfo("Fichier Java Policy : " + cheminJavaPolicy, this.getClass());

                File fichierJavaPolicy = new File(cheminJavaPolicy);
                if (fichierJavaPolicy != null && fichierJavaPolicy.exists() && fichierJavaPolicy.isFile()) {
                    FileOutputStream fichierJavaPolicyOutStream = new FileOutputStream(fichierJavaPolicy);
                    fichierJavaPolicyOutStream.write("".getBytes());
                    fichierJavaPolicyOutStream.close();
                    LogManager.getInstance().afficherMessageInfo("Le fichier " + NOM_FICHIER_JAVA_POLICY + " a été remplacé", this.getClass());
                } else {
                    LogManager.getInstance().afficherMessageWarning("Aucune fichier " + NOM_FICHIER_JAVA_POLICY + "n'a été trouvé dans le répertoire " + userHome, this.getClass());
                }
            }

            FileOutputStream outPolicy = new FileOutputStream(userHome + "/.java.policy");
            outPolicy.write("".getBytes());
            outPolicy.close();
            LogManager.getInstance().afficherMessageInfo("Ecrasement du contenu du fichier .java.policy", this.getClass());

        } catch (Exception e) {
            LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
        }
    }

    protected void appellerMethodeJavascript(String methodeJavascript, String... parametres) {
        appellerMethodeJavascript(methodeJavascript, true, parametres);
    }

    /**
     * Permet d'appeler une méthode Javascript avec des paramétres.
     *
     * @param methodeJavascript la méthode Javascript à appeler
     * @param parametres        la liste des paramétres de la méthode.
     */
    protected void appellerMethodeJavascript(String methodeJavascript, boolean verbeux, String... parametres) {
        if (methodeJavascript != null) {
            if (parametres != null && parametres.length != 0) {
                appellerMethodeJs(methodeJavascript, verbeux, parametres);
            } else {
                appellerMethodeJs(methodeJavascript, verbeux, new Object[]{});
            }
        }

    }

    /**
     * Permet d'appeler la méthode Javascript native alert afin d'afficher un message.
     *
     * @param message le message à afficher lors de l'appel de la méthode alert javascript.
     */
    protected void appellerMethodeJavascriptAlert(String message) {
        appellerMethodeJavascript("alert", false, message);
    }

    /**
     * Permet d'appeler une méthode Javascript avec des paramétres.
     *
     * @param methode    la méthode Javascript à appeler
     * @param parametres la liste des paramétres de la méthode.
     */
    protected void appellerMethodeJs(String methode, boolean verbeux, Object[] parametres) {
        if (methode != null && methode.length() > 0) {

            if (verbeux) {
                LogManager.getInstance().afficherMessageInfo("--------", this.getClass());
                LogManager.getInstance().afficherMessageInfo("Appel de la méthode Js : " + methode, this.getClass());
                LogManager.getInstance().afficherMessageInfo("Liste des paramètres de la méthode Js :", this.getClass());
                for (int i = 0; i < parametres.length; i++) {
                    Object parametre = parametres[i];
                    if (parametre instanceof String[][]) {
                        LogManager.getInstance().afficherMessageInfo("Paramètre " + i + "  : " + affichierContenuTableauDoubleDimension((String[][]) parametre), this.getClass());
                    } else {
                        LogManager.getInstance().afficherMessageInfo("Paramètre " + i + "  : " + parametre, this.getClass());
                    }
                }
                LogManager.getInstance().afficherMessageInfo("--------", this.getClass());
            }

            try {
                JSObject window = JSObject.getWindow(this);
                window.call(methode, parametres);
            } catch (Throwable t) {
                try {
                    getAppletContext().showDocument(new URL("javascript:" + methode + ";"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String affichierContenuTableauDoubleDimension(Object[][] tableau) {
        StringBuffer results = new StringBuffer();
        String separator = ",";

        for (int i = 0; i < tableau.length; ++i) {
            results.append('[');
            for (int j = 0; j < tableau[i].length; ++j)
                if (j > 0)
                    results.append(tableau[i][j]);
                else
                    results.append(tableau[i][j]).append(separator);
            results.append(']');
        }

        return results.toString();
    }

    /**
     * Transforme une valeur de type String en valeur de type boolean
     *
     * @param valeur <code>true</code> pour vrai, sinon <code>false</code>
     * @return la valeur booléenne correspondante
     */
    protected boolean getValeurBooleenne(String valeur) {
        return valeur != null && "true".equals(valeur) ? true : false;
    }

    public void setEcraserJavaPolicy(String ecraserJavaPolicy) {
        this.ecraserJavaPolicy = getValeurBooleenne(ecraserJavaPolicy);
    }

    public void setLocale(String locale) {
        this.locale = I18nUtil.toLocale(locale);
        if (this.locale != null) {
            Locale.setDefault(this.locale);
        }
    }

    public void setMethodeJavascriptRenvoiResultat(String methodeJavascriptRenvoiResultat) {
        this.methodeJavascriptRenvoiResultat = methodeJavascriptRenvoiResultat;
    }

    public void setMethodeJavascriptDebutAttente(String methodeJavascriptDebutAttente) {
        this.methodeJavascriptDebutAttente = methodeJavascriptDebutAttente;
    }

    public void setMethodeJavascriptFinAttente(String methodeJavascriptFinAttente) {
        this.methodeJavascriptFinAttente = methodeJavascriptFinAttente;
    }

    public void setMethodeJavascriptRecuperationLog(String methodeJavascriptRecuperationLog) {
        this.methodeJavascriptRecuperationLog = methodeJavascriptRecuperationLog;
    }

    public boolean isInitialisee() {
        return initialisee;
    }

    public boolean verifierCommunicationJs() {
        LogManager.getInstance().afficherMessageInfo("Vérification de la communication depuis une méthode javascript", this.getClass());
        return true;
    }
}
