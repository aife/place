package fr.atexo.signature.applet;

import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.gui.browser.SelectionRepertoire;
import fr.atexo.signature.gui.browser.ui.ExtensionFileFilter;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Applet permettant de sélectionner un fichier.
 */
public class SelectionFichierApplet extends AbstractApplet {

    private static final String NOM_LOGGER = "SelectionFichierApplet";

    protected static final String PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT = "methodeJavascriptFinTraitement";

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    private static final long serialVersionUID = 8055764070885780200L;

    private static String cheminRepertoire = null;

    private Set<String> extensions;

    private String identifiant;

    private boolean repertoire;

    private boolean fichierMultiple;

    /**
     * Liste des méthodes Javascript à appeler.
     */
    protected String methodeJavascriptFinTraitement;

    @Override
    public void init() {
        super.init();
        LogManager.getInstance().afficherMessageInfo("Chargement de l'applet", this.getClass());

        // paramètre methodeJavascriptFinTraitement
        setMethodeJavascriptFinTraitement(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT));

        // contrôles
        boolean javaVersionOk = Util.verifierJavaVersion(16);
        if (!javaVersionOk) {
            JOptionPane.showMessageDialog(null, I18nUtil.get("ERROR_JAVA_VERSION"), I18nUtil.get("ERREUR"), JOptionPane.ERROR_MESSAGE);
        }

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
    }

    @Override
    protected void initialiserGui() throws Throwable {
        LogManager.getInstance().afficherMessageInfo("Initialisation du selectionneur de fichiers...", this.getClass());
        JFileChooser fileChooser = repertoire ? new SelectionRepertoire() : new JFileChooser();
        if (cheminRepertoire != null) {
            File repertoireDepart = new File(cheminRepertoire);
            fileChooser = repertoire ? new SelectionRepertoire(repertoireDepart) : new JFileChooser(repertoireDepart);
        }
        fileChooser.setMultiSelectionEnabled(fichierMultiple);

        if (extensions != null && !extensions.isEmpty()) {
            String[] extensionsFiltre = extensions.toArray(new String[extensions.size()]);
            ExtensionFileFilter filter = new ExtensionFileFilter(Arrays.toString(extensionsFiltre), extensionsFiltre);
            fileChooser.setFileFilter(filter);
        }

        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

            if (fichierMultiple) {
                File[] fichiers = fileChooser.getSelectedFiles();
                cheminRepertoire = fichiers != null && fichiers.length > 0 ? fichiers[0].getParent() : null;
                String[] cheminFichiers = new String[fichiers.length];
                for (int i = 0; i < fichiers.length; i++) {
                    File fichier = fichiers[i];
                    if (fichier.exists()) {
                        cheminFichiers[i] = fichier.getAbsolutePath();
                    }
                    appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, cheminFichiers[i]);
                }

            } else {
                cheminRepertoire = fileChooser.getSelectedFile().isFile() ? fileChooser.getSelectedFile().getParent() : fileChooser.getSelectedFile().getAbsolutePath();
                String cheminFichier = fileChooser.getSelectedFile().getAbsolutePath();
                appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, cheminFichier);
            }

            appellerMethodeJavascript(methodeJavascriptFinTraitement);
        }
    }

    /**
     * Permet d'appeler une méthode Javascript avec des paramétres.
     *
     * @param methodeJavascript la méthode Javascript à appeler
     * @param identifiant       l'identifiant associé
     * @param cheminFichiers    la liste des chemins de fichiers
     */
    protected void appellerMethodeJavascript(String methodeJavascript, String identifiant, String cheminFichiers) {
        if (methodeJavascript != null) {
            appellerMethodeJs(methodeJavascript, true, new Object[]{identifiant, cheminFichiers});
        }
    }

    /**
     * Permet d'initialiser l'applet avec l'identifiant html auquel il doit être rattaché et d'ajouter une
     * liste d'extensions afin de filtrer les extensions de fichier utilisable.
     *
     * @param identifiant     l'identifiant du champ html auquel est associé l'action de sélection de fichier.
     * @param fichierMultiple <code>true</code> si l'on veut activer la selection multiple de fichiers, sinon <code>false</code>
     * @param extensions      la liste des extensions sur lequel filtrer la recherche de fichiers.
     */
    public void initialiser(String identifiant, boolean fichierMultiple, String... extensions) {
        this.identifiant = identifiant;
        this.extensions = new HashSet<String>();
        if (extensions != null) {
            for (String extension : extensions) {
                this.extensions.add(extension);
            }
        }
        this.repertoire = false;
        this.fichierMultiple = fichierMultiple;
    }


    /**
     * Permet d'initialiser l'applet avec l'identifiant html auquel il doit être rattaché et d'ajouter une
     * liste d'extensions afin de filtrer les extensions de fichier utilisable.
     *
     * @param identifiant l'identifiant du champ html auquel est associé l'action de sélection de fichier.
     * @param extensions  la liste des extensions sur lequel filtrer la recherche de fichiers.
     */
    public void initialiser(String identifiant, String... extensions) {
        initialiser(identifiant, false, extensions);
    }

    /**
     * Permet d'initialiser l'applet avec l'identifiant html auquel il doit être rattaché afin de sélectionner un repertoire.
     *
     * @param identifiant l'identifiant du champ html auquel est associé l'action de sélection.
     */
    public void initialiserRepertoire(String identifiant) {
        this.identifiant = identifiant;
        this.extensions = null;
        this.repertoire = true;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public Set<String> getExtensions() {
        return extensions;
    }

    public void setMethodeJavascriptFinTraitement(String methodeJavascriptFinTraitement) {
        this.methodeJavascriptFinTraitement = methodeJavascriptFinTraitement;
    }
}
