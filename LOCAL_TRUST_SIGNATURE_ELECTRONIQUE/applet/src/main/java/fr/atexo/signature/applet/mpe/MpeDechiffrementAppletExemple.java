package fr.atexo.signature.applet.mpe;

import net.iharder.Base64;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class MpeDechiffrementAppletExemple extends MpeDechiffrementApplet {

    private static final String UID = "uid_00001";

    private static final String REPERTOIRE_TELECHARGEMENT = "/home/atexo/Bureau/chiffrement/SDT11051";

    private static final String URL_TELECHARGEMENT = "http://localhost:8080/validation/telechargementBlocChiffre";

    private static final String URL_UPLOAD = "http://localhost:8080/validation/envoiBlocDechiffre";

    @Override
    public void init() {
        super.init();
        try {
            initialiserMixtePhaseDechiffrementEtEnvoi();
            executer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initialiserMixtePhaseDechiffrementEtEnvoi() throws IOException {
        File file = new File(REPERTOIRE_TELECHARGEMENT, "ReponsesAnnonce.xml");
        String reponseAnnonceBase64 = Base64.encodeFromFile(file.getAbsolutePath());
        initialiserMixtePhaseDechiffrerEtEnvoyer(reponseAnnonceBase64, UID, URL_UPLOAD, REPERTOIRE_TELECHARGEMENT);
    }
}
