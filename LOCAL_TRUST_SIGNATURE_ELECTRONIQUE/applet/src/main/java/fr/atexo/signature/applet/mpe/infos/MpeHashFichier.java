package fr.atexo.signature.applet.mpe.infos;

import fr.atexo.signature.commun.securite.processor.signature.ConteneurHashFichierSignatureXML;

/**
 *
 */
public class MpeHashFichier extends AbstractFichier implements ConteneurHashFichierSignatureXML {

    private String nomFichier;

    private String hashFichier;

    private String contenuSignatureXml;

    public MpeHashFichier(int typeEnveloppe, int numeroLot, int index, String identifiantHashFichier, String typeFichier, String origineFichier, String hashFichier, String nomFichier) {
        this(typeEnveloppe, numeroLot, index, identifiantHashFichier, typeFichier, origineFichier, hashFichier, nomFichier, null);
    }

    public MpeHashFichier(int typeEnveloppe, int numeroLot, int index, String identifiantHashFichier, String typeFichier, String origineFichier, String hashFichier, String nomFichier, String contenuSignatureXml) {
        super(typeEnveloppe, numeroLot, index, identifiantHashFichier, typeFichier, origineFichier);
        this.nomFichier = nomFichier;
        this.hashFichier = hashFichier;
        this.contenuSignatureXml = contenuSignatureXml;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public String getHashFichier() {
        return hashFichier;
    }

    public void setHashFichier(String hashFichier) {
        this.hashFichier = hashFichier;
    }

    public String getContenuSignatureXml() {
        return contenuSignatureXml;
    }

    public void setContenuSignatureXml(String contenuSignatureXml) {
        this.contenuSignatureXml = contenuSignatureXml;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("MpeHashFichier");
        sb.append("{ typeEnveloppe=").append(typeEnveloppe);
        sb.append(", numeroLot=").append(numeroLot);
        sb.append(", index=").append(index);
        sb.append(", identifiantHashFichier='").append(identifiant);
        sb.append(", typeFichier='").append(typeFichier);
        sb.append(", nomFichier=").append(nomFichier);
        sb.append(", hashFichier=").append(hashFichier);
        sb.append(", contenuSignatureXml=").append(contenuSignatureXml);
        sb.append(", signatureVerifiee='").append(signatureVerifiee);
        sb.append(" }");
        return sb.toString();
    }
}
