package fr.atexo.signature.applet.bean;

import fr.atexo.signature.commun.securite.processor.signature.ConteneurFichierSignatureXML;

import java.io.File;

/**
 * Classe représentant un fichier.
 */
public class SignatureFichier implements ConteneurFichierSignatureXML {

    private File fichier;

    private File fichierSignatureXml;

    public SignatureFichier(File fichier) {
        this(fichier, null);
    }

    public SignatureFichier(File fichier, File fichierSignatureXml) {
        this.fichier = fichier;
        this.fichierSignatureXml = fichierSignatureXml;
    }

    public File getFichier() {
        return fichier;
    }

    public void setFichier(File fichier) {
        this.fichier = fichier;
    }

    public File getFichierSignatureXml() {
        return fichierSignatureXml;
    }

    public void setFichierSignatureXml(File fichierSignatureXml) {
        this.fichierSignatureXml = fichierSignatureXml;
    }
}
