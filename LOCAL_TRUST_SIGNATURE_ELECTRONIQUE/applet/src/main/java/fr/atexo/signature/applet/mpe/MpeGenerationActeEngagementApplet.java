package fr.atexo.signature.applet.mpe;

import java.io.File;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.iharder.Base64;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import fr.atexo.signature.acteengagement.GenerationActeEngagement;
import fr.atexo.signature.applet.AbstractApplet;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.gui.browser.SelectionRepertoire;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;

/**
 * Classe effectuant la génération d'un acte d'engagement.
 */
public class MpeGenerationActeEngagementApplet extends AbstractApplet {

	private static final String NOM_LOGGER = "MpeGenerationActeEngagementApplet";

	private static final String PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT_GENERATION = "methodeJavascriptRenvoiResultatGeneration";

	private static final String PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT_VERIFICATION = "methodeJavascriptRenvoiResultatVerification";

	private static final String PARAM_METHODE_JAVASCRIPT_FIN_ATTENTE_VERIFICATION = "methodeJavascriptFinAttenteVerification";

	private String identifiantFichier;

	private String identifiantFichierXML;

	private String contenuFichierXMLEnBase64;

	private int numeroLot;

	private String urlLogo;

	private boolean generation;

	private boolean verification;

	private Map<File, File> fichiers;

	private String separateur;

	private String methodeJavascriptRenvoiResultatGeneration;

	private String methodeJavascriptRenvoiResultatVerification;

	private String methodeJavascriptFinAttenteVerification;

	static {
		LoggerFactory.setNomLogger(NOM_LOGGER);
	}

	@Override
	public void init() {
		super.init();

		// paramètre methodeJavascriptRenvoiResultatGeneration
		setMethodeJavascriptRenvoiResultatGeneration(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT_GENERATION));

		// paramètre methodeJavascriptRenvoiResultatVerification
		setMethodeJavascriptRenvoiResultatVerification(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_RENVOI_RESULTAT_VERIFICATION));

		// paramètre methodeJavascriptFinAttentetVerification
		setMethodeJavascriptFinAttenteVerification(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_ATTENTE_VERIFICATION));

		fichiers = new HashMap<File, File>();

		// appel de la méthode javascript de fin d'initialisation de l'applet
		appellerMethodeJavascript(methodeJavascriptFinAttente);

		LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
	}

	@Override
	protected void initialiserGui() throws Throwable {

		appellerMethodeJavascript(methodeJavascriptDebutAttente);

		if (generation) {
			genererActeEngagement();
		}

		if (verification) {
			verifierActesEngagements();
		}

		appellerMethodeJavascript(methodeJavascriptFinAttente);
	}

	/**
	 * Génére l'acte d'engagement.
	 * 
	 * @throws Exception
	 */
	private void genererActeEngagement() throws Exception {
		LogManager.getInstance().afficherMessageInfo("Génération d'un acte d'engagement en cours...", this.getClass());

		String repertoireSelectionne = selectionRepertoireDestination();
		if (repertoireSelectionne != null) {
			File repertoire = new File(repertoireSelectionne);
			String nomFichier = "ActeEngagement";
			if (numeroLot != 0) {
				nomFichier = nomFichier + "_Lot_" + numeroLot;
			}
			String nomFichierXML = nomFichier + ".xml";
			nomFichier = nomFichier + ".pdf";

			if (repertoire.exists() && repertoire.isDirectory()) {

				File fichier = new File(repertoire, nomFichier);
				File fichierXML = new File(repertoire, nomFichierXML);

				if (fichier.exists() || fichierXML.exists()) {
					JOptionPane.showMessageDialog(null,
							I18nUtil.get("TEXT_FILE") + " " + nomFichierXML + " ou " + nomFichier + " " + I18nUtil.get("CHOIX_AUTRE_EMPLACEMENT"),
							I18nUtil.get("ALERTE"), JOptionPane.INFORMATION_MESSAGE);
					genererActeEngagement();
				}

				// on écris dans le repertoire le fichier xml
				String contenuFichierXML = new String(Base64.decode(contenuFichierXMLEnBase64), FileUtil.ENCODING_UTF_8);
				FileUtils.writeStringToFile(fichierXML, contenuFichierXML, FileUtil.ENCODING_UTF_8);
				LogManager.getInstance().afficherMessageInfo("Création du fichier XML : [" + fichierXML.getAbsolutePath() + "]", this.getClass());

				// créer un document à partir du contenu du fichier XML
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				InputSource inputSource = new InputSource(new StringReader(contenuFichierXML));
				Document document = documentBuilder.parse(inputSource);

				GenerationActeEngagement generationActeEngagement = new GenerationActeEngagement();

				generationActeEngagement.genererActeEngagement(document, fichier, urlLogo);

				LogManager.getInstance().afficherMessageInfo("Création du fichier d'acte d'engagement pdf : [" + fichier.getAbsolutePath() + "]",
						this.getClass());

				fichiers.put(fichierXML, fichier);
				appellerMethodeJavascript(methodeJavascriptRenvoiResultatGeneration, identifiantFichier, fichier.getAbsolutePath(), identifiantFichierXML,
						fichierXML.getAbsolutePath());

			} else {
				LogManager.getInstance().afficherMessageInfo("Le répertoire n'exite pas : [" + repertoire.getAbsolutePath() + "]", this.getClass());
			}
		} else {
			LogManager.getInstance().afficherMessageInfo("Aucun répertoire sélectionné", this.getClass());
		}
	}

	/**
	 * Vérifie l'ensemble des actes d'engagements générés.
	 */
	private void verifierActesEngagements() {
		LogManager.getInstance().afficherMessageInfo("Vérifications de l'ensemble des actes d'engagements générés en cours...", this.getClass());

		boolean fichiersInexistants = false;

		for (File fichierXML : fichiers.keySet()) {

			File fichierActeEngagementPdf = fichiers.get(fichierXML);
			if (!fichierXML.exists() || !fichierActeEngagementPdf.exists()) {
				fichiersInexistants = true;
				if (!fichierXML.exists()) {
					appellerMethodeJavascript(methodeJavascriptRenvoiResultatVerification, fichierXML.getAbsolutePath());
				}
				if (!fichierActeEngagementPdf.exists()) {
					appellerMethodeJavascript(methodeJavascriptRenvoiResultatVerification, fichierActeEngagementPdf.getAbsolutePath());
				}
			}
		}

		appellerMethodeJavascript(methodeJavascriptFinAttenteVerification, String.valueOf(fichiersInexistants));
	}

	/**
	 * Initialise l'applet de génération d'acte d'engagement.
	 * 
	 * @param identifiantFichier
	 *            l'identifiant du fichier
	 * @param identifiantFichierXML
	 *            l'identifiant du fichier XML
	 * @param contenuFichierXMLEnBase64
	 *            le contenu du fichier XML en base 64
	 * @param numeroLot
	 *            le numero de lot
	 * @param urlLogo
	 *            l'url du logo à intégrer dans l'acte d'engagement qui sera
	 *            généré
	 */
	public void initialiserGeneration(String identifiantFichier, String identifiantFichierXML, String contenuFichierXMLEnBase64, int numeroLot, String urlLogo) {
		this.identifiantFichier = identifiantFichier;
		this.identifiantFichierXML = identifiantFichierXML;
		this.contenuFichierXMLEnBase64 = contenuFichierXMLEnBase64;
		this.numeroLot = numeroLot;
		this.urlLogo = urlLogo;
		initialiser(true);

		// recapitulatif des paramétres
		LogManager.getInstance().afficherMessageInfo("Generation d'un acte d'engagement", this.getClass());
		LogManager.getInstance().afficherMessageInfo("Identifiant fichier : " + identifiantFichier, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Identifiant fichier XML : " + identifiantFichierXML, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Contenu du fichier XML (en base 64) : " + contenuFichierXMLEnBase64, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Numéro de lot : " + numeroLot, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Url du logo : " + urlLogo, this.getClass());
	}

	public void initialiserVerification(String separateur) {
		this.separateur = separateur;
		initialiser(false);

		// recapitulatif des paramétres
		LogManager.getInstance().afficherMessageInfo("Vérification de l'ensemble des actes d'engagements générés", this.getClass());
		LogManager.getInstance().afficherMessageInfo("Separateur : " + separateur, this.getClass());
	}

	private void initialiser(boolean generation) {
		this.generation = generation;
		this.verification = !generation;
	}

	/**
	 * Permet de sélectionner un répertoire et de renvoyer le chemin vers
	 * celui-ci.
	 * 
	 * @return le chemin vers le répertoire sélectionné
	 */
	private String selectionRepertoireDestination() {
		SelectionRepertoire selectionRepertoire = new SelectionRepertoire();
		selectionRepertoire.setDialogTitle(I18nUtil.get("SELECTIONNEZ_LE_REPERTOIRE_DESTINATION"));
		if (selectionRepertoire.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			if (selectionRepertoire.getSelectedFile().getAbsolutePath().length() != 0) {
				return selectionRepertoire.getSelectedFile().getAbsolutePath();
			}
		}
		return null;
	}

	public void setMethodeJavascriptRenvoiResultatGeneration(String methodeJavascriptRenvoiResultatGeneration) {
		this.methodeJavascriptRenvoiResultatGeneration = methodeJavascriptRenvoiResultatGeneration;
	}

	public void setMethodeJavascriptRenvoiResultatVerification(String methodeJavascriptRenvoiResultatVerification) {
		this.methodeJavascriptRenvoiResultatVerification = methodeJavascriptRenvoiResultatVerification;
	}

	public void setMethodeJavascriptFinAttenteVerification(String methodeJavascriptFinAttenteVerification) {
		this.methodeJavascriptFinAttenteVerification = methodeJavascriptFinAttenteVerification;
	}
}
