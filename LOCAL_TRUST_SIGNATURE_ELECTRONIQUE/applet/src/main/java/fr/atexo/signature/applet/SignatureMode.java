package fr.atexo.signature.applet;

/**
 * Enumeration de l'ensemble des modes possible de signature.
 */
public enum SignatureMode {
    Distant, Disque, Memoire;
}
