package fr.atexo.signature.applet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;

import net.iharder.Base64;
import fr.atexo.signature.applet.bean.SignatureHashFichier;
import fr.atexo.signature.commun.exception.ParametrageException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.signature.SignaturePades;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.securite.signature.VisualSignatureRectangle;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardSignatureXadesProcessor;

/**
 *
 */
public class SignatureMixteXadesPadesApplet extends AbstractInfosModuleValidationApplet {

	private static final String NOM_LOGGER = "SignatureMixteXadesPadesApplet";
	private static final String PARAM_TYPE_HASH = "typeHash";

	protected static final String PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT = "methodeJavascriptFinTraitement";

	static {
		LoggerFactory.setNomLogger(NOM_LOGGER);
	}

	// algorithme de hash par defaut (sha1)
	private TypeAlgorithmHash typeHash = TypeAlgorithmHash.SHA1;

	private final Map<String, SignatureHashFichier> empreintesEnAttentes = new HashMap<String, SignatureHashFichier>();

	private boolean signatureXades;

	private StandardSignatureXadesProcessor signatureXadesProcessor;

	private boolean signaturePades;

	/**
	 * Spécifie s'il s'agit d'une signature qui devra être effectué de serveur à
	 * serveur, sur disque ou bien directement en mémoire.
	 */
	private SignatureMode modePades;

	/**
	 * Cette propriété peut prendre trois différentes valeurs en fonction du
	 * mode de signature. - Distant : Url du flux source du fichier pdf à signer
	 * - Disque : Chemin sur le disque du fichier pdf à signer - Memoire :
	 * Contenu encodé en base 64 du fichier pdf à signer;
	 */
	private String infosFichierSourcePades;

	/**
	 * Identifiant html dans lequel sera renvoyé le chemin du fichier signé
	 * lorsque celui-ci est en mode generation local et non serveur.
	 */
	private String identifiantPades;

	/**
	 * Précise la "date" de la signature.
	 */
	private String signatureDatePades;

	/**
	 * Précise le "type" de signataire
	 */
	private String typeSignataire;

	/**
	 * La position de la signature dans le fichier pdf. Par défaut on a
	 * positionne en (xb=10,yb=10) et (xh=210,yh=60).
	 */
	private VisualSignatureRectangle positionSignaturePades = new VisualSignatureRectangle(10, 10, 210, 60);

	/**
	 * Liste des méthodes Javascript à appeler.
	 */
	protected String methodeJavascriptFinTraitement;

	@Override
	public void init() {
		super.init();

		// paramètre typeHash
		String typeHash = getParameterAvecMessage(PARAM_TYPE_HASH);
		setTypeHash(typeHash);

		// paramètre methodeJavascriptFinTraitement
		setMethodeJavascriptFinTraitement(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT));

		// appel de la méthode javascript de fin d'initialisation de l'applet
		appellerMethodeJavascript(methodeJavascriptFinAttente);

		LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
	}

	@Override
	protected void initialiserGui() throws Throwable {

		LogManager.getInstance().afficherMessageInfo("Vérification de l'ensemble des paramétres", this.getClass());

		if (signatureXades) {
			LogManager.getInstance().afficherMessageInfo("Vérification des paramétres nécessaires à l'execution de la signature XAdES", this.getClass());

			// vérification de la présente d'empreintes à signer
			if (empreintesEnAttentes.isEmpty()) {
				appellerMethodeJavascript(methodeJavascriptFinAttente);
				throw new ParametrageException("Aucun hash de fichier n'est présent pour effectuer la signature XAdES");
			}
		}

		if (signaturePades) {
			LogManager.getInstance().afficherMessageInfo("Vérification des paramétres nécessaires à l'execution de la signature PAdES", this.getClass());

			// vérification de l'identifiant
			if (Util.estVide(identifiantPades)) {
				appellerMethodeJavascript(methodeJavascriptFinAttente);
				throw new ParametrageException("Aucun identifiant n'est présent pour effectuer la signature PAdES");
			}

			// vérification du contenu à signer
			if (Util.estVide(infosFichierSourcePades)) {
				appellerMethodeJavascript(methodeJavascriptFinAttente);
				throw new ParametrageException("Aucun contenu de fichier pdf encodé en base 64 n'est présent pour effectuer la signature PAdES");
			}
		}

		if (signatureXades || signaturePades) {
			selectionnerCertificat();
		}
	}

	@Override
	protected void onSelection(KeyPair keyPairSignature) throws ExecutionException {
		try {

			if (signatureXades) {

				// traitement des empreintes à signer
				if (!empreintesEnAttentes.isEmpty()) {

					for (String identifiant : empreintesEnAttentes.keySet()) {
						SignatureHashFichier signatureHashFichier = empreintesEnAttentes.get(identifiant);
						signerHashFichier(signatureHashFichier, keyPairSignature);
					}
				}

				// retour des signatures
				if (!empreintesEnAttentes.isEmpty()) {

					for (String identifiant : empreintesEnAttentes.keySet()) {
						SignatureHashFichier signatureHashFichier = empreintesEnAttentes.get(identifiant);
						appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, signatureHashFichier.getContenuSignatureXml());
					}
				}

				empreintesEnAttentes.clear();
			}

			if (signaturePades) {

				LogManager.getInstance().afficherMessageInfo("Mode Pades : " + modePades, this.getClass());

				InputStream inputStreamSource = null;
				File fichierDestination = null;
				OutputStream outputStreamDestination = null;

				try {

					byte[] contenuFichierSource = Base64.decode(infosFichierSourcePades);
					inputStreamSource = new ByteArrayInputStream(contenuFichierSource);
					fichierDestination = File.createTempFile("fichier_pdf_applet_signature_pades", ".pdf");
					outputStreamDestination = new FileOutputStream(fichierDestination);

				} catch (IOException e) {
					throw new ExecutionException("Une Erreur est survenue lors du traitement du fichier devant être signé en Pades", e);
				}

				if (inputStreamSource != null && outputStreamDestination != null) {

					PrivateKey privateKey = keyPairSignature.getPrivateKey();
					Certificate certificat = keyPairSignature.getCertificate();
					String cn = CertificatUtil.getCN(keyPairSignature.getCertificate().getSubjectX500Principal());

					String signatureTexte = I18nUtil.get("SIGNATURE_PADES_MESSAGE_SIGNATAIRE");
					if (!Util.estVide(this.typeSignataire)) {
						signatureTexte += "\n";
						signatureTexte += typeSignataire;
					}
					signatureTexte += "\n";
					signatureTexte += (cn != null ? cn : "");
					if (!Util.estVide(this.signatureDatePades)) {
						signatureTexte += "\n";
						signatureTexte += I18nUtil.get("SIGNATURE_PADES_MESSAGE_DATE") + " " + this.signatureDatePades;
					}

					try {
						SignaturePades.signerFichierPdf(inputStreamSource, outputStreamDestination, certificat, privateKey, positionSignaturePades, null, null,
								signatureTexte);
						inputStreamSource.close();
						outputStreamDestination.close();
						LogManager.getInstance().afficherMessageInfo(
								"Fin de la signature du fichier PDF se trouvant : " + fichierDestination.getAbsolutePath(), this.getClass());
						String contenuFichierPdfSigneEnBase64 = Base64.encodeFromFile(fichierDestination.getAbsolutePath());
						LogManager.getInstance().afficherMessageInfo("Contenu du fichier pdf encodé en base 64 :" + contenuFichierPdfSigneEnBase64,
								this.getClass());
						appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiantPades, contenuFichierPdfSigneEnBase64);

					} catch (IOException e) {
						throw new ExecutionException("Une erreur est survenue lors de la tentative de création de la signature Pades", e);
					}
				}
			}

		} finally {
			appellerMethodeJavascript(methodeJavascriptFinTraitement);
			appellerMethodeJavascript(methodeJavascriptFinAttente);
		}
	}

	/**
	 * Permet de signer ou bien encore de vérifier la conformité de la
	 * signature.
	 * 
	 * @param signatureHashFichier
	 *            le fichier contenant les informations sur le hash du fichier à
	 *            signé
	 * @param keyPairSignature
	 *            le certificat à utiliser pour effectuer la signature
	 * @return la réponse associé à la signature du fichier
	 * @throws fr.atexo.signature.commun.exception.execution.SignatureExecutionException
	 * 
	 */
	private void signerHashFichier(SignatureHashFichier signatureHashFichier, KeyPair keyPairSignature) throws SignatureExecutionException {
		LogManager.getInstance().afficherMessageInfo("Signature du hash du fichier [" + signatureHashFichier.getHashFichier() + "]", this.getClass());
		signatureXadesProcessor.signerEnXades(keyPairSignature, signatureHashFichier);
	}

	/**
	 * Initialise les informations dans le cas d'une signature Xades
	 */
	public void initialiserSignatureXades() {
		initialiserXades(null);
	}

	/**
	 * Initialise les infromations dans le cas d'une signature Xades
	 * 
	 * @param urlModuleValidation
	 *            url du module d'enrichissement de la signature si jamais la
	 *            signature est requise
	 */
	public void initialiserXades(String urlModuleValidation) {
		signatureXades = true;
		// recapitulatif des paramétres
		LogManager.getInstance().afficherMessageInfo("Url module validation : " + urlModuleValidation, this.getClass());
		signatureXadesProcessor = new StandardSignatureXadesProcessor(urlModuleValidation, typeHash);
		// on vide les infos
		empreintesEnAttentes.clear();
	}

	/**
	 * Initialise les informations dans le cas d'une signature Pades
	 * 
	 * @param identifiant
	 * @param contenuFichierPdfEnBase64
	 */
	public void initialiserSignaturePadesModeMemoire(String identifiant, String contenuFichierPdfEnBase64) {
		this.signaturePades = true;
		this.modePades = SignatureMode.Memoire;
		this.identifiantPades = identifiant;
		this.infosFichierSourcePades = contenuFichierPdfEnBase64;
	}

	public void setSignaturePadesInformation(Integer signaturePositionBasseX, Integer signaturePositionBasseY, Integer signaturePositionHauteX,
			Integer signaturePositionHauteY, String typeSignataire, String signatureDate) {
		if (signaturePositionBasseX != null && signaturePositionBasseY != null && signaturePositionHauteX != null && signaturePositionHauteY != null) {
			this.positionSignaturePades = new VisualSignatureRectangle(signaturePositionBasseX, signaturePositionBasseY, signaturePositionHauteX,
					signaturePositionHauteY);
		}
		this.typeSignataire = typeSignataire;
		this.signatureDatePades = signatureDate;
	}

	/**
	 * Ajoute un hash de fichier dans la liste des hashs de fichiers qui devront
	 * être signés.
	 * 
	 * @param identifiantHashFichier
	 *            l'identifiant unique du hash du fichier (pouvoir faire le lien
	 *            avec le html lors du retour)
	 * @param hashFichier
	 *            le hash du fichier à signer et/ou à chiffrer
	 * @param nomFichier
	 *            le nom du fichier
	 * @return <code>true</code> si l'ajout à bien été pris en compte, sinon
	 *         <code>false</false>
	 */
	public boolean ajouterHashFichier(String identifiantHashFichier, String hashFichier, String nomFichier) {

		// recapitulatif des paramétres
		LogManager.getInstance().afficherMessageInfo("Identifiant du hash : " + identifiantHashFichier, this.getClass());
		LogManager.getInstance().afficherMessageInfo("Valeur du hash : " + hashFichier, this.getClass());
		try {
			empreintesEnAttentes.put(identifiantHashFichier, new SignatureHashFichier(nomFichier, hashFichier));
			return true;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public void setTypeHash(String typeHash) {
		if (typeHash != null) {
			try {
				TypeAlgorithmHash typeAlgorithmHash = TypeAlgorithmHash.valueOf(typeHash);
				if (typeAlgorithmHash != null) {
					this.typeHash = typeAlgorithmHash;
				}
			} catch (IllegalArgumentException e) {
				LogManager.getInstance().afficherMessageWarning(
						"Le type de hash spécifié en paramétre n'est pas valide, du coup on utilise le type de hash par defaut : " + this.typeHash,
						this.getClass());
			}
		}
	}

	public void setMethodeJavascriptFinTraitement(String methodeJavascriptFinTraitement) {
		this.methodeJavascriptFinTraitement = methodeJavascriptFinTraitement;
	}
}
