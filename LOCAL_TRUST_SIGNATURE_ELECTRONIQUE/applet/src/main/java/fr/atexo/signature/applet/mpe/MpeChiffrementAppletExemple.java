package fr.atexo.signature.applet.mpe;

import fr.atexo.signature.commun.util.CertificatUtil;

import java.net.MalformedURLException;

/**
 *
 */
public class MpeChiffrementAppletExemple extends MpeChiffrementApplet {

    public static final String CHAINE_CERTIFICAT_BASE64 = "-----BEGIN CERTIFICATE-----" + CertificatUtil.CHAINE_CERTIFICAT_BASE64 + "-----END CERTIFICATE-----";

    private static final String URL_MODULE_VALIDATION = "http://localhost:8080/validation/signatureXades";

    @Override
    public void init() {
        super.init();
        try {
            setUrlPkcs11LibRefXml("http://localhost:8080/validation/pkcs11Lib/pkcs11Libs.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //initialisationGenerationSignatureSansChiffrement();
        //initialisationVerificationSignatureSansChiffrement();
        //initialisationVerificationSignatureUniqueSansChiffrement();
        //initialisationGenerationSignatureEtChiffrement();
        //initialisationGenerationSignatureEtChiffrementUnique();
        initialisationVerificationSignatureAvecChiffrement();
        setModeRestrictionRGS(false);
        //ajouterRestrictionTypeCertificatSignatureElectronique();
        executer();
    }

    private void initialisationVerificationSignatureUniqueSansChiffrement() {
        initialiser(false, null, "uid", false, false, true, URL_MODULE_VALIDATION, false);
        ajouterFichier(1, 0, 0, "identifiantACE", "/home/atexo/Bureau/chiffrement/FichierAssocie1_signaturePades.pdf", "ACE", null, null, true);
    }

    private void initialisationGenerationSignatureEtChiffrement() {
        //initialiser(true, "http://localhost:8080/validation/envoiBlocChiffre", "uid", true, true, false, "https://applet-crypto-test.local-trust.com/index.php?page=ModuleValidation", false);
        initialiser(true, "http://localhost:8080/validation/envoiBlocChiffre", "uid", true, false, false, URL_MODULE_VALIDATION, false);
        ajouterFichier(1, 0, 0, "identifiantFichierACE", "/home/atexo/Bureau/chiffrement/FichierPrincipal.pdf", "ACE", null, null, true);
        ajouterFichier(1, 0, 0, "identifiantFichierACE", "/home/atexo/Bureau/chiffrement/FichierAssocie1.pdf", "PRI", null, null, true);
        ajouterHashFichier(1, 0, 0, "identifiantHashFichierACE", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf", "PRI", null, null);
        ajouterCertificat(1, CHAINE_CERTIFICAT_BASE64);
    }

    private void initialisationGenerationSignatureEtChiffrementUnique() {
        initialiser(true, null, "uid", true, true, false, URL_MODULE_VALIDATION, true);
        ajouterFichier(1, 0, 0, "identifiantACE", "/home/atexo/Bureau/Parapheur/FichierPrincipal.pdf", "ACE", null, null, true);
        ajouterCertificatUnique(CHAINE_CERTIFICAT_BASE64);
    }

    private void initialisationGenerationSignatureSansChiffrement() {
        initialiser(false, null, null, true, false, false, URL_MODULE_VALIDATION, false);
        ajouterFichier(1, 0, 0, "identifiantACE", "/home/atexo/Bureau/chiffrement/FichierAssocie1_signaturePades.pdf", "ACE", null, null, true);
        ajouterHashFichier(1, 0, 0, "identifiantHashFichierACE", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf", "PRI", null, null);
    }

    private void initialisationVerificationSignatureSansChiffrement() {
        initialiser(false, null, null, false, false, true, URL_MODULE_VALIDATION, false);
        ajouterFichier(1, 1, 0, "identifiantACE1", "/home/atexo/Bureau/chiffrement/FichierAssocie1_signaturePades.pdf", "ACE", null, "/home/atexo/Bureau/chiffrement/FichierAssocie1_signaturePades.pdf - 20131002121223 - Signature 7.xml", true);
    }

    private void initialisationVerificationSignatureAvecChiffrement() {
        initialiser(true, null, null, true, false, true, URL_MODULE_VALIDATION, false);
        ajouterFichier(1, 0, 0, "identifiantACE1", "/home/atexo/Bureau/chiffrement/FichierPrincipal.pdf", "ACE", null, "/home/atexo/Bureau/chiffrement/FichierPrincipal.pdf - 20140224181220 - Signature 1.xml", true);
        ajouterHashFichier(1, 0, 0, "identifiantHashFichierACE", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf", "PRI", null, "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgSWQ9InNpZ25hdHVyZSI+PGRzOlNpZ25lZEluZm8gSWQ9InNpZ25lZEluZm9zIj48ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjxkczpTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjcnNhLXNoYTEiLz48ZHM6UmVmZXJlbmNlPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPmRmNyt2OHNjaWFtKzhaYUh0ZFlsVk9QZ3k1ND08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjxkczpSZWZlcmVuY2UgVHlwZT0iaHR0cDovL3VyaS5ldHNpLm9yZy8wMTkwMyNTaWduZWRQcm9wZXJ0aWVzIiBVUkk9IiN4bWxkc2lnLWI1ZmQyMTA5LTIwOGQtNGM4My1hNDgxLTZhZjE1NGM2OWU1Mi1zaWduZWRQcm9wcyI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+WWcyVkVabjB6M3RRVGtoVFk5KzlBZHE5eTkwPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5Jek1mZ0g5akVUb1NSVGFDQjZxL0VUa05iRy9kckVVQVJXWXZBWDB6a3Bhek15QWRwbnRqQ0FlRDJJL1RRcWpkZk8xOGgvUHZzVWkyCm5OZ0VyY2RVdk9TY1k0Y2dyQVJyUXljK2JCaU4wRDlJdTZ6cGR3NElqNkJmNkxJYVNnaW9hYXlDZlFESTJZV3RZOEY2NUpOSEI4a28KaWFDY3FCZGwrOWdXcWdZNTZhND08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOVN1YmplY3ROYW1lPk9JRC4yLjUuNC4xMj1Qcml2YXRlIFBlcnNvbiwgT0lELjIuNS40LjU9MTAxNjcyNTY4NjAwMDkzODk5ODIsIE9JRC4yLjUuNC40Mj1TUEFSRSwgT0lELjIuNS40LjQ9TjEwNDQsIENOPVNQQVJFIE4xMDQ0LCBDPUxVPC9kczpYNTA5U3ViamVjdE5hbWU+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlFOWpDQ0E5NmdBd0lCQWdJREFrRlVNQTBHQ1NxR1NJYjNEUUVCQlFVQU1FVXhDekFKQmdOVkJBWVRBa3hWTVJZd0ZBWURWUVFLCkV3MU1kWGhVY25WemRDQlRMa0V1TVI0d0hBWURWUVFERXhWTWRYaFVjblZ6ZENCUmRXRnNhV1pwWldRZ1EwRXdIaGNOTVRBeE1EQTEKTURZMU16SXdXaGNOTVRNeE1EQTFNRFkxTXpJd1dqQjdNUXN3Q1FZRFZRUUdFd0pNVlRFVU1CSUdBMVVFQXhNTFUxQkJVa1VnVGpFdwpORFF4RGpBTUJnTlZCQVFUQlU0eE1EUTBNUTR3REFZRFZRUXFFd1ZUVUVGU1JURWRNQnNHQTFVRUJSTVVNVEF4TmpjeU5UWTROakF3Ck1Ea3pPRGs1T0RJeEZ6QVZCZ05WQkF3VERsQnlhWFpoZEdVZ1VHVnljMjl1TUlHZk1BMEdDU3FHU0liM0RRRUJBUVVBQTRHTkFEQ0IKaVFLQmdRREFEWngzS0gvSkpZcEh0WTdNSVpycUhUaVZKVFBlVGUvZ05zSm1UcndkQklaeC9XN0gvM2duOElBeXQ1cUFhb09RQ1c0bgpNNmxueldZOTFzdWhCTnZBdFJvbVFqTzBZUVRWa2xzQjdYVGxDN0Y2OFQ0ZkxQL2drMXhVR3B1Vmk0MlhLQWZwc3ZEZnBFdjhFeHg4CnhuTGRPYzZ0RU9WMVpDUXc4c1pqRXpVWnN3SURBUUFCbzRJQ096Q0NBamN3REFZRFZSMFRBUUgvQkFJd0FEQmdCZ2dyQmdFRkJRY0IKQVFSVU1GSXdJd1lJS3dZQkJRVUhNQUdHRjJoMGRIQTZMeTl2WTNOd0xteDFlSFJ5ZFhOMExteDFNQ3NHQ0NzR0FRVUZCekFDaGg5bwpkSFJ3T2k4dlkyRXViSFY0ZEhKMWMzUXViSFV2VEZSUlEwRXVZM0owTUlJQkJBWURWUjBnQklIOE1JSDVNSUhzQmdncmdTc0JBUUlGCkFUQ0IzekNCc1FZSUt3WUJCUVVIQWdJd2dhUWFnYUZNZFhoVWNuVnpkQ0JSZFdGc2FXWnBaV1FnVTFCQlVrVWdRMlZ5ZEdsbWFXTmgKZEdVZ2IyNGdVMU5EUkNCamIyMXdiR2xoYm5RZ2QybDBhQ0JGVkZOSklGUlRJREV3TVNBME5UWWdVVU5RS3lCRFVDNGdTMlY1SUVkbApiaTRnWW5rZ1ExTlFMaUJCZFhSb2IzSnBjMlZrSUZWellXZGxPaUJUZFhCd2IzSjBJRzltSUZGMVlXeHBabWxsWkNCRmJHVmpkSEp2CmJtbGpJRk5wWjI1aGRIVnlaVEFwQmdnckJnRUZCUWNDQVJZZGFIUjBjRG92TDNKbGNHOXphWFJ2Y25rdWJIVjRkSEoxYzNRdWJIVXcKQ0FZR0JBQ1BlZ0VETURZR0NDc0dBUVVGQndFREJDb3dLREFJQmdZRUFJNUdBUUV3RWdZR0JBQ09SZ0VDTUFnVEFBSUJBQUlCQURBSQpCZ1lFQUk1R0FRUXdFUVlKWUlaSUFZYjRRZ0VCQkFRREFnVWdNQXNHQTFVZER3UUVBd0lHUURBZkJnTlZIU01FR0RBV2dCU05rS01ICjNSb1RkNWxNa3F0TlE5NC96U2xrQlRBeEJnTlZIUjhFS2pBb01DYWdKS0FpaGlCb2RIUndPaTh2WTNKc0xteDFlSFJ5ZFhOMExteDEKTDB4VVVVTkJMbU55YkRBUkJnTlZIUTRFQ2dRSVFFQUVDZnhTK1pZd0RRWUpLb1pJaHZjTkFRRUZCUUFEZ2dFQkFFdHpjbWNRdGlJMQowcVJIZDZyYXhMK3piQ1JHLzlRMklLbDdhTDJMdmY0dXl4RTFnTGFxVVlqZklka29GVWl6S0pONVlUZjJDczVzSlZRa1lLbnJ2T1B6CkZob3pCeWNxeXVJZ1FKWkJQOG5INXIvZU9ueVhqRnAxQlRiVkw4cjIxUmJOUFc4VzRxZlhHNUltL29jRXNZdEo0Y1RHc0Via1ZNOHoKb3Mrcmo2NzRRUTlVNndpcjExUGJ5YVR2MkErQ0tBK3hIT1gvMURMcWhHUDhxTTl5aU1aTDZ5djFORDNadkprZjJjTEJER1pLQzMxbAorZUNYZGRHd3F1QzFpWDNLR1NuRWdjVFdnZmtpK0xzVDJ6akhmY2pRZUUzQ2ZMMklsdGlHQ3h5cUFrTVltbDFZRElFNkhzdkRGd21uCnhoODR4aTBpd1diMnRWVzdLSFpWOXlKRW5tMD08L2RzOlg1MDlDZXJ0aWZpY2F0ZT48L2RzOlg1MDlEYXRhPjwvZHM6S2V5SW5mbz48ZHM6T2JqZWN0IElkPSJvYmplY3QiPjx4YWRlczpRdWFsaWZ5aW5nUHJvcGVydGllcyB4bWxuczp4YWRlcz0iaHR0cDovL3VyaS5ldHNpLm9yZy8wMTkwMy92MS4zLjIjIiBUYXJnZXQ9IiNzaWduYXR1cmUiPjx4YWRlczpTaWduZWRQcm9wZXJ0aWVzIElkPSJ4bWxkc2lnLWI1ZmQyMTA5LTIwOGQtNGM4My1hNDgxLTZhZjE1NGM2OWU1Mi1zaWduZWRQcm9wcyI+PHhhZGVzOlNpZ25lZFNpZ25hdHVyZVByb3BlcnRpZXM+PHhhZGVzOlNpZ25pbmdUaW1lPjIwMTQtMDItMjRUMTg6MTI6NDErMDE6MDA8L3hhZGVzOlNpZ25pbmdUaW1lPjx4YWRlczpTaWduaW5nQ2VydGlmaWNhdGU+PHhhZGVzOkNlcnQ+PHhhZGVzOkNlcnREaWdlc3Q+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPkg2R2VkY1pPZWtjZTVvWXB2cnRPQXJZVEQxdz08L2RzOkRpZ2VzdFZhbHVlPjwveGFkZXM6Q2VydERpZ2VzdD48eGFkZXM6SXNzdWVyU2VyaWFsPjxkczpYNTA5SXNzdWVyTmFtZT5DTj1MdXhUcnVzdCBRdWFsaWZpZWQgQ0EsIE89THV4VHJ1c3QgUy5BLiwgQz1MVTwvZHM6WDUwOUlzc3Vlck5hbWU+PGRzOlg1MDlTZXJpYWxOdW1iZXI+MTQ3Nzk2PC9kczpYNTA5U2VyaWFsTnVtYmVyPjwveGFkZXM6SXNzdWVyU2VyaWFsPjwveGFkZXM6Q2VydD48L3hhZGVzOlNpZ25pbmdDZXJ0aWZpY2F0ZT48L3hhZGVzOlNpZ25lZFNpZ25hdHVyZVByb3BlcnRpZXM+PC94YWRlczpTaWduZWRQcm9wZXJ0aWVzPjx4YWRlczpVbnNpZ25lZFByb3BlcnRpZXM+PHhhZGVzOlVuc2lnbmVkU2lnbmF0dXJlUHJvcGVydGllcz48eGFkZXM6Q29tcGxldGVDZXJ0aWZpY2F0ZVJlZnM+PHhhZGVzOkNlcnRSZWZzPjx4YWRlczpDZXJ0Pjx4YWRlczpDZXJ0RGlnZXN0PjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSIvPjxkczpEaWdlc3RWYWx1ZT5kN0d4bHllVVZ2VVdPb25LUVVyS2ZINnNvcW89PC9kczpEaWdlc3RWYWx1ZT48L3hhZGVzOkNlcnREaWdlc3Q+PHhhZGVzOklzc3VlclNlcmlhbD48ZHM6WDUwOUlzc3Vlck5hbWU+Q049THV4VHJ1c3Qgcm9vdCBDQSwgTz1MdXhUcnVzdCBzLmEuLCBDPUxVPC9kczpYNTA5SXNzdWVyTmFtZT48ZHM6WDUwOVNlcmlhbE51bWJlcj4xMDAzPC9kczpYNTA5U2VyaWFsTnVtYmVyPjwveGFkZXM6SXNzdWVyU2VyaWFsPjwveGFkZXM6Q2VydD48eGFkZXM6Q2VydD48eGFkZXM6Q2VydERpZ2VzdD48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+dXVXUnVKRWM1M0MvNEEyRHZ0czhycmtKOFo4PTwvZHM6RGlnZXN0VmFsdWU+PC94YWRlczpDZXJ0RGlnZXN0Pjx4YWRlczpJc3N1ZXJTZXJpYWw+PGRzOlg1MDlJc3N1ZXJOYW1lPkNOPUdURSBDeWJlclRydXN0IEdsb2JhbCBSb290LCBPVT0iR1RFIEN5YmVyVHJ1c3QgU29sdXRpb25zLCBJbmMuIiwgTz1HVEUgQ29ycG9yYXRpb24sIEM9VVM8L2RzOlg1MDlJc3N1ZXJOYW1lPjxkczpYNTA5U2VyaWFsTnVtYmVyPjEyMDAwMDU1NTwvZHM6WDUwOVNlcmlhbE51bWJlcj48L3hhZGVzOklzc3VlclNlcmlhbD48L3hhZGVzOkNlcnQ+PC94YWRlczpDZXJ0UmVmcz48L3hhZGVzOkNvbXBsZXRlQ2VydGlmaWNhdGVSZWZzPjx4YWRlczpDb21wbGV0ZVJldm9jYXRpb25SZWZzPjx4YWRlczpDUkxSZWZzPjx4YWRlczpDUkxSZWY+PHhhZGVzOkRpZ2VzdEFsZ0FuZFZhbHVlPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSIvPjxkczpEaWdlc3RWYWx1ZT4zT253WXRHamhTVU1HN1o5WGtDRTFJTFA3a009PC9kczpEaWdlc3RWYWx1ZT48L3hhZGVzOkRpZ2VzdEFsZ0FuZFZhbHVlPjx4YWRlczpDUkxJZGVudGlmaWVyPjx4YWRlczpJc3N1ZXI+Q049THV4VHJ1c3QgUXVhbGlmaWVkIENBLCBPPUx1eFRydXN0IFMuQS4sIEM9TFU8L3hhZGVzOklzc3Vlcj48eGFkZXM6SXNzdWVUaW1lPjIwMTMtMDctMTJUMTI6MDA6NDkrMDI6MDA8L3hhZGVzOklzc3VlVGltZT48L3hhZGVzOkNSTElkZW50aWZpZXI+PC94YWRlczpDUkxSZWY+PC94YWRlczpDUkxSZWZzPjwveGFkZXM6Q29tcGxldGVSZXZvY2F0aW9uUmVmcz48L3hhZGVzOlVuc2lnbmVkU2lnbmF0dXJlUHJvcGVydGllcz48L3hhZGVzOlVuc2lnbmVkUHJvcGVydGllcz48L3hhZGVzOlF1YWxpZnlpbmdQcm9wZXJ0aWVzPjwvZHM6T2JqZWN0PjwvZHM6U2lnbmF0dXJlPg==");
        ajouterCertificat(1, CHAINE_CERTIFICAT_BASE64);
    }
}
