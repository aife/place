package fr.atexo.signature.applet;

import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.signature.SignaturePkcs7;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import net.iharder.Base64;

import java.util.HashMap;
import java.util.Map;

/**
 * Applet de signature Pkcs7 de hash de fichiers.
 */
public class SignaturePkcs7Applet extends AbstractCertificatApplet {

    private static final String NOM_LOGGER = "SignaturePkcs7Applet";

    protected static final String PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT = "methodeJavascriptFinTraitement";

    /**
     * Liste des méthodes Javascript à appeler.
     */
    protected String methodeJavascriptFinTraitement;

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    private static final long serialVersionUID = 1L;

    private Map<String, String> mapHashs = new HashMap<String, String>();

    @Override
    public void init() {
        super.init();

        // paramètre methodeJavascriptFinTraitement
        setMethodeJavascriptFinTraitement(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT));

        // appel de la méthode javascript de fin d'initialisation de l'applet
        appellerMethodeJavascript(methodeJavascriptFinAttente);

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
    }

    @Override
    protected void initialiserGui() throws Throwable {
        selectionnerCertificat();
    }

    @Override
    protected void onSelection(KeyPair keyPair) throws ExecutionException {

        try {
            for (String identifiant : mapHashs.keySet()) {
                String hash = mapHashs.get(identifiant);
                if (hash != null) {
                    byte[] signatureGeneree = SignaturePkcs7.signerHash(keyPair, TypeAlgorithmHash.SHA1, hash);
                    String signatureEnBase64 = Base64.encodeBytes(signatureGeneree, 0, signatureGeneree.length, Base64.DO_BREAK_LINES);
                    appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, signatureEnBase64);
                }
            }

            appellerMethodeJavascript(methodeJavascriptFinTraitement);

        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            mapHashs.clear();
            appellerMethodeJavascript(methodeJavascriptFinAttente);
        }
    }

    /**
     * Permet d'ajouter un hash à signer.
     *
     * @param identifiant l'identifiant unique du hash
     * @param hash        le hash à signer
     */
    public void ajouterHash(String identifiant, String hash) {
        mapHashs.put(identifiant, hash);
    }


    public void setMethodeJavascriptFinTraitement(String methodeJavascriptFinTraitement) {
        this.methodeJavascriptFinTraitement = methodeJavascriptFinTraitement;
    }

}
