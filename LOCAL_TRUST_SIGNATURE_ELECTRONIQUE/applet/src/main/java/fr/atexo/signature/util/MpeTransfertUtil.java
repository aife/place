package fr.atexo.signature.util;

import fr.atexo.signature.applet.mpe.infos.MpeFichier;
import fr.atexo.signature.applet.mpe.infos.MpeHashFichier;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;

/**
 *
 */
public abstract class MpeTransfertUtil extends TransfertUtil {

    /**
     * Permet d'envoyer un bloc de chiffrement sur l'url d'un serveur en lui passant des metadonnées complémentaires.
     *
     * @param urlServeurUpload  l'url du serveur d'upload du bloc de chiffrement
     * @param uid               le uid
     * @param mpeHashFichier    le fichier en cours de traitement
     * @param numeroBlocFichier le numero du bloc de chiffrement du fichier
     * @param blocChiffrement   le contenu du bloc chiffré
     * @return la réponse du serveur sur l'intégration de l'envoi du bloc de chiffrement
     * @throws Exception
     */
    public static String envoyerBlocChiffre(String urlServeurUpload, String uid, MpeHashFichier mpeHashFichier, int numeroBlocFichier, String blocChiffrement) throws TransfertExecutionException {
        return envoyerBlocChiffre(urlServeurUpload, uid, mpeHashFichier.getTypeEnveloppe(), mpeHashFichier.getNumeroLot(), mpeHashFichier.getNomFichier(), mpeHashFichier.getTypeFichier(), mpeHashFichier.getIndex(), numeroBlocFichier, blocChiffrement);
    }

    /**
     * Permet d'envoyer un bloc de chiffrement sur l'url d'un serveur en lui passant des metadonnées complémentaires.
     *
     * @param urlServeurUpload  l'url du serveur d'upload du bloc de chiffrement
     * @param uid               le uid
     * @param mpeFichier        le fichier en cours de traitement
     * @param numeroBlocFichier le numero du bloc de chiffrement du fichier
     * @param blocChiffrement   le contenu du bloc chiffré
     * @return la réponse du serveur sur l'intégration de l'envoi du bloc de chiffrement
     * @throws Exception
     */
    public static String envoyerBlocChiffre(String urlServeurUpload, String uid, MpeFichier mpeFichier, int numeroBlocFichier, String blocChiffrement) throws TransfertExecutionException {
        return envoyerBlocChiffre(urlServeurUpload, uid, mpeFichier.getTypeEnveloppe(), mpeFichier.getNumeroLot(), mpeFichier.getFichier().getName(), mpeFichier.getTypeFichier(), mpeFichier.getIndex(), numeroBlocFichier, blocChiffrement);
    }
}
