package fr.atexo.signature.applet;

import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.chiffrement.ChiffrementProcessor;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import org.apache.commons.exec.OS;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Applet permettant d'effectuer le diagnostique
 * de l'envirronement de l'utilisateur.
 */
public class DiagnosticApplet extends AbstractApplet {

    private static final String NOM_LOGGER = "DiagnosticApplet";

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    private static final String PARAM_IDENTIFIANT_APPLET_DEMARRE = "identifiantAppletDemarre";
    private static final String PARAM_IDENTIFIANT_SYSTEME_EXPLOITATION = "identifiantSystemeExploitation";
    private static final String PARAM_IDENTIFIANT_JAVA_VERSION_NUMERO = "identifiantJavaVersionNumero";
    private static final String PARAM_IDENTIFIANT_JAVA_VERSION_BIT = "identifiantJavaVersionBit";
    private static final String PARAM_IDENTIFIANT_TEST_ACCES_MAGASIN = "identifiantTestAccesMagasin";
    private static final String PARAM_IDENTIFIANT_TEST_CHIFFREMENT = "identifiantTestChiffrement";
    private static final String PARAM_METHODE_JAVASCRIPT_FIN_DIAGNOSTIC = "methodeJavascriptFinDiagnostic";

    /**
     * Liste des identifiants auxquels rattacher les résultats des test à
     * effectuer.
     */
    private String identifiantAppletDemarre;

    private String identifiantSystemeExploitation;

    private String identifiantJavaVersionNumero;

    private String identifiantJavaVersionBit;

    private String identifiantTestAccesMagasin;

    private String identifiantTestChiffrement;

    /**
     * methode appelé lorsque la fin du diagnostic est effectué
     */
    private String methodeJavascriptFinDiagnostic;

    @Override
    public void init() {
        super.init();

        /**
         * Initialisation des paramètres de l'applet
         */
        LogManager.getInstance().afficherMessageInfo("Initialisation des paramètres de l'applet", this.getClass());

        // paramétre identifiantAppletDemarre
        setIdentifiantAppletDemarre(getParameterAvecMessage(PARAM_IDENTIFIANT_APPLET_DEMARRE));

        // paramétre identifiantSystemeExploitation
        setIdentifiantSystemeExploitation(getParameterAvecMessage(PARAM_IDENTIFIANT_SYSTEME_EXPLOITATION));

        // paramétre identifiantJavaVersionNumero
        setIdentifiantJavaVersionNumero(getParameterAvecMessage(PARAM_IDENTIFIANT_JAVA_VERSION_NUMERO));

        // paramétre identifiantJavaVersionBit
        setIdentifiantJavaVersionBit(getParameterAvecMessage(PARAM_IDENTIFIANT_JAVA_VERSION_BIT));

        // paramétre identifiantTestAccesMagasin
        setIdentifiantTestAccesMagasin(getParameterAvecMessage(PARAM_IDENTIFIANT_TEST_ACCES_MAGASIN));

        // paramétre identifiantTestChiffrement
        setIdentifiantTestChiffrement(getParameterAvecMessage(PARAM_IDENTIFIANT_TEST_CHIFFREMENT));

        // paramétre methodeJavascriptFinDiagnostic
        setMethodeJavascriptFinDiagnostic(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_DIAGNOSTIC));

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());

        executer();
    }

    /**
     * Permet de lancer l'ensemble des diagnotics dans l'environnement de l'utilisateur.
     */
    @Override
    protected void initialiserGui() throws Throwable {

        appellerMethodeJavascript(methodeJavascriptDebutAttente);

        // récupération des informations systèmes
        effectuerRecuperationInformationsSystemes();

        // test des magasins windows / mac os
        effectuerTestAccesMagasin();

        // test de chiffrement
        effectuerTestChiffrement();

        appellerMethodeJavascript(methodeJavascriptFinAttente);

        appellerMethodeJavascript(methodeJavascriptFinDiagnostic);
    }

    /**
     * Permet de récupérer les informations sur l'environnement de l'utilisateur.
     */
    private void effectuerRecuperationInformationsSystemes() {

        LogManager.getInstance().afficherMessageInfo("Récupération des informations systèmes", this.getClass());

        // information sur le démarrage de l'applet de diagnostique
        renvoyerInformation(identifiantAppletDemarre, "", true);

        // informations sur le système d'exploitation
        String osNom = System.getProperty("os.name");
        String osVersion = System.getProperty("os.version");
        renvoyerInformation(identifiantSystemeExploitation, osNom + " - " + osVersion, true);

        // informations sur la jre installé
        String javaVendor = System.getProperty("java.vendor");
        String javaVersion = System.getProperty("java.version");
        String javaVersionBits = System.getProperty("sun.arch.data.model");
        renvoyerInformation(identifiantJavaVersionNumero, javaVendor + " - " + javaVersion, true);
        renvoyerInformation(identifiantJavaVersionBit, javaVersionBits, true);
    }

    /**
     * Permet de tester l'accès au magasin de certificat dans le cas d'un système d'opération sous
     * Windows ou bien sous Mac Os.
     */
    private void effectuerTestAccesMagasin() {

        if (identifiantTestAccesMagasin != null) {

            LogManager.getInstance().afficherMessageInfo("Test de l'accès au Magasin de certificats pour Windows / Mac Os", this.getClass());

            if (OS.isFamilyWindows()) {
                LogManager.getInstance().afficherMessageInfo("Cas : Windows", this.getClass());
                effectuerTestAccesMagasin(TypeProvider.MSCAPI);
            } else if (OS.isFamilyMac()) {
                LogManager.getInstance().afficherMessageInfo("Cas : Mac Os", this.getClass());
                effectuerTestAccesMagasin(TypeProvider.APPLE);
            } else {
                LogManager.getInstance().afficherMessageInfo("Cas : Non Windows / Non Mac Os", this.getClass());
                renvoyerInformation(identifiantTestAccesMagasin, I18nUtil.get("TEST_MAGASIN_INDISPONIBLE"), false);
            }
        } else {
            LogManager.getInstance().afficherMessageInfo("Pas de Test de l'accès au Magasin de certificats pour Windows / Mac Os car le paramétre identifiantTestAccesMagasin n'est pas défini", this.getClass());
        }
    }

    /**
     * Permet de tester l'accès au magasin de certificat dans le cas d'un système d'opération sous
     * Windows ou bien sous Mac Os.
     *
     * @param typeProvider le type de provider permettant de savoir si il faut tester l'accès au magasin Windows ou bien
     *                     Mac Os.
     */
    private void effectuerTestAccesMagasin(TypeProvider typeProvider) {
        try {
            MagasinHandler.getInstance().getKeyStore(typeProvider, false);
            renvoyerInformation(identifiantTestAccesMagasin, I18nUtil.get("TEST_VALIDE"), true);
        } catch (Exception e) {
            e.printStackTrace();
            renvoyerInformation(identifiantTestAccesMagasin, I18nUtil.get("TEST_INVALIDE"), false);
        }
    }

    /**
     * Permet de tester la capacité de chiffrement.
     */
    private void effectuerTestChiffrement() {
        if (identifiantTestChiffrement != null) {

            try {
                // key pair pour effectuer et tester le chiffrement et déchiffrement
                KeyPair keyPair = CertificatUtil.getKeyPairTestChiffrement();

                // chiffrement
                List<X509Certificate> certificats = new ArrayList<X509Certificate>();
                certificats.add(keyPair.getCertificate());
                ChiffrementProcessor chiffrementProcessor = new StandardChiffrementProcessor(certificats);
                byte[] resultatChiffrement = chiffrementProcessor.chiffrer(NOM_LOGGER.getBytes());
                String contenuChiffrement = new String(resultatChiffrement);

                // déchiffrement
                DechiffrementProcessor dechiffrementProcessor = new StandardChiffrementProcessor(keyPair.getProvider(), keyPair);
                byte[] resultatDechiffrement = dechiffrementProcessor.dechiffrer(contenuChiffrement);
                String contenuDechiffrement = new String(resultatDechiffrement);

                // vérification
                boolean resultatChiffrementDechiffrement = NOM_LOGGER.equals(contenuDechiffrement);

                if (resultatChiffrementDechiffrement) {
                    renvoyerInformation(identifiantTestChiffrement, I18nUtil.get("TEST_VALIDE"), true);
                } else {
                    renvoyerInformation(identifiantTestChiffrement, I18nUtil.get("TEST_INVALIDE"), false);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            LogManager.getInstance().afficherMessageInfo("Pas de Test de la capacité de chiffrement car le paramétre identifiantJavaTestChiffrement n'est pas défini", this.getClass());
        }
    }

    /**
     * Permet de renvoyer un message pour le test qui a été effectué
     *
     * @param identifiant l'identifiant auquel devra être rattaché la réponse du test
     * @param information le message renvoyé
     * @param valide      <code>true</code> si le test s'est déroulé comme il se doit sinon <code>false</code>
     */
    private void renvoyerInformation(String identifiant, String information, boolean valide) {
        LogManager.getInstance().afficherMessageInfo(identifiant + " : " + information, this.getClass());
        if (methodeJavascriptRenvoiResultat != null && identifiant != null && information != null) {
            appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, information, String.valueOf(valide));
        }
    }

    public void setIdentifiantAppletDemarre(String identifiantAppletDemarre) {
        this.identifiantAppletDemarre = identifiantAppletDemarre;
    }

    public void setIdentifiantSystemeExploitation(String identifiantSystemeExploitation) {
        this.identifiantSystemeExploitation = identifiantSystemeExploitation;
    }

    public void setIdentifiantJavaVersionNumero(String identifiantJavaVersionNumero) {
        this.identifiantJavaVersionNumero = identifiantJavaVersionNumero;
    }

    public void setIdentifiantJavaVersionBit(String identifiantJavaVersionBit) {
        this.identifiantJavaVersionBit = identifiantJavaVersionBit;
    }

    public void setIdentifiantTestChiffrement(String identifiantTestChiffrement) {
        this.identifiantTestChiffrement = identifiantTestChiffrement;
    }

    public void setIdentifiantTestAccesMagasin(String identifiantTestAccesMagasin) {
        this.identifiantTestAccesMagasin = identifiantTestAccesMagasin;
    }

    public void setMethodeJavascriptFinDiagnostic(String methodeJavascriptFinDiagnostic) {
        this.methodeJavascriptFinDiagnostic = methodeJavascriptFinDiagnostic;
    }
}
