package fr.atexo.signature.applet;

import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.chiffrement.ChiffrementProcessor;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import net.iharder.Base64;

import javax.swing.*;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Applet d'extraction du bit-clé d'un certificat.
 */
public class ExtractionBitCleApplet extends AbstractCertificatApplet {

    private static final String NOM_LOGGER = "ExtractionBitCleApplet";

    private static final String EXEMPLE_DONNEES_CHIFFRER_DEFICHER = "atexo";

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    @Override
    public void init() {
        super.init();

        // appel de la méthode javascript de fin d'initialisation de l'applet
        appellerMethodeJavascript(methodeJavascriptFinAttente);

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
    }

    @Override
    protected void initialiserGui() throws Throwable {
        selectionnerCertificat();
    }

    @Override
    protected void onSelection(KeyPair keyPair) throws ExecutionException {
        try {
            List<X509Certificate> certificats = new ArrayList<X509Certificate>();
            certificats.add(keyPair.getCertificate());
            String chaineCertificatEnBase64 = null;

            try {
                byte[] contenuCertificat = keyPair.getCertificate().getEncoded();
                chaineCertificatEnBase64 = Base64.encodeBytes(contenuCertificat, 0, contenuCertificat.length, Base64.DO_BREAK_LINES);
                LogManager.getInstance().afficherMessageInfo("Base 64 du certificat : " + chaineCertificatEnBase64, this.getClass());
            } catch (CertificateEncodingException e) {
                LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, this.getClass());
                JOptionPane.showMessageDialog(null, I18nUtil.get("INFORMATION_MESSAGE_ERREUR_SOUCI_EXTRACTION_CERTIFICAT"), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, this.getClass());
                JOptionPane.showMessageDialog(null, I18nUtil.get("INFORMATION_MESSAGE_ERREUR_SOUCI_ENCODAGE_BASE64"), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
            }

            // chiffrement
            LogManager.getInstance().afficherMessageInfo("Test de chiffrement", this.getClass());
            ChiffrementProcessor chiffrementProcessor = new StandardChiffrementProcessor(certificats);
            byte[] resultatChiffrement = chiffrementProcessor.chiffrer(EXEMPLE_DONNEES_CHIFFRER_DEFICHER.getBytes());
            String contenuChiffrement = new String(resultatChiffrement);

            // déchiffrement
            LogManager.getInstance().afficherMessageInfo("Test de déchiffrement", this.getClass());
            DechiffrementProcessor dechiffrementProcessor = new StandardChiffrementProcessor(provider, keyPair);
            byte[] resultatDechiffrement = dechiffrementProcessor.dechiffrer(contenuChiffrement);
            String contenuDechiffrement = new String(resultatDechiffrement);

            // vérification
            boolean resultatChiffrementDechiffrement = EXEMPLE_DONNEES_CHIFFRER_DEFICHER.equals(contenuDechiffrement);

            if (resultatChiffrementDechiffrement) {
                LogManager.getInstance().afficherMessageInfo("Le test de chiffrement / déchiffrement s'est déroulé comme il le devait, la chaine de certification va être générée", this.getClass());
                appellerMethodeJavascript(methodeJavascriptRenvoiResultat, chaineCertificatEnBase64 + "\n");
            } else {
                LogManager.getInstance().afficherMessageWarning("Le test de chiffrement / déchiffrement ne s'est pas déroulé comme il le devait", this.getClass());
            }
        } finally {
            appellerMethodeJavascript(methodeJavascriptFinAttente);
        }
    }
}
