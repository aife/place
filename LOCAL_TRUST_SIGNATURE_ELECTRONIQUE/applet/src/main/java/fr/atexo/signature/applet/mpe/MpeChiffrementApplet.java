package fr.atexo.signature.applet.mpe;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.json.reponse.applet.*;
import fr.atexo.signature.applet.AbstractInfosModuleValidationApplet;
import fr.atexo.signature.applet.mpe.infos.*;
import fr.atexo.signature.commun.exception.ParametrageException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.chiffrement.ChiffrementProcessor;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.securite.processor.signature.SignaturePadesProcessor;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.util.*;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.commun.util.io.HashUtil;
import fr.atexo.signature.gui.barreprogression.BarreDeProgression;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import fr.atexo.signature.processor.StandardSignaturePadesProcessor;
import fr.atexo.signature.processor.StandardSignatureXadesProcessor;
import fr.atexo.signature.util.AffichageUtil;
import fr.atexo.signature.util.MpeTransfertUtil;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.xml.annonce.ReponseAnnonce;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;
import fr.atexo.signature.xml.annonce.ReponsesAnnonce;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;
import net.iharder.Base64;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe effectuant la signature et/ou le chiffrement d'un ensemble de fichiers.
 * <p/>
 * Ordre d'appel des méthode de l'applet pour l'initialiser et lancer la signature / chiffrement des fichiers :
 * - void initialiser(boolean chiffrementRequis, String urlUpload, String uid, boolean signatureRequise, boolean effectuerUniquementVerificationSignature, String urlModuleValidation, boolean afficherBarreProgression);
 * - void ajouterFichier(int typeEnveloppe, int numeroLot, int index,String identifiantFichier, String cheminAccesFichier, String typeFichier,String origineFichier, String cheminAccesFichierSignatureXml);
 * - void ajouterCertificat(int typeEnveloppe,String chaineDeCertificatEnBase64); ou void ajouterCertificatUnique(String chaineDeCertificatEnBase64);
 * - executer();
 */
public class MpeChiffrementApplet extends AbstractInfosModuleValidationApplet {

    private static final String NOM_LOGGER = "MpeChiffrementApplet";
    private static final String PARAM_TYPE_HASH = "typeHash";

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    // algorithme de hash par defaut (sha1)
    private TypeAlgorithmHash typeHash = TypeAlgorithmHash.SHA1;

    private ChiffrementProcessor chiffrementProcessor;

    private StandardSignatureXadesProcessor signatureXadesProcessor;

    private SignaturePadesProcessor signaturePadesProcessor;

    private boolean afficherBarreProgression;

    private boolean signatureRequise;

    private boolean signaturePadesActeEngagement;

    private boolean effectuerVerificationSignature;

    private boolean chiffrementRequis;

    private boolean certificatUniqueActif;

    private List<X509Certificate> certificatUniques = new ArrayList<X509Certificate>();

    private String urlUpload;

    private String uid;

    private Map<String, List<AbstractFichier>> fichiersEnAttentes = new HashMap<String, List<AbstractFichier>>();

    private Map<Integer, List<MpeCertificat>> certificatsParEnveloppe = new HashMap<Integer, List<MpeCertificat>>();

    private BarreDeProgression barreDeProgression;

    private String reponseAnnonceXML;

    @Override
    public void init() {
        super.init();

        // paramètre typeHash
        String typeHash = getParameterAvecMessage(PARAM_TYPE_HASH);
        setTypeHash(typeHash);

        // appel de la méthode javascript de fin d'initialisation de l'applet
        appellerMethodeJavascript(methodeJavascriptFinAttente);

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
    }

    @Override
    protected void initialiserGui() throws Throwable {

        LogManager.getInstance().afficherMessageInfo("Vérification de l'ensemble des paramétres", this.getClass());

        // vérification de la présente de fichiers à signer / chiffrer
        if (fichiersEnAttentes.isEmpty()) {
            throw new ParametrageException("Aucun fichier à vérifer/signer ou chiffrer n'est présent");
        }

        if (!signatureRequise && effectuerVerificationSignature) {
            for (List<AbstractFichier> abstractFichiers : fichiersEnAttentes.values()) {

                for (AbstractFichier abstractFichier : abstractFichiers) {
                    if (abstractFichier instanceof MpeHashFichier) {
                        throw new ParametrageException("En mode recherche/vérification de Signature, seul des fichiers physique peuvent être vérifier (aucun hash ne doit être paramétré)");
                    }
                }
            }
        }


        // vérification que pour la liste des fichiers que ceux-ci sont bien accessibles
        for (List<AbstractFichier> abstractFichiers : fichiersEnAttentes.values()) {

            for (AbstractFichier abstractFichier : abstractFichiers) {

                // dans le cas d'un fichier et non d'un hash on effectue cette verification
                if (abstractFichier instanceof MpeFichier) {
                    MpeFichier mpeFichier = (MpeFichier) abstractFichier;

                    boolean fichierPresent = mpeFichier.getFichier() != null && mpeFichier.getFichier().exists() && mpeFichier.getFichier().canRead();

                    // le fichier à partir duquel générer une signature ou bien un chiffrement
                    if (!fichierPresent) {
                        throw new ParametrageException("Le fichier [" + mpeFichier.getFichier().getPath() + "] est soit inexistant ou bien inaccessible en lecture");
                    }
                }
            }
        }

        // dans le cas d'un chiffrement on vérifie que des certificats sont bien présents
        if (chiffrementRequis) {

            if (certificatUniqueActif) {

                if (certificatUniques.isEmpty()) {
                    throw new ParametrageException("Aucune chaine de certification unique valide n'est présente pour la partie de chiffrement, de ce fait celui-ci ne pourra pas aboutir");
                } else {
                    // dans le cas où utilise le même ensemble de certificats pour chiffrer tout l'ensemble (qu'importe le type d'enveloppe et numéro de lot)
                    chiffrementProcessor = new StandardChiffrementProcessor(certificatUniques);
                }

            } else {

                if (certificatsParEnveloppe.isEmpty()) {
                    throw new ParametrageException("Aucune chaine de certification valide n'est présente pour la partie de chiffrement, de ce fait celui-ci ne pourra pas aboutir");
                }

                // vérification qu'il existe bien un certificat pour chaque couple type enveloppe + numéro de lot
                for (String typeEnveloppeNumeroLot : fichiersEnAttentes.keySet()) {
                    AbstractFichier mpeFichier = fichiersEnAttentes.get(typeEnveloppeNumeroLot).get(0);
                    List<MpeCertificat> certificatsParTypeEnveloppeNumeroLot = certificatsParEnveloppe.get(mpeFichier.getTypeEnveloppe());

                    if (certificatsParTypeEnveloppeNumeroLot == null || certificatsParEnveloppe.isEmpty()) {
                        throw new ParametrageException("Aucune chaine de certificat n'est présente pour le chiffrement des fichiers qui sont rattachés à l'enveloppe de type : [" + mpeFichier.getTypeEnveloppe() + "] et pour le numéro de lot [" + mpeFichier.getNumeroLot() + "]");
                    }
                }
            }
        }

        // on propage les informations dans le cas où le module de validation serait utilisé
        signatureXadesProcessor.ajouterInformationsPourModuleValidation(originePlateforme, origineOrganisme, origineItem, origineContexteMetier);

        // action lié à la signature requise
        if (signatureRequise && !effectuerVerificationSignature) {
            // on doit signer certains fichiers (selectionner un certificat obligatoire)
            selectionnerCertificat();
        } else if (!signatureRequise && effectuerVerificationSignature) {
            if (!fichiersEnAttentes.isEmpty()) {
                verifierPresenceSignature(fichiersEnAttentes);
            } else {
                throw new ParametrageException("Dans le cadre de la vérification de la précence d'un signature il est nécessaire qu'au moins seul fichier soit paramétré. Pour rappel les hashs de fichiers ne peuvent être utilisé dans le cadre d'une détection automatique de signature.");
            }
        } else {
            // chiffrement uniquement (pas besoin de signer)
            signerEtChiffrer(null);
        }
    }

    @Override
    protected void onSelection(KeyPair keyPair) throws ExecutionException {
        signerEtChiffrer(keyPair);
    }

    /**
     * Recherche la présence de fichiers de signature qui serait associé au fichier à signer et extrait dans ce cas les
     * informations du fichier de signature le plus récent.
     *
     * @param fichiers les fichiers contenant les informations sur les fichiers à signer.
     * @throws SignatureExecutionException
     */
    private void verifierPresenceSignature(Map<String, List<AbstractFichier>> fichiers) throws SignatureExecutionException {

        try {

            Reponse reponse = new Reponse();
            List<ReponseSignatureFichier> reponseSignatureFichiers = new ArrayList<ReponseSignatureFichier>();

            for (List<AbstractFichier> abstractFichiers : fichiers.values()) {

                for (AbstractFichier abstractFichier : abstractFichiers) {
                    if (abstractFichier instanceof MpeFichier) {

                        MpeFichier mpeFichier = (MpeFichier) abstractFichier;

                        boolean rechercherSignature = mpeFichier.getFichierSignatureXml() == null;
                        ReponseSignatureFichier reponseSignatureFichier = (ReponseSignatureFichier) signerFichierOuVerifierSignature(mpeFichier, true, null, false, true, rechercherSignature, null);
                        reponseSignatureFichiers.add(reponseSignatureFichier);
                    }
                }
            }

            if (!reponseSignatureFichiers.isEmpty()) {
                reponse.setFichiers(reponseSignatureFichiers);
            }

            // renvoi des données via la méthode JS
            if (methodeJavascriptRenvoiResultat != null) {
                String reponseJson = creerReponseJson(reponse);
                LogManager.getInstance().afficherMessageInfo("Retour Json : " + reponseJson, this.getClass());
                appellerMethodeJavascript(methodeJavascriptRenvoiResultat, reponseJson);
            } else {
                LogManager.getInstance().afficherMessageWarning("Aucun renvoi de données car la méthode javascript de retour n'a pas été définie", this.getClass());
            }

        } finally {
            fichiersEnAttentes.clear();
            appellerMethodeJavascript(methodeJavascriptFinAttente);
        }
    }


    /**
     * Permet de signer et/ou chiffrer
     *
     * @param keyPairSignature la clef nécessaire à la signature
     */
    private void signerEtChiffrer(KeyPair keyPairSignature) throws ExecutionException {
        try {
            List<ReponseSignatureFichier> reponseSignatureFichiers = new ArrayList<ReponseSignatureFichier>();
            List<ReponseSignatureHash> reponseSignatureHashs = new ArrayList<ReponseSignatureHash>();

            boolean uploadRequis = urlUpload != null;

            if (!fichiersEnAttentes.isEmpty()) {

                ReponseAnnonce reponseAnnonce = null;

                int nombreTotalFichiers = 0;
                for (List<AbstractFichier> fichiers : fichiersEnAttentes.values()) {
                    nombreTotalFichiers = nombreTotalFichiers + fichiers.size();
                }
                LogManager.getInstance().afficherMessageInfo(nombreTotalFichiers + " fichiers vont être traités pour la partie signature / chiffrement", this.getClass());
                if (afficherBarreProgression) {
                    barreDeProgression = new BarreDeProgression("Barre de progression", nombreTotalFichiers);
                }

                int indexFichier = 1;

                // pour chaque type d'enveloppe / numéro de lot
                for (String typeEnveloppeNumeroLot : fichiersEnAttentes.keySet()) {
                    if (chiffrementRequis) {
                        if (!certificatUniqueActif) {
                            LogManager.getInstance().afficherMessageInfo("Le chiffrement s'effectuera en mode une liste de certificats pour l'ensemble des enveloppes et lots", this.getClass());
                            AbstractFichier abstractFichier = fichiersEnAttentes.get(typeEnveloppeNumeroLot).get(0);
                            List<X509Certificate> certificats = recupererCertificatsPourChiffrement(abstractFichier.getTypeEnveloppe());
                            chiffrementProcessor = new StandardChiffrementProcessor(certificats);
                        } else {
                            LogManager.getInstance().afficherMessageInfo("Le chiffrement s'effectuera en mode une liste de certificats par ensemble d'enveloppes et lots", this.getClass());
                        }
                    } else {
                        LogManager.getInstance().afficherMessageInfo("Aucun chiffrement s'effectuera car celui-ci est désactivé", this.getClass());
                    }

                    // on réinitialise l'enveloppe (fichier XML)
                    ReponseAnnonceType.Enveloppes.Enveloppe enveloppe = null;

                    // on parcours la liste des fichiers pour un type d'enveloppe / numéro de lot
                    for (AbstractFichier abstractFichier : fichiersEnAttentes.get(typeEnveloppeNumeroLot)) {

                        MpeFichier mpeFichier = null;
                        MpeHashFichier mpeHashFichier = null;
                        boolean signatureObligatoire = true;
                        boolean estFichier = abstractFichier instanceof MpeFichier;
                        if (estFichier) {
                            mpeFichier = (MpeFichier) abstractFichier;
                            signatureObligatoire = mpeFichier.isSignatureObligatoire();
                        } else {
                            mpeHashFichier = (MpeHashFichier) abstractFichier;
                        }

                        // mise à jour de la barre de progression (partie haute) de l'avancement du traitement de l'ensemble des fichiers
                        String messageTraitementFichier = estFichier ? getMessageBarreProgressionFichier(mpeFichier, indexFichier, nombreTotalFichiers) : getMessageBarreProgressionFichier(mpeHashFichier, indexFichier, nombreTotalFichiers);
                        LogManager.getInstance().afficherMessageInfo("Traitement : " + messageTraitementFichier, this.getClass());

                        int nombreBlocs = estFichier ? JaxbReponsesAnnonceUtil.getNombreDeBlocChiffrement(mpeFichier.getFichier()) : 1;

                        // barre de progression : initialisation de la barre de progression (pour le fichier en cours de traitement)
                        if (afficherBarreProgression) {
                            barreDeProgression.setMessageProgressionTraitementFichier(messageTraitementFichier);
                            barreDeProgression.initialiserFichier(chiffrementRequis, false, uploadRequis, signaturePadesActeEngagement, signatureRequise, nombreBlocs);
                        }
                        // partie signature
                        if (signatureRequise) {
                            // génération de la signature
                            AbstractReponseSignature reponseSignature = null;

                            if (signatureObligatoire) {
                                reponseSignature = signerFichierOuVerifierSignature(abstractFichier, estFichier, keyPairSignature, signaturePadesActeEngagement, effectuerVerificationSignature, false, afficherBarreProgression ? barreDeProgression : null);
                            } else {
                                reponseSignature = creerReponseSignatureFichier(mpeFichier, null);
                            }

                            if (estFichier) {
                                reponseSignatureFichiers.add((ReponseSignatureFichier) reponseSignature);
                            } else {
                                reponseSignatureHashs.add((ReponseSignatureHash) reponseSignature);
                            }
                        }

                        // partie chiffrement et transfert
                        if (chiffrementProcessor != null || uploadRequis) {

                            // on crée la réponse annonce si elle n'a pas encore été créée (fichier XML)
                            if (reponseAnnonce == null) {
                                reponseAnnonce = JaxbReponsesAnnonceUtil.creerReponseAnnonce();
                            }

                            // on crée l'enveloppe si elle n'a pas encore été créée (fichier XML)
                            if (enveloppe == null) {
                                enveloppe = JaxbReponsesAnnonceUtil.creerEnveloppe(abstractFichier.getTypeEnveloppe(), abstractFichier.getNumeroLot(), true, chiffrementRequis, ReponseAnnonceConstantes.STATUT_ADMISSIBILITE_ATR);
                                reponseAnnonce.getEnveloppes().getEnveloppe().add(enveloppe);
                            }

                            // permet de couper le contenu du fichier en bloc afin de chiffrer et transférer
                            // l'ensemble un à un
                            long tailleFichier = estFichier ? mpeFichier.getFichier().length() : mpeHashFichier.getHashFichier().length();
                            int tailleBlocChiffrementMax = ReponseAnnonceConstantes.TAILLE_MAXIMALE_BLOC_CHIFFREMENT;
                            int tailleBlocChiffrement = 0;

                            // on crée le fichier (fichier XML)
                            ReponseAnnonceType.Enveloppes.Enveloppe.Fichier fichier = null;
                            String signatureXMLEnBase64 = null;
                            String hashFichier = null;
                            String nomFichier = null;
                            try {
                                // on génére le hash du fichier ou bien on récupére le hash d'un fichier
                                hashFichier = estFichier ? HashUtil.genererHashSha1Hexadecimal(mpeFichier.getFichier()) : mpeHashFichier.getHashFichier();
                            } catch (IOException e) {
                                throw new ExecutionException("Un problème est survenu au cours de la génération du hash du fichier : " + nomFichier, e);
                            }

                            if (estFichier) {
                                if (mpeFichier.getFichierSignatureXml() != null && mpeFichier.getFichierSignatureXml().exists()) {
                                    try {
                                        signatureXMLEnBase64 = Base64.encodeFromFile(mpeFichier.getFichierSignatureXml().getPath());
                                    } catch (IOException e) {
                                        throw new ExecutionException("Un problème est survenu au cours de l'encodage en base 64 du fichier de signature : " + mpeFichier.getFichierSignatureXml().getName(), e);
                                    }
                                }
                                nomFichier = mpeFichier.getFichier().getName();
                            } else {
                                signatureXMLEnBase64 = mpeHashFichier.getContenuSignatureXml();
                                nomFichier = mpeHashFichier.getNomFichier();
                            }
                            fichier = JaxbReponsesAnnonceUtil.creerFichier(nomFichier, abstractFichier.getTypeFichier(), abstractFichier.getOrigineFichier(), abstractFichier.getIndex(), Long.valueOf(tailleFichier).intValue(), hashFichier, signatureXMLEnBase64, chiffrementRequis, !estFichier, nombreBlocs);
                            enveloppe.getFichier().add(fichier);

                            for (int i = 0; i * tailleBlocChiffrementMax <= tailleFichier; i++) {

                                String messageAvancementTraitementBloc = "Bloc " + (i + 1) + "/" + nombreBlocs + " - ";
                                // barre de progression : chiffrement par bloc
                                if (afficherBarreProgression) {
                                    barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("CHIFFREMENT_EN_COURS"));
                                }

                                if ((i * tailleBlocChiffrementMax + tailleBlocChiffrementMax) <= tailleFichier) {
                                    tailleBlocChiffrement = tailleBlocChiffrementMax;
                                } else {
                                    tailleBlocChiffrement = (int) tailleFichier - (i * tailleBlocChiffrementMax);
                                }

                                byte[] contenuBloc = null;
                                try {
                                    contenuBloc = estFichier ? FileUtil.lire(mpeFichier.getFichier(), i * tailleBlocChiffrementMax, tailleBlocChiffrement) : mpeHashFichier.getHashFichier().getBytes();
                                } catch (IOException e) {
                                    throw new ExecutionException("Un problème est survenu au cours de l'opération d'extraction du bloc numéro " + i + " du fichier " + nomFichier, e);
                                }
                                String enveloppeXMLEnBase64 = null;
                                String contenuBlocChiffrement = null;
                                if (chiffrementRequis) {
                                    byte[] contenuBlocChiffre = chiffrementProcessor.chiffrer(contenuBloc);
                                    enveloppeXMLEnBase64 = Base64.encodeBytes(contenuBlocChiffre);
                                } else {
                                    contenuBlocChiffrement = Base64.encodeBytes(contenuBloc);
                                }

                                // barre de progression : incrémenter pour le chiffrement
                                if (afficherBarreProgression) {
                                    barreDeProgression.incrementerProgressionGlobale();
                                    barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("TRANSFERT_EN_COURS"));
                                }

                                if (urlUpload != null) {

                                    int indexBlocChiffrement = i + 1;
                                    String resultatUpload = null;
                                    String contenu = chiffrementRequis ? enveloppeXMLEnBase64 : contenuBlocChiffrement;
                                    if (estFichier) {
                                        resultatUpload = MpeTransfertUtil.envoyerBlocChiffre(urlUpload, uid, mpeFichier, indexBlocChiffrement, contenu);
                                    } else {
                                        resultatUpload = MpeTransfertUtil.envoyerBlocChiffre(urlUpload, uid, mpeHashFichier, indexBlocChiffrement, contenu);
                                    }

                                    // on vérifie que l'upload du bloc a bien été transmis correctement
                                    if (Util.estVide(resultatUpload) || !TransfertUtil.RESULTAT_RETOUR_UPLOAD_VALIDE.equals(resultatUpload)) {
                                        throw new TransfertExecutionException("Erreur lors de l'envoi à l'url [" + urlUpload + "] du contenu du bloc numéro [" + indexBlocChiffrement + "]. L'upload du bloc n'a pas été validé par le serveur.");
                                    }

                                } else {
                                    LogManager.getInstance().afficherMessageWarning("Aucune url d'upload paramètrée pour l'envoi des blocs de chiffrement...", this.getClass());
                                }

                                // barre de progression : incrémenter pour le transfert
                                if (afficherBarreProgression) {
                                    barreDeProgression.incrementerProgressionGlobale();
                                }

                                // on crée le bloc de chiffrement (fichier XML)
                                int tailleApresChiffrement = chiffrementRequis ? enveloppeXMLEnBase64.length() : contenuBlocChiffrement.length();
                                String hashSha1BlocChiffrement = HashUtil.genererHashSha1Hexadecimal(contenuBloc);
                                ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement blocChiffrement = JaxbReponsesAnnonceUtil.creerBlocChiffrement(chiffrementRequis, hashSha1BlocChiffrement, tailleBlocChiffrement, tailleApresChiffrement, i + 1);
                                fichier.getBlocsChiffrement().getBlocChiffrement().add(blocChiffrement);
                            }
                        }

                        // barre de progression : incrémenter pour le prochain fichier
                        if (afficherBarreProgression) {
                            barreDeProgression.incrementerProgressionTraitementFichier();
                        }

                        indexFichier++;
                    }
                }

                if (chiffrementProcessor != null || uploadRequis) {
                    String context = ReponsesAnnonce.class.getPackage().getName();
                    String reponseAnnonceXML = JAXBService.instance().getAsString(reponseAnnonce, context, FileUtil.ENCODING_ISO_8859_1);
                    LogManager.getInstance().afficherMessageInfo("Reponse Annonce XML : " + reponseAnnonceXML, this.getClass());
                    this.reponseAnnonceXML = reponseAnnonceXML;
                } else {
                    LogManager.getInstance().afficherMessageWarning("Aucun fichier XML de reponse annonce généré car le chiffrement est désactivé", this.getClass());
                }

                // renvoi des données via la méthode JS
                if (methodeJavascriptRenvoiResultat != null) {
                    String reponseAnnonceXMLEnBase64 = null;
                    try {
                        reponseAnnonceXMLEnBase64 = reponseAnnonceXML != null ? Base64.encodeBytes(reponseAnnonceXML.getBytes(FileUtil.ENCODING_UTF_8)) : null;
                    } catch (UnsupportedEncodingException e) {
                        LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, this.getClass());
                    }
                    Reponse reponse = creerReponse(reponseAnnonceXMLEnBase64, reponseSignatureFichiers, reponseSignatureHashs);
                    String reponseJson = creerReponseJson(reponse);
                    LogManager.getInstance().afficherMessageInfo("Retour Json : " + reponseJson, this.getClass());
                    appellerMethodeJavascript(methodeJavascriptRenvoiResultat, reponseJson);
                } else {
                    LogManager.getInstance().afficherMessageWarning("Aucun renvoi de données car la méthode javascript de retour n'a pas été définie", this.getClass());
                }

                fichiersEnAttentes.clear();
                certificatsParEnveloppe.clear();
                certificatUniques.clear();

            } else {
                LogManager.getInstance().afficherMessageWarning("Aucun fichier n'est présent pour la partie signature / chiffrement", this.getClass());
            }
        } finally {
            // suppression de la barre de progression
            if (afficherBarreProgression) {
                barreDeProgression.dispose();
            }
            appellerMethodeJavascript(methodeJavascriptFinAttente);
        }
    }

    /**
     * Permet de signer ou bien encore de vérifier la conformité de la signature.
     *
     * @param abstractFichier                le fichier contenant les informations sur le fichier à signé
     * @param estFichier                     <code>true</code> s'il s'agit d'un fichier à signer, sinon <code>false</code> dans le cas d'un hash à signer
     * @param keyPairSignature               le certificat à utiliser pour effectuer la signature
     * @param signaturePadesActeEngagement   <code>true</code> si l'on veut que le fichier pdf de l'acte d'engagement soit signé en Pades, sinon <code>false</code>
     * @param effectuerVerificationSignature <code>true</code> si l'on veut que l'on ne fait que vérifier la signature associé, sinon <code>false</code>
     * @param rechercherSignature            <code>true</code> si l'on veut que l'on recherche le dernier fichier de signature associé au fichier à signer, sinon <code>false</code>
     * @param barreDeProgression             la barre de progression
     * @return la réponse associé à la signature du fichier
     * @throws SignatureExecutionException
     */
    private AbstractReponseSignature signerFichierOuVerifierSignature(AbstractFichier abstractFichier, boolean estFichier, KeyPair keyPairSignature, boolean signaturePadesActeEngagement, boolean effectuerVerificationSignature, boolean rechercherSignature, BarreDeProgression barreDeProgression) throws SignatureExecutionException {

        MpeFichier mpeFichier = null;
        MpeHashFichier mpeHashFichier = null;
        InfosComplementairesCertificat resultatVerificationSignature = null;

        if (estFichier) {
            mpeFichier = (MpeFichier) abstractFichier;

            String extensionFichier = mpeFichier.getFichier().getName().substring(mpeFichier.getFichier().getName().lastIndexOf(".") + 1);
            if (signaturePadesActeEngagement && mpeFichier.getTypeFichier().equalsIgnoreCase(ReponseAnnonceConstantes.TYPE_FICHIER_ACE) && "pdf".equalsIgnoreCase(extensionFichier)) {
                // barre de progression : signature pades en cours
                if (barreDeProgression != null) {
                    barreDeProgression.setMessageProgressionGlobale(I18nUtil.get("SIGNATURE_PADES_EN_COURS"));
                }
                signaturePadesProcessor.signerEnPades(keyPairSignature, mpeFichier);
                // barre de progression : signature pades en cours (incrémente un traitement)
                if (barreDeProgression != null) {
                    barreDeProgression.incrementerProgressionGlobale();
                }
            }

            // cas où l'on doit recherche la signature sur le disque à partir du fichier à signer
            if (effectuerVerificationSignature) {
                try {
                    LogManager.getInstance().afficherMessageInfo("Vérification de signature : Recherche du fichier de Signature XML associé au fichier [" + mpeFichier.getFichier().getName() + "]", this.getClass());

                    File dernierFichierSignatureXML = mpeFichier.getFichierSignatureXml();
                    // on récupère le dernier fichier de signature xml correspondant
                    if (rechercherSignature) {
                        dernierFichierSignatureXML = FileUtil.getDernierFichierFichierSignatureXML(mpeFichier.getFichier().getAbsolutePath());
                        mpeFichier.setFichierSignatureXml(dernierFichierSignatureXML);
                    }
                    // vérification
                    if (dernierFichierSignatureXML != null && dernierFichierSignatureXML.exists()) {
                        LogManager.getInstance().afficherMessageInfo("La signature étant déjà présente, on vérifie l'intégrité du fichier associé : " + mpeFichier.getFichier().getName(), this.getClass());
                        String contenuFichierSignature = FileUtils.readFileToString(mpeFichier.getFichierSignatureXml(), FileUtil.ENCODING_UTF_8);
                        resultatVerificationSignature = signatureXadesProcessor.verifierConformite(mpeFichier.getFichier(), contenuFichierSignature, true);
                        LogManager.getInstance().afficherMessageInfo("Résultation de la vérification de la signature : " + resultatVerificationSignature, this.getClass());
                    } else {
                        LogManager.getInstance().afficherMessageWarning("Aucun fichier de signature étant déjà présent pour le fichier [" + mpeFichier.getFichier().getName() + "]", this.getClass());
                    }
                } catch (IOException e) {
                    throw new SignatureExecutionException("Impossible de lire le fichier de signature XML : " + mpeFichier.getFichierSignatureXml().getPath(), e);
                }
            } else {
                LogManager.getInstance().afficherMessageInfo("Vérification de signature du fichier [" + mpeFichier.getFichier().getName() + "] non demandée", this.getClass());
            }

        } else {
            mpeHashFichier = (MpeHashFichier) abstractFichier;
            if (effectuerVerificationSignature) {
                LogManager.getInstance().afficherMessageInfo("Vérification de signature :  hash à vérifier : " + mpeHashFichier.getHashFichier(), this.getClass());
                if (!Util.estVide(mpeHashFichier.getHashFichier()) && !Util.estVide(mpeHashFichier.getContenuSignatureXml())) {
                    byte[] contenuFichierSignatureXML;
                    try {
                        contenuFichierSignatureXML = Base64.decode(mpeHashFichier.getContenuSignatureXml());
                    } catch (IOException e) {
                        throw new SignatureExecutionException("Impossible de lire le contenu base64 de la signature du hash [" + mpeHashFichier.getHashFichier() + "]", e);
                    }
                    resultatVerificationSignature = signatureXadesProcessor.verifierConformite(mpeHashFichier.getHashFichier().getBytes(), new String(contenuFichierSignatureXML), true);
                    LogManager.getInstance().afficherMessageInfo("Résultation de la vérification de la signature : " + resultatVerificationSignature, this.getClass());
                } else {
                    LogManager.getInstance().afficherMessageWarning("Aucune signature est présente pour le hash [" + mpeHashFichier.getHashFichier() + "]", this.getClass());
                }
            } else {
                LogManager.getInstance().afficherMessageInfo("Vérification de signature du hash [" + mpeHashFichier.getHashFichier() + "] de fichier non demandée", this.getClass());
            }
        }

        // cas generation pure de signature
        if (!effectuerVerificationSignature) {
            InfosComplementairesCertificat resultatSignature = null;
            // barre de progression : signature xades en cours
            if (barreDeProgression != null) {
                barreDeProgression.setMessageProgressionGlobale(I18nUtil.get("SIGNATURE_XADES_EN_COURS"));
            }
            if (estFichier && mpeFichier.isSignatureObligatoire()) {
                LogManager.getInstance().afficherMessageInfo("Génération du fichier de Signature XML associé au fichier : " + mpeFichier.getFichier().getName(), this.getClass());
                mpeFichier.setFichierSignatureXml(null);
                resultatSignature = signatureXadesProcessor.signerEnXades(keyPairSignature, mpeFichier);
            } else if (!estFichier) {
                LogManager.getInstance().afficherMessageInfo("Génération du fichier de Signature XML associé au fichier (hash) : " + mpeHashFichier.getNomFichier(), this.getClass());
                resultatSignature = signatureXadesProcessor.signerEnXades(keyPairSignature, mpeHashFichier);
            }

            // barre de progression : signature xades en cours (incrémente un traitement)
            if (barreDeProgression != null) {
                barreDeProgression.incrementerProgressionGlobale();
            }

            if (resultatSignature == null) {
                throw new SignatureExecutionException("La vérification de la signature Xades généré avec le certificat client n'est pas conforme");
            } else {
                resultatVerificationSignature = resultatSignature;
            }
        }

        abstractFichier.setSignatureVerifiee(resultatVerificationSignature != null);

        InfosCertificat infosCertificat = null;
        if (resultatVerificationSignature != null) {
            infosCertificat = new InfosCertificat(resultatVerificationSignature);
        }

        AbstractReponseSignature reponseSignature;
        if (estFichier) {
            reponseSignature = creerReponseSignatureFichier(mpeFichier, infosCertificat);
        } else {
            reponseSignature = creerReponseSignatureHash(mpeHashFichier, infosCertificat);
        }

        return reponseSignature;
    }

    /**
     * Crée une réponse au format Json.
     *
     * @param reponse la réponse à transformer au format json
     * @return la réponse au format json
     */
    private String creerReponseJson(Reponse reponse) {
        JSONObject jsonObject = new JSONObject(reponse, true);
        String reponseJson = jsonObject.toString();
        return reponseJson;
    }

    /**
     * Crée une réponse signature fichier au format Json.
     *
     * @param reponse la réponse à transformer au format json
     * @return la réponse au format json
     */
    private String creerReponseSignatureFichierJson(ReponseSignatureFichier reponse) {
        JSONObject jsonObject = new JSONObject(reponse, true);
        String reponseJson = jsonObject.toString();
        return reponseJson;
    }

    /**
     * Crée une réponse.
     *
     * @param reponseAnnonceXML le contenu de la réponse annonce XML encodé en base 64
     * @param fichiers          la liste des fichiers traités
     * @param hashs             la liste des hash de fichiers traités
     * @return la réponse
     */
    private Reponse creerReponse(String reponseAnnonceXML, List<ReponseSignatureFichier> fichiers, List<ReponseSignatureHash> hashs) {
        Reponse reponse = new Reponse();
        reponse.setContenuReponseAnnonceXML(reponseAnnonceXML);
        if (fichiers != null && !fichiers.isEmpty()) {
            reponse.setFichiers(fichiers);
        }
        if (hashs != null && !hashs.isEmpty()) {
            reponse.setHashs(hashs);
        }
        return reponse;
    }

    /**
     * Crée une réponse pour un fichier
     *
     * @param fichier le contenu du fichier à transposé.
     * @return la réponse fichier/
     */
    private ReponseSignatureFichier creerReponseSignatureFichier(MpeFichier fichier, InfosComplementairesCertificat infosComplementairesCertificat) {
        ReponseSignatureFichier reponseSignatureFichier = new ReponseSignatureFichier();
        reponseSignatureFichier.setIdentifiant(fichier.getIdentifiant());
        reponseSignatureFichier.setTypeEnveloppe(fichier.getTypeEnveloppe());
        reponseSignatureFichier.setNumeroLot(fichier.getNumeroLot());
        reponseSignatureFichier.setIndex(fichier.getIndex());
        reponseSignatureFichier.setCheminFichier(fichier.getFichier().getAbsolutePath());
        reponseSignatureFichier.setSignatureVerifiee(fichier.isSignatureVerifiee());
        if (fichier.getFichierSignatureXml() != null) {
            reponseSignatureFichier.setCheminSignatureXML(fichier.getFichierSignatureXml().getAbsolutePath());
        }
        reponseSignatureFichier.setInfosSignature(infosComplementairesCertificat);
        return reponseSignatureFichier;
    }

    /**
     * Crée une réponse pour un hash de fichier
     *
     * @param fichier le contenu du fichier à transposé.
     * @return la réponse fichier/
     */
    private ReponseSignatureHash creerReponseSignatureHash(MpeHashFichier fichier, InfosComplementairesCertificat infosComplementairesCertificat) {
        ReponseSignatureHash reponseSignatureHash = new ReponseSignatureHash();
        reponseSignatureHash.setIdentifiant(fichier.getIdentifiant());
        reponseSignatureHash.setTypeEnveloppe(fichier.getTypeEnveloppe());
        reponseSignatureHash.setNumeroLot(fichier.getNumeroLot());
        reponseSignatureHash.setIndex(fichier.getIndex());
        reponseSignatureHash.setNomFichier(fichier.getNomFichier());
        reponseSignatureHash.setHashFichier(fichier.getHashFichier());
        reponseSignatureHash.setSignatureVerifiee(fichier.isSignatureVerifiee());
        if (fichier.getContenuSignatureXml() != null) {
            reponseSignatureHash.setContenuSignatureXML(fichier.getContenuSignatureXml());
        }
        reponseSignatureHash.setInfosSignature(infosComplementairesCertificat);

        return reponseSignatureHash;
    }

    private String getMessageBarreProgressionFichier(MpeFichier mpeFichier, int numeroFichierEnCours, int nombreTotalFichiers) {
        return "Fichier " + (numeroFichierEnCours) + "/" + nombreTotalFichiers + " - " + getMessageTypeEnveloppeEtNumeroLot(mpeFichier) + " : " + mpeFichier.getFichier().getName() + " (" + AffichageUtil.getMessageTailleFichier(mpeFichier.getFichier()) + ")";
    }

    private String getMessageBarreProgressionFichier(MpeHashFichier mpeHashFichier, int numeroFichierEnCours, int nombreTotalFichiers) {
        return "Fichier (Hash) " + (numeroFichierEnCours) + "/" + nombreTotalFichiers + " - " + getMessageTypeEnveloppeEtNumeroLot(mpeHashFichier) + " : " + mpeHashFichier.getNomFichier() + " (" + AffichageUtil.getMessageTailleTexte(mpeHashFichier.getHashFichier()) + ")";
    }

    /**
     * Retourne le type d'enveloppe sous forme de String
     *
     * @return le message en fonction de l'enveloppe et du numéro de lot
     */
    public static String getMessageTypeEnveloppeEtNumeroLot(AbstractMpeInfos infos) {
        return AffichageUtil.getMessageTypeEnveloppeEtNumeroLot(infos.getTypeEnveloppe(), infos.getNumeroLot());
    }


    /**
     * Initialise l'applet avec les bon processus de signature et/ou de chiffrement.
     * Attention si le paramètre effectuerUniquementVerificationSignature est à <code>true</code> alors lors de l'utilisation de la méthode d'ajout de fichier
     * à signer / chiffrer doit inclure obligatoirement le fichier XML de signature car dans ce cas on effectuera uniquement une vérification de la conformité de la
     * signature avec son fichier et alors il ne sera pas proposer le choix de sélectionner un certificat afin de signer des fichiers qui n'auraient pas de signature XML
     * associé.
     *
     * @param chiffrementRequis              <code>true</code> si l'on veut chiffrer un fichier, sinon <code>false</code>
     * @param urlUpload                      url d'upload du fichier.
     * @param uid                            l'uid
     * @param signatureRequise               <code>true</code> si l'on veut signer un fichier, sinon <code>false</code>
     * @param signaturePadesActeEngagement   <code>true</code> si l'on veut que le fichier pdf de l'acte d'engagement soit signé en Pades, sinon <code>false</code>
     * @param effectuerVerificationSignature <code>true</code> si l'on veut vérifier la présence d'un fichier de signature lié au fichier à signer, sinon <code>false</code>
     * @param afficherBarreProgression       <code>true</code> si l'on veut afficher la barre de progression, sinon <code>false</code>
     * @param urlModuleValidation            url du module d'enrichissement de la signature si jamais la signature est requise
     */
    public void initialiser(boolean chiffrementRequis, String urlUpload, String uid, boolean signatureRequise, boolean signaturePadesActeEngagement, boolean effectuerVerificationSignature, String urlModuleValidation, boolean afficherBarreProgression) {

        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Chiffrement requis : " + chiffrementRequis, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Url upload fichiers : " + urlUpload, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Uid : " + uid, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Signature requise : " + signatureRequise, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Signature Pades de l'acte d'engagement : " + signaturePadesActeEngagement, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Effectuer uniquement la vérification unitaire de la signature : " + effectuerVerificationSignature, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Url module validation : " + urlModuleValidation, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Afficher la barre de progression : " + afficherBarreProgression, this.getClass());

        // on vide les infos
        fichiersEnAttentes.clear();
        certificatUniques.clear();
        certificatsParEnveloppe.clear();
        reponseAnnonceXML = null;

        // chiffrement
        this.chiffrementRequis = chiffrementRequis;
        this.urlUpload = !Util.estVide(urlUpload) ? urlUpload : null;
        this.uid = uid;

        // signature
        this.signatureRequise = signatureRequise;
        this.signaturePadesActeEngagement = signaturePadesActeEngagement;
        this.effectuerVerificationSignature = effectuerVerificationSignature;

        // afficher le barre de progression
        this.afficherBarreProgression = afficherBarreProgression;

        // initialisation du processor permettant de générer les fichier de signature XML au format Xades
        signatureXadesProcessor = new StandardSignatureXadesProcessor(urlModuleValidation, typeHash);

        // initialisation du processor permettant de signer les fichier pdf au format Pades
        if (signaturePadesActeEngagement) {
            signaturePadesProcessor = new StandardSignaturePadesProcessor();
        }
    }

    /**
     * Ajoute un fichier dans la liste des fichiers qui devront être signés et/ou chiffrés.
     * Attention si la propriété effectuerVerificationSignature est à <code>true</code> alors l'ajout de fichier ne dois être effectué qu'une fois
     * car dans ce cas on effectuera uniquement une vérification de la conformité de la signature avec son fichier et alors il ne sera pas proposer
     * le choix de sélectionner un certificat afin de signer des fichiers qui n'auraient pas de signature XML.
     * Dans le cas où des fichiers seraient ajoutés sans de fichier de signature XML associé, ceux-ci seront automatiquement non considérés comme devant être
     * traités.
     *
     * @param typeEnveloppe                  le type d'enveloppes (voir {@link fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes} pour obtenir la liste exhaustive des types possibles)
     * @param numeroLot                      le numéro du lot auquel est associé le fichier (0 dans le cas d'une consultation non alloti)
     * @param index                          l'index de la pièce jointe
     * @param identifiantFichier             l'identifiant unique du fichier (pouvoir faire le lien avec le html lors du retour)
     * @param cheminAccesFichier             le chemin d'accès du fichier à signer et/ou à chiffrer
     * @param typeFichier                    le type de fichier (voir {@link fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes} pour obtenir la liste exhaustive des types possibles)
     * @param origineFichier                 l'origine du fichier
     * @param cheminAccesFichierSignatureXml le chemin d'accès de la signature xml associé au fichier
     * @param signatureObligatoire           <code>true</code> si la signature est obligatoire, sinon <code>false</code>
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterFichier(int typeEnveloppe, int numeroLot, int index, String identifiantFichier, String cheminAccesFichier, String typeFichier, String origineFichier, String cheminAccesFichierSignatureXml, boolean signatureObligatoire) {
        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Type d'enveloppe : " + typeEnveloppe, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Numéro de lot : " + numeroLot, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Index : " + index, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Chemin d'accès fichier : " + cheminAccesFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Type de fichier : " + typeFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Origine du fichier : " + origineFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Chemin d'accès fichier signature Xades XML : " + cheminAccesFichierSignatureXml, this.getClass());
        try {
            if (cheminAccesFichier != null) {
                File fichier = new File(cheminAccesFichier);
                File fichierXml = null;
                if (cheminAccesFichierSignatureXml != null) {
                    fichierXml = new File(cheminAccesFichierSignatureXml);
                }

                MapUtil mapUtil = new MapUtil<String, MpeFichier>();
                if (fichierXml != null) {
                    MpeFichier mpeFichier = new MpeFichier(typeEnveloppe, numeroLot, index, identifiantFichier, typeFichier, origineFichier, fichier, fichierXml, signatureObligatoire);
                    LogManager.getInstance().afficherMessageInfo("Ajout du fichier : " + mpeFichier.toString(), this.getClass());
                    mapUtil.ajouter(fichiersEnAttentes, mpeFichier.getTypeEnveloppeNumeroLot(), mpeFichier);
                    return true;
                } else {
                    MpeFichier mpeFichier = new MpeFichier(typeEnveloppe, numeroLot, index, identifiantFichier, typeFichier, origineFichier, fichier, null, signatureObligatoire);
                    LogManager.getInstance().afficherMessageInfo("Ajout du fichier : " + mpeFichier.toString(), this.getClass());
                    mapUtil.ajouter(fichiersEnAttentes, mpeFichier.getTypeEnveloppeNumeroLot(), mpeFichier);
                    return true;
                }

            } else {
                LogManager.getInstance().afficherMessageWarning("Le fichier " + cheminAccesFichier + " n'est pas accessible", this.getClass());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Ajoute un hash de fichier dans la liste des hashs de fichiers qui devront être signés et/ou chiffrés.
     * Attention si la propriété effectuerVerificationSignature est à <code>true</code> alors l'ajout de hash de fichier ne dois être effectué qu'une fois
     * car dans ce cas on effectuera uniquement une vérification de la conformité de la signature avec son fichier et alors il ne sera pas proposer
     * le choix de sélectionner un certificat afin de signer des hash de fichiers qui n'auraient pas de signature XML.
     * Dans le cas où des fichiers seraient ajoutés sans de fichier de signature XML associé, ceux-ci seront automatiquement non considérés comme devant être
     * traités.
     *
     * @param typeEnveloppe          le type d'enveloppes (voir {@link fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes} pour obtenir la liste exhaustive des types possibles)
     * @param numeroLot              le numéro du lot auquel est associé le fichier (0 dans le cas d'une consultation non alloti)
     * @param index                  l'index de la pièce jointe
     * @param identifiantHashFichier l'identifiant unique du hash du fichier (pouvoir faire le lien avec le html lors du retour)
     * @param hashFichier            le hash du fichier à signer et/ou à chiffrer
     * @param nomFichier             le nom du fichier à signer et/ou à chiffrer
     * @param typeFichier            le type de fichier (voir {@link fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes} pour obtenir la liste exhaustive des types possibles)
     * @param origineFichier         l'origne du fichier
     * @param contenuFichierSignatureXmlEnBase64
     *                               le contenu de la signature en base 64 du hash du fichier
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterHashFichier(int typeEnveloppe, int numeroLot, int index, String identifiantHashFichier, String hashFichier, String nomFichier, String typeFichier, String origineFichier, String contenuFichierSignatureXmlEnBase64) {
        //recapitulatif des paramétres

        String hashFichierFinal = hashFichier != null ? hashFichier.toUpperCase() : null;

        LogManager.getInstance().afficherMessageInfo("Type d'enveloppe : " + typeEnveloppe, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Numéro de lot : " + numeroLot, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Index : " + index, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Nom du fichier : " + nomFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Hash du fichier : " + hashFichierFinal, this.getClass());
        LogManager.getInstance().afficherMessageInfo("type de fichier : " + typeFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Origine du fichier : " + origineFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Contenu de signature Xades XML (Base 64) : " + contenuFichierSignatureXmlEnBase64, this.getClass());
        try {

            MapUtil mapUtil = new MapUtil<String, MpeHashFichier>();

            MpeHashFichier mpeHashFichier = new MpeHashFichier(typeEnveloppe, numeroLot, index, identifiantHashFichier, typeFichier, origineFichier, hashFichierFinal, nomFichier, contenuFichierSignatureXmlEnBase64);
            LogManager.getInstance().afficherMessageInfo("Ajout du fichier : " + mpeHashFichier.toString(), this.getClass());
            mapUtil.ajouter(fichiersEnAttentes, mpeHashFichier.getTypeEnveloppeNumeroLot(), mpeHashFichier);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Ajoute un certificat dans la liste des certifcats à utiliser lors des opérations de chiffrement.
     * Attention si la propriété chiffrementRequis est à <code>true</code> alors il est nécessaire que des certificats soient
     * ajoutés par le biais de cette méthode.
     *
     * @param typeEnveloppe              le type d'enveloppes (voir {@link fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes} pour obtenir la liste exhaustive des types possibles)
     * @param chaineDeCertificatEnBase64 le certificat sous la forme d'une chaine de caratères encodé en base 64
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterCertificat(int typeEnveloppe, String chaineDeCertificatEnBase64) {

        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Type d'enveloppe : " + typeEnveloppe, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Chaine de certificat : " + chaineDeCertificatEnBase64, this.getClass());

        if (chiffrementRequis) {

            if (chaineDeCertificatEnBase64 != null) {

                X509Certificate certificat = null;
                try {
                    certificat = CertificatUtil.getX509Certificate(chaineDeCertificatEnBase64);
                    MpeCertificat mpeCertificat = new MpeCertificat(typeEnveloppe, certificat);
                    LogManager.getInstance().afficherMessageInfo("Ajout d'un certificat : " + mpeCertificat.toString(), this.getClass());
                    new MapUtil<Integer, MpeCertificat>().ajouter(certificatsParEnveloppe, mpeCertificat.getTypeEnveloppe(), mpeCertificat);
                    return true;

                } catch (IOException e) {
                    LogManager.getInstance().afficherMessageWarning("Impossible de décoder le flux encodé en base 64", this.getClass());
                    LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
                } catch (CertificateException e) {
                    LogManager.getInstance().afficherMessageWarning("Impossible de générer le certificat à partir du flux décodé en base 64", this.getClass());
                    LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
                }

            } else {
                LogManager.getInstance().afficherMessageWarning("Le certificat " + chaineDeCertificatEnBase64 + " n'est pas accessible", this.getClass());
            }

            certificatUniqueActif = false;

        } else {
            LogManager.getInstance().afficherMessageWarning("Le certificat ne sera rajouté car le chiffrement n'est pas activé", this.getClass());
        }

        return false;
    }

    /**
     * Ajoute un seul et unique certificat qui sera utilisé lors des opérations de chiffrement.
     *
     * @param chaineDeCertificatEnBase64 le certificat sous la forme d'une chaine de caratères encodé en base 64
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterCertificatUnique(String chaineDeCertificatEnBase64) {

        if (chiffrementRequis) {

            if (chaineDeCertificatEnBase64 != null) {

                try {
                    X509Certificate certificatUnique = CertificatUtil.getX509Certificate(chaineDeCertificatEnBase64);
                    if (certificatUnique != null) {
                        certificatUniques.add(certificatUnique);
                        LogManager.getInstance().afficherMessageInfo("Ajout du certificat unique", this.getClass());
                        certificatUniqueActif = true;
                        return true;
                    } else {
                        return false;
                    }


                } catch (IOException e) {
                    LogManager.getInstance().afficherMessageWarning("Impossible de décoder le flux encodé en base 64", this.getClass());
                    LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
                } catch (CertificateException e) {
                    LogManager.getInstance().afficherMessageWarning("Impossible de générer le certificat à partir du flux décodé en base 64", this.getClass());
                    LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
                }
            }
        } else {
            LogManager.getInstance().afficherMessageWarning("Le certificat ne sera rajouté car le chiffrement n'est pas activé", this.getClass());
        }

        return false;
    }

    /**
     * Récupère l'ensemble des certificats qui sont lié au couple type d'enveloppe / numéro du lot.
     *
     * @param typeEnveloppe le type d'enveloppe
     * @return la liste des certificats
     */
    private List<X509Certificate> recupererCertificatsPourChiffrement(Integer typeEnveloppe) {

        List<X509Certificate> certificats = new ArrayList<X509Certificate>();

        if (this.certificatsParEnveloppe != null) {
            List<MpeCertificat> mpeCertificats = this.certificatsParEnveloppe.get(typeEnveloppe);
            for (MpeCertificat mpeCertificat : mpeCertificats) {
                certificats.add(mpeCertificat.getCertificat());
            }
        }
        return certificats;
    }

    public void setTypeHash(String typeHash) {
        if (typeHash != null) {
            try {
                TypeAlgorithmHash typeAlgorithmHash = TypeAlgorithmHash.valueOf(typeHash);
                if (typeAlgorithmHash != null) {
                    this.typeHash = typeAlgorithmHash;
                }
            } catch (IllegalArgumentException e) {
                LogManager.getInstance().afficherMessageWarning("Le type de hash spécifié en paramétre n'est pas valide, du coup on utilise le type de hash par defaut : " + this.typeHash, this.getClass());
            }
        }
    }

    public void setTypeHash(TypeAlgorithmHash typeHash) {
        this.typeHash = typeHash;
    }

    public String getReponseAnnonceXML() {
        return reponseAnnonceXML;
    }

    public boolean isSignatureRequise() {
        return signatureRequise;
    }

    public boolean isSignaturePadesActeEngagement() {
        return signaturePadesActeEngagement;
    }

    public boolean isEffectuerVerificationSignature() {
        return effectuerVerificationSignature;
    }

    public boolean isChiffrementRequis() {
        return chiffrementRequis;
    }
}
