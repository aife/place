package fr.atexo.signature.applet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import net.iharder.Base64;

import org.apache.commons.io.IOUtils;

import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.signature.SignaturePades;
import fr.atexo.signature.commun.securite.signature.VisualSignatureRectangle;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;

/**
 * Applet de signature PADES de fichiers PDFs.
 */
public class SignaturePadesApplet extends AbstractCertificatApplet {

	private static final String NOM_LOGGER = "SignaturePadesApplet";

	static {
		LoggerFactory.setNomLogger(NOM_LOGGER);
	}

	private static final long serialVersionUID = 1L;

	/**
	 * Spécifie s'il s'agit d'une signature qui devra être effectué de serveur à
	 * serveur, sur disque ou bien directement en mémoire.
	 */
	private SignatureMode mode;

	/**
	 * Cette propriété peut prendre trois différentes valeurs en fonction du
	 * mode de signature. - Distant : Url du flux source du fichier pdf à signer
	 * - Disque : Chemin sur le disque du fichier pdf à signer - Memoire :
	 * Contenu encodé en base 64 du fichier pdf à signer;
	 */
	private String infosFichierSource;

	/**
	 * Url du flux de destination du fichier pdf signé dans le cas où l'on est
	 * en mode Distant.
	 */
	private String urlFichierDestination;

	/**
	 * Identifiant html dans lequel sera renvoyé le chemin du fichier signé
	 * lorsque celui-ci est en mode generation local et non serveur.
	 */
	private String identifiant;

	/**
	 * Précise le "motif" de la signature. Cette information sera directement
	 * intégré au niveau de la signature Pades en tant que méta-donnée "Motif".
	 */
	private String signatureMotif;

	/**
	 * Précise le "lieu" de la signature. Cette information sera directement
	 * intégré au niveau de la signature Pades en tant que méta-donnée "Lieu".
	 */
	private String signatureLocalisation;

	/**
	 * Précise la "date" de la signature.
	 */
	private String signatureDate;

	/**
	 * La position de la signature dans le fichier pdf. Par défaut on a
	 * positionne en (xb=10,yb=10) et (xh=210,yh=60).
	 */
	private VisualSignatureRectangle positionSignature = new VisualSignatureRectangle(10, 10, 210, 60);

	@Override
	public void init() {
		super.init();

		// appel de la méthode javascript de fin d'initialisation de l'applet
		appellerMethodeJavascript(methodeJavascriptFinAttente);

		LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
	}

	@Override
	protected void initialiserGui() throws Throwable {
		selectionnerCertificat();
	}

	public void initialiserSignatureInformation(String signatureMotif, String signatureLocalisation, String signatureDate) {
		initialiserSignatureInformation(null, null, null, null, signatureMotif, signatureLocalisation, signatureDate);
	}

	public void initialiserSignatureInformation(Integer signaturePositionBasseX, Integer signaturePositionBasseY, Integer signaturePositionHauteX,
			Integer signaturePositionHauteY, String signatureMotif, String signatureLocalisation, String signatureDate) {
		if (signaturePositionBasseX != null && signaturePositionBasseY != null && signaturePositionHauteX != null && signaturePositionHauteY != null) {
			this.positionSignature = new VisualSignatureRectangle(signaturePositionBasseX, signaturePositionBasseY, signaturePositionHauteX,
					signaturePositionHauteY);
		}
		this.signatureMotif = signatureMotif;
		this.signatureLocalisation = signatureLocalisation;
		this.signatureDate = signatureDate;
	}

	public void initialiserSignatureModeDistant(String identifiant, String urlFichierSource, String urlFichierDestination) {
		this.mode = SignatureMode.Distant;
		this.identifiant = identifiant;
		this.infosFichierSource = urlFichierSource;
		this.infosFichierSource = urlFichierDestination;
	}

	public void initialiserSignatureModeDisque(String identifiant, String cheminFichierSource) {
		this.mode = SignatureMode.Disque;
		this.identifiant = identifiant;
		this.infosFichierSource = cheminFichierSource;
	}

	public void initialiserSignatureModeMemoire(String identifiant, String contenuFichierPdfEnBase64) {
		this.mode = SignatureMode.Memoire;
		this.identifiant = identifiant;
		this.infosFichierSource = contenuFichierPdfEnBase64;

	}

	@Override
	protected void onSelection(KeyPair keyPair) throws ExecutionException {

		InputStream inputStreamSource = null;
		File fichierDestination = null;
		OutputStream outputStreamDestination = null;

		try {
			LogManager.getInstance().afficherMessageInfo("Mode : " + mode, this.getClass());

			switch (mode) {
			case Distant:
				inputStreamSource = getInputStreamSourceDistant(infosFichierSource);
				fichierDestination = getFichierDestination(true, null);
				break;
			case Disque:
				File fichierSource = new File(infosFichierSource);
				inputStreamSource = new FileInputStream(fichierSource);
				fichierDestination = getFichierDestination(false, infosFichierSource);
				outputStreamDestination = new FileOutputStream(fichierDestination);
				break;
			case Memoire:
				byte[] contenuFichierSource = Base64.decode(infosFichierSource);
				inputStreamSource = new ByteArrayInputStream(contenuFichierSource);
				fichierDestination = getFichierDestination(true, null);
				outputStreamDestination = new FileOutputStream(fichierDestination);
				break;
			}

			PrivateKey privateKey = keyPair.getPrivateKey();
			Certificate certificat = keyPair.getCertificate();
			String cn = CertificatUtil.getCN(keyPair.getCertificate().getSubjectX500Principal());
			String signatureTexte = I18nUtil.get("SIGNATURE_PADES_MESSAGE_SIGNATAIRE") + " " + (cn != null ? cn : "");
			if (this.signatureDate != null) {
				signatureTexte += "\n";
				signatureTexte += I18nUtil.get("SIGNATURE_PADES_MESSAGE_DATE") + " " + this.signatureDate;
			}
			SignaturePades.signerFichierPdf(inputStreamSource, outputStreamDestination, certificat, privateKey, positionSignature, signatureMotif,
					signatureLocalisation, signatureTexte);
			LogManager.getInstance().afficherMessageInfo("Fin de la signature du fichier PDF", this.getClass());

			switch (mode) {

			case Distant:
				transfererFichierPdfSigne(fichierDestination, urlFichierDestination);
				appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.TRUE.toString(), null);
				fichierDestination.delete();
				break;
			case Disque:
				appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.TRUE.toString(), fichierDestination.getAbsolutePath());
				break;
			case Memoire:
				appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.TRUE.toString(),
						Base64.encodeFromFile(fichierDestination.getAbsolutePath()));
				fichierDestination.delete();
				break;
			}

		} catch (Throwable e) {
			e.printStackTrace();
			appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.FALSE.toString(), null);
			throw new SignatureExecutionException("Erreur lors de la signature Pades du fichier pdf", e);
		} finally {
			IOUtils.closeQuietly(inputStreamSource);
			IOUtils.closeQuietly(outputStreamDestination);
			appellerMethodeJavascript(methodeJavascriptFinAttente);
		}
	}

	/**
	 * Transforme un fichier source en flux (le fichier à signer)
	 * 
	 * @param urlFichierSource
	 *            l'url du fichier source (dépend du paramétre précédent)
	 * @return le flux vers le fichier source du fichier à signer
	 * @throws IOException
	 */
	private InputStream getInputStreamSourceDistant(String urlFichierSource) throws IOException {
		InputStream source = null;

		if (urlFichierSource != null) {
			URL url = new URL(urlFichierSource);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setDoInput(true);
			urlConnection.setUseCaches(false);
			urlConnection.setDefaultUseCaches(false);
			urlConnection.connect();

			source = urlConnection.getInputStream();
		}

		return source;
	}

	/**
	 * Transforme un fichier source en flux (le fichier signé)
	 * 
	 * @param temporaire
	 *            <code>true</code> si on est en mode envoi du fichier signé via
	 *            une url (le fichier source signé sera crée dans le repertoire
	 *            temporaire afin de pouvoir être ensuite transféré), sinon
	 *            <code>false</code> s'il s'agit d'un fichier qui doit être
	 *            chargé localement depuis le disque dur de l'utilisateur.
	 * @param cheminFichierSource
	 *            le chemin du fichier source à signer, afin de pouvoir créer la
	 *            version signé du fichier dans le même répertoire.
	 * @return le flux vers le fichier source du fichier signé
	 * @throws IOException
	 */
	private File getFichierDestination(boolean temporaire, String cheminFichierSource) throws IOException {

		if (temporaire) {
			File fichierTemporaire = File.createTempFile("fichier_pdf_applet_signature_pades", ".pdf");
			return fichierTemporaire;
		} else {
			String urlFichierDisque = cheminFichierSource.substring(0, cheminFichierSource.lastIndexOf('.')) + "_signaturePades.pdf";
			File fichierDestination = new File(urlFichierDisque);

			return fichierDestination;
		}
	}

	/**
	 * Permet de transférer le fichier signé vers une source distante
	 * (servlet,etc)
	 * 
	 * @param fichierSource
	 *            le fichier source à transférer
	 * @param urlFichierDestination
	 *            l'url sur laquelle envoyer le fichier
	 * @throws IOException
	 */
	private void transfererFichierPdfSigne(File fichierSource, String urlFichierDestination) throws IOException {

		LogManager.getInstance().afficherMessageInfo("Tentative de transfert du fichier pdf signé au serveur...", this.getClass());

		if (fichierSource != null && fichierSource.exists() && urlFichierDestination != null) {

			URL url = null;
			HttpURLConnection urlConnection = null;

			try {
				url = new URL(urlFichierDestination);
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoInput(true);
				urlConnection.setDoOutput(true);
				urlConnection.setUseCaches(false);
				urlConnection.setDefaultUseCaches(false);

				FileInputStream fileInputStream = new FileInputStream(fichierSource);
				int fileSize = fileInputStream.available();
				urlConnection.setFixedLengthStreamingMode(fileSize);
				urlConnection.connect();

				System.out.println("Transfert du fichier (" + fichierSource + ") : Taille estimé " + fileSize);

				int lus = 0;
				OutputStream outputStream = urlConnection.getOutputStream();
				byte[] buffer = new byte[1024];
				while ((lus = fileInputStream.read(buffer)) > -1) {
					outputStream.write(buffer, 0, lus);
					outputStream.flush();
				}

				outputStream.close();
				fileInputStream.close();
				InputStream inputStream = urlConnection.getInputStream();
				buffer = new byte[1024];
				System.out.println("Retour : ");
				while ((lus = inputStream.read(buffer)) > -1) {
					System.out.print(new String(buffer, 0, lus));
				}
				inputStream.close();
				urlConnection.disconnect();

			} finally {
				fichierSource.delete();
			}
		}
	}
}
