package fr.atexo.signature.applet.mpe.infos;

import fr.atexo.signature.commun.securite.processor.signature.ConteneurFichierSignatureXML;

import java.io.File;

/**
 *
 */
public class MpeFichier extends AbstractFichier implements ConteneurFichierSignatureXML {

    private File fichier;

    private File fichierSignatureXml;

    private boolean signatureObligatoire;

    public MpeFichier(int typeEnveloppe, int numeroLot, int index, String identifiantFichier, String typeFichier, String origineFichier, File fichier, File fichierSignatureXml, boolean signatureObligatoire) {
        super(typeEnveloppe, numeroLot, index, identifiantFichier, typeFichier, origineFichier);
        this.fichier = fichier;
        this.fichierSignatureXml = fichierSignatureXml;
        this.signatureObligatoire = signatureObligatoire;
    }

    public File getFichier() {
        return fichier;
    }

    public void setFichier(File fichier) {
        this.fichier = fichier;
    }

    public File getFichierSignatureXml() {
        return fichierSignatureXml;
    }

    public void setFichierSignatureXml(File fichierSignatureXml) {
        this.fichierSignatureXml = fichierSignatureXml;
    }

    public boolean isSignatureObligatoire() {
        return signatureObligatoire;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("MpeFichier");
        sb.append("{ typeEnveloppe=").append(typeEnveloppe);
        sb.append(", numeroLot=").append(numeroLot);
        sb.append(", index=").append(index);
        sb.append(", identifiantFichier='").append(identifiant);
        sb.append(", typeFichier='").append(typeFichier);
        sb.append(", origineFichier='").append(origineFichier);
        sb.append(", fichier=").append(fichier);
        sb.append(", fichierSignatureXml=").append(fichierSignatureXml);
        sb.append(", signatureVerifiee='").append(signatureVerifiee);
        sb.append(", signatureObligatoire='").append(signatureObligatoire);
        sb.append(" }");
        return sb.toString();
    }
}
