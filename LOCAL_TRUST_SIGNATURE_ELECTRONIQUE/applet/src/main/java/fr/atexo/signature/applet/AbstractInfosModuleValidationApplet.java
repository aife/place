package fr.atexo.signature.applet;

/**
 * Classe abstraite qui récupére l'ensemble des informations complémentaires qui doivent
 * être  envoyé lors des communications avec le module de validation de la signature XAdES.
 */
public abstract class AbstractInfosModuleValidationApplet extends AbstractCertificatApplet {

    private static final String PARAM_ORIGINE_PLATEFORME = "originePlateforme";
    private static final String PARAM_ORIGINE_ORGANISME = "origineOrganisme";
    private static final String PARAM_ORIGINE_ITEM = "origineItem";
    private static final String PARAM_ORIGINE_CONTEXT_METIER = "origineContexteMetier";

    /**
     * L'origine de la plateforme qui sera envoyé au module de validation.
     */
    protected String originePlateforme;

    /**
     * L'origine de l'organisme qui sera envoyé au module de validation.
     */
    protected String origineOrganisme;

    /**
     * L'origine de l'item qui sera envoyé au module de validation.
     */
    protected String origineItem;

    /**
     * L'origine du contexte métier qui sera envoyé au module de validation.
     */
    protected String origineContexteMetier;

    @Override
    public void init() {
        super.init();

        // paramètre originePlateforme
        setOriginePlateforme(getParameterAvecMessage(PARAM_ORIGINE_PLATEFORME));

        // paramètre origineOrganisme
        setOrigineOrganisme(getParameterAvecMessage(PARAM_ORIGINE_ORGANISME));

        // paramètre origineItem
        setOrigineItem(getParameterAvecMessage(PARAM_ORIGINE_ITEM));

        // paramètre origineContexteMetier
        setOrigineContexteMetier(getParameterAvecMessage(PARAM_ORIGINE_CONTEXT_METIER));
    }

    public void setOriginePlateforme(String originePlateforme) {
        this.originePlateforme = originePlateforme;
    }

    public void setOrigineOrganisme(String origineOrganisme) {
        this.origineOrganisme = origineOrganisme;
    }

    public void setOrigineItem(String origineItem) {
        this.origineItem = origineItem;
    }

    public void setOrigineContexteMetier(String origineContexteMetier) {
        this.origineContexteMetier = origineContexteMetier;
    }
}
