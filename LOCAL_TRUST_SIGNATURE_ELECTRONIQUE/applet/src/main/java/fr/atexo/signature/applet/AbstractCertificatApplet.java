package fr.atexo.signature.applet;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;
import fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.securite.provider.pkcs11.Pkcs11Handler;
import fr.atexo.signature.commun.securite.provider.pkcs12.Pkcs12Handler;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbPkcs11Util;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.gui.provider.magasin.MagasinCertificateUiService;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateEvent;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateListener;
import fr.atexo.signature.gui.provider.pkcs12.Pkcs12CertificateUiService;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateEvent;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateListener;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe abstraite permettant d'initialiser une applet devrant communiquer
 * avec les magasins de certificats du client.
 */
public abstract class AbstractCertificatApplet extends AbstractApplet implements MagasinCertificateListener, Pkcs12CertificateListener {

    protected static final String PARAM_URL_PKCS11_REF_XML = "urlPkcs11LibRefXml";

    protected static final String PARAM_MODE_RGS = "modeRestrictionRGS";

    /**
     * Les types de certificats utilisables
     */
    protected Set<CertificatUtil.TypeCertificat> typeCertificats = new HashSet<CertificatUtil.TypeCertificat>();

    /**
     * Le type de provider possible pour la récupération du certificat client.
     */
    protected TypeProvider provider;

    /*
     * L'url du fichier XML contenant la liste des libs pkcs11 supportés pour la lecture
     * du contenu des cartes à puce contenant des certificats.
     */
    protected URL urlPkcs11LibRefXml;

    /**
     * Spécifie si on se trouve en mode RGS
     */
    private boolean modeRestrictionRGS;

    @Override
    public void init() {
        super.init();

        // paramétre provider
        TypeProvider typeProvider = Util.determinerProvider();
        LogManager.getInstance().afficherMessageInfo("Provider déterminé automatiquement : " + typeProvider, this.getClass());
        setProvider(typeProvider);

        // paramètre urlPkcs11LibRefXml
        String url = getParameterAvecMessage(PARAM_URL_PKCS11_REF_XML);
        try {
            setUrlPkcs11LibRefXml(url);
            LogManager.getInstance().afficherMessageInfo("L'url du fichier xml contenant les lib des fabriquants implémentant le format pkcs11 : " + urlPkcs11LibRefXml, this.getClass());
        } catch (MalformedURLException e) {
            LogManager.getInstance().afficherMessageWarning("L'url du fichier xml contenant les lib des fabriquants implémentant le format pkcs11 n'est pas une url valide : " + url, this.getClass());
        }

        // paramétre RGS
        String modeRGS = getParameterAvecMessage(PARAM_MODE_RGS);
        if (modeRGS != null) {
            setModeRestrictionRGS(Boolean.valueOf(modeRGS));
        }
    }

    /**
     * Permet d'initialiser l'interface graphique de sélection du certificat en fonction
     * du provider sélectionné. Permet d'appeler la méthode abstraite onSelection(KeyPair keyPair) qui doit être
     * implémenté par les classes héritant de cette classe.
     */
    protected void selectionnerCertificat() {
        selectionnerCertificat(provider, getTypeCertificats());
    }

    /**
     * Permet d'initialiser l'interface graphique de sélection du certificat en fonction
     * du provider sélectionné. Permet d'appeler la méthode abstraite onSelection(KeyPair keyPair) qui doit être
     * implémenté par les classes héritant de cette classe.
     *
     * @param provider le type de provider.
     */
    protected void selectionnerCertificat(TypeProvider provider) {
        selectionnerCertificat(provider, getTypeCertificats());
    }

    /**
     * Permet d'initialiser l'interface graphique de sélection du certificat en fonction
     * du provider sélectionné. Permet d'appeler la méthode abstraite onSelection(KeyPair keyPair) qui doit être
     * implémenté par les classes héritant de cette classe.
     *
     * @param provider        le type de provider.
     * @param typeCertificats les types de certificats sur lesquels filtrer
     */
    protected void selectionnerCertificat(TypeProvider provider, CertificatUtil.TypeCertificat... typeCertificats) {

        LogManager.getInstance().afficherMessageInfo("Initialisation de la partie graphique de sélection du Certificat en fonction du type de provider", this.getClass());
        LogManager.getInstance().afficherMessageInfo("Le type d'os est : " + os, this.getClass());
        if (provider != null) {
            LogManager.getInstance().afficherMessageInfo("Le provider sélectionné est : " + provider, this.getClass());
            Pkcs11LibsType pkcs11LibsType = null;
            switch (provider) {
                case APPLE:
                case PKCS11:
                case MSCAPI:
                    try {
                        if (urlPkcs11LibRefXml != null && !Util.estVide(urlPkcs11LibRefXml.getProtocol()) && urlPkcs11LibRefXml.getProtocol().startsWith("http")) {
                            pkcs11LibsType = JaxbPkcs11Util.getPkcs11LibsType(urlPkcs11LibRefXml);
                        }
                    } catch (TransfertExecutionException e) {
                        LogManager.getInstance().afficherMessageWarning("Aucune librairie pkcs11 n'a été trouvé dans le descripteur xml se trouvant à l'url : " + urlPkcs11LibRefXml, this.getClass());
                    }
                    MagasinCertificateUiService.getInstance(pkcs11LibsType).initUi(this, os, provider, modeRestrictionRGS, typeCertificats);
                    break;
                case PKCS12:
                    Pkcs12CertificateUiService.getInstance().initUi(this, os, provider, modeRestrictionRGS);
                    break;
            }
        } else {
            LogManager.getInstance().afficherMessageInfo("Impossible de charger la partie graphique de sélection du Certificat car aucun provider n'a été défini", this.getClass());
        }
    }

    @Override
    public void onSelection(final MagasinCertificateEvent event) throws ManipulationCertificatException {
        final Class clazz = this.getClass();

        SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {

            public String doInBackground() {
                appellerMethodeJavascript(methodeJavascriptDebutAttente);

                LogManager.getInstance().afficherMessageInfo("onSelect => Provider d'accès au  Magasin : " + event.getCertificateItem().getTypeProvider(), clazz);
                String alias = event.getCertificateItem().getId();
                LogManager.getInstance().afficherMessageInfo("L'alias selectionné est : " + alias, clazz);
                boolean smartCard = event.getCertificateItem().isSmartCard();
                LogManager.getInstance().afficherMessageInfo("SmartCard : " + smartCard, clazz);


                KeyPair keyPair = null;
                try {

                    if (os != TypeOs.Windows && smartCard) {
                        keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
                    } else {
                        keyPair = MagasinHandler.getInstance().getKeyPair(provider, alias);
                    }

                } catch (RecuperationCertificatException e) {
                    LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                    lireLogs(clazz);
                    JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                }

                if (keyPair == null) {
                    JOptionPane.showMessageDialog(null, "Aucun certificat avec comme alias " + alias + " n'a été retrouvé dans le magasin", I18nUtil.get("INFORMATION_MESSAGE_ALERTE"), JOptionPane.WARNING_MESSAGE);
                } else {
                    LogManager.getInstance().afficherMessageInfo("Certificat sélectionné depuis le magasin : " + keyPair.getCertificate().getSubjectX500Principal().toString(), clazz);
                    try {
                        onSelection(keyPair);
                    } catch (Exception e) {
                        LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                        lireLogs(clazz);
                        JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                    }
                }

                return null;
            }

            protected void done() {
                try {
                    get();
                } catch (Throwable e) {
                    LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                    lireLogs(clazz);
                    JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        sw.execute();
    }

    @Override
    public void onSelection(final Pkcs12CertificateEvent event) throws ManipulationCertificatException {
        final Class clazz = this.getClass();

        SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {

            public String doInBackground() {
                appellerMethodeJavascript(methodeJavascriptDebutAttente);

                LogManager.getInstance().afficherMessageInfo("onSelect => Provider : " + provider, clazz);
                String cheminFichierP12 = event.getCheminFichierP12();
                String motDePasseFichierP12 = event.getMotDePasseFichierP12();
                LogManager.getInstance().afficherMessageInfo("Le fichier p12 selectionné est : " + cheminFichierP12, clazz);

                KeyPair keyPair = null;
                try {

                    keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);

                } catch (RecuperationCertificatException e) {
                    LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                    lireLogs(clazz);
                    JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                }

                if (keyPair != null) {
                    LogManager.getInstance().afficherMessageInfo("Certificat sélectionné : " + keyPair.getCertificate().getSubjectX500Principal().toString(), clazz);
                    try {
                        onSelection(keyPair);
                    } catch (Exception e) {
                        LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                        lireLogs(clazz);
                        JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                    }
                }

                return null;
            }

            protected void done() {
                try {
                    get();
                } catch (Throwable e) {
                    LogManager.getInstance().afficherMessageErreur(e.getMessage(), e, clazz);
                    lireLogs(clazz);
                    JOptionPane.showMessageDialog(null, e.getMessage(), I18nUtil.get("INFORMATION_MESSAGE_ERREUR"), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        sw.execute();
    }

    /**
     * Permet d'effectuer le traitement désiré avec le keyPair renvoyé par l'action
     * effectué par l'utilisateur. Cette méthode est automatique appelé lors que la méthode selectionnerCertificat(TypeProvider provider)
     * est elle même appelé.
     *
     * @param keyPair le keyPair contenant les informations du certificat sélectionné par l'utilisateur.
     */
    protected abstract void onSelection(KeyPair keyPair) throws ExecutionException;

    /**
     * Ensemble des paramètres qui seront accessible publiquement depuis
     * l'application qui incorporera l'applet de signature pades de fichier pdf.
     */
    private void setProvider(TypeProvider provider) {
        this.provider = provider;
    }

    public void setUrlPkcs11LibRefXml(String urlPkcs11LibRefXml) throws MalformedURLException {
        if (urlPkcs11LibRefXml != null) {
            this.urlPkcs11LibRefXml = new URL(urlPkcs11LibRefXml);
        } else {
            this.urlPkcs11LibRefXml = null;
        }
    }

    public void setModeRestrictionRGS(boolean modeRestrictionRGS) {
        this.modeRestrictionRGS = modeRestrictionRGS;
    }

    /**
     * Permettra de restreindre l'affichage de la liste des certificats disponibles uniquement à ceux utilisables
     * pour la signature electronique.
     */
    public void ajouterRestrictionTypeCertificatSignatureElectronique() {
        typeCertificats.add(CertificatUtil.TypeCertificat.SignatureElectronique);
    }

    /**
     * Permettra de restreindre l'affichage de la liste des certificats disponibles uniquement à ceux utilisables
     * pour l'authentification.
     */
    public void ajouterRestrictionTypeCertificatAuthentification() {
        typeCertificats.add(CertificatUtil.TypeCertificat.Authentification);
    }

    /**
     * Permettra de restreindre l'affichage de la liste des certificats disponibles uniquement à ceux utilisables
     * pour le chiffrement.
     */
    public void ajouterRestrictionTypeCertificatChiffrement() {
        typeCertificats.add(CertificatUtil.TypeCertificat.Chiffrement);
    }

    /**
     * Supprime les restrictions de filtre à effectuer sur les types de certificats
     */
    public void viderRestrictionTypeCertificat() {
        typeCertificats.clear();
    }

    protected CertificatUtil.TypeCertificat[] getTypeCertificats() {
        CertificatUtil.TypeCertificat[] types = typeCertificats.toArray(new CertificatUtil.TypeCertificat[typeCertificats.size()]);
        return types;
    }
}
