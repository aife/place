package fr.atexo.signature.applet;

import fr.atexo.signature.commun.util.Util;
import net.iharder.Base64;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

/**
 *
 */
public class SignatureMixteXadesPadesAppletExemple extends SignatureMixteXadesPadesApplet {

    @Override
    public void init() {
        super.init();

        try {
            setUrlPkcs11LibRefXml("http://localhost:8080/validation/pkcs11Lib/pcks11Libs.xml");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // signature xades
        initialiserSignatureXades();
        ajouterHashFichier("hashFichier1", "75FEFEBFCB1C89A9BEF19687B5D62554E3E0CB9E", "FichierPrincipal_signaturePades.pdf");

        // signature pades
        String contenuBase64FichierPdf = null;
        try {
            contenuBase64FichierPdf = Base64.encodeFromFile("/home/atexo/Bureau/chiffrement/FichierAssocie1.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
        initialiserSignaturePadesModeMemoire("fichierPdf", contenuBase64FichierPdf);
        setSignaturePadesInformation(10, 10, 400, 80, "Directeur Adjoint", Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_TIRET));

        // lancement
        executer();
    }
}
