package fr.atexo.signature.applet.mpe.infos;

/**
 *
 */
public abstract class AbstractMpeInfos {

    protected int typeEnveloppe;

    protected int numeroLot;

    protected int index;

    protected AbstractMpeInfos() {
    }

    protected AbstractMpeInfos(int typeEnveloppe, int numeroLot, int index) {
        this.typeEnveloppe = typeEnveloppe;
        this.numeroLot = numeroLot;
        this.index = index;
    }

    public int getTypeEnveloppe() {
        return typeEnveloppe;
    }

    public int getNumeroLot() {
        return numeroLot;
    }

    public int getIndex() {
        return index;
    }

    public String getTypeEnveloppeNumeroLot() {
        return typeEnveloppe + "_" + numeroLot;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("AbstractMpeInfos");
        sb.append("{ typeEnveloppe=").append(typeEnveloppe);
        sb.append(", numeroLot=").append(numeroLot);
        sb.append(", index=").append(index);
        sb.append(" }");
        return sb.toString();
    }
}
