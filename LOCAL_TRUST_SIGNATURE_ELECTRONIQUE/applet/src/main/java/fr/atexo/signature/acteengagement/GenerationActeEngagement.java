package fr.atexo.signature.acteengagement;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.atexo.pdfboxhelpers.BorderStyle;
import fr.atexo.pdfboxhelpers.Cell;
import fr.atexo.pdfboxhelpers.CellStyle;
import fr.atexo.pdfboxhelpers.CheckBox;
import fr.atexo.pdfboxhelpers.Padding;
import fr.atexo.pdfboxhelpers.PdfLayoutMgr;
import fr.atexo.pdfboxhelpers.ScaledJpeg;
import fr.atexo.pdfboxhelpers.TextStyle;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.util.TransfertUtil;

/**
 * Classe permettant de gérer un fichier d'acte d'engagement au format pdf en
 * utilisant pdfbox et en se basant sur le contenu d'un fichier XML.
 * <p/>
 * Reprise classe existante (recoder)
 */
public class GenerationActeEngagement {

	private static final int DOCUMENT_MARGIN_LEFT = 40;

	private static final int DOCUMENT_HEADER_MARGIN_BOTTOM = 40;

	private static final int BLOC_TITLE_MARGIN_BOTTOM = 7;

	private static final int BLOC_MARGIN_BOTTOM = 25;

	private static final String NODE_BLOC = "bloc";

	private static final String NODE_TITRE = "titre";

	private static final String NODE_SOUS_ELEMENT = "sousElement";

	private static final String NODE_ELEMENT = "element";

	private static final String ATTRIBUTE_TYPE = "type";

	private static final String ATTRIBUTE_HEADER = "header";

	private static final String ATTRIBUTE_H1 = "h1";

	private static final String ATTRIBUTE_H2 = "h2";

	private static final String ATTRIBUTE_H3 = "h3";

	private static final String ATTRIBUTE_BORDER = "border";

	private static final String ATTRIBUTE_COLS = "cols";

	private static final String ATTRIBUTE_MONTANT = "montant";

	private static final String ATTRIBUTE_LIBELLE = "libelle";

	private static final String ATTRIBUTE_VALEUR = "valeur";

	private static final String ATTRIBUTE_VALEUR22 = "valeur2";

	private static final String ATTRIBUTE_CHOISI = "choisi";

	private static final String SUFFIX_EURO = " €";

	private PdfLayoutMgr pageMgr = null;

	public GenerationActeEngagement() throws IOException {
		pageMgr = PdfLayoutMgr.newRgbPageMgr();
	}

	public void genererActeEngagement(org.w3c.dom.Document document, File fichierPdf, String urlLogo) throws ParserConfigurationException, SAXException,
			TransfertExecutionException, IOException, COSVisitorException {

		String libelle;
		String valeur1;
		String valeur2;

		float y = pageMgr.yPageTop();

		pageMgr.logicalPageStart();

		final float tableWidth = pageMgr.pageWidth() - (2 * DOCUMENT_MARGIN_LEFT);

		TextStyle regularFont = TextStyle.valueOf(PDType1Font.HELVETICA, 10.0f, Color.BLACK);

		TextStyle blocTitreFont = TextStyle.valueOf(PDType1Font.HELVETICA_BOLD, 11.0f, Color.BLACK);

		Padding padding = Padding.valueOf(5f, 3.5f, 4f, 3.5f);

		CellStyle regularCell = CellStyle.valueOf(CellStyle.HorizAlign.LEFT, padding, null, BorderStyle.builder().left(Color.GRAY, 1).right(Color.GRAY, 1)
				.bottom(Color.GRAY, 1).top(Color.GRAY, 1).build());

		CellStyle regularCellAlignRight = CellStyle.valueOf(CellStyle.HorizAlign.RIGHT, padding, null,
				BorderStyle.builder().left(Color.GRAY, 1).right(Color.GRAY, 1).bottom(Color.GRAY, 1).top(Color.GRAY, 1).build());

		CellStyle borderLessCell = CellStyle.valueOf(CellStyle.HorizAlign.LEFT, padding, null, BorderStyle.builder().build());

		NodeList titres = document.getElementsByTagName(NODE_TITRE);

		Node titreNode = titres.item(0);

		libelle = getNodeAttributeValue(titreNode, ATTRIBUTE_LIBELLE);

		float titColWidthPadding = tableWidth * 1.0f / 3.0f;
		float titColWidthText = tableWidth - titColWidthPadding;

		byte[] logoBytes = getLogo(urlLogo);

		ScaledJpeg scaledJpeg = null;

		if (logoBytes != null) {
			BufferedImage melonPic = ImageIO.read(new ByteArrayInputStream(logoBytes));
			scaledJpeg = ScaledJpeg.valueOf(melonPic);
		}

		// Titre document + Logo
		y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, titColWidthPadding, borderLessCell, scaledJpeg == null ? "" : scaledJpeg),
				Cell.valueOf(TextStyle.valueOf(PDType1Font.HELVETICA_BOLD, 24.0f, Color.BLACK), titColWidthText, borderLessCell, libelle));

		y -= DOCUMENT_HEADER_MARGIN_BOTTOM;

		NodeList listBlocs = document.getElementsByTagName(NODE_BLOC);

		for (int i = 0; i < listBlocs.getLength(); i++) {

			Node oneBloc = listBlocs.item(i);

			float colWidth = tableWidth / 3f;

			if (getNodeAttributeValue(oneBloc, ATTRIBUTE_TYPE).equals("signature")) {

				libelle = getNodeAttributeValue(oneBloc, ATTRIBUTE_LIBELLE);

				float sigColWidthPadding = tableWidth * 2.0f / 3.0f;
				float sigColWidthText = tableWidth - sigColWidthPadding;

				y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, sigColWidthPadding, borderLessCell, ""),
						Cell.valueOf(regularFont, sigColWidthText, borderLessCell, libelle));

				NodeList listElements = oneBloc.getChildNodes();
				String signataire = "";
				for (int j = 0; j < listElements.getLength(); j++) {
					Node oneElement = listElements.item(j);
					signataire += getNodeAttributeValue(oneElement, ATTRIBUTE_VALEUR) + " ";
				}

				y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, sigColWidthPadding, borderLessCell, ""),
						Cell.valueOf(regularFont, sigColWidthText, borderLessCell, signataire));

			} else {

				libelle = getNodeAttributeValue(oneBloc, ATTRIBUTE_LIBELLE);

				y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT - 5, y, Cell.valueOf(blocTitreFont, pageMgr.pageWidth(), borderLessCell, libelle));

				y -= BLOC_TITLE_MARGIN_BOTTOM;

				int colsCount = Integer.valueOf(getNodeAttributeValue(oneBloc, ATTRIBUTE_COLS));

				boolean hasBorder = Integer.valueOf(getNodeAttributeValue(oneBloc, ATTRIBUTE_BORDER)) > 0;

				boolean hasHeader = getNodeAttributeValue(oneBloc, ATTRIBUTE_HEADER).equals("true");

				NodeList listElements = oneBloc.getChildNodes();

				colWidth = tableWidth / colsCount;

				CellStyle cellStyle = hasBorder ? regularCell : borderLessCell;

				if (hasHeader) {

					String h1 = getNodeAttributeValue(oneBloc, ATTRIBUTE_H1);
					String h2 = getNodeAttributeValue(oneBloc, ATTRIBUTE_H2);
					String h3 = getNodeAttributeValue(oneBloc, ATTRIBUTE_H3);

					// header tableau
					y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, colWidth, cellStyle, h1),
							Cell.valueOf(regularFont, colWidth, cellStyle, h2), Cell.valueOf(regularFont, colWidth, cellStyle, h3));
				}

				for (int j = 0; j < listElements.getLength(); j++) {

					Node oneElement = listElements.item(j);

					if (oneElement.getNodeName().equals(NODE_ELEMENT) && !getNodeAttributeValue(oneElement, ATTRIBUTE_TYPE).equals("checkboxlist")) {

						List<Cell> cells = new ArrayList<Cell>();

						libelle = getNodeAttributeValue(oneElement, ATTRIBUTE_LIBELLE);

						valeur1 = getNodeAttributeValue(oneElement, ATTRIBUTE_VALEUR);

						// titre bloc
						cells.add(Cell.valueOf(regularFont, colWidth, cellStyle, libelle));

						if (getNodeAttributeValue(oneElement, ATTRIBUTE_TYPE).equals(ATTRIBUTE_MONTANT)) {

							valeur1 = adjustValueToColumn(getNodeAttributeValue(oneElement, ATTRIBUTE_VALEUR) + SUFFIX_EURO);
							cells.add(Cell.valueOf(regularFont, colWidth, regularCellAlignRight, valeur1));

						} else {

							cells.add(Cell.valueOf(regularFont, colWidth, cellStyle, valeur1));
						}

						if (colsCount == 3) {

							valeur2 = getNodeAttributeValue(oneElement, ATTRIBUTE_VALEUR22);

							if (getNodeAttributeValue(oneElement, ATTRIBUTE_TYPE).equals(ATTRIBUTE_MONTANT)) {
								valeur2 = adjustValueToColumn(getNodeAttributeValue(oneElement, ATTRIBUTE_VALEUR22) + SUFFIX_EURO);
								cells.add(Cell.valueOf(regularFont, colWidth, regularCellAlignRight, valeur2));
							} else {
								cells.add(Cell.valueOf(regularFont, colWidth, cellStyle, valeur2));
							}
						}

						Cell[] cellsAsArray = new Cell[cells.size()];

						y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, cells.toArray(cellsAsArray));

					} else if (oneElement.getNodeName().equals(NODE_ELEMENT) && getNodeAttributeValue(oneElement, ATTRIBUTE_TYPE).equals("checkboxlist")) {

						libelle = getNodeAttributeValue(oneElement, ATTRIBUTE_LIBELLE);

						y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, tableWidth, cellStyle, libelle));

						boolean aeCase1Checked = getEtatSousElmntById(oneElement, "aeCase1");

						y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, 50, cellStyle, new CheckBox("test1", aeCase1Checked)),
								Cell.valueOf(regularFont, tableWidth - 50, cellStyle, getlibelleSousElmntById(oneElement, "aeCase1")));

						boolean aeCase2Checked = getEtatSousElmntById(oneElement, "aeCase2");

						y = pageMgr.putRow(DOCUMENT_MARGIN_LEFT, y, Cell.valueOf(regularFont, 50, cellStyle, new CheckBox("test2", aeCase2Checked)),
								Cell.valueOf(regularFont, tableWidth - 50, cellStyle, getlibelleSousElmntById(oneElement, "aeCase2")));
					}
				}
			}

			y -= BLOC_MARGIN_BOTTOM;
		}

		pageMgr.logicalPageEnd();
		pageMgr.save(new FileOutputStream(fichierPdf));
	}

	public byte[] getLogo(String url) {
		if (!Util.estVide(url)) {
			try {
				byte[] logo = TransfertUtil.telecharger(url);
				if (logo != null && logo.length > 0) {
					return logo;
				} else {
					LogManager.getInstance().afficherMessageWarning("Impossible de récupérer le logo se trouvant à l'url : " + url,
							GenerationActeEngagement.class);
				}
			} catch (TransfertExecutionException e) {
				LogManager.getInstance().afficherMessageWarning("Impossible de récupérer le logo se trouvant à l'url : " + url, GenerationActeEngagement.class);
			}
		}

		return null;
	}

	private String getNodeAttributeValue(Node node, String attributeName) {
		NamedNodeMap attrs = node.getAttributes();
		if (attrs != null && attrs.getNamedItem(attributeName) != null) {
			return attrs.getNamedItem(attributeName).getNodeValue();
		} else {
			return "";
		}
	}

	private boolean getEtatSousElmntById(Node node, String name) {
		NodeList listElements = node.getChildNodes();
		for (int j = 0; j < listElements.getLength(); j++) {
			Node oneSsElement = listElements.item(j);
			if (oneSsElement.getNodeName().equals(NODE_SOUS_ELEMENT) && getNodeAttributeValue(oneSsElement, ATTRIBUTE_VALEUR).equals(name)) {
				return Boolean.parseBoolean(getNodeAttributeValue(oneSsElement, ATTRIBUTE_CHOISI));
			}
		}
		return false;
	}

	private String getlibelleSousElmntById(Node node, String name) {
		NodeList listElements = node.getChildNodes();
		for (int j = 0; j < listElements.getLength(); j++) {
			Node oneSsElement = listElements.item(j);
			if (oneSsElement.getNodeName().equals(NODE_SOUS_ELEMENT) && getNodeAttributeValue(oneSsElement, ATTRIBUTE_VALEUR).equals(name)) {
				return getNodeAttributeValue(oneSsElement, ATTRIBUTE_LIBELLE);
			}
		}
		return "";
	}

	private static String adjustValueToColumn(String string) {
		String phrs = "...................................................." + SUFFIX_EURO;
		int l = phrs.length();
		int lengthToAdd = l - string.length();
		while (lengthToAdd > 7) {
			string = " " + string;
			lengthToAdd--;
		}
		return string;
	}
}
