package fr.atexo.signature.applet;

import fr.atexo.signature.applet.bean.SignatureFichier;
import fr.atexo.signature.applet.bean.SignatureHashFichier;
import fr.atexo.signature.commun.exception.ParametrageException;
import fr.atexo.signature.commun.exception.execution.ExecutionException;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.logging.LoggerFactory;
import fr.atexo.signature.processor.StandardSignatureXadesProcessor;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe effectuant la signature Xades.
 * <p/>
 */
public class SignatureXadesApplet extends AbstractInfosModuleValidationApplet {

    private static final String NOM_LOGGER = "SignatureXadesApplet";
    private static final String PARAM_TYPE_HASH = "typeHash";

    protected static final String PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT = "methodeJavascriptFinTraitement";

    static {
        LoggerFactory.setNomLogger(NOM_LOGGER);
    }

    // algorithme de hash par defaut (sha1)
    private TypeAlgorithmHash typeHash = TypeAlgorithmHash.SHA1;

    private Map<String, SignatureFichier> fichiersEnAttentes = new HashMap<String, SignatureFichier>();

    private Map<String, SignatureHashFichier> empreintesEnAttentes = new HashMap<String, SignatureHashFichier>();

    private StandardSignatureXadesProcessor signatureXadesProcessor;

    /**
     * Liste des méthodes Javascript à appeler.
     */
    protected String methodeJavascriptFinTraitement;

    @Override
    public void init() {
        super.init();

        // paramètre typeHash
        String typeHash = getParameterAvecMessage(PARAM_TYPE_HASH);
        setTypeHash(typeHash);


        // paramètre methodeJavascriptFinTraitement
        setMethodeJavascriptFinTraitement(getParameterAvecMessage(PARAM_METHODE_JAVASCRIPT_FIN_TRAITEMENT));

        // appel de la méthode javascript de fin d'initialisation de l'applet
        appellerMethodeJavascript(methodeJavascriptFinAttente);

        LogManager.getInstance().afficherMessageInfo("--------------------- FIN INIT --------------------------", this.getClass());
    }

    @Override
    protected void initialiserGui() throws Throwable {

        LogManager.getInstance().afficherMessageInfo("Vérification de l'ensemble des paramétres", this.getClass());

        // vérification de la présente de fichiers à signer / chiffrer
        if (fichiersEnAttentes.isEmpty() && empreintesEnAttentes.isEmpty()) {
            throw new ParametrageException("Aucun fichier ou hash de fichier à signer n'est présent");
        }

        // vérification que pour la liste des fichiers que ceux-ci sont bien accessibles
        for (SignatureFichier signatureFichier : fichiersEnAttentes.values()) {

            boolean fichierPresent = signatureFichier != null && signatureFichier.getFichier().exists() && signatureFichier.getFichier().canRead();

            // le fichier à partir duquel générer une signature ou bien un chiffrement
            if (!fichierPresent) {
                throw new ParametrageException("Le fichier [" + signatureFichier.getFichier().getPath() + "] est soit inexistant ou bien inaccessible en lecture");
            }
        }
        // on propage les informations dans le cas où le module de validation serait utilisé
        signatureXadesProcessor.ajouterInformationsPourModuleValidation(originePlateforme, origineOrganisme, origineItem, origineContexteMetier);

        selectionnerCertificat();
    }

    @Override
    protected void onSelection(KeyPair keyPair) throws ExecutionException {
        signer(keyPair);
    }

    /**
     * Permet de signer.
     *
     * @param keyPairSignature la clef nécessaire à la signature
     */
    private void signer(KeyPair keyPairSignature) throws ExecutionException {

        try {

            // traitement des fichiers à signer
            if (!fichiersEnAttentes.isEmpty()) {

                for (String identifiant : fichiersEnAttentes.keySet()) {
                    SignatureFichier signatureFichier = fichiersEnAttentes.get(identifiant);
                    signerFichier(signatureFichier, keyPairSignature);
                }
            }

            // traitement des empreintes à signer
            if (!empreintesEnAttentes.isEmpty()) {

                for (String identifiant : empreintesEnAttentes.keySet()) {
                    SignatureHashFichier signatureHashFichier = empreintesEnAttentes.get(identifiant);
                    signerHashFichier(signatureHashFichier, keyPairSignature);
                }
            }

            // retour des signatures
            if (!fichiersEnAttentes.isEmpty()) {

                for (String identifiant : fichiersEnAttentes.keySet()) {
                    SignatureFichier signatureFichier = fichiersEnAttentes.get(identifiant);
                    appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.TRUE.toString(), signatureFichier.getFichierSignatureXml().getAbsolutePath());
                }
            }

            if (!empreintesEnAttentes.isEmpty()) {

                for (String identifiant : empreintesEnAttentes.keySet()) {
                    SignatureHashFichier signatureHashFichier = empreintesEnAttentes.get(identifiant);
                    appellerMethodeJavascript(methodeJavascriptRenvoiResultat, identifiant, Boolean.FALSE.toString(), signatureHashFichier.getContenuSignatureXml());
                }
            }

            fichiersEnAttentes.clear();
            empreintesEnAttentes.clear();

            appellerMethodeJavascript(methodeJavascriptFinTraitement);

        } finally {
            appellerMethodeJavascript(methodeJavascriptFinAttente);
        }

    }

    /**
     * Permet de signer ou bien encore de vérifier la conformité de la signature.
     *
     * @param signatureFichier le fichier contenant les informations sur le fichier à signé
     * @param keyPairSignature le certificat à utiliser pour effectuer la signature
     * @return la réponse associé à la signature du fichier
     * @throws fr.atexo.signature.commun.exception.execution.SignatureExecutionException
     *
     */
    private void signerFichier(SignatureFichier signatureFichier, KeyPair keyPairSignature) throws SignatureExecutionException {
        LogManager.getInstance().afficherMessageInfo("Signature du fichier [" + signatureFichier.getFichier().getAbsolutePath() + "]", this.getClass());
        signatureXadesProcessor.signerEnXades(keyPairSignature, signatureFichier);
    }

    /**
     * Permet de signer ou bien encore de vérifier la conformité de la signature.
     *
     * @param signatureHashFichier le fichier contenant les informations sur le hash du fichier à signé
     * @param keyPairSignature     le certificat à utiliser pour effectuer la signature
     * @return la réponse associé à la signature du fichier
     * @throws fr.atexo.signature.commun.exception.execution.SignatureExecutionException
     *
     */
    private void signerHashFichier(SignatureHashFichier signatureHashFichier, KeyPair keyPairSignature) throws SignatureExecutionException {
        LogManager.getInstance().afficherMessageInfo("Signature du hash du fichier [" + signatureHashFichier.getHashFichier() + "]", this.getClass());
        signatureXadesProcessor.signerEnXades(keyPairSignature, signatureHashFichier);
    }

    /**
     * Initialise l'applet
     *
     * @param urlModuleValidation url du module d'enrichissement de la signature si jamais la signature est requise
     */
    public void initialiser(String urlModuleValidation) {
        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Url module validation : " + urlModuleValidation, this.getClass());

        signatureXadesProcessor = new StandardSignatureXadesProcessor(urlModuleValidation, typeHash);

        // on vide les infos
        fichiersEnAttentes.clear();
        empreintesEnAttentes.clear();
    }

    /**
     * Ajoute un fichier dans la liste des fichiers qui devront être signés.
     *
     * @param identifiantFichier l'identifiant unique du fichier (pouvoir faire le lien avec le html lors du retour)
     * @param cheminAccesFichier le chemin d'accès du fichier à signer et/ou à chiffrer
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterFichier(String identifiantFichier, String cheminAccesFichier) {
        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Identifiant du fichier : " + identifiantFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Chemin du fichier : " + cheminAccesFichier, this.getClass());
        try {
            if (cheminAccesFichier != null) {
                SignatureFichier signatureFichier = new SignatureFichier(new File(cheminAccesFichier));
                fichiersEnAttentes.put(identifiantFichier, signatureFichier);
                return true;

            } else {
                LogManager.getInstance().afficherMessageWarning("Le fichier " + cheminAccesFichier + " n'est pas accessible", this.getClass());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Ajoute un hash de fichier dans la liste des hashs de fichiers qui devront être signés.
     *
     * @param identifiantHashFichier l'identifiant unique du hash du fichier (pouvoir faire le lien avec le html lors du retour)
     * @param hashFichier            le hash du fichier à signer et/ou à chiffrer
     * @param nomFichier             le nom du fichier
     * @return <code>true</code> si l'ajout à bien été pris en compte, sinon <code>false</false>
     */
    public boolean ajouterHashFichier(String identifiantHashFichier, String hashFichier, String nomFichier) {

        //recapitulatif des paramétres
        LogManager.getInstance().afficherMessageInfo("Identifiant du hash : " + identifiantHashFichier, this.getClass());
        LogManager.getInstance().afficherMessageInfo("Valeur du hash : " + hashFichier, this.getClass());
        try {
            empreintesEnAttentes.put(identifiantHashFichier, new SignatureHashFichier(nomFichier, hashFichier));
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void setTypeHash(String typeHash) {
        if (typeHash != null) {
            try {
                TypeAlgorithmHash typeAlgorithmHash = TypeAlgorithmHash.valueOf(typeHash);
                if (typeAlgorithmHash != null) {
                    this.typeHash = typeAlgorithmHash;
                }
            } catch (IllegalArgumentException e) {
                LogManager.getInstance().afficherMessageWarning("Le type de hash spécifié en paramétre n'est pas valide, du coup on utilise le type de hash par defaut : " + this.typeHash, this.getClass());
            }
        }
    }

    public void setTypeHash(TypeAlgorithmHash typeHash) {
        this.typeHash = typeHash;
    }

    public void setMethodeJavascriptFinTraitement(String methodeJavascriptFinTraitement) {
        this.methodeJavascriptFinTraitement = methodeJavascriptFinTraitement;
    }

}
