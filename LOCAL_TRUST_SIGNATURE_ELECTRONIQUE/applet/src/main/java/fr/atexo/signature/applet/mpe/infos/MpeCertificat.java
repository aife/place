package fr.atexo.signature.applet.mpe.infos;

import java.security.cert.X509Certificate;

/**
 *
 */
public class MpeCertificat {

    protected int typeEnveloppe;

    private X509Certificate certificat;

    public MpeCertificat(int typeEnveloppe, X509Certificate certificat) {
        this.typeEnveloppe = typeEnveloppe;
        this.certificat = certificat;
    }

    public int getTypeEnveloppe() {
        return typeEnveloppe;
    }

    public X509Certificate getCertificat() {
        return certificat;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("MpeCertificat");
        sb.append("{ typeEnveloppe=").append(typeEnveloppe);
        sb.append(" }");
        return sb.toString();
    }
}
