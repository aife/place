package fr.atexo.signature.applet.bean;

import fr.atexo.signature.commun.securite.processor.signature.ConteneurHashFichierSignatureXML;

/**
 * Classe représentant un hash de fichier.
 */
public class SignatureHashFichier implements ConteneurHashFichierSignatureXML {

    private String nomFichier;

    private String hashFichier;

    private String contenuSignatureXml;

    public SignatureHashFichier(String hashFichier) {
        this(null, hashFichier, null);
    }

    public SignatureHashFichier(String nomFichier, String hashFichier) {
        this(nomFichier, hashFichier, null);
    }

    public SignatureHashFichier(String nomFichier, String hashFichier, String contenuSignatureXml) {
        this.nomFichier = nomFichier;
        this.hashFichier = hashFichier;
        this.contenuSignatureXml = contenuSignatureXml;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public String getHashFichier() {
        return hashFichier;
    }

    public void setHashFichier(String hashFichier) {
        this.hashFichier = hashFichier;
    }

    public String getContenuSignatureXml() {
        return contenuSignatureXml;
    }

    public void setContenuSignatureXml(String contenuSignatureXml) {
        this.contenuSignatureXml = contenuSignatureXml;
    }
}
