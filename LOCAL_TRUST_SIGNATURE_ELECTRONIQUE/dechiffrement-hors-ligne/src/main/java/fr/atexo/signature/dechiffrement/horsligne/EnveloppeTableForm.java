/*
 * EnveloppeTableForm.java
 *
 * Created on __DATE__, __TIME__
 */

package fr.atexo.signature.dechiffrement.horsligne;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.iharder.Base64;

import org.apache.commons.io.FileUtils;

import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;

import fr.atexo.signature.commun.exception.execution.CertificatDechiffrementIntrouvableException;
import fr.atexo.signature.commun.exception.execution.DechiffrementExecutionException;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.signature.SignatureXades;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbReponsesAnnonceUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.XMLUtil;
import fr.atexo.signature.dechiffrement.horsligne.information.InfosEnveloppe;
import fr.atexo.signature.dechiffrement.horsligne.model.EnveloppeTableModel;
import fr.atexo.signature.gui.barreprogression.BarreDeProgression;
import fr.atexo.signature.processor.StandardChiffrementProcessor;
import fr.atexo.signature.util.AffichageUtil;
import fr.atexo.signature.util.FileNameCleaner;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;

public class EnveloppeTableForm extends JFrame {

	private final TypeOs typeOs;

	private final TypeProvider typeProvider;

	private final ReponseAnnonceType.Annonce annonce;
	private final List<InfosEnveloppe> enveloppes;

	private DechiffrementProcessor dechiffrementProcessor;

	private final File repertoire;

	private BarreDeProgression barreDeProgression;

	public EnveloppeTableForm(File repertoire, ReponseAnnonceType.Annonce annonce, List<InfosEnveloppe> enveloppes) {

		// os
		this.typeOs = Util.determinerOs();

		// paramétre provider
		this.typeProvider = Util.determinerProvider();

		initComponents();
		windowDimension();

		this.repertoire = repertoire;
		this.annonce = annonce;
		this.enveloppes = enveloppes;

		try {
			PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
			UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		setTitle(I18nUtil.get("LISTE_PLIS_CONSULTATION"));
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/signature.png")));
		setResizable(false);
		messageDechiffrerPlis.setEnabled(false);
		dechiffrerButton.setEnabled(false);
		enveloppeTable.setModel(new EnveloppeTableModel(enveloppes));
		enveloppeTable.repaint();
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		enveloppeTable = new javax.swing.JTable();
		messageDechiffrerPlis = new JTextArea();
		messageDechiffrerPlis.setEditable(false);
		messageDechiffrerPlis.setLineWrap(true);
		messageDechiffrerPlis.setOpaque(false);
		messageDechiffrerPlis.setBorder(BorderFactory.createEmptyBorder());
		messageDechiffrerPlis.setFont(UIManager.getFont("Label.font"));
		dechiffrerButton = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setBackground(new java.awt.Color(204, 255, 255));
		setFocusTraversalPolicy(null);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				closeEnveloppeTableForm(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(255, 255, 255));

		enveloppeTable.setForeground(new java.awt.Color(0, 51, 255));
		enveloppeTable.setModel(new javax.swing.table.DefaultTableModel(new Object[][] { { null, null, null, null } }, new String[] { I18nUtil.get("NUM_PLI"), I18nUtil.get("TYPE_PLI"), I18nUtil.get("DATE_DEPOT"), I18nUtil.get("RAISON_SOCIALE") }));
		enveloppeTable.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
		enveloppeTable.setGridColor(new java.awt.Color(204, 204, 204));
		enveloppeTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				enveloppeTableMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(enveloppeTable);

		messageDechiffrerPlis.setText(I18nUtil.get("MESSAGE_DECHIFFRER_PLIS"));

		dechiffrerButton.setText(I18nUtil.get("OUVRIR"));
		dechiffrerButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dechiffrerButtonActionPerformed(evt);
			}
		});

		org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
				.add(jPanel1Layout.createSequentialGroup().addContainerGap().add(messageDechiffrerPlis, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 778, Short.MAX_VALUE).addContainerGap())
				.add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup().addContainerGap(683, Short.MAX_VALUE).add(dechiffrerButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 105, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).addContainerGap()));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
				org.jdesktop.layout.GroupLayout.TRAILING,
				jPanel1Layout.createSequentialGroup().add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(messageDechiffrerPlis).addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(dechiffrerButton).addContainerGap()));

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
		// GEN-END:initComponents

	private void dechiffrerButtonActionPerformed(java.awt.event.ActionEvent evt) {
		int[] rows = enveloppeTable.getSelectedRows();
		List<InfosEnveloppe> enveloppesSelectionnees = new ArrayList<InfosEnveloppe>(rows.length);
		int nombreTotalFichiers = 0;
		for (int i : rows) {
			InfosEnveloppe infosEnveloppe = enveloppes.get(i);
			String nomRepertoireEnveloppe = I18nUtil.get("EL") + infosEnveloppe.getIndexReponseAnnonceType() + "_" + AffichageUtil.getTypeEnvNomRepertoire(infosEnveloppe.getEnveloppe().getType(), infosEnveloppe.getEnveloppe().getNumLot());
			infosEnveloppe.setNomRepertoireEnveloppe(nomRepertoireEnveloppe);
			enveloppesSelectionnees.add(infosEnveloppe);
			nombreTotalFichiers += infosEnveloppe.getEnveloppe().getFichier().size();
		}
		dechiffrerEnveloppes(nombreTotalFichiers, enveloppesSelectionnees.toArray(new InfosEnveloppe[enveloppesSelectionnees.size()]));
	}

	private void enveloppeTableMouseClicked(java.awt.event.MouseEvent evt) {

		if (evt.getClickCount() == 2) {
			Point p = evt.getPoint();
			int row = enveloppeTable.rowAtPoint(p);
			int column = enveloppeTable.convertColumnIndexToModel(enveloppeTable.columnAtPoint(p));
			int nombreTotalFichiers = 0;
			if (row >= 0 && column >= 0) {
				List<InfosEnveloppe> enveloppesSelectionnees = new ArrayList<InfosEnveloppe>(1);
				InfosEnveloppe infosEnveloppe = enveloppes.get(enveloppeTable.getSelectedRow());
				if (infosEnveloppe != null) {
					enveloppesSelectionnees.add(infosEnveloppe);
					nombreTotalFichiers += infosEnveloppe.getEnveloppe().getFichier().size();
				}

				dechiffrerEnveloppes(nombreTotalFichiers, enveloppesSelectionnees.toArray(new InfosEnveloppe[enveloppesSelectionnees.size()]));
			}
		}

		if (evt.getClickCount() == 1) {
			messageDechiffrerPlis.setEnabled(true);
			dechiffrerButton.setEnabled(true);
		}
	}

	private void dechiffrerEnveloppes(int nombreTotalFichiers, InfosEnveloppe... infosEnveloppes) {
		onSelection(nombreTotalFichiers, infosEnveloppes);
	}

	private String getMessageBarreProgressionFichier(JaxbReponsesAnnonceUtil.FichierBloc fichierBloc, int numeroFichierEnCours, int nombreTotalFichiers) {
		return "Fichier " + (numeroFichierEnCours) + "/" + nombreTotalFichiers + " - " + AffichageUtil.getMessageTypeEnveloppeEtNumeroLot(fichierBloc) + " : " + fichierBloc.getNomFichier() + " (" + AffichageUtil.getMessageTaille(fichierBloc.getTaille()) + ")";
	}

	private void onSelection(final int nombreTotalFichiers, final InfosEnveloppe... infosEnveloppes) {

		SwingWorker<String, Object> sw = new SwingWorker<String, Object>() {

			@Override
			public String doInBackground() {

				dechiffrementProcessor = new StandardChiffrementProcessor(EnveloppeTableForm.this.typeProvider, null);

				barreDeProgression = new BarreDeProgression("Barre de progression", nombreTotalFichiers);

				String nomFichiersDechiffres = "";

				try {

					int indexFichier = 1;

					for (InfosEnveloppe infosEnveloppe : infosEnveloppes) {

						List<JaxbReponsesAnnonceUtil.FichierBloc> blocs = JaxbReponsesAnnonceUtil.extraireFichierBlocChiffrements(annonce, infosEnveloppe.getEnveloppe(), null, 0);

						File repertoireEnveloppe = repertoire;

						if (infosEnveloppe.getNomRepertoireEnveloppe() != null) {
							String nomRepertoireOperateurEconomique = infosEnveloppe.getOperateurEconomique() != null ? infosEnveloppe.getOperateurEconomique().getNom() : null;
							if (nomRepertoireOperateurEconomique != null) {
								nomRepertoireOperateurEconomique = FileNameCleaner.cleanFileName(nomRepertoireOperateurEconomique);
								File repertoireOperateurEconomique = new File(repertoire, nomRepertoireOperateurEconomique);
								repertoireEnveloppe = new File(repertoireOperateurEconomique, infosEnveloppe.getNomRepertoireEnveloppe());
							} else {
								repertoireEnveloppe = new File(repertoire, infosEnveloppe.getNomRepertoireEnveloppe());
							}
						}
						nomFichiersDechiffres += "\n" + repertoireEnveloppe.getAbsolutePath();

						for (JaxbReponsesAnnonceUtil.FichierBloc fichierBloc : blocs) {

							File fichier = new File(repertoireEnveloppe, fichierBloc.getNomFichier());

							String messageTraitementFichier = getMessageBarreProgressionFichier(fichierBloc, indexFichier, nombreTotalFichiers);
							barreDeProgression.setMessageProgressionTraitementFichier(messageTraitementFichier);

							int indexBloc = 1;
							int nombreBlocs = fichierBloc.getBlocs().size();

							barreDeProgression.initialiserFichier(nombreBlocs);

							for (JaxbReponsesAnnonceUtil.FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {

								String id = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();

								String idBloc = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();
								String messageAvancementTraitementBloc = "Bloc " + indexBloc + "/" + nombreBlocs + " - ";

								File fichierPart = new File(repertoire, id);
								if (!fichierPart.exists()) {
									throw new FileNotFoundException("Impossible de trouver le fichier ayant comme nom " + id + " dans le répertoire " + repertoire.getAbsolutePath());
								}
								byte[] contenuBloc = Base64.decodeFromFile(fichierPart.getAbsolutePath());

								if (bloc.isChiffre()) {

									barreDeProgression.setMessageProgressionGlobale(messageAvancementTraitementBloc + I18nUtil.get("DECHIFFREMENT_EN_COURS"));
									contenuBloc = dechiffrementProcessor.dechiffrer(new String(contenuBloc));
									barreDeProgression.incrementerProgressionGlobale();

								}

								if (!repertoireEnveloppe.exists()) {
									repertoireEnveloppe.mkdirs();
								}
								FileUtils.writeByteArrayToFile(fichier, contenuBloc, true);
								barreDeProgression.incrementerProgressionGlobale();
								indexBloc++;
							}

							// création du fichier de signature XAdES
							String signatureXades = fichierBloc.getSignatureXades();
							if (!Util.estVide(signatureXades)) {
								signatureXades = new String(Base64.decode(signatureXades));
								String dateSignature = SignatureXades.extraireDate(SignatureXades.getSigningTime(signatureXades));
								String nomFichierSignature = XMLUtil.contruireCheminFichierSignatureXML(fichierBloc.getNomFichier(), dateSignature, 1);
								File fichierSignature = new File(repertoireEnveloppe, nomFichierSignature);
								FileUtils.writeByteArrayToFile(fichierSignature, signatureXades.getBytes());
							}

							indexFichier++;
							barreDeProgression.incrementerProgressionTraitementFichier();
							barreDeProgression.reinitialiserProgressionGlobale();

						}
					}

					// suppression des fichiers
					for (InfosEnveloppe infosEnveloppe : infosEnveloppes) {
						List<JaxbReponsesAnnonceUtil.FichierBloc> blocs = JaxbReponsesAnnonceUtil.extraireFichierBlocChiffrements(annonce, infosEnveloppe.getEnveloppe(), null, 0);
						for (JaxbReponsesAnnonceUtil.FichierBloc fichierBloc : blocs) {

							for (JaxbReponsesAnnonceUtil.FichierBloc.Bloc bloc : fichierBloc.getBlocs()) {
								String id = bloc.getId() != null ? bloc.getId().toString() : bloc.getNumero().toString();
								File fichierPart = new File(repertoire, id);
								if (fichierPart.exists()) {
									fichierPart.delete();
								}
							}
						}
					}

					barreDeProgression.dispose();
					JOptionPane.showMessageDialog(EnveloppeTableForm.this, I18nUtil.get("MSG_FIN_TRAITEMENT_SUCCES") + nomFichiersDechiffres);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					barreDeProgression.dispose();
					JOptionPane.showMessageDialog(EnveloppeTableForm.this, I18nUtil.get("ERREUR_PLI_INTROUVABLE_SUR_LE_POSTE"));
				} catch (IOException e) {
					e.printStackTrace();
					barreDeProgression.dispose();
				} catch (CertificatDechiffrementIntrouvableException e) {
					e.printStackTrace();
					barreDeProgression.dispose();
					JOptionPane.showMessageDialog(EnveloppeTableForm.this, I18nUtil.get("CLE_DECHIFFREMENT_INTROUVABLE"), I18nUtil.get("ERREUR"), JOptionPane.ERROR_MESSAGE);
				} catch (DechiffrementExecutionException e) {
					e.printStackTrace();
					barreDeProgression.dispose();
					JOptionPane.showMessageDialog(EnveloppeTableForm.this, I18nUtil.get("ERREUR_DEPOUILLEMENT"), I18nUtil.get("ERREUR"), JOptionPane.ERROR_MESSAGE);
				}

				return null;
			}

		};

		sw.execute();

	}

	private void closeEnveloppeTableForm(java.awt.event.WindowEvent evt) {
		this.dispose();
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton dechiffrerButton;
	private javax.swing.JTable enveloppeTable;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTextArea messageDechiffrerPlis;

	private void windowDimension() {
		Toolkit kit = this.getToolkit();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

		Dimension d = kit.getScreenSize();
		int max_width = (d.width - in.left - in.right);
		int max_height = (d.height - in.top - in.bottom);

		this.setSize(Math.min(max_width, 856), Math.min(max_height, 250));
		this.setLocation((max_width - this.getWidth()) / 2, (max_height - this.getHeight()) / 2);
	}
}