package fr.atexo.signature.dechiffrement.horsligne;

import java.io.PrintStream;
import java.util.Locale;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.commons.io.output.TeeOutputStream;

import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;

import fr.atexo.signature.commun.util.I18nUtil;

public class DechiffrementHorsLigne {

	public static final String MIN_REQUIRED_JAVA_VERSION = "1.6";

	public static CircularByteArrayOutputStream stdoutCopy = new CircularByteArrayOutputStream();

	public static void main(final String[] args) {

		TeeOutputStream teeOutputStream = new TeeOutputStream(System.out, stdoutCopy);
		TeeOutputStream teeErrOutputStream = new TeeOutputStream(System.err, stdoutCopy);

		System.setOut(new PrintStream(teeOutputStream));
		System.setErr(new PrintStream(teeErrOutputStream));

		Locale.setDefault(Locale.FRENCH);
		String javaVersion = System.getProperty("java.version");
		if (javaVersion.compareTo(MIN_REQUIRED_JAVA_VERSION) < 0) {
			JOptionPane.showMessageDialog(new JPanel(), I18nUtil.get("ERREUR_VERSION_JAVA"), I18nUtil.get("ERREUR"), JOptionPane.ERROR_MESSAGE);
		}
		try {
			PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
			UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				DecipherForm decipherForm = new DecipherForm();
				decipherForm.setVisible(true);
			}
		});
	}
}