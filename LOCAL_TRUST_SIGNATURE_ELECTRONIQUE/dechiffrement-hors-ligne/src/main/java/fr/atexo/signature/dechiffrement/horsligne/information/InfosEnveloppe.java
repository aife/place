package fr.atexo.signature.dechiffrement.horsligne.information;

import fr.atexo.signature.xml.annonce.ReponseAnnonceType;

/**
 *
 */
public class InfosEnveloppe implements Comparable<InfosEnveloppe> {

    private Integer indexReponseAnnonceType;

    private Integer idEnveloppe;

    private ReponseAnnonceType.Enveloppes.Enveloppe enveloppe;

    private ReponseAnnonceType.OperateurEconomique operateurEconomique;

    private String horodatageDepot;

    private String nomRepertoireEnveloppe;

    public InfosEnveloppe(Integer idEnveloppe, ReponseAnnonceType.Enveloppes.Enveloppe enveloppe, ReponseAnnonceType.OperateurEconomique operateurEconomique, String horodatageDepot) {
        this.idEnveloppe = idEnveloppe;
        this.enveloppe = enveloppe;
        this.operateurEconomique = operateurEconomique;
        this.horodatageDepot = horodatageDepot;
    }

    public Integer getIdEnveloppe() {
        return idEnveloppe;
    }

    public ReponseAnnonceType.Enveloppes.Enveloppe getEnveloppe() {
        return enveloppe;
    }

    public ReponseAnnonceType.OperateurEconomique getOperateurEconomique() {
        return operateurEconomique;
    }

    public String getHorodatageDepot() {
        return horodatageDepot;
    }

    public Integer getIndexReponseAnnonceType() {
        return indexReponseAnnonceType;
    }

    public void setIndexReponseAnnonceType(Integer indexReponseAnnonceType) {
        this.indexReponseAnnonceType = indexReponseAnnonceType;
    }

    public String getNomRepertoireEnveloppe() {
        return nomRepertoireEnveloppe;
    }

    public void setNomRepertoireEnveloppe(String nomRepertoireEnveloppe) {
        this.nomRepertoireEnveloppe = nomRepertoireEnveloppe;
    }

    @Override
    public int compareTo(InfosEnveloppe o) {
        if (o.idEnveloppe > idEnveloppe) return -1;
        if (o.idEnveloppe == idEnveloppe) return 0;
        return 1;
    }
}
