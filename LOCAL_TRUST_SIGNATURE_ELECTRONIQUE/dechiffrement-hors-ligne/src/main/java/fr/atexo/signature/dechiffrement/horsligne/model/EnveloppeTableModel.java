package fr.atexo.signature.dechiffrement.horsligne.model;

import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.dechiffrement.EnveloppeUtil;
import fr.atexo.signature.dechiffrement.horsligne.information.InfosEnveloppe;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 *
 */
public class EnveloppeTableModel extends AbstractTableModel {

    private List<InfosEnveloppe> enveloppes;

    private static final int NUM_PLI = 0;
    private static final int TYPE_PLI = 1;
    private static final int DATE_DEPOT = 2;
    private static final int RAISON_SOCIALE = 3;

    public static final String[] columnNames = {I18nUtil.get("NUM_PLI"), I18nUtil.get("TYPE_PLI"), I18nUtil.get("DATE_DEPOT"), I18nUtil.get("RAISON_SOCIALE")};

    public EnveloppeTableModel(List<InfosEnveloppe> enveloppes) {
        this.enveloppes = enveloppes;
    }

    public int getColumnCount() {
        return this.columnNames.length;
    }

    public int getRowCount() {
        return this.enveloppes.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int column) {
        InfosEnveloppe infosEnveloppe = enveloppes.get(row);
        switch (column) {
            case NUM_PLI:
                return I18nUtil.get("EL") + infosEnveloppe.getIndexReponseAnnonceType();
            case TYPE_PLI:
                return EnveloppeUtil.getTypeEnvLibelle(infosEnveloppe.getEnveloppe().getType(), infosEnveloppe.getEnveloppe().getNumLot());
            case DATE_DEPOT:
                return infosEnveloppe.getHorodatageDepot();
            case RAISON_SOCIALE:
                return infosEnveloppe.getOperateurEconomique() != null ? infosEnveloppe.getOperateurEconomique().getNom() : "";
        }
        return null;
    }

}