package fr.atexo.signature.dechiffrement.horsligne.model;

import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 *
 */
public class AnnonceTableModel extends AbstractTableModel {

    private List<ReponseAnnonceType.Annonce> annonces;

    private static final int REF_TYPE = 0;
    private static final int OBJET = 1;
    private static final int DATE_LIMITE = 2;

    public static final String[] columnNames = {I18nUtil.get("REF_TYPE"), I18nUtil.get("OBJET"), I18nUtil.get("DATE_LIMITE")};

    public AnnonceTableModel(List<ReponseAnnonceType.Annonce> annonces) {
        this.annonces = annonces;
    }

    private static final long serialVersionUID = 1L;

    public int getColumnCount() {
        return this.columnNames.length;
    }

    public int getRowCount() {
        return this.annonces.size();
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int column) {
        ReponseAnnonceType.Annonce annonce = annonces.get(row);
        switch (column) {
            case REF_TYPE:
                String referenceType = null;
                if (annonce.getTypeProcedure() != null && annonce.getTypeProcedure().length() > 0) {
                    referenceType = annonce.getReferenceUtilisateur() + " / " + annonce.getTypeProcedure();
                } else {
                    referenceType = annonce.getReferenceUtilisateur();
                }
                return referenceType;
            case OBJET:
                return annonce.getObjet();
            case DATE_LIMITE:
                return annonce.getDateLimite();

        }
        return null;
    }
}
