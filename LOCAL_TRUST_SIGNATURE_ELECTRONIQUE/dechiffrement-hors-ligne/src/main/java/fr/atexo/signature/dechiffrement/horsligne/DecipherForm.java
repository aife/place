/*
 * DecipherForm.java
 *
 * Created on __DATE__, __TIME__
 */

package fr.atexo.signature.dechiffrement.horsligne;

import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;
import com.jgoodies.looks.plastic.theme.ExperienceBlue;
import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbReponsesAnnonceUtil;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.dechiffrement.horsligne.information.InfosEnveloppe;
import fr.atexo.signature.dechiffrement.horsligne.model.AnnonceTableModel;
import fr.atexo.signature.gui.browser.SelectionRepertoire;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;
import fr.atexo.signature.xml.annonce.ReponsesAnnonce;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * @author __USER__
 */
public class DecipherForm extends JFrame {

    private ReponsesAnnonce reponsesAnnonce = null;
    private List<ReponseAnnonceType.Annonce> listeAnnonces = new ArrayList<ReponseAnnonceType.Annonce>();
    private Map<Integer, List<InfosEnveloppe>> mapEnveloppes = new HashMap<Integer, List<InfosEnveloppe>>();
    private File repertoire;

    /**
     * Creates new form DecipherForm
     */
    public DecipherForm() {
        super();
        initComponents();
        messageOuvrirAnnonce.setEnabled(false);
        ouvrirButton.setEnabled(false);
        setTitle(I18nUtil.get("OUTIL_DCHFFRMENT_HORS_LIGNE"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/signature.png")));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/quitter.gif")));
        logs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/logs.gif")));
        setResizable(false);
        try {
            PlasticLookAndFeel.setPlasticTheme(new ExperienceBlue());
            UIManager.setLookAndFeel(new PlasticXPLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        windowDimension();
    }

    //GEN-BEGIN:initComponents
    // <editor-fold defaultstate="collapsed" desc="Generated Code">

    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        cheminPlis = new javax.swing.JTextField();
        parcourirButton = new javax.swing.JButton();
        newFileDestination = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        annonceTable = new javax.swing.JTable();
        ouvrirButton = new javax.swing.JButton();
        messageOuvrirAnnonce = new javax.swing.JLabel();
        logs = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFocusTraversalPolicy(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11));
        jLabel1.setText(I18nUtil.get("FERMER"));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 24));
        jLabel11.setForeground(new java.awt.Color(0, 103, 153));
        jLabel11.setText(I18nUtil.get("OUTIL_DCHFFRMENT_HORS_LIGNE"));

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("SELECTIONNEZ_LE_REPERTOIRE_DESTINATION")));

        jLabel18.setText(I18nUtil.get("DESIGNER_REPERTOIRE_CONTENANT_PLIS"));

        jLabel20.setText(I18nUtil.get("CHEMIN_DIRECTORY"));

        cheminPlis.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);

        parcourirButton.setText(I18nUtil.get("PARCOURIR"));
        parcourirButton.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        parcourirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parcourirButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel13Layout = new org.jdesktop.layout.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(jPanel13Layout
                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel13Layout.createSequentialGroup().addContainerGap().add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 811, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(35, 35, 35))
                .add(jPanel13Layout.createSequentialGroup().add(76, 76, 76).add(newFileDestination, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE))
                .add(jPanel13Layout.createSequentialGroup().addContainerGap().add(jLabel20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 125, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED).add(cheminPlis, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 448, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(51, 51, 51).add(parcourirButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 105, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(113, Short.MAX_VALUE)));
        jPanel13Layout.setVerticalGroup(jPanel13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
                jPanel13Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(jLabel18)
                        .add(18, 18, 18)
                        .add(jPanel13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(cheminPlis, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(parcourirButton).add(jLabel20)).add(56, 56, 56)
                        .add(newFileDestination, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(I18nUtil.get("LISTE_ANNONCES")));

        annonceTable.setForeground(new java.awt.Color(0, 51, 255));
        annonceTable.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{{null, null, null}, {null, null, null}, {null, null, null}, {null, null, null}, {null, null, null}},
                new String[]{I18nUtil.get("REF_TYPE"), I18nUtil.get("OBJET"), I18nUtil.get("DATE_LIMITE")}));
        annonceTable.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        annonceTable.setGridColor(new java.awt.Color(204, 204, 204));
        annonceTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                annonceTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(annonceTable);

        ouvrirButton.setText(I18nUtil.get("OUVRIR"));
        ouvrirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ouvrirButtonActionPerformed(evt);
            }
        });

        messageOuvrirAnnonce.setText(I18nUtil.get("MESSAGE_DETAIL_ANNONCE"));

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
                jPanel8Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(jPanel8Layout
                                .createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jPanel8Layout
                                        .createSequentialGroup()
                                        .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(messageOuvrirAnnonce, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE)
                                                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE)).addContainerGap())
                                .add(org.jdesktop.layout.GroupLayout.TRAILING,
                                        jPanel8Layout.createSequentialGroup().add(ouvrirButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 105, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .add(97, 97, 97)))));
        jPanel8Layout.setVerticalGroup(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
                jPanel8Layout.createSequentialGroup().addContainerGap().add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 115, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED).add(messageOuvrirAnnonce)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(ouvrirButton)));

        logs.setFont(new java.awt.Font("Tahoma", 1, 11));
        logs.setText(I18nUtil.get("LOGS"));
        logs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logsMouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
                org.jdesktop.layout.GroupLayout.TRAILING,
                jPanel1Layout
                        .createSequentialGroup()
                        .addContainerGap()
                        .add(jPanel1Layout
                                .createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel13, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(org.jdesktop.layout.GroupLayout.LEADING,
                                        jPanel1Layout.createSequentialGroup().add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 698, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED).add(logs)
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).add(jLabel1)))
                        .addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(
                jPanel1Layout
                        .createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jPanel1Layout.createSequentialGroup().addContainerGap().add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE).add(jLabel1).add(logs)))
                                .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)).add(16, 16, 16)
                        .add(jPanel13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 103, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(32, 32, 32)
                        .add(jPanel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(45, Short.MAX_VALUE)));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
                org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE));

        pack();
    }// </editor-fold>
    //GEN-END:initComponents

    private void logsMouseClicked(java.awt.event.MouseEvent evt) {
        LogDetails logDetails = new LogDetails();
        logDetails.setVisible(true);
    }

    private void annonceTableMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getClickCount() == 1) {
            messageOuvrirAnnonce.setEnabled(true);
            ouvrirButton.setEnabled(true);
        }
    }

    private void parcourirButtonActionPerformed(java.awt.event.ActionEvent evt) {
        reponsesAnnonce = null;
        String cheminRepertoire = selectionnerRepertoire();
        cheminPlis.setText(cheminRepertoire);

        repertoire = new File(cheminRepertoire);
        File fichierReponseAnnonce = new File(repertoire, ReponseAnnonceConstantes.NOM_FICHIER_REPONSES_ANNONCE);
        if (fichierReponseAnnonce.exists() && fichierReponseAnnonce.isFile()) {
            try {
                reponsesAnnonce = getReponsesAnnonce(fichierReponseAnnonce);
            } catch (IOException e) {
                e.printStackTrace();
            }

            extraireInformations(reponsesAnnonce);
            AnnonceTableModel annonceTableModel = new AnnonceTableModel(listeAnnonces);
            annonceTable.setModel(annonceTableModel);
            messageOuvrirAnnonce.setEnabled(false);
            ouvrirButton.setEnabled(false);

        } else {
            JOptionPane.showMessageDialog(null, I18nUtil.get("AUCUNE_ANNONCE_TROUVE"), I18nUtil.get("SELECTIONNEZ_LE_REPERTOIRE_DESTINATION"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Affiche la liste des annonces
     *
     * @param reponsesAnnonce
     */
    private void extraireInformations(ReponsesAnnonce reponsesAnnonce) {

        Map<Integer, ReponseAnnonceType.Annonce> mapAnnonces = new HashMap<Integer, ReponseAnnonceType.Annonce>();
        mapAnnonces.clear();
        mapEnveloppes.clear();
        int indexReponseAnnonceType = 1;
        for (ReponseAnnonceType reponseAnnonceType : reponsesAnnonce.getReponseAnnonce()) {
            Integer idAnnonce = Integer.valueOf(reponseAnnonceType.getAnnonce().getReference());
            ReponseAnnonceType.Annonce annonce = mapAnnonces.get(idAnnonce);
            if (annonce == null) {
                mapAnnonces.put(idAnnonce, reponseAnnonceType.getAnnonce());
            }

            for (ReponseAnnonceType.Enveloppes.Enveloppe enveloppe : reponseAnnonceType.getEnveloppes().getEnveloppe()) {
                List<InfosEnveloppe> enveloppes = mapEnveloppes.get(idAnnonce);
                if (enveloppes == null) {
                    enveloppes = new ArrayList<InfosEnveloppe>();
                    mapEnveloppes.put(idAnnonce, enveloppes);
                }
                InfosEnveloppe infosEnveloppe = new InfosEnveloppe(enveloppe.getIdEnveloppe(), enveloppe, reponseAnnonceType.getOperateurEconomique(), reponseAnnonceType.getHorodatageDepot());
                infosEnveloppe.setIndexReponseAnnonceType(indexReponseAnnonceType);
                enveloppes.add(infosEnveloppe);
            }

            indexReponseAnnonceType++;
        }

        listeAnnonces = Arrays.asList(mapAnnonces.values().toArray(new ReponseAnnonceType.Annonce[mapAnnonces.size()]));
    }


    private void ouvrirButtonActionPerformed(java.awt.event.ActionEvent evt) {

        // on récupére l'annonce sélectionnée
        ReponseAnnonceType.Annonce annonceSelectionne = listeAnnonces.get(annonceTable.getSelectedRow());
        List<InfosEnveloppe> enveloppes = mapEnveloppes.get(Integer.valueOf(annonceSelectionne.getReference()));
        Collections.sort(enveloppes);
        EnveloppeTableForm enveloppeTableFrm = new EnveloppeTableForm(repertoire, annonceSelectionne, enveloppes);
        enveloppeTableFrm.setVisible(true);
    }

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {
        System.exit(0);
    }

    //GEN-BEGIN:variables
    // Variables declaration - do not modify
    private javax.swing.JTable annonceTable;
    private javax.swing.JTextField cheminPlis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel logs;
    private javax.swing.JLabel messageOuvrirAnnonce;
    private javax.swing.JLabel newFileDestination;
    private javax.swing.JButton ouvrirButton;
    private javax.swing.JButton parcourirButton;

    // End of variables declaration//GEN-END:variables

    private void windowDimension() {
        Toolkit kit = this.getToolkit();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

        Dimension d = kit.getScreenSize();
        int max_width = (d.width - in.left - in.right);
        int max_height = (d.height - in.top - in.bottom);

        this.setSize(Math.min(max_width, 892), Math.min(max_height, 460));
        this.setLocation((int) (max_width - this.getWidth()) / 2, (int) (max_height - this.getHeight()) / 2);
    }

    private String selectionnerRepertoire() {
        SelectionRepertoire selectionRepertoire = new SelectionRepertoire();
        if (selectionRepertoire.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            if (selectionRepertoire.getSelectedFile().getAbsolutePath().length() != 0) {
                return selectionRepertoire.getSelectedFile().getAbsolutePath();
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    private ReponsesAnnonce getReponsesAnnonce(File fichierReponseAnnonce) throws IOException {
        String contenuReponsesAnnonceXML = FileUtils.readFileToString(fichierReponseAnnonce, FileUtil.ENCODING_ISO_8859_1);
        System.out.println(contenuReponsesAnnonceXML);
        ReponsesAnnonce reponsesAnnonce = JaxbReponsesAnnonceUtil.getReponsesAnnonce(contenuReponsesAnnonceXML, FileUtil.ENCODING_ISO_8859_1);
        return reponsesAnnonce;
    }
}