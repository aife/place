package fr.atexo.signature.dechiffrement;

import fr.atexo.signature.commun.util.I18nUtil;

/**
 *
 */
public abstract class EnveloppeUtil {

    public static String getTypeEnvLibelle(int typeEnveloppe, int numeroLot) {
        if (typeEnveloppe == 1) {
            return I18nUtil.get("ENVELOPPE_CANDIDATURE");
        } else if (typeEnveloppe == 2 && numeroLot == 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE");
        } else if (typeEnveloppe == 2 && numeroLot > 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE") + " " + I18nUtil.get("LOT") + " " + numeroLot;
        } else if (typeEnveloppe == 3 && numeroLot == 0) {
            return I18nUtil.get("ENVELOPPE_ANONYMAT");
        } else if (typeEnveloppe == 3 && numeroLot > 0) {
            return I18nUtil.get("ENVELOPPE_ANONYMAT") + " " + I18nUtil.get("LOT") + " " + numeroLot;
        } else if (typeEnveloppe == 4 && numeroLot == 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE_TECHNIQUE");
        } else if (typeEnveloppe == 4 && numeroLot > 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE_TECHNIQUE") + " " + I18nUtil.get("LOT") + " " + numeroLot;
        }
        return "";
    }
}
