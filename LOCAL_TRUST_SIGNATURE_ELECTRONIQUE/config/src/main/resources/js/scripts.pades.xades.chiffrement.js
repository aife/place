function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs() {
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant) {
    var extensions = new Array('PDF', 'DOC', 'ZIP', 'XLS', 'PPT');
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier à signer
 */
function selectionEffectuee(identifiant, cheminFichierSelectionne) {
    document.getElementById(identifiant).value = cheminFichierSelectionne;
}

/**
 * Initialise de l'applet de signature Pades/Xades et de chiffrement.
 *
 * @param chiffrementRequis <code>true</code> si le chiffrement est requis ,sinon <code>false</code>
 * @param transfertRequis   <code>true</code> si le transfert est requis ,sinon <code>false</code>
 * @param signatureRequise  <code>true</code> si la signature Xades est requise ,sinon <code>false</code>
 * @param signaturePadesActeEngagement  <code>true</code> si la signature Pades du fichier d'acte d'engagement est requis ,sinon <code>false</code>
 * @param effectuerVerificationSignature  <code>true</code> si la verification de la signature est requis (uniquement si le fichier de signature Xades est lui aussi passé en paramètre)
 * @param afficherBarreProgression <code>true</code> si l'on veut afficher la barre de progression, sinon <code>false</code>
 * de la fonction d'ajout de fichiers),sinon <code>false</code>
 */
function initialiserApplet(chiffrementRequis, transfertRequis, signatureRequise, signaturePadesActeEngagement, effectuerVerificationSignature, afficherBarreProgression) {
    var urlPkcs11 = getUrlPcks11Libs();
    document.MpeChiffrementApplet.setUrlPkcs11LibRefXml(urlPkcs11);
    //si l'on désire pas de transfert alors ne pas mettre l'url d'envoi
    var urlServeurEnvoiBloc = null;
    if (transfertRequis) {
        urlServeurEnvoiBloc = getContextRoot() + "envoiBlocChiffre";
    }
    var uid = document.getElementById('uid').value;
    var urlModuleValidation = getContextRoot() + "signatureXades";
    document.MpeChiffrementApplet.initialiser(chiffrementRequis, urlServeurEnvoiBloc, uid, signatureRequise, signaturePadesActeEngagement, effectuerVerificationSignature, urlModuleValidation, afficherBarreProgression);
    var cheminFichier1 = document.getElementById('cheminFichier1').value;
    var fichier1GenererXades = $('input[name=fichier1GenererXades]:checked').val();
    var generationXadesNecessaire = 1;
    if (fichier1GenererXades == '0') {
        generationXadesNecessaire = 0;
    }
    if (!isEmpty(cheminFichier1)) {
        ajouterFichier(cheminFichier1, 1, 1, 0, 'cheminFichierFinal1', "ACE", "FORMULAIRE", null, generationXadesNecessaire);
        if (chiffrementRequis) {
            ajouterCertificat(1);
        }
    }
    document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.MpeChiffrementApplet.executer();
}

function ajouterFichier(cheminFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire) {
    document.MpeChiffrementApplet.ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire);
}

function ajouterCertificat(typeEnveloppe) {
    var certificatPCAO = "-----BEGIN CERTIFICATE-----MIIFbjCCA1agAwIBAgIBATANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJGUjEOMAwGA1UEBxMFUEFSSVMxDjAMBgNVBAoTBUFURVhPMRYwFAYDVQQLEw1BVEVYTyBBQyBERU1PMR8wHQYDVQQDExZBVEVYTyBBQyBERU1PIC0gQWdlbnRzMB4XDTEzMDQxNzE5MTY0OFoXDTE2MDQxNzE5MTY0OFowgagxCzAJBgNVBAYTAkZSMQ4wDAYDVQQKEwVBVEVYTzEmMCQGA1UECxMdTUFSQ0hFUyBQVUJMSUNTIEVMRUNUUk9OSVFVRVMxIDAeBgNVBAMTFyhQQ0FPKSBQUkVTSURFTlQgREUgQ0FPMSAwHgYDVQQpExcoUENBTykgUFJFU0lERU5UIERFIENBTzEdMBsGCSqGSIb3DQEJARYOcGNhb0BhdGV4by5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpV8NFneO/dI1HRRBozv2iIpTIfZBVskFlJoZFYjlAYg74EGuLoxvbE842bOqSp4wqxUFOG4b2zye/1eqXMxRf54jMpVf0crZgN7LmC2wTnyd6GXrzcf7NbVcAEPxWf8GV8UmmWssBD9rT6SsOY24w1Zv0Wg+Z/+Yt+tc1U8e79xqkugyfzj0SYvCvADeROGbEpgULeY00QM1K9L+48OxDGOuD0zAhpKdVGVkPeyd33tF/nUnFshOt9uPWvvHvQcYZ+wyXstaheTRomVjRjDTkRqet/JhS3kW0e00WWBUoPi+wf88C4UNXyj0ZJEkmr/hZyGFLoR+/DBkdZ0z6QpOjAgMBAAGjgeMwgeAwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBBAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMB0GA1UdDgQWBBSg051aEBuWmUCmFT7Ra0ShDahwxTAfBgNVHSMEGDAWgBQNrBY+l1uNs7RzZ/f1tmeiolNNMzBJBgNVHR8EQjBAMD6gPKA6hjhodHRwczovL3BraS5sb2NhbC10cnVzdC5jb20vY3JsL0FURVhPX0FDX0RFTU8tQWdlbnRzLmNybDAZBgNVHREEEjAQgQ5wY2FvQGF0ZXhvLmNvbTANBgkqhkiG9w0BAQsFAAOCAgEAQSNAe9StiAtEHw1GVaHP9ByLluUPxI3auoe7jN73ZDyTz+rVrC9htln88dLouq5l1BlYnK2jA7BvGvGNF+cvQGKb4zyHiPpukkwAJR4+xmxnU2lYmtQg+5C4fkOiT9JCz4t+5GBwW4/xgDl+2CCadhf8wdI9IzdByxV8H2sDJmlUvs4Gii3WnGqqLT3wIR6k58N9r8cS4cfzKZ49a8BdtsRFDuVrPWf1lNGefCeWc5U2XLMpm5GgW0o1c1J/kJ3Shpnzbz3X+oei0YKEkayB6/SG736SfBZ+utUcSV9kPFM4p42zVl/38OgTUy0ThFDfOAt+RhfeU2/WTUOaZkaJVpSOoF1kE+KLFjYhGI2SwkXZC/0U8c4ZWqJOjFVVfF3lBoMb4ugWWBc7iOPfOL4npDzSSulnZILbJWma0MK5L3kYSBPMBzZeNPIIC7DojEHVe9sM1SbQSavGgXeQ1UCOWBx8vajvPeVCASnKKyZgnlHT66drEpLWLuM1ev379lU2yOT0/EQjIq2ryl3cdFLOiBLYTVr2XzkbK8yRESZDR9MHjtwmALm0idqUuPTcsgOMpRRDHWDZqFOJ8rgf6VdbH+aFPO8InY2V1uE/NDo29KnLKLV42qM7Tgzrndmf6rm0qhjlB3YRGGwFWby1gxKDzTWwQYQ9qWPII+RVm43g56g=-----END CERTIFICATE-----";
    document.MpeChiffrementApplet.ajouterCertificat(typeEnveloppe, certificatPCAO);
    var certificatSCAO = "-----BEGIN CERTIFICATE-----MIIFbjCCA1agAwIBAgIBAjANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJGUjEOMAwGA1UEBxMFUEFSSVMxDjAMBgNVBAoTBUFURVhPMRYwFAYDVQQLEw1BVEVYTyBBQyBERU1PMR8wHQYDVQQDExZBVEVYTyBBQyBERU1PIC0gQWdlbnRzMB4XDTEzMDQxNzE5MTY1MloXDTE2MDQxNzE5MTY1MlowgagxCzAJBgNVBAYTAkZSMQ4wDAYDVQQKEwVBVEVYTzEmMCQGA1UECxMdTUFSQ0hFUyBQVUJMSUNTIEVMRUNUUk9OSVFVRVMxIDAeBgNVBAMTFyhTQ0FPKSBTVVBQTEVBTlQgREUgQ0FPMSAwHgYDVQQpExcoU0NBTykgU1VQUExFQU5UIERFIENBTzEdMBsGCSqGSIb3DQEJARYOc2Nhb0BhdGV4by5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCnU21YLNdp57S+UciWgicOuHJRsJUEPIM6BmxkZACPGEVG03RMVRFEeda58xLDhZMs4PmpXOAue8GamXduvausRd+NUTYrwiJH2ZNOPJ1/ZY116ozqykzdBL7h8ZCGQUrilt6qztnAaSmuj1/ANWHznwMOcve1omr0ApmFtLINHY2GCLk4GJHdRndjH3KlCkPmiEsiA0gEApdvh1OXCoWYaq3zUBphOEL9zOvprX0rVci70MogSzys9IcmkUJIjw7K4Sm4QfS/RTcEI73UwtR8c3IpnrKkQna6cDkHrPKQA7YMvLyXKhCHBJhrgRcqZhGFvjM8r6cC2QltE3lyFvarAgMBAAGjgeMwgeAwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBBAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMB0GA1UdDgQWBBSCDUErjwrPA9lblG6r0uEWQ4XcJzAfBgNVHSMEGDAWgBQNrBY+l1uNs7RzZ/f1tmeiolNNMzBJBgNVHR8EQjBAMD6gPKA6hjhodHRwczovL3BraS5sb2NhbC10cnVzdC5jb20vY3JsL0FURVhPX0FDX0RFTU8tQWdlbnRzLmNybDAZBgNVHREEEjAQgQ5zY2FvQGF0ZXhvLmNvbTANBgkqhkiG9w0BAQsFAAOCAgEANyDvL0JNLjG4yY4hI+gKjYTVbqTbVqEzRjsidHfCi+AeuhUZXIRVBk1U0eSHqDglYoizBxRTaGq2RCIWT467jp/5Rsjhh+/wp1BGE/tjgPGdYnkzsESnaS/bwnTfEmFCdA1oX55j1e/7IbkBe4CFC5A6e+dVtAI2iBWEnOy9jWOXZVZLyfDpYib7nW7y+fIiXf9OdSmpMBnCQxVTedOv+0b72TkTmHQRSrLpa0I6TStbf5XiusJ7O24D4hYBylIHQOfBjKf5kL0qH1JpHD5CwndUopYUsh5Y9EyKDAw3FG3J0voOTaSU5vGm5JpB1DJawhaRlNIyPhPg5oSV7+IIrsUHpatKPCry6eBUrejAOtY/yPrt6knoN32HDyH+W4vbAf5qvQD+kLODzKpHtZTYjpzCdqvXaMKjt37+ph8WKU+1iemqPqGShO5/iWiVmeJBKZ+LBQ7utCOtEMgOeRqjBvXeUd297t7ZMtyLknHWFZ0h0R3BkDknQIgkt0sN/jPiyEWnX/pNqeT0ZNwUid/ItRDqk8LvIyLX1UQceMJvBmtmIKZPP3v2iPyyCZsqHZObJJKNlfheIBPjGIJgosPC50cuya8INJhtzCUUOmDYzBKTxwU0QqIy+t0PH6qWsvXydQ4hOSZfiY+zatHxVQ8wthEXFd/hUGJD3lc+zUi22Ac=-----END CERTIFICATE-----";
    document.MpeChiffrementApplet.ajouterCertificat(typeEnveloppe, certificatSCAO);
    var certificatSecours = "-----BEGIN CERTIFICATE-----MIIFZjCCA06gAwIBAgIBAzANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJGUjEOMAwGA1UEBxMFUEFSSVMxDjAMBgNVBAoTBUFURVhPMRYwFAYDVQQLEw1BVEVYTyBBQyBERU1PMR8wHQYDVQQDExZBVEVYTyBBQyBERU1PIC0gQWdlbnRzMB4XDTEzMDQxNzE5MTY1MloXDTE2MDQxNzE5MTY1MlowgaExCzAJBgNVBAYTAkZSMQ4wDAYDVQQKEwVBVEVYTzEmMCQGA1UECxMdTUFSQ0hFUyBQVUJMSUNTIEVMRUNUUk9OSVFVRVMxHTAbBgNVBAMTFChDRFMpIENMRSBERSBTRUNPVVJTMR0wGwYDVQQpExQoQ0RTKSBDTEUgREUgU0VDT1VSUzEcMBoGCSqGSIb3DQEJARYNY2RzQGF0ZXhvLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMsuNQ6IcqwQM5dLXfFUHYTo7mgKGis2LLHTu/aIDfizRAuceFWibgGahteN8P74Ntp02zP5yE27KXDYSZMEfIhBo4zymgN2tRqel6wfWNhUhJYNuNpzlNsluow0a9UK8q1ulbrYJjjxZIeTiZA/g0urKY69Q78HOq+RYjc3ISnLgTamsKaAjjpXwAR+YyLzMOMbzqey7dBJKnKckv0k8hE0k4XNQ1LxPZYGAgj8LzzTo3uAPQ3kUCxTC974tfqlF+kTwLZ1ZFPiZBC41AldjvG912lVH7sLK0gBSzKvnuRT7Y8KcwE3EbF2SqffTSu6C/HVRHI5Xlm2su207wr6QZUCAwEAAaOB4jCB3zAJBgNVHRMEAjAAMA4GA1UdDwEB/wQEAwIEEDAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwHQYDVR0OBBYEFLzkkBKlvMmPeIyZ5fIIz2IB55drMB8GA1UdIwQYMBaAFA2sFj6XW42ztHNn9/W2Z6KiU00zMEkGA1UdHwRCMEAwPqA8oDqGOGh0dHBzOi8vcGtpLmxvY2FsLXRydXN0LmNvbS9jcmwvQVRFWE9fQUNfREVNTy1BZ2VudHMuY3JsMBgGA1UdEQQRMA+BDWNkc0BhdGV4by5jb20wDQYJKoZIhvcNAQELBQADggIBAIaFU7t4OXtErG0fq/C4q0mCrjdQzE+uiAZUNanHXh9tsQh0AUXEJK3sqaH27HDDGBKj8K0jGecgGRYQdIO9gszRHxcnFdIIJRuyb75ONqtttU+gytpuVbBfbaF2Ze4iw5jKc9fVM0DhDpGqobnVADewjzlnwls6wuz9Wrarmn5AjhHpbtrzvEca6STI5W3ead5SvB/+zvgGAoaAI3f2Ebm3oGxQn6gJURRsCz5iLPve1YUFIVluDNLohGVLLlvVLDy/sItVR5LgbOCFajt0Y6+OOOEHJBR9shvgFsGzXv4vvzlPIP8y4iONU8FjGltRzkWRw9XiOlOqQWQBbXoULUNZ1tuC4SJxnPVflHNZvxHe1oUwSoHX0oiEEPV7qumSy9LS0yn2j26IcBd1w8fUhyD2Bhv3Ln2MmunGbWQlkG1cRgDEtbOiWAgVN4X3aUFvnJSrnsjVqld1QTQZNjTP8lJkP6lJhAJIczgTiQuURKIr9/jAySRWMJ7HYEGJ3bO5sVNnD/Vqr43lZxEVbauqAKk50TxHlomsLA4rJiPfEvmy4AfW/rLClimxb8JATwLJaq3eD86jcc6fgOrs3ig+WbQTt7AW2++/AcaY6GKWQHDJhj+d9yvaSUOESBAm2m3A+ERHhz9prI/g9V+tErhdSZ9S5+K4Xxq9CmQYKykMkI9T-----END CERTIFICATE-----";
    document.MpeChiffrementApplet.ajouterCertificat(typeEnveloppe, certificatSecours);

}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param reponseJson le contenu de la reponse au format json
 *
 */
function renvoiResultat(reponseJson) {
    try {
        var reponse = jQuery.parseJSON(reponseJson);

        var elementReponseJson = document.getElementById('reponseJson');
        elementReponseJson.value = reponseJson;
        elementReponseJson.disabled = "";

        var elementReponseAnnonceXML = document.getElementById('reponseAnnonceXmlEnBase64');
        elementReponseAnnonceXML.value = reponse.contenuReponseAnnonceXML;
        elementReponseAnnonceXML.disabled = "";

        for (var i = 0; i < reponse.fichiers.length; i++) {
            var fichier = reponse.fichiers[i];

            var identifiantFichier = fichier.identifiant;
            var nomFichier = fichier.cheminFichier;
            var elementFichier = document.getElementById(identifiantFichier);
            elementFichier.value = nomFichier;

            var nomFichierXML = fichier.cheminSignatureXML;
            var elementSignature = document.getElementById(identifiantFichier + 'Xades');
            elementSignature.value = nomFichierXML;
        }
    } catch(e) {
    }
}