function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}


/**
 * Initialise de l'applet de signature Pades en mode Mémoire.
 *
 */
function initialiserSignatureMixteApplet() {

    // récupération de la liste des libs pkcs11 pour les supports cryptographiques
    var urlPkcs11 = getUrlPcks11Libs();
    document.SignatureMixteXadesPadesApplet.setUrlPkcs11LibRefXml(urlPkcs11);

    // signature xades
    document.SignatureMixteXadesPadesApplet.initialiserSignatureXades();
    try {
        var hashFichier1 = document.getElementById('hashFichier1').value;
        var nomFichier1 = document.getElementById('nomFichier1').value;
        if (!isEmpty(hashFichier1) && !isEmpty(nomFichier1)) {
            ajouterHashFichier('contenuFichierXades1Base64', hashFichier1, nomFichier1);
        }
    } catch(e) {}

    // signature pades
    var contenuFichierEnBase64 = document.getElementById('contenuFichierPdfEnBase64').value;
    document.SignatureMixteXadesPadesApplet.initialiserSignaturePadesModeMemoire('contenuFichierPdfFinalEnBase64', contenuFichierEnBase64);

    var signaturePositionBasseX = 10;
    var signaturePositionBasseY = 10;
    var signaturePositionHauteX = 300;
    var signaturePositionHauteY = 80;
    var typeSignataire = "Directeur Adjoint";
    var signatureDate = "23/04/2013 16h56";
    document.SignatureMixteXadesPadesApplet.setSignaturePadesInformation(signaturePositionBasseX, signaturePositionBasseY, signaturePositionHauteX, signaturePositionHauteY, typeSignataire, signatureDate);

    // execution de la signature
    document.SignatureMixteXadesPadesApplet.executer();
}

function ajouterHashFichier(identifiantHashFichier, hashFichier, nomFichier) {
    document.SignatureMixteXadesPadesApplet.ajouterHashFichier(identifiantHashFichier, hashFichier, nomFichier);
}

/**
 * Renvoie le resultat de la signature pades / xades effectuée.
 *
 * @param identifiantFinal l'identifiant final du champs html auquel rattaché le résultat (chemin du fichier signé)
 * @param donnees le chemin vers le fichier pdf signé au format Pades
 */
function renvoiResultat(identifiantFinal, donnees) {
    document.getElementById(identifiantFinal).value = donnees;
}