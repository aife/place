function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise de l'applet de déchiffrement.
 *
 */
function initialiserApplet() {
    var typeDechiffrement = $('input[name=typeDechiffrement]:checked').val();
    var typePhase = $('input[name=typePhase]:checked').val();
    var urlServeurTelechargement = getContextRoot() + "/telechargementBlocChiffre";
    var urlServeurUpload = getContextRoot() + "/envoiBlocDechiffre";
    var trigrammeOrganisme = document.getElementById('uid').value;
    var reponseAnnonceXmlEnBase64 = document.getElementById('reponseAnnonceXmlEnBase64').value;
    var cheminRepertoireTelechargement = document.getElementById('cheminRepertoireTelechargement').value;
    if (typeDechiffrement == '0') {
        // mode en ligne
        document.MpeDechiffrementApplet.initialiserEnLigne(reponseAnnonceXmlEnBase64, trigrammeOrganisme, urlServeurTelechargement, urlServeurUpload);
        document.MpeDechiffrementApplet.executer();

    } else if (typeDechiffrement == '1') {
        // mode mixte
        if (typePhase == '0') {
            // mode telecharger
            document.MpeDechiffrementApplet.initialiserMixtePhaseTelecharger(reponseAnnonceXmlEnBase64, trigrammeOrganisme, urlServeurTelechargement, cheminRepertoireTelechargement);
        } else {
            // mode chiffrer et envoyer
            document.MpeDechiffrementApplet.initialiserMixtePhaseDechiffrerEtEnvoyer(reponseAnnonceXmlEnBase64, trigrammeOrganisme, urlServeurUpload, cheminRepertoireTelechargement);

        }
        document.MpeDechiffrementApplet.executer();

    } else {
        // mode hors ligne

    }
}

/**
 * Ajoute des identifiant des enveloppes à déchiffrer (doit correspondre à celle se trouvant dans le fichier reponse annonce xml)
 * Peut être appeler n fois si l'on veut déchiffrer explicitement plusieurs enveloppe. Si on n'ajoute aucune restriction d'identifiant d'envelope, alors
 * l'ensemble des enveloppes se trouvant le fichier reponse annonce xml seront traités.
 *
 * @param idEnveloppe l'identifiant de l'enveloppe à déchiffrer
 */
function ajouterIdEnveloppe(idEnveloppe) {
    document.MpeDechiffrementApplet.ajouterIdEnveloppe(idEnveloppe);
}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param resultat <code>true</code> si le déchiffrement s'est bien passé ,sinon <code>false</code>
 *
 */
function renvoiResultat(resultat) {

}

function initialiserSelectionRepertoireApplet(identifiant) {
    document.SelectionFichierApplet.initialiserRepertoire(identifiant);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier)
 * @param cheminFichier chemin du fichier
 */
function selectionEffectuee(identifiant, cheminFichier) {
    document.getElementById(identifiant).value = cheminFichier;
}
