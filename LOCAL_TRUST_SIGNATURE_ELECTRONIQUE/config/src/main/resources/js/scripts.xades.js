function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}
/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant) {
    var extensions = new Array('PDF', 'DOC', 'ZIP', 'XLS', 'PPT');
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier à signer
 */
function selectionEffectuee(identifiant, donnees) {
    document.getElementById(identifiant).value = donnees;
}

/**
 * Initialise de l'applet de signature XAdES.
 */
function initialiserApplet() {
    var urlPkcs11 = getUrlPcks11Libs();
    document.SignatureXadesApplet.setUrlPkcs11LibRefXml(urlPkcs11);    

    var urlModuleValidation = getContextRoot() + "signatureXades";
    document.SignatureXadesApplet.initialiser(urlModuleValidation);

    // fichier
    try {
        var cheminFichier = document.getElementById('cheminFichier').value;
        if (!isEmpty(cheminFichier)) {
            ajouterFichier('cheminFichierXades', cheminFichier);
        }
    } catch(e) {}
    // hash fichier 1
    try {
        var hashFichier = document.getElementById('hashFichier1').value;
        var nomFichier = document.getElementById('nomFichier1').value;
        if (!isEmpty(hashFichier) && !isEmpty(nomFichier)) {
            ajouterHashFichier('contenuFichierXades1Base64', hashFichier, nomFichier);
        }
    } catch(e) {}

    // hash fichier 2
    try {
        var hashFichier = document.getElementById('hashFichier2').value;
        var nomFichier = document.getElementById('nomFichier2').value;
        if (!isEmpty(hashFichier) && !isEmpty(nomFichier)) {
            ajouterHashFichier('contenuFichierXades2Base64', hashFichier, nomFichier);
        }
    } catch(e) {}
    document.SignatureXadesApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.SignatureXadesApplet.executer();
}

function ajouterFichier(identifiantFichier, cheminFichier) {
    document.SignatureXadesApplet.ajouterFichier(identifiantFichier, cheminFichier);
}
function ajouterHashFichier(identifiantHashFichier, hashFichier, nomFichier) {
    document.SignatureXadesApplet.ajouterHashFichier(identifiantHashFichier, hashFichier, nomFichier);
}

function renvoiResultat(identifiant, estFichier, donnees) {
    document.getElementById(identifiant).value = donnees;
}

/**
 * Méthode appelé lorsque l'ensemble des hash a été généré.
 */
function finTraitement() {
    document.getElementById('traitement').value = "Traitement fini";
}