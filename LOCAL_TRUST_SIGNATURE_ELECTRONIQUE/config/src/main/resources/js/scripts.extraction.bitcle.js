function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Lance l'initialisation de l'applet
 */
function initialiserExtractionBitApplet() {
    var urlPkcs11 = getUrlPcks11Libs();
    document.ExtractionBitCleApplet.setUrlPkcs11LibRefXml(urlPkcs11);
    document.ExtractionBitCleApplet.ajouterRestrictionTypeCertificatChiffrement();
    document.ExtractionBitCleApplet.executer();
}

/**
 * Renvoi le resultat de l'extraction
 *
 * @param chaineCertificatEnBase64 la chaine resultant de l'extraction. 
 */
function renvoiResultat(chaineCertificatEnBase64) {
    document.getElementById('chaineCertificatEnBase64').value = chaineCertificatEnBase64;
}