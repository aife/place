function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}
/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant, extensions) {
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier pdf à signer
 */
function selectionEffectuee(identifiant, cheminFichierSelectionne) {
    document.getElementById(identifiant).value = cheminFichierSelectionne;
}

/**
 * Initialise de l'applet de signature Pades/Xades et de chiffrement.
 *
 * @param cheminFichierSelectionne l'identifiant du input html contenant le chemin du fichier signé
 * @param cheminFichierSignatureSelectionne  l'identifiant du input html contenant le chemin du fichier de signature
 *
 */
function initialiserApplet(cheminFichierSelectionne, cheminFichierSignatureSelectionne) {
    var urlPkcs11 = getUrlPcks11Libs();
    document.MpeChiffrementApplet.setUrlPkcs11LibRefXml(urlPkcs11);    

    var cheminFichier = document.getElementById(cheminFichierSelectionne).value;
    var cheminFichierSignature = document.getElementById(cheminFichierSignatureSelectionne).value;
    var urlModuleValidation = getContextRoot() + "signatureXades";
    document.MpeChiffrementApplet.initialiser(0, null, null, 0, 0, 1, urlModuleValidation, 0);
    if (!isEmpty(cheminFichier) && !isEmpty(cheminFichierSignature)) {
        ajouterFichier(cheminFichier, 1, 1, 0, 'identifiantVerificationXML', "ACE", null, cheminFichierSignature, 1);
    }    
    document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.MpeChiffrementApplet.executer();
}

function ajouterFichier(cheminFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire) {
    document.MpeChiffrementApplet.ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire);
}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param reponseJson le contenu de la reponse au format json
 *
 */
function renvoiResultat(reponseSignatureFichierJson) {
    try {
        document.getElementById('reponseJson').value = reponseSignatureFichierJson;
        document.getElementById('resultatRecherche').style.display = "block";
        var reponse = jQuery.parseJSON(reponseSignatureFichierJson);
        var reponseSignatureFichier = reponse.fichiers[0];

        var elementCheminFichier = document.getElementById("cheminFichier");
        elementCheminFichier.innerHTML = reponseSignatureFichier.cheminFichier;

        var elementSignatureXML = document.getElementById("cheminFichierXML");
        elementSignatureXML.innerHTML = reponseSignatureFichier.cheminSignatureXML;

        var infosSignature = reponseSignatureFichier.infosSignature;

        var elementSignataireCN = document.getElementById("signataireCN");
        elementSignataireCN.innerHTML = infosSignature.signatairePartiel;

        var elementSignataire = document.getElementById("signataire");
        elementSignataire.innerHTML = infosSignature.signataireComplet;

        var elementEmetteur = document.getElementById("emetteur");
        elementEmetteur.innerHTML = infosSignature.emetteur;

        var elementDateValiditeDu = document.getElementById("dateValiditeDu");
        elementDateValiditeDu.innerHTML = infosSignature.dateValiditeDu;

        var elementDateValiditeAu = document.getElementById("dateValiditeAu");
        elementDateValiditeAu.innerHTML = infosSignature.dateValiditeAu;

        var elementPeriodiciteValide = document.getElementById("periodiciteValide");
        elementPeriodiciteValide.innerHTML = infosSignature.periodiciteValide;

        var elementChaineDeCertificationValide = document.getElementById("chaineDeCertificationValide");
        elementChaineDeCertificationValide.innerHTML = infosSignature.chaineDeCertificationValide;

        var elementAbsenceRevocationCRL = document.getElementById("absenceRevocationCRL");
        elementAbsenceRevocationCRL.innerHTML = infosSignature.absenceRevocationCRL;

        var elementDateSignatureValide = document.getElementById("dateSignatureValide");
        elementDateSignatureValide.innerHTML = infosSignature.dateSignatureValide;

        var elementSignatureValide = document.getElementById("signatureValide");
        elementSignatureValide.innerHTML = infosSignature.signatureValide;


    } catch(e) {
    }
}