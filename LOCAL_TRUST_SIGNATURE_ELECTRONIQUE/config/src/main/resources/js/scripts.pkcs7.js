function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise de l'applet de signature Pades.
 *
 */
function initialiserApplet() {
    var urlPkcs11 = getUrlPcks11Libs();
    document.SignaturePkcs7Applet.setUrlPkcs11LibRefXml(urlPkcs11);

    var hashFichier1Selectionne = document.getElementById('hashFichier1').value;
    document.SignaturePkcs7Applet.ajouterHash('contenuSignature1Base64', hashFichier1Selectionne);

    var hashFichier2Selectionne = document.getElementById('hashFichier2').value;
    document.SignaturePkcs7Applet.ajouterHash('contenuSignature2Base64', hashFichier2Selectionne);

    document.SignaturePkcs7Applet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.SignaturePkcs7Applet.executer();
}

/**
 * Renvoie le resultat de la signature pades effectuée.
 *
 * @param identifiant l'identifiant final du champs html auquel rattaché le résultat
 * @param hashFichierSigneBase64 le chemin vers le fichier pdf signé au format Pades
 */
function renvoiResultat(identifiant, hashFichierSigneBase64) {
    document.getElementById(identifiant).value = hashFichierSigneBase64;
}

/**
 * Méthode appelé lorsque l'ensemble des hash a été généré.
 */
function finTraitement() {
    document.getElementById('traitement').value = "Traitement fini";
}