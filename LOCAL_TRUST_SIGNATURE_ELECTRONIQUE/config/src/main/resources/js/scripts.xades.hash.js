function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant) {
    var extensions = new Array('PDF', 'DOC', 'ZIP', 'XLS', 'PPT');
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier à signer
 */
function selectionEffectuee(identifiant, cheminFichierSelectionne) {
    document.getElementById(identifiant).value = cheminFichierSelectionne;
}

/**
 * Initialise de l'applet de signature Pades/Xades et de chiffrement.
 *
 * @param chiffrementRequis <code>true</code> si le chiffrement est requis ,sinon <code>false</code>
 * @param signatureRequise  <code>true</code> si la signature Xades est requise ,sinon <code>false</code>
 * @param signaturePadesActeEngagement  <code>true</code> si la signature Pades du fichier d'acte d'engagement est requis ,sinon <code>false</code>
 * @param effectuerVerificationSignature  <code>true</code> si la verification de la signature est requis (uniquement si le fichier de signature Xades est lui aussi passé en paramètre
 * @param afficherBarreProgression <code>true</code> si l'on veut afficher la barre de progression, sinon <code>false</code>
 * de la fonction d'ajout de fichiers),sinon <code>false</code>
 */
function initialiserApplet() {
    var urlPkcs11 = getUrlPcks11Libs();
    document.MpeChiffrementApplet.setUrlPkcs11LibRefXml(urlPkcs11);    

    var url = null;
    var uid = null;
    var urlModuleValidation = getContextRoot() + "signatureXades";
    document.MpeChiffrementApplet.initialiser(0, url, uid, 1, 0, 0, urlModuleValidation, 0);
    var cheminFichier = document.getElementById('cheminFichier').value;
    if (!isEmpty(cheminFichier)) {
        ajouterFichier(cheminFichier, 1, 1, 0, 'cheminFichierFinal', "ACE", null, null, 1);
    }
    var hashFichier = document.getElementById('hashFichier').value;
    var nomFichier = document.getElementById('nomFichier').value;
    if (!isEmpty(hashFichier) && !isEmpty(nomFichier)) {
        ajouterHashFichier(hashFichier, nomFichier, 1, 2, 1, 'hashFichier', "PRI", null, null);
    }
    document.MpeChiffrementApplet.ajouterRestrictionTypeCertificatSignatureElectronique();    
    document.MpeChiffrementApplet.executer();
}

function ajouterFichier(cheminFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire) {
    document.MpeChiffrementApplet.ajouterFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, cheminFichierSelectionne, typeFichier, origineFichier, cheminFichierSignatureXML, signatureNecessaire);
}
function ajouterHashFichier(hashFichierSelectionne, nomFichierSelectionne, typeEnveloppe, numeroLot, indexFichier, identifiantFichier, typeFichier, origineFichier, contenuFichierSignatureXML) {
    document.MpeChiffrementApplet.ajouterHashFichier(typeEnveloppe, numeroLot, indexFichier, identifiantFichier, hashFichierSelectionne, nomFichierSelectionne, typeFichier, origineFichier, contenuFichierSignatureXML);
}

/**
 * Renvoie le resultat de la signature pades / xades et le chiffrement effectuée.
 *
 * @param reponseJson le contenu de la reponse au format json
 *
 */
function renvoiResultat(reponseJson) {
    try {
        document.getElementById('reponseJson').value = reponseJson;
        var reponse = jQuery.parseJSON(reponseJson);
        document.getElementById('resultatRecherche').style.display = "block";
        for (var i = 0; i < reponse.fichiers.length; i++) {
            var fichier = reponse.fichiers[i];

            var identifiantFichier = fichier.identifiant;
            var nomFichier = fichier.cheminFichier;
            var elementFichier = document.getElementById(identifiantFichier);
            elementFichier.value = nomFichier;

            var nomFichierXML = fichier.cheminSignatureXML;
            var elementSignature = document.getElementById(identifiantFichier + 'Xades');
            elementSignature.value = nomFichierXML;
        }
        for (var i = 0; i < reponse.hashs.length; i++) {
            var hash = reponse.hashs[i];
            var contenuSignatureXML = hash.contenuSignatureXML;
            var elementSignature = document.getElementById('hashFichierSigneBase64');
            elementSignature.value = contenuSignatureXML;
        }
    } catch(e) {
        alert(e);
    }
}