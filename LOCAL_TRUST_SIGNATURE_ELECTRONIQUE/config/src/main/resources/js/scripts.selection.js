/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant) {
    var extensions = new Array('PDF', 'DOC', 'ZIP', 'XLS', 'PPT');
    document.SelectionFichierApplet.initialiser(identifiant, 1, extensions);
    document.SelectionFichierApplet.executer();
}

function initialiserSelectionRepertoireApplet(identifiant) {
    document.SelectionFichierApplet.initialiserRepertoire(identifiant);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier)
 * @param cheminFichier chemin du fichier
 */
function selectionEffectuee(identifiant, cheminFichier) {
    document.getElementById(identifiant).value = cheminFichier;
}

/**
 * Initialise la textbox avec le chemin vers le fichier (appel n fois)
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier)
 * @param cheminFichier chemin du fichier
 */
function selectionMultipleEffectuee(identifiant, cheminFichier) {
    document.getElementById(identifiant).value = cheminFichier;
}