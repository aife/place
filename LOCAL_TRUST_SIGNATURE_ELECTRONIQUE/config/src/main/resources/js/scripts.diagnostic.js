/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Permet de renvoyer les informations du test effectué.
 *
 * @param identifiant l'identifiant associé à la réponse attendu
 * @param information l'information complémentaire
 * @param valide <code>true</code> si le test s'est déroulé correctement, sinon <code>false</false>
 */
function renvoyerInformation(identifiant, information, valide) {
    var element = document.getElementById(identifiant);
    if (element != null) {
        var message = element.children[1].children[0];
        var imageAttente = element.children[2].children[0];
        message.innerHTML = information;
        if (valide != null && valide != 'true') {
            imageAttente.src = "../images/picto-check-not-ok.gif";
        } else {
            imageAttente.src = "../images/picto-check-ok.gif";
        }
    }
}
/**
 * Lance l'initialisation de l'applet
 */
function verifierCommunicationJs() {
    try {
        document.DiagnosticApplet.verifierCommunicationJs();
        renvoyerInformation("identifiantCommunicationJs", "Valide", "true");
    } catch(e) {
        renvoyerInformation("identifiantCommunicationJs", "Invalide", "false");
    }
}

function finTraitementDiagnostic() {
    alert("le Diagnostic est fini !!!!")
}