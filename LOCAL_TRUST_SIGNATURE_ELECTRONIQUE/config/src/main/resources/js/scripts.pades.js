function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs(){
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Initialise le filtre des extensions à appliquer pour la sélection de fichiers ainsi
 * que l'applet de selection.
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 *
 */
function initialiserSelectionFichierApplet(identifiant) {
    var extensions = new Array('PDF');
    document.SelectionFichierApplet.initialiser(identifiant, extensions);
    document.SelectionFichierApplet.executer();
}

/**
 * Initialise la textbox avec le chemin vers le fichier à signer
 *
 * @param identifiant l'identifiant du champs html auquel rattaché le résultat (chemin du fichier à signer)
 * @param cheminFichierPdfSelectionne chemin du fichier pdf à signer
 */
function selectionEffectuee(identifiant, cheminFichierPdfSelectionne) {
    document.getElementById(identifiant).value = cheminFichierPdfSelectionne;
}

/**
 * Initialise de l'applet de signature Pades en mode Disque.
 *
 * @param identifiantFichier l'identifiant du champs html ayant le chemin source du fichier pdf a signer (chemin du fichier à signer)
 * @param identifiantFichierSigne l'identifiant du champs html auquel rattaché le résultat (chemin du fichier pdf final signé)
 */
function initialiserSignaturePadesDisqueApplet(identifiantFichier, identifiantFichierSigne) {

    var urlPkcs11 = getUrlPcks11Libs();
    document.SignaturePadesApplet.setUrlPkcs11LibRefXml(urlPkcs11);
    var cheminFichierPdfSelectionne = document.getElementById(identifiantFichier).value;
    var signaturePositionBasseX = 10;
    var signaturePositionBasseY = 10;
    var signaturePositionHauteX = 210;
    var signaturePositionHauteY = 60;
    var signatureMotif = null;
    var signatureLocalisation = null;
    var signatureDate = null;
    document.SignaturePadesApplet.initialiserSignatureInformation(signaturePositionBasseX, signaturePositionBasseY, signaturePositionHauteX, signaturePositionHauteY, signatureMotif, signatureLocalisation, signatureDate);
    document.SignaturePadesApplet.initialiserSignatureModeDisque(identifiantFichierSigne, cheminFichierPdfSelectionne);
    document.SignaturePadesApplet.ajouterRestrictionTypeCertificatSignatureElectronique();
    document.SignaturePadesApplet.executer();
}

/**
 * Initialise de l'applet de signature Pades en mode Mémoire.
 *
 * @param identifiantFichier l'identifiant du champs html ayant le chemin source du fichier pdf a signer (contenu en base 64 du fichier à signer)
 * @param identifiantFichierSigne l'identifiant du champs html auquel rattaché le résultat (contenu en base 64 du fichier pdf final signé)
 */
function initialiserSignaturePadesMemoireApplet(identifiantFichier, identifiantFichierSigne) {

    var urlPkcs11 = getUrlPcks11Libs();
    document.SignaturePadesApplet.setUrlPkcs11LibRefXml(urlPkcs11);

    var contenuFichierEnBase64 = document.getElementById(identifiantFichier).value;
    var signaturePositionBasseX = 10;
    var signaturePositionBasseY = 10;
    var signaturePositionHauteX = 210;
    var signaturePositionHauteY = 60;
    var signatureMotif = null;
    var signatureLocalisation = null;
    var signatureDate = null;
    document.SignaturePadesApplet.initialiserSignatureInformation(signaturePositionBasseX, signaturePositionBasseY, signaturePositionHauteX, signaturePositionHauteY, signatureMotif, signatureLocalisation, signatureDate);
    document.SignaturePadesApplet.initialiserSignatureModeMemoire(identifiantFichierSigne, contenuFichierEnBase64);
    document.SignaturePadesApplet.executer();
}

/**
 * Renvoie le resultat de la signature pades effectuée.
 *
 * @param identifiantFinal l'identifiant final du champs html auquel rattaché le résultat (chemin du fichier signé)
 * @param resultatSignature <code>true</code> si la signature s'est déroulée comme prévu,sinon <code>false</code>
 * @param donnees le chemin vers le fichier pdf signé au format Pades
 */
function renvoiResultat(identifiantFinal, resultatSignature, donnees) {
    document.getElementById(identifiantFinal).value = donnees;
}