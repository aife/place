function getContextRoot() {
    //Location object's href gives the complete URL
    var url = location.protocol + "//" + location.host;
    return url + "/validation/";
}

function getUrlPcks11Libs() {
    return getContextRoot() + "pkcs11Lib/pkcs11Libs.xml";
}

/**
 * Affiche le veuillez patienter
 */
function afficherVeuillezPatienter() {
    document.getElementById('loading').style.display = 'block';
}

/**
 * Masque le veuillez patienter
 */
function masquerVeuillezPatienter() {
    document.getElementById('loading').style.display = 'none';
}

/**
 * Lance l'initialisation de l'applet en mode génération.
 */
function initialiserGenerationActeEngagementApplet() {
    var contenuFichierXMLEnBase64 = document.getElementById('contenuFichierXMLEnBase64').value;
    document.MpeGenerationActeEngagementApplet.initialiserGeneration('cheminFichierAePdfGenere', 'cheminFichierAeXmlGenere', contenuFichierXMLEnBase64, 0, "http://www.google.fr/logos/2012/Rodin-2012-homepage.png");
    document.MpeGenerationActeEngagementApplet.executer();
}

/**
 * Lance l'initialisation de l'applet en mode vérification.
 */
function initialiserVerificationActeEngagementApplet() {
    document.MpeGenerationActeEngagementApplet.initialiserVerification("|");
    document.MpeGenerationActeEngagementApplet.executer();
}

/**
 * Renvoi le resultat de la génération
 *
 * @param identifiantFichier
 * @param cheminFichier
 * @param identifiantFichierXML
 * @param cheminFichierXML.
 */
function renvoiResultatGeneration(identifiantFichier, cheminFichier, identifiantFichierXML, cheminFichierXML) {
    document.getElementById(identifiantFichier).value = cheminFichier;
    document.getElementById(identifiantFichierXML).value = cheminFichierXML;
}

/**
 * Renvoi le resultat de la vérification de chaque fichier
 *
 * @param cheminFichierInvalide
 */
function renvoiResultatVerification(cheminFichierInvalide) {
    alert("Fichier(s) invalide(s) : " + cheminFichierInvalide);
}

/**
 * Appele lors de la fin de la verification
 *
 * @param resultatVerification
 */
function finAttenteVerification(resultatVerification) {
    alert("Resultat de la verification de l'ensemble des actes d'engagements générés : " + resultatVerification);
}