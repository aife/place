package fr.atexo.signature.commun.util.io;

import junit.framework.TestCase;
import org.junit.Test;

import java.security.MessageDigest;

/**
 * Test unitaire pour tester la classe utilitaire HashUtil.
 */
public class HashUtilTest extends TestCase {

    private static final String CHAINE_DE_TEST = "http://atexo.com/#signedProps";

    @Test
    public void testHashSha256() throws Exception {

        String chaine1 = computeSHA("SHA-256", CHAINE_DE_TEST.getBytes());
        String chaine2 = HashUtil.genererHashSha256Hexadecimal(CHAINE_DE_TEST.getBytes());

        assertTrue(chaine1.equals(chaine2));
    }

    @Test
    public void testHashSha1() throws Exception {

        String chaine1 = computeSHA("SHA", CHAINE_DE_TEST.getBytes());
        String chaine2 = HashUtil.genererHashSha1Hexadecimal(CHAINE_DE_TEST.getBytes());

        assertTrue(chaine1.equals(chaine2));
    }

    @Deprecated
    public static String computeSHA(String algo, byte[] data) throws Exception {
        java.security.MessageDigest md = MessageDigest.getInstance(algo);
        md.update(data);
        byte[] digest = md.digest();
        return HashUtil.convertirBinaireEnHexadecimal(digest);
    }
}
