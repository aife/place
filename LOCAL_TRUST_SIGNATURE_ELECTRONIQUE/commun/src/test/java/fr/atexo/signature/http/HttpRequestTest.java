package fr.atexo.signature.http;

import junit.framework.TestCase;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 */
public class HttpRequestTest extends TestCase {

    @Test
    public void testHttpRequest() throws Exception {
        HttpRequest httpRequest = new HttpRequest(new URL("https://applet-crypto-test.local-trust.com/index.php?page=ModuleValidation"), true);
        httpRequest.deconnecter();
        assertTrue(httpRequest != null);
    }
}
