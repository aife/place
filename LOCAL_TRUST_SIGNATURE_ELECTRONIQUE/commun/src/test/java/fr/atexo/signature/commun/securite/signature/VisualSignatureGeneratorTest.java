package fr.atexo.signature.commun.securite.signature;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import fr.atexo.pdfboxhelpers.VisualSignatureGenerator;

@Ignore
public class VisualSignatureGeneratorTest {

	private final static Logger logger = Logger.getLogger(VisualSignatureGeneratorTest.class);

	@Test
	public void test_generation_png_signature_visuelle() {
		try {

			VisualSignatureGenerator visualSignatureGenerator = new VisualSignatureGenerator();

			VisualSignatureRectangle positionSignature = new VisualSignatureRectangle(10, 10, 210, 60);

			int width = (int) (positionSignature.getRight() - positionSignature.getLeft());
			int height = (int) (positionSignature.getTop() - positionSignature.getBottom());

			BufferedImage bufferedImage = visualSignatureGenerator.getVisibleSignature(width, height, null, "Mourad", "rais", "lieu", new Date());
			File ouput = new File("c:/Config/pic_.png");// File.createTempFile("testVisualSignature",
														// ".png");
			ImageIO.write(bufferedImage, "PNG", ouput);
			logger.info("Visual signature saved at : " + ouput.getAbsolutePath());
		} catch (Exception e) {
			Assert.fail();
		}
	}

}
