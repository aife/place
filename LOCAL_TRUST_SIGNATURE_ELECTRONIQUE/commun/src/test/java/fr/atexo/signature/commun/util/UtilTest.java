package fr.atexo.signature.commun.util;

import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.util.io.FileUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Date;

/**
 * Test unitaire pour tester la classe utilitaire Util.
 */
public class UtilTest extends TestCase {

    // architecture
    private final static String ARCHITECTURE_AMD64 = "amd64";
    private final static String ARCHITECTURE_X86 = "x86";

    // os
    private final static String OS_NOM_WINDOWS = "Windows";
    private final static String OS_NOM_LINUX = "Linux";
    private final static String OS_NOM_UNIX = "Unix";
    private final static String OS_NOM_MAC = "Mac";

    // jre version
    private final static String JRE_1_5 = "1.5";
    private final static String JRE_1_6 = "1.6";
    private final static String JRE_1_7 = "1.7";

    // bit
    private final static String BITS_32 = "32";
    private final static String BITS_64 = "64";

    @Test
    public void testProvider() {
        TypeProvider typeProvider0 = Util.determinerProvider(ARCHITECTURE_X86, OS_NOM_WINDOWS, JRE_1_5, BITS_32);
        assertTrue(typeProvider0 == TypeProvider.PKCS12);

        TypeProvider typeProvider1 = Util.determinerProvider(ARCHITECTURE_X86, OS_NOM_WINDOWS, JRE_1_7, BITS_32);
        assertTrue(typeProvider1 == TypeProvider.MSCAPI);

        TypeProvider typeProvider2 = Util.determinerProvider(ARCHITECTURE_X86, OS_NOM_WINDOWS, JRE_1_6, BITS_32);
        assertTrue(typeProvider2 == TypeProvider.MSCAPI);

        TypeProvider typeProvider4 = Util.determinerProvider(ARCHITECTURE_X86, OS_NOM_LINUX, JRE_1_6, BITS_32);
        assertTrue(typeProvider4 == TypeProvider.PKCS11);

    }

    @Test
    public void testReplacementDeChaine() {
        Date date = new Date();
        String dateModeAncien = Util.formaterDate(date, Util.DATE_TIME_PATTERN_TIRET).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String dateModeNouveau = Util.remplacerPar(Util.formaterDate(date, Util.DATE_TIME_PATTERN_TIRET), new String[]{":", " ", "-"}, "_");
        assertTrue(dateModeAncien.equals(dateModeNouveau));
    }

    @Test
    public void testXsdDateTime() {
        String datetime = Util.creerISO8601DateTime(new Date());
        assertTrue(datetime != null);
    }

    @Test
    public void testDateNomFichier() {
        String formatDateNomFichier = Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);
        assertTrue(formatDateNomFichier != null);
    }

    @Test
    public void testDateNomFichierAvecMiliseconde() {
        String formatDateNomFichier = Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_YYYMMDDHHMMSSSS);
        assertTrue(formatDateNomFichier != null);
    }
}
