package fr.atexo.signature.commun.securite.signature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import fr.atexo.pdfboxhelpers.PdfPKCS7;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;

public class SignaturePadesPdfBoxTest {

	private final static Logger logger = Logger.getLogger(SignaturePadesPdfBoxTest.class);

	private static final BouncyCastleProvider PROVIDER = new BouncyCastleProvider();

	private static final String P12_FILE = "fr/atexo/signature/commun/securite/signature/Christian_CARLET.p12";

	private final static String P12_PIN = "1234";

	private static final String PDF_FILE = "fr/atexo/signature/commun/securite/signature/Acrobat_DigitalSignatures_in_PDF.pdf";

	private PrivateKey privateKey;

	private Certificate[] certificates;

	private URL pdfFileUrl = null;

	@Before
	public void setup() throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, IOException {

		ClassLoader classLoader = SignaturePadesPdfBoxTest.class.getClassLoader();
		KeyStore keystore = KeyStore.getInstance("PKCS12", PROVIDER);
		keystore.load(classLoader.getResourceAsStream(P12_FILE), P12_PIN.toCharArray());

		Enumeration<String> aliases = keystore.aliases();
		String alias = null;

		if (aliases.hasMoreElements()) {
			alias = aliases.nextElement();
		} else {
			Assert.fail("Could not find Key.");
		}

		privateKey = (PrivateKey) keystore.getKey(alias, P12_PIN.toCharArray());
		certificates = keystore.getCertificateChain(alias);
		pdfFileUrl = classLoader.getResource(PDF_FILE);
	}

	@Test
	public void test_signature_et_verification() throws SignatureException, IOException {

		File inputFile = new File(pdfFileUrl.getFile());
		File outputFile = File.createTempFile("output", ".pdf");

		VisualSignatureRectangle positionSignature = new VisualSignatureRectangle(10, 10, 210, 60);

		// on signe le fichier
		SignaturePades.signerFichierPdf(new FileInputStream(inputFile), new FileOutputStream(outputFile), certificates[0], privateKey, positionSignature,
				"raison", "lieu", null);

		// on vérifie si la signature est valide
		List<InfosComplementairesCertificat> list = SignaturePades.verifier(outputFile);
		for (InfosComplementairesCertificat infosComplementairesCertificat : list) {
			Assert.assertTrue(infosComplementairesCertificat.getSignatureValide());
		}

		// on extrait le certificat du pdf signé et on vérifie le nom
		List<X509Certificate> certificats = SignaturePades.extraireCertificats(outputFile);
		Assert.assertEquals(1, certificats.size());
		X509Certificate certificate = certificats.get(0);
		String name = PdfPKCS7.getSubjectFields(certificate).getField("CN");

		Assert.assertEquals("Christian CARLET", name);
	}

}
