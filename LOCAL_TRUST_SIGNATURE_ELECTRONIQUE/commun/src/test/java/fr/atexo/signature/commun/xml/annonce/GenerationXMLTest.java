package fr.atexo.signature.commun.xml.annonce;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.xml.annonce.ObjectFactory;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;
import fr.atexo.signature.xml.annonce.ReponsesAnnonce;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

/**
 * Test unitaire de vérification du fonctionnement de la fonction de marshalling / unmashaling de jaxb.
 */
public class GenerationXMLTest extends TestCase {

    private final String FICHIER_ANNONCE_XML = "fr/atexo/signature/commun/xml/annonce/ReponsesAnnonce.xml";

    private ObjectFactory factory = new ObjectFactory();

    public static InputStream searchResource(String resource) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        InputStream result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        loader = ClassLoader.getSystemClassLoader();

        result = getInputStreamFormUrl(loader.getResource(resource));

        if (result != null) {
            return result;
        }

        throw new IOException("Aucun class loader n'a été trouvé pour charger la resource nommé : " + resource);
    }

    private static InputStream getInputStreamFormUrl(URL url) {
        if (url != null) {

            try {
                URLConnection connection = url.openConnection();
                return connection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String convertStreamToString(InputStream inputStream) {
        java.util.Scanner s = new java.util.Scanner(inputStream).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public String getReponsesAnnonceString() throws IOException {
        InputStream inputStream = searchResource(FICHIER_ANNONCE_XML);
        String reponsesAnnonce = convertStreamToString(inputStream);
        return reponsesAnnonce;
    }

    public ReponsesAnnonce getReponsesAnnonceObject() {

        ReponsesAnnonce reponsesAnnonce = factory.createReponsesAnnonce();

        ReponseAnnonceType reponseAnnonce = factory.createReponseAnnonceType();
        reponsesAnnonce.getReponseAnnonce().add(reponseAnnonce);
        reponseAnnonce.setIdReponseAnnonce(1);

        ReponseAnnonceType.OperateurEconomique operateurEconomique = factory.createReponseAnnonceTypeOperateurEconomique();
        reponseAnnonce.setOperateurEconomique(operateurEconomique);
        operateurEconomique.setNom("Atexo - Finance");

        ReponseAnnonceType.Annonce annonce = factory.createReponseAnnonceTypeAnnonce();
        reponseAnnonce.setAnnonce(annonce);
        annonce.setAllotissement(false);
        annonce.setChiffrement(true);
        annonce.setDateLimite(Util.formaterDate(new Date(), Util.DATE_TIME_PATTERN_TIRET));
        annonce.setEntiteAchat("ATEXO");
        annonce.setIntitule("Atexo - Test Unitaire");
        annonce.setNombreLots(0);
        annonce.setObjet("Test unitaire de génération de fichier XML");
        annonce.setOrganisme("ATX");
        annonce.setReference("234");
        annonce.setReferenceUtilisateur("2912V09000110");
        annonce.setSeparationSignatureAe(true);
        annonce.setSignature(true);


        ReponseAnnonceType.Enveloppes enveloppes = factory.createReponseAnnonceTypeEnveloppes();
        reponseAnnonce.setEnveloppes(enveloppes);

        ReponseAnnonceType.Enveloppes.Enveloppe enveloppe = factory.createReponseAnnonceTypeEnveloppesEnveloppe();
        enveloppes.getEnveloppe().add(enveloppe);
        enveloppe.setIdEnveloppe(1);
        enveloppe.setNumLot(0);
        enveloppe.setStatutOuverture(ReponseAnnonceConstantes.STATUT_OUVERTURE_OUVERT);
        enveloppe.setStatutAdmissibilite(ReponseAnnonceConstantes.STATUT_ADMISSIBILITE_ATR);
        enveloppe.setStatutChiffrement(ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE);

        ReponseAnnonceType.Enveloppes.Enveloppe.Fichier fichier = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichier();
        enveloppe.getFichier().add(fichier);
        fichier.setNom("FichierPrincipal.pdf");
        fichier.setNbrBlocsChiffrement(1);
        fichier.setNumOrdre(1);
        fichier.setEmpreinte("7F499F3D9BA0764D52DC3BF9A03DD2100B5A6282");
        fichier.setSignature("PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgSWQ9InNpZ25hdHVyZSI+PGRzOlNpZ25lZEluZm8gSWQ9InNpZ25lZEluZm9zIj48ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ii8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNyc2Etc2hhMSIvPjxkczpSZWZlcmVuY2UgVVJJPSJib2FtcDMucGRmIj48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPk4wWTBPVGxHTTBRNVFrRXdOelkwUkRVeVJFTXpRa1k1UVRBelJFUXlNVEF3UWpWQk5qSTRNZz09PC9kczpEaWdlc3RWYWx1ZT48L2RzOlJlZmVyZW5jZT48ZHM6UmVmZXJlbmNlIFVSST0iI3NpZ25lZFByb3BzIj48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+QXc2SVN3aHV1RnhpQ2dUaHA4RURCY0NLV0Q4PTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5xQWNLNVQxVmNZVThhVDNwaEkyMFVVWXdCd255cjBKejdCcGRVOHRLazNBSERweFpQVmVjbktNV2ptM3ZvSm9KTkF6TzZOK1AyZXFnDQo2VWh4TEFJUXQxVU03SnEvL28yN2hPS0lkQ2t6MXZ4eGFQbzRpK2duK05zTTZ1b09ES2orSWdrTWNmb3JKSjBXL2ZrSTFuN0dXYmRuDQppalpVVDgxcnYrK1dFcndyWUY4PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5U3ViamVjdE5hbWU+Q049Q0zDiSBTRUNPVVJTIERBTElBVDwvZHM6WDUwOVN1YmplY3ROYW1lPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJREpqQ0NBbytnQXdJQkFnSUVVTEJ6cHpBTkJna3Foa2lHOXcwQkFRUUZBREIzTVFzd0NRWURWUVFHRXdKR1VqRU9NQXdHQTFVRQ0KQnhNRlVHRnlhWE14RnpBVkJnTlZCQW9URGxacGJHeGxJR1JsSUZCaGNtbHpNU1l3SkFZRFZRUUxFeDFOUVZKRFNFVlRJRkJWUWt4Sg0KUTFNZ1JVeEZRMVJTVDA1SlVWVkZVekVYTUJVR0ExVUVBeE1PVm1sc2JHVWdaR1VnVUdGeWFYTXdIaGNOTURnd09UQTFNVEl3TmpReQ0KV2hjTk1UZ3dPVEExTVRJd05qUXlXakF2TVMwd0t3WURWUVFESGlRQVF3Qk1BTWtBSUFCVEFFVUFRd0JQQUZVQVVnQlRBQ0FBUkFCQg0KQUV3QVNRQkJBRlF3Z1o4d0RRWUpLb1pJaHZjTkFRRUJCUUFEZ1kwQU1JR0pBb0dCQU5vZytvRXlQN0UwU3NlTlJMczVLdXprY3ZuQg0KM2hsWVNXZU1NWS9kdytFNnMxanB2c0FTMUU0WkVZdXBrRnAxTGFVdkU1MkZRS1BrWW9hT2VDUVJVLzdJKyt0WklqL3RpZnpMOEVkcg0KV2VEb1JvMHQzV2NhT1M2SUZkUTMydmZLQWxEQkxHdUNTVGRzV1JHU1hoUHgvVjU1UDFjOUI1QVBrTDNLZWlPdDF5ajNBZ01CQUFHag0KZ2dFRk1JSUJBVEFKQmdOVkhSTUVBakFBTUFzR0ExVWREd1FFQXdJRjREQWRCZ05WSFE0RUZnUVVqTHA4L1dmVUVINlVpR0JuVi84ag0KLzYzbW5yRXdnYWdHQTFVZEl3U0JvRENCbllBVVluSytTT0ZrOFhUdmhCVDUrWlk4WDNlRU1vYWhnWUdrZnpCOU1Rc3dDUVlEVlFRRw0KRXdKR1VqRU9NQXdHQTFVRUJ4TUZVR0Z5YVhNeEZ6QVZCZ05WQkFvVERsWnBiR3hsSUdSbElGQmhjbWx6TVNZd0pBWURWUVFMRXgxTg0KUVZKRFNFVlRJRkJWUWt4SlExTWdSVXhGUTFSU1QwNUpVVlZGVXpFZE1Cc0dBMVVFQXhNVVZtbHNiR1VnWkdVZ1VHRnlhWE1nTFNCTg0KVUVXQ0FRSXdIUVlEVlIwbEJCWXdGQVlJS3dZQkJRVUhBd0lHQ0NzR0FRVUZCd01FTUEwR0NTcUdTSWIzRFFFQkJBVUFBNEdCQUkxNg0KV3BoN0tFOGVkK2lIelVSM0VzRy9wYkRyT1IyN0sxT1JSVHhQRGlGY3Q2cy9pNFhlY2ljZU9ZTUpaQVFDRUZ3dkMxcG5GdE1IQTFoaA0KMjNDWXdoK09rTCtMN2RvMFUwV2FVRnkzNVZ0elQyQThZRm9teXZXMDFhU1BjTTJnUGVoWXVpdzI3RUdvb1F6UHpCN2lSMDBLeXZGTw0KWWZ0SUlSalA4cW8rbmRyRTwvZHM6WDUwOUNlcnRpZmljYXRlPjwvZHM6WDUwOURhdGE+PC9kczpLZXlJbmZvPjxkczpPYmplY3QgSWQ9Im9iamVjdCI+PHhhZGVzOlF1YWxpZnlpbmdQcm9wZXJ0aWVzIHhtbG5zOnhhZGVzPSJodHRwOi8vdXJpLmV0c2kub3JnLzAxOTAzL3YxLjEuMSMiIFRhcmdldD0iI3NpZ25hdHVyZSI+PHhhZGVzOlNpZ25lZFByb3BlcnRpZXMgeG1sbnM9Imh0dHA6Ly91cmkuZXRzaS5vcmcvMDE5MDMvdjEuMS4xIyIgSWQ9InNpZ25lZFByb3BzIj48eGFkZXM6U2lnbmVkU2lnbmF0dXJlUHJvcGVydGllcz48eGFkZXM6U2lnbmluZ1RpbWU+MjAxMy0wMi0xOCAxNzozOToxMTwveGFkZXM6U2lnbmluZ1RpbWU+PHhhZGVzOlNpZ25pbmdDZXJ0aWZpY2F0ZT48eGFkZXM6Q2VydD48eGFkZXM6Q2VydERpZ2VzdD48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+MjU3RTdBOTJDNDRBMEI1NkQ4M0Y0NEIzQjM4NEE4ODlDMEY1NDE4MjwvZHM6RGlnZXN0VmFsdWU+PC94YWRlczpDZXJ0RGlnZXN0Pjx4YWRlczpYNTA5SXNzdWVyU2VyaWFsPjxkczpYNTA5SXNzdWVyTmFtZT5DTj1WaWxsZSBkZSBQYXJpcywgT1U9TUFSQ0hFUyBQVUJMSUNTIEVMRUNUUk9OSVFVRVMsIE89VmlsbGUgZGUgUGFyaXMsIEw9UGFyaXMsIEM9RlI8L2RzOlg1MDlJc3N1ZXJOYW1lPjxkczpYNTA5U2VyaWFsTnVtYmVyPjEzNTM3NDEyMjM8L2RzOlg1MDlTZXJpYWxOdW1iZXI+PC94YWRlczpYNTA5SXNzdWVyU2VyaWFsPjwveGFkZXM6Q2VydD48L3hhZGVzOlNpZ25pbmdDZXJ0aWZpY2F0ZT48L3hhZGVzOlNpZ25lZFNpZ25hdHVyZVByb3BlcnRpZXM+PC94YWRlczpTaWduZWRQcm9wZXJ0aWVzPjwveGFkZXM6UXVhbGlmeWluZ1Byb3BlcnRpZXM+PC9kczpPYmplY3Q+PC9kczpTaWduYXR1cmU+");
        fichier.setTailleEnClair(43991);
        fichier.setTypeFichier(ReponseAnnonceConstantes.TYPE_FICHIER_ACE);
        fichier.setStatutChiffrement(ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE);

        ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement blocsChiffrement = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrement();
        fichier.setBlocsChiffrement(blocsChiffrement);
        blocsChiffrement.setNbrBlocs(1);

        ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement blocChiffrement = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrementBlocChiffrement();
        blocsChiffrement.getBlocChiffrement().add(blocChiffrement);
        blocChiffrement.setEmpreinte("7F499F3D9BA0764D52DC3BF9A03DD2100B5A6282");
        blocChiffrement.setNomBloc("3884");
        blocChiffrement.setNumOrdre(1);
        blocChiffrement.setStatutChiffrement(ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE);
        blocChiffrement.setTailleEnClair(43991);
        blocChiffrement.setTailleApresChiffrement(59910);

        return reponsesAnnonce;
    }

    @Test
    public void testGenererAnnonceUniqueDepuisObject() {
        ReponsesAnnonce reponsesAnnonceObject = getReponsesAnnonceObject();
        String context = ReponsesAnnonce.class.getPackage().getName();
        String reponsesAnnonceXML = JAXBService.instance().getAsString(reponsesAnnonceObject, context);
        assertTrue(reponsesAnnonceXML != null);
    }

    @Test
    public void testGenererAnnonceUniqueDepuisXML() throws IOException {
        String reponsesAnnonceXML = getReponsesAnnonceString();
        String context = ReponsesAnnonce.class.getPackage().getName();
        Object reponsesAnnonceObject = JAXBService.instance().getAsObject(reponsesAnnonceXML, context);
        assertTrue(reponsesAnnonceObject != null && reponsesAnnonceObject instanceof ReponsesAnnonce);

        String secondeReponsesAnnonceXML = JAXBService.instance().getAsString(reponsesAnnonceObject, context);
        assertTrue(secondeReponsesAnnonceXML != null);
    }
}
