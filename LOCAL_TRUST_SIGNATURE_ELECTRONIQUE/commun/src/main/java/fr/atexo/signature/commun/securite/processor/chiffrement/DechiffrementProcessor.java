package fr.atexo.signature.commun.securite.processor.chiffrement;

import fr.atexo.signature.commun.exception.execution.DechiffrementExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;

import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Interface exposant les méthodes permettant de déchiffrer un fichier.
 */
public interface DechiffrementProcessor {

    /**
     * Execute le déchiffrement du fichier.
     *
     * @param contenuFichierXML le contenu chiffré
     * @return le contenu XML généré
     * @throws DechiffrementExecutionException
     *
     */
    public byte[] dechiffrer(String contenuFichierXML) throws DechiffrementExecutionException;
}
