package fr.atexo.signature.commun.securite.processor.signature;

import fr.atexo.json.reponse.Repertoire;

import java.util.HashSet;
import java.util.Set;

/**
 * interface exposant les propriétés contenant les informations sur le certificat
 * utilisé.
 */
public interface InfosComplementairesCertificat {

    String getSignataireComplet();

    void setSignataireComplet(String signataireComplet);

    String getSignatairePartiel();

    void setSignatairePartiel(String signatairePartiel);

    String getEmetteur();

    void setEmetteur(String emetteur);

    String getDateValiditeDu();

    void setDateValiditeDu(String dateValiditeDu);

    String getDateValiditeAu();

    void setDateValiditeAu(String dateValiditeAu);

    Boolean getDateSignatureValide();

    void setDateSignatureValide(Boolean dateSignatureValide);

    Boolean getPeriodiciteValide();

    void setPeriodiciteValide(Boolean periodiciteValide);

    Boolean getChaineDeCertificationValide();

    void setChaineDeCertificationValide(Boolean chaineDeCertificationValide);

    Boolean getAbsenceRevocationCRL();

    void setAbsenceRevocationCRL(Boolean absenceRevocationCRL);

    Boolean getSignatureValide();

    void setSignatureValide(Boolean signatureValide);

    Set<Repertoire> getRepertoiresChaineCertification();

    void setRepertoiresChaineCertification(Set<Repertoire> repertoiresChaineCertification);

    Set<Repertoire> getRepertoiresRevocation();

    void setRepertoiresRevocation(Set<Repertoire> repertoiresRevocation);
}
