package fr.atexo.signature.gui.provider.pkcs12.event;

import java.util.EventObject;

/**
 * Evenement qui est lancé lors de la validation de la sélection du du certificat de type PKCS12.
 * Cet évenement contient le chemin sur disque pour accèder au certificat ainsi que le mot de passe associé permettant
 * d'accèder au contenu du certificat.
 */
public class Pkcs12CertificateEvent extends EventObject {

    private static final long serialVersionUID = -5224024987900710300L;
    private String cheminFichierP12;
    private String motDePasseFichierP12;

    public Pkcs12CertificateEvent(Object source, String cheminFichierP12, String motDePasseFichierP12) {
        super(source);
        this.cheminFichierP12 = cheminFichierP12;
        this.motDePasseFichierP12 = motDePasseFichierP12;
    }

    public String getCheminFichierP12() {
        return this.cheminFichierP12;
    }

    public String getMotDePasseFichierP12() {
        return this.motDePasseFichierP12;
    }
}
