package fr.atexo.signature.logging;

import fr.atexo.signature.commun.util.Util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 */
public final class SingleLineLogFormatter extends Formatter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private boolean utiliserNomClasse;

    private boolean utiliserNomMethode;

    public SingleLineLogFormatter() {
        this(false, false);
    }

    public SingleLineLogFormatter(boolean utiliserNomClasse, boolean utiliserNomMethode) {
        this.utiliserNomClasse = utiliserNomClasse;
        this.utiliserNomMethode = utiliserNomMethode;
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder stringBuilder = new StringBuilder();

        // affichage de la date
        Date date = new Date(record.getMillis());
        String dateFormat = Util.formaterDate(date, Util.DATE_TIME_PATTERN_TIRET);
        stringBuilder.append(dateFormat).append(" ");

        // affichage du level de LOG
        stringBuilder.append(record.getLevel().getLocalizedName()).append(" ");

        // affichage du nom de la classe
        if (isUtiliserNomClasse() && record.getSourceClassName() != null) {
            stringBuilder.append("[").append(record.getSourceClassName()).append("]").append(" ");
            if (isUtiliserNomMethode() && record.getSourceMethodName() != null) {
                stringBuilder.append("[").append(record.getSourceMethodName()).append("]").append(" ");
            }

        } else {
            stringBuilder.append("[").append(record.getLoggerName()).append("]").append(" ");
        }

        stringBuilder.append(": ").append(formatMessage(record)).append(LINE_SEPARATOR);

        if (record.getThrown() != null) {
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                record.getThrown().printStackTrace(pw);
                pw.close();
                stringBuilder.append(sw.toString());
            } catch (Exception ex) {
                // ignore
            }
        }

        return stringBuilder.toString();
    }

    public boolean isUtiliserNomClasse() {
        return utiliserNomClasse;
    }

    public boolean isUtiliserNomMethode() {
        return utiliserNomMethode;
    }
}
