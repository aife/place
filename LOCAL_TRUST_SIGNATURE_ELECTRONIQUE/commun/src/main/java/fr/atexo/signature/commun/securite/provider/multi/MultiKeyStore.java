package fr.atexo.signature.commun.securite.provider.multi;

import java.security.KeyStore;
import java.security.Provider;
import java.util.Map;

/**
 *
 */
public class MultiKeyStore extends KeyStore {

    MultiKeyStoreSpi keyStoreSpi;

    public MultiKeyStore(final Map<String, KeyStore> keyStores) {
        this(new MultiKeyStoreSpi(keyStores));
    }

    public MultiKeyStore(final MultiKeyStoreSpi keyStoreSpi) {
        super(keyStoreSpi, null, null);
        this.keyStoreSpi = keyStoreSpi;
    }

    public final String getProviderName(final String alias) {
        return keyStoreSpi.getProviderName(alias);
    }

}
