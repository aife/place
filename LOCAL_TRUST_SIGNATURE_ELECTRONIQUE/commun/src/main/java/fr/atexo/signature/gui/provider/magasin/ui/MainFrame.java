package fr.atexo.signature.gui.provider.magasin.ui;

import fr.atexo.signature.commun.securite.provider.CertificatItem;
import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.gui.provider.AbstractMainFrame;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateEvent;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * @author Thibaut Decaudain
 */
public class MainFrame extends AbstractMainFrame<MagasinCertificateEvent> implements ActionListener {

    private static final long serialVersionUID = 1L;

    private List<CertificatItem> certificateItems;
    public static final int width = 765;
    public static final int height = 300;

    private JTable table;
    private JButton validerButton;
    private JButton annulerButton;

    /**
     * Crée une nouvelle fenetre popup affichant un tableau de certificats
     * Un clic sur Valider lance la methode certificateChosenEvent()
     * des listeners de cette popup
     *
     * @param certificatItems
     */
    public MainFrame(List<CertificatItem> certificatItems) {
        super();
        this.certificateItems = certificatItems;
        this.build();
        this.setVisible(true);

    }

    private void build() {
        setTitle(I18nUtil.get("SWING_MSCAPI_LISTE_TITRE"));
        setSize(width, height);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setContentPane(buildContentPane());
    }

    private JPanel buildContentPane() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        // Ajout de la JTable des certificats
        TableModel tableModel = new TableModel(new String[]{I18nUtil.get("SWING_MSCAPI_LISTE_NOM_CERTIFICAT"), I18nUtil.get("SWING_MSCAPI_LISTE_NOM_EMETTEUR"), I18nUtil.get("SWING_MSCAPI_LISTE_DATE_EXPIRATION"), I18nUtil.get("SWING_MSCAPI_LISTE_UTILISATION_CLE")}, certificateItems);
        table = new JTable();
        table.setModel(tableModel);
        table.setRowSelectionAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        TableColumn column = table.getColumnModel().getColumn(2);
        column.setPreferredWidth(100);
        TableColumn column1 = table.getColumnModel().getColumn(1);
        column1.setPreferredWidth(120);
        TableColumn column2 = table.getColumnModel().getColumn(0);
        column2.setPreferredWidth(175);
        TableColumn column3 = table.getColumnModel().getColumn(3);
        column3.setPreferredWidth(370);

        JScrollPane scroller = new JScrollPane(table);
        scroller.setVerticalScrollBar(new JScrollBar());
        mainPanel.add(scroller, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BorderLayout());
        validerButton = new JButton(I18nUtil.get("SWING_ACTION_VALIDER"));
        validerButton.addActionListener(this);
        annulerButton = new JButton(I18nUtil.get("SWING_ACTION_ANNULER"));
        annulerButton.addActionListener(this);
        bottomPanel.add(annulerButton, BorderLayout.WEST);
        bottomPanel.add(validerButton, BorderLayout.EAST);

        mainPanel.add(bottomPanel, BorderLayout.PAGE_END);

        return mainPanel;
    }


    /**
     * Methode appelé lors du clique sur le bouton valider
     * Si validé : on envoie a tous les CertificateEventListener
     * l'événement CertificateEvent contenant le CertificateItem sélectionné
     */
    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();

        if (source == annulerButton) {
            this.dispose();
            return;
        }

        int index = table.getSelectedRow();
        if (index >= 0) {
            MagasinCertificateEvent event = new MagasinCertificateEvent(this, certificateItems.get(index));

            // Vérification du certificat choisi
            CertificatItem item = certificateItems.get(index);
            if (Util.isExpire(item.getDateExpiration())) {
                int userClic = JOptionPane.showConfirmDialog(this, I18nUtil.get("SWING_ACTION_ALERTE_PAR_DEFAUT_CERTIFICAT_PERIME"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.YES_NO_OPTION);
                if (userClic != JOptionPane.YES_OPTION) {
                    return; // stop
                }
            }
            this.fireCertificateEvent(event);
            this.dispose();

        } else {
            JOptionPane.showMessageDialog(this, I18nUtil.get("SWING_ACTION_ALERTE_SELECTIONNER_CERTIFICAT"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.WARNING_MESSAGE);
        }
    }
}

