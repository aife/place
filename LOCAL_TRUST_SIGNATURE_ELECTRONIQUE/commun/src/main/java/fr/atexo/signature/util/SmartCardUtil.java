package fr.atexo.signature.util;

import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.pkcs11.smartcard.SmartCardInformation;
import fr.atexo.signature.xml.pkcs11.LibType;
import fr.atexo.signature.xml.pkcs11.LibsType;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibType;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class SmartCardUtil {

    public static List<SmartCardInformation> getSmartCardInformations(Pkcs11LibsType pkcs11Libs, TypeOs typeOs) {

        List<SmartCardInformation> smartCardInformations = new ArrayList<SmartCardInformation>();

        for (Pkcs11LibType pkcs11Lib : pkcs11Libs.getPkcs11Lib()) {

            LibsType libsType = null;
            switch (typeOs) {
                case Windows:
                    libsType = pkcs11Lib.getLibsWindows();
                    break;
                case MacOs:
                    libsType = pkcs11Lib.getLibsMacOs();
                    break;
                case Linux:
                    libsType = pkcs11Lib.getLibsLinux();
                    break;
            }

            try {

                if (libsType != null) {

                    for (LibType libType : libsType.getLib()) {
                        if (libType.getChemin() != null) {
                            File fichierLib = new File(libType.getChemin());
                            if (fichierLib != null && fichierLib.exists()) {
                                SmartCardInformationBean smartCardInformationBean = genererSmartCardInformationBean(null, fichierLib, pkcs11Lib.getFournisseur());
                                smartCardInformations.add(smartCardInformationBean);
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return smartCardInformations;
    }

    public static Map<String, List<File>> getPkcs11Providers(Pkcs11LibsType pkcs11Libs, TypeOs typeOs) {
        Map<String, List<File>> mapProviders = new HashMap<String, List<File>>();

        for (Pkcs11LibType pkcs11Lib : pkcs11Libs.getPkcs11Lib()) {

            LibsType libsType = null;
            switch (typeOs) {
                case Windows:
                    libsType = pkcs11Lib.getLibsWindows();
                    break;
                case MacOs:
                    libsType = pkcs11Lib.getLibsMacOs();
                    break;
                case Linux:
                    libsType = pkcs11Lib.getLibsLinux();
                    break;
            }

            if (libsType != null) {

                for (LibType libType : libsType.getLib()) {
                    if (libType.getChemin() != null) {
                        File fichierLib = new File(libType.getChemin());
                        if (fichierLib != null && fichierLib.exists()) {

                            List<File> fichiers = mapProviders.get(pkcs11Lib.getFournisseur());
                            if (fichiers == null) {
                                fichiers = new ArrayList<File>();
                            }
                            fichiers.add(fichierLib);
                            mapProviders.put(pkcs11Lib.getFournisseur(), fichiers);
                        }
                    }
                }
            }
        }

        return mapProviders;
    }

    private static SmartCardInformationBean genererSmartCardInformationBean(String atr, File librairie, String nomFabriquant) {
        SmartCardInformationBean smartCardInformationBean = new SmartCardInformationBean();
        smartCardInformationBean.setAtr(atr);
        smartCardInformationBean.setLibrairie(librairie);
        smartCardInformationBean.setNomFabriquant(nomFabriquant);
        return smartCardInformationBean;
    }
}
