package fr.atexo.signature.commun.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class MapUtil<C, T> {

    public void ajouter(Map<C, List<T>> conteneur, C clef, T valeur) {
        // vérification que le conteneur est bien instancié
        if (conteneur == null) {
            conteneur = new HashMap<C, List<T>>();
        }
        if (valeur != null) {
            List<T> fichiers = conteneur.get(clef);
            if (fichiers == null) {
                fichiers = new ArrayList<T>();
                conteneur.put(clef, fichiers);
                fichiers.add(valeur);
            } else {
                fichiers.add(valeur);
            }
        }
    }

    private void deplacer(Map<C, List<T>> conteneurSource, Map<C, List<T>> conteneurDestination, C clef, T valeur) {
        // vérification que les conteneurs sont bien instanciés
        if (conteneurSource == null) {
            conteneurSource = new HashMap<C, List<T>>();
        }
        if (conteneurDestination == null) {
            conteneurDestination = new HashMap<C, List<T>>();
        }

        if (valeur != null) {
            List<T> fichiers = conteneurSource.get(clef);
            if (fichiers != null) {
                fichiers.remove(valeur);
            }
            ajouter(conteneurDestination, clef, valeur);
        }
    }
}
