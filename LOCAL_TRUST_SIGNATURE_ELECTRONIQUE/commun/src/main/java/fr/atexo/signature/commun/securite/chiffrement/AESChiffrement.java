package fr.atexo.signature.commun.securite.chiffrement;

import org.bouncycastle.crypto.engines.AESLightEngine;

import java.security.cert.X509Certificate;
import java.util.List;

/**
 *
 */
public class AESChiffrement extends AbstractChiffrement {

    private static final int AES_KEY_LENGTH = 16; // 16 bytes for AES-128

    public AESChiffrement(List<X509Certificate> certificats) {
        super(certificats, new AESLightEngine(), AES_KEY_LENGTH);
    }
}
