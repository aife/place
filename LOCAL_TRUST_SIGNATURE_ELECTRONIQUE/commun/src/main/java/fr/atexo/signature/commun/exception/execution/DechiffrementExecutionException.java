package fr.atexo.signature.commun.exception.execution;

/**
 * Exception pour signaler un souci lors du déchiffrement.
 */
public class DechiffrementExecutionException extends ExecutionException {

    public DechiffrementExecutionException(String message) {
        super(message);
    }

    public DechiffrementExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
