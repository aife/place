package fr.atexo.signature.commun.securite.provider.bouncycastle;

import fr.atexo.signature.commun.securite.provider.AbstractKeyStoreHandler;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Provider;
import java.security.Security;

/**
 * Classe permettant de gérer les certificats se trouvant directement dans le provider de Bouncy Castle.
 */
public class BouncyCaslteHandler extends AbstractKeyStoreHandler {

    public static Provider verifierPresenceEtRecupererProvider() {

        Provider provider = Security.getProvider(TypeProvider.BC.getNom());
        if (provider == null) {
            provider = new BouncyCastleProvider();
            Security.addProvider(provider);
        }

        return provider;
    }
}
