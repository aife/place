package fr.atexo.signature.commun.securite.certificat.verification;

/**
 *
 */
public interface CertificatCodeRetour {

    int getCodeRetour();
}