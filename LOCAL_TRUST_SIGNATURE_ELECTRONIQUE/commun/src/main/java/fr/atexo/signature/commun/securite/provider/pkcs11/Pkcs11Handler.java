package fr.atexo.signature.commun.securite.provider.pkcs11;

import com.sun.security.auth.callback.DialogCallbackHandler;
import fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException;
import fr.atexo.signature.commun.securite.provider.AbstractKeyStoreHandler;
import fr.atexo.signature.commun.securite.provider.CertificatItem;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.multi.MultiKeyStore;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.logging.LogManager;
import sun.security.pkcs11.SunPKCS11;

import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * Classe permettant de gérer les certificats se trouvant directement
 * dans une smartcard et nécessitant d'utiliser des librairies du fournisseur de la
 * smartcard qui implémentent la norme pkcs11.
 */
public class Pkcs11Handler extends AbstractKeyStoreHandler {

    private static Pkcs11Handler pkcs11Handler;

    private MultiKeyStore keyStore;

    public static Pkcs11Handler getInstance() {
        if (pkcs11Handler == null) {
            pkcs11Handler = new Pkcs11Handler();
        }
        return pkcs11Handler;
    }

    public MultiKeyStore getKeyStore(Map<String, List<File>> providers) throws RecuperationCertificatException {

        Map<String, KeyStore> keyStores = new TreeMap<String, KeyStore>();
        CallbackHandler callbackHandler = new DialogCallbackHandler();
        for (Map.Entry<String, List<File>> entry : providers.entrySet()) {
            String name = entry.getKey();

            for (File library : entry.getValue()) {
                LogManager.getInstance().afficherMessageInfo("Recherche de la librairie : " + library.getAbsolutePath(), this.getClass());
                if (library.exists()) {
                    AuthProvider provider;
                    try {
                        provider = new SunPKCS11(new ByteArrayInputStream(("name=" + name + "\n" + "library=" + library + "\n" + "attributes=compatibility").getBytes()));
                    }
                    catch (ProviderException e) {
                        continue;
                    }
                    provider.setCallbackHandler(callbackHandler);
                    AuthProvider ancienProvider = (AuthProvider) Security.getProvider(provider.getName());
                    if (ancienProvider != null) {
                        LogManager.getInstance().afficherMessageInfo("Suppression du provider : " + ancienProvider.getName(), this.getClass());
                        try {
                            ancienProvider.logout();
                        } catch (LoginException e) {
                            e.printStackTrace();
                        }
                        Security.removeProvider(ancienProvider.getName());
                    }
                    int numero = Security.addProvider(provider);
                    LogManager.getInstance().afficherMessageInfo("Ajout du provider : " + provider.getName() + " en position : " + numero, this.getClass());
                    try {
                        KeyStore keyStore = KeyStore.getInstance(TypeProvider.PKCS11.getType(), provider);
                        keyStores.put(name, keyStore);
                    }
                    catch (KeyStoreException e) {
                        e.printStackTrace();
                        continue;
                    }
                    break;
                }
            }
        }

        MultiKeyStore keyStore = new MultiKeyStore(keyStores);

        try {
            keyStore.load(null, null);
            return keyStore;
        } catch (NoSuchAlgorithmException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + TypeProvider.PKCS11, e);
        } catch (CertificateException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider " + TypeProvider.PKCS11, e);
        } catch (IOException e) {
            if (e.getCause() instanceof FailedLoginException) {
                throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider  " + TypeProvider.PKCS11 + " car le pin est incorrect", e);
            } else {
                throw new RecuperationCertificatException("Erreur lors du chargement du key store du provider" + TypeProvider.PKCS11, e);
            }

        }
    }

    /**
     * Retourne la liste des certificats du Magasin de Windows / Mac Os
     *
     * @param providers l'ensemble des providers pkcs11 trouvés
     * @param hashCodes liste des hashs de certificat déjà intégré
     * @return la liste des certifcats personnels de l'utilisateur.
     * @throws RecuperationCertificatException
     *
     */
    public List<CertificatItem> recupererCertificats(Map<String, List<File>> providers, Set<String> hashCodes, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificats) throws RecuperationCertificatException {

        // chargement depuis le KeyStore
        keyStore = getKeyStore(providers);

        return super.recupererCertificats(TypeProvider.PKCS11, keyStore, hashCodes, true, filtrerSignatureModeRGS, typeCertificats);
    }

    /**
     * Retour le keyPair à partir du provider et de l'alias.
     *
     * @param alias l'alias à recherche depuis le Keystore
     * @return le keyPair
     * @throws RecuperationCertificatException
     *
     */
    public fr.atexo.signature.commun.securite.certificat.KeyPair getKeyPair(String alias) throws RecuperationCertificatException {

        try {
            // parcourt de l'ensemble des occurances contenus dans le KeyStore et récupération du bit-clé sélectionné
            Enumeration<String> aliases = keyStore.aliases();
            while (aliases.hasMoreElements()) {
                String aliasKey = aliases.nextElement();
                //System.out.println("aliasKey => " + aliasKey);
                if (aliasKey.equals(alias)) {
                    X509Certificate certificat = (X509Certificate) keyStore.getCertificate(aliasKey);
                    // dans le cas d'apple il est nécessaire pour accéder à la clef privée de mettre un mot de passe bidon
                    PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, " ".toCharArray());
                    String providerName = keyStore.getProviderName(aliasKey);
                    fr.atexo.signature.commun.securite.certificat.KeyPair keyPair = new fr.atexo.signature.commun.securite.certificat.KeyPair(certificat, privateKey, TypeProvider.PKCS11, providerName);
                    return keyPair;
                }
            }

        } catch (KeyStoreException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération des alias se trouvant dans le key store du provider " + TypeProvider.PKCS11, e);
        } catch (UnrecoverableKeyException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11 + " pour l'alias " + alias, e);
        } catch (NoSuchAlgorithmException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération de la clé privée se trouvant dans le key store du provider " + TypeProvider.PKCS11 + " pour l'alias " + alias, e);
        }

        return null;
    }
}
