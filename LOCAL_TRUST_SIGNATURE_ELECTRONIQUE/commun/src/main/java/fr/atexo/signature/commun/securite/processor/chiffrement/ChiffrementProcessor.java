package fr.atexo.signature.commun.securite.processor.chiffrement;

import fr.atexo.signature.commun.exception.execution.ChiffrementExecutionException;

import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Interface exposant les méthodes permettant de chiffrer.
 */
public interface ChiffrementProcessor {

    /**
     * Execute le chiffrement du fichier.
     *
     * @param contenu le contenu à chiffrer
     * @return le contenu XML généré
     * @throws ChiffrementExecutionException
     */
    byte[] chiffrer(byte[] contenu) throws ChiffrementExecutionException;
}
