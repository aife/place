package fr.atexo.signature.commun.exception.execution;

/**
 *
 */
public class CertificatDechiffrementIntrouvableException extends DechiffrementExecutionException {

    public CertificatDechiffrementIntrouvableException(String message) {
        super(message);
    }

    public CertificatDechiffrementIntrouvableException(String message, Throwable cause) {
        super(message, cause);
    }
}
