package fr.atexo.signature.commun.securite.signature;

import org.apache.jcp.xml.dsig.internal.dom.ApacheNodeSetData;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.crypto.*;
import javax.xml.crypto.dom.DOMURIReference;

/**
 *
 */
public class TagNameURIDereferencer implements URIDereferencer {

    private Document parent;

    private String tagName;

    public TagNameURIDereferencer(Document parent, String prefix, String tag) {
        this.parent = parent;
        if (prefix != null) {
            tagName = prefix + ":" + tag;
        } else {
            tagName = tag;
        }
    }

    public Data dereference(URIReference uriRef, XMLCryptoContext context) throws URIReferenceException {

        if (uriRef == null) {
            throw new NullPointerException("uriRef cannot be null");
        }
        if (context == null) {
            throw new NullPointerException("context cannot be null");
        }

        DOMURIReference domRef = (DOMURIReference) uriRef;
        Attr uriAttr = (Attr) domRef.getHere();
        String uri = uriRef.getURI();
        String baseURI = context.getBaseURI();

        if (uri != null && uri.length() != 0 && uri.charAt(0) == '#') {
            String id = uri.substring(1);

            NodeList nodeListSignedProperties = parent.getElementsByTagName(tagName);
            if (nodeListSignedProperties != null) {

                Element element = (Element) nodeListSignedProperties.item(0);
                String attributId = element.getAttribute("Id");
                if (attributId.equals(id)) {
                    XMLSignatureInput result = new XMLSignatureInput(element);
                    if (!uri.substring(1).startsWith("xpointer(id(")) {
                        result.setExcludeComments(true);
                    }
                    result.setMIMEType("text/xml");
                    if (baseURI != null && baseURI.length() > 0) {
                        result.setSourceURI(baseURI.concat(uriAttr.getNodeValue()));
                    } else {
                        result.setSourceURI(uriAttr.getNodeValue());
                    }
                    return new ApacheNodeSetData(result);
                }
            }
        }

        return null;
    }
}
