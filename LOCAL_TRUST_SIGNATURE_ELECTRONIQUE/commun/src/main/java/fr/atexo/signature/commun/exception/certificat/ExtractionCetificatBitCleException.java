package fr.atexo.signature.commun.exception.certificat;

/**
 * Exception pour signaler un souci lors de l'extraction du bit-clé d'un certificat.
 */
public class ExtractionCetificatBitCleException extends ManipulationCertificatException {

    public ExtractionCetificatBitCleException(String message) {
        super(message);
    }

    public ExtractionCetificatBitCleException(String message, Throwable cause) {
        super(message, cause);
    }
}
