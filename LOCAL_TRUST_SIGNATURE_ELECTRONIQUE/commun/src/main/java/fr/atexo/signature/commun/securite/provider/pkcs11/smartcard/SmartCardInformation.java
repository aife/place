package fr.atexo.signature.commun.securite.provider.pkcs11.smartcard;

import java.io.File;

/**
 *
 */
public interface SmartCardInformation {

    String getAtr();

    File getLibrairie();

    String getNomFabriquant();
}
