package fr.atexo.signature.gui.browser.event;

import java.util.EventObject;


public class SelectDirectoryEvent extends EventObject {

    private static final long serialVersionUID = -5224024987900710300L;

    private String pathDirectory;

    public SelectDirectoryEvent(Object source, String pathDirectory) {
        super(source);
        this.pathDirectory = pathDirectory;
    }

    public String getPathDirectory() {
        return this.pathDirectory;
    }
}
