package fr.atexo.signature.commun.exception.certificat;

/**
 * Exception pour signaler un souci lors de la récupération de la clé privée d'un certificat.
 */
public class RecuperationClePriveCertificatException extends RecuperationCertificatException {

    public RecuperationClePriveCertificatException(String message) {
        super(message);
    }

    public RecuperationClePriveCertificatException(String message, Throwable cause) {
        super(message, cause);
    }
}
