package fr.atexo.signature.commun.securite.certificat.verification;

/**
 *
 */
public enum CertificatValiditeEtat implements CertificatCodeRetour {

    Valide(0), Expire(2), PasEncoreValide(5), Inconnu(1);

    private int codeRetour;

    private CertificatValiditeEtat(int codeRetour) {
        this.codeRetour = codeRetour;
    }

    public int getCodeRetour() {
        return codeRetour;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CertificatValiditeEtat");
        sb.append("{type=").append(name());
        sb.append("{codeRetour=").append(codeRetour);
        sb.append('}');
        return sb.toString();
    }
}
