package fr.atexo.signature.commun.securite.signature;

import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.securite.processor.signature.InfosVerificationCertificat;
import fr.atexo.signature.commun.securite.provider.AbstractKeyStoreHandler;
import fr.atexo.signature.commun.securite.provider.bouncycastle.BouncyCaslteHandler;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.securite.provider.pkcs11.Pkcs11Handler;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.io.HashUtil;
import net.iharder.Base64;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.*;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.cert.*;
import java.security.Provider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Classe permettant de gérer la signature Pkcs7 d'un fichier.
 */
public class SignaturePkcs7 {

    /**
     * Fonction qui effectue une signature pkcs7.
     *
     * @param keyPair           le certificat et sa clef privée associée avec lequel signer les données
     * @param typeAlgorithmHash le type d'algorithm à utiliser
     * @param donnees           les données à signer
     * @param detachee          <code>true</code> si la signature doit être détachée, sinon <code>false</code>
     * @return
     * @throws Exception
     */
    public static byte[] signer(KeyPair keyPair, TypeAlgorithmHash typeAlgorithmHash, byte[] donnees, boolean detachee) throws Exception {

        Provider providerCertStore = null;
        Provider providerSignature = null;
        switch (keyPair.getProvider()) {
            case MSCAPI:
            case APPLE:
            case PKCS11:
                providerSignature = AbstractKeyStoreHandler.recupererProvider(keyPair.getNomProvider());
                providerCertStore = BouncyCaslteHandler.verifierPresenceEtRecupererProvider();
                break;
            default:
                providerCertStore = BouncyCaslteHandler.verifierPresenceEtRecupererProvider();
                providerSignature = providerCertStore;
                break;
        }

        CMSTypedData typedData = new CMSProcessableByteArray(donnees);

        List<X509Certificate> certList = new ArrayList<X509Certificate>();
        certList.add(keyPair.getCertificate());
        Store certs = new JcaCertStore(certList);

        JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(typeAlgorithmHash.getSignatureAlgorithm()).setProvider(providerSignature);
        ContentSigner sha1Signer = contentSignerBuilder.build(keyPair.getPrivateKey());

        JcaDigestCalculatorProviderBuilder digestCalculatorProviderBuilder = new JcaDigestCalculatorProviderBuilder();
        digestCalculatorProviderBuilder.setProvider(providerCertStore);
        DigestCalculatorProvider digestCalculatorProvider = digestCalculatorProviderBuilder.build();

        JcaSignerInfoGeneratorBuilder signerInfoGeneratorBuilder = new JcaSignerInfoGeneratorBuilder(digestCalculatorProvider);
        SignerInfoGenerator signerInfoGenerator = signerInfoGeneratorBuilder.build(sha1Signer, keyPair.getCertificate());

        CMSSignedDataGenerator signedDataGenerator = new CMSSignedDataGenerator();
        signedDataGenerator.addSignerInfoGenerator(signerInfoGenerator);
        signedDataGenerator.addCertificates(certs);

        CMSSignedData signedData = signedDataGenerator.generate(typedData, detachee);
        return signedData.getEncoded();
    }

    public static byte[] signerHash(KeyPair keyPair, TypeAlgorithmHash typeAlgorithmHash, String hash) throws Exception {
        byte[] hashEnByte = HashUtil.convertirHexadecimalEnBinaire(hash);
        return signer(keyPair, typeAlgorithmHash, hashEnByte, true);
    }

    public static InfosComplementairesCertificat verifierHash(File fichier, String contenuFichierSignatureEnBase64, TypeAlgorithmHash typeAlgorithmHash) throws IOException {

        InfosComplementairesCertificat infosComplementairesCertificat = null;
        String hashFichier = HashUtil.genererHashShaHexadecimal(typeAlgorithmHash, fichier);
        byte[] hashEnByte = HashUtil.convertirHexadecimalEnBinaire(hashFichier);
        try {
            Provider provider = BouncyCaslteHandler.verifierPresenceEtRecupererProvider();

            byte[] contenuFichierSignature = Base64.decode(contenuFichierSignatureEnBase64);

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(contenuFichierSignature);
            ASN1InputStream asn1InputStream = new ASN1InputStream(byteArrayInputStream);
            byteArrayInputStream.close();
            CMSProcessableByteArray contentToSign = new CMSProcessableByteArray(hashEnByte);
            CMSSignedData verifData = new CMSSignedData(contentToSign, ContentInfo.getInstance(asn1InputStream.readObject()));
            asn1InputStream.close();

            CertStore certValidationStore = verifData.getCertificatesAndCRLs("Collection", provider.getName());
            SignerInformationStore signerInfoStore = verifData.getSignerInfos();
            Collection collec = signerInfoStore.getSigners();
            Iterator it = collec.iterator();
            while (it.hasNext()) {
                SignerInformation signerInformation = (SignerInformation) it.next();
                Collection certificates = certValidationStore.getCertificates(signerInformation.getSID());
                Iterator iterator = certificates.iterator();
                X509Certificate x509Certificat = (X509Certificate) iterator.next();

                if (x509Certificat.getBasicConstraints() == -1) {
                    infosComplementairesCertificat = CertificatUtil.extraireInformations(x509Certificat, new InfosVerificationCertificat());
                    if (signerInformation.verify(x509Certificat, provider.getName())) {
                        infosComplementairesCertificat.setSignatureValide(true);
                    } else {
                        infosComplementairesCertificat.setSignatureValide(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return infosComplementairesCertificat;

    }
}
