package fr.atexo.signature.gui.barreprogression;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class BarreDeProgressionSimple extends Frame implements ChangeListener {

    private static final long serialVersionUID = 1L;
    public JProgressBar barreDeProgression;
    public String message = "";

    public BarreDeProgressionSimple() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        barreDeProgression = new JProgressBar(0, 99);
        barreDeProgression.setStringPainted(true);
        barreDeProgression.addChangeListener(this);
        add(barreDeProgression);

        setBounds(320, 240, 500, 45);
        setVisible(true);

    }

    public void stateChanged(ChangeEvent e) {
        barreDeProgression.setString(message + " " + (int) (barreDeProgression.getPercentComplete() * 100) + "%");
    }

    public JProgressBar getBarreDeProgression() {
        return barreDeProgression;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
