package fr.atexo.signature.commun.securite.certificat;

import java.io.File;
import java.security.cert.X509CRL;

/**
 *
 */
public class InfosCrl extends AbstractInfos {

    private X509CRL crl;

    public InfosCrl(X509CRL crl) {
        this(crl, null);
    }

    public InfosCrl(X509CRL crl, File fichierDisque) {
        this.crl = crl;
        this.fichierDisque = fichierDisque;
    }

    public X509CRL getCrl() {
        return crl;
    }
}
