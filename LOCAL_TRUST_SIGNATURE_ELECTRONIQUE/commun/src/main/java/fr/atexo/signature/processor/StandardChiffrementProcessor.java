package fr.atexo.signature.processor;

import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException;
import fr.atexo.signature.commun.exception.execution.CertificatDechiffrementIntrouvableException;
import fr.atexo.signature.commun.exception.execution.ChiffrementExecutionException;
import fr.atexo.signature.commun.exception.execution.DechiffrementExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.chiffrement.AESChiffrement;
import fr.atexo.signature.commun.securite.chiffrement.AbstractChiffrement;
import fr.atexo.signature.commun.securite.chiffrement.ClefIV;
import fr.atexo.signature.commun.securite.chiffrement.DonneesXML;
import fr.atexo.signature.commun.securite.chiffrement.EnveloppeXML;
import fr.atexo.signature.commun.securite.chiffrement.InfosDechiffrement;
import fr.atexo.signature.commun.securite.processor.chiffrement.ChiffrementProcessor;
import fr.atexo.signature.commun.securite.processor.chiffrement.DechiffrementProcessor;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.logging.LogManager;

/**
 * Classe effectuant le chiffrement d'un fichier.
 */
public class StandardChiffrementProcessor implements ChiffrementProcessor, DechiffrementProcessor {

	private TypeProvider typeProvider;

	private KeyPair keyPair;

	private AbstractChiffrement chiffrement;

	/**
	 * Constructeur à utiliser lors du chiffrement.
	 * 
	 * @param certificats
	 *            la liste des certificats avec lesquels chiffrer le contenu
	 */
	public StandardChiffrementProcessor(List<X509Certificate> certificats) {
		chiffrement = new AESChiffrement(certificats);
	}

	/**
	 * Constructeur à utiliser lors du déchiffrement et si il s'agit d'un OS sur
	 * lequel un magasin de certificat est disponible (Windows / Mac)
	 * 
	 * @param typeProvider
	 *            le type de provider
	 */
	public StandardChiffrementProcessor(TypeProvider typeProvider) {
		this.typeProvider = typeProvider;
	}

	/**
	 * Constructeur à utiliser lors du déchiffrement et si il s'agit d'un OS sur
	 * lequel aucun magasin de certificat est disponible (autre que Windows /
	 * Mac)
	 * 
	 * @param typeProvider
	 *            le type de provider
	 * @param keyPair
	 *            la clef à utiliser pour le déchiffrement
	 */
	public StandardChiffrementProcessor(TypeProvider typeProvider, KeyPair keyPair) {
		this(typeProvider);
		this.keyPair = keyPair;
	}

	@Override
	public byte[] chiffrer(byte[] contenu) throws ChiffrementExecutionException {
		try {
			byte[] donnees = chiffrement.chiffrer(contenu);
			List<ClefIV> clefIvs = chiffrement.getClefIVs();
			String enveloppeXML = EnveloppeXML.creerEnveloppeXml(donnees, clefIvs, contenu.length);

			return enveloppeXML.getBytes();

		} catch (Exception e) {
			throw new ChiffrementExecutionException("Un problème est survenu lors de la tentative de chiffrement du contenu d'un fichier", e);
		}
	}

	@Override
	public byte[] dechiffrer(String contenuFichierXML) throws DechiffrementExecutionException {

		// parse l'enveloppe xml pour récupérer les informations
		DonneesXML donneesXML = null;
		try {
			donneesXML = EnveloppeXML.chargerEnveloppeXml(contenuFichierXML);
		} catch (Exception e) {
			throw new DechiffrementExecutionException("Un problème est survenu lors de la tentative de chargement de l'enveloppe xml à déchiffrer", e);
		}

		// parse l'enveloppe xml pour récupérer les informations
		InfosDechiffrement infosDechiffrement = null;
		AESChiffrement chiffrementSymetrique = null;
		try {
			if (donneesXML != null) {
				chiffrementSymetrique = new AESChiffrement(null);
				if (typeProvider != null && keyPair == null) {
					// cas accès depuis magasin
					infosDechiffrement = getInfosDechiffrementDepuisMagasin(typeProvider, donneesXML.getClefIVs());
				} else {
					// cas pkcs12
					infosDechiffrement = getInfosDechiffrementDepuisPkcs12(keyPair, donneesXML.getClefIVs());
				}
			}
		} catch (Exception e) {
			throw new DechiffrementExecutionException("Impossible d'extraire / récupérer le certificat nécessaire au déchiffrement de l'enveloppe xml : " + extraireCertificatInformations(donneesXML), e);
		}

		if (infosDechiffrement == null) {
			throw new CertificatDechiffrementIntrouvableException("Impossible d'accéder à au moins l'un des bi-clés attendus pour le déchiffrement des plis de la consultation. Veuillez vous assurer qu'au moins l'un des certificats suivants soit installé sur votre poste : \n"
					+ extraireCertificatInformations(donneesXML));
		} else {
			byte[] donnees = chiffrementSymetrique.dechiffrer(donneesXML.getDonnees(), donneesXML.getTaille(), infosDechiffrement);
			return donnees;
		}
	}

	private String extraireCertificatInformations(DonneesXML donneesXML) {

		List<String> certificats = new ArrayList<String>();
		if (donneesXML != null) {
			for (ClefIV clefIV : donneesXML.getClefIVs()) {
				if (clefIV.getCertificat() != null) {
					String signataire = CertificatUtil.getCN(clefIV.getCertificat().getSubjectX500Principal());
					String emetteur = CertificatUtil.getCN(clefIV.getCertificat().getIssuerX500Principal());
					String infos = "Émis à : " + signataire + " / Émis par : " + emetteur + " / Expire le : " + Util.formaterDate(clefIV.getCertificat().getNotAfter(), Util.DATE_TIME_PATTERN_TIRET_SANS_SECONDES);
					certificats.add(infos);
				}
			}
		}

		StringBuilder stringBuilder = new StringBuilder();

		for (int i = 0; i < certificats.size(); i++) {
			String info = certificats.get(i);
			stringBuilder.append("- ");
			stringBuilder.append(info);
			if (i != certificats.size() - 1) {
				stringBuilder.append("\n");
			}
		}

		return stringBuilder.toString();
	}

	private InfosDechiffrement getInfosDechiffrementDepuisMagasin(TypeProvider typeProvider, List<ClefIV> clefIVs) throws RecuperationCertificatException {

		for (ClefIV clefIV : clefIVs) {

			X509Certificate certificat = clefIV.getCertificat();
			if (certificat != null) {
				BigInteger serial = certificat.getSerialNumber();
				String signataire = certificat.getSubjectDN().toString();
				String emetteur = certificat.getIssuerDN().toString();

				LogManager.getInstance().afficherMessageInfo("Recherche dans le magasin de certificats du signataire " + signataire + " à partir de son emetteur  " + emetteur + " et son serial " + serial, this.getClass());
				KeyPair keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, serial, emetteur);
				LogManager.getInstance().afficherMessageInfo("Le certificat a été trouvé dans le magasin et utilisé pour effectuer le déchiffrement", this.getClass());
				if (keyPair != null && keyPair.getPrivateKey() != null) {
					return new InfosDechiffrement(keyPair, clefIV);
				} else {
					LogManager.getInstance().afficherMessageWarning("Le certificat (couple clé public/clé privée) n'a pas été trouvé dans le magasin et utilisé", this.getClass());
				}
			}
		}

		return null;
	}

	private InfosDechiffrement getInfosDechiffrementDepuisPkcs12(KeyPair keyPair, List<ClefIV> clefIVs) {
		// TODO revoir pour faire correspondre le certificat du keypair avec la
		// liste des certificats de la clefIVs
		return new InfosDechiffrement(keyPair, clefIVs.get(0));
	}
}
