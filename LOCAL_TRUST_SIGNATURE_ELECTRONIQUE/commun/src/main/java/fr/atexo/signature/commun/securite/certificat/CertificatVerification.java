package fr.atexo.signature.commun.securite.certificat;

import fr.atexo.json.reponse.verification.RetourVerification;
import fr.atexo.signature.commun.securite.certificat.verification.CertificatValiditeEtat;
import fr.atexo.signature.commun.securite.certificat.verification.CertificatVerificationEtat;
import fr.atexo.signature.commun.securite.provider.bouncycastle.BouncyCaslteHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.Provider;
import java.security.SignatureException;
import java.security.cert.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe permettant de vérifier un certificat.
 */
public class CertificatVerification {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificatVerification.class);

    private String cheminRepertoireTrust;

    private InfosCertificat infosCertificat;

    private InfosCertificat infosCertificatParent;

    private List<InfosCertificat> infosCertificats = new ArrayList<InfosCertificat>();

    private InfosCrl infosCrl;

    private List<InfosCrl> infosCrls = new ArrayList<InfosCrl>();

    private List<File> fichiersCrls = new ArrayList<File>();

    private Pattern patternNomRepertoire;

    public CertificatVerification(InfosCertificat infosCertificat) {
        this(infosCertificat, null, null);
    }

    public CertificatVerification(InfosCertificat infosCertificat, String cheminRepertoireTrust, Pattern patternNomRepertoire) {
        this.infosCertificat = infosCertificat;
        this.cheminRepertoireTrust = cheminRepertoireTrust;
        this.patternNomRepertoire = patternNomRepertoire;
    }

    public RetourVerification effectuerEnsembleVerifications() throws IOException {

        LOGGER.info("Contrôle sur le certificat : " + infosCertificat.getCertificat().getSubjectX500Principal().toString());

        CertificatValiditeEtat codeRetourPeriodeValidite = verifierPeriodeDeValidite();
        LOGGER.info("Code retour pour la période de validité : " + codeRetourPeriodeValidite);

        CertificatVerificationEtat codeRetourChaineDeCertification = verifierChaineDeCertification();
        LOGGER.info("Code retour pour la vérification de la chaine de vérification : " + codeRetourChaineDeCertification);

        CertificatVerificationEtat codeRetourRevocationCertificat = verifierRevocationCertificat();
        LOGGER.info("Code retour pour la révocation du certificat : " + codeRetourRevocationCertificat);

        RetourVerification retourVerification = new RetourVerification(codeRetourPeriodeValidite, codeRetourChaineDeCertification, codeRetourRevocationCertificat);
        for (InfosCertificat infosCertificat : infosCertificats) {
            if (infosCertificat.getFichierDisque() != null) {
                String nomRepertoire = infosCertificat.getFichierDisque().getParentFile().getName();
                nomRepertoire = getNomRepertoire(nomRepertoire, patternNomRepertoire);
                retourVerification.ajouterRepertoireChaineCertification(nomRepertoire);
            }
        }
        for (InfosCrl infosCrl : infosCrls) {
            if (infosCrl.getFichierDisque() != null) {
                String nomRepertoire = infosCrl.getFichierDisque().getParentFile().getName();
                nomRepertoire = getNomRepertoire(nomRepertoire, patternNomRepertoire);
                retourVerification.ajouterRepertoireRevocation(nomRepertoire);
            }
        }

        return retourVerification;
    }

    private String getNomRepertoire(String nomRepertoire, Pattern patternNomRepertoire) {

        String nomRepertoireModifie = nomRepertoire;

        if (patternNomRepertoire != null) {
            Matcher matcher = patternNomRepertoire.matcher(nomRepertoire);
            if (matcher.find()) {
                try {
                    nomRepertoireModifie = matcher.group(1);
                } catch (Exception e) {
                    LOGGER.warn("Le répertoire " + nomRepertoire + " ne matche pas le pattern de la regex " + patternNomRepertoire.pattern());
                }
            }
        }

        return nomRepertoireModifie;
    }

    public CertificatValiditeEtat verifierPeriodeDeValidite() {
        try {
            infosCertificat.getCertificat().checkValidity();
            return CertificatValiditeEtat.Valide;
        } catch (CertificateExpiredException e) {
            return CertificatValiditeEtat.Expire;
        } catch (CertificateNotYetValidException e) {
            return CertificatValiditeEtat.PasEncoreValide;
        }
    }

    public CertificatVerificationEtat verifierChaineDeCertification() {

        CertificatVerificationEtat verification = null;
        Provider provider = BouncyCaslteHandler.verifierPresenceEtRecupererProvider();
        FilenameFilter filtreRepertoire = DirectoryFileFilter.INSTANCE;

        if (cheminRepertoireTrust != null) {
            File repertoireRacine = new File(cheminRepertoireTrust);
            List<File> repertoires = Arrays.asList(repertoireRacine.listFiles(filtreRepertoire));
            Comparator<File> fileComparator = new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o1.compareTo(o2);
                }
            };
            Collections.sort(repertoires, fileComparator);

            for (File repertoire : repertoires) {
                verification = verifierChaineDeCertification(repertoire, provider);
                if (verification.getCodeRetour() == CertificatVerificationEtat.VerificationValide.getCodeRetour()) {
                    return verification;
                }
            }
        }

        return verification;
    }

    private CertificatVerificationEtat verifierChaineDeCertification(File repertoireRacine, Provider provider) {


        Collection<File> crts = FileUtils.listFiles(repertoireRacine, new String[]{"crt"}, true);
        Comparator<File> fileComparator = new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.compareTo(o2);
            }
        };
        List<File> fichiersCrts = new ArrayList<File>(crts);
        Collections.sort(fichiersCrts, fileComparator);

        ArrayList<InfosCertificat> infosCertificats = new ArrayList<InfosCertificat>();

        for (File fichierCertificat : fichiersCrts) {
            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(fichierCertificat);
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X509", provider);
                X509Certificate certificat = (X509Certificate) certificateFactory.generateCertificate(inputStream);
                if (certificat != null) {
                    InfosCertificat infosCertificat = new InfosCertificat(certificat, fichierCertificat);
                    infosCertificats.add(infosCertificat);
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }

        InfosCertificat certificatEnTraitement = this.infosCertificat;

        // Retrieve the certificate chain of the signing certificate
        while (certificatEnTraitement != null && !certificatEnTraitement.getCertificat().getSubjectX500Principal().getName().equals(certificatEnTraitement.getCertificat().getIssuerX500Principal().getName())) {

            // certificatParent is the parent of the current certificate : certificatEnTraitement
            InfosCertificat certificatParent = null;
            for (InfosCertificat infosCertificat : infosCertificats) {

                if (infosCertificat.getCertificat().getSubjectX500Principal().getName().equals(certificatEnTraitement.getCertificat().getIssuerX500Principal().getName())) {

                    try {
                        // Certificate of the parent current certificate was found Signature verification certificate
                        certificatEnTraitement.getCertificat().verify(infosCertificat.getCertificat().getPublicKey());
                    } catch (SignatureException e) {
                        return CertificatVerificationEtat.SignatureCertificatInvalide;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // If the certificate has not been already added to the chain, it'll be added
                    if (!this.infosCertificats.contains(infosCertificat)) {
                        this.infosCertificats.add(infosCertificat);
                    }
                    certificatParent = infosCertificat;
                }
            }
            // Reset the current certificate with the certificate to parent up the chain
            certificatEnTraitement = certificatParent;
        } // End Retrieve the certificate chain of the signing certificate

        if (infosCertificats.isEmpty()) {
            // No parent certificate was found
            return CertificatVerificationEtat.ChaineCertificatNonTrouvee;
        }

        if (certificatEnTraitement == null) {
            // The certificate chain is incomplete
            return CertificatVerificationEtat.ChaineCertificatIncomplete;
        }

        // Checks the certificate chain; The self-signed certificate is a
        // certificate of higher rank in the list certificateChain; Checking the
        // chain begins with the self-signed certificate

        // Step 1: Root validation certificate (trust anchor)
        int tailleMinimun = this.infosCertificats.size() - 1;
        InfosCertificat infosCertificatRacine = this.infosCertificats.get(tailleMinimun);
        CertificatVerification certificationRacineVerification = new CertificatVerification(infosCertificatRacine);

        if (fichiersCrls.isEmpty()) {
            fichiersCrls.addAll(getFichiersCrls(repertoireRacine));
        }

        CertificatVerificationEtat verificationEtatRacine = certificationRacineVerification.verifierCertificatRacine(tailleMinimun);
        if (verificationEtatRacine.getCodeRetour() != CertificatVerificationEtat.VerificationValide.getCodeRetour()) {
            return verificationEtatRacine;
        }
        this.infosCertificatParent = infosCertificatRacine;

        // Retrieving the CRL used to verify
        InfosCrl crlRacine = certificationRacineVerification.getInfosCrl();
        if (crlRacine != null && !this.infosCrls.contains(crlRacine)) {
            this.infosCrls.add(crlRacine);
        }

        // Step 2 : validation of intermediate certificates
        for (int j = this.infosCertificats.size() - 2; j >= 0; j--) {
            // Intermediate certificate and certificate parent
            InfosCertificat certificatIntermediaire = this.infosCertificats.get(j);
            InfosCertificat certificatParent = this.infosCertificats.get(j + 1);

            // Intermediate certificate Verification
            CertificatVerification certificatIntermediareVerification = new CertificatVerification(certificatIntermediaire);

            tailleMinimun -= 1;
            CertificatVerificationEtat verificationEtatIntermediaire = certificatIntermediareVerification.verifierCertificat(certificatParent, tailleMinimun);
            if (verificationEtatIntermediaire.getCodeRetour() != CertificatVerificationEtat.VerificationValide.getCodeRetour()) {
                return verificationEtatIntermediaire;
            }

            this.infosCertificatParent = certificatParent;

            // Retrieving the CRL used to verify
            InfosCrl crlIntermediaire = certificatIntermediareVerification.getInfosCrl();
            if (crlIntermediaire != null && !this.infosCrls.contains(crlIntermediaire)) {
                this.infosCrls.add(crlIntermediaire);
            }
        } // End checking the intermediate chain

        return CertificatVerificationEtat.VerificationValide;

    }


    private CertificatVerificationEtat verifierCertificatRacine(int tailleMinimun) {
        return verifierCertificat(this.infosCertificat, tailleMinimun);
    }

    /**
     * Verification of an intermediate certificate in certificate chain function
     * : <br />
     * 1) Verifies the signature contained in the certificate from the
     * certificate issuer <br />
     * 2) Checks the validity dates of the certificate <br />
     * 3) Verifies that the certificate is not revoked by the list of CRLs input <br />
     * 4) Verifies that the certificate is a CA certificate <br />
     *
     * @param certificatParent Certificate issuer of the certificate of the current object
     * @param tailleMinimun    Minimum value of PathLen (Extension basicConstraints)
     * @return
     */
    private CertificatVerificationEtat verifierCertificat(InfosCertificat certificatParent, int tailleMinimun) {

        // 1) test the validity of the signature of the certificate
        CertificatVerificationEtat verificationEtat = verifierSignatureCerticat(this.infosCertificat, certificatParent);
        if (verificationEtat.getCodeRetour() != CertificatVerificationEtat.VerificationValide.getCodeRetour())
            return verificationEtat;

        // 2) Validity period
        CertificatValiditeEtat certificatValiditeEtat = verifierPeriodeDeValidite();
        if (certificatValiditeEtat != CertificatValiditeEtat.Valide) {
            return CertificatVerificationEtat.CertificatInvalide;
        }

        // 4) Test basicConstraints
        int taille = this.infosCertificat.getCertificat().getBasicConstraints();
        if (taille == -1) {
            // BasicConstraints absent or CA: False
            return CertificatVerificationEtat.CertificatInvalide;
        }

        if (taille < tailleMinimun) {
            return CertificatVerificationEtat.CertificatInvalide;
        }

        return CertificatVerificationEtat.VerificationValide;
    }

    /**
     * Checks the validity of this signature in the certificate
     *
     * @param certificat
     * @param certificatParent
     * @return
     */
    private CertificatVerificationEtat verifierSignatureCerticat(InfosCertificat certificat, InfosCertificat certificatParent) {
        try {
            certificat.getCertificat().verify(certificatParent.getCertificat().getPublicKey());
        } catch (Exception e) {
            return CertificatVerificationEtat.SignatureCertificatInvalide;
        }
        return CertificatVerificationEtat.VerificationValide;
    }

    private List<File> getFichiersCrls(File repertoireRacine) {
        Collection<File> crls = FileUtils.listFiles(repertoireRacine, new String[]{"crl"}, true);
        Comparator<File> fileComparator = new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return o1.compareTo(o2);
            }
        };
        List<File> fichiersCrts = new ArrayList<File>(crls);
        Collections.sort(fichiersCrts, fileComparator);

        return fichiersCrts;
    }

    public CertificatVerificationEtat verifierRevocationCertificat() throws IOException {
        CertificatVerificationEtat certificatVerificationEtat = CertificatVerificationEtat.FichierCrlNonTrouve;
        if (!infosCertificats.isEmpty()) {
            String cheminRepertoire = infosCertificats.get(0).getFichierDisque().getParentFile().getAbsolutePath();
            certificatVerificationEtat = verifierRevocationCertificat(cheminRepertoire);
            return certificatVerificationEtat;
        }
        return certificatVerificationEtat;
    }

    /**
     * This function checks the revocation of a given certificate
     *
     * @return
     * @throws IOException
     */
    public CertificatVerificationEtat verifierRevocationCertificat(String cheminRepertoire) throws IOException {

        File repertoireRacine = new File(cheminRepertoire);
        List<File> fichiersCrls = getFichiersCrls(repertoireRacine);

        for (File fichierCrl : fichiersCrls) {

            FileInputStream fichierCrlInputStream = new FileInputStream(fichierCrl);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fichierCrlInputStream);
            CertificateFactory certificateFactory;
            try {
                certificateFactory = CertificateFactory.getInstance("X.509");
                X509CRL crl = (X509CRL) certificateFactory.generateCRL(bufferedInputStream);
                InfosCrl infosCrl = new InfosCrl(crl, fichierCrl);
                if (crl.getIssuerX500Principal().getName().equals(infosCertificat.getCertificat().getIssuerX500Principal().getName())) {
                    this.infosCrl = infosCrl;
                }
            } catch (Exception e) {
                // supprimer les retours dans les catch pr ne pas bloquer s'il y a un mauvais fichier comme MakeFile.crl et MakeFile.crt
            } finally {
                IOUtils.closeQuietly(fichierCrlInputStream);
                IOUtils.closeQuietly(bufferedInputStream);
            }
        }

        if (infosCrl != null) {
            boolean revocation = infosCrl.getCrl().isRevoked(this.infosCertificat.getCertificat());
            if (!revocation) {
                if (!infosCrls.contains(infosCrl)) {
                    infosCrls.add(infosCrl);
                }
                return CertificatVerificationEtat.VerificationValide;
            } else {
                // Ajouter la crl meme revoked
                if (!infosCrls.contains(infosCrl)) {
                    infosCrls.add(infosCrl);
                }
                return CertificatVerificationEtat.CertificatRevoque;
            }
        } else {
            return CertificatVerificationEtat.FichierCrlNonTrouve;
        }
    }

    public List<File> getFichiersCrls() {
        return fichiersCrls;
    }

    public InfosCertificat getInfosCertificat() {
        return infosCertificat;
    }

    public InfosCertificat getInfosCertificatParent() {
        return infosCertificatParent;
    }

    public InfosCrl getInfosCrl() {
        return infosCrl;
    }

    public String getCheminRepertoireTrust() {
        return cheminRepertoireTrust;
    }

    public List<InfosCertificat> getInfosCertificats() {
        return infosCertificats;
    }

    public List<InfosCrl> getInfosCrls() {
        return infosCrls;
    }
}
