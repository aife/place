package fr.atexo.signature.commun.securite.processor.signature;

/**
 *
 */
public interface ConteneurHashFichierSignatureXML {

    String getNomFichier();

    public String getHashFichier();

    public String getContenuSignatureXml();

    public void setContenuSignatureXml(String contenuSignatureXml);

}
