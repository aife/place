package fr.atexo.signature.commun.exception;

/**
 * Exception pour signaler un souci lors du paramétrage.
 */
public class ParametrageException extends Exception {

    public ParametrageException(String message) {
        super(message);
    }
}
