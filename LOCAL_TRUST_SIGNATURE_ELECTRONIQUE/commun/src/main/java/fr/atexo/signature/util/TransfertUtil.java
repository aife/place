package fr.atexo.signature.util;

import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.http.HttpRequest;
import fr.atexo.signature.validation.TypeEchange;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 */
public abstract class TransfertUtil {

    public static final String RESULTAT_RETOUR_UPLOAD_VALIDE = "OK";

    public static final String RESULTAT_RETOUR_UPLOAD_INVALIDE = "ERR";

    public static final String CONTENU_FICHIER_XML = "contenuFichierXML";

    public static final String TYPE_ECHANGE = "typeEchange";

    public static final String TYPE_ALGORITHM_HASH = "typeAlgorithmHash";

    public static final String HASH_FICHIER = "hashFichier";

    public static final String ORIGINE_PLATEFORME = "plateforme";

    public static final String ORIGINE_ORGANISME = "organisme";

    public static final String ORIGINE_ITEM = "item";

    public static final String ORIGINE_CONTEXTE_METIER = "contexteMetier";

    public static enum TypeTransfert {

        TransfertBloc("0"), OuvertureEnLigne("1"), OuvertureMixte("2");

        private String valeur;

        TypeTransfert(String valeur) {
            this.valeur = valeur;
        }

        public String getValeur() {
            return valeur;
        }
    }

    /**
     * Permet de récupérer du module de validation une date serveur
     *
     * @param urlModuleValidation l'url d'accès au module de validation
     * @return la reponse json du serveur
     * @throws Exception
     */
    public static String recupererDateModuleValidation(String urlModuleValidation) throws TransfertExecutionException {

        try {
            HttpRequest httpRequest = new HttpRequest(new URL(urlModuleValidation), true);
            httpRequest.ajouterContenu(TYPE_ECHANGE, TypeEchange.RetourDateServeur.name());

            InputStream inputStream = httpRequest.envoyer();
            String reponseJson = IOUtils.toString(inputStream);
            inputStream.close();
            httpRequest.deconnecter();

            return reponseJson;


        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de la date serveur depuis le module validation", e);
        }
    }

    /**
     * Permet d'envoyer au module de validation le fichier XML devant être vérifié par le serveur.
     *
     * @param urlModuleValidation          l'url d'accès au module de validation
     * @param signatureXadesClientEnBase64 la signature encodé en base 64 devant être vérifié
     * @param typeEchange                  le type de validation
     * @param typeAlgorithmHash            le type d'algorithm de hash
     * @return la reponse json du serveur
     * @throws Exception
     */
    public static String envoyerAuModuleValidation(String urlModuleValidation, String signatureXadesClientEnBase64, TypeEchange typeEchange, TypeAlgorithmHash typeAlgorithmHash, String originePlateforme, String origineOrganisme, String origineItem, String origineContexteMetier) throws TransfertExecutionException {

        try {
            HttpRequest httpRequest = new HttpRequest(new URL(urlModuleValidation), true);
            httpRequest.ajouterContenu(CONTENU_FICHIER_XML, signatureXadesClientEnBase64);
            httpRequest.ajouterContenu(TYPE_ECHANGE, typeEchange.name());
            httpRequest.ajouterContenu(TYPE_ALGORITHM_HASH, typeAlgorithmHash.name());
            // informations complémentaires
            if (!Util.estVide(originePlateforme)) {
                httpRequest.ajouterContenu(ORIGINE_PLATEFORME, originePlateforme);
            }
            if (!Util.estVide(origineOrganisme)) {
                httpRequest.ajouterContenu(ORIGINE_ORGANISME, origineOrganisme);
            }
            if (!Util.estVide(origineItem)) {
                httpRequest.ajouterContenu(ORIGINE_ITEM, origineItem);
            }
            if (!Util.estVide(origineContexteMetier)) {
                httpRequest.ajouterContenu(ORIGINE_CONTEXTE_METIER, origineContexteMetier);
            }


            InputStream inputStream = httpRequest.envoyer();
            String resultat = IOUtils.toString(inputStream);
            inputStream.close();
            httpRequest.deconnecter();

            return resultat;

        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors du transfert des données de signature XML au serveur de validation", e);
        }
    }

    /**
     * Permet d'envoyer  sur l'url d'un serveur un bloc de données chiffrés.
     *
     * @param urlServeurUpload  l'url du serveur d'upload du bloc de chiffrement
     * @param uid               le uid
     * @param typeEnveloppe     le type d'enveloppe
     * @param numeroLot         le numéro du lot
     * @param nomfichier        le nom du fichier
     * @param typeFichier       le type de fichier
     * @param indexFichier      l'index du fichier
     * @param numeroBlocFichier le numero du bloc de chiffrement du fichier
     * @param blocChiffrement   le contenu du bloc chiffré
     * @return la réponse du serveur sur l'intégration de l'envoi du bloc de chiffrement
     * @throws Exception
     */
    public static String envoyerBlocChiffre(String urlServeurUpload, String uid, int typeEnveloppe, int numeroLot, String nomfichier, String typeFichier, int indexFichier, int numeroBlocFichier, String blocChiffrement) throws TransfertExecutionException {

        try {

            HttpRequest httpRequest = new HttpRequest(new URL(urlServeurUpload), true);
            if (blocChiffrement != null) {
                httpRequest.ajouterContenu("uidOffre", uid);
                httpRequest.ajouterContenu("typeEnv", typeEnveloppe);
                httpRequest.ajouterContenu("sousPli", numeroLot);
                httpRequest.ajouterContenu("typeFichier", typeFichier);
                httpRequest.ajouterContenu("numOrdreFichier", indexFichier);
                httpRequest.ajouterContenu("numOrdreBloc", numeroBlocFichier);
                httpRequest.ajouterContenu("bloc", blocChiffrement);
            }

            InputStream inputStream = httpRequest.envoyer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String resultat = reader.readLine();

            reader.close();
            httpRequest.deconnecter();

            return resultat;

        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de l'envoi à l'url [" + urlServeurUpload + "] du contenu du bloc numéro [" + numeroBlocFichier + "] pour le fichier [" + nomfichier + "] de type [" + typeFichier + "] pour le type d'enveloppe [" + typeEnveloppe + "] et numéro de lot [" + numeroLot + "]", e);
        }
    }

    /**
     * Permet d'envoyer sur l'url d'un serveur un bloc de données déchiffrés.
     *
     * @param urlServeurUpload       l'url du serveur d'upload du bloc de chiffrement
     * @param trigrammeOrganisme     le trigramme de l'organisme
     * @param idBlocDeChiffrement    l'identifiant du bloc déchiffré
     * @param contenuDechiffreBase64 le contenu du bloc déchiffré
     * @return
     * @throws fr.atexo.signature.commun.exception.execution.TransfertExecutionException
     *
     */
    public static String envoyerBloc(String urlServeurUpload, String trigrammeOrganisme, String idBlocDeChiffrement, String contenuDechiffreBase64) throws TransfertExecutionException {
        try {
            return envoyerInformations(urlServeurUpload, trigrammeOrganisme, null, idBlocDeChiffrement, contenuDechiffreBase64, TypeTransfert.TransfertBloc);
        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de l'envoi à l'url [" + urlServeurUpload + "] du contenu du bloc déchiffré ayant identifiant [" + idBlocDeChiffrement + "] pour l'organisme ayant comme trigramme [" + trigrammeOrganisme + "]", e);
        }
    }

    /**
     * Permet d'envoyer sur l'url d'un serveur l'information comme quoi le traitement de déchiffrement est terminé.
     *
     * @param urlServeurUpload           l'url du serveur d'upload du bloc de chiffrement
     * @param trigrammeOrganisme         le trigramme de l'organisme
     * @param idPremierBlocDeChiffrement l'identifiant du premier bloc de chiffrement
     * @return
     * @throws fr.atexo.signature.commun.exception.execution.TransfertExecutionException
     *
     */
    @Deprecated
    public static String envoyerInformationsFinTraitementDechiffrement(String urlServeurUpload, String trigrammeOrganisme, String idPremierBlocDeChiffrement) throws TransfertExecutionException {
        try {
            return envoyerInformations(urlServeurUpload, trigrammeOrganisme, null, idPremierBlocDeChiffrement, null, TypeTransfert.OuvertureEnLigne);
        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de l'envoi à l'url [" + urlServeurUpload + "]  des informations spéficiant la fin du traitement de l'ensemble des bloc dont le premier était  [" + idPremierBlocDeChiffrement + "] pour l'organisme ayant comme trigramme [" + trigrammeOrganisme + "]", e);
        }
    }

    /**
     * Permet d'envoyer sur l'url d'un serveur l'information comme quoi l'enveloppe a été ouverte.
     *
     * @param urlServeurUpload   l'url du serveur d'upload du bloc de chiffrement
     * @param trigrammeOrganisme le trigramme de l'organisme
     * @param idEnveloppe        l'identifiant de l'enveloppe qui a été ouverte
     * @param typeTransfert      le type de transfert
     * @return
     * @throws fr.atexo.signature.commun.exception.execution.TransfertExecutionException
     *
     */
    public static String envoyerInformationsOuvertureEnveloppe(String urlServeurUpload, String trigrammeOrganisme, String idEnveloppe, TypeTransfert typeTransfert) throws TransfertExecutionException {
        try {
            return envoyerInformations(urlServeurUpload, trigrammeOrganisme, idEnveloppe, null, null, typeTransfert);
        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de l'envoi à l'url [" + urlServeurUpload + "]  des informations spéficiant l'ouverture de l'enveloppe ayant comme identifiant [" + idEnveloppe + "] pour l'organisme ayant comme trigramme [" + trigrammeOrganisme + "]", e);
        }
    }

    /**
     * Permet d'envoyer un bloc déchiffré sur l'url d'un serveur en lui passant des metadonnées complémentaires.
     *
     * @param urlServeurUpload       l'url du serveur d'upload du bloc de chiffrement
     * @param trigrammeOrganisme     le trigramme de l'organisme
     * @param idBlocDeChiffrement    l'identifiant du bloc déchiffré
     * @param contenuDechiffreBase64 le contenu du bloc déchiffré
     * @param idEnveloppe            l'identifiant de l'enveloppe
     * @param typeTransfert          le type de transfert
     * @return
     * @throws fr.atexo.signature.commun.exception.execution.TransfertExecutionException
     *
     */
    public static String envoyerInformations(String urlServeurUpload, String trigrammeOrganisme, String idEnveloppe, String idBlocDeChiffrement, String contenuDechiffreBase64, TypeTransfert typeTransfert) throws Exception {

        HttpRequest httpRequest = new HttpRequest(new URL(urlServeurUpload), true);
        if (idBlocDeChiffrement != null) {
            httpRequest.ajouterContenu("idBlob", idBlocDeChiffrement);
        }
        if (trigrammeOrganisme != null) {
            httpRequest.ajouterContenu("organisme", trigrammeOrganisme);
        }

        if (typeTransfert == TypeTransfert.TransfertBloc) {
            httpRequest.ajouterContenu("blocDechiffre", contenuDechiffreBase64);
        } else {
            httpRequest.ajouterContenu("endDecrypt", typeTransfert.getValeur());
        }
        if (idEnveloppe != null) {
            httpRequest.ajouterContenu("idEnveloppe", idEnveloppe);
        }

        InputStream inputStream = httpRequest.envoyer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String resultat = reader.readLine();

        reader.close();
        httpRequest.deconnecter();

        return resultat;
    }

    public static String contruireUrlTelechargement(String urlServeurTelechargement, String trigrammeOrganisme, String idBlocDeChiffrement) throws TransfertExecutionException {
        boolean verificationUrl = urlServeurTelechargement.contains("?");
        String url = urlServeurTelechargement + (verificationUrl ? "&" : "?") + "organisme=" + trigrammeOrganisme + "&idBlob=" + idBlocDeChiffrement;
        return url;
    }

    public static byte[] telechargerBloc(String urlServeurTelechargement, String trigrammeOrganisme, String idBlocDeChiffrement) throws TransfertExecutionException {
        HttpRequest httpRequest = null;
        ByteArrayOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            boolean verificationUrl = urlServeurTelechargement.contains("?");
            String url = urlServeurTelechargement + (verificationUrl ? "&" : "?") + "organisme=" + trigrammeOrganisme + "&idBlob=" + idBlocDeChiffrement;
            httpRequest = new HttpRequest(new URL(url), false);

            inputStream = httpRequest.getInputStream();
            outputStream = new ByteArrayOutputStream();
            IOUtils.copy(inputStream, outputStream);
            byte[] contenuBlocDeChiffrement = outputStream.toByteArray();

            return contenuBlocDeChiffrement;

        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de la récupération des données du bloc de chiffrement id " + idBlocDeChiffrement + "  et organisme " + trigrammeOrganisme, e);
        } finally {

            if (httpRequest != null) {
                httpRequest.deconnecter();
            }

            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
    }

    public static byte[] telecharger(String urlServeurTelechargement) throws TransfertExecutionException {
        ByteArrayOutputStream outputStream = null;
        try {
            outputStream = telechargerStream(urlServeurTelechargement);
            byte[] contenuBlocDeChiffrement = outputStream.toByteArray();
            return contenuBlocDeChiffrement;
        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de la récupération des données via l'url [" + urlServeurTelechargement + "]", e);
        } finally {

            IOUtils.closeQuietly(outputStream);
        }
    }

    public static ByteArrayOutputStream telechargerStream(String urlServeurTelechargement) throws TransfertExecutionException {
        HttpRequest httpRequest = null;
        ByteArrayOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            httpRequest = new HttpRequest(new URL(urlServeurTelechargement), false);

            inputStream = httpRequest.getInputStream();
            outputStream = new ByteArrayOutputStream();
            IOUtils.copy(inputStream, outputStream);

            return outputStream;

        } catch (Exception e) {
            throw new TransfertExecutionException("Erreur lors de la récupération des données via l'url [" + urlServeurTelechargement + "]", e);
        } finally {

            if (httpRequest != null) {
                httpRequest.deconnecter();
            }

            IOUtils.closeQuietly(inputStream);
        }
    }

    public static boolean verifierUrl(String url) {
        HttpURLConnection connection = null;
        try {
            connection = new HttpRequest(new URL(url), false).getConnection();
            connection.connect();
            return connection.getResponseCode() == HttpURLConnection.HTTP_OK;

        } catch (Exception e) {
            return false;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }       
}
