package fr.atexo.signature.commun.securite.processor.signature;

import java.io.File;

/**
 *
 */
public interface ConteneurFichierSignatureXML {

    File getFichier();

    void setFichier(File fichier);

    File getFichierSignatureXml();

    void setFichierSignatureXml(File fichierSignatureXml);
}
