package fr.atexo.signature.gui.browser;

import fr.atexo.signature.gui.browser.event.SelectDirectoryEvent;
import fr.atexo.signature.gui.browser.event.SelectDirectoryListener;
import fr.atexo.signature.commun.util.I18nUtil;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.EventListenerList;

public class SelectionRepertoire extends JFileChooser {

    /**
     *
     */
    private static final long serialVersionUID = -2340441884576010846L;
    private EventListenerList listenerListe = new EventListenerList();

    public SelectionRepertoire() {
        super();
        this.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public SelectionRepertoire(File startDir) {
        super(startDir);
        this.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public String showDialog() {

        if (this.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            if (this.getSelectedFile().getAbsolutePath().length() != 0) {
                SelectDirectoryEvent dirEvent = new SelectDirectoryEvent(this, this.getSelectedFile().getAbsolutePath());
                fireCertificateEvent(dirEvent);
                return this.getSelectedFile().getAbsolutePath();
            } else {
                JOptionPane.showMessageDialog(this, I18nUtil.get("SWING_BROWSER_ACTION_ALERTE_SELECTIONNER_DOSSIER"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.WARNING_MESSAGE);
            }
        }
        return null;
    }

    public void addListener(SelectDirectoryListener listener) {
        listenerListe.add(SelectDirectoryListener.class, listener);
    }

    public void removeListener(SelectDirectoryListener listener) {
        listenerListe.remove(SelectDirectoryListener.class, listener);
    }

    private void fireCertificateEvent(SelectDirectoryEvent evt) {
        Object[] listeners = listenerListe.getListenerList();
        for (int i = 0; i < listeners.length; i += 2) {
            if (listeners[i] == SelectDirectoryListener.class) {
                ((SelectDirectoryListener) listeners[i + 1]).onSelect(evt);
            }
        }
    }
}
