package fr.atexo.signature.commun.securite.signature;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.signature.commun.securite.certificat.InfosCertificat;
import fr.atexo.signature.commun.securite.certificat.InfosCrl;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.InfosVerificationCertificat;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.commun.util.io.HashUtil;
import fr.atexo.signature.commun.util.io.XMLUtil;
import net.iharder.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.cert.CRLException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.*;

/**
 * Classe permettant de gérer la signature XAdES d'un fichier.
 * Possibilité d'utilier le projet apache Santuario http://santuario.apache.org/ pour remplacer les lib internes du jdk.
 */
public class SignatureXades {

    private static final String PROPRIETE_PROVIDER = "org.apache.jcp.xml.dsig.internal.dom.XMLDSigRI";

    private static final String PROPRIETE_VALIDATION_XML_JDK7 = "org.apache.jcp.xml.dsig.secureValidation";

    private static final String URI_CONSTANTE = "http://atexo.com/#signedProps";

    //hashs binaires à valider obligatoirement
    private static final List<String> HASHS_CONSTANTE_URI_ATEXO = Arrays.asList(new String[]{"nwA9oxgmt3PO38tK6863KUeLmSA=", "n3djJyN7zMjXV+fSBz5xi/pt7cs=", "XP14ZMFwHebPywzRs82sAOBGrvs+FCFTseHyxj0q4EM="});

    private static final String HASH_CONSTANTE_URI_ATEXO_NOUVEAU = "WIXo3AkjNPd7MtQNOnwEEI/DLF8=";

    /**
     * Permet de signer un fichier au format XAdES.
     *
     * @param keyPair            le keypair contenant les informations du certificat
     * @param hashBinaireFichier le hash du fichier
     * @param uriFichier         l'uri du fichier
     * @param dateSignature      la date de génération de la signature
     * @param typeAlgorithmHash  le type d'algorith à utiliser pour générer les hashs
     * @return
     */
    public static String signer(KeyPair keyPair, byte[] hashBinaireFichier, URI uriFichier, Date dateSignature, TypeAlgorithmHash typeAlgorithmHash) throws Exception {

        // id de la signature
        String idSignature = XMLConstantes.SIGNATURE_TAG_ID;

        // Create a DOM XMLSignatureFactory that will be used to generate the enveloped signature.
        String providerName = System.getProperty("jsr105Provider", PROPRIETE_PROVIDER);
        XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

        // /Decide on a digest method We use the XMLSignatureFactory nstance created in the first step to create the DigestMethod object.
        DigestMethod digestMethod = xmlSignatureFactory.newDigestMethod(typeAlgorithmHash.getAlgoDigestMethod(), null);

        //Create a Transform
        Transform transform = xmlSignatureFactory.newTransform(CanonicalizationMethod.EXCLUSIVE, (TransformParameterSpec) null);

        // Create a Reference to the element to be sign and also specify the SHA1 digest algorithm and the ENVELOPED Transform.
        String uriFichierComplet = null;
        if (uriFichier != null) {
            uriFichierComplet = uriFichier.toASCIIString();
        }
        Reference referenceSignedFile = xmlSignatureFactory.newReference(uriFichierComplet, digestMethod, Collections.singletonList(transform), null, null, hashBinaireFichier);

        // Create a reference object to the SignedProperties in signatureProperty Element
        String referenceURI = XMLConstantes.XMLDSIG + "-" + UUID.randomUUID() + "-" + XMLConstantes.SIGNED_PROPS_TAG_ID;
        String type = typeAlgorithmHash.getNameSpace() + "#SignedProperties";
        Reference referenceSignedProperties = xmlSignatureFactory.newReference("#" + referenceURI, digestMethod, Collections.singletonList(transform), type, null);

        // Create a list of reference and add references created in the
        // first step to it.
        List<Reference> referencesList = new ArrayList<Reference>();
        referencesList.add(referenceSignedFile);
        referencesList.add(referenceSignedProperties);

        // Create a canonicalization object : Canonicalization is necessary due to the nature of XML and the way it is parsed by different processors and intermediaries,
        // which can change the data in such a way that the signature is no longer valid but the signed data is still logically equivalent
        CanonicalizationMethod canonicalizationMethod = xmlSignatureFactory.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null);

        // Create a SignatureMethod : SignatureMethod element defines as a URI the digital signature algorithm used to generate the signature, in this case the
        // PKCS#1 RSA-SHA1 algorithm as described in RFC 2437
        SignatureMethod signatureMethod = xmlSignatureFactory.newSignatureMethod(typeAlgorithmHash.getAlgoSignatureMethod(), null);
        // Create the SignedInfo.
        SignedInfo signedInfo = xmlSignatureFactory.newSignedInfo(canonicalizationMethod, signatureMethod, referencesList, XMLConstantes.SIGNED_INFOS_TAG_ID);

        // Construct information about signature key
        X509Certificate x509Certificate = (X509Certificate) keyPair.getCertificate();
        KeyInfoFactory keyInfoFactory = xmlSignatureFactory.getKeyInfoFactory();
        List x509Content = new ArrayList();
        x509Content.add(x509Certificate.getSubjectX500Principal().getName(XMLConstantes.RFC1779));
        x509Content.add(x509Certificate);
        X509Data x509Data = keyInfoFactory.newX509Data(x509Content);

        // Create the KeyInfo containing the X509Data.
        KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));

        // Instantiate the document to be signed.
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setValidating(false);

        Document document = documentBuilderFactory.newDocumentBuilder().newDocument();

        // Create an empty list for Objects
        List<XMLStructure> liste = new ArrayList<XMLStructure>();

        // create a QualifyingProperties element
        Element qualifyingProperties = creerElementQualifyingProperties(document, typeAlgorithmHash, "#" + idSignature);

        // Create a SignedProperties element
        Element elementSignedProperties = creerElementSignedProperties(document, dateSignature, referenceURI, x509Certificate, typeAlgorithmHash);
        qualifyingProperties.appendChild(elementSignedProperties);

        // Create an XMLObject element
        XMLObject object = xmlSignatureFactory.newXMLObject(Collections.singletonList(new DOMStructure(qualifyingProperties)), XMLConstantes.OBJECT_TAG_ID, null, null);

        // Add the previous object the list created in the previous step
        liste.add(object);

        // Create the XMLSignature, but don't sign it yet.

        XMLSignature signature = xmlSignatureFactory.newXMLSignature(signedInfo, keyInfo, liste, idSignature, null);

        // Create a DOMSignContext and specify the RSA PrivateKey and location of the resulting XMLSignature's parent element
        DOMSignContext domSignContext = new DOMSignContext(keyPair.getPrivateKey(), document);
        domSignContext.putNamespacePrefix(XMLSignature.XMLNS, XMLConstantes.DSIG_PREFIX);
        domSignContext.putNamespacePrefix(typeAlgorithmHash.getNameSpaceVersionXades(), XMLConstantes.XADES_PREFIX);
        domSignContext.setURIDereferencer(new TagNameURIDereferencer(document, XMLConstantes.XADES_PREFIX, "SignedProperties"));
        domSignContext.setProperty(PROPRIETE_VALIDATION_XML_JDK7, false);

        // Marshal, generate, and sign the enveloped signature.
        signature.sign(domSignContext);

        // Output the resulting document in a String
        String contenuFichierSignatureXML = XMLUtil.convertir(document, FileUtil.ENCODING_UTF_8);

        return contenuFichierSignatureXML;

    }

    /**
     * Extrait le certificat se trouvant dans le contenu de la signature XAdES.
     *
     * @param contenuSignatureXML le contenu de la signature
     * @return le certificat si trouvé sinon null
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws MarshalException
     */
    public static X509Certificate extraireCertificat(String contenuSignatureXML) throws ClassNotFoundException, IllegalAccessException, InstantiationException, ParserConfigurationException, IOException, SAXException, MarshalException {

        String providerName = System.getProperty("jsr105Provider", PROPRIETE_PROVIDER);
        XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        InputSource inputSourceSignatureXML = new InputSource(new StringReader(contenuSignatureXML));
        Document document = documentBuilderFactory.newDocumentBuilder().parse(inputSourceSignatureXML);
        // Find Signature element.
        NodeList listeSignatures = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");

        if (listeSignatures.getLength() > 0) {
            // Create a DOMValidateContext and specify a KeySelector and document context.
            DOMValidateContext validateContext = new DOMValidateContext(new X509KeySelector(), listeSignatures.item(0));

            // Unmarshal the XMLSignature.
            XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(validateContext);
            KeyInfo keyInfo = signature.getKeyInfo();
            X509Data data = (X509Data) keyInfo.getContent().get(0);
            X509Certificate certificate = (X509Certificate) data.getContent().get(1);

            return certificate;
        }

        return null;
    }

    /**
     * Vérifie l'intégrité de la signature XAdES pour le fichier.
     *
     * @param contenuSignatureXML le contenu de la signature XAdES
     * @param fichier             le fichier à vérifier
     * @return l'état de la vérification ainsi que les informations sur le certificat utilisé pour effectuer la signature
     * @throws Exception
     */
    public static InfosVerificationCertificat verifier(String contenuSignatureXML, File fichier) throws Exception {
        List<String> hashFichiers = new ArrayList<String>();
        String hashSha1Fichier = HashUtil.genererHashSha1Hexadecimal(fichier);
        hashFichiers.add(hashSha1Fichier);
        String hashSha256Fichier = HashUtil.genererHashSha256Hexadecimal(fichier);
        hashFichiers.add(hashSha256Fichier);
        return verifier(contenuSignatureXML, hashFichiers);
    }

    /**
     * Vérifie l'intégrité de la signature XAdES pour le hash d'un fichier.
     *
     * @param contenuSignatureXML le contenu de la signature XAdES
     * @param hashFichiers        la liste des hashs en hexadecimal du fichier à vérifier (SHA1 et/ou SHA256)
     * @return l'état de la vérification ainsi que les informations sur le certificat utilisé pour effectuer la signature
     * @throws Exception
     */
    public static InfosVerificationCertificat verifier(String contenuSignatureXML, List<String> hashFichiers) throws Exception {

        String providerName = System.getProperty("jsr105Provider", PROPRIETE_PROVIDER);
        XMLSignatureFactory xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        InputSource inputSourceSignatureXML = new InputSource(new StringReader(contenuSignatureXML));
        Document document = documentBuilderFactory.newDocumentBuilder().parse(inputSourceSignatureXML);
        // Find Signature element.
        NodeList listeSignatures = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        InfosVerificationCertificat infosVerificationCertificat = new InfosVerificationCertificat();
        if (listeSignatures.getLength() > 0) {
            // Create a DOMValidateContext and specify a KeySelector and document context.
            DOMValidateContext validateContext = new DOMValidateContext(new X509KeySelector(), listeSignatures.item(0));

            // Unmarshal the XMLSignature.
            XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(validateContext);
            KeyInfo keyInfo = signature.getKeyInfo();

            X509Data data = (X509Data) keyInfo.getContent().get(0);
            X509Certificate certificate = (X509Certificate) data.getContent().get(1);

            // extraction des informations sur le certificat utilisé pour la signature.
            infosVerificationCertificat = (InfosVerificationCertificat) CertificatUtil.extraireInformations(certificate, new InfosVerificationCertificat());

            PublicKey publicKey = certificate.getPublicKey();
            validateContext = new DOMValidateContext(publicKey, listeSignatures.item(0));
            validateContext.setProperty(PROPRIETE_VALIDATION_XML_JDK7, false);
            // Validate the XMLSignature.
            boolean validationOk = signature.getSignatureValue().validate(validateContext);

            if (validationOk) {
                // Check the validation status of each Reference.
                Iterator<Reference> referenceIterator = signature.getSignedInfo().getReferences().iterator();
                for (int j = 0; referenceIterator.hasNext(); j++) {
                    Reference reference = referenceIterator.next();
                    if (j == 0) {
                        byte[] digestValue = reference.getDigestValue();
                        String digestValueHexadecimal = null;

                        // on vérifie que l'empreinte est bien en binaire et non en hexadecimal (retrocompatibilité avec l'ancien fonctionnement)
                        if (digestValue.length == 20 || digestValue.length == 32) {
                            digestValueHexadecimal = HashUtil.convertirBinaireEnHexadecimal(digestValue);
                        } else {
                            digestValueHexadecimal = new String(digestValue);
                        }

                        boolean hashOk = false;
                        // cas d'un fichier
                        for (String hashFichier : hashFichiers) {
                            if (hashFichier.equals(digestValueHexadecimal)) {
                                hashOk = true;
                                break;
                            }
                        }

                        if (!hashOk) {
                            infosVerificationCertificat.setEtat(XMLConstantes.VALIDATION_SIGNED_HASH_NOK);
                            infosVerificationCertificat.setSignatureValide(false);
                            return infosVerificationCertificat;
                        }

                    } else if (j == 1) {
                        // cas particulier où l'url se trouve être http://atexo.com/#signedProps
                        String referenceDigestValue = Base64.encodeBytes(reference.getDigestValue());
                        if ((URI_CONSTANTE.equals(reference.getURI()) && HASHS_CONSTANTE_URI_ATEXO.contains(referenceDigestValue)) || HASH_CONSTANTE_URI_ATEXO_NOUVEAU.equals(referenceDigestValue)) {
                            //ne rien faire
                        } else {
                            // si pas uri
                            if (!URI_CONSTANTE.equals(reference.getURI())) {
                                validateContext.setURIDereferencer(new TagNameURIDereferencer(document, XMLConstantes.XADES_PREFIX, "SignedProperties"));
                            }
                            boolean refValid = reference.validate(validateContext);
                            if (!refValid) {
                                infosVerificationCertificat.setEtat(XMLConstantes.VALIDATION_SIGNED_PROPERTIES_NOK);
                                infosVerificationCertificat.setSignatureValide(false);
                                return infosVerificationCertificat;
                            }
                        }
                    }
                }
            } else {
                infosVerificationCertificat.setEtat(XMLConstantes.VALIDATION_NOK);
                infosVerificationCertificat.setSignatureValide(false);
                return infosVerificationCertificat;
            }
        } else {
            infosVerificationCertificat.setEtat(XMLConstantes.VALIDATION_NOK);
            infosVerificationCertificat.setSignatureValide(false);
            return infosVerificationCertificat;
        }

        infosVerificationCertificat.setEtat(XMLConstantes.VALIDATION_OK);
        infosVerificationCertificat.setSignatureValide(true);

        return infosVerificationCertificat;
    }

    /**
     * Ajoute le contenu de la balise UnsignedProperties XAdES dans un fichier de signature XAdES.
     *
     * @param signatureXML      le contenu du fichier de signature au format XAdES
     * @param certificatRefs    la liste des certificats autorisés
     * @param crlRevocationRefs la liste des crl révoqués
     * @param typeAlgorithmHash le type d'algorith de hash
     * @return le contenu du fichier de signature au format XAdes enrichi des informations
     * @throws Exception
     */
    public static String ajouterUnsignedProperties(String signatureXML, List<InfosCertificat> certificatRefs, List<InfosCrl> crlRevocationRefs, TypeAlgorithmHash typeAlgorithmHash) throws Exception {

        // on vérifie qu'il exite bien des informations sur la liste des certifcats et des crls
        if (!certificatRefs.isEmpty() || !crlRevocationRefs.isEmpty()) {

            InputSource inputSource = new InputSource(new StringReader(signatureXML));

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            Document document = documentBuilderFactory.newDocumentBuilder().parse(inputSource);

            // Find the QualifyingProperties element
            NodeList nodeList = document.getElementsByTagName(XMLConstantes.XADES_PREFIX + ":QualifyingProperties");

            // create the UnsignedProperties element and add it to the document
            Element unsignedProperties = creerElementUnsignedProperties(document, certificatRefs, crlRevocationRefs, typeAlgorithmHash);
            nodeList.item(0).appendChild(unsignedProperties);

            String contenuFichierSignatureXML = XMLUtil.convertir(document, FileUtil.ENCODING_UTF_8);
            return contenuFichierSignatureXML;

        } else {
            return signatureXML;
        }
    }


    /**
     * Crée un élement QualifyingProperties XAdES.
     *
     * @param document          le document contenant la structure du fichier de signature XML
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @param cible             la référence de l'identifiant de la balise signature auquel cet element sera lié
     * @return l'élement
     */
    private static Element creerElementQualifyingProperties(Document document, TypeAlgorithmHash typeAlgorithmHash, String cible) {

        // create the qualifyingProperties element and add it to the document
        Element qualifyingProperties = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "QualifyingProperties");
        qualifyingProperties.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:" + XMLConstantes.XADES_PREFIX, typeAlgorithmHash.getNameSpaceVersionXades());
        qualifyingProperties.setAttributeNS(null, "Target", cible);
        qualifyingProperties.setPrefix(XMLConstantes.XADES_PREFIX);

        return qualifyingProperties;
    }

    /**
     * Crée un élement SignedProperties XAdES.
     *
     * @param document          le document contenant la structure du fichier de signature XML
     * @param dateSignature     la date de la signature effectuée
     * @param certificate       le certificat utilisée pour générer la signature
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @return l'élement
     * @throws ParserConfigurationException
     * @throws NoSuchAlgorithmException
     * @throws CertificateEncodingException
     */
    private static Element creerElementSignedProperties(Document document, Date dateSignature, String referenceURI, X509Certificate certificate, TypeAlgorithmHash typeAlgorithmHash) throws ParserConfigurationException, CertificateEncodingException, NoSuchAlgorithmException {

        // create SignedProperties element, add an attribute, and add to qualifyingProperties element
        Element signedProperties = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "SignedProperties");
        signedProperties.setPrefix(XMLConstantes.XADES_PREFIX);
        //signedProperties.setAttributeNS(XMLConstants.XMLNS_ATTRIBUTE_NS_URI, "xmlns:" + XMLConstantes.XADES_PREFIX, typeAlgorithmHash.getNameSpaceVersionXades());
        signedProperties.setAttributeNS(null, "Id", referenceURI);

        // create SignedSignatureProperties element, and add to signedProperties element
        Element signedSignatureProperties = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "SignedSignatureProperties");
        signedSignatureProperties.setPrefix(XMLConstantes.XADES_PREFIX);
        signedProperties.appendChild(signedSignatureProperties);

        // SigningTime : create SigningTime element, and add to
        // SignedSignatureProperties element
        Element signingTime = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "SigningTime");
        signingTime.setPrefix(XMLConstantes.XADES_PREFIX);
        signedSignatureProperties.appendChild(signingTime);

        // add a text element to the child signingTime
        String dateSignatureISO8601 = Util.creerISO8601DateTime(dateSignature);
        Text textSigningTime = document.createTextNode(dateSignatureISO8601);
        signingTime.appendChild(textSigningTime);

        // SigningCertificate : create SigningCertificate element, and add to
        // SignedSignatureProperties element
        Element signingCertificate = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "SigningCertificate");
        signingCertificate.setPrefix(XMLConstantes.XADES_PREFIX);
        signedSignatureProperties.appendChild(signingCertificate);

        // SigningCertificate->Cert : create Cert element, and add to SigningCertificate element
        Element cert = creerElementCert(document, XMLConstantes.XADES_PREFIX, XMLSignature.XMLNS, XMLConstantes.DSIG_PREFIX, certificate, typeAlgorithmHash);
        signingCertificate.appendChild(cert);

        return signedProperties;
    }

    /**
     * Crée un élement UnsignedProperties XAdES.
     *
     * @param document          le document contenant la structure du fichier de signature XML
     * @param certificatRefs    la liste des certificats
     * @param crlRevocationRefs la liste des crl revoqués
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @return l'élement
     * @throws ParserConfigurationException
     * @throws NoSuchAlgorithmException
     * @throws CertificateEncodingException
     */
    private static Element creerElementUnsignedProperties(Document document, List<InfosCertificat> certificatRefs, List<InfosCrl> crlRevocationRefs, TypeAlgorithmHash typeAlgorithmHash) throws CertificateEncodingException, CRLException {

        // create the UnsignedProperties element and add it to the document
        Element unsignedProperties = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "UnsignedProperties");
        unsignedProperties.setPrefix(XMLConstantes.XADES_PREFIX);

        // create UnsignedSignatureProperties element, and add to UnsignedProperties element
        Element unsignedSignatureProperties = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "UnsignedSignatureProperties");
        unsignedSignatureProperties.setPrefix(XMLConstantes.XADES_PREFIX);
        unsignedProperties.appendChild(unsignedSignatureProperties);

        if (!certificatRefs.isEmpty()) {

            // create CompleteCertificateRefs element, and add to UnsignedSignatureProperties element
            Element completeCertificateRefs = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "CompleteCertificateRefs");
            completeCertificateRefs.setPrefix(XMLConstantes.XADES_PREFIX);
            unsignedSignatureProperties.appendChild(completeCertificateRefs);

            // create CertRefs element, and add to CompleteCertificateRefs element
            Element certRefs = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "CertRefs");
            certRefs.setPrefix(XMLConstantes.XADES_PREFIX);
            completeCertificateRefs.appendChild(certRefs);

            for (InfosCertificat infosCertificat : certificatRefs) {
                // create Cert element, and add to CertRefs element
                Element cert = creerElementCert(document, XMLConstantes.XADES_PREFIX, XMLSignature.XMLNS, XMLConstantes.DSIG_PREFIX, infosCertificat.getCertificat(), typeAlgorithmHash);
                certRefs.appendChild(cert);
            }
        }

        if (!crlRevocationRefs.isEmpty()) {
            // create CompleteRevocationRefs element, and add to UnsignedSignatureProperties element
            Element completeRevocationRefs = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "CompleteRevocationRefs");
            completeRevocationRefs.setPrefix(XMLConstantes.XADES_PREFIX);
            unsignedSignatureProperties.appendChild(completeRevocationRefs);

            // create CRLRefs element, and add to CompleteRevocationRefs element
            Element crlRefs = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "CRLRefs");
            crlRefs.setPrefix(XMLConstantes.XADES_PREFIX);
            completeRevocationRefs.appendChild(crlRefs);

            for (InfosCrl infosCrl : crlRevocationRefs) {
                // create Cert element, and add to CertRefs element
                Element cert = creerElementCRLRef(document, XMLConstantes.XADES_PREFIX, XMLSignature.XMLNS, XMLConstantes.DSIG_PREFIX, infosCrl.getCrl(), typeAlgorithmHash);
                crlRefs.appendChild(cert);
            }
        }

        return unsignedProperties;
    }


    /**
     * Crée un élement IssuerSerial XAdes.
     *
     * @param document       le document
     * @param nameSpaceXades le namespace XAdes
     * @param prefixXades    le préfixe XAdes
     * @param nameSpaceDsig  le namespace XMLDsig
     * @param prefixDsig     le préfixe XMLDsig
     * @param certificate    le certificat
     * @return l'élement
     */
    private static Element creerElementIssuerSerial(Document document, String nameSpaceXades, String prefixXades, String nameSpaceDsig, String prefixDsig, X509Certificate certificate) {

        /*
           <xades:IssuerSerial>
               <ds:X509IssuerName>CN=ATEXO AC DEMO - Entreprises, OU=ATEXO AC DEMO, O=ATEXO, L=PARIS,
                   C=FR
               </ds:X509IssuerName>
               <ds:X509SerialNumber>1</ds:X509SerialNumber>
           </xades:IssuerSerial>
        */

        // create IssuerSerial element, and add to Cert element
        Element issuerSerial = document.createElementNS(nameSpaceXades, "IssuerSerial");
        issuerSerial.setPrefix(prefixXades);

        // create X509IssuerName element, and add to IssuerSerial element
        Element x509IssuerName = document.createElementNS(nameSpaceDsig, "X509IssuerName");
        x509IssuerName.setPrefix(prefixDsig);
        issuerSerial.appendChild(x509IssuerName);

        // add a text element to the child X509IssuerName
        Text textX509IssuerName = document.createTextNode(certificate.getIssuerX500Principal().getName(XMLConstantes.RFC1779));
        x509IssuerName.appendChild(textX509IssuerName);

        // create X509SerialNumber element, and add to IssuerSerial element
        Element x509SerialNumber = document.createElementNS(nameSpaceDsig, "X509SerialNumber");
        x509SerialNumber.setPrefix(prefixDsig);
        issuerSerial.appendChild(x509SerialNumber);


        // add a text element to the child X509IssuerName
        Text textX509SerialNumber = document.createTextNode(String.valueOf(certificate.getSerialNumber()));
        x509SerialNumber.appendChild(textX509SerialNumber);

        return issuerSerial;
    }

    /**
     * Crée un élement de type Digest XAdes (DigestAlgAndValue,CertDigest,...).
     *
     * @param document          le document
     * @param nameSpaceXades    le namespace XAdes
     * @param prefixXades       le préfixe XAdes
     * @param nameSpaceDsig     le namespace XMLDsig
     * @param prefixDsig        le préfixe XMLDsig
     * @param nomElement        le nom de l'element certificat
     * @param infosEncodes      les infos encodés à patir desquels un hash sera généré
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @return l'élement
     */
    private static Element creerElementDigest(Document document, String nameSpaceXades, String prefixXades, String nameSpaceDsig, String prefixDsig, String nomElement, byte[] infosEncodes, TypeAlgorithmHash typeAlgorithmHash) {

        /*
           <xades:nomElement>
               <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
               <ds:DigestValue>7C35AFC5283CC8E89484AB74812A6E2CDBE71C83</ds:DigestValue>
           </xades:nomElement>
        */

        // create IssuerSerial element, and add to Cert element
        Element digestElement = document.createElementNS(nameSpaceXades, nomElement);
        digestElement.setPrefix(prefixXades);

        // create DigestMethod element, and add to CertDigest element
        Element digestMethod = document.createElementNS(nameSpaceDsig, "DigestMethod");
        digestMethod.setPrefix(prefixDsig);
        digestElement.appendChild(digestMethod);
        digestMethod.setAttributeNS(null, "Algorithm", typeAlgorithmHash.getAlgoDigestMethod());

        // create DigestValue element, and add to CertDigest element
        Element digestValue = document.createElementNS(nameSpaceDsig, "DigestValue");
        digestValue.setPrefix(prefixDsig);
        digestElement.appendChild(digestValue);

        // add a text element to the child DigestValue        
        byte[] hash = HashUtil.genererHashShaBinaire(typeAlgorithmHash, infosEncodes);
        String hashEnBase64 = Base64.encodeBytes(hash);
        Text textDigestValueCrtRef = document.createTextNode(hashEnBase64);
        digestValue.appendChild(textDigestValueCrtRef);

        return digestElement;
    }

    /**
     * Crée un élement Cert XAdes
     *
     * @param document          le document
     * @param prefixXades       le préfixe XAdes
     * @param nameSpaceDsig     le namespace XMLDsig
     * @param prefixDsig        le préfixe XMLDsig
     * @param certificate       le certificat
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @return l'élement
     */
    private static Element creerElementCert(Document document, String prefixXades, String nameSpaceDsig, String prefixDsig, X509Certificate certificate, TypeAlgorithmHash typeAlgorithmHash) throws CertificateEncodingException {

        /*
           <xades:Cert>
               <xades:CertDigest>
                   <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
                   <ds:DigestValue>7C35AFC5283CC8E89484AB74812A6E2CDBE71C83</ds:DigestValue>
               </xades:CertDigest>
               <xades:IssuerSerial>
                   <ds:X509IssuerName>CN=ATEXO AC DEMO - Entreprises, OU=ATEXO AC DEMO, O=ATEXO, L=PARIS,
                       C=FR
                   </ds:X509IssuerName>
                   <ds:X509SerialNumber>1</ds:X509SerialNumber>
               </xades:IssuerSerial>
           </xades:Cert>
        */

        // Cert : create Cert element
        Element cert = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "Cert");
        cert.setPrefix(prefixXades);

        // Cert->CertDigest : create CertDigest element, and add to Cert element
        Element certDigest = creerElementDigest(document, typeAlgorithmHash.getNameSpaceVersionXades(), prefixXades, nameSpaceDsig, prefixDsig, "CertDigest", certificate.getEncoded(), typeAlgorithmHash);
        cert.appendChild(certDigest);

        // Cert->X509IssuerSerial: create X509IssuerSerial element, and add to Cert element
        Element x509IssuerSerial = creerElementIssuerSerial(document, typeAlgorithmHash.getNameSpaceVersionXades(), prefixXades, nameSpaceDsig, prefixDsig, certificate);
        cert.appendChild(x509IssuerSerial);

        return cert;
    }

    /**
     * Crée un élement CRLRef XAdES.
     *
     * @param document          le document
     * @param prefixXades       le préfixe XAdes
     * @param nameSpaceDsig     le namespace XMLDsig
     * @param prefixDsig        le préfixe XMLDsig
     * @param crl               le crl
     * @param typeAlgorithmHash le type d'algorithm de hash à utiliser
     * @return l'élement
     */
    private static Element creerElementCRLRef(Document document, String prefixXades, String nameSpaceDsig, String prefixDsig, X509CRL crl, TypeAlgorithmHash typeAlgorithmHash) throws CRLException {

        /*
            <xades:CRLRef>
               <xades:DigestAlgAndValue>
                   <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
                   <ds:DigestValue>
                       C09E14D506E7063554EB85F58E7DA716C5D09B566626CE16F31EBDD9797BD622
                   </ds:DigestValue>
               </xades:DigestAlgAndValue>
               <xades:CRLIdentifier>
                   <xades:Issuer>CN=ATEXO AC DEMO - Entreprises, OU=ATEXO AC DEMO, O=ATEXO, L=PARIS,
                       C=FR
                   </xades:Issuer>
                   <xades:IssueTime>2013-07-02T21:30:02+02:00</xades:IssueTime>
               </xades:CRLIdentifier>
            </xades:CRLRef>
        */

        // CRLRef : create CRLRef element
        Element crlRef = document.createElementNS(typeAlgorithmHash.getNameSpaceVersionXades(), "CRLRef");
        crlRef.setPrefix(prefixXades);

        // CRLRef->DigestAlgAndValue : create DigestAlgAndValue element, and add to CRLRef element
        Element certDigest = creerElementDigest(document, typeAlgorithmHash.getNameSpaceVersionXades(), prefixXades, nameSpaceDsig, prefixDsig, "DigestAlgAndValue", crl.getEncoded(), typeAlgorithmHash);
        crlRef.appendChild(certDigest);

        // CRLRef->CRLIdentifier : create CRLIdentifier element, and add to CRLRef element
        Element crlIdentifier = creerElementCRLIdentifier(document, typeAlgorithmHash.getNameSpaceVersionXades(), prefixXades, crl);
        crlRef.appendChild(crlIdentifier);

        return crlRef;
    }

    /**
     * Crée un élement CRLIdentifier XAdES.
     *
     * @param document       le document
     * @param nameSpaceXades le namespace XAdES
     * @param prefixXades    le préfixe XAdES
     * @param crl            le crl
     * @return l'élement
     */
    private static Element creerElementCRLIdentifier(Document document, String nameSpaceXades, String prefixXades, X509CRL crl) {

        /*
           <xades:CRLIdentifier>
               <xades:Issuer>CN=ATEXO AC DEMO - Entreprises, OU=ATEXO AC DEMO, O=ATEXO, L=PARIS,
                   C=FR
               </xades:Issuer>
               <xades:IssueTime>2013-07-02T21:30:02+02:00</xades:IssueTime>
           </xades:CRLIdentifier>
        */

        // create CRLIdentifier element
        Element crlIdentifier = document.createElementNS(nameSpaceXades, "CRLIdentifier");
        crlIdentifier.setPrefix(prefixXades);

        // create Issuer element, and add to CRLIdentifier element
        Element issuer = document.createElementNS(nameSpaceXades, "Issuer");
        issuer.setPrefix(prefixXades);
        crlIdentifier.appendChild(issuer);

        // add a text element to the child Issuer
        Text textX509IssuerName = document.createTextNode(crl.getIssuerX500Principal().getName(XMLConstantes.RFC1779));
        issuer.appendChild(textX509IssuerName);

        // create IssueTime element, and add to CRLIdentifier element
        Element issueTime = document.createElementNS(nameSpaceXades, "IssueTime");
        issueTime.setPrefix(prefixXades);
        crlIdentifier.appendChild(issueTime);

        String date = Util.creerISO8601DateTime(crl.getThisUpdate());

        // add a text element to the child IssueTime
        Text textIssueTime = document.createTextNode(date);
        issueTime.appendChild(textIssueTime);

        return crlIdentifier;
    }

    /**
     * Extrait la date de signature du contenu du fichier XML de signature au format XAdES.
     *
     * @param contenuSignatureXML le contenu de la signature XML XAdES
     * @return La date de signature
     */
    public static Date getDateSignature(String contenuSignatureXML) {
        Date dateSignature = null;
        String baliseOuvrante = "<xades:SigningTime>";
        String baliseFermante = "</xades:SigningTime>";
        if (contenuSignatureXML != null && contenuSignatureXML.contains(baliseOuvrante) && contenuSignatureXML.contains(baliseFermante)) {
            int positionDebutSignature = contenuSignatureXML.indexOf(baliseOuvrante);
            int positionFinSignature = contenuSignatureXML.indexOf(baliseFermante);
            String dateSignatureString = contenuSignatureXML.substring(positionDebutSignature + baliseOuvrante.length(), positionFinSignature);
            if (dateSignatureString != null) {
                try {
                    if (!dateSignatureString.contains("T")) {
                        try {
                            dateSignature = Util.convertirDate(extraireDate(dateSignatureString), Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);
                        } catch (ParseException e) {}
                    } else {
                        dateSignature = Util.convertirISO8601DateTime(dateSignatureString);
                    }
                } catch (IllegalArgumentException e) {}
            }
        }

        return dateSignature;
    }

    /**
     * Extrait la date de signature du contenu du fichier XML de signature au format XAdES.
     *
     * @param contenuFichierXML le contenu de la signature XML XAdES
     * @return La date de signature
     */
    public static String getSigningTime(String contenuFichierXML) {
        int positionDebut = contenuFichierXML.indexOf("<xades:SigningTime>");
        int positionFin = contenuFichierXML.indexOf("</xades:SigningTime>");
        String date = contenuFichierXML.substring(positionDebut + "<xades:SigningTime>".length(), positionFin);
        return date;
    }

    /**
     * Converti la date de signature (ancien et nouveau format)
     *
     * @param dateSignature la date de signature
     * @return la date du format YYYYmmDDHHmmss
     */
    public static String extraireDate(String dateSignature) {
        String date = Util.remplacerPar(dateSignature, new String[]{" ", ":", "-", "T"}, "");
        if (date.length() > 14) {
            date = date.substring(0, 14);
        }

        return date;
    }
}
