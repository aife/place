package fr.atexo.signature.commun.util;

import static fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes.TAILLE_MAXIMALE_BLOC_CHIFFREMENT;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.util.AffichageUtil;
import fr.atexo.signature.xml.annonce.ObjectFactory;
import fr.atexo.signature.xml.annonce.ReponseAnnonce;
import fr.atexo.signature.xml.annonce.ReponseAnnonceType;
import fr.atexo.signature.xml.annonce.ReponsesAnnonce;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;

/**
 * Classe d'utilitaire permettant de créer une ReponsesAnnonce XML.
 */
public abstract class JaxbReponsesAnnonceUtil {

	private final static ObjectFactory factory = new ObjectFactory();

	public static ReponsesAnnonce getReponsesAnnonce(String reponsesAnnonceXML, String encoding) {
		String context = ReponsesAnnonce.class.getPackage().getName();
		Object reponsesAnnonceObject = JAXBService.instance().getAsObject(reponsesAnnonceXML, context, encoding);
		return (ReponsesAnnonce) reponsesAnnonceObject;
	}

	public static ReponsesAnnonce creerReponsesAnnonce() {
		ReponsesAnnonce reponsesAnnonce = factory.createReponsesAnnonce();

		ReponseAnnonceType reponseAnnonce = factory.createReponseAnnonceType();
		reponsesAnnonce.getReponseAnnonce().add(reponseAnnonce);

		ReponseAnnonceType.Enveloppes enveloppes = factory.createReponseAnnonceTypeEnveloppes();
		reponseAnnonce.setEnveloppes(enveloppes);

		return reponsesAnnonce;
	}

	public static ReponseAnnonce creerReponseAnnonce() {
		ReponseAnnonce reponseAnnonce = factory.createReponseAnnonce();
		ReponseAnnonceType.Enveloppes enveloppes = factory.createReponseAnnonceTypeEnveloppes();
		reponseAnnonce.setEnveloppes(enveloppes);

		return reponseAnnonce;
	}

	public static ReponseAnnonceType.Enveloppes.Enveloppe creerEnveloppe(int typeEnveloppe, int numeroLot, boolean ouverte, boolean chiffree, String statutAdmissibilite) {
		ReponseAnnonceType.Enveloppes.Enveloppe enveloppe = factory.createReponseAnnonceTypeEnveloppesEnveloppe();
		enveloppe.setType(typeEnveloppe);
		enveloppe.setNumLot(numeroLot);
		// les différents statuts
		enveloppe.setStatutAdmissibilite(statutAdmissibilite);
		String statutChiffrement = chiffree ? ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE : ReponseAnnonceConstantes.STATUT_CHIFFREMENT_NON_CHIFFRE;
		enveloppe.setStatutChiffrement(statutChiffrement);
		String statutOuverture = ouverte ? ReponseAnnonceConstantes.STATUT_OUVERTURE_OUVERT : ReponseAnnonceConstantes.STATUT_OUVERTURE_FERME;
		enveloppe.setStatutOuverture(statutOuverture);

		return enveloppe;
	}

	public static ReponseAnnonceType.Enveloppes.Enveloppe.Fichier creerFichier(String nomFichier, String typeFichier, String origineFichier, int numeroFichier, int tailleFichier, String empreinte, String signature, boolean chiffree, boolean hashFichier, int nombreBlocs) {
		ReponseAnnonceType.Enveloppes.Enveloppe.Fichier fichier = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichier();
		fichier.setNom(nomFichier);
		fichier.setTypeFichier(typeFichier);
		fichier.setOrigineFichier(origineFichier);
		fichier.setNumOrdre(numeroFichier);
		fichier.setTailleEnClair(tailleFichier);
		fichier.setEmpreinte(empreinte);
		fichier.setSignature(signature);
		fichier.setHash(hashFichier);

		String statutChiffrement = chiffree ? ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE : ReponseAnnonceConstantes.STATUT_CHIFFREMENT_NON_CHIFFRE;
		fichier.setStatutChiffrement(statutChiffrement);

		fichier.setNbrBlocsChiffrement(nombreBlocs);
		ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement blocsChiffrement = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrement();
		blocsChiffrement.setNbrBlocs(nombreBlocs);
		fichier.setBlocsChiffrement(blocsChiffrement);

		return fichier;
	}

	public static ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement creerBlocChiffrement(boolean chiffree, String empreinte, Integer tailleEnClair, Integer tailleApresChiffrement, int numeroBloc) {
		ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement blocChiffrement = factory.createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrementBlocChiffrement();
		blocChiffrement.setEmpreinte(empreinte);
		String statutChiffrement = chiffree ? ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE : ReponseAnnonceConstantes.STATUT_CHIFFREMENT_NON_CHIFFRE;
		blocChiffrement.setStatutChiffrement(statutChiffrement);
		blocChiffrement.setTailleEnClair(tailleEnClair);
		blocChiffrement.setTailleApresChiffrement(tailleApresChiffrement);
		blocChiffrement.setNumOrdre(numeroBloc);
		return blocChiffrement;
	}

	public static Map<Integer, List<FichierBloc>> extraireFichierBlocChiffrements(ReponsesAnnonce reponsesAnnonce, Set<Integer> idEnveloppes) {

		Map<Integer, List<FichierBloc>> map = new LinkedHashMap<Integer, List<FichierBloc>>();

		int indexReponseAnnonceType = 1;

		LogManager.getInstance().afficherMessageInfo("Nombre de réponses : " + reponsesAnnonce.getReponseAnnonce().size(), JaxbReponsesAnnonceUtil.class);

		for (ReponseAnnonceType reponseAnnonceType : reponsesAnnonce.getReponseAnnonce()) {

			Integer indexReponse = reponseAnnonceType.getIndexReponse();
			if (indexReponse != null && indexReponse != -1) {
				indexReponseAnnonceType = indexReponse;
			}

			LogManager.getInstance().afficherMessageInfo("Traitement de la réponse : " + reponseAnnonceType.getIdReponseAnnonce(), JaxbReponsesAnnonceUtil.class);

			ReponseAnnonceType.Annonce annonce = reponseAnnonceType.getAnnonce();
			ReponseAnnonceType.OperateurEconomique operateurEconomique = reponseAnnonceType.getOperateurEconomique();

			for (ReponseAnnonceType.Enveloppes.Enveloppe enveloppe : reponseAnnonceType.getEnveloppes().getEnveloppe()) {

				if (idEnveloppes == null || (idEnveloppes != null && idEnveloppes.contains(enveloppe.getIdEnveloppe()))) {

					List<FichierBloc> fichierBlocs = map.get(enveloppe.getIdEnveloppe());
					if (fichierBlocs == null) {
						fichierBlocs = new ArrayList<FichierBloc>();
						map.put(enveloppe.getIdEnveloppe(), fichierBlocs);
					}

					List<FichierBloc> fichierBlocsTemporaire = extraireFichierBlocChiffrements(annonce, enveloppe, operateurEconomique, indexReponseAnnonceType);
					if (fichierBlocsTemporaire != null && !fichierBlocsTemporaire.isEmpty()) {
						fichierBlocs.addAll(fichierBlocsTemporaire);
					}
				}
			}
			indexReponseAnnonceType++;
		}

		return map;
	}

	public static List<FichierBloc> extraireFichierBlocChiffrements(ReponseAnnonceType.Annonce annonce, ReponseAnnonceType.Enveloppes.Enveloppe enveloppe, ReponseAnnonceType.OperateurEconomique operateurEconomique, int index) {

		List<FichierBloc> liste = new ArrayList<FichierBloc>();

		String nomRepertoireEnveloppe = "El" + index + "_" + AffichageUtil.getTypeEnvNomRepertoire(enveloppe.getType(), enveloppe.getNumLot());

		for (ReponseAnnonceType.Enveloppes.Enveloppe.Fichier fichier : enveloppe.getFichier()) {

			List<FichierBloc.Bloc> blocs = new ArrayList<FichierBloc.Bloc>();

			for (ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement blocChiffrement : fichier.getBlocsChiffrement().getBlocChiffrement()) {

				Integer id = blocChiffrement.getNomBloc() != null ? Integer.valueOf(blocChiffrement.getNomBloc()) : null;
				Integer numero = blocChiffrement.getNumOrdre();
				Integer taille = blocChiffrement.getTailleApresChiffrement();
				boolean chiffre = ReponseAnnonceConstantes.STATUT_CHIFFREMENT_CHIFFRE.equals(blocChiffrement.getStatutChiffrement());

				FichierBloc.Bloc bloc = new FichierBloc.Bloc(id, numero, taille, chiffre);
				blocs.add(bloc);
			}

			String informationsUtilisateur = annonce != null ? annonce.getReferenceUtilisateur() : null;
			FichierBloc fichierBloc = new FichierBloc(enveloppe.getType(), enveloppe.getNumLot(), fichier.getNom(), blocs, informationsUtilisateur, fichier.getSignature(), fichier.getTailleEnClair());
			fichierBloc.setNomEnveloppe(nomRepertoireEnveloppe);
			if (operateurEconomique != null && !Util.estVide(operateurEconomique.getNom())) {
				fichierBloc.setNomOperateurEconomique(operateurEconomique.getNom());
			}
			liste.add(fichierBloc);
		}

		return liste;
	}

	public static class FichierBloc {

		protected int typeEnveloppe;

		protected int numeroLot;

		private String nomEnveloppe;

		private String nomOperateurEconomique;

		private final String nomFichier;

		private List<Bloc> blocs = new ArrayList<Bloc>();

		private final String referenceUtilisateur;

		private final String signatureXades;

		private final long taille;

		public static class Bloc {

			private final Integer id;

			private final Integer numero;

			private final long tailleChiffre;

			private final boolean chiffre;

			public Bloc(Integer id, Integer numero, long tailleChiffre, boolean chiffre) {
				this.id = id;
				this.numero = numero;
				this.tailleChiffre = tailleChiffre;
				this.chiffre = chiffre;
			}

			public Integer getId() {
				return id;
			}

			public long getTailleChiffre() {
				return tailleChiffre;
			}

			public boolean isChiffre() {
				return chiffre;
			}

			public Integer getNumero() {
				return numero;
			}
		}

		public FichierBloc(int typeEnveloppe, int numeroLot, String nomFichier, List<Bloc> blocs, String referenceUtilisateur, String signatureXades, long taille) {
			this.typeEnveloppe = typeEnveloppe;
			this.numeroLot = numeroLot;
			this.nomFichier = nomFichier;
			this.blocs = blocs;
			this.referenceUtilisateur = referenceUtilisateur;
			this.signatureXades = signatureXades;
			this.taille = taille;
		}

		public int getTypeEnveloppe() {
			return typeEnveloppe;
		}

		public int getNumeroLot() {
			return numeroLot;
		}

		public String getNomFichier() {
			return nomFichier;
		}

		public List<Bloc> getBlocs() {
			return blocs;
		}

		public String getReferenceUtilisateur() {
			return referenceUtilisateur;
		}

		public String getSignatureXades() {
			return signatureXades;
		}

		public long getTaille() {
			return taille;
		}

		public String getNomEnveloppe() {
			return nomEnveloppe;
		}

		public void setNomEnveloppe(String nomEnveloppe) {
			this.nomEnveloppe = nomEnveloppe;
		}

		public String getNomOperateurEconomique() {
			return nomOperateurEconomique;
		}

		public void setNomOperateurEconomique(String nomOperateurEconomique) {
			this.nomOperateurEconomique = nomOperateurEconomique;
		}
	}

	/**
	 * Retourne le nombre de blocs dans lesquels sera découpé le fichier.
	 * 
	 * @param fichier
	 *            le fichier à analyser
	 * @return le nombre de blocs
	 */
	public static int getNombreDeBlocChiffrement(File fichier) {
		long l = fichier.length() / TAILLE_MAXIMALE_BLOC_CHIFFREMENT;
		if (fichier.length() % TAILLE_MAXIMALE_BLOC_CHIFFREMENT == 0) {
			return (int) fichier.length() / TAILLE_MAXIMALE_BLOC_CHIFFREMENT;
		}
		return (int) fichier.length() / TAILLE_MAXIMALE_BLOC_CHIFFREMENT + 1;
	}
}
