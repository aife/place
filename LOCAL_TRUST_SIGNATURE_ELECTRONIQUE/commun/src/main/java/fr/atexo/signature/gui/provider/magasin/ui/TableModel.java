package fr.atexo.signature.gui.provider.magasin.ui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.atexo.signature.commun.securite.provider.CertificatItem;


public class TableModel extends AbstractTableModel {

    private static final long serialVersionUID = -8979696875693066443L;
    private static final int NOM_CERTIF = 0;
    private static final int NOM_EMETTEUR = 1;
    private static final int DATE = 2;
    private static final int USAGE = 3;

    protected String[] columnNames;
    protected List<CertificatItem> certificateItems;

    public TableModel(String[] colonnes, List<CertificatItem> certificats) {
        this.columnNames = colonnes;
        this.certificateItems = certificats;
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }


    public int getRowCount() {
        return certificateItems.size();
    }

    public Object getValueAt(int row, int col) {
        CertificatItem certificate = (CertificatItem) certificateItems.get(row);
        switch (col) {
            case TableModel.NOM_CERTIF:
                return certificate.getNom();
            case TableModel.NOM_EMETTEUR:
                return certificate.getNomEmetteur();
            case TableModel.DATE:
                return certificate.getStrDateExpiration();
            case TableModel.USAGE:
                return certificate.getUtilisationCle();
            default:
                return "";
        }
    }
}



