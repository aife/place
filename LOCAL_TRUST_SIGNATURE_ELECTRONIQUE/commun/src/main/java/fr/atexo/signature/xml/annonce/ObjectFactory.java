
package fr.atexo.signature.xml.annonce;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.atexo.signature.xml.annonce package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.atexo.signature.xml.annonce
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Enveloppes.Enveloppe }
     * 
     */
    public ReponseAnnonceType.Enveloppes.Enveloppe createReponseAnnonceTypeEnveloppesEnveloppe() {
        return new ReponseAnnonceType.Enveloppes.Enveloppe();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement }
     * 
     */
    public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrementBlocChiffrement() {
        return new ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement }
     * 
     */
    public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement createReponseAnnonceTypeEnveloppesEnveloppeFichierBlocsChiffrement() {
        return new ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier }
     * 
     */
    public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier createReponseAnnonceTypeEnveloppesEnveloppeFichier() {
        return new ReponseAnnonceType.Enveloppes.Enveloppe.Fichier();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Annonce.TypesEnveloppe }
     * 
     */
    public ReponseAnnonceType.Annonce.TypesEnveloppe createReponseAnnonceTypeAnnonceTypesEnveloppe() {
        return new ReponseAnnonceType.Annonce.TypesEnveloppe();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Annonce }
     * 
     */
    public ReponseAnnonceType.Annonce createReponseAnnonceTypeAnnonce() {
        return new ReponseAnnonceType.Annonce();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.OperateurEconomique }
     * 
     */
    public ReponseAnnonceType.OperateurEconomique createReponseAnnonceTypeOperateurEconomique() {
        return new ReponseAnnonceType.OperateurEconomique();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Enveloppes }
     * 
     */
    public ReponseAnnonceType.Enveloppes createReponseAnnonceTypeEnveloppes() {
        return new ReponseAnnonceType.Enveloppes();
    }

    /**
     * Create an instance of {@link ReponsesAnnonce }
     * 
     */
    public ReponsesAnnonce createReponsesAnnonce() {
        return new ReponsesAnnonce();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe }
     * 
     */
    public ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe createReponseAnnonceTypeAnnonceTypesEnveloppeTypeEnveloppe() {
        return new ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe();
    }

    /**
     * Create an instance of {@link ReponseAnnonce }
     * 
     */
    public ReponseAnnonce createReponseAnnonce() {
        return new ReponseAnnonce();
    }

    /**
     * Create an instance of {@link ReponseAnnonceType }
     * 
     */
    public ReponseAnnonceType createReponseAnnonceType() {
        return new ReponseAnnonceType();
    }

}
