package fr.atexo.signature.gui.browser.event;

import java.util.EventListener;




public interface SelectDirectoryListener extends EventListener {

    public void onSelect(SelectDirectoryEvent evt);

}


