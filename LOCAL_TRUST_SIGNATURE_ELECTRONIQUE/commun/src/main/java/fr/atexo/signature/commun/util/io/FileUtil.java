package fr.atexo.signature.commun.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public abstract class FileUtil {

    public static final String SIGNATURE_XADES_FILE_EXTENSION_REGEX = " - ([0-9]+)\\ - Signature ([0-9]+)\\.xml";

    public static final String SIGNATURE_PKCS7_FILE_EXTENSION_REGEX = " - Signature ([0-9]+)\\.p7s";

    public static final String ENCODING_ISO_8859_1 = "ISO-8859-1";

    public static final String ENCODING_UTF_8 = "UTF-8";

    /**
     * Renvoi les bits du fichier depuis le début et la fin qui lui est passé en paramétre.
     *
     * @param fichier le fichier à lire
     * @param debut   le point de départ de la lecture
     * @param fin     la fin de la lecture
     * @return
     * @throws FileNotFoundException
     */
    public static byte[] lire(File fichier, int debut, int fin) throws IOException {
        byte[] bytes = new byte[fin];
        RandomAccessFile randomAccessFile = new RandomAccessFile(fichier, "r");
        randomAccessFile.seek(debut);
        randomAccessFile.read(bytes);
        randomAccessFile.close();
        return bytes;
    }

    /**
     * Retourne l'index de la prochaine signature qui devra être générée
     *
     * @param cheminFichier le chemin d'accès vers le fichier auquel est associé le fichier XML de signature
     * @return la prochaine valeur de l'index
     */
    public static int getProchaineIndexFichierSignature(String cheminFichier, String regex) {
        int indexFile = 0;
        File fichier = new File(cheminFichier);
        String repertoireParent = fichier.getParent();
        String signedFileName = fichier.getName();
        File repertoire = new File(repertoireParent);
        File[] dirFiles = repertoire.listFiles();
        for (int i = 0; i < dirFiles.length; i++) {
            File listedFile = dirFiles[i];
            if (listedFile.isFile()) {
                signedFileName = signedFileName.replaceAll("\\).*?\\(", " ").replaceAll("\\(|\\)", "");
                signedFileName = signedFileName.replaceAll("\\[(.*?)\\]", " ").replaceAll("\\[(.*?)\\]", "");

                String listedFileName = listedFile.getName();

                listedFileName = listedFileName.replaceAll("\\).*?\\(", " ").replaceAll("\\(|\\)", "");
                listedFileName = listedFileName.replaceAll("\\[(.*?)\\]", " ").replaceAll("\\[(.*?)\\]", "");
                Pattern patt = null;
                patt = Pattern.compile(signedFileName + regex);
                Matcher m = patt.matcher(listedFileName);
                if (m.find()) {
                    int currIndex = Integer.parseInt((listedFileName.substring(listedFileName.indexOf("Signature") + 9, listedFileName.length() - 4)).trim());
                    if (currIndex > indexFile) {
                        indexFile = currIndex;
                    }
                }
            }
        }
        return indexFile + 1;
    }

    public static int getProchaineIndexFichierSignatureXML(String cheminFichier) {
        return getProchaineIndexFichierSignature(cheminFichier, SIGNATURE_XADES_FILE_EXTENSION_REGEX);
    }

    public static int getProchaineIndexFichierSignaturePkcs7(String cheminFichier) {
        return getProchaineIndexFichierSignature(cheminFichier, SIGNATURE_PKCS7_FILE_EXTENSION_REGEX);
    }

    public static File getDernierFichierFichierSignatureXML(String cheminFichier) {
        int indexFile = 0;
        File fichier = new File(cheminFichier);
        String repertoireParent = fichier.getParent();
        String signedFileName = fichier.getName();
        File repertoire = new File(repertoireParent);
        File[] dirFiles = repertoire.listFiles();
        for (int i = 0; i < dirFiles.length; i++) {
            File listedFile = dirFiles[i];
            if (listedFile.isFile()) {
                signedFileName = signedFileName.replaceAll("\\).*?\\(", " ").replaceAll("\\(|\\)", "");
                signedFileName = signedFileName.replaceAll("\\[(.*?)\\]", " ").replaceAll("\\[(.*?)\\]", "");

                String listedFileName = listedFile.getName();

                listedFileName = listedFileName.replaceAll("\\).*?\\(", " ").replaceAll("\\(|\\)", "");
                listedFileName = listedFileName.replaceAll("\\[(.*?)\\]", " ").replaceAll("\\[(.*?)\\]", "");
                Pattern patt = null;
                patt = Pattern.compile(signedFileName + SIGNATURE_XADES_FILE_EXTENSION_REGEX);
                Matcher m = patt.matcher(listedFileName);
                if (m.find()) {

                    File file = new File(repertoire, listedFileName);
                    return file;

                }
            }
        }
        return null;
    }

}
