package fr.atexo.signature.util;

/**
 *
 */
public abstract class ModuleValidationUtil {

    public static Boolean getEtatChaineCertification(Integer code) {
        if (code == 0) {
            return true;
        } else if (code == 2) {
            return false;
        } else {
            return null;
        }
    }

    public static Boolean getEtatRevocation(Integer code) {
        if (code == 0) {
            return true;
        } else if (code == 2) {
            return false;
        } else {
            return null;
        }
    }

    public static Boolean getEtatValidite(Integer code) {
        if (code == 0) {
            return true;
        } else  {
            return false;
        }
    }
}
