package fr.atexo.signature.commun.securite.signature;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.Store;

import fr.atexo.pdfboxhelpers.CMSProcessableInputStream;
import fr.atexo.pdfboxhelpers.PDVisibleSigProperties;
import fr.atexo.pdfboxhelpers.PDVisibleSignDesigner;
import fr.atexo.pdfboxhelpers.PdfPKCS7;
import fr.atexo.pdfboxhelpers.VisualSignatureGenerator;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.securite.processor.signature.InfosVerificationCertificat;
import fr.atexo.signature.commun.util.CertificatUtil;

public class SignaturePades {

	private static final String DIGEST_ALGO = "SHA256withRSA";

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public static void signerFichierPdf(InputStream source, OutputStream destination, Certificate certificat, PrivateKey clefPrivee,
			VisualSignatureRectangle positionSignature, String texte) throws IOException {

		signerFichierPdf(source, destination, certificat, clefPrivee, positionSignature, null, null, texte);
	}

	public static void signerFichierPdf(final InputStream source, OutputStream destination, final Certificate certificat, final PrivateKey clefPrivee,
			VisualSignatureRectangle positionSignature, String raison, String lieu, String texte) throws IOException {

		// on dumpe les flux ( nécessaire pour la modif incrémentale )
		File document = File.createTempFile("fichier_pdf_applet_original_", ".pdf");
		FileOutputStream documentSourceOutputStream = null;

		File outputDocument = File.createTempFile("fichier_pdf_applet_original_signed_", ".pdf");

		FileInputStream fis = null;
		FileOutputStream fos = null;

		// préparation pour la modification incrémentale
		try {
			documentSourceOutputStream = new FileOutputStream(document);
			IOUtils.copy(source, documentSourceOutputStream);

			fis = new FileInputStream(document);
			fos = new FileOutputStream(outputDocument);

			IOUtils.copy(fis, fos);
		} catch (IOException e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(source);
			IOUtils.closeQuietly(documentSourceOutputStream);
			IOUtils.closeQuietly(fis);
		}

		try {

			fis = new FileInputStream(outputDocument);

			PDDocument doc = PDDocument.load(document);
			PDSignature signature = new PDSignature();
			signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
			signature.setSubFilter(PDSignature.SUBFILTER_ADBE_PKCS7_DETACHED);
			signature.setReason(raison);
			signature.setLocation(lieu);
			Calendar cal = Calendar.getInstance();
			signature.setSignDate(cal);

			SignatureOptions signatureOptions = new SignatureOptions();

			signatureOptions.setPage(doc.getNumberOfPages());

			int width = (int) (positionSignature.getRight() - positionSignature.getLeft());
			int height = (int) (positionSignature.getTop() - positionSignature.getBottom());

			PDVisibleSignDesigner visibleSig = new PDVisibleSignDesigner(doc, doc.getNumberOfPages());

			String nom = PdfPKCS7.getSubjectFields((X509Certificate) certificat).getField("CN");

			VisualSignatureGenerator visualSignatureGenerator = new VisualSignatureGenerator();

			BufferedImage signaturebBufferedImage = visualSignatureGenerator.getVisibleSignature(width, height, texte, nom, raison, lieu, cal.getTime());

			visibleSig.signatureBufferedImage(signaturebBufferedImage).xAxis(positionSignature.getLeft())
					.yAxis(visibleSig.getPageHeight() - positionSignature.getTop());

			PDVisibleSigProperties signatureProperties = new PDVisibleSigProperties();
			signatureProperties.signerLocation(lieu).signatureReason(raison).visualSignEnabled(true).setPdVisibleSignature(visibleSig).buildSignature();

			signatureOptions.setVisualSignature(signatureProperties.getVisibleSignature());

			SignatureInterface signatureInterface = new SignatureInterface() {
				@Override
				public byte[] sign(InputStream content) throws IOException {

					List<Certificate> certList = Arrays.asList(certificat);
					CMSTypedData msg = new CMSProcessableInputStream(content);

					try {
						Store certs = new JcaCertStore(certList);

						CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
						ContentSigner shasigner = new JcaContentSignerBuilder(DIGEST_ALGO).build(clefPrivee);

						gen.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build()).build(shasigner,
								(X509Certificate) certificat));

						gen.addCertificates(certs);
						CMSSignedData sigData = gen.generate(msg, false);

						return sigData.getEncoded();

					} catch (Exception e) {
						throw new RuntimeException("Problem while preparing signature", e);
					}
				}
			};

			doc.addSignature(signature, signatureInterface, signatureOptions);
			doc.saveIncremental(fis, fos);

		} catch (Exception e) {
			throw new IOException(e);
		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(fos);
		}

		// on copie le fichier dans le strem de sortie
		try {
			fis = new FileInputStream(outputDocument);
			IOUtils.copy(fis, destination);
			destination.flush();
		} catch (IOException e) {
			throw e;
		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(destination);
		}

	}

	public static List<InfosComplementairesCertificat> verifier(byte[] contenuFichierPdfSigne) throws IOException, SignatureException {

		return verifier(new ByteArrayInputStream(contenuFichierPdfSigne));
	}

	public static List<InfosComplementairesCertificat> verifier(File fichierPdfSigne) throws IOException, SignatureException {
		FileInputStream fis = null;
		ByteArrayOutputStream bos = null;
		try {
			fis = new FileInputStream(fichierPdfSigne);
			bos = new ByteArrayOutputStream();
			IOUtils.copy(fis, bos);
		} catch (IOException e) {
			throw e;
		} finally {
			fis.close();
		}

		ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
		return verifier(bis);
	}

	private static List<InfosComplementairesCertificat> verifier(ByteArrayInputStream docStream) throws IOException, SignatureException {
		List<InfosComplementairesCertificat> infosComplementairesCertificats = new ArrayList<InfosComplementairesCertificat>();

		PDDocument doc = PDDocument.load(docStream);
		List<PDSignature> fields = doc.getSignatureDictionaries();

		for (PDSignature pdSignature : fields) {
			COSDictionary field = pdSignature.getDictionary();
			COSString certString = (COSString) field.getDictionaryObject(COSName.CONTENTS);
			byte[] certData = certString.getBytes();

			PdfPKCS7 pkcs7 = new PdfPKCS7(certData, null);

			InfosComplementairesCertificat infosVerificationCertificat = CertificatUtil.extraireInformations(pkcs7.getSigningCertificate(),
					new InfosVerificationCertificat());

			// on reset le stream pour pouvoir extraire les bits qui servent à
			// la signature
			docStream.reset();

			byte[] signedContent = pdSignature.getSignedContent(docStream);

			pkcs7.update(signedContent, 0, signedContent.length);

			infosVerificationCertificat.setSignatureValide(pkcs7.verify());
			infosComplementairesCertificats.add(infosVerificationCertificat);
		}

		return infosComplementairesCertificats;
	}

	public static List<X509Certificate> extraireCertificats(File fichierPdfSigne) throws IOException {
		PDDocument doc = PDDocument.load(fichierPdfSigne);
		return extraireCertificats(doc);
	}

	public static List<X509Certificate> extraireCertificats(byte[] contenuFichierPdfSigne) throws IOException {
		PDDocument doc = PDDocument.load(new ByteArrayInputStream(contenuFichierPdfSigne));
		return extraireCertificats(doc);
	}

	private static List<X509Certificate> extraireCertificats(PDDocument doc) throws IOException {
		List<X509Certificate> certificats = new ArrayList<X509Certificate>();
		List<PDSignature> fields = doc.getSignatureDictionaries();

		for (PDSignature pdSignature : fields) {
			COSDictionary field = pdSignature.getDictionary();
			COSString certString = (COSString) field.getDictionaryObject(COSName.CONTENTS);
			byte[] certData = certString.getBytes();
			PdfPKCS7 pkcs7 = new PdfPKCS7(certData, null);
			certificats.add(pkcs7.getSigningCertificate());
		}
		return certificats;
	}

}
