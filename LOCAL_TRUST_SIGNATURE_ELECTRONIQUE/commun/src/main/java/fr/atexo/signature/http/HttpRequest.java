package fr.atexo.signature.http;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.security.cert.X509Certificate;


public class HttpRequest {

    private static final String BOUNDARY = "---------------------------7d22bb3b890472";
    private static final String CONTENT_DISPOSITION = "Content-Disposition: form-data; name=\"";
    private static final String FIN = "--";
    private static final String FIN_FICHIER = "\"";
    private static final String LIGNE = "\r\n";
    private static final String METHOD_POST = "POST";

    private HttpURLConnection connection;

    private OutputStream outputStream;

    public HttpRequest(final URL url, final boolean modePost) throws Exception {

        final boolean modeHttps = url.getProtocol().equals("https") ? true : false;

        // Création d'un trust manager qui ne valide pas la chaine de certification du serveur
        AccessController.doPrivileged(new PrivilegedExceptionAction<Object>() {

            public Object run() throws Exception {

                if (modeHttps) {

                    TrustManager[] trustAllCerts = new TrustManager[]{
                            new X509TrustManager() {
                                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                    return null;
                                }

                                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                                }

                                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                                }
                            }
                    };

                    SSLContext sslContext = SSLContext.getInstance("SSL");
                    sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                    HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    };

                    HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
                    HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

                }
                connection = (HttpURLConnection) url.openConnection();
                if (modePost) {
                    connection.setRequestMethod(METHOD_POST);
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setUseCaches(false);
                    connection.setAllowUserInteraction(true);
                    connection.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
                    connection.addRequestProperty("Connection", "Keep-Alive");
                    connection.addRequestProperty("Cache-Control", "no-cache");
                    outputStream = connection.getOutputStream();
                }                

                return null;
            }
        });

    }

    /**
     * Ajoute un fichier afin de pouvoir l'envoyer.
     *
     * @param nom     le nom du paramètre
     * @param fichier le fichier à ajouter
     * @throws IOException
     */
    public void ajouterFichier(String nom, File fichier) throws IOException {
        byte[] buffer = new byte[(int) fichier.length()];
        InputStream fis = new FileInputStream(fichier);
        fis.read(buffer, 0, buffer.length);
        ajouterFichier(nom, fichier.getName(), buffer);
        fis.close();
    }

    /**
     * Ajoute un fichier afin de pouvoir l'envoyer.
     *
     * @param nom            le nom du paramètre
     * @param nomFichier     le nom du fichier à ajouter
     * @param contenuFichier le contenu du fichier
     * @throws IOException
     */
    public void ajouterFichier(String nom, String nomFichier, byte[] contenuFichier) throws IOException {
        preparerMetadonneesFichier(nom, nomFichier);
        outputStream.write(contenuFichier);
    }

    /**
     * Prépare les méta-données pour l'envoi de fichier.
     *
     * @param nom        le nom du paramétre
     * @param nomFichier le nom du fichier
     * @throws IOException
     */
    private void preparerMetadonneesFichier(String nom, String nomFichier) throws IOException {
        outputStream.write(LIGNE.getBytes());
        outputStream.write(FIN.getBytes());
        outputStream.write(BOUNDARY.getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write((CONTENT_DISPOSITION + nom + FIN_FICHIER).getBytes());
        outputStream.write(("; filename=\"" + nomFichier + FIN_FICHIER).getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write(LIGNE.getBytes());
    }

    /**
     * Prépare les méta-données pour l'envoi d'informations.
     *
     * @param nom    le nom du paramétre
     * @param valeur la valeur de ce paramétre
     * @throws IOException
     */
    public void ajouterContenu(String nom, String valeur) throws IOException {
        outputStream.write(LIGNE.getBytes());
        outputStream.write(FIN.getBytes());
        outputStream.write(BOUNDARY.getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write((CONTENT_DISPOSITION + nom + FIN_FICHIER).getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write(LIGNE.getBytes());
        outputStream.write(valeur.getBytes());
    }

    public void ajouterContenu(String nom, Integer valeur) throws IOException {
        ajouterContenu(nom, valeur != null ? String.valueOf(valeur) : null);
    }

    /**
     * Permet d'envoyer la requête au serveur
     *
     * @return le retour de la reponse serveur
     * @throws IOException
     */
    public InputStream envoyer() throws IOException {

        outputStream.write(LIGNE.getBytes());
        outputStream.write(FIN.getBytes());
        outputStream.write(BOUNDARY.getBytes());
        outputStream.write(FIN.getBytes());
        outputStream.flush();
        outputStream.close();
        return getInputStream();
    }

    public void deconnecter() {
        connection.disconnect();
    }

    public InputStream getInputStream() throws IOException {
        return connection.getInputStream();
    }

    public HttpURLConnection getConnection() {
        return connection;
    }
}

