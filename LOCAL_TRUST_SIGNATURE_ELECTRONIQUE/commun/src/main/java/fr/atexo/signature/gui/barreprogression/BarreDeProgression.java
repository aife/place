package fr.atexo.signature.gui.barreprogression;

import fr.atexo.signature.commun.util.I18nUtil;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 *
 */
public class BarreDeProgression extends JFrame implements ChangeListener {

    private JProgressBar progressionTraitementFichier;

    private JProgressBar progressionGlobale;

    private JProgressBar progressionUnitaire;

    private String messageProgressionTraitementFichier;

    private String messageProgressionGlobale;

    public BarreDeProgression(int nombreFichiers) throws HeadlessException {
        this(I18nUtil.get("SWING_BARREPROGRESSION_TITRE"), nombreFichiers);
    }

    public BarreDeProgression(String titre, int nombreFichiers) throws HeadlessException {
        super(titre);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        // barre de progression
        progressionTraitementFichier = new JProgressBar(1, nombreFichiers + 1);
        progressionTraitementFichier.addChangeListener(this);
        progressionTraitementFichier.setStringPainted(true);

        progressionGlobale = new JProgressBar();
        progressionGlobale.setMinimum(1);
        progressionGlobale.addChangeListener(this);
        progressionGlobale.setStringPainted(true);

        progressionUnitaire = new JProgressBar(1, 100);
        progressionUnitaire.setStringPainted(true);

        add(progressionTraitementFichier);
        add(progressionGlobale);
        add(progressionUnitaire);

        setBounds(320, 240, 600, 100);
        setLocationRelativeTo(null);
        setVisible(true);
    }


    public void initialiserFichier(boolean chiffrementRequis, boolean telechargementRequis, boolean uploadRequis, boolean signaturePadesRequise, boolean signatureXadesRequise, int nombreBlocs) {
        int nombreTraitementsParFichier = 0;

        // signature pades
        if (signaturePadesRequise) {
            nombreTraitementsParFichier = nombreTraitementsParFichier + 1;
        }

        // signature xades
        if (signatureXadesRequise) {
            nombreTraitementsParFichier = nombreTraitementsParFichier + 1;
        }

        // nombre de blocs à chiffrer
        if (chiffrementRequis) {
            nombreTraitementsParFichier = nombreBlocs;
        }

        // nombre de blocs à chiffrer
        if (telechargementRequis) {
            nombreTraitementsParFichier = nombreBlocs;
        }

        // nombre de blocs à transferer
        if (uploadRequis) {
            nombreTraitementsParFichier = nombreTraitementsParFichier + nombreBlocs;
        }

        initialiserFichier(nombreTraitementsParFichier);
    }

    public void initialiserFichier(int nombreTraitements) {
        progressionGlobale.setMaximum(nombreTraitements + 1);
    }


    public void incrementerProgressionTraitementFichier() {
        progressionTraitementFichier.setValue(progressionTraitementFichier.getValue() + 1);
    }

    public void setMessageProgressionTraitementFichier(String message) {
        this.messageProgressionTraitementFichier = message;
    }

    public void incrementerProgressionGlobale() {
        progressionGlobale.setValue(progressionGlobale.getValue() + 1);
        try {
            validerProgressionUnitaire();
        } catch (InterruptedException e) {
        }
        reinitialiserProgressionUnitaire();
    }

    public void setMessageProgressionGlobale(String message) {
        this.messageProgressionGlobale = message;
    }

    private void validerProgressionUnitaire() throws InterruptedException {
        for (int i = 2; i < 101; i++) {
            progressionUnitaire.setValue(i);
            Thread.sleep(2);
        }
    }

    private void reinitialiserProgressionUnitaire() {
        progressionUnitaire.setValue(1);
    }

    public void reinitialiserProgressionGlobale() {
        progressionGlobale.setValue(1);
        reinitialiserProgressionUnitaire();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        progressionTraitementFichier.setString(messageProgressionTraitementFichier);
        progressionGlobale.setString(messageProgressionGlobale);
    }
}
