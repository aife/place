package fr.atexo.signature.util;

import fr.atexo.signature.commun.securite.provider.pkcs11.smartcard.SmartCardInformation;

import java.io.File;

/**
 * 
 */
public class SmartCardInformationBean implements SmartCardInformation {
    
    private String atr;

    private File librairie;

    private String nomFabriquant;

    public String getAtr() {
        return atr;
    }

    public void setAtr(String atr) {
        this.atr = atr;
    }

    public File getLibrairie() {
        return librairie;
    }

    public void setLibrairie(File librairie) {
        this.librairie = librairie;
    }

    public String getNomFabriquant() {
        return nomFabriquant;
    }

    public void setNomFabriquant(String nomFabriquant) {
        this.nomFabriquant = nomFabriquant;
    }
}
