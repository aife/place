package fr.atexo.signature.commun.util;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.xml.pkcs11.ObjectFactory;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;

import javax.xml.bind.JAXBElement;
import java.net.URL;

/**
 *
 */
public class JaxbPkcs11Util {

    private final static ObjectFactory factory = new ObjectFactory();

    public static Pkcs11LibsType getPkcs11Libs(String pkcs11LibsXML) {
        return getPkcs11Libs(pkcs11LibsXML, FileUtil.ENCODING_UTF_8);
    }

    public static Pkcs11LibsType getPkcs11Libs(String pkcs11LibsXML, String encoding) {
        String context = Pkcs11LibsType.class.getPackage().getName();
        Object resultatObject = JAXBService.instance().getAsObject(pkcs11LibsXML, context, encoding);
        return ((JAXBElement<Pkcs11LibsType>) resultatObject).getValue();
    }

    public static Pkcs11LibsType getPkcs11LibsType(URL urlTelechargementXML) throws TransfertExecutionException {
        byte[] contenuFichierXML = TransfertUtil.telecharger(urlTelechargementXML.toString());
        return getPkcs11LibsType(contenuFichierXML);
    }

    public static Pkcs11LibsType getPkcs11LibsType(byte[] contenuFichierXML) throws TransfertExecutionException {
        return JaxbPkcs11Util.getPkcs11Libs(new String(contenuFichierXML));
    }
}
