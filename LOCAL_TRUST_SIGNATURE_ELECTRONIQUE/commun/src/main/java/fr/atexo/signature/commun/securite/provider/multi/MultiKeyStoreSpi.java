package fr.atexo.signature.commun.securite.provider.multi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.*;

/**
 *
 */
public class MultiKeyStoreSpi extends KeyStoreSpi {

    private final Map<String, KeyStore> keyStores;
    private final Set<String> aliases = new TreeSet<String>();

    public MultiKeyStoreSpi(final Map<String, KeyStore> keyStores) {
        this.keyStores = keyStores;
    }

    @Override
    public final Enumeration<String> engineAliases() {
        return Collections.enumeration(aliases);
    }

    @Override
    public final boolean engineContainsAlias(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).containsAlias(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public final void engineDeleteEntry(final String alias) throws KeyStoreException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        keyStores.get(providerNameAndEntryAlias[0]).deleteEntry(providerNameAndEntryAlias[1]);
        aliases.remove(alias);
    }

    @Override
    public final boolean engineEntryInstanceOf(final String alias, final Class<? extends java.security.KeyStore.Entry> entryClass) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).entryInstanceOf(providerNameAndEntryAlias[1], entryClass);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public final Certificate engineGetCertificate(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).getCertificate(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final String engineGetCertificateAlias(final Certificate cert) {
        for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
            try {
                final String entryAlias = entry.getValue().getCertificateAlias(cert);
                if (entryAlias == null) {
                    continue;
                } else {
                    return entry.getKey() + " - " + entryAlias;
                }
            } catch (final KeyStoreException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public final Certificate[] engineGetCertificateChain(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).getCertificateChain(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final Date engineGetCreationDate(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).getCreationDate(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final java.security.KeyStore.Entry engineGetEntry(final String alias, final KeyStore.ProtectionParameter protParam) throws KeyStoreException,
            NoSuchAlgorithmException, UnrecoverableEntryException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        return keyStores.get(providerNameAndEntryAlias[0]).getEntry(providerNameAndEntryAlias[1], protParam);
    }

    @Override
    public final Key engineGetKey(final String alias, final char[] password) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).getKey(providerNameAndEntryAlias[1], password);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final boolean engineIsCertificateEntry(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).isCertificateEntry(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public final boolean engineIsKeyEntry(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        try {
            return keyStores.get(providerNameAndEntryAlias[0]).isKeyEntry(providerNameAndEntryAlias[1]);
        } catch (final KeyStoreException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public final void engineLoad(final InputStream stream, final char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
        for (final KeyStore keyStore : keyStores.values()) {
            keyStore.load(stream, password);
        }

        for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
            try {
                final Enumeration<String> keyStoreAliases = entry.getValue().aliases();
                while (keyStoreAliases.hasMoreElements()) {
                    aliases.add(entry.getKey() + " - " + keyStoreAliases.nextElement());
                }
            } catch (final KeyStoreException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public final void engineLoad(final KeyStore.LoadStoreParameter param) throws IOException, NoSuchAlgorithmException, CertificateException {
        for (final KeyStore keyStore : keyStores.values()) {
            keyStore.load(param);
        }

        for (final java.util.Map.Entry<String, KeyStore> entry : keyStores.entrySet()) {
            try {
                final Enumeration<String> keyStoreAliases = entry.getValue().aliases();
                while (keyStoreAliases.hasMoreElements()) {
                    aliases.add(entry.getKey() + " - " + keyStoreAliases.nextElement());
                }
            } catch (final KeyStoreException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public final void engineSetCertificateEntry(final String alias, final Certificate cert) throws KeyStoreException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        keyStores.get(providerNameAndEntryAlias[0]).setCertificateEntry(providerNameAndEntryAlias[1], cert);
        aliases.add(alias);
    }

    @Override
    public final void engineSetEntry(final String alias, final java.security.KeyStore.Entry entry, final KeyStore.ProtectionParameter protParam)
            throws KeyStoreException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        keyStores.get(providerNameAndEntryAlias[0]).setEntry(providerNameAndEntryAlias[1], entry, protParam);
        aliases.add(alias);
    }

    @Override
    public final void engineSetKeyEntry(final String alias, final byte[] key, final Certificate[] chain) throws KeyStoreException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        keyStores.get(providerNameAndEntryAlias[0]).setKeyEntry(providerNameAndEntryAlias[1], key, chain);
        aliases.add(alias);
    }

    @Override
    public final void engineSetKeyEntry(final String alias, final Key key, final char[] password, final Certificate[] chain) throws KeyStoreException {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        keyStores.get(providerNameAndEntryAlias[0]).setKeyEntry(providerNameAndEntryAlias[1], key, password, chain);
        aliases.add(alias);
    }

    @Override
    public final int engineSize() {
        return aliases.size();
    }

    @Override
    public final void engineStore(final KeyStore.LoadStoreParameter param) throws IOException, NoSuchAlgorithmException, CertificateException {
        for (final KeyStore keyStore : keyStores.values()) {
            try {
                keyStore.store(param);
            } catch (final KeyStoreException e) {
                throw new IOException(e);
            }
        }
    }

    @Override
    public final void engineStore(final OutputStream stream, final char[] password) throws IOException, NoSuchAlgorithmException, CertificateException {
        for (final KeyStore keyStore : keyStores.values()) {
            try {
                keyStore.store(stream, password);
            } catch (final KeyStoreException e) {
                throw new IOException(e);
            }
        }
    }

    public final String getProviderName(final String alias) {
        final String[] providerNameAndEntryAlias = alias.split(" - ", 2);
        KeyStore keyStore = keyStores.get(providerNameAndEntryAlias[0]);
        return keyStore != null ? keyStore.getProvider().getName() : null;
    }
}
