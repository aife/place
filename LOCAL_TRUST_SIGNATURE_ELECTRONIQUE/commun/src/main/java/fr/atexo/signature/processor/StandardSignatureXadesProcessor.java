package fr.atexo.signature.processor;

import fr.atexo.json.reponse.Repertoire;
import fr.atexo.json.reponse.verification.CodeReponse;
import fr.atexo.json.reponse.verification.ReponseSignature;
import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.exception.execution.TransfertExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.*;
import fr.atexo.signature.commun.securite.signature.SignatureXades;
import fr.atexo.signature.commun.securite.signature.TypeAlgorithmHash;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.commun.util.io.FileUtil;
import fr.atexo.signature.commun.util.io.HashUtil;
import fr.atexo.signature.commun.util.io.XMLUtil;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.util.ModuleValidationUtil;
import fr.atexo.signature.util.TransfertUtil;
import fr.atexo.signature.validation.TypeEchange;
import net.iharder.Base64;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Classe effectuant la signature Xades d'un fichier.
 */
public class StandardSignatureXadesProcessor implements SignatureXadesProcessor {

    private String urlModuleValidation;

    private TypeAlgorithmHash typeAlgorithmHash;

    /**
     * L'origine de la plateforme qui sera envoyé au module de validation.
     */
    private String originePlateforme;

    /**
     * L'origine de l'organisme qui sera envoyé au module de validation.
     */
    private String origineOrganisme;

    /**
     * L'origine de l'item qui sera envoyé au module de validation.
     */
    private String origineItem;

    /**
     * L'origine du contexte métier qui sera envoyé au module de validation.
     */
    private String origineContexteMetier;

    private String cheminFichierSignatureXML;

    private String contenuFichierSignatureXML;

    private File fichierSignatureXML;

    public StandardSignatureXadesProcessor(String urlModuleValidation, TypeAlgorithmHash typeAlgorithmHash) {
        super();
        this.urlModuleValidation = urlModuleValidation;
        this.typeAlgorithmHash = typeAlgorithmHash;
    }

    public void ajouterInformationsPourModuleValidation(String originePlateforme, String origineOrganisme, String origineItem, String origineContexteMetier) {
        this.originePlateforme = originePlateforme;
        this.origineOrganisme = origineOrganisme;
        this.origineItem = origineItem;
        this.origineContexteMetier = origineContexteMetier;
    }

    @Override
    public InfosComplementairesCertificat signerEnXades(KeyPair keyPair, ConteneurHashFichierSignatureXML conteneur) throws SignatureExecutionException {
        return signerEnXades(keyPair, null, conteneur);
    }

    @Override
    public InfosComplementairesCertificat signerEnXades(KeyPair keyPair, ConteneurFichierSignatureXML conteneur) throws SignatureExecutionException {
        return signerEnXades(keyPair, conteneur, null);
    }

    private InfosComplementairesCertificat signerEnXades(KeyPair keyPair, ConteneurFichierSignatureXML conteneurFichierSignatureXML, ConteneurHashFichierSignatureXML conteneurHashFichierSignatureXML) throws SignatureExecutionException {

        boolean estFichier = conteneurFichierSignatureXML != null;

        String signatureXadesServeurEnBase64 = null;
        byte[] hashBinaireFichier = null;
        URI uriFichier = null;

        try {
            uriFichier = estFichier ? conteneurFichierSignatureXML.getFichier().toURI() : null;
            hashBinaireFichier = estFichier ? HashUtil.genererHashShaBinaire(typeAlgorithmHash, conteneurFichierSignatureXML.getFichier()) : HashUtil.convertirHexadecimalEnBinaire(conteneurHashFichierSignatureXML.getHashFichier());
        } catch (IOException e) {
            throw new SignatureExecutionException("Un problème est survenu lors de la génération du hash du fichier : " + conteneurFichierSignatureXML.getFichier().getPath(), e);
        }

        Date dateSignature = null;
        if (!Util.estVide(urlModuleValidation)) {
            try {
                String reponseJson = TransfertUtil.recupererDateModuleValidation(urlModuleValidation);
                if (!Util.estVide(reponseJson)) {
                    Date dateISO8601 = getDateServeur(reponseJson);
                    if (dateISO8601 == null) {
                        LogManager.getInstance().afficherMessageWarning("Impossible de récupérer la date de signature depuis le serveur => la date de signature est une date de signature générée côté client", this.getClass());
                        dateISO8601 = new Date();
                    }
                    dateSignature = dateISO8601;
                }
            } catch (TransfertExecutionException e) {
                LogManager.getInstance().afficherMessageWarning(e.getMessage(), e, this.getClass());
            }
        } else {
            LogManager.getInstance().afficherMessageInfo("Aucune url de module de validation paramétré => la date de signature est une date de signature générée côté client", this.getClass());
            dateSignature = new Date();
        }
        LogManager.getInstance().afficherMessageInfo("Date de signature : " + dateSignature, this.getClass());
        String signatureXadesFinale = null;
        try {
            signatureXadesFinale = SignatureXades.signer(keyPair, hashBinaireFichier, uriFichier, dateSignature, typeAlgorithmHash);
        } catch (Exception e) {
            String message = "Un problème est survenu lors de la tentative de création de la signature XAdES" + (estFichier ? " du fichier " + conteneurFichierSignatureXML.getFichier() : " du hash de fichier " + conteneurHashFichierSignatureXML.getHashFichier());
            throw new SignatureExecutionException(message, e);
        }

        Boolean dateSignatureValide = null;
        Boolean periodiciteValide = null;
        Boolean absenceRevocationCRL = null;
        Boolean chaineDeCertificationValide = null;
        Set<Repertoire> repertoiresChaineCertification = null;
        Set<Repertoire> repertoiresChaineRevocation = null;
        String reponseModuleValidationJson = null;

        if (!Util.estVide(urlModuleValidation)) {

            try {
                reponseModuleValidationJson = TransfertUtil.envoyerAuModuleValidation(urlModuleValidation, Base64.encodeBytes(signatureXadesFinale.getBytes(FileUtil.ENCODING_UTF_8)), TypeEchange.EnrichissementSignature, typeAlgorithmHash, originePlateforme, origineOrganisme, origineItem, origineContexteMetier);
            } catch (Exception e) {
                throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature", e);
            }

            if (!Util.estVide(reponseModuleValidationJson)) {
                ReponseSignature reponseSignature = getReponseSignature(reponseModuleValidationJson, true);
                if (reponseSignature == null || reponseSignature.getCodeRetour() != CodeReponse.Ok.getCode()) {
                    throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature" + reponseSignature.getMessage());
                }

                dateSignatureValide = reponseSignature.getDateSignatureValide();
                periodiciteValide = reponseSignature.getPeriodiciteValide();
                chaineDeCertificationValide = reponseSignature.getChaineDeCertificationValide();
                absenceRevocationCRL = reponseSignature.getAbsenceRevocationCRL();
                repertoiresChaineCertification = reponseSignature.getRepertoiresChaineCertification();
                repertoiresChaineRevocation = reponseSignature.getRepertoiresRevocation();

                signatureXadesServeurEnBase64 = reponseSignature.getSignatureXadesServeurEnBase64();

            } else {
                throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature");
            }

            try {
                // vérification que la signature est valide                                       
                signatureXadesFinale = new String(Base64.decode(signatureXadesServeurEnBase64.getBytes(FileUtil.ENCODING_UTF_8)), FileUtil.ENCODING_UTF_8);
            } catch (IOException e) {
                throw new SignatureExecutionException("Un problème est survenu lors du décode en base 64 du contenu de la signature enrichi par le module de validation ", e);
            }

        }

        // vérification de l'enregistrement effectué côté serveur module de validation
        InfosComplementairesCertificat resultatVerificationSignature = estFichier ? verifierConformite(conteneurFichierSignatureXML.getFichier(), signatureXadesFinale, false) : verifierConformite(conteneurHashFichierSignatureXML.getHashFichier().getBytes(), signatureXadesFinale, false);
        resultatVerificationSignature.setDateSignatureValide(dateSignatureValide);
        resultatVerificationSignature.setPeriodiciteValide(periodiciteValide);
        resultatVerificationSignature.setAbsenceRevocationCRL(absenceRevocationCRL);
        resultatVerificationSignature.setChaineDeCertificationValide(chaineDeCertificationValide);
        resultatVerificationSignature.setRepertoiresChaineCertification(repertoiresChaineCertification);
        resultatVerificationSignature.setRepertoiresRevocation(repertoiresChaineRevocation);

        if (resultatVerificationSignature != null) {
            // dans le cas d'un fichier on ecris le contenu de la signature sur disque
            if (estFichier) {
                int indexFichier = FileUtil.getProchaineIndexFichierSignatureXML(conteneurFichierSignatureXML.getFichier().getPath());
                String formatDateNomFichier = Util.formaterDate(dateSignature, Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);
                cheminFichierSignatureXML = XMLUtil.contruireCheminFichierSignatureXML(conteneurFichierSignatureXML.getFichier().getPath(), formatDateNomFichier, indexFichier);
                fichierSignatureXML = new File(cheminFichierSignatureXML);
                contenuFichierSignatureXML = signatureXadesFinale;
                try {
                    FileUtils.writeStringToFile(fichierSignatureXML, signatureXadesFinale, FileUtil.ENCODING_UTF_8);
                } catch (IOException e) {
                    throw new SignatureExecutionException("Un problème est survenu lors de l'enregistrement sur le disque du fichier de signature XML : " + cheminFichierSignatureXML, e);
                }

                boolean fichierExistant = fichierSignatureXML.exists();
                if (fichierExistant) {
                    conteneurFichierSignatureXML.setFichierSignatureXml(fichierSignatureXML);
                }
            }
            // dans le cas contraire on conserve en mémoire la signature
            else {
                conteneurHashFichierSignatureXML.setContenuSignatureXml(Base64.encodeBytes(signatureXadesFinale.getBytes()));
            }

            return resultatVerificationSignature;

        } else {
            return null;
        }
    }

    @Override
    public InfosComplementairesCertificat verifierConformite(byte[] hashFichier, String contenuFichierSignatureXML, boolean utiliserModuleValidation) throws SignatureExecutionException {
        try {
            List<String> hashFichiers = new ArrayList<String>();
            hashFichiers.add(new String(hashFichier));
            InfosVerificationCertificat resultatVerificationSignature = SignatureXades.verifier(contenuFichierSignatureXML, hashFichiers);

            if (utiliserModuleValidation && !Util.estVide(urlModuleValidation)) {

                String reponseModuleValidationJson = null;

                try {
                    reponseModuleValidationJson = TransfertUtil.envoyerAuModuleValidation(urlModuleValidation, Base64.encodeBytes(contenuFichierSignatureXML.getBytes(FileUtil.ENCODING_UTF_8)), TypeEchange.VerificationSignatureClient, typeAlgorithmHash, originePlateforme, origineOrganisme, origineItem, origineContexteMetier);
                } catch (Exception e) {
                    throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature", e);
                }

                if (!Util.estVide(reponseModuleValidationJson)) {
                    ReponseSignature reponseSignature = getReponseSignature(reponseModuleValidationJson, false);
                    if (reponseSignature.getCodeRetour() != CodeReponse.Ok.getCode()) {
                        throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature : " + reponseSignature.getMessage());
                    }
                    resultatVerificationSignature.setDateSignatureValide(reponseSignature.getDateSignatureValide());
                    resultatVerificationSignature.setPeriodiciteValide(reponseSignature.getPeriodiciteValide());
                    resultatVerificationSignature.setChaineDeCertificationValide(reponseSignature.getChaineDeCertificationValide());
                    resultatVerificationSignature.setAbsenceRevocationCRL(reponseSignature.getAbsenceRevocationCRL());
                    resultatVerificationSignature.setRepertoiresChaineCertification(reponseSignature.getRepertoiresChaineCertification());
                    resultatVerificationSignature.setRepertoiresRevocation(reponseSignature.getRepertoiresRevocation());
                } else {
                    throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature");
                }
            }

            return resultatVerificationSignature;
        } catch (Exception e) {
            throw new SignatureExecutionException("Un problème est survenu lors de la vérification du hash du fichier [" + new String(hashFichier) + "] avec son fichier de signature XML associé", e);
        }
    }

    @Override
    public InfosComplementairesCertificat verifierConformite(File fichier, String contenuFichierSignatureXML, boolean utiliserModuleValidation) throws SignatureExecutionException {
        try {
            InfosVerificationCertificat resultatVerificationSignature = SignatureXades.verifier(contenuFichierSignatureXML, fichier);

            if (utiliserModuleValidation && !Util.estVide(urlModuleValidation)) {

                String reponseModuleValidationJson = null;

                try {
                    reponseModuleValidationJson = TransfertUtil.envoyerAuModuleValidation(urlModuleValidation, Base64.encodeBytes(contenuFichierSignatureXML.getBytes(FileUtil.ENCODING_UTF_8)), TypeEchange.VerificationSignatureClient, typeAlgorithmHash, originePlateforme, origineOrganisme, origineItem, origineContexteMetier);
                } catch (Exception e) {
                    throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature", e);
                }

                if (!Util.estVide(reponseModuleValidationJson)) {
                    ReponseSignature reponseSignature = getReponseSignature(reponseModuleValidationJson, false);
                    if (reponseSignature.getCodeRetour() != CodeReponse.Ok.getCode()) {
                        throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature : " + reponseSignature.getMessage());
                    }
                    resultatVerificationSignature.setDateSignatureValide(reponseSignature.getDateSignatureValide());
                    resultatVerificationSignature.setPeriodiciteValide(reponseSignature.getPeriodiciteValide());
                    resultatVerificationSignature.setChaineDeCertificationValide(reponseSignature.getChaineDeCertificationValide());
                    resultatVerificationSignature.setAbsenceRevocationCRL(reponseSignature.getAbsenceRevocationCRL());
                    resultatVerificationSignature.setRepertoiresChaineCertification(reponseSignature.getRepertoiresChaineCertification());
                    resultatVerificationSignature.setRepertoiresRevocation(reponseSignature.getRepertoiresRevocation());
                } else {
                    throw new SignatureExecutionException("Un problème est survenu lors de la tentative d'accès au module de validation de signature");
                }
            }

            return resultatVerificationSignature;

        } catch (Exception e) {
            throw new SignatureExecutionException("Un problème est survenu lors de la vérification du fichier [" + fichier.getPath() + "] avec son fichier de signature XML associé", e);
        }
    }

    public ReponseSignature getReponseSignature(String reponseJson, boolean signature) {

        ReponseSignature reponseSignature = null;

        try {
            JSONObject reponseModuleValidationJsonObject = new JSONObject(reponseJson);
            String message = reponseModuleValidationJsonObject.getString("message");
            Integer codeRetour = reponseModuleValidationJsonObject.getInt("codeRetour");
            reponseSignature = new ReponseSignature(codeRetour, message);

            if (codeRetour == CodeReponse.Ok.getCode()) {

                String dateSignatureValide = reponseModuleValidationJsonObject.getString("dateSignatureValide");
                if (dateSignatureValide != null) {
                    reponseSignature.setDateSignatureValide(Boolean.valueOf(dateSignatureValide));
                }

                JSONObject reponseRetourVerificationJsonObject = reponseModuleValidationJsonObject.getJSONObject("resultat");

                Integer codeRetourPeriodeValidite = reponseRetourVerificationJsonObject.getInt("periodeValidite");
                Boolean periodiciteValide = ModuleValidationUtil.getEtatValidite(codeRetourPeriodeValidite);
                reponseSignature.setPeriodiciteValide(periodiciteValide);

                Integer codeRetourChaineCertification = reponseRetourVerificationJsonObject.getInt("chaineCertification");
                Boolean chaineDeCertificationValide = ModuleValidationUtil.getEtatChaineCertification(codeRetourChaineCertification);
                reponseSignature.setChaineDeCertificationValide(chaineDeCertificationValide);

                Integer codeRetourRevocation = reponseRetourVerificationJsonObject.getInt("revocation");
                Boolean absenceRevocationCRL = ModuleValidationUtil.getEtatRevocation(codeRetourRevocation);
                reponseSignature.setAbsenceRevocationCRL(absenceRevocationCRL);

                if (signature && reponseModuleValidationJsonObject.has("contenuFichierXMLEnBase64")) {
                    String signatureXadesServeurEnBase64 = reponseModuleValidationJsonObject.getString("contenuFichierXMLEnBase64");
                    reponseSignature.setSignatureXadesServeurEnBase64(signatureXadesServeurEnBase64);
                }

                // liste des repertoires ds lesquels les crt on été trouvés pour effectuer la validation de la chaine de certificat
                if (reponseRetourVerificationJsonObject.has("repertoiresChaineCertification") && !reponseRetourVerificationJsonObject.isNull("repertoiresChaineCertification")) {
                    JSONArray repertoires = reponseRetourVerificationJsonObject.getJSONArray("repertoiresChaineCertification");
                    for (int i = 0; i < repertoires.length(); i++) {
                        JSONObject repertoireChaineCertification = repertoires.getJSONObject(i);
                        if (repertoireChaineCertification != null && repertoireChaineCertification.has("nom"))
                            reponseSignature.ajouterRepertoireChaineCertification(repertoireChaineCertification.getString("nom"));

                    }
                }

                // liste des repertoires ds lesquels les crl on été trouvés pour effectuer la validation de la revication du certificat
                if (reponseRetourVerificationJsonObject.has("repertoiresRevocation") && !reponseRetourVerificationJsonObject.isNull("repertoiresRevocation")) {
                    JSONArray repertoires = reponseRetourVerificationJsonObject.getJSONArray("repertoiresRevocation");
                    for (int i = 0; i < repertoires.length(); i++) {
                        JSONObject repertoireRevocation = repertoires.getJSONObject(i);
                        if (repertoireRevocation != null && repertoireRevocation.has("nom")) {
                            reponseSignature.ajouterRepertoireRevocation(repertoireRevocation.getString("nom"));
                        }
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reponseSignature;
    }

    public Date getDateServeur(String reponseJson) {

        Date dateServeur = null;

        try {
            JSONObject reponseModuleValidationJsonObject = new JSONObject(reponseJson);
            Integer codeRetour = reponseModuleValidationJsonObject.getInt("codeRetour");

            if (codeRetour == CodeReponse.Ok.getCode()) {
                String dateISO8601 = reponseModuleValidationJsonObject.getString("dateISO8601");
                if (dateISO8601 != null) {
                    dateServeur = Util.convertirISO8601DateTime(dateISO8601);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dateServeur;
    }

    public File getFichierSignatureXML() {
        return fichierSignatureXML;
    }

    public String getCheminFichierSignatureXML() {
        return cheminFichierSignatureXML;
    }

    public String getContenuFichierSignatureXML() {
        return contenuFichierSignatureXML;
    }
}
