package fr.atexo.signature.commun.securite.certificat;

import fr.atexo.signature.commun.securite.provider.TypeProvider;

import java.security.cert.X509Certificate;
import java.security.PrivateKey;

/**
 * Clef stockant le certificat / la clef privé / le type de provider.
 */
public class KeyPair {

    private X509Certificate certificate = null;
    private PrivateKey privateKey = null;
    private TypeProvider provider;
    private String nomProvider;

    public KeyPair(X509Certificate certificate, PrivateKey privateKey, TypeProvider provider) {
        this.certificate = certificate;
        this.privateKey = privateKey;
        this.provider = provider;
        this.nomProvider = provider.getNom();
    }

    public KeyPair(X509Certificate certificate, PrivateKey privateKey, TypeProvider provider, String nomProvider) {
        this.certificate = certificate;
        this.privateKey = privateKey;
        this.provider = provider;
        this.nomProvider = nomProvider;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public TypeProvider getProvider() {
        return provider;
    }

    public String getNomProvider() {
        return nomProvider;
    }

    public void setNomProvider(String nomProvider) {
        this.nomProvider = nomProvider;
    }
}
