package fr.atexo.signature.gui.barreprogression;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

public class BarreDeProgressionDouble extends Frame implements ChangeListener {

    private JProgressBar barreProgressionHaute;
    private String messageBarreProgressionHaute = "";

    private JProgressBar barreProgressionBasse;
    private String messageBarreProgressionBasse = "";

    public BarreDeProgressionDouble() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        barreProgressionHaute = new JProgressBar(0, 99);
        barreProgressionHaute.setStringPainted(true);
        barreProgressionHaute.addChangeListener(this);
        add(barreProgressionHaute);

        barreProgressionBasse = new JProgressBar(0, 99);
        barreProgressionBasse.setStringPainted(true);
        barreProgressionBasse.addChangeListener(this);
        add(barreProgressionBasse);
        setBounds(320, 240, 500, 70);
        setVisible(true);

    }

    @Override
    public void stateChanged(ChangeEvent e) {
        barreProgressionHaute.setString(messageBarreProgressionHaute);
        barreProgressionBasse.setString(messageBarreProgressionBasse + " " + (int) (barreProgressionBasse.getPercentComplete() * 100) + "%");
    }

    public JProgressBar getBarreProgressionHaute() {
        return barreProgressionHaute;
    }

    public void setMessageBarreProgressionHaute(String messageBarreProgressionHaute) {
        this.messageBarreProgressionHaute = messageBarreProgressionHaute;
    }

    public JProgressBar getBarreProgressionBasse() {
        return barreProgressionBasse;
    }

    public void setMessageBarreProgressionBasse(String messageBarreProgressionBasse) {
        this.messageBarreProgressionBasse = messageBarreProgressionBasse;
    }
}
