package fr.atexo.signature.commun.exception.execution;

/**
 * Exception pour signaler un souci lors de l'execution d'une action de signature.
 */
public class SignatureExecutionException extends ExecutionException {

    public SignatureExecutionException(String message) {
        super(message);
    }

    public SignatureExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
