package fr.atexo.signature.commun.securite.provider;

import java.util.Date;


public class CertificatItem {

    private String id;
    private String nom;
    private String nomEmetteur;
    private String strDateExpiration;
    private Date dateExpiration;
    private String utilisationCle;
    private boolean smartCard;
    private String hashCode;
    private TypeProvider typeProvider;

    public CertificatItem(String id, String nom, String nomEmetteur, String strDateExpiration, Date dateExpiration, String utilisationCle,boolean smartCard, String hashCode,TypeProvider typeProvider) {
        this.id = id;
        this.nom = nom;
        this.nomEmetteur = nomEmetteur;
        this.strDateExpiration = strDateExpiration;
        this.dateExpiration = dateExpiration;
        this.utilisationCle = utilisationCle;
        this.smartCard = smartCard;
        this.hashCode = hashCode;
        this.typeProvider = typeProvider;
    }

    public String getUtilisationCle() {
        return utilisationCle;
    }

    public void setUtilisationCle(String utilisationCle) {
        this.utilisationCle = utilisationCle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomEmetteur() {
        return nomEmetteur;
    }

    public void setNomEmetteur(String nomEmetteur) {
        this.nomEmetteur = nomEmetteur;
    }

    public String getStrDateExpiration() {
        return strDateExpiration;
    }

    public void setStrDateExpiration(String dateExpiration) {
        this.strDateExpiration = dateExpiration;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public boolean isSmartCard() {
        return smartCard;
    }

    public TypeProvider getTypeProvider() {
        return typeProvider;
    }

    @Override
    public String toString() {
        return "CertificateItem{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", smartCard='" + smartCard + '\'' +
                ", nomEmetteur='" + nomEmetteur + '\'' +
                ", strDateExpiration='" + strDateExpiration + '\'' +
                ", utilisationCle='" + utilisationCle + '\'' +
                ", typeProvider='" + typeProvider + '\'' +
                ", hashCode='" + hashCode + '\'' +
                '}';
    }


}
