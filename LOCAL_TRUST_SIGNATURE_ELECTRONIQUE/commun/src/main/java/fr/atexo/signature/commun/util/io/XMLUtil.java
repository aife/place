package fr.atexo.signature.commun.util.io;

import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

/**
 *
 */
public abstract class XMLUtil {

    /**
     * Permet de transformer un Document XML en chaine de caractère
     *
     * @param document
     * @return
     * @throws javax.xml.transform.TransformerException
     *
     */
    public static String convertir(Document document, String encodingTransformation) throws TransformerException, UnsupportedEncodingException {

        StringWriter stringWriter = new StringWriter();
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        trans.setOutputProperty(OutputKeys.INDENT, "no");
        trans.setOutputProperty(OutputKeys.ENCODING, encodingTransformation);

        // create string from XML tree
        StreamResult result = new StreamResult(stringWriter);
        DOMSource source = new DOMSource(document);
        trans.transform(source, result);
        byte[] resultat = stringWriter.toString().getBytes(FileUtil.ENCODING_UTF_8);
        String contenuXML = new String(resultat, encodingTransformation);

        return contenuXML;
    }

    public static String contruireCheminFichierSignatureXML(String prefixeNomFichier, String date, Integer index) {
        String nomFichierSignature = prefixeNomFichier + " - " + date + " - Signature " + index + ".xml";
        return nomFichierSignature;
    }

    public static String contruireCheminFichierSignaturePkcs7(String prefixeNomFichier, Integer index) {
        String nomFichierSignature = prefixeNomFichier + " - Signature " + index + ".p7s";
        return nomFichierSignature;
    }


}
