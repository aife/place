package fr.atexo.signature.commun.securite.chiffrement;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class AbstractChiffrement {

    private List<X509Certificate> certificats;

    private CBCBlockCipher cipherBlockChainingBlockCipher;

    private ParametersWithIV parametersWithIV;

    public AbstractChiffrement(List<X509Certificate> certificats, BlockCipher blockCipher, int tailleClef) {
        this.certificats = certificats;
        creerClefAlgorithm(blockCipher, tailleClef);
    }

    protected void creerClefAlgorithm(BlockCipher blockCipher, int tailleClef) {
        SecureRandom secureRandom = new SecureRandom();
        cipherBlockChainingBlockCipher = new CBCBlockCipher(blockCipher);

        byte[] bytes = new byte[tailleClef];
        byte[] iv = new byte[cipherBlockChainingBlockCipher.getBlockSize()];
        secureRandom.nextBytes(bytes);
        secureRandom.nextBytes(iv);
        parametersWithIV = new ParametersWithIV(new KeyParameter(bytes), iv);
    }

    public byte[] chiffrer(byte[] donnees) {

        int tailleBloc = cipherBlockChainingBlockCipher.getBlockSize();
        cipherBlockChainingBlockCipher.init(true, parametersWithIV);

        int nombreBlocs = (donnees.length / tailleBloc) + 1;
        byte[] plaintext = new byte[nombreBlocs * tailleBloc];
        System.arraycopy(donnees, 0, plaintext, 0, donnees.length);

        byte[] ciphertext = new byte[nombreBlocs * tailleBloc];
        for (int i = 0; i < ciphertext.length; i += tailleBloc) {
            cipherBlockChainingBlockCipher.processBlock(plaintext, i, ciphertext, i);
        }

        return ciphertext;
    }

    public byte[] dechiffrer(byte[] donnees, int taille, InfosDechiffrement infosDechiffrement) {

        PrivateKey privateKey = infosDechiffrement.getKeyPair().getPrivateKey();
        ParametersWithIV parametersWithIV = getClefDechiffrement(infosDechiffrement.getClefIV().getClef(), infosDechiffrement.getClefIV().getIv(), privateKey);

        cipherBlockChainingBlockCipher.init(false, parametersWithIV);

        byte[] plaintext = new byte[donnees.length];

        for (int i = 0; i < donnees.length; i += cipherBlockChainingBlockCipher.getBlockSize()) {
            cipherBlockChainingBlockCipher.processBlock(donnees, i, plaintext, i);
        }

        byte[] result = new byte[taille];
        System.arraycopy(plaintext, 0, result, 0, result.length);

        return result;
    }

    public List<ClefIV> getClefIVs() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        List<ClefIV> listKey = new ArrayList<ClefIV>();
        byte[] clef = ((KeyParameter) parametersWithIV.getParameters()).getKey();
        byte[] iv = parametersWithIV.getIV();

        Cipher cipher = Cipher.getInstance("RSA");

        for (int i = 0; i < certificats.size(); i++) {
            X509Certificate certificate = certificats.get(i);
            PublicKey publicKey = certificate.getPublicKey();
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            ClefIV keyIv = new ClefIV(cipher.doFinal(clef), cipher.doFinal(iv), certificate);
            listKey.add(keyIv);
        }
        return listKey;
    }

    private ParametersWithIV getClefDechiffrement(byte[] cryptKey, byte[] cryptIv, PrivateKey privateKey) {

        byte[] key;
        byte[] iv;

        try {
            Cipher decipher = Cipher.getInstance("RSA");
            decipher.init(Cipher.DECRYPT_MODE, privateKey);

            key = decipher.doFinal(cryptKey);
            iv = decipher.doFinal(cryptIv);

            return new ParametersWithIV(new KeyParameter(key), iv);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }


    public List<X509Certificate> getCertificats() {
        return certificats;
    }

    public void setCertificats(List<X509Certificate> certificats) {
        this.certificats = certificats;
    }
}
