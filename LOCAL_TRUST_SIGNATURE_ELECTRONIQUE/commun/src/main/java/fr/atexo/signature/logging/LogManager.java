package fr.atexo.signature.logging;

import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe d'abstraction pour lesquels le nom de l'instance du log doit être
 * propagé.
 */
public class LogManager {

    public static final int CLIENT_CODE_STACK_INDEX;

    protected static Logger LOGGER;

    protected static boolean activerLogger = true;

    private static LogManager logManager;

    public static LogManager getInstance() {
        if (logManager == null) {
            logManager = new LogManager();
        }
        return logManager;
    }

    static {
        if (LoggerFactory.getNomLogger() != null) {
            LOGGER = LoggerFactory.getLogger(LoggerFactory.getNomLogger());
        }

        int i = 0;
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            i++;
            if (ste.getClassName().equals(LogManager.class.getName())) {
                break;
            }
        }
        CLIENT_CODE_STACK_INDEX = i;
    }

    public String getNomMethode() {
        return getNomMethode(CLIENT_CODE_STACK_INDEX);
    }

    public String getNomMethode(int index) {
        return Thread.currentThread().getStackTrace()[index].getMethodName();
    }

    public String getNomClasse() {
        return getNomMethode(CLIENT_CODE_STACK_INDEX);
    }

    public String getNomClasse(int index) {
        return Thread.currentThread().getStackTrace()[index].getClassName();
    }

    public static boolean isLoggerDisponible() {
        return LOGGER != null;
    }

    public void afficherMessageErreur(String message, Class clazz) {
        afficherMessage(Level.SEVERE, message, null, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    public void afficherMessageErreur(String message, Throwable throwable, Class clazz) {
        afficherMessage(Level.SEVERE, message, throwable, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    public void afficherMessageWarning(String message, Class clazz) {
        afficherMessage(Level.WARNING, message, null, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    public void afficherMessageWarning(String message, Throwable throwable, Class clazz) {
        afficherMessage(Level.WARNING, message, throwable, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    public void afficherMessageInfo(String message, Class clazz) {
        afficherMessage(Level.INFO, message, null, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    public void afficherMessageInfo(String message, Throwable throwable, Class clazz) {
        afficherMessage(Level.INFO, message, throwable, CLIENT_CODE_STACK_INDEX + 2, clazz);
    }

    private void afficherMessage(Level level, String message, Throwable throwable, int index, Class clazz) {

        if (activerLogger) {
            if (isLoggerDisponible() && level != null && message != null) {

                String nomClasse = null;
                if (clazz != null) {
                    nomClasse = clazz.getSimpleName();
                } else {
                    nomClasse = getNomClasse(index);
                }
                String nomMethode = getNomMethode(index);

                if (throwable != null) {
                    LOGGER.logp(level, nomClasse, nomMethode, message, throwable);
                } else {
                    LOGGER.logp(level, nomClasse, nomMethode, message);
                }
            } else {

                if (level != null && message != null) {
                    if (level.getName().equals(Level.SEVERE.getName())) {
                        System.err.println(message);
                        if (throwable != null) {
                            System.err.println(ExceptionUtils.getFullStackTrace(throwable));
                        }
                    } else {
                        System.out.println(message);
                        if (throwable != null) {
                            System.out.println((ExceptionUtils.getFullStackTrace(throwable)));
                        }
                    }
                }
            }
        }
    }

    public void setActiverLogger(boolean activerLogger) {
        LogManager.activerLogger = activerLogger;
    }
}
