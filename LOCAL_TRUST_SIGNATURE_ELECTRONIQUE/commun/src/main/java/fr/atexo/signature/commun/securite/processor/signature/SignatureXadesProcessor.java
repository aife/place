package fr.atexo.signature.commun.securite.processor.signature;

import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;

import java.io.File;

/**
 * Interface exposant les méthodes permettant de signer les fichiers au format Xades.
 */
public interface SignatureXadesProcessor {

    /**
     * Execute la signature Xades du hash d'un fichier.
     *
     * @param keyPair   le certificat avec lequel signer le fichier
     * @param conteneur le conteneur du hash du fichier à signer.
     * @return les informations complémentaire sur le certificat utilisé pour effectuer la signature
     * @throws SignatureExecutionException
     */
    InfosComplementairesCertificat signerEnXades(KeyPair keyPair, ConteneurHashFichierSignatureXML conteneur) throws SignatureExecutionException;

    /**
     * Execute la signature Xades du fichier.
     *
     * @param keyPair   le certificat avec lequel signer le fichier
     * @param conteneur le conteneur du fichier à signer.
     * @return les informations complémentaire sur le certificat utilisé pour effectuer la signature
     * @throws SignatureExecutionException
     */
    InfosComplementairesCertificat signerEnXades(KeyPair keyPair, ConteneurFichierSignatureXML conteneur) throws SignatureExecutionException;

    /**
     * Execute la vérification de la conformité du fichier avec le fichier
     * de signature Xades
     *
     * @param fichier                    le fichier à vérifier
     * @param contenuFichierSignatureXML le contenu de la signature XML à utiliser pour effectuer la vérification
     * @param utiliserModuleValidation   spécifie si le serveur de validation doit être utilisé
     * @return les informations complémentaire sur le certificat utilisé pour effectuer la signature
     * @throws SignatureExecutionException
     */
    InfosComplementairesCertificat verifierConformite(File fichier, String contenuFichierSignatureXML, boolean utiliserModuleValidation) throws SignatureExecutionException;

    /**
     * Execute la vérification de la conformité du hash du fichier avec le fichier
     * de signature Xades
     *
     * @param hashFichier                le hash du fichier à vérifier
     * @param contenuFichierSignatureXML le contenu de la signature XML à utiliser pour effectuer la vérification
     * @param utiliserModuleValidation   spécifie si le serveur de validation doit être utilisé
     * @return les informations complémentaire sur le certificat utilisé pour effectuer la signature
     * @throws SignatureExecutionException
     */
    InfosComplementairesCertificat verifierConformite(byte[] hashFichier, String contenuFichierSignatureXML, boolean utiliserModuleValidation) throws SignatureExecutionException;
}
