package fr.atexo.signature.gui.provider.pkcs12;

import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateListener;
import fr.atexo.signature.gui.provider.CertificateSelectUiService;
import fr.atexo.signature.gui.provider.pkcs12.ui.MainFrame;

/**
 * Singleton permettant d'initialiser l'interfage graphique de l'utilisateur ainsi que d'associer le listener
 * qui sera lancé lors de la sélection du certificat PKCS12 sélectionné par l'utilisateur depuis son disque dur.
 */
public class Pkcs12CertificateUiService implements CertificateSelectUiService<Pkcs12CertificateListener> {

    private static Pkcs12CertificateUiService uiService;

    public static Pkcs12CertificateUiService getInstance() {
        if (uiService == null) {
            uiService = new Pkcs12CertificateUiService();
        }
        return uiService;
    }

    @Override
    public void initUi(Pkcs12CertificateListener listener, TypeOs typeOs, TypeProvider typeProvider, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificat) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.addListener(listener);
    }
}
