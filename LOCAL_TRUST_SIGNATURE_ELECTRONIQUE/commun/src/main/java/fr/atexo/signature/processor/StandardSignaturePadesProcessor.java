package fr.atexo.signature.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.ConteneurFichierSignatureXML;
import fr.atexo.signature.commun.securite.processor.signature.SignaturePadesProcessor;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.signature.SignaturePades;
import fr.atexo.signature.commun.securite.signature.VisualSignatureRectangle;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.commun.util.I18nUtil;

/**
 * Classe effectuant la signature Pades d'un fichier pdf.
 */
public class StandardSignaturePadesProcessor implements SignaturePadesProcessor {

	private TypeProvider typeProvider;
	private String signatureMotif;
	private String signatureLocalisation;

	public StandardSignaturePadesProcessor() {
	}

	public StandardSignaturePadesProcessor(TypeProvider typeProvider, String signatureMotif, String signatureLocalisation) {
		this.typeProvider = typeProvider;
		this.signatureMotif = signatureMotif;
		this.signatureLocalisation = signatureLocalisation;
	}

	@Override
	public boolean signerEnPades(KeyPair keyPair, ConteneurFichierSignatureXML conteneur) throws SignatureExecutionException {

		InputStream fichierInputStream = null;
		File fichierDestination = null;
		FileOutputStream fichierDestinationOutputStream = null;
		try {
			// fichier source et destination
			fichierInputStream = new FileInputStream(conteneur.getFichier());
			fichierDestination = getFichierDestination(conteneur.getFichier().getPath());
			fichierDestinationOutputStream = new FileOutputStream(fichierDestination);

			// certificats
			PrivateKey privateKey = keyPair.getPrivateKey();
			Certificate certificat = keyPair.getCertificate();
			String cn = CertificatUtil.getCN(keyPair.getCertificate().getSubjectX500Principal());
			String signatureTexte = I18nUtil.get("SIGNATURE_PADES_MESSAGE_SIGNATAIRE") + " " + (cn != null ? cn : "");

			VisualSignatureRectangle positionSignature = new VisualSignatureRectangle(10, 10, 210, 60);
			SignaturePades.signerFichierPdf(fichierInputStream, fichierDestinationOutputStream, certificat, privateKey, positionSignature,
					signatureMotif, signatureLocalisation, signatureTexte);
			conteneur.setFichier(fichierDestination);
			return true;

		} catch (Exception e) {
			if (fichierDestination != null && fichierDestination.exists()) {
				fichierDestination.delete();
			}
			throw new SignatureExecutionException("Un problème est survenu lors de la tentative de signature Pades du fichier ["
					+ conteneur.getFichier().getPath() + "]", e);
		} finally {
			try {
				if (fichierInputStream != null) {
					fichierInputStream.close();
				}
			} catch (IOException e) {
			}
			try {
				if (fichierDestinationOutputStream != null) {
					fichierDestinationOutputStream.close();
				}
			} catch (IOException e) {
			}

		}
	}

	/**
	 * Transforme un fichier source en flux (le fichier signé)
	 * 
	 * @param cheminFichierSource
	 *            le chemin du fichier source à signer, afin de pouvoir créer la
	 *            version signé du fichier dans le même répertoire.
	 * @return le flux vers le fichier source du fichier signé
	 * @throws java.io.IOException
	 */
	private File getFichierDestination(String cheminFichierSource) throws IOException {

		String urlFichierDisque = cheminFichierSource.substring(0, cheminFichierSource.lastIndexOf('.')) + "_signaturePades.pdf";
		File fichierDestination = new File(urlFichierDisque);

		return fichierDestination;
	}
}
