package fr.atexo.signature.gui.provider;

import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.util.CertificatUtil;

import java.util.EventListener;

/**
 * Interface générique d'intialisation de l'interface graphique de
 * l'utilisateur.
 */
public interface CertificateSelectUiService<C extends EventListener> {

    /**
     * Permet d'initialiser l'interface graphique de l'utilisateur.
     *
     * @param listener                le type de listeneur
     * @param typeOs                  le type d'os
     * @param typeProvider            le type de provider
     * @param filtrerSignatureModeRGS pour activer le filtre sur les roles pour le mode RGS ou non
     * @param typeCertificat          l'ensemble des types de rôles a appliquer si mode RGS non active
     */
    void initUi(C listener, TypeOs typeOs, TypeProvider typeProvider, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificat);
}
