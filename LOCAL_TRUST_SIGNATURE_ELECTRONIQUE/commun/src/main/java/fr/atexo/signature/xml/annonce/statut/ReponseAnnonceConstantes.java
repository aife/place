package fr.atexo.signature.xml.annonce.statut;

/**
 * Ensemble des constantes possibles.
 */
public class ReponseAnnonceConstantes {

    public static final String NOM_FICHIER_REPONSES_ANNONCE = "ReponsesAnnonce.xml";

    public static final int TAILLE_MAXIMALE_BLOC_CHIFFREMENT = 1024 * 1024 * 4;

    // statut d'ouverture
    public static final String STATUT_OUVERTURE_OUVERT = "OUV";
    public static final String STATUT_OUVERTURE_FERME = "FER";

    // statut d'admissibilité
    public static final String STATUT_ADMISSIBILITE_ATR = "ATR";
    public static final String STATUT_ADMISSIBILITE_ADM = "ADM";
    public static final String STATUT_ADMISSIBILITE_NAD = "NAD";

    // statut de chiffrement
    public static final String STATUT_CHIFFREMENT_CHIFFRE = "CHI";
    public static final String STATUT_CHIFFREMENT_NON_CHIFFRE = "NCH";

    // type enveloppe
    public static final int TYPE_ENVELOPPE_CANDIDATURE = 1;
    public static final int TYPE_ENVELOPPE_OFFRE = 2;
    public static final int TYPE_ENVELOPPE_ANONYME = 3;

    // type de fichier
    public static final String TYPE_FICHIER_PRI = "PRI";
    public static final String TYPE_FICHIER_ACE = "ACE";
    public static final String TYPE_FICHIER_SEC = "SEC";
}
