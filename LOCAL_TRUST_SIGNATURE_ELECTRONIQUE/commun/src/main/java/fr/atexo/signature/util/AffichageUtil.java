package fr.atexo.signature.util;

import fr.atexo.signature.commun.util.I18nUtil;
import fr.atexo.signature.commun.util.JaxbReponsesAnnonceUtil;
import fr.atexo.signature.commun.util.Util;
import fr.atexo.signature.xml.annonce.statut.ReponseAnnonceConstantes;

import java.io.File;
import java.util.List;

/**
 *
 */
public abstract class AffichageUtil {

    public static String getTypeEnvNomRepertoire(int typeEnveloppe, int numeroLot) {
        if (typeEnveloppe == 1) {
            return "Candidature";
        } else if (typeEnveloppe == 2 && numeroLot == 0) {
            return "Offre";
        } else if (typeEnveloppe == 2 && numeroLot > 0) {
            return "Offre_Lot_" + numeroLot;
        } else if (typeEnveloppe == 3 && numeroLot == 0) {
            return "Anonymat";
        } else if (typeEnveloppe == 3 && numeroLot > 0) {
            return "Anonymat_Lot_" + numeroLot;
        } else if (typeEnveloppe == 4 && numeroLot == 0) {
            return "OffreTechnique";
        } else if (typeEnveloppe == 4 && numeroLot > 0) {
            return "OffreTechnique_Lot" + numeroLot;
        }
        return "Indetermine";
    }

    /**
     * Retourne le type d'enveloppe sous forme de String
     *
     * @return le message en fonction de l'enveloppe et du numéro de lot
     */
    public static String getMessageTypeEnveloppeEtNumeroLot(JaxbReponsesAnnonceUtil.FichierBloc fichierBloc) {
        return getMessageTypeEnveloppeEtNumeroLot(fichierBloc.getTypeEnveloppe(), fichierBloc.getNumeroLot());
    }

    /**
     * Retourne le type d'enveloppe sous forme de String
     *
     * @return le message en fonction de l'enveloppe et du numéro de lot
     */
    public static String getMessageTypeEnveloppeEtNumeroLot(int typeEnveloppe, int numeroLot) {

        if (typeEnveloppe == ReponseAnnonceConstantes.TYPE_ENVELOPPE_CANDIDATURE) {
            return I18nUtil.get("ENVELOPPE_CANDIDATURE");
        } else if (typeEnveloppe == ReponseAnnonceConstantes.TYPE_ENVELOPPE_OFFRE && numeroLot == 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE");
        } else if (typeEnveloppe == ReponseAnnonceConstantes.TYPE_ENVELOPPE_OFFRE && numeroLot > 0) {
            return I18nUtil.get("ENVELOPPE_OFFRE") + " " + I18nUtil.get("LOT") + " " + numeroLot;
        } else if (typeEnveloppe == ReponseAnnonceConstantes.TYPE_ENVELOPPE_ANONYME && numeroLot == 0) {
            return I18nUtil.get("ENVELOPPE_ANONYMAT");
        } else if (typeEnveloppe == ReponseAnnonceConstantes.TYPE_ENVELOPPE_ANONYME && numeroLot > 0) {
            return I18nUtil.get("ENVELOPPE_ANONYMAT") + " " + I18nUtil.get("LOT") + " " + numeroLot;
        }

        return "";
    }

    /**
     * Retourne la taille totale d'un fichier.
     *
     * @param file le fichier
     * @return
     */
    public static String getMessageTailleFichier(File file) {
        return getMessageTaille(file.length());
    }

    /**
     * Retourne la taille totale d'un fichier.
     *
     * @param texte le fichier
     * @return
     */
    public static String getMessageTailleTexte(String texte) {
        return getMessageTaille(texte.length());
    }

    /**
     * Retourne la taille totale d'une liste de fichiers
     *
     * @param fichiers
     * @return
     */
    public static String getMessageTailleFichier(List<File> fichiers) {
        long tailleTotale = 0;
        for (File fichier : fichiers) {
            tailleTotale += fichier.length();
        }
        return getMessageTaille(tailleTotale);
    }

    /**
     * Retourne la taille en Ko / Mo / Go.
     *
     * @param taille la taille à convertir
     * @return
     */
    public static String getMessageTaille(long taille) {
        final int unKiloOctet = 1024;
        if (taille < 1000) {
            return taille + " " + I18nUtil.get("OCTETS");
        } else if (taille < 1000000) { //KO
            long ko = taille / unKiloOctet;
            return Util.arrondir(ko, 2) + " " + I18nUtil.get("ABBR_KILO_OCTETS");
        } else { // Mo ou Go
            if (taille < 1000000000) {// Mo
                long mo = taille / (unKiloOctet * unKiloOctet);
                return Util.arrondir(mo, 2) + " " + I18nUtil.get("ABBR_MEGA_OCTETS");
            } else {
                long go = taille / (unKiloOctet * unKiloOctet * unKiloOctet);
                return Util.arrondir(go, 2) + " " + I18nUtil.get("ABBR_GIGA_OCTETS");
            }
        }
    }
}
