package fr.atexo.signature.commun.securite.certificat.verification;

/**
 *
 */
public enum CertificatVerificationEtat implements CertificatCodeRetour {

    VerificationValide(0),

    CertificatInvalide(1),
    SignatureCertificatInvalide(1),

    ChaineCertificatNonTrouvee(2),
    ChaineCertificatIncomplete(2),

    CertificatRevoque(2),
    FichierCrlNonTrouve(1);

    private int codeRetour;

    private CertificatVerificationEtat(int codeRetour) {
        this.codeRetour = codeRetour;
    }

    public int getCodeRetour() {
        return codeRetour;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CertificatVerificationEtat");
        sb.append("{type=").append(name());
        sb.append("{codeRetour=").append(codeRetour);
        sb.append('}');
        return sb.toString();
    }
}
