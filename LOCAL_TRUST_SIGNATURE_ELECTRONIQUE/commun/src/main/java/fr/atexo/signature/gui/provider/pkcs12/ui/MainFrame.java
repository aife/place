/**
 *
 */
package fr.atexo.signature.gui.provider.pkcs12.ui;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;
import fr.atexo.signature.gui.provider.AbstractMainFrame;
import fr.atexo.signature.gui.browser.ui.ExtensionFileFilter;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateEvent;
import fr.atexo.signature.commun.util.I18nUtil;

/**
 * Frame correspondant au formulaire de sélection d'un fichier p12 depuis le disque dur
 * de l'utilisateur ainsi que le mot de passe associé à ce fichier afin de pouvoir accèder à son
 * contenu. Lors de la validation du formulaire alors un evenement est lancé afin de signer le fichier
 * pdf au format Pades avec le certificat p12 sélectionné.
 */
public class MainFrame extends AbstractMainFrame<Pkcs12CertificateEvent> implements ActionListener {

    private static final long serialVersionUID = -1835196612081409281L;
    public static final int width = 600;
    public static final int height = 160;

    private JButton validerButton;
    private JButton annulerButton;
    private JButton parcourirButton;
    private JTextField cheminFichierCertificatField;
    private JTextField motDePasseField;

    public MainFrame() {
        super();
        this.build();
        this.setVisible(true);
    }

    private void build() {
        setTitle(I18nUtil.get("SWING_PKCS12_TITRE"));
        setSize(width, height);
        setLocationRelativeTo(null);
        buildContainer(getContentPane());
    }

    private void buildContainer(Container container) {
        container.setLayout(new GridBagLayout());

        /* 2- Création et initialisation d'une série de composants. */
        JLabel cheminFichierCertificatLabel = new JLabel(I18nUtil.get("SWING_PKCS12_CHEMIN_CERTIFICAT"));
        JLabel motDePasseLabel = new JLabel(I18nUtil.get("SWING_PKCS12_MOT_DE_PASSE"));

        cheminFichierCertificatField = new JTextField();
        motDePasseField = new JPasswordField();

        parcourirButton = new JButton(I18nUtil.get("SWING_ACTION_PARCOURIR"));
        parcourirButton.addActionListener(this);
        annulerButton = new JButton(I18nUtil.get("SWING_ACTION_ANNULER"));
        annulerButton.addActionListener(this);
        annulerButton.setPreferredSize(parcourirButton.getPreferredSize());
        validerButton = new JButton(I18nUtil.get("SWING_ACTION_VALIDER"));
        validerButton.addActionListener(this);
        validerButton.setPreferredSize(parcourirButton.getPreferredSize());

        /*3- Ajout de ces composants en spécifiant les contraintes de type GridBagConstraints. */
        GridBagConstraints gbc = new GridBagConstraints();

        // positionnement  du textfield permettant de parcourir les
        // répertoires de l'utilisateur afin de trouver un certificat p12
        gbc.gridx = gbc.gridy = 0;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        gbc.insets = new Insets(0, 10, 0, 0);
        container.add(cheminFichierCertificatLabel, gbc);

        gbc.gridx = 1;
        gbc.gridwidth = GridBagConstraints.RELATIVE;
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 10, 0, 5);
        container.add(cheminFichierCertificatField, gbc);

        gbc.gridx = 2;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(0, 0, 0, 10);
        container.add(parcourirButton, gbc);

        // positionnement  du passwordfield permettant de préciser le mot
        // de passe associé au fichier p12 sélectionné par l'utilisateur
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 10, 0, 0);
        container.add(motDePasseLabel, gbc);

        gbc.gridx = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 10, 0, 10);
        container.add(motDePasseField, gbc);

        // positionnement du button d'annulation de l'action
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridheight = gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
        gbc.weightx = 1.;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(5, 0, 10, 5);
        container.add(annulerButton, gbc);

        // positionnement du button de validation de l'action
        gbc.gridx = 2;
        gbc.weightx = 0.;
        gbc.insets = new Insets(5, 0, 10, 10);
        container.add(validerButton, gbc);
    }


    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();

        if (source == annulerButton) {
            this.dispose();
            return;
        } else if (source == parcourirButton) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    final JFileChooser chooser = new JFileChooser();
                    ExtensionFileFilter filter = new ExtensionFileFilter("P12", new String[]{"P12"});
                    chooser.setFileFilter(filter);
                    chooser.setApproveButtonText(I18nUtil.get("SWING_PKCS12_SELECTIONNER"));
                    if (chooser.showOpenDialog(MainFrame.this) == JFileChooser.APPROVE_OPTION) {
                        cheminFichierCertificatField.setText(chooser.getSelectedFile().getAbsolutePath());
                    }
                }
            });
        } else if (source == validerButton) {
            if (cheminFichierCertificatField.getText().length() == 0) {
                JOptionPane.showMessageDialog(this, I18nUtil.get("SWING_ACTION_ALERTE_SELECTIONNER_CERTIFICAT"), I18nUtil.get("SWING_ACTION_ALERTE"), JOptionPane.WARNING_MESSAGE);
            } else if (motDePasseField.getText().length() == 0) {
                JOptionPane.showMessageDialog(this, I18nUtil.get("SWING_PKCS12_ACTION_ERREUR_MOT_DE_PASSE"), I18nUtil.get("SWING_ACTION_ERREUR"), JOptionPane.ERROR_MESSAGE);
            } else {
                Pkcs12CertificateEvent certEvt = new Pkcs12CertificateEvent(this, cheminFichierCertificatField.getText(), motDePasseField.getText());
                this.fireCertificateEvent(certEvt);
                this.dispose();
            }

        } else {
            this.dispose();
            return;
        }
    }
}
