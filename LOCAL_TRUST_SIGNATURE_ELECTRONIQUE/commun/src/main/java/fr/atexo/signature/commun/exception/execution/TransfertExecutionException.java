package fr.atexo.signature.commun.exception.execution;

/**
 *  Exception pour signaler un souci lors du transfert d'informations
 */
public class TransfertExecutionException extends ExecutionException {

    public TransfertExecutionException(String message) {
        super(message);
    }

    public TransfertExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
