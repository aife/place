package fr.atexo.signature.commun.securite.signature;

public class VisualSignatureRectangle {

	protected float llx;
	protected float lly;
	protected float urx;
	protected float ury;

	public VisualSignatureRectangle(final float llx, final float lly, final float urx, final float ury) {
		this.llx = llx;
		this.lly = lly;
		this.urx = urx;
		this.ury = ury;
	}

	public float getLeft() {
		return llx;
	}

	public float getRight() {
		return urx;
	}

	public float getWidth() {
		return urx - llx;
	}

	public float getTop() {
		return ury;
	}

	public float getBottom() {
		return lly;
	}
}
