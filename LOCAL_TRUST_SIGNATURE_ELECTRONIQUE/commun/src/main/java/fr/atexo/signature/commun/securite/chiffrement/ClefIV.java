package fr.atexo.signature.commun.securite.chiffrement;

import java.security.cert.X509Certificate;

/**
 *
 */
public class ClefIV {

    private byte[] clef;

    private byte[] iv;

    private X509Certificate certificat;

    public ClefIV(byte[] clef, byte[] iv, X509Certificate certificat) {
        this.clef = clef;
        this.iv = iv;
        this.certificat = certificat;
    }

    public byte[] getClef() {
        return clef;
    }

    public byte[] getIv() {
        return iv;
    }

    public X509Certificate getCertificat() {
        return certificat;
    }
}
