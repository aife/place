package fr.atexo.signature.commun.securite.chiffrement;

import net.iharder.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class EnveloppeXML {

    public static String creerEnveloppeXml(byte[] donnees, List<ClefIV> clefIvs, int tailleDonnees) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        // root elements
        Document document = documentBuilder.newDocument();
        Element racineElement = document.createElement("enveloppe");
        document.appendChild(racineElement);

        // envData elements
        Element envDataElement = document.createElement("envData");
        envDataElement.appendChild(document.createTextNode(Base64.encodeBytes(donnees)));
        racineElement.appendChild(envDataElement);

        // envSize elements
        Element envSizeElement = document.createElement("envSize");
        envSizeElement.appendChild(document.createTextNode(String.valueOf(tailleDonnees)));
        racineElement.appendChild(envSizeElement);

        // keysIvs elements
        Element keysIvsElement = document.createElement("keysIvs");
        racineElement.appendChild(keysIvsElement);

        for (int i = 0; i < clefIvs.size(); i++) {

            ClefIV clefIV = clefIvs.get(i);

            // keysIv elements
            Element keyIvElement = document.createElement("keyIv");
            keysIvsElement.appendChild(keyIvElement);

            // key elements
            Element keyElement = document.createElement("key");
            String keyData = Base64.encodeBytes(clefIV.getClef());
            keyElement.appendChild(document.createTextNode(keyData));
            keyIvElement.appendChild(keyElement);

            // iv elements
            Element ivElement = document.createElement("iv");
            String ivData = Base64.encodeBytes(clefIV.getIv());
            ivElement.appendChild(document.createTextNode(ivData));
            keyIvElement.appendChild(ivElement);

            Element certificatElement = document.createElement("certificat");
            try {
                String certificatData = Base64.encodeBytes(clefIV.getCertificat().getEncoded());
                certificatElement.appendChild(document.createTextNode(certificatData));
            } catch (Exception e) {
                e.printStackTrace();
                //TODO ?
            }
            keyIvElement.appendChild(certificatElement);
        }

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);

        // Output to console for testing
        StringWriter writer = new StringWriter();
        Result result = new StreamResult(writer);

        transformer.transform(source, result);
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }

    public static DonneesXML chargerEnveloppeXml(String enveloppeXML) throws CertificateException, ParserConfigurationException, IOException, SAXException {

        DonneesXML donneesXML = null;
        InputStream inputStreamEnveloppeXML = null;

        inputStreamEnveloppeXML = new ByteArrayInputStream(enveloppeXML.getBytes());

        // document factory
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        // racine xml
        Document document = documentBuilder.parse(inputStreamEnveloppeXML);
        document.getDocumentElement().normalize();


        byte[] donnees = null;
        int taille = 0;

        NodeList nodeListEnveloppeElement = document.getElementsByTagName("enveloppe");
        for (int i = 0; i < nodeListEnveloppeElement.getLength(); i++) {
            Node nodeEnveloppeElement = nodeListEnveloppeElement.item(i);
            if (nodeEnveloppeElement.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nodeEnveloppeElement;
                donnees = Base64.decode(getValeurContenuDansTag("envData", element));
                taille = Integer.parseInt(getValeurContenuDansTag("envSize", element));
                break;
            }
        }

        List<ClefIV> clefIVs = new ArrayList<ClefIV>();

        NodeList nodeListKeyIvElement = document.getElementsByTagName("keyIv");
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");

        for (int i = 0; i < nodeListKeyIvElement.getLength(); i++) {

            Node nodeKeyIv = nodeListKeyIvElement.item(i);
            if (nodeKeyIv.getNodeType() == Node.ELEMENT_NODE) {

                Element elementKeyIv = (Element) nodeKeyIv;
                byte[] key = Base64.decode(getValeurContenuDansTag("key", elementKeyIv));
                byte[] iv = Base64.decode(getValeurContenuDansTag("iv", elementKeyIv));

                byte[] certificat = Base64.decode(getValeurContenuDansTag("certificat", elementKeyIv));
                InputStream inputStreamCertificat = new ByteArrayInputStream(certificat);

                X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(inputStreamCertificat);

                ClefIV clefIV = new ClefIV(key, iv, certificate);
                clefIVs.add(clefIV);
            }
        }

        donneesXML = new DonneesXML(donnees, taille, clefIVs);

        return donneesXML;
    }

    private static String getValeurContenuDansTag(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }
}
