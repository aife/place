
package fr.atexo.signature.xml.pkcs11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for libType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="libType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="chemin" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="information" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "libType", propOrder = {
    "value"
})
public class LibType {

    @XmlValue
    protected String value;
    @XmlAttribute(required = true)
    protected String chemin;
    @XmlAttribute
    protected String information;

    /**
     * Gets the value of the value property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the chemin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getChemin() {
        return chemin;
    }

    /**
     * Sets the value of the chemin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setChemin(String value) {
        this.chemin = value;
    }

    /**
     * Gets the value of the information property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getInformation() {
        return information;
    }

    /**
     * Sets the value of the information property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setInformation(String value) {
        this.information = value;
    }

}
