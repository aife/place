package fr.atexo.signature.commun.exception.certificat;

/**
 * Exception pour signaler un souci lors de la manipulation des certificats.
 */
public class ManipulationCertificatException extends Exception {

    public ManipulationCertificatException(String message) {
        super(message);
    }

    public ManipulationCertificatException(String message, Throwable cause) {
        super(message, cause);
    }
}
