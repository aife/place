package fr.atexo.signature.commun.securite.certificat;

import java.io.File;
import java.security.cert.X509Certificate;

/**
 *
 */
public class InfosCertificat extends AbstractInfos {

    private X509Certificate certificat;

    public InfosCertificat(X509Certificate certificat) {
        this(certificat, null);
    }

    public InfosCertificat(X509Certificate certificat, File fichierDisque) {
        this.certificat = certificat;
        this.fichierDisque = fichierDisque;
    }

    public X509Certificate getCertificat() {
        return certificat;
    }

    public File getFichierDisque() {
        return fichierDisque;
    }
}