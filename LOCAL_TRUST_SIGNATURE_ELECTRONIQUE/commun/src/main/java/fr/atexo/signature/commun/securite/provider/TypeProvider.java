package fr.atexo.signature.commun.securite.provider;

/**
 * Le type de provider possible pour la récupération du certificat client.
 */
public enum TypeProvider {

    MSCAPI("SunMSCAPI", "Windows-MY", "sun.security.mscapi.KeyStore$MY"),
    APPLE("Apple", "KeychainStore", null /*"com.apple.crypto.provider.KeychainStore"*/),
    PKCS11(null,"PKCS11",null),
    PKCS12(null, "PKCS12", null),
    BC("BC", "BC", null);

    private String nom;

    private String type;

    private String keyStoreVeritable;

    TypeProvider(String nom, String type, String keyStoreVeritable) {
        this.nom = nom;
        this.type = type;
        this.keyStoreVeritable = keyStoreVeritable;
    }

    public String getKeyStoreVeritable() {
        return keyStoreVeritable;
    }

    public String getNom() {
        return nom;
    }

    public String getType() {
        return type;
    }

    public static TypeProvider get(String nom) {

        for (TypeProvider typeProvider : values()) {
            if (typeProvider.name().equals(nom)) {
                return typeProvider;
            }
        }

        return null;
    }
}
