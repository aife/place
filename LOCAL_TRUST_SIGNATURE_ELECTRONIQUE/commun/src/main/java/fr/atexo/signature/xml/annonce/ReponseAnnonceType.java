package fr.atexo.signature.xml.annonce;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ReponseAnnonceType complex type.
 * <p/>
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p/>
 * 
 * <pre>
 * &lt;complexType name="ReponseAnnonceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperateurEconomique" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Annonce" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Intitule" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Objet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TypesEnveloppe">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="typeProcedure" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="nombreLots" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                 &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="entiteAchat" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="referenceUtilisateur" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="chiffrement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="signature" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="allotissement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="separationSignatureAe" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                 &lt;attribute name="dateLimite" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Enveloppes">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Enveloppe" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Fichier" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BlocsChiffrement">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="BlocChiffrement" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                                         &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute ref="{}statutChiffrement"/>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="typeFichier" use="required">
 *                                       &lt;simpleType>
 *                                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                           &lt;enumeration value="PRI"/>
 *                                           &lt;enumeration value="ACE"/>
 *                                           &lt;enumeration value="SEC"/>
 *                                         &lt;/restriction>
 *                                       &lt;/simpleType>
 *                                     &lt;/attribute>
 *                                     &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                     &lt;attribute ref="{}statutChiffrement"/>
 *                                     &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                           &lt;attribute name="type" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                                 &lt;enumeration value="1"/>
 *                                 &lt;enumeration value="2"/>
 *                                 &lt;enumeration value="3"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                           &lt;attribute name="statutOuverture" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="OUV"/>
 *                                 &lt;enumeration value="FER"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute name="statutAdmissibilite" use="required">
 *                             &lt;simpleType>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                 &lt;enumeration value="ATR"/>
 *                                 &lt;enumeration value="ADM"/>
 *                                 &lt;enumeration value="NAD"/>
 *                               &lt;/restriction>
 *                             &lt;/simpleType>
 *                           &lt;/attribute>
 *                           &lt;attribute ref="{}statutChiffrement"/>
 *                           &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="idReponseAnnonce" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="indexReponse" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="horodatageDepot" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="typeSignature" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReponseAnnonceType", propOrder = { "operateurEconomique", "annonce", "enveloppes" })
@XmlSeeAlso({ ReponseAnnonce.class })
public class ReponseAnnonceType {

	@XmlElement(name = "OperateurEconomique")
	protected ReponseAnnonceType.OperateurEconomique operateurEconomique;
	@XmlElement(name = "Annonce")
	protected ReponseAnnonceType.Annonce annonce;
	@XmlElement(name = "Enveloppes", required = true)
	protected ReponseAnnonceType.Enveloppes enveloppes;
	@XmlAttribute(required = true)
	protected int idReponseAnnonce;
	@XmlAttribute
	protected Integer indexReponse;
	@XmlAttribute
	protected String organisme;
	@XmlAttribute
	protected String reference;
	@XmlAttribute
	protected String horodatageDepot;
	@XmlAttribute
	protected String typeSignature;

	/**
	 * Gets the value of the operateurEconomique property.
	 * 
	 * @return possible object is {@link ReponseAnnonceType.OperateurEconomique }
	 */
	public ReponseAnnonceType.OperateurEconomique getOperateurEconomique() {
		return operateurEconomique;
	}

	/**
	 * Sets the value of the operateurEconomique property.
	 * 
	 * @param value
	 *            allowed object is
	 *            {@link ReponseAnnonceType.OperateurEconomique }
	 */
	public void setOperateurEconomique(ReponseAnnonceType.OperateurEconomique value) {
		this.operateurEconomique = value;
	}

	/**
	 * Gets the value of the annonce property.
	 * 
	 * @return possible object is {@link ReponseAnnonceType.Annonce }
	 */
	public ReponseAnnonceType.Annonce getAnnonce() {
		return annonce;
	}

	/**
	 * Sets the value of the annonce property.
	 * 
	 * @param value
	 *            allowed object is {@link ReponseAnnonceType.Annonce }
	 */
	public void setAnnonce(ReponseAnnonceType.Annonce value) {
		this.annonce = value;
	}

	/**
	 * Gets the value of the enveloppes property.
	 * 
	 * @return possible object is {@link ReponseAnnonceType.Enveloppes }
	 */
	public ReponseAnnonceType.Enveloppes getEnveloppes() {
		return enveloppes;
	}

	/**
	 * Sets the value of the enveloppes property.
	 * 
	 * @param value
	 *            allowed object is {@link ReponseAnnonceType.Enveloppes }
	 */
	public void setEnveloppes(ReponseAnnonceType.Enveloppes value) {
		this.enveloppes = value;
	}

	/**
	 * Gets the value of the idReponseAnnonce property.
	 */
	public int getIdReponseAnnonce() {
		return idReponseAnnonce;
	}

	/**
	 * Sets the value of the idReponseAnnonce property.
	 */
	public void setIdReponseAnnonce(int value) {
		this.idReponseAnnonce = value;
	}

	/**
	 * @return
	 */
	public Integer getIndexReponse() {
		return indexReponse;
	}

	/**
	 * @param indexReponse
	 */
	public void setIndexReponse(Integer indexReponse) {
		this.indexReponse = indexReponse;
	}

	/**
	 * Gets the value of the organisme property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getOrganisme() {
		return organisme;
	}

	/**
	 * Sets the value of the organisme property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setOrganisme(String value) {
		this.organisme = value;
	}

	/**
	 * Gets the value of the reference property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * Sets the value of the reference property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setReference(String value) {
		this.reference = value;
	}

	/**
	 * Gets the value of the horodatageDepot property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getHorodatageDepot() {
		return horodatageDepot;
	}

	/**
	 * Sets the value of the horodatageDepot property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setHorodatageDepot(String value) {
		this.horodatageDepot = value;
	}

	/**
	 * Gets the value of the typeSignature property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getTypeSignature() {
		return typeSignature;
	}

	/**
	 * Sets the value of the typeSignature property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setTypeSignature(String value) {
		this.typeSignature = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * <p/>
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * <p/>
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Intitule" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="Objet" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="TypesEnveloppe">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                           &lt;/sequence>
	 *                           &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                           &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *       &lt;attribute name="typeProcedure" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="nombreLots" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *       &lt;attribute name="organisme" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="entiteAchat" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="reference" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="referenceUtilisateur" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *       &lt;attribute name="chiffrement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
	 *       &lt;attribute name="signature" type="{http://www.w3.org/2001/XMLSchema}boolean" />
	 *       &lt;attribute name="allotissement" type="{http://www.w3.org/2001/XMLSchema}boolean" />
	 *       &lt;attribute name="separationSignatureAe" type="{http://www.w3.org/2001/XMLSchema}boolean" />
	 *       &lt;attribute name="dateLimite" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "intitule", "objet", "typesEnveloppe" })
	public static class Annonce {

		@XmlElement(name = "Intitule", required = true)
		protected String intitule;
		@XmlElement(name = "Objet", required = true)
		protected String objet;
		@XmlElement(name = "TypesEnveloppe", required = true)
		protected ReponseAnnonceType.Annonce.TypesEnveloppe typesEnveloppe;
		@XmlAttribute
		protected String typeProcedure;
		@XmlAttribute
		protected Integer nombreLots;
		@XmlAttribute
		protected String organisme;
		@XmlAttribute
		protected String entiteAchat;
		@XmlAttribute
		protected String reference;
		@XmlAttribute
		protected String referenceUtilisateur;
		@XmlAttribute
		protected Boolean chiffrement;
		@XmlAttribute
		protected Boolean signature;
		@XmlAttribute
		protected Boolean allotissement;
		@XmlAttribute
		protected Boolean separationSignatureAe;
		@XmlAttribute
		protected String dateLimite;

		/**
		 * Gets the value of the intitule property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getIntitule() {
			return intitule;
		}

		/**
		 * Sets the value of the intitule property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setIntitule(String value) {
			this.intitule = value;
		}

		/**
		 * Gets the value of the objet property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getObjet() {
			return objet;
		}

		/**
		 * Sets the value of the objet property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setObjet(String value) {
			this.objet = value;
		}

		/**
		 * Gets the value of the typesEnveloppe property.
		 * 
		 * @return possible object is
		 *         {@link ReponseAnnonceType.Annonce.TypesEnveloppe }
		 */
		public ReponseAnnonceType.Annonce.TypesEnveloppe getTypesEnveloppe() {
			return typesEnveloppe;
		}

		/**
		 * Sets the value of the typesEnveloppe property.
		 * 
		 * @param value
		 *            allowed object is
		 *            {@link ReponseAnnonceType.Annonce.TypesEnveloppe }
		 */
		public void setTypesEnveloppe(ReponseAnnonceType.Annonce.TypesEnveloppe value) {
			this.typesEnveloppe = value;
		}

		/**
		 * Gets the value of the typeProcedure property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getTypeProcedure() {
			return typeProcedure;
		}

		/**
		 * Sets the value of the typeProcedure property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setTypeProcedure(String value) {
			this.typeProcedure = value;
		}

		/**
		 * Gets the value of the nombreLots property.
		 * 
		 * @return possible object is {@link Integer }
		 */
		public Integer getNombreLots() {
			return nombreLots;
		}

		/**
		 * Sets the value of the nombreLots property.
		 * 
		 * @param value
		 *            allowed object is {@link Integer }
		 */
		public void setNombreLots(Integer value) {
			this.nombreLots = value;
		}

		/**
		 * Gets the value of the organisme property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getOrganisme() {
			return organisme;
		}

		/**
		 * Sets the value of the organisme property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setOrganisme(String value) {
			this.organisme = value;
		}

		/**
		 * Gets the value of the entiteAchat property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getEntiteAchat() {
			return entiteAchat;
		}

		/**
		 * Sets the value of the entiteAchat property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setEntiteAchat(String value) {
			this.entiteAchat = value;
		}

		/**
		 * Gets the value of the reference property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getReference() {
			return reference;
		}

		/**
		 * Sets the value of the reference property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setReference(String value) {
			this.reference = value;
		}

		/**
		 * Gets the value of the referenceUtilisateur property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getReferenceUtilisateur() {
			return referenceUtilisateur;
		}

		/**
		 * Sets the value of the referenceUtilisateur property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setReferenceUtilisateur(String value) {
			this.referenceUtilisateur = value;
		}

		/**
		 * Gets the value of the chiffrement property.
		 * 
		 * @return possible object is {@link Boolean }
		 */
		public Boolean isChiffrement() {
			return chiffrement;
		}

		/**
		 * Sets the value of the chiffrement property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 */
		public void setChiffrement(Boolean value) {
			this.chiffrement = value;
		}

		/**
		 * Gets the value of the signature property.
		 * 
		 * @return possible object is {@link Boolean }
		 */
		public Boolean isSignature() {
			return signature;
		}

		/**
		 * Sets the value of the signature property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 */
		public void setSignature(Boolean value) {
			this.signature = value;
		}

		/**
		 * Gets the value of the allotissement property.
		 * 
		 * @return possible object is {@link Boolean }
		 */
		public Boolean isAllotissement() {
			return allotissement;
		}

		/**
		 * Sets the value of the allotissement property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 */
		public void setAllotissement(Boolean value) {
			this.allotissement = value;
		}

		/**
		 * Gets the value of the separationSignatureAe property.
		 * 
		 * @return possible object is {@link Boolean }
		 */
		public Boolean isSeparationSignatureAe() {
			return separationSignatureAe;
		}

		/**
		 * Sets the value of the separationSignatureAe property.
		 * 
		 * @param value
		 *            allowed object is {@link Boolean }
		 */
		public void setSeparationSignatureAe(Boolean value) {
			this.separationSignatureAe = value;
		}

		/**
		 * Gets the value of the dateLimite property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getDateLimite() {
			return dateLimite;
		}

		/**
		 * Sets the value of the dateLimite property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setDateLimite(String value) {
			this.dateLimite = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * <p/>
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * <p/>
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="TypeEnveloppe" maxOccurs="unbounded">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                 &lt;/sequence>
		 *                 &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                 &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "typeEnveloppe" })
		public static class TypesEnveloppe {

			@XmlElement(name = "TypeEnveloppe", required = true)
			protected List<ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe> typeEnveloppe;

			/**
			 * Gets the value of the typeEnveloppe property.
			 * <p/>
			 * <p/>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the typeEnveloppe property.
			 * <p/>
			 * <p/>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getTypeEnveloppe().add(newItem);
			 * </pre>
			 * <p/>
			 * <p/>
			 * <p/>
			 * Objects of the following type(s) are allowed in the list
			 * {@link ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe }
			 */
			public List<ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe> getTypeEnveloppe() {
				if (typeEnveloppe == null) {
					typeEnveloppe = new ArrayList<ReponseAnnonceType.Annonce.TypesEnveloppe.TypeEnveloppe>();
				}
				return this.typeEnveloppe;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * <p/>
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * <p/>
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="CertificatChiffrement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *       &lt;/sequence>
			 *       &lt;attribute name="type" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *       &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "certificatChiffrement" })
			public static class TypeEnveloppe {

				@XmlElement(name = "CertificatChiffrement")
				protected String certificatChiffrement;
				@XmlAttribute(required = true)
				protected int type;
				@XmlAttribute(required = true)
				protected int numLot;

				/**
				 * Gets the value of the certificatChiffrement property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getCertificatChiffrement() {
					return certificatChiffrement;
				}

				/**
				 * Sets the value of the certificatChiffrement property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setCertificatChiffrement(String value) {
					this.certificatChiffrement = value;
				}

				/**
				 * Gets the value of the type property.
				 */
				public int getType() {
					return type;
				}

				/**
				 * Sets the value of the type property.
				 */
				public void setType(int value) {
					this.type = value;
				}

				/**
				 * Gets the value of the numLot property.
				 */
				public int getNumLot() {
					return numLot;
				}

				/**
				 * Sets the value of the numLot property.
				 */
				public void setNumLot(int value) {
					this.numLot = value;
				}

			}

		}

	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * <p/>
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * <p/>
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Enveloppe" maxOccurs="unbounded">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="Fichier" maxOccurs="unbounded">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
	 *                             &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="BlocsChiffrement">
	 *                               &lt;complexType>
	 *                                 &lt;complexContent>
	 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                     &lt;sequence>
	 *                                       &lt;element name="BlocChiffrement" maxOccurs="unbounded">
	 *                                         &lt;complexType>
	 *                                           &lt;complexContent>
	 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                                               &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                                               &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                                               &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                                               &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                                               &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                                               &lt;attribute ref="{}statutChiffrement"/>
	 *                                             &lt;/restriction>
	 *                                           &lt;/complexContent>
	 *                                         &lt;/complexType>
	 *                                       &lt;/element>
	 *                                     &lt;/sequence>
	 *                                     &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                                   &lt;/restriction>
	 *                                 &lt;/complexContent>
	 *                               &lt;/complexType>
	 *                             &lt;/element>
	 *                           &lt;/sequence>
	 *                           &lt;attribute name="typeFichier" use="required">
	 *                             &lt;simpleType>
	 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                                 &lt;enumeration value="PRI"/>
	 *                                 &lt;enumeration value="ACE"/>
	 *                                 &lt;enumeration value="SEC"/>
	 *                               &lt;/restriction>
	 *                             &lt;/simpleType>
	 *                           &lt;/attribute>
	 *                           &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                           &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                           &lt;attribute ref="{}statutChiffrement"/>
	 *                           &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                 &lt;/sequence>
	 *                 &lt;attribute name="type" use="required">
	 *                   &lt;simpleType>
	 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
	 *                       &lt;enumeration value="1"/>
	 *                       &lt;enumeration value="2"/>
	 *                       &lt;enumeration value="3"/>
	 *                     &lt;/restriction>
	 *                   &lt;/simpleType>
	 *                 &lt;/attribute>
	 *                 &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *                 &lt;attribute name="statutOuverture" use="required">
	 *                   &lt;simpleType>
	 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                       &lt;enumeration value="OUV"/>
	 *                       &lt;enumeration value="FER"/>
	 *                     &lt;/restriction>
	 *                   &lt;/simpleType>
	 *                 &lt;/attribute>
	 *                 &lt;attribute name="statutAdmissibilite" use="required">
	 *                   &lt;simpleType>
	 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
	 *                       &lt;enumeration value="ATR"/>
	 *                       &lt;enumeration value="ADM"/>
	 *                       &lt;enumeration value="NAD"/>
	 *                     &lt;/restriction>
	 *                   &lt;/simpleType>
	 *                 &lt;/attribute>
	 *                 &lt;attribute ref="{}statutChiffrement"/>
	 *                 &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "enveloppe" })
	public static class Enveloppes {

		@XmlElement(name = "Enveloppe", required = true)
		protected List<ReponseAnnonceType.Enveloppes.Enveloppe> enveloppe;

		/**
		 * Gets the value of the enveloppe property.
		 * <p/>
		 * <p/>
		 * This accessor method returns a reference to the live list, not a
		 * snapshot. Therefore any modification you make to the returned list
		 * will be present inside the JAXB object. This is why there is not a
		 * <CODE>set</CODE> method for the enveloppe property.
		 * <p/>
		 * <p/>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getEnveloppe().add(newItem);
		 * </pre>
		 * <p/>
		 * <p/>
		 * <p/>
		 * Objects of the following type(s) are allowed in the list
		 * {@link ReponseAnnonceType.Enveloppes.Enveloppe }
		 */
		public List<ReponseAnnonceType.Enveloppes.Enveloppe> getEnveloppe() {
			if (enveloppe == null) {
				enveloppe = new ArrayList<ReponseAnnonceType.Enveloppes.Enveloppe>();
			}
			return this.enveloppe;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * <p/>
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * <p/>
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="Fichier" maxOccurs="unbounded">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
		 *                   &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="BlocsChiffrement">
		 *                     &lt;complexType>
		 *                       &lt;complexContent>
		 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                           &lt;sequence>
		 *                             &lt;element name="BlocChiffrement" maxOccurs="unbounded">
		 *                               &lt;complexType>
		 *                                 &lt;complexContent>
		 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                                     &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                                     &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                                     &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                                     &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                                     &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *                                     &lt;attribute ref="{}statutChiffrement"/>
		 *                                   &lt;/restriction>
		 *                                 &lt;/complexContent>
		 *                               &lt;/complexType>
		 *                             &lt;/element>
		 *                           &lt;/sequence>
		 *                           &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                         &lt;/restriction>
		 *                       &lt;/complexContent>
		 *                     &lt;/complexType>
		 *                   &lt;/element>
		 *                 &lt;/sequence>
		 *                 &lt;attribute name="typeFichier" use="required">
		 *                   &lt;simpleType>
		 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *                       &lt;enumeration value="PRI"/>
		 *                       &lt;enumeration value="ACE"/>
		 *                       &lt;enumeration value="SEC"/>
		 *                     &lt;/restriction>
		 *                   &lt;/simpleType>
		 *                 &lt;/attribute>
		 *                 &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                 &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *                 &lt;attribute ref="{}statutChiffrement"/>
		 *                 &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *       &lt;/sequence>
		 *       &lt;attribute name="type" use="required">
		 *         &lt;simpleType>
		 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
		 *             &lt;enumeration value="1"/>
		 *             &lt;enumeration value="2"/>
		 *             &lt;enumeration value="3"/>
		 *           &lt;/restriction>
		 *         &lt;/simpleType>
		 *       &lt;/attribute>
		 *       &lt;attribute name="numLot" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *       &lt;attribute name="statutOuverture" use="required">
		 *         &lt;simpleType>
		 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *             &lt;enumeration value="OUV"/>
		 *             &lt;enumeration value="FER"/>
		 *           &lt;/restriction>
		 *         &lt;/simpleType>
		 *       &lt;/attribute>
		 *       &lt;attribute name="statutAdmissibilite" use="required">
		 *         &lt;simpleType>
		 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
		 *             &lt;enumeration value="ATR"/>
		 *             &lt;enumeration value="ADM"/>
		 *             &lt;enumeration value="NAD"/>
		 *           &lt;/restriction>
		 *         &lt;/simpleType>
		 *       &lt;/attribute>
		 *       &lt;attribute ref="{}statutChiffrement"/>
		 *       &lt;attribute name="idEnveloppe" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "fichier" })
		public static class Enveloppe {

			@XmlElement(name = "Fichier", required = true)
			protected List<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier> fichier;
			@XmlAttribute(required = true)
			protected int type;
			@XmlAttribute(required = true)
			protected int numLot;
			@XmlAttribute(required = true)
			protected String statutOuverture;
			@XmlAttribute(required = true)
			protected String statutAdmissibilite;
			@XmlAttribute
			protected String statutChiffrement;
			@XmlAttribute(required = true)
			protected int idEnveloppe;

			/**
			 * Gets the value of the fichier property.
			 * <p/>
			 * <p/>
			 * This accessor method returns a reference to the live list, not a
			 * snapshot. Therefore any modification you make to the returned
			 * list will be present inside the JAXB object. This is why there is
			 * not a <CODE>set</CODE> method for the fichier property.
			 * <p/>
			 * <p/>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getFichier().add(newItem);
			 * </pre>
			 * <p/>
			 * <p/>
			 * <p/>
			 * Objects of the following type(s) are allowed in the list
			 * {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier }
			 */
			public List<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier> getFichier() {
				if (fichier == null) {
					fichier = new ArrayList<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier>();
				}
				return this.fichier;
			}

			/**
			 * Gets the value of the type property.
			 */
			public int getType() {
				return type;
			}

			/**
			 * Sets the value of the type property.
			 */
			public void setType(int value) {
				this.type = value;
			}

			/**
			 * Gets the value of the numLot property.
			 */
			public int getNumLot() {
				return numLot;
			}

			/**
			 * Sets the value of the numLot property.
			 */
			public void setNumLot(int value) {
				this.numLot = value;
			}

			/**
			 * Gets the value of the statutOuverture property.
			 * 
			 * @return possible object is {@link String }
			 */
			public String getStatutOuverture() {
				return statutOuverture;
			}

			/**
			 * Sets the value of the statutOuverture property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 */
			public void setStatutOuverture(String value) {
				this.statutOuverture = value;
			}

			/**
			 * Gets the value of the statutAdmissibilite property.
			 * 
			 * @return possible object is {@link String }
			 */
			public String getStatutAdmissibilite() {
				return statutAdmissibilite;
			}

			/**
			 * Sets the value of the statutAdmissibilite property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 */
			public void setStatutAdmissibilite(String value) {
				this.statutAdmissibilite = value;
			}

			/**
			 * Gets the value of the statutChiffrement property.
			 * 
			 * @return possible object is {@link String }
			 */
			public String getStatutChiffrement() {
				return statutChiffrement;
			}

			/**
			 * Sets the value of the statutChiffrement property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 */
			public void setStatutChiffrement(String value) {
				this.statutChiffrement = value;
			}

			/**
			 * Gets the value of the idEnveloppe property.
			 */
			public int getIdEnveloppe() {
				return idEnveloppe;
			}

			/**
			 * Sets the value of the idEnveloppe property.
			 */
			public void setIdEnveloppe(int value) {
				this.idEnveloppe = value;
			}

			/**
			 * <p>
			 * Java class for anonymous complex type.
			 * <p/>
			 * <p>
			 * The following schema fragment specifies the expected content
			 * contained within this class.
			 * <p/>
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="Signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
			 *         &lt;element name="Empreinte" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="BlocsChiffrement">
			 *           &lt;complexType>
			 *             &lt;complexContent>
			 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                 &lt;sequence>
			 *                   &lt;element name="BlocChiffrement" maxOccurs="unbounded">
			 *                     &lt;complexType>
			 *                       &lt;complexContent>
			 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *                           &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *                           &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *                           &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *                           &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *                           &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
			 *                           &lt;attribute ref="{}statutChiffrement"/>
			 *                         &lt;/restriction>
			 *                       &lt;/complexContent>
			 *                     &lt;/complexType>
			 *                   &lt;/element>
			 *                 &lt;/sequence>
			 *                 &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *               &lt;/restriction>
			 *             &lt;/complexContent>
			 *           &lt;/complexType>
			 *         &lt;/element>
			 *       &lt;/sequence>
			 *       &lt;attribute name="typeFichier" use="required">
			 *         &lt;simpleType>
			 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
			 *             &lt;enumeration value="PRI"/>
			 *             &lt;enumeration value="ACE"/>
			 *             &lt;enumeration value="SEC"/>
			 *           &lt;/restriction>
			 *         &lt;/simpleType>
			 *       &lt;/attribute>
			 *       &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *       &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *       &lt;attribute ref="{}statutChiffrement"/>
			 *       &lt;attribute name="nbrBlocsChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "nom", "signature", "empreinte", "blocsChiffrement" })
			public static class Fichier {

				@XmlElement(name = "Nom", required = true)
				protected String nom;
				@XmlElement(name = "Signature")
				protected String signature;
				@XmlElement(name = "Empreinte", required = true)
				protected String empreinte;
				@XmlElement(name = "BlocsChiffrement", required = true)
				protected ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement blocsChiffrement;
				@XmlAttribute
				protected Integer idFichier;
				@XmlAttribute(required = true)
				protected String typeFichier;
				@XmlAttribute
				protected String origineFichier;
				@XmlAttribute
				protected Integer numOrdre;
				@XmlAttribute
				protected Integer tailleEnClair;
				@XmlAttribute
				protected String statutChiffrement;
				@XmlAttribute
				protected Integer nbrBlocsChiffrement;
				@XmlAttribute
				protected String verificationCertificat;
				@XmlAttribute
				protected Boolean hash;

				/**
				 * Gets the value of the hash property.
				 * 
				 * @return possible object is {@link Boolean }
				 */
				public Boolean isHash() {
					return hash;
				}

				/**
				 * Sets the value of the hash property.
				 * 
				 * @param value
				 *            allowed object is {@link Boolean }
				 */
				public void setHash(Boolean value) {
					this.hash = value;
				}

				/**
				 * Gets the value of the nom property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getNom() {
					return nom;
				}

				/**
				 * Sets the value of the nom property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setNom(String value) {
					this.nom = value;
				}

				/**
				 * Gets the value of the signature property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getSignature() {
					return signature;
				}

				/**
				 * Sets the value of the signature property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setSignature(String value) {
					this.signature = value;
				}

				/**
				 * Gets the value of the empreinte property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getEmpreinte() {
					return empreinte;
				}

				/**
				 * Sets the value of the empreinte property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setEmpreinte(String value) {
					this.empreinte = value;
				}

				/**
				 * Gets the value of the blocsChiffrement property.
				 * 
				 * @return possible object is
				 *         {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement }
				 */
				public ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement getBlocsChiffrement() {
					return blocsChiffrement;
				}

				/**
				 * Sets the value of the blocsChiffrement property.
				 * 
				 * @param value
				 *            allowed object is
				 *            {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement }
				 */
				public void setBlocsChiffrement(ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement value) {
					this.blocsChiffrement = value;
				}

				/**
				 * Gets the value of the typeFichier property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getTypeFichier() {
					return typeFichier;
				}

				/**
				 * Sets the value of the origineFichier property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setOrigineFichier(String value) {
					this.origineFichier = value;
				}

				/**
				 * Gets the value of the origineFichier property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getOrigineFichier() {
					return origineFichier;
				}

				/**
				 * Sets the value of the typeFichier property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setTypeFichier(String value) {
					this.typeFichier = value;
				}

				/**
				 * Gets the value of the idFichier property.
				 * 
				 * @return possible object is {@link Integer }
				 */
				public Integer getIdFichier() {
					return idFichier;
				}

				/**
				 * Sets the value of the idFichier property.
				 * 
				 * @param value
				 *            allowed object is {@link Integer }
				 */
				public void setIdFichier(Integer value) {
					this.idFichier = value;
				}

				/**
				 * Gets the value of the numOrdre property.
				 * 
				 * @return possible object is {@link Integer }
				 */
				public Integer getNumOrdre() {
					return numOrdre;
				}

				/**
				 * Sets the value of the numOrdre property.
				 * 
				 * @param value
				 *            allowed object is {@link Integer }
				 */
				public void setNumOrdre(Integer value) {
					this.numOrdre = value;
				}

				/**
				 * Gets the value of the tailleEnClair property.
				 * 
				 * @return possible object is {@link Integer }
				 */
				public Integer getTailleEnClair() {
					return tailleEnClair;
				}

				/**
				 * Sets the value of the tailleEnClair property.
				 * 
				 * @param value
				 *            allowed object is {@link Integer }
				 */
				public void setTailleEnClair(Integer value) {
					this.tailleEnClair = value;
				}

				/**
				 * Gets the value of the statutChiffrement property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getStatutChiffrement() {
					return statutChiffrement;
				}

				/**
				 * Sets the value of the statutChiffrement property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setStatutChiffrement(String value) {
					this.statutChiffrement = value;
				}

				/**
				 * Gets the value of the nbrBlocsChiffrement property.
				 * 
				 * @return possible object is {@link Integer }
				 */
				public Integer getNbrBlocsChiffrement() {
					return nbrBlocsChiffrement;
				}

				/**
				 * Sets the value of the nbrBlocsChiffrement property.
				 * 
				 * @param value
				 *            allowed object is {@link Integer }
				 */
				public void setNbrBlocsChiffrement(Integer value) {
					this.nbrBlocsChiffrement = value;
				}

				/**
				 * Gets the value of the verificationCertificat property.
				 * 
				 * @return possible object is {@link String }
				 */
				public String getVerificationCertificat() {
					return verificationCertificat;
				}

				/**
				 * Sets the value of the verificationCertificat property.
				 * 
				 * @param value
				 *            allowed object is {@link String }
				 */
				public void setVerificationCertificat(String value) {
					this.verificationCertificat = value;
				}

				/**
				 * <p>
				 * Java class for anonymous complex type.
				 * <p/>
				 * <p>
				 * The following schema fragment specifies the expected content
				 * contained within this class.
				 * <p/>
				 * 
				 * <pre>
				 * &lt;complexType>
				 *   &lt;complexContent>
				 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *       &lt;sequence>
				 *         &lt;element name="BlocChiffrement" maxOccurs="unbounded">
				 *           &lt;complexType>
				 *             &lt;complexContent>
				 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
				 *                 &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
				 *                 &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
				 *                 &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
				 *                 &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
				 *                 &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
				 *                 &lt;attribute ref="{}statutChiffrement"/>
				 *               &lt;/restriction>
				 *             &lt;/complexContent>
				 *           &lt;/complexType>
				 *         &lt;/element>
				 *       &lt;/sequence>
				 *       &lt;attribute name="nbrBlocs" type="{http://www.w3.org/2001/XMLSchema}int" />
				 *     &lt;/restriction>
				 *   &lt;/complexContent>
				 * &lt;/complexType>
				 * </pre>
				 */
				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = { "blocChiffrement" })
				public static class BlocsChiffrement {

					@XmlElement(name = "BlocChiffrement", required = true)
					protected List<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement> blocChiffrement;
					@XmlAttribute
					protected Integer nbrBlocs;

					/**
					 * Gets the value of the blocChiffrement property.
					 * <p/>
					 * <p/>
					 * This accessor method returns a reference to the live
					 * list, not a snapshot. Therefore any modification you make
					 * to the returned list will be present inside the JAXB
					 * object. This is why there is not a <CODE>set</CODE>
					 * method for the blocChiffrement property.
					 * <p/>
					 * <p/>
					 * For example, to add a new item, do as follows:
					 * 
					 * <pre>
					 * getBlocChiffrement().add(newItem);
					 * </pre>
					 * <p/>
					 * <p/>
					 * <p/>
					 * Objects of the following type(s) are allowed in the list
					 * {@link ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement }
					 */
					public List<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement> getBlocChiffrement() {
						if (blocChiffrement == null) {
							blocChiffrement = new ArrayList<ReponseAnnonceType.Enveloppes.Enveloppe.Fichier.BlocsChiffrement.BlocChiffrement>();
						}
						return this.blocChiffrement;
					}

					/**
					 * Gets the value of the nbrBlocs property.
					 * 
					 * @return possible object is {@link Integer }
					 */
					public Integer getNbrBlocs() {
						return nbrBlocs;
					}

					/**
					 * Sets the value of the nbrBlocs property.
					 * 
					 * @param value
					 *            allowed object is {@link Integer }
					 */
					public void setNbrBlocs(Integer value) {
						this.nbrBlocs = value;
					}

					/**
					 * <p>
					 * Java class for anonymous complex type.
					 * <p/>
					 * <p>
					 * The following schema fragment specifies the expected
					 * content contained within this class.
					 * <p/>
					 * 
					 * <pre>
					 * &lt;complexType>
					 *   &lt;complexContent>
					 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
					 *       &lt;attribute name="tailleEnClair" type="{http://www.w3.org/2001/XMLSchema}int" />
					 *       &lt;attribute name="tailleApresChiffrement" type="{http://www.w3.org/2001/XMLSchema}int" />
					 *       &lt;attribute name="numOrdre" type="{http://www.w3.org/2001/XMLSchema}int" />
					 *       &lt;attribute name="empreinte" type="{http://www.w3.org/2001/XMLSchema}string" />
					 *       &lt;attribute name="nomBloc" type="{http://www.w3.org/2001/XMLSchema}string" />
					 *       &lt;attribute ref="{}statutChiffrement"/>
					 *     &lt;/restriction>
					 *   &lt;/complexContent>
					 * &lt;/complexType>
					 * </pre>
					 */
					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "")
					public static class BlocChiffrement {

						@XmlAttribute
						protected Integer tailleEnClair;
						@XmlAttribute
						protected Integer tailleApresChiffrement;
						@XmlAttribute
						protected Integer numOrdre;
						@XmlAttribute
						protected String empreinte;
						@XmlAttribute
						protected String nomBloc;
						@XmlAttribute
						protected String statutChiffrement;

						/**
						 * Gets the value of the tailleEnClair property.
						 * 
						 * @return possible object is {@link Integer }
						 */
						public Integer getTailleEnClair() {
							return tailleEnClair;
						}

						/**
						 * Sets the value of the tailleEnClair property.
						 * 
						 * @param value
						 *            allowed object is {@link Integer }
						 */
						public void setTailleEnClair(Integer value) {
							this.tailleEnClair = value;
						}

						/**
						 * Gets the value of the tailleApresChiffrement
						 * property.
						 * 
						 * @return possible object is {@link Integer }
						 */
						public Integer getTailleApresChiffrement() {
							return tailleApresChiffrement;
						}

						/**
						 * Sets the value of the tailleApresChiffrement
						 * property.
						 * 
						 * @param value
						 *            allowed object is {@link Integer }
						 */
						public void setTailleApresChiffrement(Integer value) {
							this.tailleApresChiffrement = value;
						}

						/**
						 * Gets the value of the numOrdre property.
						 * 
						 * @return possible object is {@link Integer }
						 */
						public Integer getNumOrdre() {
							return numOrdre;
						}

						/**
						 * Sets the value of the numOrdre property.
						 * 
						 * @param value
						 *            allowed object is {@link Integer }
						 */
						public void setNumOrdre(Integer value) {
							this.numOrdre = value;
						}

						/**
						 * Gets the value of the empreinte property.
						 * 
						 * @return possible object is {@link String }
						 */
						public String getEmpreinte() {
							return empreinte;
						}

						/**
						 * Sets the value of the empreinte property.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 */
						public void setEmpreinte(String value) {
							this.empreinte = value;
						}

						/**
						 * Gets the value of the nomBloc property.
						 * 
						 * @return possible object is {@link String }
						 */
						public String getNomBloc() {
							return nomBloc;
						}

						/**
						 * Sets the value of the nomBloc property.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 */
						public void setNomBloc(String value) {
							this.nomBloc = value;
						}

						/**
						 * Gets the value of the statutChiffrement property.
						 * 
						 * @return possible object is {@link String }
						 */
						public String getStatutChiffrement() {
							return statutChiffrement;
						}

						/**
						 * Sets the value of the statutChiffrement property.
						 * 
						 * @param value
						 *            allowed object is {@link String }
						 */
						public void setStatutChiffrement(String value) {
							this.statutChiffrement = value;
						}

					}

				}

			}

		}

	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * <p/>
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * <p/>
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Nom" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "nom" })
	public static class OperateurEconomique {

		@XmlElement(name = "Nom", required = true)
		protected String nom;

		/**
		 * Gets the value of the nom property.
		 * 
		 * @return possible object is {@link String }
		 */
		public String getNom() {
			return nom;
		}

		/**
		 * Sets the value of the nom property.
		 * 
		 * @param value
		 *            allowed object is {@link String }
		 */
		public void setNom(String value) {
			this.nom = value;
		}

	}

}
