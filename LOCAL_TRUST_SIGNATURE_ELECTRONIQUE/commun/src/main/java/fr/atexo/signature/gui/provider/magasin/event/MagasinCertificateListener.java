package fr.atexo.signature.gui.provider.magasin.event;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;

import java.util.EventListener;


/**
 * Listener permettant de récupérer le Certificat sélectionné par l'utilisateur
 * depuis son magasin de certificats.
 */
public interface MagasinCertificateListener extends EventListener {

    /**
     * Méthode appelé lors de la selection d'un certificat par l'utilisateur
     * depuis le magasin de certificats.
     *
     * @param event l'evenement contenant le certificat sélectionné
     */
    public void onSelection(MagasinCertificateEvent event) throws ManipulationCertificatException;
}

