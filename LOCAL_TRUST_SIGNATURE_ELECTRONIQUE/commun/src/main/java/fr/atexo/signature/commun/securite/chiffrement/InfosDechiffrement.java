package fr.atexo.signature.commun.securite.chiffrement;

import fr.atexo.signature.commun.securite.certificat.KeyPair;

/**
 *
 */
public class InfosDechiffrement {

    private KeyPair keyPair;

    private ClefIV clefIV;

    public InfosDechiffrement(KeyPair keyPair, ClefIV clefIV) {
        this.keyPair = keyPair;
        this.clefIV = clefIV;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public ClefIV getClefIV() {
        return clefIV;
    }
}
