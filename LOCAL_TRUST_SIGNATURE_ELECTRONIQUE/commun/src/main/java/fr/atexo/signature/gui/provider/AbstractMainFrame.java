package fr.atexo.signature.gui.provider;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateEvent;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateListener;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateEvent;
import fr.atexo.signature.gui.provider.pkcs12.event.Pkcs12CertificateListener;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.util.EventListener;
import java.util.EventObject;

/**
 * Classe abstraite permettant de déclencher l'événement de sélection du certificat
 * lors de la validation du formulaire par l'utilisateur. Permet aussi d'enregistrer les
 * évenement qui devront être lancé lors de l'action de validation.
 */
public abstract class AbstractMainFrame<E extends EventObject> extends JFrame {

    private EventListenerList listeners = new EventListenerList();

    public EventListenerList getListeners() {
        return listeners;
    }

    public void addListener(EventListener listener) {
        this.listeners.add(EventListener.class, listener);
    }

    public void removeListener(EventListener listener) {
        this.listeners.remove(EventListener.class, listener);
    }

    /**
     * Déclenche l'appel à la fonction onSelect du listener associé
     * à l'action de validation de la frame.
     *
     * @param event
     */
    protected void fireCertificateEvent(E event) {
        Object[] listeners = getListeners().getListenerList();
        for (Object listener : listeners) {
            try {
                if (listener instanceof MagasinCertificateListener && event instanceof MagasinCertificateEvent) {
                    ((MagasinCertificateListener) listener).onSelection((MagasinCertificateEvent) event);
                } else if (listener instanceof Pkcs12CertificateListener && event instanceof Pkcs12CertificateEvent) {
                    ((Pkcs12CertificateListener) listener).onSelection((Pkcs12CertificateEvent) event);
                }
            } catch (ManipulationCertificatException e) {
                e.printStackTrace();
            }
        }
    }

}
