package fr.atexo.signature.commun.securite.processor.signature;

import fr.atexo.signature.commun.exception.execution.SignatureExecutionException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;

/**
 * Interface exposant les méthodes permettant de signer les fichiers pdf au format Pades.
 */
public interface SignaturePadesProcessor {

    /**
     * Execute la signature Pades du fichier pdf.
     *
     * @param keyPair   le certificat avec lequel signer le fichier
     * @param conteneur le fichier à signer.
     * @return <code>true</code> si valide, sinon <code>false</code>
     * @throws fr.atexo.signature.commun.exception.execution.SignatureExecutionException
     *
     */
    boolean signerEnPades(KeyPair keyPair, ConteneurFichierSignatureXML conteneur) throws SignatureExecutionException;
}
