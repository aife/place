package fr.atexo.signature.commun.securite.provider.pkcs12;

import fr.atexo.signature.commun.exception.certificat.RecuperationCertificatException;
import fr.atexo.signature.commun.exception.certificat.RecuperationClePriveCertificatException;
import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.provider.AbstractKeyStoreHandler;
import fr.atexo.signature.commun.securite.provider.TypeProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Enumeration;

public class Pkcs12Handler extends AbstractKeyStoreHandler {

    /**
     * Retourne un bi-clé à partir du fichier p12 et de son mot de passe
     * d'accès.
     *
     * @param cheminFichierP12 le chemin vers le fichier p12
     * @param motDePasse       le mot de passe pour pouvoir accèder au contenu du fichier p12
     * @return le bit clé.
     * @throws RecuperationCertificatException
     *
     */
    public static KeyPair getKeyPair(String cheminFichierP12, String motDePasse) throws RecuperationCertificatException {

        File fichierP12 = new File(cheminFichierP12);
        FileInputStream fichierP12InputStrean = null;

        try {
            if (fichierP12.exists()) {
                fichierP12InputStrean = new FileInputStream(fichierP12);

                KeyStore keyStore = getKeyStore(TypeProvider.PKCS12);
                keyStore.load(fichierP12InputStrean, motDePasse.toCharArray());

                // recherche de la clé privée
                RSAPrivateKey privateKey = (RSAPrivateKey) getPrivateKey(keyStore.aliases(), keyStore, motDePasse);
                // recherche du certificat
                X509Certificate certificat = getCertificate(privateKey.getModulus(), keyStore.aliases(), keyStore);
                // création du bit-clé
                KeyPair keyPair = new KeyPair(certificat, privateKey, TypeProvider.PKCS12);


                return keyPair;
            }

        } catch (CertificateException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement dans le key store du fichier p12 : " + cheminFichierP12, e);
        } catch (NoSuchAlgorithmException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement dans le key store du fichier p12 " + cheminFichierP12, e);
        } catch (KeyStoreException e) {
            throw new RecuperationCertificatException("Erreur lors de la récupération des alias du fichier p12 " + cheminFichierP12, e);
        } catch (FileNotFoundException e) {
            throw new RecuperationCertificatException("Erreur lors de l'accès au fichier p12 " + cheminFichierP12, e);
        } catch (IOException e) {
            throw new RecuperationCertificatException("Erreur lors du chargement dans le key store du fichier p12 " + cheminFichierP12, e);
        } finally {
            try {
                fichierP12InputStrean.close();
            } catch (IOException e) {
            }
        }

        return null;
    }

    /**
     * Retourne la cle privée se trouve dans le certificat.
     *
     * @param aliases    la liste des alias
     * @param keyStore   le key store dans lequel a été chargé le certificat
     * @param motDePasse le mot de passe pour accéder au certificat dans le key store
     * @return la cle privée assoicée au certificat
     * @throws RecuperationClePriveCertificatException
     *
     */
    private static PrivateKey getPrivateKey(Enumeration<String> aliases, KeyStore keyStore, String motDePasse) throws RecuperationClePriveCertificatException {
        try {
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                if (alias != null && (keyStore.isKeyEntry(alias))) {
                    // on considère qu'il ne peut y avoir qu'une clé privée par fichier PKCS12
                    PrivateKey key = (PrivateKey) keyStore.getKey(alias, motDePasse.toCharArray());
                    if (key != null) {
                        return key;
                    }
                }
            }
        } catch (Exception e) {
            throw new RecuperationClePriveCertificatException("Impossible d'accéder à la clé privée", e);
        }
        return null;
    }

    /**
     * Retourne le certificat.
     *
     * @param modulus Modulus de la cle privee correspondant au certificat.
     * @param aliases alias du keystore.
     * @throws RecuperationCertificatException
     *
     */
    private static X509Certificate getCertificate(BigInteger modulus, Enumeration<String> aliases, KeyStore keyStore) throws RecuperationCertificatException {
        try {
            while (aliases.hasMoreElements()) {
                // on récupère le certificat qui correspond à la clé privée mais
                // aussi au numéro de série et à l'issuer du certificat passé en
                // paramêtres
                String alias = aliases.nextElement();
                X509Certificate signingCertificate = (X509Certificate) keyStore.getCertificate(alias);

                if (((RSAPublicKey) signingCertificate.getPublicKey()).getModulus().equals(modulus)) {
                    return signingCertificate;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RecuperationCertificatException("Impossible de trouver le certificat", e);
        }
        return null;
    }

}
