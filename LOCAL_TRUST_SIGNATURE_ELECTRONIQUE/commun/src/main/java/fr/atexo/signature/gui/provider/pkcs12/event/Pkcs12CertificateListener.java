package fr.atexo.signature.gui.provider.pkcs12.event;

import fr.atexo.signature.commun.exception.certificat.ManipulationCertificatException;

import java.util.EventListener;

/**
 * Listener permettant de récupérer le Certificat PKCS12 sélectionné par l'utilisateur
 * depuis son disque dur.
 */
public interface Pkcs12CertificateListener extends EventListener {

    /**
     * Méthode appelé lors de la selection d'un certificat PKCS12 par l'utilisateur
     * depuis son disque dur.
     *
     * @param event l'evenement contenant les informations du certificat sélectionné
     */
    public void onSelection(Pkcs12CertificateEvent event) throws ManipulationCertificatException;

}


