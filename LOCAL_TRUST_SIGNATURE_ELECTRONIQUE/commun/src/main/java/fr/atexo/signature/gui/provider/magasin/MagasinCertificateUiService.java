package fr.atexo.signature.gui.provider.magasin;

import fr.atexo.signature.commun.securite.provider.CertificatItem;
import fr.atexo.signature.commun.securite.provider.TypeOs;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.magasin.MagasinHandler;
import fr.atexo.signature.commun.securite.provider.pkcs11.Pkcs11Handler;
import fr.atexo.signature.commun.util.CertificatUtil;
import fr.atexo.signature.gui.provider.CertificateSelectUiService;
import fr.atexo.signature.gui.provider.magasin.event.MagasinCertificateListener;
import fr.atexo.signature.gui.provider.magasin.ui.MainFrame;
import fr.atexo.signature.logging.LogManager;
import fr.atexo.signature.util.SmartCardUtil;
import fr.atexo.signature.xml.pkcs11.Pkcs11LibsType;

import java.io.File;
import java.util.*;

/**
 * Singleton permettant d'initialiser l'interfage graphique de l'utilisateur ainsi que d'associer le listener
 * qui sera lancé lors de la sélection du certificat depuis le magasin de certificats par l'utilisateur.
 */
public class MagasinCertificateUiService implements CertificateSelectUiService<MagasinCertificateListener> {

    private static MagasinCertificateUiService uiService;

    private static Pkcs11LibsType pkcs11Libs;

    public static MagasinCertificateUiService getInstance(Pkcs11LibsType pkcs11Libs) {
        if (uiService == null) {
            uiService = new MagasinCertificateUiService();
        }
        MagasinCertificateUiService.pkcs11Libs = pkcs11Libs;

        return uiService;
    }

    @Override
    public void initUi(MagasinCertificateListener listener, TypeOs typeOs, TypeProvider typeProvider, boolean filtrerSignatureModeRGS, CertificatUtil.TypeCertificat... typeCertificat) {

        List<CertificatItem> certificatItems = new ArrayList<CertificatItem>();
        try {
            Set<String> hashCodes = new HashSet<String>();
            // dans le cas de mac os ou linux, on charge en premier les certificats se trouvant sur clé usb
            if (pkcs11Libs != null) {

                if (pkcs11Libs != null && typeOs != TypeOs.Windows) {
                    Map<String, List<File>> providers = SmartCardUtil.getPkcs11Providers(pkcs11Libs, typeOs);
                    if (providers != null && providers.size() > 0) {
                        List<CertificatItem> certificatsPkcs11 = Pkcs11Handler.getInstance().recupererCertificats(providers, hashCodes, filtrerSignatureModeRGS, typeCertificat);
                        certificatItems.addAll(certificatsPkcs11);
                    } else {
                        LogManager.getInstance().afficherMessageWarning("Aucune librairie pkcs11 n'a été trouvé au niveau de l'installation du poste client => pas de dialogue avec de potentiel smartcard", this.getClass());
                    }
                }

            } else {
                LogManager.getInstance().afficherMessageWarning("Aucune librairie téléchargeable vu que l'url de référence est injoignable ou incorrect", this.getClass());
            }

            if (typeOs == TypeOs.Windows || typeOs == TypeOs.MacOs) {
                List<CertificatItem> certificatsMagasin = MagasinHandler.getInstance().recupererCertificats(typeProvider, hashCodes, filtrerSignatureModeRGS, typeCertificat);
                certificatItems.addAll(certificatsMagasin);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        MainFrame mainFrame = new MainFrame(certificatItems);
        mainFrame.addListener(listener);
    }
}
