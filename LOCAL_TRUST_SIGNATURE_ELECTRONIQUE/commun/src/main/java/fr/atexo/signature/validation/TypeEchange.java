package fr.atexo.signature.validation;

/**
 *
 */
public enum TypeEchange {

    EnrichissementSignature, VerificationSignatureClient, VerificationSignatureServeur, RetourDateServeur;
}
