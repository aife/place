
package fr.atexo.signature.xml.pkcs11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pkcs11LibsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="pkcs11LibsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pkcs11Lib" type="{}pkcs11LibType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pkcs11LibsType", propOrder = {
    "pkcs11Lib"
})
public class Pkcs11LibsType {

    @XmlElement(required = true)
    protected List<Pkcs11LibType> pkcs11Lib;

    /**
     * Gets the value of the pkcs11Lib property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pkcs11Lib property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPkcs11Lib().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Pkcs11LibType }
     *
     *
     */
    public List<Pkcs11LibType> getPkcs11Lib() {
        if (pkcs11Lib == null) {
            pkcs11Lib = new ArrayList<Pkcs11LibType>();
        }
        return this.pkcs11Lib;
    }

}
