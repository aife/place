package fr.atexo.signature.commun.util;


import fr.atexo.signature.commun.securite.certificat.KeyPair;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;
import fr.atexo.signature.commun.securite.provider.TypeProvider;
import fr.atexo.signature.commun.securite.provider.bouncycastle.BouncyCaslteHandler;
import net.iharder.Base64;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;

/**
 * Classe d'utilitaire d'extraction / manipulation des informations d'un
 * certificat.
 */
public abstract class CertificatUtil {

    public static final String CHAINE_CERTIFICAT_BASE64 = "MIIFYjCCA0qgAwIBAgIBATANBgkqhkiG9w0BAQsFADBrMQswCQYDVQQGEwJGUjEOMAwGA1UEBxMFUEFSSVMxDjAMBgNVBAoTBUFURVhPMRYwFAYDVQQLEw1BVEVYTyBBQyBERU1PMSQwIgYDVQQDExtBVEVYTyBBQyBERU1PIC0gRW50cmVwcmlzZXMwHhcNMTMwNDE3MTkxNzU3WhcNMTYwNDE3MTkxNzU3WjCBlDELMAkGA1UEBhMCRlIxDjAMBgNVBAoTBUFURVhPMSYwJAYDVQQLEx1NQVJDSEVTIFBVQkxJQ1MgRUxFQ1RST05JUVVFUzEXMBUGA1UEAxMOQ2hhcmxlcyBDSVZFVEUxFzAVBgNVBCkTDkNoYXJsZXMgQ0lWRVRFMRswGQYJKoZIhvcNAQkBFgxjY0BhdGV4by5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDovtQHb+g22rU1S+BIdnfIdDF+XZgV7RdRMPzBjTFqQFxWVCa9lZx/OWLqBnUgB9J/9CzYPW5bpG55UUlkCbe66iFGIdLCMKIk4WOlWmxz4Zq0Pphr/Np4rS0UKLFh170r01ezmlRvlFesmYDI/XIVnjo8v+VGdoyiSzgqo2WgtonklTGkfePpUGllMMAp3v0rLJOe55bWFTnXSl85ndzZyIr4XL6VA1y6ev/LCxqYPzZKFMgbRZB13Nb6CDRF8hJWa0hEA2w3t6xvLYZ+4oKu8A5+WXEIo35baWaAPgxH9H5GbOmWnCxzs7+0luV5GdejEWgV+xpL2+rMbMzngjzjAgMBAAGjgeYwgeMwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBsAwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMB0GA1UdDgQWBBSD4uxZAuEc8eAIhQEWcwZNdujmQjAfBgNVHSMEGDAWgBSNms8QxETmT9eP+6SW/mqMyrFsZTBOBgNVHR8ERzBFMEOgQaA/hj1odHRwczovL3BraS5sb2NhbC10cnVzdC5jb20vY3JsL0FURVhPX0FDX0RFTU8tRW50cmVwcmlzZXMuY3JsMBcGA1UdEQQQMA6BDGNjQGF0ZXhvLmNvbTANBgkqhkiG9w0BAQsFAAOCAgEAj8KSlFQ37eDMNxfTmyjG+845i3ucT4dua6HvW3jJWHCwzqCB6dV/0zMzGN5CabE8qnBp1Sv8E3N+JH0VzCG1uPHtfQ1un3ydGYf7c/+hUmLGq0GRc/dDIqjS8VocHQ71GM60YrS0mp0brXJMbUr4TxU0elV760Uvr0l2a3Mu8yZ8x/NwSEoLTPZiTWCwwuA2l2Qk2e4XeGKpyRsYLZmXkYdSGveag/9Y0/gAfAoApLozYhbCaSIZuyPKLjHQa1LfuGL881xxsNlVx4d6JckM2k1Krzs2PaShAMNwONJgUA1aJdGIoDmjYuO9oAiAL1bvZEj3YHXUvHFiTlKP/87kCinS6EHulWq7mEZdMzqcSJUJGpR09Tr51NdwnQI97Pa+ASoPdGeFOjENyFzYLD9XbOqy523/Ok0tlVoBhjIjaokg2Ov60QYMShO8ynDukX0h3iqx3MYu6RpTGaIJBZw75oLHshrrP4Ojzt5bNjRpPw/GKrAbpxO6IpjgiJXVRdtc6hfqOZxAgWGJ5wL6LgAk+fGvC8X5PmsEDhtGuCYxj3FjxL+OG7pSS6/kz2678asbcC8U+qwfAqdJWtdj1s7eBHbgw6Qn9wpX4UPJS8t85DBVvU4z+p7dTToHbghqic/zm7z7ymWshFqlLeTb1jFtjTRXat2TRfhfGANBN78H8pI=";
    private static final String CHAINE_CLEF_PRIVEE_BASE64 = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDovtQHb+g22rU1S+BIdnfIdDF+XZgV7RdRMPzBjTFqQFxWVCa9lZx/OWLqBnUgB9J/9CzYPW5bpG55UUlkCbe66iFGIdLCMKIk4WOlWmxz4Zq0Pphr/Np4rS0UKLFh170r01ezmlRvlFesmYDI/XIVnjo8v+VGdoyiSzgqo2WgtonklTGkfePpUGllMMAp3v0rLJOe55bWFTnXSl85ndzZyIr4XL6VA1y6ev/LCxqYPzZKFMgbRZB13Nb6CDRF8hJWa0hEA2w3t6xvLYZ+4oKu8A5+WXEIo35baWaAPgxH9H5GbOmWnCxzs7+0luV5GdejEWgV+xpL2+rMbMzngjzjAgMBAAECggEAcX91eDb7P5zB1z6sHcofuZHn/N55Zt/aig5gg6Zt3YmLPdIFnlgSG/yJHuSNQ1RtM1aIc97pLSlvchvQtUcD4NOB7GhcFbSPrXp4FE+XKZ9vyMvpmmeQxl506Cq77aG+L1v9naj52fu2EYY9xkXJ1370mWFe1lDDXfVea3SI/6h2Rcrhd63eoF7Wz7xYTPNdWmRGFXW2kt/FTWteETlXihVYHEVKyiOqcrklVVrv2n4gqXwkHLMXUlpCP5/Cwb7CRv3L0sFTUuG5Hk+kPgA/XHtPLRjY0VVsbyDmDOB/hiilYGq+epAbMZJqISAKunTDGNEScK2NEM7cEu6rHbk0eQKBgQD7HMzHcYTwQ2zuXHaOopPxbJ60xZs0u7/f9hXiL2MYnqa06hmW+WVj+cuY0oPdimIiyaGOJPpIvd877wHSeDZYcHCTZRYlYvDi/Y56Y6uVY3ovkENxaHo1cCCnyeC7uY7cudoe4eIThPfOrtpVxKAgmtyzhVONVECwxPmG/LZTPwKBgQDtRoMTb3qzFVb0gVAEY2bfAD/UqXJaAf7Wvf6bhbRZI2w3yLS7VGb149A8YFCps7Ez5ftU/3z70WL0ZgNwcT4gA+OZMODwtAGS/2/YrN6H4QXPWtVErj/X9kFsEoCOfo0kMi026gXFtWAzzLXLOaqKhbu9g7fON0fRFpuWoB1BXQKBgQDkSKNVsTq0nedaro5NTzmFokSJfJeDkid1+Cae0IubJyfQkn2fBa0J/V382Wxtq89ZZspLCgzKsmpN9xNF/mkRb264YI6IgHETOoUbeJ7VGXFL6i+V2vA2wm+9ecangqKAwSitKJa5Pgl2SPlOPYmA+qgnP87cTbrduMRL1lP+SwKBgFZmLqSNG2jKhHdT/wcaCwN428VcTj9oKpuXY3bOIPW0aFuHwgfUnAk9gz349GZXAFedjv9MaP5pKFdGLkprcevGfsHxgncjlM8qJFgJ2p4v2iW/NZlkueqxyPDJa/Z3Ln3Xp48veBqeCQHTsP7naKo4ODyyEeQNTSFd0hnvA7ghAoGBAIRyhMMXLLuqjvPcDnXsN0BsCb+2NJ/ILcY19kVhYe+R8U8zYr+PVI33reqTSGdlMHKhzhZZKK99hLwhpLpZssJ+FoJOkTktzS/GMQeaGI6Dj4hGtKs2qI37cWjjduMTlZgP2zgWNaT0FJIngUxiaL1ssrCTjlOG+tvIzkk3DmWC";

    public enum TypeCertificat {
        SignatureElectronique, Chiffrement, Authentification;
    }

    /**
     * Récupére l'attribut CN d'un certificat
     *
     * @param principal le certificat
     * @return la valeur du CN
     */
    public static String getCN(Principal principal) {

        if (principal != null) {

            String[] principalDecompose = principal.toString().split(",");

            for (String valeur : principalDecompose) {
                String valeurTrim = valeur.trim();
                if (valeurTrim.startsWith("CN=")) {
                    return valeurTrim.substring(3);
                }
            }
        }

        return null;
    }

    /**
     * Cette fonction permet de récupérer un certificat depuis une chaine de caractére encode en base 64.
     *
     * @param chaineDeCertificatEnBase64 la chaine correspondant au certificat encodé en base 64.
     * @return X509Certificate le certificat X509.
     */
    public static X509Certificate getX509Certificate(String chaineDeCertificatEnBase64) throws IOException, CertificateException {

        if (chaineDeCertificatEnBase64 == null) {
            return null;
        }
        X509Certificate certificate = null;
        InputStream inputStream = null;
        CertificateFactory certificateFactory = null;

        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("-----BEGIN CERTIFICATE-----", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("-----END CERTIFICATE-----", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("\n", "");
        chaineDeCertificatEnBase64 = chaineDeCertificatEnBase64.replaceAll("\r", "");

        byte[] chaineDeCertificatDecode = Base64.decode(chaineDeCertificatEnBase64);
        inputStream = new ByteArrayInputStream(chaineDeCertificatDecode);
        certificateFactory = CertificateFactory.getInstance("X.509");
        certificate = (X509Certificate) certificateFactory.generateCertificate(inputStream);
        inputStream.close();

        return certificate;
    }

    public static String reconstuireChaineDeCertificat(X509Certificate certificat) throws CertificateEncodingException {

        String issuer = getCN(certificat.getIssuerX500Principal()).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String date = Util.formaterDate(certificat.getNotAfter(), Util.DATE_TIME_PATTERN_TIRET).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String subject = getCN(certificat.getSubjectX500Principal()).replaceAll(":", "_").replaceAll(" ", "_").replaceAll("-", "_");
        String certificatEnBase64 = Base64.encodeBytes(certificat.getEncoded());
        String chaineCertificat = issuer + "_" + date + "_" + subject + "|" + certificatEnBase64 + "##";

        return chaineCertificat;

    }

    /**
     * Vérifie si le certificat peut être utilisé pour de l'authentification.
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de l'authentification, sinon false.
     */
    public static boolean isUtilisablePourAuthentification(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && x509Certificate.getKeyUsage()[0];
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de la signature électronique.
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de la signature électronique, sinon false.
     */
    public static boolean isUtilisablePourSignatureElectronique(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && x509Certificate.getKeyUsage()[1];
    }

    /**
     * Vérifie si le certificat peut être utilisé pour de le chiffrement
     *
     * @param x509Certificate le certificat à vérifier.
     * @return true si le certificat peut être utilisé pour de le chiffrement, sinon false.
     */
    public static boolean isUtilisablePourChiffrement(X509Certificate x509Certificate) {
        return x509Certificate.getKeyUsage() != null && (x509Certificate.getKeyUsage()[2] || x509Certificate.getKeyUsage()[3] || x509Certificate.getKeyUsage()[4]);

    }

    public static String getUtilisablePour(X509Certificate x509Certificate) {
        // on détermine à quel usage peut être utilisé le certificat
        String utilisationPossible = "";
        if (isUtilisablePourSignatureElectronique(x509Certificate)) {
            utilisationPossible += I18nUtil.get("SIGNATURE_NUMERIQUE") + ", ";
        }
        if (isUtilisablePourChiffrement(x509Certificate)) {
            utilisationPossible += I18nUtil.get("CHIFFREMENT") + ", ";
        }
        if (isUtilisablePourAuthentification(x509Certificate)) {
            utilisationPossible += I18nUtil.get("AUTHENTIFICATION") + ", ";
        }

        if (!utilisationPossible.equals("")) {
            utilisationPossible = utilisationPossible.substring(0, utilisationPossible.length() - 2);
        } else {
            utilisationPossible = null;
        }

        return utilisationPossible;
    }

    public static KeyPair getKeyPairTestChiffrement() throws IOException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeySpecException {

        BouncyCaslteHandler.verifierPresenceEtRecupererProvider();

        X509Certificate certificat = getX509Certificate(CHAINE_CERTIFICAT_BASE64);
        byte[] clefPriveeDecode = Base64.decode(CHAINE_CLEF_PRIVEE_BASE64);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA", TypeProvider.BC.getType());
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(clefPriveeDecode);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        KeyPair keyPair = new KeyPair(certificat, privateKey, TypeProvider.BC);

        return keyPair;
    }

    public static InfosComplementairesCertificat extraireInformations(X509Certificate certificat, InfosComplementairesCertificat infosComplementairesCertificat) {

        // propriétaire du certificat
        X500Principal proprietaireCertificat = certificat.getSubjectX500Principal();
        infosComplementairesCertificat.setSignatairePartiel(getCN(proprietaireCertificat));
        infosComplementairesCertificat.setSignataireComplet(proprietaireCertificat.toString());

        // issuer du certificat
        X500Principal issuerCertificat = certificat.getIssuerX500Principal();
        infosComplementairesCertificat.setEmetteur(issuerCertificat.toString());

        // periodicité du certificat
        String dateValiditeDu = Util.creerISO8601DateTime(certificat.getNotBefore());
        infosComplementairesCertificat.setDateValiditeDu(dateValiditeDu);
        String dateValiditeAu = Util.creerISO8601DateTime(certificat.getNotAfter());
        infosComplementairesCertificat.setDateValiditeAu(dateValiditeAu);

        boolean datePeriodiciteValide = Util.isDateCompriseEntre(new Date(), certificat.getNotBefore(), certificat.getNotAfter());
        infosComplementairesCertificat.setPeriodiciteValide(datePeriodiciteValide);

        return infosComplementairesCertificat;
    }
}
