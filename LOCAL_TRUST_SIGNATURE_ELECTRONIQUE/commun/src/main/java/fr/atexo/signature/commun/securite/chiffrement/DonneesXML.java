package fr.atexo.signature.commun.securite.chiffrement;

import java.util.List;

/**
 * Classe encapsulant les données utilisés pour effectuer le déchiffrement.
 */
public class DonneesXML {

    private byte[] donnees;

    private int taille;

    private List<ClefIV> clefIVs;

    public DonneesXML(byte[] donnees, int taille, List<ClefIV> clefIVs) {
        this.donnees = donnees;
        this.taille = taille;
        this.clefIVs = clefIVs;
    }

    public byte[] getDonnees() {
        return donnees;
    }

    public int getTaille() {
        return taille;
    }

    public List<ClefIV> getClefIVs() {
        return clefIVs;
    }
}
