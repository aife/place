package fr.atexo.signature.commun.exception.execution;

import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * Exception générique pour signaler un souci lors de l'execution.
 */
public class ExecutionException extends Exception {

    public ExecutionException(String message) {
        super(message);
    }

    public ExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
