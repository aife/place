package fr.atexo.signature.commun.securite.provider;

/**
 * Le type d'os
 */
public enum TypeOs {
    Windows, MacOs, Linux, Indetermine;
}
