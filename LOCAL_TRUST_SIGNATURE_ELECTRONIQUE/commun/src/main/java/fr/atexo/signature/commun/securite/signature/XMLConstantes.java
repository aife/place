package fr.atexo.signature.commun.securite.signature;

/**
 *
 */
public class XMLConstantes {

    public static final String DSIG_PREFIX = "ds";
    public static final String XADES_PREFIX = "xades";

    public static final String SIGNATURE_TAG_ID = "signature";    
    public static final String SIGNED_PROPS_TAG_ID = "signedProps";
    public static final String SIGNED_INFOS_TAG_ID = "signedInfos";
    public static final String OBJECT_TAG_ID = "object";
    public static final String XMLNS = "http://www.w3.org/2000/xmlns/";
    public static final String XMLDSIG = "xmldsig";
    public static final String RFC1779 = "RFC1779";

    // partie code de retour pour la validation
    final public static int VALIDATION_OK = 0;
    public static final int ERR_SIG_FORMAT = 1;    
    final public static int VALIDATION_NOK = 2;
    final public static int UNSIGNED_DOCUMENT = 4;
    final public static int VALIDATION_SIGNED_HASH_NOK = 5;
    final public static int VALIDATION_SIGNED_PROPERTIES_NOK = 6;


}
