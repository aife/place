package fr.atexo.signature.commun.securite.processor.signature;

import fr.atexo.json.reponse.Repertoire;

import java.util.Set;

/**
 *
 */
public class InfosVerificationCertificat implements InfosComplementairesCertificat {

    private int etat;

    private String signatairePartiel;

    private String signataireComplet;

    private String emetteur;

    private String dateValiditeDu;

    private String dateValiditeAu;

    private Boolean periodiciteValide;

    private Boolean chaineDeCertificationValide;

    private Boolean absenceRevocationCRL;

    private Boolean dateSignatureValide;

    private Boolean signatureValide;

    private String signatureXades;

    private Set<Repertoire> repertoiresChaineCertification;

    private Set<Repertoire> repertoiresRevocation;       

    public InfosVerificationCertificat() {
    }

    public String getSignataireComplet() {
        return signataireComplet;
    }

    public void setSignataireComplet(String signataireComplet) {
        this.signataireComplet = signataireComplet;
    }

    public String getSignatairePartiel() {
        return signatairePartiel;
    }

    public void setSignatairePartiel(String signatairePartiel) {
        this.signatairePartiel = signatairePartiel;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public String getDateValiditeDu() {
        return dateValiditeDu;
    }

    public void setDateValiditeDu(String dateValiditeDu) {
        this.dateValiditeDu = dateValiditeDu;
    }

    public String getDateValiditeAu() {
        return dateValiditeAu;
    }

    public void setDateValiditeAu(String dateValiditeAu) {
        this.dateValiditeAu = dateValiditeAu;
    }

    public Boolean getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(Boolean dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }

    public Boolean getPeriodiciteValide() {
        return periodiciteValide;
    }

    public void setPeriodiciteValide(Boolean periodiciteValide) {
        this.periodiciteValide = periodiciteValide;
    }

    public Boolean getChaineDeCertificationValide() {
        return chaineDeCertificationValide;
    }

    public void setChaineDeCertificationValide(Boolean chaineDeCertificationValide) {
        this.chaineDeCertificationValide = chaineDeCertificationValide;
    }

    public Boolean getAbsenceRevocationCRL() {
        return absenceRevocationCRL;
    }

    public void setAbsenceRevocationCRL(Boolean absenceRevocationCRL) {
        this.absenceRevocationCRL = absenceRevocationCRL;
    }

    public Boolean getSignatureValide() {
        return signatureValide;
    }

    public void setSignatureValide(Boolean signatureValide) {
        this.signatureValide = signatureValide;
    }

    public Set<Repertoire> getRepertoiresChaineCertification() {
        return repertoiresChaineCertification;
    }

    public void setRepertoiresChaineCertification(Set<Repertoire> repertoiresChaineCertification) {
        this.repertoiresChaineCertification = repertoiresChaineCertification;
    }

    public Set<Repertoire> getRepertoiresRevocation() {
        return repertoiresRevocation;
    }

    public void setRepertoiresRevocation(Set<Repertoire> repertoiresRevocation) {
        this.repertoiresRevocation = repertoiresRevocation;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getSignatureXades() {
        return signatureXades;
    }

    public void setSignatureXades(String signatureXades) {
        this.signatureXades = signatureXades;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("InfosVerificationCertificat");
        sb.append("{etat=").append(etat);
        sb.append(", signatairePartiel='").append(signatairePartiel).append('\'');
        sb.append(", signataireComplet='").append(signataireComplet).append('\'');
        sb.append(", emetteur='").append(emetteur).append('\'');
        sb.append(", dateValiditeDu='").append(dateValiditeDu).append('\'');
        sb.append(", dateValiditeAu='").append(dateValiditeAu).append('\'');
        sb.append(", dateSignatureValide='").append(dateSignatureValide);
        sb.append(", periodiciteValide=").append(periodiciteValide);
        sb.append(", chaineDeCertificationValide=").append(chaineDeCertificationValide);
        sb.append(", absenceRevocationCRL=").append(absenceRevocationCRL);
        sb.append(", signatureValide=").append(signatureValide);
        sb.append('}');
        return sb.toString();
    }
}
