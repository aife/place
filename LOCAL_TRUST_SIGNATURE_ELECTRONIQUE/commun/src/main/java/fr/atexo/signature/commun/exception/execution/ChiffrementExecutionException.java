package fr.atexo.signature.commun.exception.execution;

/**
 * Exception pour signaler un souci lors du chiffrement.
 */
public class ChiffrementExecutionException extends ExecutionException {

    public ChiffrementExecutionException(String message) {
        super(message);
    }

    public ChiffrementExecutionException(String message, Throwable cause) {
        super(message, cause);
    }
}
