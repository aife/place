package fr.atexo.pdfboxhelpers;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class VisualSignatureGenerator {

	private final static int MARGIN = 2;

	public BufferedImage getVisibleSignature(int width, int height, String texte, String nom, String raison, String lieu, Date date) {

		String textToFill = null;

		Font font = new Font("Arial", Font.PLAIN, 10);
		Canvas c = new Canvas();
		FontMetrics fontMetrics = c.getFontMetrics(font);

		if (texte == null) {
			StringBuffer buf = new StringBuffer();
			buf.append(nom).append('\n');
			SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
			buf.append("Date: ").append(sd.format(date));
			if (raison != null)
				buf.append('\n').append("Raison: ").append(raison);
			if (lieu != null)
				buf.append('\n').append("Lieu: ").append(lieu);
			textToFill = buf.toString();

		} else {
			StringBuffer buf = new StringBuffer();
			if (StringUtils.isNotBlank(texte)) {
				buf.append(texte).append('\n');
			}
			SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
			buf.append("Date: ").append(sd.format(date));
			textToFill = buf.toString();
		}

		int linesCount = StringUtils.countMatches(textToFill, "\n") + 1;
		int minHeight = linesCount * fontMetrics.getHeight() + MARGIN * 2;

		if (height < minHeight) {
			height = minHeight;
		}

		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = bufferedImage.createGraphics();
		g.setColor(Color.BLACK);

		g.setFont(font);

		g.draw(new Rectangle2D.Double(0, 0, width - 1, height - 1));

		drawString(g, textToFill, MARGIN, MARGIN);

		g.dispose();
		return bufferedImage;
	}

	void drawString(Graphics2D g, String text, int x, int y) {
		y += g.getFontMetrics().getAscent();
		for (String line : text.split("\n")) {
			g.drawString(line, x, y);
			y += g.getFontMetrics().getHeight();
		}
	}

}
