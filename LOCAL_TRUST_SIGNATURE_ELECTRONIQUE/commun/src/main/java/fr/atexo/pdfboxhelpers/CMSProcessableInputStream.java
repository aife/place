package fr.atexo.pdfboxhelpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.util.io.Streams;

public class CMSProcessableInputStream implements CMSTypedData {
	private final InputStream input;

	private final ASN1ObjectIdentifier type;

	public CMSProcessableInputStream(InputStream input) {
		this.input = input;
		this.type = new ASN1ObjectIdentifier(CMSObjectIdentifiers.data.getId());
	}

	public InputStream getInputStream() {
		return input;
	}

	@Override
	public void write(OutputStream zOut) throws IOException, CMSException {
		Streams.pipeAll(input, zOut);
		input.close();
	}

	@Override
	public Object getContent() {
		return getInputStream();
	}

	@Override
	public ASN1ObjectIdentifier getContentType() {
		return type;
	}

}
