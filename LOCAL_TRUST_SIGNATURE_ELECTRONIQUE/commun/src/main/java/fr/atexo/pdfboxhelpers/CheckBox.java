package fr.atexo.pdfboxhelpers;

public class CheckBox {

	String id;
	boolean checked;

	public CheckBox(String id, boolean checked) {
		super();
		this.id = id;
		this.checked = checked;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
