package fr.atexo.json.reponse.verification;

/**
 *
 */
public class RetourVerificationReponse extends AbstractRepertoireReponse {

    private Integer periodeValidite;

    private Integer chaineCertification;

    private Integer revocation;

    public RetourVerificationReponse() {
    }

    public RetourVerificationReponse(RetourVerification retourVerification) {
        if (retourVerification != null) {
            this.periodeValidite = retourVerification.getPeriodeValidite() != null ? retourVerification.getPeriodeValidite().getCodeRetour() : null;
            this.chaineCertification = retourVerification.getChaineCertification() != null ? retourVerification.getChaineCertification().getCodeRetour() : null;
            this.revocation = retourVerification.getRevocation() != null ? retourVerification.getRevocation().getCodeRetour() : null;
            setRepertoiresChaineCertification(retourVerification.getRepertoiresChaineCertification());
            setRepertoiresRevocation(retourVerification.getRepertoiresRevocation());
        }
    }

    public Integer getPeriodeValidite() {
        return periodeValidite;
    }

    public void setPeriodeValidite(Integer periodeValidite) {
        this.periodeValidite = periodeValidite;
    }

    public Integer getChaineCertification() {
        return chaineCertification;
    }

    public void setChaineCertification(Integer chaineCertification) {
        this.chaineCertification = chaineCertification;
    }

    public Integer getRevocation() {
        return revocation;
    }

    public void setRevocation(Integer revocation) {
        this.revocation = revocation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RetourVerificationReponse");
        sb.append("{ repertoiresChaineCertification=").append(getRepertoiresChaineCertification());
        sb.append(", repertoiresRevocation=").append(getRepertoiresRevocation());
        sb.append(", periodeValidite=").append(periodeValidite);
        sb.append(", chaineCertification=").append(chaineCertification);
        sb.append(", revocation=").append(revocation);
        sb.append('}');
        return sb.toString();
    }
}
