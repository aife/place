package fr.atexo.json.reponse.verification;

/**
 *
 */
public enum CodeReponse {

    Ok(0, null),
    ContenuFichierXmlNonPresent(1, "Fichier xml non présent"),
    TypeValidationNonPresent(2, "Le type de validation n'est pas présent"),
    TypeAlgorithmHashNonPresent(3, "Le type d'algorithm n'est pas présent"),
    HashFichierNonPresent(4,"Le hash du fichier n'est pas présent");


    private int code;

    private String message;

    CodeReponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
