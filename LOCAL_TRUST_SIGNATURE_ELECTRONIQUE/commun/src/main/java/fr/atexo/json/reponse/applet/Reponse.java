package fr.atexo.json.reponse.applet;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class Reponse implements Serializable {

    private String contenuReponseAnnonceXML;

    private List<ReponseSignatureFichier> fichiers;

    private List<ReponseSignatureHash> hashs;

    public Reponse() {
    }

    public String getContenuReponseAnnonceXML() {
        return contenuReponseAnnonceXML;
    }

    public void setContenuReponseAnnonceXML(String contenuReponseAnnonceXML) {
        this.contenuReponseAnnonceXML = contenuReponseAnnonceXML;
    }

    public List<ReponseSignatureFichier> getFichiers() {
        return fichiers;
    }

    public void setFichiers(List<ReponseSignatureFichier> fichiers) {
        this.fichiers = fichiers;
    }

    public List<ReponseSignatureHash> getHashs() {
        return hashs;
    }

    public void setHashs(List<ReponseSignatureHash> hashs) {
        this.hashs = hashs;
    }
}
