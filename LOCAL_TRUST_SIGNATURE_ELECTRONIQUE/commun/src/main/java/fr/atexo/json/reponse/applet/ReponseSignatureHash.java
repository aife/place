package fr.atexo.json.reponse.applet;

/**
 *
 */
public class ReponseSignatureHash extends AbstractReponseSignature {

    private String nomFichier;

    private String hashFichier;

    private String contenuSignatureXML;

    public ReponseSignatureHash() {
        super();
    }

    public String getHashFichier() {
        return hashFichier;
    }

    public void setHashFichier(String hashFichier) {
        this.hashFichier = hashFichier;
    }

    public String getContenuSignatureXML() {
        return contenuSignatureXML;
    }

    public void setContenuSignatureXML(String contenuSignatureXML) {
        this.contenuSignatureXML = contenuSignatureXML;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }
}
