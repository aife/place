package fr.atexo.json.reponse.verification;

/**
 *
 */
public class ReponseEnrichissementSignature extends ReponseVerificationSignature {

    private String contenuFichierXMLEnBase64;

    public ReponseEnrichissementSignature() {
    }

    public ReponseEnrichissementSignature(CodeReponse codeReponse, RetourVerification resultat, Boolean dateSignatureValide, String contenuFichierXMLEnBase64) {
        super(codeReponse, resultat, dateSignatureValide);
        this.contenuFichierXMLEnBase64 = contenuFichierXMLEnBase64;
    }

    public String getContenuFichierXMLEnBase64() {
        return contenuFichierXMLEnBase64;
    }

    public void setContenuFichierXMLEnBase64(String contenuFichierXMLEnBase64) {
        this.contenuFichierXMLEnBase64 = contenuFichierXMLEnBase64;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ReponseEnrichissementSignature");
        sb.append("{ codeRetour=").append(getCodeRetour());
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append(", resultat=").append(getResultat());
        sb.append(", dateSignatureValide=").append(getDateSignatureValide());
        sb.append('}');
        return sb.toString();
    }
}
