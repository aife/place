package fr.atexo.json.reponse.applet;

import fr.atexo.json.reponse.Repertoire;
import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class InfosCertificat implements InfosComplementairesCertificat, Serializable {

    private String signataireComplet;

    private String signatairePartiel;

    private String emetteur;

    private String dateValiditeDu;

    private String dateValiditeAu;

    private Boolean dateSignatureValide;

    private Boolean periodiciteValide;

    private Boolean chaineDeCertificationValide;

    private Boolean absenceRevocationCRL;

    private Boolean signatureValide;

    private Set<Repertoire> repertoiresChaineCertification;

    private Set<Repertoire> repertoiresRevocation;

    public InfosCertificat() {
    }

    public InfosCertificat(InfosComplementairesCertificat infosComplementairesCertificat) {
        if (infosComplementairesCertificat != null) {
            this.signataireComplet = infosComplementairesCertificat.getSignataireComplet();
            this.signatairePartiel = infosComplementairesCertificat.getSignatairePartiel();
            this.emetteur = infosComplementairesCertificat.getEmetteur();
            this.dateValiditeDu = infosComplementairesCertificat.getDateValiditeDu();
            this.dateValiditeAu = infosComplementairesCertificat.getDateValiditeAu();
            this.dateSignatureValide = infosComplementairesCertificat.getDateSignatureValide();
            this.periodiciteValide = infosComplementairesCertificat.getPeriodiciteValide();
            this.chaineDeCertificationValide = infosComplementairesCertificat.getChaineDeCertificationValide();
            this.absenceRevocationCRL = infosComplementairesCertificat.getAbsenceRevocationCRL();
            this.signatureValide = infosComplementairesCertificat.getSignatureValide();
            this.repertoiresChaineCertification = infosComplementairesCertificat.getRepertoiresChaineCertification();
            this.repertoiresRevocation = infosComplementairesCertificat.getRepertoiresRevocation();
        }
    }

    public String getSignataireComplet() {
        return signataireComplet;
    }

    public void setSignataireComplet(String signataireComplet) {
        this.signataireComplet = signataireComplet;
    }

    public String getSignatairePartiel() {
        return signatairePartiel;
    }

    public void setSignatairePartiel(String signatairePartiel) {
        this.signatairePartiel = signatairePartiel;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }

    public String getDateValiditeDu() {
        return dateValiditeDu;
    }

    public void setDateValiditeDu(String dateValiditeDu) {
        this.dateValiditeDu = dateValiditeDu;
    }

    public String getDateValiditeAu() {
        return dateValiditeAu;
    }

    public void setDateValiditeAu(String dateValiditeAu) {
        this.dateValiditeAu = dateValiditeAu;
    }

    public Boolean getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(Boolean dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }

    public Boolean getPeriodiciteValide() {
        return periodiciteValide;
    }

    public void setPeriodiciteValide(Boolean periodiciteValide) {
        this.periodiciteValide = periodiciteValide;
    }

    public Boolean getChaineDeCertificationValide() {
        return chaineDeCertificationValide;
    }

    public void setChaineDeCertificationValide(Boolean chaineDeCertificationValide) {
        this.chaineDeCertificationValide = chaineDeCertificationValide;
    }

    public Boolean getAbsenceRevocationCRL() {
        return absenceRevocationCRL;
    }

    public void setAbsenceRevocationCRL(Boolean absenceRevocationCRL) {
        this.absenceRevocationCRL = absenceRevocationCRL;
    }

    public Boolean getSignatureValide() {
        return signatureValide;
    }

    public void setSignatureValide(Boolean signatureValide) {
        this.signatureValide = signatureValide;
    }

    public Set<Repertoire> getRepertoiresChaineCertification() {
        return repertoiresChaineCertification;
    }

    public void setRepertoiresChaineCertification(Set<Repertoire> repertoiresChaineCertification) {
        this.repertoiresChaineCertification = repertoiresChaineCertification;
    }

    public Set<Repertoire> getRepertoiresRevocation() {
        return repertoiresRevocation;
    }

    public void setRepertoiresRevocation(Set<Repertoire> repertoiresRevocation) {
        this.repertoiresRevocation = repertoiresRevocation;
    }  
}
