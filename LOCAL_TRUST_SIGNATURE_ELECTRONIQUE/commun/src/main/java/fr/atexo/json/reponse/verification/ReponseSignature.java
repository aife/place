package fr.atexo.json.reponse.verification;

/**
 * Réponse la réponse Json d'un signature entre l'applet et le client (js)
 */
public class ReponseSignature extends AbstractRepertoireReponse {

    private Integer codeRetour;

    private String message;

    private String signatureXadesServeurEnBase64;

    private Boolean dateSignatureValide;

    private Boolean periodiciteValide;

    private Boolean chaineDeCertificationValide;

    private Boolean absenceRevocationCRL;

    public ReponseSignature(Integer codeRetour, String message) {
        this.codeRetour = codeRetour;
        this.message = message;
    }

    public Integer getCodeRetour() {
        return codeRetour;
    }

    public void setCodeRetour(Integer codeRetour) {
        this.codeRetour = codeRetour;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSignatureXadesServeurEnBase64() {
        return signatureXadesServeurEnBase64;
    }

    public void setSignatureXadesServeurEnBase64(String signatureXadesServeurEnBase64) {
        this.signatureXadesServeurEnBase64 = signatureXadesServeurEnBase64;
    }

    public Boolean getPeriodiciteValide() {
        return periodiciteValide;
    }

    public void setPeriodiciteValide(Boolean periodiciteValide) {
        this.periodiciteValide = periodiciteValide;
    }

    public Boolean getChaineDeCertificationValide() {
        return chaineDeCertificationValide;
    }

    public void setChaineDeCertificationValide(Boolean chaineDeCertificationValide) {
        this.chaineDeCertificationValide = chaineDeCertificationValide;
    }

    public Boolean getAbsenceRevocationCRL() {
        return absenceRevocationCRL;
    }

    public void setAbsenceRevocationCRL(Boolean absenceRevocationCRL) {
        this.absenceRevocationCRL = absenceRevocationCRL;
    }

    public Boolean getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(Boolean dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }
}