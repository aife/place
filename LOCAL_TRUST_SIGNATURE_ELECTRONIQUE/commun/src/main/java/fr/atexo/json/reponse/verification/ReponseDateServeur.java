package fr.atexo.json.reponse.verification;

/**
 *
 */
public class ReponseDateServeur extends MessageReponse {

    private String dateISO8601;

    public ReponseDateServeur(String dateISO8601) {
        this.dateISO8601 = dateISO8601;
    }

    public ReponseDateServeur(int codeRetour, String message, String dateISO8601) {
        super(codeRetour, message);
        this.dateISO8601 = dateISO8601;
    }

    public ReponseDateServeur(CodeReponse codeReponse, String dateISO8601) {
        super(codeReponse);
        this.dateISO8601 = dateISO8601;
    }

    public String getDateISO8601() {
        return dateISO8601;
    }

    public void setDateISO8601(String dateISO8601) {
        this.dateISO8601 = dateISO8601;
    }
}
