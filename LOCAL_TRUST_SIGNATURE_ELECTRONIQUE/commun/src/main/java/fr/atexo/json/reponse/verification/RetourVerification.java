package fr.atexo.json.reponse.verification;

import fr.atexo.signature.commun.securite.certificat.verification.CertificatValiditeEtat;
import fr.atexo.signature.commun.securite.certificat.verification.CertificatVerificationEtat;

import java.util.Arrays;

/**
 *
 */
public class RetourVerification extends AbstractRepertoireReponse {

    private CertificatValiditeEtat periodeValidite;

    private CertificatVerificationEtat chaineCertification;

    private CertificatVerificationEtat revocation;

    public RetourVerification() {
    }

    public RetourVerification(CertificatValiditeEtat periodeValidite, CertificatVerificationEtat chaineCertification, CertificatVerificationEtat revocation) {
        this.periodeValidite = periodeValidite;
        this.chaineCertification = chaineCertification;
        this.revocation = revocation;
    }

    public CertificatValiditeEtat getPeriodeValidite() {
        return periodeValidite;
    }

    public void setPeriodeValidite(CertificatValiditeEtat periodeValidite) {
        this.periodeValidite = periodeValidite;
    }

    public CertificatVerificationEtat getChaineCertification() {
        return chaineCertification;
    }

    public void setChaineCertification(CertificatVerificationEtat chaineCertification) {
        this.chaineCertification = chaineCertification;
    }

    public CertificatVerificationEtat getRevocation() {
        return revocation;
    }

    public void setRevocation(CertificatVerificationEtat revocation) {
        this.revocation = revocation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RetourVerification");
        sb.append("{periodeValidite=").append(periodeValidite);
        sb.append(", chaineCertification=").append(chaineCertification);
        sb.append(", revocation=").append(revocation);
        if (getRepertoiresChaineCertification() != null) {
            sb.append(", repertoiresChaineCertification=").append(Arrays.toString(getRepertoiresChaineCertification().toArray()));
        }
        if (getRepertoiresRevocation() != null) {
            sb.append(", repertoiresRevocation=").append(Arrays.toString(getRepertoiresRevocation().toArray()));
        }
        sb.append('}');
        return sb.toString();
    }
}