package fr.atexo.json.reponse.applet;

import fr.atexo.signature.commun.securite.processor.signature.InfosComplementairesCertificat;

import java.io.Serializable;

/**
 *
 */
public abstract class AbstractReponseSignature implements Serializable {

    private String identifiant;

    private InfosComplementairesCertificat infosSignature;

    private Integer typeEnveloppe;

    private Integer numeroLot;

    private Integer index;

    private boolean signatureVerifiee;

    protected AbstractReponseSignature() {
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public InfosComplementairesCertificat getInfosSignature() {
        return infosSignature;
    }

    public void setInfosSignature(InfosComplementairesCertificat infosSignature) {
        this.infosSignature = infosSignature;
    }

    public Integer getTypeEnveloppe() {
        return typeEnveloppe;
    }

    public void setTypeEnveloppe(Integer typeEnveloppe) {
        this.typeEnveloppe = typeEnveloppe;
    }

    public Integer getNumeroLot() {
        return numeroLot;
    }

    public void setNumeroLot(Integer numeroLot) {
        this.numeroLot = numeroLot;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public boolean isSignatureVerifiee() {
        return signatureVerifiee;
    }

    public void setSignatureVerifiee(boolean signatureVerifiee) {
        this.signatureVerifiee = signatureVerifiee;
    }
}
