package fr.atexo.json.reponse.verification;

import fr.atexo.json.reponse.Repertoire;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public abstract class AbstractRepertoireReponse implements Serializable {

    private Set<Repertoire> repertoiresChaineCertification;

    private Set<Repertoire> repertoiresRevocation;

    public Set<Repertoire> getRepertoiresChaineCertification() {
        return repertoiresChaineCertification;
    }

    public void setRepertoiresChaineCertification(Set<Repertoire> repertoiresChaineCertification) {
        this.repertoiresChaineCertification = repertoiresChaineCertification;
    }

    public void ajouterRepertoireChaineCertification(String nomRepertoire) {
        if (repertoiresChaineCertification == null) {
            repertoiresChaineCertification = new HashSet<Repertoire>();
        }
        repertoiresChaineCertification.add(new Repertoire(nomRepertoire));
    }

    public Set<Repertoire> getRepertoiresRevocation() {
        return repertoiresRevocation;
    }

    public void setRepertoiresRevocation(Set<Repertoire> repertoiresRevocation) {
        this.repertoiresRevocation = repertoiresRevocation;
    }

    public void ajouterRepertoireRevocation(String nomRepertoire) {
        if (repertoiresRevocation == null) {
            repertoiresRevocation = new HashSet<Repertoire>();
        }
        repertoiresRevocation.add(new Repertoire(nomRepertoire));
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AbstractRepertoireReponse");
        sb.append("{repertoiresChaineCertification=").append(repertoiresChaineCertification);
        sb.append(", repertoiresRevocation=").append(repertoiresRevocation);
        sb.append('}');
        return sb.toString();
    }
}
