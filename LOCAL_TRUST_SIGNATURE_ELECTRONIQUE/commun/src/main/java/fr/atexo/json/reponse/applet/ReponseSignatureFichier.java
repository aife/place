package fr.atexo.json.reponse.applet;

/**
 *
 */
public class ReponseSignatureFichier extends AbstractReponseSignature {

    private String cheminFichier;

    private String cheminSignatureXML;

    public ReponseSignatureFichier() {
        super();
    }

    public String getCheminFichier() {
        return cheminFichier;
    }

    public void setCheminFichier(String cheminFichier) {
        this.cheminFichier = cheminFichier;
    }

    public String getCheminSignatureXML() {
        return cheminSignatureXML;
    }

    public void setCheminSignatureXML(String cheminSignatureXML) {
        this.cheminSignatureXML = cheminSignatureXML;
    }
}
