package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageOpenOfficeTesteur implements DecrireUnParametreDeTesteur {
	OPENOFFICE_SERVICEHOST(true,String.class),
	OPENOFFICE_PORT(true,String.class),
	OPENOFFICE_REPERTOIRE_TEMPORAIRE(true,String.class),
	OPENOFFICE_CHEMIN_TEMPLATE(true,String.class),
    /**
     * Puisqu'on ne teste que la possibilité d'une connexion execution sans
     * erreur, inutile d'attendre une réponse précise. Ce paramètre ne doit pas
     * exister.
     */
    REPONSE_ATTENDUE(false, null);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageOpenOfficeTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return returnReponseAttendue();
    }

    private static DecrireUnParametreDeTesteur returnReponseAttendue() {
        return REPONSE_ATTENDUE;
    }

}
