package fr.atexo.urlvie.toolbox.testers.validators.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;
import fr.atexo.urlvie.toolbox.testers.validators.ValideLesParametres;

public class ValidateurListeDeTests implements ValideLesParametres {

    private static final Log LOG = LogFactory
            .getLog(ValidateurListeDeTests.class);

    private static final String PAS_TESTEUR = "L'un des paramètres n'est pas un testeur - type détecté : %1$s";

    public boolean execute(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametres,
            Map<String, Object> parametres) throws ValidationException {
        boolean result = true;
        if (parametres != null && !parametres.isEmpty()) {
            Collection<Object> paramValues = parametres.values();
            String typeTesteurPotentiel = null;
            for (Iterator<Object> it = paramValues.iterator(); it.hasNext()
                    && result;) {
                Object testeurPotentiel = it.next();
                result = testeurPotentiel instanceof UrlVieTesteur;
                if(testeurPotentiel != null){
                    typeTesteurPotentiel = testeurPotentiel.getClass().getSimpleName();
                } else {
                    typeTesteurPotentiel = "null";
                }
                if (result) {
                    LOG.info(String.format(TESTEUR_RECONNU,
                            ((UrlVieTesteur) testeurPotentiel)
                                    .getNomCibleDuTest()));
                }
            }
            LOG.info(String.format(STATUT_VALIDATION, this.getClass(),
                    result ? "OK" : "KO"));
            if (!result) {
                throw new ValidationException(String.format(PAS_TESTEUR,typeTesteurPotentiel));
            }
        }
        return result;

    }

}
