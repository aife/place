/**
 * 
 */
package fr.atexo.urlvie.toolbox.testers.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.helpers.FabriquerUneReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.brute.TypeCibleTest;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.AbstractConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.ConstruireDocument;
import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.exceptions.NoValidatorException;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ListeDeTesteurs;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.SansParametre;
import fr.atexo.urlvie.toolbox.testers.validators.ValideLesParametres;
import fr.atexo.urlvie.toolbox.testers.validators.impl.ValidateurBasic;
import fr.atexo.urlvie.toolbox.testers.validators.impl.ValidateurListeDeTests;

/**
 * Cette classe définit le squelette général d'un test :
 * <ul>
 * <li>création de l'objet {@link ReponseBrute} daté</li>
 * <li>enregistrement de l'identité du test
 * {@link #identificationDuSujetDuTest(ReponseBrute)}</li>
 * <li>validation des paramètres de test
 * {@link #valideLesParametres(ReponseBrute)}</li>
 * <li>reformulation des paramètres de test {@link #reformulerLesParametres()}</li>
 * <li>recuperation de la réponse au test
 * {@link #collecteResultats(ReponseBrute)}</li>
 * <li>enregistrement des parametres du test
 * {@link #enregistreLesParametres(ReponseBrute)}</li>
 * <li>fabrication du xml résumant le déroulement du test
 * {@link #fabriqueXml(ReponseBrute)}</li>
 * </ul>
 * Il a été définit que des classes utilitaires dites "helpers" du package
 * <code>fr.atexo.urlvie.toolbox.helpers</code> doivent effectuer les opérations
 * de test. (cf. l'implémentation de {@link #collecteResultats(ReponseBrute)}.
 * 
 * @author Cyril DUBY
 * 
 */
public class UrlVieTesteurImpl implements UrlVieTesteur {

    protected final Log log = LogFactory.getLog(this.getClass());

    protected static final String ECHEC_CHARGEMENT_CLASS = "Chargement de la class : %1$s impossible.";
    protected static final String DEBUT_TEST = "DEBUT DU TEST - nom testeur : %1$s - cible du test : %2$s";
    protected static final String ECHEC_VALIDATION_PARAM = "Aucun validateur.";
    protected static final String DEFAULT_NAME = "TEST SANS NOM";
    protected static final TypeCibleTest DEFAULT_TYPE = TypeCibleTest.MODULE;
    protected static final Class<? extends DecrireUnParametreDeTesteur> DEFAULT_CLASS_PARAM = SansParametre.class;

    // ELEMENTS D'IDENTIFICATION DU TEST

    /**
     * Sert à identifier la raison de l'appel de ce test : il doit s'agir du nom
     * du module ou de l'appication concerné.
     */
    protected String nomCibleDuTest = DEFAULT_NAME;

    /**
     * Précise si ce test est un test d'application ou de module.
     * 
     * @see TypeCibleTest
     */
    protected TypeCibleTest typeCibleTest = DEFAULT_TYPE;

    // PARAMETRAGE DU TEST

    /**
     * Afin de permettre une automatisation aussi poussée que possible du
     * fonctionnement d'un test, ce membre, rendu obligatoire par la présente
     * implémentation de la méthode {@link #valideLesParametres()}, permet
     * d'effectuer la {@link #validationSimple(DecrireUnParametreDeTesteur[])}
     * des paramètres.
     */
    protected Class<? extends DecrireUnParametreDeTesteur> clazzParametres = DEFAULT_CLASS_PARAM;

    /**
     * Contient l'ensemble des paramètres nécessaires à l'exécution du test.
     * Chacune des clés est censée être la représentation d'une instance de
     * l'Enum dont la classe est celle décrite par {@link #clazzParametres}.
     * Initialisé avec une implémentation de Map.
     */
    protected Map<String, Object> parametres = new HashMap<String, Object>();

    // OBJETS SPECIALISES

    /**
     * Liste des validateurs de paramètres à exécuter.
     * 
     * @see #valideLesParametres(ReponseBrute)
     */
    protected List<ValideLesParametres> validateurs;

    /**
     * A partir des {@link #parametres}, exécute le test et tire les conclusions
     * des résultats.
     * 
     * @see FabriquerUneReponseBrute
     */
    protected FabriquerUneReponseBrute helper;

    /**
     * Objet sachant décrypter la réponse au test pour fabriquer le xml.
     * 
     * @see AbstractConstructeurReponse
     */
    protected ConstruireDocument construireDocument;

    /**
     * <ul>
     * <li>création de l'objet {@link ReponseBrute} daté</li>
     * <li>enregistrement de l'identité du test
     * {@link #identificationDuSujetDuTest(ReponseBrute)}</li>
     * <li>validation des paramètres de test
     * {@link #valideLesParametres(ReponseBrute)}</li>
     * <li>reformulation des paramètres de test
     * {@link #reformulerLesParametres()}</li>
     * <li>recuperation de la réponse au test
     * {@link #collecteResultats(ReponseBrute)}</li>
     * <li>enregistrement des parametres du test
     * {@link #enregistreLesParametres(ReponseBrute)}</li>
     * <li>fabrication du xml résumant le déroulement du test
     * {@link #fabriqueXml(ReponseBrute)}</li>
     * </ul>
     * Il a été définit que des classes utilitaires dites "helpers" du package
     * <code>fr.atexo.urlvie.toolbox.helpers</code> doivent effectuer les
     * opérations de test. (cf. l'implémentation de
     * {@link #collecteResultats(ReponseBrute)}.
     * 
     * @see fr.atexo.urlvie.toolbox.testers.UrlVieTesteur#lanceTest()
     */
    public synchronized String lanceTest() {
        standardInit();
        String result = null;
        log.info(String.format(
                "DEBUT DU TEST - nom testeur : %1$s - cible du test : %2$s",
                this.getClass(), nomCibleDuTest));
        try {
            ReponseBrute reponseBrute = new ReponseBrute();
            reponseBrute.setDateDebutTest(new Date());
            identificationDuSujetDuTest(reponseBrute);
            log.info(String.format(
                    "Validation des paramètres pour la cible : %1$s",
                    nomCibleDuTest));
            valideLesParametres(reponseBrute);
            if (reponseBrute.isParametresValides()) {
                reformulerLesParametres();
            }
            log.info(String
                    .format("Lancement de la collecte de résultats de test pour la cible : %1$s",
                            nomCibleDuTest));
            collecteResultats(reponseBrute);
            log.info(String
                    .format("Enregistrement des paramètres du test pour la cible : %1$s",
                            nomCibleDuTest));
            enregistreLesParametres(reponseBrute);
            log.info(String.format(MESSAGE_RESULTAT, reponseBrute
                    .getStatutDuTest().toString()));
            log.info(String.format("Marshalling du XML pour la cible : %1$s",
                    nomCibleDuTest));
            result = fabriqueXml(reponseBrute);
            if (result != null) {
                log.info(String.format("Génération XML pour la cible %1$s: OK",
                        nomCibleDuTest));
                log.trace(result);
            } else {
                log.error(String
                        .format("Génération XML pour la cible %1$s: KO",
                                nomCibleDuTest));
            }
        } finally {
            log.info(String.format("FIN DU TEST - pour la cible : %1$s",
                    nomCibleDuTest));
        }
        return result;
    }

    //

    /**
     * Initialisation automatique avec les membres par defaut.
     */
    protected void standardInit() {
        InitialiseurStandard initialiseurStandard = InitialiseurStandard
                .trouveCorrespondance(clazzParametres);
        if (initialiseurStandard != null) {
            log.info("Debut d'initialisation standard.");
            if (validateurs == null) {
                validateurs = initialiseurStandard.getValidateurs();
                if (validateurs != null) {
                    log.info("Validateurs initialisés");
                }
            }
            if (helper == null) {
                helper = initialiseurStandard.getHelper();
                if (helper != null) {
                    log.info(String.format("Helper initialisé : %1$s", helper
                            .getClass().getCanonicalName()));
                }
            }
            if (construireDocument == null) {
                construireDocument = initialiseurStandard
                        .getConstructeurReponse();
                if (construireDocument != null) {
                    log.info(String.format(
                            "ConstruireDocument initialisé : %1$s",
                            construireDocument.getClass().getCanonicalName()));
                }
            }
            log.info("Fin d'initialisation standard.");
        }
    }

    /**
     * Enregistre dans {@link ReponseBrute} les diverses informations qui
     * identifient la cible du test.
     * 
     * @param reponseBrute
     */
    protected void identificationDuSujetDuTest(ReponseBrute reponseBrute) {
        reponseBrute.setNomCibleTest(nomCibleDuTest);
        reponseBrute.setTypeCibleTest(typeCibleTest);
        reponseBrute.setTesteurClazz(this.getClass());
    }

    /**
     * Si la liste {@link #validateurs} est vide, un validateur est
     * automatiquement ajouté à cette liste, puis exécuté.<br/>
     * Il s'agira de
     * <ul>
     * <li>{@link ValidateurBasic}s'il s'agit d'un test simple (
     * {@link #isListeDeTests()} == <code>false</code>) ;</li>
     * <li>{@link ValidateurListeDeTests}s'il s'agit d'un test simple (
     * {@link #isListeDeTests()} == <code>true</code>).</li>
     * </ul>
     * Les {@link ValidationException} renvoyées par les validateurs sont
     * interceptées et servent à renseigner l'objet {@link ReponseBrute}.
     * 
     * @param reponseBrute
     *            est modifié au cours de l'exécution de cette méthode (le
     *            résultat renvoyé par
     *            {@link ReponseBrute#isParametresValides()} est mis à jour)
     * 
     */
    protected void valideLesParametres(ReponseBrute reponseBrute) {
        boolean result = false;
        try {
            if (validateurs != null && !validateurs.isEmpty()) {
                result = true;
                for (ValideLesParametres validateur : validateurs) {
                    try {
                        result &= validateur.execute(clazzParametres,
                                parametres);
                    } catch (ValidationException e) {
                        log.info(e.getMessage());
                        reponseBrute.getExceptionsInterceptees().add(e);
                        result = false;
                    }
                }
            } else {
                throw new NoValidatorException(ECHEC_VALIDATION_PARAM);
            }
        } catch (Exception e) {
            reponseBrute.getExceptionsInterceptees().add(e);
        } finally {
            reponseBrute.setParametresValides(result);
        }
    }

    /**
     * Cette méthode permet d'implanter une étape supplémentaire de traitement
     * des paramètres. En effet, la simple copie de paramètres d'un objet à
     * l'autre n'est pas toujours possible. Il est parfois nécessaire de faire
     * s'exécuter un interpréteur de paramètres afin de les transcoder.
     * Couramment, les objets d'interprétation des paramètres ne sont connus que
     * de l'objet de plus haut niveau (typiquement le connecteur) et non de
     * l'objet établissant réellement la connexion avec le service (typiquement
     * les classes DAO). Lorsqu'il est nécessaire de faire appel à la classe
     * d'interprétation des paramètres, surcharger cette méthode permet
     * d'inclure l'étape sans voir à modifier l'implémentation d'autres méthodes
     * pour intégrer artificiellement une telle étape. Il est aussi rendu
     * possible d'insérer le résultat de ce traitement dans la {@link Map} de
     * paramètres.
     */
    protected void reformulerLesParametres() {
        return;
    }

    /**
     * Enregistre dans {@link ReponseBrute} les paramètres du test. Le membre
     * recueillant les paramètres utilisés permet d'organiser souplement le
     * stockage des paramètres. Cette implémentation enregistre le membre
     * {@link #parametres} tel quel.
     * 
     * @param reponseBrute
     */
    protected void enregistreLesParametres(ReponseBrute reponseBrute) {
        if (clazzParametres.isAssignableFrom(ListeDeTesteurs.class)
                && reponseBrute.isParametresValides()) {
            for (Iterator<Object> it = parametres.values().iterator(); it
                    .hasNext();) {
                Object obj = it.next();
                String name = "null";
                if (obj != null) {
                    name = ((UrlVieTesteur) obj).getNomCibleDuTest();
                }
                reponseBrute.getParametresUtilises().add(name);
            }
        } else {
            reponseBrute.getParametresUtilises().add(parametres);
        }
    }

    /**
     * @return <code>true</code> la map de parametres n'a dans ses valeurs que
     *         des instances de {@link UrlVieTesteur}
     */
    protected boolean isListeDeTests() {
        boolean result = parametres != null && !parametres.isEmpty();
        if (result) {
            Collection<Object> paramValues = parametres.values();
            for (Iterator<Object> paramIterator = paramValues.iterator(); result
                    && paramIterator.hasNext();) {
                result = paramValues.iterator().next() instanceof UrlVieTesteur;
            }
        }
        return result;
    }

    /**
     * Essentiellement, appelle la méthode
     * {@link FabriquerUneReponseBrute#execute(ReponseBrute, Map)} du membre
     * nommé {@link #helper}.
     * 
     * @param reponseBrute
     * 
     * @see FabriquerUneReponseBrute
     */
    protected void collecteResultats(ReponseBrute reponseBrute) {
        helper.execute(reponseBrute, parametres);
    }

    /**
     * Demande la fabrication du document XML sous la forme d'une chaîne de
     * caractères.
     * 
     * @param reponseBrute
     * @return
     */
    protected String fabriqueXml(ReponseBrute reponseBrute) {
        return construireDocument
                .transformeEnChaineContenantUnXml(reponseBrute);
    }

    // ACCESSEURS

    public void setClazzParametres(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametres) {
        this.clazzParametres = clazzParametres;
    }

    @SuppressWarnings("unchecked")
    public void setNomClazzParametres(String nomClazzParametres) {
        try {
            this.clazzParametres = (Class<? extends DecrireUnParametreDeTesteur>) Class
                    .forName(nomClazzParametres);
        } catch (ClassNotFoundException e) {
            log.error(String.format(ECHEC_CHARGEMENT_CLASS, nomClazzParametres));
            log.debug(e.getMessage(), e);
        }
    }

    public final void setConstructeurReponse(
            ConstruireDocument constructeurReponseImpl) {
        this.construireDocument = constructeurReponseImpl;
    }

    public void setParametres(Map<String, Object> parametres) {
        this.parametres = parametres;
    }

    public void setNomCibleDuTest(String nomObjetDuTest) {
        this.nomCibleDuTest = nomObjetDuTest;
    }

    public String getNomCibleDuTest() {
        return nomCibleDuTest;
    }

    public void setTypeCibleTest(TypeCibleTest type) {
        this.typeCibleTest = type;
    }

    public void setNomTypeCibleTest(String type) {
        this.typeCibleTest = TypeCibleTest.valueOf(type.toUpperCase());
    }

    public void setValidateurs(List<ValideLesParametres> validateurs) {
        this.validateurs = validateurs;
    }

    public void setHelper(FabriquerUneReponseBrute helper) {
        this.helper = helper;
    }

}
