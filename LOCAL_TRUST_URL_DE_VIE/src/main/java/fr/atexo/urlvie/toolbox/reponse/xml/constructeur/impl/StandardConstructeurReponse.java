package fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.AbstractConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.DetailsDuTest;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.ListeDeParametres;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.Parametre;

public class StandardConstructeurReponse extends AbstractConstructeurReponse {
    @Override
    protected void remplitDetails(DetailsDuTest details,
            ReponseBrute reponseBrute) {
        ListeDeParametres params = factory.createListeDeParametres();
        List<Object> paramsUtil = reponseBrute.getParametresUtilises();
        if (paramsUtil != null && !paramsUtil.isEmpty()
                && paramsUtil.get(0) != null) {
            Parametre temp = null;
            for (Entry<?, ?> entry : ((Map<?, ?>) paramsUtil.get(0)).entrySet()) {
                temp = factory.createParametre();
                temp.setNom(entry.getKey().toString());
                if (entry.getValue() != null) {
                    temp.setValeur(transformeValue(entry.getValue()));
                }
                params.getParametre().add(temp);
            }
        }
        details.setParametres(params);
    }

    protected String transformeValue(Object value) {
        String result = "null";
        if (value != null) {
            if (value instanceof String) {
                result = (String) value;
            } else if (value instanceof Map<?, ?>) {
                Map<?, ?> map = (Map<?, ?>) value;
                result = map.toString();
            } else {
                result = value.toString();
            }
        }

        return result;
    }
}
