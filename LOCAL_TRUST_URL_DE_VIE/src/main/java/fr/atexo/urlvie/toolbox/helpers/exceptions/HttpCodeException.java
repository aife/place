package fr.atexo.urlvie.toolbox.helpers.exceptions;

public class HttpCodeException extends Exception {

    private static final long serialVersionUID = -2933224616805143258L;

    public HttpCodeException() {
        super();
    }

    public HttpCodeException(String message) {
        super(message);
    }

}
