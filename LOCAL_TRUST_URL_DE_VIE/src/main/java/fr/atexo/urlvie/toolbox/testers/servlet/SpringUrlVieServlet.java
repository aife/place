package fr.atexo.urlvie.toolbox.testers.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;

/**
 * <b>Cette servlet est liée à Spring.</b><br/>
 * Un paramètre doit lui être passé dans le web.xml : le nom du paramètre est
 * fixé à {@value #NOM_PARAM_LISTE_TESTEUR} et sa valeur est les nom du bean
 * correspondant à l'instance de {@link UrlVieTesteur} qui lancera l'ensemble
 * des tests prévu pour la release du service proxy.
 * 
 * @author Cyril DUBY
 * 
 */
public class SpringUrlVieServlet extends UrlVieServlet {

    private static final long serialVersionUID = -6573817453073583209L;

    private static final Log LOG = LogFactory.getLog(SpringUrlVieServlet.class);

    private static final String NOM_PARAM_LISTE_TESTEUR = "nomDuBeanTesteur";

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOG.info(config.getServletName() + " init");
        super.init(config);
        String nomBean = config.getInitParameter(NOM_PARAM_LISTE_TESTEUR);
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(config.getServletContext());
        testeur = (UrlVieTesteur) context.getBean(nomBean);
        LOG.info(config.getServletName() + " fin init");
    }

}
