package fr.atexo.urlvie.toolbox.reponse.xml.constructeur;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

/**
 * Récupère de l'objet {@link ReponseBrute} les informations nécessaires et
 * fabrique un document.
 * 
 * @author Cyril DUBY
 * 
 */
public interface ConstruireDocument {

    /**
     * Récupère de l'objet {@link ReponseBrute} les informations nécessaires et
     * fabrique un document XML retournée sous la forme d'une chaîne de
     * caractères.
     * 
     * @param reponseBrute
     * @return le xml décrivant le résultat du test
     */
    String transformeEnChaineContenantUnXml(ReponseBrute reponseBrute);

}
