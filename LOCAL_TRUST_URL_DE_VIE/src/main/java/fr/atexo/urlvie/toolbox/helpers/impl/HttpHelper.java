/**
 * 
 */
package fr.atexo.urlvie.toolbox.helpers.impl;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import fr.atexo.urlvie.toolbox.helpers.impl.status.impl.HttpCalculateurStatut;
import fr.atexo.urlvie.toolbox.helpers.utils.MethodeHttpUtilisee;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageHttpTesteur;

/**
 * @author Cyril DUBY
 * 
 */
public class HttpHelper extends AbstractHelper {

    private static final String METHODE_UTILISEE = "Méthode utilisée : %1$s";
    private static final String EXCEPTION_INTERCEPTEE = "Exception interceptée : %1$s";

    public HttpHelper() {
        calculateurStatut = new HttpCalculateurStatut();
    }

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        String url = (String) parametres.get(ParametrageHttpTesteur.URL
                .toString());

        construitEtExecuteHttpMethod(reponseBrute,
                determineMethode(parametres), url, parametres);
    }

    protected MethodeHttpUtilisee determineMethode(
            Map<String, Object> parametres) {
        @SuppressWarnings("unchecked")
        Map<String, String> paramPost = (Map<String, String>) parametres
                .get(ParametrageHttpTesteur.POST.toString());

        MethodeHttpUtilisee method = null;
        method = paramPost != null && !paramPost.isEmpty() ? MethodeHttpUtilisee.POST
                : MethodeHttpUtilisee.GET;
        log.info(String.format(METHODE_UTILISEE, method.toString()));
        return method;
    }

    protected void construitEtExecuteHttpMethod(ReponseBrute reponseBrute,
            MethodeHttpUtilisee methodeHttpUtilisee, String url,
            Map<String, Object> params) {
        HttpClient client = new DefaultHttpClient();
        HttpRequestBase method = null;
        try {
            method = methodeHttpUtilisee.construitHttpMethod(params);
            HttpResponse response = client.execute(method);
            recueilleElementsRecus(response, reponseBrute);
        } catch (Exception e) {
            log.warn(String.format(EXCEPTION_INTERCEPTEE, e.getMessage()));
            reponseBrute.getExceptionsInterceptees().add(e);
        }
        // finally {
        // if (method != null) {
                // En prévision de la httpcomponents:httpclient 4.2
                // method.reset();
        // }
        // }
    }

    protected void recueilleElementsRecus(HttpResponse response,
            ReponseBrute reponseBrute) throws
            IOException {
        reponseBrute.getElementsRecus().add(
                response.getStatusLine().getStatusCode());
        log.debug(response.getStatusLine());
    }

}
