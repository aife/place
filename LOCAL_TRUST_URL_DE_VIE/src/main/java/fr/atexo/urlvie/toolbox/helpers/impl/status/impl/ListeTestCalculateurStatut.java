package fr.atexo.urlvie.toolbox.helpers.impl.status.impl;

import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseAuTest;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.Status;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.Test;

/**
 * Implémentation de base pour un test constitué d'une liste de tests.
 * 
 * @author Cyril DUBY
 * 
 */
public class ListeTestCalculateurStatut extends CalculateurStatut {

    /**
     * Cette implémentation recherche les statuts des sous-tests (stockés dans
     * les éléments reçus) et si un au moins est KO, considère que l'ensemble
     * est KO.
     * 
     * @see fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut#execute(fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute,
     *      java.util.Map)
     */
    @Override
    protected ReponseAuTest traitementSiParametresValides(
            ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        ReponseAuTest result = ReponseAuTest.OK;
        for (Object element : reponseBrute.getElementsRecus()) {
            Test test = (Test) element;
            if (test.getStatut().equals(Status.KO)) {
                result = ReponseAuTest.KO;
                break;
            }
        }
        return result;
    }
}
