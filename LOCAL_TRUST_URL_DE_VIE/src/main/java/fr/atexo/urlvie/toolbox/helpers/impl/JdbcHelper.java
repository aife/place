package fr.atexo.urlvie.toolbox.helpers.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.impl.status.impl.JdbcCalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageJdbcTesteur;

public class JdbcHelper extends AbstractHelper {

    private static final String DRIVER_LOADING_ERROR = "Chargement du driver en erreur : %1$s";

    public JdbcHelper() {
        calculateurStatut = new JdbcCalculateurStatut();
    }

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        String driver = (String) parametres.get(ParametrageJdbcTesteur.DRIVER
                .toString());
        String url = (String) parametres.get(ParametrageJdbcTesteur.URL
                .toString());
        String user = (String) parametres.get(ParametrageJdbcTesteur.USER
                .toString());
        String password = (String) parametres.get(ParametrageJdbcTesteur.PASSWORD
                .toString());
        String sql = (String) parametres.get(ParametrageJdbcTesteur.SQL
                .toString());
        if (driver != null && driver.trim().length() > 0) {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                log.warn(String.format(DRIVER_LOADING_ERROR, driver));
                log.debug(e.getMessage(), e);
                reponseBrute.getExceptionsInterceptees().add(e);
            }
        }
        getReponse(reponseBrute, url, user, password, sql);
    }

    private void getReponse(ReponseBrute reponse, String url,
            String user, String password, String sql) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
            conn.setAutoCommit(false);
            PreparedStatement prepSt = conn.prepareStatement(sql);
            prepSt.execute();
        } catch (SQLException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponse.getExceptionsInterceptees().add(e);
        } finally {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
                log.debug(e.getMessage(), e);
            } finally {
                if (conn != null) {
                    try {
                        if (conn != null) {
                            conn.close();
                        }
                    } catch (SQLException e) {
                        log.error(e.getMessage());
                        log.debug(e.getMessage(), e);
                    }
                }
            }
        }
    }


}
