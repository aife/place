package fr.atexo.urlvie.toolbox.helpers.exceptions;

public class ReadPermissionException extends Exception {

    private static final long serialVersionUID = -2933224616805143258L;

    public ReadPermissionException() {
        super();
    }

    public ReadPermissionException(String message) {
        super(message);
    }

    public ReadPermissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
