package fr.atexo.urlvie.toolbox.testers.validators.impl;

import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.SansParametre;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.utils.ParametrageUtils;
import fr.atexo.urlvie.toolbox.testers.validators.ValideLesParametres;

public class ValidateurBasic implements ValideLesParametres {

    private static final Log LOG = LogFactory.getLog(ValidateurBasic.class);

    static final String ERROR_PARAM_OBLIGATOIRE = "Le paramètre obligatoire %1$s est absent ou incorrect.";
    static final String WARN_PARAM_FACULTATIF = "Le paramètre facultatif %1$s est présent mais incorrect.";
    static final String AUCUN_PARAMETRE = "Paramètres du test absents";
    static final String VALIDE_SIMPLE = "Validation simple paramètres : OK.";
    static final String CLASS_DESCRIPTIVE_NULL = "Pas d'énumération de description des paramètres.";
    static final String CLASS_DESCRIPTIVE_NOT_ENUM = "Les paramètres doivent être décrit par un enum implémentant l'interface "
            + DecrireUnParametreDeTesteur.class.getCanonicalName();

    public boolean execute(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametres,
            Map<String, Object> parametres) throws ValidationException {
        boolean result = true;
        if (clazzParametres == null) {
            result = false;
            logStatut(result);
            LOG.error(CLASS_DESCRIPTIVE_NULL);
            throw new ValidationException(CLASS_DESCRIPTIVE_NULL);
        } else if (!clazzParametres.isEnum()) {
            result = false;
            logStatut(result);
            LOG.error(CLASS_DESCRIPTIVE_NOT_ENUM);
            throw new ValidationException(CLASS_DESCRIPTIVE_NOT_ENUM);
        }
        if (clazzParametres != SansParametre.class) {
            DecrireUnParametreDeTesteur[] listeDesParametres = ParametrageUtils
                    .extraitListeDesDescripteurs(clazzParametres);
            result = validationSimple(listeDesParametres, parametres);
        }
        logStatut(result);
        return result;
    }

    protected boolean validationSimple(
            DecrireUnParametreDeTesteur[] listeDesParametres,
            Map<String, Object> parametres) throws ValidationException {
        boolean result = parametres != null && !parametres.isEmpty();
        if (result) {
            for (DecrireUnParametreDeTesteur param : listeDesParametres) {
                String paramName = param.toString();
                if (param.isObligatoire()) {
                    result = parametres.containsKey(paramName)
                            && parametres.get(paramName) != null
                            && param.getType() != null
                            && param.getType().isAssignableFrom(
                                    parametres.get(paramName).getClass());
                    if (!result) {
                        String message = String.format(ERROR_PARAM_OBLIGATOIRE,
                                param.toString());
                        LOG.error(message);
                        throw new ValidationException(message);
                    }
                } else if (parametres.containsKey(param.toString())) {
                    result = parametres.get(paramName) != null
                            && param.getType() != null
                            && param.getType().isAssignableFrom(
                                    parametres.get(paramName).getClass());
                    if (!result) {
                        String message = String.format(WARN_PARAM_FACULTATIF,
                                param.toString());
                        LOG.warn(message);
                        throw new ValidationException(message);
                    }
                }
            }
        } else {
            LOG.error(AUCUN_PARAMETRE);
            throw new ValidationException(AUCUN_PARAMETRE);
        }
        if (result) {
            LOG.info(VALIDE_SIMPLE);
        }
        return result;
    }

    private void logStatut(boolean result) {
        LOG.info(String.format(STATUT_VALIDATION, this.getClass(),
                result ? "OK" : "KO"));
    }

}
