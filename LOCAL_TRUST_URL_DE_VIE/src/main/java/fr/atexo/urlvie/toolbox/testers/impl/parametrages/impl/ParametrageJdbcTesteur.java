package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageJdbcTesteur implements DecrireUnParametreDeTesteur {
    DRIVER(false, String.class),
    URL(true, String.class),
    USER(true, String.class),
    PASSWORD(true, String.class),
    SQL(true, String.class),
    /**
     * Puisqu'on ne teste que la possibilité d'une connexion effective à la
     * base, inutile d'attendre une réponse précise. Ce paramètre ne doit pas
     * exister.
     */
    REPONSE_ATTENDUE(false, null);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageJdbcTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return returnReponseAttendue();
    }

    private static DecrireUnParametreDeTesteur returnReponseAttendue() {
        return REPONSE_ATTENDUE;
    }

}
