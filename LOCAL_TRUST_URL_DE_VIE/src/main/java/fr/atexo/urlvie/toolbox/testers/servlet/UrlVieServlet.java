package fr.atexo.urlvie.toolbox.testers.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;

import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;

public abstract class UrlVieServlet extends HttpServlet {

    private static final long serialVersionUID = -6649343425761325416L;

    protected UrlVieTesteur testeur;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String result = testeur.lanceTest();
        resp.setContentType(MediaType.APPLICATION_XML.toString());
        resp.getWriter().write(result);
    }

    public void setTesteur(UrlVieTesteur testeur) {
        this.testeur = testeur;
    }

}
