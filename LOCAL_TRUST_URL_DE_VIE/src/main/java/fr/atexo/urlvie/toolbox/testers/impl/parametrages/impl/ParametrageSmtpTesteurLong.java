package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageSmtpTesteurLong
        implements
        DecrireUnParametreDeTesteur {
    HOTE(true, String.class),
    PORT(true, String.class),
    TLS(true, String.class),
    SSL(true, String.class),
    USER(true, String.class),
    PASSWORD(true, String.class),
    EXP(true, String.class),
    DEST(true, String.class),
    SUJET(true, String.class),
    TXT(true, String.class);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageSmtpTesteurLong(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return null;
    }

}
