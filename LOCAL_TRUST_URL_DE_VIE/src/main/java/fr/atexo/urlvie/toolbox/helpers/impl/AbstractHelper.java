package fr.atexo.urlvie.toolbox.helpers.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.helpers.FabriquerUneReponseBrute;
import fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

public abstract class AbstractHelper implements FabriquerUneReponseBrute {

    protected final Log log = LogFactory.getLog(this.getClass());
    protected CalculateurStatut calculateurStatut = new CalculateurStatut();
    private static final String TEST_NON_EFFECTUE = "Requête de test non envoyée pour cause de paramètres invalides.";
    public void execute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        if (reponseBrute.isParametresValides()) {
            effectueTestEtRemplitReponseBrute(reponseBrute, parametres);
        } else {
            log.warn(TEST_NON_EFFECTUE);
        }
        log.info("Calcul du résultat du test.");
        calculeStatutDuTest(reponseBrute, parametres);
    }

    protected abstract void effectueTestEtRemplitReponseBrute(
            ReponseBrute reponseBrute, Map<String, Object> parametres);

    protected void calculeStatutDuTest(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        calculateurStatut.execute(reponseBrute, parametres);
    }

    public void setCalculateurStatut(CalculateurStatut calculateurStatut) {
        this.calculateurStatut = calculateurStatut;
    }

}
