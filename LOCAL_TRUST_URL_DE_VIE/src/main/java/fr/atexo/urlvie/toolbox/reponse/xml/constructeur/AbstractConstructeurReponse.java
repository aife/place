/**
 *
 */
package fr.atexo.urlvie.toolbox.reponse.xml.constructeur;

import fr.atexo.commun.jaxb.JAXBService;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseAuTest;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.GregorianCalendar;

/**
 * Classe dédiée au traitement de {@link ReponseBrute} afin de créer un xml
 * compilant ses informations. <br/>
 * La fabrication du xml fait intervenir l'API JAXB.
 *
 * @author Cyril DUBY
 */
public abstract class AbstractConstructeurReponse implements ConstruireDocument {

    protected Log log = LogFactory.getLog(this.getClass());

    protected static ObjectFactory factory = new ObjectFactory();

    /**
     * Récupère de l'objet {@link ReponseBrute} les informations nécessaire et
     * fabrique un document XML contenu dans la chaîne de caractère retournée.
     *
     * @param reponseBrute
     * @return le xml décrivant le résultat du test
     */
    public String transformeEnChaineContenantUnXml(ReponseBrute reponseBrute) {
        Test tempTest = resumeLeTest(reponseBrute);
        return transformeEnXml(tempTest);
    }

    /**
     * Extrait les informations de l'objet {@link ReponseBrute} pour fabriquer
     * la représentation Java de l'objet {@link Test}. Les détails (les
     * paramètres, la liste des sous-tests) sont remplis par la méthode
     * {@link #remplitDetails(DetailsDuTest, ReponseBrute)}.
     *
     * @param reponseBrute
     * @return l'objet {@link Test} qui servira à la fabrication du XML.
     */
    protected Test resumeLeTest(ReponseBrute reponseBrute) {
        Test result = factory.createTest();
        identifieLeTest(result, reponseBrute);
        dateLeTest(result, reponseBrute);
        logLesExceptions(result, reponseBrute);
        logLesDetails(result, reponseBrute);
        inscritStatutEtMessage(result, reponseBrute);
        return result;
    }

    /**
     * Insère les éléments : type du sujet du test (application, module...) et
     * son nom.
     *
     * @param result
     * @param reponseBrute
     */
    protected void identifieLeTest(Test result, ReponseBrute reponseBrute) {
        result.setSujetDuTest(reponseBrute.getNomCibleTest());
        result.setTypeDuSujet(TypeDuSujet.valueOf(reponseBrute
                .getTypeCibleTest().toString()));
    }

    /**
     * Insère la date du début du test.
     *
     * @param result
     * @param reponseBrute
     */
    protected void dateLeTest(Test result, ReponseBrute reponseBrute) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(reponseBrute.getDateDebutTest());
        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
        }
        result.setDateDebut(xmlCal);
    }

    /**
     * Insère les exceptions interceptées lors de l'exécution du test.
     *
     * @param result
     * @param reponseBrute
     */
    protected void logLesExceptions(Test result, ReponseBrute reponseBrute) {
        ListeDExceptions exceptions = factory.createListeDExceptions();
        remplitExceptions(exceptions, reponseBrute);
        result.setExceptions(exceptions);
    }

    /**
     * Insère ce qui concerne les sous-tests ou les paramètres du test.
     *
     * @param result
     * @param reponseBrute
     */
    protected void logLesDetails(Test result, ReponseBrute reponseBrute) {
        DetailsDuTest details = factory.createDetailsDuTest();
        remplitDetails(details, reponseBrute);
        result.setDetailsTest(details);
    }

    /**
     * Insère la stack trace de chaque exception rencontré au cours du test.
     *
     * @param exceptions
     * @param reponseBrute
     */
    protected void remplitExceptions(ListeDExceptions exceptions,
                                     ReponseBrute reponseBrute) {
        for (Exception exception : reponseBrute.getExceptionsInterceptees()) {
            OutputStream os = new ByteArrayOutputStream();
            PrintWriter ps = new PrintWriter(os);
            exception.printStackTrace(ps);
            ps.flush();
            exceptions.getException().add(os.toString());
            try {
                os.close();
            } catch (IOException e) {
                log.error(e.getMessage());
                log.debug(e.getMessage(), e);
            }
        }
    }

    /**
     * Cette méthode se charge d'insérer les paramètres ou les sous-tests dans
     * l'objet {@link Test}. Son implémentation dépend donc entièrement du
     * paramétrage du test.
     *
     * @param details
     * @param reponseBrute
     */
    protected abstract void remplitDetails(DetailsDuTest details,
                                           ReponseBrute reponseBrute);

    protected void inscritStatutEtMessage(Test result, ReponseBrute reponseBrute) {
        ReponseAuTest statutDuTest = reponseBrute.getStatutDuTest();
        switch (statutDuTest) {
            case OK:
                result.setStatut(Status.OK);
                break;
            case KO:
            case KO_PARAM:
                result.setStatut(Status.KO);
                break;
        }

        result.setMessage(statutDuTest.getDescription(reponseBrute
                .getTesteurClazz()));

    }

    /**
     * Effectue la transformation POJO -> XML.
     *
     * @param tempTest
     * @return le document XML sous forme de chaîne de caractères
     */
    protected String transformeEnXml(Test tempTest) {
        String result = null;
        JAXBElement<Test> test = factory.createTest(tempTest);
        result = JAXBService.instance().getAsString(test, ObjectFactory.class.getPackage().getName());
        log.info("XML RESULTANT :\n" + result);
        return result;
    }

    public void setFactory(ObjectFactory factory) {
        AbstractConstructeurReponse.factory = factory;
    }
}
