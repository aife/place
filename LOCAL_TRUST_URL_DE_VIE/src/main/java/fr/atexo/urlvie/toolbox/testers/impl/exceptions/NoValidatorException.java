package fr.atexo.urlvie.toolbox.testers.impl.exceptions;

public class NoValidatorException extends Exception {

    private static final long serialVersionUID = -5358462431175244347L;

    public NoValidatorException(String message) {
        super(message);
    }

}
