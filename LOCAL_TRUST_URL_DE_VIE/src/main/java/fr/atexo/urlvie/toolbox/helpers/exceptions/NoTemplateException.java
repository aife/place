package fr.atexo.urlvie.toolbox.helpers.exceptions;

public class NoTemplateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6982263308091208244L;

	public NoTemplateException(){
		super();
	}
	
	public NoTemplateException(String message){
		super(message);
	}
}
