/**
 * 
 */
package fr.atexo.urlvie.toolbox.reponse.brute;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.AbstractConstructeurReponse;
import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;

/**
 * Permet de gérer automatiquement la génération de messages à inclure dans les
 * xml générés par les {@link AbstractConstructeurReponse}. Des messages par défaut sont
 * prévus.<br/>
 * Si un fichier properties nommé {@value #BASENAME} est présent dans le
 * classpath, il sera utilisé. Pour chaque implémentation de
 * {@link UrlVieTesteur}, deux messages peuvent être stocké. Les clés de ces
 * messages sont formées du nom de la classe en minuscule suivi de ".ok" ou
 * ".ko".
 * <p>
 * Exemple :<br/>
 * pour la classe (fictive) <code>AbcDefTesteur</code> deux messages peuvent
 * exister :
 * <ul>
 * <li><code>abcdeftesteur.ok</code></li>
 * <li><code>abcdeftesteur.ko</code></li>
 * </ul>
 * 
 * @author Cyril DUBY
 * 
 */
public enum ReponseAuTest {
    OK("Fonctionne correctement"),
    KO("Hors service"),
    KO_PARAM("Paramètres invalides");

    private String defaultDescription;

    private ReponseAuTest(String defDesc) {
        defaultDescription = defDesc;
    }

    private static final Log LOG = LogFactory.getLog(ReponseAuTest.class);
    private static final String BASENAME = "messagesReponsesTests";
    private static final String KEY_TEMPLATE = "%1$s.%2$s";
    private static ResourceBundle resourceBundle = null;

    public String getDescription(Class<? extends UrlVieTesteur> clazz) {
        String keyRoot = clazz.getSimpleName();
        String descriptionKey = String.format(KEY_TEMPLATE, keyRoot,
                this.toString()).toLowerCase();
        return getMessage(descriptionKey);
    }

    private String getMessage(String key) {
        String result = defaultDescription;
        try {
            if (resourceBundle == null) {
                resourceBundle = ResourceBundle.getBundle(BASENAME);
            }
            result = resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            LOG.warn(e.getMessage());
        }
        return result;
    }

}
