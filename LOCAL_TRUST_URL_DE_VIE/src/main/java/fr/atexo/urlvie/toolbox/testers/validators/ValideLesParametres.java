package fr.atexo.urlvie.toolbox.testers.validators;

import java.util.Map;

import javax.xml.bind.ValidationException;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

/**
 * Cette interface définit le comportement d'un validateur de paramètres.
 * 
 * @author Cyril DUBY
 * 
 */
public interface ValideLesParametres {
    String STATUT_VALIDATION = "Validateur : %1$s - Validation des parametres : %2$s";
    String TESTEUR_RECONNU = "Testeur validé nommé : %1$s";

    /**
     * Déclenche la validation des paramètres.
     * 
     * @param clazzParametres
     *            la classe de l'enum décrivant les paramètres consommés par le
     *            test.
     * @param parametres
     *            le paramétrage du test.
     * @return <code>true</code> si la validation s'est bien déroulée.
     * @throws ValidationException
     *             si les paramètres ne sont pas valides. Cette méthode n'est
     *             donc pas censée renvoyer <code>false</code>.
     */
    boolean execute(Class<? extends DecrireUnParametreDeTesteur> clazzParametres,
            Map<String, Object> parametres) throws ValidationException;
}
