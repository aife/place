/**
 * 
 */
package fr.atexo.urlvie.toolbox.testers.impl.parametrages;

/**
 * Cette interface définit un cadre permettant une vérification basique
 * automatique des paramètres passés à un testeur.
 * 
 * @author Cyril DUBY
 * 
 */
public interface DecrireUnParametreDeTesteur {

    /**
     * @return <code>true</code> si le paramètre décrit par l'instance doit être
     *         renseigné.
     */
    boolean isObligatoire();

    /**
     * @return le type attendu du paramètre.
     */
    Class<?> getType();

    /**
     * Cette méthode a le comportement d'une méthode <code>static</code>. Quel
     * que soit l'élément qui reçoit ce message, il retourne l'instance qui
     * décrit la réponse attendue.
     * 
     * @return l'instance qui décrit la réponse attendue.
     */
    DecrireUnParametreDeTesteur getParametreReponseAttendue();
}
