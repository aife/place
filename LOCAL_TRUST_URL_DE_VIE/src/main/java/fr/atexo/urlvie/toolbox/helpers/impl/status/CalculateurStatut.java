package fr.atexo.urlvie.toolbox.helpers.impl.status;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseAuTest;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

/**
 * Implémentation basique.
 */
public class CalculateurStatut {

    protected final Log log = LogFactory.getLog(this.getClass());

    /**
     * Implémentation basique.<br/>
     * Statut renvoyé :
     * <ul>
     * <li>{@link ReponseAuTest#KO_PARAM} : si
     * {@link ReponseBrute#isParametresValides()} == <code>false</code></li>
     * <li>{@link ReponseAuTest#KO} : si
     * {@link ReponseBrute#getExceptionsInterceptees()} n'est pas vide</li>
     * <li>{@link ReponseAuTest#OK} : dans les autres cas.</li>
     * </ul>
     * 
     * @param reponseBrute
     * @param parametres
     * 
     */
    public void execute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        ReponseAuTest statut = null;
        if (reponseBrute.isParametresValides()) {
            statut = traitementSiParametresValides(reponseBrute, parametres);
        } else {
            statut = ReponseAuTest.KO_PARAM;
        }
        reponseBrute.setStatutDuTest(statut);
    }

    protected ReponseAuTest traitementSiParametresValides(
            ReponseBrute reponseBrute, Map<String, Object> parametres) {
        ReponseAuTest result = null;
        if (reponseBrute.getExceptionsInterceptees().isEmpty()) {
            result = ReponseAuTest.OK;
        } else {
            result = ReponseAuTest.KO;
        }
        return result;
    }
}
