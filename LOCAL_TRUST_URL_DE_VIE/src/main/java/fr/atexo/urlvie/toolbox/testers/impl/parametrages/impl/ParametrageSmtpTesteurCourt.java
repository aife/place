package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.commun.email.Email;
import fr.atexo.commun.email.ServeurSmtpConnexion;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageSmtpTesteurCourt implements DecrireUnParametreDeTesteur {
    SMTP_CONNECTION(true, ServeurSmtpConnexion.class),
    EMAIL(true, Email.class);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageSmtpTesteurCourt(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return null;
    }

}
