package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageLdapTesteur implements DecrireUnParametreDeTesteur {
    LDAP_URL(true, String.class),
    LOGIN(true, String.class),
    PASSWORD(true, String.class),
    LDAP_BASE(true, String.class),
    NOM_OBJET_RECHERCHE(true, String.class),
    QUERY(true, String.class),
    /**
     * Puisqu'on ne teste que la possibilité d'une connexion effective au LDAP,
     * inutile d'attendre une réponse précise. Ce paramètre ne doit pas exister.
     */
    REPONSE_ATTENDUE(false, null);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageLdapTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return returnReponseAttendue();
    }

    private static DecrireUnParametreDeTesteur returnReponseAttendue() {
        return REPONSE_ATTENDUE;
    }

}
