package fr.atexo.urlvie.toolbox.testers.impl.parametrages.utils;

import java.lang.reflect.Method;

import javax.xml.bind.ValidationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public final class ParametrageUtils {
    private static final String EXTRACTION_INSTANCE_IMPOSSIBLE = "Impossible d'extraire l'instance : %1$s de l'enum %2$s";

    private static final Log LOG = LogFactory.getLog(ParametrageUtils.class);

    private ParametrageUtils() {
    }
    
    public static DecrireUnParametreDeTesteur[] extraitListeDesDescripteurs(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametres)
            throws ValidationException {
        DecrireUnParametreDeTesteur[] result = null;
        try {
            Method valuesMethod = clazzParametres.getMethod("values",
                    (Class<?>[]) null);
            result = (DecrireUnParametreDeTesteur[]) valuesMethod.invoke(
                    clazzParametres, (Object[]) null);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            LOG.debug(e.getMessage(), e);
            throw new ValidationException(e);
        }
        return result;
    }

    public static DecrireUnParametreDeTesteur extraitInstance(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametres,
            String nomInstance) {
        DecrireUnParametreDeTesteur result = null;
        try {
            for (DecrireUnParametreDeTesteur param : extraitListeDesDescripteurs(clazzParametres)) {
                if (param.toString().equals(nomInstance)) {
                    result = param;
                    break;
                }
            }
        } catch (ValidationException e) {
            LOG.error(String.format(EXTRACTION_INSTANCE_IMPOSSIBLE,
                    nomInstance, clazzParametres));
            LOG.debug(e.getMessage(), e);
        }
        return result; 
    }

    public static DecrireUnParametreDeTesteur determinePremierParametreObligatoire(
            Class<? extends DecrireUnParametreDeTesteur> clazz) {
        DecrireUnParametreDeTesteur result = null;
        try {
            DecrireUnParametreDeTesteur[] values = extraitListeDesDescripteurs(clazz);
            for (DecrireUnParametreDeTesteur value : values) {
                if (value.isObligatoire()) {
                    result = value;
                    break;
                }
            }
        } catch (ValidationException e) {
            LOG.error(e.getMessage());
            LOG.debug(e.getMessage(), e.fillInStackTrace());
        }
        return result;
    }

}
