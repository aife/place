package fr.atexo.urlvie.toolbox.testers.impl.exceptions;

public class NoXmlGeneratedException extends Exception {

    private static final long serialVersionUID = -8610453490833457024L;

    public NoXmlGeneratedException(String message) {
        super(message);
    }

}
