package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import java.util.Map;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageHttpTesteur implements DecrireUnParametreDeTesteur {
    URL(true, String.class),
    GET(false, Map.class),
    POST(false, Map.class),
    REPONSE_ATTENDUE(false, String.class);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageHttpTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return returnReponseAttendue();
    }

    private static DecrireUnParametreDeTesteur returnReponseAttendue() {
        return REPONSE_ATTENDUE;
    }

}
