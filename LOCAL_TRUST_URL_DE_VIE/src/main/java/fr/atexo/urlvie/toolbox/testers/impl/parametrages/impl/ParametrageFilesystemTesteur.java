package fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;

public enum ParametrageFilesystemTesteur implements DecrireUnParametreDeTesteur {
    PATH(true, String.class),
    READ(true, String.class),
    WRITE(true, String.class);

    private boolean obligatoire;
    private Class<?> type;

    private ParametrageFilesystemTesteur(boolean obligatoire, Class<?> type) {
        this.obligatoire = obligatoire;
        this.type = type;
    }

    public boolean isObligatoire() {
        return obligatoire;
    }

    public Class<?> getType() {
        return type;
    }

    public DecrireUnParametreDeTesteur getParametreReponseAttendue() {
        return null;
    }

}
