package fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs.impl;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs.DeconstructeurXmlVersObject;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.ObjectFactory;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.Test;

public class DeconstructeurXmlVersTest implements
        DeconstructeurXmlVersObject<Test> {

    private static final Log LOG = LogFactory
            .getLog(DeconstructeurXmlVersTest.class);

    @SuppressWarnings({ "unchecked" })
    public JAXBElement<Test> unmarshall(String xml) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(ObjectFactory.class
                .getPackage().getName());
        Unmarshaller unmarshaller = context.createUnmarshaller();
        byte[] byteArray = null;
        try{
            byteArray = xml.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.info(e.getMessage(), e.fillInStackTrace());
            byteArray = xml.getBytes();
        }
        return (JAXBElement<Test>) unmarshaller
                .unmarshal(new ByteArrayInputStream(byteArray));
    }
}
