/**
 * 
 */
package fr.atexo.urlvie.toolbox.reponse.brute;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.atexo.urlvie.toolbox.helpers.FabriquerUneReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.AbstractConstructeurReponse;
import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;

/**
 * Résume l'ensemble d'un test effectué. regroupe les paramètres du test aussi
 * bien que les réponses reçues ou les exceptions générées lors de l'exécution,
 * et le staut du test.
 * 
 * @author Cyril DUBY
 * 
 */
public class ReponseBrute {
    /**
     * Permet de convoyer jusqu'au {@link AbstractConstructeurReponse} le nom du
     * module ou de l'application pour qui le test représente l'accès à une
     * ressource.
     */
    private String nomCibleTest = null;
    private TypeCibleTest typeCibleTest = null;
    private Date dateDebutTest = null;
    private boolean parametresValides = false;
    private ReponseAuTest statutDuTest = null;
    private Class<? extends UrlVieTesteur> testeurClazz = null;
    private List<Object> parametresUtilises = new ArrayList<Object>();
    private List<Object> elementsRecus = new ArrayList<Object>();
    private List<Exception> exceptionsInterceptees = new ArrayList<Exception>();

    public void setNomCibleTest(String nomCibleTest) {
        this.nomCibleTest = nomCibleTest;
    }

    public String getNomCibleTest() {
        return nomCibleTest;
    }

    public void setDateDebutTest(Date dateDebutTest) {
        this.dateDebutTest = dateDebutTest;
    }

    public Date getDateDebutTest() {
        return dateDebutTest;
    }

    /**
     * Cette méthode n'est destinée qu'à la génération finale (XML) de la
     * réponse. Puisque certains paramètres pourraient ne pas être destinés à
     * être "loggés", il ne faut pas considérer cette méthode comme un moyen
     * fiable pour accéder aux paramètres nécessaires au test.
     * 
     * @return Ne renvoie jamais une liste <code>null</code>
     */
    public List<Object> getParametresUtilises() {
        return parametresUtilises;
    }

    /**
     * @return Ne renvoie jamais une liste <code>null</code>
     */
    public List<Object> getElementsRecus() {
        return elementsRecus;
    }

    /**
     * @return Ne renvoie jamais une liste <code>null</code>
     */
    public List<Exception> getExceptionsInterceptees() {
        return exceptionsInterceptees;
    }

    /**
     * Méthode destinée aux classes qui implémentent
     * {@link FabriquerUneReponseBrute}.
     * 
     * @param statutDuTest
     */
    public void setStatutDuTest(ReponseAuTest statutDuTest) {
        this.statutDuTest = statutDuTest;
    }

    public ReponseAuTest getStatutDuTest() {
        return statutDuTest;
    }

    /**
     * Méthode destinée aux classes qui implémentent {@link UrlVieTesteur} afin
     * de fournir des informations humainement lisibles pour identifier le
     * niveau du test.
     * 
     * @param typeCibleTest
     */
    public void setTypeCibleTest(TypeCibleTest typeCibleTest) {
        this.typeCibleTest = typeCibleTest;
    }

    public void setTypeCibleTest(String typeCibleTest) {
        this.typeCibleTest = TypeCibleTest.valueOf(typeCibleTest.toUpperCase());
    }

    public TypeCibleTest getTypeCibleTest() {
        return typeCibleTest;
    }

    public void setTesteurClazz(Class<? extends UrlVieTesteur> testeurClazz) {
        this.testeurClazz = testeurClazz;
    }

    public Class<? extends UrlVieTesteur> getTesteurClazz() {
        return testeurClazz;
    }

    public void setParametresValides(boolean parametresValides) {
        this.parametresValides = parametresValides;
    }

    public boolean isParametresValides() {
        return parametresValides;
    }
}
