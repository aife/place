package fr.atexo.urlvie.toolbox.helpers.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import fr.atexo.urlvie.toolbox.helpers.impl.status.impl.ListeTestCalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs.DeconstructeurXmlVersObject;
import fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs.impl.DeconstructeurXmlVersTest;
import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.exceptions.NoXmlGeneratedException;

public class ListeTestsHelper extends AbstractHelper {

    protected static final String NOT_GENERATED = "Pas de génération de xml par le testeur de classe :%1$s ayant pour cible : %2$s.";

    protected static final String NOT_UNMARSHALLED = "Désérialisation des résultats impossible - cause : %1$s";

    protected static final String RESULTAT_NULL = "Un résultat de test semble être revenu null - resultat %1$d / %2$d";

    private DeconstructeurXmlVersObject<?> deconstructeur;

    public ListeTestsHelper() {
        calculateurStatut = new ListeTestCalculateurStatut();
        deconstructeur = new DeconstructeurXmlVersTest();
    }

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        Collection<Object> listeTests = parametres.values();
        List<Exception> exceptions = reponseBrute.getExceptionsInterceptees();
        List<String> listeResultats = recupereLesResultatsDesTest(listeTests,
                exceptions);
        List<Object> listeResultatEnTest = null;
        try {
            listeResultatEnTest = unmarshall(listeResultats);
        } catch (Exception e) {
            log.error(String.format(NOT_UNMARSHALLED, e.getMessage()));
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        }
        if (listeResultatEnTest != null) {
            reponseBrute.getElementsRecus().addAll(listeResultatEnTest);
        }
    }

    protected List<String> recupereLesResultatsDesTest(
            Collection<Object> listeTests, List<Exception> exceptions) {
        List<String> listeResultats = new ArrayList<String>();
        for (Object testeur : listeTests) {
            String xml = ((UrlVieTesteur) testeur).lanceTest();
            if (xml == null || xml.trim().length() == 0) {
                String message = String.format(NOT_GENERATED, testeur
                        .getClass().getCanonicalName(),
                        ((UrlVieTesteur) testeur).getNomCibleDuTest());
                log.info(message);
                exceptions.add(new NoXmlGeneratedException(message));
            }
            listeResultats.add(xml);
        }
        return listeResultats;
    }

    protected List<Object> unmarshall(List<String> listeResultats)
            throws JAXBException {
        List<Object> result = new ArrayList<Object>();
        for (int index = 0; index < listeResultats.size(); index++) {
            String xml = listeResultats.get(index);
            if (xml != null) {
                result.add(deconstructeur.unmarshall(xml).getValue());
            } else {
                log.warn(String.format(RESULTAT_NULL, index + 1,
                        listeResultats.size()));
            }
        }
        return result;
    }

    public void setDeconstructeur(DeconstructeurXmlVersObject<?> deconstructeur) {
        this.deconstructeur = deconstructeur;
    }

}
