package fr.atexo.urlvie.toolbox.helpers.exceptions;

public class NoResultException extends Exception {

    private static final long serialVersionUID = -2933224616805143258L;

    public NoResultException() {
        super();
    }

    public NoResultException(String message) {
        super(message);
    }

}
