package fr.atexo.urlvie.toolbox.helpers.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import fr.atexo.commun.document.administration.commun.ChampFusionDTO;
import fr.atexo.commun.document.administration.commun.DocumentDTO;
import fr.atexo.commun.document.administration.commun.DocumentModeleDTO;
import fr.atexo.commun.document.administration.commun.FichierDTO;
import fr.atexo.commun.document.administration.commun.TypeDocumentModele;
import fr.atexo.commun.document.administration.commun.TypeFormat;
import fr.atexo.commun.document.generation.service.AtexoDocumentGenerationException;
import fr.atexo.commun.document.generation.service.openoffice.OpenOfficeGenerationConnexion;
import fr.atexo.commun.document.generation.service.openoffice.OpenOfficeService;
import fr.atexo.urlvie.toolbox.helpers.exceptions.NoResultException;
import fr.atexo.urlvie.toolbox.helpers.exceptions.NoTemplateException;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageOpenOfficeTesteur;

public class OpenOfficeHelper extends AbstractHelper {

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
                                                     Map<String, Object> parametres) {
        String openOfficeServiceHost = (String) parametres
                .get(ParametrageOpenOfficeTesteur.OPENOFFICE_SERVICEHOST
                        .toString());
        String openOfficePort = (String) parametres
                .get(ParametrageOpenOfficeTesteur.OPENOFFICE_PORT.toString());
        String repertoireTemporaire = (String) parametres
                .get(ParametrageOpenOfficeTesteur.OPENOFFICE_REPERTOIRE_TEMPORAIRE
                        .toString());
        String cheminFichierTemplate = (String) parametres
                .get(ParametrageOpenOfficeTesteur.OPENOFFICE_CHEMIN_TEMPLATE
                        .toString());

        getReponse(reponseBrute, openOfficeServiceHost, openOfficePort,
                repertoireTemporaire, cheminFichierTemplate);
    }

    public void getReponse(ReponseBrute reponseBrute,
                           String openOfficeServiceHost, String openOfficePort,
                           String repertoireTemporaire, String cheminFichierTemplate) {

        OpenOfficeService openOfficeService = null;
        File fileForDTO = new File(cheminFichierTemplate);
        InputStream inputStream = null;
        File tmp = new File(cheminFichierTemplate + System.currentTimeMillis());
        try {
            if (!fileForDTO.exists()) {
                throw new NoTemplateException(
                        "La template de test est absente ou mal parametrée");
            }
            openOfficeService = new OpenOfficeService(openOfficeServiceHost, openOfficePort, repertoireTemporaire);
            DocumentModeleDTO documentModeleDTO = new DocumentModeleDTO();
            documentModeleDTO.setTypeDocument(TypeDocumentModele.Document);
            documentModeleDTO.setTypeFormat(TypeFormat.ODT);
            if (tmp.exists()) {
                FileUtils.forceDelete(tmp);
            }
            FileUtils.copyFile(fileForDTO, tmp);
            DocumentDTO documentDTO = new DocumentDTO(documentModeleDTO,
                    tmp.getName(), new ArrayList<ChampFusionDTO>());
            FichierDTO fichierDTO = new FichierDTO(fileForDTO);
            int tailleDocument = 0;
            inputStream = openOfficeService.genererDocument(documentDTO,
                    fichierDTO);
            while (inputStream.read() != -1) {
                tailleDocument++;
            }
            if (tailleDocument <= 0) {
                throw new NoResultException("Le document generé est vide.");
            }
        } catch (AtexoDocumentGenerationException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                    log.debug(e.getMessage(), e);
                    reponseBrute.getExceptionsInterceptees().add(e);
                }
            }
            if (tmp.exists()) {
                FileUtils.deleteQuietly(tmp);
            }
        }
    }

}
