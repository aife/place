package fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.AbstractConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.DetailsDuTest;
import fr.atexo.urlvie.toolbox.reponse.xml.representation.Test;

public class ListeTestsConstructeurReponse extends AbstractConstructeurReponse {

    @Override
    protected void remplitDetails(DetailsDuTest details,
            ReponseBrute reponseBrute) {
        if (details.getListeDeSousTests() == null) {
            details.setListeDeSousTests(factory.createListeDeSousTests());
        }
        for (Object testObject : reponseBrute.getElementsRecus()) {
            Test test = (Test) testObject;
            details.getListeDeSousTests().getSoustest().add(test);
        }
    }

}
