package fr.atexo.urlvie.toolbox.testers;

import fr.atexo.urlvie.toolbox.testers.impl.UrlVieTesteurImpl;

/**
 * Pour lancer un test dit d'URL de vie, il suffit de paramétrer correctement
 * l'instance implémentant cette interface et d'appeler sa méthode
 * {@link #lanceTest()}. <br/>
 * 
 * @see UrlVieTesteurImpl
 * @author Cyril DUBY
 * 
 */
public interface UrlVieTesteur {

    String MESSAGE_RESULTAT = "RESULTAT DU TEST : %1$s.";

    /**
     * Déroule le processus global du test.
     * 
     * @return le xml résumant le déroulement du test.
     */
    String lanceTest();

    /**
     * @return l'identifiant de l'application ou du module testé.
     */
    String getNomCibleDuTest();
}
