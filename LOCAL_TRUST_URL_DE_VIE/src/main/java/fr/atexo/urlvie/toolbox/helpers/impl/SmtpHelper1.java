/**
 * 
 */
package fr.atexo.urlvie.toolbox.helpers.impl;

import java.util.Map;

import org.apache.commons.mail.EmailException;

import fr.atexo.commun.email.Email;
import fr.atexo.commun.email.EmailService;
import fr.atexo.commun.email.EmailServiceImpl;
import fr.atexo.commun.email.ServeurSmtpConnexion;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageSmtpTesteurCourt;

/**
 * @author Cyril DUBY
 * 
 */
public class SmtpHelper1 extends AbstractHelper {

    /*
     * (non-Javadoc)
     * 
     * @see fr.atexo.urlvie.toolbox.helpers.impl.AbstractHelper#
     * effectueTestEtRemplitReponseBrute
     * (fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute, java.util.Map)
     */
    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        try {
            ServeurSmtpConnexion cnx = (ServeurSmtpConnexion) parametres
                    .get(ParametrageSmtpTesteurCourt.SMTP_CONNECTION
                            .toString());
            Email email = (Email) parametres
                    .get(ParametrageSmtpTesteurCourt.EMAIL
                    .toString());
            EmailService service = new EmailServiceImpl();
            service.envoyerEmails(cnx, email);
        } catch (EmailException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        }
    }
}
