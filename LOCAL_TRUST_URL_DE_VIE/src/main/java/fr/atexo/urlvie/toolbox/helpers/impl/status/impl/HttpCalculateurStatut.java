package fr.atexo.urlvie.toolbox.helpers.impl.status.impl;

import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.exceptions.HttpCodeException;
import fr.atexo.urlvie.toolbox.helpers.exceptions.NoResultException;
import fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseAuTest;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

public class HttpCalculateurStatut extends CalculateurStatut {

    public static final String CODE_RETOUR_INVALIDE = "Code retour : %1$d";
    public static final int CODE_RETOUR_DEFAULT = -1;
    public static final int CODE_RETOUR_VALIDE = 200;

    /**
     * Cette implémentation ne récupère pas dans les paramètres un élément de
     * comparaison, elle vérifie juste que le code de retour passé comme en
     * premier dans les éléments reçus, est bien 200.
     * 
     * @see fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut#execute(fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute,
     *      java.util.Map)
     */
    @Override
    protected ReponseAuTest traitementSiParametresValides(
            ReponseBrute reponseBrute, Map<String, Object> parametres) {
        ReponseAuTest result = ReponseAuTest.KO;
        try {
            int codeRetour = CODE_RETOUR_DEFAULT;
            if (!reponseBrute.getElementsRecus().isEmpty()) {
                codeRetour = (Integer) reponseBrute.getElementsRecus().get(0);
                if (codeRetour == CODE_RETOUR_VALIDE) {
                    result = ReponseAuTest.OK;
                }
                if (result.equals(ReponseAuTest.KO)) {
                    throw new HttpCodeException(String.format(
                            CODE_RETOUR_INVALIDE, codeRetour));
                }
            } else {
                throw new NoResultException(String.format(CODE_RETOUR_INVALIDE,
                        codeRetour));
            }
        } catch (HttpCodeException e) {
            reponseBrute.getExceptionsInterceptees().add(e);
        } catch (NoResultException e) {
            reponseBrute.getExceptionsInterceptees().add(e);
        }
        return result;
    }
}
