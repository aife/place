package fr.atexo.urlvie.toolbox.helpers.impl;

import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageLdapTesteur;

public class LdapHelper extends AbstractHelper {

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        String base = (String) parametres.get(ParametrageLdapTesteur.LDAP_BASE
                .toString());
        String url = (String) parametres.get(ParametrageLdapTesteur.LDAP_URL
                .toString());
        String login = (String) parametres.get(ParametrageLdapTesteur.LOGIN
                .toString());
        String password = (String) parametres
                .get(ParametrageLdapTesteur.PASSWORD.toString());
        String query = (String) parametres.get(ParametrageLdapTesteur.QUERY
                .toString());
        String nom = (String) parametres
                .get(ParametrageLdapTesteur.NOM_OBJET_RECHERCHE.toString());

        getReponse(reponseBrute, url, login, password, base, nom, query);
    }

    public void getReponse(ReponseBrute reponseBrute, String ldapUrl,
            String login, String motDePasse, String ldapBase,
            String nomObjetRecherche, String query) {
        Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapUrl);
        env.put(Context.SECURITY_PRINCIPAL, login);
        env.put(Context.SECURITY_CREDENTIALS, motDePasse);
        InitialDirContext ctx = null;
        try {
            ctx = new InitialDirContext(env);
            SearchControls ctr = new SearchControls();
            ctr.setSearchScope(SearchControls.SUBTREE_SCOPE);
            ctx.search(nomObjetRecherche + "," + ldapBase, query, ctr);
        } catch (NamingException e) {
            log.error(e.getMessage());
            log.debug(e.getMessage(), e);
            reponseBrute.getExceptionsInterceptees().add(e);
        } finally {
            try {
                ctx.close();
            } catch (NamingException e) {
                log.error(e.getMessage());
                log.debug(e.getMessage(), e);
                reponseBrute.getExceptionsInterceptees().add(e);
            }
        }
    }
}
