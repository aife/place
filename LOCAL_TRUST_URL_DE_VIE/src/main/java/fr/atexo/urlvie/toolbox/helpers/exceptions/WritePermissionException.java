package fr.atexo.urlvie.toolbox.helpers.exceptions;

public class WritePermissionException extends Exception {

    private static final long serialVersionUID = 7219422010860971514L;

    public WritePermissionException() {
        super();
    }

    public WritePermissionException(String message) {
        super(message);
    }

    public WritePermissionException(String message, Throwable cause) {
        super(message, cause);
    }

}
