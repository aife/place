package fr.atexo.urlvie.toolbox.helpers.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.exceptions.ReadPermissionException;
import fr.atexo.urlvie.toolbox.helpers.exceptions.WritePermissionException;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageFilesystemTesteur;

public class FilesystemHelper extends AbstractHelper {

    private static final String PERMISSION_REFUSEE = "Permission refusée pour : %1$s";
    private static final String CHEMIN_INTROUVABLE = "Le chemin n'existe pas : %1$s ";

    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        String path = (String) parametres.get(ParametrageFilesystemTesteur.PATH
                .toString());
        boolean read = Boolean.valueOf((String) parametres
                .get(ParametrageFilesystemTesteur.READ.toString()));
        boolean write = Boolean.valueOf((String) parametres
                .get(ParametrageFilesystemTesteur.WRITE.toString()));
        getReponse(reponseBrute, path, read, write);
    }

    public void getReponse(ReponseBrute reponseBrute, String path,
            boolean read, boolean write) {
        String msgErreur = String.format(PERMISSION_REFUSEE, path);
        try {
            if (path != null) {
                File file = new File(path);
                if (file.exists()) {
                    if (read) {
                        try {
                            if (!file.canRead()) {
                                reponseBrute.getExceptionsInterceptees().add(
                                        new ReadPermissionException(msgErreur));
                            }
                        } catch (SecurityException e) {
                            log.error(e.getMessage());
                            log.debug(e.getMessage(), e);
                            reponseBrute.getExceptionsInterceptees().add(
                                    new ReadPermissionException(msgErreur, e));
                        }
                    }
                    if (write) {
                        try {
                            if (!file.canWrite()) {
                                reponseBrute.getExceptionsInterceptees()
                                        .add(new WritePermissionException(
                                                msgErreur));
                            }
                        } catch (SecurityException e) {
                            log.error(e.getMessage());
                            log.debug(e.getMessage(), e);
                            reponseBrute.getExceptionsInterceptees().add(
                                    new WritePermissionException(msgErreur, e));
                        }
                    }
                } else {
                    throw new FileNotFoundException(String.format(
                            CHEMIN_INTROUVABLE, path));
                }
            }
        } catch (FileNotFoundException e) {
            log.info(e.getMessage());
            reponseBrute.getExceptionsInterceptees().add(e);
        }
    }

}
