package fr.atexo.urlvie.toolbox.helpers.impl.status.impl;

import java.sql.SQLException;
import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseAuTest;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

public class JdbcCalculateurStatut extends CalculateurStatut {

    /**
     * Cette implémentation distingue parmi les exceptions celle qui résulte
     * d'un problème de chargement de driver (qui n'est pas en soi déterminante
     * pour le succès du test), d'une SQLException, qui implique l'échec.
     * 
     * @see fr.atexo.urlvie.toolbox.helpers.impl.status.CalculateurStatut#execute(fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute,
     *      java.util.Map)
     */
    @Override
    protected ReponseAuTest traitementSiParametresValides(
            ReponseBrute reponseBrute, Map<String, Object> parametres) {
        ReponseAuTest result = ReponseAuTest.KO;
        if (reponseBrute.getExceptionsInterceptees().isEmpty()) {
            result = ReponseAuTest.OK;
        } else {
            boolean grave = false;
            for (Throwable exception : reponseBrute.getExceptionsInterceptees()) {
                if (exception instanceof LinkageError
                        || exception instanceof ExceptionInInitializerError
                        || exception instanceof ClassNotFoundException) {
                    grave |= false;
                } else if (exception instanceof SQLException) {
                    grave |= true;
                    break;
                }
            }
            if (!grave) {
                log.info("Erreur de driver sans incidence.");
                result = ReponseAuTest.OK;
            }
        }
        return result;
    }
}
