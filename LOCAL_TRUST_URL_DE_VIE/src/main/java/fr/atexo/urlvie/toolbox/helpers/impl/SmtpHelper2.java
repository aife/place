/**
 * 
 */
package fr.atexo.urlvie.toolbox.helpers.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.atexo.commun.email.Email;
import fr.atexo.commun.email.ServeurSmtpConnexion;
import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageSmtpTesteurCourt;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageSmtpTesteurLong;

/**
 * @author Cyril DUBY
 * 
 */
public class SmtpHelper2 extends SmtpHelper1 {

    /*
     * (non-Javadoc)
     * 
     * @see fr.atexo.urlvie.toolbox.helpers.impl.SmtpHelper1#
     * effectueTestEtRemplitReponseBrute
     * (fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute, java.util.Map)
     */
    @Override
    protected void effectueTestEtRemplitReponseBrute(ReponseBrute reponseBrute,
            Map<String, Object> parametres) {
        ServeurSmtpConnexion cnx = creeSmtpConnexion(parametres);
        Email email = creeEmail(parametres);
        Map<String, Object> parametresSimples = new HashMap<String, Object>();
        parametresSimples.put(
                ParametrageSmtpTesteurCourt.SMTP_CONNECTION.toString(), cnx);
        parametresSimples.put(ParametrageSmtpTesteurCourt.EMAIL.toString(),
                email);
        super.effectueTestEtRemplitReponseBrute(reponseBrute, parametresSimples);
    }

    private ServeurSmtpConnexion creeSmtpConnexion(
            Map<String, Object> parametres) {
        String hote = (String) parametres
                .get(ParametrageSmtpTesteurLong.HOTE.toString());
        Integer port = Integer.valueOf((String) parametres
                .get(ParametrageSmtpTesteurLong.PORT.toString()));
        boolean tls = Boolean.valueOf((String) parametres
                .get(ParametrageSmtpTesteurLong.TLS.toString()));
        boolean ssl = Boolean.valueOf((String) parametres
                .get(ParametrageSmtpTesteurLong.SSL.toString()));
        String utilsateurAuthentification = (String) parametres
                .get(ParametrageSmtpTesteurLong.USER.toString());
        String motDePasseAuthentification = (String) parametres
                .get(ParametrageSmtpTesteurLong.PASSWORD.toString());
        return new ServeurSmtpConnexion(hote, port, tls, ssl,
                utilsateurAuthentification, motDePasseAuthentification);
    }

    private Email creeEmail(Map<String, Object> parametres) {
        String exp = (String) parametres.get(ParametrageSmtpTesteurLong.EXP
                .toString());
        String dest = (String) parametres
                .get(ParametrageSmtpTesteurLong.DEST.toString());
        String sujet = (String) parametres
                .get(ParametrageSmtpTesteurLong.SUJET.toString());
        String txt = (String) parametres.get(ParametrageSmtpTesteurLong.TXT
                .toString());

        Set<String> destinataires = new HashSet<String>();
        destinataires.add(dest);

        return new Email(exp, destinataires, sujet, txt, null);
    }
}
