package fr.atexo.urlvie.toolbox.helpers.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;

import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageHttpTesteur;

/**
 * Enumère et implémente les méthodes HTTP et leurs prises en charge.
 * 
 * @author Cyril DUBY
 * 
 */
public enum MethodeHttpUtilisee {
    GET {
        @Override
        @SuppressWarnings("unchecked")
        public HttpRequestBase construitHttpMethod(Map<String, Object> params) {
            String url = (String) params.get(ParametrageHttpTesteur.URL
                    .toString());
            HttpGet request = new HttpGet(url);
            MethodeHttpUtilisee.setParams(request, (Map<String, String>) params
                    .get(ParametrageHttpTesteur.GET.toString()));
            return request;
        }
    },
    POST {
        /**
         * Dans le cas d'un formulaire, les paramètres doivent apparaître sous
         * la forme de couples clef-valeur dans la {@link Map} associée à la
         * clef {@link MethodeHttpUtilisee}#POST Si le "POST" ne concerne pas un
         * formulaire, les données doivent avoir la forme d'une seule chaine de
         * caractères. La {@link Map} dont la clef est
         * {@link MethodeHttpUtilisee} #POST devra contenir cette chaine de
         * caractère avec pour clef la valeur de {@link MethodeHttpUtilisee}
         * #CLEF_ENTITY.
         * 
         * @see fr.atexo.urlvie.toolbox.helpers.utils.MethodeHttpUtilisee#
         *      construitHttpMethod(java.util.Map)
         */
        @Override
        @SuppressWarnings("unchecked")
        public HttpRequestBase construitHttpMethod(Map<String, Object> params)
                throws UnsupportedEncodingException {
            String url = (String) params.get(ParametrageHttpTesteur.URL
                    .toString());
            HttpPost request = new HttpPost(url);
            MethodeHttpUtilisee.setParams(request, (Map<String, String>) params
                    .get(ParametrageHttpTesteur.GET.toString()));
            if (params != null) {
                Map<String, String> paramMap = (Map<String, String>) params
                        .get(ParametrageHttpTesteur.POST.toString());
                HttpEntity entity = null;
                if (paramMap != null && !paramMap.isEmpty()) {
                    boolean entityStringPresente = paramMap
                            .containsKey(CLEF_ENTITY);
                    if (entityStringPresente) {
                        entity = new StringEntity(paramMap.get(CLEF_ENTITY),
                                "UTF-8");
                        Map<String, String> copieReduite = new HashMap<String, String>(
                                paramMap);
                        if (!copieReduite.isEmpty()) {
                            copieReduite.remove(CLEF_ENTITY);
                            setParams(request, copieReduite);
                        }
                    } else {
                        List<NameValuePair> formparams = new ArrayList<NameValuePair>();
                        for (Entry<String, String> param : paramMap.entrySet()) {
                            String key = param.getKey();
                            formparams.add(new BasicNameValuePair(key, param
                                    .getValue()));
                        }
                        entity = new UrlEncodedFormEntity(formparams, "UTF-8");
                    }
                    request.setEntity(entity);
                }
            }
            return request;
        }
    };

    public static final String CLEF_ENTITY = "ENTITY";

    public abstract HttpRequestBase construitHttpMethod(
            Map<String, Object> params) throws UnsupportedEncodingException;

    private static void setParams(HttpRequestBase request,
            Map<String, String> httpParameters) {
        HttpParams httpParams = request.getParams();
        if (httpParameters != null) {
            for (Entry<String, String> param : httpParameters.entrySet()) {
                httpParams.setParameter(param.getKey(), param.getValue());
            }
        }
    }
}
