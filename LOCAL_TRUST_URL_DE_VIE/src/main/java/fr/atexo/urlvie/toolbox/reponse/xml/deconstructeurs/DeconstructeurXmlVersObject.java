package fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

public interface DeconstructeurXmlVersObject<T> {
    JAXBElement<T> unmarshall(String xml) throws JAXBException;
}
