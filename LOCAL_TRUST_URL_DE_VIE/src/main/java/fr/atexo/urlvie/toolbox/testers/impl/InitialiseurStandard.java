package fr.atexo.urlvie.toolbox.testers.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.helpers.FabriquerUneReponseBrute;
import fr.atexo.urlvie.toolbox.helpers.impl.FilesystemHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.HttpHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.JdbcHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.LdapHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.ListeTestsHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.OpenOfficeHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.SmtpHelper1;
import fr.atexo.urlvie.toolbox.helpers.impl.SmtpHelper2;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.ConstruireDocument;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl.ListeTestsConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl.StandardConstructeurReponse;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.DecrireUnParametreDeTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ListeDeTesteurs;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageFilesystemTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageHttpTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageJdbcTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageLdapTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageOpenOfficeTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageSmtpTesteurCourt;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageSmtpTesteurLong;
import fr.atexo.urlvie.toolbox.testers.validators.ValideLesParametres;
import fr.atexo.urlvie.toolbox.testers.validators.impl.ValidateurBasic;
import fr.atexo.urlvie.toolbox.testers.validators.impl.ValidateurListeDeTests;

/**
 * Cette classe permet d'effectuer l'initialisation des divers éléments
 * nécessaires à l'exécution de tests les plus courants.<br/>
 * Dans le cas d'une implémentation particulière d'un test (très liée à un
 * projet, non réutilisable en dehors de celui-ci), un initialiseur minimal est
 * prévu : {@link #ANONYMOUS}.
 * 
 * @author Cyril DUBY
 * 
 */
public enum InitialiseurStandard {
    LISTE(
            ListeDeTesteurs.class,
            ListeTestsHelper.class,
            ListeTestsConstructeurReponse.class,
            ValidateurListeDeTests.class),
    HTTP(ParametrageHttpTesteur.class, HttpHelper.class),
    JDBC(ParametrageJdbcTesteur.class, JdbcHelper.class),
    LDAP(ParametrageLdapTesteur.class, LdapHelper.class),
    FILESYSTEM(ParametrageFilesystemTesteur.class, FilesystemHelper.class),
    SMTP1(ParametrageSmtpTesteurCourt.class, SmtpHelper1.class),
    SMTP2(ParametrageSmtpTesteurLong.class, SmtpHelper2.class),
    OPENOFFICE(ParametrageOpenOfficeTesteur.class, OpenOfficeHelper.class),
    /**
     * Permet une initialisation minimale quelque soit la classe fille de
     * {@link DecrireUnParametreDeTesteur}
     */
    ANONYMOUS(
            null,
            null,
            StandardConstructeurReponse.class,
            ValidateurBasic.class);

    private static final Log LOG = LogFactory
            .getLog(InitialiseurStandard.class);
    private static final String INSTANTIATION_PAR_DEFAUT = "Instanciation par défaut de : %1$s";
    private static final String INSTANTIATION_PAR_REFLEXION = "Demande d'instance de : %1$s";

    private Class<? extends DecrireUnParametreDeTesteur> clazzParametre;
    private Class<? extends ValideLesParametres> validateurClazz;
    private Class<? extends FabriquerUneReponseBrute> helperClazz;
    private Class<? extends ConstruireDocument> constructeurReponseClazz;

    private InitialiseurStandard(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametre,
            Class<? extends FabriquerUneReponseBrute> helperClazz,
            Class<? extends ConstruireDocument> constructeurReponseClazz,
            Class<? extends ValideLesParametres> validateur) {
        this.clazzParametre = clazzParametre;
        this.validateurClazz = validateur;
        this.helperClazz = helperClazz;
        this.constructeurReponseClazz = constructeurReponseClazz;
    }

    private InitialiseurStandard(
            Class<? extends DecrireUnParametreDeTesteur> clazzParametre,
            Class<? extends FabriquerUneReponseBrute> helperClazz) {
        this(clazzParametre, helperClazz, null, null);
    }

    /**
     * @return un liste contenant un {@link ValidateurBasic} par défaut, ou une
     *         instance de la classe spécifiée.
     */
    public List<ValideLesParametres> getValidateurs() {
        List<ValideLesParametres> validateurs = new ArrayList<ValideLesParametres>();
        ValideLesParametres validateur = null;
        if (validateurClazz == null) {
            LOG.info(String.format(INSTANTIATION_PAR_DEFAUT,
                    ValidateurBasic.class));
            validateur = new ValidateurBasic();
        } else {
            validateur = (ValideLesParametres) instancie(validateurClazz);
        }
        if (validateur != null) {
            validateurs.add(validateur);
        }
        return validateurs;
    }

    /**
     * @return <code>null</code> si {@link #helperClazz} n'est pas renseigné
     */
    public FabriquerUneReponseBrute getHelper() {
        FabriquerUneReponseBrute result = null;
        if (helperClazz != null) {
            result = (FabriquerUneReponseBrute) instancie(helperClazz);
        }
        return result;
    }

    /**
     * @return une instance de {@link StandardConstructeurReponse} si 
     *         {@link #constructeurReponseClazz} n'est pas renseigné
     */
    public ConstruireDocument getConstructeurReponse() {
        ConstruireDocument constructeur = null;
        if (constructeurReponseClazz == null) {
            LOG.info(String.format(INSTANTIATION_PAR_DEFAUT,
                    StandardConstructeurReponse.class));
            constructeur = new StandardConstructeurReponse();
        } else {
            constructeur = (ConstruireDocument) instancie(constructeurReponseClazz);
        }
        return constructeur;
    }

    /**
     * @param clazz
     * @return renvoie {@link #ANONYMOUS} si le paramètre est <code>null</code>
     *         ou si aucune correspondance exacte n'est trouvée.
     */
    public static InitialiseurStandard trouveCorrespondance(
            Class<? extends DecrireUnParametreDeTesteur> clazz) {
        InitialiseurStandard result = ANONYMOUS;
        if (clazz != null) {
            for (InitialiseurStandard is : InitialiseurStandard.values()) {
                if (clazz.equals(is.clazzParametre)) {
                    result = is;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Instanciation par le package java.lang.reflect, avec le constructeur sans
     * argument.
     * 
     * @param clazz
     * @return <code>null</code> en cas de problème.
     */
    private static Object instancie(Class<?> clazz) {
        Object result = null;
        try {
            LOG.info(String.format(INSTANTIATION_PAR_REFLEXION, clazz));
            result = clazz.getConstructor((Class<?>[]) null).newInstance(
                    (Object[]) null);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            LOG.debug(e.getMessage(), e);
        }
        return result;
    }
}
