package fr.atexo.urlvie.toolbox.helpers;

import java.util.Map;

import fr.atexo.urlvie.toolbox.reponse.brute.ReponseBrute;

public interface FabriquerUneReponseBrute {

    /**
     * Exécute le test et remplit {@link ReponseBrute} avec les exceptions,
     * voire les éléments retournés, mais ce uniquement si
     * {@link ReponseBrute#isParametresValides()} == <code>true</code>.
     * 
     * @param reponseBrute
     *            sert à récolter les éléments significatifs du déroulemetn du
     *            test
     * @param parametres
     *            éléments servant à effectuer le test
     */
    void execute(ReponseBrute reponseBrute, Map<String, Object> parametres);

}
