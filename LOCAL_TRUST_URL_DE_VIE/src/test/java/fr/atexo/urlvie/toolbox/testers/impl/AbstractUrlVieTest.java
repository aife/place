package fr.atexo.urlvie.toolbox.testers.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.atexo.urlvie.toolbox.testers.UrlVieTesteur;

public abstract class AbstractUrlVieTest {
    private final Log log = LogFactory.getLog(this.getClass());
    protected boolean localSave = false;

    void saveResult(String result) {
        if (result != null && !result.trim().isEmpty()) {
            if (localSave) {
                String path = "src/test/resources/%1$s-%2$s.xml";
                File file = new File(String.format(path, this.getClass()
                        .getSimpleName(), new Date().getTime()));

                try {
                    FileWriter fw = new FileWriter(file);
                    fw.append(result);
                    fw.flush();
                    fw.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e.fillInStackTrace());
                }
            } else {
                log.info(result);
            }
        }
    }

    protected void test(UrlVieTesteur testeur) {
        String result = testeur.lanceTest();
        Assert.assertNotNull(result);
        Assert.assertFalse(result.trim().isEmpty());
        saveResult(result);
    }

}
