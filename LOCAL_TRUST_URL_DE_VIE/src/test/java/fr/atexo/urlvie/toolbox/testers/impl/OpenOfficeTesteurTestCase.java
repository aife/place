package fr.atexo.urlvie.toolbox.testers.impl;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.atexo.urlvie.toolbox.reponse.brute.TypeCibleTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/openoffice-context.xml")
public class OpenOfficeTesteurTestCase extends AbstractUrlVieTest {

    @Resource(name = "ooTesteur1")
    private UrlVieTesteurImpl testeur1;
    @Resource(name = "ooTesteur2")
    private UrlVieTesteurImpl testeur2;
    @Resource(name = "ooTesteur3")
    private UrlVieTesteurImpl testeur3;

    public void setTesteur1(UrlVieTesteurImpl testeur) {
        this.testeur1 = testeur;
    }

    public void setTesteur2(UrlVieTesteurImpl testeur) {
        this.testeur2 = testeur;
    }

    public void setTesteur3(UrlVieTesteurImpl testeur) {
        this.testeur3 = testeur;
    }

    @Before
    public void setUp() throws Exception {
        testeur1.setTypeCibleTest(TypeCibleTest.MODULE);
        testeur2.setTypeCibleTest(TypeCibleTest.MODULE);
        testeur3.setTypeCibleTest(TypeCibleTest.APPLICATION);
    }

    @Ignore
    @Test
    public void testLanceTest() {
        test(testeur1);
        test(testeur2);
        test(testeur3);
    }

}
