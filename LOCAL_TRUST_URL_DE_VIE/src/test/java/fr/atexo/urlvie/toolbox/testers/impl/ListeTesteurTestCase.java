package fr.atexo.urlvie.toolbox.testers.impl;

import java.util.HashMap;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.atexo.urlvie.toolbox.reponse.brute.TypeCibleTest;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ListeDeTesteurs;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/simple-url-context.xml",
        "/jdbc-context.xml", "/filesystem-context.xml" })
public class ListeTesteurTestCase extends AbstractUrlVieTest {

    @Resource(name = "simpleUrlTesteur1")
    public UrlVieTesteurImpl testeur11;

    @Resource(name = "simpleUrlTesteur2")
    public UrlVieTesteurImpl testeur12;

    @Resource(name = "jdbcTesteur1")
    public UrlVieTesteurImpl testeur21;

    @Resource(name = "jdbcTesteur2")
    public UrlVieTesteurImpl testeur22;

    @Resource(name = "jdbcTesteur3")
    public UrlVieTesteurImpl testeur23;

    @Resource(name = "filesystemTesteur1")
    public UrlVieTesteurImpl testeur31;

    @Resource(name = "filesystemTesteur2")
    public UrlVieTesteurImpl testeur32;

    class ListeTesteur extends UrlVieTesteurImpl {
        public ListeTesteur() {
            clazzParametres = ListeDeTesteurs.class;
            nomCibleDuTest = "testeur liste";
            typeCibleTest = TypeCibleTest.APPLICATION;

            parametres = new HashMap<String, Object>();
            parametres.put("testeur11", testeur11);
            parametres.put("testeur12", testeur12);
            parametres.put("testeur21", testeur21);
            parametres.put("testeur22", testeur22);
            parametres.put("testeur23", testeur23);
            parametres.put("testeur31", testeur31);
            parametres.put("testeur32", testeur32);

        }
    }

    private ListeTesteur listeTesteur;

    @Before
    public void setUp() throws Exception {
        listeTesteur = new ListeTesteur();
    }

    @Ignore
    @Test
    public void testLanceTest() {
        test(listeTesteur);
    }

}
