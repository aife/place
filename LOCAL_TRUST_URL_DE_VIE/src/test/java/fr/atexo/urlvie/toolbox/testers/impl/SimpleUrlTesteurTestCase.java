package fr.atexo.urlvie.toolbox.testers.impl;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.atexo.urlvie.toolbox.reponse.brute.TypeCibleTest;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageHttpTesteur;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/simple-url-context.xml")
public class SimpleUrlTesteurTestCase extends AbstractUrlVieTest {

    @Resource(name = "simpleUrlTesteur1")
    private UrlVieTesteurImpl testeur1;

    @Resource(name = "simpleUrlTesteur2")
    private UrlVieTesteurImpl testeur2;

    public void setTesteur1(UrlVieTesteurImpl testeur) {
        this.testeur1 = testeur;
        testeur1.setClazzParametres(ParametrageHttpTesteur.class);
    }

    public void setTesteur2(UrlVieTesteurImpl testeur) {
        this.testeur2 = testeur;
        testeur2.setClazzParametres(ParametrageHttpTesteur.class);
    }

    @Before
    public void setUp() throws Exception {
        testeur1.setClazzParametres(ParametrageHttpTesteur.class);
        testeur1.setTypeCibleTest(TypeCibleTest.MODULE);
        testeur2.setClazzParametres(ParametrageHttpTesteur.class);
        testeur2.setTypeCibleTest(TypeCibleTest.APPLICATION);
    }

    @Ignore
    @Test
    public void testLanceTest() {
        // test(new ServiceProxyTesteur());
        test(testeur1);
        test(testeur2);
    }

}
