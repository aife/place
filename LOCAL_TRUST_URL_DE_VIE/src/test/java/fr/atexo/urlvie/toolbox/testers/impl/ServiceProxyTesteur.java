package fr.atexo.urlvie.toolbox.testers.impl;

import java.util.HashMap;
import java.util.Map;

import fr.atexo.urlvie.toolbox.helpers.impl.HttpHelper;
import fr.atexo.urlvie.toolbox.helpers.impl.ListeTestsHelper;
import fr.atexo.urlvie.toolbox.reponse.brute.TypeCibleTest;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.ConstruireDocument;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl.ListeTestsConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.constructeur.impl.StandardConstructeurReponse;
import fr.atexo.urlvie.toolbox.reponse.xml.deconstructeurs.impl.DeconstructeurXmlVersTest;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.ParametrageHttpTesteur;
import fr.atexo.urlvie.toolbox.testers.impl.parametrages.impl.SansParametre;

public class ServiceProxyTesteur extends UrlVieTesteurImpl {

    public String url = "http://cas1:8080/utilisateurs/utilisateurs/webservice/rest/utilisateurs/SYSADMIN";

    public Map<String, String> paramMap;

    public ServiceProxyTesteur() {
        clazzParametres = SansParametre.class;
        construireDocument = new ListeTestsConstructeurReponse();

        helper = new ListeTestsHelper();
        ((ListeTestsHelper) helper)
                .setDeconstructeur(new DeconstructeurXmlVersTest());

        nomCibleDuTest = "service proxy";
        typeCibleTest = TypeCibleTest.APPLICATION;

        parametres = new HashMap<String, Object>();
        UrlVieTesteurImpl testeur = new UrlVieTesteurImpl();
        parametres.put("testeur", testeur);

        testeur.setClazzParametres(ParametrageHttpTesteur.class);
        ConstruireDocument consRep = new StandardConstructeurReponse();
        testeur.setConstructeurReponse(consRep);
        testeur.setHelper(new HttpHelper());
        testeur.setNomCibleDuTest("test service rest utilisateur");
        testeur.setNomTypeCibleTest("module");
        
        Map<String, Object> testeurParametres = new HashMap<String, Object>();
        testeur.setParametres(testeurParametres);
        testeurParametres.put(ParametrageHttpTesteur.URL.toString(), url);
        testeurParametres.put(ParametrageHttpTesteur.GET.toString(), paramMap);
    }

}
