package fr.atexo.urlvie.toolbox.testers.impl;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/filesystem-context.xml")
public class FilesystemTesteurTestCase extends AbstractUrlVieTest {

    @Resource(name = "filesystemTesteur1")
    private UrlVieTesteurImpl testeur1;

    @Resource(name = "filesystemTesteur2")
    private UrlVieTesteurImpl testeur2;

    public void setTesteur1(UrlVieTesteurImpl testeur) {
        this.testeur1 = testeur;
    }

    public void setTesteur2(UrlVieTesteurImpl testeur) {
        this.testeur2 = testeur;
    }

    @Ignore
    @Test
    public void testLanceTest() {
        test(testeur1);
        test(testeur2);
    }

}
