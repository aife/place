package fr.atexo.urlvie.toolbox.testers.impl;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/smtp2-context.xml")
public class SmtpTesteurTestCase extends AbstractUrlVieTest {
    @Resource(name = "smtpTesteur2_1")
    private UrlVieTesteurImpl testeur1;

    public void setTesteur1(UrlVieTesteurImpl testeur) {
        this.testeur1 = testeur;
    }

    @Ignore
    @Test
    public void testLanceTest() {
        test(testeur1);
    }

}
