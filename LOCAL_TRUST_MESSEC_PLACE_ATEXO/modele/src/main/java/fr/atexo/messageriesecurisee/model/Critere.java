package fr.atexo.messageriesecurisee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity(name = "critere")
@Table(name = "CRITERE")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = "message")
public class Critere extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CRITERE")
    private Integer id;

    @Lob
    @Column(name = "NOM")
    private String nom;

    @Column(name = "VALEUR")
    private String valeur;

    @Column(name = "LIBELLE")
    private String libelle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MESSAGE")
    @JsonIgnore
    private Message message;

}
