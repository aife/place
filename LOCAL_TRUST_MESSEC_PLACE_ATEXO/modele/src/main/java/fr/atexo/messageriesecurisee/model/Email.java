package fr.atexo.messageriesecurisee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = {"message", "parent", "reponses"})
@Audited
@Entity(name = "destinataire")
@Table(name = "DESTINATAIRE", indexes = {
        @Index(name = "IX_DESTINATAIRE_IDENTIFIANT_OBJET_METIER", columnList = "IDENTIFIANT_OBJET_METIER"),
        @Index(name = "IX_DESTINATAIRE_STATUT_EMAIL", columnList = "STATUT_EMAIL"),
        @Index(name = "IX_DESTINATAIRE_ID_SMTP_TEMPORAIRE", columnList = "ID_SMTP_TEMPORAIRE"),
        @Index(name = "IX_DESTINATAIRE_EMAIL", columnList = "EMAIL"),
        @Index(name = "IX_CODE_LIEN_EMAIL", columnList = "CODE_LIEN"),
        @Index(name = "IX_SERVEUR_ENVOI_EMAIL", columnList = "SERVEUR_ENVOI")
})
public class Email extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMAIL")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "ID_EMAIL_PARENT")
    @NotAudited
    @JsonIgnore
    private Email parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @NotAudited
    @Builder.Default
    @JsonIgnore
    private Set<Email> reponses = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_DESTINATAIRE", nullable = false)
    private TypeDestinataire typeDestinataire;

    @Column(name = "IDENTIFIANT_OBJET_METIER", length = 100)
    private String identifiantObjetMetier;

    @Column(name = "IDENTIFIANT_ENTREPRISE")
    private String identifiantEntreprise;

    @Column(name = "IDENTIFIANT_CONTACT")
    private Integer identifiantContact;

    @Column(name = "NOM_ENTREPRISE")
    private String nomEntreprise;

    @Column(name = "NOM_CONTACT")
    private String nomContact;

    @Column(name = "EMAIL", nullable = false, length = 100)
    private String email;


    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT_EMAIL", nullable = false, length = 100)
    private TypeStatutEmail statutEmail;

    @Column(name = "CODE_LIEN", nullable = false)
    private String codeLien;

    @Column(name = "DATE_DEMANDE_ENVOI")
    private LocalDateTime dateDemandeEnvoi;

    @Column(name = "DATE_ENVOI")
    private LocalDateTime dateEnvoi;

    @Column(name = "DATE_AR")
    private LocalDateTime dateAR;

    @NotAudited
    @Column(name = "NOMBRE_ESSAIS")
    private Integer nombreEssais = 0;

    @Lob
    @Column(name = "DESCRIPTION_ERREUR")
    @Basic(fetch = FetchType.LAZY)
    private String erreur;

    @Column(name = "IDENTIFIANT_ENTREPRISE_AR")
    private String identifiantEntrepriseAR;

    @Column(name = "IDENTIFIANT_CONTACT_AR")
    private String identifiantContactAR;

    @Column(name = "NOM_ENTREPRISE_AR")
    private String nomEntrepriseAR;

    @Column(name = "NOM_CONTACT_AR")
    private String nomContactAR;

    @Column(name = "EMAIL_CONTACT_AR")
    private String emailContactAR;

    @Column(name = "ID_SMTP_TEMPORAIRE", length = 100)
    private String identifiantSmtp;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MESSAGE")
    @NotAudited
    @JsonIgnore
    private Message message;

    @Column(name = "DATE_MODIF_BROUILLON")
    private LocalDateTime dateModificationBrouillon;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "DATE_RELANCE")
    private LocalDateTime dateRelance;

    @Column(name = "DATE_ECHEC")
    private LocalDateTime dateEchec;

    @Column(name = "FAVORI")
    private Boolean favori;

    @Column(name = "ORDRE")
    private Integer ordre;
    
    @Column(name = "SERVEUR_ENVOI")
    private String serveurEnvoi;
}
