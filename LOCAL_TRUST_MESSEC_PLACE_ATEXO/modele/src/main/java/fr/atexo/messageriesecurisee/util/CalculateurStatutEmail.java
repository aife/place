package fr.atexo.messageriesecurisee.util;

import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;

import java.util.HashMap;
import java.util.Map;


public class CalculateurStatutEmail {

	public static final TypeStatutEmail execute(Message message) {
		var statuts = new HashMap<TypeStatutEmail, Integer>();
		for (TypeStatutEmail t : TypeStatutEmail.values()) {
			statuts.put(t, 0);
		}
		for (Email d : message.getDestinatairesPlateFormeDestinataire()) {
			if (d.getStatutEmail() != null) {
				statuts.put(d.getStatutEmail(), statuts.get(d.getStatutEmail()) + 1);
			}

		}
		for (Email d : message.getDestinatairesPlateFormeEmettreur()) {
			if (d.getStatutEmail() != null) {
				statuts.put(d.getStatutEmail(), statuts.get(d.getStatutEmail()) + 1);
			}
		}
		int nbMessagesTotal = message.getDestinatairesPlateFormeDestinataire().size()
				+ message.getDestinatairesPlateFormeEmettreur().size();

		return calcule(statuts, nbMessagesTotal);
	}

	private static final TypeStatutEmail calcule(Map<TypeStatutEmail, Integer> statuts, int nbMessagesTotal) {
		// si il y a au moins un message en cours d'envoi, le statut est à en
		// cours d'envoi
		if (statuts.get(TypeStatutEmail.EN_COURS_ENVOI) >= 1) {
			return TypeStatutEmail.EN_COURS_ENVOI;
		}
		// si il y au moins un message envoyé (ou plus (acquité, echec...) ET un
		// message en attente d'envoi => en cours d'envoi
		boolean acquitteEchechEnCours = statuts.get(TypeStatutEmail.ENVOYE) >= 1 || statuts.get(TypeStatutEmail.ACQUITTE) >= 1
				|| statuts.get(TypeStatutEmail.ECHEC_ENVOI) >= 1 || statuts.get(TypeStatutEmail.EN_COURS_ENVOI) >= 1;
		boolean auMoinsEnAttenteEnvoi = statuts.get(TypeStatutEmail.EN_ATTENTE_ENVOI) >= 1;
		if (auMoinsEnAttenteEnvoi && acquitteEchechEnCours) {
			return TypeStatutEmail.EN_COURS_ENVOI;
		}
		// si il n'y pas de message en attente d'envoi, pas de message en cours
		// d'envoi
		if (statuts.get(TypeStatutEmail.EN_COURS_ENVOI) <= 0 && statuts.get(TypeStatutEmail.EN_ATTENTE_ENVOI) <= 0) {
			// marquage des echec, echec partiel, => au moins un echec
			if (statuts.get(TypeStatutEmail.ECHEC_ENVOI) > 0) {
				if (statuts.get(TypeStatutEmail.ECHEC_ENVOI) >= nbMessagesTotal) {
					return TypeStatutEmail.ECHEC_ENVOI;
				} else {
					return TypeStatutEmail.ECHEC_PARTIEL;
				}
			}
			// marquage des acquittement acquittement partiel => au moins un
			// acquittement
			if (statuts.get(TypeStatutEmail.ACQUITTE) > 0) {
				if (statuts.get(TypeStatutEmail.ACQUITTE) >= nbMessagesTotal) {
					return TypeStatutEmail.ACQUITTE;
				} else {
					return TypeStatutEmail.ACQUITTEMENT_PARTIEL;
				}
			}
			// marquage de l'envoi autres cas => en cours d'envoi
			if (statuts.get(TypeStatutEmail.ENVOYE) > 0) {
				if (statuts.get(TypeStatutEmail.ENVOYE) >= nbMessagesTotal) {
					return TypeStatutEmail.ENVOYE;
				} else {
					return TypeStatutEmail.EN_COURS_ENVOI;
				}
			}
			// marquage de l'envoi autres cas => en cours d'envoi
			if (statuts.get(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT) > 0) {
				if (statuts.get(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT) >= nbMessagesTotal) {
					return TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT;
				} else {
					return TypeStatutEmail.EN_COURS_ENVOI;
				}
			}
		}
		return TypeStatutEmail.BROUILLON;
	}

}
