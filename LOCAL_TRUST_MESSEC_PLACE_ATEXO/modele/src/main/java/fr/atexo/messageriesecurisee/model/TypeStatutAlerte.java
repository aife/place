package fr.atexo.messageriesecurisee.model;

public enum TypeStatutAlerte {
    A_ENVOYER,
    ENVOYE,
    ECHEC;
}
