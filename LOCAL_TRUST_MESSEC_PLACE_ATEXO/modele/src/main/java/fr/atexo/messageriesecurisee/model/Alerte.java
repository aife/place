package fr.atexo.messageriesecurisee.model;


import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "alerte")
@Table(name = "ALERTE", indexes = {
        @Index(name = "IX_ALERTE_STATUT", columnList = "STATUT"),
        @Index(name = "IX_ALERTE_TYPE", columnList = "TYPE")
})
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = "message")
public class Alerte extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ALERTE")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Message.class)
    @JoinColumn(name = "ID_MESSAGE")
    private Message message;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "EXPEDITEUR")
    private String expediteur;

    @Lob
    @Column(name = "OBJET")
    @Basic(fetch = FetchType.LAZY)
    private String objet;

    @Lob
    @Column(name = "CONTENU", nullable = false)
    private String contenu;

    @Lob
    @Column(name = "ERREUR")
    private String erreur;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUT", nullable = false, length = 100)
    private TypeStatutAlerte statut;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false, length = 100)
    private TypeAlerte type;

    @Column(name = "ID_SMTP")
    private String idSmtp;

    @Column(name = "DATE_DEMANDE_ENVOI")
    private LocalDateTime dateDemandeEnvoi = LocalDateTime.now();

    @Column(name = "DATE_ENVOI")
    private LocalDateTime dateEnvoi;

    @Column(name = "SERVEUR_ENVOI")
    private String serveurEnvoi;

}
