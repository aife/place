package fr.atexo.messageriesecurisee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "FICHIER")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = "message")
public class PieceJointe extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PIECE_JOINTE")
    private Integer id;

    @Column(name = "ID_EXTERNE")
    private String idExterne;

    @Column(name = "NOM", nullable = false)
    private String nom;

    @Lob
    @Column(name = "CHEMIN", nullable = false)
    private String chemin;

    @Column(name = "TAILLE", nullable = false)
    private Integer taille;

    @Column(name = "DATE_CREATION")
    private LocalDateTime dateCreation;

    @ManyToOne
    @JoinColumn(name = "ID_MESSAGE")
    @JsonIgnore
    private Message message;

    @Column(name = "CONTENT_TYPE")
    private String contentType;

    @Transient
    private String tailleLisible;
}
