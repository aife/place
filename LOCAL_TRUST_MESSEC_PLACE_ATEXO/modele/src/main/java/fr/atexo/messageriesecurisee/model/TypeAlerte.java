package fr.atexo.messageriesecurisee.model;

public enum TypeAlerte {
    /**
     * Cette alerte est un accusé de réponse envoyé à un agent quand il a répondu au message d'un tiers.
     */
    ENTREPRISE,
    /**
     * Cette alerte informe un agent de la réponse d'un tiers.
     */
    AGENT,
    /**
     * Cette alerte informe un agent de l'envoi d'un message par un tiers.
     */
    MESSAGE_ENTREPRISE,
    /**
     * Cette alerte informe un tiers de la réponse d'un agent.
     */
    REPONSE_AGENT,
    /**
     * Cette alerte est un accusé d'envoi envoyé au tiers quand il envoie un message.
     */
    AR_ENVOI_ENTREPRISE,

}
