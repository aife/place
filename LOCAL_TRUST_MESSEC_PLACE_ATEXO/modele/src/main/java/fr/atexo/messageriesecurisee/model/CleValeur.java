package fr.atexo.messageriesecurisee.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "cle_valeur")
@Table(name = "CLE_VALEUR")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = "message")
public class CleValeur extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CLE_VALEUR")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "CLE")
    private String cle;

    @Column(name = "VALEUR")
    @Lob
    private String valeur;

    @ManyToOne(fetch=FetchType.LAZY, targetEntity = Message.class)
    @JoinColumn(name = "ID_MESSAGE")
    @JsonIgnore
    private Message message;

    public CleValeur(String code, String cle, String valeur) {
        this.code = code;
        this.cle = cle;
        this.valeur = valeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CleValeur cleValeur = (CleValeur) o;
        return Objects.equals(code, cleValeur.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
