package fr.atexo.messageriesecurisee.model;

public class ReferentielDiscriminators {

    public static final String SURCHARGE_LIBELLE = "surcharge-libelle";
    public static final String ERREUR_SMTP = "erreur-smtp";

    private ReferentielDiscriminators() {

    }

}
