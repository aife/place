package fr.atexo.messageriesecurisee.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.ERREUR_SMTP)
public class ErreurSMTP extends AbstractReferentielEntity {

}
