package fr.atexo.messageriesecurisee.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ReferentielDiscriminators.SURCHARGE_LIBELLE)
public class SurchargeLibelle extends AbstractReferentielEntity {

}
