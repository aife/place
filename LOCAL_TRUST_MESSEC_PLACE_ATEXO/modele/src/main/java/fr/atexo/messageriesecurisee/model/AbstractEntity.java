package fr.atexo.messageriesecurisee.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;
import java.util.UUID;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class AbstractEntity {

    @CreatedDate
    @Column(name = "DATE_CREATION", updatable = false)
    private LocalDateTime dateCreation;

    @LastModifiedDate
    @Column(name = "DATE_MODIFICATION")
    private LocalDateTime dateModification;

    @Column(name = "uuid", length = 36, unique = true, nullable = false)
    @ColumnDefault("uuid()")
    @Builder.Default
    private String uuid = UUID.randomUUID().toString();

    @PrePersist
    public void prePersist() {
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
    }

}
