package fr.atexo.messageriesecurisee.model;

import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.util.CalculateurStatutEmail;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity(name = "message")
@Table(name = "MESSAGE", indexes = {
        @Index(name = "IX_MESSAGE_REFERENCE_OBJET_METIER", columnList = "REFERENCE_OBJET_METIER"),
        @Index(name = "IX_MESSAGE_TYPE_MESSAGE", columnList = "TYPE_MESSAGE"),
        @Index(name = "IX_IDENTIFIANT_OBJET_METIER_PF_DESTINATAIRE", columnList = "IDENTIFIANT_OBJET_METIER_PF_DESTINATAIRE"),
        @Index(name = "IX_IDENTIFIANT_OBJET_METIER_PF_EMETTEUR", columnList = "IDENTIFIANT_OBJET_METIER_PF_EMETTEUR"),
        @Index(name = "IX_IDENTIFIANT_SOUS_OBJET_METIER_PF_EMETTEUR", columnList = "IDENTIFIANT_SOUS_OBJET_METIER_PF_EMETTEUR"),
        @Index(name = "IX_IDENTIFIANT_PF_EMETTEUR", columnList = "IDENTIFIANT_PF_EMETTEUR"),
        @Index(name = "IX_IDENTIFIANT_PF_DESTINATAIRE", columnList = "IDENTIFIANT_PF_DESTINATAIRE")
})
public class Message extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MESSAGER")
    private Integer id;

    @Column(name = "REFERENCE_OBJET_METIER", length = 100)
    private String referenceObjetMetier;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE_MESSAGE", nullable = false, length = 100)
    private TypeMessage typeMessage;

    @Lob
    @Column(name = "CARTOUCHE")
    @Basic(fetch = FetchType.LAZY)
    private String cartouche;

    @Lob
    @Column(name = "SIGNATURE_AVIS_PASSAGE")
    @Basic(fetch = FetchType.LAZY)
    private String signatureAvisPassage;

    @Lob
    @Column(name = "NOM_COMPLET_EXPEDITEUR")
    @Basic(fetch = FetchType.LAZY)
    private String nomCompletExpediteur;

    @Column(name = "NOM_CONTACT_EXPEDITEUR")
    private String nomContactExpediteur;

    @Column(name = "EMAIL_EXPEDITEUR", nullable = false)
    private String emailExpediteur;

    @Column(name = "EMAIL_EXPEDITEUR_TECHNIQUE")
    private String emailExpediteurTechnique;

    @Column(name = "EMAIL_REPONSE_EXPEDITEUR")
    private String emailReponseExpediteur;

    @Lob
    @Column(name = "CONTENU", nullable = false)
    @Basic(fetch = FetchType.LAZY)
    private String contenu;

    @Lob
    @Column(name = "OBJET")
    @Basic(fetch = FetchType.LAZY)
    private String objet;

    @ColumnDefault("0")
    @Column(name = "REPONSE_ATTENDUE", nullable = false)
    @Builder.Default
    private boolean reponseAttendue = false;


    @ColumnDefault("0")
    @Column(name = "REPONSE_AGENT_ATTENDUE", nullable = false)
    @Builder.Default
    private boolean reponseAgentAttendue = false;

    /*
    Permet de savoir s'il faut appliquer une date limite pour la réponse.
     */
    @ColumnDefault("0")
    @Column(name = "REPONSE_DATE_ATTENDUE", nullable = false)
    @Builder.Default
    private boolean dateLimiteReponseAttendue = false;
    /*
    Date limite d'une réponse.
     */
    @Column(name = "DATE_LIMITE_REPONSE")
    private LocalDateTime dateLimiteReponse;

    /*
    Permet de savoir si l'accès par les agents au contenu de la réponse du destinataire est bloqué jusqu'a cette date.
     */
    @ColumnDefault("0")
    @Column(name = "REPONSE_BLOQUE", nullable = false)
    @Builder.Default
    private boolean reponseBloque = false;

    @Column(name = "DATE_REPONSE")
    private LocalDateTime dateReponse;

    @Column(name = "DATE_MODIFICATION")
    private LocalDateTime dateModification;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "message", fetch = FetchType.EAGER)
    @Builder.Default
    private Set<Email> destinataires = new HashSet<>();

    @Transient
    @Builder.Default
    private List<Email> destinatairesPreSelectionnes = new ArrayList<>();

    @OneToMany(mappedBy = "message", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Critere> criteres;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "message", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @Builder.Default
    private List<PieceJointe> piecesJointes = new ArrayList<>();

    @Column(name = "EMAILS_ALERTE_REPONSE", nullable = true)
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String emailsAlerteReponse;

    @Column(name = "MAX_TAILLE_FICHIERS", nullable = true)
    private BigInteger maxTailleFichiers;

    @Transient
    private LocalDateTime dateDemandeEnvoi;

    private transient List<TypeMessage> typesInterdits;

    private transient boolean masquerOptionsEnvoi;

    @Column(name = "LOGO_SRC")
    private String logoSrc;

    @Column(name = "TEMPLATE_MAIL")
    private String templateMail;

    @Column(name = "TEMPLATE_FIGE", nullable = false)
    @Builder.Default
    private boolean templateFige = false;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "message", fetch = FetchType.EAGER)
    @Builder.Default
    private Set<CleValeur> metaDonnees = new HashSet<>();

    @Transient
    private String codeLien;

    @ColumnDefault("0")
    @Column(name = "BROUILLON", nullable = false)
    private boolean brouillon = false;

    @Transient
    @Builder.Default
    private List<DossierVolumineux> dossiersVolumineux = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_DOSSIER_VOLUMINEUX", foreignKey = @ForeignKey(name = "fk_message_dossier_volumineux"))
    private DossierVolumineux dossierVolumineux;

    // plateforme
    @Column(name = "IDENTIFIANT_PF_EMETTEUR", nullable = false)
    private String identifiantPfEmetteur;

    @Column(name = "IDENTIFIANT_PF_DESTINATAIRE", nullable = false)
    private String identifiantPfDestinataire;

    @Column(name = "NOM_PF_EMETTEUR", nullable = false)
    private String nomPfEmetteur;

    @Column(name = "NOM_PF_DESTINATAIRE", nullable = false)
    private String nomPfDestinataire;

    @Column(name = "URL_PF_EMETTEUR", nullable = false)
    private String urlPfEmetteur;

    @Column(name = "URL_PF_DESTINATAIRE", nullable = false)
    private String urlPfDestinataire;

    @Column(name = "URL_PF_EMETTEUR_VISUALISATION")
    private String urlPfEmetteurVisualisation;

    @Column(name = "URL_PF_DESTINATAIRE_VISUALISATION")
    private String urlPfDestinataireVisualisation;

    @Column(name = "URL_PF_REPONSE")
    private String urlPfReponse;

    @Column(name = "IDENTIFIANT_OBJET_METIER_PF_DESTINATAIRE")
    private Integer identifiantObjetMetierPlateFormeDestinataire;

    @Column(name = "IDENTIFIANT_OBJET_METIER_PF_EMETTEUR")
    private Integer identifiantObjetMetierPlateFormeEmetteur;

    @Column(name = "IDENTIFIANT_SOUS_OBJET_METIER_PF_EMETTEUR")
    private Integer identifiantSousObjetMetier;

    @Enumerated(EnumType.STRING)
    @Column(name = "INITIALISATEUR", length = 20)
    @Builder.Default
    private TypeInitialisateur initialisateur = TypeInitialisateur.AGENT;

    public List<Email> getDestinatairesPlateFormeEmettreur() {
        return Optional.of(destinataires).orElse(new HashSet<>()).stream().filter(d -> d.getTypeDestinataire() == TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE).collect(Collectors.toList());
    }

    public List<Email> getDestinatairesPlateFormeDestinataire() {
        return Optional.of(destinataires).orElse(new HashSet<>()).stream().filter(d -> d.getTypeDestinataire() == TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE).collect(Collectors.toList());
    }

    public TypeStatutEmail getStatut() {
        return CalculateurStatutEmail.execute(this);
    }

    @Override
    public Message clone() {
        // on copie tout sauf les destinataires
        Message cloned = new Message();
        cloned.setId(id);
        cloned.setReferenceObjetMetier(referenceObjetMetier);
        cloned.setTypeMessage(typeMessage);
        cloned.setCartouche(cartouche);
        cloned.setSignatureAvisPassage(signatureAvisPassage);
        cloned.setNomCompletExpediteur(nomCompletExpediteur);
        cloned.setNomContactExpediteur(nomContactExpediteur);
        cloned.setEmailExpediteur(emailExpediteur);
        cloned.setEmailReponseExpediteur(emailReponseExpediteur);
        cloned.setContenu(contenu);
        cloned.setObjet(objet);
        cloned.setReponseBloque(reponseBloque);
        cloned.setReponseAttendue(reponseAttendue);
        cloned.setDateReponse(dateReponse);
        cloned.setEmailsAlerteReponse(emailsAlerteReponse);
        cloned.setMaxTailleFichiers(maxTailleFichiers);
        cloned.setTypesInterdits(typesInterdits);
        cloned.setMasquerOptionsEnvoi(masquerOptionsEnvoi);
        cloned.setLogoSrc(logoSrc);
        cloned.setTemplateMail(templateMail);
        cloned.setTemplateFige(templateFige);
        cloned.setCodeLien(codeLien);
        cloned.setCriteres(criteres);
        cloned.setPiecesJointes(piecesJointes);
        cloned.setBrouillon(brouillon);
        cloned.setDossierVolumineux(dossierVolumineux);
        cloned.setDossiersVolumineux(dossiersVolumineux);
        return cloned;
    }
}
