package fr.atexo.messageriesecurisee.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "DOSSIER_VOLUMINEUX")
@Table(name = "DOSSIER_VOLUMINEUX")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class DossierVolumineux extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DOSSIER_VOLUMINEUX")
    private Long id;

    @Column(name = "DATE_CREATION")
    private LocalDateTime dateCreation;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "TAILLE")
    private Long taille;

    @Column(name = "UUID_REFERENCE")
    private String uuidReference;

    @Column(name = "UUID_TECHNIQUE")
    private String uuidTechnique;
}
