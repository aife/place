package fr.atexo.messageriesecurisee.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity(name = "TOKEN")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = {"contenu"})
public class Token extends AbstractEntity {

	@Lob
	@Column(name = "CONTENU", nullable = false)
	String contenu;
	@Column(name = "TYPE")
	String type;

	@Id
	@Column(name = "TOKEN")
	private String token;

	@Column(name = "IDENTIFIANT_PLATEFORME")
	String plateforme;

	@Column(name = "IDENTIFIANT_OBJET_METIER")
	Integer identifiantObjetMetier;

	@Column(name = "REFERENCE_OBJET_METIER")
	String reference;

}
