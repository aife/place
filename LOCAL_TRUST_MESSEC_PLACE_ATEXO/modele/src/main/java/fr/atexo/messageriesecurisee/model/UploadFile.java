package fr.atexo.messageriesecurisee.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity(name = "FICHIER_UPLOAD")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = {"contenu"})
@Table(name = "FICHIER_UPLOAD", indexes = {
        @Index(name = "IX_FICHIER_UPLOAD_IDENTIFIANT", columnList = "IDENTIFIANT")
})
public class UploadFile extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_FICHIER_UPLOAD")
    private Integer id;

    @Column(name = "NOM", nullable = false)
    String nom;

    @Column(name = "IDENTIFIANT", unique = true, nullable = false)
    String identifiant;

    @Column(name = "TAILLE", nullable = false)
    long taille;

    @Column(name = "MIME_TYPE")
    String mimeType;

    @Column(name = "CHEMIN")
    private String chemin;
}
