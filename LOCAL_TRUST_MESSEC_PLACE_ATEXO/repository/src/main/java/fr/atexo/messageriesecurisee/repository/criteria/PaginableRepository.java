package fr.atexo.messageriesecurisee.repository.criteria;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import java.util.List;
import java.util.function.Function;

public interface PaginableRepository<T, U> {

	Page<T> rechercher(U criteria, Pageable pageable);

	List<T> rechercher(U criteria);

	Long compter(U criteria);

	<V> TypedQuery<V> requeter(U criteria, final CriteriaQuery<V> query, final Function<Expression<T>, Expression<V>> projection, Sort sort);

	Class<T> getTarget();
}
