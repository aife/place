package fr.atexo.messageriesecurisee.repository;


import fr.atexo.messageriesecurisee.model.SurchargeLibelle;
import org.springframework.stereotype.Repository;

@Repository
public interface SurchargeLibelleRepository extends ReferentielRepository<SurchargeLibelle> {


}
