package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface EmailRepository extends JpaRepository<Email, Integer> {
    Optional<Email> findByCodeLien(String codeLien);

    Optional<Email> findByUuid(String uuid);

    @Query("from destinataire as email where email.message.referenceObjetMetier=:referenceObjetMetier and email.message.identifiantObjetMetierPlateFormeEmetteur=:idObjetMetier and email.message.identifiantPfEmetteur =:identifiantPlateforme and email.statutEmail in (:statutEmails)")
    List<Email> findEmailByIdObjetMetierAndReferenceObjetMetier(@Param("idObjetMetier") Integer idObjetMetier, @Param("referenceObjetMetier") String referenceObjetMetier, @Param("identifiantPlateforme") String idenfitiantPlateforme, @Param("statutEmails") List<TypeStatutEmail> statutEmails);

    @Query("from destinataire as email where email.parent.id=:idMsgInitial")
    List<Email> findByIdMessageInitial(@Param("idMsgInitial") Integer idMsgInitial);

    @Query("select id from destinataire where identifiantSmtp = :identifiantSmtp")
    Set<Integer> findIdsByIdentifiantSmtp(@Param("identifiantSmtp") String identifiantSmtp);

    List<Email> findByStatutEmailInAndDateDemandeEnvoiBefore(List<TypeStatutEmail> statutEmail, LocalDateTime dateDemandeEnvoi);

    @Query("from destinataire as email where email.parent.id =:idMessageInitial and email.statutEmail in (:status) and email.typeDestinataire = :typeDestinataire")
    List<Email> findByIdMessageInitialAndStatutEmail(@Param("idMessageInitial") Integer idMessageInitial, @Param("status") List<TypeStatutEmail> status, @Param("typeDestinataire") TypeDestinataire typeDestinataire);

}
