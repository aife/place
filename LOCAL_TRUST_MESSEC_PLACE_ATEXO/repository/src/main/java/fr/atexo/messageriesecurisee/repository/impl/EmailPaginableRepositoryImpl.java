package fr.atexo.messageriesecurisee.repository.impl;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.repository.EmailPaginableRepository;
import fr.atexo.messageriesecurisee.repository.criteria.BasePaginableRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail.*;
import static java.lang.Boolean.TRUE;

@Repository
public class EmailPaginableRepositoryImpl extends BasePaginableRepository<Email, SerializableRechercheMessage> implements EmailPaginableRepository {

	private static final Logger LOG = LoggerFactory.getLogger(EmailPaginableRepositoryImpl.class);

	@Override
	public <V> TypedQuery<V> requeter(
			SerializableRechercheMessage criteria,
			CriteriaQuery<V> query,
			Function<Expression<Email>, Expression<V>> projection,
			Sort sort
	) {
		final var builder = em.getCriteriaBuilder();
		final From<?, Email> cursor;

		final var predicates = new HashSet<Predicate>();

		final var root = query.from(Email.class);
		var messageJoin = root.join("message", JoinType.INNER);
		var critereJoin = messageJoin.join("criteres", JoinType.LEFT);
		var pieceJointJoin = messageJoin.join("piecesJointes", JoinType.LEFT);
		var reponseJoin = root.join("reponses", JoinType.LEFT);
		query.select(projection.apply(root));
		// ne pas envoyer les réponses
		predicates.add(builder.isNull(root.get("parent")));

		cursor = root;


		/** fixme: non utilisé
		 * Plateforme Emetteur ou Destinataire

		 if(criteria.getTypePlateformeRecherche() != null) {
		 predicates.add(builder.equal(messageJoin.get("typePlateforme"), criteria.getTypePlateformeRecherche()));
		 }
		 */

		/**
		 * Identifiant de la plateforme concernée
		 */
		if (!StringUtils.isEmpty(criteria.getIdPlateformeRecherche())) {
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
				predicates.add(builder.equal(messageJoin.get("identifiantPfEmetteur"), criteria.getIdPlateformeRecherche()));
			}
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
				predicates.add(builder.equal(messageJoin.get("identifiantPfDestinataire"), criteria.getIdPlateformeRecherche()));
			}
		}


		/**
		 * Identifiant de l'objet métier
		 */
		if (criteria.getIdObjetMetier() != null) {
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
				predicates.add(builder.equal(messageJoin.get("identifiantObjetMetierPlateFormeEmetteur"), criteria.getIdObjetMetier()));
			}
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
				predicates.add(builder.equal(messageJoin.get("identifiantObjetMetierPlateFormeDestinataire"), criteria.getIdObjetMetier()));
			}
		}

		/**
		 * Identifiants de l'objet métier
		 */
		if (criteria.getIdentifiantsObjetsMetier() != null && !criteria.getIdentifiantsObjetsMetier().isEmpty()) {
			if (criteria.getIdObjetMetier() == null || criteria.getIdObjetMetier() == 0) {
				var ids = TRUE.equals(criteria.getMesConsultations()) ? criteria.getIdentifiantsObjetsMetierAgentConnecte() : criteria.getIdentifiantsObjetsMetier();
				if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
					predicates.add(messageJoin.get("identifiantObjetMetierPlateFormeEmetteur").in(ids));
				}
				if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
					predicates.add(messageJoin.get("identifiantObjetMetierPlateFormeDestinataire").in(ids));
				}
			}
		}

		/**
		 * Identifiant sous objet métier
		 */
		if (criteria.getIdSousObjetMetier() != null && criteria.getIdSousObjetMetier() != 0) {
			predicates.add(builder.equal(messageJoin.get("identifiantSousObjetMetier"), criteria.getIdSousObjetMetier()));
		}
		/**
		 * Identifiant du contact destinataire sur la plateforme selectionnée
		 */
		if (criteria.getIdContactDestList() != null && !criteria.getIdContactDestList().isEmpty()) {
			predicates.add(cursor.get("identifiantContact").in(criteria.getIdContactDestList()));
		}

		/*
		  Identifiant de l'entreprise destinataire sur la plateforme sélectionnée
		 */
		if (criteria.getIdEntrepriseDest() != null) {
			predicates.add(builder.equal(cursor.get("identifiantEntreprise"), criteria.getIdEntrepriseDest()));
		}

		/**
		 * Adresse email du destinataire sur la plateforme Emetteur ou sur la
		 * plateforme Destinataire
         */
        if (!StringUtils.isEmpty(criteria.getMailDestinataire())) {
            if (!StringUtils.isEmpty(criteria.getMailExpediteur())) {
                var p1 = builder.equal(root.get("email"), criteria.getMailDestinataire());
                var p2 = builder.equal(messageJoin.get("emailExpediteur"), criteria.getMailExpediteur());
                predicates.add(builder.or(p1, p2));
            } else {
                predicates.add(builder.equal(root.get("email"), criteria.getMailDestinataire()));
            }

        }

        // -----------------------------------------------
        // Critères de recherche définis par l'utilisateur
        // -----------------------------------------------

        /**
         * Référence de l'objet métier
         */
        if (!StringUtils.isEmpty(criteria.getRefObjetMetier())) {
            predicates.add(builder.equal(messageJoin.get("referenceObjetMetier"), criteria.getRefObjetMetier()));
		}

		/**
		 * Références objets métier
		 */
		// fixme: on garde ça ? sachant que le in est couteux, et que pour les ID on utilisaera des between
		//if (criteria.getReferencesObjetsMetier() != null && !criteria.getReferencesObjetsMetier().isEmpty()) {
		//	predicates.add(messageJoin.get("referenceObjetMetier").in(criteria.getReferencesObjetsMetier()));
		//}

		/**
		 * Object du message
		 */
		if (!StringUtils.isEmpty(criteria.getObjetMessage())) {
			predicates.add(builder.like(messageJoin.get("objet"), appendLikeWildcard(criteria.getObjetMessage())));
		}

		/**
		 * Destinataire (recherche approximative sur le nom et le mail)
		 */
		if (criteria.getNomOuMailDestList() != null && !criteria.getNomOuMailDestList().isEmpty()) {
			var p1 = cursor.get("email").in(criteria.getNomOuMailDestList());
			var p2 = cursor.get("nomContact").in(criteria.getNomOuMailDestList());
			predicates.add(builder.or(p1, p2));
		}

		/**
		 * Expéditeur (recherche approximative sur le nom et le mail)
		 */
		if (!StringUtils.isEmpty(criteria.getNomOuMailExp())) {
			var p1 = builder.like(messageJoin.get("nomCompletExpediteur"), appendLikeWildcard(criteria.getNomOuMailExp()));
			var p2 = builder.like(messageJoin.get("emailExpediteur"), appendLikeWildcard(criteria.getNomOuMailExp()));
			predicates.add(builder.or(p1, p2));
		}

		/*
		 * Date envoi début
		 * */
		if (criteria.getDateEnvoiDebut() != null) {
			predicates.add(builder.greaterThanOrEqualTo(cursor.get("dateEnvoi"), toLocalDateTime(criteria.getDateEnvoiDebut())));
		}

		/*
		 * Date envoi fin
		 * */
		if (criteria.getDateEnvoiFin() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dateEnvoi"), toLocalDateTime(criteria.getDateEnvoiFin())));
		}

		/*
		 * Date AR début
		 * */
		if (criteria.getDateARDebut() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get("dateAR"), toLocalDateTime(criteria.getDateARDebut())));
		}

		/*
		 * Date AR fin
		 * */
		if (criteria.getDateARFin() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dateAR"), toLocalDateTime(criteria.getDateARFin())));
		}

		/*
		 * Ids email
		 * */
		if (criteria.getIds() != null && !criteria.getIds().isEmpty()) {
			predicates.add(cursor.get("id").in(criteria.getIds()));
		}

		/**
		 * Statut du message : liste déroulante
		 */
		if (criteria.getStatuts() != null && !criteria.getStatuts().isEmpty()) {
			var statuts = criteria.getStatuts();
			predicates.add(cursor.get("statutEmail").in(statuts));
		}

		/**
		 *  emails sans réponse
		 */

		if (TRUE.equals(criteria.getSansReponse())) {
			predicates.add(builder.isEmpty(cursor.get("reponses")));
		}

		/**
		 * Statut à exclure
		 */
		if (criteria.getStatutsAExclure() != null && !criteria.getStatutsAExclure().isEmpty()) {
			var aExclure = criteria.getStatutsAExclure();
			predicates.add(builder.not(cursor.get("statutEmail").in(aExclure)));
		}

		/**
		 * criteres
		 */
		if (criteria.getCriteres() != null && criteria.getCriteres().getCritere() != null && !criteria.getCriteres().getCritere().isEmpty()) {
			var noms = criteria.getCriteres().getCritere().stream().map(SerializableRechercheMessage.Criteres.Critere::getNom).filter(Objects::nonNull).collect(Collectors.toSet());
			var valeurs = criteria.getCriteres().getCritere().stream().map(SerializableRechercheMessage.Criteres.Critere::getValeur).filter(Objects::nonNull).collect(Collectors.toSet());
			predicates.add(critereJoin.get("nom").in(noms));
			predicates.add(critereJoin.get("valeur").in(valeurs));
		}

		// -----------------------------------------------
		// Critères de recherche supplémentaires
		// -----------------------------------------------
		/*
		 * réponse attendue
		 * */
		if (Boolean.TRUE.equals(criteria.getReponseAttendue())) {
			predicates.add(builder.equal(messageJoin.get("reponseAttendue"), criteria.getReponseAttendue()));
		}

		/*
		 * Type message
		 * */
		if (criteria.getTypeMessage() != null) {
			predicates.add(builder.equal(messageJoin.get("typeMessage"), criteria.getReponseAttendue()));
		}

		/*
		 * dateDemandeEnvoiDebut
		 * */
		if (criteria.getDateDemandeEnvoiDebut() != null) {
			predicates.add(builder.greaterThanOrEqualTo(root.get("dateDemandeEnvoi"), toLocalDateTime(criteria.getDateDemandeEnvoiDebut())));
		}

		/*
		 * dateDemandeEnvoiFin
		 * */
		if (criteria.getDateDemandeEnvoiFin() != null) {
			predicates.add(builder.lessThanOrEqualTo(root.get("dateDemandeEnvoi"), toLocalDateTime(criteria.getDateDemandeEnvoiFin())));
		}

		/*
		 * pieceJointe (si le message a des pieces jointes)
		 * */
		if (!StringUtils.isEmpty(criteria.getPieceJointe())) {
			predicates.add(builder.isNotEmpty(messageJoin.get("piecesJointes")));
		}

		/*
		 * pieceJointe nom
		 * */
		if (!StringUtils.isEmpty(criteria.getPieceJointeNom())) {
			predicates.add(builder.like(pieceJointJoin.get("nom"), appendLikeWildcard(criteria.getPieceJointeNom())));
		}

		/*
		 * idMessageDestinataire
		 * */
		if (!StringUtils.isEmpty(criteria.getIdMessageDestinataire())) {
			predicates.add(builder.equal(messageJoin.get("id"), criteria.getIdMessageDestinataire()));
		}

		/*
		 * mots clés
		 * */
		if (!StringUtils.isEmpty(criteria.getMotsCles())) {

			var p1 = builder.like(messageJoin.get("objet"), appendLikeWildcard(criteria.getMotsCles()));
			var p2 = builder.like(messageJoin.get("contenu"), appendLikeWildcard(criteria.getMotsCles()));
			predicates.add(builder.or(p1, p2));
		}

		/*
		 * réponse non lue
		 * */

		if (TRUE.equals(criteria.getReponseNonLue())) {
			predicates.add(builder.equal(messageJoin.get("reponseAttendue"), true));
			predicates.add(builder.isNotEmpty(cursor.get("reponses")));
			predicates.add(builder.isNull(reponseJoin.get("dateAR")));
		}

		/*
		 * réponse lue
		 * */
		if (TRUE.equals(criteria.getReponseLue())) {
			predicates.add(builder.equal(messageJoin.get("reponseAttendue"), true));
			predicates.add(builder.isNotEmpty(cursor.get("reponses")));
			predicates.add(builder.isNotNull(reponseJoin.get("dateAR")));
		}

		/*
		 * favoris
		 * */
		if (TRUE.equals(criteria.getFavoris())) {
			predicates.add(builder.equal(cursor.get("favori"), true));
		}

		/*
		 * en attente reponse
		 * */
		if (TRUE.equals(criteria.getEnAttenteReponse())) {
			var status = Arrays.asList(ENVOYE, ACQUITTE, EN_ATTENTE_ACQUITTEMENT);
			predicates.add(cursor.get("statutEmail").in(status));
			predicates.add(builder.equal(messageJoin.get("reponseAttendue"), true));
			predicates.add(builder.isEmpty(cursor.get("reponses")));
		}

		/*
		 * Id email initial/parent
		 * */
		if (criteria.getIdMessageInitial() != null) {
			predicates.add(builder.equal(cursor.get("parent"), criteria.getIdMessageInitial()));
		}

		/*
		 * Code lien
		 * */
		if (!StringUtils.isEmpty(criteria.getCodeLien())) {
			predicates.add(builder.equal(cursor.get("codeLien"), criteria.getCodeLien()));
		}

		/*
		 * Initialisateur
		 * */

		if (criteria.getInitialisateur() != null) {
			if (criteria.getInitialisateur() == TypeInitialisateur.ENTREPRISE) {
				predicates.add(builder.equal(messageJoin.get("initialisateur"), criteria.getInitialisateur()));
			} else {
				predicates.add(builder.notEqual(messageJoin.get("initialisateur"), TypeInitialisateur.ENTREPRISE));
			}
		}

		/*
		  Destinataire (recherche approximative sur le nom et le mail)
		 */
		if (!StringUtils.isEmpty(criteria.getContact())) {
			var p1 = builder.like(cursor.get("nomContact"), appendLikeWildcard(criteria.getContact()));
			var p2 = builder.like(cursor.get("email"), appendLikeWildcard(criteria.getContact()));
			predicates.add(builder.or(p1, p2));
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null != sort) {
			query.orderBy(super.order(sort, builder, cursor));
		}
		query.distinct(true);
		return em.createQuery(query);
	}

	@Override
	public Class<Email> getTarget() {
		return Email.class;
	}
}
