package fr.atexo.messageriesecurisee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ReferentielRepository<T> extends JpaRepository<T, Long> {

    Optional<T> findByCode(String code);

}
