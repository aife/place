package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.Critere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CritereRepository extends JpaRepository<Critere, Integer> {
}
