package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.CleValeur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CleValeurRepository extends JpaRepository<CleValeur, Integer> {
}
