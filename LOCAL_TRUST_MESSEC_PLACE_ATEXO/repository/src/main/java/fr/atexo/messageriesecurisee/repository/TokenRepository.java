package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface TokenRepository extends JpaRepository<Token, String> {

    List<Token> findByType(String className);

    @Query("select token from TOKEN where dateCreation <= :before")
    Set<String> findByDateCreationBefore(@Param("before") LocalDateTime before);
}
