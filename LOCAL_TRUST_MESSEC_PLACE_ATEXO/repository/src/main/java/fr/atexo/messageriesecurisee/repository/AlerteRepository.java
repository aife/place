package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.Alerte;
import fr.atexo.messageriesecurisee.model.TypeStatutAlerte;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlerteRepository extends JpaRepository<Alerte, Long> {

	@Query("select distinct (a.id) from alerte a left join message m on m = a.message and length(m.contenu) < 1000000 where a.statut = :statut ")
	List<Long> findAllIdByStatut( @Param("statut") TypeStatutAlerte typeStatut, Pageable pageable);
}
