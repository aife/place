package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.DossierVolumineux;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DossierVolumineuxRepository extends JpaRepository<DossierVolumineux, Long> {
    DossierVolumineux findByUuidTechnique(String uuid);
}
