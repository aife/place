package fr.atexo.messageriesecurisee.repository;


import fr.atexo.messageriesecurisee.model.ErreurSMTP;
import org.springframework.stereotype.Repository;

@Repository
public interface ErreurSMTPRepository extends ReferentielRepository<ErreurSMTP> {


}
