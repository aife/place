package fr.atexo.messageriesecurisee.repository.impl;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.repository.MessagePaginableRepository;
import fr.atexo.messageriesecurisee.repository.criteria.BasePaginableRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.HashSet;
import java.util.function.Function;

@Repository
public class MessagePaginableRepositoryImpl extends BasePaginableRepository<Message, SerializableRechercheMessage> implements MessagePaginableRepository {

	@Override
	public <V> TypedQuery<V> requeter(
			SerializableRechercheMessage criteria,
			CriteriaQuery<V> query,
			Function<Expression<Message>, Expression<V>> projection,
			Sort sort
	) {
		final var builder = em.getCriteriaBuilder();
		final From<?, Message> cursor;

		final var predicates = new HashSet<Predicate>();

		final var root = query.from(Message.class);
		var emailJoin = root.join("destinataires", JoinType.INNER);
		query.select(projection.apply(root));
		// ne pas envoyer les réponses
		predicates.add(builder.isNull(emailJoin.get("parent")));

		cursor = root;

		/**
		 * Identifiant de la plateforme concernée
		 */
		if (!StringUtils.isEmpty(criteria.getIdPlateformeRecherche())) {
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
				predicates.add(builder.equal(root.get("identifiantPfEmetteur"), criteria.getIdPlateformeRecherche()));
			}
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
				predicates.add(builder.equal(root.get("identifiantPfDestinataire"), criteria.getIdPlateformeRecherche()));
			}
		}


		/**
		 * Identifiant de l'objet métier
		 */
		if (criteria.getIdObjetMetier() != null) {
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
				predicates.add(builder.equal(root.get("identifiantObjetMetierPlateFormeEmetteur"), criteria.getIdObjetMetier()));
			}
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
				predicates.add(builder.equal(root.get("identifiantObjetMetierPlateFormeDestinataire"), criteria.getIdObjetMetier()));
			}
		}

		/**
		 * Identifiants de l'objet métier
		 */
		if (criteria.getIdentifiantsObjetsMetier() != null && !criteria.getIdentifiantsObjetsMetier().isEmpty()) {
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.EMETTEUR) {
				predicates.add(root.get("identifiantObjetMetierPlateFormeEmetteur").in(criteria.getIdentifiantsObjetsMetier()));
			}
			if (criteria.getTypePlateformeRecherche() == SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE) {
				predicates.add(root.get("identifiantObjetMetierPlateFormeDestinataire").in(criteria.getIdentifiantsObjetsMetier()));
			}
		}

		/**
		 * Identifiant sous objet métier
		 */
		if (criteria.getIdSousObjetMetier() != null) {
			predicates.add(builder.equal(root.get("identifiantSousObjetMetier"), criteria.getIdSousObjetMetier()));
		}

		// -----------------------------------------------
		// Critères de recherche définis par l'utilisateur
		// -----------------------------------------------

		/**
		 * Référence de l'objet métier
		 */
		if (!StringUtils.isEmpty(criteria.getRefObjetMetier())) {
			predicates.add(builder.equal(root.get("referenceObjetMetier"), criteria.getRefObjetMetier()));
		}

		/**
		 * Statut du message : liste déroulante
		 */
		if (criteria.getStatuts() != null && !criteria.getStatuts().isEmpty()) {
			var statuts = criteria.getStatuts();
			predicates.add(emailJoin.get("statutEmail").in(statuts));
		}

		/*
		 * Code lien
		 * */
		if (!StringUtils.isEmpty(criteria.getCodeLien())) {
			predicates.add(builder.equal(emailJoin.get("codeLien"), criteria.getCodeLien()));
		}

		query.where(predicates.toArray(Predicate[]::new));

		if (null != sort) {
			query.orderBy(super.order(sort, builder, cursor));
		}
		query.distinct(true);
		return em.createQuery(query);
	}

	@Override
	public Class<Message> getTarget() {
		return Message.class;
	}
}
