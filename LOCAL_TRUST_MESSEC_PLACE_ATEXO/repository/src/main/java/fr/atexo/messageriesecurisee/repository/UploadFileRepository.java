package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.UploadFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Repository
public interface UploadFileRepository extends JpaRepository<UploadFile, Integer> {

    Optional<UploadFile> findByIdentifiant(String identifiant);

    @Query("select id from FICHIER_UPLOAD where dateCreation <= :before")
    Set<Integer> findByDateCreationBefore(@Param("before") LocalDateTime expirationDate);
}
