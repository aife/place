package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.repository.criteria.PaginableRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessagePaginableRepository extends PaginableRepository<Message, SerializableRechercheMessage> {
}
