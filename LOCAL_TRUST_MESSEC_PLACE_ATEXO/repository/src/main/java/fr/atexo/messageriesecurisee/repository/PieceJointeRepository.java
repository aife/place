package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.PieceJointe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PieceJointeRepository extends JpaRepository<PieceJointe, Integer> {
    Optional<PieceJointe> findByUuid(String uuid);

    Optional<PieceJointe> findByUuidAndMessageUuid(String uuid, String messageUuid);
}
