package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.repository.criteria.PaginableRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailPaginableRepository extends PaginableRepository<Email, SerializableRechercheMessage> {
}
