package fr.atexo.messageriesecurisee.repository.criteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class BasePaginableRepository<U, T> implements PaginableRepository<U, T> {

	private static final Logger LOG = LoggerFactory.getLogger(BasePaginableRepository.class);
	@PersistenceContext
	protected EntityManager em;

	@Override
	public List<U> rechercher(final T criteria) {
		final var builder = em.getCriteriaBuilder();
		final var query = builder.createQuery(this.getTarget());
		final var data = this.requeter(criteria, query, Function.identity(), null);

		return data.getResultList();
	}

	@Override
	public Page<U> rechercher(final T criteria, final Pageable pageable) {
		final var builder = em.getCriteriaBuilder();
		final var query = builder.createQuery(this.getTarget());
		final var data = this.requeter(criteria, query, Function.identity(), pageable.getSort());

		data.setFirstResult(Math.toIntExact(pageable.getOffset()));
		data.setMaxResults(pageable.getPageSize());

		final var resultList = data.getResultList();

		return new PageImpl<>(resultList, pageable, this.compter(criteria));
	}

	protected List<Order> order(Sort sort, CriteriaBuilder builder, From<?, ?> from) {
		return sort.stream()
				.map(
						order ->
								new OrderApplier(
										order.isAscending() ? builder::asc : builder::desc,
										travel(from, order.getProperty()).orElseThrow(() -> new IllegalArgumentException(String.format("Aucun attribut correspondant a %s", order.getProperty())))
								)
				)
				.map(OrderApplier::apply)
				.collect(Collectors.toList());
	}

	private Optional<Path<?>> travel(From<?, ?> from, String property) {
		var tokens = Arrays.stream(property.split("\\.")).collect(Collectors.toCollection(ArrayDeque::new));

		From<?, ?> temp = from;
		Path<?> result = null;

		do {
			var token = tokens.pop();

			if (tokens.isEmpty()) {
				result = temp.get(token);
			} else {
				temp = temp.join(token);
			}
		} while (!tokens.isEmpty());

		return Optional.ofNullable(result);
	}

	@Override
	public Long compter(final T criteria) {
		final var builder = em.getCriteriaBuilder();
		final var query = builder.createQuery(Long.class);
		final var contrats = this.requeter(criteria, query, builder::countDistinct, null);
		return contrats.getResultList().get(0);
	}

	private static class OrderApplier {

		private final Function<Expression<?>, Order> function;
		private final Path<?> path;

		public OrderApplier(Function<Expression<?>, Order> orderFunction, Path<?> pathToOrder) {
			this.function = orderFunction;
			this.path = pathToOrder;
		}

		public Order apply() {
			return this.function.apply(this.path);
		}
	}

	public static LocalDateTime toLocalDateTime(Date date) {
		if (date == null) {
			return null;
		}
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
	}

	public String appendLikeWildcard(String param) {
		return "%" + param + "%";
	}

}
