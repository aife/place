package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    @Query("select distinct m.identifiantObjetMetierPlateFormeEmetteur from message m where m.identifiantPfEmetteur = :identifiantPfEmetteur and m.referenceObjetMetier != '' and m.referenceObjetMetier is not null")
    List<Integer> findIdentifiantObjetMetierByPlatforme(@Param("identifiantPfEmetteur") String identifiantPfEmetteur);

    @Query("select distinct m from message m where m.identifiantPfEmetteur = :identifiantPfEmetteur and m.identifiantObjetMetierPlateFormeEmetteur =:identifiantObjetMetier and m.referenceObjetMetier != '' and m.referenceObjetMetier is not null")
    List<Message> findMessagesByPlatforme(@Param("identifiantPfEmetteur") String identifiantPfEmetteur, @Param("identifiantObjetMetier") Integer identifiantObjetMetier);

    @Query("select distinct m from message m join m.destinataires d where m.identifiantPfEmetteur = :identifiantPfEmetteur and m.identifiantObjetMetierPlateFormeEmetteur = :identifiantObjetMetier and d.statutEmail = 'BROUILLON' order by d.dateModificationBrouillon desc ")
    Page<Message> findBrouillons(@Param("identifiantPfEmetteur") String identifiantPfEmetteur, @Param("identifiantObjetMetier") Integer identifiantObjetMetier, Pageable pageable);

    @Query("select distinct m from message m join m.destinataires d where m.identifiantPfEmetteur = :identifiantPfEmetteur and m.identifiantObjetMetierPlateFormeEmetteur in (:identifiantsObjetMetier) and d.statutEmail = 'BROUILLON' order by d.dateModificationBrouillon desc ")
    Page<Message> findBrouillons(@Param("identifiantPfEmetteur") String identifiantPfEmetteur, @Param("identifiantsObjetMetier") Set<Integer> identifiantsObjetMetier, Pageable pageable);

    Optional<Message> findByUuid(String uuid);
}
