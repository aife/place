package fr.atexo.messageriesecurisee.repository.datasets;

import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;

import java.util.UUID;

public class EmailDataset {

    public static String CODE_LIEN = "code_lien";

    public static String EMAIL = "tu@atexo.com";

    public static Email getEmail(Message message) {
        return Email.builder()
                .typeDestinataire(TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE)
                .statutEmail(TypeStatutEmail.EN_COURS_ENVOI)
                .codeLien(CODE_LIEN)
                .email(EMAIL)
                .message(message)
                .uuid(UUID.randomUUID().toString())
                .build();
    }
}
