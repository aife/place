package fr.atexo.messageriesecurisee.repository.datasets;

import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.model.Message;

import java.util.UUID;

public class MessageDataset {

    public static String PF_EMETTEUR = "PF1";
    public static String PF_DESTINATAIRE = "PF2";
    public static Integer ID_OBJET_PF_EMETTEUR = 1;
    public static Integer ID_OBJET_PF_DESTINATAIRE = 2;
    public static String REFERENCE = "REF1";
    public static String CONTENU = "CONTENU";
    public static String EMAIL_EXPEDITEUR = "email_expediteur@atexo.com";
    public static TypeMessage TYPE_MESSAGE = TypeMessage.TypeMessage1;

    public static Message getMessage() {
        return Message.builder()
                .identifiantPfEmetteur(PF_EMETTEUR)
                .identifiantPfDestinataire(PF_DESTINATAIRE)
                .nomPfEmetteur(PF_EMETTEUR)
                .nomPfDestinataire(PF_DESTINATAIRE)
                .identifiantObjetMetierPlateFormeEmetteur(ID_OBJET_PF_EMETTEUR)
                .identifiantObjetMetierPlateFormeDestinataire(ID_OBJET_PF_DESTINATAIRE)
                .referenceObjetMetier(REFERENCE)
                .urlPfEmetteur("URL_PF1")
                .urlPfDestinataire("URL_PF2")
                .contenu(CONTENU)
                .emailExpediteur(EMAIL_EXPEDITEUR)
                .typeMessage(TYPE_MESSAGE)
                .uuid(UUID.randomUUID().toString())
                .build();
    }
}
