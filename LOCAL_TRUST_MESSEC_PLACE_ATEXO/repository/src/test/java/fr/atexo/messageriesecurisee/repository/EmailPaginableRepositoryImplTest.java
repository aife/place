package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.repository.config.RepositoryConfig;
import fr.atexo.messageriesecurisee.repository.datasets.CritereDataset;
import fr.atexo.messageriesecurisee.repository.datasets.EmailDataset;
import fr.atexo.messageriesecurisee.repository.datasets.MessageDataset;
import fr.atexo.messageriesecurisee.repository.datasets.PieceJointeDataset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.repository.datasets.CritereDataset.CRITERE_NOM;
import static fr.atexo.messageriesecurisee.repository.datasets.CritereDataset.CRITERE_VALEUR;
import static fr.atexo.messageriesecurisee.repository.datasets.EmailDataset.CODE_LIEN;
import static fr.atexo.messageriesecurisee.repository.datasets.MessageDataset.*;
import static fr.atexo.messageriesecurisee.repository.datasets.PieceJointeDataset.PJ_NOM;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfig.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class EmailPaginableRepositoryImplTest {

    @Autowired
    EmailPaginableRepository emailPaginableRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    CritereRepository critereRepository;

    @Autowired
    PieceJointeRepository pieceJointeRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testPetsistMessage() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message = messageRepository.saveAndFlush(message);
        assertEquals("count messages en BDD", 1, messageRepository.findAll().size());
    }

    @Test
    public void requeterObjet() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setObjetMessage("message1");
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur l'objet ou le contenu", 1, results.size());
    }

    @Test
    public void requeterContenu() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message.setContenu("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setMotsCles("message1");
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur l'objet ou le contenu", 1, results.size());
    }

    @Test
    public void requeterCodeLien() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message.setContenu("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setCodeLien(CODE_LIEN);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur le code lien", 1, results.size());
    }

    @Test
    public void requeterStatut() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message.setContenu("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        var statut = TypeStatutEmail.ACQUITTE;
        email.setStatutEmail(statut);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setStatuts(List.of(statut));
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur le statut", 1, results.size());
    }

    @Test
    public void requeterStatutAsclure() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message.setContenu("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        var statutAExclure = TypeStatutEmail.BROUILLON;
        email.setStatutEmail(statutAExclure);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setStatutsAExclure(List.of(statutAExclure));
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur le statut à exclure", 0, results.size());
    }

    @Test
    public void requeterIdentifiantPfEmetteur() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setTypePlateformeRecherche(SerializableRechercheMessage.TypeDestinataire.EMETTEUR);
        critere.setIdObjetMetier(ID_OBJET_PF_EMETTEUR);
        critere.setIdPlateformeRecherche(PF_EMETTEUR);
        critere.setRefObjetMetier(REFERENCE);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur PF emettrice", 1, results.size());
    }

    @Test
    public void requeterIdentifiantPfDestinataire() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setTypePlateformeRecherche(SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE);
        critere.setIdObjetMetier(ID_OBJET_PF_DESTINATAIRE);
        critere.setIdPlateformeRecherche(PF_DESTINATAIRE);
        critere.setRefObjetMetier(REFERENCE);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur PF destinataire", 1, results.size());
    }

    @Test
    public void requeterReponseAttendue() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setReponseAttendue(true);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur réponse attendue", 1, results.size());
    }

    @Test
    public void requeterFavoris() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email.setFavori(true);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setReponseAttendue(true);
        critere.setFavoris(true);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur réponse attendue ET favoris", 1, results.size());
    }

    @Test
    public void requeterEmailExpediteur() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setNomOuMailExp(EMAIL_EXPEDITEUR);
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur email expediteur", 1, results.size());
    }

    @Test
    public void requeterEmailDestinataireOrEmailExpediteur() {
        var message1 = MessageDataset.getMessage();
        message1 = messageRepository.saveAndFlush(message1);
        var message2 = MessageDataset.getMessage();
        message2.setEmailExpediteur("expediteur@atexo.com");
        message2 = messageRepository.saveAndFlush(message2);
        var email1 = EmailDataset.getEmail(message1);
        email1 = emailRepository.saveAndFlush(email1);
        var email2 = EmailDataset.getEmail(message2);
        email2.setEmail("agent@atexo.com");
        email2 = emailRepository.saveAndFlush(email2);

        var critere = new SerializableRechercheMessage();
        critere.setMailDestinataire(email1.getEmail());
        critere.setMailExpediteur(message2.getEmailExpediteur());
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur email destinataire OU expediteur", results.size(), 2);
    }

    @Test
    public void requeterCritere() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);
        var critere = CritereDataset.getCritere();
        critere.setMessage(message);
        critereRepository.saveAndFlush(critere);

        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        criteria.setCriteres(new SerializableRechercheMessage.Criteres());
        var critereRecherche = new SerializableRechercheMessage.Criteres.Critere();
        critereRecherche.setNom(CRITERE_NOM);
        critereRecherche.setValeur(CRITERE_VALEUR);
        criteria.getCriteres().getCritere().add(critereRecherche);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur critere libre", 1, results.size());
    }

    @Test
    public void requeterPiecesJointes() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);
        var pieceJointe = PieceJointeDataset.getPieceJointe();
        pieceJointe.setMessage(message);
        pieceJointeRepository.saveAndFlush(pieceJointe);

        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        criteria.setPieceJointe("OUI");
        criteria.setPieceJointeNom(PJ_NOM.substring(2));
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur piece jointe", 1, results.size());
    }

    @Test
    public void requeterDateDemandeEnvoiDebut() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setDateDemandeEnvoi(LocalDateTime.now());
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        var cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        criteria.setDateDemandeEnvoiDebut(cal.getTime());
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur date demande envoi début", 1, results.size());
    }

    @Test
    public void requeterDateDemandeEnvoiFin() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setDateDemandeEnvoi(LocalDateTime.now());
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        var cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        criteria.setDateDemandeEnvoiFin(cal.getTime());
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur date demande envoi fin", 1, results.size());
    }

    @Test
    public void requeterDateEnvoiDebut() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setDateEnvoi(LocalDateTime.now());
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        var cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        criteria.setDateEnvoiDebut(cal.getTime());
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur date envoi début", 1, results.size());
    }

    @Test
    public void requeterDateEnvoiFin() {
        var message = MessageDataset.getMessage();
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setDateEnvoi(LocalDateTime.now());
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        var cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        criteria.setDateEnvoiFin(cal.getTime());
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur date envoi fin", 1, results.size());
    }

    @Test
    public void requeterEnAttenteReponse() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setStatutEmail(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT);
        email = emailRepository.saveAndFlush(email);
        var criteria = new SerializableRechercheMessage();
        criteria.setEnAttenteReponse(true);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur en attente réponse", 1, results.size());
    }

    @Test
    public void requeterReponseLue() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var reponse = EmailDataset.getEmail(message);
        reponse.setParent(email);
        reponse.setDateAR(LocalDateTime.now());
        emailRepository.saveAndFlush(reponse);
        var criteria = new SerializableRechercheMessage();
        criteria.setReponseLue(true);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur réponse lue", 1, results.size());
    }

    @Test
    public void requeterSansReponse() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email.setStatutEmail(TypeStatutEmail.ACQUITTE);
        email = emailRepository.saveAndFlush(email);
        var reponse = EmailDataset.getEmail(message);
        reponse.setParent(email);
        reponse.setDateAR(LocalDateTime.now());
        emailRepository.saveAndFlush(reponse);
        var criteria = new SerializableRechercheMessage();
        criteria.setSansReponse(true);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur réponse lue", 0, results.size());
    }

    @Test
    public void requeterReponseNonLue() {
        var message = MessageDataset.getMessage();
        message.setReponseAttendue(true);
        message = messageRepository.saveAndFlush(message);

        var email = EmailDataset.getEmail(message);
        email = emailRepository.saveAndFlush(email);
        var reponse = EmailDataset.getEmail(message);
        reponse.setParent(email);
        reponse.setDateAR(null);
        emailRepository.saveAndFlush(reponse);
        var criteria = new SerializableRechercheMessage();
        criteria.setReponseNonLue(true);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur réponse non lue", 1, results.size());
    }

    @Test
    public void requeterMultiReferences() {
        var ids = Set.of(1, 2, 3);
        var refs = ids.stream().map(id -> "ref_" + id).collect(Collectors.toSet());
        for (var id : ids) {
            var message = MessageDataset.getMessage();
            message.setIdentifiantObjetMetierPlateFormeEmetteur(id);
            message.setReferenceObjetMetier("ref_" + id);
            message = messageRepository.saveAndFlush(message);

            var email = EmailDataset.getEmail(message);
            email = emailRepository.saveAndFlush(email);
        }

        var criteria = new SerializableRechercheMessage();
        criteria.setIdentifiantsObjetsMetier(ids);
        criteria.setReferencesObjetsMetier(refs);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur les refs & ids des objets métier", results.size(), ids.size());
    }

    @Test
    public void requeterContact() {
        var message = MessageDataset.getMessage();
        message.setObjet("message1");
        message.setContenu("message1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email.setEmail("test@atexo.com");
        email.setNomContact("contact");
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setContact("atex");
        var results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur l'email du contact", 1, results.size());

        critere.setContact("cont");
        results = emailPaginableRepository.rechercher(critere);
        assertEquals("criteres sur le nom du contact", 1, results.size());
    }

    @Test
    public void requeterPerimetreMesConsultations() {
        var message1 = MessageDataset.getMessage();
        message1.setIdentifiantObjetMetierPlateFormeEmetteur(1);
        message1 = messageRepository.saveAndFlush(message1);

        var message2 = MessageDataset.getMessage();
        message2.setIdentifiantObjetMetierPlateFormeEmetteur(2);
        message2 = messageRepository.saveAndFlush(message2);

        var email1 = EmailDataset.getEmail(message1);
        email1 = emailRepository.saveAndFlush(email1);

        var email2 = EmailDataset.getEmail(message2);
        email2 = emailRepository.saveAndFlush(email2);

        var criteria = new SerializableRechercheMessage();
        criteria.setIdentifiantsObjetsMetier(Set.of(1, 2));
        criteria.setIdentifiantsObjetsMetierAgentConnecte(Set.of(2));
        criteria.setMesConsultations(true);
        var results = emailPaginableRepository.rechercher(criteria);
        assertEquals("criteres sur le périmètre de vision", 1, results.size());
    }
}
