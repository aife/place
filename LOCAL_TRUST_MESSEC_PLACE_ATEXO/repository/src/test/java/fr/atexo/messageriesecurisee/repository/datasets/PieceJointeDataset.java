package fr.atexo.messageriesecurisee.repository.datasets;

import fr.atexo.messageriesecurisee.model.PieceJointe;

import java.util.UUID;

public class PieceJointeDataset {

    public static String PJ_NOM = "piece_jointe";

    public static PieceJointe getPieceJointe() {
        return PieceJointe.builder()
                .nom(PJ_NOM)
                .chemin("/tmp/")
                .taille(40000)
                .uuid(UUID.randomUUID().toString())
                .build();
    }
}
