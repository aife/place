package fr.atexo.messageriesecurisee.repository;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.repository.config.RepositoryConfig;
import fr.atexo.messageriesecurisee.repository.datasets.EmailDataset;
import fr.atexo.messageriesecurisee.repository.datasets.MessageDataset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfig.class}, loader = AnnotationConfigContextLoader.class)
@Transactional
public class MessagePaginableRepositoryImplTest {

    @Autowired
    MessagePaginableRepository messagePaginableRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    EmailRepository emailRepository;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void requeterStatutEmail() {
        var message = MessageDataset.getMessage();
        message.setIdentifiantPfEmetteur("PF1");
        message = messageRepository.saveAndFlush(message);
        var email = EmailDataset.getEmail(message);
        email.setStatutEmail(TypeStatutEmail.BROUILLON);
        email = emailRepository.saveAndFlush(email);
        var critere = new SerializableRechercheMessage();
        critere.setIdPlateformeRecherche("PF1");
        critere.setStatuts(asList(TypeStatutEmail.BROUILLON));
        var results = messagePaginableRepository.rechercher(critere);
        assertEquals("criteres sur l'identifiant PF et le statut de l'email", results.size(), 1);
    }
}
