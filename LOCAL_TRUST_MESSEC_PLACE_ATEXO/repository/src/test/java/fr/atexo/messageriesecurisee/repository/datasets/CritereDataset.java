package fr.atexo.messageriesecurisee.repository.datasets;

import fr.atexo.messageriesecurisee.model.Critere;

import java.util.UUID;

public class CritereDataset {

    public static String CRITERE_NOM = "template";
    public static String CRITERE_VALEUR = "id_1";

    public static Critere getCritere() {
        return Critere.builder()
                .nom(CRITERE_NOM)
                .valeur(CRITERE_VALEUR)
                .uuid(UUID.randomUUID().toString())
                .build();
    }
}
