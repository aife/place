const concat = require('concat');
const messecWebComponent = './dist/ng-messagerie/';
const fs = require('fs');

const originalFiles = ['runtime.js', 'polyfills.js', 'main.js', 'scripts.js'];
let build = async (prefix) => {
  const files = originalFiles.map(file => `${messecWebComponent}/${file}`);
  await concat(files, `${messecWebComponent}/messec${prefix}.js`);
}
let copyFiles = async () => {
  for (let file of originalFiles) {
    fs.copyFileSync(`${messecWebComponent}/${file}`, messecWebComponent + "/" + file.replace('.js', '-es2015.js'));
  }
}

console.log("build des web components messec")
build('').then(r => console.log("build terminé"));
build('-es2015').then(r => console.log("build es2015 terminé"));
copyFiles().then(r => console.log("copy terminé"));

