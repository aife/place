import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, Inject, Injector, NgModule, Type} from '@angular/core';
import {APPLICATION_PIPES} from './pipes/pipes';
import {DocumentUploadComponent} from './components/document-upload/document-upload.component';
import {MessagesComponent} from './components/messages/messages.component';
import {RedactionComponent} from './components/redaction/redaction.component';
import {DOCUMENT} from '@angular/common';
import {QuillModule} from 'ngx-quill';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FiltresComponent} from './components/filtres/filtres.component';
import {DetailComponent} from './components/detail/detail.component';
import {PaginationComponent} from './components/pagination/pagination.component';
import {PiecesJointesComponent} from './components/pieces-jointes/pieces-jointes.component';
import {SuiviEntrepriseComponent} from './components/suivi-entreprise/suivi-entreprise.component';
import {
  VisualisationEntrepriseComponent
} from './components/visualisation-entreprise/visualisation-entreprise.component';
import {SucessModalComponent} from './components/modals/SuccessModal';
import {ErrorModalComponent} from './components/modals/ErrorModal';
import {ConfirmationModalComponent} from './components/modals/ConfirmationModal';
import {MessagesAgentComponent} from "./components/messages-agent/messages-agent.component";
import {BrouillonComponent} from "./components/messages-agent/brouillon/brouillon.component";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {ModalModule} from "ngx-bootstrap/modal";
import {TabsModule} from "ngx-bootstrap/tabs";
import {CollapseModule} from "ngx-bootstrap/collapse";
import {ButtonsModule} from "ngx-bootstrap/buttons";
import {TooltipModule} from "ngx-bootstrap/tooltip";
import {createCustomElement} from "@angular/elements";
import {UiSwitchModule} from "ngx-ui-switch";
import {RedactionEntrepriseComponent} from './components/redaction-entreprise/redaction-entreprise.component';
import {LigneEmailAgentComponent} from './components/messages/ligne-email-agent/ligne-email-agent.component';
import {
  LigneEmailEntrepriseComponent
} from './components/suivi-entreprise/ligne-email-entreprise/ligne-email-entreprise.component';
import {AuthInterceptor} from "src/app/security/auth-interceptor";
import {EditeurHtmlComponent} from './components/editeur-html/editeur-html.component';
import {TimepickerModule} from "ngx-bootstrap/timepicker";
import {ReferentielService} from "src/app/services/referentiel.service";
import {setTheme} from 'ngx-bootstrap/utils';
import {DropzoneModule} from "ngx-dropzone-wrapper";

function initializeRefs(referentielService: ReferentielService): () => Promise<any> {
  console.debug("chargement des référentiels");
  return () => referentielService.load().toPromise();
}

@NgModule({
  declarations: [
    RedactionComponent,
    MessagesComponent,
    MessagesAgentComponent,
    DocumentUploadComponent,
    APPLICATION_PIPES,
    FiltresComponent,
    DetailComponent,
    PaginationComponent,
    PiecesJointesComponent,
    SuiviEntrepriseComponent,
    VisualisationEntrepriseComponent,
    SucessModalComponent,
    ErrorModalComponent,
    ConfirmationModalComponent,
    BrouillonComponent,
    RedactionEntrepriseComponent,
    LigneEmailAgentComponent,
    LigneEmailEntrepriseComponent,
    BrouillonComponent,
    EditeurHtmlComponent
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {
      provide: APP_INITIALIZER,
      useFactory: initializeRefs,
      multi: true,
      deps: [ReferentielService]
    }
  ],
  imports: [
    HttpClientXsrfModule.disable(),
    BrowserModule, BrowserAnimationsModule, BsDatepickerModule.forRoot(), TimepickerModule.forRoot(), ModalModule.forRoot(), TooltipModule.forRoot(),
    QuillModule.forRoot(), FormsModule, NgSelectModule, HttpClientModule, ButtonsModule, CollapseModule.forRoot(), TabsModule.forRoot(),
    BrowserAnimationsModule,
    NgSelectModule, HttpClientModule, ButtonsModule, CollapseModule.forRoot(), ReactiveFormsModule, BrowserModule,
    UiSwitchModule, DropzoneModule
  ],
  exports: [ModalModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  private browserDocument;

  constructor(@Inject(DOCUMENT) private document: any, private injector: Injector) {
    setTheme('bs4');
    this.browserDocument = document;
    // redaction agent Web Component
    this.registerWebComponent('messagerie-redaction', RedactionComponent, injector);

    // suivi agent Web Component
    this.registerWebComponent('messagerie-suivi', MessagesAgentComponent, injector);

    // suivi entreprise Web Component
    this.registerWebComponent('messagerie-suivi-entreprise', SuiviEntrepriseComponent, injector);

    // visualisation entreprise Web Component
    this.registerWebComponent('messagerie-visualisation-entreprise', VisualisationEntrepriseComponent, injector);

    // redaction entreprise Web Component
    this.registerWebComponent('messagerie-redaction-entreprise', RedactionEntrepriseComponent, injector);

  }

  private registerWebComponent(tagName: string, component: Type<any>, injector: Injector) {
    if (!customElements.get(tagName)) {
      console.log(`registring ${tagName}`)
      const webComponent = createCustomElement(component, {injector: injector});
      customElements.define(tagName, webComponent);
    } else {
      console.log(`${tagName} already registred`)
    }
  }

  ngDoBootstrap(appRef: ApplicationRef) {
    console.log(`Bootstrap !`);
  }
}

