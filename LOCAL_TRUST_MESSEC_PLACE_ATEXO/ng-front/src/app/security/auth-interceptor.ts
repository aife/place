import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {MS_ACCESS_TOKEN, MS_WEBSERVICE_ENDPOINT, MS_WEBSERVICE_SECURE_ENDPOINT} from "src/app/messagerie-constants";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const accessToken = localStorage.getItem(MS_ACCESS_TOKEN);

    if (accessToken && req.url && req.url.indexOf(MS_WEBSERVICE_ENDPOINT) != -1) {
      console.debug(`passage en mode sécurisé pour l'appel ${req.url}`)
      const cloned = req.clone({
        url: req.url.replace(MS_WEBSERVICE_ENDPOINT, MS_WEBSERVICE_SECURE_ENDPOINT),
        headers: req.headers.set("Authorization", "Bearer " + accessToken)
      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}
