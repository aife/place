import {DropzoneFile} from "dropzone";

export interface SerializableRechercheMessage {
  token?: string;
  typePlateformeRecherche?: string | TypeDestinataire;
  idPlateformeRecherche?: string;
  idObjetMetier?: string;
  idContactDestList?: number[];
  idEntrepriseDest?: string;
  mailDestinataire?: string;
  refObjetMetier?: string;
  objetMessage?: string;
  nomOuMailDest?: string;
  nomOuMailExp?: string;
  dateEnvoiDebut?: Date | string;
  dateEnvoiFin?: Date | string;
  dateARDebut?: Date;
  dateARFin?: Date;
  ids?: any;
  statuts?: TypeStatutEmail[];
  statutsAExclure?: TypeStatutEmail[];
  criteres?: Criteres;
  reponseAttendue?: boolean;
  typeMessage?: string;
  dateDemandeEnvoiDebut?: Date | string;
  dateDemandeEnvoiFin?: Date | string;
  pieceJointe?: string;
  pieceJointeNom?: string;
  idMessageDestinataire?: string;
  nomOuMailDestList?: string[];
  motsCles?: string;
  enAttenteReponse?: boolean;
  reponseAttendu?: boolean;
  reponseNonLue?: boolean;
  reponseLue?: boolean;
  inclureReponses?: boolean;
  exclureReponses?: boolean;
  favoris?: boolean;
  objetsMetier?: ObjetMetierDTO[]
  filtresAppliques?: FiltreDTO;
  sansReponse?: boolean;
  afficherFiltreEntreprise?: boolean;
  initialisateur?: TypeInitialisateur;
  mesConsultations?: boolean;
  agent?: Agent;
}

export interface ObjetMetierDTO {
  identifiant?: number;
  reference?: string;
  objet?: string;
  urlSuivi?: string;
  urlRetour?: string;
}

export interface CritereDTO extends AbstractDTO {
  id?: number;
  nom?: string;
  valeur?: string;
  libelle?: string;
}

export interface DestinataireDTO {
  typeDestinataire?: string | TypeDestinataire;
  idEntrepriseDest?: string;
  idContactDest?: number;
  nomEntrepriseDest?: string;
  nomContactDest?: string;
  mailContactDestinataire?: string;
  type?: string;
  disabled?: boolean;
  ordre?: number;
}

export interface EmailDTO extends AbstractDTO {
  id?: number;
  typeDestinataire?: string | TypeDestinataire;
  identifiantObjetMetier?: string;
  identifiantEntreprise?: string;
  identifiantContact?: number;
  nomEntreprise?: string;
  nomContact?: string;
  email?: string;
  emailConnecte?: string;
  statutEmail?: string | TypeStatutEmail;
  codeLien?: string;
  dateDemandeEnvoi?: number;
  dateEnvoi?: number;
  dateAR?: number;
  nombreEssais?: number;
  erreur?: string;
  identifiantEntrepriseAR?: string;
  identifiantContactAR?: string;
  nomEntrepriseAR?: string;
  nomContactAR?: string;
  emailContactAR?: string;
  message?: MessageDTO;
  dateDemandeEnvoiAsDate?: Date;
  dateEnvoiAsDate?: Date;
  dateARAsDate?: Date;
  reponse?: EmailDTO;
  dateModificationBrouillonAsDate?: Date;
  toutesLesReponses: Array<EmailDTO>;
  template?: Template;
  connecte?: boolean;
  dateRelance?: Date;
  dateEchec?: Date;
  favori?: boolean;
  reponseMasquee?: boolean;
  reponseExpiree?: boolean;
  dateLimiteExpiree?: boolean;
  reponseBloquee?: boolean;
  enAttenteEnvoi?: boolean;
  echecEnvoi?: boolean;
  delivre?: boolean;
  lu?: boolean;
  envoye?: boolean;
  emailEntreprise?: boolean;
  traite?: boolean;
}

export interface MessageDTO extends AbstractDTO {
  id?: number;
  typeMessage?: string | TypeMessage;
  cartouche?: string;
  contenu?: string;
  contenuHTML?: boolean;
  objet?: string;
  reponseAttendue?: boolean;
  reponseAgentAttendue?: boolean;
  criteres?: CritereDTO[];
  dateModification?: Date;
  piecesJointes?: PieceJointeDTO[];
  statut?: string | TypeStatutEmail;
  destsPfEmettreur?: DestinataireDTO[];
  destsPfDestinataire?: DestinataireDTO[];
  destinatairesPreSelectionnes?: DestinataireDTO[];
  templateCritere?: string;
  emailsAlerteReponse?: string;
  dateReponseAttendue?: boolean;
  dateNow?: Date;
  dateLimiteReponseAsDate?: Date;
  dateLimiteReponseAttendue?: boolean;
  reponseBloque?: boolean;
  reponseLectureBloque?: boolean;
  nbDestinataires?: number;
  destinatairesNotifies?: DestinataireDTO[];
  maxTailleFichiers?: number;
  templateMail?: string;
  templateFige?: boolean;
  nomCompletExpediteur?: string;
  limitationTailleFichier?: any;
  maxCaracteresContenu?: number;
  codeLien?: string;
  brouillon?: boolean;
  dossiersVolumineux?: DossierVolumineuxDTO[];
  dossierVolumineux?: DossierVolumineuxDTO;
  identifiantObjetMetierPlateFormeEmetteur?: number;
  initialisateur?: string | TypeInitialisateur;
  emailExpediteur?: string;
  nomContactExpediteur?: string;
}

export interface PieceJointeDTO extends AbstractDTO {
  id?: string;
  idExterne?: string;
  reference?: string | Object;
  nom?: string;
  chemin?: string;
  taille?: number;
  contentType?: string;
  file?: DropzoneFile;
  progress?: number;
  erreur?: string;
  pending?: boolean;
}

export interface DocumentDTO extends AbstractDTO {
  base?: string;
  extension?: string;
  fichier?: string;
  id?: string
  nom?: string;
  poids?: number;
  type?: string;
  url_telechargement?: string;
}

export enum TypeDestinataire {
  DESTINATAIRE_PLATEFORME_EMETTRICE, DESTINATAIRE_PLATEFORME_DESTINATAIRE
}

export enum TypeMessage {
  Unknown, TypeMessage1, TypeMessage2, TypeMessage3, TypeMessage4
}

export enum TypeStatutEmail {
  BROUILLON,
  EN_ATTENTE_ENVOI,
  EN_COURS_ENVOI,
  ECHEC_ENVOI,
  ENVOYE,
  EN_ATTENTE_ACQUITTEMENT,
  ACQUITTE,
  ECHEC_PARTIEL,
  ACQUITTEMENT_PARTIEL,
  TRAITE
}

export enum TypeInitialisateur {
  AGENT, ENTREPRISE
}


export interface Criteres {
  critere?: Critere[];
}

export interface Critere extends AbstractDTO {
  nom?: string;
  valeur?: string;
}

export class PaginationPage<T> {
  content?: Array<T>;
  last?: boolean;
  first?: boolean;
  number: number;
  size: number;
  totalPages?: number;
  totalElements?: number;
  sort?: Array<PaginationPropertySort>;
}

export interface PaginationPropertySort {
  direction: string;
  property: string;
}

// templates
export interface MailType {
  label?: string;
  code?: string;
}

export interface Template {
  id: number;
  mailType?: MailType;
  code?: string;
  objet?: string;
  corps?: string;
  ordreAffichage: number;
  envoiModalite?: string;
  envoiModaliteFigee?: boolean;
  reponseAttendue?: boolean;
  reponseAgentAttendue?: boolean;
  reponseAttendueFigee?: boolean;
}

export interface ContexteDTO {
  token?: string;
  access_token?: string;
  url_suivi?: string;
  url_retour_consultation?: string;
  url_nouveau_message?: string;
  url_modification_message?: string;
  id_template_selectionne?: string;
  url_visualisation_message?: string;
  url_callback_reponse_a_question?: string;
  id_espace_doc_modal?: string;
  templates_path?: string;
  url_visualisation_message_legacy?: string;
  mode_legacy?: boolean;
  peut_creer_message?: boolean;
  peut_modifier_message?: boolean;
}

export interface PageChangeEvent {
  currentPage: number;
  pageSize: number;
}

export interface EvenementDTO {
  nom: string;
  date: Date;
  icone: string;
  style: string;
  titre: string;
  reponse?: EmailDTO;
  erreurSMTP?: ErreurSMTP;
}

export interface DossierVolumineuxDTO {
  id?: number;
  dateCreationAsDate?: Date;
  nom?: string;
  taille?: number;
  uuidReference?: string;
  uuidTechnique?: string;
  libelle?: string;
}

export interface ExportDTO {
  templates: Array<CritereDTO>;
  critere: SerializableRechercheMessage;
}

export interface FiltreDTO {
  reponseLueEntreprise?: boolean;
  reponseNonLueEntreprise?: boolean;
  statutAcquitteEntreprise?: boolean;
  statutDelivreEntreprise?: boolean;
  statutEnCourEnvoi?: boolean;
  statutNonDelivre?: boolean;
  statutDelivre?: boolean;
  statutAcquitte?: boolean;
  dateEnvoiStatut?: boolean;
  reponseAttendue?: boolean;
  reponseLue?: boolean;
  reponseNonLue?: boolean;
  statutTraite?: boolean;
}

export interface AbstractDTO {

  dateCreation?: Date;
  dateModification?: Date;
  uuid?: string;
}

export interface CodeLabelDTO {

  code?: string;
  label?: string;
}

export interface ErreurSMTP {
  code?: string;
  description?: string;
  erreur?: string;
}

export interface ConfigurationDTO {
  antivirusActif: boolean;
  delaiPooling?: number;
}

export interface Agent {
  id?: number;
  identifiant?: string;
  email?: string;
  nom?: string;
  prenom?: string;
  acronymeOrganisme;
}
