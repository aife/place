import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ContextService} from "src/app/services/context.service";

@Injectable({
  providedIn: 'root'
})
export class MpeService {

  constructor(private http: HttpClient, private contextService: ContextService) {
  }

  getTemplates() {
    return this.http.get<any>(this.contextService.templatesPath);
  }

  acquitterReponse(url: string) {
    console.log(`Acquittement de la réponse à la question en utilisant l'url: ${url}`);
    if (url) {

      const date = new Date();
      const urlRequest = `${url}${url.indexOf('?') > 0 ? '&' : '?'}dateReponse=${date.toISOString()}`
      console.log(`Acquittement de la réponse en utilisant l'url : ${urlRequest}`)
      this.http.get(urlRequest).subscribe(r => {
        console.log(`Acquittement de la réponse à la question en utilisant l'url: ${url} OK, la réponse est : ${r}`);
      })
    }

  }
}
