import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  ConfigurationDTO,
  DestinataireDTO,
  EmailDTO,
  EvenementDTO,
  ExportDTO,
  MessageDTO,
  PaginationPage,
  PaginationPropertySort,
  PieceJointeDTO,
  SerializableRechercheMessage
} from '../messagerie-dto';
import {DATE_TIME_ISO_FORMAT, MS_WEBSERVICE_ENDPOINT} from '../messagerie-constants';
import * as moment from 'moment';
import {saveAs} from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class MessagerieService {
  constructor(private http: HttpClient) {
    this._token = ""
  }

  _token: string;

  public set token(value: string) {
    this._token = value;
  }

  findEmails(emailCriteriaDTO: SerializableRechercheMessage, page: number, pageSize: number,
             sort: PaginationPropertySort): Observable<PaginationPage<EmailDTO>> {
    const httpParams = {size: '' + pageSize, page: '' + page, sort: ''};
    if (sort) {
      httpParams.sort = `${sort.property},${sort.direction}`;
    }
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/messages/?token=${this._token}`;
    console.log(`Appel de l'url ${url}, avec les paramètres: size : ${pageSize}, page: ${page}, sort : ${sort}`);

    const requestParams: SerializableRechercheMessage = emailCriteriaDTO;

    // TODO revoir la sérialisation des dates ...
    if (emailCriteriaDTO.dateEnvoiDebut != null) {
      requestParams.dateEnvoiDebut = moment(emailCriteriaDTO.dateEnvoiDebut).format(DATE_TIME_ISO_FORMAT);
    }
    if (emailCriteriaDTO.dateEnvoiFin != null) {
      requestParams.dateEnvoiFin = moment(emailCriteriaDTO.dateEnvoiFin).format(DATE_TIME_ISO_FORMAT);
    }
    requestParams.inclureReponses = false;
    // fixme: workaround RSEM & EXEC
    requestParams.exclureReponses = true;
    return this.http.post<PaginationPage<EmailDTO>>(url, requestParams, {params: httpParams});
  }

  getPieceJointeDownloadURL(message: MessageDTO, pieceJointe: PieceJointeDTO): string {
    if (pieceJointe.uuid != null) {
      return `${MS_WEBSERVICE_ENDPOINT}/pieces-jointes/${this._token}/${message.uuid}/${pieceJointe.uuid}`;
    } else if (pieceJointe.reference != null) {
      return `${MS_WEBSERVICE_ENDPOINT}/pieces-jointes/${this._token}/${message.uuid}/reference/${pieceJointe.reference}`;
    }
    return null;
  }

  findBrouillons(page: number, pageSize: number,
                 sort: PaginationPropertySort): Observable<PaginationPage<MessageDTO>> {
    const httpParams = {size: '' + pageSize, page: '' + page, sort: ''};
    if (sort) {
      httpParams.sort = `${sort.property},${sort.direction}`;
    }
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/brouillons/?token=${this._token}`;
    console.log(`Appel de l'url ${url}, avec les paramètres: size : ${pageSize}, page: ${page}, sort : ${sort}`);

    return this.http.post<PaginationPage<MessageDTO>>(url, null, {params: httpParams});
  }

  findEmailUpdated(listeCodeLien: Array<string>): Observable<EmailDTO[]> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/findEmailUpdated?token=${this._token}`;
    return this.http.post<EmailDTO[]>(url, listeCodeLien);
  }

  acquitterReponse(codeLien: string) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/acquitterReponse`;
    const httpParams = {token: this._token, codeLien};
    return this.http.get<EmailDTO>(url, {params: httpParams});
  }

  acquitter(codeLien: string) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/acquitter`;
    const httpParams = {token: this._token, codeLien};
    return this.http.get<EmailDTO>(url, {params: httpParams});
  }

  deleteEmail(codeLien: string): Observable<any> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/deleteEmail?token=${this._token}&codeLien=${codeLien}`;
    return this.http.delete(url);
  }

  deleteMessage(id: number): Observable<any> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/deleteMessage?token=${this._token}&id=${id}`;
    return this.http.delete(url);
  }

  relaunchEmail(codeLien: string): Observable<any> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/relancerMessage`;
    const httpParams = {token: this._token, codeLien};
    return this.http.get(url, {params: httpParams});
  }

  initialiseMessage(): Observable<MessageDTO> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/initialiseMessage`;
    const httpParams = {token: this._token};
    return this.http.get<MessageDTO>(url, {params: httpParams});

  }


  deleteUploadedPieceJointe(message: MessageDTO, pieceJointe: PieceJointeDTO): Observable<any> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/pieces-jointes/${this._token}/${message.uuid}/${pieceJointe.reference}`;
    return this.http.delete(url);
  }

  saveMessage(message: MessageDTO, brouillon: boolean): Observable<MessageDTO> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/saveMessage?token=${this._token}&brouillon=${brouillon}`;
    return this.http.post(url, message);
  }

  getDownloadpreuvesURL(uuidEmail: string) {
    const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    return `${MS_WEBSERVICE_ENDPOINT}/preuves/${uuidEmail}?token=${this._token}&timezone=${timeZone}`;
  }

  initialiseMessageEntreprise() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/initialiseMessageEntreprise`;
    const httpParams = {token: this._token};
    return this.http.get<EmailDTO>(url, {params: httpParams});
  }

  repondre(email: EmailDTO, reponse: MessageDTO) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/answerMessage?token=${this._token}&codeLien=${email.codeLien}`;
    return this.http.post<boolean>(url, reponse);
  }

  reponseAgent(email: EmailDTO, reponse: MessageDTO) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/redaction/agent/answerMessage?token=${this._token}&codeLien=${email.codeLien}`;
    return this.http.post<boolean>(url, reponse);
  }

  getDestinataires(term: string) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/destinataires?token=${this._token}`;
    return this.http.post<any>(url, term);
  }

  getHistorique(email: EmailDTO) {
    return this.http.get<EvenementDTO[]>(`${MS_WEBSERVICE_ENDPOINT}/suivi/historique?token=${this._token}&codeLien=${email.codeLien}`);
  }

  toggleFavori(email: EmailDTO) {
    return this.http.post<boolean>(`${MS_WEBSERVICE_ENDPOINT}/suivi/favoris/toggle?token=${this._token}&codeLien=${email.codeLien}`, {});
  }

  downloadFile(url: string) {
    return this.http.get(url, {responseType: "blob"})
  }

  findEmailsEntreprise(page: number, pageSize: number) {
    const pageable = {size: pageSize, page: page, sort: 'dateEnvoi'};
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/messages/entreprise/?token=${this._token}`;
    console.log(`Appel de l'url ${url}, avec les paramètres: size : ${pageSize}, page: ${page}`);
    return this.http.post<PaginationPage<EmailDTO>>(url, pageable);
  }

  export(exportDto: ExportDTO) {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/export/?token=${this._token}`;
    return this.http.post(url, exportDto, {responseType: "blob"});
  }

  getFiltres() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/suivi/filtres`;
    const httpParams = {token: this._token};
    return this.http.get<SerializableRechercheMessage>(url, {params: httpParams});
  }

  getConfiguration() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/configuration/`;
    const httpParams = {token: this._token};
    return this.http.get<ConfigurationDTO>(url, {params: httpParams});
  }

  deleteToken() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/tokens/${this._token}`;
    return this.http.delete<boolean>(url);
  }

  downloadFileFromUrl(url: string) {
    return this.http.get(url, {responseType: 'blob'})

  }

  saveAs(r: any, filename: string) {
    const blob = new Blob([r]);
    saveAs(blob, filename);
  }

  updateStatutEmail(codeLien: string) {
    return this.http.patch<EmailDTO>(`${MS_WEBSERVICE_ENDPOINT}/emails/update-status?token=${this._token}&codeLien=${codeLien}`, {});
  }

  getDestinatairesMessage() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/destinataires/`;
    const httpParams = {token: this._token};
    return this.http.get<DestinataireDTO[]>(url, {params: httpParams});
  }

  getDestinatairesNotifiesMessage() {
    const url = `${MS_WEBSERVICE_ENDPOINT}/destinataires/notifies`;
    const httpParams = {token: this._token};
    return this.http.get<DestinataireDTO[]>(url, {params: httpParams});
  }
}
