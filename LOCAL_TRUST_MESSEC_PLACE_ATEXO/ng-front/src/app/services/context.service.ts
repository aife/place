import {Injectable} from '@angular/core';
import {ContexteDTO} from '../messagerie-dto';
import {MS_ACCESS_TOKEN, TEMPLATES_PATH} from "src/app/messagerie-constants";

@Injectable({
  providedIn: 'root'
})
export class ContextService {
  constructor() {

  }

  private _contexte: ContexteDTO;

  public get contexte() {
    return this._contexte;
  }

  public set contexte(value: any) {
    if (value) {
      console.debug(`injection du contexte depuis le web component => ${value}`)
      this._contexte = JSON.parse(value);
    } else {
      try {
        this._contexte = eval('ms_contexte');
      } catch (e) {
        console.debug('variable JS ms_contexte introuvable');
      }
    }
    localStorage.removeItem(MS_ACCESS_TOKEN);
    if (this._contexte?.access_token) {
      localStorage.setItem(MS_ACCESS_TOKEN, this._contexte?.access_token);
    }
  }

  get token() {
    return this._contexte.token;
  }

  get accessToken() {
    return this._contexte?.access_token;
  }

  get idTemplatePreselectionne() {
    return this._contexte.id_template_selectionne;
  }

  get templatesPath() {
    return this._contexte.templates_path ? this._contexte.templates_path : TEMPLATES_PATH;
  }

  get idEspaceDocModal() {
    return this._contexte.id_espace_doc_modal;
  }

  setToken(token: string) {
    this._contexte.token = token;
  }

  annulerRedaction() {
    this.redirect(this._contexte.url_suivi);
  }

  annulerSuivi() {
    this.redirect(this._contexte.url_retour_consultation);
  }

  nouveauMessage() {
    this.redirect(this._contexte.url_nouveau_message);
  }

  modifierMessage(codeLien: string) {
    if (this._contexte.url_modification_message) {
      this.redirect(this._contexte.url_modification_message + codeLien);
    }
  }

  visualiserMessage(codeLien: string) {
    this.redirect(this._contexte.url_visualisation_message + codeLien);
  }

  canCreateMessage() {
    return this._contexte != null && (this._contexte.peut_creer_message || this._contexte.url_nouveau_message != null);
  }

  canEditMessage() {
    return this._contexte != null && (this._contexte.peut_modifier_message || this._contexte.url_modification_message != null);
  }

  hasEspaceDocumentaire() {
    return this._contexte != null && this._contexte.id_espace_doc_modal != null;
  }

  isReponseLegacy() {
    return this._contexte != null && this._contexte.url_visualisation_message_legacy != null;
  }

  visualiserMessageLegacy(codeLien: string) {
    this.redirect(this._contexte.url_visualisation_message_legacy + codeLien);
  }

  private redirect(url: string) {
    if (url) {
      window.location.href = url;
    }
  }
}
