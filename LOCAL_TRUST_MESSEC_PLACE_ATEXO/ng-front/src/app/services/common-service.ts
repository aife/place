import {PaginationPropertySort} from '../messagerie-dto';
import {HttpParams} from '@angular/common/http';

export class HeadersBuilder {

  headers: any;

  constructor() {
    this.headers = {};
    this.setAuthorization();
  }

  setContentTypeFormUrlEncoded(): HeadersBuilder {
    this.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    return this;
  }

  setContentTypeJson(): HeadersBuilder {
    this.headers['Content-Type'] = 'application/json; charset=utf-8';
    return this;
  }

  setContentTypeTextPlain(): HeadersBuilder {
    this.headers['Content-Type'] = 'text/plain;charset=UTF-8';
    return this;
  }

  build(): { [key: string]: any } {
    return this.headers;
  }

  private setAuthorization(): HeadersBuilder {
    this.headers.Authorization = 'Bearer qsdqs';

    return this;
  }
}

export class PaginationRequestParamsBuilder {
  page = 0;
  pageSize = 10;
  sort: PaginationPropertySort;

  constructor() {

  }

  setPage(page: number): PaginationRequestParamsBuilder {
    this.page = page;
    return this;
  }

  setPageSize(pageSize: number): PaginationRequestParamsBuilder {
    this.pageSize = pageSize;
    return this;
  }

  setSort(sort: PaginationPropertySort): PaginationRequestParamsBuilder {
    this.sort = sort;
    return this;
  }

  build(params: HttpParams) {
    params.set('size', this.pageSize.toString());
    params.set('page', this.page.toString());
    if (this.sort) {
      params.set('sort', `${this.sort.property},${this.sort.direction}`);
    }
  }
}
