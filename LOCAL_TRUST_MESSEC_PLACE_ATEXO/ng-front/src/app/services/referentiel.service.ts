import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {MS_WEBSERVICE_ENDPOINT} from "../messagerie-constants";
import {CodeLabelDTO} from "../messagerie-dto";

@Injectable({
  providedIn: 'root'
})
export class ReferentielService {
  private referentiels: CodeLabelDTO[];

  constructor(private http: HttpClient) {
  }

  load(): Observable<CodeLabelDTO[]> {
    const url = `${MS_WEBSERVICE_ENDPOINT}/referentiels/surcharge-libelle`;
    console.log(url)
    const observable = this.http.get<CodeLabelDTO[]>(url);
    observable.subscribe(refs => this.referentiels = refs)
    return observable;
  }
  getReferentielsByCode(code: string): string {
    if (this.referentiels == null || this.referentiels.length === 0) {
      return null;
    }
    return this.referentiels.find(ref => ref.code === code)?.label;
  }

}

