import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {DestinataireDTO, EmailDTO} from '../messagerie-dto';
import {DomSanitizer} from '@angular/platform-browser';
import {ReferentielService} from "src/app/services/referentiel.service";

@Pipe({name: 'fileSize'})
@Injectable()
export class FileSizePipe implements PipeTransform {
  // FROM  http://scratch99.com/web-development/javascript/convert-bytes-to-mb-kb/
  transform(value: number, args: Array<any> = null): string {
    const units = [' Octets', ' Ko', ' Mo', ' Go', ' To'];

    if (isNaN(value)) {
      return '0';
    }
    let amountOf2s = Math.floor(Math.log(+value) / Math.log(2));
    if (amountOf2s < 1) {
      amountOf2s = 0;
    }
    const i = Math.floor(amountOf2s / 10);

    value = +value / Math.pow(2, 10 * i);

    // arrondi 2 chiffres après la virgule
    if (value.toString().length > value.toFixed(2).toString().length) {
      return value.toFixed(2) + units[i];
    }
    return value + units[i];
  }
}


@Pipe({name: 'newlineToBreakline'})
@Injectable()
export class NewlineToBreaklinePipe implements PipeTransform {
  transform(value: string, args: Array<any> = null): string {
    if (value != null) {
      return value.replace(/(\r\n|\n|\r)/gm, '<br/>');
    }
    return null;
  }
}

@Pipe({
  name: 'stripHtml'
})

export class StripHtmlPipe implements PipeTransform {
  transform(value: string): any {
    return value.replace(/<.*?>/g, ''); // replace tags
  }
}

@Pipe({
  name: 'destinataireLabel'
})
export class DestinataireLabelPipe implements PipeTransform {
  transform(item: DestinataireDTO): any {
    let label = '';
    if (item.nomContactDest) {
      label = label += item.nomContactDest + ' | ';
    }
    if (item.nomEntrepriseDest) {
      label = label += item.nomEntrepriseDest + ' | ';
    }
    if (item.mailContactDestinataire) {
      label = label += item.mailContactDestinataire;
    }
    return label;
  }
}

@Pipe({
  name: 'emailLabel'
})
export class EmailLabelPipe implements PipeTransform {
  transform(item: EmailDTO): any {
    let label = '';
    if (item.nomContact) {
      label = label += item.nomContact + ' | ';
    }
    if (item.email) {
      label = label += item.email;
    }
    return label;
  }
}

@Pipe({
  name: 'emaiEntrepriselLabel'
})
export class EmailEntrepriseLabelPipe implements PipeTransform {
  transform(item: EmailDTO): any {
    let label = '';
    if (item?.message?.nomContactExpediteur) {
      label = label += item?.message?.nomContactExpediteur + ' | ';
    }
    if (item?.message?.emailExpediteur) {
      label = label += item.message?.emailExpediteur;
    }
    return label;
  }
}

@Pipe({name: 'safeHtml'})
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {
  }

  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Pipe({name: 'surchargeLibelle'})
export class SurchargeLibellePipe implements PipeTransform {
  constructor(private referentielService: ReferentielService) {
  }

  transform(cle, valeurParDefaut) {
    const value = this.referentielService.getReferentielsByCode(cle);
    if (value) {
      return value;
    }
    return valeurParDefaut;
  }
}

export const APPLICATION_PIPES = [SurchargeLibellePipe, FileSizePipe, NewlineToBreaklinePipe, StripHtmlPipe, DestinataireLabelPipe, EmailLabelPipe, EmailEntrepriseLabelPipe, SafeHtmlPipe];

