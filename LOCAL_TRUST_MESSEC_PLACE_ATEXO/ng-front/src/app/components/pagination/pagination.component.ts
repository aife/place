import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PageChangeEvent, PaginationPage} from '../../messagerie-dto';
import {defaultItemsCountPerPage} from '../../messagerie-constants';

@Component({
  selector: 'messagerie-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() pagination: PaginationPage<any>;
  @Input() pageSize = defaultItemsCountPerPage;
  @Input() currentPage = 1;
  @Output() paginationChange = new EventEmitter<PageChangeEvent>();

  constructor() {
  }

  ngOnInit() {
  }

  onPaginationChange() {
    if (this.pageSize > 0 && this.currentPage > 0) {
      this.paginationChange.emit({currentPage: this.currentPage, pageSize: this.pageSize});
    }
  }

}
