import {Component, EventEmitter, Injector, Output, ViewEncapsulation} from '@angular/core';
import {MessagerieWebComponent} from "src/app/components/messagerie-web-component";

@Component({
  selector: 'atexo-messagerie-suivi',
  templateUrl: './messages-agent.component.html',
  styleUrls: ['./messages-agent.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class MessagesAgentComponent extends MessagerieWebComponent {

  nbBrouillons: number = 0;
  nbMessages: number = 0;
  @Output("onnouveaumessage") onNouveauMessage = new EventEmitter<any>();
  @Output("onmodifiermessage") onModifierMessage = new EventEmitter<String>();

  constructor(injector: Injector) {
    super(injector);
  }
}
