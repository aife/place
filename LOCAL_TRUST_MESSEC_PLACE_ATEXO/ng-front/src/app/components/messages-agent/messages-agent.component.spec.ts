import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MessagesAgentComponent} from './messages-agent.component';

describe('MessagesAgentComponent', () => {
  let component: MessagesAgentComponent;
  let fixture: ComponentFixture<MessagesAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MessagesAgentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
