import {AfterViewInit, Component, EventEmitter, isDevMode, OnDestroy, OnInit, Output} from '@angular/core';
import {
  MessageDTO,
  PageChangeEvent,
  PaginationPage,
  PaginationPropertySort,
  SerializableRechercheMessage,
  Template
} from "../../../messagerie-dto";
import {CRITERE_TEMPLATE, defaultItemsCountPerPage} from "../../../messagerie-constants";
import {MessagerieService} from "../../../services/messagerie.service";
import {ContextService} from "../../../services/context.service";
import {Observable} from "rxjs";
import {MpeService} from "../../../services/mpe.service";
import {Utils} from "../../shared/utils";
import {map} from "rxjs/operators";
import {ConfirmationModalComponent} from "../../modals/ConfirmationModal";
import {ErrorModalComponent} from "../../modals/ErrorModal";
import {SucessModalComponent} from "../../modals/SuccessModal";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {BsLocaleService} from "ngx-bootstrap/datepicker";

@Component({
  selector: 'brouillon-agent',
  templateUrl: './brouillon.component.html',
  styleUrls: ['./brouillon.component.css']
})
export class BrouillonComponent implements OnInit, OnDestroy, AfterViewInit {

  self: any;
  templates: Template[] = [];
  messages: PaginationPage<MessageDTO>;
  updateEmails: any;
  currentPage = 1;
  pageSize: number = defaultItemsCountPerPage;
  messageCriteriaDTO: SerializableRechercheMessage = {};
  sort: PaginationPropertySort;
  modalRef: BsModalRef;
  @Output() nbItemsChange = new EventEmitter<number>();
  @Output() onModifierMessage = new EventEmitter<String>();
  erreurServeur = 'Une erreur s\'est produite, Veuillez réessayer ultérieurement';

  constructor(private messagerieService: MessagerieService, private mpeService: MpeService,
              private contexteService: ContextService, private localeService: BsLocaleService,
              private modalService: BsModalService, private utils: Utils) {
    this.localeService.use('fr');
    this.self = this;
  }

  ngOnInit() {
    this.messages = {totalElements: 0, number: 0, size: defaultItemsCountPerPage};
    console.log("find page")
    this.fetchPage();
  }

  ngOnDestroy() {
    window.clearInterval(this.updateEmails);
  }

  ngAfterViewInit() {
  }

  onFilterChange(messageCriteriaDTO: SerializableRechercheMessage) {
    this.messageCriteriaDTO = messageCriteriaDTO;
    this.currentPage = 1;
    this.fetchPage();
  }

  findMessagesProxy(messageCriteriaDTO: SerializableRechercheMessage, page: number, pageSize: number,
                    sort: PaginationPropertySort): Observable<PaginationPage<MessageDTO>> {

    console.log("requête des brouillons")
    return this.messagerieService.findBrouillons(page, pageSize, sort).pipe(map(messagesPage => {
      // messagesPage.content = messagesPage.content.map(message => {
      //   return message;
      // });
      return messagesPage;
    })).pipe(map((messagePage: PaginationPage<MessageDTO>) => {

      messagePage.content.forEach(message => {
        this.extractTemplate(message);
        if (isDevMode()) {
          console.log(message);
        }
      });
      return messagePage;
    }));
  }

  extractTemplate(message: MessageDTO) {
    if (message != null && message.criteres != null) {
      const critereTemplate = message.criteres.find(critere => critere.nom === CRITERE_TEMPLATE);
      if (critereTemplate != null) {
        const foundTemplate = this.templates.find(t => t.id === parseInt(critereTemplate.valeur, 0));
        if (!foundTemplate) {
          console.log(`aucun template correspondant à l'id : ${critereTemplate.valeur} et l'email ; ${message.id}`);
        }
        message = foundTemplate;
      }
    }
  }

  fetchPage(): Observable<PaginationPage<MessageDTO>> {

    const observable: Observable<PaginationPage<MessageDTO>> = this.findMessagesProxy(this.messageCriteriaDTO,
      this.currentPage - 1, this.pageSize, this.sort);
    observable.subscribe(messages => {
      this.messages = messages;
      this.nbItemsChange.emit(messages.totalElements);
    }, e => {
      console.log(e);
    });
    return observable;
  }

  modifierMessage(messageDTO: MessageDTO) {
    this.onModifierMessage.emit(messageDTO.codeLien);
    this.contexteService.modifierMessage(messageDTO.codeLien);
  }

  canEditMessage() {
    return this.contexteService.canEditMessage();
  }

  supprimerEmail(message: MessageDTO) {
    this.openModal(ConfirmationModalComponent);
    this.modalRef.content.title = 'Demande de confirmation';
    this.modalRef.content.message = 'Confirmez-vous la suppression de ce courrier ?';
    this.modalRef.content.yes.subscribe(result => {
      const deleteEmail = this.messagerieService.deleteEmail(message.codeLien);
      deleteEmail.subscribe((e) => {
        this.openModal(SucessModalComponent);
        this.modalRef.content.title = 'Succès';
        this.modalRef.content.message = 'Le courrier a été supprimé.';
        this.fetchPage();
      }, (error) => {
        this.showError('Erreur', this.erreurServeur);
      });
    });
  }

  openModal(component, initialState?: any) {
    this.modalRef = this.modalService.show(component, {initialState, class: 'msv2'});
  }

  paginationChange(event: PageChangeEvent) {
    this.currentPage = event.currentPage;
    this.pageSize = event.pageSize;
    if (isDevMode()) {
      console.log(`chargement de la page ${event.currentPage}, nombre d'éléments par page : ${event.pageSize}`);
    }
    this.fetchPage();
  }

  getStatus() {
    const status = {label: 'NA', cssClass: 'NA'};
    status.label = 'Brouillon';
    status.cssClass = 'alert-color-draft';
    return status;
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }
}

