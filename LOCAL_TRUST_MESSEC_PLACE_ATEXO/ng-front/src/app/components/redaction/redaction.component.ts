import {Component, EventEmitter, Injector, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {CRITERE_TEMPLATE, LISTE_SAISIE_LIBRE, TEMPLATE_AVEC_AR, TEMPLATE_SANS_AR} from '../../messagerie-constants';
import {isBlank, isEmpty, isPresent} from '../shared/utils/validation';
import {
  DestinataireDTO,
  MessageDTO,
  PieceJointeDTO,
  Template,
  TypeDestinataire,
  TypeMessage
} from '../../messagerie-dto';
import {Observable} from 'rxjs';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {frLocale} from 'ngx-bootstrap/locale';
import {MpeService} from '../../services/mpe.service';
import {Utils} from '../shared/utils';
import {ConfirmationModalComponent} from '../modals/ConfirmationModal';
import {SucessModalComponent} from '../modals/SuccessModal';
import {ErrorModalComponent} from '../modals/ErrorModal';
import * as moment from 'moment';
import {NgSelectComponent} from '@ng-select/ng-select';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import 'quill';
import {MessagerieWebComponent} from 'src/app/components/messagerie-web-component';

defineLocale('fr', frLocale);

@Component({
  selector: 'atexo-messagerie-redaction',
  templateUrl: './redaction.component.html',
  styleUrls: ['./redaction.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})

export class RedactionComponent extends MessagerieWebComponent implements OnInit {
  message: MessageDTO;
  destinatairesSelectionnes: Array<DestinataireDTO> = [];
  allDestinataires: Array<DestinataireDTO> = [];
  selectedTemplate: Template;
  templates: Template[] = [];
  modalRef: BsModalRef;
  envoiModaliteFigee = false;
  reponseAttendue = false;
  dateLimiteAttendueFigee = false;
  agentBloqueFigee = false;
  reponseAttendueFigee = false;
  erreurEnvoi = 'Une erreur s\'est produite, votre message n\'a pas été envoyé, Veuillez réessayer ultérieurement';
  erreurServeur = 'Une erreur s\'est produite, Veuillez réessayer ultérieurement';
  errors = [];
  currentUploadedFilesSize = 0;
  submitted = false;
  brouillon = false;
  minDate = new Date();
  dateLimiteTime: Date;
  @Output("onpiecejointesupprimee") onPieceJointeRemoved = new EventEmitter<PieceJointeDTO>();
  @Output("onquitterredaction") onQuitterRedaction = new EventEmitter<MessageDTO>();
  @Output('onopenespacedocumentaire') onOpenEspaceDocumentaire: EventEmitter<void> = new EventEmitter();

  constructor(injector: Injector, private mpeService: MpeService,
              private localeService: BsLocaleService, private modalService: BsModalService,
              private utils: Utils) {
    super(injector);
    this.localeService.use('fr');
  }

  isEnvoiMessageModelValid(): boolean {
    this.fillErrors();
    let valid = this.errors.length === 0;

    if (!this.isValidContenu()) {
      this.showError('Erreur', `Le contenu de votre message ne peut pas faire plus de ${new Intl.NumberFormat('fr-FR', { currency: 'EUR' }).format(this.getMaxCaracteresContenu())} caractères. Merci de votre compréhension.`);
      return false;
    }

    if (!this.isValidFileSizes()) {
      this.showError('Erreur', `La modalité d'envoi séléctionnée ne permet pas l'envoi d'un message dont la somme des pièces jointes excède ${this.getMaxTailleFichier()}  Mo.`);
      return false;
    }

    if (!valid) {
      this.showError('Erreur', 'Merci de renseigner tous les champs obligatoires.');
    }
    return valid;
  }

  fillErrors() {
    this.errors = [];
    if (isBlank(this.message.contenu) || this.message.contenu.replace(/<.*?>/g, '').trim() === '') {
      this.errors.push('contenu');
    }

    if (isEmpty(this.message.objet)) {
      this.errors.push('objet');
    }
    if (isEmpty(this.destinatairesSelectionnes)) {
      this.errors.push('destinataires');
    }

    if (this.message.reponseAttendue && !this.message.dateLimiteReponseAttendue) {
      this.message.reponseBloque = false;
      this.message.dateLimiteReponseAsDate = null;
    } else if(!this.message.reponseAttendue || this.message.typeMessage == "TypeMessage1") {
      this.message.reponseAttendue = false;
      this.message.reponseBloque = false;
      this.message.dateLimiteReponseAsDate = null;
      this.message.dateLimiteReponseAttendue = false;
    }

    if (this.message.dateLimiteReponseAttendue) {
      if (this.message.dateLimiteReponseAsDate == null || this.dateLimiteTime == null) {
        this.errors.push('dateLimiteReponse')
      }
    }
  }

  hasErrors(field: string) {
    this.fillErrors();
    return this.submitted && this.errors.indexOf(field) >= 0;
  }

  setDateTime() {
    if (this.message.dateLimiteReponseAsDate == null) {
      this.message.dateLimiteReponseAsDate = moment(new Date()).hours(17).minutes(0).second(0).millisecond(0).toDate();
      this.dateLimiteTime = moment(new Date()).hours(17).minutes(0).second(0).millisecond(0).toDate();
    }
  }

  annulerMessage() {
    this.openModal(ConfirmationModalComponent);
    this.modalRef.content.title = 'Demande de confirmation';
    this.modalRef.content.message = 'Êtes-vous sûr de vouloir quitter sans enregistrer ?';
    this.modalRef.content.yes.subscribe(result => {
      this.onQuitterRedaction.emit(this.message);
      this.contextService.annulerRedaction();
    });
  }

  envoyer() {
    this.submitMessage(false).subscribe((message) => {
      this.openModal(SucessModalComponent);
      this.modalRef.content.title = 'Succès';
      this.modalRef.content.message = 'Le message a été envoyé';
      this.mpeService.acquitterReponse(this.contextService.contexte.url_callback_reponse_a_question);
      this.modalRef.content.yes.subscribe(() => {
        this.onQuitterRedaction.emit(message);
        console.log('OnQuitterRedaction.emit(message) OK', JSON.stringify(message));
        this.contextService.annulerRedaction();
      });
    }, (error) => {
      this.showError('Erreur', this.erreurEnvoi);
    });
  }

  enregistrer() {
    this.submitted = true;
    if (this.isEnvoiMessageModelValid()) {
      this.submitMessage(true).subscribe((m) => {
        this.onQuitterRedaction.emit(m);
        this.contextService.annulerRedaction();
      }, (error) => {
        this.showError('Erreur', this.erreurEnvoi);
      });
    }
  }
  confirmer() {
    console.log(this.message);
    this.submitted = true;
    if (this.isEnvoiMessageModelValid()) {
      this.openModal(ConfirmationModalComponent);
      this.modalRef.content.title = 'Demande de confirmation';
      this.modalRef.content.message = 'Confirmez-vous l\'envoi de ce message ?';
      this.modalRef.content.yes.subscribe(result => {
        this.envoyer();
      });
    }
  }

  onAddFreeText = (term) => {
    if (!this.utils.isValidEmail(term) || this.utils.contientEmail(this.message.destsPfDestinataire, term)) {
      return null;
    } else {
      const ret: DestinataireDTO = {mailContactDestinataire: term, type: LISTE_SAISIE_LIBRE};
      return ret;
    }
  }

  onChangeTemplate() {
    this.bindSelectedTemplate();
    if (this.selectedTemplate) {
      this.message.contenu = this.selectedTemplate.corps;
    }
  }

  bindSelectedTemplate() {
    if (this.selectedTemplate) {
      if (this.selectedTemplate.envoiModalite === 'AVEC_AR') {
        this.message.typeMessage = 'TypeMessage4';
      } else {
        this.message.typeMessage = 'TypeMessage1';
      }
      this.message.reponseAttendue = this.selectedTemplate.reponseAttendue;
      this.message.reponseAgentAttendue = this.selectedTemplate.reponseAgentAttendue;
      this.envoiModaliteFigee = this.selectedTemplate.envoiModaliteFigee;
      this.reponseAttendue = this.selectedTemplate.reponseAttendue;
      this.reponseAttendueFigee = this.selectedTemplate.reponseAttendueFigee;
      this.message.objet = this.selectedTemplate.objet;
      this.message.contenu = this.selectedTemplate.corps;
    }
  }

  openModal(component) {
    this.modalRef = this.modalService.show(component, {class: 'msv2'});
  }

  ngOnInit() {
    super.ngOnInit();
    // Recuperation du message à modifier
    const messageObservable: Observable<MessageDTO> = this.messagerieService.initialiseMessage();
    messageObservable.subscribe((message) => {
      this.message = message;
      this.brouillon = message.brouillon;
      this.message.templateFige = isPresent(this.contextService.idTemplatePreselectionne);
      // préremplissage des destinataires
      this.destinatairesSelectionnes = message.destinatairesPreSelectionnes ? message.destinatairesPreSelectionnes : [];
      // si pas de Type par défaut alors message Simple
      if (!message.typeMessage) {
        this.message.typeMessage = 'TypeMessage1';
      }
      this.message.contenuHTML = true;
      if (this.message.piecesJointes == null) {
        this.message.piecesJointes = [];
      } else {
        this.message.piecesJointes.forEach(pj => pj.progress = 100);
      }
      this.messagerieService.getDestinatairesMessage().subscribe(destinataires => {
        this.allDestinataires = destinataires;
        this.messagerieService.getDestinatairesNotifiesMessage().subscribe(destinatairesNotifies => {
          if (destinatairesNotifies != null && destinatairesNotifies.length > 0) {
            destinatairesNotifies.forEach(e => {
              if (!this.utils.contientEmail(this.allDestinataires, e.mailContactDestinataire)) {
                e.type = LISTE_SAISIE_LIBRE;
                this.allDestinataires = this.allDestinataires.concat(e);
              }
            });
          }
        })
      })
      if (message.dateLimiteReponseAsDate != null) {
        this.message.dateLimiteReponseAsDate = moment(message.dateLimiteReponseAsDate).toDate()
        this.dateLimiteTime = moment(message.dateLimiteReponseAsDate).toDate()
      }
      // récupération des templates
      this.mpeService.getTemplates().subscribe(t => {
        this.templates = t.mpe.reponse.mail_template;
        // Initialisation du template
        if (this.message.templateFige) {
          this.selectedTemplate = this.templates.find(l => '' + l.id === this.contextService.idTemplatePreselectionne);
        } else if (isPresent(this.message.criteres) && this.message.criteres.length > 0) {
          this.message.criteres.forEach(critere => {
            if (critere.nom === CRITERE_TEMPLATE) {
              this.selectedTemplate = this.templates.find(l => '' + l.id === critere.valeur);
            }
          });
        }
        if (!this.selectedTemplate && this.templates.length > 0) {
          this.selectedTemplate = this.templates[0];
        }
        if (message.id == null || !this.brouillon) {
          // le binding dynamique s'applique aux nouveaux message
          this.bindSelectedTemplate();
        }
      }, (er) => {
        this.showError('Erreur', this.erreurServeur);
      });
    }, (e) => {
      this.showError('Erreur', this.erreurServeur);
    });
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }

  getMaxTailleFichier(): number {
    return this.getMaxTailleFichierByType(this.message.typeMessage);
  }

  getMaxTailleFichierByType(type: string | TypeMessage): number {
    return this.message.limitationTailleFichier[type] / (1024 * 1024);
  }

  isValidFileSizes(): boolean {
    const maxTailleFichier = this.message.limitationTailleFichier[this.message.typeMessage];
    return this.currentUploadedFilesSize < maxTailleFichier;
  }

  uploadedFileSizeChange(event) {
    this.currentUploadedFilesSize = event;
  }

  onPaste(e, destinatairesSelect: NgSelectComponent) {
    e.stopPropagation();
    e.preventDefault();
    destinatairesSelect.close();
    let items = e.clipboardData.getData('Text').split(/[\s,;]+/);
    if (items && items.length > 0) {
      const destinatairesAjoutes = items
        .map(e => e.trim())
        .filter(e => e != '' && this.utils.isValidEmail(e) && !this.utils.contientEmail(this.allDestinataires, e))
        .map(e => {
          const dest: DestinataireDTO = {
            typeDestinataire: TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE,
            type: LISTE_SAISIE_LIBRE,
            mailContactDestinataire: e
          };
          return dest
        });
      this.allDestinataires = [].concat(this.allDestinataires, destinatairesAjoutes);
      const destinatairesColles = this.allDestinataires
        .filter(da => !this.utils.contientEmail(this.destinatairesSelectionnes, da.mailContactDestinataire))
        .filter(da => items.find(i => i === da.mailContactDestinataire) != null)
      ;
      this.destinatairesSelectionnes = [].concat(this.destinatairesSelectionnes, destinatairesColles);
      const notAdded = items.filter(e => !this.utils.isValidEmail(e)).map(e => e.trim()).join(", ");
      if (notAdded) {
        this.showError("Erreur", `Les adresses suivantes n’ont pas été ajoutées à la liste des destinataires (format incorrect) : ${notAdded}`)
      }
    }
  }

  private submitMessage(brouillon: boolean) {

    // Ajout des criteres (s'il n'existe pas dans la liste)
    this.message.criteres = [];
    if (this.selectedTemplate) {
      this.message.criteres.push({
        nom: CRITERE_TEMPLATE,
        valeur: '' + this.selectedTemplate.id,
        libelle: this.selectedTemplate.objet
      });
    }
    // si date response attendu est false (initialisé les valeurs de la date et bloquage)
    if (this.message.dateLimiteReponseAttendue === false) {
      this.message.dateLimiteReponseAsDate = null;
    } else {
      this.message.dateLimiteReponseAsDate.setHours(this.dateLimiteTime.getHours(), this.dateLimiteTime.getMinutes(), 0, 0);
    }
    // forcer le type de destinataire
    const typeDestinataire = TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
    this.destinatairesSelectionnes.forEach(d => d.typeDestinataire = typeDestinataire);
    this.message.destsPfDestinataire = this.destinatairesSelectionnes;
    this.message.destsPfEmettreur = [];
    // bind du template cote back
    if (this.message.typeMessage === 'TypeMessage4') {
      this.message.templateMail = TEMPLATE_AVEC_AR;
    } else {
      this.message.templateMail = TEMPLATE_SANS_AR;
      this.message.reponseAttendue = false;
      this.message.dossierVolumineux = null;
    }
    return this.messagerieService.saveMessage(this.message, brouillon);
  }

  private isValidContenu(): boolean {
    return this.message?.contenu?.length <= this.getMaxCaracteresContenu();
  }

  private getMaxCaracteresContenu(): number {
    return this.message?.maxCaracteresContenu;
  }

  searchByNomPrenomMail(term: string, item: DestinataireDTO) {
    term = term.toLocaleLowerCase();
    return item?.nomContactDest?.toLocaleLowerCase().indexOf(term) > -1
      || item?.nomEntrepriseDest?.toLocaleLowerCase().indexOf(term) > -1
      || item?.mailContactDestinataire?.toLocaleLowerCase().indexOf(term) > -1;
  }

}
