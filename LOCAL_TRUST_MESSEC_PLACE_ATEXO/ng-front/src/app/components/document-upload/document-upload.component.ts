import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DocumentDTO, MessageDTO, PieceJointeDTO} from '../../messagerie-dto';
import {MS_ACCESS_TOKEN, MS_WEBSERVICE_ENDPOINT, MS_WEBSERVICE_SECURE_ENDPOINT} from '../../messagerie-constants';
import {MessagerieService} from '../../services/messagerie.service';
import {DropzoneFile} from 'dropzone';
import {ContextService} from '../../services/context.service';
import {ErrorModalComponent} from '../modals/ErrorModal';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DropzoneConfigInterface, DropzoneDirective} from 'ngx-dropzone-wrapper';

@Component({
  selector: 'messagerie-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.scss']
})


export class DocumentUploadComponent implements OnInit {

  private _piecesJointes: Array<PieceJointeDTO>;
  @Output() filesupdated: EventEmitter<Array<PieceJointeDTO>>;
  currentUploadedFilesSize: number = 0;
  @Output() sizeChange: EventEmitter<number> = new EventEmitter();
  @Output() onPieceJointeRemoved: EventEmitter<PieceJointeDTO> = new EventEmitter();
  @Output() onOpenEspaceDocumentaire: EventEmitter<void> = new EventEmitter();

  @Input() set piecesJointes(_piecesJointes: Array<PieceJointeDTO>) {
    this._piecesJointes = _piecesJointes;
    if (_piecesJointes && _piecesJointes.length > 0) {
      this.currentUploadedFilesSize = _piecesJointes.map(pc => pc.taille).reduce((sum, current) => sum += current);
      this.sizeChange.emit(this.currentUploadedFilesSize);
    }
  }

  @Input() message: MessageDTO;

  showEspaceDocumentaire: boolean = false;
  isAntivirusActivated = false;

  get piecesJointes() {
    return this._piecesJointes;
  }

  @ViewChild(DropzoneDirective, {static: false}) dropzdropzoneDirective: DropzoneDirective;
  options: DropzoneConfigInterface;
  private modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private messagerieService: MessagerieService, private contextService: ContextService) {
    this.filesupdated = new EventEmitter();
    this.showEspaceDocumentaire = contextService.hasEspaceDocumentaire();
    this.messagerieService.getConfiguration().subscribe(value => {
      this.isAntivirusActivated = value.antivirusActif;
    })
  }

  ngOnInit() {
    if (this.showEspaceDocumentaire) {
      setInterval(() => {
        const selectionEvent = 'espace-documentaire-documents-selectionnes';
        const documents: DocumentDTO[] = JSON.parse(localStorage.getItem(selectionEvent));
        if (documents != null && documents.length > 0) {
          documents.forEach(doc => {
            console.log(`téléchargement du document : ${JSON.stringify(doc)}`);
            this.messagerieService.downloadFile(doc.url_telechargement).subscribe(blob => {
              const file = new File([blob], doc.nom);
              this.dropzdropzoneDirective.dropzone().addFile(file as DropzoneFile);
            })
          })
          localStorage.removeItem(selectionEvent);
        }
      }, 1000);
    }
    const self = this;
    this.generateDropZone();

  }

  generateDropZone() {
    const accessToken = localStorage.getItem(MS_ACCESS_TOKEN);
    let url = MS_WEBSERVICE_ENDPOINT;
    let headers = {};
    if (accessToken) {
      url = MS_WEBSERVICE_SECURE_ENDPOINT;
      headers = {"Authorization": `Bearer ${accessToken}`}
    }
    this.options = {
      url: `${url}/pieces-jointes/`,
      dictDefaultMessage: 'Déposer les fichiers ici pour les joindre',
      dictFallbackMessage: 'Votre navigateur ne supports pas le drag\'n\'drop.',
      dictFileTooBig: 'La taille totale des pièces jointes associées au message dépasse la taille totale autorisée : {{maxFilesize}} Mo',
      dictResponseError: 'Le serveur a répond  avec le statut : {{statusCode}}.',
      dictCancelUpload: 'Annuler',
      dictCancelUploadConfirmation: 'Voulez vous vraiment annuler cet upload?',
      dictRemoveFile: '<i class=\'fa fa-trash\'></i>',
      dictRemoveFileConfirmation: null,
      dictInvalidFileType: 'You can not upload any more files.',
      dictMaxFilesExceeded: 'You can not upload any more files.',
      previewTemplate: '<div style="display:none"></div>',
      addRemoveLinks: false,
      paramName: 'uploadfile',
      maxFilesize: 60,
      maxThumbnailFilesize: 5,
      timeout: -1,
      headers: headers,
      uploadMultiple: true
    }
  }

  deletePieceJointe(pieceJointe: PieceJointeDTO) {
    this.currentUploadedFilesSize -= pieceJointe.taille;
    const deleteFileCallBack = () => {
      this.piecesJointes.splice(this.piecesJointes.indexOf(pieceJointe), 1);
      this.filesupdated.next(this.piecesJointes);
      this.onPieceJointeRemoved.emit(pieceJointe);
    };

    // si la pièce jointe est correctement renseigné
    if (pieceJointe.file) {
      this.dropzdropzoneDirective.dropzone().removeFile(pieceJointe.file);
    } else {
      // si la pièce jointe est mal renseigné.
      let file = this.dropzdropzoneDirective.dropzone().getAcceptedFiles().find(af => af.name == pieceJointe.nom && af.size == pieceJointe.taille);
      if (file) {
        this.dropzdropzoneDirective.dropzone().removeFile(file);
      }
    }

    if (pieceJointe.reference != null) { // upload récent
      this.messagerieService.deleteUploadedPieceJointe(this.message, pieceJointe).subscribe(deleteFileCallBack);
    } else {
      deleteFileCallBack();
    }
    this.sizeChange.emit(this.currentUploadedFilesSize);
  }

  downlaodPieceJointe(pieceJointe: PieceJointeDTO) {
    this.messagerieService.downloadFileFromUrl(this.messagerieService.getPieceJointeDownloadURL(this.message, pieceJointe))
      .subscribe(r => {
        this.messagerieService.saveAs(r, pieceJointe.nom)
      }, e => {
        const error = new Blob([e.error]);
        error.text().then(value => {
          pieceJointe.erreur = value;
          this.showError('Erreur lors du téléchargement de la pièce jointe', value)
        })
      });
  }

  private showError(title: string, message: string) {
    this.modalRef = this.modalService.show(ErrorModalComponent, {class: 'msv2'});
    this.modalRef.content.message = message
    this.modalRef.content.title = title;
  }

  onError = (files: any[]) => {
    if (files.length > 1) {
      const first = files[0];
      const pj = this.piecesJointes.find(p => p.file === first);
      if (pj) {
        const response = first.xhr;
        // si erreur serveur
        if (response) {
          pj.erreur = response.status === 409 ? response.responseText : files[1];
        } else {
          pj.erreur = files[1];
        }

        pj.pending = false;
        pj.progress = 100;
        this.showError('Erreur lors de l\'upload de la pièce jointe', pj.erreur)
      }
    }
  }

  openEspaceDocumentaire() {
    const modale = document.getElementById(this.contextService.idEspaceDocModal);
    if (modale) {
      console.log('ouverture de la modale ' + this.contextService.idEspaceDocModal);
      modale.classList.add('show');
    } else {
      console.log('emission de l\'evenement onOpenEspaceDocumentaire');
      this.onOpenEspaceDocumentaire.emit();
    }
  }

  onSuccess(files: any[]) {
    if (files.length >= 2) {
      let uploadDesFiles: PieceJointeDTO[] = files[1];
      for (const pj of this._piecesJointes) {
        const reference = uploadDesFiles.find((u: PieceJointeDTO) => u.nom === pj.nom && u.taille === pj.taille)?.reference;
        if (reference) {
          pj.reference = reference;
          pj.pending = false;
          pj.progress = 100;
          this.filesupdated.next(this._piecesJointes);
        }
      }
    }
  }

  onAddedFile(file: DropzoneFile) {
    const pieceJointe: PieceJointeDTO = {nom: file.name, progress: 0, taille: file.size, file: file, pending: true};
    this._piecesJointes.push(pieceJointe);
    this.currentUploadedFilesSize += file.size;
    this.sizeChange.emit(this.currentUploadedFilesSize);

  }

  onUploadprogress(files: DropzoneFile[]) {
    files.filter(f => typeof f === 'object').forEach(file => {
      const uploadFile = this._piecesJointes.find(pj => pj.file === file);
      // à calculer
      uploadFile.progress = file?.upload?.progress;
    });

  }

  onQueuecomplete($event: any, dropzone: any) {
    this.dropzdropzoneDirective.reset(false);
  }
}
