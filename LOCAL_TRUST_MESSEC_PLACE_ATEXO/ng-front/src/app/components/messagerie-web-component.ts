import {ContextService} from 'src/app/services/context.service';
import {Component, Injector, Input, OnInit} from '@angular/core';
import {MessagerieService} from 'src/app/services/messagerie.service';

@Component({
  selector: 'atexo-messagerie-web-component',
  template: ''
})
export abstract class MessagerieWebComponent implements OnInit {
  @Input() contexte: any;
  erreurServeur = 'Une erreur s\'est produite, Veuillez réessayer ultérieurement';

  protected constructor(private injector: Injector) {
    this._messagerieService = injector.get(MessagerieService);
    this._contextService = injector.get(ContextService);
  }

  private _messagerieService: MessagerieService;

  public get messagerieService() {
    return this._messagerieService;
  }

  private _contextService: ContextService;

  public get contextService() {
    return this._contextService;
  }

  contexteFound() {
    return this.contextService.contexte;
  }

  ngOnInit(): void {
    this.contextService.contexte = this.contexte;
    this.messagerieService.token = this.contextService.token;
    const listCss = [
      '/messagerieSecurisee/front/assets/css/font-awesome.css',
      '/messagerieSecurisee/front/assets/css/bs-datepicker.css',
    ];
    for (const css of listCss) {
      console.log('Ajout de la feuille de style : ' + css + ' via proxypass');
      const font = document.createElement('link');
      font.href = css;
      font.rel = 'stylesheet';
      document.head.appendChild(font);
    }
  }

  nouveauMessage() {
    this.contextService.nouveauMessage();
  }

  canCreateMessage() {
    return this.contextService.canCreateMessage();
  }
}
