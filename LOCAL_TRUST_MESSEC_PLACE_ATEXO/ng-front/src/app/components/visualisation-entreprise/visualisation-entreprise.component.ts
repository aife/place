import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {EmailDTO, MessageDTO, TypeInitialisateur} from '../../messagerie-dto';
import {ErrorModalComponent} from '../modals/ErrorModal';
import {ConfirmationModalComponent} from '../modals/ConfirmationModal';
import {SucessModalComponent} from '../modals/SuccessModal';
import {QuillModules} from "ngx-quill";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import "quill";
import {MessagerieWebComponent} from "src/app/components/messagerie-web-component";

@Component({
  selector: 'atexo-messagerie-visualisation-entreprise',
  templateUrl: './visualisation-entreprise.component.html',
  styleUrls: ['./visualisation-entreprise.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class VisualisationEntrepriseComponent extends MessagerieWebComponent implements OnInit {
  erreurEnvoi = 'Une erreur s\'est produite, votre réponse n\'a pas été envoyée, Veuillez réessayer ultérieurement';
  email: EmailDTO;
  reponse: MessageDTO;
  modalRef: BsModalRef;
  submitted = false;
  reponseAgent = false;
  modules: QuillModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{'color': []}],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      ['clean']
    ]
  };

  constructor(injector: Injector, private modalService: BsModalService) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.loadVisualisation();
  }

  loadVisualisation() {
    this.messagerieService.initialiseMessageEntreprise().subscribe(m => {
      const nbReponse = m.toutesLesReponses.length
      const reponsesAgent = m.toutesLesReponses.filter(e => e.message.initialisateur === TypeInitialisateur[TypeInitialisateur.AGENT]);
      this.reponseAgent = nbReponse !== 0 && reponsesAgent.length > 0;
      this.reponse = {piecesJointes: [], initialisateur: TypeInitialisateur.ENTREPRISE}
      this.email = m;
      // acquittement du message au cas où il n'est pas lu
      if (!this.email.emailEntreprise) {
        const codesLiensAAquitter = [this.email.codeLien].concat(reponsesAgent.map(e => e.codeLien));
        for (const codeLien of codesLiensAAquitter) {
          this.messagerieService.acquitter(codeLien).subscribe(ack => {
            this.email.statutEmail = ack.statutEmail;
            this.email.dateARAsDate = ack.dateARAsDate;
            this.email.lu = ack.lu;
          });
        }
      } else {
        this.messagerieService.acquitterReponse(this.email.codeLien).subscribe(e => {
          console.log(`Acquittement des réponses OK pour le code lien ${this.email?.codeLien}`)
        });
      }
    });
  }

  hasErrors() {
    return this.submitted && (this.reponse.contenu == null || this.reponse.contenu.replace(/<.*?>/g, '').trim() === '');
  }

  answer() {
    this.submitted = true;
    if (this.hasErrors()) {
      this.showError('Erreur', 'Merci de renseigner tous les champs obligatoires.');
    } else {
      this.openModal(ConfirmationModalComponent);
      this.modalRef.content.message = 'Confirmez-vous l\'envoi de votre réponse ?';
      this.modalRef.content.title = 'Demande de confirmation';
      this.modalRef.content.yes.subscribe(result => {
        this.envoyer();
      });
    }
  }

  envoyer() {
    this.messagerieService.repondre(this.email, this.reponse).subscribe(rep => {
      this.openModal(SucessModalComponent);
      this.modalRef.content.title = 'Succès';
      this.modalRef.content.message = 'Votre réponse a été envoyée';
      this.modalRef.content.yes.subscribe(result => {
        location.reload();
      });
    }, (error) => {
      this.showError('Erreur', this.erreurEnvoi);
    });
  }

  openModal(component) {
    this.modalRef = this.modalService.show(component, {class: 'msv2'});
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }

}
