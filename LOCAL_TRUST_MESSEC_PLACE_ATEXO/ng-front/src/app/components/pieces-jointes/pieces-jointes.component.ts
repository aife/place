import {Component, Input, OnInit} from '@angular/core';
import {DossierVolumineuxDTO, MessageDTO, PieceJointeDTO} from '../../messagerie-dto';
import {Utils} from '../shared/utils';
import {MessagerieService} from '../../services/messagerie.service';
import {ErrorModalComponent} from "../modals/ErrorModal";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";

@Component({
  selector: 'messagerie-pieces-jointes',
  templateUrl: './pieces-jointes.component.html'
})
export class PiecesJointesComponent implements OnInit {
  @Input() piecesJointes: PieceJointeDTO[];
  @Input() message: MessageDTO;
  private modalRef: BsModalRef

  constructor(private modalService: BsModalService, private utils: Utils, private messagerieService: MessagerieService) {
  }

  ngOnInit() {
  }

  downlaodPieceJointe(pieceJointe: PieceJointeDTO) {
    this.messagerieService.downloadFileFromUrl(this.messagerieService.getPieceJointeDownloadURL(this.message, pieceJointe))
      .subscribe(r => {
        this.messagerieService.saveAs(r, pieceJointe.nom)
      }, e => {
        const error = new Blob([e.error]);
        error.text().then(value => {
          pieceJointe.erreur = value;
          this.showError('Erreur lors du téléchargement de la pièce jointe', value)
        })
      });

  }

  private showError(title: string, message: string) {
    this.modalRef = this.modalService.show(ErrorModalComponent, {class: 'msv2'});
    this.modalRef.content.message = message
    this.modalRef.content.title = title;
  }

  downloadDossierVolumineux(item: DossierVolumineuxDTO) {
    const showModalEnvol = window['showModalEnvol'];
    if (showModalEnvol) {
      showModalEnvol(item.uuidReference);
    } else {
      console.log('la fonction showModalEnvol est introuvable');
    }
  }
}
