import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {CRITERE_TEMPLATE, TEMPLATE_AVEC_AR} from '../../messagerie-constants';
import {isBlank, isEmpty} from '../shared/utils/validation';
import {MessageDTO, Template, TypeDestinataire, TypeInitialisateur, TypeMessage} from '../../messagerie-dto';
import {Observable} from 'rxjs';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {frLocale} from 'ngx-bootstrap/locale';
import {MpeService} from '../../services/mpe.service';
import {Utils} from '../shared/utils';
import {ConfirmationModalComponent} from '../modals/ConfirmationModal';
import {SucessModalComponent} from '../modals/SuccessModal';
import {ErrorModalComponent} from '../modals/ErrorModal';
import {QuillModules} from "ngx-quill";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import "quill";
import {MessagerieWebComponent} from "src/app/components/messagerie-web-component";

defineLocale('fr', frLocale);

@Component({
  selector: 'atexo-messagerie-redaction-entreprise',
  templateUrl: './redaction-entreprise.component.html',
  styleUrls: ['./redaction-entreprise.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})

export class RedactionEntrepriseComponent extends MessagerieWebComponent implements OnInit {
  message: MessageDTO;
  modalRef: BsModalRef;
  erreurEnvoi = 'Une erreur s\'est produite, votre courrier n\'a pas été envoyé, Veuillez réessayer ultérieurement';
  erreurServeur = 'Une erreur s\'est produite, Veuillez réessayer ultérieurement';
  errors = [];
  templates: Template[] = [];
  currentUploadedFilesSize = 0;
  submitted = false;
  brouillon = false;
  modules: QuillModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{'color': []}],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      ['clean']
    ]
  };

  constructor(injector: Injector, private mpeService: MpeService,
              private localeService: BsLocaleService, private modalService: BsModalService,
              private utils: Utils) {
    super(injector);
    this.localeService.use('fr');

  }

  isEnvoiMessageModelValid(): boolean {
    this.fillErrors();
    let valid = this.errors.length === 0;

    if (!this.isValidFileSizes()) {
      this.showError('Erreur', `La modalité d'envoi séléctionnée ne permet pas l'envoi d'un courrier dont la somme des pièces jointes excède ${this.getMaxTailleFichier()}  Mo.`);
      return false;
    }

    if (!valid) {
      this.showError('Erreur', 'Merci de renseigner tous les champs obligatoires.');
    }
    return valid;
  }

  fillErrors() {
    this.errors = [];
    if (isBlank(this.message.contenu) || this.message.contenu.replace(/<.*?>/g, '').trim() === '') {
      this.errors.push('contenu');
    }
    if (isEmpty(this.message.objet)) {
      this.errors.push('objet');
    }
  }

  hasErrors(field: string) {
    this.fillErrors();
    return this.submitted && this.errors.indexOf(field) >= 0;
  }

  annulerMessage() {
    this.openModal(ConfirmationModalComponent);
    this.modalRef.content.title = 'Demande de confirmation';
    this.modalRef.content.message = 'Êtes-vous sûr de vouloir quitter sans enregistrer ?';
    this.modalRef.content.yes.subscribe(result => {
      this.contextService.annulerRedaction();
    });
  }

  envoyer() {
    this.submitMessage(false).subscribe(() => {
      this.openModal(SucessModalComponent);
      this.modalRef.content.title = 'Succès';
      this.modalRef.content.message = 'Le courrier a été envoyé';
      this.mpeService.acquitterReponse(this.contextService.contexte.url_callback_reponse_a_question);
      this.modalRef.content.yes.subscribe(result => {
        this.contextService.annulerRedaction();
      });
    }, (error) => {
      this.showError('Erreur', this.erreurEnvoi);
    });
  }

  confirmer() {
    console.log(this.message);
    this.submitted = true;
    if (this.isEnvoiMessageModelValid()) {
      this.openModal(ConfirmationModalComponent);
      this.modalRef.content.title = 'Demande de confirmation';
      this.modalRef.content.message = 'Confirmez-vous l\'envoi de ce courrier ?';
      this.modalRef.content.yes.subscribe(result => {
        this.envoyer();
      });
    }
  }

  openModal(component) {
    this.modalRef = this.modalService.show(component, {class: 'msv2'});
  }

  getMaxTailleFichier(): number {
    return this.getMaxTailleFichierByType(this.message.typeMessage);
  }

  getMaxTailleFichierByType(type: string | TypeMessage): number {
    return this.message.limitationTailleFichier[type] / (1024 * 1024);
  }

  isValidFileSizes(): boolean {
    const maxTailleFichier = this.message.limitationTailleFichier[this.message.typeMessage];
    return this.currentUploadedFilesSize < maxTailleFichier;
  }

  uploadedFileSizeChange(event) {
    this.currentUploadedFilesSize = event;
  }

  ngOnInit() {
    super.ngOnInit();
    // Recuperation du message à modifier
    const messageObservable: Observable<MessageDTO> = this.messagerieService.initialiseMessage();
    messageObservable.subscribe((message) => {
      this.message = message;
      this.brouillon = message.brouillon;
      this.message.typeMessage = 'TypeMessage4';
      this.message.contenuHTML = true;
      if (this.message.piecesJointes == null) {
        this.message.piecesJointes = [];
      } else {
        this.message.piecesJointes.forEach(pj => pj.progress = 100);
      }
      this.messagerieService.getDestinatairesMessage().subscribe(destinataires => {
        this.message.destsPfEmettreur = destinataires;
      })
      this.mpeService.getTemplates().subscribe(t => {
        this.templates = t.mpe.reponse.mail_template;
      })
    }, (e) => {
      this.showError('Erreur', this.erreurServeur);
    });
  }

  private submitMessage(brouillon: boolean): Observable<any> {

    // Ajout des criteres (s'il n'existe pas dans la liste)
    this.message.criteres = [];
    // forcer le type de destinataire
    const typeDestinataire = TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE;
    if (!this.message.destsPfEmettreur) {
      this.message.destsPfEmettreur = [];
    } else {
      this.message.destsPfEmettreur.forEach(dest => dest.typeDestinataire = typeDestinataire);
    }
    this.message.typeMessage = 'TypeMessage4';
    this.message.templateMail = TEMPLATE_AVEC_AR;
    this.message.initialisateur = TypeInitialisateur.ENTREPRISE;
    this.message.reponseAttendue = true;
    this.message.criteres = [];
    const idTemplate = this.contextService.idTemplatePreselectionne;
    if (idTemplate) {
      const template = this.templates.find(t => t.id === parseInt(idTemplate));
      if (template) {
        this.message.criteres.push({
          nom: CRITERE_TEMPLATE,
          valeur: idTemplate,
          libelle: template.objet
        });
      }
    }
    return this.messagerieService.saveMessage(this.message, brouillon);
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }

}
