import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmailDTO, ObjetMetierDTO} from "src/app/messagerie-dto";

@Component({
  selector: '[app-ligne-email-agent]',
  templateUrl: './ligne-email-agent.component.html',
  styleUrls: ['./ligne-email-agent.component.scss']
})
export class LigneEmailAgentComponent implements OnInit {

  @Input() email: EmailDTO;
  @Input() visionTransverse = false;
  @Input() objetsMetier: ObjetMetierDTO[] = [];

  @Output() onShowDetails = new EventEmitter<EmailDTO>();
  @Output() onRelancer = new EventEmitter<EmailDTO>();
  @Output() onSupprimer = new EventEmitter<EmailDTO>();
  @Output() onToggleFavori = new EventEmitter<EmailDTO>();
  @Output() onChangerStatut = new EventEmitter<EmailDTO>();

  constructor() {
  }

  ngOnInit(): void {
  }

  getStatus(email: EmailDTO) {
    const status = {label: 'NA', cssClass: 'NA', code: 'NA'};
    if (email?.enAttenteEnvoi) {
      status.label = 'En cours d\'envoi';
      status.code = 'statut.en-cours-envoi.titre';
      status.cssClass = 'alert-color-draft';
    } else if (email?.echecEnvoi) {
      status.label = 'Non délivré';
      status.code = 'statut.non-delivre.titre';
      status.cssClass = 'alert-color-danger question-mark-picto';
    } else if (email?.traite) {
      status.label = 'Traité dans un autre envoi';
      status.code = 'statut.traite.titre';
      status.cssClass = 'alert-color-danger question-mark-picto';
    } else if (email?.delivre) {
      status.label = 'Délivré';
      status.code = 'statut.delivre.titre';
      status.cssClass = 'alert-color-send';
      if (email?.lu) {
        status.label = 'Lu par le destinataire';
        status.code = 'statut.lu-destinataire.titre';
        status.cssClass = 'alert-color-readed';
      }
      if (email?.reponse != null) {
        if (email.reponse.dateARAsDate == null) {
          status.label = 'Réponse non lue';
          status.code = 'statut.reponse-non-lue.titre';
          status.cssClass = 'alert-color-success question-mark-picto';
        } else {
          status.label = 'Réponse lue';
          status.code = 'statut.reponse-lue.titre';
          status.cssClass = 'alert-color-success';
        }
      }
    }
    return status;
  }

  getStatusEntreprise(email: EmailDTO) {
    const status = {label: 'NA', cssClass: 'NA', code: 'NA'};
    if (email?.delivre) {
      status.label = 'Délivré';
      status.code = 'statut.delivre.titre';
      status.cssClass = 'alert-color-send';
      if (email?.lu) {
        status.label = 'Message lu';
        status.code = 'statut.message-lu.titre';
        status.cssClass = 'alert-color-send';
      } else {
        status.label = 'Message non lu';
        status.code = 'statut.message-non-lu.titre';
        status.cssClass = 'alert-color-send question-mark-picto';
      }
      if (email?.reponse != null) {
        if (email.reponse.dateARAsDate == null) {
          status.label = 'Réponse non lue par le destinataire';
          status.code = 'statut.reponse-non-lue-destinataire.titre';
          status.cssClass = 'alert-color-success question-mark-picto';
        } else {
          status.label = 'Réponse lue par le destinataire';
          status.code = 'statut.reponse-lue-destinataire.titre';
          status.cssClass = 'alert-color-success';
        }
      }
    }
    return status;
  }

  getObjetMetier(email: EmailDTO) {
    const id = email?.message?.identifiantObjetMetierPlateFormeEmetteur;
    const objetMetier = this.objetsMetier?.find(om => om.identifiant === id);
    return objetMetier;

  }

  showDetails() {
    this.onShowDetails.emit(this.email);
  }

  relancer() {
    this.onRelancer.emit(this.email);
  }

  supprimerEmail() {
    this.onSupprimer.emit(this.email);
  }

  toggleFavori() {
    this.onToggleFavori.emit(this.email);
  }

  changerStatutTraite() {
    this.onChangerStatut.emit(this.email);
  }
}
