import {Component, EventEmitter, isDevMode, OnDestroy, OnInit, Output} from '@angular/core';
import {isPresent} from '../shared/utils/validation';
import {
  CritereDTO,
  EmailDTO,
  EvenementDTO,
  FiltreDTO,
  ObjetMetierDTO,
  PageChangeEvent,
  PaginationPage,
  PaginationPropertySort,
  SerializableRechercheMessage,
  Template,
  TypeStatutEmail
} from '../../messagerie-dto';
import {CRITERE_TEMPLATE, defaultItemsCountPerPage} from '../../messagerie-constants';
import {MessagerieService} from '../../services/messagerie.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {frLocale} from 'ngx-bootstrap/locale';
import {MpeService} from '../../services/mpe.service';
import {ContextService} from '../../services/context.service';
import {ConfirmationModalComponent} from '../modals/ConfirmationModal';
import {SucessModalComponent} from '../modals/SuccessModal';
import {ErrorModalComponent} from '../modals/ErrorModal';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {saveAs} from 'file-saver';
import * as moment from "moment";

defineLocale('fr', frLocale);


@Component({
  selector: 'boite-reception',
  templateUrl: './messages.component.html',
  styleUrls: ['messages.component.scss']
})
export class MessagesComponent implements OnInit, OnDestroy {

  self: any;
  templates: Template[] = [];
  objetsMetier: ObjetMetierDTO[] = [];
  emails: PaginationPage<EmailDTO>;
  updateEmails: any;
  currentPage = 1;
  pageSize: number = defaultItemsCountPerPage;
  @Output() nbItemsChange = new EventEmitter<number>();
  @Output() onNouveauMessage = new EventEmitter<any>();
  emailCriteriaDTO: SerializableRechercheMessage = {};
  sort: PaginationPropertySort;
  modalRef: BsModalRef;
  erreurServeur = 'Une erreur s\'est produite, Veuillez réessayer ultérieurement';
  exportEnCours = false;
  visionTransverse = false;
  filtresAppliques: FiltreDTO;
  afficherFiltreEntreprise = false;
  isModalShown = false;
  historique: EvenementDTO[] = [];
  selectedEmail: EmailDTO;


  constructor(private messagerieService: MessagerieService, private mpeService: MpeService,
              private contexteService: ContextService, private localeService: BsLocaleService,
              private modalService: BsModalService) {
    this.localeService.use('fr');
    this.self = this;
  }

  ngOnInit() {

    this.emails = {totalElements: 0, number: 0, size: defaultItemsCountPerPage};
    this.mpeService.getTemplates().subscribe(t => {
      this.templates = t.mpe.reponse.mail_template;
      this.fetchPage();
    });
    this.messagerieService.getFiltres().subscribe(filtres => {
      this.objetsMetier = filtres.objetsMetier;
      this.emailCriteriaDTO = filtres;
      this.visionTransverse = filtres?.objetsMetier?.length > 0;
      this.filtresAppliques = filtres.filtresAppliques;
      this.afficherFiltreEntreprise = filtres.afficherFiltreEntreprise;
    })
    this.messagerieService.getConfiguration().subscribe(config => {
      this.updateEmails = setInterval(() => this.updateEmailStatus(), config.delaiPooling);
    })
  }

  ngOnDestroy() {
    window.clearInterval(this.updateEmails);
  }

  onFilterChange(emailCriteriaDTO: SerializableRechercheMessage) {
    this.emailCriteriaDTO = emailCriteriaDTO;
    this.currentPage = 1;
    this.fetchPage();
  }

  findEmailsProxy(emailCriteriaDTO: SerializableRechercheMessage, page: number, pageSize: number,
                  sort: PaginationPropertySort): Observable<PaginationPage<EmailDTO>> {

    return this.messagerieService.findEmails(emailCriteriaDTO, page, pageSize, sort).pipe(map(emailsPage => {
      emailsPage.content = emailsPage.content.map(email => {
        return email;
      });
      return emailsPage;
    })).pipe(map((emailsPage: PaginationPage<EmailDTO>) => {

      emailsPage.content.forEach(email => {
        this.extractTemplate(email);
        if (isDevMode()) {
          console.log(email.template);
        }
      });
      return emailsPage;
    }));
  }

  extractTemplate(email: EmailDTO) {
    if (email.message != null && email.message.criteres != null) {
      const critereTemplate = email.message.criteres.find(critere => critere.nom === CRITERE_TEMPLATE);
      if (critereTemplate != null) {
        const foundTemplate = this.templates.find(t => t.id === parseInt(critereTemplate.valeur, 0));
        if (!foundTemplate) {
          console.log(`aucun template correspondant à l'id : ${critereTemplate.valeur} et l'email ; ${email.id}`);
        }
        email.template = foundTemplate;
      }
    }
  }

  fetchPage(): Observable<PaginationPage<EmailDTO>> {

    const observable: Observable<PaginationPage<EmailDTO>> = this.findEmailsProxy(this.emailCriteriaDTO,
      this.currentPage - 1, this.pageSize, this.sort);
    observable.subscribe(emails => {
      this.emails = emails;
      this.nbItemsChange.emit(emails.totalElements);
    }, e => {
      this.showError("Erreur lors de la récupération des messages", this.erreurServeur)
    });
    return observable;
  }

  nouveauMessage() {
    this.onNouveauMessage.emit();
    this.contexteService.nouveauMessage();
  }

  canCreateMessage() {
    return this.contexteService.canCreateMessage();
  }

  supprimerEmail(email: EmailDTO) {
    this.openModal(ConfirmationModalComponent, '');
    this.modalRef.content.title = 'Demande de confirmation';
    this.modalRef.content.message = 'Confirmez-vous la suppression de ce courrier ?';
    this.modalRef.content.yes.subscribe(result => {
      const deleteEmail = this.messagerieService.deleteEmail(email.codeLien);
      deleteEmail.subscribe((e) => {
        this.openModal(SucessModalComponent, '');
        this.modalRef.content.title = 'Succès';
        this.modalRef.content.message = 'Le courrier a été supprimé.';
        this.fetchPage();
      }, (error) => {
        this.showError('Erreur', this.erreurServeur);
      });
    });
  }

  relancer(emailDTO: EmailDTO, index: number) {
    this.openModal(ConfirmationModalComponent, '');
    this.modalRef.content.title = 'Demande de confirmation';
    this.modalRef.content.message = 'Confirmez-vous la relance de l\'envoi du courrier ?';
    this.modalRef.content.yes.subscribe(result => {
      const relaunchEmail = this.messagerieService.relaunchEmail(emailDTO.codeLien);
      relaunchEmail.subscribe((email) => {
        this.openModal(SucessModalComponent, '');
        this.modalRef.content.title = 'Succès';
        this.modalRef.content.message = 'L\'envoi du courrier a été relancé.';
        this.extractTemplate(email);
        this.emails.content[index] = email;
      }, (error) => {
        this.showError('Erreur', this.erreurServeur);
      });
    });
  }

  openModal(component, additionalStyle?: string, initialState?: any) {
    this.modalRef = this.modalService.show(component, {initialState, class: `msv2 ${additionalStyle}`});
  }


  /**
   * Mettre a jour l'etat de la liste des emails affiché.
   * Lancer cette fonction periodiquement pour recuperer les mises à jours des emails
   */
  updateEmailStatus() {
    if (isPresent(this.emails) && isPresent(this.emails.content) && this.emails.content.length > 0) {
      const emailsToUpdate = this.emails.content.filter(email =>
        email.statutEmail === TypeStatutEmail[TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT] || email.statutEmail === TypeStatutEmail[TypeStatutEmail.EN_COURS_ENVOI] || email.statutEmail === TypeStatutEmail[TypeStatutEmail.EN_ATTENTE_ENVOI]
      );
      if (isPresent(emailsToUpdate) && emailsToUpdate.length > 0) {
        const listeCodeLien = emailsToUpdate.map((mail) => mail.codeLien);
        const emailsStatusUpdateObservable = this.messagerieService.findEmailUpdated(listeCodeLien);
        emailsStatusUpdateObservable.subscribe(email => {
          this.emails.content.forEach((e, index) => {
            this.updateEmail(e, email, index);
          });
        });

      }
    }
  }

  private updateEmail(e: EmailDTO, email: EmailDTO[], index: number) {
    if (isPresent(e) && isPresent(email[index]) && e.id === email[index].id && e.statutEmail !== email[index].statutEmail) {
      this.extractTemplate(email[index]);
      this.emails.content[index] = email[index];
    }
  }

  showDetails(email: EmailDTO) {
    this.messagerieService.getHistorique(email).subscribe(hist => {
      this.historique = hist;
      this.selectedEmail = email;
      this.isModalShown = true;
    }, (error => this.showError('Erreur', this.erreurServeur)));
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }

  paginationChange(event: PageChangeEvent) {
    this.currentPage = event.currentPage;
    this.pageSize = event.pageSize;
    if (isDevMode()) {
      console.log(`chargement de la page ${event.currentPage}, nombre d'éléments par page : ${event.pageSize}`);
    }
    this.fetchPage();
  }

  private updateEmailWithIndex(email: EmailDTO) {
    const index = this.emails.content.findIndex(e => e.id === email.id);
    if (index != -1) {
      this.extractTemplate(email);
      this.emails.content[index] = email;
    }
  }

  toggleFavori(email: EmailDTO) {
    if (isDevMode()) {
      console.log('Toggle favoris');
    }
    this.messagerieService.toggleFavori(email).subscribe(r => {
      email.favori = r;
      if (this.emailCriteriaDTO.favoris === true) {
        this.fetchPage();
      }
    });
  }


  exporter() {
    this.exportEnCours = true;
    const criteres: Array<CritereDTO> = this.templates.map(t => {
      const c: CritereDTO = {valeur: '' + t.id, libelle: t.objet};
      return c;
    })
    this.messagerieService.export({templates: criteres, critere: this.emailCriteriaDTO}).subscribe(r => {
      var blob = new Blob([r]);
      const date = new Date();
      const fichier = `Export messagerie - ${moment(date).format('yyyyMMDD HHmmss')}.xlsx`
      saveAs(blob, fichier);
      this.exportEnCours = false;
    }, error => {
      this.exportEnCours = false;
      console.log(error);
    })

  }

  getObjetMetier(email: EmailDTO) {
    const id = email?.message?.identifiantObjetMetierPlateFormeEmetteur;
    const objetMetier = this.objetsMetier?.find(om => om.identifiant === id);
    return objetMetier;

  }

  changerStatut($event: EmailDTO) {
    this.messagerieService.updateStatutEmail($event.codeLien).subscribe(value => {
      this.fetchPage();
    })
  }
}
