import {Component} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'messagerie-error-modal',
  templateUrl: './ErrorModal.html'
})
export class ErrorModalComponent {
  title: string;
  message: string;

  constructor(public modalRef: BsModalRef) {
  }

}
