import {Component, EventEmitter, Output} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'messagerie-success-modal',
  templateUrl: './SuccessModal.html'
})
export class SucessModalComponent {
  title: string;
  message: string;
  @Output() yes = new EventEmitter<string>();

  constructor(public modalRef: BsModalRef) {

  }

  close() {
    this.modalRef.hide();
    this.yes.emit('succès');
  }


}
