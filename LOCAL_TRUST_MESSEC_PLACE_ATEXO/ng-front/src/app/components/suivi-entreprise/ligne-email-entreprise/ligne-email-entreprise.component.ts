import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EmailDTO} from "src/app/messagerie-dto";

@Component({
  selector: '[app-ligne-email-entreprise]',
  templateUrl: './ligne-email-entreprise.component.html',
  styleUrls: ['./ligne-email-entreprise.component.scss']
})
export class LigneEmailEntrepriseComponent implements OnInit {

  @Input() email: EmailDTO;
  @Input() modeLegacy = false;

  @Output() onVisualiserMessage = new EventEmitter<EmailDTO>();
  @Output() onVisualiserMessageLegacy = new EventEmitter<EmailDTO>();

  constructor() {
  }

  ngOnInit(): void {
  }

  visualiserMessageLegacy() {
    this.onVisualiserMessageLegacy.emit(this.email);
  }

  visualiserMessage() {
    this.onVisualiserMessage.emit(this.email);
  }
}
