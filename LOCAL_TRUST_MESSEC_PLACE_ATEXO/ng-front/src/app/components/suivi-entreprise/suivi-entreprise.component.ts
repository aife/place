import {Component, Injector, OnInit, ViewEncapsulation} from '@angular/core';
import {EmailDTO, PageChangeEvent, PaginationPage} from '../../messagerie-dto';
import {defaultItemsCountPerPage} from '../../messagerie-constants';
import {Observable} from 'rxjs';
import {MessagerieWebComponent} from "src/app/components/messagerie-web-component";
import {ErrorModalComponent} from "src/app/components/modals/ErrorModal";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";

@Component({
  selector: 'atexo-messagerie-suivi-entreprise',
  templateUrl: './suivi-entreprise.component.html',
  styleUrls: ['./suivi-entreprise.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class SuiviEntrepriseComponent extends MessagerieWebComponent implements OnInit {

  emails: PaginationPage<EmailDTO>;
  currentPage = 1;
  pageSize: number = defaultItemsCountPerPage;
  modeLegacy = false;
  modalRef: BsModalRef;

  constructor(injector: Injector, private modalService: BsModalService) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.emails = {totalElements: 0, number: 0, size: defaultItemsCountPerPage};
    this.fetchPage();
    this.modeLegacy = this.contextService.isReponseLegacy() && this.contextService.contexte?.mode_legacy;
  }

  fetchPage(): Observable<PaginationPage<EmailDTO>> {

    const observable: Observable<PaginationPage<EmailDTO>> = this.messagerieService.findEmailsEntreprise(this.currentPage - 1, this.pageSize);
    observable.subscribe(emails => {
      this.emails = emails;
    }, e => {
      this.showError("Erreur lors de la récupération des messages", this.erreurServeur)
    });
    console.log(this.emails.size)
    return observable;
  }

  paginationChange(event: PageChangeEvent) {
    this.currentPage = event.currentPage;
    this.pageSize = event.pageSize;
    console.log(`Suivi entreprise : chargement de la page ${event.currentPage}, nombre d'éléments par page : ${event.pageSize}`);
    this.fetchPage();
  }

  visualiserMessage(email: EmailDTO) {
    this.contextService.visualiserMessage(email.codeLien);
  }

  visualiserMessageLegacy(email: EmailDTO) {
    this.contextService.visualiserMessageLegacy(email.codeLien);
  }

  openModal(component, additionalStyle?: string, initialState?: any) {
    this.modalRef = this.modalService.show(component, {initialState, class: `msv2 ${additionalStyle}`});
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.modalRef.content.message = message;
    this.modalRef.content.title = title;
  }
}
