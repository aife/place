export class EmailUtils {
  constructor() {
  }

  isReponse(email): boolean {
    return email.reponse != null;
  }
}
