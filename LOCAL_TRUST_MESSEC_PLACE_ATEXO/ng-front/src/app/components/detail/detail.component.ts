import {Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {EmailDTO, EvenementDTO, MessageDTO, TypeInitialisateur} from '../../messagerie-dto';
import {MessagerieService} from '../../services/messagerie.service';
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {QuillModules} from "ngx-quill";
import {ConfirmationModalComponent} from "src/app/components/modals/ConfirmationModal";
import {SucessModalComponent} from "src/app/components/modals/SuccessModal";
import {ErrorModalComponent} from "src/app/components/modals/ErrorModal";
import * as moment from "moment";
import {Clipboard} from '@angular/cdk/clipboard';
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'messagerie-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @Input() email: EmailDTO;
  @Output() onUpdate = new EventEmitter<EmailDTO>();
  @Output() onClose = new EventEmitter();
  first: EvenementDTO;
  @Input() historique: EvenementDTO[] = [];
  isCollapsed = true;
  erreurEnvoi = 'Une erreur s\'est produite, votre réponse n\'a pas été envoyée, Veuillez réessayer ultérieurement';
  modules: QuillModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{'color': []}],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      ['clean']
    ]
  };
  submitted = false;
  reponse: MessageDTO;
  reponseAgent: boolean = false;
  @ViewChild("listeEvenements", {static: false}) listeEvenements: ElementRef;

  constructor(@Inject(DOCUMENT) private document: any, private messagerieService: MessagerieService, public modalRef: BsModalRef, public dialogModal: BsModalRef, private modalService: BsModalService, private clipboard: Clipboard) {
  }

  telechargerPreuve() {
    if (this.email) {
      const filename = `echanges_${moment(new Date()).format('yyyy_MM_DD_HHmm')}.pdf`
      this.messagerieService.downloadFileFromUrl(this.messagerieService.getDownloadpreuvesURL(this.email.uuid))
        .subscribe(r => {
          this.messagerieService.saveAs(r, filename)
        }, e => {
          const error = new Blob([e.error]);
          error.text().then(value => {
            this.showError('Erreur lors du téléchargement de la pièce jointe', value)
          })
        });
    }
  }

  afficherMessage() {
    this.isCollapsed = !this.isCollapsed;
  }

  calculateHistory() {
    this.first = this.historique.find(e => e?.nom === 'initial');
    this.historique = this.historique.filter(e => e.nom !== 'initial');
  }

  ngOnInit(): void {
    this.reponse = {piecesJointes: []};
    this.calculateHistory();
    // acquittement en cas de réponse
    if (this.email && this.email?.toutesLesReponses?.length > 0 && !this.email?.emailEntreprise) {
      // on acquitte la réponse du message systématiquement après l'ouverture du mail de réponse
      const accuseReceptionObservable = this.messagerieService.acquitterReponse(this.email.codeLien);
      accuseReceptionObservable.subscribe((e) => {
        console.log(`Acquittement de toutes les réponses pour le mail ${this.email.codeLien} OK`);
        this.onUpdate.emit(e);
      });
    }
    if (this.email?.emailEntreprise) {
      this.messagerieService.acquitter(this.email?.codeLien).subscribe((e: EmailDTO) => {
        console.log(`Acquittement de l'email ${this.email.codeLien} OK`);
        this.onUpdate.emit(e);
      });
    }
    const nbReponse = this.email.toutesLesReponses.length
    this.reponseAgent = nbReponse !== 0 && this.email.toutesLesReponses.find(e => e.message.initialisateur === TypeInitialisateur[TypeInitialisateur.AGENT]) != null;

    setTimeout(() => this.scrollToLastEvent(this), 1000);
  }

  scrollToLastEvent(self) {
    const selector = 'discussion-thread-item';
    const elements = self.listeEvenements.nativeElement.getElementsByClassName(selector);
    if (elements && elements.length > 0) {
      const lastElement = elements.item(elements.length - 1);
      lastElement.scrollIntoView({behavior: 'smooth'});
    }
  }

  hasErrors() {
    return this.submitted && (this.reponse.contenu == null || this.reponse.contenu.replace(/<.*?>/g, '').trim() === '');
  }

  openModal(component) {
    this.dialogModal = this.modalService.show(component, {class: 'msv2'});
  }

  answer(initieParEntreprise: boolean) {
    this.submitted = true;
    if (this.hasErrors()) {
      this.showError('Erreur', 'Merci de renseigner tous les champs obligatoires.');
    } else {
      this.openModal(ConfirmationModalComponent);
      this.dialogModal.content.message = 'Confirmez-vous l\'envoi de votre réponse ?';
      this.dialogModal.content.title = 'Demande de confirmation';
      this.dialogModal.content.yes.subscribe(result => {
        this.reponse.initialisateur = initieParEntreprise ? TypeInitialisateur.ENTREPRISE : TypeInitialisateur.AGENT;
        this.envoyer();
      });
    }

  }

  envoyer() {
    this.messagerieService.reponseAgent(this.email, this.reponse).subscribe(rep => {
      this.openModal(SucessModalComponent);
      this.dialogModal.content.title = 'Succès';
      this.dialogModal.content.message = 'Votre réponse a été envoyée';
      this.dialogModal.content.yes.subscribe(result => {
        location.reload();
      });
    }, (error) => {
      this.showError('Erreur', this.erreurEnvoi);
    });
  }

  private showError(title: string, message: string) {
    this.openModal(ErrorModalComponent);
    this.dialogModal.content.message = message;
    this.dialogModal.content.title = title;
  }

  copyError(ev: EvenementDTO) {
    this.clipboard.copy(JSON.stringify(ev.erreurSMTP));
  }
}
