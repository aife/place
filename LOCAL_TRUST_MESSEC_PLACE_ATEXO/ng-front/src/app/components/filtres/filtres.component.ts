import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MessagerieService} from '../../services/messagerie.service';
import {
  DestinataireDTO,
  FiltreDTO,
  ObjetMetierDTO,
  SerializableRechercheMessage,
  Template,
  TypeStatutEmail
} from '../../messagerie-dto';
import {CRITERE_TEMPLATE, LISTE_SAISIE_LIBRE} from '../../messagerie-constants';
import {Utils} from '../shared/utils';
import {BsDatepickerConfig} from "ngx-bootstrap/datepicker";
import {concat, Observable, of, Subject} from "rxjs";
import {catchError, distinctUntilChanged, switchMap, tap} from "rxjs/operators";

@Component({
  selector: 'messagerie-filtres',
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss']
})
export class FiltresComponent implements OnInit {
  emailCriteriaDTO: SerializableRechercheMessage = {statutsAExclure: [TypeStatutEmail.BROUILLON]};
  @Input() templates: Template[] = [];
  @Input() objetsMetier: ObjetMetierDTO[] = [];
  @Input() afficherFiltreEntreprise = false;
  @Output() filterChange = new EventEmitter<SerializableRechercheMessage>();
  selectedTemplate: Template;
  selectedObjetMetier: ObjetMetierDTO;
  selectedDestinataires: DestinataireDTO[] = [];
  destinataires: DestinataireDTO[] = [];
  dateRange: Date[];
  _filtresAppliques: FiltreDTO;
  destinataires$: Observable<any>;
  destinatairesInput$ = new Subject<string>();
  loading = false;

  @Input() set filtresAppliques(value: FiltreDTO) {
    this._filtresAppliques = value;
    this.onCriteresChanges();
  }

  bsConfig: Partial<BsDatepickerConfig>;

  constructor(private messagerieService: MessagerieService, private utils: Utils) {
    this.bsConfig = {containerClass: 'theme-dark-blue', isAnimated: true};
  }

  ngOnInit() {
    this.loadDestinataires();
  }

  onCriteresChanges() {
    this.emailCriteriaDTO.criteres = {critere: []};
    this.emailCriteriaDTO.statuts = this.getStatuts();
    this.emailCriteriaDTO.sansReponse = this.emailCriteriaDTO?.statuts?.length > 0;
    this.emailCriteriaDTO.nomOuMailDestList = [];
    if (this.selectedTemplate) {
      this.emailCriteriaDTO.criteres.critere.push({
        nom: CRITERE_TEMPLATE,
        valeur: '' + this.selectedTemplate.id
      });
    }
    if (this.selectedObjetMetier) {
      this.emailCriteriaDTO.idObjetMetier = this.selectedObjetMetier.identifiant + "";
      this.emailCriteriaDTO.refObjetMetier = this.selectedObjetMetier.reference;
    } else {
      this.emailCriteriaDTO.idObjetMetier = null;
      this.emailCriteriaDTO.refObjetMetier = null;
    }

    if (this.selectedDestinataires && this.selectedDestinataires.length > 0) {
      const mails = this.selectedDestinataires.map(d => d.mailContactDestinataire);
      this.emailCriteriaDTO.nomOuMailDestList = mails;
    }

    this.emailCriteriaDTO.reponseAttendue = this._filtresAppliques?.reponseAttendue;
    this.emailCriteriaDTO.reponseLue = this._filtresAppliques?.reponseLue || this._filtresAppliques?.reponseLueEntreprise;
    this.emailCriteriaDTO.reponseNonLue = this._filtresAppliques?.reponseNonLue || this._filtresAppliques?.reponseNonLueEntreprise;

    if (this.dateRange && this.dateRange.length === 2) {
      this.emailCriteriaDTO.dateEnvoiDebut = this.dateRange[0];
      this.emailCriteriaDTO.dateEnvoiFin = this.dateRange[1];
    } else {
      this.emailCriteriaDTO.dateEnvoiDebut = null;
      this.emailCriteriaDTO.dateEnvoiFin = null;
    }
    this.filterChange.emit(this.emailCriteriaDTO);
  }

  getStatuts() {
    let retour: TypeStatutEmail [] = [];
    if (this._filtresAppliques?.statutEnCourEnvoi) {
      retour.push(TypeStatutEmail.EN_ATTENTE_ENVOI);
      retour.push(TypeStatutEmail.EN_COURS_ENVOI);
    }
    if (this._filtresAppliques?.statutNonDelivre) {
      retour.push(TypeStatutEmail.ECHEC_ENVOI);
    }
    if (this._filtresAppliques?.statutTraite) {
      retour.push(TypeStatutEmail.TRAITE);
    }
    if (this._filtresAppliques?.statutDelivre || this._filtresAppliques?.statutDelivreEntreprise) {
      retour.push(TypeStatutEmail.ENVOYE);
      retour.push(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT);
    }
    if (this._filtresAppliques?.statutAcquitte || this._filtresAppliques?.statutAcquitteEntreprise) {
      retour.push(TypeStatutEmail.ACQUITTE);
    }
    return retour;
  }

  onAddFreeText = (term) => {
    if (!this.utils.isValidEmail(term) || this.destinataires.find(d => term === d) != null) {
      return null;
    } else {
      const ret: DestinataireDTO = {mailContactDestinataire: term, type: LISTE_SAISIE_LIBRE};
      return ret;
    }
  }

  onStatutChange(event) {
    this._filtresAppliques.reponseAttendue = event;
    this.onCriteresChanges();
  }

  onKeyWordChange(event) {
    event.preventDefault();
    const keyWords = event.target.value;
    if (keyWords.length === 0 || keyWords.length > 2) {
      this.emailCriteriaDTO.motsCles = keyWords;
      this.onCriteresChanges();
    }
  }

  private loadDestinataires() {
    this.destinataires$ = concat(
      of([]), // default items
      this.destinatairesInput$.pipe(
        distinctUntilChanged(),
        tap(() => this.loading = true),
        switchMap(term => this.messagerieService.getDestinataires(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.loading = false)
        ))
      )
    );
  }
}
