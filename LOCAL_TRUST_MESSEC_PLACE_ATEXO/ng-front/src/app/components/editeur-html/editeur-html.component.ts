import {Component, Input, OnInit} from '@angular/core';
import {QuillModules} from "ngx-quill";
import {MessageDTO} from "src/app/messagerie-dto";

@Component({
  selector: 'messagerie-editeur-html',
  templateUrl: './editeur-html.component.html',
  styleUrls: ['./editeur-html.component.scss']
})
export class EditeurHtmlComponent implements OnInit {

  @Input() message: MessageDTO;
  @Input() hasErrors = false;
  @Input() placeHolder = 'Renseignez ici le corps de votre message';
  @Input() maxlength: number = 0;

  modules: QuillModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{'color': []}],
      [{'list': 'ordered'}, {'list': 'bullet'}],
      ['clean']
    ]
  };
  formats = ['bold', 'italic', 'underline', 'strike', 'color', 'list', 'ordered', 'bullet', 'clean'];

  constructor() {
  }

  ngOnInit(): void {
  }

  editorCreated(editor: any) {
    editor.clipboard.addMatcher(Node.ELEMENT_NODE, (node, delta) => {
      let ops = []
      delta.ops.forEach(op => {
        if (op.insert && typeof op.insert === 'string') {
          ops.push({
            insert: op.insert
          })
        }
      })
      delta.ops = ops
      return delta
    })

  }

  textChanged($event) {
    if (this.maxlength > 0 && $event.editor.getLength() > this.maxlength) {
      $event.editor.deleteText(this.maxlength, $event.editor.getLength());
    }
  }
}
