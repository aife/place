import mjml2html from 'mjml'
import fs from 'fs'
import path from 'path'

const templatesFolder = "./src/main/resources/mjml/";
const destinationFolder = 'target/generated-sources/templates/'
const templates = [];
fs.readdirSync(path.resolve(templatesFolder)).forEach(template => {
    templates.push(template);
});
if (templates.length !== 0) {
    templates.forEach(template => {
        console.log(`Processing template : ${template}`);
        const templatePath = path.resolve(templatesFolder, template);
        const mjmlContent = fs.readFileSync(templatePath, "utf-8")
        console.log(mjmlContent)
        const htmlOutput = mjml2html(mjmlContent, {mode: 'strict'}).html
        console.log(htmlOutput)
        const outputFtlName = template.replace('.mjml', '.ftl');

        fs.mkdirSync(path.resolve(destinationFolder), {recursive: true});
        const destinationPath = path.resolve(destinationFolder, outputFtlName);

        fs.writeFile(destinationPath, htmlOutput, (wr => {
            if (wr)
                console.log(wr)
        }));
    })
} else {
    console.log('No template found :(');
}
