package fr.atexo.messageriesecurisee.service.referentiel;


import fr.atexo.messageriesecurisee.dto.CodeLabelDTO;
import fr.atexo.messageriesecurisee.mapper.ReferentielMapper;
import fr.atexo.messageriesecurisee.repository.SurchargeLibelleRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReferentielServiceImpl implements ReferentielService {

    private final SurchargeLibelleRepository surchargeLibelleRepository;
    private final ReferentielMapper referentielMapper;

    public ReferentielServiceImpl(SurchargeLibelleRepository surchargeLibelleRepository, ReferentielMapper referentielMapper) {
        this.surchargeLibelleRepository = surchargeLibelleRepository;
        this.referentielMapper = referentielMapper;
    }

    @Override
    public List<CodeLabelDTO> getSurchargeLibellesList() {
        return surchargeLibelleRepository.findAll().stream().map(referentielMapper::toDto).collect(Collectors.toList());
    }
}
