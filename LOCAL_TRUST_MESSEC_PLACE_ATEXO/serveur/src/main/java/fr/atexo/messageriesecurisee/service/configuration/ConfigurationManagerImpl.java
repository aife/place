package fr.atexo.messageriesecurisee.service.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.net.InetAddress;

@Component
@Slf4j
public class ConfigurationManagerImpl implements ConfigurationManager {

    @Value("${download.path}")
    private String repertoireTelechargement;
    @Value("${max.size.one.file}")
    private BigInteger tailleMaxUnFichier;
    @Value("${max.size.all.file}")
    private BigInteger tailleMaxTousfichiers;

    @Value("${export.path:/data/messageriesecurisee/archivage/}")
    private String exportPath;

    /**
     * Pour l'envoi des mails
     */
    @Value("${smtp.adresse}")
    private String smtpServeur;
    @Value("${smtp.port}")
    private Integer smtpPort;

    @Value("${smtp.debug:false}")
    private boolean smtpDebug;

    @Value("${postfix.log.message.path}")
    private String cheminLogPostFix;

    @Value("${token.cache.size:1000000}")
    private int tokenCacheSize;

    @Value("${toke.cache.expiration.delay:1}")
    private int tokenExpirationDelay;

    @Value("${tmp.file.cache.expiration.delay:1}")
    private int tmpFileExpirationDelay;

    @Value("${nombre.essais.max:3}")
    private int nombreEssaisMax;

    @Value("${delai.pooling:15000}")
    private int delaiPooling;

    @Value("${temporary.path}")
    private String temporaryPath;

    /**
     * Retourne le chemin vers le repertoire de telechargement
     *
     * @return le chemin vers le repertoire de telechargement
     */
    @Override
    public String getRepertoireTelechargement() {
        return repertoireTelechargement;
    }

    /**
     * Defini le chemin vers le repertoire de telechargement
     *
     * @param repertoireTelechargement le chemin vers le repertoire de telechargement
     */
    public void setRepertoireTelechargement(String repertoireTelechargement) {
        this.repertoireTelechargement = repertoireTelechargement;
    }

    /**
     * Retourne la taille maximum autorisé pour un seul fichier
     *
     * @return la taille maximum autorisé pour un seul fichier
     */
    @Override
    public BigInteger getTailleMaxUnFichier() {
        return tailleMaxUnFichier;
    }

    /**
     * Defini la taille maximum autorisé pour un seul fichier
     *
     * @param tailleMaxUnFichier la taille maximum autorisé pour un seul fichier
     */
    public void setTailleMaxUnFichier(BigInteger tailleMaxUnFichier) {
        this.tailleMaxUnFichier = tailleMaxUnFichier;
    }

    /**
     * Retourne le maximum pour la somme des tailles des fichiers
     *
     * @return le maximum pour la somme des tailles des fichiers
     */
    @Override
    public BigInteger getTailleMaxTousfichiers() {
        return tailleMaxTousfichiers;
    }

    /**
     * Defini le maximum pour la somme des tailles des fichiers
     *
     * @param tailleMaxTousfichiers le maximum pour la somme des tailles des fichiers
     */
    public void setTailleMaxTousfichiers(BigInteger tailleMaxTousfichiers) {
        this.tailleMaxTousfichiers = tailleMaxTousfichiers;
    }

    /**
     * Retourne le chemin vers le fichier XML contenant le paramétrage de
     * rédaction des mails
     *
     * @return le chemin vers le fichier XML contenant le paramétrage de
     * rédaction des mails
     */
    @Override
    public String getCheminXMLGenerationMessage() {
        return "templates/templateMailHtml.ftl";
    }

    @Override
    public String getCheminXMLGenerationAlerte() {
        return "templates/templateMailAlerteAgent.ftl";
    }

    @Override
    public String getCheminXmlAccuseReceptionEntreprise() {
        return "templates/templateMPEAcquittementEntreprise.ftl";
    }

    @Override
    public String getCheminXmlAlerteMessageEntreprise() {
        return "templates/templateAlerteMessageEntreprise.ftl";
    }


    /**
     * Retourne l'adresse du serveur SMTP
     *
     * @return l'adresse du serveur SMTP
     */
    @Override
    public String getSmtpServeur() {
        return smtpServeur;
    }

    /**
     * Defini l'adresse du serveur SMTP
     *
     * @param smtpServeur l'adresse du serveur SMTP
     */
    public void setSmtpServeur(String smtpServeur) {
        this.smtpServeur = smtpServeur;
    }

    /**
     * Retourne le numero de port du serveur SMTP
     *
     * @return le numero de port du serveur SMTP
     */
    @Override
    public Integer getSmtpPort() {
        return smtpPort;
    }

    /**
     * Defini le numero de port du serveur SMTP
     *
     * @param smtpPort le numero de port du serveur SMTP
     */
    public void setSmtpPort(Integer smtpPort) {
        this.smtpPort = smtpPort;
    }

    @Override
    public boolean isSmtpDebug() {
        return smtpDebug;
    }

    /*
     *  Activer les logs SMTP
     * */
    public void setSmtpDebug(boolean smtpDebug) {
        this.smtpDebug = smtpDebug;
    }

    /**
     * Retourne le chemin vers le fichier de log postfix
     *
     * @return le chemin vers le fichier de log postfix
     */


    @Override
    public String getCheminLogPostFix() {
        return cheminLogPostFix;
    }

    /**
     * Defini le chemin vers le fichier de log postfix
     *
     * @param cheminLogPostFix le chemin vers le fichier de log postfix
     */
    public void setCheminLogPostFix(String cheminLogPostFix) {
        this.cheminLogPostFix = cheminLogPostFix;
    }

    /**
     * Retourne le chemin vers le fichier template de generation de la partie
     * texte du mail
     *
     * @return le chemin vers le fichier template de generation de la partie
     * texte du mail
     */
    @Override
    public String getCheminXmlGenerationMessageTexte() {
        return "templates/templateMailTexte.ftl";
    }

    @Override
    public String getExportPath() {
        return exportPath;
    }

    /**
     * @return délai d'expiration d'un jeton (en jour)
     */

    @Override
    public int getTokenExpirationDelay() {
        return tokenExpirationDelay;
    }

    @Override
    public int getNombreEssaisMax() {
        return nombreEssaisMax;
    }

    @Override
    public String getServeurEnvoi() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (Exception ex) {
            log.error("Impossible de récupérer le nom du host", ex);
            return getSmtpServeur();
        }
    }

    @Override
    public long getTmpFileExpirationDelay() {
        return tmpFileExpirationDelay;
    }

    @Override
    public String getCheminXmlAccuseReceptionEnvoiEntreprise() {
        return "templates/templateAcquittementEnvoiEntreprise.ftl";
    }

    @Override
    public String getCheminXMLGenerationAlerteTiers() {
        return "templates/templateAlerteReponseAgent.ftl";
    }

    /**
     * Retourne le délai en milisecondes pour récupérer le statut des emails coté front
     *
     * @return le délai en milisecondes pour récupérer le statut des emails coté front
     */
    @Override
    public int getDelaiPooling() {
        return delaiPooling;
    }

    /**
     * Retourne le chemin vers le repertoire temporaire
     *
     * @return le chemin vers le repertoire temporaire
     */
    @Override
    public String getTemporaryPath() {
        return temporaryPath;
    }


}
