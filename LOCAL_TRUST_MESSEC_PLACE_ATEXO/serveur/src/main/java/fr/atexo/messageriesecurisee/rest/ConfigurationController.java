package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.dto.ConfigurationDTO;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.validation.FileValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = {"/rest/configuration", "/rest/v2/configuration"})
@RequiredArgsConstructor
@Slf4j
public class ConfigurationController {


    private final FileValidationService fileValidationService;
    private final ConfigurationManager configurationManager;

    @GetMapping("/")
    public ResponseEntity<ConfigurationDTO> isAntivirusActif() {
        var configuration = ConfigurationDTO.builder()
                .antivirusActif(fileValidationService.isAntivirusActif())
                .delaiPooling(configurationManager.getDelaiPooling())
                .build();
        return new ResponseEntity<>(configuration, HttpStatus.OK);
    }

}
