package fr.atexo.messageriesecurisee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
@EnableWebMvc
@EnableTransactionManagement
@EnableJpaAuditing
@PropertySource(value = "classpath:config.properties")
public class MessagerieSecurisee extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MessagerieSecurisee.class, args);
	}
}
