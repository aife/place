package fr.atexo.messageriesecurisee.service.document;


import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;

import java.io.File;
import java.util.Collection;
import java.util.List;

public interface DocumentGenerator {

    List<CleValeurDTO> groupeByCode(Collection<CleValeurDTO> clesValeur, String... codes);

    File genererPDFPreuveEchanges(Integer idEmail, String timezone) throws MessageDAOException, ServiceException;

    File genererPDFPreuveEchanges(String uuidEmail, String timezone) throws MessageDAOException, ServiceException;
}
