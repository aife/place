package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

import static org.springframework.http.HttpStatus.OK;

@Controller
@RequestMapping(value = {"/rest/emails", "/rest/v2/emails"})
public class EmailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    EmailService emailService;

    @Autowired
    LoggingService loggingUtils;

    @Autowired
    TokenManager tokenManager;


    @PatchMapping(value = "/update-status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmailDTO> updateStatutEmail(@RequestParam(required = true) String token, @RequestParam(required = true) String codeLien) throws ServiceException, MessageDAOException {

        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("modification du statut de l'email code lien {}", codeLien);
        EmailDTO email = emailService.updateStatut(codeLien);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(email, OK);
    }


}
