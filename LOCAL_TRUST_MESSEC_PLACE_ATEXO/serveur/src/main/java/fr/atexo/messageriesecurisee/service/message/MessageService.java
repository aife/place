package fr.atexo.messageriesecurisee.service.message;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.*;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface MessageService {

    PieceJointeDTO searchPJByCodeLienAndCodePj(String codeLien, String codePJ) throws MessageDAOException;

    MessageDTO saveOrCreateMessage(Message msg, Email parent, boolean brouillon) throws MessageDAOException;

    EmailDTO searchEmailByCodeLien(String codeLien) throws MessageDAOException;

    Message getMessageByCodeLien(String codeLien) throws MessageDAOException;

    boolean acquitteEmail(String codeLien) throws MessageDAOException;

	Optional<EmailDTO> fetchOne( SerializableRechercheMessage critere) throws MessageDAOException;

    List<EmailDTO> fetch(SerializableRechercheMessage critere) throws MessageDAOException;

    Page<EmailDTO> search(SerializableRechercheMessage critereEmis, Pageable pageable) throws MessageDAOException;

    MessageDTO initialiseMessage(String token) throws MessageDAOException;

    void integreLesDestinataires(List<DestinataireDTO> destPfDestinataire, List<DestinataireDTO> destPfEmetteur, Message msg) throws ServiceException;

    void integreLesCriteres(HashMap<String, String> criteresLibresSup, Message msg);

    void persisteUploadedfiles(Message msg) throws IOException, PieceJointeIntrouvableException;

    void persisteUploadedfiles(Message msg, MessageDTO messageDTO) throws IOException, PieceJointeIntrouvableException;

    void transformeMessageInitialisationEnMessage(MessageSecuriseInit messageSecuriseInit, Message message, String encoding);

    void deleteMessage(String codeLien) throws MessageDAOException;

    void reintroductionDuFiltreDefiniParPlateforme(SerializableRechercheMessage champsRecherches,
                                                   SerializableRechercheMessage filtre);

    Page<MessageDTO> getBrouillons(SerializableRechercheMessage filtre, Pageable pageable);

    boolean repondre(String codeLien, MessageDTO messageDTO, String emailContact) throws IOException, PieceJointeIntrouvableException, MessageDAOException, ServiceException;

    EmailDTO initialiseMessageEntreprise(String token) throws MessageDAOException, ServiceException;

    MessageStatusDTO countMessageStatus(SerializableRechercheMessage critereRechercheMessage);

    void deleteMessageById(Integer id) throws MessageDAOException;

    PieceJointeDTO searchPJByCodeLienAndUuidPj(String codeLien, String pieceJointeId) throws MessageDAOException;

    PieceJointeDTO searchByMessageUuidAndPjUuid(String messageUuid, String uuid) throws MessageDAOException;

    MessageDTO getMessageByUuid(String messageUuid) throws MessageDAOException;

    List<DestinataireDTO> getDestinataires(String token);

    List<DestinataireDTO> getDestinatairesNotifies(String token);
}
