package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.service.export.ArchivageDTO;
import fr.atexo.messageriesecurisee.service.export.ExportService;
import fr.atexo.messageriesecurisee.service.message.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;

@Controller
@RequestMapping(value = "/archivage")
public class ExportController {

    @Autowired
    MessageService messageService;

    @Autowired
    ExportService exportService;

    @Autowired
    EmailService emailService;

    @PostMapping
    public ResponseEntity<String> export(@RequestBody @Valid ArchivageDTO archivageDTO) throws InterruptedException {
        var reponse = String.format(exportService.generatePackage(archivageDTO.getIdentifiantPlateforme()));
        return new ResponseEntity<>(reponse, OK);
    }
}
