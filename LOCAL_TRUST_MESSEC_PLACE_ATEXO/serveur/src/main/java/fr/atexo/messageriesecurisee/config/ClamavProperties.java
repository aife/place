package fr.atexo.messageriesecurisee.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "clamav")
@Getter
@Setter
public class ClamavProperties {

    private boolean enabled;
    private String host;
    private Integer port;
}
