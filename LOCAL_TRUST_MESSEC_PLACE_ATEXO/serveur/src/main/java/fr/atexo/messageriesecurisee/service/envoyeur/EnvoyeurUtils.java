package fr.atexo.messageriesecurisee.service.envoyeur;

import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.util.LienARUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EnvoyeurUtils {

    private static final Logger logger = LoggerFactory.getLogger(EnvoyeurUtils.class);

    private static final Pattern lignePattern = Pattern.compile("(?<date>\\d{4}-\\d{2}-\\d{2}\\s?\\d{2}:\\d{2}:\\d{2})(\\s+)(?<token>.+)(\\s+)(?<type>(>|\\*|=){2})(\\s+)(?<details>.*)");
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final String DELIVERY_SUPPRESSED = "*>";   // delivery suppressed by -N
    private static final String DELIVERY_FAILED = "**";    // delivery failed; address bounced
    private static final String DELIVERRY_DEFERED = "==";    //delivery deferred; temporary problem
    private static final List<String> KO_STATUS = Arrays.asList(DELIVERRY_DEFERED, DELIVERY_FAILED, DELIVERY_SUPPRESSED);
    private final String MESSAGE_DELIVERY = "=>";     //normal message delivery

    /**
     * <b>Provoquera l'acquittement du message.</b> Retourne un lien pour un
     * email et un message donne.
     *
     * @param message le message (contient les url des plateformes)
     * @param email   l'email (pour le code line)
     * @return un lien AR
     */
    public static String creerLienAR(Message message, Email email, boolean addPjToken) {
        return LienARUtils.creerLienAR(false, message.getUrlPfDestinataireVisualisation(), message.getUrlPfEmetteurVisualisation(), email.getTypeDestinataire(), email.getCodeLien(), addPjToken);
    }

    /**
     * Lit le fichier de log exim afin d'exraire touts les données relative à
     * l'état des envois, a comparé à une liste de de jeton à recherché passé en
     * paramètre
     *
     * @param smtpIds       la liste des identifiants SMTP pour lesquels effectuer la vérification
     * @param cheminLogExim le chemin du fichier de log postfix
     * @return la liste des jeton avec leur statut
     */
    public List<JetonPostFix> listeJetonLog(List<String> smtpIds, String cheminLogExim) {
        List<JetonPostFix> tokens = new ArrayList<>();
        File logfile = new File(cheminLogExim);
        if (logfile.exists()) {
            try (FileReader fr = new FileReader(logfile)) {
                BufferedReader reader = new BufferedReader(fr);
                String line = "";
                while ((line = reader.readLine()) != null) {
                    var eximToken = extractToken(smtpIds, line);
                    if (eximToken != null) {
                        tokens.add(eximToken);
                    }
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e.fillInStackTrace());
            }
        } else {
            logger.warn("fichier de log exim4 : {} inexistant ou illisible", cheminLogExim);
        }
        return tokens;
    }

    private JetonPostFix extractToken(List<String> smtpIds, String line) {
        Matcher matcher = lignePattern.matcher(line);
        if (matcher.find()) {
            String token = matcher.group("token");
            if (smtpIds.contains(token)) {
                String date = matcher.group("date");
                String details = matcher.group("details");
                String type = matcher.group("type");
                JetonPostFix jetonPostFix = new JetonPostFix();
                jetonPostFix.setJeton(token);
                jetonPostFix.setLigne(line);
                try {
                    jetonPostFix.setDate(LocalDateTime.parse(date, dateFormat));
                } catch (Exception pe) {
                    logger.error("Impossible de parse la date exim : {}", date);
                }

                jetonPostFix.setTypeSymbole(type);
                jetonPostFix.setDetails(details);
                if (MESSAGE_DELIVERY.equalsIgnoreCase(type)) {
                    jetonPostFix.setEtat(EtatPostFix.OK);
                } else if (DELIVERY_FAILED.equalsIgnoreCase(type)) {
                    jetonPostFix.setEtat(EtatPostFix.KO);
                } else {
                    jetonPostFix.setEtat(EtatPostFix.INDETERMINE);
                }
                return jetonPostFix;
            }
        }
        return null;
    }

}
