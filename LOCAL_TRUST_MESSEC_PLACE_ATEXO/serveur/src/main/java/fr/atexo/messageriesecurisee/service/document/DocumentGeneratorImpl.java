package fr.atexo.messageriesecurisee.service.document;


import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.EvenementDTO;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.util.StringFormatterUtil.format;
import static freemarker.template.Configuration.VERSION_2_3_30;
import static java.util.Arrays.asList;

/**
 * Gestion de la generation de document par le biais de document type
 *
 * @author ARD
 */
@Service
@Transactional
public class DocumentGeneratorImpl implements DocumentGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentGeneratorImpl.class);

    private static final String[] codePreuve = {"REFERENCE", "INTITULE", "OBJET", "DATE_HEURE_LIMITE_REMISE_PLIS", "ENTITE_PUBLIQUE", "ENTITE_ACHAT"};

    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private EmailMapper emailMapper;

    @Autowired
    private EmailService emailService;

    String imgPattern = "<img[^>]*>";

    private String generateResults(EmailDTO messageInitial, List<EvenementDTO> evenements, String timezone) {
        var context = new HashMap<String, Object>();
        Configuration cfg = new Configuration(VERSION_2_3_30);
        var message = messageInitial.getMessage();
        context.put("email", messageInitial);
        context.put("evenements", evenements);
        context.put("metaDonnees", groupeByCode(message.getMetaDonnees(), codePreuve));
        context.put("cartouche", Optional.ofNullable(message.getCartouche()).map(c -> c.replaceAll(imgPattern, "$0</img>")).orElse(""));
        context.put("cartoucheEscaped", Optional.ofNullable(message.getCartouche()).map(c -> c.replaceAll(imgPattern, "$0</img>").replace("\n", "<br>")).orElse(""));
        context.put("reponseMasquee", message.isReponseBloque() && message.getDateLimiteReponseAsDate() != null && message.getDateLimiteReponseAsDate().after(new Date()));
        context.put("dateLimiteReponse", message.getDateLimiteReponseAsDate());
        context.put("toutesLesReponses", messageInitial.getToutesLesReponses());
        cfg.setClassForTemplateLoading(DocumentGenerator.class, "/");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTimeZone(TimeZone.getTimeZone(timezone));
        try {
            Template template = cfg.getTemplate("templates/preuve_echanges.ftl");
            StringWriter stringWriter = new StringWriter();
            template.process(context, stringWriter);
            return stringWriter.toString();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
            return String.format("Erreur durant la génération du fichier : %s", e.getMessage());
        }
    }

    @Override
    public List<CleValeurDTO> groupeByCode(Collection<CleValeurDTO> clesValeur, String... codes) {
        if (clesValeur == null || clesValeur.isEmpty()) {
            return new ArrayList<>();
        }
        var listCodes = asList(codes).stream().map(String::toUpperCase).collect(Collectors.toList());
        return clesValeur.stream().filter(cv -> cv.getCode() != null && listCodes.contains(cv.getCode().toUpperCase()))
                .sorted(Comparator.comparing(cv -> listCodes.indexOf(cv.getCode().toUpperCase()))).collect(Collectors.toList());
    }


    @Override
    public File genererPDFPreuveEchanges(Integer idEmail, String timezone) throws MessageDAOException, ServiceException {
        try {
            var email = emailRepository.findById(idEmail).orElseThrow(() -> new MessageDAOException("email introuvable  id:" + idEmail));
            return getFile(email, timezone);
        } catch (IOException e) {
            LOGGER.error("Erreur lors de la génération du PDF");
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public File genererPDFPreuveEchanges(String uuidEmail, String timezone) throws MessageDAOException, ServiceException {
        try {
            var email = emailRepository.findByUuid(uuidEmail).orElseThrow(() -> new MessageDAOException("email introuvable  uuid:" + uuidEmail));
            return getFile(email, timezone);
        } catch (IOException e) {
            LOGGER.error("Erreur lors de la génération du PDF");
            throw new ServiceException(e.getMessage());
        }
    }

    private File getFile(Email email, String timezone) throws IOException {
        EmailDTO emailInitial = emailMapper.toDto(email);
        var evenements = emailService.getHistory(email.getCodeLien());
        File pdfFile = File.createTempFile("preuve", ".pdf");
        var doc = Jsoup.parse(this.generateResults(emailInitial, evenements, timezone), "", Parser.xmlParser());
        String html = doc.toString();
        try (OutputStream os = new FileOutputStream(pdfFile)) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withHtmlContent(html, null);
            builder.toStream(os);
            builder.run();
        }
        return pdfFile;
    }

    private String getPj(EmailDTO messageInitialAvecReponses) {
        List<PieceJointeDTO> pjs = messageInitialAvecReponses.getMessage().getPiecesJointes();
        if (messageInitialAvecReponses.getMessage() != null && pjs != null && !pjs.isEmpty()) {
            return pjs.stream().map(p -> p.getNom() + " - (" + format(p.getTaille(), 2) + ")").collect(Collectors.joining("\n"));
        } else {
            return null;
        }
    }


}
