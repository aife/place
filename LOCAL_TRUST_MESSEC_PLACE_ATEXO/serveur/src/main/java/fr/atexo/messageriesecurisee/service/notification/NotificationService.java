package fr.atexo.messageriesecurisee.service.notification;

import fr.atexo.messageriesecurisee.dto.EmailDTO;

import java.util.List;

public interface NotificationService {

    void notifier(List<EmailDTO> emails);
}
