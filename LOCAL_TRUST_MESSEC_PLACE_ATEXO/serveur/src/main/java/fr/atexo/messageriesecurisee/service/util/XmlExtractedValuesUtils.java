package fr.atexo.messageriesecurisee.service.util;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.Criteres;
import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.Criteres.Critere;
import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.dto.DossierVolumineuxDTO;
import fr.atexo.messageriesecurisee.dto.FiltreDTO;
import fr.atexo.messageriesecurisee.dto.ObjetMetierDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.messages.consultation.CleValeurType;
import fr.atexo.messageriesecurisee.messages.consultation.FiltreType;
import fr.atexo.messageriesecurisee.messages.consultation.ObjetMetierType;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.messages.envoi.DossierVolumineuxType;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfEmetteur.DestinatairePfEmetteur;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPreSelectionnes.DestinatairePreSelectionne;
import fr.atexo.messageriesecurisee.model.CleValeur;
import fr.atexo.messageriesecurisee.model.DossierVolumineux;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import org.springframework.util.StringUtils;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public final class XmlExtractedValuesUtils {

	private XmlExtractedValuesUtils() {
		super();
	}

	public static SerializableRechercheMessage convertir(RechercheMessage original) {
		SerializableRechercheMessage result = new SerializableRechercheMessage();
		result.setIdPlateformeRecherche(original.getIdPlateformeRecherche());
		result.setObjetMessage(original.getObjetMessage());
		result.setCriteres(convertir(original.getCriteres()));
		result.setDateARDebut(convertir(original.getDateARDebut()));
		result.setDateARFin(convertir(original.getDateARFin()));
		result.setDateDemandeEnvoiDebut(convertir(original.getDateDemandeEnvoiDebut()));
		result.setDateDemandeEnvoiFin(convertir(original.getDateDemandeEnvoiFin()));
		result.setDateEnvoiDebut(convertir(original.getDateEnvoiDebut()));
		result.setDateEnvoiFin(convertir(original.getDateEnvoiFin()));
		result.setMetaDonnees(convertir(original.getMetaDonnees()));
		if (original.getDossiersVolumineux() != null) {
			result.setDossiersVolumineux(convertirFromResponse(original.getDossiersVolumineux()));
		}
		if (original.getNomOuMailDest() != null) {
			result.setNomOuMailDestList(Collections.singletonList(original.getNomOuMailDest()));
		}
		result.setNomOuMailExp(original.getNomOuMailExp());
		// rollback gere la reference d'objet qui n'est pas encore passe
		// result.setRefObjetMetier(original.getRefObjetMetier());
		result.setIdMessageDestinataire(original.getIdMessageDestinataire());
		result.setIdObjetMetier(original.getIdObjetMetier());
		result.setIdSousObjetMetier(original.getIdSousObjetMetier());
		result.setRefObjetMetier(original.getRefObjetMetier());
		result.setMailDestinataire(original.getMailDestinataire());
		result.setMailExpediteur(original.getMailExpediteur());
		result.setPieceJointe(original.getPieceJointe());
		result.setPieceJointeNom(original.getPieceJointeNom());
		Boolean raTemp = StringUtils.hasText(original.getReponseAttendue())
				? Boolean.valueOf(SerializableRechercheMessage.REP_ATT_OUI.equalsIgnoreCase(original.getReponseAttendue()))
				: null;
		result.setReponseAttendue(raTemp);
		result.setTypeMessage(original.getTypeMessage());
		result.setTypePlateformeRecherche(convertir(original.getTypePlateformeRecherche()));
		result.setCodeLien(original.getCodeLien());
		result.getEmailsAlerteReponse().addAll(original.getEmailsAlerteReponse());
		result.setControleMail(original.isControleMail());
		result.setReferencesObjetsMetier(original.getObjetsMetier().stream().map(ObjetMetierType::getReference).collect(Collectors.toSet()));
		result.setIdentifiantsObjetsMetier(original.getObjetsMetier().stream().map(ObjetMetierType::getIdentifiant).collect(Collectors.toSet()));
		if (original.getAgent() != null) {
			result.setIdentifiantsObjetsMetierAgentConnecte(original.getObjetsMetier().stream().filter(om -> om.getCreePar() != null).filter(om -> om.getCreePar().equalsIgnoreCase(original.getAgent().getIdentifiant())).map(ObjetMetierType::getIdentifiant).collect(Collectors.toSet()));
		}
		result.setObjetsMetier(convertirObjetMetier(original.getObjetsMetier()));
		result.setFiltresAppliques(convertir(original.getFiltresAppliques()));
		result.setAfficherFiltreEntreprise(original.isAfficherFiltreEntreprise());
		return result;
	}

	private static FiltreDTO convertir(FiltreType filtreType) {
		if (filtreType == null) {
			return new FiltreDTO();
		}
		return FiltreDTO.builder()
				.dateEnvoiStatut(filtreType.isDateEnvoiStatut())
				.statutAcquitte(filtreType.isStatutAcquitte())
				.statutDelivre(filtreType.isStatutDelivre())
				.reponseAttendue(filtreType.isReponseAttendue())
				.statutEnCourEnvoi(filtreType.isStatutEnCourEnvoi())
				.statutNonDelivre(filtreType.isStatutNonDelivre())
				.reponseLue(filtreType.isReponseLue())
				.reponseNonLue(filtreType.isReponseNonLue())
				.build();
	}

	private static List<DossierVolumineuxDTO> convertirFromResponse(List<fr.atexo.messageriesecurisee.messages.consultation.DossierVolumineuxType> dossierVolumineux) {
		var dossierVolumineuxDto = new ArrayList<DossierVolumineuxDTO>();
		if (dossierVolumineux != null && !dossierVolumineux.isEmpty()) {
			for (var dvXml : dossierVolumineux) {
				var dv = new DossierVolumineuxDTO();
				var dateCreation = toLocalDateTime(dvXml.getDateCreation());
				dv.setDateCreation(dateCreation == null ? null : Date.from(dateCreation.atZone(ZoneId.systemDefault()).toInstant()));
				dv.setNom(dvXml.getNom());
				dv.setUuidReference(dvXml.getUuidReference());
				dv.setUuidTechnique(dvXml.getUuidTechnique());
				dv.setTaille(dvXml.getTaille());
				dossierVolumineuxDto.add(dv);
			}
		}

		return dossierVolumineuxDto;
	}

	private static List<CleValeurDTO> convertir(List<CleValeurType> metaDonnees) {
		return metaDonnees.stream()
				.map(meta -> new CleValeurDTO(meta.getCle(), meta.getCode(), meta.getValeur()))
				.collect(Collectors.toList());
	}

	private static List<ObjetMetierDTO> convertirObjetMetier(List<ObjetMetierType> objetsMetier) {
		return objetsMetier.stream()
				.map(om -> ObjetMetierDTO.builder()
						.urlRetour(om.getUrlRetour())
						.urlSuivi(om.getUrlSuivi())
						.identifiant(om.getIdentifiant())
						.reference(om.getReference())
						.objet(om.getObjet())
						.creePar(om.getCreePar())
						.build())
				.collect(Collectors.toList());
	}

	public static Date convertir(XMLGregorianCalendar date) {
		Date result = null;
		if (date != null) {
			result = date.toGregorianCalendar().getTime();
		}
		return result;
	}

	public static Criteres convertir(fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage.Criteres original) {
		Criteres result = null;
		if (original != null && original.getCritere() != null) {
			result = new Criteres();
			for (fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage.Criteres.Critere origCrit : original
					.getCritere()) {
				Critere critere = new Critere();
				critere.setNom(origCrit.getNom());
				critere.setValeur(origCrit.getValeur());
				result.getCritere().add(critere);
			}
		}
		return result;
	}

	public static fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire convertir(
			fr.atexo.messageriesecurisee.messages.consultation.TypeDestinataire xmlTypeDestinataire) {
		fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire result = fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire.EMETTEUR;
		if (xmlTypeDestinataire != null) {
			switch (xmlTypeDestinataire) {
				case DESTINATAIRE:
					result = fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE;
					break;
				case EMETTEUR:
					result = fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire.EMETTEUR;
					break;
				case BROUILLON:
					result = SerializableRechercheMessage.TypeDestinataire.BROUILLON;
					break;
			}
		}
		return result;
	}

	public static Message convertir(Message message, MessageSecuriseInit messageInit, BigInteger taileMaxFichiers) {
		message.setCartouche(messageInit.getCartouche());
		message.setEmailExpediteur(messageInit.getEmailExpediteur());
		message.setEmailExpediteurTechnique(messageInit.getEmailExpediteurTechnique());
		message.setNomContactExpediteur(messageInit.getNomContactExpediteur());
		message.setEmailReponseExpediteur(messageInit.getEmailReponseExpediteur());
		message.setIdentifiantObjetMetierPlateFormeDestinataire(messageInit.getIdObjetMetierPfDestinataire());
		message.setIdentifiantObjetMetierPlateFormeEmetteur(messageInit.getIdObjetMetierPfEmetteur());
		message.setIdentifiantPfDestinataire(messageInit.getIdPfDestinataire());
		message.setIdentifiantSousObjetMetier(messageInit.getIdSousObjetMetier());
		message.setIdentifiantPfEmetteur(messageInit.getIdPfEmetteur());
		message.setNomCompletExpediteur(messageInit.getNomCompletExpediteur());
		message.setNomPfDestinataire(messageInit.getNomPfDestinataire());
		message.setNomPfEmetteur(messageInit.getNomPfEmetteur());
		message.setUrlPfDestinataire(messageInit.getUrlPfDestinataire());
		message.setUrlPfEmetteur(messageInit.getUrlPfEmetteur());
		message.setUrlPfDestinataireVisualisation(messageInit.getUrlPfDestinataireVisualisation());
		message.setUrlPfEmetteurVisualisation(messageInit.getUrlPfEmetteurVisualisation());
		message.setUrlPfReponse(messageInit.getUrlPfReponse());
		message.setReferenceObjetMetier(messageInit.getRefObjetMetier());
		message.setSignatureAvisPassage(messageInit.getSignatureAvisPassage());
		message.setDestinataires(new HashSet<>());
		message.setDestinatairesPreSelectionnes(new ArrayList<>());
		message.setCriteres(new ArrayList<fr.atexo.messageriesecurisee.model.Critere>());
		if (messageInit.getEmailsAlerteReponse() != null) {
			message.setEmailsAlerteReponse(messageInit.getEmailsAlerteReponse().stream().collect(Collectors.joining(" ")));
		}
		// ajout des nomOuMailDest
		// destinataire destinataire
		if (messageInit.getDestinatairesPfDestinataire() != null
				&& messageInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire() != null
				&& !messageInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().isEmpty()) {
			for (DestinatairePfDestinataire d : messageInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire()) {
				Email e = new Email();
				e.setEmail(d.getMailContactDestinataire());
				e.setType(d.getType());
				e.setOrdre(d.getOrdre());
				e.setIdentifiantContact(d.getIdContactDest());
				e.setNomContact(d.getNomContactDest());
				e.setIdentifiantEntreprise(d.getIdEntrepriseDest());
				e.setNomEntreprise(d.getNomEntrepriseDest());
				e.setTypeDestinataire(TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE);
				e.setMessage(message);
				message.getDestinataires().add(e);
			}
		}
		// destinataire emettreur
		if (messageInit.getDestinatairesPfEmetteur() != null
				&& messageInit.getDestinatairesPfEmetteur().getDestinatairePfEmetteur() != null
				&& !messageInit.getDestinatairesPfEmetteur().getDestinatairePfEmetteur().isEmpty()) {
			for (DestinatairePfEmetteur d : messageInit.getDestinatairesPfEmetteur().getDestinatairePfEmetteur()) {
				Email e = new Email();
				e.setEmail(d.getMailContactDestinataire());
				e.setType(d.getType());
				e.setOrdre(d.getOrdre());
				e.setIdentifiantContact(d.getIdContactDest());
				e.setNomContact(d.getNomContactDest());
				e.setIdentifiantEntreprise(d.getIdEntrepriseDest());
				e.setNomEntreprise(d.getNomEntrepriseDest());
				e.setTypeDestinataire(TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE);
				e.setMessage(message);
				message.getDestinataires().add(e);
			}
		}
		// contacts préselectionnés
		if (messageInit.getDestinatairesPreSelectionnes() != null
				&& messageInit.getDestinatairesPreSelectionnes().getDestinatairePreSelectionne() != null
				&& !messageInit.getDestinatairesPreSelectionnes().getDestinatairePreSelectionne().isEmpty()) {
			for (DestinatairePreSelectionne d : messageInit.getDestinatairesPreSelectionnes().getDestinatairePreSelectionne()) {
				Email e = new Email();
				e.setEmail(d.getMailContactDestinataire());
				e.setType(d.getType());
				e.setOrdre(d.getOrdre());
				e.setIdentifiantContact(d.getIdContactDest());
				e.setNomContact(d.getNomContactDest());
				e.setIdentifiantEntreprise(d.getIdEntrepriseDest());
				e.setNomEntreprise(d.getNomEntrepriseDest());
				e.setTypeDestinataire(TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE);
				message.getDestinatairesPreSelectionnes().add(e);
			}
		}
		// les eventuels criteres de recherche
		if (messageInit.getCriteres() != null && messageInit.getCriteres().getCritere() != null
				&& !messageInit.getCriteres().getCritere().isEmpty()) {
			for (fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.Criteres.Critere d : messageInit.getCriteres()
					.getCritere()) {
				fr.atexo.messageriesecurisee.model.Critere c = new fr.atexo.messageriesecurisee.model.Critere();
				c.setMessage(message);
				c.setNom(d.getNom());
				c.setValeur(d.getValeur());
				message.getCriteres().add(c);
			}
		}
		// defini le type de message par defaut
		if (messageInit.getTypeMessageDefaut() != null && !messageInit.getTypeMessageDefaut().isEmpty()) {
			TypeMessage result = convertir(Integer.valueOf(messageInit.getTypeMessageDefaut()));
			message.setTypeMessage(result);
		}

        message.setMasquerOptionsEnvoi(messageInit != null && messageInit.isMasquerOptionsEnvoi() != null && messageInit.isMasquerOptionsEnvoi());

		if (messageInit.getTypesInterdits() != null && messageInit.getTypesInterdits().getTypeInterdit() != null
				&& !messageInit.getTypesInterdits().getTypeInterdit().isEmpty()) {
			message.setTypesInterdits(new ArrayList<TypeMessage>());
			for (int idx : messageInit.getTypesInterdits().getTypeInterdit()) {
				message.getTypesInterdits().add(convertir(idx));
			}
		}
		if (messageInit.getMaxTailleFichiers() == null) {
			message.setMaxTailleFichiers(taileMaxFichiers);
		} else {
			message.setMaxTailleFichiers(messageInit.getMaxTailleFichiers());
		}
		message.setLogoSrc(messageInit.getLogoSrc());
		message.setCodeLien(messageInit.getCodeLien());
		message.setMetaDonnees(messageInit.getMetaDonnees().stream().map(m -> new CleValeur(m.getCode(), m.getCle(), m.getValeur())).collect(Collectors.toSet()));
		message.setDossiersVolumineux(toDossiersVoumineux(messageInit.getDossiersVolumineux()));
		return message;
	}

	private static TypeMessage convertir(int idx) {
		TypeMessage result = null;
		if (idx > 0 && idx <= TypeMessage.values().length) {
			for (TypeMessage temp : TypeMessage.values()) {
				if (temp.ordinal() == idx) {
					result = temp;
					break;
				}
			}
		}
		return result;
	}

	private static List<DossierVolumineux> toDossiersVoumineux(List<DossierVolumineuxType> dossierVolumineuxTypes) {
		if (dossierVolumineuxTypes == null) {
			return new ArrayList<>();
		}
		return dossierVolumineuxTypes.stream().map(dv -> DossierVolumineux.builder()
				.nom(dv.getNom())
				.taille(dv.getTaille())
				.uuidReference(dv.getUuidReference())
				.uuidTechnique(dv.getUuidTechnique())
				.dateCreation(toLocalDateTime(dv.getDateCreation()))
				.build()
		).collect(Collectors.toList());
	}

    private static LocalDateTime toLocalDateTime(XMLGregorianCalendar xmlCalendar) {
        if (xmlCalendar == null) {
            return null;
        }
        return xmlCalendar.toGregorianCalendar().toZonedDateTime().toLocalDateTime();
    }
}
