package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import no.api.freemarker.java8.Java8ObjectWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.atexo.messageriesecurisee.service.export.ExportUtils.normalizeString;
import static fr.atexo.messageriesecurisee.util.StringFormatterUtil.format;
import static java.util.stream.Collectors.joining;

@Service
@Transactional(readOnly = true)
public class ExportViewBuilderImpl implements ExportViewBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportViewBuilderImpl.class);
    @Autowired
    DocumentGenerator documentGenerator;
    @Autowired
    private ExportPathConfig paths;

    public ExportViewBuilderImpl() {
    }

    @Override
    public void setIdPlateform(String idPlateform) {
        this.paths.setIdPlateform(idPlateform);
    }

    @Override
    public void generateResponseView(Message initialMessage, Email response) throws IOException, TemplateException {
        LOGGER.info("génération de la vue de la réponse : {}", response.getId());
        String atachementPath;
        response.getMessage().getPiecesJointes().forEach(pieceJointe -> {
            pieceJointe.setTailleLisible(format(pieceJointe.getTaille(), 2));
            pieceJointe.setNom(normalizeString(pieceJointe.getNom()));
        });
        String fileName = String.format("%s/Détails_réponse_reçue.html", paths.getResponsePath(response.getMessage(), initialMessage));
        atachementPath = paths.getAtachementPathForResponse(response.getMessage(), initialMessage);
        LOGGER.info(fileName);

        if (!new File(paths.getResponsePath(response.getMessage(), initialMessage)).exists()) {
            new File(paths.getResponsePath(response.getMessage(), initialMessage)).mkdirs();
        }

        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(new Java8ObjectWrapper(Configuration.VERSION_2_3_23));
        Map<String, Object> context = new HashMap<>();
        context.put("message", response.getMessage());
        context.put("piecesJointesPath", atachementPath);
        context.put("destinataires", response.getEmail());
        cfg.setClassForTemplateLoading(ExportService.class, "/");
        Template template = cfg.getTemplate("/templates/export/export_message.ftl");
        Writer fileWriter = new FileWriter(new File(fileName));
        template.process(context, fileWriter);
    }

    @Override
    public void generateHtmlMessageView(Message message) {
        LOGGER.info("génération de la vue du message : {}", message.getId());
        String atachementPath;
        message.getPiecesJointes().forEach(pieceJointe -> {
            pieceJointe.setTailleLisible(format(pieceJointe.getTaille(), 2));
            pieceJointe.setNom(normalizeString(pieceJointe.getNom()));
        });
        String fileName;
        fileName = String.format("%s/Détail_message_envoyé.html", paths.getMessagePath(message));
        atachementPath = paths.getAtachementPathForMessage(message);
        LOGGER.info(fileName);

        if (!new File(paths.getMessageBasePath(message)).exists()) {
            new File(paths.getMessageBasePath(message)).mkdirs();
        }
        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(new Java8ObjectWrapper(Configuration.VERSION_2_3_23));
        Map<String, Object> context = new HashMap<>();
        context.put("message", message);
        context.put("piecesJointesPath", atachementPath);
        context.put("destinataires", message.getDestinatairesPlateFormeDestinataire().stream().map(Email::getEmail).collect(joining(", ")));
        cfg.setClassForTemplateLoading(ExportService.class, "/");
        Template template = null;
        try {
            template = cfg.getTemplate("/templates/export/export_message.ftl");
            Writer fileWriter = new FileWriter(new File(fileName));
            template.process(context, fileWriter);
        } catch (IOException | TemplateException e) {
            LOGGER.error("Erreur lors de la génération HTML pour le message {}", message.getId());
        }

    }

    @Override
    public void generateHtmlListEmailsView(List<Email> emails) {
        LOGGER.info("génération de la liste des vue des mails");
        Message msg = emails.stream().findFirst().map(Email::getMessage).orElse(null);
        if (msg != null) {
            String fileName = String.format("%s/export.html", paths.getExportExchangesPath(msg));
            new File(paths.getExportExchangesPath(emails.stream().findFirst().get().getMessage())).mkdirs();
            Configuration cfg = new Configuration();
            cfg.setObjectWrapper(new Java8ObjectWrapper(Configuration.VERSION_2_3_23));
            Map<String, Object> context = new HashMap<>();
            context.put("emails", emails);
            context.put("referenceObjetMetier", msg.getReferenceObjetMetier());
            cfg.setClassForTemplateLoading(ExportService.class, "/");
            Template template = null;
            try {
                template = cfg.getTemplate("/templates/export/export_suivi.ftl");
                Writer fileWriter = new FileWriter(new File(fileName));
                template.process(context, fileWriter);
            } catch (IOException | TemplateException e) {
                LOGGER.error("Erreur durant la génération de la liste export suivie d'échange", e);
            }
        }
    }

}
