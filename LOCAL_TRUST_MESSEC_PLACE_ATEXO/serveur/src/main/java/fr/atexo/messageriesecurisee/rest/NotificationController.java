package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.dto.EmailDTO;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class NotificationController {
    @MessageMapping("/suivi/{token}")
    @SendTo("/topic/suivi/{token}")
    public List<EmailDTO> send(@DestinationVariable String token, @Payload List<EmailDTO> emails) {
        return emails;
    }
}
