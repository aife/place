package fr.atexo.messageriesecurisee.service.envoyeur;

/**
 * Bean decrivant le corps du message
 * 
 * @author ARD
 * 
 */
public class CorpsMessage extends CorpsMessageBase {

    private String texte1;
    private String texte2;
    /**
     * Pour les messages de types 2 à 4. Dans le cas des messages de type :
     * <ul>
     * <li>2 : il correspond au lien dont l'usage est facultatif ;
     * <li>3 : il prendra place au niveau du bloc des pièces jointes - son usage
     * est obligatoire pour les récupérer</li>
     * <li>4 : son usage est obligatoire pour la consultation du message et de
     * ses pièces jointes.</li>
     * </ul>
     */
    private String lienAR;
    private String signature;

    /**
     * Retourne la partie de texte n°1
     * 
     * @return la partie de texte n°1
     */
    public String getTexte1() {
        return texte1;
    }

    /**
     * Defini la partie de texte n°1
     * 
     * @param texte1
     *            la partie de texte n°1
     */
    public void setTexte1(String texte1) {
        this.texte1 = texte1;
    }

    /**
     * Retourne la partie de texte n°2
     * 
     * @return la partie de texte n°2
     */
    public String getTexte2() {
        return texte2;
    }

    /**
     * Defini la partie de texte n°2
     * 
     * @param texte2
     *            la partie de texte n°2
     */
    public void setTexte2(String texte2) {
        this.texte2 = texte2;
    }

    /**
     * Retourne le lien d'accusé reception
     * 
     * @return le lien d'accusé reception
     * @see #lienAR
     */
    public String getLienAR() {
        return lienAR;
    }

    /**
     * Defini le lien d'accusé reception
     * 
     * @param lienAR
     *            le lien d'accusé reception
     * @see #lienAR
     */
    public void setLienAR(String lienAR) {
        this.lienAR = lienAR;
    }

    /**
     * Retourne la signature à écrire au bas du mail
     * 
     * @return la signature à écrire au bas du mail
     */
    public String getSignature() {
        return signature;
    }

    /**
     * Defini la signature à écrire au bas du mail
     * 
     * @param signature
     *            la signature à écrire au bas du mail
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

}
