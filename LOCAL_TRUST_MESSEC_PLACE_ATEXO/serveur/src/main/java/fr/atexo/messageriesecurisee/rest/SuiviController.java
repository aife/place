package fr.atexo.messageriesecurisee.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.*;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.service.export.ExportService;
import fr.atexo.messageriesecurisee.service.message.MessageService;
import fr.atexo.messageriesecurisee.service.util.XmlExtractedValuesUtils;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@Controller
@RequestMapping(value = {"/rest/suivi", "/rest/v2/suivi"})
public class SuiviController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SuiviController.class);
    @Autowired
    MessageService messageService;

    @Autowired
    EmailService emailService;

    @Autowired
    ExportService exportService;

    @Autowired
    DocumentGenerator documentGenerator;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    TokenManager tokenManager;

    @Autowired
    LoggingService loggingUtils;

    @RequestMapping(value = "/initToken", method = RequestMethod.POST)
    public ResponseEntity<String> init(@RequestBody RechercheMessage rechercheMessage) throws MessageDAOException {
        var plateforme = loggingUtils.getPlateforme(rechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("initialisation d'un token de suivi pour la référence {}", rechercheMessage.getRefObjetMetier());
        LOGGER.debug("données tranmises {}", rechercheMessage);
        SerializableRechercheMessage critereEnregistre = creeMasqueRecherche(rechercheMessage);
        String token = tokenManager.associateData(critereEnregistre, rechercheMessage.getIdPlateformeRecherche(), rechercheMessage.getIdObjetMetier(), rechercheMessage.getRefObjetMetier());
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(token, OK);
    }

    private SerializableRechercheMessage creeMasqueRecherche(RechercheMessage critere) {
        SerializableRechercheMessage result = XmlExtractedValuesUtils.convertir(critere);
        return result;
    }

    @RequestMapping(value = "/brouillons", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<MessageDTO>> searchBrouillon(@RequestParam String token, @PageableDefault
    @SortDefault.SortDefaults({
            @SortDefault(sort = "dateModification", direction = Direction.DESC)
    }) Pageable pageable) throws ServiceException {
        var filtreDefiniParPlateforme = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(filtreDefiniParPlateforme);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("Récupération des brouillons pour la référece {}", filtreDefiniParPlateforme.getRefObjetMetier());
        Page<MessageDTO> messageDTOS = messageService.getBrouillons(filtreDefiniParPlateforme, pageable);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(messageDTOS, OK);
    }

    @RequestMapping(value = "/messages", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationPage<EmailDTO>> search(@RequestParam String token, @RequestBody SerializableRechercheMessage critereEmis, @PageableDefault
    @SortDefault.SortDefaults({
            @SortDefault(sort = "dateDemandeEnvoi", direction = Direction.DESC)
    }) Pageable pageable) throws MessageDAOException, ServiceException {

        var filtreDefiniParPlateforme = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plaeforme = loggingUtils.getPlateforme(filtreDefiniParPlateforme);
        loggingUtils.populateLog(plaeforme);
        LOGGER.info("récupération des messages pour la référence {}", filtreDefiniParPlateforme.getRefObjetMetier());
        messageService.reintroductionDuFiltreDefiniParPlateforme(critereEmis, filtreDefiniParPlateforme);
        Page<EmailDTO> search = messageService.search(critereEmis, pageable);

        LOGGER.debug("Nombre de résultats {}", search);
        loggingUtils.removeLog(plaeforme);
        var page = new PaginationPage<EmailDTO>(search);
        return new ResponseEntity<>(page, OK);
    }

    @RequestMapping(value = "/deleteEmail", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteEmail(@RequestParam(required = true) String token, @RequestParam(required = true) String codeLien) throws MessageDAOException, ServiceException {

        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("suppression de l'email associé au code lien {}", codeLien);
        messageService.deleteMessage(codeLien);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>((Void) null, OK);
    }

    @RequestMapping(value = "/deleteMessage", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteMessage(@RequestParam(required = true) String token, @RequestParam(required = true) Integer id) throws ServiceException, MessageDAOException {

        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("suppression du message associé à l'id {}", id);
        messageService.deleteMessageById(id);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>((Void) null, OK);
    }

    @RequestMapping(value = "/emails/update-status", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmailDTO> updateStatutEmail(@RequestParam(required = true) String token, @RequestParam(required = true) String codeLien) throws ServiceException, MessageDAOException {

        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("modificationdu statut de l'email code lien {}", codeLien);
        EmailDTO email = emailService.updateStatut(codeLien);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(email, OK);
    }

    @RequestMapping(value = "/downloadPieceJointe", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> downloadUploadedFile(@RequestParam(required = false) String token, @RequestParam(required = true) String codeLien, @RequestParam(required = false) String pieceJointeId, @RequestParam(required = false) String pieceJointeUuid) throws MessageDAOException, FileNotFoundException {
        PieceJointeDTO pieceJointe = null;
        if (pieceJointeUuid != null && !pieceJointeUuid.isEmpty()) {
            LOGGER.info("téléchargement de la PJ avec l'uuid {}", pieceJointeUuid);
            pieceJointe = messageService.searchPJByCodeLienAndUuidPj(codeLien, pieceJointeUuid);

        } else {
            LOGGER.info("téléchargement de la PJ avec l'id {}", pieceJointeId);
            pieceJointe = messageService.searchPJByCodeLienAndCodePj(codeLien, pieceJointeId);
        }
        if (pieceJointe == null) {
            return new ResponseEntity<>((InputStreamResource) null, NO_CONTENT);
        }
        try {
            var fis = new FileInputStream(pieceJointe.getChemin());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(pieceJointe.getTaille());
            headers.setContentType(MediaType.parseMediaType(pieceJointe.getContentType()));
            headers.setContentDispositionFormData(pieceJointe.getNom(), pieceJointe.getNom());
            return new ResponseEntity<>(new InputStreamResource(fis), headers, OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new ResponseEntity<>((InputStreamResource) null, INTERNAL_SERVER_ERROR);
        }
    }

    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/getPieceJointe", method = RequestMethod.GET)
    public ResponseEntity<PieceJointeDTO> getPieceJointe(@RequestParam(required = true) String codeLien,
                                                         @RequestParam(required = true) String pieceJointeId) throws MessageDAOException {

        PieceJointeDTO pieceJointe = messageService.searchPJByCodeLienAndCodePj(codeLien, pieceJointeId);

        if (pieceJointe == null) {
            return new ResponseEntity<>((PieceJointeDTO) null, NO_CONTENT);
        }
        return new ResponseEntity<>(pieceJointe, OK);
    }

    @RequestMapping(value = "/acquitterReponse", method = RequestMethod.GET)
    public ResponseEntity<EmailDTO> acquitterReponse(@RequestParam(required = true) String token, @RequestParam(required = true) String codeLien) throws ServiceException, MessageDAOException {
        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("acquittement d'une réponse à partir du token {} et du code lien {}", token, codeLien);
        HttpStatus httpStatus = OK;
        EmailDTO emailDTO = emailService.acquitterReponseEntreprise(codeLien);
        LOGGER.debug("données tranmises {}", emailDTO);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(emailDTO, httpStatus);
    }

    @RequestMapping(value = "/acquitter", method = RequestMethod.GET)
    public ResponseEntity<EmailDTO> acquitterEmail(@RequestParam(required = true) String token, @RequestParam(required = true) String codeLien) throws ServiceException, MessageDAOException {
        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("acquittement de l'email à partir du token {} et du code lien {}", token, codeLien);
        HttpStatus httpStatus = OK;
        EmailDTO emailDTO = emailService.acquitter(codeLien);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(emailDTO, httpStatus);
    }

    @RequestMapping(value = "/countMessageStatus", method = RequestMethod.GET)
    public ResponseEntity<MessageStatusDTO> countMessageStatus(@RequestParam(required = true) String token, Integer idObjetMetier, String[] idsFavoris) throws ServiceException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("récupération des statuts des messages pour les objets {}", idsFavoris);
        HttpStatus httpStatus = OK;
        critereRechercheMessage.setIdObjetMetier(idObjetMetier);
        if (idsFavoris != null && idsFavoris.length > 0) {
            List<Integer> ids = new ArrayList<>();
            for (String id : idsFavoris) {
                ids.add(Integer.valueOf(id));
            }
            critereRechercheMessage.setIds(ids);
        }
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(messageService.countMessageStatus(critereRechercheMessage), httpStatus);
    }

    /**
     * @param listIds liste des id objets métier séparés par des ';'
     * @return
     */


    /**
     * @param listeIds        liste des id metier separee par des ";"
     * @param listeReferences liste des reference (ex numero consultation ou numero contrat ...) metier separee par des ";"
     * @return
     */
    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/countMessageStatusByIds", method = RequestMethod.GET)
    public ResponseEntity<List<MessageStatusDTO>> countMessageStatusByIds(@RequestParam(required = true) String listeIds, @RequestParam(required = true) String listeReferences, @RequestParam(required = false) String mailDestinataire) {
        SerializableRechercheMessage critereRechercheMessage = new SerializableRechercheMessage();


        HttpStatus httpStatus = OK;
        var reponse = new ArrayList<MessageStatusDTO>();
        if (listeIds == null || listeIds.isEmpty() || listeReferences == null || listeReferences.isEmpty())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        String[] idsSplites = listeIds.split(";");
        String[] referencesSplites = listeReferences.split(";");
        if (idsSplites.length != referencesSplites.length)
            return new ResponseEntity<>(reponse, httpStatus);

        for (int i = 0; i < idsSplites.length; i++) {
            // recuperes
            Integer idObjetMetier = Integer.valueOf(idsSplites[i]);
            String reference = referencesSplites[i];
            critereRechercheMessage.setIdObjetMetier(idObjetMetier);
            critereRechercheMessage.setRefObjetMetier(reference);
            critereRechercheMessage.setMailDestinataire(mailDestinataire);

            MessageStatusDTO messageStatusDTO = messageService.countMessageStatus(critereRechercheMessage);
            messageStatusDTO.setIdObjetMetier(idObjetMetier);
            messageStatusDTO.setReferenceObjetMetier(reference);
            reponse.add(messageStatusDTO);
        }
        return new ResponseEntity<>(reponse, httpStatus);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<MessageStatusDTO> countMessagesByPlateforme(@RequestParam(required = true, name = "token") String token) throws ServiceException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("récupération de nombre de messages par plateforme");
        var reponse = messageService.countMessageStatus(critereRechercheMessage);
        loggingUtils.removeLog(plateforme);
        return ResponseEntity.ok(reponse);
    }

    @RequestMapping(value = "/filtres", method = RequestMethod.GET)
    public ResponseEntity<SerializableRechercheMessage> getFiltres(@RequestParam(required = true, name = "token") String token) throws ServiceException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("récupération des filtres associés au token {}", token);
        var reponse = critereRechercheMessage;
        loggingUtils.removeLog(plateforme);
        return ResponseEntity.ok(reponse);
    }


    /**
     * @param listIdsAndReferencesString
     * @param messagerieIdPfEmetteur
     * @return
     */
    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/countMessagesWaitingForAckByIds", method = RequestMethod.GET)
    public ResponseEntity<List<MessageStatusDTO>> countMessagesWaitingForAckByIds(@RequestParam(required = true) String listIdsAndReferencesString, @RequestParam(required = true) String messagerieIdPfEmetteur) {


        HttpStatus httpStatus = OK;
        var reponse = new ArrayList<MessageStatusDTO>();
        if (listIdsAndReferencesString == null || listIdsAndReferencesString.isEmpty() || messagerieIdPfEmetteur == null || messagerieIdPfEmetteur.isEmpty())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        String[] listIdsAndReferencesArray = listIdsAndReferencesString.split(";");

        List<MessageStatusDTO> messageStatusDTOList = emailService.countMessagesWaitingForAckByIds(listIdsAndReferencesArray, messagerieIdPfEmetteur);

        reponse.addAll(messageStatusDTOList);

        return new ResponseEntity<>(reponse, httpStatus);
    }

    @RequestMapping(value = "/findEmailUpdated", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmailDTO>> findEmailUpdated(@RequestParam(required = true) String token, @RequestBody String[] listeCodeLien) {
        List<EmailDTO> emailsUpdated = emailService.findEmailUpdated(listeCodeLien);
        HttpStatus httpStatus = OK;
        return new ResponseEntity<>(emailsUpdated, httpStatus);
    }

    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/preuveEchanges/{idEmail}", method = RequestMethod.GET)
    public void genererPreuveEchanges(@PathVariable Integer idEmail, @RequestParam String token, @RequestParam(required = false, defaultValue = "Europe/Paris") String timezone, HttpServletResponse response) throws IOException, ServiceException, MessageDAOException {
        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("génération de la preuve des échanges pour l'id email {} et le token {}", idEmail, token);

        var file = documentGenerator.genererPDFPreuveEchanges(idEmail, timezone);
        try {
            var fis = new BufferedInputStream(new FileInputStream(file));
            DateTimeFormatter newPattern = DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmm");
            String date = LocalDateTime.now().format(newPattern);
            String fileName = "echanges_" + date + ".pdf";
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
            response.setContentType(Files.probeContentType(file.toPath()));
            response.setContentLength((int) file.length());
            FileCopyUtils.copy(fis, response.getOutputStream());
            response.flushBuffer();
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            LOGGER.error("Erreur durant la génération de preuve d'échanges : ", e);
        }
        loggingUtils.removeLog(plateforme);
    }

    @PostMapping(value = "/destinataires")
    public ResponseEntity<List<DestinataireDTO>> getDestinataires(@RequestParam String token, @RequestBody String term) throws ServiceException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("récupération des destinataires depuis le token {}", token);
        LOGGER.debug("données transmises {}", critereRechercheMessage);
        // seulement coté agent
        if (critereRechercheMessage == null || critereRechercheMessage.getIdEntrepriseDest() != null || critereRechercheMessage.getMailDestinataire() != null) {
            throw new ServiceException("Fonctionnalité Destinataires indisponible coté entreprise");
        }

        var identifiantObjetMetier = critereRechercheMessage.getIdObjetMetier();
        String referenceObjetMetier = critereRechercheMessage.getRefObjetMetier();
        var identifiantPlateforme = critereRechercheMessage.getIdPlateformeRecherche();
        List<DestinataireDTO> destinatairesNotifies = emailService.getDestinataires(identifiantObjetMetier, referenceObjetMetier, identifiantPlateforme, term);
        // dedoublonnage
        List<DestinataireDTO> destinatairesUniques = new ArrayList<>();
        destinatairesNotifies.stream().filter(d -> d.getMailContactDestinataire() != null && d.getMailContactDestinataire().contains("@")).forEach(d -> {
            if (destinatairesUniques.stream().noneMatch(p -> d.getMailContactDestinataire().equalsIgnoreCase(p.getMailContactDestinataire()))) {
                destinatairesUniques.add(d);
            }
        });
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(destinatairesUniques, OK);
    }

    @RequestMapping(value = "/historique", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EvenementDTO>> getHistory(@RequestParam(required = true) String token, @RequestParam String codeLien) throws ServiceException, MessageDAOException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        // seulement coté agent
        if (critereRechercheMessage == null || critereRechercheMessage.getIdEntrepriseDest() != null || critereRechercheMessage.getMailDestinataire() != null) {
            throw new ServiceException("Fonctionnalité Historique indisponible coté entreprise");
        }
        List<EvenementDTO> emails = emailService.getHistory(codeLien);
        loggingUtils.removeLog(plateforme);
        return ResponseEntity.ok(emails);
    }

    @RequestMapping(value = "/favoris/toggle", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> toggleFavoris(@RequestParam(required = true) String token, @RequestParam String codeLien) throws ServiceException, MessageDAOException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("passage de l'email avec code lien {} en favoris", codeLien);
        // seulement coté agent
        if (critereRechercheMessage.getIdEntrepriseDest() != null || critereRechercheMessage.getMailDestinataire() != null) {
            throw new ServiceException("Fonctionnalité indisponible coté entreprise");
        }
        loggingUtils.removeLog(plateforme);
        return ResponseEntity.ok(emailService.toggleFavori(codeLien));
    }

    @RequestMapping(value = "/messages/entreprise", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginationPage<EmailDTO>> searchEntreprise(@RequestParam String token, @RequestBody PaginationDTO pageable) throws MessageDAOException, ServiceException {
        var critereEmis = new SerializableRechercheMessage();
        var statutsAutorises = Arrays.asList(TypeStatutEmail.ENVOYE, TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT, TypeStatutEmail.ACQUITTE);
        var statusAExclure = Arrays.stream(TypeStatutEmail.values()).filter(statut -> !statutsAutorises.contains(statut)).collect(Collectors.toList());
        critereEmis.setStatutsAExclure(statusAExclure);
        critereEmis.setInclureReponses(false);
        // fixme: workaround RSEM & EXEC
        critereEmis.setExclureReponses(true);
        return search(token, critereEmis, PageRequest.of(pageable.getPage(), pageable.getSize(), Sort.by(Direction.DESC, pageable.getSort())));
    }

    @PostMapping(value = "/export")
    public void exportSuivi(@RequestParam String token, @RequestBody ExportDTO exportDTO, HttpServletResponse response) throws ServiceException, IOException {
        var critereRechercheMessage = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critereRechercheMessage);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("export des résultats pour le filtre {}", critereRechercheMessage);
        // seulement coté agent
        if (critereRechercheMessage.getIdEntrepriseDest() != null || critereRechercheMessage.getMailDestinataire() != null) {
            throw new ServiceException("Fonctionnalité indisponible coté entreprise");
        }
        messageService.reintroductionDuFiltreDefiniParPlateforme(exportDTO.getCritere(), critereRechercheMessage);
        loggingUtils.removeLog(plateforme);
        File file = exportService.export(exportDTO.getCritere(), exportDTO.getTemplates());
        var contentType = Files.probeContentType(file.toPath());
        try {
            var fis = new BufferedInputStream(new FileInputStream(file));
            DateTimeFormatter newPattern = DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmm");
            String date = LocalDateTime.now().format(newPattern);
            String fileName = "export_" + date + ".xlsx";
            response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
            response.setContentType(contentType);
            response.setContentType(Files.probeContentType(file.toPath()));
            response.setContentLength((int) file.length());
            FileCopyUtils.copy(fis, response.getOutputStream());
            response.flushBuffer();
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            LOGGER.error("Erreur durant la génération de l'export: ", e);
        }
    }
}
