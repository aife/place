package fr.atexo.messageriesecurisee.service.envoyeur;

/**
 * Exception genereée lors de l'nvoi d'un email
 * 
 * @author ARD
 * 
 */
public class ExceptionEnvoiMail extends Exception {

	/**
	 * Construit une exception d'envoi d'email
	 */
	public ExceptionEnvoiMail() {
		super();
	}

	/**
	 * Construit une exception d'envoi d'email
	 * 
	 * @param message
	 *            le message de l'exception
	 */
	public ExceptionEnvoiMail(String message) {
		super(message);
	}

	/**
	 * Construit une exception d'envoi d'email
	 * 
	 * @param message
	 *            le message de l'exception
	 * @param e
	 *            l'exception parente
	 */
	public ExceptionEnvoiMail(String message, Exception e) {
		super(message, e);
	}
}
