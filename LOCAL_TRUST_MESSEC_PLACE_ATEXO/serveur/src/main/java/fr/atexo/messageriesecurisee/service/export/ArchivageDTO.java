package fr.atexo.messageriesecurisee.service.export;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class ArchivageDTO {

    @NotNull
    @JsonProperty("identifiantPlateforme")
    private String identifiantPlateforme;

    public String getIdentifiantPlateforme() {
        return identifiantPlateforme;
    }

    public void setIdentifiantPlateforme(String identifiantPlateforme) {
        this.identifiantPlateforme = identifiantPlateforme;
    }
}
