package fr.atexo.messageriesecurisee.service.configuration;

import java.math.BigInteger;

public interface ConfigurationManager {

    BigInteger getTailleMaxUnFichier();

    String getRepertoireTelechargement();

    String getTemporaryPath();

    BigInteger getTailleMaxTousfichiers();

    String getCheminXmlGenerationMessageTexte();

    String getCheminXMLGenerationMessage();

    String getCheminXmlAlerteMessageEntreprise();

    String getCheminLogPostFix();

    Integer getSmtpPort();

    String getSmtpServeur();

    boolean isSmtpDebug();

    String getCheminXMLGenerationAlerte();

    String getCheminXmlAccuseReceptionEntreprise();

    String getExportPath();

    int getTokenExpirationDelay();

    int getNombreEssaisMax();

    String getServeurEnvoi();

    long getTmpFileExpirationDelay();

    String getCheminXmlAccuseReceptionEnvoiEntreprise();

    String getCheminXMLGenerationAlerteTiers();

    int getDelaiPooling();
}
