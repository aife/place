package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static fr.atexo.messageriesecurisee.service.export.ExportUtils.getStringDate;

@Component
public class ExportPathConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportPathConfig.class);

    private static String MESSAGERIE_FOLDER = "6. Messagerie sécurisée";

    @Autowired
    private ConfigurationManager configuration;

    private String idPlateform;

    public String getMessagePath(Message msg) {
        return String.format("%s/Détail_message_envoyé", this.getMessageBasePath(msg));
    }

    public String getResponsePath(Message response, Message msgInitial) {
        return String.format("%s/Réponse_reçue_%s_%s", this.getMessageBasePath(msgInitial), response.getId(), getStringDate(response.getDateDemandeEnvoi()));
    }

    public String getAtachementPathForMessage(Message msg) {
        return String.format("%s/PJ transmises", this.getMessagePath(msg));
    }

    public String getAtachementPathForResponse(Message response, Message msgInitial) {
        return String.format("%s/PJ transmises", this.getResponsePath(response, msgInitial));
    }

    public String getExportExchangesPath(Message msg) {
        return String.format("%s/Export suivi échanges", this.getBasePath(msg));
    }

    public String getMessageBasePath(Message msg) {
        return String.format("%s/Message_envoyé_%s_%s", this.getBasePath(msg), msg.getId(), getStringDate(msg.getDateDemandeEnvoi()));
    }

    public String getBasePath(Message msg) {
        String refObjetMetier = msg.getReferenceObjetMetier();
        if (refObjetMetier == null || "".equals(refObjetMetier.trim())) {
            LOGGER.warn("Référence objet métier introuvable, on exporte vers : {}", refObjetMetier);
            refObjetMetier = "ID_" + msg.getIdentifiantObjetMetierPlateFormeEmetteur();
        }
        return String.format("%s/%s/%s/%s", configuration.getExportPath(), this.getIdPlateform(), refObjetMetier, MESSAGERIE_FOLDER);
    }

    public String getBasePath() {
        return String.format("%s/%s", configuration.getExportPath(), this.getIdPlateform());
    }

    public String getIdPlateform() {
        return idPlateform;
    }

    public void setIdPlateform(String idPlateform) {
        this.idPlateform = idPlateform;
    }
}
