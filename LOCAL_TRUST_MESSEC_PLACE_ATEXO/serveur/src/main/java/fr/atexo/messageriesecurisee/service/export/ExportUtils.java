package fr.atexo.messageriesecurisee.service.export;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ExportUtils {

    public static String normalizeString(String txt) {
        return txt.replaceAll("[^a-zA-Z0-9\\.\\-_]", "_");
    }

    public static String getStringDate(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        return date.format(DateTimeFormatter.ofPattern("yyyy_MM_dd"));
    }

}
