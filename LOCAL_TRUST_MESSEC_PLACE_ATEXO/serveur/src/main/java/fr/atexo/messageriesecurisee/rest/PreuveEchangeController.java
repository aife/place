package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@RestController
@RequestMapping(value = {"/rest/preuves", "/rest/v2/preuves"})
@RequiredArgsConstructor
@Slf4j
public class PreuveEchangeController {

    @NonNull
    DocumentGenerator documentGenerator;
    @NonNull
    LoggingService loggingUtils;

    @NonNull
    TokenManager tokenManager;

    @GetMapping(value = "/{uuidEmail}")
    public ResponseEntity<Resource> genererPreuveEchanges(@PathVariable String uuidEmail, @RequestParam String token, @RequestParam(required = false, defaultValue = "Europe/Paris") String timezone) throws ServiceException, MessageDAOException, IOException {
        var critere = Optional.ofNullable((SerializableRechercheMessage) tokenManager.getData(token)).orElseThrow(() -> new ServiceException(ServiceException.AUCUN_CRITERE_PLATEFORME + token));
        var plateforme = loggingUtils.getPlateforme(critere);
        loggingUtils.populateLog(plateforme);
        log.info("génération de la preuve des échanges pour l'uuid email {} et le token {}", uuidEmail, token);

        var file = documentGenerator.genererPDFPreuveEchanges(uuidEmail, timezone);
        var newPattern = DateTimeFormatter.ofPattern("yyyy_MM_dd_HHmm");
        var date = LocalDateTime.now().format(newPattern);
        var fileName = "echanges_" + date + ".pdf";
        var resource = new ByteArrayResource(FileCopyUtils.copyToByteArray(file));
        Files.deleteIfExists(file.toPath());
        loggingUtils.removeLog(plateforme);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
                .body(resource);
    }
}
