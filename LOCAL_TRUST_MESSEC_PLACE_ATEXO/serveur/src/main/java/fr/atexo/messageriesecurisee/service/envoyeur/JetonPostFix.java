package fr.atexo.messageriesecurisee.service.envoyeur;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JetonPostFix {


    private String jeton;
    private EtatPostFix etat;
    private String ligne;
    private String details;
    private LocalDateTime date = LocalDateTime.now();
    private String typeSymbole;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((jeton == null) ? 0 : jeton.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JetonPostFix other = (JetonPostFix) obj;
        if (jeton == null) {
            if (other.jeton != null)
                return false;
        } else if (!jeton.equals(other.jeton))
            return false;
        return true;
    }
}
