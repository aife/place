package fr.atexo.messageriesecurisee.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.service.message.MessageService;
import fr.atexo.messageriesecurisee.service.upload.UploadManager;
import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = {"/rest/pieces-jointes", "/rest/v2/pieces-jointes"})
@RequiredArgsConstructor
@Slf4j
public class PieceJointeController {

    @NonNull
    private final MessageService messageService;

    @NonNull
    private final TokenManager tokenManager;

    @NonNull
    private final UploadManager uploadManager;

    @NonNull
    ObjectMapper objectMapper;

    @GetMapping(value = "/{token}/{messageUuid}/{uuid}")
    public ResponseEntity<InputStreamResource> downloadUploadedFileByUuid(@PathVariable String token, @PathVariable String messageUuid, @PathVariable String uuid) throws MessageDAOException, IOException {
        log.info("téléchargement de la PJ avec l'uuid {}", uuid);
        PieceJointeDTO pieceJointe = messageService.searchPJByCodeLienAndUuidPj(messageUuid, uuid);
        log.info("verification de l'acces a la pj {}, depuis le token {} et le message {}", uuid, token, messageUuid);
        if (!pjAccessible(token, messageUuid, uuid)) {
            log.warn("PJ {} inaccessible", uuid);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        try (var fisCheck = new FileInputStream(pieceJointe.getChemin())) {
            uploadManager.verifInputStream(pieceJointe.getNom(), fisCheck);
        }
        var fis = new FileInputStream(pieceJointe.getChemin());
        var headers = new HttpHeaders();
        headers.setContentLength(pieceJointe.getTaille());
        headers.setContentType(MediaType.parseMediaType(pieceJointe.getContentType()));
        headers.setContentDispositionFormData(pieceJointe.getNom(), pieceJointe.getNom());
        return new ResponseEntity<>(new InputStreamResource(fis), headers, OK);
    }

    @GetMapping(value = "/{token}/{messageUuid}/reference/{reference}")
    public ResponseEntity<InputStreamResource> downloadUploadedFileByReference(@PathVariable String token, @PathVariable String messageUuid, @PathVariable String reference) throws PieceJointeIntrouvableException, IOException {
        log.info("téléchargemenet de la piece jointe {} depuis le token {}", reference, token);
        var uploadFile = uploadManager.getFile(reference);
        var inputStream = new ByteArrayInputStream(Files.readAllBytes(Path.of(uploadFile.getChemin())));
        var inputStreamCheck = new ByteArrayInputStream(Files.readAllBytes(Path.of(uploadFile.getChemin())));
        var headers = new HttpHeaders();
        headers.setContentDisposition(ContentDisposition.builder("attachment; filename=\"" + uploadFile.getNom() + "\"").build());
        headers.setContentLength(uploadFile.getTaille());
        headers.setContentType(MediaType.parseMediaType(uploadFile.getMimeType()));
        headers.setContentDispositionFormData(uploadFile.getNom(), uploadFile.getNom());
        uploadManager.verifInputStream(uploadFile.getNom(), inputStreamCheck);
        return new ResponseEntity<>(new InputStreamResource(inputStream), headers, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{token}/{messageUuid}/{reference}")
    public ResponseEntity<String> deleteUploadedFile(@PathVariable String token, @PathVariable String messageUuid, @PathVariable String reference) throws MessageDAOException {
        log.info("suppression du fichier {}", reference);
        uploadManager.removeFile(reference);
        return new ResponseEntity<>((String) null, HttpStatus.OK);
    }

    @PostMapping(value = "/")
    public ResponseEntity<List<PieceJointeDTO>> uploadFile(MultipartHttpServletRequest request, @RequestParam(required = false) String fileName) throws IOException {
        log.info("upload du fichier {}", fileName);
        var references = new ArrayList<PieceJointeDTO>();
        Iterator<String> itr = request.getFileNames();
        while (itr.hasNext()) {
            String uploadedFile = itr.next();
            MultipartFile file = request.getFile(uploadedFile);
            if (file != null) {
                String storedName = fileName == null ? file.getOriginalFilename() : fileName;
                uploadManager.verifInputStream(storedName, file.getInputStream());
                var reference = uploadManager.saveFile(storedName, file.getSize(), file.getContentType(), file.getInputStream());
                references.add(PieceJointeDTO.builder()
                        .nom(storedName)
                        .taille(Long.valueOf(file.getSize()).intValue())
                        .reference(reference)
                        .build());
            }
        }
        return new ResponseEntity<>(references, HttpStatus.OK);
    }

    private boolean pjAccessible(String token, String messageUuid, String uuid) throws MessageDAOException {
        var messageDto = messageService.getMessageByUuid(messageUuid);
        var pjUuids = messageDto.getPiecesJointes().stream().map(PieceJointeDTO::getUuid).collect(Collectors.toSet());
        var tokenData = tokenManager.getData(token);
        if (tokenData == null) {
            throw new MessageDAOException("token " + token + " introuvable");
        }
        return pjUuids.contains(uuid);
    }

}
