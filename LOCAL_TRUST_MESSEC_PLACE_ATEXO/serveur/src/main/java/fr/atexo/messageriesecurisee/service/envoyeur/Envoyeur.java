package fr.atexo.messageriesecurisee.service.envoyeur;

import fr.atexo.messageriesecurisee.service.upload.UploadManager;
import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class Envoyeur {

    @Autowired
    EmailEnvoiService envoiService;

	@Value("${polling.sender:*/50 * * * * ?}")
	private String cronMails;

    @Value("${polling.parser:0 0/2 * * * ?}")
    private String cronStatuts;

    @Value("${alerte.sender:0 0/2 * * * ?}")
    private String cronAlerte;


    @Autowired
    TokenManager tokenManager;

    @Autowired
    UploadManager uploadManager;

    /**
     * Envoi les mails
     * appelé par le scheduler
     */
    @Scheduled(cron = "${polling.sender:*/50 * * * * ?}")
    public void envoi() {
        log.debug("Envoie des messages en attente");

        envoiService.envoieTousMessagesEnAttente();
    }

    /**
     * Verifie l'etat de leur retour sur Exim
     * appelé par le scheduler
     */
    @Scheduled(cron = "${polling.parser:0 0/2 * * * ?}")
    public void miseAJourStatut() {
        log.debug("Mise a jour des status des mails");

        envoiService.verifieEnvoiParServeurPostFix();
    }

	/**
	 * Envoi des alertes
	 */
    @Scheduled(cron = "${alerte.sender:0 0/2 * * * ?}")
    public void envoiAlerte() {
        log.info("Envoi des alertes");

        envoiService.envoyerAlertes();
    }

    @PostConstruct
    public void echo() {
        log.info("Expressions CRON : envoi des mails en attente = '{}', mise a jour des statuts = '{}', alertes = '{}'", cronMails, cronStatuts, cronAlerte);
    }

    @Scheduled(cron = "${token.expiration.sender:0 0 0/1 * * ?}")
    public void suppressionTokenExpires() {
        tokenManager.removeExpiredTokens();
    }


    /**
     * Traitement des fichiers temporaires expirés
     * appelé par le scheduler
     */
    @Scheduled(cron = "${tmp.file.expiration.sender:0 0 0/1 * * ?}")
    public void suppressionFichiersTemporairesExpires() {
        uploadManager.removeExpiredFiles();
    }

    /**
     * Traitement des mails expirés
     * appelé par le scheduler
     */
    @Scheduled(cron = "${polling.expired:0 0 0/1 * * ?}")
    public void traitementMailsExpires() {
        envoiService.envoiEmailsExpires();
    }
}
