package fr.atexo.messageriesecurisee.service.envoyeur;

import fr.atexo.commun.freemarker.FreeMarkerUtil;
import fr.atexo.messageriesecurisee.mapper.CleValeurMapper;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.CleValeur;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import fr.atexo.messageriesecurisee.util.StringTransformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.util.StringFormatterUtil.format;

/**
 * Constructeur de message, cette classe permet de construire un message pour un
 * destinataire
 *
 * @author ARD
 */
@Component
public class MessageBuilderImpl implements MessageBuilder {

    private static final String EMAIL_FREEMARKER_NOM = "email";
    private static final String ERREUR = "erreur";
    private static final String LOGO = "logoSrc";
    private static final String CARTOUCHE = "cartouche";
    private static final String TEXTE1 = "Le contenu du message est :";
    private static final String TEXTE2 = "Accès direct au message :";
    private static final String TEXTE4 = "Le contenu de ce message est à venir consulter à l'adresse suivante :";
    private static final String[] codesAccuseDeReceptionAgent = {"ENTITE_PUBLIQUE", "ENTITE_ACHAT", "INTITULE", "OBJET", "REFERENCE", "TYPE_PROCEDURE", "DATE_MISE_EN_LIGNE", "DATE_HEURE_LIMITE_REMISE_PLIS"};
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageBuilderImpl.class);

    @Autowired
    private ConfigurationManager configurationManager;

    @Autowired
    private DocumentGenerator documentGenerator;

    @Autowired
    private EmailMapper emailMapper;

    @Autowired
    private CleValeurMapper cleValeurMapper;

    /**
     * Construit la chaine de caractères mise en page constituant le corps du
     * message, soit sous format html, soit sous format texte
     *
     * @param message le message à envoyer
     * @param email   le destinataire du message
     * @param html    <ul>
     *                <li><code>true</code> pour générer le corps du message en html
     *                </li>
     *                <li><code>false</code> pour le generer en texte</li>
     *                </ul>
     * @return la chaine de caractere constituant le corsp du message
     */
    @Override
    public String buildContenuMessage(Message message, Email email, boolean html) {
        CorpsMessage corpsMessage = definitionContenuMail(message, email);
        var hasDateLimiteResponse = true;
        if (html) {
            miseEnFormeSpecifiqueHtml(corpsMessage);
        }
        Map<String, Object> mapFreeMarker = new HashMap<String, Object>();
        mapFreeMarker.put(EMAIL_FREEMARKER_NOM, corpsMessage);
        mapFreeMarker.put("limiteReponse", message.isDateLimiteReponseAttendue());
        mapFreeMarker.put("msgDateLimiteReponse", checkIfShowMessage(message));
        mapFreeMarker.put("isResponseAttendue", hasDateLimiteResponse);
        String xmlFile = getTemplate(message.getTemplateMail(), html);

        try {
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(xmlFile)), StandardCharsets.UTF_8);
            return FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
        } catch (Exception e) {
            LOGGER.error(ERREUR, e);
            return null;
        }
    }

    private String checkIfShowMessage(Message message) {
        if (message.isDateLimiteReponseAttendue() && message.getDateLimiteReponse() != null) {
            return String.format("Une r&eacute;ponse de votre part est attendue avant le <b>%s</b>.", getDateFormat("dd/MM/yyyy HH:mm", message.getDateLimiteReponse()));
        }
        return null;
    }

    private void miseEnFormeSpecifiqueHtml(CorpsMessage corpsMessage) {
        corpsMessage.setTexte1(traitementHtmlPourString(corpsMessage.getTexte1()));
        corpsMessage.setTexte2(traitementHtmlPourString(corpsMessage.getTexte2()));
        corpsMessage.setLienAR(traitementHtmlPourString(corpsMessage.getLienAR()));
        // si le contenu est au format HTML on ne le traite pas
        if (corpsMessage.getContenuHTML() != null && !corpsMessage.getContenuHTML()) {
            corpsMessage.setContenu(traitementHtmlPourString(corpsMessage.getContenu()));
        }

        corpsMessage.setSignature(traitementHtmlPourString(corpsMessage.getSignature()));
    }

    private String traitementHtmlPourString(String original) {
        String result = original;
        if (!ObjectUtils.isEmpty(original)) {
            result = StringTransformer.traitementGlobalPourHtml(StringEscapeUtils.escapeHtml4(original));
        }
        return result;
    }

    /**
     * Fonction retournant le corps du message
     *
     * @param message le message principale
     * @param email   le destinataire du message
     * @return l'objet décrivant le corps du message
     */
    private CorpsMessage definitionContenuMail(Message message, Email email) {
        CorpsMessage result = instancieEtRemplitCorpsMessage(message, email);
        switch (message.getTypeMessage()) {
            // L'inclusion des pièces-jointes
            // est effectuée lors de la transmission au serveur SMTP.
            case TypeMessage1:
                result.setTexte2(null);
                result.setLienAR(null);
                break;
            // cf. TypeMessage1
            case TypeMessage2:
                break;
            // Aucune inclusion de pièces jointes lors de la transmission au serveur
            // SMTP. Le lien AR en fait office.
            case TypeMessage3:
                result.setTexte2(null);
                break;
            // cf. TypeMessage3, le contenu du mail étant lui aussi supprimé au
            // profit du lien AR.
            case TypeMessage4:
                result.setTexte1(TEXTE4);
                result.setTexte2(null);
                result.setContenu(null);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Le remplissage est fait intégralement puisqu'il suffit le plus souvent de
     * n'ôter que peu d'éléments afin de correspondre au type de message
     * souhaité.
     *
     * @param message : Message
     * @param email : Email
     * @return CorpsMessage
     */
    private CorpsMessage instancieEtRemplitCorpsMessage(Message message, Email email) {
        CorpsMessage result = new CorpsMessage();
        result.setCartouche(message.getCartouche());
        result.setTexte1(TEXTE1);
        result.setTexte2(TEXTE2);
        result.setContenu(message.getContenu());
        result.setContenuHTML(true);
        result.setLienAR(EnvoyeurUtils.creerLienAR(message, email, false));
        result.setSignature(Optional.ofNullable(message.getSignatureAvisPassage()).orElse(message.getNomCompletExpediteur()));
        result.setLogoSrc(message.getLogoSrc());
        result.setObjet(message.getObjet());
        return result;
    }


    public String buildContenuMessageAlerte(Email emailInitial, Set<CleValeur> metaDonnees) {
        var emailDto = emailMapper.toDto(emailInitial);
        var messageExpediteur = emailDto.getMessage();
        var clesValeursDto = cleValeurMapper.toDtos(metaDonnees);
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put("ObjetMessage", messageExpediteur.getObjet());
        mapFreeMarker.put("DateEnvoiMessage", emailInitial.getDateEnvoi());
        mapFreeMarker.put("nomPf", Optional.ofNullable(emailInitial.getMessage().getSignatureAvisPassage()).orElse(emailInitial.getMessage().getNomCompletExpediteur()));
        mapFreeMarker.put("ref", emailInitial.getMessage().getReferenceObjetMetier());
        mapFreeMarker.put(LOGO, emailInitial.getMessage().getLogoSrc());
        mapFreeMarker.put("metaDonnees", documentGenerator.groupeByCode(clesValeursDto, codesAccuseDeReceptionAgent));
        mapFreeMarker.put(CARTOUCHE, emailInitial.getMessage().getCartouche());
        LOGGER.info("Création du contexte sur la génération du contenue agent");

        if (!ObjectUtils.isEmpty(messageExpediteur.getCartouche())) {
            mapFreeMarker.put(CARTOUCHE, messageExpediteur.getCartouche());
        }
        if (!ObjectUtils.isEmpty(messageExpediteur.getUrlPfEmetteurVisualisation())) {
            mapFreeMarker.put("urlPfEmetteurVisualisation", messageExpediteur.getUrlPfEmetteurVisualisation());
        }
        try {
            String templateMailAlerte = configurationManager.getCheminXMLGenerationAlerte();
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(templateMailAlerte)), StandardCharsets.UTF_8);
            String contenu = FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
            LOGGER.info("Génération du message alerte agent terminé");
            return contenu;
        } catch (Exception e) {
            LOGGER.error("Problème à la lecture du fichier xml de freamker", e);
            return null;
        }
    }

    @Override
    public String buildContenuMesageAcquittementEntreprise(Message reponse, Message messageInitial, String objet) {
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put(LOGO, messageInitial.getLogoSrc());
        mapFreeMarker.put("objetInitial", objet);
        mapFreeMarker.put("objetReponse", reponse.getObjet());
        mapFreeMarker.put("contenu", reponse.getContenu());
        mapFreeMarker.put("piecesJointes", reponse.getPiecesJointes().stream().map(pj -> pj.getNom() + " (" + format(Optional.ofNullable(pj.getTaille()).map(Integer::doubleValue).orElse(0d), 2) + ")").collect(Collectors.toList()));
        mapFreeMarker.put(CARTOUCHE, messageInitial.getCartouche());
        try {
            String templateAccuseReception = configurationManager.getCheminXmlAccuseReceptionEntreprise();
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(templateAccuseReception)), StandardCharsets.UTF_8);
            return FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
        } catch (Exception e) {
            LOGGER.error(ERREUR, e);
            return null;
        }
    }

    @Override
    public String buildContenuMessageAlerteMessageEntreprise(Message message) {
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put(LOGO, message.getLogoSrc());
        mapFreeMarker.put(CARTOUCHE, message.getCartouche());
        try {
            String templateAlerteMessageEntreprise = configurationManager.getCheminXmlAlerteMessageEntreprise();
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(templateAlerteMessageEntreprise)), StandardCharsets.UTF_8);
            return FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
        } catch (Exception e) {
            LOGGER.error(ERREUR, e);
            return null;
        }
    }

    @Override
    public String buildContenuMessageAcquittementEnvoiEntreprise(Message message, String objet) {
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put(LOGO, message.getLogoSrc());
        mapFreeMarker.put("objetInitial", objet);
        mapFreeMarker.put("objetEnvoi", message.getObjet());
        mapFreeMarker.put("contenu", message.getContenu());
        mapFreeMarker.put("piecesJointes", message.getPiecesJointes().stream().map(pj -> pj.getNom() + " (" + format(Optional.ofNullable(pj.getTaille()).map(Integer::doubleValue).orElse(0d), 2) + ")").collect(Collectors.toList()));
        mapFreeMarker.put(CARTOUCHE, message.getCartouche());
        try {
            String templateAccuseReception = configurationManager.getCheminXmlAccuseReceptionEnvoiEntreprise();
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(templateAccuseReception)), StandardCharsets.UTF_8);
            return FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
        } catch (Exception e) {
            LOGGER.error(ERREUR, e);
            return null;
        }
    }

    @Override
    public String buildContenuMessageAlerteTiers(Email emailInitial, String objet) {
        Map<String, Object> mapFreeMarker = new HashMap<>();
        mapFreeMarker.put("objetMessage", objet);
        mapFreeMarker.put("lien", EnvoyeurUtils.creerLienAR(emailInitial.getMessage(), emailInitial, false));
        mapFreeMarker.put(LOGO, emailInitial.getMessage().getLogoSrc());
        mapFreeMarker.put(CARTOUCHE, emailInitial.getMessage().getCartouche());
        LOGGER.info("Création du contexte sur la génération du contenu alerte tiers");

        try {
            String templateMailAlerte = configurationManager.getCheminXMLGenerationAlerteTiers();
            String freeMarkerXML = IOUtils.toString(Objects.requireNonNull(MessageBuilderImpl.class.getClassLoader().getResourceAsStream(templateMailAlerte)), StandardCharsets.UTF_8);
            String contenu = FreeMarkerUtil.executeDefaultScriptFreeMarker(mapFreeMarker, freeMarkerXML);
            LOGGER.info("Génération du message alerte tiers terminé");
            return contenu;
        } catch (Exception e) {
            LOGGER.error("Problème à la lecture du fichier xml de freemarker", e);
            return null;
        }
    }

    private String getTemplate(String templateName, boolean html) {
        if (html) {
            if (templateName != null) {
                return "templates/" + templateName;
            } else {
                return configurationManager.getCheminXMLGenerationMessage();
            }

        } else {
            return configurationManager.getCheminXmlGenerationMessageTexte();
        }
    }

    private String getDateFormat(String format, LocalDateTime date) {
        var formatter = DateTimeFormatter.ofPattern(format).withZone(ZoneId.of("UTC"));
        return formatter.format(date);
    }
}
