package fr.atexo.messageriesecurisee.service.referentiel;


import fr.atexo.messageriesecurisee.dto.CodeLabelDTO;

import java.util.List;

public interface ReferentielService {


    List<CodeLabelDTO> getSurchargeLibellesList();
}
