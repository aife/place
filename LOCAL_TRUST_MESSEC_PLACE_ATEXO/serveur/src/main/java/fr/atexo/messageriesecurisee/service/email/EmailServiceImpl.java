package fr.atexo.messageriesecurisee.service.email;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.*;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.mapper.DestinataireMapper;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.repository.EmailPaginableRepository;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.repository.ErreurSMTPRepository;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
@Transactional
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);
    private static final String RAW_SMTP_REPLACE_PATTERN = "[^a-zA-Z0-9]";

    private static final String RAW_SMTP_CODE = "\\d{6}.+";

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    EmailPaginableRepository emailPaginableRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    EmailMapper emailMapper;

    @Autowired
    DestinataireMapper destinataireMapper;

    @Autowired
    ErreurSMTPRepository erreurSMTPRepository;

    @Value("${nombre.destinataires.max:200}")
    private int LIMIT_DESTINATAIRES;

    @Override
    public EmailDTO relancerEmail(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException("Code lien introuvable " + codeLien));
        email.getMessage().setDateModification(LocalDateTime.now());
        email.setNombreEssais(0);
        email.setDateDemandeEnvoi(LocalDateTime.now());
        email.setDateRelance(LocalDateTime.now());
        email.setDateEnvoi(null);
        email.setStatutEmail(TypeStatutEmail.EN_ATTENTE_ENVOI);
        emailRepository.save(email);
        return this.findEmail(email.getId());
    }

    @Override
    public EmailDTO findEmail(Integer id) {
        Email email = emailRepository.findById(id).orElse(null);
        EmailDTO emailDTO = emailMapper.toDto(email);
        return emailDTO;
    }

    @Override
    public EmailDTO acquitterReponseEntreprise(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        var reponseMasquee = (email.getMessage().isReponseBloque()
                && email.getMessage().getDateLimiteReponse() != null && LocalDateTime.now().isBefore(email.getMessage().getDateLimiteReponse()));
        if (!reponseMasquee) {
            var reponsesEntreprises = email.getReponses().stream().filter(r -> r.getMessage().getInitialisateur() != TypeInitialisateur.AGENT).collect(Collectors.toList());
            for (var reponse : reponsesEntreprises) {
                LOGGER.info("Acquittement de la réponse {} pour le code lien {}", reponse.getId(), codeLien);
                reponse.setStatutEmail(TypeStatutEmail.ACQUITTE);
                if (reponse.getDateAR() == null) {
                    reponse.setDateAR(LocalDateTime.now());
                }
                reponse.getMessage().setDateModification(LocalDateTime.now());
                emailRepository.save(reponse);
            }
        } else {
            LOGGER.warn("non acquitté, réponse bloquée pour le mail : {}", email);
        }

        return this.findEmail(email.getId());
    }

    @Override
    public EmailDTO acquitter(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException("Code lien introivable :" + codeLien));
        if (email.getMessage().getTypeMessage() != TypeMessage.TypeMessage1) {
            if (email.getDateAR() == null) {
                email.setDateAR(LocalDateTime.now());
            }
            email.setStatutEmail(TypeStatutEmail.ACQUITTE);
            email.getMessage().setDateModification(LocalDateTime.now());
            emailRepository.save(email);
        } else {
            LOGGER.warn("tentative d'acquittement d'un email {}, dont le type de message {} est sans accusé de réception", email.getId(), email.getMessage().getTypeMessage());
        }

        return emailMapper.toDto(email);
    }

    @Override
    public List<EmailDTO> findEmailUpdated(String... listeCodeLien) {
        List<EmailDTO> emailDTOs = new ArrayList<>();
        if (listeCodeLien != null) {
            for (String codeLien : listeCodeLien) {
                Email emailsByCodeLien = emailRepository.findByCodeLien(codeLien).orElse(null);
                if (emailsByCodeLien != null) {
                    emailDTOs.add(emailMapper.toDto(emailsByCodeLien));
                }
            }
        }
        return emailDTOs;
    }

    @Override
    public List<MessageStatusDTO> countMessagesWaitingForAckByIds(String[] listIdsAndReferencesArray, String messagerieIdPfEmetteur) {
        List<MessageStatusDTO> messageStatusDTOList = new ArrayList<>();
        try {
            String hql = " select dest.message.identifiantObjetMetierPlateFormeEmetteur, dest.message.referenceObjetMetier, count(dest) " +
                    "  From destinataire AS dest " +
                    "  Where dest.message.identifiantPfEmetteur = :messagerieIdPfEmetteur " +
                    "  AND dest.statutEmail IN (:deststatutEmail) " +
                    "  AND concat(dest.message.identifiantObjetMetierPlateFormeEmetteur,dest.message.referenceObjetMetier ) IN (:listIdsAndReferencesString)" +
                    "  Group By dest.message.identifiantObjetMetierPlateFormeEmetteur, dest.message.referenceObjetMetier  ";

            Query query = entityManager.createQuery(hql);
            query.setParameter("messagerieIdPfEmetteur", messagerieIdPfEmetteur);
            query.setParameter("deststatutEmail", asList(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT, TypeStatutEmail.ACQUITTEMENT_PARTIEL));
            query.setParameter("listIdsAndReferencesString", asList(listIdsAndReferencesArray));
            List<?> rows = query.getResultList();
            if (rows != null) {
                for (int i = 0; i < rows.size(); i++) {
                    MessageStatusDTO msg = new MessageStatusDTO();
                    Object[] row = (Object[]) rows.get(i);
                    if (row != null && row.length == 3) {
                        msg.setIdObjetMetier((Integer) row[0]);
                        msg.setReferenceObjetMetier((String) row[1]);
                        if (row[2] != null) {
                            msg.setNombreMessageNonLu(((Long) row[2]).intValue());
                        }

                    }
                    messageStatusDTOList.add(msg);
                }
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return messageStatusDTOList;
    }

    @Override
    public List<DestinataireDTO> getDestinataires(Integer identifiantObjetMetier, String referenceObjetMetier, String identifiantPlateforme, String term) {
        var page = PageRequest.of(0, LIMIT_DESTINATAIRES, Sort.Direction.DESC, "dateEnvoi");
        Set<DestinataireDTO> destinatairesNotifies = new HashSet<>();
        var statuts = asList(TypeStatutEmail.ACQUITTE, TypeStatutEmail.ENVOYE, TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT);
        var critere = new SerializableRechercheMessage();
        critere.setIdPlateformeRecherche(identifiantPlateforme);
        critere.setIdObjetMetier(identifiantObjetMetier);
        critere.setRefObjetMetier(referenceObjetMetier);
        critere.setStatuts(statuts);
        critere.setContact(term);
        List<Email> tousLesEmails = emailPaginableRepository.rechercher(critere, page).getContent();
        if (tousLesEmails != null && !tousLesEmails.isEmpty()) {
            destinatairesNotifies = tousLesEmails.stream()
                    .map(destinataireMapper::toDto)
                    .collect(Collectors.toSet());
        }
        return new ArrayList<>(destinatairesNotifies);
    }

    @Override
    public List<EvenementDTO> getHistory(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        List<Email> listeBo = new ArrayList<>();
        AuditReader reader = AuditReaderFactory.get(entityManager);
        List<Number> revisions = reader.getRevisions(Email.class, email.getId());
        for (Number rev : revisions) {
            var emailBo = reader.find(Email.class, email.getId(), rev);
            emailBo.setMessage(email.getMessage());
            listeBo.add(emailBo);
        }
        List<EmailDTO> list = listeBo.stream().map(emailMapper::toDto).collect(Collectors.toList());
        var uniqueEvents = new HashSet<EvenementDTO>();
        if (email.getMessage().getInitialisateur() == TypeInitialisateur.ENTREPRISE) {
            list.stream().filter(EmailDTO::isEnvoye)
                    .findFirst()
                    .ifPresent(e -> uniqueEvents.add(EvenementDTO.builder()
                            .nom("initial").reponse(e).date(e.getDateEnvoiAsDate()).icone("fa-envelope").titre("Relance du message").style("info")
                            .build()));
        } else {
            list.stream().filter(EmailDTO::isEnAttenteEnvoi)
                    .findFirst()
                    .ifPresent(e -> uniqueEvents.add(EvenementDTO.builder()
                            .nom("initial").reponse(e).date(e.getDateDemandeEnvoiAsDate()).icone("fa-envelope").titre("Relance du message").style("info")
                            .build()));
        }

        for (var e : list) {
            if (e.isEnAttenteEnvoi() && e.getDateRelanceAsDate() != null) {
                uniqueEvents.add(EvenementDTO.builder().nom("relance").date(e.getDateRelanceAsDate()).icone("fa-repeat").titre("Relance du message").style("info").build());
            }
            if (e.isEchecEnvoi() && e.getDateEchec() != null) {
                uniqueEvents.add(EvenementDTO.builder().nom("echec").date(e.getDateEchecAsDate()).icone("fa-times").titre("Message non délivré").style("error").erreurSMTP(processErreur(e)).build());
            }
            if (e.isDelivre() && e.getDateEnvoiAsDate() != null) {
                uniqueEvents.add(EvenementDTO.builder().nom("delivre").date(e.getDateEnvoiAsDate()).icone("fa-check").titre("Message délivré à " + e.getEmail()).style("delivered").build());
            }
            if (e.isLu() && e.getDateARAsDate() != null) {
                uniqueEvents.add(EvenementDTO.builder().nom("lu").date(e.getDateARAsDate()).icone("fa-envelope-open").titre("Message lu par " + e.getEmail()).style("readed").build());
            }
        }
        for (var reponse : emailMapper.toDtos(email.getReponses())) {
            var titreReponse = "Réponse de " + reponse.getEmail();
            var style = "info";
            var icone = "fa-reply";
            if (reponse.getMessage().getInitialisateur() == TypeInitialisateur.AGENT) {
                titreReponse = "Acquittement de l'agent";
                style = "delivered";
                icone = "fa-check";
            }
            uniqueEvents.add(EvenementDTO.builder().nom("reponse").reponse(reponse).date(reponse.getDateEnvoiAsDate()).icone(icone).titre(titreReponse).style(style).build());
        }
        if (entityManager.isOpen()) {
            entityManager.close();
        }
        return new ArrayList<>(uniqueEvents).stream().sorted(Comparator.comparing(EvenementDTO::getDate)).collect(Collectors.toList());
    }

    private ErreurSMTP processErreur(EmailDTO e) {
        if (e.getErreur() == null || e.getErreur().isEmpty()) {
            return null;
        }
        var erreur = e.getErreur();
        var rawText = erreur.replaceAll(RAW_SMTP_REPLACE_PATTERN, "");
        var email = e.getEmail().replaceAll(RAW_SMTP_REPLACE_PATTERN, "");
        if (rawText.contains(email)) {
            var pendingText = rawText.substring(rawText.lastIndexOf(email));
            for (var err : erreurSMTPRepository.findAll(Sort.by(Sort.Direction.DESC, "code"))) {
                if (err.getCode() != null) {
                    var code = err.getCode().replaceAll(RAW_SMTP_REPLACE_PATTERN, "");
                    if (pendingText.contains(code)) {
                        return ErreurSMTP.builder().code(err.getCode()).description(err.getLabel()).erreur(erreur).build();
                    }
                }
            }
        }
        return ErreurSMTP.builder().erreur(erreur).build();
    }

    @Override
    public boolean toggleFavori(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        email.setFavori(email.getFavori() == null || !email.getFavori());
        email.getMessage().setDateModification(LocalDateTime.now());
        emailRepository.save(email);
        return email.getFavori();
    }

    @Override
    public EmailDTO findByCodeLien(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        return emailMapper.toDto(email);
    }

    @Override
    public EmailDTO updateStatut(String codeLien) {
        Email original = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        if (original.getStatutEmail().equals(TypeStatutEmail.ECHEC_ENVOI)) {
            original.setStatutEmail(TypeStatutEmail.TRAITE);
        } else if (original.getStatutEmail().equals(TypeStatutEmail.TRAITE)) {
            original.setStatutEmail(TypeStatutEmail.ECHEC_ENVOI);
        } else throw new MessageDAOException("Le statut du message ne permet pas la modification");
        return emailMapper.toDto(emailRepository.save(original));
    }

    @Override
    public EmailDTO findEmailEmailByCodeLien(String codeLien) throws MessageDAOException {
        var email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        return emailMapper.toDto(email);
    }
}
