package fr.atexo.messageriesecurisee.service.envoyeur;

import com.sun.mail.smtp.SMTPTransport;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.*;
import fr.atexo.messageriesecurisee.repository.AlerteRepository;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.notification.NotificationService;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component("emailEnvoiService")
@Slf4j
public class EmailEnvoiServiceImpl  implements EmailEnvoiService {

    public static final String SAUT_DE_LIGNE = "\n";
    private static final Pattern msgEnvoyePattern = Pattern.compile(".id=(?<token>.+)$");
    public static final String CODE_EN_COURS_ENVOI = "-1";
    private static final int CODE_RETOUR_OK = 250;
    private static final String SEPARATEUR_DESTINATAIRES_EMAIL = ";|,|\\s+";
    @Autowired
    ConfigurationManager configurationManager;
    @Value("${alerte.limit:100}")
    private int alerteLimit = 100;

    @Autowired
    MessageBuilder messageBuilder;

    @Autowired
    EnvoyeurUtils envoyeurUtils;

    @Autowired
    EntityManager entityManager;

    @Autowired
    AlerteRepository alerteRepository;

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    NotificationService notificationService;

    @Autowired
    EmailMapper emailMapper;

    @Autowired
    LoggingService loggingUtils;

    /**
     * Envoie tous les messages en attente d'envoi et met à jour les statuts
     */

    public void envoieTousMessagesEnAttente() {
        log.info("Envoi de tous les messages en attente d'envoi");
        List<Integer> emails = entityManager.createQuery("select d.id from destinataire d where d.parent is NULL AND d.statutEmail=:statutemail and d.nombreEssais <= :nombreEssais")
                .setParameter("statutemail", TypeStatutEmail.EN_ATTENTE_ENVOI)
                .setParameter("nombreEssais", configurationManager.getNombreEssaisMax())
                .getResultList();
        if (!emails.isEmpty()) {
            log.info("La file d'envoi contient {} emails", emails.size());

            emails.forEach(
                    email -> {
                        try {
                            this.envoyerEmail(email);
                        } catch(Exception e) {
                        log.error("Impossible d'envoyer l'email {}", email, e);
                        }
                    }
            );
        }
        log.info("Fin d'envoi de tous les messages en attente d'envoi");
    }

    @Transactional
    public void envoyerEmail(Integer idEmail) {
        var emailOptional = emailRepository.findById(idEmail);
        if (emailOptional.isPresent()) {
            var email = emailOptional.get();
            var plateforme = loggingUtils.getPlateforme(email);
            loggingUtils.populateLog(plateforme);
            log.info("envoi du mail à {}, id email = {}", email.getEmail(), email.getId());
            email.getMessage().setDateModification(LocalDateTime.now());
            // recupere tous les messages envoyeable
            List<String> erreurs = new ArrayList<>();
            email.setNombreEssais(email.getNombreEssais() + 1);
            try {
                String smtpCode = envoiUnEmailSMTPRaw(email, erreurs);
                if (!"-1".equalsIgnoreCase(smtpCode)) {
                    email.setStatutEmail(TypeStatutEmail.EN_COURS_ENVOI);
                    email.setIdentifiantSmtp(smtpCode);
                    email.setServeurEnvoi(configurationManager.getServeurEnvoi());
                } else {
                    email.setErreur(erreurs.stream().collect(Collectors.joining("\n")));
                }
                if (email.getNombreEssais() >= configurationManager.getNombreEssaisMax()) {
                    email.setErreur("Nombre maximal d'essais atteint.");
                    email.setStatutEmail(TypeStatutEmail.ECHEC_ENVOI);
                }
            } catch (Exception e) {
                log.error("impossible d'envoyer le mail", e);
                email.setErreur(erreurs.stream().collect(Collectors.joining("\n")));
            } finally {
                emailRepository.save(email);
            }
            loggingUtils.removeLog(plateforme);
        } else {
            log.warn("aucun email trouvé avec l'id {}", idEmail);
        }
    }

    /**
     * Verifie que les mails ont bien été envoyés par le serveur SMTP Pour ce
     * faire, lit le contenu du fichier de log postfix en cherchant pour chacun
     * l'identifiant d'envoi si l'envoi est correct, alors efface l'id postfix,
     * passe le message à envoyé, et positionne la date d'envoi sinon, repasse
     * le message à en attente d'envoi
     */
    public void verifieEnvoiParServeurPostFix() {
        var serveurEnvoi = configurationManager.getServeurEnvoi();
        log.info("Mise à jour des status des mails vs fichier de log EXIM4 depuis le serveur {}", serveurEnvoi);
        String cheminPostfix = configurationManager.getCheminLogPostFix();
        try {
            List<String> jetonsEnAttente = entityManager.createQuery("SELECT identifiantSmtp FROM destinataire Where statutEmail=:statutemail and serveurEnvoi=:serveurEnvoi").setParameter("statutemail", TypeStatutEmail.EN_COURS_ENVOI).setParameter("serveurEnvoi", serveurEnvoi)
                    .getResultList();
            if (jetonsEnAttente.isEmpty()) {
                log.debug("Aucun jeton EXIM4 trouvé");
                return;
            }
            List<JetonPostFix> jetonsPostFixs = envoyeurUtils.listeJetonLog(jetonsEnAttente, cheminPostfix);
            // traite la reception
            processEximKO(jetonsPostFixs);
            processEximOK(jetonsPostFixs);
        } catch (Exception e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        log.info("Fin de la mise à jour des statuts vs fichier EXIM4");
        loggingUtils.removeLog();
    }

    private void processEximOK(List<JetonPostFix> jetonsPostFixs) {
        Set<String> recu = jetonsPostFixs.stream().filter(j -> j.getEtat() == EtatPostFix.OK).map(JetonPostFix::getJeton).collect(Collectors.toSet());
        if (!recu.isEmpty()) {
            log.info("Traitement des emails envoyés : {}", recu.size());
            for (String identifiantSmtpRecu : recu) {
                log.debug("Traitement de l'email envoyé : {}", identifiantSmtpRecu);
                // met a jour les email ok
                // on est censé récupérer une seule ligne , par précaution on récupère une liste
                var idsEmailsOK = emailRepository.findIdsByIdentifiantSmtp(identifiantSmtpRecu);
                for (var idEmail : idsEmailsOK) {
                    processEximOK(idEmail, jetonsPostFixs);
                }
            }
        } else {
            log.debug("Aucune ligne exim en statut ENVOYE");
        }
    }

    @Transactional
    public void processEximOK(Integer idEmail, List<JetonPostFix> jetonsPostFixs) {
        var emailOpt = emailRepository.findById(idEmail);
        if (emailOpt.isPresent()) {
            var email = emailOpt.get();
            var plateforme = loggingUtils.getPlateforme(email);
            loggingUtils.populateLog(plateforme);
            if (email.getStatutEmail().ordinal() < TypeStatutEmail.ENVOYE.ordinal()) {
                email.setErreur(null);
                if (email.getMessage().getTypeMessage().ordinal() >= TypeMessage.TypeMessage2.ordinal()) {
                    email.setStatutEmail(TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT);
                } else {
                    email.setStatutEmail(TypeStatutEmail.ENVOYE);
                }
            }
            if (email.getDateEnvoi() == null) {
                Optional<JetonPostFix> jetonPostFix = jetonsPostFixs.stream().filter(j -> j.getJeton().equals(email.getIdentifiantSmtp()) && EtatPostFix.OK == j.getEtat()).findFirst();
                // parsing de la date
                if (jetonPostFix.isPresent()) {
                    JetonPostFix jeton = jetonPostFix.get();
                    email.setDateEnvoi(jeton.getDate());
                }
            }
            email.getMessage().setDateModification(LocalDateTime.now());
            emailRepository.save(email);
            loggingUtils.removeLog(plateforme);
        }
    }

    private void processEximKO(List<JetonPostFix> jetonsPostFixs) {
        Set<String> echec = jetonsPostFixs.stream().filter(j -> j.getEtat() == EtatPostFix.KO).map(JetonPostFix::getJeton).collect(Collectors.toSet());
        if (!echec.isEmpty()) {
            log.info("Traitement des emails en échec : {}", echec.size());
            for (String identifiantSmtpEchec : echec) {
                log.debug("Traitement de l'email en échec : {}", identifiantSmtpEchec);
                // met a jour les email ko
                // on est censé récupérer une seule ligne , par précaution on récupère une liste
                var idsKO = emailRepository.findIdsByIdentifiantSmtp(identifiantSmtpEchec);
                for (var idEmail : idsKO) {
                    processEximKO(idEmail, jetonsPostFixs);
                }
            }
        } else {
            log.debug("Aucune ligne exim en statut ECHECH");
        }
    }

    @Transactional
    public void processEximKO(Integer idEmail, List<JetonPostFix> jetonsPostFixs) {
        var emailOpt = emailRepository.findById(idEmail);
        if (emailOpt.isPresent()) {
            var email = emailOpt.get();
            var plateforme = loggingUtils.getPlateforme(email);
            loggingUtils.populateLog(plateforme);
            log.warn("l'email vers {}, avec l'id smtp {} est en echec", email.getEmail(), email.getIdentifiantSmtp());
            email.setStatutEmail(TypeStatutEmail.ECHEC_ENVOI);
            email.setDateEchec(LocalDateTime.now());
            var erreur = (getRaisonEchec(jetonsPostFixs, email.getIdentifiantSmtp()));
            email.setErreur(erreur);
            emailRepository.save(email);
            loggingUtils.removeLog(plateforme);
        }
    }

    private String getRaisonEchec(List<JetonPostFix> jetons, String idSmtp) {
        if (StringUtils.isEmpty(idSmtp) || jetons == null || jetons.isEmpty()) {
            return "";
        }
        for (JetonPostFix jeton : jetons) {
            if (idSmtp.equalsIgnoreCase(jeton.getJeton())) {
                return jeton.getLigne();
            }
        }
        return "";
    }

    /**
     * Envoi un email et retourne l'identifiant postfix associé si l'envoi est
     * fonction, la chaine de caractere -1 sinon
     *
     * @param email   l'email à envoyer
     * @param erreurs la list des erreurs générée part l'envoi
     * @return l'identifiant postfix associé si l'envoi est fonction, la chaine
     * de caractere -1 sinon
     * @throws ExceptionEnvoiMail
     */
    public String envoiUnEmailSMTPRaw(Email email, List<String> erreurs) throws ExceptionEnvoiMail {
        String ret = "";
        try {
            String contentHtml = construitContenu(email, true);
            ret = envoiMessage(email, contentHtml, erreurs);
        } catch (IOException e) {
            erreurs.add(e.getMessage() + SAUT_DE_LIGNE + ExceptionUtils.getStackTrace(e));
            throw new ExceptionEnvoiMail("Erreur lors de l'envoi d'email " + e.getMessage(), e);
        }
        return ret;
    }

    private String construitContenu(Email email, boolean html) {

        String result = null;
        try {
            // construit le corps text du message
            result = messageBuilder.buildContenuMessage(email.getMessage(), email, html);
        } catch (Exception e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return result;
    }

    private String envoiMessage(Email email, String contentHtml, List<String> erreurs) throws IOException {
        String result = "";
        try {
            var mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper message = hasPieceJointe(email.getMessage()) ? new MimeMessageHelper(mimeMessage, true, "utf-8") : new MimeMessageHelper(mimeMessage, "utf-8");
            var from = new InternetAddress(email.getMessage().getEmailExpediteur(), email.getMessage().getNomCompletExpediteur());
            message.setFrom(from);
            message.setTo(email.getEmail());
            message.setSubject(email.getMessage().getObjet());
            message.setText(contentHtml, true);
            if (!StringUtils.isEmpty(email.getMessage().getEmailReponseExpediteur())) {
                message.setReplyTo(email.getMessage().getEmailReponseExpediteur());
            }
            // debut de la partie message
            if (hasPieceJointe(email.getMessage())) {
                for (var pieceJointe : email.getMessage().getPiecesJointes()) {
                    FileSystemResource file = new FileSystemResource(new File(pieceJointe.getChemin()));
                    message.addAttachment(pieceJointe.getNom(), file);

                }
            }
            SMTPTransport transport = (SMTPTransport) mimeMessage.getSession().getTransport();
            if (!transport.isConnected()) {
                transport.connect();
            }
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            var returnCode = transport.getLastReturnCode();
            var serverResponse = transport.getLastServerResponse();
            var replyStrings = Optional.ofNullable(serverResponse).map(s -> Arrays.asList(s.split(SAUT_DE_LIGNE))).orElse(new ArrayList<>());
            result = traiteInfosRetour(returnCode, replyStrings, erreurs);
            transport.close();
        } catch (Exception ex) {
            log.error("impossible d'envoyer le mail pour {}", email, ex);
            erreurs.add(ex.toString());
            result = CODE_EN_COURS_ENVOI;
        }
        return result;
    }

    private String traiteInfosRetour(int coderetour, List<String> replyStrings, List<String> erreurs) {
        String result = "";
        if (coderetour == CODE_RETOUR_OK) {
            for (String retour : replyStrings) {
                Matcher matcher = msgEnvoyePattern.matcher(retour);
                if (matcher.find()) {
                    result = matcher.group("token").trim();
                }
            }
        } else {
            erreurs.addAll(replyStrings);
        }
        return result;
    }

    private boolean hasPieceJointe(Message message) {
        if (message.getPiecesJointes().isEmpty()) {
            return false;
        }
        if (message.getTypeMessage() == TypeMessage.TypeMessage3 || message.getTypeMessage() == TypeMessage.TypeMessage4) {
            return false;
        }
        return message.getPiecesJointes() != null && !message.getPiecesJointes().isEmpty();
    }

    @Override
    public void envoyerAlerte( Email emailInitial, Message reponse) {
        if (emailInitial != null && emailInitial.getMessage() != null) {

            var messageExpediteur = emailInitial.getMessage();
            var plateforme = loggingUtils.getPlateforme(messageExpediteur);
            loggingUtils.populateLog(plateforme);
            String emailsAlerteReponse = reponse.getEmailsAlerteReponse() + ";" + messageExpediteur.getEmailsAlerteReponse();
            if (TypeInitialisateur.ENTREPRISE == messageExpediteur.getInitialisateur()) {
                emailsAlerteReponse = reponse.getEmailExpediteur();
            }

            if (StringUtils.isBlank(emailsAlerteReponse)) {
                return;
            }

            var emailsAAlerter = Arrays.stream(emailsAlerteReponse.split(SEPARATEUR_DESTINATAIRES_EMAIL)).filter(StringUtils::isNotBlank).map(String::trim).collect(Collectors.toSet());
            log.info("envoi des alertes aux destinataires => {}", emailsAAlerter);

            log.info("Envoi de l'alerte du mail {} a {} destinataires", emailInitial.getId(), emailsAAlerter.size());
            String contenu;
            if (messageExpediteur.getInitialisateur().equals(TypeInitialisateur.ENTREPRISE)) {
                String objet = "Un agent a répondu à votre message - Réf : " + messageExpediteur.getReferenceObjetMetier();
                contenu = messageBuilder.buildContenuMessageAlerteTiers(emailInitial, objet);
            } else {
                contenu = messageBuilder.buildContenuMessageAlerte(emailInitial, reponse.getMetaDonnees());
            }


            emailsAAlerter.stream().forEach(email -> {
                log.debug("Alerte a l'agent ou tiers {}", email);

                var alerte = new Alerte();
                alerte.setEmail(email);
                alerte.setMessage(reponse);

                alerte.setContenu(contenu);
                alerte.setExpediteur(getExpediteurTechnique(messageExpediteur));
                if (messageExpediteur.getInitialisateur().equals(TypeInitialisateur.ENTREPRISE)) {
                    alerte.setType(TypeAlerte.REPONSE_AGENT);
                    alerte.setObjet("Un agent a répondu à votre message -Réf:" + messageExpediteur.getReferenceObjetMetier());
                } else if (messageExpediteur.getInitialisateur().equals(TypeInitialisateur.AGENT)) {
                    alerte.setType(TypeAlerte.AGENT);
                    alerte.setObjet("Réponse au message : " + messageExpediteur.getObjet());
                }

                alerte.setStatut(TypeStatutAlerte.A_ENVOYER);
                try {
                    alerteRepository.save(alerte);
                } catch (Exception e) {
                    log.error("Impossible de sauvegarder l'alerte à l'agent ou tiers {}", email, e);
                }
            });
        }
    }


    @Override
    public void envoyerAcquittementReponseEntreprise(Message reponse, Message messageInitial) {
        if (reponse != null && messageInitial != null) {
            var plateforme = loggingUtils.getPlateforme(messageInitial);
            loggingUtils.populateLog(plateforme);
            log.info("génération de l'acquittement entreprise");
        }
        String emailsAlerteReponse = reponse.getDestinataires().stream().map(Email::getEmail).collect(Collectors.joining(";"));

        if (StringUtils.isBlank(emailsAlerteReponse)) {
            return;
        }

        var emailsAAlerter = Arrays.stream(emailsAlerteReponse.split(SEPARATEUR_DESTINATAIRES_EMAIL)).filter(StringUtils::isNotBlank).map(String::trim).collect(Collectors.toSet());

        log.info("Envoi de l'acquitemment entreprises de la réponse {} a {} destinataires", reponse.getId(), emailsAAlerter.size());

        String objet = "Accusé de réception de réponse à un message - Réf : " + messageInitial.getReferenceObjetMetier();
        String contenu = messageBuilder.buildContenuMesageAcquittementEntreprise(reponse, messageInitial, objet);

        emailsAAlerter.stream().forEach(email -> {
            log.debug("Acquitemment entreprise a {}", email);

            var alerte = new Alerte();
            alerte.setEmail(email.trim());
            alerte.setMessage(reponse);
            alerte.setObjet(objet);
            alerte.setExpediteur(getExpediteurTechnique(messageInitial));
            alerte.setStatut(TypeStatutAlerte.A_ENVOYER);
            alerte.setType(TypeAlerte.ENTREPRISE);
            alerte.setContenu(contenu);
            try {
                alerteRepository.save(alerte);
            } catch (Exception e) {
                log.error("Impossible de sauvegarder l'acquittement {}", email, e);
            }
        });
        }

    @Override
    @Transactional
    public void envoyerAlertes() {
        loggingUtils.populateLog();
        log.info("Envoi des alertes");
        log.debug("Récupération de {} alertes", alerteLimit);

        var pageable = PageRequest.of(0, alerteLimit, Sort.by("dateDemandeEnvoi").descending());

        var alertes = alerteRepository.findAllIdByStatut(TypeStatutAlerte.A_ENVOYER, pageable);

        log.debug("Envoi des alertes = {}", alertes);

        if (alertes.isEmpty()) {
            log.info("Aucune alerte a envoyer");
        } else {
            log.info("{} alerte(s) en cours d'envoi", alertes.size());

            for(Long alerte:alertes) {
                try {
                    this.envoyerAlerte(alerte);
                } catch(Exception e){
                    log.error("Erreur durant l'envoi de l'alerte", e);
                }
            }
        }
        log.info("Fin de l'envoi des alertes");
        loggingUtils.removeLog();
    }

    @Transactional
    public void envoyerAlerte(Long idAlerte) {
        var alerte = alerteRepository.findById(idAlerte).orElseThrow(()->new RuntimeException("Impossible de trouver l'alerte avec l'identiifant "+idAlerte));
        var plateforme = loggingUtils.getPlateforme(alerte);
        loggingUtils.populateLog(plateforme);

        log.debug("Préparation du process d'envoi des alertes");
        var email = new Email();
        var message = new Message();
        message.setEmailExpediteur(alerte.getExpediteur());
        if (null!=alerte.getMessage()) {
            message.setNomCompletExpediteur(alerte.getMessage().getNomCompletExpediteur());
        }
        message.setObjet(alerte.getObjet());
        message.setContenu(alerte.getContenu());
        email.setMessage(message);
        email.setEmail(alerte.getEmail());
        try {
            log.info("Lancement du process d'envoi de l'alerte {}", idAlerte);
            var erreurs = new ArrayList<String>();
            var idSMTP = envoiMessage(email, email.getMessage().getContenu(), erreurs);
            if (CODE_EN_COURS_ENVOI.equalsIgnoreCase(idSMTP)) {
                alerte.setStatut(TypeStatutAlerte.ECHEC);
                alerte.setErreur(String.join("\n", erreurs));
            } else {
                alerte.setStatut(TypeStatutAlerte.ENVOYE);
                alerte.setDateEnvoi(LocalDateTime.now());
                alerte.setIdSmtp(idSMTP);
            }
        } catch (Exception e) {
            log.error("Erreur durant l'envoi de l'alerte {}", idAlerte, e);
            alerte.setStatut(TypeStatutAlerte.ECHEC);
            alerte.setErreur(e.getMessage());
        }
        finally {
            alerte.setServeurEnvoi(configurationManager.getServeurEnvoi());
            log.info("Sauvegarde du résultat");
            alerteRepository.save(alerte);
        }
        loggingUtils.removeLog(plateforme);
    }

    @Override
    public void envoiEmailsExpires() {
        // ajout de 3 jours a la date d'envoi.
        var dateExpiration = LocalDateTime.now().minusDays(3);
        var statuts = List.of(TypeStatutEmail.EN_COURS_ENVOI, TypeStatutEmail.EN_ATTENTE_ENVOI);
        List<Email> emailsPerimes = emailRepository.findByStatutEmailInAndDateDemandeEnvoiBefore(statuts, dateExpiration);
        for (Email email : emailsPerimes) {
            var plateforme = loggingUtils.getPlateforme(email);
            loggingUtils.populateLog(plateforme);
            log.info("passage de l'email préimé en statut échec id email = {}", email.getId());
            email.setStatutEmail(TypeStatutEmail.ECHEC_ENVOI);
            if (email.getErreur() == null || email.getErreur().isEmpty()) {
                email.setErreur("Impossible d'envoyer le message");
            }
            if (email.getMessage() != null) {
                email.getMessage().setDateModification(LocalDateTime.now());
            }
            emailRepository.save(email);
            loggingUtils.removeLog(plateforme);
        }
    }

    @Override
    public void ennvoyerAlerteMessageEntreprise(Message message) {
        var plateforme = loggingUtils.getPlateforme(message);
        loggingUtils.populateLog(plateforme);
        log.info("génération de l'alerte agent (initialisation entreprise)");
        String emailsAlerteReponse = message.getEmailsAlerteReponse();

        if (StringUtils.isNotBlank(emailsAlerteReponse)) {
            String[] emailsAAlerter = emailsAlerteReponse.split(SEPARATEUR_DESTINATAIRES_EMAIL);

            String objetReponseAlerte = "Un Tiers a envoyé un message – Réf : " + message.getReferenceObjetMetier();
            for (String emailAlerteReponse : emailsAAlerter) {
                if (StringUtils.isBlank(emailAlerteReponse)) {
                    continue;
                }
                var alerte = new Alerte();
                alerte.setEmail(emailAlerteReponse.trim());
                alerte.setMessage(message);
                alerte.setObjet(objetReponseAlerte);
                var expediteurTechnique = getExpediteurTechnique(message);
                alerte.setExpediteur(expediteurTechnique);
                alerte.setType(TypeAlerte.MESSAGE_ENTREPRISE);
                alerte.setStatut(TypeStatutAlerte.A_ENVOYER);
                try {
                    String contenu = messageBuilder.buildContenuMessageAlerteMessageEntreprise(message);
                    alerte.setContenu(contenu);
                    log.info("Persistance de l'acquittement avant l'envoi de l'alerte");
                    alerteRepository.save(alerte);
                } catch (Exception exceptionEnvoiMail) {
                    log.error("Impossible de générer l'alerte :", exceptionEnvoiMail);
                }
            }
        }
        loggingUtils.removeLog(plateforme);
    }

    @Override
    public void envoyerAcquittementEnvoi(Message msg) {

        var plateforme = loggingUtils.getPlateforme(msg);
        loggingUtils.populateLog(plateforme);
        log.info("génération de l'acquittement entreprise");

        String emailAlerteEnvoi = msg.getEmailExpediteur();
        if (StringUtils.isBlank(emailAlerteEnvoi)) {
            return;
        }

        log.info("Envoi de l'acquitemment entreprises d'envoi' {} a {} destinataires", msg.getId(), emailAlerteEnvoi);

        String objet = "Accusé de réception d'envoi de message - Réf : " + msg.getReferenceObjetMetier();
        String contenu = messageBuilder.buildContenuMessageAcquittementEnvoiEntreprise(msg, objet);

        log.debug("Acquittement envoi entreprise a {}", emailAlerteEnvoi);

        var alerte = new Alerte();
        alerte.setEmail(emailAlerteEnvoi.trim());
        alerte.setMessage(msg);
        alerte.setObjet(objet);
        alerte.setExpediteur(getExpediteurTechnique(msg));
        alerte.setStatut(TypeStatutAlerte.A_ENVOYER);
        alerte.setType(TypeAlerte.AR_ENVOI_ENTREPRISE);
        alerte.setContenu(contenu);
        try {
            this.alerteRepository.save(alerte);
        } catch (Exception e) {
            log.error("Impossible de sauvegarder l'acquittement {}", alerte.getEmail(), e);
        }

    }

    private String getExpediteurTechnique(Message message) {
        var expediteurTechnique = message.getEmailExpediteurTechnique();
        if (expediteurTechnique == null || expediteurTechnique.isEmpty()) {
            expediteurTechnique = message.getEmailExpediteur();
        }
        log.debug("expéditeur technique {}", expediteurTechnique);
        return expediteurTechnique;
    }
}
