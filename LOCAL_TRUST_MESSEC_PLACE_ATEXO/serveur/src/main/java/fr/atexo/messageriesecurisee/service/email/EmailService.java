package fr.atexo.messageriesecurisee.service.email;

import fr.atexo.messageriesecurisee.dto.DestinataireDTO;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.EvenementDTO;
import fr.atexo.messageriesecurisee.dto.MessageStatusDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;

import java.util.List;

public interface EmailService {

    EmailDTO relancerEmail(String codeLien) throws MessageDAOException;

    EmailDTO findEmail(Integer id);

    EmailDTO acquitterReponseEntreprise(String codeLien) throws MessageDAOException;

    EmailDTO acquitter(String codeLien) throws MessageDAOException;

    List<EmailDTO> findEmailUpdated(String... listeCodeLien);

    List<MessageStatusDTO> countMessagesWaitingForAckByIds(String[] listIdsAndReferencesArray, String messagerieIdPfEmetteur);

    List<DestinataireDTO> getDestinataires(Integer identifiantObjetMetier, String referenceObjetMetier, String identifiantPlateforme, String term);

    List<EvenementDTO> getHistory(String codeLien) throws MessageDAOException;

    boolean toggleFavori(String codeLien) throws MessageDAOException;

    EmailDTO findEmailEmailByCodeLien(String codeLien) throws MessageDAOException;

    EmailDTO findByCodeLien(String codeLien) throws MessageDAOException;

    EmailDTO updateStatut(String codeLien);
}
