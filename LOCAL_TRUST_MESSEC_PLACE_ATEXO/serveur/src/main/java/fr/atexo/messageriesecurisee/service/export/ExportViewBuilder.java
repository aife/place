package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.List;

public interface ExportViewBuilder {
    void setIdPlateform(String idPlateform);

    void generateResponseView(Message initialMessage, Email response) throws IOException, TemplateException;

    void generateHtmlMessageView(Message message);

    void generateHtmlListEmailsView(List<Email> emails);
}
