package fr.atexo.messageriesecurisee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class PaginationPage<T> {

    @JsonProperty("content")
    private final List<T> content;

    @JsonProperty("last")
    private final boolean last;

    @JsonProperty("first")
    private final boolean first;

    @JsonProperty("sort")
    private final ArrayList<PaginationSort> sort = new ArrayList<>();

    @JsonProperty("totalElements")
    private final long totalElements;

    @JsonProperty("number")
    private final int number;

    @JsonProperty("size")
    private final int size;

    @JsonProperty("totalPages")
    private final int totalPages;

    public PaginationPage(Page page) {
        this.content = page.getContent();
        this.last = page.isLast();
        this.first = page.isFirst();
        this.number = page.getNumber();
        this.size = page.getSize();
        this.totalPages = page.getTotalPages();
        //this.itemsPerPage = page.get
        this.totalElements = page.getTotalElements();
    }

    public void PaginationPage() {

    }

    public List<T> getContent() {
        return content;
    }

    public boolean isLast() {
        return last;
    }

    public boolean isFirst() {
        return first;
    }

    public ArrayList<PaginationSort> getSort() {
        return sort;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getNumber() {
        return number;
    }

    public int getSize() {
        return size;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
