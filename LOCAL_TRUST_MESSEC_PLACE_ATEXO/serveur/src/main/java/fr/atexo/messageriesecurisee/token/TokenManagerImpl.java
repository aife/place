package fr.atexo.messageriesecurisee.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.model.Token;
import fr.atexo.messageriesecurisee.repository.TokenRepository;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.random.RandomData;
import org.apache.commons.math3.random.RandomDataImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class TokenManagerImpl implements TokenManager {

	@NonNull
	private final TokenRepository tokenRepository;

	@NonNull
	private final ObjectMapper objectMapper;

	@NonNull
	private final LoggingService loggingService;

	@NonNull
	private final ConfigurationManager configurationManager;


	private static final RandomData RANDOM_DATA = new RandomDataImpl();

	/**
	 * Retourne un nouveau token sans l'enregister dans le cache des token
	 *
	 * @return un nouveau token
	 */
	public String generateToken() {
		int rnd = RANDOM_DATA.nextInt(0, Integer.MAX_VALUE);
		return String.valueOf(System.currentTimeMillis()) + rnd;
	}

	/**
	 * Enregistre les données passe en parametre dans le cache en l'associant à
	 * un token
	 *
	 * @param data les données à enregistrer dans le cache
	 * @return le token associé à ces données
	 */
	public String associateData(Object data, String identifiantPlateforme, Integer identifiantObjetMetier, String referenceObjetMetier) throws MessageDAOException {
		String token = generateToken();
		try {
			var className = data.getClass().getCanonicalName();
			var tokenBdd = Token.builder()
					.type(className)
					.token(token)
					.contenu(objectMapper.writeValueAsString(data))
					.identifiantObjetMetier(identifiantObjetMetier)
					.plateforme(identifiantPlateforme)
					.reference(referenceObjetMetier)
					.build();
			tokenRepository.save(tokenBdd);
			return token;
		} catch (Exception ex) {
			log.error("impossible de générer le token", ex);
			throw new MessageDAOException("Impossible de générer le token");
		}

	}

	/**
	 * Retourne les données associées à un token ; si l'objet correspondant
	 * implémente {@link Cloneable}, l'objet retourné est un clone de l'original
	 * stocké.
	 *
	 * @param token le token auquelle les données sont associées
	 * @return les données associées au token fourni, null si non trouvé
	 */
	public Object getData(String token) {
		var tokenBddOpt = tokenRepository.findById(token);
		if (tokenBddOpt.isEmpty()) {
			return null;
		}
		var tokenBdd = tokenBddOpt.get();

		return getData(tokenBdd);
	}

	/**
	 * Supprime le token et ses données associées du cache
	 *
	 * @param token le token associé aux données
	 */
	public void removeToken(String token) {
		tokenRepository.findById(token).ifPresent(tokenRepository::delete);
	}

	/**
	 * Met à jour les données associées à un token
	 *
	 * @param token le token clef utilisé pour re
	 * @param data  les données à mettre à jour
	 */
	public void updateData(String token, Object data) {
		var tokenBddOpt = tokenRepository.findById(token);
		if (tokenBddOpt.isPresent()) {
			var tokenBdd = tokenBddOpt.get();
			try {
				var json = objectMapper.writeValueAsString(data);
				tokenBdd.setContenu(json);
				tokenRepository.save(tokenBdd);
			} catch (Exception ex) {
				log.error("Impossible de mettre à jour la valeur pour le token {}", token, ex);
			}

		}
	}

	@Override
	public List<Token> getActiveTokenByType(Class clazz) {
		var className = clazz.getCanonicalName();
		return tokenRepository.findByType(className);
	}

	private Object getData(Token token) {
		try {
			var clazz = Class.forName(token.getType());
			var data = objectMapper.readValue(token.getContenu(), clazz);
			return data;
		} catch (Exception ex) {
			log.error("erreur lors de la récupération du token", ex);
			return null;
		}
	}

	@Override
	public void removeExpiredTokens() {

		var expirationDate = LocalDateTime.now().minus(configurationManager.getTokenExpirationDelay(), ChronoUnit.HOURS);

		log.info("nettoyage des token expirés depuis {}", expirationDate);

		for (var id : tokenRepository.findByDateCreationBefore(expirationDate)) {
			removeExpiredToken(id);
		}
	}

	public void removeExpiredToken(String id) {
		var token = tokenRepository.findById(id).orElse(null);
		if (token != null) {
			var plateforme = loggingService.getPlateforme(token);
			loggingService.populateLog(plateforme);
			log.info("suppression du token {}", token);
			tokenRepository.delete(token);
			loggingService.removeLog(plateforme);
		}

	}

}
