package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = {"/rest/tokens", "/rest/v2/tokens"})
public class TokenController {

    @NonNull
    private final TokenManager tokenManager;

    @DeleteMapping(value = "/{token}")
    public ResponseEntity<Boolean> deleteToken(@PathVariable("token") String token) {
        log.info("suppression du token {}", token);
        tokenManager.removeToken(token);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
