package fr.atexo.messageriesecurisee.service.validation;

import fi.solita.clamav.ClamAVClient;
import fr.atexo.messageriesecurisee.config.ClamavProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
@Slf4j
public class FileValidationServiceImpl implements FileValidationService {

    private final ClamavProperties clamavProperties;

    public FileValidationServiceImpl(ClamavProperties clamavProperties) {
        this.clamavProperties = clamavProperties;
    }

    @Override
    public boolean checkFile(InputStream fileToCheck) {
        if (!clamavProperties.isEnabled()) {
            return true;
        }
        try {
            String hostName = clamavProperties.getHost();
            int portNumber = clamavProperties.getPort();
            ClamAVClient cl = new ClamAVClient(hostName, portNumber, 30000);
            return ClamAVClient.isCleanReply(cl.scan(fileToCheck));
        } catch (Exception e) {
            log.error("Erreur lors de l'analyse du fichier : {}", e.getMessage());
            return true;
        }
    }

    @Override
    public boolean isAntivirusActif() {
        return clamavProperties.isEnabled();
    }

}
