package fr.atexo.messageriesecurisee.service.envoyeur;

import fr.atexo.messageriesecurisee.model.CleValeur;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;

import java.util.Set;

public interface MessageBuilder {

    String buildContenuMessage(Message message, Email email, boolean html);

    String buildContenuMessageAlerte(Email emailInitial, Set<CleValeur> metaDonnees);

    String buildContenuMesageAcquittementEntreprise(Message reponse, Message messageInitial, String objet);

    String buildContenuMessageAlerteMessageEntreprise(Message message);

    String buildContenuMessageAcquittementEnvoiEntreprise(Message message, String objet);

    String buildContenuMessageAlerteTiers(Email emailInitial, String objet);
}
