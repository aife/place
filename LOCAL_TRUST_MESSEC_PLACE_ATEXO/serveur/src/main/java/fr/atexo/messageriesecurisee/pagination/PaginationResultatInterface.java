package fr.atexo.messageriesecurisee.pagination;

import java.util.List;

/**
 * Interface pour l'objet de gestion des resultats de recherche permettant la mise en place d'un systeme de pagination
 * @author ARD
 *
 * @param <T>
 */
public interface PaginationResultatInterface<T> {
	/**
	 * Retourne le nombre de resultat de la recherche
	 * @return le nombre de resultat de la recherche
	 */
    int getNombreResultat();
	/**
	 * Defini le nombre de resultat de la recherche
	 * @param nombreResultat le nombre de resultat de la recherche
	 */
    void setNombreResultat(int nombreResultat);
	/**
	 * Retourne la liste des resultats de la recherche
	 * @return la liste des resultats de la recherche
	 */
    List<T> getResultats();
	/**
	 * Defini la liste des resultats de la recherche
	 * @param resultats la liste des resultats de la recherche
	 */
    void setResultats(List<T> resultats);

}