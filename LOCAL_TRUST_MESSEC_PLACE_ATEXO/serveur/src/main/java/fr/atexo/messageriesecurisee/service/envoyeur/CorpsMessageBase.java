package fr.atexo.messageriesecurisee.service.envoyeur;

public class CorpsMessageBase {

    private String cartouche;
    private String contenu;
    private Boolean contenuHTML;
    private String objet;
    private String logoSrc;

	public CorpsMessageBase() {
		super();
	}

	/**
	 * Retourne le contenu du message tel que saisie par l'utilisateur
	 *
	 * @return le contenu du message tel que saisie par l'utilisateur
	 */
	public String getContenu() {
		return contenu;
	}

	/**
	 * Defini le contenu du message tel que saisie par l'utilisateur
	 *
	 * @param contenu
	 *            le contenu du message tel que saisie par l'utilisateur
	 */
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	/**
	 *
	 * Sécifie si le contenu est au format HTML
	 *
	 * @return
	 */
	public Boolean getContenuHTML() {
		if (contenuHTML == null) {
			return false;
		}
		return contenuHTML;
	}

	/**
	 * @param contenuHTML
	 */
	public void setContenuHTML(Boolean contenuHTML) {
		this.contenuHTML = contenuHTML;
	}

	/**
	 * Retourne le cartouche du message
	 *
	 * @return le cartouche du message
	 */
	public String getCartouche() {
		return cartouche;
	}

	/**
	 * Defini le cartouche du message
	 *
	 * @param cartouche
	 *            le cartouche du message
	 */
	public void setCartouche(String cartouche) {
		this.cartouche = cartouche;
	}

	/**
	 * Retourne l'objet du message
	 *
	 * @return l'objet du message
	 */
	public String getObjet() {
		return objet;
	}

	/**
	 * Defini l'objet du message
	 *
	 * @param objet
	 *            l'objet du message
	 */
	public void setObjet(String objet) {
		this.objet = objet;
	}

    public String getLogoSrc() {
        return logoSrc;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }
}
