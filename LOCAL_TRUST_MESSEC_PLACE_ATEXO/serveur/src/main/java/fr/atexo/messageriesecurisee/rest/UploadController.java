package fr.atexo.messageriesecurisee.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.service.upload.UploadManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

@Controller
@RequestMapping(value = {"/rest/upload", "/rest/v2/upload"})
public class UploadController {

    final static Logger LOG = LoggerFactory.getLogger(UploadController.class);

    @Autowired
    UploadManager uploadManager;

    @Autowired
    ObjectMapper objectMapper;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest request, @RequestParam(required = false) String fileName) throws IOException {
        LOG.info("upload du fichier {}", fileName);
        String reference = null;
        Iterator<String> itr = request.getFileNames();
        while (itr.hasNext()) {
            String uploadedFile = itr.next();
            MultipartFile file = request.getFile(uploadedFile);
            if (file != null) {
                String fileName_ = fileName == null ? file.getOriginalFilename() : fileName;
                uploadManager.verifInputStream(fileName_, file.getInputStream());
                reference = uploadManager.saveFile(fileName_, file.getSize(), file.getContentType(), file.getInputStream());
            }
        }
        return new ResponseEntity<>(objectMapper.writeValueAsString(reference), HttpStatus.OK);
    }

    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/download/{reference}", method = RequestMethod.GET)
    public ResponseEntity<InputStreamResource> downloadUploadedFile(@PathVariable String reference) throws PieceJointeIntrouvableException {
        LOG.info("téléchargement du fichier {}", reference);
        var uploadFile = uploadManager.getFile(reference);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDisposition(ContentDisposition.builder("attachment; filename=\"" + uploadFile.getNom() + "\"").build());
            headers.setContentLength(uploadFile.getTaille());
            headers.setContentType(MediaType.parseMediaType(uploadFile.getMimeType()));
            headers.setContentDispositionFormData(uploadFile.getNom(), uploadFile.getNom());
            return new ResponseEntity<>(new InputStreamResource(new ByteArrayInputStream(Files.readAllBytes(Path.of(uploadFile.getChemin())))), headers, HttpStatus.OK);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return new ResponseEntity<>((InputStreamResource) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Deprecated(forRemoval = true)
    @RequestMapping(value = "/delete/{reference}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUploadedFile(@PathVariable String reference) {
        LOG.info("suppression du fichier {}", reference);
        uploadManager.removeFile(reference);
        return new ResponseEntity<>((String) null, HttpStatus.OK);
    }
}
