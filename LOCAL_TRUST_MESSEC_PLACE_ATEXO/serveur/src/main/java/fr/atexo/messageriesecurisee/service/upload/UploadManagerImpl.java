package fr.atexo.messageriesecurisee.service.upload;

import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeVeroleException;
import fr.atexo.messageriesecurisee.model.UploadFile;
import fr.atexo.messageriesecurisee.repository.UploadFileRepository;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.validation.FileValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class UploadManagerImpl implements UploadManager {


    private final ConfigurationManager configurationManager;

    private final UploadFileRepository uploadFileRepository;
    private final FileValidationService fileValidationService;

    @Override
    public UploadFile getFile(String reference) throws PieceJointeIntrouvableException {
        return uploadFileRepository.findByIdentifiant(reference).orElseThrow(() -> new PieceJointeIntrouvableException("Piece jointe introuvable " + reference));
    }

    @Override
    public String saveFile(String name, long size, String mimeType, InputStream inputStream)
            throws IOException {
        String reference = UUID.randomUUID().toString();
        var file = new File(configurationManager.getTemporaryPath() + File.separator + reference);
        Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        UploadFile uploadFile = UploadFile.builder().identifiant(reference).nom(name).taille(size).mimeType(mimeType).chemin(file.getAbsolutePath()).build();
        uploadFileRepository.save(uploadFile);
        return reference;
    }

    @Override
    public void verifInputStream(String name, InputStream inputStream) {
        boolean valid = fileValidationService.checkFile(inputStream);
        if (!valid) {
            log.error("Votre fichier {} est vérolé", name);
            throw new PieceJointeVeroleException(name);
        }
    }

    @Override
    public void removeFile(String reference) {
        uploadFileRepository.findByIdentifiant(reference).ifPresent(this::delete);
    }

    private void delete(UploadFile uploadFile) {
        log.debug("suppression du fichier temporaire {}", uploadFile);
        try {
            uploadFileRepository.delete(uploadFile);
            Files.deleteIfExists(Path.of(uploadFile.getChemin()));
        } catch (IOException e) {
            log.error("Erreur lors de la suppression du fichier temporaire {}", uploadFile, e);
        }
        uploadFileRepository.delete(uploadFile);
    }

    @Override
    public Optional<File> transferFile(String reference, File directory) throws IOException, PieceJointeIntrouvableException {
        var uploadFile = getFile(reference);
        var fichier = new File(directory.getAbsolutePath() + File.separator + uploadFile.getIdentifiant());
        try (FileOutputStream outputStream = new FileOutputStream(fichier)) {
            outputStream.write(Files.readAllBytes(Path.of(uploadFile.getChemin())));
        }
        delete(uploadFile);
        return Optional.of(fichier);
    }

    @Override
    public void removeExpiredFiles() {
        var expirationDate = LocalDateTime.now().minusHours(configurationManager.getTmpFileExpirationDelay());
        log.info("nettoyage des fichiers temporaires expirés depuis {}", expirationDate);
        var expiredFilesIds = uploadFileRepository.findByDateCreationBefore(expirationDate);
        if (expiredFilesIds.isEmpty()) {
            log.info("Aucun fichier temporaire");
        } else {
            for (var id : expiredFilesIds) {
                removeExpiredFile(id);
            }
        }
    }

    public void removeExpiredFile(Integer id) {
        var fichierUpload = uploadFileRepository.findById(id).orElse(null);
        if (fichierUpload != null && fichierUpload.getChemin() != null) {
            try {
                log.info("suppression du fichier temporaire avec l'id {}", id);
                var path = Path.of(fichierUpload.getChemin());
                if (Files.exists(path)) {
                    Files.deleteIfExists(path);
                    uploadFileRepository.delete(fichierUpload);
                } else {
                    log.warn("Le fichier temporaire {} n'existe pas", fichierUpload);
                }
            } catch (IOException e) {
                log.error("Erreur lors de la suppression du fichier temporaire {}", fichierUpload, e);
            }

        }
    }
}
