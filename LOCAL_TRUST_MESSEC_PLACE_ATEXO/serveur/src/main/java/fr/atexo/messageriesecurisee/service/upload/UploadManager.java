package fr.atexo.messageriesecurisee.service.upload;

import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.model.UploadFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public interface UploadManager {

    String saveFile(String name, long size, String mimeType, InputStream inputStream)
            throws IOException;

    UploadFile getFile(String reference) throws PieceJointeIntrouvableException;

    void verifInputStream(String name, InputStream inputStream);

    void removeFile(String reference);

    Optional<File> transferFile(String reference, File directory) throws IOException, PieceJointeIntrouvableException;

    void removeExpiredFiles();
}
