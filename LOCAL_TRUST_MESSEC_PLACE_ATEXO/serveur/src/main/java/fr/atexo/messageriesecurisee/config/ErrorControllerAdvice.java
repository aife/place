package fr.atexo.messageriesecurisee.config;

import fr.atexo.messageriesecurisee.dto.ErrorDTO;
import fr.atexo.messageriesecurisee.exceptions.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class ErrorControllerAdvice {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<String> handleException(final Exception exception) {
        log.error("handling Exception", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<String> handleApplicationTechnicalException(final ServiceException exception) {
        log.debug("handling ServiceException", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({MessageDAOException.class})
    public ResponseEntity<String> handleApplicationObjectNotFoundException(final MessageDAOException exception) {
        log.debug("handling MessageDAOException", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({PieceJointeVeroleException.class})
    public ResponseEntity<String> handlePieceJointeVeroleException(final PieceJointeVeroleException exception) {
        log.debug("handling PieceJointeIntrouvableException", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler({PieceJointeIntrouvableException.class})
    public ResponseEntity<String> handleApplicationPieceIntrouvableException(final PieceJointeIntrouvableException exception) {
        log.debug("handling PieceJointeVeroleException", exception);
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorDTO>> handleMethodArgumentNotValidException(final MethodArgumentNotValidException exception) {
        final BindingResult bindingResult = exception.getBindingResult();
        final List<ErrorDTO> result = new ArrayList<>();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();

        for (final FieldError fieldError : fieldErrors) {
            final ErrorDTO error = new ErrorDTO(HttpStatus.BAD_REQUEST.value(), fieldError.getDefaultMessage(), fieldError.getField(), fieldError.toString());
            error.setRejectedValue(fieldError.getRejectedValue());
            result.add(error);
        }
        log.error("erreur de validation {}", exception.getMessage());
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<String> handleClientError(HttpClientErrorException ex) {
        log.error("handleClientError {}", ex.getMessage());
        return new ResponseEntity<>(ex.getResponseBodyAsString(), ex.getStatusCode());
    }

    @ExceptionHandler(AtexoException.class)
    public ResponseEntity<String> handleAtexoException(AtexoException ex) {
        log.error("handleAtexoException {} : {}", ex.getMessage(), ex.getClass());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler({MissingRequestHeaderException.class})
    public ResponseEntity<String> handleResourceMissingRequestHeaderException(MissingRequestHeaderException e) {

        final String message = e.getHeaderName() + " est manquant";
        log.error("handleResourceMissingRequestHeaderException {}", message);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({UnsatisfiedServletRequestParameterException.class})
    public ResponseEntity<String> handleResourceUnsatisfiedServletRequestParameterException(UnsatisfiedServletRequestParameterException e) {

        final String message = String.join(", ", e.getParamConditions()) + " sont manquant(e)s";
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<String> handleResourceMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        String message = e.getName() + " est du mauvais type";
        Class<?> requiredType = e.getRequiredType();
        if (requiredType != null) {
            message = e.getName() + " est doit être de type " + requiredType.getName();
        }
        log.error("handleResourceMethodArgumentTypeMismatchException {}", message);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<String> handleResourceUnsatisfiedServletRequestParameterException(MissingServletRequestParameterException e) {

        final String message = format("Le paramètre '%s' est obligatoire", e.getParameterName());
        log.error("handleResourceUnsatisfiedServletRequestParameterException {}", message);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

}
