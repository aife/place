package fr.atexo.messageriesecurisee.service.notification;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    TokenManager tokenManager;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void notifier(List<EmailDTO> emails) {
        if (emails != null && !emails.isEmpty()) {
            var tokensANotifier = new HashMap<String, Set<EmailDTO>>();
            var tokens = tokenManager.getActiveTokenByType(SerializableRechercheMessage.class);
            for (var token : tokens) {
                var data = (SerializableRechercheMessage) tokenManager.getData(token.getToken());
                for (var email : emails) {
                    var message = email.getMessage();
                    if (message.getIdentifiantPfEmetteur().equals(data.getIdPlateformeRecherche()) && message.getIdentifiantObjetMetierPlateFormeEmetteur().equals(data.getIdObjetMetier())) {
                        var emailsSet = tokensANotifier.get(token.getToken());
                        if (emailsSet == null) {
                            emailsSet = new HashSet<>();
                            tokensANotifier.put(token.getToken(), emailsSet);
                        }
                        tokensANotifier.get(token).add(email);
                    }
                }
            }
            if (!tokensANotifier.isEmpty()) {
                tokensANotifier.forEach((k, v) -> {
                    var url = "/topic/suivi/" + k;
                    log.info("notification de l'url {}", url);
                    simpMessagingTemplate.convertAndSend(url, new ArrayList<>(v));
                });
            }
        }
    }
}
