package fr.atexo.messageriesecurisee.service.message;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.*;
import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.*;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfDestinataire;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfEmetteur;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.DestinatairesPfEmetteur.DestinatairePfEmetteur;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.TypesInterdits;
import fr.atexo.messageriesecurisee.messages.envoi.ObjectFactory;
import fr.atexo.messageriesecurisee.model.*;
import fr.atexo.messageriesecurisee.repository.*;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.envoyeur.EmailEnvoiService;
import fr.atexo.messageriesecurisee.service.upload.UploadManager;
import fr.atexo.messageriesecurisee.service.util.GenerateurCodeLien;
import fr.atexo.messageriesecurisee.service.util.XmlExtractedValuesUtils;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.util.ConstantServletParameters;
import fr.atexo.messageriesecurisee.util.ConversionTypeDestinataire;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.utils.DateUtils.toLocalDateTime;
import static org.codehaus.groovy.runtime.InvokerHelper.asList;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);
    @Autowired
    UploadManager uploadManager;

    @Autowired
    ConfigurationManager configurationManager;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    MessagePaginableRepository messagePaginableRepository;

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    PieceJointeRepository pieceJointeRepository;

    @Autowired
    CritereRepository critereRepository;

    @Autowired
    CleValeurRepository cleValeurRepository;

    @Autowired
    DossierVolumineuxRepository dossierVolumineuxRepository;


    @Autowired
    EmailEnvoiService emailEnvoiService;

    @Autowired
    EntityManager entityManager;

    @Autowired
    TokenManager tokenManager;

    @Autowired
    MessageFullMapper messageFullMapper;

    @Autowired
    MessageMapper messageMapper;

    @Autowired
    PieceJointeMapper pieceJointMapper;

    @Autowired
    EmailMapper emailMapper;

    @Autowired
    DestinataireMapper destinataireMapper;

    @Autowired
    DossierVolumineuxMapper dossierVolumineuxMapper;

    @Autowired
    EmailPaginableRepository emailPaginableRepository;


    @Value("${max.caracteres.contenu}")
    Long maxCaracteresContenu;

    @Override
    public PieceJointeDTO searchPJByCodeLienAndCodePj(String codeLien, String codePJ) throws MessageDAOException {
        try {
            return pieceJointMapper.toDto(fetchPJ(codeLien, codePJ));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw new MessageDAOException(ex);
        }
    }

    private PieceJointe fetchPJ(String codeLien, String codePJ) {
        return pieceJointeRepository.findById(Integer.valueOf(codePJ)).orElse(null);
    }

    @Override
    @Transactional
    public MessageDTO saveOrCreateMessage(Message msg, Email emailInitial, boolean brouillon) throws MessageDAOException {
        Message message = persist(msg, emailInitial, brouillon);
        messageRepository.saveAndFlush(message);
        //cas réponse
        if (emailInitial != null && !brouillon) {
            Message messageInitial = emailInitial.getMessage();
            if (messageInitial != null && messageInitial.isReponseAttendue()) {

                try {
                    //Envoi AR de réponse à celui qui a écrit la réponse
                    emailEnvoiService.envoyerAcquittementReponseEntreprise(msg, messageInitial);
                } catch(Exception e) {
                    LOGGER.error("Impossible d'envoyer l'acquitement entreprise pour le mail {}", msg.getId(), e);
                }

                try {
                    //Envoi alerte à celui qui reçoit la réponse
                    emailEnvoiService.envoyerAlerte(emailInitial, message);
                } catch(Exception e) {
                    LOGGER.error("Impossible d'envoyer les alertes aux agents pour le mail {}", msg.getId(), e);
                }

            }

        }
        //cas envoi initial tiers à agent
        if (emailInitial == null && !brouillon && msg.getInitialisateur() == TypeInitialisateur.ENTREPRISE && StringUtils.isNotBlank(msg.getEmailsAlerteReponse())) {
            //envoi alerte à l'agent qui reçoit le message
            emailEnvoiService.ennvoyerAlerteMessageEntreprise(msg);
            try {
                //envoi AR d'envoi au tiers qui a écrit le message
                emailEnvoiService.envoyerAcquittementEnvoi(msg);
            } catch(Exception e) {
                LOGGER.error("Impossible d'envoyer l'acquittement d'envoi entreprise pour le message {}", msg.getId(), e);
            }
        }


        return messageMapper.toDto(message);
    }

    private Message persist(Message message, Email parent, boolean brouillon) throws MessageDAOException {

        Collection<Email> destinataires = message.getDestinataires();
        List<Critere> criteres = message.getCriteres();
        List<PieceJointe> piecesJointes = message.getPiecesJointes();

        try {
            // positionne les valeurs par defaut
            var time = LocalDateTime.now();
            if (destinataires != null) {
                for (Email d : destinataires) {
                    d.setParent(parent);
                    // fixme pourquoi ? d.setTypeDestinataire(TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE);
                    TypeStatutEmail statutEmail = null;
                    if (brouillon) {
                        statutEmail = TypeStatutEmail.BROUILLON;
                    } else {
                        if (parent != null || message.getInitialisateur() == TypeInitialisateur.ENTREPRISE) { // dans le cas de réponse ou message entreprise on envoie pas d'email -> envoyé
                            statutEmail = TypeStatutEmail.ENVOYE;
                            d.setDateEnvoi(time);
                        } else {
                            statutEmail = TypeStatutEmail.EN_ATTENTE_ENVOI;
                        }
                    }

                    d.setStatutEmail(statutEmail);
                    d.setMessage(message);
                    if (brouillon) {
                        d.setDateModificationBrouillon(time);
                    }
                    d.setDateDemandeEnvoi(time);
                    if (parent != null) { // dans le cas de réponse  on envoie pas d'email -> on sette la date d'envoi
                        d.setDateEnvoi(time);
                    }
                    if (d.getCodeLien() == null) {
                        d.setCodeLien(GenerateurCodeLien.generateCodeLien(message, d));
                    }

                    emailRepository.save(d);
                }
            }
            message.getMetaDonnees().forEach(v -> v.setMessage(message));

            if (criteres != null) {
                for (Critere c : criteres) {
                    c.setMessage(message);
                    critereRepository.save(c);
                }
            }

            if (piecesJointes != null) {
                for (PieceJointe p : piecesJointes) {
                    p.setMessage(message);
                    File fichier = new File(p.getChemin());
                    if (fichier.exists()) {
                        pieceJointeRepository.save(p);
                    } else {
                        throw new MessageDAOException("Le fichier n'existe pas :" + p.getNom());
                    }
                }
            }
            message.setDateModification(LocalDateTime.now());
            message.setBrouillon(brouillon);
            messageRepository.save(message);
        } catch (Exception e) {
            if (e instanceof MessageDAOException) {
                throw (MessageDAOException) e;
            } else {
                throw new MessageDAOException(e);
            }
        }
        return message;
    }

    @Override
    public EmailDTO searchEmailByCodeLien(String codeLien) throws MessageDAOException {
        EmailDTO result = null;
        try {
            var email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
            result = emailMapper.toDto(email);
        } catch (IllegalArgumentException ex) {
            LOGGER.info("Il n'existe pas de message correspondant au code lien: " + codeLien);
            LOGGER.debug(ex.getMessage());
            LOGGER.trace(ex.getMessage(), ex.fillInStackTrace());
            throw new MessageDAOException(ex);
        } catch (Exception ex) {

            LOGGER.debug(ex.getMessage(), ex);
            throw new MessageDAOException(ex);
        }
        return result;
    }

    @Override
    public Message getMessageByCodeLien(String codeLien) throws MessageDAOException {
        Message message = emailRepository.findByCodeLien(codeLien).map(Email::getMessage).orElseThrow(() -> new MessageDAOException("Code lien introuvable"));
        return message;
    }

    @Override
    public boolean acquitteEmail(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException(MessageDAOException.CODE_LIEN_INTROUVABLE + codeLien));
        if (email.getDateEnvoi() == null) {
            email.setDateEnvoi(LocalDateTime.now());
        }
        if (email.getStatutEmail() != TypeStatutEmail.ACQUITTE) {
            updateStatut(email, TypeStatutEmail.ACQUITTE);
        }
        return true;
    }

    private Email updateStatut(Email email, TypeStatutEmail statut) {
        if (email != null) {
            email.setStatutEmail(statut);
            if (statut == TypeStatutEmail.ACQUITTE) {
                email.setDateAR(LocalDateTime.now());
            }
            email.getMessage().setDateModification(LocalDateTime.now());
            email = emailRepository.save(email);
        }
        return email;
    }

    @Override
    public Optional<EmailDTO> fetchOne(SerializableRechercheMessage critereRechercheMessage) throws MessageDAOException {
        List<Email> emails = emailPaginableRepository.rechercher(critereRechercheMessage);

        if (emails.size() > 1) {
            throw new MessageDAOException("Plusieurs emails pour le jeton " + critereRechercheMessage.getToken());
        }

        return emails.stream().findFirst().map(emailMapper::toDto);
    }

    @Override
    public List<EmailDTO> fetch(SerializableRechercheMessage critereRechercheMessage) {
        List<EmailDTO> result;
        result = emailPaginableRepository.rechercher(critereRechercheMessage).stream().map(emailMapper::toDto).collect(Collectors.toList());
        return result;
    }

    @Override
    public Page<EmailDTO> search(SerializableRechercheMessage critereRechercheMessage, Pageable pageable) throws MessageDAOException {
        try {
            var result = emailPaginableRepository.rechercher(critereRechercheMessage, pageable);
            var emails = emailMapper.toDtos(result.getContent());
            return new PageImpl<>(emails, pageable, result.getTotalElements());
        } catch (Exception ex) {
            throw new MessageDAOException(ex);
        }
    }


    @Override
    @Transactional
    public MessageDTO initialiseMessage(String token) throws MessageDAOException {
        MessageDTO dto = null;
        // retrouve le message par le bias du contenu de la fifo
        Message message = (Message) tokenManager.getData(token);
        boolean brouillon = false;
        if (message != null && message.isBrouillon()) {
            brouillon = true;
            message = emailRepository.findByCodeLien(message.getCodeLien()).map(Email::getMessage).orElseThrow(() -> new MessageDAOException("Code lien introuvable"));
        }
        if (message != null) {
            dto = messageFullMapper.toFull(message);
            if (brouillon) {
                processBrouillon(token, dto, message);
            } else {
                // pour un nouveau message, il faut réinitialiser les destinataires stockés en session
                message = messageFullMapper.toBo(dto);
                message.getDestinataires().clear();
            }
            dto.setPiecesJointes(pieceJointMapper.toDtos(message.getPiecesJointes()));
            if (message.getIdentifiantPfEmetteur() != null && message.getIdentifiantPfEmetteur().toUpperCase().startsWith("RSEM")) {
                dto.setDestinatairesNotifies(getDestinatairesNotifies(token));
            }

            dto.setMaxCaracteresContenu(maxCaracteresContenu);
        } else {
            return null;
        }
        return dto; //called
    }

    private void processBrouillon(String token, MessageDTO dto, Message message) {
        // récupérer les destinataires du token
        Message messageByToken = ((Message) tokenManager.getData(token));
        message.setCodeLien(messageByToken.getCodeLien());
        Set<Email> allDestinataires = new HashSet<>(messageByToken.getDestinataires());
        allDestinataires.addAll(message.getDestinatairesPlateFormeDestinataire());

        var destinataires = allDestinataires.stream().map(destinataireMapper::toDto).collect(Collectors.toList());

        if (null == dto.getDestsPfDestinataire() || dto.getDestsPfDestinataire().isEmpty()) {
            LOGGER.info("La liste des destinataires du message est vide. Ajout de {} destinataires supplémentaires", destinataires.size());

            dto.setDestsPfDestinataire(destinataires);
        } else {
            LOGGER.info("{} destinataire(s) déjà existant(s) pour le message. Ajout de {} destinataires supplémentaires",  dto.getDestsPfDestinataire().size(), destinataires.size());

            dto.getDestsPfDestinataire().addAll(destinataires);
        }

        dto.getDestinatairesPreSelectionnes().addAll(message.getDestinatairesPlateFormeDestinataire().stream().map(destinataireMapper::toDto).collect(Collectors.toSet()));

       // on associe le message récupéré au meme token
        tokenManager.updateData(token, message);
    }

    @Override
    public void transformeMessageInitialisationEnMessage(MessageSecuriseInit messageInit, Message message,
                                                         String encodingSource) {
        EmailDTO original = null;
        // si la référence d'objet métier correspond à un code
        // lien, il s'agira d'une réponse et un traitement spécial doit être
        // déclenché
        try {
            LOGGER.info("Essaye de recuperer si reponse");
            original = recupereMessageInitialSiReponse(messageInit.getRefObjetMetier());
        } catch (Throwable e) {
            LOGGER.warn(e.getMessage());
            original = null;
            LOGGER.info("Pas de message original recupere");
        }

        if (original != null) {
            // Si c'est une réponse, le message d'initialisation doit être
            // complété du destinataire
            creeDestinataireSiReponse(original, messageInit);
            // c'est une code lien : il s'agit donc d'une réponse à un mail
            completeMessageInitialisationSiReponse(original, messageInit);
        }
        // Dans tous les cas on transfert les données d'initialisation dans le
        // message
        XmlExtractedValuesUtils.convertir(message, messageInit, configurationManager.getTailleMaxTousfichiers());
        if (original != null) {
            // Et s'il s'agit d'une réponse on trasfert les dernières données
            // dans
            // le message
            completeMessageSiReponse(original, message);
        }
    }

    private EmailDTO recupereMessageInitialSiReponse(String code) {
        EmailDTO result = null;
        try {
            result = searchEmailByCodeLien(code);
        } catch (MessageDAOException e) {
            LOGGER.warn("Cette initialisation n'est pas celle d'une réponse : " + e.getMessage());
            return null;
        }
        return result;
    }

    private void creeDestinataireSiReponse(EmailDTO emailDto, MessageSecuriseInit messageInit) {
        if (emailDto != null && emailDto.getTypeDestinataire() != null) {
            switch (emailDto.getTypeDestinataire()) {
                case DESTINATAIRE_PLATEFORME_DESTINATAIRE:
                    DestinatairesPfDestinataire destinatairesPfDestinataire = messageInit.getDestinatairesPfDestinataire();
                    if (destinatairesPfDestinataire == null) {
                        destinatairesPfDestinataire = new DestinatairesPfDestinataire();
                    }
                    List<DestinatairePfDestinataire> listDestinatairePfDestinataires = destinatairesPfDestinataire
                            .getDestinatairePfDestinataire();
                    if (listDestinatairePfDestinataires == null) {
                        listDestinatairePfDestinataires = new ArrayList<MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire>();
                    }
                    DestinatairePfDestinataire destinatairePfDestinataire = new DestinatairePfDestinataire();
                    destinatairePfDestinataire.setMailContactDestinataire(emailDto.getMessage().getEmailReponseExpediteur());
                    destinatairePfDestinataire.setNomContactDest(emailDto.getMessage().getNomCompletExpediteur());
                    listDestinatairePfDestinataires.add(destinatairePfDestinataire);
                    break;
                case DESTINATAIRE_PLATEFORME_EMETTRICE:
                    DestinatairesPfEmetteur destinatairesPfEmetteur = messageInit.getDestinatairesPfEmetteur();
                    if (destinatairesPfEmetteur == null) {
                        destinatairesPfEmetteur = new DestinatairesPfEmetteur();
                    }
                    List<DestinatairePfEmetteur> listDestinatairePfEmetteurs = destinatairesPfEmetteur.getDestinatairePfEmetteur();
                    if (listDestinatairePfEmetteurs == null) {
                        listDestinatairePfEmetteurs = new ArrayList<MessageSecuriseInit.DestinatairesPfEmetteur.DestinatairePfEmetteur>();
                    }
                    DestinatairePfEmetteur destinatairePfEmetteur = new DestinatairePfEmetteur();
                    destinatairePfEmetteur.setMailContactDestinataire(emailDto.getMessage().getEmailReponseExpediteur());
                    destinatairePfEmetteur.setNomContactDest(emailDto.getMessage().getNomCompletExpediteur());
                    listDestinatairePfEmetteurs.add(destinatairePfEmetteur);
                    break;
            }
        }
    }

    /**
     * Afin de retourner aussi les réponses aux mails lors des recherches, on
     * remplit ce qui est inutile de d'envoyer pour l'initialisation d'un
     * message de réponse, c'est à dire les identifiants objet métier pour les
     * plateformes, la référence objet métier originale, les critères libres,
     * l'objet (débutant par "Re :". S'y ajoutent : le type de message de la
     * réponse (il est spécifié que c'est toujours un type 3), la réponse
     * attendue à <code>false</code> et l'id du message initial.
     *
     * @param emailDto
     * @param result
     */
    private void completeMessageInitialisationSiReponse(EmailDTO emailDto, MessageSecuriseInit result) {
        if (emailDto != null) {
            ObjectFactory factory = new ObjectFactory();
            LOGGER.info("Cas de la rédaction d'un réposne : ajout des compléments d'information.");
            MessageDTO dto = emailDto.getMessage();
            // inversion des plateformes
            result.setIdObjetMetierPfDestinataire(dto.getIdentifiantObjetMetierPlateFormeEmetteur());
            result.setNomPfDestinataire(dto.getNomPfEmetteur());
            result.setUrlPfDestinataire(dto.getUrlPfEmetteur());
            result.setIdPfDestinataire(dto.getIdentifiantPfEmetteur());
            result.setUrlPfDestinataireVisualisation(dto.getUrlPfEmetteurVisualisation());
            result.setUrlPfEmetteurVisualisation(dto.getUrlPfDestinataireVisualisation());

            result.setRefObjetMetier(dto.getReferenceObjetMetier());
            for (CritereDTO critDto : dto.getCriteres()) {
                fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit.Criteres.Critere critere = factory
                        .createMessageSecuriseInitCriteresCritere();
                critere.setNom(critDto.getNom());
                critere.setValeur(critDto.getValeur());
            }
            result.setTypeMessageDefaut("3");
            TypesInterdits typesInterdits = factory.createMessageSecuriseInitTypesInterdits();
            typesInterdits.getTypeInterdit().add(1);
            typesInterdits.getTypeInterdit().add(2);
            typesInterdits.getTypeInterdit().add(4);
            result.setTypesInterdits(typesInterdits);
        }
    }

    private void completeMessageSiReponse(EmailDTO dto, Message message) {
        if (dto != null) {
            message.setDateLimiteReponseAttendue(false);
            message.setObjet(ConstantServletParameters.PREFIX_RE.concat(dto.getMessage().getObjet()));
            message.setCartouche(dto.getMessage().getCartouche());
            message.setPiecesJointes(new ArrayList<PieceJointe>());
            //Setter les parametre du message de reponse
            message.setDateLimiteReponse(toLocalDateTime(dto.getMessage().getDateLimiteReponseAsDate()));
            message.setReponseBloque(dto.getMessage().isReponseBloque());
            message.setDateLimiteReponseAttendue(dto.getMessage().isDateLimiteReponseAttendue());
            if (dto.getMessage().isDateReponseAttendue()) {

                if (dto.getMessage().getDateReponse() != null) {
                    try {
                        message.setDateReponse(LocalDateTime.parse(dto.getMessage().getDateReponse(), formatter));
                    } catch (Exception e) {
                        LOGGER.warn("Erreur de parsing de la date de reponse");
                    }
                }
                if (dto.getMessage().getDateReponseAsDate() != null) {
                    message.setDateReponse(toLocalDateTime(dto.getMessage().getDateReponseAsDate()));
                }

            } else {
                message.setDateReponse(null);
            }
        }
    }

    /**
     * Remplace la liste des destinataires constituée au moment de
     * l'initialisation du message par ceux passés en paramètre.
     *
     * @param destPfDestinataire la liste finale des destinataires.
     * @param destPfEmetteur     la liste finale des destinataires (PF emetteur).
     * @param msg
     */

    @Override
    public void integreLesDestinataires(List<DestinataireDTO> destPfDestinataire, List<DestinataireDTO> destPfEmetteur, Message msg) throws ServiceException {
        if ((destPfDestinataire == null || destPfDestinataire.isEmpty()) && (destPfEmetteur == null || destPfEmetteur.isEmpty())) {
            throw new ServiceException("La liste de destinataires est vide.");
        }
        Set<DestinataireDTO> allDestinataires = new HashSet<>();
        if (null != destPfDestinataire && !destPfDestinataire.isEmpty()) {
            allDestinataires.addAll(destPfDestinataire);
        }
        if (null != destPfEmetteur && !destPfEmetteur.isEmpty()) {
            allDestinataires.addAll(destPfEmetteur);
        }
        processList(allDestinataires, TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE, msg);
    }

    /**
     * Remplace la liste des critères constituée au moment de l'initialisation
     * du message par ceux passés en paramètre.
     *
     * @param listeCritSup
     * @param msg
     */

    @Override
    @Transactional
    public void integreLesCriteres(HashMap<String, String> listeCritSup, Message msg) {
        List<Critere> listeInitiale = msg.getCriteres();
        listeInitiale.clear();
        if (listeCritSup != null) {
            for (Entry<String, String> nvlleComposantesCritere : listeCritSup.entrySet()) {
                Critere nvCritere = new Critere();
                nvCritere.setNom(nvlleComposantesCritere.getKey());
                nvCritere.setValeur(nvlleComposantesCritere.getValue());
                // Référencement bidirectionnel
                nvCritere.setMessage(msg);
                listeInitiale.add(nvCritere);
            }
        }
    }

    private void processList(Set<DestinataireDTO> destinataires, TypeDestinataire typeDestinataire, Message message) {
        // listes finale des adresses emails
        Set<String> uniqueEmails = destinataires.stream().map(dst -> dst.getMailContactDestinataire().toLowerCase()).collect(Collectors.toSet());
        List<DestinataireDTO> finalList = new ArrayList<>();
        if (!uniqueEmails.isEmpty()) {
            finalList.addAll(uniqueEmails.stream().map(ue -> destinataires.stream().filter(d -> d.getMailContactDestinataire().equalsIgnoreCase(ue)).findFirst().orElse(null)).collect(Collectors.toList()));
        }
        for (final Email email : message.getDestinataires()) {
            var id = email.getId();
            if (id != null) {
                emailRepository.findById(id).ifPresent(emailRepository::delete);
            }
        }
        if (message.getDestinataires() != null) {
            message.getDestinataires().clear();
        }
        for (DestinataireDTO nvDest : finalList) {
            Email nvEmail = message.getDestinataires().stream()
                    .filter(e -> nvDest.getMailContactDestinataire().equalsIgnoreCase(e.getEmail()))
                    .findFirst().orElse(new Email());
            nvEmail.setIdentifiantEntreprise(nvDest.getIdEntrepriseDest());
            nvEmail.setNomEntreprise(nvDest.getNomEntrepriseDest());
            nvEmail.setIdentifiantContact(nvDest.getIdContactDest());
            nvEmail.setNomContact(nvDest.getNomContactDest());
            nvEmail.setTypeDestinataire(Optional.ofNullable(ConversionTypeDestinataire.convert(nvDest.getTypeDestinataire())).orElse(typeDestinataire));
            nvEmail.setEmail(nvDest.getMailContactDestinataire());
            nvEmail.setType(nvDest.getType());
            // Référencement bidirectionnel en cas de nouvel email
            if (nvEmail.getId() == null) {
                nvEmail.setMessage(message);
                message.getDestinataires().add(nvEmail);
            }
        }
    }

    /**
     * Deplace les pieces jointes à sauvegarder du repertoire temporaire vers
     *
     * @param msg le message qui sera par la suite sauvegarde
     */

    @Override
    public void persisteUploadedfiles(Message msg) throws IOException, PieceJointeIntrouvableException {
        if (msg.getPiecesJointes() != null && msg.getPiecesJointes().size() > 0) {
            for (PieceJointe p : msg.getPiecesJointes()) {
                File f = new File(p.getChemin());
                if (f.exists()) {
                    File newFile = new File(generateRepertoryFile(msg) + File.separator + f.getName());
                    FileUtils.copyFile(f, newFile);
                    FileUtils.deleteQuietly(f);
                    p.setChemin(newFile.getAbsolutePath());
                } else {
                    throw new PieceJointeIntrouvableException(p.getNom());
                }
            }
        }
    }

    private String generateRepertoryFile(Message msg) {
        var reggex = "[+^:,/><]";
        var objetMetier = String.valueOf(msg.getIdentifiantObjetMetierPlateFormeEmetteur());
        if (msg.getReferenceObjetMetier() != null) {
            objetMetier = msg.getReferenceObjetMetier();
        }
        var year = String.valueOf(Year.now().getValue());
        var path = Paths.get(configurationManager.getRepertoireTelechargement(), msg.getIdentifiantPfEmetteur().replaceAll(reggex, ""), year.replaceAll(reggex, ""), objetMetier.replaceAll(reggex, ""));
        path.toFile().mkdirs();
        return String.format(path.toString());
    }

    @Override
    public void persisteUploadedfiles(Message msg, MessageDTO messageDTO) throws IOException, PieceJointeIntrouvableException {

        // on supprime les Pièces jointes supprimes

        // ids des pièces jointes déjà enregistrées
        List<Integer> ids = messageDTO.getPiecesJointes().stream().filter(pieceJointeDTO -> (pieceJointeDTO.getId() != null))
                .map(pieceJointeDTO -> pieceJointeDTO.getId()).collect(Collectors.toList());

        // listes des pièces jointes à supprimer
        List<PieceJointe> piecesJointesASupprimer = msg.getPiecesJointes().stream()
                .filter(pieceJointe -> !ids.contains(pieceJointe.getId())).collect(Collectors.toList());

        piecesJointesASupprimer.forEach(pj -> {
            msg.getPiecesJointes().remove(pj);
        });

        // nouvelles pièces jointes
        if (messageDTO.getPiecesJointes() != null) {

            messageDTO.getPiecesJointes().stream().filter(pj -> pj.getId() == null && pj.getReference() != null).forEach(pj -> {

                UploadFile uploadFile = null;
                try {
                    uploadFile = uploadManager.getFile(pj.getReference());
                    PieceJointe pieceJointe = new PieceJointe();
                    pieceJointe.setIdExterne(uploadFile.getIdentifiant());
                    pieceJointe.setNom(uploadFile.getNom());
                    pieceJointe.setTaille((int) uploadFile.getTaille());
                    pieceJointe.setContentType(uploadFile.getMimeType());
                    pieceJointe.setDateCreation(LocalDateTime.now());

                    Optional<File> transferedFileOpt = null;

                    try {
                        transferedFileOpt = uploadManager.transferFile(pj.getReference(),
                                new File(generateRepertoryFile(msg)));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                    if (transferedFileOpt.isPresent()) {
                        pieceJointe.setChemin(transferedFileOpt.get().getAbsolutePath());
                        msg.getPiecesJointes().add(pieceJointe);
                    }
                } catch (PieceJointeIntrouvableException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    @Override
    public void deleteMessage(String codeLien) throws MessageDAOException {
        Email email = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException("Code lien introuvable"));
        var message = email.getMessage();
        message.getDestinataires().remove(email);
        message.setDateModification(LocalDateTime.now());
        messageRepository.save(message);
    }

    @Override
    public void deleteMessageById(Integer id) throws MessageDAOException {
        var message = messageRepository.findById(id).orElseThrow(() -> new MessageDAOException("Message introuvable :" + id));
        emailRepository.deleteAll(message.getDestinataires());
        messageRepository.delete(message);
    }

    @Override
    public void reintroductionDuFiltreDefiniParPlateforme(SerializableRechercheMessage champsRecherches,
                                                          SerializableRechercheMessage filtre) {

        LOGGER.info("Application du filtre de recherche défini par la plateforme");

        // ----------------------------------------
        // Filtres définis par les SF v0.7
        // ----------------------------------------
        if (filtre.getTypePlateformeRecherche() != null) {
            champsRecherches.setTypePlateformeRecherche(filtre.getTypePlateformeRecherche());
        }
        if (filtre.getIdPlateformeRecherche() != null) {
            champsRecherches.setIdPlateformeRecherche(filtre.getIdPlateformeRecherche());
        }
        if (filtre.getIdObjetMetier() != null) {
            champsRecherches.setIdObjetMetier(filtre.getIdObjetMetier());
        }
        if (filtre.getIdSousObjetMetier() != null) {
            champsRecherches.setIdSousObjetMetier(filtre.getIdSousObjetMetier());
        }
        if (filtre.getIdContactDestList() != null) {
            champsRecherches.setIdContactDestList(filtre.getIdContactDestList());
        }
        if (filtre.getIdEntrepriseDest() != null && !"".equals(filtre.getIdEntrepriseDest())) {
            champsRecherches.setIdEntrepriseDest(filtre.getIdEntrepriseDest());
        }
        if (filtre.getMailDestinataire() != null && !"".equals(filtre.getMailDestinataire())) {
            champsRecherches.setMailDestinataire(filtre.getMailDestinataire());
        }

        if (filtre.getMailExpediteur() != null && !"".equals(filtre.getMailExpediteur())) {
            champsRecherches.setMailExpediteur(filtre.getMailExpediteur());
        }

        if (filtre.getReponseAttendue() != null) {
            champsRecherches.setReponseAttendue(filtre.getReponseAttendue());
        }
        // ----------------------------------------
        // Filtres définis à partir des critères de recherche définis par
        // l'utilisateur (cf SF v0.7)
        // ----------------------------------------
        if (filtre.getRefObjetMetier() != null && !"".equals(filtre.getRefObjetMetier())) {
            champsRecherches.setRefObjetMetier(filtre.getRefObjetMetier());
        }
        if (filtre.getObjetMessage() != null && !"".equals(filtre.getObjetMessage())) {
            champsRecherches.setObjetMessage(filtre.getObjetMessage());
        }
        if (filtre.getNomOuMailDestList() != null) {
            champsRecherches.setNomOuMailDestList(filtre.getNomOuMailDestList());
        }
        if (filtre.getDateEnvoiDebut() != null) {
            champsRecherches.setDateEnvoiDebut(filtre.getDateEnvoiDebut());
        }
        if (filtre.getDateEnvoiFin() != null) {
            champsRecherches.setDateEnvoiFin(filtre.getDateEnvoiFin());
        }
        if (filtre.getDateARDebut() != null) {
            champsRecherches.setDateARDebut(filtre.getDateARDebut());
        }
        if (filtre.getDateARFin() != null) {
            champsRecherches.setDateARFin(filtre.getDateARFin());
        }
        if (filtre.getStatuts() != null) {
            champsRecherches.setStatuts(filtre.getStatuts());
        }
        if (filtre.getCriteres() != null) {
            champsRecherches.setCriteres(filtre.getCriteres());
        }
        // ----------------------------------------
        // Filtres définis à partir des critères de recherche supplémentaires
        // existants dans l'objet SerializableRechercheMessage
        // ----------------------------------------
        if (filtre.getReponseAttendue() != null) {
            champsRecherches.setReponseAttendue(filtre.getReponseAttendue());
        }
        if (filtre.getTypeMessage() != null && !"".equals(filtre.getTypeMessage())) {
            champsRecherches.setTypeMessage(filtre.getTypeMessage());
        }
        if (filtre.getDateDemandeEnvoiDebut() != null) {
            champsRecherches.setDateDemandeEnvoiDebut(filtre.getDateDemandeEnvoiDebut());
        }
        if (filtre.getDateDemandeEnvoiFin() != null) {
            champsRecherches.setDateDemandeEnvoiFin(filtre.getDateDemandeEnvoiFin());
        }
        if (filtre.getPieceJointe() != null && !"".equals(filtre.getPieceJointe())) {
            champsRecherches.setPieceJointe(filtre.getPieceJointe());
        }
        if (filtre.getPieceJointeNom() != null && !"".equals(filtre.getPieceJointeNom())) {
            champsRecherches.setPieceJointeNom(filtre.getPieceJointeNom());
        }
        if (filtre.getIdMessageDestinataire() != null && !"".equals(filtre.getIdMessageDestinataire())) {
            champsRecherches.setIdMessageDestinataire(filtre.getIdMessageDestinataire());
        }
        if (filtre.getIdentifiantsObjetsMetier() != null) {
            champsRecherches.setIdentifiantsObjetsMetier(filtre.getIdentifiantsObjetsMetier());
        }
        if (filtre.getReferencesObjetsMetier() != null) {
            champsRecherches.setReferencesObjetsMetier(filtre.getReferencesObjetsMetier());
        }
        if (filtre.getIdentifiantsObjetsMetierAgentConnecte() != null && !filtre.getIdentifiantsObjetsMetierAgentConnecte().isEmpty()) {
            champsRecherches.setIdentifiantsObjetsMetierAgentConnecte(filtre.getIdentifiantsObjetsMetierAgentConnecte());
        }
    }

    @Override
    public Page<MessageDTO> getBrouillons(SerializableRechercheMessage filtre, Pageable pageable) {
        filtre.setStatuts(asList(TypeStatutEmail.BROUILLON));
        Page<Message> messsages = messagePaginableRepository.rechercher(filtre, pageable);

        var result = messsages.stream().map(message -> {
            MessageDTO dto = messageMapper.toDto(message);
            dto.setNbDestinataires(message.getDestinataires().size());
            dto.setCodeLien(message.getDestinataires().stream().findFirst().map(Email::getCodeLien).orElse(null));
            return dto;
        }).collect(Collectors.toList());

        return new PageImpl<>(result, pageable, messsages.getTotalElements());
    }

    @Override
    public boolean repondre(String codeLien, MessageDTO messageDTO, String emailReponse) throws IOException, PieceJointeIntrouvableException, MessageDAOException, ServiceException {
        Email original = emailRepository.findByCodeLien(codeLien).orElseThrow(() -> new MessageDAOException("Code lien introuvable " + codeLien));
        Message messageOriginal = original.getMessage();
        LOGGER.info("Réponse au message : {}", messageOriginal);
        // 1 - Initialisation du message et du mail
        DestinataireDTO reponse = destinataireMapper.toDto(original);
        if (emailReponse != null) {
            reponse.setMailContactDestinataire(emailReponse);
        }
        Message messageReponse = messageFullMapper.copy(original.getMessage());
        messageReponse.setId(null);
        messageReponse.setUuid(UUID.randomUUID().toString());
        messageReponse.setDateModification(LocalDateTime.now());
        messageReponse.setContenu(messageDTO.getContenu());
        messageReponse.setEmailsAlerteReponse(messageDTO.getEmailsAlerteReponse());
        messageReponse.setInitialisateur(messageDTO.getInitialisateur());
        messageReponse.getPiecesJointes().clear();
        // vidage des 1:n
        messageReponse.setDestinataires(new HashSet<>());
        messageReponse.setCriteres(new ArrayList<>());
        messageReponse.setMetaDonnees(new HashSet<>());
        messageReponse.setMetaDonnees(messageDTO.getMetaDonnees().stream().map(meta -> new CleValeur(meta.getCode(), meta.getCle(), meta.getValeur())).collect(Collectors.toSet()));
        // 2 - sauve les fichiers
        persisteUploadedfiles(messageReponse, messageDTO);
        List<DestinataireDTO> listeDests = new ArrayList<>();
        listeDests.add(reponse);
        integreLesDestinataires(listeDests, new ArrayList<>(), messageReponse);
        // dossier volumineux
        messageReponse.setDossierVolumineux(dossierVolumineuxMapper.toBo(messageDTO.getDossierVolumineux()));
        LOGGER.info("le contenu de la réponse est : {}", messageReponse);

        saveOrCreateMessage(messageReponse, original, false);
        return true;
    }

    @Override
    @Transactional
    public EmailDTO initialiseMessageEntreprise(String token) throws MessageDAOException, ServiceException {
        // on doit avoir un email UNIQUE
        var critereRechercheMessage = (SerializableRechercheMessage) tokenManager.getData(token);
        var emailEntrepriseConnectee = critereRechercheMessage.getMailDestinataire();
        var codeLien = critereRechercheMessage.getCodeLien();
        // seulement coté agent
        if (codeLien == null) {
            throw new ServiceException("Token non valide");
        }
        var dto = emailRepository.findByCodeLien(codeLien).map(emailMapper::toDto).orElseThrow(() -> new MessageDAOException("Aucun mail correspondant au code lien " + codeLien));
        // si l'utilisateur est connecté
        var controleMail = critereRechercheMessage.isControleMail();
        if ((!controleMail && emailEntrepriseConnectee != null) || (controleMail && emailEntrepriseConnectee != null && emailEntrepriseConnectee.equalsIgnoreCase(dto.getEmail()))) {
            dto.setConnecte(true);
        } else {
            dto.setToutesLesReponses(new ArrayList<>());
        }
        dto.setEmailConnecte(emailEntrepriseConnectee);
        dto.getMessage().setDossiersVolumineux(critereRechercheMessage.getDossiersVolumineux());
        dto.setConnecte(dto.isConnecte() || dto.isEmailEntreprise());
        return dto;
    }

    @Override
    public MessageStatusDTO countMessageStatus(SerializableRechercheMessage critereRechercheMessage) {
        MessageStatusDTO messageStatusDTO = new MessageStatusDTO();
        SerializableRechercheMessage critere = new SerializableRechercheMessage();
        reintroductionDuFiltreDefiniParPlateforme(critere, critereRechercheMessage);
        critere.setStatutsAExclure(asList(TypeStatutEmail.BROUILLON));

        try {
            // count messages
            int nombreMessages = emailPaginableRepository.compter(critere).intValue();

            // Count message non lu
            var critereReponseNonLue = new SerializableRechercheMessage();
            critereReponseNonLue.setReponseNonLue(true);
            reintroductionDuFiltreDefiniParPlateforme(critereReponseNonLue, critereRechercheMessage);
            int nombreMessageNonLu = emailPaginableRepository.compter(critereReponseNonLue).intValue();

            //Count message en attente de reponse
            var critereAttenteReponse = new SerializableRechercheMessage();
            critereAttenteReponse.setEnAttenteReponse(true);
            reintroductionDuFiltreDefiniParPlateforme(critereAttenteReponse, critereRechercheMessage);
            int nombreMessageEnAttenteReponse = emailPaginableRepository.compter(critereAttenteReponse).intValue();

            // Count message non délivré
            List<TypeStatutEmail> status = List.of(TypeStatutEmail.ECHEC_ENVOI);
            var critereEchec = new SerializableRechercheMessage();
            critereEchec.setStatuts(status);
            reintroductionDuFiltreDefiniParPlateforme(critereEchec, critereRechercheMessage);
            int nombreMessageNonDelivre = emailPaginableRepository.compter(critereEchec).intValue();

            messageStatusDTO.setNombreMessageNonLu(nombreMessageNonLu);
            messageStatusDTO.setNombreMessageEnAttenteReponse(nombreMessageEnAttenteReponse);
            messageStatusDTO.setNombreMessageNonDelivre(nombreMessageNonDelivre);
            messageStatusDTO.setNombreMessages(nombreMessages);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return messageStatusDTO;
    }

    @Override
    public PieceJointeDTO searchPJByCodeLienAndUuidPj(String codeLien, String uuid) throws MessageDAOException {
        var pj = pieceJointeRepository.findByUuid(uuid).orElseThrow(() -> new MessageDAOException("PJ introuvable " + uuid));
        return pieceJointMapper.toDto(pj);
    }

    @Override
    public PieceJointeDTO searchByMessageUuidAndPjUuid(String messageUuid, String uuid) throws MessageDAOException {
        var pj = pieceJointeRepository.findByUuidAndMessageUuid(uuid, messageUuid).orElseThrow(() -> new MessageDAOException("PJ introuvable " + uuid));
        return pieceJointMapper.toDto(pj);
    }

    @Override
    public MessageDTO getMessageByUuid(String messageUuid) throws MessageDAOException {
        var message = messageRepository.findByUuid(messageUuid).orElseThrow(() -> new MessageDAOException("message introuvable " + messageUuid));
        return messageMapper.toDto(message);
    }

    @Override
    public List<DestinataireDTO> getDestinataires(String token) {
        Message message = (Message) tokenManager.getData(token);
        if (message == null) {
            throw new MessageDAOException("aucun message associé au token " + token);
        }
        var destinataires = destinataireMapper.toDtos(message.getDestinatairesPlateFormeDestinataire());
        if (message.getDestinatairesPlateFormeEmettreur() != null && !message.getDestinatairesPlateFormeEmettreur().isEmpty()) {
            destinataires.addAll(destinataireMapper.toDtos(message.getDestinatairesPlateFormeEmettreur()));
        }
        messageFullMapper.sort(destinataires);
        return destinataires;
    }

    @Override
    public List<DestinataireDTO> getDestinatairesNotifies(String token) {
        Message message = (Message) tokenManager.getData(token);
        if (message == null) {
            throw new MessageDAOException("aucun message associé au token " + token);
        }
        List destinatairesNotifies = new ArrayList<DestinataireDTO>();
        List<Email> tousLesEmails = emailRepository.findEmailByIdObjetMetierAndReferenceObjetMetier(message.getIdentifiantObjetMetierPlateFormeEmetteur(), message.getReferenceObjetMetier(), message.getIdentifiantPfEmetteur(), Arrays.asList(TypeStatutEmail.ACQUITTE, TypeStatutEmail.ENVOYE, TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT));
        if (tousLesEmails != null && !tousLesEmails.isEmpty()) {
            destinatairesNotifies = destinataireMapper.toDtos(tousLesEmails);
        }
        messageFullMapper.sort(destinatairesNotifies);
        return destinatairesNotifies;
    }

}
