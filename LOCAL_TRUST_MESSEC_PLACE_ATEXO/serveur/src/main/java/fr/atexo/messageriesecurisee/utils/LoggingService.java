package fr.atexo.messageriesecurisee.utils;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import fr.atexo.messageriesecurisee.model.Alerte;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.model.Token;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class LoggingService {

    private static final String TARGET_LOGGING_FILE = "target_file";
    private static final String ROOT_PATH = "root";

    public String getPlateforme(SerializableRechercheMessage critereRechercheMessage) {
        return Optional.ofNullable(critereRechercheMessage)
                .map(m -> this.getPath(m.getIdPlateformeRecherche(), m.getRefObjetMetier()))
                .orElse(null);
    }

    public String getPlateforme(RechercheMessage rechercheMessage) {
        return Optional.ofNullable(rechercheMessage)
                .map(m -> this.getPath(m.getIdPlateformeRecherche(), m.getRefObjetMetier()))
                .orElse(null);
    }

    public String getPlateforme(MessageSecuriseInit messageSecuriseInit) {
        return Optional.of(messageSecuriseInit)
                .map(m -> this.getPath(m.getIdPfEmetteur(), m.getRefObjetMetier()))
                .orElse(null);
    }

    public String getPlateforme(Message message) {
        return Optional.ofNullable(message)
                .map(m -> this.getPath(m.getIdentifiantPfEmetteur(), m.getReferenceObjetMetier()))
                .orElse(null);
    }

    public String getPlateforme(MessageDTO message) {
        return Optional.ofNullable(message)
                .map(m -> this.getPath(m.getIdentifiantPfEmetteur(), m.getReferenceObjetMetier()))
                .orElse(null);
    }

    public String getPlateforme(EmailDTO email) {
        return Optional.ofNullable(email)
                .map(EmailDTO::getMessage)
                .map(this::getPlateforme)
                .orElse(null);
    }

    public String getPlateforme(Email email) {
        return Optional.ofNullable(email)
                .map(Email::getMessage)
                .map(this::getPlateforme)
                .orElse(null);
    }

    public String getPlateforme(Alerte alerte) {
        return Optional.ofNullable(alerte)
                .map(Alerte::getMessage)
                .map(this::getPlateforme)
                .orElse(null);
    }

    public String getPlateforme(Token token) {
        return Optional.ofNullable(token)
                .map(t -> this.getPath(t.getPlateforme(), t.getReference()))
                .orElse(null);
    }

    public void populateLog(String plateforme) {
        if (plateforme != null)
            MDC.put(TARGET_LOGGING_FILE, plateforme);
    }

    public void populateLog() {
        MDC.put(TARGET_LOGGING_FILE, ROOT_PATH);
    }

    public void removeLog(String plateforme) {
        if (plateforme != null) {
            MDC.remove(plateforme);
        }
    }

    public void removeLog() {
        MDC.clear();
    }

    public String getPath(String... ids) {
        return Arrays.stream(ids).filter(Objects::nonNull).collect(Collectors.joining(File.separator));
    }

}
