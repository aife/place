package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.CritereDTO;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;

import java.io.File;
import java.util.List;

public interface ExportService {

    String generatePackage(String idPlateform) throws InterruptedException;

    File export(SerializableRechercheMessage critere, List<CritereDTO> templates) throws ServiceException;
}
