package fr.atexo.messageriesecurisee.service.util;

import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import org.apache.commons.math3.random.RandomData;
import org.apache.commons.math3.random.RandomDataImpl;
import org.apache.commons.net.util.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;

/**
 * Classe generatrice d'information ici genere le code lien
 * 
 * @author ARD
 * 
 */
public final class GenerateurCodeLien {
    private static final int ITERATION_COUNT = 65536;
    private static final int KEY_LENGTH = 128;
	private static final String KEY = "messag3ri*_secur1s.e-EncrYption";
	private static final RandomData RANDOM_DATA = new RandomDataImpl();
	private static final byte[] AES_KEY = new byte[] { 0x1, 0x2, 0x3, 0x4, 0x5,
			0x6, 0x7, 0x8 };
	private static SecretKey secret = null;

	private GenerateurCodeLien() {
        super();
	}

	/**
	 * Genere le code lien pour un email
	 * 
	 * @param message
	 *            le message auquel l'email est rattaché
	 * @param email
	 *            l'email pour lequel génerer le code lien
	 * @return le code lien generer
	 * @throws CannotGenerateCodeException
	 *             il n'a pas ete possible de generer le code lien
	 */
	public static String generateCodeLien(Message message, Email email)
			throws CannotGenerateCodeException {
		StringBuffer ret = new StringBuffer();

		ret.append(RANDOM_DATA.nextInt(0, Integer.MAX_VALUE))
				.append(System.currentTimeMillis())
				.append(email.getEmail())
				.append(RANDOM_DATA.nextInt(0, Integer.MAX_VALUE))
				.append(message.getObjet()
						.charAt(RANDOM_DATA.nextInt(0, message.getObjet()
								.length() - 1)));
		try {
			if (secret == null) {
				SecretKeyFactory factory = SecretKeyFactory
						.getInstance("PBKDF2WithHmacSHA1");
				KeySpec spec = new PBEKeySpec(KEY.toCharArray(), AES_KEY,
                        ITERATION_COUNT, KEY_LENGTH);
				SecretKey tmp = factory.generateSecret(spec);
				secret = new SecretKeySpec(tmp.getEncoded(), "AES");
			}
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			AlgorithmParameters params = cipher.getParameters();
			@SuppressWarnings("unused")
			byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
			byte[] cipherText = cipher.doFinal(ret.toString().getBytes());
			return new String(Base64.encodeBase64URLSafe(cipherText));
		} catch (InvalidKeyException e) {
			throw new CannotGenerateCodeException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new CannotGenerateCodeException(e);
		} catch (NoSuchPaddingException e) {
			throw new CannotGenerateCodeException(e);
		} catch (InvalidParameterSpecException e) {
			throw new CannotGenerateCodeException(e);
		} catch (IllegalBlockSizeException e) {
			throw new CannotGenerateCodeException(e);
		} catch (BadPaddingException e) {
			throw new CannotGenerateCodeException(e);
		} catch (InvalidKeySpecException e) {
			throw new CannotGenerateCodeException(e);
		}
	}
}
