package fr.atexo.messageriesecurisee.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.service.util.XmlExtractedValuesUtils;
import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Controller
@Slf4j
public class ReponseLegacyController {

    @Autowired
    EmailService emailService;

    @Autowired
    TokenManager tokenManager;

    @Autowired
    RestTemplate restTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private Environment environment;

    private List<String> jsDev = asList("runtime.js", "polyfills.js", "scripts.js", "main.js", "vendor.js");

    private List<String> jsProd = asList("/messagerieSecurisee/front/messec.js");

    private List<String> cssDev = asList("styles.css");

    private List<String> cssProd = asList("/messagerieSecurisee/front/styles.css", "/messagerieSecurisee/front/assets/css/font-awesome.css", "/messagerieSecurisee/css/ng-style.css");

    @GetMapping(value = "/telechargement", produces = "text/html")
    public String getReponsePage(@RequestParam("codelien") String codeLien, Model model, HttpServletResponse response) {
        try {
            var email = emailService.findEmailEmailByCodeLien(codeLien);
            var message = email.getMessage();
            var rechercheInit = new RechercheMessage();
            rechercheInit.setIdPlateformeRecherche(message.getIdentifiantPfEmetteur());
            rechercheInit.setIdObjetMetier(message.getIdentifiantObjetMetierPlateFormeEmetteur());
            rechercheInit.setRefObjetMetier(message.getReferenceObjetMetier());
            rechercheInit.setCodeLien(codeLien);
            rechercheInit.setMailDestinataire(email.getEmail());
            var emailsAlerteReponse = message.getEmailsAlerteReponse();
            var critere = XmlExtractedValuesUtils.convertir(rechercheInit);
            if (emailsAlerteReponse != null && !"".equals(emailsAlerteReponse)) {
                critere.getEmailsAlerteReponse().addAll(Arrays.asList(emailsAlerteReponse.split(";|,|\\s+")));
            }
            String token = tokenManager.associateData(critere, message.getIdentifiantPfEmetteur(), message.getIdentifiantObjetMetierPlateFormeEmetteur(), message.getReferenceObjetMetier());
            var contexte = objectMapper.writeValueAsString(ContexteDTO.builder()
                    .token(token)
                    .build());
            model.addAttribute("contexte", contexte);
            model.addAttribute("styleSheets", getCss());
            model.addAttribute("scriptsJS", getJs());
            response.addHeader("content-type", "text/html");
            response.addHeader("X-Frame-Options", "sameorigin");
            notifier(message.getUrlPfDestinataireVisualisation());
            return "reponse";
        } catch (Exception ex) {
            model.addAttribute("erreur", ex.getMessage());
            return "error";
        }
    }

    private void notifier(String urlPfDestinataire) {
        if (urlPfDestinataire == null) {
            log.warn("Impossible de notifier le contrat/acte car l'url de la plateforme destinataire est null");
        } else {
            var queryParams = UriComponentsBuilder.fromUriString(urlPfDestinataire).build().getQueryParams();
            var contratUuid = queryParams.getFirst("contrat");
            var acteUuid = queryParams.getFirst("acte");
            var urlNotification = acteUuid == null ? buildNotificationContratURL(urlPfDestinataire, contratUuid) : buildNotificationActeURL(urlPfDestinataire, acteUuid);
            log.info("Notification du contrat/acte {}/{} via l'url {}", contratUuid, acteUuid, urlNotification);
            if (urlNotification != null) {
                try {
                    restTemplate.postForEntity(urlNotification, null, Object.class).getBody();
                    log.info("Le contrat/acte {}/{} a été notifié avec succès", contratUuid, acteUuid);
                } catch (Exception ex) {
                    log.warn("Impossible de notifier le contrat/acte {}/{} via l'url {}", contratUuid, acteUuid, urlNotification, ex);
                }
            }
        }
    }

    public String buildBaseURL(String urlPfDestinataire) {
        try {
            var uri = new URI(urlPfDestinataire);
            return uri.getScheme() + "://" + uri.getHost();
        } catch (Exception ex) {
            log.warn("Impossible de notifier le contrat {} car l'url de la plateforme destinataire est mal formée", urlPfDestinataire);
            return null;
        }
    }

    public String buildNotificationContratURL(String urlPfDestinataire, String contratUuid) {
        return buildBaseURL(urlPfDestinataire) + "/exec-api/api/contrat/notification/" + contratUuid;
    }

    public String buildNotificationActeURL(String urlPfDestinataire, String acteUuid) {
        return buildBaseURL(urlPfDestinataire) + "/exec-api/api/actes/notification/" + acteUuid;
    }

    private List<String> getCss() {

        if (isDev()) {
            return cssDev.stream().map(css -> "http://localhost:4200/" + css).collect(Collectors.toList());
        }
        return cssProd;

    }

    private List<String> getJs() {
        if (isDev()) {
            return jsDev.stream().map(js -> "http://localhost:4200/" + js).collect(Collectors.toList());
        }
        return jsProd;
    }

    private boolean isDev() {
        if (environment == null) return false;
        var env = environment.getActiveProfiles();
        return env != null && asList(env).contains("dev");
    }

    @PostConstruct
    public void init() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Bean
    RestTemplate messecRestTemplate() {
        return new RestTemplate();
    }

}
