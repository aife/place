package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.dto.DestinataireDTO;
import fr.atexo.messageriesecurisee.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = {"/rest/destinataires", "/rest/v2/destinataires"})
@RequiredArgsConstructor
@Slf4j
public class DestinataireController {


    private final MessageService messageService;

    @GetMapping("/")
    public ResponseEntity<List<DestinataireDTO>> getDestinataires(@RequestParam String token) {
        log.info("récupération des destinataires associés au token {}", token);
        var destinataires = messageService.getDestinataires(token);
        return new ResponseEntity<>(destinataires, HttpStatus.OK);
    }

    @GetMapping("/notifies")
    public ResponseEntity<List<DestinataireDTO>> getDestinatairesNotifies(@RequestParam String token) {
        log.info("récupération des destinataires notifiés associés au token {}", token);
        var destinatairesNotifies = messageService.getDestinatairesNotifies(token);
        return new ResponseEntity<>(destinatairesNotifies, HttpStatus.OK);
    }

}
