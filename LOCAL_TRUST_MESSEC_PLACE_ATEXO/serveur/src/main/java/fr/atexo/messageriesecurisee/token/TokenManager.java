package fr.atexo.messageriesecurisee.token;

import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.model.Token;

import java.util.List;

public interface TokenManager {

    String generateToken();

    String associateData(Object data, String identifiantPlateforme, Integer identifiantObjetMetier, String reference) throws MessageDAOException;

    void updateData(String token, Object data);

    void removeToken(String token);

    Object getData(String token);

    List<Token> getActiveTokenByType(Class clazz);

    void removeExpiredTokens();
}
