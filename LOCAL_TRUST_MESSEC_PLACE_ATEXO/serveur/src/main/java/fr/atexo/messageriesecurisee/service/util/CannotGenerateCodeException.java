package fr.atexo.messageriesecurisee.service.util;
/**
 * Exception Emise s'il na été possible de générer un code lien
 * @author ARD
 *
 */
public class CannotGenerateCodeException extends Exception {

	/**
	 * Creer une nouvelle exception
	 */
	public CannotGenerateCodeException(){
		super();
	}
	/**
	 * Creer une nouvelle exception
	 * @param e exception parente
	 */
	public CannotGenerateCodeException(Exception e){
		super(e);
	}
	/**
	 * Creer une nouvelle exception
	 * @param message le message à afficher
	 */
	public CannotGenerateCodeException(String message){
		super(message);
	}
}
