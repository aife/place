package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.dto.CodeLabelDTO;
import fr.atexo.messageriesecurisee.service.referentiel.ReferentielService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = {"/rest/referentiels", "/rest/v2/referentiels"})
public class ReferentielController {

    final
    ReferentielService referentielService;

    public ReferentielController(ReferentielService referentielService) {
        this.referentielService = referentielService;
    }


    @GetMapping(value = "/surcharge-libelle")
    public ResponseEntity<List<CodeLabelDTO>> getSurchargeLibelleList() {
        return new ResponseEntity<>(referentielService.getSurchargeLibellesList(), HttpStatus.OK);
    }
}
