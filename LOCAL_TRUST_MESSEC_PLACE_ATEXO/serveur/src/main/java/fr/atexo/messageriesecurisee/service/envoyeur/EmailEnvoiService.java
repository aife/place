package fr.atexo.messageriesecurisee.service.envoyeur;

import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;

public interface EmailEnvoiService {


    void envoyerAlerte( Email emailInitial, Message reponse);

    void envoyerAcquittementReponseEntreprise(Message reponse, Message messageInitial);

    void envoieTousMessagesEnAttente();

    void verifieEnvoiParServeurPostFix();

    void envoyerAlertes();

    void envoiEmailsExpires();

    void ennvoyerAlerteMessageEntreprise(Message message);

    void envoyerAcquittementEnvoi(Message msg);
}
