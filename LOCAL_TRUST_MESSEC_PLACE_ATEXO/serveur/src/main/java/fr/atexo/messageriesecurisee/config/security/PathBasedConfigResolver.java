package fr.atexo.messageriesecurisee.config.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.representations.adapters.config.AdapterConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class PathBasedConfigResolver implements KeycloakConfigResolver {

    private final ConcurrentHashMap<String, KeycloakDeployment> cache = new ConcurrentHashMap<>();

    @Autowired
    private AdapterConfig defaultAdapterConfig;

    @Override
    public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {

        var tokenOpt = Optional.ofNullable(request.getHeader("Authorization")).map(h -> h.substring("Bearer ".length()));
        if (tokenOpt.isPresent()) {
            var token = tokenOpt.get();
            DecodedJWT jwt = JWT.decode(token);
            var issuer = jwt.getIssuer();
            var realm = issuer.substring(issuer.lastIndexOf("/") + 1);
            if (!cache.containsKey(realm)) {
                var adapterConfig = new AdapterConfig();
                adapterConfig.setRealm(realm);
                adapterConfig.setAuthServerUrl(issuer.substring(0, issuer.indexOf("realms")));
                adapterConfig.setSslRequired(adapterConfig.getSslRequired());
                adapterConfig.setResource(defaultAdapterConfig.getResource());
                adapterConfig.setPublicClient(adapterConfig.isPublicClient());
                adapterConfig.setConfidentialPort(adapterConfig.getConfidentialPort());
                var deploymentBuilder = KeycloakDeploymentBuilder.build(adapterConfig);
                cache.put(realm, deploymentBuilder);
            }

            return cache.get(realm);
        }
        // default config
        return KeycloakDeploymentBuilder.build(defaultAdapterConfig);

    }

}
