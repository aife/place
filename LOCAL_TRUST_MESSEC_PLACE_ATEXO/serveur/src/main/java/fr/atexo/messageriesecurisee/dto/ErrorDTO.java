package fr.atexo.messageriesecurisee.dto;

public class ErrorDTO {

    Object rejectedValue;
    String message;
    private Integer code;
    private String exception;
    private String path;

    public ErrorDTO(Integer code, String message, String exception) {
        super();
        this.code = code;
        this.message = message;
        this.exception = exception;
    }

    public ErrorDTO(Integer code, String message, String path, String exception) {
        super();
        this.code = code;
        this.message = message;
        this.exception = exception;
        this.path = path;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Object getRejectedValue() {
        return rejectedValue;
    }

    public void setRejectedValue(Object rejectedValue) {
        this.rejectedValue = rejectedValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
