package fr.atexo.messageriesecurisee.service.validation;

import java.io.InputStream;

public interface FileValidationService {

    boolean checkFile(InputStream fileToCheck);

    boolean isAntivirusActif();
}
