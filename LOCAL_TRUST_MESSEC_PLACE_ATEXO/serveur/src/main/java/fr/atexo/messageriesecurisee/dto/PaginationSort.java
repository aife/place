package fr.atexo.messageriesecurisee.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaginationSort {

    @JsonProperty("direction")
    private String direction;

    @JsonProperty("property")
    private String property;

    public void PaginationSort() {

    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
