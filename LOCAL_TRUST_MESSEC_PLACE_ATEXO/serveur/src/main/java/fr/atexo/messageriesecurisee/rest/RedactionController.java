package fr.atexo.messageriesecurisee.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.MessageNonValideException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.CriterMapper;
import fr.atexo.messageriesecurisee.mapper.DossierVolumineuxMapper;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.service.message.MessageService;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.utils.DateUtils.toLocalDateTime;
import static org.springframework.http.HttpStatus.*;

@Controller
@RequestMapping(value = {"/rest/redaction", "/rest/v2/redaction"})
public class RedactionController {

    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
    static final Logger LOGGER = LoggerFactory.getLogger(RedactionController.class);
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

    private final MessageService messageService;

    private final EmailService emailService;

    private final TokenManager tokenManager;

    private final LoggingService loggingUtils;

    private final DossierVolumineuxMapper dossierVolumineuxMapper;

    private final CriterMapper criterMapper;

	@Value("${max.caracteres.contenu}")
	private Long maxCaracteresContenu;

    public RedactionController(MessageService messageService, EmailService emailService, TokenManager tokenManager, DossierVolumineuxMapper dossierVolumineuxMapper, CriterMapper criterMapper, LoggingService loggingUtils) {
        this.messageService = messageService;
        this.emailService = emailService;
        this.tokenManager = tokenManager;
        this.loggingUtils = loggingUtils;
        this.dossierVolumineuxMapper = dossierVolumineuxMapper;
        this.criterMapper = criterMapper;
    }

    @PostMapping(value = "/initToken", produces = "text/plain")
    public ResponseEntity<String> initToken(@RequestBody MessageSecuriseInit messageSecuriseInit, HttpServletRequest request) throws JsonProcessingException, ServiceException, MessageDAOException {
        var plateforme = loggingUtils.getPlateforme(messageSecuriseInit);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("Initialisation d'un token de rédaction pour la référence {}", messageSecuriseInit.getRefObjetMetier());
        LOGGER.debug("Données transmises {}", messageSecuriseInit);
        String encoding = request.getCharacterEncoding();
        List<String> erreursVerif = new ArrayList<>();
        if (!isMessageInitlialisationValide(messageSecuriseInit, erreursVerif)) {
            StringBuilder erreurs = new StringBuilder("Message d'initilialisation invalide : ");
            for (String s : erreursVerif) {
                erreurs.append("\n").append(s);
            }
            throw new ServiceException(erreurs.toString());
        }

        Message message = new Message();
        if (messageSecuriseInit.getCodeLien() != null) {
            LOGGER.info("Initialisation d'un token de rédaction à partir du code lien {}", messageSecuriseInit.getCodeLien());
            message = messageService.getMessageByCodeLien(messageSecuriseInit.getCodeLien());
        }
        messageService.transformeMessageInitialisationEnMessage(messageSecuriseInit, message, encoding);
        String token = tokenManager.associateData(message, message.getIdentifiantPfEmetteur(), message.getIdentifiantObjetMetierPlateFormeEmetteur(), message.getReferenceObjetMetier());
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(token, OK);
    }

    @GetMapping(value = "/initTokenByCodeLien")
    public ResponseEntity<String> initTokenByCodeLien(@RequestParam(required = true) String codeLien) throws MessageDAOException {
        Message message = messageService.getMessageByCodeLien(codeLien);
        message.setCodeLien(codeLien);
        var plateforme = loggingUtils.getPlateforme(message);
        LOGGER.info("Initialisation d'un token de rédaction à partir du code lien {}", codeLien);
        loggingUtils.populateLog(plateforme);
        if (message == null) {
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>((String) null, NO_CONTENT);
        } else {
            String token = tokenManager.associateData(message, message.getIdentifiantPfEmetteur(), message.getIdentifiantObjetMetierPlateFormeEmetteur(), message.getReferenceObjetMetier());
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>(token, OK);
        }
    }

    @GetMapping(value = "/initialiseMessage")
    public ResponseEntity<MessageDTO> initialiseMessage(@RequestParam(required = true) String token) throws MessageDAOException {

        MessageDTO dto = messageService.initialiseMessage(token);
        var plateforme = loggingUtils.getPlateforme(dto);
        LOGGER.info("Initialisation du message à partir du token {}", token);
        LOGGER.debug("données transmises {}", dto);
        loggingUtils.populateLog(plateforme);
        if (dto != null) {
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>(dto, OK);
        } else {
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>((MessageDTO) null, UNAUTHORIZED);
        }
    }

    @GetMapping(value = "/initialiseMessageEntreprise")
    public ResponseEntity<EmailDTO> initialiseMessageEntreprise(@RequestParam(required = true) String token) throws MessageDAOException, ServiceException {
        EmailDTO dto = messageService.initialiseMessageEntreprise(token);
        var plateforme = loggingUtils.getPlateforme(dto);
        LOGGER.info("Initialisation d'un message entreprise à partir du token {}", token);
        LOGGER.debug("données transmises {}", dto);
        if (dto != null) {
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>(dto, OK);
        } else {
            loggingUtils.removeLog(plateforme);
            return new ResponseEntity<>((EmailDTO) null, UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/saveMessage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDTO> saveMessage(@RequestParam(required = true) String token, @RequestParam(required = false) Boolean brouillon, @RequestBody @Valid MessageDTO messageDTO) throws MessageDAOException, IOException, PieceJointeIntrouvableException, ServiceException {

        Message msg = (Message) tokenManager.getData(token);
        var plateforme = loggingUtils.getPlateforme(msg);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("sauvegarde du message associé au token {}", token);
        LOGGER.debug("données du token {}", msg);
        LOGGER.debug("données tranmises depuis l'IHM {} => brouillon : {}", messageDTO, brouillon);
        // 1 -positionne les valeurs
        msg.setObjet(messageDTO.getObjet());
        msg.setContenu(messageDTO.getContenu());
        msg.setTypeMessage(messageDTO.getTypeMessage());
        msg.setDateLimiteReponseAttendue(messageDTO.isDateLimiteReponseAttendue());
        msg.setReponseAttendue(messageDTO.isReponseAttendue());
        msg.setReponseAgentAttendue(messageDTO.isReponseAgentAttendue());
        String cartouche = messageDTO.getCartouche();
        msg.setCartouche(cartouche);
        msg.setCriteres(criterMapper.toBos(messageDTO.getCriteres()));
        msg.setEmailsAlerteReponse(messageDTO.getEmailsAlerteReponse());
        msg.setTemplateMail(messageDTO.getTemplateMail());
        msg.setTemplateFige(messageDTO.isTemplateFige());
        msg.setDateLimiteReponseAttendue(messageDTO.isDateLimiteReponseAttendue());
        msg.setDateLimiteReponse(toLocalDateTime(messageDTO.getDateLimiteReponseAsDate()));
        msg.setReponseBloque(messageDTO.isReponseBloque());
        msg.setBrouillon(messageDTO.isBrouillon());
        msg.setInitialisateur(messageDTO.getInitialisateur());
        if (messageDTO.getDossierVolumineux() != null) {
            var dv = dossierVolumineuxMapper.toBo(messageDTO.getDossierVolumineux());
            msg.setDossierVolumineux(dv);
        } else {
            msg.setDossierVolumineux(null);
        }
        if (messageDTO.isDateReponseAttendue()) {
            if (messageDTO.getDateReponse() != null) {
                try {
                    msg.setDateReponse(LocalDateTime.parse(messageDTO.getDateReponse(), formatter));
                } catch (Exception ex) {
                    msg.setDateReponse(null);
                }
            }
            if (messageDTO.getDateReponseAsDate() != null) {
                msg.setDateReponse(toLocalDateTime(messageDTO.getDateReponseAsDate()));
            }
        } else {
            msg.setDateReponse(null);
        }
        messageService.integreLesDestinataires(messageDTO.getDestsPfDestinataire(), messageDTO.getDestsPfEmettreur(), msg);

        // 2 - sauve les fichiers
        messageService.persisteUploadedfiles(msg, messageDTO);
        LOGGER.debug("{}", msg);
        // 3 - sauve le message
        var messageSauvegarde = messageService.saveOrCreateMessage(msg, null, Boolean.TRUE.equals(brouillon));
        // 4 - retrait de la fifo des token
        tokenManager.removeToken(token);
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(messageSauvegarde, CREATED);
    }

    @GetMapping(value = "/downloadPieceJointe")
    public ResponseEntity<InputStreamResource> downloadUploadedFile(@RequestParam(required = true) String token, @RequestParam(required = true) Integer pieceJointeId) throws MessageDAOException {

        var data = tokenManager.getData(token);
        if (data != null) {
            Message msg = (Message) data;
            var plateforme = loggingUtils.getPlateforme(msg);
            loggingUtils.populateLog(plateforme);
            LOGGER.info("téléchargemenet de la piece jointe {} depuis le token {}", pieceJointeId, token);
            var pieceJointe = msg.getPiecesJointes().stream()
                    .filter(pj -> pj.getId().equals(pieceJointeId)).findFirst().orElseThrow(() -> new MessageDAOException("Piece jointe introuvable " + pieceJointeId));
            try {
                var fis = new FileInputStream(pieceJointe.getChemin());
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(pieceJointe.getTaille());
                headers.setContentType(MediaType.parseMediaType(pieceJointe.getContentType()));
                headers.setContentDispositionFormData(pieceJointe.getNom(), pieceJointe.getNom());
                return new ResponseEntity<>(new InputStreamResource(fis), headers, OK);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                loggingUtils.removeLog(plateforme);
                return new ResponseEntity<>((InputStreamResource) null, INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>((InputStreamResource) null, NO_CONTENT);
        }
    }

    @GetMapping(value = "/relancerMessage")
    public ResponseEntity<EmailDTO> relancerMessage(@RequestParam(required = true) String codeLien) throws MessageDAOException {
        EmailDTO emailDTO = emailService.findByCodeLien(codeLien);
        var plateforme = loggingUtils.getPlateforme(emailDTO);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("relance de l'email avec le code lien {}", codeLien);
        emailDTO = emailService.relancerEmail(codeLien);
        LOGGER.debug("données transmises {}", emailDTO);
        HttpStatus status = INTERNAL_SERVER_ERROR;
        if (emailDTO != null) {
            status = CREATED;
        }
        loggingUtils.removeLog(plateforme);
        return new ResponseEntity<>(emailDTO, status);
    }

    @PostMapping(value = "/answerMessage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> answerMessage(@RequestParam(required = true) String token, @RequestParam(required = false) String codeLien, @RequestBody @Valid MessageDTO messageDTO) throws ServiceException, MessageDAOException, IOException, PieceJointeIntrouvableException {
        var filtreDefiniParPlateforme = (SerializableRechercheMessage) tokenManager.getData(token);
        var plateforme = loggingUtils.getPlateforme(filtreDefiniParPlateforme);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("réponse au message associé au token {} et au code lien {}", token, codeLien);
        LOGGER.debug("données tranmises {}", messageDTO);
        if (filtreDefiniParPlateforme == null || !codeLien.equals(filtreDefiniParPlateforme.getCodeLien())) {
            return new ResponseEntity<>(UNAUTHORIZED);
        }
        if (null != messageDTO && null != messageDTO.getContenu() && messageDTO.getContenu().length() > this.maxCaracteresContenu) {
            throw new MessageNonValideException("Le contenu du message est trop long");
        }
        messageDTO.setMetaDonnees(filtreDefiniParPlateforme.getMetaDonnees());
        messageDTO.setEmailsAlerteReponse(filtreDefiniParPlateforme.getEmailsAlerteReponse().stream().collect(Collectors.joining(";")));
        var emailContact = filtreDefiniParPlateforme.getMailDestinataire();
        Boolean reponse = messageService.repondre(codeLien, messageDTO, emailContact);
        loggingUtils.populateLog(plateforme);
        return new ResponseEntity<>(reponse, OK);
    }

    @PostMapping(value = "/agent/answerMessage", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> agentAnswerMessage(@RequestParam(required = true) String token, @RequestParam(required = false) String codeLien, @RequestBody MessageDTO messageDTO) throws ServiceException, MessageDAOException, IOException, PieceJointeIntrouvableException {
        var filtreDefiniParPlateforme = (SerializableRechercheMessage) tokenManager.getData(token);
        var plateforme = loggingUtils.getPlateforme(filtreDefiniParPlateforme);
        loggingUtils.populateLog(plateforme);
        LOGGER.info("réponse agent au message associé au token {} et au code lien {}", token, codeLien);
        LOGGER.debug("données transmises {}", messageDTO);
        if (filtreDefiniParPlateforme == null) {
            return new ResponseEntity<>(UNAUTHORIZED);
        }
        EmailDTO emailInitial = emailService.findByCodeLien(codeLien);
        messageDTO.setEmailsAlerteReponse(emailInitial.getMessage().getEmailsAlerteReponse());
        var emailContact = emailInitial.getEmail();
        Boolean reponse = messageService.repondre(codeLien, messageDTO, emailContact);
        loggingUtils.populateLog(plateforme);
        return new ResponseEntity<>(reponse, OK);
    }

    private boolean isMessageInitlialisationValide(
            MessageSecuriseInit message, List<String> erreurs) {
        boolean ret = true;
        if (message != null) {
            if (estVide(message.getIdPfEmetteur())) {
                ret = false;
                erreurs.add("Identifiant plateforme emetteur absent");
            }
            if (estVide(message.getUrlPfEmetteur())) {
                ret = false;
                erreurs.add("URL plateforme emetteur absent");
            }
            if (estVide(message.getNomPfEmetteur())) {
                ret = false;
                erreurs.add("Nom plateforme emetteur absent");
            }
            if (message.getIdObjetMetierPfEmetteur() == null) {
                ret = false;
                erreurs.add("Identifiant objet metier plateforme emetteur absent");
            }
            if (estVide(message.getNomCompletExpediteur())) {
                ret = false;
                erreurs.add("Nom complet expediteur absent");
            }
            if (estVide(message.getEmailExpediteur())) {
                ret = false;
                erreurs.add("Email complet expediteur absent");
            }
        }
        return ret;
    }

    private boolean estVide(String valeur) {
        return valeur == null || valeur.trim().isEmpty();
    }
}
