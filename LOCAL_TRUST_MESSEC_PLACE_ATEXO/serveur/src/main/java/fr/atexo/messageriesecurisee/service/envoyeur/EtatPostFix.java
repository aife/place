package fr.atexo.messageriesecurisee.service.envoyeur;
/**
 * Les differents etat postfix
 * @author ARD
 *
 */
public enum EtatPostFix {
	OK,
	KO,
	INDETERMINE;
}
