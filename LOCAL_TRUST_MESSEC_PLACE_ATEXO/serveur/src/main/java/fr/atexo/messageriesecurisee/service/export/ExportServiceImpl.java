package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.CritereDTO;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.export.Colonne;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.AbstractReferentielEntity;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.model.PieceJointe;
import fr.atexo.messageriesecurisee.repository.EmailPaginableRepository;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.repository.MessageRepository;
import fr.atexo.messageriesecurisee.repository.SurchargeLibelleRepository;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import fr.atexo.messageriesecurisee.util.CalculateurStatutEmail;
import fr.atexo.messageriesecurisee.util.StringFormatterUtil;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static fr.atexo.messageriesecurisee.service.export.ExportUtils.normalizeString;

@Service
@Slf4j
@Transactional(readOnly = true)
public class ExportServiceImpl implements ExportService {

    private static final int MAX_THREADS = 10;

    @Autowired
    EmailRepository emailRepository;
    @Autowired
    SurchargeLibelleRepository surchargeLibelleRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    DocumentGenerator documentGenerator;

    @Autowired
    ExportViewBuilder exportViewBuilder;

    @Autowired
    EmailPaginableRepository emailPaginableRepository;

    @Autowired
    EmailMapper emailMapper;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    @Autowired
    private ExportPathConfig paths;

    private final CalculateurStatutEmail calculateurStatutEmail = new CalculateurStatutEmail();

    @Override
    public String generatePackage(String idPlateform) throws InterruptedException {
        log.info("Début de l'archivage pour la plateforme : {}", idPlateform);
        List<Integer> references = messageRepository.findIdentifiantObjetMetierByPlatforme(idPlateform);
        if (!references.isEmpty()) {
            this.paths.setIdPlateform(idPlateform);
            this.exportViewBuilder.setIdPlateform(idPlateform);
            ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
            for (final Integer identifiantObjetMetierPfEmetteur : references) {
                executor.execute(() -> {
                    String threadName = Thread.currentThread()
                            .getName();
                    log.info("Thread {} -> Génération des échanges pour l'id : {}", threadName, identifiantObjetMetierPfEmetteur);
                    generateFolder(idPlateform, identifiantObjetMetierPfEmetteur);
                });
            }
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
            log.info("Fin d'exécution de toutes les taches d'export");
            return paths.getBasePath();
        }
        return "Aucun message à exporter pour cette plateforme";
    }

    public void generateFolder(String idPlateforme, Integer identitifiantObjetMetier) {
        log.info("génération du dossier pour l'identifiant objet métier : {}", identitifiantObjetMetier);
        Set<Message> messages = new HashSet<>(messageRepository.findMessagesByPlatforme(idPlateforme, identitifiantObjetMetier));
        messages.forEach(this::generateMessage);
        List<Email> emailsSuivi = messages.stream()
                .filter(m -> m.getDestinatairesPlateFormeDestinataire() != null)
                .flatMap(m -> m.getDestinatairesPlateFormeDestinataire().stream())
                .filter(e -> e.getDateDemandeEnvoi() != null)
                .sorted((e1, e2) -> e2.getDateDemandeEnvoi().compareTo(e1.getDateDemandeEnvoi()))
                .collect(Collectors.toList());

        exportViewBuilder.generateHtmlListEmailsView(emailsSuivi);
    }

    private void generateMessage(Message message) {
        log.debug("génération du message numéro : {} ", message.getId());
        message.setDateDemandeEnvoi(message.getDestinatairesPlateFormeDestinataire().stream().findFirst().map(Email::getDateDemandeEnvoi).orElse(LocalDateTime.now()));

        File destinationFile;
        destinationFile = new File(paths.getAtachementPathForMessage(message));
        destinationFile.mkdirs();
        exportViewBuilder.generateHtmlMessageView(message);
        this.copyAtachement(message.getPiecesJointes(), destinationFile);
        message.getDestinatairesPlateFormeDestinataire().forEach(e -> {
            var reponses = e.getReponses();
            for (var reponse : reponses) {
                try {
                    generateReponse(reponse, message);
                } catch (Exception ex) {
                    log.error("Erreur lors de la génétion de la réponse avec l'id {} -> {}", reponse.getId(), e);
                }
            }
        });
    }

    private void generateReponse(Email response, Message messageInitial) throws IOException, TemplateException {
        log.debug("génération de la réponse pour le méssage numéro : {} ", response.getMessage().getId());
        response.getMessage().setDateDemandeEnvoi(Optional.of(response.getDateDemandeEnvoi()).orElse(LocalDateTime.now()));
        File destinationFile;
        destinationFile = new File(paths.getAtachementPathForResponse(response.getMessage(), messageInitial));
        destinationFile.mkdirs();
        exportViewBuilder.generateResponseView(messageInitial, response);
        this.copyAtachement(response.getMessage().getPiecesJointes(), destinationFile);
    }

    private void copyAtachement(List<PieceJointe> piecesJointes, File destinationFile) {
        if (!piecesJointes.isEmpty()) {
            piecesJointes.forEach(pc -> {
                File origineFile = new File(pc.getChemin());
                if (origineFile.exists()) {
                    try {
                        pc.setNom(normalizeString(pc.getNom()));
                        log.debug("Copie du fichier dans le répertoire {} ", String.format("%s/%s", destinationFile.getPath(), origineFile.getName()));
                        FileUtils.copyFile(origineFile, new File(String.format("%s/%s", destinationFile.getPath(), pc.getNom())));
                    } catch (IOException e) {
                        log.error("Erreur lors de la copie des pièces jointes", e);
                    }
                }
            });
        }
    }

    @Override
    public File export(SerializableRechercheMessage critere, List<CritereDTO> templates) throws ServiceException {
        try (var workbook = new XSSFWorkbook()) {
            var emails = emailPaginableRepository.rechercher(critere).stream().map(emailMapper::toDto).collect(Collectors.toList());
            var resul = Files.createTempFile("", ".xlsx").toFile();
            var rowIx = 0;

            var sheet = workbook.createSheet("export");
            log.info("génération de l'entête");
            createHeader(rowIx, sheet, workbook);
            rowIx++;
            var lignesAFusionner = new HashMap<Integer, Set<Integer>>();
            for (var email : emails) {
                rowIx = genererLigne(templates, workbook, rowIx, sheet, email, null);
                var repIxes = lignesAFusionner.get(email.getId());
                if (repIxes == null) {
                    repIxes = new HashSet<>();
                    repIxes.add(rowIx);
                }
                lignesAFusionner.put(email.getId(), repIxes);
                if (email.getToutesLesReponses() != null && !email.getToutesLesReponses().isEmpty()) {
                    // on crée une copie de la liste, pour éviter que le Sort dans le getter ne modifie la liste en cours de boucle pour éviter le ConcurrentModificationException exception
                    var reponses = new ArrayList<>(email.getToutesLesReponses());
                    for (var rep : reponses) {
                        rowIx = genererLigne(templates, workbook, rowIx, sheet, rep, email);
                        lignesAFusionner.get(email.getId()).add(rowIx);
                    }
                }
            }
            // merge
            if (!lignesAFusionner.isEmpty()) {
                for (var key : lignesAFusionner.keySet()) {
                    var rowMin = lignesAFusionner.get(key).stream().min(Integer::compareTo).orElse(key) - 1;
                    var rowMax = lignesAFusionner.get(key).stream().max(Integer::compareTo).orElse(key) - 1;
                    if (rowMax > rowMin) {
                        // fusion objet
                        sheet.addMergedRegion(new CellRangeAddress(rowMin, rowMax, 0, 0));
                        // fusion type
                        sheet.addMergedRegion(new CellRangeAddress(rowMin, rowMax, 1, 1));
                        // fusion statut
                        sheet.addMergedRegion(new CellRangeAddress(rowMin, rowMax, 2, 2));
                    }
                }
            }

            workbook.write(new FileOutputStream(resul));
            return resul;
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
    }

    private int genererLigne(List<CritereDTO> templates, XSSFWorkbook workbook, int rowIx, XSSFSheet sheet, EmailDTO email, EmailDTO parent) {
        var row = sheet.createRow(rowIx);
        var cellIx = 0;
        for (var col : Colonne.values()) {
            var cell = row.createCell(cellIx);
            setValue(email, parent, workbook, cell, col, templates);
            sheet.autoSizeColumn(cellIx);
            cellIx++;
        }
        row.setHeight((short) -1);
        return rowIx + 1;
    }

    private Row createHeader(int index, XSSFSheet sheet, XSSFWorkbook workbook) {
        Row header = sheet.createRow(index);
        var headerStyle = workbook.createCellStyle();
        setBorders(headerStyle);
        var font = workbook.createFont();
        font.setBold(true);
        headerStyle.setFont(font);
        var cellIx = 0;
        for (var col : Colonne.values()) {
            var cell = header.createCell(cellIx);
            cell.setCellValue(col.getLibelle());
            cell.setCellStyle(headerStyle);
            cellIx++;
        }
        return header;
    }

    private void setValue(EmailDTO email, EmailDTO parent, XSSFWorkbook workbook, XSSFCell cell, Colonne col, List<CritereDTO> templates) {
        var style = getDefaultStyle(workbook);
        cell.setCellStyle(style);
        var italicStyle = getItalicStyle(workbook);
        var reponse = parent != null;
        var message = email.getMessage();
        switch (col) {
            case Objet:
                cell.setCellValue(reponse ? parent.getMessage().getObjet() : message.getObjet());
                break;
            case Type:
                cell.setCellValue(reponse ? getTemplate(parent.getMessage().getCriteres(), templates) : getTemplate(message.getCriteres(), templates));
                break;
            case Statut:
                var libelle = reponse ? this.getLibelleStatut(parent) : this.getLibelleStatut(email);
                cell.setCellValue(libelle);
                break;
            case Emetteur:
                cell.setCellValue(reponse ? email.getEmail() : message.getNomCompletExpediteur());
                break;
            case Destinataire:
                cell.setCellValue(reponse ? parent.getMessage().getNomCompletExpediteur() : email.getEmail());
                break;
            case Contenu:
                var emailContent = Optional.ofNullable(message.getContenu()).map(StringFormatterUtil::convertHtmlToText).orElse("");
                setContent(cell, emailContent, reponse, parent, italicStyle);
                break;
            case PiecesJointes:
                var pjContent = StringFormatterUtil.convertHtmlToText(Optional.ofNullable(message.getPiecesJointes()).orElse(new ArrayList<>()).stream().map(PieceJointeDTO::getNom).collect(Collectors.joining("<br>")));
                setContent(cell, pjContent, reponse, parent, italicStyle);
                break;
            case DateDemandeEnvoi:
                setDateValue(cell, workbook, email.getDateDemandeEnvoiAsDate());
                break;
            case DateEnvoi:
                setDateValue(cell, workbook, email.getDateEnvoiAsDate());
                break;
            case DateAR:
                setDateValue(cell, workbook, email.getDateARAsDate());
                break;
            default:
                cell.setCellValue("");
        }
    }

    private XSSFCellStyle getItalicStyle(XSSFWorkbook workbook) {
        var italicStyle = workbook.createCellStyle();
        var italicFont = workbook.createFont();
        italicFont.setFontHeightInPoints((short) 10);
        italicFont.setItalic(true);
        italicStyle.setFont(italicFont);
        setBorders(italicStyle);
        return italicStyle;
    }

    private void setBorders(XSSFCellStyle style) {
        var borderStyle = BorderStyle.THIN;
        style.setBorderBottom(borderStyle);
        style.setBorderTop(borderStyle);
        style.setBorderRight(borderStyle);
        style.setBorderLeft(borderStyle);
    }

    private XSSFCellStyle getDefaultStyle(XSSFWorkbook workbook) {
        var style = workbook.createCellStyle();
        style.setWrapText(true);
        setBorders(style);
        return style;
    }

    private void setContent(Cell cell, String content, boolean reponse, EmailDTO parent, CellStyle italicStyle) {
        if (reponse && parent.isReponseMasquee()) {
            var contenu = formatDateLimite(parent.getMessage());
            cell.setCellStyle(italicStyle);
            cell.setCellValue(contenu);

        } else {
            cell.setCellValue(content);
        }
    }

    private String formatDateLimite(MessageDTO messageDTO) {
        if (messageDTO.getDateLimiteReponseAsDate() != null) {
            return "Le contenu de la réponse sera accessible après le " + simpleDateFormat.format(messageDTO.getDateLimiteReponseAsDate());
        }
        return null;
    }

    private String getTemplate(List<CritereDTO> criteres, List<CritereDTO> templates) {
        if (criteres != null && templates != null && !criteres.isEmpty() && !templates.isEmpty()) {
            var templateOpt = criteres.stream().filter(t -> "template".equalsIgnoreCase(t.getNom())).findFirst();
            if (templateOpt.isPresent()) {
                var template = templateOpt.get();
                return templates.stream().filter(t -> template.getValeur().equalsIgnoreCase(t.getValeur()))
                        .map(CritereDTO::getLibelle)
                        .findFirst()
                        .orElse("");
            }
        }
        return "";
    }

    private void setDateValue(XSSFCell cell, XSSFWorkbook wb, Date value) {
        var style = wb.createCellStyle();
        var createHelper = wb.getCreationHelper();
        var borderStyle = BorderStyle.THIN;
        style.setBorderBottom(borderStyle);
        style.setBorderTop(borderStyle);
        style.setBorderRight(borderStyle);
        style.setBorderLeft(borderStyle);
        style.setDataFormat(createHelper.createDataFormat().getFormat("d/m/yy h:mm:ss"));
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }


    public String getLibelleStatut(EmailDTO email) {
        var libelle = "NA";
        if (email.isEnAttenteEnvoi()) {
            libelle = surchargeLibelleRepository.findByCode("statut.en-cours-envoi.titre").map(AbstractReferentielEntity::getLabel).orElse("En cours d'envoi");
        } else if (email.isEchecEnvoi()) {
            libelle = surchargeLibelleRepository.findByCode("statut.non-delivre.titre").map(AbstractReferentielEntity::getLabel).orElse("Non délivré");
        } else if (email.isDelivre()) {
            libelle = surchargeLibelleRepository.findByCode("statut.delivre.titre").map(AbstractReferentielEntity::getLabel).orElse("Délivré");
            if (email.isLu()) {
                libelle = surchargeLibelleRepository.findByCode("statut.lu-destinataire.titre").map(AbstractReferentielEntity::getLabel).orElse("Lu par le destinataire");
            }
            if (email.getReponse() != null) {
                if (email.getReponse().getDateARAsDate() == null) {
                    libelle = surchargeLibelleRepository.findByCode("statut.reponse-non-lue.titre").map(AbstractReferentielEntity::getLabel).orElse("Réponse non lue");
                } else {
                    libelle = surchargeLibelleRepository.findByCode("statut.reponse-lue.titre").map(AbstractReferentielEntity::getLabel).orElse("Réponse lue");
                }
            }
        }
        return libelle;
    }
}
