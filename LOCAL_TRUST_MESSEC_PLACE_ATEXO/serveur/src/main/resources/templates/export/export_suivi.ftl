<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Référence objet métier : ${referenceObjetMetier!}</title>
</head>
<body class="p-4">
<h1>Référence objet métier : ${referenceObjetMetier!}</h1>
<table class="table table-hover">
    <tbody>
    <tr>
        <td scope="col">Email expediteur</td>
        <td scope="col">Email destinataire</td>
        <td scope="col">Objet</td>
        <td scope="col">Statut</td>
        <td scope="col">Date d'envoi</td>
        <td scope="col">Date d'AR</td>
    </tr>
    <#list emails as email>
        <tr>
            <td><a href="mailto:${email.message.emailExpediteur!}">${email.message.emailExpediteur!}</a></td>
            <td><a href="mailto:${email.email!}">${email.email!}</a></td>
            <td><a target="_blank"
                   href="../Message_envoyé_${email.message.id?string["#"]}_${(email.message.dateDemandeEnvoi).format("yyyy_MM_dd")}/Détail_message_envoyé/Détail_message_envoyé.html">${email.message.objet!}</a>
            </td>
            <td>${email.statutEmail.libelle!}</td>
            <td>${email.dateDemandeEnvoi!}</td>
            <td>${email.dateAR!}</td>
        </tr>
    </#list>
    </tbody>
</table>
</body>
</html>
