<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Référence objet métier : ${message.referenceObjetMetier!}</title>
</head>
<body class="p-4">
<div class="card mb-4">
    <div class="card-body">
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Référence objet métier :</label>
            <div class="col-sm-10">
                ${message.referenceObjetMetier!}
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Objet :</label>
            <div class="col-sm-10">
                ${message.objet!}
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">De : </label>
            <div class="col-sm-10">
                ${message.emailExpediteur!}
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">À : </label>
            <div class="col-sm-10">
                ${destinataires!}
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Envoyé le : </label>
            <div class="col-sm-10">
                <#if message.dateDemandeEnvoi??>
                    ${(message.dateDemandeEnvoi).format('dd/MM/yyyy HH:mm:ss')}
                <#else>
                    -
                </#if>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label">Message :</label>
            <div class="col-sm-10">
                ${message.contenu!}
            </div>
        </div>
    </div>
</div>
<#if message.piecesJointes?has_content>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Pièces jointes</h5>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Nom</th>
                    <th scope="col">Taille</th>
                </tr>
                </thead>
                <tbody>
                <#list message.piecesJointes as pc>
                    <tr>
                        <td><a target="_blank" href="PJ transmises/${pc.nom}">${pc.nom}</a>
                        </td>
                        <td>${pc.tailleLisible}</td>
                    </tr>
                </#list>
            </table>
        </div>
    </div>
</#if>

</body>
</html>
