<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');

        div.breakafter {
            page-break-after: always;
        }

        table {
            border: 1px solid black;
            border-collapse: collapse;
        }

        table th {
            background-color: #2473B7;
            font-weight: bold;
            color: white;
            font-size: 18px;
        }

        html body {
            font-family: 'Roboto', cursive;
            font-size: 18px;
        }

        .pj {
            list-style: square;
        }

        .evt {
            font-weight: bold;
            color: white;
            background-color: #2473B7;
        }

        .evt-date {
            font-weight: bold;
            color: white;
            background-color: #2473B7;
        }

        .titre {
            padding: 6px;
            font-size: larger;
            font-weight: bold;
            text-align: center;
            width: 100%;
            background-color: #002060;
            color: white;
            margin-bottom: 10px;

        }

        .titre-bloc {
            padding: 6px;
            font-size: large;
            font-weight: bold;
            width: 100%;
            background-color: #002060;
            color: white;
        }

        br {
            line-height: 10px;
        }

        .date-limite {
            font-style: italic;
        }
    </style>
</head>
<body>
<!-- titre -->
<div class="titre">Attestation des échanges</div>
<div style="margin-bottom: 10px"/>

<!-- Identification de l'élément concerné -->

<table style="width: 100%;border: 1px solid #002060">
    <tbody>
    <tr>
        <td class="titre-bloc">Identification de l'élément concerné</td>
    </tr>
    <tr>
        <td style="width: 100%">
            <#if metaDonnees?hasContent>
                <#list metaDonnees as metaDonne>
                    <p style="padding-left: 4px"><b>${metaDonne.cle!}:</b></p>
                    <p style="padding-left: 4px">${metaDonne.valeur!}</p>
                    <div style="margin-bottom: 10px"/>
                </#list>
            <#else>
                ${cartoucheEscaped!}
            </#if>
        </td>
    </tr>
    </tbody>
</table>
<div class="breakafter"/>
<!-- identification de l'échange -->
<table style="width: 100%;border: 1px solid #002060">
    <tbody>
    <tr>
        <td class="titre-bloc">Identification de l'échange</td>
    </tr>

    <tr>

        <td>
            <!-- objet -->
            <p style="padding-left: 4px"><b>Objet:</b></p>
            <p style="padding-left: 4px">${email.message.objet!}</p>
            <div style="margin-bottom: 10px"/>
            <!-- Emetteur -->
            <p style="padding-left: 4px"><b>Emetteur:</b></p>
            <p style="padding-left: 4px">${email.message.nomCompletExpediteur!''}</p>
            <div style="margin-bottom: 10px"/>
            <!-- Destinataire -->
            <p style="padding-left: 4px"><b>Destinataire:</b></p>
            <p style="padding-left: 4px">${email.email!}</p>
            <div style="margin-bottom: 10px"/>
        </td>
    </tr>
    </tbody>
</table>
<div style="margin-bottom: 10px"/>

<!-- Résumé des échanges -->
<table cellpadding="4" cellspacing="4" style="width: 100%; border-collapse: collapse" border="1px">
    <tbody>
    <tr>
        <td colspan="2" class="titre-bloc">Résumé des échanges</td>
    </tr>
    <tr>
        <td style="width: 50%" class="evt">Événement</td>
        <td style="width: 50%" class="evt-date">Date / Heure</td>
    </tr>
    <#if evenements?hasContent>
        <#list evenements as evt>
            <#if evt.nom == 'initial'>
                <tr>
                    <td width="80%">
                        Envoi du message par <span
                                style="font-style: italic">${email.message.nomCompletExpediteur!''}</span> à
                        <span
                                style="font-style: italic">${email.email!''}</span>
                    </td>
                    <td width="20%">
                        <#if (evt.date??)>
                            ${evt.date?string["dd/MM/yyyy"]}
                            <div style="margin-bottom: 10px"/>
                            ${evt.date?string["HH:mm:ss"]}
                        </#if>
                    </td>
                </tr>
            <#else >
                <tr>
                    <td width="80%">${evt.titre}</td>
                    <td width="20%">
                        ${evt.date?string["dd/MM/yyyy"]}
                        <div style="margin-bottom: 10px"/>
                        ${evt.date?string["HH:mm:ss"]}
                    </td>
                </tr>
            </#if>
        </#list>
    </#if>
    </tbody>
</table>
<div class="breakafter"/>
<!-- identification de la consultation -->
<div class="titre-bloc">Détail des échanges</div>
<div style="margin-bottom: 10px"/>
<table cellpadding="4" cellspacing="4" style="padding: 5px; width: 100%; border-collapse: collapse" border="1px">
    <thead>
    <!-- Courrier initial -->
    <th>
    <td>Message initial</td>
    </th>
    </thead>
    <tbody>
    <tr>
        <td>
            <#if (email.dateDemandeEnvoiAsDate??)>
                <div>
                    <span style="font-weight: bold">Date d'envoi : </span><span>${email.dateDemandeEnvoiAsDate?string["dd/MM/yyyy HH:mm:ss"]}</span>
                </div>
            </#if>
            <div><span style="font-weight: bold">Objet : </span><span>${email.message.objet!''}</span></div>
            <div><span style="font-weight: bold">De : </span><span>${email.message.nomCompletExpediteur!''}</span></div>
            <div><span style="font-weight: bold">À : </span><span>${email.email!''}</span></div>
        </td>
    </tr>
    <!-- contenu + cartouche -->
    <tr>
        <td>
            <div style="padding: 5px">${email.message.contenu!}</div>
            <div style="padding: 5px">${cartouche!}</div>
        </td>
    </tr>
    <#if email.message.piecesJointes?hasContent>
        <tr>
            <td>
                <div style="font-weight: bold">Pièce(s) jointe(s)</div>
                <ul>
                    <#list email.message.piecesJointes as pj>
                        <li class="pj"> ${pj.nom}</li>
                    </#list>
                </ul>
            </td>
        </tr>
    </#if>
    <#if email.message.dossierVolumineux??>
        <tr>
            <td>
                <div style="font-weight: bold">Dossier volumineux</div>
                <ul>
                    <li class="pj"> ${email.message.dossierVolumineux.libelle}</li>
                </ul>
            </td>
        </tr>
    </#if>
    </tbody>
</table>

<!-- Réponses -->
<#if email.toutesLesReponses?hasContent>
    <div class="breakafter"/>
    <#list email.toutesLesReponses as rep>
        <table cellpadding="4" cellspacing="4" style="padding: 5px; width: 100%; border-collapse: collapse"
               border="1px">
            <thead>
            <!-- Courrier initial -->
            <th>
            <td>Réponse</td>
            </th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div>
                        <span style="font-weight: bold">Date d'envoi : </span><span>${rep.dateEnvoiAsDate?string["dd/MM/yyyy HH:mm:ss"]}</span>
                    </div>
                    <div><span style="font-weight: bold">Objet : </span><span>${rep.message.objet!}</span></div>
                    <div><span style="font-weight: bold">De : </span><span>${rep.email!}</span></div>
                    <div>
                        <span style="font-weight: bold">À : </span><span>${rep.message.nomCompletExpediteur!}</span>
                    </div>
                </td>
            </tr>
            <#if reponseMasquee>
                <tr>
                    <td>Le contenu de la réponse sera accessible après le <span
                                class="date-limite">${dateLimiteReponse?string["dd/MM/yyyy HH:mm"]}</span></td>
                </tr>
            <#else >
                <!-- contenu + cartouche -->
                <tr>
                    <td>
                        <div style="padding: 5px">${rep.message.contenu!}</div>
                    </td>
                </tr>
                <#if rep.message.piecesJointes?hasContent>
                    <tr>
                        <td>
                            <div style="font-weight: bold">Pièce(s) jointe(s)</div>
                            <ul>
                                <#list rep.message.piecesJointes as pj>
                                    <li class="pj"> ${pj.nom}</li>
                                </#list>
                            </ul>
                        </td>
                    </tr>
                </#if>
                <#if rep.message.dossierVolumineux??>
                    <tr>
                        <td>
                            <div style="font-weight: bold">Dossier volumineux</div>
                            <ul>
                                <li class="pj"> ${rep.message.dossierVolumineux.libelle}</li>
                            </ul>
                        </td>
                    </tr>
                </#if>
            </#if>
            </tbody>
        </table>
        <div style="margin-bottom: 10px"/>
    </#list>
</#if>
</body>
</html>
