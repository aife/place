<html>
<head>
    <title></title>
</head>
<body>
<table>
    <tr>
        <td>${email.cartouche?replace("\n", "<br/>")}</td>
    </tr>
    <tr>
        <#if email.texte1??>
    <tr>
        <td>
            ${email.texte1}
        </td>
    </tr>
    </#if>
    <#if email.contenu??>
					<tr>
						<#if email.contenuHTML>
						<td>
							${email.contenu}
						</td>
						<#else>
						<td>
							${email.contenu}
						</td>
						</#if>						
					</tr>
				</#if>			
			</tr>
				<#if email.texte2??>
					<tr>
						<td>
							${email.texte2}
						</td>
					</tr>
				</#if>
				<#if email.lienAR??>
					<tr>
						<td>
							<a href='${email.lienAR}'>Suivez ce lien</a>
						</td>
					</tr>
				</#if>
			<tr>
				<td>Merci de votre int&eacute;r&ecirc;t.<br />
				<#if email.signature??>
					<!--${email.signature}-->
				</#if>
				 </td>
			</tr>
		</table>
	</body>
</html>
