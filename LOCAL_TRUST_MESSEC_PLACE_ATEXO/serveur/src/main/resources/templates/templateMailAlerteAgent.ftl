<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=HTTP_ENCODING_MAIL">
    <meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La plate-forme des Achats de l'Etat - Accusé de réception de réponse électronique</title>
    <style type="text/css">
        /* Client-specific Styles */
        body, html {
            background: #efefef;
            text-align: center;
        }

        #outlook a {
            padding: 0;
        }

        /* Force Outlook to provide a "view in browser" menu link. */
        body {
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }

        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
        .ExternalClass {
            width: 100%;
        }

        /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        /* Force Hotmail to display normal line spacing. */
        #backgroundTable, .backgroundTable, {
            margin: 0;
            padding: 0;
            width: 100% !important;
            line-height: 100% !important;
        }

        img {
            outline: none;
            text-decoration: none;
            border: none;
            -ms-interpolation-mode: bicubic;
        }

        a img {
            border: none;
        }

        .image_fix {
            display: block;
        }

        p {
            margin: 0px 0px 10px !important;
        }

        table td {
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        a {
            color: #2473B7;
            text-decoration: underline !important;
            line-height: normal;
        }

        a:hover {
            text-decoration: none !important;
        }

        .lien {
            display: inline-block;
            line-height: 13px;
        }

        .header {
            width: 100%;
            margin: 0;
            padding: 0;
        }

        /*STYLES*/
        table[class=full] {
            width: 100%;
            clear: both;
        }

        /*IPAD STYLES*/
        @media only screen and (max-width: 640px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #2473B7; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #2473B7 !important;
                pointer-events: auto;
                cursor: default;
            }

            table[class=devicewidth] {
                width: 440px !important;
                text-align: center !important;
            }

            table[class=devicewidthmob] {
                width: 420px !important;
                text-align: center !important;
            }

            table[class=devicewidthinner] {
                width: 420px !important;
                text-align: center !important;
            }

            img[class=banner] {
                width: 440px !important;
                height: 157px !important;
            }

            /*img[class=col2img] {width: 440px!important;height:330px!important;}*/
            table[class="cols3inner"] {
                width: 100px !important;
            }

            table[class="col3img"] {
                width: 100% !important;
            }

            /*img[class="col3img"] {width: 131px!important;height: 82px!important;}*/
            table[class='removeMobile'] {
                width: 10px !important;
            }

            img[class="blog"] {
                width: 420px !important;
                height: 162px !important;
            }

            .center {
                text-align: center !important;
                padding: 0 10px;
            }

            .col2img {
                margin-bottom: 25px;
            }

            .devicewidth {
                height: 0 !important;
            }
        }

        /*IPHONE STYLES*/
        @media only screen and (max-width: 480px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #2473B7; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }

            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #2473B7 !important;
                pointer-events: auto;
                cursor: default;
            }

            table[class=devicewidth] {
                width: 280px !important;
                text-align: center !important;
            }

            table[class=devicewidthmob] {
                width: 260px !important;
                text-align: center !important;
            }

            table[class=devicewidthinner] {
                width: 260px !important;
                text-align: center !important;
            }

            img[class=banner] {
                width: 280px !important;
                height: 100px !important;
            }

            /*img[class=col2img] {width: 280px!important;height:210px!important;}*/
            table[class="cols3inner"] {
                width: 260px !important;
            }

            /*img[class="col3img"] {width: 280px!important;height: 122px!important;}*/
            table[class="col3img"] {
                width: 280px !important;
            }

            img[class="blog"] {
                width: 260px !important;
                height: 100px !important;
            }

            td[class="padding-top-right15"] {
                padding: 15px 15px 0 0 !important;
            }

            td[class="padding-right15"] {
                padding-right: 15px !important;
            }

            .devicewidth {
                height: auto;
            }

            .puce {
                display: block;
                float: left;
                margin-bottom: 20px !important;
            }
        }
    </style>
</head>
<body>
<!-- Start of header -->
<table width="100%" bgcolor="#efefef" cellpadding="0" cellspacing="0" border="0" class="backgroundTable"
       st-sortable="header">
    <tbody>
    <tr>
        <td>

            <table width="560" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                <tbody>
                <!-- Spacing -->
                <tr>
                    <td height="20" bgcolor="#efefef" style=" line-height:1px; mso-line-height-rule: exactly;"></td>
                </tr>
                <!-- Spacing -->
                <!-- HEADER -->
                <tr>
                    <td bgcolor="#ffffff"
                        style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color: #2473B7;"
                        align="center">
                        <#if logoSrc??>
                            <img style="display: block; border: none; outline: none; text-decoration: none;"
                                 src="${logoSrc}"
                                 alt="le logo de la plateforme est indisponible"/>
                        </#if>
                    </td>
                </tr>
                <!-- end of HEADER -->
                <tr>
                    <td width="100%">
                        <table bgcolor="#ffffff" width="560" cellpadding="0" cellspacing="0" border="0" align="center"
                               class="devicewidth">
                            <tbody>
                            <!-- Spacing -->
                            <tr>
                                <td height="20" style=" line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                            </tr>
                            <!-- /Spacing -->
                            <tr>
                                <td>
                                    <table width="530" cellpadding="0" cellspacing="0" border="0" align="center"
                                           class="devicewidthinner">
                                        <tbody>
                                        <tr>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size: 18px; color: #2473B7; text-align:left; line-height: 20px;">
                                                <!-- Titre OBJET-->Réponse à un message sollicité - Réf
                                                : ${ref!}<!-- /Titre OBJET-->
                                            </td>
                                        </tr>                                          <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"
                                                style=" line-height:1px; mso-line-height-rule: exactly;">&nbsp;
                                            </td>
                                        </tr>
                                        <!-- /Spacing -->                                          <!-- content -->
                                        <tr>
                                            <td style="font-family:Arial, Helvetica, sans-serif; font-size: 14px; color: #767676; text-align:left; line-height: 20px;">
                                                <!-- Texte CONTENU-->
                                                <p>Bonjour,</p>
                                                <p>Un message sollicité a été déposé pour l'élément mentionné ci-après
                                                    :</p>
                                                <#list metaDonnees as metaDonne>
                                                    <p>
                                                        ${metaDonne.cle!} : ${metaDonne.valeur!}
                                                    </p>
                                                </#list>
                                                <p>NB : Un message sollicité est une réponse à un message précisant
                                                    qu'une réponse est attendue (ex : demande de complément). Ces
                                                    messages sollicités sont accessibles depuis le suivi des messages.
                                                </p>
                                                <p>Cordialement</p>
                                                <p>${nomPf}</p>
                                            </td>
                                        </tr>
                                        <!-- end of content -->
                                        <!-- Spacing -->
                                        <tr>
                                            <td width="100%" height="20"
                                                style=" line-height:1px; mso-line-height-rule: exactly;">&nbsp;
                                            </td>
                                        </tr>
                                        </tbody>
                                        <td>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                </td>
                                            </table>
                                            <!-- End of Header -->


</body>
</html>
