#Guide d'utilisation du script de fusion

#### Objectif :
Fusionner l'export des messages avec les autres parties archivées.

#### Pré requis :
- Avoir votre dossier source d'export de messages communiqué après l'export de votre message
- Dossier cible contenant vos avenants et consultations.

Le script va effectuer une comparaison entre votre dossier source et cible et ajouter les exports de messages.

Pour lancer le script de fusion.sh, veuillez ouvrir votre terminal puis écrire :

    /bin/bash ./fusion.sh /dossier-source /dossier-cible

Il suffit de suivre les indications dans le script.

En cas d'erreur, veuillez lire le fichier log qui sera généré dans la racine de votre dossier source.

Après la fin du process, votre dossier cible sera mise à jour avec les nouveaux exports d'échanges.
