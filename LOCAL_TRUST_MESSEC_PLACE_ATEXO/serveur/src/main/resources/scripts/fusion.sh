#!/bin/bash
echo "

               AAA         TTTTTTTTTTTTTTTTTTTTTTTEEEEEEEEEEEEEEEEEEEEEEXXXXXXX       XXXXXXX     OOOOOOOOO
              A:::A        T:::::::::::::::::::::TE::::::::::::::::::::EX:::::X       X:::::X   OO:::::::::OO
             A:::::A       T:::::::::::::::::::::TE::::::::::::::::::::EX:::::X       X:::::X OO:::::::::::::OO
            A:::::::A      T:::::TT:::::::TT:::::TEE::::::EEEEEEEEE::::EX::::::X     X::::::XO:::::::OOO:::::::O
           A:::::::::A     TTTTTT  T:::::T  TTTTTT  E:::::E       EEEEEEXXX:::::X   X:::::XXXO::::::O   O::::::O
          A:::::A:::::A            T:::::T          E:::::E                X:::::X X:::::X   O:::::O     O:::::O
         A:::::A A:::::A           T:::::T          E::::::EEEEEEEEEE       X:::::X:::::X    O:::::O     O:::::O
        A:::::A   A:::::A          T:::::T          E:::::::::::::::E        X:::::::::X     O:::::O     O:::::O
       A:::::A     A:::::A         T:::::T          E:::::::::::::::E        X:::::::::X     O:::::O     O:::::O
      A:::::AAAAAAAAA:::::A        T:::::T          E::::::EEEEEEEEEE       X:::::X:::::X    O:::::O     O:::::O
     A:::::::::::::::::::::A       T:::::T          E:::::E                X:::::X X:::::X   O:::::O     O:::::O
    A:::::AAAAAAAAAAAAA:::::A      T:::::T          E:::::E       EEEEEEXXX:::::X   X:::::XXXO::::::O   O::::::O
   A:::::A             A:::::A   TT:::::::TT      EE::::::EEEEEEEE:::::EX::::::X     X::::::XO:::::::OOO:::::::O
  A:::::A               A:::::A  T:::::::::T      E::::::::::::::::::::EX:::::X       X:::::X OO:::::::::::::OO
 A:::::A                 A:::::A T:::::::::T      E::::::::::::::::::::EX:::::X       X:::::X   OO:::::::::OO
AAAAAAA                   AAAAAAATTTTTTTTTTT      EEEEEEEEEEEEEEEEEEEEEEXXXXXXX       XXXXXXX     OOOOOOOOO






                                                                                                                "

echo "Votre répertoire de base est ${1}"
echo "Votre répertoire cible est ${2}"
read -p "Voulez vous lancer le process avec ces paramètres ? (Tapez 'O' pour oui et 'N' pour annuler)" -n 1 -r
echo

echo "Lancement du process :"
cd $1

if [[ $REPLY =~ ^[Oo]$ ]]; then

  for repertoireObjet in $(ls $1); do
    find $2 -iname "$repertoireObjet" | xargs -I '{}' cp -r $repertoireObjet/* {} 2>>error.txt
  done

  for repertoireObjet in $(ls "$2/avenants"); do
    cd $2/avenants/$repertoireObjet
    cp -r 6.\ Messagerie\ sécurisée/* 2.\ Messagerie\ sécurisée/
    rm -r 6.\ Messagerie\ sécurisée/
  done
  echo "Traitement terminé"
else
  echo "Process annulé"
fi
