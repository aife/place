package fr.atexo.messageriesecurisee.service.document;

import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocumentGeneratorImplTest {

    @Mock
    EmailRepository emailRepository;

    @Mock
    EmailMapper emailMapper;

    @Mock
    EmailService emailService;

    @InjectMocks
    DocumentGeneratorImpl documentGenerator;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGenererPDFPreuveEchanges() throws IOException, MessageDAOException, ServiceException {
        var email = new Email();
        email.setMessage(new Message());
        when(emailRepository.findById(any())).thenReturn(Optional.of(email));
        when(emailMapper.toDto(any()))
                .thenReturn(EmailDTO.builder()
                        .message(MessageDTO.builder()
                                .build())
                        .build());

        var pdfFile = documentGenerator.genererPDFPreuveEchanges(1, "Europe/Paris");
        assertEquals(Files.probeContentType(pdfFile.toPath()), "application/pdf");

        // cleanup
        pdfFile.delete();
    }

    @Test
    public void testGroupByCode() {
        var codes = new String[]{"c2", "C1", "c3"};
        var cv1 = CleValeurDTO.builder().code("c1").build();
        var cv2 = CleValeurDTO.builder().code("c3").build();
        var cv3 = CleValeurDTO.builder().code("Y").build();

        var result = documentGenerator.groupeByCode(asList(cv2, cv1, cv3), codes);
        assertEquals("liste clés valeur non null", result.size(), 2);
        assertEquals("liste clés valeur ordonnée", result.get(1).getCode(), "c3");
    }
}
