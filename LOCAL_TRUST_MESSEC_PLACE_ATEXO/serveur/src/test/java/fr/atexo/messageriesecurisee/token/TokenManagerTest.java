package fr.atexo.messageriesecurisee.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.model.Token;
import fr.atexo.messageriesecurisee.repository.TokenRepository;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TokenManagerTest {

    @Mock
    TokenRepository tokenRepository;

    @Mock
    LoggingService loggingService;

    @Mock
    ConfigurationManager configurationManager;

    @Spy
    ObjectMapper objectMapper;

    @InjectMocks()
    TokenManagerImpl tokenManager;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(tokenRepository.findById(any())).thenReturn(Optional.of(Token.builder().token("token").contenu("{'data':214}").build()));
    }

    @Test
    public void testGetData() throws MessageDAOException {

        for (int i = 0; i < 10; i++) {
            var data = "{'data':" + i + "}";
            tokenManager.associateData(data, "pf" + i, i, "ref" + i);
        }
        verify(tokenRepository, times(10)).save(any());
    }

    @Test
    public void testGenerateToken() {
        assertNotNull("vérification que chaque token généré est non null", tokenManager.generateToken());
    }

    @Test
    public void testAssociateData() throws MessageDAOException {
        var token = tokenManager.associateData(new SerializableRechercheMessage(), "pf", 1, "ref1");
        verify(tokenRepository, times(1)).save(any());
    }

    @Test
    public void testTestGetData() throws MessageDAOException {
        var json = "{\"idObjetMetier\":50, \"mailDestinataire\":\"mail\"}";
        var type = SerializableRechercheMessage.class.getCanonicalName();
        when(tokenRepository.findById(any())).thenReturn(Optional.of(Token.builder().type(type).token("token").contenu(json).build()));
        var data = tokenManager.getData("token");
        assertNotNull(data);
    }

    @Test
    public void testRemoveToken() throws MessageDAOException {
        var tokenOptional = Optional.of(Token.builder().token("token").build());
        when(tokenRepository.findById(any())).thenReturn(tokenOptional);

        tokenManager.removeToken("token");
        verify(tokenRepository, times(1)).delete(tokenOptional.get());
    }

    @Test
    public void testUpdateData() throws MessageDAOException {
        var tokenOptional = Optional.of(Token.builder().token("token").contenu("d1").build());
        when(tokenRepository.findById(any())).thenReturn(tokenOptional);

        tokenManager.updateData("token", "d2");
        tokenOptional.get().setContenu("d2");
        verify(tokenRepository, times(1)).save(tokenOptional.get());
    }

    @Test
    public void testRemoveExpiredTokens() {
        var tokenIds = new HashSet<String>();
        for (int i = 0; i < 2; i++) {
            tokenIds.add(String.valueOf(i));
        }
        when(tokenRepository.findByDateCreationBefore(any())).thenReturn(tokenIds);
        when(configurationManager.getTokenExpirationDelay()).thenReturn(1);

        tokenManager.removeExpiredTokens();
        verify(tokenRepository, times(2)).delete(any());
    }
}
