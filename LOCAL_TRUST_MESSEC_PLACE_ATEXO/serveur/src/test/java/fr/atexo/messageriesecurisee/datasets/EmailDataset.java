package fr.atexo.messageriesecurisee.datasets;

import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;

import java.util.Set;

public class EmailDataset {

    public static Email emailWithReponses() {
        Set<Email> reponses = Set.of(Email.builder().message(Message.builder().initialisateur(TypeInitialisateur.ENTREPRISE).build()).build());
        return Email.builder().reponses(reponses).message(Message.builder().build()).build();
    }
}
