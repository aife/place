package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.service.document.DocumentGenerator;
import fr.atexo.messageriesecurisee.token.TokenManager;
import fr.atexo.messageriesecurisee.utils.LoggingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.File;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class PreuveEchangeControllerTest {

    @InjectMocks
    PreuveEchangeController preuveEchangeController;

    @Mock
    DocumentGenerator documentGenerator;

    @Mock
    LoggingService loggingService;

    @Mock
    TokenManager tokenManager;

    private MockMvc mvc;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .standaloneSetup(preuveEchangeController)
                .build();
    }

    @Test
    void genererPreuveEchangesOK() throws Exception {
        when(tokenManager.getData(any())).thenReturn(new SerializableRechercheMessage());
        when(documentGenerator.genererPDFPreuveEchanges(anyString(), anyString())).thenReturn(File.createTempFile("test-", ".pdf"));
        mvc.perform(MockMvcRequestBuilders.get("/rest/preuves/uuid?token=token"))
                .andDo(print())
                .andExpect(header().stringValues("content-type", "application/pdf"))
                .andExpect(status().isOk());
    }
}
