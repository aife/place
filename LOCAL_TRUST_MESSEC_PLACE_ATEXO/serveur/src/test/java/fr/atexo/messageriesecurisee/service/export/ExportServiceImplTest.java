package fr.atexo.messageriesecurisee.service.export;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.repository.EmailPaginableRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ExportServiceImplTest {

    @InjectMocks
    ExportServiceImpl exportService;

    @Mock
    EmailPaginableRepository emailPaginableRepository;

    @Mock
    EmailMapper emailMapper;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void export() throws ServiceException, IOException {
        when(emailPaginableRepository.rechercher(any())).thenReturn(asList(Email.builder().build()));
        var email = EmailDTO.builder().message(MessageDTO.builder().build()).build();
        when(emailMapper.toDto(any())).thenReturn(email);
        //when(emailMapper.toDtos(any())).thenReturn(asList(email));
        var result = exportService.export(new SerializableRechercheMessage(), new ArrayList<>());
        assertNotNull("fichier généré", result);
        assertEquals("fichier excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Files.probeContentType(result.toPath()));
        assertFalse("fichier non vide", Files.readAllBytes(result.toPath()).length == 0);
        Files.deleteIfExists(result.toPath());
    }
}
