package fr.atexo.messageriesecurisee.service.email;

import fr.atexo.messageriesecurisee.datasets.EmailDataset;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.EvenementDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.mapper.EmailMapperImpl;
import fr.atexo.messageriesecurisee.mapper.MessageMapperImpl;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.ErreurSMTP;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.repository.ErreurSMTPRepository;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
@SpringBootTest
public class EmailServiceImplTest {

    @Mock
    EmailRepository emailRepository;

    @Mock
    ErreurSMTPRepository erreurSMTPRepository;

    @Mock
    AuditReader auditReader;

    @Mock
    EntityManager entityManager;

    @Mock
    EmailMapperImpl emailMapper;

    @Mock
    MessageMapperImpl messageMapper;



    @InjectMocks
    EmailServiceImpl emailService;

    public EmailServiceImplTest() {

    }


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void acquitterReponseSansReponseBloquee() throws MessageDAOException {
        var email = EmailDataset.emailWithReponses();
        when(emailRepository.findByCodeLien(any())).thenReturn(Optional.of(email));
        emailService.acquitterReponseEntreprise("codeLien");
        verify(emailRepository, times(email.getReponses().size())).save(any());
    }

    @Test
    public void acquitterReponseAvecReponseBloqueeAvantDateLimite() throws MessageDAOException {
        var email = EmailDataset.emailWithReponses();
        var dateLimite = LocalDateTime.now().plusHours(1L);
        email.getMessage().setReponseBloque(true);
        email.getMessage().setDateLimiteReponse(dateLimite);
        when(emailRepository.findByCodeLien(any())).thenReturn(Optional.of(email));
        emailService.acquitterReponseEntreprise("codeLien");
        verify(emailRepository, times(0)).save(any());
    }

    @Test
    public void acquitterReponseAvecReponseBloqueeApresDateLimite() throws MessageDAOException {
        var email = EmailDataset.emailWithReponses();
        var dateLimite = LocalDateTime.now().minusHours(1L);
        email.getMessage().setReponseBloque(true);
        email.getMessage().setDateLimiteReponse(dateLimite);
        when(emailRepository.findByCodeLien(any())).thenReturn(Optional.of(email));
        emailService.acquitterReponseEntreprise("codeLien");
        verify(emailRepository, times(email.getReponses().size())).save(any());
    }

    @Test
    public void Given_MessageWithStatutAcquitte_When_UpdateStatutMessage_Then_ThrowError() {
        Email email = Email.builder()
                .statutEmail(TypeStatutEmail.ACQUITTE)
                .build();
        when(emailRepository.findByCodeLien(anyString())).thenReturn(Optional.ofNullable(email));
        MessageDAOException exception = assertThrows(MessageDAOException.class, () -> {
            emailService.updateStatut("codelien");
        });
        assertEquals("Le statut du message ne permet pas la modification", exception.getMessage());
    }

    @Test
    public void Given_MessageWithStatutEchecEnvoi_When_UpdateStatutMessage_Then_ChangeStatut() {
        Email email = Email.builder()
                .statutEmail(TypeStatutEmail.ECHEC_ENVOI)
                .build();
        Email email2 = Email.builder()
                .statutEmail(TypeStatutEmail.TRAITE)
                .build();
        EmailDTO emailDTO = EmailDTO.builder().statutEmail(TypeStatutEmail.TRAITE).build();
        when(emailRepository.findByCodeLien(anyString())).thenReturn(Optional.ofNullable(email));
        when(emailRepository.save(any())).thenReturn(email2);
        when(emailMapper.toDto(any())).thenReturn(emailDTO);
        EmailDTO result = emailService.updateStatut("codelien");
        assertNotNull(result);
        assertEquals(TypeStatutEmail.TRAITE, result.getStatutEmail());
    }


    @Test
    public void testToggleFavoriWhenEmailExists() throws MessageDAOException {
        String codeLien = "12345";
        var email = EmailDataset.emailWithReponses();
        email.setFavori(false);
        when(emailRepository.findByCodeLien(codeLien)).thenReturn(Optional.of(email));
        when(emailRepository.save(email)).thenReturn(email);
        boolean result = emailService.toggleFavori(codeLien);
        assertTrue(result);
        assertTrue(email.getFavori());
        assertNotNull(email.getMessage().getDateModification());
        verify(emailRepository, times(1)).findByCodeLien(codeLien);
        verify(emailRepository, times(1)).save(email);
    }

    @Test
    public void testToggleFavoriWhenEmailDoesNotExist() {
        String codeLien = "12345";
        when(emailRepository.findByCodeLien(codeLien)).thenReturn(Optional.empty());
        MessageDAOException exception = assertThrows(MessageDAOException.class, () -> emailService.toggleFavori(codeLien));
        assertEquals("code lien introuvable 12345", exception.getMessage());
        verify(emailRepository, times(1)).findByCodeLien(codeLien);
        verify(emailRepository, never()).save(any(Email.class));
    }

    @Test
    public void getHistoryForEmailStatutEchecEnvoiWithKnownError() {
        prepareKnownErrors();
        try (MockedStatic<AuditReaderFactory> keyStoreMocked = mockStatic(AuditReaderFactory.class)) {
            keyStoreMocked
                    .when(() -> AuditReaderFactory.get(entityManager))
                    .thenReturn(auditReader);
            Message message = Message.builder()
                    .contenu("contenu test")
                    .initialisateur(TypeInitialisateur.ENTREPRISE)
                    .build();
            Email email = Email.builder()
                    .email("jean@atexo.com")
                    .message(message)
                    .erreur("erreur technique jean@atexo.com520 test")
                    .dateDemandeEnvoi(LocalDateTime.now())
                    .build();
            MessageDTO messageDTO = MessageDTO.builder()
                    .contenu("contenu test")
                    .initialisateur(TypeInitialisateur.ENTREPRISE)
                    .build();
            EmailDTO emailDTO = EmailDTO.builder()
                    .email("jean@atexo.com")
                    .message(messageDTO)
                    .erreur("erreur technique jean@atexo.com520 test")
                    .statutEmail(TypeStatutEmail.ECHEC_ENVOI)
                    .dateEchec(1L)
                    .dateDemandeEnvoi(1L)
                    .build();
            List<Number> revisions = new ArrayList<>();
            revisions.add(1);
            revisions.add(2);
            when(emailRepository.findByCodeLien(anyString())).thenReturn(Optional.of(email));
            when(auditReader.getRevisions(any(), any())).thenReturn(revisions);
            when(auditReader.find(any(), any(), anyInt())).thenReturn(email);
            when(emailMapper.toDto(any())).thenReturn(emailDTO);
            when(messageMapper.toDto(any())).thenReturn(messageDTO);
            List<EvenementDTO> result = emailService.getHistory("code test");
            assertEquals(1, result.size());
            assertEquals("Message non délivré",  result.get(0).getTitre());
            assertEquals("error",  result.get(0).getStyle());
            assertEquals("fa-times",  result.get(0).getIcone());
            assertEquals("520",  result.get(0).getErreurSMTP().getCode());
            assertEquals("description2",  result.get(0).getErreurSMTP().getDescription());
            assertEquals("erreur technique jean@atexo.com520 test",  result.get(0).getErreurSMTP().getErreur());
        }
    }

    @Test
    public void getHistoryForEmailStatutEchecEnvoiWithUnknownError() {
        prepareKnownErrors();
        // Create a mock AuditReader
        try (MockedStatic<AuditReaderFactory> keyStoreMocked = mockStatic(AuditReaderFactory.class)) {
            keyStoreMocked
                    .when(() -> AuditReaderFactory.get(entityManager))
                    .thenReturn(auditReader);
            Message message = Message.builder()
                    .contenu("contenu test")
                    .initialisateur(TypeInitialisateur.ENTREPRISE)
                    .build();
            Email email = Email.builder()
                    .email("jean@atexo.com")
                    .message(message)
                    .erreur("erreur technique jean@atexo.com599 test")
                    .dateDemandeEnvoi(LocalDateTime.now())
                    .build();
            MessageDTO messageDTO = MessageDTO.builder()
                    .contenu("contenu test")
                    .initialisateur(TypeInitialisateur.ENTREPRISE)
                    .build();
            EmailDTO emailDTO = EmailDTO.builder()
                    .email("jean@atexo.com")
                    .message(messageDTO)
                    .erreur("erreur technique jean@atexo.com599 test")
                    .statutEmail(TypeStatutEmail.ECHEC_ENVOI)
                    .dateEchec(1L)
                    .dateDemandeEnvoi(1L)
                    .build();
            List<Number> revisions = new ArrayList<>();
            revisions.add(1);
            revisions.add(2);
            when(emailRepository.findByCodeLien(anyString())).thenReturn(Optional.of(email));
            when(auditReader.getRevisions(any(), any())).thenReturn(revisions);
            when(auditReader.find(any(), any(), anyInt())).thenReturn(email);
            when(emailMapper.toDto(any())).thenReturn(emailDTO);
            when(messageMapper.toDto(any())).thenReturn(messageDTO);
            List<EvenementDTO> result = emailService.getHistory("code test");
            assertEquals(1, result.size());
            assertEquals("Message non délivré",  result.get(0).getTitre());
            assertEquals("error",  result.get(0).getStyle());
            assertEquals("fa-times",  result.get(0).getIcone());
            assertNull(result.get(0).getErreurSMTP().getCode());
            assertNull(result.get(0).getErreurSMTP().getDescription());
            assertEquals("erreur technique jean@atexo.com599 test",  result.get(0).getErreurSMTP().getErreur());
        }
    }

    private void prepareKnownErrors() {
        ErreurSMTP erreur1 = new ErreurSMTP();
        erreur1.setCode("510");
        erreur1.setLabel("description1");
        ErreurSMTP erreur2 = new ErreurSMTP();
        erreur1.setCode("520");
        erreur1.setLabel("description2");
        List<ErreurSMTP> erreurs = new ArrayList<>();
        erreurs.add(erreur1);
        erreurs.add(erreur2);
        when(erreurSMTPRepository.findAll(Sort.by(Sort.Direction.DESC, "code"))).thenReturn(erreurs);
    }

}
