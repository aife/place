package fr.atexo.messageriesecurisee.service.validation;

import fr.atexo.messageriesecurisee.config.ClamavProperties;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class FileValidationServiceImplTest {

    @InjectMocks()
    FileValidationServiceImpl fileValidationService;

    @Mock
    ClamavProperties clamavProperties;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void givenValidFileWhenActivatedClamavReturnValid() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/test.txt");
        when(clamavProperties.isEnabled()).thenReturn(true);
        when(clamavProperties.getHost()).thenReturn("antivirus.local-trust.com");
        when(clamavProperties.getPort()).thenReturn(3310);
        boolean result = fileValidationService.checkFile(fileInputStream);
        assertTrue(result);
    }
    @Test
    public void givenWrongConfigurationWhenActivatedClamavReturnValid() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/test.txt");
        when(clamavProperties.isEnabled()).thenReturn(true);
        when(clamavProperties.getHost()).thenReturn("antivirus.local-trust.com");
        when(clamavProperties.getPort()).thenReturn(3311);
        boolean result = fileValidationService.checkFile(fileInputStream);
        assertTrue(result);
    }
    @Test
    public void givenVirusConfigurationWhenActivatedClamavReturnValid() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/test-virus.txt");
        when(clamavProperties.isEnabled()).thenReturn(true);
        when(clamavProperties.getHost()).thenReturn("antivirus.local-trust.com");
        when(clamavProperties.getPort()).thenReturn(3310);
        boolean result = fileValidationService.checkFile(fileInputStream);
        assertFalse(result);
    }

    @Test
    public void givenFileWhenDeactivatedClamavReturnValid() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("src/test/resources/test.txt");
        when(clamavProperties.isEnabled()).thenReturn(false);
        boolean result = fileValidationService.checkFile(fileInputStream);
        assertTrue(result);
    }


}
