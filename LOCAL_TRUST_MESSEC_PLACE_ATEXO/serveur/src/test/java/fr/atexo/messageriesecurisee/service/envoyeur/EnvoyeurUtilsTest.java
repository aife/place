package fr.atexo.messageriesecurisee.service.envoyeur;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;


public class EnvoyeurUtilsTest {

    String eximlogs;

    @InjectMocks
    EnvoyeurUtils envoyeurUtils;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.eximlogs = getClass().getClassLoader().getResource("mainlog").getFile();
    }

    @Test
    public void listeJetonLog() {
        var idStmp = asList("TTTTTT-000001-Oa", "TTTTTT-000001-Ob", "TTTTTT-000001-Oc", "TTTTTT-000001-Od");
        var tokens = envoyeurUtils.listeJetonLog(idStmp, eximlogs);
        assertFalse("jetons envoyés", tokens.isEmpty());
        assertEquals("jetons OK", tokens.stream().filter(t -> EtatPostFix.OK.equals(t.getEtat())).collect(Collectors.toList()).size(), 1);
        assertEquals("jetons KO", tokens.stream().filter(t -> EtatPostFix.KO.equals(t.getEtat())).collect(Collectors.toList()).size(), 1);
        assertEquals("jetons INDETERMINE", tokens.stream().filter(t -> EtatPostFix.INDETERMINE.equals(t.getEtat())).collect(Collectors.toList()).size(), 2);
    }

    @Test
    public void listerJetonFileNotFound() {
        var idStmp = asList("TTTTTT-000001-Oa", "TTTTTT-000001-Ob", "TTTTTT-000001-Oc", "TTTTTT-000001-Od");
        var tokens = envoyeurUtils.listeJetonLog(idStmp, "file not found path");
        assertTrue("File not found", tokens.isEmpty());
    }
}
