package fr.atexo.messageriesecurisee.rest;

import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.service.email.EmailService;
import fr.atexo.messageriesecurisee.token.TokenManager;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
class ReponseLegacyControllerTest {

    @InjectMocks
    ReponseLegacyController reponseLegacyController;

    @Mock
    EmailService emailService;

    @Mock
    TokenManager tokenManager;

    @Mock
    RestTemplate restTemplate;

    private MockMvc mvc;

    private static final String URL_VISUALISATION = "https://mpe-develop.local-trust.com/index.php?page=entreprise.VisualiserEchangeExterne";

    @BeforeEach
    public void init() {
        try (var mocks = openMocks(this)) {
            mvc = MockMvcBuilders
                    .standaloneSetup(reponseLegacyController)
                    .build();
        } catch (Exception e) {
            log.error("Erreur lors de l'initialisation des mocks", e);
        }
    }

    @Test
    void testBuildNotificationContratURL() {
        var uuid = UUID.randomUUID().toString();
        var url = reponseLegacyController.buildNotificationContratURL(URL_VISUALISATION, uuid);
        assertEquals(url, "https://mpe-develop.local-trust.com/exec-api/api/contrat/notification/" + uuid);
    }

    @Test
    void testBuildNotificationActeURL() {
        var uuid = UUID.randomUUID().toString();
        var url = reponseLegacyController.buildNotificationActeURL(URL_VISUALISATION, uuid);
        assertEquals(url, "https://mpe-develop.local-trust.com/exec-api/api/actes/notification/" + uuid);
    }

    @Test
    void testNotificationCall() throws Exception {
        var codeLien = "codeLien";
        var uuid = UUID.randomUUID().toString();
        var urlVisualisation = "https://mpe-develop.local-trust.com/telechargement?codelien=" + codeLien + "&contrat=" + uuid;
        when(emailService.findEmailEmailByCodeLien(any())).thenReturn(EmailDTO.builder().message(MessageDTO.builder()
                .urlPfDestinataireVisualisation(urlVisualisation)
                .build()).build());
        var recherche = new RechercheMessage();
        recherche.setCodeLien(codeLien);
        when(tokenManager.getData(anyString())).thenReturn(recherche);
        mvc.perform(MockMvcRequestBuilders.get("/telechargement?codelien=codelien&contrat=uuid"))
                .andExpect(status().isOk());
        verify(restTemplate, times(1)).postForEntity(anyString(), any(), any());
    }
}
