package fr.atexo.messageriesecurisee.service.message;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.exceptions.MessageDAOException;
import fr.atexo.messageriesecurisee.exceptions.PieceJointeIntrouvableException;
import fr.atexo.messageriesecurisee.exceptions.ServiceException;
import fr.atexo.messageriesecurisee.mapper.EmailMapper;
import fr.atexo.messageriesecurisee.mapper.MessageMapper;
import fr.atexo.messageriesecurisee.model.Email;
import fr.atexo.messageriesecurisee.model.Message;
import fr.atexo.messageriesecurisee.model.PieceJointe;
import fr.atexo.messageriesecurisee.repository.EmailPaginableRepository;
import fr.atexo.messageriesecurisee.repository.EmailRepository;
import fr.atexo.messageriesecurisee.repository.MessageRepository;
import fr.atexo.messageriesecurisee.service.configuration.ConfigurationManager;
import fr.atexo.messageriesecurisee.service.envoyeur.EmailEnvoiService;
import fr.atexo.messageriesecurisee.token.TokenManager;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Year;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class MessageServiceImplTest {

    @InjectMocks()
    MessageServiceImpl messageService;

    @Mock
    ConfigurationManager configurationManager;

    @Mock
    TokenManager tokenManager;

    @Mock
    EmailPaginableRepository emailPaginableRepository;

    @Mock
    EmailRepository emailRepository;

    @Mock
    MessageRepository messageRepository;

    @Mock
    EmailMapper emailMapper;

    @Mock
    EmailEnvoiService emailEnvoiService;

    @Mock
    MessageMapper messageMapper;

    Message message;

    SerializableRechercheMessage critere = new SerializableRechercheMessage();

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        message = new Message();
        message.setReferenceObjetMetier("ref-objet-mertier");
        message.setIdentifiantPfEmetteur("id-pf-emeteur");
        message.setReponseAttendue(true);

        PieceJointe pc = new PieceJointe();
        new File("src/test/resources/testFiles/").mkdirs();
        var fileTest = File.createTempFile("hello", ".tmp", new File("src/test/resources/testFiles/"));
        pc.setNom(fileTest.getName());
        pc.setChemin(fileTest.getAbsolutePath());

        message.setPiecesJointes(List.of(pc));
        when(configurationManager.getRepertoireTelechargement()).thenReturn("src/test/resources/testFiles/");
        when(tokenManager.getData(any())).thenReturn(critere);
        when(emailPaginableRepository.rechercher(any())).thenReturn(Collections.singletonList(Email.builder().email("email1").build()));
        when(emailRepository.findByCodeLien(any())).thenReturn(Optional.of(Email.builder().email("email1").build()));
        when(messageMapper.toDto(any())).thenReturn(new MessageDTO());
        critere.setCodeLien("codeLien");

    }

    @Test
    public void testRepertotyFolder() throws IOException, PieceJointeIntrouvableException {

        messageService.persisteUploadedfiles(message);
        var file = Paths.get(configurationManager.getRepertoireTelechargement(), message.getIdentifiantPfEmetteur(), String.valueOf(Year.now().getValue()), message.getReferenceObjetMetier()).toFile();
        assertTrue(file.exists());
        FileUtils.deleteDirectory(new File("src/test/resources/testFiles"));
    }

    @Test
    public void initMessageEntrepriseControleMailFalse() throws ServiceException, MessageDAOException {
        critere.setMailDestinataire("email1");
        var emailDto = buildEmail("email1");
        when(emailMapper.toDto(any())).thenReturn(emailDto);
        var email = messageService.initialiseMessageEntreprise("token");
        assertNotNull(email);
        assertFalse("controle mail désactivé par défaut", critere.isControleMail());
        assertTrue("connecté", email.isConnecte());
        assertFalse("réponses visibles", email.getToutesLesReponses().isEmpty());

    }

    @Test
    public void initMessageEntrepriseControleMailTrue() throws ServiceException, MessageDAOException {
        var emailDto = buildEmail("email2");
        when(emailMapper.toDto(any())).thenReturn(emailDto);
        critere.setControleMail(false);
        critere.setMailDestinataire("email2");
        var email = messageService.initialiseMessageEntreprise("token");
        assertNotNull(email);
        assertTrue("connecté", email.isConnecte());
        assertFalse("réponses visibles", email.getToutesLesReponses().isEmpty());
    }

    @Test
    public void initMessageEntrepriseConnecteControleMailTrue() throws ServiceException, MessageDAOException {
        var emailDto = buildEmail("email1");
        when(emailMapper.toDto(any())).thenReturn(emailDto);
        critere.setMailDestinataire("email1");
        critere.setControleMail(true);
        var email = messageService.initialiseMessageEntreprise("token");
        assertNotNull(email);
        assertTrue("connecté", email.isConnecte());
        assertFalse("réponses visibles", email.getToutesLesReponses().isEmpty());
    }

    @Test
    public void initMessageEntrepriseNonConnecte() throws ServiceException, MessageDAOException {
        var emailDto = buildEmail("email1");
        when(emailMapper.toDto(any())).thenReturn(emailDto);
        critere.setMailDestinataire(null);
        var email = messageService.initialiseMessageEntreprise("token");
        assertNotNull(email);
        assertFalse("non connecté", email.isConnecte());
        assertTrue("réponses non visibles", email.getToutesLesReponses().isEmpty());

    }

    @Test
    public void saveOrCreateMessageInitialTiersVersAgent() throws ServiceException, MessageDAOException {
        Set<Email> destinataires = new HashSet<>();
        Message message2 = new Message();
        message2.setReferenceObjetMetier("ref-objet-mertier");
        message2.setIdentifiantPfEmetteur("id-pf-emeteur");
        message2.setDestinataires(destinataires);
        message2.setEmailsAlerteReponse("agent@atexo.com");
        message2.setInitialisateur(TypeInitialisateur.ENTREPRISE);
        var result = messageService.saveOrCreateMessage(message2, null, false);
        verify(emailEnvoiService).ennvoyerAlerteMessageEntreprise(any());
        verify(emailEnvoiService).envoyerAcquittementEnvoi(any());
        verify(emailEnvoiService, never()).envoyerAcquittementReponseEntreprise(any(), any());
        verify(emailEnvoiService, never()).envoyerAlerte(any(), any());
        assertNotNull(result);
    }

    @Test
    public void saveOrCreateMessageReponseTiersVersAgent() throws ServiceException, MessageDAOException {
        Email mailInitial = Email.builder()
                .message(message)
                .build();
        Set<Email> destinataires = new HashSet<>();
        Message message2 = new Message();
        message2.setReferenceObjetMetier("ref-objet-mertier");
        message2.setIdentifiantPfEmetteur("id-pf-emeteur");
        message2.setDestinataires(destinataires);
        message2.setEmailsAlerteReponse("agent@atexo.com");
        message2.setInitialisateur(TypeInitialisateur.ENTREPRISE);
        var result = messageService.saveOrCreateMessage(message2, mailInitial, false);
        verify(emailEnvoiService).envoyerAcquittementReponseEntreprise(any(), any());
        verify(emailEnvoiService).envoyerAlerte(any(), any());
        verify(emailEnvoiService, never()).ennvoyerAlerteMessageEntreprise(any());
        verify(emailEnvoiService, never()).envoyerAcquittementEnvoi(any());
        assertNotNull(result);
    }




    private EmailDTO buildEmail(String email) {
        var reponses = Collections.singletonList(EmailDTO.builder().email("rep1").build());
        return EmailDTO.builder().email(email).toutesLesReponses((List<EmailDTO>) reponses).message(MessageDTO.builder().build()).build();
    }

}
