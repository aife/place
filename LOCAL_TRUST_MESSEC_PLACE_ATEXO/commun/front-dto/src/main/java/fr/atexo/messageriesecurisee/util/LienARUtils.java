package fr.atexo.messageriesecurisee.util;

import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;

/**
 * Classe de generation desw lien à insérer dans les messages
 * 
 * @author ARD
 * 
 */
public final class LienARUtils {
	
    private LienARUtils() {
        super();
    }

    /**
     * Creer un lien AR pour un email en fonction de l'argument booléen :
     * <ul>
     * <li><code>true</code> vers la servlet de visualisation, n'entraînant pas
     * alors l'acquittement du message.</li>
     * <li><code>false</code> vers l'url de plateforme correspodant au type de
     * destinataire de l'email.</li>
     * </ul>
     * @param pourVisualisation <code>false</code> s'il s'agit du lien de
     *            consulation du mail par un destinataire ; <code>true</code>
     *            dans le cas de la visualisation après recherche via le suivi
     *            des messages.
     * @param urlPfDestinataire url de la plateforme destinataire
     * @param urlPfEmetteur url de la plateforme emmeteur
     * @param typeDestinataire le type de l'email
     * @param codeLien le code lien de l'email
     * @param addPjToken uniquement ajouter dans le cas de l'utilisatin de token
     * @return un lien AR
     */
    public static String creerLienAR(boolean pourVisualisation,
            String urlPfDestinataire, String urlPfEmetteur,
            TypeDestinataire typeDestinataire, String codeLien,boolean addPjToken) {
        StringBuffer ret = new StringBuffer();
        String urlToAdd = null;
        if (pourVisualisation) {
            urlToAdd = ConstantServletParameters.NOM_SERVLET_VISUALISATION;
        } else {
            urlToAdd = urlPfDestinataire;
            if (
                typeDestinataire == TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE) {
                urlToAdd = urlPfEmetteur;
            }
        }
        if(urlToAdd.contains(ConstantServletParameters.TOKEN_CODE_LIEN)){
        	return urlToAdd.replace(ConstantServletParameters.TOKEN_CODE_LIEN, ConstantServletParameters.CODE_LIEN+"="+codeLien+(addPjToken?ConstantServletParameters.TOKEN_PIECE_JOINTE:""));
        } else {
	        ret.append(urlToAdd).append(urlToAdd.contains("?") ? "&" : "?")
	                .append(ConstantServletParameters.CODE_LIEN).append("=")
	                .append(codeLien);
	        return ret.toString();
        }
    }
}
