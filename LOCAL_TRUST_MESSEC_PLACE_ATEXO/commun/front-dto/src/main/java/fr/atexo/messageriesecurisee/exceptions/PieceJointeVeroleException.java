package fr.atexo.messageriesecurisee.exceptions;

public class PieceJointeVeroleException extends AtexoException {

    public PieceJointeVeroleException(String nomPj) {
        super("Le fichier " + nomPj + " est vérolé");
    }
}
