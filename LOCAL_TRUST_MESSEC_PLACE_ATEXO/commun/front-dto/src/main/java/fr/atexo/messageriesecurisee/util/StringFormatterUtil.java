package fr.atexo.messageriesecurisee.util;

import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Safelist;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class StringFormatterUtil {

	protected static final String BR = "<BR/>";
	protected static final String LISTE_DEBUT = "<ul>";
	protected static final String LISTE_FIN = "</ul>";
	protected static final String ANCRE_DEBUT_1 = "<a href=\"";
	protected static final String ANCRE_DEBUT_2 = "\">";
	protected static final String ANCRE_FIN = "</a>";
	protected static final String LISTE_ITEM_DEBUT = "<li>";
	protected static final String LISTE_ITEM_FIN = "</li>";

	protected static String asString(String... composantes) {
		StringBuilder sb = new StringBuilder();
		for (String composante : composantes) {
			if (sb.length() > 0) {
				sb.append(" - ");
			}
			if (composante != null) {
				sb.append(composante.trim());
			}
		}
		return sb.toString();
	}

	/**
	 * <b>Pour la visualisation du message et de ses pièces jointes.</b>
	 * Retourne un lien pour un email et un message donne.
	 *
	 * @param message
	 *            le message (contient les url des plateformes)
	 * @param email
	 *            l'email (pour le code line)
	 * @return un lien AR
	 */
	public static String creerLienAR(MessageDTO message, EmailDTO email, boolean addPjToken) {
		return LienARUtils.creerLienAR(true, message.getUrlPfDestinataire(), message.getUrlPfEmetteur(),
				email.getTypeDestinataire(), email.getCodeLien(), addPjToken);
	}

	public static String genereExpediteur(EmailDTO email) {
		return asString(email.getMessage().getNomCompletExpediteur(), email.getMessage().getEmailExpediteur());
	}

	public static String genereDestinataire(EmailDTO email) {
		return asString(email.getNomEntreprise(), email.getNomContact(), email.getEmail());
	}

	public static List<String> getNomsDesPiecesJointes(EmailDTO email) {
		List<String> result = new ArrayList<String>();
		List<PieceJointeDTO> listPJ = email.getMessage().getPiecesJointes();
		if (listPJ != null && !listPJ.isEmpty()) {
			for (Object pieceJointe : listPJ) {
				PieceJointeDTO pj = (PieceJointeDTO) pieceJointe;
				result.add(pj.getNom());
			}
		}
		return result;
	}

	public static List<Integer> getTaillesDesPiecesJointes(EmailDTO email) {
		List<Integer> result = new ArrayList<Integer>();
		List<PieceJointeDTO> listPJ = email.getMessage().getPiecesJointes();
		if (listPJ != null && !listPJ.isEmpty()) {
			for (Object pieceJointe : listPJ) {
				PieceJointeDTO pj = (PieceJointeDTO) pieceJointe;
				result.add(pj.getTaille());
			}
		}
		return result;
	}

	public static List<String> getUriDesPiecesJointes(EmailDTO email) {
		List<String> result = new ArrayList<String>();
		MessageDTO message = email.getMessage();
		String rootAR = creerLienAR(message, email, true);
		List<PieceJointeDTO> listPJ = message.getPiecesJointes();
		if (listPJ != null && !listPJ.isEmpty()) {
			for (Object pieceJointe : listPJ) {
				result.add(fabriqueUriPieceJointe((PieceJointeDTO) pieceJointe, rootAR));
			}
		}
		return result;
	}

	protected static String fabriqueUriPieceJointe(PieceJointeDTO pj, String rootAR) {
		if (rootAR.contains(ConstantServletParameters.TOKEN_PIECE_JOINTE)) {
			StringBuilder pjParam = new StringBuilder("&");
			pjParam.append(ConstantServletParameters.CODE_PIECE_JOINTE).append("=").append(pj.getId());
			return rootAR.replace(ConstantServletParameters.TOKEN_PIECE_JOINTE, pjParam.toString());
		} else {
			StringBuilder result = new StringBuilder();
			result.append(rootAR).append("&").append(ConstantServletParameters.CODE_PIECE_JOINTE).append("=").append(pj.getId());

			return result.toString();
		}
	}

	public static String convertHtmlToText(String htmlText) {
		String text = null;
		if (htmlText != null) {
			Document document = Jsoup.parse(htmlText);
			document.outputSettings(new Document.OutputSettings().prettyPrint(false));//makes html() preserve linebreaks and spacing
			document.select("br").append("\\n");
			document.select("p").prepend("\\n");
			String s = document.html().replaceAll("\\\\n", "\n");
			return Jsoup.clean(s, "", Safelist.none(), new Document.OutputSettings().prettyPrint(false)).replace("&nbsp;", " ").trim();
		}
		return text;
	}

	public static String format(double bytes, int digits) {
		String[] dictionary = {"octets", "Ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo"};
		int index;
		for (index = 0; index < dictionary.length; index++) {
			if (bytes < 1024) {
				break;
			}
			bytes = bytes / 1024;
		}
		var decimalValue = BigDecimal.valueOf(bytes).setScale(digits, RoundingMode.CEILING);
		return decimalValue + " " + dictionary[index];
	}
}
