package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ObjetMetierDTO extends AbstractDTO {

    private int identifiant;
    private String reference;
    private String objet;
    private String urlSuivi;
    private String urlRetour;
    private String creePar;
}
