package fr.atexo.messageriesecurisee.exceptions;

/**
 * Message exception
 */
public class MessageNonValideException extends AtexoException {
	public MessageNonValideException(String message) {
		super("Le message n'est pas valide : "+message);
	}
}
