package fr.atexo.messageriesecurisee.exceptions;

import lombok.Getter;

@Getter
public class AtexoException extends RuntimeException {
    protected final String message;

    public AtexoException(String errorMessage,  Throwable err) {
        super(errorMessage, err);
        this.message = errorMessage;
    }

    public AtexoException(String errorMessage) {
        super(errorMessage);
        this.message = errorMessage;
    }
}
