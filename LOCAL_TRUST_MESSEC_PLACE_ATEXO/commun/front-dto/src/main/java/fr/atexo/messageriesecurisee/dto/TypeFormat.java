package fr.atexo.messageriesecurisee.dto;

/**
 * Created by MZO on 07/07/2016.
 */
public enum TypeFormat {

    PDF(".pdf"), EXCEL(".xls"), WORD(".docx");

    private String extension;

    public String getExtension() {
        return extension;
    }

    private TypeFormat(String extension) {
        this.extension = extension;
    }

}
