package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

import static fr.atexo.messageriesecurisee.util.StringFormatterUtil.format;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class DossierVolumineuxDTO extends AbstractDTO {

    private Long id;

    private String nom;

    private Long taille;

    private String uuidReference;

    private String uuidTechnique;

    public String getLibelle() {
        return Optional.ofNullable(nom).orElse("") + " (Identifiant: " + Optional.ofNullable(uuidReference).orElse("") + " - Taille: " + format(Optional.ofNullable(taille).orElse(0L), 2) + ")";
    }
}
