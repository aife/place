package fr.atexo.messageriesecurisee.util;

public interface ConstantServletParameters {
	String PREFIX_RE = "Re : ";
	/**
	 * Clef pour le libelle taille maximum un fichier
	 */
	String CLE_MAX_TAILLE_UN_FICHIER = "taille_max_un_fichier";
	/**
	 * Clef pour le libelle taille maximum tous fichiers
	 */
	String CLE_MAX_TAILLE_TOUS_FICHIER = "taille_max_tous_fichier";

	String TOKEN = "token";
	String GENERER_ENREGISTRER = "saveitoo";
	String TYPE_HTML_PAGE = "typehtmlpage";
	String FORMAT_EXPORT = "format";

	String CODE_LIEN = "codelien";
	String CODE_PIECE_JOINTE = "piecejointe";
	String TOKEN_CODE_LIEN = "{CODE_LIEN}";
	String TOKEN_PIECE_JOINTE = "{PJ}";

	String TYPE_FORMAT_XLS = "xls";
	String TYPE_FORMAT_PDF = "pdf";

	/**
	 * Pour la session
	 */
	String CONTEXTE_CRITERE = "critere_contexte";

	String NOM_SERVLET_VISUALISATION = "visualisation";
}
