package fr.atexo.messageriesecurisee.dto;

import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import fr.atexo.messageriesecurisee.validator.XSSConstraint;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.math.BigInteger;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class MessageDTO extends AbstractDTO {

    private Integer id;

    private String referenceObjetMetier;

    private TypeMessage typeMessage;

    private Boolean masquerOptionsEnvoi;

    private String cartouche;

    private String signatureAvisPassage;

    private String nomCompletExpediteur;

    private String nomContactExpediteur;

    private String emailExpediteur;

    private String emailReponseExpediteur;

    /**
     * Sert d'adresse destinataire dans le cas d'une réponse
     */
    private String emailContactDestinatairePfEmetteur;

    private Integer idMsgInitial;

    @XSSConstraint
    private String contenu;

    @Builder.Default
    private boolean contenuHTML = true;

    @XSSConstraint
    private String objet;

    private boolean reponseAttendue;

    private boolean reponseAgentAttendue;

    private List<CritereDTO> criteres = new ArrayList<>();

    private List<PieceJointeDTO> piecesJointes = new ArrayList<>();

    private TypeStatutEmail statut;

    private List<TypeMessage> typesInterdits;

    private List<DestinataireDTO> destsPfEmettreur = new ArrayList<>();

    private List<DestinataireDTO> destsPfDestinataire = new ArrayList<>();

    private List<DestinataireDTO> destinatairesPreSelectionnes = new ArrayList<>();

    private List<CleValeurDTO> metaDonnees = new ArrayList<>();

    private boolean dateLimiteReponseAttendue = false;

    private boolean reponseBloque;

    private Long dateLimiteReponse;

    private Date dateLimiteReponseAsDate;

    private String emailsAlerteReponse;

    private boolean dateReponseAttendue;

    private boolean reponseLectureBloque;

    private String dateReponse;

    private Date dateReponseAsDate;

    private Integer nbDestinataires;

    private List<DestinataireDTO> destinatairesNotifies;

    private BigInteger maxTailleFichiers;

	private Long maxCaracteresContenu;

    @Builder.Default
    private Map<TypeMessage, Long> limitationTailleFichier = new HashMap<>();

    private String templateMail;

    private boolean templateFige;

    private String codeLien;

    private boolean brouillon;

    private List<DossierVolumineuxDTO> dossiersVolumineux = new ArrayList<>();

    private DossierVolumineuxDTO dossierVolumineux;

    private String libelleDossierVolumineux;

    public Map<TypeMessage, Long> getLimitationTailleFichier() {
        if (limitationTailleFichier == null) {
            limitationTailleFichier = new HashMap<>();
        }
        limitationTailleFichier.put(TypeMessage.TypeMessage1, 5242880L);
        limitationTailleFichier.put(TypeMessage.TypeMessage4, 62914560L);
        return limitationTailleFichier;
    }

    private String identifiantPfEmetteur;

    private String identifiantPfDestinataire;

    private String nomPfEmetteur;

    private String nomPfDestinataire;

    private String urlPfEmetteur;

    private String urlPfDestinataire;

    private String urlPfEmetteurVisualisation;

    private String urlPfDestinataireVisualisation;

    private String urlPfReponse;

    private Integer identifiantObjetMetierPlateFormeDestinataire;

    private Integer identifiantObjetMetierPlateFormeEmetteur;

    private Integer identifiantSousObjetMetier;

    private TypeInitialisateur initialisateur;

    public String getContenuText() {
        return truncateHtml(contenu, 350);
    }

    public String getObjetText() {
        return truncateHtml(objet, 150);
    }

    private String truncateHtml(String html, int maxLength) {
        if (html == null) {
            return null;
        }
        var result = html.replaceAll("<[^>]*>", "").replace("&nbsp;", " ");
        if (result.length() > maxLength) {
            result = result.substring(0, maxLength) + " ...";
        }
        return result;
    }

}
