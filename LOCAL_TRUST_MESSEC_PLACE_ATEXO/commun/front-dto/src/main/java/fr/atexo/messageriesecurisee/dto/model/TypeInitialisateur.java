package fr.atexo.messageriesecurisee.dto.model;

public enum TypeInitialisateur {

    AGENT, ENTREPRISE;
}
