package fr.atexo.messageriesecurisee.dto;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(of = {"mailContactDestinataire", "type"})
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class DestinataireDTO extends AbstractDTO {
    private TypeDestinataire typeDestinataire;
    private String idEntrepriseDest;
    private Integer idContactDest;
    private String nomEntrepriseDest;
    private String nomContactDest;
    private String mailContactDestinataire;
    private String type;
    private Integer ordre;

    public DestinataireDTO(TypeDestinataire typeDestinataire,
            String id_entreprise_dest, String id_contact_dest,
            String nom_entreprise_dest, String nomContactDest,
            String mail_contact_destinataire) {
        this.typeDestinataire = typeDestinataire;
        this.idEntrepriseDest = id_entreprise_dest;
        try {
            this.idContactDest = Integer.valueOf(id_contact_dest);
        } catch (NumberFormatException e) {
            this.idContactDest = -1;
        }
        this.nomEntrepriseDest = nom_entreprise_dest;
        this.nomContactDest = nomContactDest;
        this.mailContactDestinataire = mail_contact_destinataire;
    }

}
