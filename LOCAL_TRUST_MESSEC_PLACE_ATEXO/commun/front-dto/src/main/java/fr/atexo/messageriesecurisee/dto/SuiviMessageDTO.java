package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class SuiviMessageDTO extends AbstractDTO {

    private Integer id;

    private String nomEntreprise;

    private String nomContact;

    private String email;

    private String dateEnvoi;

    private String dateAR;

    private String message;
}
