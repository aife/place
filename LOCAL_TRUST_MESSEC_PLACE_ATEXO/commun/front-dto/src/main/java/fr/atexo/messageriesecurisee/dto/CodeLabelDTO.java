package fr.atexo.messageriesecurisee.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CodeLabelDTO {

    String code;
    String label;
}
