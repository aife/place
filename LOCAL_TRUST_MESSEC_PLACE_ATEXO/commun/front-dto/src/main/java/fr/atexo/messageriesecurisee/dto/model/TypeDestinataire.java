package fr.atexo.messageriesecurisee.dto.model;

/**
 * Les types de destinataire des Emails
 *
 * @author ARD
 */
public enum TypeDestinataire {

	DESTINATAIRE_PLATEFORME_EMETTRICE("Destinataire plateforme emettrice"),
	DESTINATAIRE_PLATEFORME_DESTINATAIRE("Destinataire plateforme destinataire");

	private final String libelle;

	private TypeDestinataire(String libelle) {
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
    }

}
