package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class FiltreDTO extends AbstractDTO {
    Boolean statutEnCourEnvoi;
    Boolean statutNonDelivre;
    Boolean statutDelivre;
    Boolean statutAcquitte;
    Boolean dateEnvoiStatut;
    Boolean reponseAttendue;
    Boolean reponseLue;
    Boolean reponseNonLue;
    Boolean statutTraite;
}
