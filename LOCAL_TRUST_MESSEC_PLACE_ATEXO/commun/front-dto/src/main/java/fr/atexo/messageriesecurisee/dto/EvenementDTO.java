package fr.atexo.messageriesecurisee.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
@EqualsAndHashCode(of = {"nom", "date"})
public class EvenementDTO extends AbstractDTO {

    private String nom;

    private Date date;

    private EmailDTO reponse;

    private String titre;

    private String style;

    private String icone;

    private ErreurSMTP erreurSMTP;

}
