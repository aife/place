package fr.atexo.messageriesecurisee.dto.model;

/**
 * Les statut d'un email et d'un message
 *
 * @author ARD
 */
public enum TypeStatutEmail {

	// pour un email
	BROUILLON("Brouillon", 0),
	EN_ATTENTE_ENVOI("En attente d'envoi", 1),
	EN_COURS_ENVOI("En cours d'envoi", 2),
	ECHEC_ENVOI("Echec envoi", 3),
	ENVOYE("Envoyé", 4),
	EN_ATTENTE_ACQUITTEMENT("En attente d'acquittement", 5),
	ACQUITTE("Acquitté", 6),

	// pour le message ( ensemble des emails )
	ECHEC_PARTIEL("Echec partiel", 7),
	ACQUITTEMENT_PARTIEL("Acquittement partiel", 8),

	// pour un email
	TRAITE("Traité dans un autre envoi", 9);

	private String libelle;
	private int etat;

	private TypeStatutEmail(String libelle, int etat) {
		this.etat = etat;
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public int getEtat() {
		return etat;
	}
}
