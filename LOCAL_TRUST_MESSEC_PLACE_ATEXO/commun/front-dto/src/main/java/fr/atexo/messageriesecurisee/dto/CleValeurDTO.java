package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class CleValeurDTO extends AbstractDTO {

    private Long id;

    private String cle;

    private String code;

    private String valeur;

    public CleValeurDTO(String cle, String code, String valeur) {
        this.cle = cle;
        this.code = code;
        this.valeur = valeur;
    }
}
