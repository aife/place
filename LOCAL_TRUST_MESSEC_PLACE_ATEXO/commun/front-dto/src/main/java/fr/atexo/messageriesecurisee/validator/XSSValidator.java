package fr.atexo.messageriesecurisee.validator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Safelist;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.stream.Collectors;

public class XSSValidator implements
		ConstraintValidator<XSSConstraint, String> {

	private final Cleaner cleaner = new Cleaner(
			Safelist
					.relaxed()
                    // HTML 5 Tags
                    .addTags( "a",  "abbr",  "acronym",  "address",  "applet",  "area",  "article",  "aside",  "audio",  "b",  "base",  "basefont",  "bdi",  "bdo",  "big",  "blockquote",  "body",  "br",  "button",  "canvas",  "caption",  "center",  "cite",  "code",  "col",  "colgroup",  "data",  "datalist",  "dd",  "del",  "details",  "dfn",  "dialog",  "dir",  "div",  "dl",  "dt",  "em",  "embed",  "fieldset",  "figcaption",  "figure",  "font",  "footer",  "form",  "frame",  "frameset",  "head",  "header",  "hgroup", "hr",  "html",  "i",  "iframe",  "img",  "input",  "ins",  "kbd",  "keygen",  "label",  "legend",  "li",  "link",  "main",  "map",  "mark",  "menu",  "menuitem",  "meta",  "meter",  "nav",  "noframes",  "noscript",  "object",  "ol",  "optgroup",  "option",  "output",  "p",  "param",  "picture",  "pre",  "progress",  "q",  "rp",  "rt",  "ruby",  "s",  "samp",  "script",  "section",  "select",  "small",  "source",  "span",  "strike",  "strong",  "style",  "sub",  "summary",  "sup",  "svg",  "table",  "tbody",  "td",  "template",  "textarea",  "tfoot",  "th",  "thead",  "time",  "title",  "tr",  "track",  "tt",  "u",  "ul",  "var",  "video",  "wbr")
					.addAttributes(":all", "role", "accesskey", "class", "contenteditable", "contextmenu", "dir", "draggable", "dropzone", "hidden", "id", "lang", "spellcheck", "style", "tabindex", "title")
					.addAttributes("table", "align", "bgcolor", "border", "cellpadding", "cellspacing", "frame", "rules", "summary", "width", "vertical-align")
					.addAttributes("td", "abbr", "align", "axis", "bgcolor", "char", "charoff", "colspan", "headers", "rowspan", "scope", "valign", "vertical-align")
					.addAttributes("a", "charset", "coords", "datafld", "datasrc", "href", "hreflang", "media", "methods", "name", "rel", "rev", "shape", "target", "_self", "_blank", "_parent", "_top", "type", "urn")
	);

	@Override
	public boolean isValid(String unsafeHtml, ConstraintValidatorContext cxt) {
		if (unsafeHtml == null || unsafeHtml.isEmpty()) {
			return true;
		}
		var result = Jsoup.parse(unsafeHtml);

		// Les commentaires invalident le document, on les supprime avant de tester
		removeComments(result);

		return cleaner.isValid(result);
	}

	private void removeComments(Node node) {
		var nodes = node.childNodes().stream()
				.filter(Objects::nonNull)
				.filter(n -> "#comment".equals(n.nodeName())).collect(Collectors.toSet());

		for (Node n : nodes) {
			n.remove();
		}

		node.childNodes().forEach(this::removeComments);
	}
}
