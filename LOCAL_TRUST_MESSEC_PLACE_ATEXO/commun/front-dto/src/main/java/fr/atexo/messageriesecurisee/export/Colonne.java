package fr.atexo.messageriesecurisee.export;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Colonne {

    Objet("Objet de l'échange"), Type("Type de message"), Statut("Statut de l'échange"), Emetteur("Emetteur"), Destinataire("Destinataire"), Contenu("Corps du message"), PiecesJointes("Pièces jointes"), DateDemandeEnvoi("Envoyé le"), DateEnvoi("Délivré le"), DateAR("Lu le");

    private String libelle;
}
