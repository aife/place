package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MessageStatusDTO extends AbstractDTO {

    private Integer nombreMessageNonLu;
    private Integer nombreMessageNonDelivre;
    private Integer nombreMessageEnAttenteReponse;
    private Integer nombreMessages;
    private Integer idObjetMetier;
    private String referenceObjetMetier;
}
