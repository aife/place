package fr.atexo.messageriesecurisee.exceptions;

public class ServiceException extends AtexoException {

    public static final String AUCUN_CRITERE_PLATEFORME = "aucun critère associé au token ";

    public ServiceException(String message) {
        super(message);
    }
}
