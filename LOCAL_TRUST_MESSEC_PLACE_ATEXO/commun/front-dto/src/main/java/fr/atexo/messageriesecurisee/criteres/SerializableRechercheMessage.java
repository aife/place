package fr.atexo.messageriesecurisee.criteres;

import com.fasterxml.jackson.annotation.JsonFormat;
import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.dto.DossierVolumineuxDTO;
import fr.atexo.messageriesecurisee.dto.FiltreDTO;
import fr.atexo.messageriesecurisee.dto.ObjetMetierDTO;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import lombok.*;

import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SerializableRechercheMessage {

    public static String REP_ATT_OUI = "oui";

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private String token;

    // -----------------------------------------------
    // Critères d'affichage définis par la plateforme
    // -----------------------------------------------

    /**
     * Plateforme Emetteur ou Destinataire
     */
    private TypeDestinataire typePlateformeRecherche = TypeDestinataire.EMETTEUR;

    /**
     * Identifiant de la plateforme concernée
     */
    private String idPlateformeRecherche;

    /**
     * Identifiant de l'objet métier
     */
    private Integer idObjetMetier;

    /**
     * Identifiants de l'objet métier
     */
    private Set<Integer> identifiantsObjetsMetier;

    /**
     * Identifiants de l'objet métier
     */
    private Set<Integer> identifiantsObjetsMetierAgentConnecte = new HashSet<>();

    private Integer idSousObjetMetier;
    /**
     * Identifiant du contact destinataire sur la plateforme selectionnée
     */
    private List<Integer> idContactDestList;

    /**
     * Identifiant de l'entreprise destinataire sur la plateforme sélectionnée
     */
    private String idEntrepriseDest;

    /**
     * Adresse email du destinataire sur la plateforme Emetteur ou sur la
     * plateforme Destinataire
     */
    private String mailDestinataire;

    /**
     * Adresse email de l'expéditeur sur la plateforme Emetteur ou sur la
     * plateforme Destinataire
     */
    private String mailExpediteur;

    // -----------------------------------------------
    // Critères de recherche définis par l'utilisateur
    // -----------------------------------------------

    /**
     * Référence de l'objet métier
     */
    private String refObjetMetier;

    /**
     * RéférenceS objets métier
     */
    private Set<String> referencesObjetsMetier;


    /**
     * Object du message
     */
    private String objetMessage;

    /**
     * Destinataire (recherche approximative sur le nom et le mail)
     */
    private List<String> nomOuMailDestList;

    private Set<String> emailsAlerteReponse = new HashSet<>();

    /**
     * Expéditeur (recherche approximative sur le nom et le mail)
     */
    private String nomOuMailExp;

    private Date dateEnvoiDebut;

    private Date dateEnvoiFin;

    private Date dateARDebut;

    private Date dateARFin;

    private Collection<Integer> ids;

    /**
     * Statut du message : liste déroulante
     */
    private List<TypeStatutEmail> statuts;

    private List<TypeStatutEmail> statutsAExclure;

    private Criteres criteres;

    // -----------------------------------------------
    // Critères de recherche supplémentaires
    // -----------------------------------------------

    private Boolean reponseAttendue;

    private String typeMessage;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    private Date dateDemandeEnvoiDebut;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    private Date dateDemandeEnvoiFin;

    private String pieceJointe;

    private String pieceJointeNom;

    private String idMessageDestinataire;

    private String motsCles;

    private Boolean reponseNonLue;

    private Boolean reponseLue;

    private Boolean favoris;

    private Boolean enAttenteReponse;

    private boolean inclureReponses = true;

    private boolean exclureReponses = false;

    private Boolean sansReponse = false;

    private Integer idMessageInitial;

    private String codeLien;

    private List<CleValeurDTO> metaDonnees = new ArrayList<>();

    private List<DossierVolumineuxDTO> dossiersVolumineux = new ArrayList<>();

    private List<ObjetMetierDTO> objetsMetier = new ArrayList<>();

    private FiltreDTO filtresAppliques;

    private boolean controleMail;

    private boolean afficherFiltreEntreprise;

    private TypeInitialisateur initialisateur;

    private String contact;

    private Boolean mesConsultations;

    public enum TypeDestinataire {

        EMETTEUR("emetteur"), DESTINATAIRE("destinataire"), BROUILLON("brouillon");
        private final String value;

        TypeDestinataire(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static TypeDestinataire fromValue(String v) {
            for (TypeDestinataire c : TypeDestinataire.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }

    public static class Criteres {

        private List<Critere> critere;

        public List<Critere> getCritere() {
            if (critere == null) {
                critere = new ArrayList<Critere>();
            }
            return this.critere;
        }

        public static class Critere {

            private String nom;

            private String valeur;

            public Critere() {
            }

            public Critere(String nom, String valeur) {
                this.nom = nom;
                this.valeur = valeur;
            }

            public String getNom() {
                return nom;
            }

            public void setNom(String value) {
                this.nom = value;
            }

            public String getValeur() {
                return valeur;
            }

            public void setValeur(String value) {
                this.valeur = value;
            }
        }
    }

}
