package fr.atexo.messageriesecurisee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class PieceJointeDTO extends AbstractDTO {

	private Integer id;

	private String idExterne;

	private String reference;

	private String nom;

	private String chemin;

	private Integer taille;

	private String tailleFormatee;

	private String contentType;

}
