package fr.atexo.messageriesecurisee.exceptions;

public class PieceJointeIntrouvableException extends AtexoException {

	public PieceJointeIntrouvableException(String nomPj) {
		super("Le fichier " + nomPj + " est introuvable");
	}
}
