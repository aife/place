package fr.atexo.messageriesecurisee.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContexteDTO {

    @JsonProperty("token")
    private String token;

    @JsonProperty("access_token")
    private String accessToken;

    @JsonProperty("id_template_selectionne")
    private String idTemplateSelectionne;


    @JsonProperty("templates_path")
    private String templatesPath;

    @JsonProperty("url_suivi")
    private String urlSuivi;

    @JsonProperty("url_retour_consultation")
    private String urlRetourConsultation;

    @JsonProperty("url_nouveau_message")
    private String urlNouveauMessage;

    @JsonProperty("url_modification_message")
    private String urlModificationMessage;

    @JsonProperty("url_visualisation_message")
    private String urlVisualisationMessage;

    @JsonProperty("url_visualisation_message_legacy")
    private String urlVisualisationMessageLegacy;

    @JsonProperty("mode_legacy")
    @Builder.Default
    private Boolean modeLegacy = null;

    @JsonProperty("peut_creer_message")
    private Boolean peutCreerMessage;

    @JsonProperty("peut_modifier_message")
    private Boolean peutModifierMessage;

}
