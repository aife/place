package fr.atexo.messageriesecurisee.dto;

import fr.atexo.messageriesecurisee.dto.model.TypeDestinataire;
import fr.atexo.messageriesecurisee.dto.model.TypeInitialisateur;
import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString(exclude = {"message", "toutesLesReponses"})
public class EmailDTO extends AbstractDTO {

    private Integer id;

    private TypeDestinataire typeDestinataire;

    private String identifiantObjetMetier;

    private String identifiantEntreprise;

    private Integer identifiantContact;

    private String nomEntreprise;

    private String nomContact;

    private String email;

    private String emailConnecte;

    private TypeStatutEmail statutEmail;

    private String codeLien;

    private Long dateDemandeEnvoi;

    private Long dateEnvoi;

    private Long dateAR;

    private Integer nombreEssais;

    private String erreur;

    private String identifiantEntrepriseAR;

    private String identifiantContactAR;

    private String nomEntrepriseAR;

    private String nomContactAR;

    private String emailContactAR;

    private MessageDTO message;

    private boolean reponseExpiree = false;

    @Builder.Default
    private List<EmailDTO> toutesLesReponses = new ArrayList<>();

    private Long dateModificationBrouillon;

    private boolean connecte;

    private Long dateRelance;

    private Long dateEchec;

    private Boolean favori;

    public Date getDateDemandeEnvoiAsDate() {
        if (dateDemandeEnvoi == null) {
            return null;
        }
        return new Date(dateDemandeEnvoi);
    }

    public Date getDateEnvoiAsDate() {
        if (dateEnvoi == null) {
            return null;
        }
        return new Date(dateEnvoi);
    }

    public Date getDateARAsDate() {
        if (dateAR == null) {
            return null;
        }
        return new Date(dateAR);
    }

    public Date getDateModificationBrouillonAsDate() {
        if (dateModificationBrouillon == null) {
            return null;
        }
        return new Date(dateModificationBrouillon);
    }

    public Date getDateRelanceAsDate() {
        if (dateRelance == null) {
            return null;
        }
        return new Date(dateRelance);
    }

    public Date getDateEchecAsDate() {
        if (dateEchec == null) {
            return null;
        }
        return new Date(dateEchec);
    }

    public boolean isReponseMasquee() {
        return !toutesLesReponses.isEmpty() && message.isReponseBloque() && message.getDateLimiteReponseAsDate() != null && message.getDateLimiteReponseAsDate().after(new Date());
    }

    public boolean isReponseExpiree() {
        return !toutesLesReponses.isEmpty() && message != null && message.getDateLimiteReponseAsDate() != null && toutesLesReponses.stream().filter(r -> r.getDateEnvoiAsDate() != null && r.getDateEnvoiAsDate().after(message.getDateLimiteReponseAsDate())).count() > 0;
    }

    public boolean isDateLimiteExpiree() {
        return message != null && message.getDateLimiteReponseAsDate() != null && getDateEnvoiAsDate() != null && getDateEnvoiAsDate().after(message.getDateLimiteReponseAsDate());
    }

    public boolean isReponseBloquee() {
        return message != null && message.getDateLimiteReponseAsDate() != null && message.isReponseBloque() && new Date().before(message.getDateLimiteReponseAsDate());
    }

    public List<EmailDTO> getToutesLesReponses() {
        toutesLesReponses.sort(Comparator.comparing(EmailDTO::getId));
        return toutesLesReponses;
    }

    public EmailDTO getReponse() {
        // envoie la derniere réponse
        if (toutesLesReponses == null || toutesLesReponses.isEmpty()) {
            return null;
        }
        var nbElements = toutesLesReponses.size();
        return getToutesLesReponses().get(nbElements - 1);
    }

    // statuts calculés
    public boolean isEnAttenteEnvoi() {
        return statutEmail == TypeStatutEmail.EN_ATTENTE_ENVOI || statutEmail == TypeStatutEmail.EN_COURS_ENVOI;
    }

    public boolean isEchecEnvoi() {
        return statutEmail == TypeStatutEmail.ECHEC_ENVOI;
    }

    public boolean isDelivre() {
        return statutEmail == TypeStatutEmail.ENVOYE || statutEmail == TypeStatutEmail.ACQUITTE || statutEmail == TypeStatutEmail.EN_ATTENTE_ACQUITTEMENT;
    }

    public boolean isLu() {
        return message != null && TypeMessage.TypeMessage1 != message.getTypeMessage() && statutEmail == TypeStatutEmail.ACQUITTE;
    }

    public boolean isEnvoye() {
        return statutEmail == TypeStatutEmail.ENVOYE;
    }

    public boolean isEmailEntreprise() {
        return message != null && message.getInitialisateur() == TypeInitialisateur.ENTREPRISE;
    }

    public boolean isTraite() {
        return statutEmail == TypeStatutEmail.TRAITE;
    }

}
