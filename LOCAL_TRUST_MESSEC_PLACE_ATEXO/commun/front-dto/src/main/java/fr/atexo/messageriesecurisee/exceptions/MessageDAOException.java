package fr.atexo.messageriesecurisee.exceptions;

/**
 * Exception générique lancée lors d'une erreur DAO
 *
 * @author ARD
 */
public class MessageDAOException extends AtexoException {


    public static final String CODE_LIEN_INTROUVABLE = "code lien introuvable ";


    public MessageDAOException(String message) {
        super(message);
    }

    public MessageDAOException(Exception ex) {
        super(ex.getMessage(), ex);
    }
}
