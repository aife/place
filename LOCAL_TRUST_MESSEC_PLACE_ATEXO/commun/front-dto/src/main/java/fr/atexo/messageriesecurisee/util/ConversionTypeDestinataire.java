package fr.atexo.messageriesecurisee.util;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage.TypeDestinataire;

public class ConversionTypeDestinataire {

    private ConversionTypeDestinataire() {
        return;
    }

    public static fr.atexo.messageriesecurisee.dto.model.TypeDestinataire convert(
            TypeDestinataire type) {
        fr.atexo.messageriesecurisee.dto.model.TypeDestinataire result = null;
        if (type != null) {
            switch (type) {
                case DESTINATAIRE:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
                    break;
                case EMETTEUR:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE;
                    break;
                default:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
                    break;
            }
        }
        return result;
    }

    public static TypeDestinataire convert(
            fr.atexo.messageriesecurisee.dto.model.TypeDestinataire type) {
        TypeDestinataire result = null;
        if(type != null) {
            switch (type) {
                case DESTINATAIRE_PLATEFORME_DESTINATAIRE:
                    result = TypeDestinataire.DESTINATAIRE;
                    break;
                case DESTINATAIRE_PLATEFORME_EMETTRICE:
                    result = TypeDestinataire.EMETTEUR;
                    break;
            }
        }
        return result;
    }

}
