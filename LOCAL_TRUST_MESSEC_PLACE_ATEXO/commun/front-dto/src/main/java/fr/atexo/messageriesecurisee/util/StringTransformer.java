package fr.atexo.messageriesecurisee.util;

import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitaire pour préserver l'affichage HTML des chaines de caractères
 * saisies.
 * 
 * @author ARD
 * 
 */
public class StringTransformer {

    public enum TransformationVersHtml {
        RETOUR_A_LA_LIGNE("\n", "<BR/>");

        private String aRemplacer;

        private String remplacerPar;

        private TransformationVersHtml(String aRemplacer, String remplacerPar) {
            this.aRemplacer = aRemplacer;
            this.remplacerPar = remplacerPar;
        }

        String getaRemplacer() {
            return aRemplacer;
        }

        String getRemplacerPar() {
            return remplacerPar;
        }
    }

    private StringTransformer() {
        return;
    }
    
    /**
     * Effectue les transformations listées dans l'enum {@link TransformationVersHtml}
     * dans l'ordre.
     * 
     * @param contenu
     *            la chaine de caractère à modifier
     * @return la chaine de caractère modifiée
     */
    public static String traitementGlobalPourHtml(String contenu) {
        return traitementFinPourHtml(Arrays.asList(TransformationVersHtml.values()),
                contenu);
    }

    /**
     * Effectue les transformations listées dans l'ordre.
     * 
     * @param transformationsAEffectuer
     * @param contenu
     * @return
     */
    public static String traitementFinPourHtml(
            List<TransformationVersHtml> transformationsAEffectuer, String contenu) {
        String result = contenu;
        for (TransformationVersHtml transfo : transformationsAEffectuer) {
            result = result.replace(transfo.getaRemplacer(),
                    transfo.getRemplacerPar());
        }
        return result;
    }
}
