package fr.atexo.messageriesecurisee.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ToString
public class ErreurSMTP {
    private String code;
    private String description;
    private String erreur;
}
