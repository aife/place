package fr.atexo.messageriesecurisee.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConfigurationDTO {
    private int delaiPooling;
    private boolean antivirusActif;
}
