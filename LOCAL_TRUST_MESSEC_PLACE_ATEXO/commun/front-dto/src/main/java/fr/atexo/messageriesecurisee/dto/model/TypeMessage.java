package fr.atexo.messageriesecurisee.dto.model;

/**
 * Le type de message. Voir la description de chacun d'eux.
 *
 * @author ARD
 */
public enum TypeMessage {
    /**
     * Instance retournée par certaines méthodes afin d'éviter <code>null</code>
     * .
     */
    Unknown("Ne doit pas exister dans les enregistrements", 0, ""),
    /**
     * Un courrier électronique simple est envoyé au destinataire. Le contenu
     * intégral du message et les pièces jointes sont disponibles directement
     * dans le courrier électronique reçu. Aucun accusé de réception n’est
     * effectué.
     */
    TypeMessage1("Type message 1", 1, "option_envoi_1"),
    /**
     * Le contenu intégral du message et les pièces jointes sont disponibles
     * directement dans le courrier électronique reçu. Un lien AR à cliquer
     * permet également à par chaque destinataire d’envoyer un accusé de
     * réception. Le clic sur le lien AR est à la discrétion du destinataire et
     * non obligatoire pour voir le contenu.
     */
    TypeMessage2("Type message 2", 2, "option_envoi_2"),
    /**
     * Le contenu du message est disponible directement dans le courrier
     * électronique reçu. Les pièces-jointes sont à télécharger en cliquant sur
     * le lien disponible dans le mail. Un accusé de réception est envoyé
     * préalablement à l’accès au contenu des pièces-jointes.
     */
    TypeMessage3("Type message 3", 3, "option_envoi_3"),
    /**
     * Le courrier électronique reçu ne contient pas le message mais uniquement
     * un lien de téléchargement obligatoire pour accéder au contenu et aux
     * pièces-jointes du message. Un accusé de réception est envoyé
     * préalablement à l’accès au contenu du message et des pièces-jointes.
     */
    TypeMessage4("Type message 4", 4, "option_envoi_4");

    private final String libelle;
    private final int index;
    private final String optionDEnvoiMsg;

    private TypeMessage(String libelle, int type, String optionDEnvoiMsg) {
        this.index = type;
        this.libelle = libelle;
        this.optionDEnvoiMsg = optionDEnvoiMsg;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getIndex() {
        return index;
    }

    public String getOptionDEnvoiMsg() {
        return optionDEnvoiMsg;
    }

}
