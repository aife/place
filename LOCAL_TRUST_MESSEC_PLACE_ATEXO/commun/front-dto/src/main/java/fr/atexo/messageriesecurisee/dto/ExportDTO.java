package fr.atexo.messageriesecurisee.dto;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ExportDTO extends AbstractDTO {
    private List<CritereDTO> templates;
    private SerializableRechercheMessage critere;
}
