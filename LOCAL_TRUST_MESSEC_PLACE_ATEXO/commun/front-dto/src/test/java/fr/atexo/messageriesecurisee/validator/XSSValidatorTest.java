package fr.atexo.messageriesecurisee.validator;

import fr.atexo.messageriesecurisee.dto.MessageDTO;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class XSSValidatorTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void checkUnsafeHtmlScript() {
        var unsafeHtml = "<script>alert('unsafe')</script>";
        var message = MessageDTO.builder().contenu(unsafeHtml).objet(unsafeHtml).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void checkUnsafeHtmlButton() {
        var unsfeHtml = "<p><br><button autofocus onfocus =\"alert('unsafe !')\">.</button></p>";
        var message = MessageDTO.builder().contenu(unsfeHtml).objet(unsfeHtml).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertFalse(violations.isEmpty());
    }

    @Test
    public void checkSafeHtml() {
        var message = MessageDTO.builder().contenu("<p><ol><li>safe one</li><li>safe two</li></ol></p>").build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void checkSafeHtmlLink() {
        var message = MessageDTO.builder().contenu("<a href='https://www.atexo.com'>click here</a>").build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void checkSafeRealContent() {
        var content = "<p>Bonjour,</p>\n" +
                "<p><strong>TEXTE COURRIER LIBRE</strong></p>\n" +
                "<p>Merci de votre intérêt pour cette consultation.</p>\n" +
                "<p>La plate-forme de dématérialisation des marchés publics</p>";
        var message = MessageDTO.builder().contenu(content).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void checkSafeMPECartouche() {
        var content = "<div style=\"color: #757575; background-color: #fafafa; box-shadow: 1px 2px 2px #c0c0c0; border: 1px solid #dedede; border-radius: 3px; padding: 15px 15px 0 15px;\">\n" +
                "    <h2 style=\" font-size: 18px; color: #333333; font-weight: normal; margin-top: 5px;\">Consultation concernée par cet échange</h2>\n" +
                "    <hr style=\"border-top: solid 1px #e5e5e5;\"/>\n" +
                "    <ul style=\"list-style-type: none; padding-left: 0px;  font-size: 14px;\">\n" +
                "        <li style=\"margin-bottom: 10px; line-height: 20px;\">\n" +
                "            <img style=\"vertical-align: top;\" src=\"https://mpe-release.local-trust.com//app/img/tag.png\" alt=\"Référence | Intitulé\">\n" +
                "            <b>Référence | Intitulé :</b>\n" +
                "            REDAC_2222 | Test complet de REDAC 2\n" +
                "        </li>\n" +
                "        <li style=\"margin-bottom: 10px; line-height: 20px;\">\n" +
                "            <img style=\"vertical-align: top;\" src=\"https://mpe-release.local-trust.com//app/img/quote.png\" alt=\"Objet\">\n" +
                "            <b>Objet :</b> Test complet de REDAC grace aux canevas de tests.\n" +
                "        </li>\n" +
                "        <li>\n" +
                "            <hr style=\"border-top: solid 1px #e5e5e5;\"/>\n" +
                "        </li>\n" +
                "        <li style=\"margin-bottom: 10px; line-height: 20px;\">\n" +
                "            <img style=\"vertical-align: top;\" src=\"https://mpe-release.local-trust.com//app/img/calendar.png\" alt=\"Date et heure limite de remise des plis\">\n" +
                "            <b>Date et heure limite de remise des plis :</b>\n" +
                "                            31/12/2030 17:30\n" +
                "                    </li>\n" +
                "        <li style=\"margin-bottom: 10px; line-height: 20px;\">\n" +
                "            <img style=\"vertical-align: top;\" src=\"https://mpe-release.local-trust.com//app/img/bank.png\" alt=\"Organisme | Entité d&#039;achat\">\n" +
                "            <b>Organisme | Entité d&#039;achat :</b>\n" +
                "            Organisme de démonstration ATEXO\n" +
                "                    </li>\n" +
                "        <li>\n" +
                "            <hr style=\"border-top: solid 1px #e5e5e5;\"/>\n" +
                "        </li>\n" +
                "                    </ul>\n" +
                "</div>\n";
        var message = MessageDTO.builder().cartouche(content).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void checkSafeMessageWithStyle() {
        var content = "<p>Bonjour,</p><p><strong>TEXTE COURRIER </strong><strong style=\"color: rgb(255, 153, 0);\">LIBRE</strong></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>";
        var message = MessageDTO.builder().cartouche(content).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void checkSafeSUBContenu() {
        var content = "<div style=\"font-family: Verdana, sans-serif; color: #333; padding: 15px 0;\">" +
                "    <div style=\"word-spacing:normal;background-color:#E7E7E7;\">" +
                "        <div style=\"display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;\">" +
                "            Invitation à vous inscrire sur le sourcing Nukema" +
                "        </div>" +
                "        <div style=\"background-color:#E7E7E7;\">" +
                "            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                   style=\"background:#003B5C;background-color:#003B5C;width:100%;\">" +
                "                <tbody>" +
                "                <tr>" +
                "                    <td>" +
                "                        <!--[if mso | IE]>" +
                "                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\"" +
                "                               width=\"600\" bgcolor=\"#003B5C\">" +
                "                            <tr>" +
                "                                <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">" +
                "                        <![endif]-->" +
                "                        <div style=\"margin:0px auto;max-width:600px;\">" +
                "                            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                   style=\"width:100%;\">" +
                "                                <tbody>" +
                "                                <tr>" +
                "                                    <td style=\"direction:ltr;font-size:0px;padding:20px 0;text-align:center;\">" +
                "                                        <!--[if mso | IE]>" +
                "                                        <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "                                            <tr>" +
                "                                                <td class=\"\" style=\"vertical-align:top;width:600px;\">" +
                "                                        <![endif]-->" +
                "                                        <div class=\"mj-column-per-100 mj-outlook-group-fix\"" +
                "                                             style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                   style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                <tbody>" +
                "                                                <tr>" +
                "                                                    <td align=\"center\"" +
                "                                                        style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"" +
                "                                                               role=\"presentation\"" +
                "                                                               style=\"border-collapse:collapse;border-spacing:0px;\">" +
                "                                                            <tbody>" +
                "                                                            <tr>" +
                "                                                                <td style=\"width:150px;\">" +
                "                                                                    <img alt height=\"auto\"" +
                "                                                                         src=\"https://atexo-anm-rec.local-trust.com//assets/images/logo-annonces-marches-white.png\"" +
                "                                                                         style=\"border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;\"" +
                "                                                                         width=\"150\">" +
                "                                                                </td>" +
                "                                                            </tr>" +
                "                                                            </tbody>" +
                "                                                        </table>" +
                "                                                    </td>" +
                "                                                </tr>" +
                "                                                <tr>" +
                "                                                    <td align=\"center\"" +
                "                                                        style=\"font-size:0px;padding:10px 25px;padding-top:30px;word-break:break-word;\">" +
                "                                                        <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:bold;letter-spacing:1px;line-height:24px;text-align:center;text-transform:uppercase;color:#ffffff;\">" +
                "                                                            Invitation à vous inscrire au sourcing" +
                "                                                        </div>" +
                "                                                    </td>" +
                "                                                </tr>" +
                "                                                </tbody>" +
                "                                            </table>" +
                "                                        </div>" +
                "                                        <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                                    </td>" +
                "                                </tr>" +
                "                                </tbody>" +
                "                            </table>" +
                "                        </div>" +
                "                        <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                    </td>" +
                "                </tr>" +
                "                </tbody>" +
                "            </table>" +
                "            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">" +
                "                <tbody>" +
                "                <tr>" +
                "                    <td>" +
                "                        <!--[if mso | IE]>" +
                "                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\"" +
                "                               width=\"600\">" +
                "                            <tr>" +
                "                                <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">" +
                "                        <![endif]-->" +
                "                        <div style=\"margin:0px auto;max-width:600px;\">" +
                "                            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                   style=\"width:100%;\">" +
                "                                <tbody>" +
                "                                <tr>" +
                "                                    <td style=\"direction:ltr;font-size:0px;padding:20px 0;text-align:center;\">" +
                "                                        <!--[if mso | IE]>" +
                "                                        <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "                                            <tr>" +
                "                                                <td class=\"\" style=\"vertical-align:top;width:600px;\">" +
                "                                        <![endif]-->" +
                "                                        <div class=\"mj-column-per-100 mj-outlook-group-fix\"" +
                "                                             style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                   style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                <tbody>" +
                "                                                </tbody>" +
                "                                            </table>" +
                "                                        </div>" +
                "                                        <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                                    </td>" +
                "                                </tr>" +
                "                                </tbody>" +
                "                            </table>" +
                "                        </div>" +
                "                        <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                    </td>" +
                "                </tr>" +
                "                </tbody>" +
                "            </table>" +
                "            <!--[if mso | IE]>" +
                "            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body-section-outlook\"" +
                "                   style=\"width:600px;\" width=\"600\">" +
                "                <tr>" +
                "                    <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">" +
                "            <![endif]-->" +
                "            <div class=\"body-section\"" +
                "                 style=\"-webkit-box-shadow: 1px 4px 11px 0px rgba(0, 0, 0, 0.15); -moz-box-shadow: 1px 4px 11px 0px rgba(0, 0, 0, 0.15); box-shadow: 1px 4px 11px 0px rgba(0, 0, 0, 0.15); margin: 0px auto; max-width: 600px;\">" +
                "                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                       style=\"width:100%;\">" +
                "                    <tbody>" +
                "                    <tr>" +
                "                        <td style=\"direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0;text-align:center;\">" +
                "                            <!--[if mso | IE]>" +
                "                            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "                                <tr>" +
                "                                    <td class=\"\" width=\"600px\">" +
                "                                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\"" +
                "                                               style=\"width:600px;\" width=\"600\" bgcolor=\"#ffffff\">" +
                "                                            <tr>" +
                "                                                <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">" +
                "                            <![endif]-->" +
                "                            <div style=\"background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;\">" +
                "                                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                       style=\"background:#ffffff;background-color:#ffffff;width:100%;\">" +
                "                                    <tbody>" +
                "                                    <tr>" +
                "                                        <td style=\"direction:ltr;font-size:0px;padding:20px 0;padding-left:15px;padding-right:15px;text-align:center;\">" +
                "                                            <!--[if mso | IE]>" +
                "                                            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "                                                <tr>" +
                "                                                    <td class=\"\" style=\"vertical-align:top;width:570px;\">" +
                "                                            <![endif]-->" +
                "                                            <div class=\"mj-column-per-100 mj-outlook-group-fix\"" +
                "                                                 style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                       style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                    <tbody>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:20px;font-weight:bold;line-height:24px;text-align:left;color:#212b35;\">" +
                "                                                                Marc Passet (Mairie de Saint-Pierre)" +
                "                                                                souhaiterait vous inviter à rejoindre gratuitement le" +
                "                                                                sourcing Nukema" +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:400;line-height:24px;text-align:left;color:#637381;\">" +
                "                                                                Bonjour Ismail Atitallah," +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:400;line-height:24px;text-align:left;color:#637381;\">" +
                "                                                                Notre outil de sourcing permet de mettre directement en" +
                "                                                                relation les acheteurs publics et les entreprises" +
                "                                                                privées. Afin de pouvoir interagir avec vous et vous" +
                "                                                                contacter directement dans le cadre de procédures" +
                "                                                                d'achats publics Marc Passet souhaiterait" +
                "                                                                vous inviter à créer une fiche sur notre plateforme." +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:400;line-height:24px;text-align:left;color:#637381;\">" +
                "                                                                L'inscription ne prend que quelques minutes. Seuls un" +
                "                                                                SIRET et une adresse mail sont obligatoires. Les autres" +
                "                                                                éléments pourront être enrichis au fur et à mesure pour" +
                "                                                                permettre à davantage d'acheteurs de vous trouver via" +
                "                                                                notre outil. <br> <br> Pour créer votre profil, cliquez" +
                "                                                                sur le bouton ci-dessous :" +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"center\" vertical-align=\"middle\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"" +
                "                                                                   role=\"presentation\"" +
                "                                                                   style=\"border-collapse:separate;width:300px;line-height:100%;\">" +
                "                                                                <tr>" +
                "                                                                    <td align=\"center\" bgcolor=\"#003B5C\"" +
                "                                                                        role=\"presentation\"" +
                "                                                                        style=\"border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#003B5C;\"" +
                "                                                                        valign=\"middle\">" +
                "                                                                        <a href=\"https://atexo-anm-rec.local-trust.com//#/sourcing\"" +
                "                                                                           style=\"display:inline-block;width:250px;background:#003B5C;color:#ffffff;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:17px;font-weight:bold;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;\"" +
                "                                                                           target=\"_blank\"> Créer mon profil </a>" +
                "                                                                    </td>" +
                "                                                                </tr>" +
                "                                                            </table>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:400;line-height:24px;text-align:left;color:#637381;\">" +
                "                                                                <br> En espérant vous aider à travailler facilement avec" +
                "                                                                des acheteurs publics.<br>" +
                "                                                                <br> Cordialement,<br> L'équipe Nukema" +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:10px;font-weight:400;line-height:24px;text-align:left;color:#637381;\">" +
                "                                                                Astuce : si vous souhaitez que nous vous aidions dans" +
                "                                                                votre veille des marchés publics, n'hésitez pas à" +
                "                                                                consulter notre moteur de recherche sur notre site" +
                "                                                                internet." +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    </tbody>" +
                "                                                </table>" +
                "                                            </div>" +
                "                                            <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                                        </td>" +
                "                                    </tr>" +
                "                                    </tbody>" +
                "                                </table>" +
                "                            </div>" +
                "                            <!--[if mso | IE]></td></tr></table></td></tr>" +
                "                            <tr>" +
                "                                <td class=\"\" width=\"600px\">" +
                "                                    <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\"" +
                "                                           style=\"width:600px;\" width=\"600\" bgcolor=\"#003B5C\">" +
                "                                        <tr>" +
                "                                            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">" +
                "                            <![endif]-->" +
                "                            <div style=\"background:#003B5C;background-color:#003B5C;margin:0px auto;max-width:600px;\">" +
                "                                <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                       style=\"background:#003B5C;background-color:#003B5C;width:100%;\">" +
                "                                    <tbody>" +
                "                                    <tr>" +
                "                                        <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;\">" +
                "                                            <!--[if mso | IE]>" +
                "                                            <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "                                                <tr>" +
                "                                                    <td class=\"\" style=\"vertical-align:top;width:420px;\">" +
                "                                            <![endif]-->" +
                "                                            <div class=\"mj-column-per-70 mj-outlook-group-fix\"" +
                "                                                 style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                       style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                    <tbody>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:16px;font-weight:bold;line-height:24px;text-align:left;color:white;\">" +
                "                                                                Accélérez votre croissance avec Nukema" +
                "                                                            </div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    </tbody>" +
                "                                                </table>" +
                "                                            </div>" +
                "                                            <!--[if mso | IE]></td>" +
                "                                            <td class=\"\" style=\"vertical-align:top;width:180px;\">" +
                "                                            <![endif]-->" +
                "                                            <div class=\"mj-column-per-30 mj-outlook-group-fix\"" +
                "                                                 style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                       style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                    <tbody>" +
                "                                                    <tr>" +
                "                                                        <td align=\"center\" vertical-align=\"middle\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"" +
                "                                                                   role=\"presentation\"" +
                "                                                                   style=\"border-collapse:separate;line-height:100%;\">" +
                "                                                                <tr>" +
                "                                                                    <td align=\"center\" bgcolor=\"#003B5C\"" +
                "                                                                        role=\"presentation\"" +
                "                                                                        style=\"border:1px solid #FFF;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#003B5C;\"" +
                "                                                                        valign=\"middle\">" +
                "                                                                        <a href=\"https://www.actu.nukema.com/\"" +
                "                                                                           style=\"display:inline-block;background:#003B5C;color:#ffffff;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;\"" +
                "                                                                           target=\"_blank\"> Découvrir </a>" +
                "                                                                    </td>" +
                "                                                                </tr>" +
                "                                                            </table>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    </tbody>" +
                "                                                </table>" +
                "                                            </div>" +
                "                                            <!--[if mso | IE]></td>" +
                "                                            <td class=\"\" style=\"vertical-align:top;width:600px;\">" +
                "                                            <![endif]-->" +
                "                                            <div class=\"mj-column-per-100 mj-outlook-group-fix\"" +
                "                                                 style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                       style=\"vertical-align:top;\" width=\"100%\">" +
                "                                                    <tbody>" +
                "                                                    <tr>" +
                "                                                        <td align=\"left\"" +
                "                                                            style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">" +
                "                                                            <div style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:13px;font-weight:400;line-height:24px;text-align:left;color:white;\">" +
                "                                                                Trouvez les consultations publiques qui vous" +
                "                                                                correspondent en un clic.<br><br></div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    </tbody>" +
                "                                                </table>" +
                "                                            </div>" +
                "                                            <!--[if mso | IE]></td>" +
                "                                            <td class=\"\" style=\"vertical-align:top;width:600px;\">" +
                "                                            <![endif]-->" +
                "                                            <div class=\"mj-column-per-100 mj-outlook-group-fix\"" +
                "                                                 style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">" +
                "                                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\"" +
                "                                                       style=\"background-color:#E7E7E7;vertical-align:top;\"" +
                "                                                       width=\"100%\">" +
                "                                                    <tbody>" +
                "                                                    <tr>" +
                "                                                        <td style=\"font-size:0px;word-break:break-word;\">" +
                "                                                            <div style=\"height:20px;line-height:20px;\">&#8202;</div>" +
                "                                                        </td>" +
                "                                                    </tr>" +
                "                                                    </tbody>" +
                "                                                </table>" +
                "                                            </div>" +
                "                                            <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "                                        </td>" +
                "                                    </tr>" +
                "                                    </tbody>" +
                "                                </table>" +
                "                            </div>" +
                "                            <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->" +
                "                        </td>" +
                "                    </tr>" +
                "                    </tbody>" +
                "                </table>" +
                "            </div>" +
                "            <!--[if mso | IE]></td></tr></table><![endif]-->" +
                "        </div>" +
                "    </div>" +
                "    </html>" +
                "</div>";
        var message = MessageDTO.builder().contenu(content).build();
        Set<ConstraintViolation<MessageDTO>> violations = validator.validate(message);
        assertTrue(violations.isEmpty());
    }

}
