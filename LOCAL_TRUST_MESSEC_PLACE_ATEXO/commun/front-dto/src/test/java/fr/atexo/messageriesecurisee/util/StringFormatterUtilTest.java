package fr.atexo.messageriesecurisee.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringFormatterUtilTest {

    @Test
    public void testFileSizeformat() {
        assertEquals("formatage en Mo", StringFormatterUtil.format(1024 * 1024 + 1, 2), "1.01 Mo");
        assertEquals("formatage en Go", StringFormatterUtil.format(1024 * 1024 * 1024 + 1, 2), "1.01 Go");
    }
}
