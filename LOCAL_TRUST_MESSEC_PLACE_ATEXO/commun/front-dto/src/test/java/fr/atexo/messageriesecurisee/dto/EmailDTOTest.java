package fr.atexo.messageriesecurisee.dto;


import fr.atexo.messageriesecurisee.dto.model.TypeMessage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static fr.atexo.messageriesecurisee.dto.model.TypeStatutEmail.*;
import static java.util.Arrays.stream;
import static org.junit.Assert.*;

public class EmailDTOTest {

    private MessageDTO message;

    @Before
    public void init() {
        var date = new Date();
        message = MessageDTO.builder()
                .reponseAttendue(true)
                .dateLimiteReponseAsDate(date)
                .build();
    }

    @Test
    public void isReponseExpireeTrue() {
        var reponses = new ArrayList<EmailDTO>();
        for (var i : new int[]{-1, 0, 1}) {
            var calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, i);
            var dateReponse = calendar.getTime();
            reponses.add(EmailDTO.builder().message(message).dateEnvoi(dateReponse.getTime()).build());
        }
        var email = EmailDTO.builder().message(message).toutesLesReponses(reponses).build();
        assertTrue("réponse expirée", email.isReponseExpiree());
    }

    @Test
    public void isReponseExpireeFalse() {
        var reponses = new ArrayList<EmailDTO>();
        for (var i : new int[]{-3, -2, -1}) {
            var calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, i);
            var dateReponse = calendar.getTime();
            reponses.add(EmailDTO.builder().message(message).dateEnvoi(dateReponse.getTime()).build());
        }
        var email = EmailDTO.builder().message(message).toutesLesReponses(reponses).build();
        assertFalse("réponse non expirée", email.isReponseExpiree());
    }

    @Test
    public void isDateLimiteExpireeFalse() {
        var calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        var dateReponse = calendar.getTime();
        var email = EmailDTO.builder().message(message).dateEnvoi(dateReponse.getTime()).build();
        assertFalse("date limite réponse non expirée", email.isDateLimiteExpiree());
    }

    @Test
    public void isDateExpireeTrue() {
        var calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        var dateReponse = calendar.getTime();
        var email = EmailDTO.builder().message(message).dateEnvoi(dateReponse.getTime()).build();
        assertFalse("date limite réponse expirée", email.isReponseExpiree());
    }

    @Test
    public void isReponseBloqueeFalse() {
        message.setReponseBloque(true);
        var calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        var dateLimite = calendar.getTime();
        message.setDateLimiteReponseAsDate(dateLimite);
        var email = EmailDTO.builder().message(message).build();
        assertFalse("réponse non bloquée", email.isReponseBloquee());
    }

    @Test
    public void isReponseBloqueeTrue() {
        message.setReponseBloque(true);
        var calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        var dateLimite = calendar.getTime();
        message.setDateLimiteReponseAsDate(dateLimite);
        var email = EmailDTO.builder().message(message).build();
        assertTrue("réponse bloquée", email.isReponseBloquee());
    }

    @Test
    public void getReponseNull() {
        var email = EmailDTO.builder().toutesLesReponses(null).message(message).build();
        assertNull("réponse null", email.getReponse());

        email.setToutesLesReponses(new ArrayList<>());
        assertNull("réponse null", email.getReponse());
    }

    @Test
    public void getDerniereReponse() {
        var ids = new Integer[]{9, 5, 6, 3};
        var email = EmailDTO.builder().message(message).build();
        var reponses = new ArrayList<EmailDTO>();
        stream(ids).forEach(id -> reponses.add(EmailDTO.builder().id(id).build()));
        email.setToutesLesReponses(reponses);

        assertNotNull("réponse not null", email.getReponse());
        assertEquals("id réponse", email.getReponse().getId(), stream(ids).max(Integer::compareTo).orElse(null));
    }

    @Test
    public void isEnAttenteEnvoi() {
        var email1 = EmailDTO.builder().statutEmail(EN_ATTENTE_ENVOI).build();
        var email2 = EmailDTO.builder().statutEmail(EN_COURS_ENVOI).build();
        assertTrue("isEnAttenteEnvoi EN_ATTENTE_ENVOI", email1.isEnAttenteEnvoi());
        assertTrue("isEnAttenteEnvoi EN_COURS_ENVOI", email2.isEnAttenteEnvoi());

    }


    @Test
    public void isEchecEnvoi() {
        var email1 = EmailDTO.builder().statutEmail(ECHEC_ENVOI).build();
        assertTrue("isEchecEnvoi ECHEC_ENVOI", email1.isEchecEnvoi());
    }

    @Test
    public void isDelivre() {
        var email1 = EmailDTO.builder().statutEmail(ENVOYE).build();
        var email2 = EmailDTO.builder().statutEmail(ACQUITTE).build();
        var email3 = EmailDTO.builder().statutEmail(EN_ATTENTE_ACQUITTEMENT).build();
        assertTrue("isDelivre ENVOYE", email1.isDelivre());
        assertTrue("isDelivre ACQUITTE", email2.isDelivre());
        assertTrue("isDelivre EN_ATTENTE_ACQUITTEMENT", email3.isDelivre());
    }

    @Test
    public void isLu() {
        var email1 = EmailDTO.builder().statutEmail(ACQUITTE).message(MessageDTO.builder().typeMessage(TypeMessage.TypeMessage4).build()).build();
        assertTrue("isLu ACQUITTE", email1.isLu());
    }

    @Test
    public void isEnvoye() {
        var email1 = EmailDTO.builder().statutEmail(ENVOYE).build();
        assertTrue("isEnvoye ENVOYE", email1.isEnvoye());
    }
}
