package fr.atexo.messagerieSecurisee.administration;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LienDTO {

    private String libelle;

    private String lien;

    private String target = "_self";
}
