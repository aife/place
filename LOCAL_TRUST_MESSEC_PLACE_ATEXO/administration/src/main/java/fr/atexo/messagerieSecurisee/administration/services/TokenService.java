package fr.atexo.messagerieSecurisee.administration.services;

import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import fr.atexo.messageriesecurisee.dto.MessageStatusDTO;
import fr.atexo.messageriesecurisee.messages.consultation.AgentType;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.messages.envoi.CleValeurType;
import fr.atexo.messageriesecurisee.messages.envoi.DossierVolumineuxType;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static fr.atexo.messagerieSecurisee.administration.Utils.getXmlDate;
import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
public class TokenService {

    @Value("${ms.backend.url}")
    String urlBackendlMS;
    private final RestTemplate restTemplate = new RestTemplate();
    @Value("${ms.plateforme.identifiant}")
    private String pfIdentifiant;


    @Value("${ms.plateforme.nom}")
    private String pfNom;

    @Value("${ms.url.visualisation}")
    private String urlVisualisation;

    @NonNull
    private final AuthenticationService authenticationService;

    private static final String ADMIN = "admin";

    public MessageSecuriseInit getMessageSecuriseInit(Integer identifiant) {
        var messageInit = new MessageSecuriseInit();
        messageInit.setNomPfEmetteur(pfNom);
        messageInit.setNomPfDestinataire(pfNom);
        messageInit.setIdPfEmetteur(pfIdentifiant);
        messageInit.setIdPfDestinataire(pfIdentifiant);
        messageInit.setIdObjetMetierPfEmetteur(identifiant);
        messageInit.setIdObjetMetierPfDestinataire(identifiant);
        messageInit.setRefObjetMetier("ref_" + identifiant);
        messageInit.setUrlPfEmetteur(pfNom);
        messageInit.setUrlPfDestinataire(pfNom);
        messageInit.setNomCompletExpediteur(pfNom);
        messageInit.setSignatureAvisPassage(pfNom);
        messageInit.setUrlPfEmetteurVisualisation(urlVisualisation + identifiant);
        messageInit.setUrlPfDestinataireVisualisation(urlVisualisation + identifiant);
        messageInit.setUrlPfReponse(urlVisualisation + identifiant);
        messageInit.setCartouche("Cartouche généré depuis le module d'aministration");
        messageInit.setEmailExpediteur("administration-messec@atexo.com");
        messageInit.setEmailExpediteurTechnique("administration-messec@atexo.com");
        messageInit.getDossiersVolumineux().addAll(getDossiersVolumineux());
        messageInit.getEmailsAlerteReponse().add("agentAlerte" + identifiant + "@atexo.com");
        messageInit.setDestinatairesPfDestinataire(new MessageSecuriseInit.DestinatairesPfDestinataire());
        var cleValeur = new CleValeurType();
        cleValeur.setCle("cle");
        cleValeur.setValeur("valeur");
        messageInit.getMetaDonnees().add(cleValeur);
        return messageInit;
    }

    public ContexteDTO getToken(Object xml, String path) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(UTF_8));
        var accessToken = authenticationService.getAccessToken();
        headers.set("Authorization", "Bearer " + accessToken);
        var request = new HttpEntity<>(xml, headers);
        var messecToken = restTemplate.postForEntity(urlBackendlMS + path, request, String.class).getBody();
        return ContexteDTO.builder().accessToken(accessToken).token(messecToken).build();
    }

    List<DossierVolumineuxType> getDossiersVolumineux() {
        var dossiersVolumineux = new ArrayList<DossierVolumineuxType>();
        for (int i = 1; i < 5; i++) {
            var dv = new DossierVolumineuxType();
            dv.setNom("Dossier volumineux " + i);
            dv.setUuidReference("Ref " + i);
            dv.setUuidTechnique("uuid " + i);
            dv.setDateCreation(getXmlDate());
            dv.setTaille(i * 25679400778L);
            dossiersVolumineux.add(dv);
        }
        return dossiersVolumineux;
    }

    public RechercheMessage getSuiviInit(Integer identifiant) {
        var rechercheInit = new RechercheMessage();
        rechercheInit.setIdPlateformeRecherche(pfIdentifiant);
        rechercheInit.setIdObjetMetier(identifiant);
        rechercheInit.setRefObjetMetier("ref_" + identifiant);
        return rechercheInit;
    }

    public RechercheMessage getSuiviInit() {
        var rechercheInit = new RechercheMessage();
        rechercheInit.setIdPlateformeRecherche(pfIdentifiant);
        var agent = new AgentType();
        agent.setEmail("admin@atexo.com");
        agent.setIdentifiant(ADMIN);
        agent.setId(1L);
        agent.setNom(ADMIN);
        agent.setPrenom(ADMIN);
        agent.setAcronymeOrganisme("e3r");
        rechercheInit.setAgent(agent);
        return rechercheInit;
    }

    public MessageStatusDTO getMessageStatus(ContexteDTO tokenPair) {
        var headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + tokenPair.getAccessToken());
        return restTemplate.exchange(urlBackendlMS + "/rest/v2/suivi/count?token=" + tokenPair.getToken(), HttpMethod.GET, new HttpEntity<>(null, headers), MessageStatusDTO.class).getBody();
    }
}
