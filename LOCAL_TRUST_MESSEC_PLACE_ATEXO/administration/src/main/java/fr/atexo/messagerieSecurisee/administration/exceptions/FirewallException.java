package fr.atexo.messagerieSecurisee.administration.exceptions;

public class FirewallException extends RuntimeException{

	public FirewallException( String parametre) {
		super("Le parametre saisi est invalide = "+parametre);
	}
}
