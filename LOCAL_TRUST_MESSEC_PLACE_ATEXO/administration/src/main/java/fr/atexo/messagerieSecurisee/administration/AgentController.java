package fr.atexo.messagerieSecurisee.administration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messagerieSecurisee.administration.services.TokenService;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import fr.atexo.messageriesecurisee.messages.consultation.FiltreType;
import fr.atexo.messageriesecurisee.messages.consultation.ObjetMetierType;
import fr.atexo.messageriesecurisee.messages.consultation.RechercheMessage;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequiredArgsConstructor
@Slf4j
public class AgentController {

    @NonNull
    private final ObjectMapper objectMapper;

    @NonNull
    private final TokenService tokenService;


    @GetMapping("/agent/redaction/{identifiant}")
    public String redaction(Model model, @PathVariable(name = "identifiant") Integer identifiant, HttpServletRequest request) throws JsonProcessingException {
        MessageSecuriseInit messageInit = tokenService.getMessageSecuriseInit(identifiant);
        messageInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().add(buildDestinataire("registre réponse", "email@atexo.com"));
        messageInit.getDestinatairesPfDestinataire().getDestinatairePfDestinataire().add(buildDestinataire("registre retrait", "email@atexo.com"));

        var tokenPair = tokenService.getToken(messageInit, "/rest/v2/redaction/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlSuivi(request.getContextPath() + "/agent/suivi/" + identifiant)
                .templatesPath(request.getContextPath() + "/templates")
                .build()));
        return "agent/redaction";
    }



    private MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire buildDestinataire(String type, String email) {
        var destinataire = new MessageSecuriseInit.DestinatairesPfDestinataire.DestinatairePfDestinataire();
        destinataire.setType(type);
        destinataire.setMailContactDestinataire(email);
        destinataire.setNomContactDest("contact " + email);
        return destinataire;
    }

    @GetMapping("/agent/brouillons/{identifiant}/{codeLien}")
    public String brouillon(Model model, @PathVariable("identifiant") Integer identifiant, @PathVariable("codeLien") String codeLien, HttpServletRequest request) throws JsonProcessingException {
        var messageInit = tokenService.getMessageSecuriseInit(identifiant);
        messageInit.setCodeLien(codeLien);
        var tokenPair = tokenService.getToken(messageInit, "/rest/v2/redaction/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlSuivi(request.getContextPath() + "/agent/suivi/" + identifiant)
                .templatesPath(request.getContextPath() + "/templates")
                .build()));
        return "agent/redaction";
    }


    @GetMapping("/agent/suivi/{identifiant}")
    public String suivi(Model model, @PathVariable(name = "identifiant") Integer identifiant, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit(identifiant);
        rechercheInit.setAfficherFiltreEntreprise(true);
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlModificationMessage(request.getContextPath() + "/agent/brouillons/" + identifiant + "/")
                .urlNouveauMessage(request.getContextPath() + "/agent/redaction/" + rechercheInit.getIdObjetMetier())
                .templatesPath(request.getContextPath() + "/templates")
                .build()));
        return "agent/suivi";
    }

    @GetMapping("/agent/suivi/transverse")
    public String suiviTransverse(Model model, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit();
        generateObjetsMetier(rechercheInit);
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .templatesPath(request.getContextPath() + "/templates")
                .build()));
        return "agent/suivi";
    }

    @GetMapping("/agent/suivi/transverse/{filtre}")
    public String suiviTransverseAvecFiltre(Model model, @PathVariable("filtre") String filtre, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit();
        generateObjetsMetier(rechercheInit);
        rechercheInit.setFiltresAppliques(new FiltreType());
        switch (filtre.toUpperCase()) {
            case "REPONSE_ATTENDUE":
                rechercheInit.getFiltresAppliques().setStatutAcquitte(true);
                rechercheInit.getFiltresAppliques().setStatutDelivre(true);
                rechercheInit.getFiltresAppliques().setReponseAttendue(true);
                break;
            case "NON_DELIVRE":
                rechercheInit.getFiltresAppliques().setStatutNonDelivre(true);
                break;
            case "NON_LU":
                rechercheInit.getFiltresAppliques().setReponseNonLue(true);
                break;
            default:
                break;
        }
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .templatesPath(request.getContextPath() + "/templates")
                .build()));
        return "agent/suivi";
    }

    @GetMapping("/agent/dashboard")
    public String dashboard(Model model) {
        var rechercheInit = tokenService.getSuiviInit();
        generateObjetsMetier(rechercheInit);

        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        var statuts = tokenService.getMessageStatus(tokenPair);
        model.addAttribute("statuts", statuts);
        return "agent/dashboard";
    }

    private void generateObjetsMetier(RechercheMessage rechercheInit) {
        for (int i = 0; i < 20; i++) {
            var objetMetier = new ObjetMetierType();
            objetMetier.setIdentifiant(i);
            objetMetier.setReference("ref_" + i);
            objetMetier.setObjet("Objet de la consultation " + objetMetier.getReference());
            objetMetier.setUrlSuivi("/agent/suivi/" + i);
            objetMetier.setCreePar(i % 2 == 0 ? "admin" : "admin_autre");
            rechercheInit.getObjetsMetier().add(objetMetier);
        }
    }


}
