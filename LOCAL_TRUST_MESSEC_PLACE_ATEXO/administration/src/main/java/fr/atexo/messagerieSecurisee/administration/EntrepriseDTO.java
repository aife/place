package fr.atexo.messagerieSecurisee.administration;

import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class EntrepriseDTO {

    @Email(message = "Adresse email non valide")
    private String email;

    @NotNull(message = "l'identifiant de la consultation est obligatoire")
    private Integer identifiant;

    @NotNull(message = "le nom du contact est obligatoire")
    private String contact;
}
