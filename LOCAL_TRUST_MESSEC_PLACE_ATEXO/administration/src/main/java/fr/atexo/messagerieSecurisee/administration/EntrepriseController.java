package fr.atexo.messagerieSecurisee.administration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.atexo.messagerieSecurisee.administration.services.TokenService;
import fr.atexo.messageriesecurisee.dto.ContexteDTO;
import fr.atexo.messageriesecurisee.messages.consultation.DossierVolumineuxType;
import fr.atexo.messageriesecurisee.messages.envoi.MessageSecuriseInit;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static fr.atexo.messagerieSecurisee.administration.Utils.getXmlDate;

@Controller
@Slf4j
@RequiredArgsConstructor
@SessionAttributes("entreprise")
public class EntrepriseController {

    @NonNull
    private final ObjectMapper objectMapper;

    @NonNull
    private final TokenService tokenService;

    @GetMapping("/entreprise/visualisation/{identifiant}/{codeLien}")
    public String redaction(@ModelAttribute("entreprise") EntrepriseDTO entreprise, Model model, @PathVariable("identifiant") Integer identifiant, @PathVariable("codeLien") String codeLien, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit(identifiant);
        rechercheInit.setCodeLien(codeLien);
        rechercheInit.setMailDestinataire(entreprise.getEmail());
        rechercheInit.setMailExpediteur(entreprise.getEmail());
        rechercheInit.getDossiersVolumineux().addAll(getDossiersVolumineux());
        rechercheInit.getEmailsAlerteReponse().addAll(Arrays.asList("agent-notif1@atexo.com", "agent-notif2@atexo.com"));
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlSuivi(request.getContextPath() + "/entreprise/suivi/" + identifiant)
                .build()));
        return "entreprise/visualisation";
    }

    @GetMapping("/entreprise/visualisation/{identifiant}")
    public String redactionCodeLien(Model model, @ModelAttribute("entreprise") EntrepriseDTO entreprise, @PathVariable("identifiant") Integer identifiant, @RequestParam("codelien") String codeLien, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit(identifiant);
        rechercheInit.setCodeLien(codeLien);
        rechercheInit.setMailDestinataire(entreprise.getEmail());
        rechercheInit.setMailExpediteur(entreprise.getEmail());
        rechercheInit.getEmailsAlerteReponse().addAll(Arrays.asList("agent-notif1@atexo.com", "agent-notif2@atexo.com"));
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .build()));
        return "entreprise/visualisation";
    }

    @GetMapping("/entreprise/visualisation-echange-externe")
    public String getReponsePage(@RequestParam("codelien") String codeLien, Model model) {
        var urlVisualisation = "/messagerieSecurisee/telechargement?codelien=" + codeLien;
        model.addAttribute("urlVisualisation", urlVisualisation);
        return "entreprise/visualisation-echange-externe";
    }


    @GetMapping("/entreprise/suivi/{identifiant}")
    public String suivi(@ModelAttribute("entreprise") EntrepriseDTO entreprise, @PathVariable("identifiant") Integer identifiant, Model model, HttpServletRequest request) throws JsonProcessingException {
        var rechercheInit = tokenService.getSuiviInit(identifiant);
        rechercheInit.setMailDestinataire(entreprise.getEmail());
        rechercheInit.setMailExpediteur(entreprise.getEmail());
        var tokenPair = tokenService.getToken(rechercheInit, "/rest/v2/suivi/initToken");
        model.addAttribute("context", objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlVisualisationMessage(request.getContextPath() + "/entreprise/visualisation/" + identifiant + "/")
                .modeLegacy(true)
                .urlVisualisationMessageLegacy(request.getContextPath() + "/entreprise/visualisation-echange-externe?codelien=")
                .urlNouveauMessage(request.getContextPath() + "/entreprise/redaction/" + identifiant).build()));
        return "entreprise/suivi";
    }

    @GetMapping("/entreprise/redaction/{identifiant}")
    public String redaction(@ModelAttribute("entreprise") EntrepriseDTO entreprise, @PathVariable("identifiant") Integer identifiant, Model model, HttpServletRequest request) throws JsonProcessingException {
        var redactionInit = tokenService.getMessageSecuriseInit(identifiant);
        redactionInit.setEmailExpediteur(entreprise.getEmail());
        redactionInit.setNomContactExpediteur(entreprise.getContact());
        redactionInit.getEmailsAlerteReponse().clear();
        redactionInit.getEmailsAlerteReponse().add("agent@atexo.com");
        var agent = new MessageSecuriseInit.DestinatairesPfEmetteur.DestinatairePfEmetteur();
        agent.setMailContactDestinataire("agent@atexo.com");
        agent.setNomContactDest("agent");
        MessageSecuriseInit.DestinatairesPfEmetteur dest = new MessageSecuriseInit.DestinatairesPfEmetteur();
        dest.getDestinatairePfEmetteur().add(agent);
        redactionInit.setDestinatairesPfEmetteur(dest);
        var tokenPair = tokenService.getToken(redactionInit, "/rest/v2/redaction/initToken");
        var context = objectMapper.writeValueAsString(ContexteDTO.builder()
                .token(tokenPair.getToken())
                .accessToken(tokenPair.getAccessToken())
                .urlVisualisationMessage(request.getContextPath() + "/entreprise/visualisation/" + identifiant + "/")
                .modeLegacy(true)
                .templatesPath(request.getContextPath() + "/templates")
                .idTemplateSelectionne("10")
                .urlSuivi(request.getContextPath() + "/entreprise/suivi/" + identifiant).build());
        model.addAttribute("context", context);
        return "entreprise/redaction";
    }

    @GetMapping("/entreprise/connexion")
    public String connexion(@ModelAttribute("entreprise") EntrepriseDTO entreprise) {
        return "entreprise/connexion";
    }

    @GetMapping("/entreprise/deconnexion")
    public String deconnexion(@ModelAttribute("entreprise") EntrepriseDTO entreprise) {
        entreprise.setIdentifiant(null);
        entreprise.setEmail(null);
        return "redirect:/entreprise/suivi";
    }

    @PostMapping("/entreprise/connexion")
    public String submitConnexion(Model model, @ModelAttribute("entreprise") @Valid EntrepriseDTO entreprise, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage", bindingResult.getAllErrors());
            return "forward:/entreprise/connexion";
        }
        return "redirect:/entreprise/suivi/" + entreprise.getIdentifiant();
    }

    List<DossierVolumineuxType> getDossiersVolumineux() {
        var dossiersVolumineux = new ArrayList<DossierVolumineuxType>();
        for (int i = 1; i < 5; i++) {
            var dv = new DossierVolumineuxType();
            dv.setNom("Dossier volumineux réponse " + i);
            dv.setUuidReference("Ref réponse" + i);
            dv.setUuidTechnique("uuid réponse" + i);
            dv.setDateCreation(getXmlDate());
            dv.setTaille(i * 25679400778L);
            dossiersVolumineux.add(dv);
        }
        return dossiersVolumineux;
    }

    @ModelAttribute("entreprise")
    public EntrepriseDTO entreprise() {
        return new EntrepriseDTO();
    }

}
