package fr.atexo.messagerieSecurisee.administration;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class BlocDTO {

    private String libelle;

    private String description;

    private String icone;

    private List<LienDTO> liens = new ArrayList<>();
}
