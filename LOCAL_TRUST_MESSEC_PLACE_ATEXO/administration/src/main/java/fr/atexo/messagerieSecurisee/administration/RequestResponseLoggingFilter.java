package fr.atexo.messagerieSecurisee.administration;

import fr.atexo.messagerieSecurisee.administration.exceptions.FirewallException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.List;

@Component
@Order(2)
@Slf4j
public class RequestResponseLoggingFilter implements Filter {

	private static final List<String> BLACKLIST = List.of(".*class.*",".*Class.*");

	private boolean forbidden( String pattern) {
		return BLACKLIST.stream().anyMatch(pattern::matches);
	}

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		var parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements()) {
			var name = parameterNames.nextElement();

			if (forbidden(name)) {
				throw new FirewallException(name);
			}

			var values = request.getParameterValues(name);

			for (var value : values) {
				if ( forbidden(value)) {
					throw new FirewallException(value);
				}
			}
		}

		chain.doFilter(request, response);
	}
}
