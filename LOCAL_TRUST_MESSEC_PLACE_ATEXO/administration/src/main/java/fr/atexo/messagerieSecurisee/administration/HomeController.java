package fr.atexo.messagerieSecurisee.administration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@Controller
@Slf4j
@SessionAttributes("entreprise")
public class HomeController {

    @Value("${ms.front.url}")
    private String urlFrontMS;

    @Value("${ms.swagger.url}")
    private String urlSwagger;

    @Value("${ms.javaMelody.url}")
    private String urlJavaMelody;

    @Value("${ms.js.mode}")
    private String jsMode;

    private List<String> jsDev = asList("runtime.js", "polyfills.js", "scripts.js", "main.js", "vendor.js");

    private List<String> jsProd = asList("messec.js");


    @GetMapping("/")
    public ModelAndView home(@ModelAttribute("entreprise") EntrepriseDTO entreprise, ModelMap model, HttpSession session, Principal principal) {
        session.setAttribute("blocs", buildMenu());
        session.setAttribute("frontUrl", urlFrontMS);
        session.setAttribute("scriptsJS", "dev".equalsIgnoreCase(jsMode) ? jsDev : jsProd);
        session.setAttribute("compteAgent", principal.getName());
        return new ModelAndView("redirect:/agent/redaction/1", model);
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    private List<BlocDTO> buildMenu() {
        var blocs = new ArrayList<BlocDTO>();
        // bloc agent
        var liensAgent = new ArrayList<LienDTO>();
        liensAgent.add(LienDTO.builder().lien("/agent/redaction/1").libelle("Agent : Rédaction d'un message").build());
        liensAgent.add(LienDTO.builder().lien("/agent/suivi/1").libelle("Agent : Suivi des messages").build());
        liensAgent.add(LienDTO.builder().lien("/agent/suivi/transverse").libelle("Agent : Suivi TRANSVERSE des messages").build());
        liensAgent.add(LienDTO.builder().lien("/agent/dashboard").libelle("Agent : Dashboard").build());
        var agent = BlocDTO.builder()
                .libelle("Agent")
                .description("Utilitaires côté agent")
                .icone("fas fa-chalkboard-teacher")
                .liens(liensAgent)
                .build();


        // bloc entreprise
        var liensEntreprise = new ArrayList<LienDTO>();
        liensEntreprise.add(LienDTO.builder().lien("/entreprise/connexion").libelle("Entreprise : Suivi des messages").build());
        var entreprise = BlocDTO.builder()
                .libelle("Entreprise")
                .description("Utilitaires côté entreprise")
                .icone("txt-primary fas fa-industry")
                .liens(liensEntreprise)
                .build();

        // bloc technique

        var liensTechniques = new ArrayList<LienDTO>();
        liensTechniques.add(LienDTO.builder().target("_blank").lien(urlSwagger).libelle("Swagger").build());
        liensTechniques.add(LienDTO.builder().target("_blank").lien(urlJavaMelody).libelle("Java Melody").build());
        var technique = BlocDTO.builder()
                .libelle("Outils techniques")
                .description("Outils techniques")
                .icone("fas fa-cogs")
                .liens(liensTechniques)
                .build();

        blocs.add(agent);
        blocs.add(entreprise);
        blocs.add(technique);
        return blocs;
    }

    @ModelAttribute("entreprise")
    public EntrepriseDTO entreprise() {
        return new EntrepriseDTO();
    }
}
