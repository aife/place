package fr.atexo.messagerieSecurisee.administration.services;

import fr.atexo.messageriesecurisee.dto.AccessToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
@Service
public class AuthenticationService {

    @Value("${administration.keycloak.client_id}")
    private String clientId;

    @Value("${administration.keycloak.username}")
    private String username;

    @Value("${administration.keycloak.password}")
    private String password;

    @Value("${administration.keycloak.grant_type}")
    private String grantType;

    @Value("${administration.keycloak.auth.url}")
    private String authUrl;


    public String getAccessToken() {
        try {
            var restTemplate = new RestTemplate();
            var uri = new URI(authUrl);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("client_id", clientId);
            map.add("username", username);
            map.add("password", password);
            map.add("grant_type", grantType);

            var request = new HttpEntity<>(map, headers);

            ResponseEntity<AccessToken> result = restTemplate.postForEntity(uri, request, AccessToken.class);
            return result.getBody().getAccessToken();
        } catch (URISyntaxException e) {
            log.error("impossible de récupérer un access token", e);
            return null;
        }
    }
}
