# Installation du poste de travail
## Configuration de EXIM
``` shell 
docker-compose down
```
``` shell 
docker-compose up
```
Si exim4 ne se lance pas avec en message d'erreur l'inexistence du fichier /var/log/exim4/mainlog, le créer et se donner les droits avec:
``` shell 
sudo mkdir --parents /var/log/exim4/
```
``` shell 
sudo chmod -R 666 /var/log/exim4/
```

pour le lancement en mode DEV, exécuter avec le profil __dev__ et les arguments suivants __--add-opens
java.base/jdk.internal.loader=ALL-UNNAMED__
