package fr.atexo.messageriesecurisee.mapper;

import junit.framework.TestCase;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MapperUtilsTest extends TestCase {

    private String format = "dd/MM/yyyy à HH:mm:ss";
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

    @Test
    public void testToTimeStamp() {
        var dateTime = LocalDateTime.now();
        var date = new Date(MapperUtils.toTimeStamp(dateTime));
        var dateFormatee = simpleDateFormat.format(date);
        var localDateTimeFormatee = dateTimeFormatter.format(dateTime);
        assertEquals("meme valeur entre localDateTime et java.utl.Date", dateFormatee, localDateTimeFormatee);
    }
}
