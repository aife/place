package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.CleValeurDTO;
import fr.atexo.messageriesecurisee.model.CleValeur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface CleValeurMapper extends AbstractMapper<CleValeur, CleValeurDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    CleValeurDTO toDto(CleValeur bo);
}
