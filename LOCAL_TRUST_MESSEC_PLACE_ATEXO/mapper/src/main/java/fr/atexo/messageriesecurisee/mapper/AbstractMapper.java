package fr.atexo.messageriesecurisee.mapper;

import java.util.Collection;
import java.util.List;

public interface AbstractMapper<S, T> {

    T toDto(S bo);

    List<T> toDtos(Collection<S> bos);

    List<S> toBos(Collection<T> bos);

    S toBo(T dto);
}
