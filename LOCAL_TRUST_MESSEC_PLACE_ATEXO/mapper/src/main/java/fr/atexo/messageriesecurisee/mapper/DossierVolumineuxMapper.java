package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.DossierVolumineuxDTO;
import fr.atexo.messageriesecurisee.model.DossierVolumineux;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface DossierVolumineuxMapper extends AbstractMapper<DossierVolumineux, DossierVolumineuxDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    DossierVolumineuxDTO toDto(DossierVolumineux bo);
}
