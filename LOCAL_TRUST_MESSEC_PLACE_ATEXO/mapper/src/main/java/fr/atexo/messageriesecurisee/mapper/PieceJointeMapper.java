package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.PieceJointeDTO;
import fr.atexo.messageriesecurisee.model.PieceJointe;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface PieceJointeMapper extends AbstractMapper<PieceJointe, PieceJointeDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    PieceJointeDTO toDto(PieceJointe bo);
}
