package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.EmailDTO;
import fr.atexo.messageriesecurisee.model.Email;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, MessageMapper.class})
public interface EmailMapper extends AbstractMapper<Email, EmailDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    @Mapping(source = "reponses", target = "toutesLesReponses")
    EmailDTO toDto(Email bo);
}
