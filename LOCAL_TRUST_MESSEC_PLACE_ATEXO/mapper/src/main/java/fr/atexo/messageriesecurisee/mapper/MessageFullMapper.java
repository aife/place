package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.DestinataireDTO;
import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.model.Message;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Comparator;
import java.util.List;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, CleValeurMapper.class, CriterMapper.class, PieceJointeMapper.class, DestinataireMapper.class, DossierVolumineuxMapper.class})
public interface MessageFullMapper {

    default void sort(List<DestinataireDTO> destinataires) {
        if (destinataires != null && !destinataires.isEmpty()) {
            destinataires.sort(Comparator.comparing(DestinataireDTO::getOrdre, Comparator.nullsFirst(Comparator.naturalOrder())));
        }
    }

    @Mapping(source = "id", target = "id")
    @Mapping(ignore = true, target = "destsPfDestinataire")
    @Mapping(ignore = true, target = "destsPfEmettreur")
    @Mapping(source = "destinatairesPreSelectionnes", target = "destinatairesPreSelectionnes")
    @Mapping(target = "dateLimiteReponseAsDate", source = "dateLimiteReponse")
    MessageDTO toDto(Message bo);

    default MessageDTO toFull(Message bo) {
        var dto = toDto(bo);
        sort(dto.getDestinatairesPreSelectionnes());
        sort(dto.getDestinatairesNotifies());
        return dto;
    }

    @Mapping(source = "id", target = "id")
    Message toBo(MessageDTO dto);

    @Mapping(source = "id", target = "id")
    @Mapping(ignore = true, target = "uuid")
    Message copy(Message original);
}
