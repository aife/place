package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.CritereDTO;
import fr.atexo.messageriesecurisee.model.Critere;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class})
public interface CriterMapper extends AbstractMapper<Critere, CritereDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    CritereDTO toDto(Critere bo);
}
