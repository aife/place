package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.criteres.SerializableRechercheMessage;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class MapperUtils {

    public static Date toDate(Long timeStamp) {
        if (timeStamp == null) {
            return null;
        } else {
            return new Date(timeStamp);
        }
    }

    public static Long toTimeStamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }

    public static Long toTimeStamp(LocalDateTime date) {
        if (date == null) {
            return null;
        } else {
            return date.atZone(ZoneId.systemDefault()).toEpochSecond() * 1000;
        }
    }

    public static Date toDate(LocalDateTime date) {
        if (date == null) {
            return null;
        } else {
            return Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
        }
    }

    public static fr.atexo.messageriesecurisee.dto.model.TypeDestinataire convert(
            SerializableRechercheMessage.TypeDestinataire type) {
        fr.atexo.messageriesecurisee.dto.model.TypeDestinataire result = null;
        if (type != null) {
            switch (type) {
                case DESTINATAIRE:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
                    break;
                case EMETTEUR:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_EMETTRICE;
                    break;
                default:
                    result = fr.atexo.messageriesecurisee.dto.model.TypeDestinataire.DESTINATAIRE_PLATEFORME_DESTINATAIRE;
                    break;
            }
        }
        return result;
    }

    public static SerializableRechercheMessage.TypeDestinataire convert(
            fr.atexo.messageriesecurisee.dto.model.TypeDestinataire type) {
        SerializableRechercheMessage.TypeDestinataire result = null;
        if (type != null) {
            switch (type) {
                case DESTINATAIRE_PLATEFORME_DESTINATAIRE:
                    result = SerializableRechercheMessage.TypeDestinataire.DESTINATAIRE;
                    break;
                case DESTINATAIRE_PLATEFORME_EMETTRICE:
                    result = SerializableRechercheMessage.TypeDestinataire.EMETTEUR;
                    break;
            }
        }
        return result;
    }

}
