package fr.atexo.messageriesecurisee.mapper;


import fr.atexo.messageriesecurisee.dto.CodeLabelDTO;
import fr.atexo.messageriesecurisee.model.AbstractReferentielEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReferentielMapper extends AbstractMapper<AbstractReferentielEntity, CodeLabelDTO> {

    @Override
    CodeLabelDTO toDto(AbstractReferentielEntity bo);

    @Override
    default AbstractReferentielEntity toBo(CodeLabelDTO dto) {
        return null;
    }

}
