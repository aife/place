package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.MessageDTO;
import fr.atexo.messageriesecurisee.model.Message;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, CleValeurMapper.class, CriterMapper.class, PieceJointeMapper.class, DossierVolumineuxMapper.class})
public interface MessageMapper extends AbstractMapper<Message, MessageDTO> {

    @Override
    @Mapping(source = "id", target = "id")
    @Mapping(target = "destinatairesNotifies", ignore = true)
    @Mapping(target = "destinatairesPreSelectionnes", ignore = true)
    @Mapping(target = "destsPfDestinataire", ignore = true)
    @Mapping(target = "destsPfEmettreur", ignore = true)
    @Mapping(target = "dateLimiteReponseAsDate", source = "dateLimiteReponse")
    MessageDTO toDto(Message bo);

}
