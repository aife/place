package fr.atexo.messageriesecurisee.mapper;

import fr.atexo.messageriesecurisee.dto.DestinataireDTO;
import fr.atexo.messageriesecurisee.model.Email;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MapperUtils.class, MessageMapper.class})
public interface DestinataireMapper extends AbstractMapper<Email, DestinataireDTO> {

    @Override
    @Mapping(source = "nomContact", target = "nomContactDest")
    @Mapping(source = "email", target = "mailContactDestinataire")
    @Mapping(source = "type", target = "type")
    @Mapping(source = "typeDestinataire", target = "typeDestinataire")
    @Mapping(source = "identifiantEntreprise", target = "idEntrepriseDest")
    @Mapping(source = "identifiantContact", target = "idContactDest")
    @Mapping(source = "ordre", target = "ordre")
    @Mapping(source = "nomEntreprise", target = "nomEntrepriseDest")
    DestinataireDTO toDto(Email bo);
}
