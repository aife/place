<%@ page contentType="application/x-java-jnlp-file" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>
<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="1.0+" href="${urlPubliqueServeurCrypto}/checkPadesSignatureJNLP?uuid=${uuid}">
    <information>
        <title>Signature Electronique</title>
        <vendor>Atexo</vendor>
        <offline-allowed/>
    </information>
    <resources>
        <!-- Application Resources -->
        <j2se version="1.8+" initial-heap-size="64m" max-heap-size="1024m" java-vm-args="-XX:+IgnoreUnrecognizedVMOptions --add-modules=java.xml.bind,javafx.controls,javafx.fxml" />
        <jar href="${urlPubliqueServeurCrypto}/resources/signature-client.jar" main="true"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-document.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-token.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-spi.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/pdfbox.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/fontbox.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-enumerations.jar" main="false"/>
		<jar href="${urlPubliqueServeurCrypto}/resources/libs/specs-xmldsig.jar" main="false"/>
		<jar href="${urlPubliqueServeurCrypto}/resources/libs/specs-xades.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-pades.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-pades-pdfbox.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-xades.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-cades.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-model.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-service.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-utils-apache-commons.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-utils.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/xmlsec.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcprov-jdk15on.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcmail-jdk15on.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcpkix-jdk15on.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/slf4j.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/jcl-over-slf4j.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/logback-classic.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/logback-core.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/httpcore.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/httpclient.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/commons-codec.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/commons-collections.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/commons-io.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/commons-lang.jar" main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/json.jar" main="false"/>
    </resources>

    <security>
        <all-permissions/>
    </security>
    <application-desc name="Signature" main-class="com.atexo.serveurCryptographique.jnlp.ApplicationSignaturePades">
    <c:forEach items="${parameters}" var="element"> 
       <argument>${element}</argument>
    </c:forEach>
    </application-desc>
    <update check="background"/>
</jnlp>
