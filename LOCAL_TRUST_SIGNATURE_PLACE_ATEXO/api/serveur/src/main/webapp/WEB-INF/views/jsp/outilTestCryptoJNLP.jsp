<%@ page contentType="application/x-java-jnlp-file" %>
<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="1.0+"
      href="${urlPubliqueServeurCrypto}/outilTest?serveurCryptoURLPublique=${urlPubliqueServeurCrypto}">
    <information>
        <title>Signature Electronique</title>
        <vendor>Atexo</vendor>
        <offline-allowed/>

    </information>
    <resources>
        <!-- Application Resources -->
        <j2se version="1.8+" initial-heap-size="64m" max-heap-size="1024m"
              java-vm-args="-XX:+IgnoreUnrecognizedVMOptions --add-modules=java.xml.bind,javafx.controls,javafx.fxml"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/cryptographie-lib.jar"
             main="true"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/signature-client.jar"
             main="true"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/slf4j.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/jcl-over-slf4j.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/logback-classic.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/logback-core.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcprov-jdk15on.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcmail-jdk15on.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/bcpkix-jdk15on.jar"
             main="false"/>
        <jar href="${urlPubliqueServeurCrypto}/resources/libs/dss-model.jar"
             main="false"/>
    </resources>

    <security>
        <all-permissions/>
    </security>

    <application-desc name="Signature"
                      main-class="com.atexo.serveurCryptographique.jnlp.ApplicationTestConfiguration">
        <argument>${urlPubliqueServeurCrypto}/signatureXades</argument>
    </application-desc>
    <update check="background"/>
</jnlp>
