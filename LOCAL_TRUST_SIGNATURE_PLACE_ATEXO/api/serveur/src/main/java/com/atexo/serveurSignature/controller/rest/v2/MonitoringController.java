package com.atexo.serveurSignature.controller.rest.v2;

import com.atexo.serveurSignature.core.domain.monitoring.model.FilesDetailResponse;
import com.atexo.serveurSignature.core.domain.monitoring.service.ApplicationService;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v2/monitoring")
public class MonitoringController {

	private final ApplicationService applicationService;

	public MonitoringController(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

	@GetMapping(value = "/signed-files")
	public List<FilesDetailResponse> affichelogs(@RequestParam(required = false) String filtre, @RequestParam(required = false) String type)
			throws MauvaisParametreException {

		return applicationService.getSignedFiles(filtre, type);
	}

	@GetMapping(value = "/signed-files/download")
	public ResponseEntity<InputStreamResource> downloadSignedFile(@RequestParam String nom)
			throws MauvaisParametreException {
		return applicationService.downloadSignedFile(nom);
	}
}
