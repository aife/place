package com.atexo.serveurSignature.controller.web.v1.mock;

import com.atexo.serveurSignature.core.utilitaire.IOUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"/v1/uploadFile"})
public class UploadServlet extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                config.getServletContext());
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        IOUtils.copy(request.getInputStream(), new FileOutputStream(new File("/tmp/" + request.getParameter("id") + ".pdf")));
        response.getWriter().println("OK");
    }
}
