package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.domain.signature.model.*;
import com.atexo.serveurSignature.core.domain.signature.service.ReferentielCertificatConfiguration;
import com.atexo.serveurSignature.core.domain.signature.service.impl.SignatureValidationServiceImpl;
import com.atexo.serveurSignature.core.enums.TypeAlgorithmHashEnum;
import com.atexo.serveurSignature.core.enums.TypeEchangeEnum;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.core.utilitaire.CertificatUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Set;

/**
 * api de validation des certifications d'authentification
 */
@RestController
@RequestMapping("/v1")
public class ValidationController {
	private static final Logger logger = LoggerFactory.getLogger(ValidationController.class);
	private final JMXService jmxService;

	private final SignatureValidationServiceImpl signatureValidationService;
	private final ConfigurationService configurationService;
	private final ReferentielCertificatConfiguration referentielCertificatConfiguration;

	public ValidationController(JMXService jmxService, SignatureValidationServiceImpl signatureValidationService, ConfigurationService configurationService, ReferentielCertificatConfiguration referentielCertificatConfiguration) {
		this.jmxService = jmxService;
		this.signatureValidationService = signatureValidationService;
		this.configurationService = configurationService;
		this.referentielCertificatConfiguration = referentielCertificatConfiguration;
	}

	@RequestMapping(value = "checkCertificat", method = RequestMethod.POST, consumes = {"application/x-www-form-urlencoded"})
	public ResponseEntity<InfosVerificationCertificat> checkCertificat(@RequestParam(name = "certificatBase64") String certificatBase64, @RequestParam(name = "plateforme", required = false) String originePlateforme,
																	   @RequestParam(name = "organisme", required = false) String origineOrganisme, @RequestParam(name = "contexteMetier", required = false) String origineContexteMetier, HttpServletResponse resp) throws MauvaisParametreException, IOException {
		long start = System.currentTimeMillis();
		CertificateFactory certFactory;
		InputStream in = null;
		InfosVerificationCertificat infosVerificationCertificat = null;
		try {
			certFactory = CertificateFactory.getInstance("X.509");
			in = new ByteArrayInputStream(Base64.getDecoder().decode(certificatBase64.getBytes()));
			X509Certificate certificat = (X509Certificate) certFactory.generateCertificate(in);
			infosVerificationCertificat = (InfosVerificationCertificat) CertificatUtil.extraireInformations(certificat, new InfosVerificationCertificat());

			MessageReponse reponse = null;
			if (configurationService.getValidationURL() != null && !configurationService.getValidationURL().isEmpty()) {
				reponse = signatureValidationService.getReponseServeurValidation(null, certificatBase64, TypeEchangeEnum.VerificationSignatureClient.name(), TypeAlgorithmHashEnum.SHA1.name(), null, originePlateforme, origineContexteMetier);
			}
			if (reponse instanceof ReponseEnrichissementSignature) {
				ReponseEnrichissementSignature reponseValidation = (ReponseEnrichissementSignature) reponse;
				if (reponseValidation.getResultat() != null) {
					Set<Repertoire> repertoiresChaine = reponseValidation.getResultat().getRepertoiresChaineCertification();
					if (repertoiresChaine != null) {
						for (Repertoire repertoire : repertoiresChaine) {
							repertoire.setNom(referentielCertificatConfiguration.getReferentielNorme(repertoire.getNom()));
						}
						infosVerificationCertificat.setRepertoiresChaineCertification(repertoiresChaine);
					}
					Set<Repertoire> repertoiresRevoc = reponseValidation.getResultat().getRepertoiresRevocation();
					if (repertoiresRevoc != null) {
						for (Repertoire repertoire : repertoiresRevoc) {
							repertoire.setNom(referentielCertificatConfiguration.getReferentielNorme(repertoire.getNom()));
						}
						infosVerificationCertificat.setRepertoiresRevocation(repertoiresRevoc);
					}
					infosVerificationCertificat.setEtat(reponseValidation.getCodeRetour());
					infosVerificationCertificat.setAbsenceRevocationCRL(reponseValidation.getResultat().getRevocation());
					infosVerificationCertificat.setChaineDeCertificationValide(reponseValidation.getResultat().getChaineCertification());
				}

			}
		} catch (CertificateException e) {
			throw new MauvaisParametreException("Certificat incorrect", e.fillInStackTrace());
		} finally {
			IOUtils.closeQuietly(in);
		}
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_SIGNATURE);
		return new ResponseEntity<InfosVerificationCertificat>(infosVerificationCertificat, HttpStatus.OK);
	}

	/**
	 * api de verification de signature au format XAdES
	 */
	@RequestMapping(value = "signatureXades", method = RequestMethod.POST, consumes = {"application/x-www-form-urlencoded"})
	public ResponseEntity<MessageReponse> signatureXades(@RequestParam(name = "contenuFichierXML") String signatureXML, @RequestParam(name = "typeEchange") String typeEchange, @RequestParam(name = "typeAlgorithmHash") String typeAlgorithmHash,
														 @RequestParam(name = "hashFichier", required = false) String hashFichier, @RequestParam(name = "plateforme", required = false) String originePlateforme, @RequestParam(name = "organisme", required = false) String origineOrganisme,
														 @RequestParam(name = "item", required = false) String origineItem, @RequestParam(name = "contexteMetier", required = false) String origineContexteMetier, HttpServletResponse resp) throws MauvaisParametreException {
		long start = System.currentTimeMillis();

		TypeEchangeEnum typeEchangeEnum = TypeEchangeEnum.valueOf(typeEchange);
		TypeAlgorithmHashEnum typeAlgorithmHashEnum = TypeAlgorithmHashEnum.valueOf(typeAlgorithmHash);
		signatureXML = new String(Base64.getDecoder().decode(signatureXML.getBytes()));
		MessageReponse reponse = null;
		if (configurationService.getValidationURL() != null && !configurationService.getValidationURL().isEmpty()) {
			reponse = signatureValidationService.getReponseServeurValidation(signatureXML, null, typeEchange, typeAlgorithmHash, hashFichier, origineItem, origineContexteMetier);
		}
		if (reponse instanceof ReponseVerificationSignatureServeur) {
			if (((ReponseVerificationSignatureServeur) reponse).getResultat() != null) {
				Set<Repertoire> repertoires = ((ReponseVerificationSignatureServeur) reponse).getResultat().getRepertoiresChaineCertification();
				if (repertoires != null) {
					for (Repertoire repertoire : repertoires) {
						repertoire.setNom(referentielCertificatConfiguration.getReferentielNorme(repertoire.getNom()));
					}
				}
			}
		}
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_SIGNATURE);
		return new ResponseEntity<MessageReponse>(reponse, HttpStatus.OK);
	}
}
