package com.atexo.serveurSignature.controller.rest.v1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;

import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.trustedlist.jaxb.tsl.TSPServiceType;
import eu.europa.esig.trustedlist.jaxb.tsl.TSPType;
import eu.europa.esig.trustedlist.jaxb.tsl.TrustServiceProviderListType;
import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;

@RestController
@RequestMapping("v1")
public class TslController {
    private static final Logger logger = LoggerFactory.getLogger(SignatureController.class);

    @RequestMapping(value = "tslPays", method = RequestMethod.GET, produces = "application/xml; charset=UTF-8" )
    public String getTsl(@RequestParam(name = "acSignature", required = false) boolean acSignature, @RequestParam(name = "tslURL", required = true) String tslURL) throws MauvaisParametreException, JAXBException {

        CommonsDataLoader commonsHttpDataLoader = new CommonsDataLoader();
        byte[] bytes = commonsHttpDataLoader.get(tslURL);

        InputStream euLOTL = new ByteArrayInputStream(bytes);
        JAXBContext jc = JAXBContext.newInstance("eu.europa.esig.jaxb.tsl");
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        JAXBElement<TrustStatusListType> unmarshalled = (JAXBElement<TrustStatusListType>) unmarshaller.unmarshal(euLOTL);
        TrustServiceProviderListType list = unmarshalled.getValue().getTrustServiceProviderList();
        List<TSPType> tspTypeArray = new ArrayList<TSPType>();
        for (TSPType tsp : list.getTrustServiceProvider()) {
            for (TSPServiceType tspService : tsp.getTSPServices().getTSPService()) {
                if (!acSignature || tspService.getServiceInformation().getServiceTypeIdentifier().trim().equalsIgnoreCase("http://uri.etsi.org/TrstSvc/Svctype/CA/QC")) {
                    tspTypeArray.add(tsp);
                    break;
                }
            }
        }
        list.getTrustServiceProvider().clear();
        for (TSPType tsp : tspTypeArray) {
            list.getTrustServiceProvider().add(tsp);
        }

        TrustStatusListType euLOTLObj = unmarshalled.getValue();

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        JAXBIntrospector introspector = jc.createJAXBIntrospector();

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        if (null == introspector.getElementName(euLOTLObj)) {
            JAXBElement<Object> jaxbElement = new JAXBElement<Object>(new QName("TrustServiceStatusList "), Object.class, euLOTLObj);
            marshaller.marshal(jaxbElement, os);
        }
        String xml = os.toString();
        xml = xml.replaceAll("<ns3:", "<").replaceAll("</ns3:", "</").replaceAll("<ns2:", "<").replaceAll("</ns2:", "</").replaceAll("<ns4:", "<").replaceAll("</ns4:", "</").replaceAll("<ns5:", "<").replaceAll("</ns5:", "</");
        return xml;
    }
}
