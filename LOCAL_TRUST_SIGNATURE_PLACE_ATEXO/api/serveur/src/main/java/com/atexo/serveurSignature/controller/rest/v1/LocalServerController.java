package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.controller.config.DssConfiguration;
import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.localServer.model.SignatureEnLigneRequest;
import com.atexo.serveurSignature.core.domain.signature.model.SignResponse;
import liquibase.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/v1/local-server")
public class LocalServerController {
	private static Map<String, Map<String, Object>> hashSignature = new HashMap<String, Map<String, Object>>();
	private final ConfigurationService configurationService;

	private final DssConfiguration dssConfiguration;

	public LocalServerController(ConfigurationService configurationService, DssConfiguration dssConfiguration) {
		this.configurationService = configurationService;
		this.dssConfiguration = dssConfiguration;
	}

	@GetMapping(value = "/signature")
	public SignResponse getSignatureParam() {
		return SignResponse.builder()
				.activeHorodatageSignature(configurationService.getApplicationJwsHorodatageSignature())
				.lotlRootSchemeInfoUri(dssConfiguration.getLotlRootSchemeInfoUri())
				.lotlUrl(dssConfiguration.getLotlUrl())
				.ojUrl(dssConfiguration.getOjUrl())
				.shaHorodatage(configurationService.getApplicationSHAHorodatageType())
				.shaSignature(configurationService.getApplicationSHASignatureType())
				.build();
	}

	@PostMapping(value = "/signature-en-ligne")
	public SignatureEnLigneRequest generateSignatureRequest(@RequestBody SignatureEnLigneRequest request) throws Exception {
		request.setCallbackUrl(request.getCallbackUrl().replaceAll("&", "&amp;"));
		request.getUrlList().forEach(inputOutputUrl -> {
			if (null != inputOutputUrl.getInputUrl())
				inputOutputUrl.setInputUrl(inputOutputUrl.getInputUrl().replaceAll("&", "&amp;"));
			if (null != inputOutputUrl.getOutputUrl())
				inputOutputUrl.setOutputUrl(inputOutputUrl.getOutputUrl().replaceAll("&", "&amp;"));
		});
		String urlJNLP = request.getServeurCryptoURLPublique();
		if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
			urlJNLP = configurationService.getURLJNLP();
		}
		request.setServeurCryptoURLPublique(urlJNLP + configurationService.getApplicationContext());
		request.setAppId("SIGNATURE_EN_LIGNE");
		request.setSessionId(UUID.randomUUID().toString());
		return request;
	}

}
