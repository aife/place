package com.atexo.serveurSignature.controller.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.Principal;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${signature.version}")
	private String projectVersion;

	@Bean
	public Docket restDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.ignoredParameterTypes(Principal.class)
				.groupName("REST")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.atexo.serveurSignature.controller.rest"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder().version(projectVersion)
						.title("Serveur Signature REST API").description("Documentation Serveur Signature REST API v" + projectVersion).build());

	}

	@Bean
	public Docket webDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.ignoredParameterTypes(Principal.class)
				.groupName("WEB")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.atexo.serveurSignature.controller.web"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder().version(projectVersion)
						.title("Serveur Signature WEB").description("Documentation Serveur Signature WEB v" + projectVersion).build()) ;
	}


	@Bean
	UiConfiguration uiConfiguration() {
		return UiConfigurationBuilder.builder()
				.build();
	}
}
