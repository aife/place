package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.config.LogDBService;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.enums.CertificatVerificationEtatEnum;
import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.domain.signature.service.SignatureVerificationService;
import com.atexo.serveurSignature.core.exception.JetonSignatureIncorrectException;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.core.exception.SignatureException;
import com.atexo.serveurSignature.core.exception.ValidationException;
import com.atexo.serveurSignature.core.utilitaire.Signature;
import com.atexo.serveurSignature.core.utilitaire.Signatures;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/v1")
public class SignatureController {
	private static final Logger logger = LoggerFactory.getLogger(SignatureController.class);
	private final LogDBService logDBService;
	private final JMXService jmxService;
	private final SignatureVerificationService signatureVerificationService;

	public SignatureController(LogDBService logDBService, JMXService jmxService, SignatureVerificationService signatureVerificationService) {
		this.logDBService = logDBService;
		this.jmxService = jmxService;
		this.signatureVerificationService = signatureVerificationService;
	}


	@GetMapping(value = "urlvie")
	public ResponseEntity<String> test() throws MauvaisParametreException {
		return new ResponseEntity<>("Ok", HttpStatus.OK);
	}

	@RequestMapping(value = "verifierSignature", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Signature> verifierSignatureV1(@RequestBody(required = false) ContextSignature context, @RequestParam(name = "plateforme", required = false) String plateforme) throws MauvaisParametreException {
		long start = System.currentTimeMillis();

		Signatures info = signatureVerificationService.extractSignatures(context);
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_SIGNATURE);
		logDBService.insertValidation(plateforme, UtilitaireMarshal.marshalJSON(context), info, System.currentTimeMillis() - start);
		assert info != null;
		if (info.getSignatures() != null && !info.getSignatures().isEmpty()) {
			return new ResponseEntity<>(info.getSignatures().get(0), HttpStatus.OK);
		} else {
			return new ResponseEntity<Signature>(new Signature(), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "v2/verifierSignature", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Signatures> verifierSignature(@RequestBody(required = false) ContextSignature context, @RequestParam(name = "plateforme", required = false) String plateforme) throws MauvaisParametreException {
		long start = System.currentTimeMillis();

		Signatures info = signatureVerificationService.extractSignatures(context);
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_SIGNATURE);
		logDBService.insertValidation(plateforme, UtilitaireMarshal.marshalJSON(context), info, System.currentTimeMillis() - start);
		return new ResponseEntity<>(info, HttpStatus.OK);
	}


	@RequestMapping(value = {"verifierSignaturePDF", "v2/verifierSignaturePDF"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Signatures> uploadFile(@RequestParam("fichier") MultipartFile file) throws MauvaisParametreException, IOException {
		File signaturePDFTmp = File.createTempFile("signaturePDF", ".atx");
		try {
			FileUtils.copyInputStreamToFile(file.getInputStream(), signaturePDFTmp);
			Signatures info = null;
			long memoireLibre = Runtime.getRuntime().freeMemory();
			logger.info("memoire disponible: {}", memoireLibre / 1024 / 1014);
			if (FileUtils.sizeOf(signaturePDFTmp) * 2 > memoireLibre) {
				logger.error("pas assez memoire disponible: {}Mo", memoireLibre / 1024 / 1014);
				throw new IOException("pas de memoire disponible");
			}
			try {
				info = signatureVerificationService.validationDocumentDSS(null, signaturePDFTmp);
			} catch (ValidationException e) {
				info = new Signatures();
				info.setEtat(CertificatVerificationEtatEnum.CertificatInvalide.getCodeRetour());
			}
			return new ResponseEntity<Signatures>(info, HttpStatus.OK);
		} finally {
			signaturePDFTmp.delete();
		}
	}
}
