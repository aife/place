package com.atexo.serveurSignature.controller.config;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.io.InputStream;

@Configuration
public class SignatureConfiguration implements WebMvcConfigurer {
	@Value("${signature.mock-folder}")
	private String signatureMockFolder;
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
		StatusPrinter.print(lc);
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean(name = "multipartResolver")
	public StandardServletMultipartResolver resolver() {
		return new StandardServletMultipartResolver();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String storageOnlyOfficeFolderResources = "file:" + signatureMockFolder + "/";
		registry.addResourceHandler("/v1/files/**").addResourceLocations(storageOnlyOfficeFolderResources);
		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("swagger-resources/**").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/v1/resources/**").addResourceLocations("classpath:/public/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Bean
	public JoranConfigurator log() {
		LoggerContext context = (LoggerContext) org.slf4j.LoggerFactory.getILoggerFactory();
		JoranConfigurator configurator = null;
		try {
			configurator = new JoranConfigurator();
			configurator.setContext(context);
			context.reset();
			InputStream logConfStream = null;
			logConfStream = SignatureConfiguration.class.getResourceAsStream("/logback-signature.xml");
			if (logConfStream == null) {
				logConfStream = SignatureConfiguration.class.getResourceAsStream("/logback.xml");
			}
			configurator.doConfigure(logConfStream);
		} catch (JoranException e) {
			e.printStackTrace();
		}
		StatusPrinter.printInCaseOfErrorsOrWarnings(context);
		return configurator;
	}

}
