package com.atexo.serveurSignature.controller.rest.v1.mock;

import com.atexo.serveurSignature.core.exception.MessageExceptionEnum;
import com.atexo.serveurSignature.core.exception.SignatureException;
import com.atexo.serveurSignature.core.utilitaire.DocumentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@RestController("MPEMockRestController-v1")
@RequestMapping("/v1/mpeMock")
@Slf4j
public class MPEMockRestController {

	@Value("${signature.mock-folder}")
	private String signatureMockFolder;


	@PostMapping(value = "output")
	public String outputMock(HttpServletRequest request) throws SignatureException {
		try {
			String disposition = request.getHeader("content-disposition");
			String fileName = disposition.replaceFirst("(?i)^.*filename=\"?([^\"]+)\"?.*$", "$1");
			File saved = DocumentUtils.saveFile(request.getInputStream(), signatureMockFolder + File.separator +
					"signedFile", fileName, true);
			log.info("Fichier signé enregistré {}", saved.getAbsolutePath());

			return "OK";
		} catch (Exception e) {
			throw new SignatureException(MessageExceptionEnum.SIGNATURE_ECHEC_ENREGISTREMENT, e);
		}
	}

}
