package com.atexo.serveurSignature.controller.rest.v2;

import com.atexo.serveurSignature.controller.web.v1.mock.FileReponse;
import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.localServer.model.InputOutputUrl;
import com.atexo.serveurSignature.core.domain.localServer.model.SignatureEnLigneRequest;
import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.enums.SignatureTypeEnum;
import com.atexo.serveurSignature.core.exception.SignatureException;
import com.atexo.serveurSignature.core.utilitaire.DocumentUtils;
import com.atexo.serveurSignature.core.utilitaire.Signatures;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Collections;

@RestController("MPEMockRestController-v2")
@RequestMapping("/v2/mpeMock")
public class MPEMockRestController {

	@Value("${signature.mock-folder}")
	private String signatureMockFolder;
	private final ConfigurationService configurationService;

	public MPEMockRestController(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@PostMapping(value = "validation")
	@ResponseBody
	public ResponseEntity<Signatures> singleFileUpload(FileReponse fileBucket) {
		ContextSignature context = new ContextSignature();
		String[] array = new String[2];
		try {
			array[0] = DigestUtils.shaHex(fileBucket.getFichier().getBytes());
			array[1] = DigestUtils.sha256Hex(fileBucket.getFichier().getBytes());
			context.setHashes(array);
			if (!fileBucket.getSignature().isEmpty()) {
				context.setSignatureBase64(Base64.getEncoder().encodeToString(fileBucket.getSignature().getBytes()));
				context.setSignatureType(SignatureTypeEnum.INCONNUE);
			} else {
				context.setPdfBase64(Base64.getEncoder().encodeToString(fileBucket.getFichier().getBytes()));
				context.setSignatureType(SignatureTypeEnum.PADES);
			}

			RestTemplate restTemplate = new RestTemplate();
			String mockUrl = configurationService.getMockUrl() + "/" + configurationService.getApplicationContext();
			/*
			MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
			bodyMap.add("fichier", new FileSystemResource(convert(fileBucket.getFichier())));
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
			*/
			Signatures resultat = restTemplate.postForObject(mockUrl + "/v2/verifierSignature?plateforme=PLACE", context,
					Signatures.class);
			return ResponseEntity.ok().body(resultat);
		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}

	@GetMapping(value = "signatureJNLP")
	public ResponseEntity<String> getSignatureJNLP(@RequestParam(name = "useragent", required = false) String useragent, @RequestParam(name = "typeJnlp", required = false) String typeJnlp) {
		RestTemplate restTemplate = new RestTemplate();
		String mockUrl = configurationService.getMockUrl();

		return restTemplate.getForEntity(mockUrl + "/" + configurationService.getApplicationContext() + "/" + typeJnlp + "?serveurCryptoURLPublique=" + mockUrl +
						"&useragent=" + useragent,
				String.class);

	}


	@GetMapping(value = "verificationSignatureJNLP")
	public ResponseEntity<String> getverificationSignatureJNLP(@RequestParam(name = "useragent", required = false) String useragent, @RequestParam(name = "typeJnlp", required = false) String typeJnlp) {
		RestTemplate restTemplate = new RestTemplate();

		String mockUrl = configurationService.getMockUrl();
		return restTemplate.postForEntity(mockUrl + "/" + configurationService.getApplicationContext() + "/" + typeJnlp + "?serveurCryptoURLPublique=" + mockUrl +
						"&useragent=" + useragent, null,
				String.class);

	}


	@PostMapping(value = "/signature-en-ligne")
	public ResponseEntity<SignatureEnLigneRequest> signFileOnline(@RequestPart MultipartFile file) throws IOException, SignatureException {
		File toSign = DocumentUtils.saveFile(file.getInputStream(), signatureMockFolder + File.separator + "onlineSign", file.getOriginalFilename(), true);
		RestTemplate restTemplate = new RestTemplate();
		String mockUrl = configurationService.getMockUrl();
		String inputUrl = mockUrl + configurationService.getApplicationContext() + "/files/onlineSign/" + toSign.getName();
		String outputUrl = mockUrl + configurationService.getApplicationContext() + "/mpeMock/output";
		SignatureEnLigneRequest request = SignatureEnLigneRequest.builder()
				.serveurCryptoURLPublique(mockUrl)
				.callbackUrl(mockUrl)
				.urlList(Collections.singletonList(InputOutputUrl.builder()
						.size(toSign.length())
						.fileName(toSign.getName())
						.inputUrl(inputUrl).outputUrl(outputUrl).build()))
				.build();
		return restTemplate.postForEntity(mockUrl + "/" + configurationService.getApplicationContext() + "/local-server/signature-en-ligne", request,
				SignatureEnLigneRequest.class);

	}


}
