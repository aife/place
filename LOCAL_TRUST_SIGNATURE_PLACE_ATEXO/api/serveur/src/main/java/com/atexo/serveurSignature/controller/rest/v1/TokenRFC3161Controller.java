package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.core.config.LogDBService;
import com.atexo.serveurSignature.core.domain.horodatage.service.HorodatageService;
import com.atexo.serveurSignature.core.domain.horodatage.service.impl.HorodatageFactory;
import com.atexo.serveurSignature.core.exception.HorodatageException;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@RequestMapping("v1/tokenRFC3161")
public class TokenRFC3161Controller {
	private static final Logger logger = LoggerFactory.getLogger(TokenRFC3161Controller.class);
	private final HorodatageFactory horodatageFactory;
	private final LogDBService logDBService;

	public TokenRFC3161Controller(HorodatageFactory horodatageFactory, LogDBService logDBService) {
		this.horodatageFactory = horodatageFactory;
		this.logDBService = logDBService;
	}


	@PostMapping
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		long start = System.currentTimeMillis();
		byte[] der = null;
		byte[] digest = null;
		try {
			digest = IOUtils.toByteArray(request.getInputStream());
			logger.info("Timestamp digest debut    : " + Utils.toHex(digest));
			HorodatageService horodatageService = horodatageFactory.getProviderHorodatage(request.getParameter("plateforme"));
			der = horodatageService.getTokenRFC3161(digest);
			IOUtils.copy(new ByteArrayInputStream(der), response.getOutputStream());
		} catch (HorodatageException | IOException e) {
			throw new MauvaisParametreException("HORODATAGE", e.fillInStackTrace());
		} finally {
			boolean ok = (der != null);
			logDBService.insertHorodatage(request.getParameter("plateforme"), Utils.toHex(digest), der, ok, System.currentTimeMillis() - start);
		}
	}

	@GetMapping
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		long start = System.currentTimeMillis();
		byte[] der = null;
		byte[] digest = null;
		try {
			String hash = request.getParameter("hash256");
			digest = IOUtils.toByteArray(request.getInputStream());
			logger.info("Timestamp digest algorithm: " + DigestAlgorithm.SHA1.getName());
			logger.info("Timestamp digest debut    : " + Utils.toHex(digest));
			HorodatageService horodatageService = horodatageFactory.getProviderHorodatage(request.getParameter("plateforme"));
			der = horodatageService.getTokenRFC3161(digest);
			IOUtils.copy(new ByteArrayInputStream(der), response.getOutputStream());
		} catch (HorodatageException | IOException e) {
			throw new MauvaisParametreException("HORODATAGE", e.fillInStackTrace());
		} finally {
			boolean ok = (der != null);
			logDBService.insertHorodatage(request.getParameter("plateforme"), Utils.toHex(digest), der, ok, System.currentTimeMillis() - start);
		}
	}


}
