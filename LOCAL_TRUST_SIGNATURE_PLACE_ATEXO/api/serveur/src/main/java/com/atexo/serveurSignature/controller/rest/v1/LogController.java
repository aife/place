package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.core.config.LogDBService;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

@RestController
@RequestMapping("/v1")
public class LogController {
	private static final Logger logger = LoggerFactory.getLogger(LogController.class);

	private final LogDBService logDBService;

	public LogController(LogDBService logDBService) {
		this.logDBService = logDBService;
	}

	@RequestMapping(value = "logSignature", method = RequestMethod.POST, produces = MediaType.TEXT_HTML_VALUE)
	public String verifierSignature(@RequestParam String certificat,
			@RequestParam String typeSignature,
			@RequestParam String plateforme,
			@RequestParam int tempsRequete) throws MauvaisParametreException {

		byte[] bytes = Base64.getMimeDecoder().decode(certificat);
		InputStream in = new ByteArrayInputStream(bytes);
		CertificateFactory certFactory;
		try {
			certFactory = CertificateFactory.getInstance("X.509");
			X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(in);
			logDBService.insertSignature(plateforme, certificate, typeSignature, tempsRequete);
		} catch (CertificateException e) {
			logger.warn("Erreur certificate ", e.fillInStackTrace());
		}
		return "OK";
	}

}
