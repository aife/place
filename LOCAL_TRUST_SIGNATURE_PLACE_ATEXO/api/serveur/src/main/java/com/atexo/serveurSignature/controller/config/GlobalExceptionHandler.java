package com.atexo.serveurSignature.controller.config;

import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.domain.signature.model.Message;
import com.atexo.serveurSignature.core.exception.JetonSignatureIncorrectException;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.core.exception.SignatureException;
import com.atexo.serveurSignature.core.utilitaire.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger("fileError");
	private final JMXService jmxService;

	public GlobalExceptionHandler(JMXService jmxService) {
		this.jmxService = jmxService;
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler({MauvaisParametreException.class})
	protected ResponseEntity<Object> handleInvalidRequest(MauvaisParametreException e, WebRequest request) {
		Message error = new Message();
		error.setCode("MAUVAIS PARAMETRE");
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		e.printStackTrace(pw);
		error.setMessage(writer.toString());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		jmxService.addActionErreur(JMXService.MAUVAIS_PARAMETRE);
		IOUtils.closeQuietly(pw);
		IOUtils.closeQuietly(writer);
		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler({SignatureException.class})
	protected ResponseEntity<Object> handleInvalidRequest(SignatureException e, WebRequest request) {
		Message error = new Message();
		error.setCode("ERREUR DE SIGNATURE");
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		e.printStackTrace(pw);
		error.setMessage(writer.toString());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		jmxService.addActionErreur(JMXService.VERIFICATION_SIGNATURE);
		IOUtils.closeQuietly(pw);
		IOUtils.closeQuietly(writer);
		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}


	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({JetonSignatureIncorrectException.class})
	protected ResponseEntity<Object> handleInvalidRequest(JetonSignatureIncorrectException e, WebRequest request) {
		Message error = new Message();
		error.setCode("JETON DE SIGNATURE INCORRECT");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		jmxService.addActionErreur(JMXService.VERIFICATION_SIGNATURE);
		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> handleIOException(RuntimeException e, WebRequest request) {
		logger.error("", e.fillInStackTrace());
		Message error = new Message();
		error.setCode("ERREUR");
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		e.printStackTrace(pw);
		error.setMessage(writer.toString());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		jmxService.addActionErreur(JMXService.INTERNAL_SERVER_ERROR);
		IOUtils.closeQuietly(pw);
		IOUtils.closeQuietly(writer);
		return handleExceptionInternal(e, error, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
