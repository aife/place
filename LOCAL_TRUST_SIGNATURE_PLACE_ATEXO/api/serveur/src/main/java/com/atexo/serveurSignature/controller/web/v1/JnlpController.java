package com.atexo.serveurSignature.controller.web.v1;

import com.atexo.serveurSignature.controller.config.DssConfiguration;
import com.atexo.serveurSignature.core.config.ConfigurationService;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.enumerations.SignaturePackaging;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.pades.PAdESTimestampParameters;
import eu.europa.esig.dss.pades.signature.PAdESService;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxNativeObjectFactory;
import eu.europa.esig.dss.signature.DocumentSignatureService;
import eu.europa.esig.dss.token.AbstractSignatureTokenConnection;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import liquibase.util.StringUtils;
import net.sf.uadetector.OperatingSystemFamily;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyStore.PasswordProtection;
import java.util.*;

@Controller
@RequestMapping("/v1")
public class JnlpController {
    private static Map<String, Map<String, Object>> hashSignature = new HashMap<String, Map<String, Object>>();

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private DssConfiguration dssConfiguration;

    @GetMapping(value = "/generateSignatureJNLP", produces = {"application/x-java-jnlp-file"})
    public String generateSignatureJNLP(@RequestParam(name = "serveurCryptoURLPublique", required = false) String serveurCryptoURLPublique, @RequestParam(name = "useragent", required = false) String useragent, Model model) throws Exception {

        String urlJNLP = serveurCryptoURLPublique;
        if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
            urlJNLP = configurationService.getURLJNLP();
        }
        model.addAttribute("urlPubliqueServeurCrypto", urlJNLP + configurationService.getApplicationContext());
        model.addAttribute("horodatagePadesCades", configurationService.getApplicationJwsHorodatageSignature());
        model.addAttribute("shaSignatureType", configurationService.getApplicationSHASignatureType());
        model.addAttribute("shaHorodatageType", configurationService.getApplicationSHAHorodatageType());
        model.addAttribute("plateforme", configurationService.getValidationOriginePlateforme());
        String type = "";
        UserAgentStringParser parser = UADetectorServiceFactory.getOnlineUpdatingParser();
        ReadableUserAgent agent = parser.parse(useragent);
        if (agent.getOperatingSystem().getFamily().equals(OperatingSystemFamily.OS_X) || agent.getOperatingSystem().getFamily().equals(OperatingSystemFamily.MAC_OS)) {
            type = "Swing";
        }
        model.addAttribute("useragent", URLEncoder.encode(useragent, "UTF-8"));
        model.addAttribute("type", type);
        model.addAttribute("lotlRootSchemeInfoUri", dssConfiguration.getLotlRootSchemeInfoUri());
        model.addAttribute("lotlUrl", dssConfiguration.getLotlUrl());
        model.addAttribute("ojUrl", dssConfiguration.getOjUrl());
        return "signatureJNLP";
    }

    @PostMapping(value = "/generateSignatureHashJNLP", produces = {"application/x-java-jnlp-file"})
    public String generateSignatureHashJNLP(@RequestParam(name = "serveurCryptoURLPublique", required = false) String serveurCryptoURLPublique, Model model, HttpServletResponse response, HttpServletRequest request) throws Exception {

        List<String> parameters = new ArrayList<String>();
        int i = 1;
        while (true) {
            String hash = request.getParameter("hash" + i);
            String jeton = request.getParameter("jeton" + i);

            if (hash == null || jeton == null) {
                break;
            }
            parameters.add(hash);
            parameters.add(jeton);
            i++;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        String uuid = UUID.randomUUID().toString();
        model.addAttribute("uuid", uuid);
        map.put("urlServeurAppelant", request.getParameter("urlServeurAppelant"));
        model.addAttribute("urlServeurAppelant", request.getParameter("urlServeurAppelant"));
        map.put("urlPubliqueServeurCrypto", serveurCryptoURLPublique + configurationService.getApplicationContext());
        String urlJNLP = serveurCryptoURLPublique;
        if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
            urlJNLP = configurationService.getURLJNLP();
        }
        model.addAttribute("urlPubliqueServeurCrypto", urlJNLP + configurationService.getApplicationContext());
        model.addAttribute("horodatagePadesCades", configurationService.getApplicationJwsHorodatageSignature());
        model.addAttribute("shaType", configurationService.getApplicationSHASignatureType());
        model.addAttribute("parameters", parameters);
        map.put("parameters", parameters);
        hashSignature.put(uuid, map);
        return "signatureHashJNLP";
    }

    @PostMapping(value = "/generateSignaturePadesJNLP", produces = {"application/x-java-jnlp-file"})
    public String generateSignaturePadesJNLP(@RequestParam(name = "serveurCryptoURLPublique", required = false) String serveurCryptoURLPublique, @RequestParam(name = "callbackUrl", required = false) String callbackUrl, Model model, HttpServletResponse response, HttpServletRequest request) throws Exception {

        List<String> parameters = new ArrayList<String>();
        int i = 1;
        parameters.add(request.getParameter("callbackUrl").replaceAll("&", "&amp;"));
        while (true) {
            String inputFileUrl = request.getParameter("inputFileUrl" + i);
            String outputFileUrl = request.getParameter("outputFileUrl" + i);

            if (outputFileUrl == null || outputFileUrl == null) {
                break;
            }
            parameters.add(inputFileUrl.replaceAll("&", "&amp;"));
            parameters.add(outputFileUrl.replaceAll("&", "&amp;"));
            i++;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("urlPubliqueServeurCrypto", serveurCryptoURLPublique + configurationService.getApplicationContext());
        String urlJNLP = serveurCryptoURLPublique;
        if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
            urlJNLP = configurationService.getURLJNLP();
        }
        model.addAttribute("urlPubliqueServeurCrypto", urlJNLP + configurationService.getApplicationContext());
        String uuid = UUID.randomUUID().toString();
        model.addAttribute("uuid", uuid);
        model.addAttribute("parameters", parameters);
        map.put("parameters", parameters);
        hashSignature.put(uuid, map);
        return "signaturePadesJNLP";
    }

    @GetMapping(value = "/checkSignatureJNLP", produces = {"application/x-java-jnlp-file"})
    public String checkSignatureJNLP(@RequestParam(name = "serveurCryptoURLPublique", required = false) String serveurCryptoURLPublique, @RequestParam(name = "useragent", required = true) String useragent, Model model) {
        String urlJNLP = serveurCryptoURLPublique;
        if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
            urlJNLP = configurationService.getURLJNLP();
        }
        model.addAttribute("urlPubliqueServeurCrypto", urlJNLP + configurationService.getApplicationContext());
        model.addAttribute("horodatagePadesCades", configurationService.getApplicationJwsHorodatageSignature());
        model.addAttribute("shaSignatureType", configurationService.getApplicationSHASignatureType());
        model.addAttribute("shaHorodatageType", configurationService.getApplicationSHAHorodatageType());
        model.addAttribute("plateforme", configurationService.getValidationOriginePlateforme());
        String type = "";
        UserAgentStringParser parser = UADetectorServiceFactory.getOnlineUpdatingParser();
        ReadableUserAgent agent = parser.parse(useragent);
        if (agent.getOperatingSystem().getFamily().equals(OperatingSystemFamily.MAC_OS) || agent.getOperatingSystem().getFamily().equals(OperatingSystemFamily.OS_X)) {
            type = "Swing";
        }
        try {
            model.addAttribute("useragent", URLEncoder.encode(useragent, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
        }
        model.addAttribute("lotlRootSchemeInfoUri", dssConfiguration.getLotlRootSchemeInfoUri());
        model.addAttribute("lotlUrl", dssConfiguration.getLotlUrl());
        model.addAttribute("ojUrl", dssConfiguration.getOjUrl());
        model.addAttribute("type", type);
        return "signatureJNLP";
    }

    @GetMapping(value = "/checkHashSignatureJNLP", produces = {"application/x-java-jnlp-file"})
    public String checkhashSignatureJNLP(@RequestParam(name = "uuid", required = false) String uuid, Model model) {
        Map<String, Object> map = hashSignature.get(uuid);
        model.addAttribute("urlPubliqueServeurCrypto", map.get("urlPubliqueServeurCrypto"));
        model.addAttribute("parameters", map.get("parameters"));
        model.addAttribute("uuid", uuid);
        model.addAttribute("urlServeurAppelant", map.get("urlServeurAppelant"));
        hashSignature.remove(uuid);
        return "signatureHashJNLP";
    }

    @GetMapping(value = "/checkPadesSignatureJNLP", produces = {"application/x-java-jnlp-file"})
    public String checkPadesSignatureJNLP(@RequestParam(name = "uuid", required = false) String uuid, Model model) {
        Map<String, Object> map = hashSignature.get(uuid);
        model.addAttribute("urlPubliqueServeurCrypto", map.get("urlPubliqueServeurCrypto"));
        model.addAttribute("parameters", map.get("parameters"));
        model.addAttribute("uuid", uuid);
        hashSignature.remove(uuid);
        return "signaturePadesJNLP";
    }

    @GetMapping(value = "/outilTest", produces = {"application/x-java-jnlp-file"})
    public String outilTest(@RequestParam(name = "serveurCryptoURLPublique", required = false) String serveurCryptoURLPublique, Model model, HttpServletResponse response) {
        String urlJNLP = serveurCryptoURLPublique;
        if (!StringUtils.isEmpty(configurationService.getURLJNLP())) {
            urlJNLP = configurationService.getURLJNLP();
        }
        model.addAttribute("urlPubliqueServeurCrypto", urlJNLP + configurationService.getApplicationContext());
        response.setHeader("Content-Disposition", "attachment; filename=\"Outil de test de configuration.jnlp\"");
        return "outilTestCryptoJNLP";
    }

    @RequestMapping(value = "padesSignature", method = RequestMethod.POST, produces = MediaType.APPLICATION_PDF_VALUE)
    public void padesSignature(@RequestPart(name = "certificat", required = true) MultipartFile certificat, @RequestParam(name = "password") String password, @RequestPart(name = "pdfFichier", required = true) MultipartFile pdfFichier,
                               HttpServletResponse response) throws IOException {
        File certificatFile = null;
        File padesFile = null;
        try {
            certificatFile = File.createTempFile("certificat_", "" + System.currentTimeMillis());
            padesFile = File.createTempFile("pades_", "" + System.currentTimeMillis());
            FileUtils.copyToFile(pdfFichier.getInputStream(), padesFile);
            FileUtils.copyToFile(certificat.getInputStream(), certificatFile);
        } catch (IOException e) {
            IOUtils.closeQuietly(pdfFichier.getInputStream());
            IOUtils.closeQuietly(certificat.getInputStream());
        }

        DSSDocument documentToSign = new FileDocument(padesFile);

        AbstractSignatureTokenConnection signingToken = new Pkcs12SignatureToken(certificatFile, new PasswordProtection(password.toCharArray()));
        DSSPrivateKeyEntry privateKey = signingToken.getKeys().get(0);
        PAdESSignatureParameters signatureParameters = new PAdESSignatureParameters();
        signatureParameters.bLevel().setSigningDate(new Date());
        signatureParameters.setSigningCertificate(privateKey.getCertificate());
        signatureParameters.setCertificateChain(privateKey.getCertificateChain());
        signatureParameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
        signatureParameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_B);
        signatureParameters.setSignWithExpiredCertificate(true);
        CertificateVerifier certificateVerifier = new CommonCertificateVerifier();
        DocumentSignatureService<PAdESSignatureParameters, PAdESTimestampParameters> service = new PAdESService(certificateVerifier);
        ((PAdESService) service).setPdfObjFactory(new PdfBoxNativeObjectFactory());
        ToBeSigned dataToSign = service.getDataToSign(documentToSign, signatureParameters);
        DigestAlgorithm digestAlgorithm = signatureParameters.getDigestAlgorithm();
        SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKey);
        final DSSDocument signedDocument = service.signDocument(documentToSign, signatureParameters, signatureValue);
        InputStream in = signedDocument.openStream();
        response.setHeader("contentType", "application/pdf");
        try {
            IOUtils.copy(signedDocument.openStream(), response.getOutputStream());
        } catch (IOException e) {
            throw e;
        } finally {
            FileUtils.deleteQuietly(padesFile);
            FileUtils.deleteQuietly(certificatFile);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(signedDocument.openStream());
            signingToken.close();
        }
    }
}
