package com.atexo.serveurSignature.controller.rest.v2;

import com.atexo.serveurSignature.controller.web.v1.JnlpController;
import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.signature.model.Parametre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v2/admin/parametrage")
public class ParametrageController {
	private static final Logger logger = LoggerFactory.getLogger(JnlpController.class);

	private final ConfigurationService configurationService;

	public ParametrageController(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@GetMapping
	public List<Parametre> parametrage() throws Exception {
		return configurationService.getConfiguration();

	}

	@PostMapping
	public List<Parametre> parametrageValidation(@RequestBody Parametre parametre) throws Exception {
		configurationService.setConfiguration(parametre);
		return configurationService.getConfiguration();

	}

}
