package com.atexo.serveurSignature.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication()
@EnableWebMvc
@EnableScheduling
@ComponentScan(basePackages = {"eu.europa.esig.dss", "com.atexo.serveurSignature"})
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
public class AppWeb extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppWeb.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AppWeb.class, args);
	}

}
