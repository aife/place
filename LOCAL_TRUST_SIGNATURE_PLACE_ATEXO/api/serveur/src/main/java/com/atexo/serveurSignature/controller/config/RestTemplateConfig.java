package com.atexo.serveurSignature.controller.config;

import com.atexo.serveurSignature.core.utilitaire.TrustAllManager;
import eu.europa.esig.dss.service.http.proxy.ProxyConfig;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
@Configuration
@Slf4j
public class RestTemplateConfig {
	private static final String SSL_PROTOCOL = "TLS";
	@Bean
	RestTemplate restTemplate() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		try {
			requestFactory.setHttpClient(createHttpClient());
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return new RestTemplate(requestFactory);
	}
	private HttpClient createHttpClient() throws KeyManagementException, NoSuchAlgorithmException {
		HttpClientBuilder httpClientBuilder = getHttpClientBuilder();
		httpClientBuilder.setSSLContext(createSSLContext());
		return httpClientBuilder.build();
	}
	private HttpClientBuilder getHttpClientBuilder() {
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		ProxyConfig proxyConfig = getProxyConfig();
		if (proxyConfig != null) {
			HttpHost proxy = new HttpHost(proxyConfig.getHttpProperties().getHost(), proxyConfig.getHttpProperties().getPort(), Proxy.Type.HTTP.name());
			httpClientBuilder.setProxy(proxy);
		}
		return httpClientBuilder;
	}
	private SSLContext createSSLContext() throws NoSuchAlgorithmException, KeyManagementException {
		SSLContext sslContext = SSLContext.getInstance(SSL_PROTOCOL);
		sslContext.init(null, new TrustManager[]{TrustAllManager.THE_INSTANCE}, new SecureRandom());
		return sslContext;
	}
	private ProxyConfig getProxyConfig() {
		ProxyConfig proxyConfig = new ProxyConfig();
		if (System.getProperty("http.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("http.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("http.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("http.proxyUser"));
			proxyProperties.setPassword(System.getProperty("http.proxyPassword"));
			proxyConfig.setHttpProperties(proxyProperties);
		}
		if (System.getProperty("https.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("https.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("https.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("https.proxyUser"));
			proxyProperties.setPassword(System.getProperty("https.proxyPassword"));
			proxyConfig.setHttpsProperties(proxyProperties);
		}
		if (proxyConfig.getHttpProperties() == null && proxyConfig.getHttpsProperties() == null) {
			return null;
		}
		return proxyConfig;
	}
}
