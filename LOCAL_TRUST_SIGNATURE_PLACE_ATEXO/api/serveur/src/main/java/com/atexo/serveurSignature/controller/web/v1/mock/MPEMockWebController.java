package com.atexo.serveurSignature.controller.web.v1.mock;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.enums.SignatureTypeEnum;
import com.atexo.serveurSignature.core.utilitaire.DocumentUtils;
import com.atexo.serveurSignature.core.utilitaire.Signatures;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.xml.security.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

@Controller()
@RequestMapping("/v1")
public class MPEMockWebController {
	private static final Logger logger = LoggerFactory.getLogger(MPEMockWebController.class);

	@Value("${signature.mock-folder}")
	private String signatureMockFolder;
	private final FileValidator fileValidator;

	private final ConfigurationService configurationService;

	public MPEMockWebController(FileValidator fileValidator, ConfigurationService configurationService) {
		this.fileValidator = fileValidator;
		this.configurationService = configurationService;
	}

	@InitBinder("fileBucket")
	protected void initBinderFileBucket(WebDataBinder binder) {
		binder.setValidator(fileValidator);
	}

	@RequestMapping(value = "/mpeMock/signatureJNLP", method = RequestMethod.GET)
	public String getSignatureJNLP(ModelMap model, HttpServletResponse response, HttpServletRequest request) {
		FileReponse fileModel = new FileReponse();
		model.addAttribute("fileReponse", fileModel);
		model.addAttribute("urlPubliqueServeurCrypto", FileUtils.getTempDirectory());
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> resultat = restTemplate.getForEntity(configurationService.getMockUrl() + "/generateSignatureJNLP?serveurCryptoURLPublique" + configurationService.getMockUrl(),
				String.class);

		model.addAttribute("message", UtilitaireMarshal.marshalJSON(resultat).replaceAll(",", ",<br/>"));

		response.setHeader("content-Type", "jnlp");
		response.setHeader("filename", "signature.jnlp");
		return "mock/signatureJNLP.";
	}

	@RequestMapping(value = "/mpeMock/validation", method = RequestMethod.GET)
	public String getSingleUploadPage(ModelMap model) {
		FileReponse fileModel = new FileReponse();
		model.addAttribute("fileReponse", fileModel);
		model.addAttribute("urlPubliqueServeurCrypto", FileUtils.getTempDirectory());
		return "mock/singleFileUploader";
	}

	@RequestMapping(value = "/mpeMock/validation", method = RequestMethod.POST)
	public String singleFileUpload(FileReponse fileBucket, BindingResult result, ModelMap model) {
		ContextSignature context = new ContextSignature();
		String[] array = new String[2];
		try {
			array[0] = DigestUtils.shaHex(fileBucket.getFichier().getBytes());
			array[1] = DigestUtils.sha256Hex(fileBucket.getFichier().getBytes());
			context.setHashes(array);
			if (!fileBucket.getSignature().isEmpty()) {
				context.setSignatureBase64(Base64.encode(fileBucket.getSignature().getBytes()));
				context.setSignatureType(SignatureTypeEnum.INCONNUE);
			} else {
				context.setPdfBase64(Base64.encode(fileBucket.getFichier().getBytes()));
				context.setSignatureType(SignatureTypeEnum.PADES);
			}

			RestTemplate restTemplate = new RestTemplate();

			Signatures resultat = restTemplate.postForObject(configurationService.getMockUrl() + "/v2/verifierSignature?plateforme=PLACE", context,
					Signatures.class);
			model.addAttribute("message", UtilitaireMarshal.marshalJSON(resultat).replaceAll(",", ",<br/>"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FileReponse fileModel = new FileReponse();
		model.addAttribute("fileReponse", fileModel);
		model.addAttribute("urlPubliqueServeurCrypto", FileUtils.getTempDirectory());
		return "mock/validationResultat";
	}


}
