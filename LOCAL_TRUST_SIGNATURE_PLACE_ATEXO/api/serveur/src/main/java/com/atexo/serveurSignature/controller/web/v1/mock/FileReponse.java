package com.atexo.serveurSignature.controller.web.v1.mock;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileReponse {

    MultipartFile fichier;
    MultipartFile signature;
    
    
}
