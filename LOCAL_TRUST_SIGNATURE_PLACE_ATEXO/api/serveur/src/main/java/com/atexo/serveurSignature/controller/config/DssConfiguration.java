package com.atexo.serveurSignature.controller.config;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.utilitaire.AtexoCrlSource;
import com.atexo.serveurSignature.core.utilitaire.AtexoDataLoader;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.http.commons.FileCacheDataLoader;
import eu.europa.esig.dss.service.http.commons.OCSPDataLoader;
import eu.europa.esig.dss.service.http.commons.TimestampDataLoader;
import eu.europa.esig.dss.service.http.proxy.ProxyConfig;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.spi.client.http.DSSFileLoader;
import eu.europa.esig.dss.spi.client.http.IgnoreDataLoader;
import eu.europa.esig.dss.spi.client.http.NativeHTTPDataLoader;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.spi.x509.KeyStoreCertificateSource;
import eu.europa.esig.dss.spi.x509.tsp.TSPSource;
import eu.europa.esig.dss.token.KeyStoreSignatureTokenConnection;
import eu.europa.esig.dss.tsl.alerts.LOTLAlert;
import eu.europa.esig.dss.tsl.alerts.TLAlert;
import eu.europa.esig.dss.tsl.alerts.detections.LOTLLocationChangeDetection;
import eu.europa.esig.dss.tsl.alerts.detections.OJUrlChangeDetection;
import eu.europa.esig.dss.tsl.alerts.detections.TLExpirationDetection;
import eu.europa.esig.dss.tsl.alerts.detections.TLSignatureErrorDetection;
import eu.europa.esig.dss.tsl.alerts.handlers.log.LogLOTLLocationChangeAlertHandler;
import eu.europa.esig.dss.tsl.alerts.handlers.log.LogOJUrlChangeAlertHandler;
import eu.europa.esig.dss.tsl.alerts.handlers.log.LogTLExpirationAlertHandler;
import eu.europa.esig.dss.tsl.alerts.handlers.log.LogTLSignatureErrorAlertHandler;
import eu.europa.esig.dss.tsl.cache.CacheCleaner;
import eu.europa.esig.dss.tsl.function.OfficialJournalSchemeInformationURI;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import eu.europa.esig.dss.tsl.source.LOTLSource;
import eu.europa.esig.dss.tsl.sync.AcceptAllStrategy;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import eu.europa.esig.dss.ws.server.signing.common.RemoteSignatureTokenConnection;
import eu.europa.esig.dss.ws.server.signing.common.RemoteSignatureTokenConnectionImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore.PasswordProtection;
import java.util.Arrays;

/**
 * This class will configure DSS framework.
 */
@Configuration
@Slf4j
public class DssConfiguration {
	private static final int EXPIRATION_CACHE = 24 * 60 * 60 * 1000;
	@Value("${current.lotl.url}")
	private String lotlUrl;
	@Value("${lotl.country.code}")
	private String lotlCountryCode;
	@Value("${lotl.root.scheme.info.uri}")
	private String lotlRootSchemeInfoUri;
	@Value("${current.oj.url}")
	private String ojUrl;
	@Value("${oj.content.keystore.type}")
	private String ksType;
	@Value("${oj.content.keystore.filename}")
	private String ksFilename;
	@Value("${oj.content.keystore.password}")
	private String ksPassword;
	@Value("${dss.server.signing.keystore.type}")
	private String serverSigningKeystoreType;
	@Value("${dss.server.signing.keystore.filename}")
	private String serverSigningKeystoreFilename;
	@Value("${dss.server.signing.keystore.password}")
	private String serverSigningKeystorePassword;
	private final ConfigurationService configurationService;
	TrustedListsCertificateSource tslCertificateSource = new TrustedListsCertificateSource();

	public DssConfiguration(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@Bean
	public CommonsDataLoader dataLoader() {
		AtexoDataLoader dataLoader = new AtexoDataLoader();
		dataLoader.setProxyConfig(getProxyConfig());
		return dataLoader;
	}

	@Bean
	public TimestampDataLoader timestampDataLoader() {
		TimestampDataLoader timestampDataLoader = new TimestampDataLoader();
		timestampDataLoader.setProxyConfig(getProxyConfig());
		return timestampDataLoader;
	}

	@Bean
	public OCSPDataLoader ocspDataLoader() {
		OCSPDataLoader ocspDataLoader = new OCSPDataLoader();
		ocspDataLoader.setProxyConfig(getProxyConfig());
		return ocspDataLoader;
	}

	@Bean
	public FileCacheDataLoader fileCacheDataLoader() {
		FileCacheDataLoader fileCacheDataLoader = new FileCacheDataLoader();
		fileCacheDataLoader.setDataLoader(dataLoader());
		fileCacheDataLoader.setCacheExpirationTime(EXPIRATION_CACHE);
		fileCacheDataLoader.setFileCacheDirectory(new File(configurationService.getDssKeyStoreCachePath()));
		return fileCacheDataLoader;
	}

	@Bean
	public OnlineCRLSource onlineCRLSource() {
		OnlineCRLSource onlineCRLSource = new OnlineCRLSource();
		onlineCRLSource.setDataLoader(new NativeHTTPDataLoader());
		return onlineCRLSource;
	}


	@Bean
	public AtexoCrlSource offlineCRLSource() {
		return new AtexoCrlSource();
	}

	@Bean
	public OnlineOCSPSource ocspSource() {
		OnlineOCSPSource onlineOCSPSource = new OnlineOCSPSource();
		onlineOCSPSource.setDataLoader(ocspDataLoader());
		return onlineOCSPSource;
	}

	/**
	 * This bean will be used by the TSLValidation job in order to get and validate
	 * all EU trusted List. Before forwording this bean to TSLValidation job this
	 * bean will load all atexo trusted list.
	 *
	 * @return {@link CommonTrustedCertificateSource} object.
	 */
	@Bean
	public CommonTrustedCertificateSource trustedListSource() {
		return new CommonTrustedCertificateSource();
	}

	@Bean
	public TSPSource tspSource() {
		OnlineTSPSource onlineTSPSource = new OnlineTSPSource();
		onlineTSPSource.setDataLoader(timestampDataLoader());
		onlineTSPSource.setTspServer(configurationService.getTsaURL());
		return onlineTSPSource;
	}

	@Bean
	public CertificateVerifier certificateVerifier() {
		CommonCertificateVerifier certificateVerifier = new CommonCertificateVerifier();
		certificateVerifier.setTrustedCertSource(trustedListSource());
		certificateVerifier.setTrustedCertSource(tslCertificateSource);
		certificateVerifier.setCrlSource(onlineCRLSource());
		certificateVerifier.setOcspSource(ocspSource());
		certificateVerifier.setDataLoader(new NativeHTTPDataLoader());
		return certificateVerifier;
	}

	@Bean
	public ClassPathResource defaultPolicy() {
		return new ClassPathResource("atexo_policy.xml");
	}

	@Bean
	public KeyStoreSignatureTokenConnection remoteToken() throws IOException {
		return new KeyStoreSignatureTokenConnection(new ClassPathResource(serverSigningKeystoreFilename).getFile(), serverSigningKeystoreType,
				new PasswordProtection(serverSigningKeystorePassword.toCharArray()));
	}

	@Bean
	public RemoteSignatureTokenConnection serverToken() throws IOException {
		RemoteSignatureTokenConnectionImpl remoteSignatureTokenConnectionImpl = new RemoteSignatureTokenConnectionImpl();
		remoteSignatureTokenConnectionImpl.setToken(remoteToken());
		return remoteSignatureTokenConnectionImpl;
	}

	@Bean
	public KeyStoreCertificateSource ojContentKeyStore() throws IOException {
		return new KeyStoreCertificateSource(new ClassPathResource(ksFilename).getFile(), ksType, ksPassword);
	}

	@Bean
	public TLValidationJob tslValidationJob() {
		TLValidationJob job = new TLValidationJob();
		job.setOfflineDataLoader(offlineLoader());
		job.setOnlineDataLoader(onlineLoader());
		job.setTrustedListCertificateSource(tslCertificateSource);
		job.setSynchronizationStrategy(new AcceptAllStrategy());
		job.setCacheCleaner(cacheCleaner());
		LOTLSource europeanLOTL = europeanLOTL();
		job.setListOfTrustedListSources(europeanLOTL);
		job.setAlerts(Arrays.asList(tlSigningAlert(), tlExpirationDetection(), ojUrlAlert(europeanLOTL), lotlLocationAlert(europeanLOTL)));
		return job;
	}

	public String getLotlUrl() {
		return lotlUrl;
	}

	public String getLotlRootSchemeInfoUri() {
		return lotlRootSchemeInfoUri;
	}

	public String getOjUrl() {
		return ojUrl;
	}

	private CacheCleaner cacheCleaner() {
		CacheCleaner cacheCleaner = new CacheCleaner();
		cacheCleaner.setCleanMemory(true);
		cacheCleaner.setCleanFileSystem(true);
		cacheCleaner.setDSSFileLoader(offlineLoader());
		return cacheCleaner;
	}

	private LOTLSource europeanLOTL() {
		LOTLSource lotlSource = new LOTLSource();
		lotlSource.setUrl(lotlUrl);
		try {
			lotlSource.setCertificateSource(new KeyStoreCertificateSource(new ClassPathResource(serverSigningKeystoreFilename).getFile(), serverSigningKeystoreType, serverSigningKeystorePassword));
		} catch (IOException e) {
			log.error("integration europeanLOTL", e.fillInStackTrace());
		}
		lotlSource.setSigningCertificatesAnnouncementPredicate(new OfficialJournalSchemeInformationURI(ojUrl));
		lotlSource.setPivotSupport(true);
		return lotlSource;
	}

	private DSSFileLoader offlineLoader() {
		FileCacheDataLoader offlineFileLoader = new FileCacheDataLoader();
		offlineFileLoader.setCacheExpirationTime(EXPIRATION_CACHE);
		offlineFileLoader.setDataLoader(new IgnoreDataLoader());
		offlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
		return offlineFileLoader;
	}

	private DSSFileLoader onlineLoader() {
		FileCacheDataLoader offlineFileLoader = new FileCacheDataLoader();
		offlineFileLoader.setCacheExpirationTime(0);
		offlineFileLoader.setDataLoader(dataLoader());
		offlineFileLoader.setFileCacheDirectory(tlCacheDirectory());
		return offlineFileLoader;
	}

	private File tlCacheDirectory() {
		File rootFolder = new File(System.getProperty("java.io.tmpdir"));
		File tslCache = new File(rootFolder, "dss-tsl-loader");
		if (tslCache.mkdirs()) {
			log.info("TL Cache folder : {}", tslCache.getAbsolutePath());
		}
		return tslCache;
	}

	private TLAlert tlSigningAlert() {
		TLSignatureErrorDetection signingDetection = new TLSignatureErrorDetection();
		LogTLSignatureErrorAlertHandler handler = new LogTLSignatureErrorAlertHandler();
		return new TLAlert(signingDetection, handler);
	}

	private TLAlert tlExpirationDetection() {
		TLExpirationDetection expirationDetection = new TLExpirationDetection();
		LogTLExpirationAlertHandler handler = new LogTLExpirationAlertHandler();
		return new TLAlert(expirationDetection, handler);
	}

	private LOTLAlert ojUrlAlert(LOTLSource source) {
		OJUrlChangeDetection ojUrlDetection = new OJUrlChangeDetection(source);
		LogOJUrlChangeAlertHandler handler = new LogOJUrlChangeAlertHandler();
		return new LOTLAlert(ojUrlDetection, handler);
	}

	private LOTLAlert lotlLocationAlert(LOTLSource source) {
		LOTLLocationChangeDetection lotlLocationDetection = new LOTLLocationChangeDetection(source);
		LogLOTLLocationChangeAlertHandler handler = new LogLOTLLocationChangeAlertHandler();
		return new LOTLAlert(lotlLocationDetection, handler);
	}

	private ProxyConfig getProxyConfig() {
		ProxyConfig proxyConfig = new ProxyConfig();
		if (System.getProperty("http.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("http.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("http.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("http.proxyUser"));
			proxyProperties.setPassword(System.getProperty("http.proxyPassword"));
			proxyConfig.setHttpProperties(proxyProperties);
		}
		if (System.getProperty("https.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("https.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("https.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("https.proxyUser"));
			proxyProperties.setPassword(System.getProperty("https.proxyPassword"));
			proxyConfig.setHttpsProperties(proxyProperties);
		}
		if (proxyConfig.getHttpProperties() == null && proxyConfig.getHttpsProperties() == null) {
			return null;
		}
		return proxyConfig;
	}
}
