package com.atexo.serveurSignature.controller.rest.v1;

import com.atexo.serveurSignature.core.domain.horodatage.service.HorodatageService;
import com.atexo.serveurSignature.core.domain.horodatage.service.impl.HorodatageFactory;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.domain.signature.model.JetonHorodatageReponse;
import com.atexo.serveurSignature.core.enums.RestException;
import com.atexo.serveurSignature.core.exception.HorodatageException;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import eu.europa.esig.dss.enumerations.TimestampType;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.validation.timestamp.TimestampToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Api(value = "TimestampController", tags = {"Atexo date time controller"})
@RestController
@RequestMapping("v1")
public class TimestampController {
	private static final Logger logger = LoggerFactory.getLogger(TimestampController.class);
	private final JMXService jmxService;
	private final HorodatageFactory horodatageFactory;

	public TimestampController(JMXService jmxService, HorodatageFactory horodatageFactory) {
		this.jmxService = jmxService;
		this.horodatageFactory = horodatageFactory;
	}

	@RequestMapping(value = "validationJetonHorodatage2", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JetonHorodatageReponse> verifierJetonHorodatage2(@RequestParam(name = "jetonTSR", required = true) String jetonTSR,
																		   @RequestParam(name = "string", required = true) String string) throws MauvaisParametreException, RestException {
		long start = System.currentTimeMillis();
		logger.info("jetonTSR   : {}", jetonTSR);
		logger.info("hashSha256 : {}", string);
		HorodatageService horodatageService = horodatageFactory.getProviderHorodatage(HorodatageFactory.DEFAULT);
		JetonHorodatageReponse jetonReponse = new JetonHorodatageReponse();
		try {
			jetonReponse.setJetonValide(horodatageService.isTSRValide(string, Base64.decode(jetonTSR)));
		} catch (HorodatageException e) {
			e.fillInStackTrace();
		}
		try {
			jetonReponse.setHorodatage(horodatageService.getTSRDate(string, Base64.decode(jetonTSR)));
		} catch (HorodatageException e) {
			e.fillInStackTrace();
		}
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_HORODATAGE);
		UtilitaireMarshal.marshalJSON(jetonReponse);
		return new ResponseEntity<JetonHorodatageReponse>(jetonReponse, HttpStatus.OK);
	}


	@RequestMapping(value = "validationJetonHorodatage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<JetonHorodatageReponse> verifierJetonHorodatage(@RequestParam(name = "jetonTSR", required = true) String jetonTSQ, @RequestParam(name = "string", required = true) String string)
			throws MauvaisParametreException, RestException {
		long start = System.currentTimeMillis();
		logger.info("jetonTSQ   : {}", jetonTSQ);
		logger.info("hashSha256 : {}", string);
		JetonHorodatageReponse jetonReponse = new JetonHorodatageReponse();
		HorodatageService horodatageService = horodatageFactory.getProviderHorodatage(HorodatageFactory.DEFAULT);
		byte[] byteArray = null;
		try {
			byteArray = Base64.decode(jetonTSQ);
			CMSSignedData cms = new CMSSignedData(byteArray);
			cms.toASN1Structure().getEncoded();
			TimestampToken token = new TimestampToken(cms, TimestampType.CONTENT_TIMESTAMP, null, null, null);
			jetonReponse.setJetonValide(token.matchData(Base64.decode(string)));
			jetonReponse.setHorodatage(token.getGenerationTime());
			try {
				logger.error(string);
				byte[] tsr = horodatageService.getJetonDER(byteArray);
				jetonReponse.setJetonValide(horodatageService.isTSRValide(string, tsr));
				jetonReponse.setHorodatage(horodatageService.getTSRDate(string, byteArray));
			} catch (HorodatageException e1) {
				string = new String(Base64.decode(string));
				logger.error(string);
				try {
					byte[] tsr = horodatageService.getJetonDER(byteArray);
					jetonReponse.setJetonValide(horodatageService.isTSRValide(string, tsr));
					jetonReponse.setHorodatage(horodatageService.getTSRDate(string, byteArray));
				} catch (HorodatageException e2) {
					e2.fillInStackTrace();
				}
			}
			// jetonReponse.setJetonValide(horodatageService.isTSRValide(string,
			// cms.toASN1Structure().getEncoded()));
		} catch (IOException e) {
			throw new MauvaisParametreException("Probleme encoding jeton TSR", e.fillInStackTrace());
		} catch (TSPException | CMSException e) {
			try {
				jetonReponse.setJetonValide(horodatageService.isTSRValide(string, byteArray));
				jetonReponse.setHorodatage(horodatageService.getTSRDate(string, byteArray));
			} catch (HorodatageException e1) {
				throw new RestException("Probleme Horodatage", e1.fillInStackTrace());
			}
		}
		jmxService.addActionValide(System.currentTimeMillis() - start, JMXService.VERIFICATION_HORODATAGE);
		UtilitaireMarshal.marshalJSON(jetonReponse);
		jetonReponse.setJetonValide(true);
		return new ResponseEntity<JetonHorodatageReponse>(jetonReponse, HttpStatus.OK);
	}

	@ApiOperation(value = "Get Time at Server", notes = "Current date time in validation server", response = Date.class)
	@RequestMapping(value = "serverDate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Date> getServerDate() {
		return ResponseEntity.ok(new Date());
	}

	// @RequestMapping(value = "tokenRfc3161", method = RequestMethod.POST, pro)
	public @ResponseBody
	byte[] getTokenRfc3161(HttpServletRequest request, HttpServletResponse response) throws MauvaisParametreException {
		HorodatageService horodatageService = horodatageFactory.getProviderHorodatage(HorodatageFactory.DEFAULT);
		try {
			byte[] digest = IOUtils.toByteArray(request.getInputStream());
			logger.info("Timestamp digest debut    : " + Utils.toHex(digest));
			byte[] byteArray = horodatageService.getJetonDER(digest);
			logger.info("Timestamp digest fin    : " + Utils.toHex(byteArray));
			return byteArray;
		} catch (HorodatageException | IOException e) {
			throw new MauvaisParametreException("HORODATAGE", e.fillInStackTrace());
		}
	}
}


