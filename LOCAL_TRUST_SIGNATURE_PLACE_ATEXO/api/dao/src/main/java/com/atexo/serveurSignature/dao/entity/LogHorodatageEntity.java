package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "LOG_HORODATAGE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogHorodatageEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_LOG_HORODATAGE")
	private int idLogHorodatage;
	@Column(name = "DATE_INSERTION")
	private Timestamp dateInsertion;
	@Column(name = "PLATEFORME")
	private String plateforme;
	@Column(name = "HASH")
	private String hash;
	@Lob
	@Column(name = "JETON", columnDefinition = "blob")
	private byte[] jeton;
	@Column(name = "OK")
	private boolean ok;
	@Column(name = "TEMPS_EXECUTION")
	private int tempExecution;


}
