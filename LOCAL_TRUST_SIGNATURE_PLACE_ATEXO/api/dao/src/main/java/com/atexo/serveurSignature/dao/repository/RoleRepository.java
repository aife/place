package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.ERole;
import com.atexo.serveurSignature.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
