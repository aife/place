package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.LogSignatureEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogSignatureRepository extends CrudRepository<LogSignatureEntity, Long> {


}
