package com.atexo.serveurSignature.dao.entity;


public enum ERole {
    ROLE_USER,
    ROLE_DEV,
    ROLE_ADMIN
}
