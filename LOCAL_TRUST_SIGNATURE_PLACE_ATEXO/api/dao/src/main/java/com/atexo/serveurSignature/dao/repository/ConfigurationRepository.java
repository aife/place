package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.ConfigurationEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface ConfigurationRepository extends CrudRepository<ConfigurationEntity, String> {


	ConfigurationEntity getByParametre(String s);

	@Transactional
	@Modifying
	@Query("UPDATE ConfigurationEntity SET valeur = :valeur WHERE parametre = :parametre")
	void updateValeurByParametre(@Param("parametre") String nom, @Param("valeur") String valeur);
}
