package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "LOG_SIGNATURE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogSignatureEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_LOGS")
	private int idLogs;
	@Column(name = "DATE_INSERTION")
	private Timestamp dateInsertion;
	@Column(name = "PLATEFORME")
	private String plateforme;
	@Column(name = "CN")
	private String cn;
	@Column(name = "AC", columnDefinition = "longtext")
	private String ac;
	@Column(name = "ID")
	private String id;
	@Column(name = "TYPE_SIGNATURE")
	private String typeSignature;
	@Column(name = "CERTIFICAT", columnDefinition = "longtext")
	private String certificat;
	@Column(name = "TEMPS_EXECUTION", columnDefinition = "int(11)")
	private int tempExecution;


}
