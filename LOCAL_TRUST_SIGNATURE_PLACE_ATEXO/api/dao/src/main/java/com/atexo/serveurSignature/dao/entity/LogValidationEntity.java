package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "LOG_VALIDATION")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LogValidationEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_LOGS")
	private int idLogs;
	@Column(name = "DATE_INSERTION")
	private Timestamp dateInsertion;
	@Column(name = "PLATEFORME")
	private String plateforme;
	@Column(name = "ORGANISME")
	private String organisme;
	@Column(name = "CN")
	private String cn;
	@Column(name = "AC", columnDefinition = "longtext")
	private String ac;
	@Column(name = "ID")
	private String id;
	@Column(name = "TYPE_SIGNATURE")
	private String typeSignature;
	@Column(name = "TSL")
	private String tsl;
	@Column(name = "CERTIFICAT_VALIDE")
	private boolean certificatValide;
	@Column(name = "VALIDATION_ATEXO")
	private boolean validationAtexo;
	@Column(name = "VALIDATION_DSS")
	private boolean validationDss;
	@Column(name = "TEMPS_EXECUTION", columnDefinition = "int(11)")
	private int tempExecution;


}
