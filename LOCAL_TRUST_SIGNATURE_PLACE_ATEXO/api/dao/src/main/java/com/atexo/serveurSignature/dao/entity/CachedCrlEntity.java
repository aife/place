package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "CACHED_CRL")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CachedCrlEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "char", length = 40)
	private String id;
	@Lob
	@Column(name = "DATA")
	private byte[] data;
	@Column(name = "SIGNATURE_ALGORITHM", length = 20)
	private String signatureAlgorithm;
	@Column(name = "THIS_UPDATE")
	private Timestamp thisUpdate;
	@Column(name = "NEXT_UPDATE")
	private Timestamp nextUpdate;
	@Column(name = "EXPIRED_CERTS_ON_CRL")
	private Timestamp expiredCertsOnCrl;
	@Lob
	@Column(name = "ISSUER", columnDefinition = "blob")
	private byte[] issuer;
	@Column(name = "SIGNATURE_INTACT")
	private boolean signatureIntact;
	@Column(name = "ISSUER_PRINCIPAL_MATCH")
	private boolean issuerPrincipalMatch;
	@Column(name = "CRL_SIGN_KEY_USAGE")
	private boolean crlSignKeyUsage;
	@Column(name = "UNKNOWN_CRITICAL_EXTENSION")
	private boolean unknownCriticalExtension;
	@Column(name = "SIGNATURE_INVALID_REASON")
	private String signatureInvalidReason;


}
