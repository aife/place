package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.Session;
import com.atexo.serveurSignature.dao.entity.SessionId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public interface SessionRepository extends CrudRepository<Session, Long> {

	@Transactional
	@Modifying
	void deleteByDateInsertionLessThan(Date date);

	Session findBySessionId(SessionId sessionId);

	boolean existsBySessionIdAndActif(SessionId sessionId, boolean actif);

	@Transactional
	@Modifying
	void deleteBySessionId(SessionId sessionId);
}
