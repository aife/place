package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "SESSION")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Session implements Serializable {

    @EmbeddedId
    private SessionId sessionId;

    @Column(name = "DATE_INSERTION")
    private Timestamp dateInsertion;

    @Column(name = "ACTIF")
    private boolean actif;

    @Lob
    @Column(name = "VALEUR")
    private byte[] valeur;

}
