package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SessionId implements Serializable {

    private String uuid;

    private String cle;

}
