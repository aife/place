package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.CachedCrlEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CachedCrlRepository extends CrudRepository<CachedCrlEntity, String> {


}
