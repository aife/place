package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.LogHorodatageEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogHorodatageRepository extends CrudRepository<LogHorodatageEntity, Long> {


}
