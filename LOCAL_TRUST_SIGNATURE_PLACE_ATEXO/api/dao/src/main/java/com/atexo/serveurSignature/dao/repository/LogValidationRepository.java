package com.atexo.serveurSignature.dao.repository;

import com.atexo.serveurSignature.dao.entity.LogValidationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogValidationRepository extends CrudRepository<LogValidationEntity, Long> {


}
