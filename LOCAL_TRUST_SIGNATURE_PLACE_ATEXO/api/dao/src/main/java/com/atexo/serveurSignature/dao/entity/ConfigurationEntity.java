package com.atexo.serveurSignature.dao.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CONFIGURATION")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConfigurationEntity implements Serializable {

	@Id
	@Column(name = "PARAMETRE")
	private String parametre;

	@Column(name = "VALEUR")
	private String valeur;

	@Column(name = "DESCRIPTION")
	private String description;

}
