package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignResponse {
	private String lotlUrl;
	private boolean activeHorodatageSignature;
	private String shaSignature;
	private String shaHorodatage;
	private String ojUrl;
	private String lotlRootSchemeInfoUri;

}
