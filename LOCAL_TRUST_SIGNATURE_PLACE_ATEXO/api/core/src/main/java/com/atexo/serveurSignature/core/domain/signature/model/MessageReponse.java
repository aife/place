package com.atexo.serveurSignature.core.domain.signature.model;

import com.atexo.serveurSignature.core.enums.CodeReponseEnum;

import java.io.Serializable;

/**
 *
 */
public class MessageReponse implements Serializable {

    private int codeRetour;

    private String message;

    public MessageReponse() {
    }

    public MessageReponse(int codeRetour, String message) {
        this.codeRetour = codeRetour;
        this.message = message;
    }

    public MessageReponse(CodeReponseEnum codeReponse) {
        this.codeRetour = codeReponse.getCode();
        this.message = codeReponse.getMessage();
    }

    public int getCodeRetour() {
        return codeRetour;
    }

    public void setCodeRetour(int codeRetour) {
        this.codeRetour = codeRetour;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MessageReponse");
        sb.append("{codeRetour=").append(codeRetour);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
