package com.atexo.serveurSignature.core.domain.signature.model;


import com.atexo.serveurSignature.core.enums.CodeReponseEnum;

/**
 *
 */
@SuppressWarnings("serial")
public class ReponseVerificationSignature extends MessageReponse {

    private RetourVerificationReponse resultat;
    
    private RetourVerification retourVerification;

    private Boolean dateSignatureValide;

    public ReponseVerificationSignature() {
    }

    public ReponseVerificationSignature(CodeReponseEnum codeReponse, RetourVerification resultat, Boolean dateSignatureValide) {
        super(codeReponse);
        retourVerification = resultat;
        this.resultat = new RetourVerificationReponse(resultat);
        this.dateSignatureValide = dateSignatureValide;
    }

    public RetourVerificationReponse getResultat() {
        return resultat;
    }

    public void setResultat(RetourVerificationReponse resultat) {
        this.resultat = resultat;
    }

    public Boolean getDateSignatureValide() {
        return dateSignatureValide;
    }

    public void setDateSignatureValide(Boolean dateSignatureValide) {
        this.dateSignatureValide = dateSignatureValide;
    }

    public RetourVerification getRetourVerification() {
        return retourVerification;
    }

    public void setRetourVerification(RetourVerification retourVerification) {
        this.retourVerification = retourVerification;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ReponseVerificationSignature");
        sb.append("{ codeRetour=").append(getCodeRetour());
        sb.append(", message='").append(getMessage()).append('\'');
        sb.append(", resultat=").append(resultat);
        sb.append(", dateSignatureValide=").append(dateSignatureValide);
        sb.append('}');
        return sb.toString();
    }
}
