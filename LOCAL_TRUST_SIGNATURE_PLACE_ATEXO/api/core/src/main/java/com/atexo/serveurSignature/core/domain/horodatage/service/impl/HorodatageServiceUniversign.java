package com.atexo.serveurSignature.core.domain.horodatage.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.horodatage.OnLineTSPSource;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.exception.HorodatageException;
import com.atexo.serveurSignature.core.exception.MessageExceptionEnum;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.util.Date;

public class HorodatageServiceUniversign extends HorodatageServiceImpl {
	private static final Logger logger = LoggerFactory.getLogger(HorodatageServiceUniversign.class);
	private ConfigurationService configurationService;
	private JMXService jmxService;

	public HorodatageServiceUniversign(ConfigurationService configurationService, JMXService jmxService, RestTemplate restTemplate) {
		super(configurationService, jmxService, restTemplate);
		this.jmxService = jmxService;
		this.configurationService = configurationService;
	}

	@Override
	public byte[] getTokenRFC3161(byte[] xml) throws HorodatageException {
		OnLineTSPSource tspSource = new OnLineTSPSource(configurationService.getHorodatageUniversignURL());
		CommonsDataLoader commonsDataLoader = new CommonsDataLoader();
		URL url;
		try {
			url = new URL(configurationService.getHorodatageUniversignURL());
			commonsDataLoader.addAuthentication(url.getHost(), url.getDefaultPort(), url.getProtocol(), configurationService.getHorodatageUniversignLogin(), configurationService.getHorodatageUniversignPassword());
			tspSource.setDataLoader(commonsDataLoader);

			TimeStampResponse timeStampResponse = tspSource.getTimeStampResponse2(DigestAlgorithm.SHA256, xml);
			return timeStampResponse.getEncoded();
		} catch (IOException e) {
			logger.error("Erreur horodatage {}", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		}

	}

	@Override
	public Date getTSRDate(String xml, byte[] timeToken) throws HorodatageException {
		TimeStampToken token;
		try {
			token = new TimeStampToken(new CMSSignedData(timeToken));
		} catch (TSPException | IOException | CMSException e) {
			logger.error("Erreur horodatage {}", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		}
		return token.getTimeStampInfo().getGenTime();
	}
}
