package com.atexo.serveurSignature.core.exception;

@SuppressWarnings("serial")
public class SignatureException extends Exception {

    public SignatureException(MessageExceptionEnum message) {
        super(message.name());
    }
    
    public SignatureException(MessageExceptionEnum message, Throwable cause) {
        super(message.name(), cause);
    }

}
