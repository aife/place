package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

/**
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RetourVerificationReponse extends AbstractRepertoireReponse {
    private static final long serialVersionUID = 1L;

    private Integer periodeValidite;

    private Integer chaineCertification;

    private Integer revocation;


    public RetourVerificationReponse(RetourVerification retourVerification) {
        if (retourVerification != null) {
            this.periodeValidite = retourVerification.getPeriodeValidite() != null ? retourVerification.getPeriodeValidite().getCodeRetour() : null;
            this.chaineCertification = retourVerification.getChaineCertification() != null ? retourVerification.getChaineCertification().getCodeRetour() : null;
            this.revocation = retourVerification.getRevocation() != null ? retourVerification.getRevocation().getCodeRetour() : null;
            setRepertoiresChaineCertification(retourVerification.getRepertoiresChaineCertification());
            setRepertoiresRevocation(retourVerification.getRepertoiresRevocation());
        }
    }
}
