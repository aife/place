package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Parametre implements Serializable {

	private String nom;
	private String valeur;
	private String description;


}
