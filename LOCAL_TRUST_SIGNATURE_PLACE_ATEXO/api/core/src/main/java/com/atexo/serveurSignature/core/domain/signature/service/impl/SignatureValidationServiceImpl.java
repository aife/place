package com.atexo.serveurSignature.core.domain.signature.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.signature.model.Repertoire;
import com.atexo.serveurSignature.core.domain.signature.model.ReponseEnrichissementSignature;
import com.atexo.serveurSignature.core.domain.signature.service.ReferentielCertificatConfiguration;
import com.atexo.serveurSignature.core.domain.signature.service.SignatureValidationService;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Service
public class SignatureValidationServiceImpl implements SignatureValidationService {
	private static final Logger logger = LoggerFactory.getLogger(SignatureValidationServiceImpl.class);
	private final ConfigurationService configurationService;
	private final ReferentielCertificatConfiguration referentielCertificatConfiguration;
	private final RestTemplate restTemplate;

	public SignatureValidationServiceImpl(ConfigurationService configurationService, ReferentielCertificatConfiguration referentielCertificatConfiguration, RestTemplate restTemplate) {
		this.configurationService = configurationService;
		this.referentielCertificatConfiguration = referentielCertificatConfiguration;
		this.restTemplate = restTemplate;
	}

	@Override
	public ReponseEnrichissementSignature getReponseServeurValidation(String contenuFichierXML, String certificat, String typeEchange, String typeAlgorithmHash, String hashFichier,
																	  String intituleConsultation, String fonction) {
		@SuppressWarnings("rawtypes")
		HttpEntity requestEntity = null;
		ResponseEntity<ReponseEnrichissementSignature> reponse = null;
		MultiValueMap<String, Object> form = new LinkedMultiValueMap<String, Object>();
		if (contenuFichierXML != null) {
			form.add("contenuFichierXML", Base64.encodeBase64(contenuFichierXML.getBytes()));
		}
		if (certificat != null) {
			form.add("contenuCertificat", certificat);
		}
		form.add("typeEchange", typeEchange);
		form.add("typeAlgorithmHash", typeAlgorithmHash);
		form.add("hashFichier", hashFichier);
		form.add("plateforme", configurationService.getValidationOriginePlateforme());
		form.add("organisme", "ND");
		form.add("item", intituleConsultation);
		form.add("contexteMetier", fonction);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("content-type", "multipart/form-data");
		requestEntity = new HttpEntity<>(form, headers);
		String url = configurationService.getValidationURL() + "signatureXades";
		logger.info("Envoie bloc chiffre à l'URL suivante: {} ", url);
		logger.info("Parametre numeroOrdreBloc {} ", form.getFirst("numeroOrdreBloc"));
		logger.info("Parametre organisme {} ", form.getFirst("organisme"));
		logger.info("Parametre bloc impossible a afficher car fichier binaire ");
		reponse = restTemplate.postForEntity(url, requestEntity, ReponseEnrichissementSignature.class);
		logger.info("retour serveur de validation : {}", UtilitaireMarshal.marshalJSON(reponse.getBody()));
		ReponseEnrichissementSignature resultat = reponse.getBody();
		if (resultat != null && resultat.getResultat() != null) {
			Set<Repertoire> repertoires = resultat.getResultat().getRepertoiresChaineCertification();
			if (repertoires != null) {
				for (Repertoire repertoire : repertoires) {
					repertoire.setNom(referentielCertificatConfiguration.getReferentielNorme(repertoire.getNom()));
					if (referentielCertificatConfiguration.isStatut(repertoire.getNom())) {
						repertoire.setStatut("OK");
					} else {
						repertoire.setStatut("INCONNU");
					}
				}
			}
			repertoires = resultat.getResultat().getRepertoiresRevocation();
			if (repertoires != null) {
				for (Repertoire repertoire : repertoires) {
					repertoire.setNom(referentielCertificatConfiguration.getReferentielNorme(repertoire.getNom()));
					if (referentielCertificatConfiguration.isStatut(repertoire.getNom())) {
						repertoire.setStatut("OK");
					} else {
						repertoire.setStatut("INCONNU");
					}
				}
			}
		}
		return resultat;
	}

}
