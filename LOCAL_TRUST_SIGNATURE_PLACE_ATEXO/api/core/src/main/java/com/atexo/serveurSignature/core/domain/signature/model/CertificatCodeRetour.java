package com.atexo.serveurSignature.core.domain.signature.model;

/**
 *
 */
public interface CertificatCodeRetour {

    int getCodeRetour();
}
