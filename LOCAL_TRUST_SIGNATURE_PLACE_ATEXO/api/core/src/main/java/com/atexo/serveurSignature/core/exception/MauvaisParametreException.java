package com.atexo.serveurSignature.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Parametre invalide ou absent")
public class MauvaisParametreException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    
    public MauvaisParametreException() {
        super();
    }  
      
    public MauvaisParametreException(String message) {
        super(message);
    }  
    
    public MauvaisParametreException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
