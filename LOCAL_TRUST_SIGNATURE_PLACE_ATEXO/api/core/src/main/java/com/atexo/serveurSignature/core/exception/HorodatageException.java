package com.atexo.serveurSignature.core.exception;

@SuppressWarnings("serial")
public class HorodatageException extends Exception {

	public HorodatageException(MessageExceptionEnum message) {
		super(message.name());
	}

	public HorodatageException(MessageExceptionEnum message, Throwable cause) {
		super(message.name(), cause);
	}

}
