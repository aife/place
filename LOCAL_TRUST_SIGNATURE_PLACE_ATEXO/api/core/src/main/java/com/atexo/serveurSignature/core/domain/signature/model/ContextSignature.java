package com.atexo.serveurSignature.core.domain.signature.model;

import com.atexo.serveurSignature.core.enums.SignatureTypeEnum;
import lombok.*;

import java.io.Serializable;

/**
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ContextSignature implements Serializable {

    private static final long serialVersionUID = 1L;

    private String idFileSystem;
    
    private String mpeFilePath;

    private String hash;
    
    private String[] hashes;
    
    private String pdfBase64;
    
    private String signatureBase64;
    
    private SignatureTypeEnum signatureType;

}
