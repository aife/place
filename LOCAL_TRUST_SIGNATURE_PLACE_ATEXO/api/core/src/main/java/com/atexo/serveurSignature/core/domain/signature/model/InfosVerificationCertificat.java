package com.atexo.serveurSignature.core.domain.signature.model;

import com.atexo.serveurSignature.core.utilitaire.ObjectNaming;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class InfosVerificationCertificat implements Serializable {
    private static final long serialVersionUID = 5443861700601895753L;

    private int etat = -1;

    @JsonIgnore
    private boolean dssValidationEffectuee = false;

    @JsonIgnore
    private boolean atexoValidationEffectuee = false;

    private String signatairePartiel;

    private String signataireComplet;

    private String emetteur;

    private String dateValiditeDu;

    private String dateValiditeAu;

    private Boolean periodiciteValide;

    private int chaineDeCertificationValide;

    private int absenceRevocationCRL;

    private int dateSignatureValide;

    private Boolean signatureValide;

    private ObjectNaming signataireDetails;
    private ObjectNaming signataireDetail;

    private ObjectNaming emetteurDetails;

    private Set<Repertoire> repertoiresChaineCertification;

    private Set<Repertoire> repertoiresRevocation;

    private String signatureXadesServeurEnBase64;

    private String typeSignature = "INCONNUE";

    @JsonIgnore
    private String reportsDiagnostic;

    @JsonIgnore
    private String reportsDetailed;

    @JsonIgnore
    private String serialNumber;


}
