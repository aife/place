package com.atexo.serveurSignature.core.utilitaire;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public final class TrustAllManager implements X509TrustManager {

    public static final TrustAllManager THE_INSTANCE = new TrustAllManager();

    private TrustAllManager() {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
        // DO nothing
    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
        // DO nothing
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }
}

