package com.atexo.serveurSignature.core.config;

import com.atexo.serveurSignature.core.utilitaire.Signatures;

import java.security.cert.X509Certificate;

public interface LogDBService {
	void insertHorodatage(String plateforme, String hash, byte[] jeton, boolean ok, long tempsRequete);

	void insertValidation(String plateforme, String context, Signatures sigantures, long tempsRequete);

	void insertSignature(String plateforme, X509Certificate certificate, String typeSignature, int tempsRequete);

}
