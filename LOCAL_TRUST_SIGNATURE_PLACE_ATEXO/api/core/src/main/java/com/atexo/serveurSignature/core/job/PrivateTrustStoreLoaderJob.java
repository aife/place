package com.atexo.serveurSignature.core.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PrivateTrustStoreLoaderJob {

	private final PrivateTrustStoreJob job;

	public PrivateTrustStoreLoaderJob(PrivateTrustStoreJob job) {
		this.job = job;
	}

	// 1000 * 60 * 60 * 24 = 86400000
	@Scheduled(fixedDelayString = "86400000", initialDelay = 0)
	public void refresh() {
		job.refresh();
	}


}


