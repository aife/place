package com.atexo.serveurSignature.core.enums;

/**
 *
 */
public enum TypeEchangeEnum {

    EnrichissementSignature, VerificationSignatureClient, VerificationSignatureServeur, RetourDateServeur;
}
