package com.atexo.serveurSignature.core.domain.signature.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.enums.CertificatVerificationEtatEnum;
import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.domain.signature.service.SignatureVerificationService;
import com.atexo.serveurSignature.core.enums.SignatureTypeEnum;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.core.exception.ValidationException;
import com.atexo.serveurSignature.core.utilitaire.AtexoDssUtils;
import com.atexo.serveurSignature.core.utilitaire.Signatures;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.DigestDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.SignedDocumentValidator;
import eu.europa.esig.dss.validation.executor.ValidationLevel;
import eu.europa.esig.dss.validation.reports.Reports;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.atexo.serveurSignature.core.utilitaire.AtexoDssUtils.constructValidationResponseV2;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.bouncycastle.util.encoders.Base64.decode;

@Service
public class SignatureVerificationServiceImpl implements SignatureVerificationService {
	private static final Logger logger = LoggerFactory.getLogger(SignatureVerificationServiceImpl.class);
	private static final Logger loggerFile = LoggerFactory.getLogger("file_logger");

	private final CertificateVerifier certificateVerifier;

	private final ConfigurationService configurationService;

	public SignatureVerificationServiceImpl(CertificateVerifier certificateVerifier, Resource defaultPolicy, ConfigurationService configurationService) {
		this.certificateVerifier = certificateVerifier;
		this.configurationService = configurationService;
	}

	/**
	 * This method will validate a document received from MPE.
	 *
	 * @param validationContext it contains all parameters for the validation.
	 * @return InfosVerificationCertificat to be forwarded to MPE
	 * @throws ValidationException if something goes wrong.
	 */
	@Override
	public Signatures validationDocumentDSS(final ContextSignature validationContext, File fichierPDF) throws ValidationException {
		Signatures retValue = new Signatures();

		try {
			DSSDocument signedDocument;
			DigestDocument digestDocument = null;
			if (null != validationContext) {
				final byte[] signedData = isNotBlank(validationContext.getSignatureBase64()) ? decode(validationContext.getSignatureBase64()) : decode(validationContext.getPdfBase64());
				signedDocument = AtexoDssUtils.toDSSDocument(signedData);
				digestDocument = new DigestDocument();
				if (null != validationContext.getHashes() && validationContext.getHashes().length > 0) {
					if (validationContext.getHashes().length == 1) {
						digestDocument.addDigest(DigestAlgorithm.SHA1, Utils.toBase64(AtexoDssUtils.hexToBytes(validationContext.getHashes()[0])));
					} else {
						digestDocument.addDigest(DigestAlgorithm.SHA1, Utils.toBase64(AtexoDssUtils.hexToBytes(validationContext.getHashes()[0])));
						digestDocument.addDigest(DigestAlgorithm.SHA256, Utils.toBase64(AtexoDssUtils.hexToBytes(validationContext.getHashes()[1])));
					}
				}
			} else {
				signedDocument = new FileDocument(fichierPDF);
			}

			Reports reports = validateDssDocument(signedDocument, digestDocument);
			if (reports.getSimpleReport().getJaxbModel().getSignatureOrTimestamp().size() != 0) {
				retValue = constructValidationResponseV2(reports);
			} else {
				retValue.setEtat(-1);
			}
			retValue.setDssValidationEffectuee(true);
			loggerFile.trace(UtilitaireMarshal.marshalJSON(validationContext));
			loggerFile.trace(reports.getXmlDetailedReport());
			loggerFile.trace(reports.getXmlDiagnosticData());
			loggerFile.trace(UtilitaireMarshal.marshalJSON(retValue));
			retValue.setReportsDetailed(reports.getXmlDetailedReport());
			retValue.setReportsDiagnostic(reports.getXmlDiagnosticData());
		} catch (DSSException | IOException e) {
			logger.error(e.getMessage(), e);
		}
		return retValue;
	}

	@Override
	public Signatures extractSignatures(ContextSignature context) {
		logger.info(UtilitaireMarshal.marshalJSON(context));
		Signatures info = null;
		if (context == null) {
			throw new MauvaisParametreException("objet parametre signature est null");
		}
		if (context.getHash() != null) {
			String[] hashes = new String[1];
			hashes[0] = context.getHash();
			context.setHashes(hashes);
		}
		if (context.getPdfBase64() != null && !context.getPdfBase64().isEmpty()) {
			context.setSignatureType(SignatureTypeEnum.PADES);
		}
		if (context.getPdfBase64() == null && context.getHashes() == null && (context.getIdFileSystem() == null || context.getMpeFilePath() == null)) {
			throw new MauvaisParametreException("hash/IdFileSystem/mpeFilePath sont nuls");
		}
		if (context.getPdfBase64() == null && (context.getSignatureBase64() == null || context.getSignatureBase64().isEmpty())) {
			logger.error("signatureBase64 est null");
			throw new MauvaisParametreException("signatureBase64 est null");
		}
		File fichier = null;
		if (context.getIdFileSystem() != null) {
			fichier = new File(context.getMpeFilePath() + context.getIdFileSystem());
			if (!fichier.exists()) {
				throw new MauvaisParametreException("fichier existe pas: " + fichier.getAbsolutePath());
			}
			try {
				String hashSha1Fichier = DigestUtils.shaHex(new FileInputStream(fichier));
				context.setPdfBase64(hashSha1Fichier);
			} catch (IOException e) {
				logger.error("Impossible de generer hash {}", fichier.getPath());
			}
		}
		if (configurationService.isDssValidationEnabled()) {
			try {
				info = this.validationDocumentDSS(context, null);
			} catch (ValidationException e) {
				info = new Signatures();
				info.setEtat(CertificatVerificationEtatEnum.CertificatInvalide.getCodeRetour());
			}
		}
		logger.info("resultat verification signature : {} ", UtilitaireMarshal.marshalJSON(info));
		return info;
	}

	private Reports validateDssDocument(DSSDocument signedDocument, DigestDocument digestDocument) throws IOException, DSSException, ValidationException {
		SignedDocumentValidator validator;
		try {
			validator = SignedDocumentValidator.fromDocument(signedDocument);
		} catch (DSSException e) {
			logger.info(e.getMessage(), e.fillInStackTrace());
			throw new ValidationException();
		}
		if (digestDocument != null) {
			List<DSSDocument> detachedContents = new ArrayList<DSSDocument>();
			detachedContents.add(digestDocument);
			validator.setDetachedContents(detachedContents);
		}
		validator.setValidationLevel(ValidationLevel.TIMESTAMPS);
		validator.setCertificateVerifier(certificateVerifier);

		return validator.validateDocument();
	}

}
