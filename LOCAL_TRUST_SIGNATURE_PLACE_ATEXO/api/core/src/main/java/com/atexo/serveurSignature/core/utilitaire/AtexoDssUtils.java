package com.atexo.serveurSignature.core.utilitaire;

import com.atexo.serveurSignature.core.domain.signature.model.Repertoire;
import com.atexo.serveurSignature.core.enums.CertificatValiditeEtatEnum;
import com.atexo.serveurSignature.core.enums.CertificatVerificationEtatEnum;
import eu.europa.esig.dss.diagnostic.*;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.model.MimeType;
import eu.europa.esig.dss.utils.Utils;
import eu.europa.esig.dss.validation.reports.Reports;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Utility class that contains various method to handle conversion to DSS
 * document format.
 */
public class AtexoDssUtils {
	private static final Logger logger = LoggerFactory.getLogger(AtexoDssUtils.class);
	private static final byte[] xmlPreamble = new byte[]{'<', '?', 'x', 'm', 'l'};
	private static final byte[] xmlPreamble2 = new byte[]{'<'};
	private static final byte[] xmlUtf8 = new byte[]{-17, -69, -65, '<', '?'};

	/**
	 * This method will transform a {@link MultipartFile} object to a
	 * {@link DSSDocument}
	 *
	 * @param signedFile the file to be transformed
	 * @return {@link DSSDocument} object
	 */
	public static DSSDocument toDSSDocument(final byte[] signedFile) {
		DSSDocument retValue = null;
		boolean isXML = isXmlPreamble(signedFile);
		if (isXML && null != signedFile) {
			String redressementSignature = new String(signedFile);
			int position1 = redressementSignature.indexOf("<ds:X509IssuerName>");
			if (position1 != -1) {
				int position2 = redressementSignature.indexOf("</ds:X509IssuerName>");
				String issuer = redressementSignature.substring(position1, position2);
				issuer = issuer.replaceAll(".toiles", "étoiles");
				redressementSignature = redressementSignature.substring(0, position1) + issuer + redressementSignature.substring(position2);
			}
			retValue = new InMemoryDocument(redressementSignature.getBytes());
			retValue.setMimeType(MimeType.XML);
			retValue.setName("dumm.xml");
		} else {
			retValue = new InMemoryDocument(signedFile);
			retValue.setName("dumm");
		}
		return retValue;
	}

	/**
	 * This method will convert a String hexa to it's representation as an array of
	 * Bytes.
	 *
	 * @param hex
	 * @return {@link String} representation of the Hexa.
	 */
	public static byte[] hexToBytes(final String hex) {
		int len = hex.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
		}
		return data;
	}

	/**
	 * This method will load all certification files in the defined path.
	 *
	 * @return List of All files in the trusted store path.
	 */
	public static List<File> loadAllFile(final String pathToBeLoaded) {
		return (List<File>) FileUtils.listFiles(new File(pathToBeLoaded), new RegexFileFilter("^(.*?)"), DirectoryFileFilter.DIRECTORY);
	}

	/**
	 * this method will extract all needed data from DSS Validation Report.
	 *
	 * @param reports
	 * @return
	 */
	public static Signatures constructValidationResponseV2(final Reports reports) {
		Signatures signaturesGlobales = new Signatures();
		DiagnosticData diagnostic = reports.getDiagnosticData();
		signaturesGlobales.setTypeSignature(diagnostic.getFirstSignatureFormat().name());
		List<Signature> list = new ArrayList<Signature>();
		for (String signatureId : diagnostic.getSignatureIdList()) {
			Signature signature = extractSignature(diagnostic, signatureId);
			if (signaturesGlobales.getEtat() == CertificatVerificationEtatEnum.VerificationValide.getCodeRetour() && signature.getEtat() != CertificatVerificationEtatEnum.VerificationValide.getCodeRetour()) {
				signaturesGlobales.setEtat(signature.getEtat());
			}
			list.add(signature);
		}
		signaturesGlobales.setSignatures(list);
		return signaturesGlobales;
	}

	/**
	 * This method will extract all common validation result.
	 *
	 * @param diagnostic
	 * @param signatureId
	 * @return
	 */
	private static Signature extractSignature(DiagnosticData diagnostic, String signatureId) {
		Signature info = new Signature();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		info.setEtat(CertificatVerificationEtatEnum.VerificationValide.getCodeRetour());
		SignatureWrapper signature = diagnostic.getSignatureById(signatureId);
		CertificateWrapper certificate = signature.getSigningCertificate();
		info.setSignatairePartiel(certificate.getCommonName());
		info.setChaineDeCertificationValide(
				!certificate.getCertificateChain().isEmpty() ? CertificatVerificationEtatEnum.VerificationValide.getCodeRetour() : CertificatVerificationEtatEnum.ChaineCertificatNonTrouvee.getCodeRetour());
		info.setSignataireComplet(certificate.getCertificateDN());
		info.setEmetteur(certificate.getCertificateIssuerDN());
		info.setSerialNumber(certificate.getSerialNumber());
		info.setSignataireDetails(CertificatUtil.getObjectNaming(diagnostic.getCertificateDN(certificate.getId()), null));
		info.setSignataireDetail(CertificatUtil.getObjectNaming(diagnostic.getCertificateDN(certificate.getId()), null));
		info.setEmetteurDetails(CertificatUtil.getObjectNaming(diagnostic.getCertificateIssuerDN(certificate.getId()), null));
		info.setSignatureValide(signature.isSignatureValid());
		info.setFormatSignature(signature.getSignatureFormat().name());
		for (TrustedServiceWrapper service : certificate.getTrustedServices()) {
			if (service.getType() != null && service.getType().equalsIgnoreCase("http://uri.etsi.org/TrstSvc/Svctype/CA/QC")) {
				info.setQualifieEIDAS(1);
				break;
			}
		}
		info.setDateIndicative(format.format(signature.getClaimedSigningTime()));
		List<TimestampWrapper> timestamps = signature.getTimestampList();
		for (TimestampWrapper timestampWrapper : timestamps) {
			if (timestampWrapper.isSignatureValid() && timestampWrapper.isSignatureIntact()) {
				info.setJetonHorodatage(1);
				info.setDateHorodatage(format.format(timestampWrapper.getProductionTime()));
				break;
			}
		}
		if (!signature.isSignatureValid()) {
			info.setEtat(CertificatVerificationEtatEnum.CertificatInvalide.getCodeRetour());
		}

		if (certificate.isRevocationDataAvailable() && AtexoDssUtils.getLatestRevocationData(certificate).isRevoked()) {
			info.setAbsenceRevocationCRL(2);
		} else {
			info.setAbsenceRevocationCRL(0);
		}
		if (signature.getTimestampIdsList() != null && !signature.getTimestampIdsList().isEmpty()) {
			for (TimestampWrapper timestampWrapper : timestamps) {
				info.setPeriodiciteValide(Util.isDateCompriseEntre(timestampWrapper.getProductionTime(), certificate.getNotBefore(), certificate.getNotAfter()));
				break;
			}
		} else {
			info.setPeriodiciteValide(Util.isDateCompriseEntre(signature.getClaimedSigningTime(), certificate.getNotBefore(), certificate.getNotAfter()));
		}
		info.setDateValiditeDu(format.format(certificate.getNotBefore()));
		info.setDateValiditeAu(format.format(certificate.getNotAfter()));

		info.setDateSignatureValide(info.getPeriodiciteValide() ? CertificatValiditeEtatEnum.Valide.getCodeRetour() : CertificatValiditeEtatEnum.Expire.getCodeRetour());
		Set<Repertoire> repertoires = new HashSet<>();

		if (certificate.isTrustedChain()) {
			if (certificate.isTrustedListReached()) {
				Repertoire repertoire = new Repertoire("TSL-" + certificate.getCountryName());
				repertoire.setStatut("OK");
				repertoires.add(repertoire);
			} else {
				Repertoire repertoire = new Repertoire("TSL-LOCALE");
				repertoire.setStatut("OK");
				repertoires.add(repertoire);
			}
		}
		if (repertoires.isEmpty()) {
			Repertoire repertoire = new Repertoire("TSL-INCONNUE");
			repertoire.setStatut("KO");
			info.setEtat(CertificatVerificationEtatEnum.ChaineCertificatNonTrouvee.getCodeRetour());
			repertoires.add(repertoire);
		}

		info.setRepertoiresChaineCertification(repertoires);
		return info;
	}


	private static boolean isXmlPreamble(byte[] preamble) {
		byte[] startOfPramble = Utils.subarray(preamble, 0, xmlPreamble.length);
		return Arrays.equals(startOfPramble, xmlPreamble) || Arrays.equals(startOfPramble, xmlUtf8) || Arrays.equals(startOfPramble, xmlPreamble2);
	}

	public static CertificateRevocationWrapper getLatestRevocationData(CertificateWrapper certificate) {
		CertificateRevocationWrapper latest = null;
		Iterator<CertificateRevocationWrapper> var2 = certificate.getCertificateRevocationData().iterator();

		while (true) {
			CertificateRevocationWrapper revoc;
			do {
				if (!var2.hasNext()) {
					return latest;
				}

				revoc = (CertificateRevocationWrapper) var2.next();
			} while (latest != null && (latest.getProductionDate() == null || revoc == null || revoc.getProductionDate() == null || !revoc.getProductionDate().after(latest.getProductionDate())));

			latest = revoc;
		}
	}
}
