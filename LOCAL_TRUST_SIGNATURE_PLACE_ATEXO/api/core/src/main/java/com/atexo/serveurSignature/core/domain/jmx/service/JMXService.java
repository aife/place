package com.atexo.serveurSignature.core.domain.jmx.service;

public interface JMXService {
    public static final String VERIFICATION_HORODATAGE = "VERIFICATION_HORODATAGE";
    public static final String VERIFICATION_SIGNATURE = "VERIFICATION_HORODATAGE";
    public static final String MAUVAIS_PARAMETRE = "MAUVAIS_PARAMETRE";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    
    void addActionValide(long temps, String action);

    void addActionErreur(String action);

}
