package com.atexo.serveurSignature.core.config;

import com.atexo.serveurSignature.core.domain.signature.model.Parametre;

import java.net.URI;
import java.util.List;

public interface ConfigurationService {
	String getApplicationDirectoryTmp();

	String getValidationURL();

	String getApplicationPathOpenSSL();

	String getApplicationContext();

	String getValidationOriginePlateforme();

	String getApplicationOpenSSLCaPath();

	String getApplicationSHASignatureType();

	Boolean isDssValidationEnabled();

	Boolean isAtexoValidationEnabled();

	List<Parametre> getConfiguration();

	void setConfiguration(Parametre parameter);

	String getDssKeyStorePath();

	String getDssKeyStoreCachePath();

	boolean isCronTlLoaderEnable();

	String getTsaURL();

	String getMockUrl();

	boolean getApplicationJwsHorodatageSignature();

	String getHorodatageUniversignURL();

	String getHorodatageUniversignLogin();

	String getHorodatageUniversignPassword();

	boolean verifieAccesHorodatageUniversign(String url);

	String getHorodatagePisteURL();

	boolean verifieAccesHorodatagePiste(String url);

	String getHorodatagePisteClientId();

	String getHorodatagePisteClientSecret();

	URI getHorodatagePisteAuthentificationURL();

	String getApplicationSHAHorodatageType();

	String getURLJNLP();
}
