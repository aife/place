package com.atexo.serveurSignature.core.domain.horodatage.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.horodatage.service.HorodatageService;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
@Slf4j
public class HorodatageFactory {

	public static final String DEFAULT = "DEFAULT";

	private final ConfigurationService configurationService;
	private final JMXService jmxService;
	private final RestTemplate restTemplate;

	public HorodatageFactory(ConfigurationService configurationService, JMXService jmxService, RestTemplate restTemplate) {
		this.configurationService = configurationService;
		this.jmxService = jmxService;
		this.restTemplate = restTemplate;
	}

	public HorodatageService getProviderHorodatage(String valeur) {
		if (valeur != null && configurationService.verifieAccesHorodatageUniversign(valeur)) {
			return new HorodatageServiceUniversign(configurationService, jmxService, restTemplate);
		} else if (valeur != null && configurationService.verifieAccesHorodatagePiste(valeur)) {
			return new HorodatageServicePiste(configurationService, jmxService, restTemplate);
		} else {
			return new HorodatageServiceImpl(configurationService, jmxService, restTemplate);
		}
	}


}
