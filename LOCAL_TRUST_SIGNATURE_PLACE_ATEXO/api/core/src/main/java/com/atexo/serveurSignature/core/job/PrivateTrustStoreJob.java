package com.atexo.serveurSignature.core.job;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.utilitaire.AtexoDssUtils;
import com.atexo.serveurSignature.core.utilitaire.AtexoCrlSource;
import eu.europa.esig.dss.model.x509.CertificateToken;
import eu.europa.esig.dss.spi.DSSUtils;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@Service
@Slf4j
public class PrivateTrustStoreJob {

	private final CommonTrustedCertificateSource trustedCertificateSource;

	private final AtexoCrlSource atexoCrlSource;


	private List<File> allFiles;

	private final ConfigurationService configurationService;

	public PrivateTrustStoreJob(CommonTrustedCertificateSource trustedCertificateSource, AtexoCrlSource atexoCrlSource, ConfigurationService configurationService) {
		this.trustedCertificateSource = trustedCertificateSource;
		this.atexoCrlSource = atexoCrlSource;
		this.configurationService = configurationService;
	}

	public void refresh() {
		log.debug("Private Trust Store Job is starting ...");
		allFiles = AtexoDssUtils.loadAllFile(configurationService.getDssKeyStorePath());

		if (!CollectionUtils.isEmpty(allFiles)) {
			for (final File file : allFiles) {
				try {
					final String fileExtension = FilenameUtils.getExtension(file.getName());
					if ("crt".equals(fileExtension) || "0".equals(fileExtension)) {
						final CertificateToken token = DSSUtils.loadCertificate(file);
						trustedCertificateSource.addCertificate(token);
					} else if ("crl".equals(fileExtension)) {
						atexoCrlSource.addCRLToken(new FileInputStream(file));
					} else {
						log.debug(file.getName() + " Unsupported file type ");
					}
				} catch (Exception e) {
					log.error(file.getPath(), e.fillInStackTrace());
				}
			}
			log.debug("Updating DSS Trust Store using private Store Done ...");
		}

	}

}
