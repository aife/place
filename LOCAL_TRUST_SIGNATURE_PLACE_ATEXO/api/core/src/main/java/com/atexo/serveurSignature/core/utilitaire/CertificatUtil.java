package com.atexo.serveurSignature.core.utilitaire;

import com.atexo.serveurSignature.core.domain.signature.model.InfosVerificationCertificat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;
import java.security.Principal;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Classe d'utilitaire d'extraction / manipulation des informations d'un
 * certificat.
 */
@SuppressWarnings("restriction")
public abstract class CertificatUtil {
	private static final Logger logger = LoggerFactory.getLogger(CertificatUtil.class);


	public static ObjectNaming getObjectNaming(String proprietaireCertificat, Collection<List<?>> alternativeName) {
		ObjectNaming object = new ObjectNaming();
		if (alternativeName != null) {
			Iterator<List<?>> it = alternativeName.iterator();
			if (it != null) {
				List<?> result = it.next();
				if (result != null) {
					if (result.get(1) instanceof String) {
						object.setAlternativeName((String) result.get(1));
					}
				}
			}
		}
		try {
			LdapName names = new LdapName(proprietaireCertificat);
			for (Rdn tmp : names.getRdns()) {
				String cle = tmp.getType();
				String valeur = tmp.getValue().toString();
				if (cle.equalsIgnoreCase("CN")) {
					if (object.getCn() != null) {
						object.setCn(object.getCn() + ", " + valeur);
					} else {
						object.setCn(valeur);
					}
				} else if (cle.equalsIgnoreCase("T")) {
					if (object.getT() != null) {
						object.setT(object.getT() + ", " + valeur);
					} else {
						object.setT(valeur);
					}
				} else if (cle.equalsIgnoreCase("OU")) {
					if (object.getOu() != null) {
						object.setOu(object.getOu() + ", " + valeur);
					} else {
						object.setOu(valeur);
					}
				} else if (cle.equalsIgnoreCase("E")) {
					if (object.getE() != null) {
						object.setE(object.getE() + ", " + valeur);
					} else {
						object.setE(valeur);
					}
				} else if (cle.equalsIgnoreCase("O")) {
					if (object.getO() != null) {
						object.setO(object.getO() + ", " + valeur);
					} else {
						object.setO(valeur);
					}
				} else if (cle.equalsIgnoreCase("C")) {
					if (object.getC() != null) {
						object.setC(cle.equals("C") + ", " + valeur);
					} else {
						object.setC(valeur);
					}
				}
			}
		} catch (InvalidNameException e) {
			logger.error("Probleme a la lecture du certificat", e.fillInStackTrace());
		}

		return object;
	}


	public static InfosVerificationCertificat extraireInformations(X509Certificate certificat, InfosVerificationCertificat infosComplementairesCertificat) throws CertificateParsingException {

		// propriétaire du certificat
		X500Principal proprietaireCertificat = certificat.getSubjectX500Principal();
		infosComplementairesCertificat.setSignatairePartiel(CertificatUtil.getCN(proprietaireCertificat));
		infosComplementairesCertificat.setSignataireComplet(proprietaireCertificat.toString());
		// issuer du certificat
		X500Principal issuerCertificat = certificat.getIssuerX500Principal();
		infosComplementairesCertificat.setEmetteur(issuerCertificat.toString());
		infosComplementairesCertificat.setSerialNumber(certificat.getSerialNumber().toString());

		// periodicité du certificat
		String dateValiditeDu = Util.creerISO8601DateTime(certificat.getNotBefore());
		infosComplementairesCertificat.setDateValiditeDu(dateValiditeDu);
		String dateValiditeAu = Util.creerISO8601DateTime(certificat.getNotAfter());
		infosComplementairesCertificat.setDateValiditeAu(dateValiditeAu);

		boolean datePeriodiciteValide = Util.isDateCompriseEntre(new Date(), certificat.getNotBefore(), certificat.getNotAfter());
		infosComplementairesCertificat.setPeriodiciteValide(datePeriodiciteValide);
		infosComplementairesCertificat.setSignataireDetails(getObjectNaming(proprietaireCertificat.toString(), certificat.getSubjectAlternativeNames()));
		infosComplementairesCertificat.setSignataireDetail(getObjectNaming(proprietaireCertificat.toString(), certificat.getSubjectAlternativeNames()));
		infosComplementairesCertificat.setEmetteurDetails(getObjectNaming(issuerCertificat.toString(), certificat.getSubjectAlternativeNames()));
		return infosComplementairesCertificat;
	}

	/**
	 * Récupére l'attribut CN d'un certificat
	 *
	 * @param principal
	 *            le certificat
	 * @return la valeur du CN
	 */
	public static String getCN(Principal principal) {

		if (principal != null) {
			String[] principalDecompose = principal.toString().split(",");

			for (String valeur : principalDecompose) {
				String valeurTrim = valeur.trim();
				if (valeurTrim.startsWith("CN=")) {
					return valeurTrim.substring(3);
				}
			}
		}

		return null;
	}
}
