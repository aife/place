package com.atexo.serveurSignature.core.domain.localServer.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignatureEnLigneRequest {
	private List<InputOutputUrl> urlList;
	private String serveurCryptoURLPublique;
	private String callbackUrl;
	private String appId;
	private String sessionId;
}
