package com.atexo.serveurSignature.core.exception;


public enum MessageExceptionEnum {
    ECHEC_AUTHENTIFICATION(1),
    REPONSE_ANNONCE_DONNEES_MANQUANTES(2),
    FICHIER_A_DECHIFFRER_MANQUANT(3),
    DECHIFFREMENT_ENVELOPPE_IMPOSSIBLE(4),
    SIGNATURE_ECHEC_VERIFICATION(5),
    SIGNATURE_ECHEC_ENRICHISSEMENT(6),
    BASE64_ENCODING_ERREUR(7),
    CERTIFICAT_INCORRECT(8),
    FICHIER_A_CHIFFRER_MANQUANT(9),
    CHIFFREMENT_GENERATION_HASH(10),
    CHIFFREMENT_LECTURE_BLOC(11),
    ECHEC_BLOC_CHIFFRER(12),
    MAUVAIS_PARAMETRE_REQUETE(13),
    HORODATAGE_ECHEC(14),
    SIGNATURE_ECHEC_ENREGISTREMENT(15);

    private int code;
    // Constructeur
    MessageExceptionEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
