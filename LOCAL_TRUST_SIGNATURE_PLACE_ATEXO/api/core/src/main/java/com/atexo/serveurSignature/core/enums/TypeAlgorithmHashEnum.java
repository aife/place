package com.atexo.serveurSignature.core.enums;

import javax.xml.crypto.dsig.DigestMethod;

/**
 *
 */
public enum TypeAlgorithmHashEnum {

    SHA1("SHA-1", "http://www.w3.org/2000/09/xmldsig#rsa-sha1", DigestMethod.SHA1, "http://uri.etsi.org/01903", "http://uri.etsi.org/01903/v1.3.2#", "SHA1withRSA"),
    SHA256("SHA-256", "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", DigestMethod.SHA256, "http://uri.etsi.org/01903", "http://uri.etsi.org/01903/v1.3.2#", "SHA256withRSA");

    private String nom;

    private String algoSignatureMethod;

    private String algoDigestMethod;

    private String nameSpace;

    private String nameSpaceVersionXades;

    private String signatureAlgorithm;

    TypeAlgorithmHashEnum(String nom, String algoSignatureMethod, String algoDigestMethod, String nameSpace, String nameSpaceVersionXades, String signatureAlgorithm) {
        this.nom = nom;
        this.algoSignatureMethod = algoSignatureMethod;
        this.algoDigestMethod = algoDigestMethod;
        this.nameSpace = nameSpace;
        this.nameSpaceVersionXades = nameSpaceVersionXades;
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAlgoSignatureMethod() {
        return algoSignatureMethod;
    }

    public String getAlgoDigestMethod() {
        return algoDigestMethod;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public String getNameSpaceVersionXades() {
        return nameSpaceVersionXades;
    }

    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }
}

