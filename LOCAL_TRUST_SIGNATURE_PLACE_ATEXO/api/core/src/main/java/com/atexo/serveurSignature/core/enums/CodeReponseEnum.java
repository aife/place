package com.atexo.serveurSignature.core.enums;

import lombok.*;

/**
 *
 */
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public enum CodeReponseEnum {

    Ok(0, null),
    ContenuFichierXmlNonPresent(1, "Fichier xml non présent"),
    TypeValidationNonPresent(2, "Le type de validation n'est pas présent"),
    TypeAlgorithmHashNonPresent(3, "Le type d'algorithm n'est pas présent"),
    HashFichierNonPresent(4, "Le hash du fichier n'est pas présent"),
    ContenuCertificatNonPresent(5, "Certificat non présent");

  private int code;

  private String message;

}
