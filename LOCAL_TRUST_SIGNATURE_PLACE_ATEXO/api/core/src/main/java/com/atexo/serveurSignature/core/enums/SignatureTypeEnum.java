package com.atexo.serveurSignature.core.enums;

public enum SignatureTypeEnum {
    CADES(1), PADES(2), XADES(3), INCONNUE(4);
    private int code;

    SignatureTypeEnum(int code) {
        this.code = code;
    }
    
    public int getCode() {
        return code;
    }
}
