package com.atexo.serveurSignature.core.utilitaire;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Signatures implements Serializable {
    private static final long serialVersionUID = 1970953344799449387L;

    private List<Signature> signatures;
    
    private String typeSignature = "INCONNUE";

    @JsonIgnore
    private boolean dssValidationEffectuee = false;

    @JsonIgnore
    private boolean atexoValidationEffectuee = false;

    @JsonIgnore
    private String reportsDiagnostic;

    @JsonIgnore
    private String reportsDetailed;
    
    // //XMLConstantes contient les constantes acceptés 
    private int etat;
    
    
    public List<Signature> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<Signature> signatures) {
        this.signatures = signatures;
    }

    public String getTypeSignature() {
        return typeSignature;
    }

    public void setTypeSignature(String typeSignature) {
        this.typeSignature = typeSignature;
    }
    
    public boolean isDssValidationEffectuee() {
        return dssValidationEffectuee;
    }

    public void setDssValidationEffectuee(boolean dssValidationEffectuee) {
        this.dssValidationEffectuee = dssValidationEffectuee;
    }

    public boolean isAtexoValidationEffectuee() {
        return atexoValidationEffectuee;
    }

    public void setAtexoValidationEffectuee(boolean atexoValidationEffectuee) {
        this.atexoValidationEffectuee = atexoValidationEffectuee;
    }

    public String getReportsDiagnostic() {
        return reportsDiagnostic;
    }

    public void setReportsDiagnostic(String reportsDiagnostic) {
        this.reportsDiagnostic = reportsDiagnostic;
    }

    public String getReportsDetailed() {
        return reportsDetailed;
    }

    public void setReportsDetailed(String reportsDetailed) {
        this.reportsDetailed = reportsDetailed;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public int getEtat() {
        return etat;
    }
    
}