package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Holder {

    private String holderName;
    private List<CertificateDetails> certList = new ArrayList<>();


}
