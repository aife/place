package com.atexo.serveurSignature.core.enums;


import com.atexo.serveurSignature.core.domain.signature.model.CertificatCodeRetour;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 */
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public enum CertificatValiditeEtatEnum implements CertificatCodeRetour {

	Valide(0), Expire(2), PasEncoreValide(5), Inconnu(1);

	private int codeRetour;


}
