package com.atexo.serveurSignature.core.enums;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Erreur Rest")
public class RestException extends RuntimeException {
    private static final long serialVersionUID = 100L;
    private int codeHTTP;
    
    public RestException(String message, int codeHTTP) {
        super(message);
        this.codeHTTP = codeHTTP;
    }
    
    
    public RestException(String message) {
        super(message);
    }
    
    public RestException(String message, Throwable cause) {
        super(message, cause);
    }


    public int getCodeHTTP() {
        return codeHTTP;
    }


    public void setCodeHTTP(int codeHTTP) {
        this.codeHTTP = codeHTTP;
    }
    
    
}
