package com.atexo.serveurSignature.core.domain.localServer.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InputOutputUrl {
	private String inputUrl;
	private String fileName;
	private String outputUrl;
	private Long size;
}
