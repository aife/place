package com.atexo.serveurSignature.core.domain.monitoring.service.impl;


import com.atexo.serveurSignature.core.domain.monitoring.model.FilesDetailResponse;
import com.atexo.serveurSignature.core.domain.monitoring.service.ApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ApplicationServiceImpl implements ApplicationService {

	private static final long MB = 1024L * 1024L;
	private static final long KB = 1024L;
	@Value("${signature.mock-folder}")
	private String signatureMockFolder;


	@Override
	public ResponseEntity<InputStreamResource> downloadSignedFile(String nom) {
		File file = new File(signatureMockFolder + File.separator +
				"signedFile", nom);
		try {
			return ResponseEntity.ok()
					// Content-Disposition
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + nom)
					// Content-Type
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					// Contet-Length
					.contentLength(file.length()) //
					.body(new InputStreamResource(new FileInputStream(file)));
		} catch (IOException e) {
			log.error("", e.fillInStackTrace());
		}
		return null;
	}


	@Override
	public List<FilesDetailResponse> getSignedFiles(String filtre, String type) {
		File logPath = new File(signatureMockFolder + File.separator +
				"signedFile");

		if (filtre != null && !filtre.isEmpty()) {
			filtre = "*" + filtre + "*";
		} else {
			filtre = "*";
		}
		Collection<File> fichierLogs = FileUtils.listFiles(logPath, new WildcardFileFilter(filtre), null);
		File[] fichiers = new File[fichierLogs.size()];
		fichiers = fichierLogs.toArray(fichiers);
		if (type != null) {
			if (type.equals("CROISSANT")) {
				Arrays.sort(fichiers, Comparator.comparingLong(File::lastModified));
			} else if (type.equals("DECROISSANT")) {
				Arrays.sort(fichiers, (a, b) -> Long.compare(b.lastModified(), a.lastModified()));
			}
		}

		return Arrays.stream(fichiers).map(file -> {
			try {
				BasicFileAttributes attr =
						Files.readAttributes(Paths.get(file.getAbsolutePath()), BasicFileAttributes.class);
				return FilesDetailResponse.builder().nom(file.getName()).dateModification(getDateFromFileTime(attr.lastModifiedTime()))
						.dateCreation(getDateFromFileTime(attr.creationTime()))
						.taille(KBToMB(attr.size()))
						.dernierAccess(getDateFromFileTime(attr.lastAccessTime()))
						.build();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return FilesDetailResponse.builder().nom(file.getName()).taille(KBToMB(file.length())).build();
		}).collect(Collectors.toList());
	}

	private String getDateFromFileTime(FileTime fileTime) {
		long cTime = fileTime.toMillis();
		ZonedDateTime t = Instant.ofEpochMilli(cTime).atZone(ZoneId.of("Europe/Berlin"));
		return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(t);

	}

	public String KBToMB(long octet) {
		float megaOctet = (float) octet / (float) MB;
		return megaOctet >= 1 ? (megaOctet + "MB") : ((float) octet / (float) KB + "KB");
	}

}
