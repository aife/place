package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.io.Serializable;

/**
 * Représente un répertoire pour effectuer la reponse json.
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Repertoire implements Serializable {

	private String nom;

	private String statut;

	public Repertoire(String nom) {
		this.nom = nom;
	}

}

