package com.atexo.serveurSignature.core.config.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.signature.model.Parametre;
import com.atexo.serveurSignature.core.exception.MauvaisParametreException;
import com.atexo.serveurSignature.dao.entity.ConfigurationEntity;
import com.atexo.serveurSignature.dao.repository.ConfigurationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@ManagedResource(objectName = "com.atexo:name=ConfigurationService")
public class ConfigurationServiceImpl implements ConfigurationService {
	private static final Logger logger = LoggerFactory.getLogger(ConfigurationServiceImpl.class);


	private final ConfigurationRepository configurationRepository;

	public ConfigurationServiceImpl(ConfigurationRepository configurationRepository) {
		this.configurationRepository = configurationRepository;
	}


	@Override
	public String getApplicationDirectoryTmp() {
		return System.getProperty("java.io.tmpdir");
	}

	@Override
	public String getValidationURL() {
		return this.getByParametre("application.validation.URL");
	}

	private String getByParametre(String parametre) throws MauvaisParametreException {

		return Optional.ofNullable(configurationRepository.getByParametre(parametre)).map(ConfigurationEntity::getValeur).orElse(null);
	}

	@Override
	public String getDssKeyStorePath() {
		return this.getByParametre("dss.keyStore.path");
	}

	@Override
	public String getDssKeyStoreCachePath() {
		return this.getByParametre("dss.keyStore.cache.path");
	}

	@Override
	public String getApplicationPathOpenSSL() {
		return this.getByParametre("application.openSSL.path");
	}

	@Override
	public String getApplicationContext() {
		return this.getByParametre("application.context");
	}

	@Override
	public String getValidationOriginePlateforme() {
		return this.getByParametre("application.validation.plateforme");
	}

	@Override
	public String getApplicationOpenSSLCaPath() {
		return this.getByParametre("application.openSSL.capath");
	}

	@Override
	public String getApplicationSHASignatureType() {
		return this.getByParametre("application.SHA.signature");
	}

	@Override
	public boolean getApplicationJwsHorodatageSignature() {
		String param = this.getByParametre("application.jws.horodatageSignature");
		return param != null && param.equalsIgnoreCase("true");
	}

	@Override
	public String getHorodatageUniversignURL() {
		return this.getByParametre("horodatage.universign.URL");
	}

	@Override
	public String getHorodatageUniversignLogin() {
		return this.getByParametre("horodatage.universign.login");
	}

	@Override
	public String getHorodatageUniversignPassword() {
		return this.getByParametre("horodatage.universign.password");
	}

	@Override
	public boolean verifieAccesHorodatageUniversign(String url) {
		return url.contains(this.getByParametre("horodatage.universign.url.contains"));
	}

	@Override
	public String getHorodatagePisteURL() {
		return this.getByParametre("horodatage.piste.URL");
	}

	@Override
	public boolean verifieAccesHorodatagePiste(String url) {
		return url.contains(this.getByParametre("horodatage.piste.url.contains"));
	}

	@Override
	public String getHorodatagePisteClientId() {
		return this.getByParametre("horodatage.piste.client.id");
	}

	@Override
	public String getHorodatagePisteClientSecret() {
		return this.getByParametre("horodatage.piste.clientSecret");
	}

	@Override
	public URI getHorodatagePisteAuthentificationURL() {
		try {
			return new URI(this.getByParametre("horodatage.piste.authentification.URL"));
		} catch (URISyntaxException e) {
			return null;
		}
	}

	@Override
	public String getApplicationSHAHorodatageType() {
		return this.getByParametre("application.SHA.horodatage");
	}

	@Override
	public String getURLJNLP() {
		return this.getByParametre("application.url.jnlp");
	}

	@Override
	public Boolean isDssValidationEnabled() {
		return this.getByParametre("dss.validation.enabled").equalsIgnoreCase("TRUE");
	}

	@Override
	public Boolean isAtexoValidationEnabled() {
		return this.getByParametre("atexo.validation.enabled").equalsIgnoreCase("TRUE");
	}

	@Override
	public List<Parametre> getConfiguration() {
		List<Parametre> parametres = new ArrayList<Parametre>();
		Iterable<ConfigurationEntity> configurationEntities = configurationRepository.findAll();
		logger.debug("Configuration serveur de validation");
		configurationEntities.forEach(configurationEntity -> {
			Parametre parametre = new Parametre();
			parametre.setNom(configurationEntity.getParametre());
			parametre.setValeur(configurationEntity.getValeur());
			parametre.setDescription(configurationEntity.getDescription());
			parametres.add(parametre);
			logger.debug(parametre.toString());
		});

		return parametres;

	}

	@Override
	public void setConfiguration(Parametre parameter) {

		configurationRepository.updateValeurByParametre(parameter.getNom(), parameter.getValeur());
	}

	@Override
	public boolean isCronTlLoaderEnable() {
		return Boolean.parseBoolean(this.getByParametre("cron.tl.loader.enable"));
	}

	@Override
	public String getTsaURL() {
		return this.getByParametre("tsa.url");
	}

	@Override
	public String getMockUrl() {
		return this.getByParametre("mock.url");
	}
}
