package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class JetonHorodatageReponse implements Serializable {
	private String code;
	private String message;
	private boolean jetonValide;
	private Date horodatage;


}
