package com.atexo.serveurSignature.core.domain.signature.service;

import com.atexo.serveurSignature.core.domain.signature.model.ReponseEnrichissementSignature;

public interface SignatureValidationService {

    ReponseEnrichissementSignature getReponseServeurValidation(String contenuFichierXML, String certificat, String typeEchange, String typeAlgorithmHash, String hashFichier, String intituleConsultation, String fonction);

}
