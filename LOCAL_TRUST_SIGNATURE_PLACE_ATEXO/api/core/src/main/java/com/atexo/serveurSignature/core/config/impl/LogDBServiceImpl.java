package com.atexo.serveurSignature.core.config.impl;

import com.atexo.serveurSignature.core.config.LogDBService;
import com.atexo.serveurSignature.core.utilitaire.Signature;
import com.atexo.serveurSignature.core.utilitaire.Signatures;
import com.atexo.serveurSignature.dao.entity.LogHorodatageEntity;
import com.atexo.serveurSignature.dao.entity.LogSignatureEntity;
import com.atexo.serveurSignature.dao.entity.LogValidationEntity;
import com.atexo.serveurSignature.dao.repository.LogHorodatageRepository;
import com.atexo.serveurSignature.dao.repository.LogSignatureRepository;
import com.atexo.serveurSignature.dao.repository.LogValidationRepository;
import org.springframework.stereotype.Service;

import javax.security.auth.x500.X500Principal;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class LogDBServiceImpl implements LogDBService {
	private final LogValidationRepository logValidationRepository;

	private final LogSignatureRepository logSignatureRepository;

	private final LogHorodatageRepository logHorodatageRepository;

	public LogDBServiceImpl(LogValidationRepository logValidationRepository, LogSignatureRepository logSignatureRepository, LogHorodatageRepository logHorodatageRepository) {
		this.logValidationRepository = logValidationRepository;
		this.logSignatureRepository = logSignatureRepository;
		this.logHorodatageRepository = logHorodatageRepository;
	}

	@Override
	public void insertValidation(String plateforme, String context, Signatures signatures, long tempsRequete) {
		if (signatures.getSignatures() == null || signatures.getSignatures().isEmpty()) {
			return;
		}
		List<LogValidationEntity> logValidationEntityList = new ArrayList<>();
		for (Signature signature : signatures.getSignatures()) {
			LogValidationEntity logValidationEntity = new LogValidationEntity();
			logValidationEntity.setPlateforme(plateforme);
			if (signature.getSignataireDetails() != null) {
				logValidationEntity.setCn(signature.getSignataireDetails().getCn());
			} else {
				logValidationEntity.setCn(null);
			}
			if (signature.getEmetteurDetails() != null) {
				logValidationEntity.setAc(signature.getEmetteurDetails().getCn());
			} else {
				logValidationEntity.setAc(null);
			}
			logValidationEntity.setId(signature.getSerialNumber());
			logValidationEntity.setCertificatValide(signature.getEtat() != 0);
			if (signature.getRepertoiresChaineCertification() != null && !signature.getRepertoiresChaineCertification().isEmpty()) {
				logValidationEntity.setTsl(signature.getRepertoiresChaineCertification().iterator().next().getNom());
			} else {
				logValidationEntity.setTsl(null);
			}
			logValidationEntity.setTypeSignature(signatures.getTypeSignature());
			logValidationEntity.setValidationAtexo(signatures.isAtexoValidationEffectuee());
			logValidationEntity.setValidationDss(signatures.isDssValidationEffectuee());
			logValidationEntity.setTempExecution((int) tempsRequete);
			logValidationEntityList.add(logValidationEntity);
		}
		logValidationRepository.saveAll(logValidationEntityList);
	}

	@Override
	public void insertHorodatage(String plateforme, String hash, byte[] jeton, boolean ok, long tempsRequete) {

		LogHorodatageEntity logHorodatageEntity = new LogHorodatageEntity();
		logHorodatageEntity.setPlateforme(plateforme);
		logHorodatageEntity.setHash(hash);
		logHorodatageEntity.setJeton(jeton);
		logHorodatageEntity.setOk(ok);
		logHorodatageEntity.setTempExecution((int) tempsRequete);

		logHorodatageRepository.save(logHorodatageEntity);
	}

	private final static String QUERY_SIGNATURE = "INSERT INTO LOG_SIGNATURE (PLATEFORME, CN, AC, ID, TYPE_SIGNATURE, CERTIFICAT, TEMPS_EXECUTION) VALUES(?,?,?,?,?,?,?)";

	@Override
	public void insertSignature(String plateforme, X509Certificate certificate, String typeSignature, int tempsRequete) {
		LogSignatureEntity logSignatureEntity = new LogSignatureEntity();
		logSignatureEntity.setPlateforme(plateforme);
		if (certificate.getIssuerX500Principal() != null) {
			logSignatureEntity.setCn(certificate.getIssuerX500Principal().getName(X500Principal.RFC2253));
		} else {
			logSignatureEntity.setCn(null);
		}
		if (certificate.getSubjectX500Principal() != null) {
			logSignatureEntity.setAc(certificate.getSubjectX500Principal().getName(X500Principal.CANONICAL));
		} else {
			logSignatureEntity.setAc(null);
		}
		logSignatureEntity.setId(certificate.getSerialNumber().toString());
		logSignatureEntity.setTypeSignature(typeSignature);
		try {
			logSignatureEntity.setCertificat(Base64.getEncoder().encodeToString(certificate.getEncoded()));
		} catch (CertificateEncodingException e) {
			logSignatureEntity.setCertificat(null);
		}
		logSignatureEntity.setTempExecution(tempsRequete);

		logSignatureRepository.save(logSignatureEntity);
	}
}
