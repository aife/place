package com.atexo.serveurSignature.core.utilitaire;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * Classe d'utilitaire.
 */
public abstract class Util {


	public static boolean isDateCompriseEntre(Date dateComparaitre, Date dateDebut, Date dateFin) {
		return dateDebut.before(dateComparaitre) && dateFin.after(dateComparaitre);
	}

	public static String creerISO8601DateTime(Date date) {
		DateFormat ISO8601Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		TimeZone timeZone = TimeZone.getDefault();
		ISO8601Local.setTimeZone(timeZone);
		int offset = timeZone.getOffset(date.getTime());

		String sign = "+";
		if (offset < 0) {
			offset = -offset;
			sign = "-";
		}

		int hours = offset / 3600000;
		int minutes = (offset - hours * 3600000) / 60000;
		if (offset != hours * 3600000 + minutes * 60000) {
			// E.g. TZ=Asia/Riyadh87
			throw new RuntimeException("TimeZone offset (" + sign + offset + " ms) is not an exact number of minutes");
		}

		DecimalFormat twoDigits = new DecimalFormat("00");
		String ISO8601Now = ISO8601Local.format(date) + sign + twoDigits.format(hours) + ":" + twoDigits.format(minutes);

		return ISO8601Now;
	}

}
