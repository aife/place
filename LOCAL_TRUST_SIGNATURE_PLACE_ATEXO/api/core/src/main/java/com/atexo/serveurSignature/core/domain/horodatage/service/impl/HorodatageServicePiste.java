package com.atexo.serveurSignature.core.domain.horodatage.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.horodatage.OnLineTSPSource;
import com.atexo.serveurSignature.core.domain.horodatage.model.PisteDTO;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.exception.HorodatageException;
import com.atexo.serveurSignature.core.exception.MessageExceptionEnum;
import com.atexo.serveurSignature.core.utilitaire.AtexoDataLoader;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

public class HorodatageServicePiste extends HorodatageServiceImpl {
	private static final Logger logger = LoggerFactory.getLogger(HorodatageServicePiste.class);
	private ConfigurationService configurationService;
	private final RestTemplate restTemplate;

	public HorodatageServicePiste(ConfigurationService configurationService, JMXService jmxService, RestTemplate restTemplate) {
		super(configurationService, jmxService, restTemplate);
		this.configurationService = configurationService;
		this.restTemplate = restTemplate;
	}

	@Override
	public byte[] getTokenRFC3161(byte[] digest) throws HorodatageException {
		OnLineTSPSource tspSource = new OnLineTSPSource(configurationService.getHorodatagePisteURL());
		AtexoDataLoader dataLoader = new AtexoDataLoader();
		String bearer = getAuthentification();
		try {
			dataLoader.addBearer(bearer);
			dataLoader.setContentType("application/timestamp-query");
			tspSource.setDataLoader(dataLoader);

			TimeStampResponse timeStampResponse = tspSource.getTimeStampResponse2(DigestAlgorithm.SHA256, digest);
			return timeStampResponse.getEncoded();
		} catch (IOException e) {
			logger.error("Erreur horodatage {}", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		}

	}

	@Override
	public Date getTSRDate(String xml, byte[] timeToken) throws HorodatageException {
		TimeStampToken token;
		try {
			token = new TimeStampToken(new CMSSignedData(timeToken));
		} catch (TSPException | IOException | CMSException e) {
			logger.error("Erreur horodatage {}", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		}

		return token.getTimeStampInfo().getGenTime();
	}

	private String getAuthentification() throws HorodatageException {
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("grant_type", "client_credentials");
		parameters.add("scope", "openid");
		parameters.add("client_id", configurationService.getHorodatagePisteClientId());
		parameters.add("client_secret", configurationService.getHorodatagePisteClientSecret());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(parameters, headers);
		ResponseEntity<PisteDTO> bearer = null;
		try {
			bearer = restTemplate.postForEntity(configurationService.getHorodatagePisteAuthentificationURL(), requestEntity, PisteDTO.class);
			logger.info("bearer :{}", Objects.requireNonNull(bearer.getBody()).getAccess_token());
		} catch (RestClientException e) {
			logger.error("Impossible de s'authentifier aupres du serveur {} avec le login \"{}\" et mot de passe \"{}\"", configurationService.getHorodatagePisteAuthentificationURL(),
					configurationService.getHorodatagePisteClientId(), configurationService.getHorodatagePisteClientSecret());
			logger.error("erreur Authentificzation pPISTE", e.fillInStackTrace());
			throw e;
		}
		if (bearer.getStatusCodeValue() != 200) {
			logger.error("Impossible de s'authentifier aupres du serveur {} avec le login \"{}\" et mot de passe \"{}\"", configurationService.getHorodatagePisteAuthentificationURL(),
					configurationService.getHorodatagePisteClientId(), configurationService.getHorodatagePisteClientSecret());
			throw new HorodatageException(MessageExceptionEnum.ECHEC_AUTHENTIFICATION);
		}
		if (bearer.getBody() == null) {
			logger.error("Impossible de s'authentifier aupres du serveur {} avec le login \"{}\" et mot de passe \"{}\"", configurationService.getHorodatagePisteAuthentificationURL(),
					configurationService.getHorodatagePisteClientId(), configurationService.getHorodatagePisteClientSecret());
			throw new HorodatageException(MessageExceptionEnum.ECHEC_AUTHENTIFICATION);
		}
		if (bearer.getBody().getAccess_token().isEmpty()) {
			logger.error("Impossible de s'authentifier aupres du serveur {} avec le clientID \"{}\" et clientSecret \"{}\"", configurationService.getHorodatagePisteAuthentificationURL(),
					configurationService.getHorodatagePisteClientId(), configurationService.getHorodatagePisteClientSecret());
			throw new HorodatageException(MessageExceptionEnum.ECHEC_AUTHENTIFICATION);
		}
		return bearer.getBody().getAccess_token();
	}
}
