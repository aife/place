package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.math.BigInteger;
import java.util.Date;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class CertificateDetails {

	private Date notBefore;
	private Date notAfter;
	private Integer version;
	private BigInteger serialNumber;
	private String issuerDN;
	private String issuerX500Principal;
	private String subjectDN;
	private String subjectX500Principal;
	private String sigAlgName;
	private String sigAlgOID;

}
