package com.atexo.serveurSignature.core.domain.monitoring.service;


import com.atexo.serveurSignature.core.domain.monitoring.model.FilesDetailResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ApplicationService {
	List<FilesDetailResponse> getSignedFiles(String filtre, String type);

	ResponseEntity<InputStreamResource> downloadSignedFile(String nom);
}
