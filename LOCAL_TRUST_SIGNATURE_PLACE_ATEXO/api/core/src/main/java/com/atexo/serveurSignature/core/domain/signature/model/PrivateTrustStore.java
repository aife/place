package com.atexo.serveurSignature.core.domain.signature.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class PrivateTrustStore {

	private List<Holder> holders = new ArrayList<>();

}
