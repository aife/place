package com.atexo.serveurSignature.core.domain.signature.service;

import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.exception.ValidationException;
import com.atexo.serveurSignature.core.utilitaire.Signatures;

import java.io.File;

public interface SignatureVerificationService {

	/**
	 * This method will validate a document received from MPE.
	 *
	 * @param validationContext it contains all parameters for the validation.
	 * @return InfosVerificationCertificat to be forwarded to MPE
	 * @throws ValidationException if something goes wrong.
	 */
	Signatures validationDocumentDSS(final ContextSignature validationContext, File fichierPDF) throws ValidationException;

	Signatures extractSignatures(ContextSignature context);
}
