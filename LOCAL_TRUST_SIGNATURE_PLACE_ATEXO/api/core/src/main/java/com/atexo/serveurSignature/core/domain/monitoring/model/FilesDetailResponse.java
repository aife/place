package com.atexo.serveurSignature.core.domain.monitoring.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilesDetailResponse {
    private String nom;
    private String dateCreation;
    private String dernierAccess;
    private String dateModification;
    private String taille;
}
