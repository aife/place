package com.atexo.serveurSignature.core.domain.jmx.service;

import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.jmx.export.notification.NotificationPublisher;
import org.springframework.jmx.export.notification.NotificationPublisherAware;
import org.springframework.stereotype.Service;

import javax.management.Notification;

@Service
@ManagedResource(description = "comptabilise le nombe d'acces au service de validation siganture")
public class SignatureBean implements NotificationPublisherAware {
	private String typeAction;
	private int compteurGlobal;
	private int compteurErreur;
	private long temps;
	private NotificationPublisher notificationPublisher;

	public String getActionSignature() {
		return typeAction;
	}

	public void setTypeActionValide(long timer, String typeAction) {
		this.typeAction = typeAction;
		temps += Math.round(timer / 10.0);
		compteurGlobal++;
	}

	public void setTypeActionErreur(String typeAction) {
		this.typeAction = typeAction;
		compteurErreur++;
		notificationPublisher.sendNotification(new Notification(typeAction + "_ERROR", this, compteurGlobal++));
	}

	public int getCompteurErreur() {
		return compteurErreur;
	}

	public int getCompteurGlobal() {
		return compteurGlobal;
	}

	public void setCompteurGlobal(int compteurGlobal) {
		this.compteurGlobal = compteurGlobal;
	}

	@Override
	public void setNotificationPublisher(NotificationPublisher notificationPublisher) {
		this.notificationPublisher = notificationPublisher;
	}

	public long getTemps() {
		return temps;
	}
}
