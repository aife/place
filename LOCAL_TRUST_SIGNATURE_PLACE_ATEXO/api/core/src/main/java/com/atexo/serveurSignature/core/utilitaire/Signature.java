package com.atexo.serveurSignature.core.utilitaire;

import com.atexo.serveurSignature.core.domain.signature.model.Repertoire;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.util.Set;

/**
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Signature implements Serializable {
	private static final long serialVersionUID = 5443861700601895753L;
	private int etat = -1;
	private String signatairePartiel;
	private String signataireComplet;
	private String emetteur;
	private String dateValiditeDu;
	private String dateValiditeAu;
	private Boolean periodiciteValide;
	private int chaineDeCertificationValide;
	private int absenceRevocationCRL;
	private int dateSignatureValide;
	private Boolean signatureValide;
	private ObjectNaming signataireDetails;
	private ObjectNaming signataireDetail;
	private ObjectNaming emetteurDetails;
	private Set<Repertoire> repertoiresChaineCertification;
	private Set<Repertoire> repertoiresRevocation;
	private String signatureXadesServeurEnBase64;
	private int qualifieEIDAS;
	private String formatSignature;
	private String dateIndicative;
	private int jetonHorodatage;
	private String dateHorodatage;
	@JsonIgnore
	private String serialNumber;
}
