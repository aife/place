package com.atexo.serveurSignature.core.domain.signature.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atexo.serveurSignature.core.domain.signature.service.ReferentielCertificatConfiguration;
import org.apache.commons.io.IOUtils;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

@Service
@ManagedResource(objectName = "com.atexo:name=ReferentielCertificatConfiguration")
public class ReferentielCertificatConfigurationImpl implements ReferentielCertificatConfiguration {
    private final Map<String, String> referentiel;

    public ReferentielCertificatConfigurationImpl() throws IOException {
        InputStream reponseStream = ReferentielCertificatConfigurationImpl.class.getClassLoader().getResourceAsStream("config-signature-referentiels.csv");
        referentiel = new HashMap<String, String>();
        List<String> enregistrement = IOUtils.readLines(reponseStream, "UTF-8");
        for (int i = 0; i < enregistrement.size(); i++) {
            String[] tokens = enregistrement.get(i).split(",");
            if (tokens.length == 2) {
                referentiel.put(tokens[0].toUpperCase(), tokens[1]);
            }
            if (tokens.length == 1) {
                referentiel.put(tokens[0].toUpperCase(), "");
            }
        }
    }

    @Override
    public String getReferentielNorme(String valeurNonNorme) {
        if (valeurNonNorme != null) {
            if (referentiel.get(valeurNonNorme.toUpperCase()) == null) {
                return valeurNonNorme;
            }
            return referentiel.get(valeurNonNorme.toUpperCase());
        } else {
            return "";
        }
    }

    @Override
    public boolean isStatut(String valeurNonNorme) {
        if (valeurNonNorme != null) {
            return referentiel.get(valeurNonNorme.toUpperCase()) == null;
        }
        return false;
    }
}
