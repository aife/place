package com.atexo.serveurSignature.core.exception;

@SuppressWarnings("serial")
public class CryptographieServeurException extends Exception {

	public CryptographieServeurException(MessageExceptionEnum message) {
		super(message.name());
	}

	public CryptographieServeurException(MessageExceptionEnum message, Throwable cause) {
		super(message.name(), cause);
	}

}
