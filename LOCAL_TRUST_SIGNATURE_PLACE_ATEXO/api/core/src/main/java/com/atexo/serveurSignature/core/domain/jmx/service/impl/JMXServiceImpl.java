package com.atexo.serveurSignature.core.domain.jmx.service.impl;

import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.domain.jmx.service.SignatureBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JMXServiceImpl implements JMXService {
    private final SignatureBean bean;

    public JMXServiceImpl(SignatureBean bean) {
        this.bean = bean;
    }

    @Override
    public void addActionValide(long temps, String action) {
        bean.setTypeActionValide(temps, action);
    }   
    
    @Override
    public void addActionErreur(String action) {
        bean.setTypeActionErreur(action);
    }   
}
