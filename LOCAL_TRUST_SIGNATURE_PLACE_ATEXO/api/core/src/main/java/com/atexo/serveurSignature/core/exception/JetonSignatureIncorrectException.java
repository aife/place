package com.atexo.serveurSignature.core.exception;

@SuppressWarnings("serial")
public class JetonSignatureIncorrectException extends Exception {

	public JetonSignatureIncorrectException(MessageExceptionEnum message) {
		super(message.name());
	}

	public JetonSignatureIncorrectException(MessageExceptionEnum message, Throwable cause) {
		super(message.name(), cause);
	}

}
