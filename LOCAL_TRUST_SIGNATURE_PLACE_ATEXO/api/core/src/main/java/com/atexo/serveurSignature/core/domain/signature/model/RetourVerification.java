package com.atexo.serveurSignature.core.domain.signature.model;

import com.atexo.serveurSignature.core.enums.CertificatValiditeEtatEnum;
import com.atexo.serveurSignature.core.enums.CertificatVerificationEtatEnum;
import lombok.*;


/**
 *
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class RetourVerification extends AbstractRepertoireReponse {
	private static final long serialVersionUID = 1L;

	private CertificatValiditeEtatEnum periodeValidite;

	private CertificatVerificationEtatEnum chaineCertification;

	private CertificatVerificationEtatEnum revocation;

}
