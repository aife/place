package com.atexo.serveurSignature.core.utilitaire;

import com.atexo.serveurSignature.core.exception.MessageExceptionEnum;
import com.atexo.serveurSignature.core.exception.SignatureException;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

@Slf4j
public class DocumentUtils {
	private DocumentUtils() {
		throw new IllegalStateException("Classe utilitaire");
	}

	public static File saveFile(InputStream file, String directory, String fileName, boolean override) throws SignatureException {
		try {
			File directoryFile = new File(directory);
			if (!directoryFile.exists()) {
				log.info("Création des dossiers {}", directory);
				Files.createDirectories(directoryFile.toPath());
			}

			File convFile = new File(directory, fileName);
			if (convFile.exists() && !override) {
				log.info("Le fichier {} existe déjà", convFile.getAbsolutePath());
				return convFile;
			}
			Files.deleteIfExists(convFile.toPath());

			log.info("Création du fichier {}", convFile.getAbsolutePath());
			Files.createFile(convFile.toPath());
			try (FileOutputStream fos = new FileOutputStream(convFile)) {
				fos.write(IOUtils.toByteArray(file));
				log.info("Fichier {} est sauvegardé dans {}", fileName, directory);
			}
			return convFile;
		} catch (IOException e) {
			log.error("Erreur lors du sauvegarde {}", e.getMessage());
			throw new SignatureException(MessageExceptionEnum.ECHEC_BLOC_CHIFFRER, e);
		}
	}

	public static JsonNode getReplacement(JsonNode jsonNode, String champsFusion) {
		String[] attributs = champsFusion.split("\\.");
		try {

			for (String attribut : attributs) {
				String field = attribut;
				int index = -1;
				if (attribut.contains("(") && attribut.endsWith(")")) {
					String[] split = attribut.split("\\(");
					field = split[0];
					index = Integer.parseInt(split[1]
							.split("\\)")[0]);
				}
				jsonNode = index == -1 ? jsonNode.get(field) : jsonNode.get(field).get(index);
				if (jsonNode == null) {
					break;
				}
			}
			return jsonNode;
		} catch (Exception e) {
			log.error("Erreur lors de la récupération de la valeur : {}", e.getMessage());
			return null;
		}

	}

	public static void deleteFolder(File folder) throws IOException {
		if (Files.exists(folder.toPath())) {
			Files.walk(folder.toPath())
					.sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.forEach(File::delete);
			log.info("Suppression de {}", folder.getAbsolutePath());

		}
	}

	public static File createFolder(String parent, String child) throws IOException {
		File folder = new File(parent, child);
		if (!Files.exists(folder.toPath()))
			Files.createDirectory(folder.toPath());

		return folder;
	}
}
