package com.atexo.serveurSignature.core.enums;


import com.atexo.serveurSignature.core.domain.signature.model.CertificatCodeRetour;
import lombok.*;

/**
 *
 */
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public enum CertificatVerificationEtatEnum implements CertificatCodeRetour {

	VerificationValide(0),

	CertificatInvalide(1), SignatureCertificatInvalide(1),

	ChaineCertificatNonTrouvee(2), ChaineCertificatIncomplete(2),

	CertificatRevoque(2), FichierCrlNonTrouve(1);

	private int codeRetour;


}
