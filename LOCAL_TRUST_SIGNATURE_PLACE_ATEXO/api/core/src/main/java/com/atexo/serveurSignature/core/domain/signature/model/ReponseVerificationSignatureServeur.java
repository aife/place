package com.atexo.serveurSignature.core.domain.signature.model;

import com.atexo.serveurSignature.core.enums.CodeReponseEnum;

/**
 *
 */
public class ReponseVerificationSignatureServeur extends ReponseVerificationSignature {
	private static final long serialVersionUID = 1L;
	private boolean signatureValide;

	public ReponseVerificationSignatureServeur() {
	}

	public ReponseVerificationSignatureServeur(CodeReponseEnum codeReponse, RetourVerification resultat, Boolean dateSignatureValide, boolean signatureValide) {
		super(codeReponse, resultat, dateSignatureValide);
		this.signatureValide = signatureValide;
	}

	public boolean isSignatureValide() {
		return signatureValide;
	}

	public void setSignatureValide(boolean signatureValide) {
		this.signatureValide = signatureValide;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("ReponseVerificationSignatureServeur");
		sb.append("{ codeRetour=").append(getCodeRetour());
		sb.append(", message='").append(getMessage()).append('\'');
		sb.append(", resultat=").append(getResultat());
		sb.append(", dateSignatureValide=").append(getDateSignatureValide());
		sb.append(", signatureValide=").append(signatureValide);
		sb.append('}');
		return sb.toString();
	}
}
