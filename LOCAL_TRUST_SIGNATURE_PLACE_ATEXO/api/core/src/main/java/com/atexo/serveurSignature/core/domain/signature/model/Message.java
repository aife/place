package com.atexo.serveurSignature.core.domain.signature.model;

import java.io.Serializable;

import lombok.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message implements Serializable {
    private transient static final Logger logger = LoggerFactory.getLogger(Message.class);
    private static final long serialVersionUID = 1L;
    private String code;
	private String message;


	public void setMessage(String message) {
	    logger.info(message);
		this.message = message;
	}
}
