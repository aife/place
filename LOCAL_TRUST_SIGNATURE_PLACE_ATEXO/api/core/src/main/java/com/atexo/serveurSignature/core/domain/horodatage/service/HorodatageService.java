package com.atexo.serveurSignature.core.domain.horodatage.service;

import com.atexo.serveurSignature.core.exception.HorodatageException;

import java.util.Date;

public interface HorodatageService {

	boolean isTSRValide(String xml, byte[] tsr) throws HorodatageException;

	byte[] getJetonDER(byte[] jetonTSQ) throws HorodatageException;

	byte[] getJetonTSQ(byte[] xml) throws HorodatageException;

	Date getTSRDate(String xml, byte[] tsr) throws HorodatageException;

	byte[] getTokenRFC3161(byte[] hash) throws HorodatageException;
}
