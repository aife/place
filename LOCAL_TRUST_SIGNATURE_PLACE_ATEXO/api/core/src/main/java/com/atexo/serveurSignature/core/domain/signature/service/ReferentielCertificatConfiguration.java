package com.atexo.serveurSignature.core.domain.signature.service;

public interface ReferentielCertificatConfiguration {

    String getReferentielNorme(String valeurNonNorme);

    boolean isStatut(String valeurNonNorme);

}
