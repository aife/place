package com.atexo.serveurSignature.core.domain.horodatage.service.impl;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.core.domain.horodatage.service.HorodatageService;
import com.atexo.serveurSignature.core.domain.jmx.service.JMXService;
import com.atexo.serveurSignature.core.exception.HorodatageException;
import com.atexo.serveurSignature.core.exception.MessageExceptionEnum;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class HorodatageServiceImpl implements HorodatageService {
	private static final Logger logger = LoggerFactory.getLogger(HorodatageServiceImpl.class);
	private ConfigurationService configurationService;

	public HorodatageServiceImpl(ConfigurationService configurationService, JMXService jmxService, RestTemplate restTemplate) {
		this.configurationService = configurationService;
	}

	@Override
	public boolean isTSRValide(String xml, byte[] tsr) throws HorodatageException {
		File reponseAnnonce = new File(configurationService.getApplicationDirectoryTmp() + "/reponseAnnonce_" + System.currentTimeMillis());
		File jetonTSR = null;
		ByteArrayInputStream xmlStream = null;
		ByteArrayInputStream tsrStream = null;
		try {
			xmlStream = new ByteArrayInputStream(xml.getBytes());
			FileUtils.copyToFile(xmlStream, reponseAnnonce);
			jetonTSR = new File(configurationService.getApplicationDirectoryTmp() + "/jeton_" + System.currentTimeMillis() + ".tsr");
			tsrStream = new ByteArrayInputStream(tsr);
			FileUtils.copyToFile(tsrStream, jetonTSR);
		} catch (IOException e) {
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		} finally {
			IOUtils.closeQuietly(xmlStream);
			IOUtils.closeQuietly(tsrStream);
		}

		List<String> command = new ArrayList<String>();
		command.add(configurationService.getApplicationPathOpenSSL());
		command.add("ts");
		command.add("-verify");
		command.add("-data");
		command.add(reponseAnnonce.getAbsolutePath());

		command.add("-in");
		command.add(jetonTSR.getAbsolutePath());
		command.add("-CApath");
		command.add(configurationService.getApplicationOpenSSLCaPath());
		String resultat = null;
		try {
			resultat = executeCommand(command);
		} catch (HorodatageException e) {
			throw e;
		} finally {
			FileUtils.deleteQuietly(jetonTSR);
			FileUtils.deleteQuietly(reponseAnnonce);
		}
		return resultat.contains("Verification: OK");
	}

	/*
	 * /usr/bin/openssl ts -reply -queryfile jeton.tsq -config /tmp/tmp/openssl.cnf
	 * -signer /data/local-trust/mpe_atexo_int/common/ca/certs/ts.crt -inkey
	 * /data/local-trust/mpe_atexo_int/common/ca/keys/ts_pkey.pem -out jeton.tsr
	 * -section TSA_HORODATAGE
	 *
	 * @see com.atexo.serveurCryptographique.service.HorodatageService#getJetonTSR()
	 */
	@Override
	public byte[] getJetonDER(byte[] tsq) throws HorodatageException {
		List<String> command = new ArrayList<String>();
		File jetonTSQ = null;
		File jetonTSR = null;
		ByteArrayInputStream bis = null;
		try {
			jetonTSQ = File.createTempFile("jeton_", ".tsq");
			jetonTSR = File.createTempFile("jeton_", ".tsr");

			bis = new ByteArrayInputStream(tsq);
			FileUtils.copyInputStreamToFile(new ByteArrayInputStream(tsq), jetonTSQ);
		} catch (IOException e) {
			logger.error("Impossible de recuperer le reponseAnnonce dans le cadre de l'horodatage ", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC);
		} finally {
			try {
				bis.close();
			} catch (IOException e) {
			}
		}
		command.add(configurationService.getApplicationPathOpenSSL());
		command.add("ts");
		command.add("-reply");
		command.add("-queryfile");
		command.add(jetonTSQ.getAbsolutePath());
		command.add("-config");
		command.add(configurationService.getApplicationOpenSSLCaPath() + "openssl.cnf");
		command.add("-signer");
		command.add(configurationService.getApplicationOpenSSLCaPath() + "ts.crt");
		command.add("-inkey");
		command.add(configurationService.getApplicationOpenSSLCaPath() + "ts_pkey.pem");
		command.add("-out");
		command.add(jetonTSR.getAbsolutePath());
		command.add("-section");
		command.add("TSA_HORODATAGE");
		executeCommand(command);
		FileInputStream fis = null;
		try {
			if (jetonTSR.length() == 0) {
				throw new IOException("Fichier Horodatage est vide");
			}
			fis = new FileInputStream(jetonTSR);
			return IOUtils.toByteArray(fis);
		} catch (IOException e) {
			logger.error("Impossible de recuperer le reponseAnnonce dans le cadre de l'horodatage ", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC);
		} finally {
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(bis);
			FileUtils.deleteQuietly(jetonTSQ);
			FileUtils.deleteQuietly(jetonTSR);
		}
	}

	/**
	 * /usr/bin/openssl ts -query -data reponseAnnonce.xml -cert -out jeton.tsq;
	 *
	 * @throws IOException
	 */
	@Override
	public byte[] getJetonTSQ(byte[] xml) throws HorodatageException {
		logger.info("hash  pour creer le jeton TSR {}", xml);
		File reponseAnnonce = null;
		File jetonTSQ = null;
		ByteArrayInputStream bis = null;
		FileInputStream fis = null;
		try {
			reponseAnnonce = File.createTempFile("reponseAnnonce_", ".xml");
			bis = new ByteArrayInputStream(xml);
			FileUtils.copyToFile(bis, reponseAnnonce);
			List<String> command = new ArrayList<String>();

			jetonTSQ = File.createTempFile("jeton_", ".tsq");
			command.add(configurationService.getApplicationPathOpenSSL());
			command.add("ts");
			command.add("-query");
			command.add("-data");
			command.add(reponseAnnonce.getAbsolutePath());
			command.add("-cert");
			command.add("-out");
			command.add(jetonTSQ.getAbsolutePath());
			executeCommand(command);
			fis = new FileInputStream(jetonTSQ);
			return IOUtils.toByteArray(fis);
		} catch (IOException e) {
			logger.error("Impossible de recuperer le jeton horodatage", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC);
		} finally {
			IOUtils.closeQuietly(bis);
			IOUtils.closeQuietly(fis);
			FileUtils.deleteQuietly(reponseAnnonce);
			FileUtils.deleteQuietly(jetonTSQ);
		}
	}

	/**
	 * // /usr/bin/openssl ts -reply -data reponseAnnonce -in jeton.tsr -text
	 *
	 * @throws ParseException
	 */
	@Override
	public Date getTSRDate(String xml, byte[] der) throws HorodatageException {
		File reponseAnnonce = null;
		File jetonDER = null;
		ByteArrayInputStream tsrStream = null;
		ByteArrayInputStream xmlStream = null;
		Date date = null;
		try {
			reponseAnnonce = File.createTempFile("reponseAnnonce_", ".xml");
			xmlStream = new ByteArrayInputStream(xml.getBytes());
			FileUtils.copyToFile(xmlStream, reponseAnnonce);
			jetonDER = File.createTempFile("jeton_", ".der");
			tsrStream = new ByteArrayInputStream(der);
			FileUtils.copyToFile(tsrStream, jetonDER);

			List<String> command = new ArrayList<String>();
			command.add(configurationService.getApplicationPathOpenSSL());
			command.add("ts");
			command.add("-reply");
			command.add("-in");
			command.add(jetonDER.getAbsolutePath());
			command.add("-token_in");
			command.add("-text");
			command.add("-token_out");
			String resultat = executeCommand(command);
			int min = resultat.indexOf("Time stamp:") + "Time stamp:".length();
			int max = resultat.indexOf("GMT") + 3;
			SimpleDateFormat simpleDate = new SimpleDateFormat("MMM dd HH:mm:ss yyyy Z", Locale.ENGLISH);

			date = simpleDate.parse(resultat.substring(min, max).trim());
		} catch (IOException | ParseException e) {
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		} finally {
			IOUtils.closeQuietly(tsrStream);
			IOUtils.closeQuietly(xmlStream);
			FileUtils.deleteQuietly(reponseAnnonce);
			FileUtils.deleteQuietly(jetonDER);
		}
		return date;
	}

	private String executeCommand(List<String> command) throws HorodatageException {
		Process p = null;
		String resultat = null;
		try {
			logger.info(command.toString());
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.redirectErrorStream(true);
			p = pb.start();
			p.waitFor();
			resultat = IOUtils.toString(p.getInputStream(), "UTF-8");
			if (resultat == null) {
				resultat = IOUtils.toString(p.getErrorStream(), "UTF-8");
			}
			logger.info(resultat);
		} catch (Exception e) {
			logger.error("erreur a l'execution de la commande openSSL ", e.fillInStackTrace());
			throw new HorodatageException(MessageExceptionEnum.HORODATAGE_ECHEC, e.fillInStackTrace());
		} finally {
			IOUtils.closeQuietly(p.getInputStream());
			IOUtils.closeQuietly(p.getErrorStream());
		}
		return resultat;
	}

	@Override
	public byte[] getTokenRFC3161(byte[] digest) throws HorodatageException {
		// byte[] tsq = getJetonTSQ(digest);
		byte[] tst = getJetonDER(digest);
		return tst;
	}

}
