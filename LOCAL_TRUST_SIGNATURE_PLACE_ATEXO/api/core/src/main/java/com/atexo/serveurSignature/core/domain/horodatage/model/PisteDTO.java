package com.atexo.serveurSignature.core.domain.horodatage.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PisteDTO implements Serializable {
	private String access_token;
	private String token_type;
	private int expire_fin;
	private String scope;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public int getExpire_fin() {
		return expire_fin;
	}

	public void setExpire_fin(int expire_fin) {
		this.expire_fin = expire_fin;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}
}
