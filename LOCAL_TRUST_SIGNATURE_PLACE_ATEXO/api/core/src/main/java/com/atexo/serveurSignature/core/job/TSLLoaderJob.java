package com.atexo.serveurSignature.core.job;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import eu.europa.esig.dss.tsl.job.TLValidationJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class TSLLoaderJob {

	private final TLValidationJob job;

	private final ConfigurationService configurationService;

	public TSLLoaderJob(TLValidationJob job, ConfigurationService configurationService) {
		this.job = job;
		this.configurationService = configurationService;
	}

	@PostConstruct
	public void init() {
		job.onlineRefresh();
	}

	@Scheduled(fixedDelayString = "86400000", initialDelay = 0)
	public void refresh() {
		if (configurationService.isCronTlLoaderEnable()) {
			job.onlineRefresh();
		}
	}
}
