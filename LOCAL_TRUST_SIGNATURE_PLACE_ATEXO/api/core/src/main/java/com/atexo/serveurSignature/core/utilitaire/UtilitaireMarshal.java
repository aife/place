package com.atexo.serveurSignature.core.utilitaire;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public abstract class UtilitaireMarshal {
    private static final Logger logger = LoggerFactory.getLogger(UtilitaireMarshal.class);

    public static Object unmarshalXML(String string, Class clazz) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(string.getBytes()));
        } catch (JAXBException e) {
            logger.error("Impossible de transformer la chaine {} en objet {}", string, clazz.getName());
            logger.error("", e.fillInStackTrace());
            return null;
        }
    }

    public static String marshalXML(Object object, Class clazz) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Writer writer = new StringWriter();
            jaxbMarshaller.marshal(object, writer);
            return writer.toString();
        } catch (JAXBException e) {
            logger.error("Impossible de transformer l'objet {}", clazz.getName());
            logger.error("", e.fillInStackTrace());
            return null;
        }
    }

    public static String marshalJSON(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = null;
        try {
            writer = new StringWriter();
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, object);
        } catch (IOException e) {
            logger.error("", e.fillInStackTrace());
        }
        return writer.toString();
    }

    public static Object unMarshalJSON(String string, Class clazz) {
        ObjectMapper mapper = new ObjectMapper();
        Object objet = null;
        try {
            objet = mapper.readValue(string, clazz);
        } catch (IOException e) {
            logger.error("", e.fillInStackTrace());
        }
        return objet;
    }

}
