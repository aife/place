package com.atexo.serveurSignature.core.domain.service;

import com.atexo.serveurSignature.core.domain.signature.service.SignatureValidationService;
import com.atexo.serveurSignature.utilitaire.TestConfiguration;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class})
public class SignatureValidationServiceTest {
	private static Logger logger = LoggerFactory.getLogger(SignatureValidationServiceTest.class);
	private static byte[] fichierByte;
	@Autowired
	SignatureValidationService signatureValidationService;

	@BeforeClass
	public static void initialisation() throws IOException {
		InputStream fichierStream = HorodatageServiceTest.class.getClassLoader().getResourceAsStream("exemple-validation.pdf");
		fichierByte = IOUtils.toByteArray(fichierStream);
	}
}