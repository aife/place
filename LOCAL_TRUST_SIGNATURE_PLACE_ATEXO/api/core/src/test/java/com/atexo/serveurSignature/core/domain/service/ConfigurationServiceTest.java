package com.atexo.serveurSignature.core.domain.service;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import com.atexo.serveurSignature.utilitaire.TestConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class})
public class ConfigurationServiceTest {
	@Autowired
	ConfigurationService configurationsercice;

	@Test
	public void applicationParamtres() {
		Assert.assertEquals("/tmp/", configurationsercice.getApplicationDirectoryTmp());
		Assert.assertEquals("/usr/bin/openssl", configurationsercice.getApplicationPathOpenSSL());
	}


}
