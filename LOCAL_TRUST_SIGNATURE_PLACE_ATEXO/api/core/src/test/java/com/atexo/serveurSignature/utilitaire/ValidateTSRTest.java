package com.atexo.serveurSignature.utilitaire;

import com.atexo.serveurSignature.core.config.ConfigurationService;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class ValidateTSRTest {
	private static final Logger logger = LoggerFactory.getLogger(ValidateTSRTest.class);

	@Autowired
	ConfigurationService configuration;

	@Test
	public void valideTST() throws ParseException, IOException {
		List resultats = new ArrayList();
		File reponseAnnonce = File.createTempFile("reponseAnnonce", "", new File(configuration.getApplicationDirectoryTmp()));
		List<String> command = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		command.add("/usr/bin/openssl");
		command.add("ts");
		command.add("-reply");
		command.add("-data");
		command.add(reponseAnnonce.getAbsolutePath());
		File jetonTSR = File.createTempFile("jeton", "tsr", new File(configuration.getApplicationDirectoryTmp()));
		command.add("-in");
		command.add(jetonTSR.getAbsolutePath());
		command.add("-text");
		String resultat = executeCommand(command);
		int min = resultat.indexOf("Time stamp:") + "Time stamp:".length();
		int max = resultat.indexOf("Time stamp:");
		SimpleDateFormat simpleDate = new SimpleDateFormat("");
		resultats.add(resultat.contains("Status: Granted"));
		resultats.add(simpleDate.parse(resultat.substring(min, max)));
	}

	private String executeCommand(List<String> command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			logger.info(command.toString());
			ProcessBuilder pb = new ProcessBuilder(command);
			pb.redirectErrorStream(true);
			p = pb.start();

			// p.waitFor();
			String line = IOUtils.toString(p.getInputStream());
			logger.info(line);
			logger.info("----------------------");
			String line2 = IOUtils.toString(p.getErrorStream());
			logger.info("error " + line);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}

}
