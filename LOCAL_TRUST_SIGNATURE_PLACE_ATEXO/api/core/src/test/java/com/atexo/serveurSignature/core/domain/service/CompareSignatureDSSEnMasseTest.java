package com.atexo.serveurSignature.core.domain.service;

import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.domain.signature.model.InfosVerificationCertificat;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.*;


public class CompareSignatureDSSEnMasseTest {
	private static final Logger logger = LoggerFactory.getLogger(CompareSignatureDSSEnMasseTest.class);

	@Test
	public void compareDSSAtexo() {
		FileOutputStream fosKO = null;
		try {
			fosKO = new FileOutputStream(new File("/tmp/signature-comparaison.log"));
			InputStream in = new FileInputStream(new File("C:\\Users\\RME\\git\\lt_serveur_signature4\\signature-serveur\\src\\test\\resources\\signature-OK.log"));
			BufferedReader buff = new BufferedReader(new InputStreamReader(in));
			int compteur = 0;
			String line;
			while ((line = buff.readLine()) != null) {
				RestTemplate restTemplate = new RestTemplate();
				ContextSignature signature = (ContextSignature) UtilitaireMarshal.unMarshalJSON(line.toString(), ContextSignature.class);
				try {
					InfosVerificationCertificat resultatDSS = (InfosVerificationCertificat) restTemplate.postForObject("http://127.0.0.1:8080/signature-serveur/verifierSignature", signature,
							InfosVerificationCertificat.class);
					InfosVerificationCertificat resultatATEXO = (InfosVerificationCertificat) restTemplate.postForObject("http://crypto-01-prod-ovh.pcc37:8080/signature/verifierSignature", signature,
							InfosVerificationCertificat.class);
					if (!resultatDSS.equals(resultatATEXO)) {
						IOUtils.write("-----", fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write(line.toString(), fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write("-----DSS", fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write(UtilitaireMarshal.marshalJSON(resultatDSS), fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write("-----ATEXO", fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write(UtilitaireMarshal.marshalJSON(resultatATEXO), fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						IOUtils.write("-----", fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
					}
				} catch (RestClientException e) {
					try {
						e.fillInStackTrace();
						IOUtils.write(line, fosKO, "UTF-8");
						IOUtils.write("\r\n", fosKO, "UTF-8");
						fosKO.flush();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				compteur++;
				if (compteur % 50 == 0) {
					logger.info("{}", compteur);
				}
			}
			logger.info("{}", compteur);
			IOUtils.closeQuietly(buff);
			IOUtils.closeQuietly(in);

			IOUtils.closeQuietly(fosKO);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
