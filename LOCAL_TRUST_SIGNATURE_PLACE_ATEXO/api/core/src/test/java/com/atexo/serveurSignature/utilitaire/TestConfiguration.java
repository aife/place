package com.atexo.serveurSignature.utilitaire;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;


@Configuration
@ComponentScan(basePackages = {"com.atexo.serveurSignature"})
@PropertySources({ @PropertySource(value = "classpath:config-test.properties")})
public class TestConfiguration  {
    

}
