package com.atexo.serveurSignature.core.domain.service;

import com.atexo.serveurSignature.core.domain.signature.model.ContextSignature;
import com.atexo.serveurSignature.core.domain.signature.model.InfosVerificationCertificat;
import com.atexo.serveurSignature.core.utilitaire.UtilitaireMarshal;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.Collection;


public class ValidateDSSTest {
	private static final Logger logger = LoggerFactory.getLogger(ValidateDSSTest.class);
	private static final String PATH = "src/test/resources/";

	@Test
	public void compareDSSAtexo() {
		FileOutputStream fosKO = null;
		FileOutputStream fosOK = null;
		try {
			fosKO = new FileOutputStream(new File(PATH + "signature-KO.log"));
			fosOK = new FileOutputStream(new File(PATH + "signature-OK.log"));
			Collection<File> files = FileUtils.listFiles(new File(PATH), new String[]{"log"}, true);
			for (File file : files) {
				System.out.print(file.getPath() + " --> ");
				InputStream in = new FileInputStream(file);
				BufferedReader buff = new BufferedReader(new InputStreamReader(in));
				int compteur = 0;
				String line;
				while ((line = buff.readLine()) != null) {
					line = line.trim();
					if (line.equals("\"idFileSystem\" : null,")) {
						StringBuilder tmp = new StringBuilder("{");
						do {
							line = line.trim();
							tmp.append(line);
						} while (!line.equals("\"signatureType\" : \"XADES\"")
								&& !line.equals("\"signatureType\" : \"PADES\"")
								&& !line.equals("\"signatureType\" : \"CADES\"")
								&& !line.equals("\"signatureType\" : \"INCONNUE\"")
								&& (line = buff.readLine()) != null);
						tmp.append("}");
						if (line != null && (line.equals("\"signatureType\" : \"XADES\"")
								|| line.equals("\"signatureType\" : \"CADES\"")
								|| line.equals("\"signatureType\" : \"INCONNUE\""))) {
							RestTemplate restTemplate = new RestTemplate();
							ContextSignature signature = (ContextSignature) UtilitaireMarshal.unMarshalJSON(tmp.toString(), ContextSignature.class);
							try {
								InfosVerificationCertificat resultat = restTemplate.postForObject("http://atexo-crypto-local/signature/v2/verifierSignature", signature,
										InfosVerificationCertificat.class);
								IOUtils.write(tmp, fosOK, "UTF-8");
								IOUtils.write("\r\n", fosOK, "UTF-8");
							} catch (RestClientException | IOException e) {
								try {
									IOUtils.write(tmp, fosKO, "UTF-8");
									IOUtils.write("\r\n", fosKO, "UTF-8");
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}
							compteur++;
						}
					}
				}
				logger.info("{}",compteur);
				IOUtils.closeQuietly(buff);
				IOUtils.closeQuietly(in);
			}
			IOUtils.closeQuietly(fosKO);
			IOUtils.closeQuietly(fosOK);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
