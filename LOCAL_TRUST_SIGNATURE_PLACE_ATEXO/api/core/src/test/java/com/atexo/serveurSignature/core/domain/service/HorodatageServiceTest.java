package com.atexo.serveurSignature.core.domain.service;

import com.atexo.serveurSignature.core.utilitaire.AtexoPrivateKeyEntry;
import com.atexo.serveurSignature.core.utilitaire.AtexoTSPSource;
import com.atexo.serveurSignature.core.utilitaire.IOUtils;
import eu.europa.esig.dss.enumerations.EncryptionAlgorithm;
import eu.europa.esig.dss.model.x509.CertificateToken;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class HorodatageServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(HorodatageServiceTest.class);
	private byte[] jetonTsr;
	private String reponseAnnnonceXML;
	private AtexoTSPSource tsp;


	public void initialisation() throws IOException {
		InputStream reponseStrem = HorodatageServiceTest.class.getClassLoader().getResourceAsStream("reponseAnnonce_valide.xml");
		reponseAnnnonceXML = IOUtils.toString(reponseStrem);
		InputStream jetonStream = HorodatageServiceTest.class.getClassLoader().getResourceAsStream("jeton_valide.tsr");
		jetonTsr = IOUtils.toByteArray(jetonStream);
		FileInputStream is;
		try {
			is = new FileInputStream("/tools/client-truststore.jks");

			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(is, "123456".toCharArray());
			String alias = "timestamping";

			Key key = keystore.getKey(alias, "123456".toCharArray());
			if (key instanceof PrivateKey) {
				// Get certificate of public key
				Certificate cert = keystore.getCertificate(alias);

				// Get public key
				PublicKey publicKey = cert.getPublicKey();
				// Return a key pair
				new KeyPair(publicKey, (PrivateKey) key);
				Certificate[] chain = keystore.getCertificateChain(alias);
				tsp = new AtexoTSPSource(new AtexoPrivateKeyEntry(EncryptionAlgorithm.RSA, new CertificateToken((X509Certificate) chain[0]), (PrivateKey) key));
			}
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void openssl() throws IOException {
		initialisation();
		FileInputStream fis = new FileInputStream(new File("C:\\Users\\RME\\git\\lt_serveur_crypto\\web\\src\\test\\resources\\jeton_1.tsr"));
		try {
			final TimeStampResponse timeStampResponse = new TimeStampResponse(fis);
			logger.info(timeStampResponse.getStatusString());
		} catch (TSPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

    /*    @Test

    public void TSRJeton() throws IOException {
        initialisation();
        logger.info("");
        byte[] bytes = null;
        try {
            byte[] digest = DSSUtils.digest(DigestAlgorithm.SHA256,
                    "Hello sqddsqdsqdsqdsqdsqdsqdsqdsqdsqdsqdsqdsqcxvdsqdsqdsqdcvcxvxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsqdsqdsqdsqdsqworld".getBytes());

            TimeStampToken token = tsp.getTimeStampResponse(DigestAlgorithm.SHA256, digest);
            // Handle the TSA response
            final TimeStampResponse timeStampResponse = new TimeStampResponse(token.getEncoded());

            String statusString = timeStampResponse.getStatusString();
            if (statusString != null) {
                logger.info("Status: " + statusString);
            }
            timeStampResponse.getEncoded();
        } catch (IOException | TSPException e) {
            logger.error("Impossible de recuperer le reponseAnnonce dans le cadre de l'horodatage ", e.fillInStackTrace());
            // jmxService.addHorodatageErreur();
        }
        assertNotNull(bytes);
    }*/
}
