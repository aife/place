package com.atexo.serveurCryptographique.jnlp;

import com.atexo.serveurCryptographique.utilitaire.SignatureServiceUtil;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore.PasswordProtection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValidateCertificatTest {
	private static File fichier;
	private static DSSPrivateKeyEntry privateKeyEntry;
	private static String PARENT = "src/test/resources";

	@BeforeClass
	public static void initialisation() throws IOException {
		InputStream in = ValidateCertificatTest.class.getClassLoader().getResourceAsStream("atexo_20131228.p12");
		Pkcs12SignatureToken token = new Pkcs12SignatureToken(in, new PasswordProtection("convergence".toCharArray()));
		privateKeyEntry = token.getKeys().get(0);
		InputStream fichierInputStream = ValidateCertificatTest.class.getClassLoader().getResourceAsStream("exemple.pdf");
		fichier = new File(PARENT, "exemple-test.pdf");
		FileUtils.copyToFile(fichierInputStream, fichier);
	}

	@AfterClass
	public static void nettoyage() {
		fichier.delete();
	}

	@Test
	public void signatureXadesTest() throws IOException {
		DSSDocument documentToSign = new FileDocument(fichier);
		DSSDocument document = SignatureServiceUtil.getSignatureXades(ApplicationSignature.getCertificateVerifier(), privateKeyEntry, documentToSign, DigestAlgorithm.SHA256, DigestAlgorithm.SHA1, null);
		Assert.notNull(document);
		Assert.notNull(document.openStream());
		InputStream in = document.openStream();
		FileUtils.copyToFile(document.openStream(), new File(PARENT, "exemple-xades.xml"));
		IOUtils.closeQuietly(in);
	}

	@Test
	public void signatureCadesTest() throws DSSException, IOException {
		DSSDocument documentToSign = new FileDocument(fichier);
		DSSDocument document = SignatureServiceUtil.getSignatureCades(ApplicationSignature.getCertificateVerifier(), privateKeyEntry, documentToSign, DigestAlgorithm.SHA256, DigestAlgorithm.SHA1, null);
		Assert.notNull(document);
		Assert.notNull(document.openStream());
		InputStream in = document.openStream();
		FileUtils.copyToFile(document.openStream(), new File(PARENT, "exemple-cades.p7s"));
		IOUtils.closeQuietly(in);
	}

	@Test
	public void signaturePadesTest() throws DSSException, IOException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");

		DSSDocument documentToSign = new FileDocument(fichier);
		DSSDocument document = SignatureServiceUtil.getSignaturePades(ApplicationSignature.getCertificateVerifier(), privateKeyEntry, documentToSign, DigestAlgorithm.SHA256, DigestAlgorithm.SHA1,
				null, dateFormat.format(new Date()));
		Assert.notNull(document);
		Assert.notNull(document.openStream());
		InputStream in = document.openStream();
		FileUtils.copyToFile(in, new File(PARENT, "exemple-pades.pdf"));
		IOUtils.closeQuietly(in);
	}

}
