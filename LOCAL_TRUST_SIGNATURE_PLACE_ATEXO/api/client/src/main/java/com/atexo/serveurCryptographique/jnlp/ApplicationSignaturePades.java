package com.atexo.serveurCryptographique.jnlp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.PrivilegedActionException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.serveurCryptographique.utilitaire.HttpPOSTRequest;
import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.ManipulationCertificatException;
import com.atexo.serveurCryptographique.utilitaire.SignatureServiceUtil;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.Util;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateListener;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinHandler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
//import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateListener;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12Handler;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;

import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.MSCAPISignatureToken;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ApplicationSignaturePades extends Application
		implements MagasinCertificateListener, Pkcs12CertificateListener {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationSignaturePades.class);
	private TypeOs typeOs;
	private TypeProvider typeProvider;
	private static List<URL> inputFileUrl = new ArrayList<URL>();
	private static List<URL> outputFileUrl = new ArrayList<URL>();
	private static URL callbackUrl;
	private static URL dateUrl;
	private final Alert alert = new Alert(Alert.AlertType.INFORMATION);

	public static void main(String[] args) {
		int index = 0;
		if (args.length == 0) {
			logger.error("configuration URL serveur validaton absente");
		} else {
			try {
				callbackUrl = new URL(args[index]);
				dateUrl = new URL(callbackUrl.getProtocol() + "://" + callbackUrl.getHost() + "/signature/serverDate");
				for (int i = ++index; i < args.length; i += 2) {
					inputFileUrl.add(new URL(args[i]));
					outputFileUrl.add(new URL(args[i + 1]));
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		launch(args);
	}

	public void start(final Stage stage) throws Exception {
		stage.setResizable(false);
		this.typeOs = Util.determinerOs();
		this.typeProvider = Util.determinerProvider();
		alert.initStyle(StageStyle.UTILITY);
		alert.setTitle("Signature (@version@)");
		selectionnerCertificat(ApplicationSignaturePades.this.typeProvider);
	}

	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

	private static Pkcs11LibsType pkcs11LibsType;

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11LibsType = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			logger.error("", e.fillInStackTrace());
		}
	}

	protected void selectionnerCertificat(TypeProvider provider) {
		if (provider != null) {
			switch (provider) {
			case APPLE:
			case PKCS11:
			case MSCAPI:
				MagasinCertificateUiService.getInstance(pkcs11LibsType).initUi(this, typeOs, provider, true, true);
				break;
			case PKCS12:
				Pkcs12CertificateUiService.getInstance().initUi(this, typeOs, provider, true, true);
				break;
			}
		}
	}

	@Override
	public void onSelection(MagasinCertificateEvent event) throws ManipulationCertificatException {
		KeyPair keyPair = null;

		logger.info("onSelect => Provider d'accès au  Magasin : " + event.getCertificateItem().getTypeProvider());
		String alias = event.getCertificateItem().getId();
		logger.info("L'alias selectionné est : " + alias);
		boolean smartCard = event.getCertificateItem().isSmartCard();
		logger.info("SmartCard : " + smartCard);

		if (typeOs != TypeOs.Windows && smartCard) {
			keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
		} else {
			keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, alias);
		}
		try {
			onSelection(keyPair);
		} catch (DSSException e) {
			logger.error("Erreur de siganture", e.fillInStackTrace());
		} catch (IOException e) {
			logger.error("Erreur de siganture", e.fillInStackTrace());
		}
	}

	@Override
	public void onSelection(Pkcs12CertificateEvent event) throws ManipulationCertificatException {
		String cheminFichierP12 = event.getCheminFichierP12();
		String motDePasseFichierP12 = event.getMotDePasseFichierP12();
		KeyPair keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);
		try {
			onSelection(keyPair);
		} catch (DSSException | IOException e) {
			logger.error("Erreur de siganture", e.fillInStackTrace());
		}
	}

	protected void onSelection(KeyPair keyPair) throws DSSException, IOException {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				alert.setHeaderText("Fichier(s) en cours de signature... (0 / " + inputFileUrl.size() + ")");
				alert.show();

			}
		});
		for (int i = 0; i < inputFileUrl.size(); i++) {
			final int count = i + 1;
			MSCAPISignatureToken token = new MSCAPISignatureToken();
			DSSDocument documentToSign = new InMemoryDocument(inputFileUrl.get(i).openStream());
			DSSDocument signedDocument = null;
			DSSPrivateKeyEntry privateKeyEntry = getPrivateKey(keyPair, token.getKeys());
			signedDocument = SignatureServiceUtil.getSignaturePades(new CommonCertificateVerifier(), privateKeyEntry,
					documentToSign, DigestAlgorithm.SHA256, DigestAlgorithm.SHA1, null, getDate());
			sendPades(signedDocument, outputFileUrl.get(i));
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					alert.setHeaderText(
							"Fichier(s) en cours de signature... (" + count + " / " + inputFileUrl.size() + ")");
					alert.show();

				}
			});
		}
		if (!checkOK()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					alert.setAlertType(AlertType.ERROR);
					alert.setHeaderText("Erreur Envoie fichier.");

				}
			});
		} else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					alert.setHeaderText(inputFileUrl.size() + " fichier(s) signé(s) et envoyé(s).");

				}
			});
		}
	}

	private boolean sendPades(DSSDocument signedDocument, URL url) throws IOException {
		URLConnection con = url.openConnection();
		HttpURLConnection http = (HttpURLConnection) con;
		http.setRequestMethod("POST");
		http.setDoOutput(true);
		http.setUseCaches(false);
		http.setDefaultUseCaches(false);
		http.setChunkedStreamingMode(4096);
		http.setRequestProperty("Content-Type", "application/octet-stream; charset=UTF-8");
		InputStream is = signedDocument.openStream();
		http.setRequestProperty("Content-Length", String.valueOf(is.available()));
		http.setRequestProperty("Transfer-Encoding", "chunked");
		http.connect();
		OutputStream os = http.getOutputStream();
		IOUtils.copy(is, os);
		IOUtils.closeQuietly(os);
		IOUtils.closeQuietly(is);
		int code = http.getResponseCode();
		http.disconnect();
		return code == 200;
	}

	private boolean checkOK() {
		try {
			logger.info(callbackUrl.toString());
			HttpURLConnection con = (HttpURLConnection) callbackUrl.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			return responseCode == 200;
		} catch (IOException e) {
			logger.error("Erreur envoi signature", e.fillInStackTrace());
		}
		return false;
	}

	private String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz");
		try {
			logger.info(dateUrl.toString());
			HttpURLConnection con = (HttpURLConnection) dateUrl.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				String reponse = IOUtils.toString(con.getInputStream());
				logger.info(reponse);
				return dateFormat.format(new Date(Long.valueOf(reponse)));
			} else {
				return "NC";
			}
		} catch (IOException e) {
			logger.error("Erreur envoi signature", e.fillInStackTrace());
			return "NC";
		}
	}

	private DSSPrivateKeyEntry getPrivateKey(KeyPair keyPair, List<DSSPrivateKeyEntry> privateKey) {
		if (keyPair == null) {
			logger.error("keypair non disponible");
			return null;
		}
		for (DSSPrivateKeyEntry dssPrivateKeyEntry : privateKey) {
			if (dssPrivateKeyEntry != null) {
				logger.info("" + dssPrivateKeyEntry);
				if (dssPrivateKeyEntry.getCertificate() != null
						&& dssPrivateKeyEntry.getCertificate().getCertificate() != null
						&& dssPrivateKeyEntry.getCertificate().getCertificate().equals(keyPair.getCertificate())) {
					return dssPrivateKeyEntry;
				}
			} else {
				logger.info("dssPrivateKeyEntry est null");
			}
		}
		logger.error("privateKey non disponible {}", keyPair.toString());
		return null;
	}
}
