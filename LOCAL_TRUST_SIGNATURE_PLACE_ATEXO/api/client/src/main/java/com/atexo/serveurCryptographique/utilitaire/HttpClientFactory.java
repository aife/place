package com.atexo.serveurCryptographique.utilitaire;


import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class HttpClientFactory {

	private int RETRY_COUNT = 2;

	private int TIME_OUT_SECONDS = 11 * 60 * 1000;

	public CloseableHttpClient createHtpClient() {
		HttpClientBuilder httpClientBuilder = HttpClientBuilder
				.create()
				.setDefaultRequestConfig(RequestConfig.custom().setConnectionRequestTimeout(TIME_OUT_SECONDS).setSocketTimeout(TIME_OUT_SECONDS).setConnectTimeout(TIME_OUT_SECONDS).build())
				.setRetryHandler((exception, executionCount, context) -> executionCount <= RETRY_COUNT)
				.setMaxConnTotal(200)
				.setMaxConnPerRoute(100);
		try {
			SSLContextBuilder builder = SSLContexts.custom();
			builder.loadTrustMaterial(null, (TrustStrategy) (chain, authType) -> true);

			SSLContext sslContext = builder.build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					sslContext, (s, sslSession) -> true);
			Registry<ConnectionSocketFactory> sfr = RegistryBuilder
					.<ConnectionSocketFactory>create()
					.register("http",
							PlainConnectionSocketFactory.getSocketFactory())
					.register(
							"https",
							sslsf).build();
			httpClientBuilder.setConnectionManager(new PoolingHttpClientConnectionManager(sfr));

		} catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
			e.printStackTrace();
		}

		return httpClientBuilder.build();

	}

}
