package com.atexo.serveurCryptographique.jnlp;

import com.atexo.serveurCryptographique.utilitaire.AppIdEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class ApplicationProxy {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationProxy.class);

    public static void main(String[] args) {
        AppIdEnum appId = AppIdEnum.valueOf(args[0]);
        try {
            if (appId == AppIdEnum.APPLICATION_SIGNATURE) {
                logger.info("Launch ApplicationSignature");
                ApplicationSignature.main(args);
            } else if (appId == AppIdEnum.APPLICATION_SIGNATURE_HASH) {
                logger.info("Launch ApplicationSignatureHash avec les parametres ---> {}", String.join(" ", args));
                ApplicationSignatureHash.main(Arrays.copyOfRange(args, 1, args.length));
            } else if (appId == AppIdEnum.APPLICATION_SIGNATURE_PADES) {
                logger.info("Launch ApplicationSignaturePades avec les parametres ----> {}", String.join(" ", args));
                ApplicationSignaturePades.main(Arrays.copyOfRange(args, 1, args.length));
            } else if (appId == AppIdEnum.APPLICATION_SIGNATURE_SWING) {
                logger.info("Launch ApplicationSignatureSwing avec les parametres ----> {}", String.join(" ", args));
                ApplicationSignatureSwing.main(Arrays.copyOfRange(args, 1, args.length));
            }else if (appId == AppIdEnum.APPLICATION_TEST_CONFIGURATION) {
                logger.info("Launch ApplicationTestConfiguration avec les parametres ----> {}", String.join(" ", args));
                ApplicationTestConfiguration.main(Arrays.copyOfRange(args, 1, args.length));
            } else {
                logger.info("Aucune application trouvée -----> {}", String.join(" ", args));
            }
        } catch (Exception e) {
            logger.error("Errur lors l'exécution de l'application : {}", e.getStackTrace());
        }
    }


}
