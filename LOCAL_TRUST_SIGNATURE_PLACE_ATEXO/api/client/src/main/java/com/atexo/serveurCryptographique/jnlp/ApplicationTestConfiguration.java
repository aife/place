package com.atexo.serveurCryptographique.jnlp;

import com.atexo.serveurCryptographique.utilitaire.CertificatItem;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.Util;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.InputStream;
import java.util.List;

public class ApplicationTestConfiguration extends Application {
	private static final String MESSAGE_ECHEC = "La configuration de votre poste ne respecte pas certains pré-requis techniques nécessaires sur la plateforme de dématérialisation.\nVeuillez vous conformez aux pré-requis techniques de la plateforme.";
	private static final String MESSAGE_OK = "La configuration de votre poste respecte bien les pré-requis techniques nécessaires sur la plateforme de dématérialisation.";
	private TypeOs typeOs;
	private TypeProvider typeProvider;
	private Stage primaryStage;
	private Alert alert;
	private Label sequestreLabel;
	private Label accesMPELabel;
	private Label sequestreStatutLabel;
	private Label accesMPEStatutLabel;
	private Label versionJavaLabel;
	private Label versionOsLabel;
	private ImageView okGrand = new ImageView(this.getClass().getResource("/images/ok_grand.png").toString());
	private ImageView echecGrand = new ImageView(this.getClass().getResource("/images/echec_grand.png").toString());
	private static Pkcs11LibsType pkcs11Libs = null;
	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11Libs = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Test de configuration de mon poste (@version@)");
		this.typeOs = Util.determinerOs();
		this.typeProvider = Util.determinerProvider();

		alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Test de configuration de mon poste");
		alert.setHeaderText("\nVérification en cours...\n");

		VBox expContent = new VBox();

		expContent.setMinWidth(500);
		expContent.setMinHeight(150);
		alert.getDialogPane().setContent(expContent);
		versionJavaLabel = new Label("- JAVA version " + System.getProperty("java.version") + ": ");
		Label statutJAVA = null;
		if (System.getProperty("java.version").startsWith("1.8") || System.getProperty("java.version").startsWith("1.9")) {
			statutJAVA = new Label("", new ImageView(this.getClass().getResource("/images/ok_petit.png").toString()));
		} else {
			statutJAVA = new Label("", new ImageView(this.getClass().getResource("/images/echec_petit.png").toString()));
		}

		versionOsLabel = new Label("- Système d'exploitation " + System.getProperty("os.name") + ": ");
		Label statutOS = null;
		if (System.getProperty("java.version").startsWith("1.8") || System.getProperty("java.version").startsWith("1.9")) {
			statutOS = new Label("", new ImageView(this.getClass().getResource("/images/ok_petit.png").toString()));
		} else {
			statutOS = new Label("", new ImageView(this.getClass().getResource("/images/echec_petit.png").toString()));
		}
		BorderPane bp = new BorderPane();
		bp.setPadding(new Insets(5, 30, 5, 10));
		bp.setLeft(versionJavaLabel);
		bp.setRight(statutJAVA);
		expContent.getChildren().add(bp);
		bp = new BorderPane();
		bp.setPadding(new Insets(5, 30, 5, 10));
		bp.setLeft(versionOsLabel);
		bp.setRight(statutOS);
		expContent.getChildren().add(bp);
		bp = new BorderPane();
		bp.setPadding(new Insets(5, 30, 5, 10));
		sequestreLabel = new Label("- Accès au magasin de certificats:");
		bp.setLeft(sequestreLabel);
		sequestreStatutLabel = new Label("Test en cours");
		bp.setRight(sequestreStatutLabel);
		expContent.getChildren().add(bp);
		bp = new BorderPane();
		bp.setPadding(new Insets(5, 30, 5, 10));
		accesMPELabel = new Label("- Communication avec le serveur:");
		bp.setLeft(accesMPELabel);
		accesMPEStatutLabel = new Label("Test en cours");
		bp.setRight(accesMPEStatutLabel);
		expContent.getChildren().add(bp);
		alert.show();

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				sequestreStatutLabel.setText("");
				if (sequestreDisponible(ApplicationTestConfiguration.this.typeProvider)) {
					sequestreStatutLabel.setGraphic(new ImageView(this.getClass().getResource("/images/ok_petit.png").toString()));
					alert.setHeaderText(MESSAGE_OK);
					alert.setGraphic(okGrand);
				} else {
					sequestreStatutLabel.setGraphic(new ImageView(this.getClass().getResource("/images/echec_petit.png").toString()));
					alert.setGraphic(echecGrand);
					alert.setHeaderText(MESSAGE_ECHEC);
				}
			}
		});

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				accesMPEStatutLabel.setText("");
				accesMPEStatutLabel.setGraphic(new ImageView(this.getClass().getResource("/images/ok_petit.png").toString()));
			}
		});

		alert.setGraphic(new ImageView(this.getClass().getResource("/images/ok_grand.png").toString()));
	}

	protected boolean sequestreDisponible(TypeProvider provider) {
		System.out.println("Type de provider : " + provider);
		List<CertificatItem> certificats = null;
		if (provider != null) {
			switch (provider) {
				case APPLE:
				case PKCS11:
				case MSCAPI:
					certificats = MagasinCertificateUiService.getInstance(pkcs11Libs).getCertificat(provider, typeOs, true);
					break;
			}
		}
		return certificats != null;
	}

}
