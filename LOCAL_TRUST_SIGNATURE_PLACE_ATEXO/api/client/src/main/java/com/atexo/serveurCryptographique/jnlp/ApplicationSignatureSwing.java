package com.atexo.serveurCryptographique.jnlp;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import com.atexo.serveurCryptographique.utilitaire.*;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateListener;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateUiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinHandler;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateListener;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12Handler;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;

import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.http.proxy.ProxyConfig;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;

public class ApplicationSignatureSwing extends JFrame implements MagasinCertificateListener, Pkcs12CertificateListener {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationSignatureSwing.class);
    private static TypeOs typeOs;
    private static TypeProvider typeProvider;
     private static String lotlRootSchemeInfoUri = "https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl.html";
    private static String lotlUrl = "https://ec.europa.eu/tools/lotl/eu-lotl.xml";
    private static String ojUrl = "https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.C_.2019.276.01.0001.01.ENG";
    private static String plateforme = "INCONNUE";
    private static boolean activeHorodatageSignature;    
    private static boolean activeHorodatagePadesCades;
    private static boolean typeChiffrementActive = false;
    private static CertificateVerifier certificateVerifier = null;
    private File lastDirectory;
    private List<FileItem> filesToSign = new ArrayList<FileItem>();
    private List<FileItem> files = new ArrayList<FileItem>();
    private SignatureTable signatureTable;
    private JDialog dialog;
    private static DigestAlgorithm shaSignature = DigestAlgorithm.SHA256;
    private static DigestAlgorithm shaHorodatage = DigestAlgorithm.SHA256;
    private static OnlineTSPSource tpsSource;
    private static URL logURL;
    private static CommonsDataLoader commonsHttpDataLoader;
    private static Pkcs11LibsType pkcs11Libs = null;
    private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

    static {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
        try {
            byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
            pkcs11Libs = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private DefaultTableModel model = new DefaultTableModel(null, SignatureTable.colNames) {

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == SignatureTable.BOOLEAN_COL) {
                return Boolean.class;
            } else {
                return String.class;
            }
        }
    };

    public ApplicationSignatureSwing() {
        signatureTable = new SignatureTable(model, filesToSign, typeChiffrementActive);
        add(new JScrollPane(signatureTable), BorderLayout.CENTER);
        add(buttonPanel(), BorderLayout.SOUTH);
        setTitle("Utilitaire de signature électronique (@version@)");
        setPreferredSize(new Dimension(900, 500));
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("configuration URL serveur validaton absente");
        } else {
            if (args.length == 8) {
                plateforme = args[7];
            } else {
                plateforme="DEFAULT";
            }
            tpsSource = new OnlineTSPSource(args[0] + "tokenRFC3161?plateforme=" + plateforme);
            try {
                logURL = new URL(args[0] + "/logSignature");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        activeHorodatageSignature = args[1] != null && args[1].equalsIgnoreCase("true");
        if (args[2] != null && args[2].equals("SHA1")) {
            shaSignature = DigestAlgorithm.SHA1;
        }
        if (args[3] != null && args[3].equals("SHA1")) {
            shaHorodatage = DigestAlgorithm.SHA1;
        }        
        if (args[4] != null) {
            lotlRootSchemeInfoUri = args[4];
        }
        if (args[5] != null) {
            lotlUrl = args[5];
            try {
                commonsHttpDataLoader = new CommonsDataLoader();
                ProxyConfig proxy = new ProxyConfig();
                if (System.getProperty("http.proxyHost") != null) {
                    ProxyProperties prop = new ProxyProperties();
                    prop.setHost(System.getProperty("http.proxyHost"));
                    prop.setPort(Integer.parseInt(System.getProperty("http.proxyPort")));
                    proxy.setHttpProperties(prop);
                    proxy.setHttpsProperties(prop);
                    System.out.println("proxy --> " + prop.getHost() + ":" + prop.getPort());
                    commonsHttpDataLoader.setProxyConfig(proxy);
                }
                if (System.getProperty("https.proxyHost") != null) {
                    ProxyProperties prop = new ProxyProperties();
                    prop.setHost(System.getProperty("https.proxyHost"));
                    prop.setPort(Integer.parseInt(System.getProperty("https.proxyPort")));
                    proxy.setHttpProperties(prop);
                    proxy.setHttpsProperties(prop);
                    System.out.println(prop.getHost() + ":" + prop.getPort());                    
                    commonsHttpDataLoader.setProxyConfig(proxy);
                }                
                commonsHttpDataLoader.setTimeoutConnection(5000);
                System.out.println(lotlUrl);
//                commonsHttpDataLoader.get(lotlUrl);
                System.out.println("URL OK --> " + lotlUrl);
            } catch (Exception e) {
                System.out.println("TSL non accessible ");
                lotlUrl = null;
                e.printStackTrace();
            }
        }
        if (args[6] != null) {
            ojUrl = args[6];
        }
        if (args.length == 8) {
            plateforme = args[7];
        } else {
            System.out.println("les parametres plateforme ne sont pas presents originePlateforme, origineOrganisme, origineItem, origineContexteMetier");
        }
        typeOs = Util.determinerOs();
        typeProvider = Util.determinerProvider();
        new ApplicationSignatureSwing();
    }

    private JPanel buttonPanel() {
        FlowLayout flowLayout = new FlowLayout();
        JPanel buttonBar = new JPanel(flowLayout);

        JButton signerButton = new JButton("Signer les fichiers séléctionnés");
        signerButton.setEnabled(false);
        signerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean padesOK = true;
                filesToSign.clear();
                for (int i = 0; i < model.getRowCount(); i++) {
                    FileItem fileItem = null;
                    if (((Boolean) model.getValueAt(i, 3)) && !((String) model.getValueAt(i, 0)).endsWith("pdf") && ((String) model.getValueAt(i, 2)).equalsIgnoreCase("PADES")) {
                        padesOK = false;
                        break;
                    }
                    if ((Boolean) model.getValueAt(i, 3)) {
                        fileItem = files.get(i);
                        fileItem.setSignatureType((String) model.getValueAt(i, 2));
                        fileItem.setSigner((Boolean) model.getValueAt(i, 3));
                        filesToSign.add(fileItem);
                    }
                }
                if (!padesOK) {
                    JOptionPane.showMessageDialog(signatureTable, "Pour certains fichiers le format de signature sélectionné n'est pas applicable.\n Veuillez-vous assurer que le format PADES est seulement sélectionné pour des fichiers PDF.");
                } else if (!filesToSign.isEmpty()) {
                    selectionnerCertificat(typeProvider);
                }
            }
        });

        JButton ajouterFichierButton = new JButton("Ajouter un fichier");

        ajouterFichierButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(lastDirectory);
                fc.setMultiSelectionEnabled(true);
                int returnVal = fc.showOpenDialog(buttonBar);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File[] tmp = fc.getSelectedFiles();
                    if (files != null) {
                        signerButton.setEnabled(true);
                        for (File file : tmp) {
                            if (file != null) {
                                FileItem fileItem = new FileItem(file);
                                lastDirectory = file.getParentFile();
                                signatureTable.addRow(fileItem);
                                files.add(fileItem);
                            }
                        }
                    }

                }

            }
        });

        JButton viderButton = new JButton("Vider la liste");

        viderButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                signerButton.setEnabled(false);
                signatureTable.clean();
                filesToSign.clear();
                files.clear();
            }
        });

        buttonBar.add(ajouterFichierButton);
        buttonBar.add(viderButton);
        buttonBar.add(signerButton);
        return buttonBar;
    }
    protected void selectionnerCertificat(TypeProvider provider) {
        System.out.println("Type de provider : " + provider);
        if (provider != null) {
            switch (provider) {
                case APPLE:
                case PKCS11:
                case MSCAPI:
                    MagasinCertificateUiService.getInstance(pkcs11Libs).initUi(this,typeOs,typeProvider,true,true);
                    break;
                case PKCS12:
                    Pkcs12CertificateUiService.getInstance().initUi(this, typeOs, provider, true, true);
                    break;
            }
        }
    }

    @Override
    public void onSelection(MagasinCertificateEvent event) throws ManipulationCertificatException {
        KeyPair keyPair = null;

        System.out.println("onSelect => Provider d'accès au  Magasin : " + event.getCertificateItem().getTypeProvider());
        String alias = event.getCertificateItem().getId();
        System.out.println("L'alias selectionné est : " + alias);
        boolean smartCard = event.getCertificateItem().isSmartCard();
        System.out.println("SmartCard : " + smartCard);

        if (typeOs == TypeOs.Windows || !smartCard) {
            keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, alias);
        } else {
            keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
        }
        onSelection(keyPair);
    }

    protected void onSelection(KeyPair keyPair) {
        SwingWorker<Void, Void> mySwingWorker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                if (certificateVerifier == null) {
                    certificateVerifier = getCertificateVerifier();
                }
                for (FileItem fileToSign : filesToSign) {
                    String signingFile = null;
                    long start = System.currentTimeMillis();
                    X509Certificate[] x509 = null;
                    try {
                        int indexFichier = FileUtil.getProchaineIndexFichierSignatureXML(fileToSign.getFile().getPath());
                        Date dateSignature = new Date();
                        String formatDateNomFichier = Util.formaterDate(dateSignature, Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);
                        x509 = new X509Certificate[1];
                        x509[0] = keyPair.getCertificate();
                        KeyStore.PrivateKeyEntry privateKey = new KeyStore.PrivateKeyEntry(keyPair.getPrivateKey(), x509);
                        DSSPrivateKeyEntry privateKeyEntry = new KSPrivateKeyEntry(privateKey.toString(), privateKey);
                        DSSDocument documentToSign = new FileDocument(fileToSign.getFile());
                        DSSDocument signedDocument = null;
                        InputStream docInputStream = null;
                        if (fileToSign.getSignatureType().getValue().trim().equalsIgnoreCase("PADES")) {
                            if (activeHorodatagePadesCades) {
                                signedDocument = SignatureServiceUtil.getSignaturePades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource, null);
                            } else {
                                signedDocument = SignatureServiceUtil.getSignaturePades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, null, null);
                            }
                            signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignaturePDF(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
                            docInputStream = signedDocument.openStream();
                        } else if (fileToSign.getSignatureType().getValue().trim().equalsIgnoreCase("XADES")) {
                            signedDocument = SignatureServiceUtil.getSignatureXades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource);
                            try {
                                String resultat = new String(IOUtils.toByteArray(signedDocument.openStream()), "UTF-8");
                                docInputStream = new ByteArrayInputStream(resultat.getBytes());

                            } catch (Exception e) {
                                logger.error("probleme avec le serveur de validaton. enrichissement impossible", e.fillInStackTrace());
                            }
                            signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignatureXML(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
                        } else if (fileToSign.getSignatureType().getValue().trim().equalsIgnoreCase("CADES")) {
                            if (activeHorodatagePadesCades) {
                                signedDocument = SignatureServiceUtil.getSignatureCades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource);
                            } else {
                                signedDocument = SignatureServiceUtil.getSignatureCades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, null);
                            }
                            signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignatureCades(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
                            docInputStream = signedDocument.openStream();
                        }
                        OutputStream os = null;
                        try {
                            os = new FileOutputStream(signingFile);
                            if (docInputStream == null) {
                                docInputStream = signedDocument.openStream();
                            }
                            IOUtils.copy(docInputStream, os);
                        } catch (IOException e) {
                            logger.error("", e.fillInStackTrace());
                        } finally {
                            try {
                                os.close();
                                docInputStream.close();
                            } catch (IOException e) {
                            }
                        }
                        fileToSign.setSignatureInfo(signingFile);
                    } catch (Exception e) {
                        signingFile = "L'application a détecté un problème sur le fichier. Veuillez vous assurer qu'il est lisible";
                        logger.error("impossible de signer le fichier : " + fileToSign.getFilename(), e.fillInStackTrace());

                    }
                    signatureTable.setValueAt(signingFile, files.indexOf(fileToSign), 1);
                    signatureTable.repaint();
                    start = System.currentTimeMillis() - start;
                    envoiLog(x509[0], fileToSign.getSignatureType().getValue().trim(), fileToSign, (int) start);
                }
                return null;
            }
        };

        mySwingWorker.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("state")) {
                    if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
                        dialog.dispose();
                        JOptionPane.showMessageDialog(signatureTable, "Fichier(s) signé(s).");
                    } else if (evt.getNewValue() == SwingWorker.StateValue.STARTED) {
                        JProgressBar progressBar = new JProgressBar();
                        progressBar.setIndeterminate(true);
                        JPanel panel = new JPanel(new BorderLayout());
                        panel.add(progressBar, BorderLayout.PAGE_START);
                        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                        JLabel label = new JLabel("Veuillez patienter...", JLabel.CENTER);
                        label.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
                        panel.add(label, BorderLayout.CENTER);
                        dialog = new JDialog(null, "Veuillez patienter...", ModalityType.APPLICATION_MODAL);
                        dialog.add(panel);
                        dialog.setPreferredSize(new Dimension(300, 75));
                        dialog.pack();
                        dialog.setLocationRelativeTo(null);
                        dialog.setVisible(true);
                    }
                }
            }
        });
        mySwingWorker.execute();

    }

    @Override
    public void onSelection(Pkcs12CertificateEvent event) throws ManipulationCertificatException {
        String cheminFichierP12 = event.getCheminFichierP12();
        String motDePasseFichierP12 = event.getMotDePasseFichierP12();
        KeyPair keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);
        onSelection(keyPair);
    }

    private void envoiLog(X509Certificate x509, String typeSignature, FileItem fileToSign, int tempsRequete) {
        HttpPOSTRequest request = new HttpPOSTRequest();
        try {
            request.setUrl(logURL.toString());
            request.ajouterParametre("certificat", Base64.getEncoder().encodeToString(x509.getEncoded()));
            request.ajouterParametre("typeSignature", typeSignature);
            request.ajouterParametre("nomFichier", fileToSign.getFilename());
            request.ajouterParametre("plateforme", plateforme);
            request.ajouterParametre("tempsRequete", String.valueOf(tempsRequete));
            request.envoyer();
        } catch (Exception e) {
            logger.warn("communication impossible avec le serveur", e.fillInStackTrace());
        }

    }

    private static CertificateVerifier getCertificateVerifier() {
        CertificateVerifier certificateVerifier = new CommonCertificateVerifier();
//        if (lotlRootSchemeInfoUri != null) {
//            KeyStoreCertificateSource keyStoreCertificateSource;
//            keyStoreCertificateSource = new KeyStoreCertificateSource(ClassLoader.class.getResourceAsStream("user_a_rsa.p12"), "PKCS12", "password");
//
//            TrustedListsCertificateSource tslCertificateSource = new TrustedListsCertificateSource();
//
//            TSLRepository tslRepository = new TSLRepository();
//            tslRepository.setTrustedListsCertificateSource(tslCertificateSource);
//
//            TLValidationJob job = new TLValidationJob();
//            job.setDataLoader(commonsHttpDataLoader);
//            job.setOjContentKeyStore(keyStoreCertificateSource);
//            //job.setLotlRootSchemeInfoUri(lotlRootSchemeInfoUri);
//            job.setLotlUrl(lotlUrl);
//            job.setOjUrl(ojUrl);
//            job.setLotlCode("EU");
//            job.setRepository(tslRepository);
//            job.refresh();
//            certificateVerifier.setTrustedCertSource(tslCertificateSource);
//
//            OnlineCRLSource onlineCRLSource = new OnlineCRLSource();
//            onlineCRLSource.setDataLoader(commonsHttpDataLoader);
//            certificateVerifier.setCrlSource(onlineCRLSource);
//
//            OnlineOCSPSource onlineOCSPSource = new OnlineOCSPSource();
//            onlineOCSPSource.setDataLoader(commonsHttpDataLoader);
//            certificateVerifier.setOcspSource(onlineOCSPSource);
//        }
        return certificateVerifier;
    }
    
//    protected CertificateVerifier getCompleteCertificateVerifier() {
//        CertificateVerifier cv = new CommonCertificateVerifier();
//        cv.setDataLoader(getFileCacheDataLoader());
//        cv.setCrlSource(cacheCRLSource());
//        cv.setOcspSource(cacheOCSPSource());
//        cv.setTrustedCertSource(getTrustedCertificateSource());
//        return cv;
//    }
}
