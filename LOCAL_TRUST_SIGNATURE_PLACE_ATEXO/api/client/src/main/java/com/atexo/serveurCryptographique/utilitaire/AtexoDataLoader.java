package com.atexo.serveurCryptographique.utilitaire;

import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.service.http.commons.CommonsDataLoader;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import eu.europa.esig.dss.spi.client.http.Protocol;
import eu.europa.esig.dss.utils.Utils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public class AtexoDataLoader extends CommonsDataLoader {
	public static final String AUTHORIZATION = "Authorization";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final long serialVersionUID = 1L;
	private String bearer;
	private static final Logger logger = LoggerFactory.getLogger(AtexoDataLoader.class);

	public AtexoDataLoader(String contentType) {
		this.contentType = contentType;
	}
	public AtexoDataLoader() {
	}

	@Override
	public byte[] post(final String url, final byte[] content) throws DSSException {

		logger.debug("URL  " + url);
		HttpPost httpRequest = null;
		CloseableHttpResponse httpResponse = null;
		CloseableHttpClient client = null;
		try {
			final URI uri = URI.create(url.trim());
			httpRequest = new HttpPost(uri);
			final ByteArrayInputStream bis = new ByteArrayInputStream(content);
			final HttpEntity httpEntity = new InputStreamEntity(bis, content.length);
			final HttpEntity requestEntity = new BufferedHttpEntity(httpEntity);
			httpRequest.setEntity(requestEntity);
			if (contentType != null) {
				httpRequest.setHeader(CONTENT_TYPE, contentType);
			}
			if (bearer != null) {
				httpRequest.setHeader(AUTHORIZATION, "Bearer " + bearer);
			}
			client = getHttpClient(url);
			httpResponse = getHttpResponse(client, httpRequest);
			return readHttpResponse(httpResponse);
		} catch (IOException e) {
			throw new DSSException(e);
		} finally {
			try {
				if (httpRequest != null) {
					httpRequest.releaseConnection();
				}
				if (httpResponse != null) {
					EntityUtils.consumeQuietly(httpResponse.getEntity());
				}
			} finally {
				close(client);
			}
		}
	}

	private void close(CloseableHttpClient httpClient) {
		if (httpClient != null) {
			try {
				httpClient.close();
			} catch (Exception ex) {
				logger.warn("Could not close client", ex);
			} finally {
				httpClient = null;
			}
		}
	}

	public void addBearer(String bearer) {
		this.bearer = bearer;
	}

	@Override
	protected synchronized CloseableHttpClient getHttpClient(String url) {
		logger.info("get Http Client {}", url);
		return createHttpClientWithProxy(url).build();
	}

	public HttpClientBuilder createHttpClientWithProxy(String url) {

		final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		try {
			final SSLContext sslContext = new SSLContextBuilder()
					.loadTrustMaterial(null, (x509CertChain, authType) -> true)
					.build();

			httpClientBuilder.setSSLContext(sslContext)
					.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
					.setConnectionManager(
							new PoolingHttpClientConnectionManager(
									RegistryBuilder.<ConnectionSocketFactory>create()
											.register("http", PlainConnectionSocketFactory.INSTANCE)
											.register("https", new SSLConnectionSocketFactory(sslContext,
													NoopHostnameVerifier.INSTANCE))
											.build()
							));
		} catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
			logger.error("Erreur lors de l'initialisation du contexte SSL {}", e.getMessage());
		}


		if (this.getProxyConfig() == null) {
			return httpClientBuilder;
		}

		final String protocol = getURL(url).getProtocol();
		final boolean proxyHTTPS = Protocol.isHttps(protocol) && (this.getProxyConfig().getHttpsProperties() != null);
		final boolean proxyHTTP = Protocol.isHttp(protocol) && (this.getProxyConfig().getHttpProperties() != null);

		ProxyProperties proxyProps = null;
		if (proxyHTTPS) {
			logger.debug("Use proxy https parameters");
			proxyProps = this.getProxyConfig().getHttpsProperties();
		} else if (proxyHTTP) {
			logger.debug("Use proxy http parameters");
			proxyProps = this.getProxyConfig().getHttpProperties();
		} else {
			return httpClientBuilder;
		}

		String proxyHost = proxyProps.getHost();
		int proxyPort = proxyProps.getPort();
		String proxyUser = proxyProps.getUser();
		String proxyPassword = proxyProps.getPassword();

		if (Utils.isStringNotEmpty(proxyUser) && Utils.isStringNotEmpty(proxyPassword)) {
			AuthScope proxyAuth = new AuthScope(proxyHost, proxyPort);
			UsernamePasswordCredentials proxyCredentials = new UsernamePasswordCredentials(proxyUser, proxyPassword);
			CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
			credentialsProvider.setCredentials(proxyAuth, proxyCredentials);
			httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
		}
		logger.debug("proxy host/port: {}:{}", proxyHost, proxyPort);
		final HttpHost proxy = new HttpHost(proxyHost, proxyPort, Protocol.HTTP.getName());

		return httpClientBuilder.setProxy(proxy);
	}

	private URL getURL(String urlString) {
		try {
			return new URL(urlString);
		} catch (MalformedURLException e) {
			throw new DSSException("Unable to create URL instance", e);
		}
	}
}
