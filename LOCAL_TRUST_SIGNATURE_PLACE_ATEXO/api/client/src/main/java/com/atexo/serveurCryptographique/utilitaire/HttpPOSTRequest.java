package com.atexo.serveurCryptographique.utilitaire;

import java.io.DataOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;


public class HttpPOSTRequest {
    private static final Logger logger = LoggerFactory.getLogger(HttpPOSTRequest.class);
    private String response;
    private URL url;
    private StringBuilder parametre = new StringBuilder();

    public void ajouterParametre(String cle, String valeur) {
        if (!parametre.toString().isEmpty()) {
            parametre.append("&");
        }
        try {
            parametre.append(cle).append("=").append(URLEncoder.encode(valeur, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.error("", e.fillInStackTrace());
        }
    }

    public void setUrl(final String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public void envoyer() throws PrivilegedActionException {

        final boolean modeHttps = url.getProtocol().equals("https");

        // Création d'un trust manager qui ne valide pas la chaine de
        // certification du serveur

        AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () -> {

            if (modeHttps) {

                TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                } };

                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new SecureRandom());

                HostnameVerifier hostnameVerifier = (urlHostName, session) -> true;

                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            }
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("charset", "utf-8");
            connection.setDoOutput(true);
            DataOutputStream wos = new DataOutputStream(connection.getOutputStream());
            wos.writeBytes(parametre.toString());
            wos.flush();
            wos.close();

            int responseCode = connection.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + parametre);
            System.out.println("Response Code : " + responseCode);

            response = IOUtils.toString(connection.getInputStream());
            // print result
            System.out.println(response.toString());
            connection.disconnect();
            return response;
        });
    }

    public String getResponse() {
        return response;
    }
}
