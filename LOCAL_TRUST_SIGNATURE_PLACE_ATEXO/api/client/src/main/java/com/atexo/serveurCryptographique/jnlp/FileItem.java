package com.atexo.serveurCryptographique.jnlp;

import java.io.File;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FileItem {

    private File file;
    private final BooleanProperty signer = new SimpleBooleanProperty(false);
    private final StringProperty signatureInfo = new SimpleStringProperty("");
    private final StringProperty signatureType = new SimpleStringProperty("XAdES");
    private final StringProperty filename;
    
    public FileItem(File file) {
        this.file = file;
        this.filename = new SimpleStringProperty(file.getName());
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFilename() {
        return filename.get();
    }

    public String getSignatureInfo() {
        return signatureInfo.get();
    }

    public StringProperty getSignatureInfoProperty() {
        return signatureInfo;
    }

    public void setSignatureInfo(String signatureInfo) {
        this.signatureInfo.set(signatureInfo);
    }

    public Boolean getSigner() {
        return signer.get();
    }

    public BooleanProperty getSignerProperty() {
        return signer;
    }

    public void setSigner(Boolean signer) {
        this.signer.set(signer);
    }

    public StringProperty getSignatureType() {
        return signatureType;
    }
    
    public void setSignatureType(String signatureType) {
        this.signatureType.set(signatureType);
    }
}
