package com.atexo.serveurCryptographique.jnlp;

import com.atexo.serveurCryptographique.utilitaire.*;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateListener;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinHandler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateListener;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12Handler;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.service.crl.OnlineCRLSource;
import eu.europa.esig.dss.service.http.commons.OCSPDataLoader;
import eu.europa.esig.dss.service.http.commons.TimestampDataLoader;
import eu.europa.esig.dss.service.http.proxy.ProxyConfig;
import eu.europa.esig.dss.service.http.proxy.ProxyProperties;
import eu.europa.esig.dss.service.ocsp.OnlineOCSPSource;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.spi.tsl.TrustedListsCertificateSource;
import eu.europa.esig.dss.spi.x509.CommonTrustedCertificateSource;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.KSPrivateKeyEntry;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

//import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
//import eu.europa.esig.dss.tsl.job.TLValidationJob;

public class ApplicationSignature extends Application implements MagasinCertificateListener, Pkcs12CertificateListener {
	private TypeOs typeOs;
	private TypeProvider typeProvider;
	private static OnlineTSPSource tpsSource;
	private static URL logURL;
	private static String lotlRootSchemeInfoUri = "https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl.html";
	private static String lotlUrl = "https://ec.europa.eu/tools/lotl/eu-lotl.xml";
	private static String ojUrl = "https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.C_.2019.276.01.0001.01.ENG";
	private List<FileItem> filesToSign = new ArrayList<FileItem>();
	private File lastDirectory;
	private static String plateforme = "INCONNUE";
	private static boolean activeHorodatageSignature;
	private TableView table = new TableView();
	private final ObservableList<FileItem> data = FXCollections.observableArrayList();
	private final Alert alert = new Alert(Alert.AlertType.INFORMATION);
	private static DigestAlgorithm shaSignature = DigestAlgorithm.SHA256;
	private static DigestAlgorithm shaHorodatage = DigestAlgorithm.SHA256;
	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";
	private CertificateVerifier certificateVerifier = null;
	private static AtexoDataLoader commonsHttpDataLoader;

	private static Pkcs11LibsType pkcs11LibsType;

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11LibsType = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("configuration URL serveur validaton absente");
		} else {
			if (args.length == 8) {
				plateforme = args[7];
			} else {
				plateforme = "DEFAULT";
			}
			tpsSource = tspSource(args[0] + "tokenRFC3161?plateforme=" + plateforme);
			try {
				logURL = new URL(args[0] + "/logSignature");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		activeHorodatageSignature = args[1] != null && args[1].equalsIgnoreCase("true");
		if (args[2] != null && args[2].equals("SHA1")) {
			shaSignature = DigestAlgorithm.SHA1;
		}
		if (args[3] != null && args[3].equals("SHA1")) {
			shaHorodatage = DigestAlgorithm.SHA1;
		}
		if (args[4] != null) {
			lotlRootSchemeInfoUri = args[4];
		}
		if (args[5] != null) {
			lotlUrl = args[5];
			try {
				commonsHttpDataLoader = new AtexoDataLoader();
				ProxyConfig proxy = new ProxyConfig();
				if (System.getProperty("http.proxyHost") != null) {
					ProxyProperties prop = new ProxyProperties();
					prop.setHost(System.getProperty("http.proxyHost"));
					prop.setPort(Integer.parseInt(System.getProperty("http.proxyPort")));
					proxy.setHttpProperties(prop);
					proxy.setHttpsProperties(prop);
					System.out.println("proxy --> " + prop.getHost() + ":" + prop.getPort());
					commonsHttpDataLoader.setProxyConfig(proxy);
				}
				if (System.getProperty("https.proxyHost") != null) {
					ProxyProperties prop = new ProxyProperties();
					prop.setHost(System.getProperty("https.proxyHost"));
					prop.setPort(Integer.parseInt(System.getProperty("https.proxyPort")));
					proxy.setHttpProperties(prop);
					proxy.setHttpsProperties(prop);
					System.out.println(prop.getHost() + ":" + prop.getPort());
					commonsHttpDataLoader.setProxyConfig(proxy);
				}
				commonsHttpDataLoader.setTimeoutConnection(5000);
				System.out.println(lotlUrl);
				//commonsHttpDataLoader.get(lotlUrl);
				System.out.println("URL OK --> " + lotlUrl);
			} catch (DSSException e) {
				System.out.println("TSL non accessible ");
				lotlUrl = null;
				e.printStackTrace();
			}
		}
		if (args[6] != null) {
			ojUrl = args[6];
		}
		if (args.length == 8) {
			plateforme = args[7];
		} else {
			System.out.println("les parametres plateforme ne sont pas presents originePlateforme, origineOrganisme, origineItem, origineContexteMetier");
		}
		launch(args);
	}

	public void start(final Stage stage) throws Exception {
		stage.setResizable(false);
		this.typeOs = Util.determinerOs();
		this.typeProvider = Util.determinerProvider();
		alert.initStyle(StageStyle.UTILITY);
		alert.setTitle("Signature");

		Scene scene = new Scene(new Group());

		stage.setTitle("Utilitaire de signature électronique (@version@)");
		int width = 0;
		int ajustement = 0;
		if (!System.getProperty("java.version").startsWith("8.")) {
			width = 30;
			ajustement = 10;
		}
		stage.setWidth(890 + width);
		stage.setHeight(505 + ajustement);

		final Label label = new Label("Liste des fichiers");
		label.setFont(new Font("Arial", 20));

		table.setEditable(true);
		table.setFixedCellSize(Region.USE_COMPUTED_SIZE);

		TableColumn fileNameColumn = new TableColumn("Nom du fichier");
		fileNameColumn.setCellValueFactory(new PropertyValueFactory<FileItem, String>("filename"));

		fileNameColumn.setPrefWidth(225);

		TableColumn<FileItem, String> signatureColumn = new TableColumn("Chemin de la Signature");

		signatureColumn.setCellValueFactory(cell -> {
			return cell.getValue().getSignatureInfoProperty();

		});

		signatureColumn.setCellFactory(cell -> {

			return new TableCell<FileItem, String>() {
				@Override
				protected void updateItem(String item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
						setStyle("");
					} else {
						Text text = new Text(item);
						text.setWrappingWidth(cell.getPrefWidth() - 35);

						// the only change that i make
						this.setPrefHeight(text.getLayoutBounds().getHeight() + 10);
						this.setGraphic(text);
						this.setMaxHeight(30);

					}
				}
			};
		});

		signatureColumn.setPrefWidth(470);

		TableColumn<FileItem, String> typeColumn = new TableColumn<>("Format");
		typeColumn.setCellValueFactory(f -> f.getValue().getSignatureType());
		typeColumn.setCellFactory(new Callback<TableColumn<FileItem, String>, TableCell<FileItem, String>>() {

			@Override
			public TableCell<FileItem, String> call(TableColumn<FileItem, String> p) {
				LiveComboBoxTableCell cell = new LiveComboBoxTableCell(FXCollections.observableArrayList("XAdES", "CAdES", "PAdES"));
				cell.setAlignment(Pos.CENTER);
				cell.setVisible(true);
				return cell;
			}
		});

		typeColumn.setPrefWidth(90);

		TableColumn<FileItem, Boolean> loadedColumn = new TableColumn<>();
		Label lb = new Label("Signer");
		CheckBox checkBoxSigne = new CheckBox();
		lb.setGraphic(checkBoxSigne);
		lb.setContentDisplay(ContentDisplay.RIGHT);
		checkBoxSigne.setTextAlignment(TextAlignment.LEFT);
		checkBoxSigne.setOnAction(e -> {
			data.forEach(fileToSign -> {
				fileToSign.setSigner(((CheckBox) e.getSource()).isSelected());
			});
		});
		loadedColumn.setGraphic(lb);
		loadedColumn.setCellValueFactory(f -> f.getValue().getSignerProperty());
		loadedColumn.setCellFactory(tc -> new CheckBoxTableCell<FileItem, Boolean>());
		loadedColumn.setPrefWidth(90 + width);

		table.getColumns().addAll(fileNameColumn, signatureColumn, typeColumn, loadedColumn);

		table.setItems(data);

		Button ajoutFichierButton = new Button();
		ajoutFichierButton.setText("Ajouter un fichier");

		final FileChooser fileChooser = new FileChooser();

		Button signerButton = new Button();
		signerButton.setText("Signer les fichiers sélectionnés");
		signerButton.setDisable(true);
		signerButton.setAlignment(Pos.CENTER_RIGHT);

		ajoutFichierButton.setOnAction(e -> {
			if (lastDirectory != null) {
				fileChooser.setInitialDirectory(lastDirectory);
			}
			List<File> files = fileChooser.showOpenMultipleDialog(stage);
			if (files != null) {
				for (File file : files) {
					if (file != null) {
						FileItem fileItem = new FileItem(file);
						lastDirectory = file.getParentFile();
						fileItem.setSigner(checkBoxSigne.isSelected());
						if (file.getName().toLowerCase().endsWith(".pdf")) {
							fileItem.setSignatureType("PAdES");
						}
						data.add(fileItem);

						signerButton.setDisable(false);
					}
				}
				table.getProperties().put("recreateKey", Boolean.TRUE);
			}
		});

		Button viderButton = new Button();
		viderButton.setText("Vider la liste");
		viderButton.setOnAction(e -> {
			filesToSign.clear();
			data.clear();
			signerButton.setDisable(true);
			checkBoxSigne.setSelected(false);
			// table.getProperties().put(TableViewSkinBase.RECREATE, Boolean.TRUE);
		});

		signerButton.setOnAction(e -> {
			filesToSign.clear();
			boolean padesOK = true;
			for (int i = 0; i < data.size(); i++) {
				FileItem fileItem = data.get(i);
				if (fileItem.getSigner() && !fileItem.getFilename().toLowerCase().endsWith("pdf") && fileItem.getSignatureType().getValue().equals("PAdES")) {
					padesOK = false;
					break;
				}
				if (fileItem.getSigner()) {
					filesToSign.add(fileItem);
				}
			}
			if (!padesOK) {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setHeaderText("Pour certains fichiers le format de signature sélectionné\nn'est pas applicable.");
				alert.setContentText("Veuillez-vous assurer que le format PADES est seulement\nsélectionné pour des fichiers PDF.");
				alert.showAndWait();
			} else if (!filesToSign.isEmpty()) {
				selectionnerCertificat(ApplicationSignature.this.typeProvider);
			}
		});

		ajoutFichierButton.setAlignment(Pos.CENTER);
		ButtonBar.setButtonData(ajoutFichierButton, ButtonBar.ButtonData.LEFT);

		viderButton.setAlignment(Pos.CENTER);
		ButtonBar.setButtonData(viderButton, ButtonBar.ButtonData.LEFT);

		signerButton.setAlignment(Pos.CENTER);
		ButtonBar.setButtonData(signerButton, ButtonBar.ButtonData.RIGHT);

		ButtonBar buttonBar = new ButtonBar();

		buttonBar.getButtons().add(ajoutFichierButton);
		buttonBar.getButtons().add(viderButton);
		buttonBar.getButtons().add(signerButton);

		final VBox vbox = new VBox();
		vbox.setSpacing(5);
		vbox.setPadding(new Insets(10, ajustement, ajustement, 10));
		vbox.setPrefWidth(875 + width);

		vbox.getChildren().addAll(label, table, buttonBar);
		((Group) scene.getRoot()).getChildren().addAll(vbox);

		stage.setScene(scene);
		table.setPlaceholder(new Label("Veuillez patientez. Chargement TSL en cours..."));
		stage.show();
		certificateVerifier = getCertificateVerifier();
		table.setPlaceholder(new Label("Aucun fichier sélectionné."));

	}

	protected void selectionnerCertificat(TypeProvider provider) {
		System.out.println("Type de provider : " + provider);
		if (provider != null) {
			switch (provider) {
				case APPLE:
				case PKCS11:
				case MSCAPI:
					MagasinCertificateUiService.getInstance(pkcs11LibsType).initUi(this, typeOs, provider, true, true);
					break;
				case PKCS12:
					Pkcs12CertificateUiService.getInstance().initUi(this, typeOs, provider, true, true);
					break;
			}
		}
	}

	@Override
	public void onSelection(MagasinCertificateEvent event) throws ManipulationCertificatException {
		KeyPair keyPair = null;
		String alias = event.getCertificateItem().getId();
		boolean smartCard = event.getCertificateItem().isSmartCard();

		if (typeOs != TypeOs.Windows && smartCard) {
			keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
		} else {
			keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, alias);
		}
		onSelection(keyPair);
	}

	@Override
	public void onSelection(Pkcs12CertificateEvent event) throws ManipulationCertificatException {
		String cheminFichierP12 = event.getCheminFichierP12();
		String motDePasseFichierP12 = event.getMotDePasseFichierP12();
		KeyPair keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);
		onSelection(keyPair);
	}

	protected void onSelection(KeyPair keyPair) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				alert.setHeaderText("Fichier(s) en cours de signature ...");
				alert.show();

			}
		});
		filesToSign.forEach(fileToSign -> {
			int indexFichier = FileUtil.getProchaineIndexFichierSignatureXML(fileToSign.getFile().getPath());
			Date dateSignature = new Date();
			String formatDateNomFichier = Util.formaterDate(dateSignature, Util.DATE_TIME_PATTERN_YYYMMDDHHMMSS);

			String signingFile = null;
			X509Certificate[] x509 = new X509Certificate[1];
			x509[0] = keyPair.getCertificate();
			KeyStore.PrivateKeyEntry privateKey = new KeyStore.PrivateKeyEntry(keyPair.getPrivateKey(), x509);
			DSSPrivateKeyEntry privateKeyEntry = new KSPrivateKeyEntry(privateKey.toString(), privateKey);
			DSSDocument documentToSign = new FileDocument(fileToSign.getFile());
			DSSDocument signedDocument = null;
			InputStream docInputStream = null;
			long start = System.currentTimeMillis();
			if (fileToSign.getSignatureType().getValue().trim().equals("PAdES")) {
				if (activeHorodatageSignature) {
					signedDocument = SignatureServiceUtil.getSignaturePades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource, null);
				} else {
					signedDocument = SignatureServiceUtil.getSignaturePades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, null, null);
				}

				signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignaturePDF(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
				docInputStream = signedDocument.openStream();
			} else if (fileToSign.getSignatureType().getValue().trim().equals("XAdES")) {
				if (activeHorodatageSignature) {
					signedDocument = SignatureServiceUtil.getSignatureXades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource);
				} else {
					signedDocument = SignatureServiceUtil.getSignatureXades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, null);
				}

				try {
					String signatureXML = new String(IOUtils.toByteArray(signedDocument.openStream()), "UTF-8");
					docInputStream = new ByteArrayInputStream(signatureXML.getBytes());

				} catch (Exception e) {
					e.printStackTrace();
				}
				signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignatureXML(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
			} else if (fileToSign.getSignatureType().getValue().trim().equals("CAdES")) {
				if (activeHorodatageSignature) {
					signedDocument = SignatureServiceUtil.getSignatureCades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, tpsSource);
				} else {
					signedDocument = SignatureServiceUtil.getSignatureCades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage, null);
				}
				signingFile = com.atexo.serveurCryptographique.utilitaire.utilitaire.XMLUtil.contruireCheminFichierSignatureCades(fileToSign.getFile().getPath(), formatDateNomFichier, indexFichier);
				docInputStream = signedDocument.openStream();
			}
			OutputStream os = null;
			try {
				os = new FileOutputStream(signingFile);
				if (docInputStream == null) {
					docInputStream = signedDocument.openStream();
				}
				IOUtils.copy(docInputStream, os);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				IOUtils.closeQuietly(os);
				IOUtils.closeQuietly(docInputStream);
			}
			fileToSign.setSignatureInfo(signingFile);
			start = System.currentTimeMillis() - start;
			envoiLog(x509[0], fileToSign.getSignatureType().getValue().trim(), fileToSign, (int) start);
		});
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				alert.setHeaderText("Fichier(s) signé(s).");
			}
		});
	}

	private void envoiLog(X509Certificate x509, String typeSignature, FileItem fileToSign, int tempsRequete) {
		HttpPOSTRequest request = new HttpPOSTRequest();
		try {
			if (logURL == null) {
				System.out.println("communication impossible avec le serveur. URL n'est pas presente");
				return;
			}
			request.setUrl(logURL.toString());
			request.ajouterParametre("certificat", Base64.getEncoder().encodeToString(x509.getEncoded()));
			request.ajouterParametre("typeSignature", typeSignature);
			request.ajouterParametre("nomFichier", fileToSign.getFilename());
			request.ajouterParametre("plateforme", plateforme);
			request.ajouterParametre("tempsRequete", String.valueOf(tempsRequete));
			request.envoyer();
		} catch (Exception e) {
			System.out.println("communication impossible avec le serveur");
			e.printStackTrace();
		}

	}


	public static CertificateVerifier getCertificateVerifier() {
		CertificateVerifier certificateVerifier = new CommonCertificateVerifier();
		try {
			certificateVerifier.setTrustedCertSource(new CommonTrustedCertificateSource());
			certificateVerifier.setTrustedCertSource(new TrustedListsCertificateSource());

			certificateVerifier.setCrlSource(onlineCRLSource());
			certificateVerifier.setOcspSource(ocspSource());
			certificateVerifier.setDataLoader(dataLoader());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERREUR CHARGEMENT TSL");
			activeHorodatageSignature = false;
		}
		return certificateVerifier;
	}

	public static OnlineCRLSource onlineCRLSource() {
		OnlineCRLSource onlineCRLSource = new OnlineCRLSource();
		onlineCRLSource.setDataLoader(dataLoader());
		return onlineCRLSource;
	}

	public static AtexoDataLoader dataLoader() {
		AtexoDataLoader dataLoader = new AtexoDataLoader();
		dataLoader.setProxyConfig(getProxyConfig());
		return dataLoader;
	}

	public static OnlineOCSPSource ocspSource() {
		OnlineOCSPSource onlineOCSPSource = new OnlineOCSPSource();
		onlineOCSPSource.setDataLoader(ocspDataLoader());
		return onlineOCSPSource;
	}

	public static OCSPDataLoader ocspDataLoader() {
		OCSPDataLoader ocspDataLoader = new OCSPDataLoader();
		ocspDataLoader.setProxyConfig(getProxyConfig());
		return ocspDataLoader;
	}


	public static OnlineTSPSource tspSource(String tsaURL) {
		OnlineTSPSource onlineTSPSource = new OnlineTSPSource();
		onlineTSPSource.setDataLoader(timestampDataLoader());
		onlineTSPSource.setTspServer(tsaURL);
		return onlineTSPSource;
	}


	private static ProxyConfig getProxyConfig() {
		ProxyConfig proxyConfig = new ProxyConfig();
		if (System.getProperty("http.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("http.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("http.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("http.proxyUser"));
			proxyProperties.setPassword(System.getProperty("http.proxyPassword"));
			proxyConfig.setHttpProperties(proxyProperties);
		}
		if (System.getProperty("https.proxyHost") != null) {
			ProxyProperties proxyProperties = new ProxyProperties();
			proxyProperties.setHost(System.getProperty("https.proxyHost"));
			proxyProperties.setPort(Integer.parseInt(System.getProperty("https.proxyPort", "80")));
			proxyProperties.setUser(System.getProperty("https.proxyUser"));
			proxyProperties.setPassword(System.getProperty("https.proxyPassword"));
			proxyConfig.setHttpsProperties(proxyProperties);
		}
		if (proxyConfig.getHttpProperties() == null && proxyConfig.getHttpsProperties() == null) {
			return null;
		}
		return proxyConfig;
	}

	public static AtexoTimestampDataLoader timestampDataLoader() {
		AtexoTimestampDataLoader timestampDataLoader = new AtexoTimestampDataLoader();
		timestampDataLoader.setProxyConfig(getProxyConfig());
		return timestampDataLoader;
	}

}
