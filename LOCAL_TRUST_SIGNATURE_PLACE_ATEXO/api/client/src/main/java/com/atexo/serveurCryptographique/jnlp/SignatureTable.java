package com.atexo.serveurCryptographique.jnlp;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class SignatureTable extends JTable {

    public static final int BOOLEAN_COL = 3;
    public static final Object colNames[] = { "Nom du fichier", "Chemin de la signature", "Format", "" };
    private List<FileItem> filesToSign;
    private DefaultTableModel model;

    public SignatureTable(DefaultTableModel model, List<FileItem> filesToSign, boolean typeChiffrementActive) {
        super(model);
        this.model = model;
        this.filesToSign = filesToSign;
        setAutoCreateRowSorter(true);
        javax.swing.table.TableColumn column = getColumnModel().getColumn(2);
        JComboBox comboBox = new JComboBox();
        comboBox.setEnabled(typeChiffrementActive);
        comboBox.addItem("XAdES");
        comboBox.addItem("CAdES");
        comboBox.addItem("PadES");
        comboBox.setAlignmentY(CENTER_ALIGNMENT);
        column.setCellEditor(new DefaultCellEditor(comboBox));
        getColumnModel().getColumn(0).setPreferredWidth(230);
        getColumnModel().getColumn(1).setPreferredWidth(500);
        getColumnModel().getColumn(2).setPreferredWidth(75);
        getColumnModel().getColumn(3).setPreferredWidth(70);
        // Set up tool tips for the sport cells.
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setToolTipText("Cliquez");
        column.setCellRenderer(renderer);
        
        javax.swing.table.TableColumn tc = getColumnModel().getColumn(BOOLEAN_COL);
        tc.setHeaderRenderer(new SelectAllHeader(this, BOOLEAN_COL));
        String newLookAndFeel = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
        try {
            UIManager.setLookAndFeel(newLookAndFeel);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void addRow(FileItem fileItem) {
        model.addRow(new Object[] { fileItem.getFilename(), fileItem.getSignatureInfo(), "XAdES", fileItem.getSigner() });
    }

    public void clean() {
        model.getDataVector().clear();
        filesToSign.clear();
        model.fireTableDataChanged();
        repaint();
    }

}

/**
 * A TableCellRenderer that selects all or none of a Boolean column.
 * 
 * @param targetColumn
 *            the Boolean column to manage
 */
class SelectAllHeader extends JCheckBox implements TableCellRenderer {

    private static final String ALL = "Signer";
    private static final String NONE = "Signer";
    private JTable table;
    private javax.swing.table.TableModel tableModel;
    private JTableHeader header;
    private TableColumnModel tcm;
    private int targetColumn;
    private int viewColumn;

    public SelectAllHeader(JTable table, int targetColumn) {
        super(ALL);
        this.table = table;
        this.tableModel = table.getModel();
        if (tableModel.getColumnClass(targetColumn) != Boolean.class) {
            throw new IllegalArgumentException("Boolean column required.");
        }
        this.targetColumn = targetColumn;
        this.header = table.getTableHeader();
        this.tcm = table.getColumnModel();
        this.applyUI();
        this.addItemListener(new ItemHandler());
        header.addMouseListener(new MouseHandler());
        tableModel.addTableModelListener(new ModelHandler());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return this;
    }

    private class ItemHandler implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            boolean state = e.getStateChange() == ItemEvent.SELECTED;
            setText((state) ? NONE : ALL);
            for (int r = 0; r < table.getRowCount(); r++) {
                try {
                    table.setValueAt(state, r, 3);
                } catch (Exception e1) {
                    e1.fillInStackTrace();
                }
            }
        }
    }

    @Override
    public void updateUI() {
        super.updateUI();
        applyUI();
    }

    private void applyUI() {
        this.setFont(UIManager.getFont("TableHeader.font"));
        this.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        this.setBackground(UIManager.getColor("TableHeader.background"));
        this.setForeground(UIManager.getColor("TableHeader.foreground"));
    }

    private class MouseHandler extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            viewColumn = header.columnAtPoint(e.getPoint());
            int modelColumn = tcm.getColumn(viewColumn).getModelIndex();
            if (modelColumn == targetColumn) {
                doClick();
            }
        }
    }

    private class ModelHandler implements TableModelListener {

        @Override
        public void tableChanged(TableModelEvent e) {
            if (needsToggle()) {
                doClick();
                header.repaint();
            }
        }
    }

    // Return true if this toggle needs to match the model.
    private boolean needsToggle() {
        boolean allTrue = true;
        boolean allFalse = true;
        for (int r = 0; r < tableModel.getRowCount(); r++) {
            boolean b = (Boolean) tableModel.getValueAt(r, targetColumn);
            allTrue &= b;
            allFalse &= !b;
        }
        return allTrue && !isSelected() || allFalse && isSelected();
    }
}
