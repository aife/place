package com.atexo.serveurCryptographique.jnlp;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.PrivilegedActionException;
import java.util.ArrayList;
import java.util.List;

import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateListener;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateUiService;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.JaxbPkcs11Util;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11Handler;
import com.atexo.serveurCryptographique.utilitaire.pkcs11.Pkcs11LibsType;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateUiService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atexo.serveurCryptographique.utilitaire.HttpPOSTRequest;
import com.atexo.serveurCryptographique.utilitaire.KeyPair;
import com.atexo.serveurCryptographique.utilitaire.ManipulationCertificatException;
import com.atexo.serveurCryptographique.utilitaire.SignatureServiceUtil;
import com.atexo.serveurCryptographique.utilitaire.TypeOs;
import com.atexo.serveurCryptographique.utilitaire.TypeProvider;
import com.atexo.serveurCryptographique.utilitaire.Util;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinCertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.magasin.MagasinHandler;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateEvent;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12CertificateListener;
import com.atexo.serveurCryptographique.utilitaire.pkcs12.Pkcs12Handler;
import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;

import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.InMemoryDocument;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.MSCAPISignatureToken;
import eu.europa.esig.dss.validation.CommonCertificateVerifier;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ApplicationSignatureHash extends Application
		implements MagasinCertificateListener, Pkcs12CertificateListener {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationSignatureHash.class);
	private static Pkcs11LibsType pkcs11Libs = null;
	private static final String NOM_RESOURCE_PKCS11 = "pkcs11Libs.xml";

	static {
		ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
		InputStream inputStream = contextClassLoader.getResourceAsStream(NOM_RESOURCE_PKCS11);
		try {
			byte[] contenuFichierXML = IOUtils.toByteArray(inputStream);
			pkcs11Libs = JaxbPkcs11Util.getPkcs11LibsType(contenuFichierXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private TypeOs typeOs;
	private TypeProvider typeProvider;
	private static String urlPlateforme;
	private static List<String> hashs;
	private static List<String> ids;
	private static List<byte[]> signature;
	private final Alert alert = new Alert(Alert.AlertType.INFORMATION);

	public static void main(String[] args) {
		if (args.length == 0) {
			logger.error("configuration URL serveur validaton absente");
		} else {
			urlPlateforme = args[0];
			hashs = new ArrayList<String>();
			ids = new ArrayList<String>();
			for (int i = 1; i < args.length; i += 2) {
				hashs.add(args[i].toUpperCase());
				ids.add(args[i + 1].toUpperCase());
			}
		}
		launch(args);
	}

	public void start(final Stage stage) throws Exception {
		stage.setResizable(false);
		this.typeOs = Util.determinerOs();
		this.typeProvider = Util.determinerProvider();
		alert.initStyle(StageStyle.UTILITY);
		alert.setTitle("Signature (@version@)");
		selectionnerCertificat(ApplicationSignatureHash.this.typeProvider);
	}

	protected void selectionnerCertificat(TypeProvider provider) {
		System.out.println("Type de provider : " + provider);
		if (provider != null) {
			switch (provider) {
				case APPLE:
				case PKCS11:
				case MSCAPI:
					MagasinCertificateUiService.getInstance(pkcs11Libs).initUi(this,typeOs,typeProvider,true,true);
					break;
				case PKCS12:
					Pkcs12CertificateUiService.getInstance().initUi(this, typeOs, provider, true, true);
					break;
			}
		}
	}


	@Override
	public void onSelection(MagasinCertificateEvent event) throws ManipulationCertificatException {
		KeyPair keyPair = null;

		logger.info("onSelect => Provider d'accès au  Magasin : " + event.getCertificateItem().getTypeProvider());
		String alias = event.getCertificateItem().getId();
		logger.info("L'alias selectionné est : " + alias);
		boolean smartCard = event.getCertificateItem().isSmartCard();
		logger.info("SmartCard : " + smartCard);

		if (typeOs == TypeOs.Windows || !smartCard) {
			keyPair = MagasinHandler.getInstance().getKeyPair(typeProvider, alias);
		}   else {
			keyPair = Pkcs11Handler.getInstance().getKeyPair(alias);
		}
		try {
			onSelection(keyPair);
		} catch (DSSException | IOException e) {
			logger.error("Erreur de siganture", e.fillInStackTrace());
		}
	}

	@Override
	public void onSelection(Pkcs12CertificateEvent event) throws ManipulationCertificatException {
		String cheminFichierP12 = event.getCheminFichierP12();
		String motDePasseFichierP12 = event.getMotDePasseFichierP12();
		KeyPair keyPair = Pkcs12Handler.getKeyPair(cheminFichierP12, motDePasseFichierP12);
		try {
			onSelection(keyPair);
		} catch (DSSException | IOException e) {
			logger.error("Erreur de siganture", e.fillInStackTrace());
		}
	}

	protected void onSelection(KeyPair keyPair) throws DSSException, IOException {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				alert.setHeaderText("Fichier(s) en cours de signature ...");
				alert.show();

			}
		});
		signature = new ArrayList<byte[]>();
		for (int i = 0; i < hashs.size(); i++) {
			MSCAPISignatureToken token = new MSCAPISignatureToken();
			DSSDocument documentToSign = new InMemoryDocument(hashs.get(i).getBytes());
			DSSDocument signedDocument = null;
			DSSPrivateKeyEntry privateKeyEntry = getPrivateKey(keyPair, token.getKeys());
			signedDocument = SignatureServiceUtil.getSignatureCades(new CommonCertificateVerifier(), privateKeyEntry, documentToSign, DigestAlgorithm.SHA1, DigestAlgorithm.SHA1, null);
			signature.add(IOUtils.toByteArray(signedDocument.openStream()));
		}
		if (!sendHash(signature, ids, hashs)) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					alert.setAlertType(AlertType.ERROR);
					alert.setHeaderText("Erreur signature fichier.");

				}
			});
		} else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					alert.setHeaderText("Fichier(s) signé(s).");

				}
			});
		}
	}

	private boolean sendHash(List<byte[]> signatures, List<String> ids, List<String> hashs) {
		HttpPOSTRequest httpRequest = new HttpPOSTRequest();
		try {
			logger.info(urlPlateforme);
			httpRequest.setUrl(urlPlateforme);
			int i = 1;
			for (String string : hashs) {
				httpRequest.ajouterParametre("hash" + i, string);
				logger.info("hash{}={}",i, string);
				httpRequest.ajouterParametre("jetonActe" + i, ids.get(i - 1));
				logger.info("jetonActe{}={}",i, ids.get(i - 1));
				httpRequest.ajouterParametre("signature" + i,  Base64.encodeBase64String(signatures.get(i - 1)));
				logger.info("signature {}={}",i, Base64.encodeBase64String(signatures.get(i - 1)));
				i++;
			}

			httpRequest.envoyer();
			String reponse = httpRequest.getResponse();
			return !(reponse == null || reponse.matches("\"etat\":\0\""));
		} catch (MalformedURLException | PrivilegedActionException e) {
			logger.error("Erreur envoi signature", e.fillInStackTrace());
		}
		return false;
	}

	private DSSPrivateKeyEntry getPrivateKey(KeyPair keyPair, List<DSSPrivateKeyEntry> privateKey) {
		for (DSSPrivateKeyEntry dssPrivateKeyEntry : privateKey) {
			if (dssPrivateKeyEntry != null && dssPrivateKeyEntry.getCertificate() != null && dssPrivateKeyEntry.getCertificate() != null && dssPrivateKeyEntry.getCertificate().getCertificate() !=  null && dssPrivateKeyEntry.getCertificate().getCertificate().equals(keyPair.getCertificate())) {
				return dssPrivateKeyEntry;
			}
		}
		return null;
	}
}
