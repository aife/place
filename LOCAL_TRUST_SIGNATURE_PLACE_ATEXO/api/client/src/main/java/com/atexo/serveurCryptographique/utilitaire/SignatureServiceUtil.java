package com.atexo.serveurCryptographique.utilitaire;

import com.atexo.serveurCryptographique.utilitaire.utilitaire.IOUtils;
import eu.europa.esig.dss.cades.CAdESSignatureParameters;
import eu.europa.esig.dss.cades.signature.CAdESService;
import eu.europa.esig.dss.cades.signature.CAdESTimestampParameters;
import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.enumerations.SignaturePackaging;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.pades.PAdESTimestampParameters;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import eu.europa.esig.dss.pades.SignatureImageTextParameters;
import eu.europa.esig.dss.pades.signature.PAdESService;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxNativeObjectFactory;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.signature.DocumentSignatureService;
import eu.europa.esig.dss.token.AbstractSignatureTokenConnection;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.MSCAPISignatureToken;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.xades.XAdESSignatureParameters;
import eu.europa.esig.dss.xades.signature.XAdESService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

public class SignatureServiceUtil {
	private static Logger logger = LoggerFactory.getLogger(SignatureServiceUtil.class);

	public static DSSDocument getSignaturePades(CertificateVerifier certificateVerifier,
												DSSPrivateKeyEntry privateKeyEntry,
												DSSDocument documentToSign, DigestAlgorithm shaSignature,
												DigestAlgorithm shaHorodatage, OnlineTSPSource tpsSource, String date) {
		PAdESSignatureParameters signatureParameters = new PAdESSignatureParameters();
		signatureParameters.bLevel().setSigningDate(new Date());
		signatureParameters.setDigestAlgorithm(shaSignature);
		signatureParameters.setSigningCertificate(privateKeyEntry.getCertificate());
		signatureParameters.setCertificateChain(privateKeyEntry.getCertificateChain());
		signatureParameters.setSignaturePackaging(SignaturePackaging.ENVELOPING);
		signatureParameters.setSignWithExpiredCertificate(true);
		if (date != null) {
			try {

				SignatureImageParameters imageParameters = new SignatureImageParameters();
				SignatureImageTextParameters textParameters = new SignatureImageTextParameters();
				// Allows you to set a DSSFont object that defines the text style (see more
				// information in the section "Fonts usage")
				// textParameters.setFont(font);
				float x = 10;
				float y = 10;
				try {
					PDDocument inputPdfReader = PDDocument.load(documentToSign.openStream());
					x = inputPdfReader.getPage(0).getMediaBox().getWidth() - 157;
					y = inputPdfReader.getPage(0).getMediaBox().getHeight() - 55;
				} catch (Exception e) {
					logger.error("Erreur lors de la récupération des dimensions du fichier");
				}
				// Defines the text content
				imageParameters.setPage(1);
				imageParameters.setxAxis(x);
				imageParameters.setyAxis(y);
				String extractCN = privateKeyEntry.getCertificate().getCanonicalizedSubject();
				int indexCN = extractCN.lastIndexOf("cn=");
				extractCN = extractCN.substring(indexCN + 3, extractCN.indexOf(",", indexCN));
				textParameters.setText("Signé electroniquement par: \n" + extractCN + "\nDate: " + date);

// Specifies the text size value (the default font size is 12pt)
				textParameters.setSize(10);
				// Defines the color of the characters
				textParameters.setTextColor(Color.BLACK);
				// Defines the background color for the area filled out by the text
				textParameters.setBackgroundColor(Color.decode("#F1F1F1"));
				// Defines a padding between the text and a border of its bounding area
				textParameters.setPadding(20);
				// Set textParameters to a SignatureImageParameters object
				imageParameters.setTextParameters(textParameters);
				signatureParameters.setImageParameters(imageParameters);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		}
		DocumentSignatureService<PAdESSignatureParameters, PAdESTimestampParameters> service = new PAdESService(
				certificateVerifier);
		((PAdESService) service).setPdfObjFactory(new PdfBoxNativeObjectFactory());
		if (tpsSource != null) {
			service.setTspSource(tpsSource);
			signatureParameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_T);
			signatureParameters.getSignatureTimestampParameters().setDigestAlgorithm(shaHorodatage);
		} else {
			signatureParameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_B);
		}

		ToBeSigned dataToSign = service.getDataToSign(documentToSign, signatureParameters);
		DigestAlgorithm digestAlgorithm = signatureParameters.getDigestAlgorithm();
		AbstractSignatureTokenConnection signingToken;
		signingToken = new MSCAPISignatureToken();
		SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKeyEntry);
		DSSDocument signedDocument = null;
		try {
			signedDocument = service.signDocument(documentToSign, signatureParameters, signatureValue);
		} catch (Exception e) {
			logger.warn("Erreur signature", e.fillInStackTrace());
			return getSignaturePades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage,
					null, date);
		}
		return signedDocument;
	}

	public static DSSDocument getSignatureCades(CertificateVerifier certificateVerifier,
												DSSPrivateKeyEntry privateKeyEntry, DSSDocument documentToSign, DigestAlgorithm shaSignature,
												DigestAlgorithm shaHorodatage, OnlineTSPSource tpsSource) {
		CAdESSignatureParameters signatureParameters;
		signatureParameters = new CAdESSignatureParameters();
		signatureParameters.bLevel().setSigningDate(new Date());
		signatureParameters.setDigestAlgorithm(shaSignature);
		signatureParameters.setSigningCertificate(privateKeyEntry.getCertificate());
		signatureParameters.setCertificateChain(privateKeyEntry.getCertificateChain());
		signatureParameters.setSignaturePackaging(SignaturePackaging.DETACHED);
		signatureParameters.setSignWithExpiredCertificate(true);
		DocumentSignatureService<CAdESSignatureParameters, CAdESTimestampParameters> service = new CAdESService(
				certificateVerifier);
		if (tpsSource != null) {
			service.setTspSource(tpsSource);
			signatureParameters.setSignatureLevel(SignatureLevel.CAdES_BASELINE_T);
			signatureParameters.getSignatureTimestampParameters().setDigestAlgorithm(shaHorodatage);
		} else {
			signatureParameters.setSignatureLevel(SignatureLevel.CAdES_BASELINE_B);
		}
		ToBeSigned dataToSign = service.getDataToSign(documentToSign, signatureParameters);
		DigestAlgorithm digestAlgorithm = signatureParameters.getDigestAlgorithm();
		AbstractSignatureTokenConnection signingToken;
		signingToken = new MSCAPISignatureToken();
		SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKeyEntry);
		DSSDocument signedDocument = null;
		try {
			signedDocument = service.signDocument(documentToSign, signatureParameters, signatureValue);
		} catch (DSSException e) {
			logger.warn("Erreur signature", e.fillInStackTrace());
			return getSignatureCades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature, shaHorodatage,
					null);
		}
		return signedDocument;
	}

	public static DSSDocument getSignatureXades(CertificateVerifier certificateVerifier,
												DSSPrivateKeyEntry privateKeyEntry, DSSDocument documentToSign, DigestAlgorithm shaSignature,
												DigestAlgorithm shaHorodatage, OnlineTSPSource tpsSource) {
		XAdESSignatureParameters signatureParameters = new XAdESSignatureParameters();
		signatureParameters.bLevel().setSigningDate(new Date());
		signatureParameters.setSigningCertificate(privateKeyEntry.getCertificate());
		signatureParameters.setEn319132(true);
		signatureParameters.setCertificateChain(privateKeyEntry.getCertificateChain());
		signatureParameters.setDigestAlgorithm(shaSignature);
		signatureParameters.setSigningCertificateDigestMethod(shaSignature);
		signatureParameters.setSignaturePackaging(SignaturePackaging.DETACHED);
		signatureParameters.setSignWithExpiredCertificate(true);
		signatureParameters.bLevel().setTrustAnchorBPPolicy(Boolean.TRUE);

		XAdESService service = new XAdESService(certificateVerifier);
		if (tpsSource != null) {
			signatureParameters.setSignatureLevel(SignatureLevel.XAdES_BASELINE_T);
			signatureParameters.getSignatureTimestampParameters().setDigestAlgorithm(shaHorodatage);
			service.setTspSource(tpsSource);
		} else {
			signatureParameters.setSignatureLevel(SignatureLevel.XAdES_BASELINE_B);
		}
		ToBeSigned dataToSign = service.getDataToSign(documentToSign, signatureParameters);
		AbstractSignatureTokenConnection signingToken;
		signingToken = new MSCAPISignatureToken();
		DigestAlgorithm digestAlgorithm = signatureParameters.getDigestAlgorithm();
		SignatureValue signatureValue = signingToken.sign(dataToSign, digestAlgorithm, privateKeyEntry);
		DSSDocument signedDocument = null;
		try {
			signedDocument = service.signDocument(documentToSign, signatureParameters, signatureValue);
		} catch (DSSException e) {
			System.out.println("Erreur signature");
			e.printStackTrace();
			if (tpsSource != null) {
				return getSignatureXades(certificateVerifier, privateKeyEntry, documentToSign, shaSignature,
						shaHorodatage, null);
			}
		}
		return signedDocument;
	}
}
