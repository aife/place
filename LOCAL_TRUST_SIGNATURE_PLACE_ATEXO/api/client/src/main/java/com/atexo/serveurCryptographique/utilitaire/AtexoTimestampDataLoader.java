package com.atexo.serveurCryptographique.utilitaire;

public class AtexoTimestampDataLoader extends AtexoDataLoader {
	public static final String TIMESTAMP_QUERY_CONTENT_TYPE = "application/timestamp-query";

	public AtexoTimestampDataLoader() {
		super(TIMESTAMP_QUERY_CONTENT_TYPE);
	}

	public void setContentType(String contentType) {
	}
}
