package com.atexo.serveurCryptographique.utilitaire;

import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.spi.x509.revocation.crl.ExternalResourcesCRLSource;
import eu.europa.esig.dss.spi.x509.revocation.crl.OfflineCRLSource;
import eu.europa.esig.dss.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class if a basic skeleton that is able to retrieve needed CRL data from
 * the contained list. The child need to retrieve the list of wrapped CRLs.
 */
public class AtexoCrlSource extends OfflineCRLSource {

    private static final long serialVersionUID = -985602836642741439L;

    private static final Logger LOG = LoggerFactory.getLogger(ExternalResourcesCRLSource.class);

    public void addCRLToken(final InputStream inputStream) {
        try (InputStream is = inputStream) {
            addCRLBinary(Utils.toByteArray(is), null);
        } catch (IOException e) {
            throw new DSSException(e);
        }
    }
}
