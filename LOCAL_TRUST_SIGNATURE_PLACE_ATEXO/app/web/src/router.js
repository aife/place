import Vue from 'vue';
import Router from 'vue-router';
import Login from "./pages/Login";
import Home from "./pages/Home";
import VueJwtDecode from 'vue-jwt-decode'
import * as moment from 'moment';

Vue.use(Router);

var router = new Router({
    routes: [

        {
            path: '/',
            redirect: {name: 'dashboard-signature'}
        },
        {
            path: '/dashboard-signature',
            name: 'dashboard-signature',
            component: Home
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/profile',
            name: 'profile',
            // lazy-loaded
            component: () => import('./pages/Profile.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/signed-files',
            component: () => import('./pages/Signed-files.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/validation',
            component: () => import('./pages/SignatureValidation.vue'),
            meta: {
                requiresAuth: true
            }
        }, {
            path: '/outils',
            component: () => import('./pages/SignatureOutils.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/swagger-ui/:id',
            component: () => import('./pages/Swagger.vue'),
            meta: {
                requiresAuth: true
            }
        },
       , {
            path: '/configuration',
            component: () => import('./pages/Configuration.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '*',
            redirect: {name: 'dashboard-signature'}
        },
    ],
    scrollBehavior() {
        return {x: 0, y: 0};
    }
});


router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem('user'));
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!user) {
            next({path: '/dashboard-signature'});
        } else {
            var token = VueJwtDecode.decode(user.token);
            const expirationDate = new Date(0);
            expirationDate.setUTCSeconds(token.exp);
            var now = new Date();
            if (moment(expirationDate).isBefore(now)) {
                localStorage.removeItem('user');
                next({path: '/dashboard-signature'});
            } else {
                next();
            }
        }

    } else {
        next();
    }
});

export default router;
