export default class ParametreModel {
    constructor(nom, valeur, description) {
        this.nom = nom;
        this.valeur = valeur;
        this.description = description;
        this.newValeur = valeur;
    }
}
