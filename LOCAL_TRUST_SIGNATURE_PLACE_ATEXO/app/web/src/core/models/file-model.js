export default class FileModel {
    constructor() {
        this.fileCandidature = new File([], "");
        this.signatureCandidature = new File([], "");
        this.fileOffreLot2 = new File([], "");
        this.fileOffreLot1 = new File([], "");
        this.signatureOffreLot1 = new File([], "");
        this.signatureOffreLot2 = new File([], "");
    }

}
