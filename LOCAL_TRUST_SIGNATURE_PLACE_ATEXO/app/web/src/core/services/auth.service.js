import axios from './api';
import {config} from '../../../config/env.config'

const API_SIGNATURE_URL = config.ROOT_API_PREFIX + '/signature/v1/auth/';

class AuthService {
    login(user) {
        return axios
            .post(API_SIGNATURE_URL + 'signin', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_SIGNATURE_URL + 'signup', {
            username: user.username,
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();
