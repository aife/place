export default function authToken() {
  let user = JSON.parse(localStorage.getItem('user'));

  if (user && user.token) {
    return  'Bearer ' + user.token ; // for Spring Boot back-end
    // return { 'x-access-token': user.token };       // for Node.js Express back-end
  } else {
    return null;
  }
}
