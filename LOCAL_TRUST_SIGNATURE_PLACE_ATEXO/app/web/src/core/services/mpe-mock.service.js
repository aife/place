import axios from './api';
import {config} from '../../../config/env.config'

const API_SIGNATURE_URL_V2 = config.ROOT_API_PREFIX + '/signature/v2/mpeMock/';

class MpeMockService {

    signatureValidationUpload(file) {
        return axios
            .post(API_SIGNATURE_URL_V2 + 'validation', file)
            .then(response => {

                return response;
            });
    }
    signFile(file) {
        return axios
            .post(API_SIGNATURE_URL_V2 + 'signature-en-ligne', file)
            .then(response => {

                return response;
            });
    }


    genererSignatureJnlp(typeJnlp) {
        return axios
            .get(API_SIGNATURE_URL_V2 + 'signatureJNLP', {
                params: {
                    useragent: navigator.userAgent,
                    typeJnlp: typeJnlp
                }
            })
            .then(response => {

                return response;
            });
    }

}

export default new MpeMockService();
