import axios from './api';
import {config} from '../../../config/env.config'

const API_URL_V2 = config.ROOT_API_PREFIX + '/signature/v2/admin/parametrage/';

class ConfigurationService {
    getAll() {

        return axios
            .get(API_URL_V2)
            .then(response => {
                return response.data;
            });
    }

    editParametre(parametre) {
        return axios
            .post(API_URL_V2, parametre)
            .then(response => {
                return response.data;
            });
    }

}

export default new ConfigurationService();
