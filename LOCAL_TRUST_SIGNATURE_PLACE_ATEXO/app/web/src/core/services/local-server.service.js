import axios from './api';

const LOCAL_SERVER_URL = 'https://mon-assistant.local-trust.com:11993/';

class LocalServerService {
    ping() {
        return axios
            .get(LOCAL_SERVER_URL + 'health')
            .then(response => {
                return response.data;
            });
    }

    runLocalApplication(params) {
        return axios
            .post(LOCAL_SERVER_URL + 'run-local-application', params)
            .then(response => {
                return response.data;
            });
    }
}

export default new LocalServerService();
