import axios from 'axios';

const RESOURCE_URL = 'https://ressources.local-trust.com/telechargements';

class ResourcesService {
    getVersion(env, system) {
        let url = `${RESOURCE_URL}`
        if (env !== 'prod') {
            url += '-' + env
        }

        url += '/latest';
        switch (system) {
            case 'mac':
                url += '-mac'
                break;
            case 'linux':
                url += '-linux'
                break;

        }
        return axios
            .get(url + '.yml')
            .then(response => {
                return response.data;
            });
    }

    download(env, system) {
        let url = `${RESOURCE_URL}`
        if (env !== 'prod') {
            url += '-' + env
        }
        url += '/mon-assistant-marche-public'
        if (env !== 'prod') {
            url += '-' + env
        }
        switch (system) {
            case 'mac':
                return url += '.dmg'
            case 'win':
                return url += '.exe'
            case 'linux':
                return url += '.AppImage'

        }
    }
}

export default new ResourcesService();
