import axios from './api';
import {config} from '../../../config/env.config'

const API_URL_V2 = config.ROOT_API_PREFIX + '/signature/v2/monitoring/';

class MonitoringService {
    getSignedFilesMonitoring(type, filter) {
        var url = API_URL_V2 + 'signed-files';
        if (!!type && !!filter) {
            url += '?type=' + type + '&filtre=' + filter;
        } else if (!!type) {
            url += '?type=' + type;
        } else {
            url += '?filtre=' + filter;
        }
        return axios
            .get(url)
            .then(response => {
                return response.data;
            });
    }

    downloadSignedFiles(nom) {
        return axios
            .get(API_URL_V2 + "signed-files/download?nom=" + nom, {
                responseType: "arraybuffer",
            })
            .then(response => {
                return response.data;
            });
    }

}

export default new MonitoringService();
