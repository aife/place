import MpeMockService from '../core/services/mpe-mock.service';


export const mpeMock = {
    namespaced: true,

    actions: {
        genererSignatureJnlp({commit}, typeJnlp) {
            return MpeMockService.genererSignatureJnlp(typeJnlp).then(
                response => {
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement("a");
                    link.href = url;
                    link.setAttribute("download", typeJnlp + '.jnlp');
                    document.body.appendChild(link);
                    link.click();
                    link.parentNode.removeChild(link)
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        signatureValidationUpload({commit}, file) {
            var formData = new FormData();

            formData.append("fichier", file.fileCandidature)
            formData.append("signature", file.signatureCandidature)

            return MpeMockService.signatureValidationUpload(formData).then(
                reponse => {
                    return Promise.resolve(reponse);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        }, signFile({commit}, file) {
            var formData = new FormData();

            formData.append("file", file)

            return MpeMockService.signFile(formData).then(
                reponse => {
                    return Promise.resolve(reponse);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
    }
};
