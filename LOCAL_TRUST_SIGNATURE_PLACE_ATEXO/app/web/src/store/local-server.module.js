import LocalServerService from "../core/services/local-server.service";

const initialState = {
    isActive: false
};

export const localServer = {
    namespaced: true,
    state: initialState,
    actions: {

        ping({commit}) {
            return LocalServerService.ping().then(
                response => {
                    commit('pingSuccess');
                    return Promise.resolve(response);
                },
                error => {
                    commit('pingFailure');
                    return Promise.reject(error);
                }
            );
        }, runLocalApplication({commit}, params) {
            return LocalServerService.runLocalApplication(params).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },

    },
    mutations: {
        pingSuccess(state) {
            state.isActive = true;
        },
        pingFailure(state) {
            state.isActive = false;
        },
    }
};
