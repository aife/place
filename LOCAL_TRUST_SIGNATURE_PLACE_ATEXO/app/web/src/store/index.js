import Vue from 'vue';
import Vuex from 'vuex';

import {auth} from './auth.module';
import {configuration} from './configuration.module';
import {mpeMock} from "./mpe-mock.module";
import {localServer} from "./local-server.module";
import {monitoring} from "./monitoring.module";
import {resources} from "@/store/resources.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth, mpeMock, configuration, localServer, monitoring, resources
    }
});
