import MonitoringService from '../core/services/monitoring.service';

const initialState = {
    signedFiles: []
};

export const monitoring = {
    namespaced: true,
    state: initialState,
    actions: {
        getSignedFilesMonitoring({commit}, param) {
            return MonitoringService.getSignedFilesMonitoring(param.type, param.filtre).then(
                list => {
                    commit('signedFilesSuccess', list);
                    return Promise.resolve(list);
                },
                error => {
                    commit('signedFilesFailure');
                    return Promise.reject(error);
                }
            );
        },
        downloadSignedFile({}, nom) {
            return MonitoringService.downloadSignedFiles(nom).then(
                response => {
                    const url = window.URL.createObjectURL(new Blob([response]));
                    const link = document.createElement("a");
                    link.href = url;
                    link.setAttribute("download", nom);
                    document.body.appendChild(link);
                    link.click();
                    link.parentNode.removeChild(link)
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        }

    },
    mutations: {
        signedFilesFailure(state) {
            state.signedFiles = [];
        },
        signedFilesSuccess(state, list) {
            state.signedFiles = list
        }
    }
};
