import ConfigurationService from '../core/services/configuration.service';
import ParametreModel from "../core/models/parametre-model";

const initialState = {parametres: []};

export const configuration = {
    namespaced: true,
    state: initialState,
    actions: {
        getAll({commit}) {
            return ConfigurationService.getAll().then(
                list => {
                    commit('configurationSuccess', list.map(value => new ParametreModel(value.nom, value.valeur, value.description)));
                    return Promise.resolve(list);
                },
                error => {
                    commit('configurationFailure');
                    return Promise.reject(error);
                }
            );
        },
        editParametre({commit}, params) {
            params.valeur = params.newValeur;
            return ConfigurationService.editParametre(params).then(
                response => {
                    commit('modificationSuccess', response.map(value => new ParametreModel(value.nom, value.valeur, value.description)));
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        }
    },
    mutations: {
        configurationSuccess(state, list) {
            state.parametres = list
        },
        modificationSuccess(state, list) {
            state.parametres = list
        },
        configurationFailure(state) {
            state.parametres = [];
        }
    }
};
