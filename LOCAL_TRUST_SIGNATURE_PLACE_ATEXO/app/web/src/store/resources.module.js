import ResourcesService from "../core/services/resources.service";

const initialState = {
    isActive: false
};

export const resources = {
    namespaced: true,
    state: initialState,
    actions: {

        getVersion({commit}, params) {
            return ResourcesService.getVersion(params.env, params.system).then(
                response => {
                    return Promise.resolve(response);
                },
                error => {
                    return Promise.reject(error);
                }
            );
        },
        download({commit}, params) {
            return window.location.assign(ResourcesService.download(params.env, params.system));
        },

    },
    mutations: {}
};
