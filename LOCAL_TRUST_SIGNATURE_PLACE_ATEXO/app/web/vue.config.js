module.exports = {
    devServer: {
        proxy: {
            "/signature/": {
                target: "http://localhost:8080/"
            }
        },
        port: 8083
    },
    publicPath: 'admin-signature'
}
