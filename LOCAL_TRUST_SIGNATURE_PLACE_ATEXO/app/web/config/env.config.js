let config;

if (process.env.NODE_ENV === "production") {
    config = {
        ROOT_API_PREFIX: "/api"
    };
} else {
    config = {
        ROOT_API_PREFIX: ""

    };
}

export { config }
