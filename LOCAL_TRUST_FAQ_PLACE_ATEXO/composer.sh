#!/bin/bash
# Entête correcte pour un script Bash.
phpbin=$(which php8.1 || which php)
composerbin=$(which composer)
"${phpbin}" ${composerbin} install --no-plugins --no-interaction --no-ansi --no-dev --ignore-platform-reqs --no-scripts

# Hack : on appelle 2 fois pour la CI sur Jenkins (pb quand pas de BDD)
"${phpbin}" ./bin/console "assets:install"  -e prod  "public/faq" || "${phpbin}" ./bin/console "assets:install" -e prod "public/faq"
"${phpbin}" ./bin/console "fos:js-routing:dump"  -e prod  "--format=json" "--target=public/faq/js/fos_js_routes.json"
