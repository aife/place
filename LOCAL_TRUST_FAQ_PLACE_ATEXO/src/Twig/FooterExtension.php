<?php

    namespace App\Twig;

    use App\Service\FooterService;
    use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
    use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
    use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
    use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
    use Twig\Extension\AbstractExtension;
    use Twig\TwigFunction;

    class FooterExtension extends AbstractExtension
    {
        public function __construct(private readonly FooterService $footerService)
        {
        }

        /**
         * @throws ServerExceptionInterface
         * @throws RedirectionExceptionInterface
         * @throws ClientExceptionInterface
         */
        public function getFunctions(): array
        {
            return [
                new TwigFunction('footerPlace', [$this, 'getFooter']),
                new TwigFunction('footerStylePlace', [$this, 'getFooterStyle']),
            ];
        }

        /**
         * @throws TransportExceptionInterface
         * @throws ServerExceptionInterface
         * @throws RedirectionExceptionInterface
         * @throws ClientExceptionInterface
         */
        public function getFooter(): string
        {
            return $this->footerService->getFooterFromMPE();
        }

        public function getFooterStyle(): string
        {
            return $this->footerService->getFooterStyleFromMPE();
        }
    }
