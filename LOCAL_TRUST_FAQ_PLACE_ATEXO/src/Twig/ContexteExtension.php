<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\Environment;

/**
 * Class ContexteExtension.
 *
 * Filtre Twig 'contexte' afin de bien rendre les différents types de champs de contextes
 * tels que les adresses ou les établissements.
 */
class ContexteExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('contexte', $this->contexteFilter(...), [
                'needs_environment' => true,
            ]),
        ];
    }

    public function contexteFilter(Environment $env, $valeur, string $type_valeur, string $locale = 'fr')
    {
        switch ($type_valeur) {
            case 'boolean':
                return $this->renderBoolean($valeur);
            case 'double':
                return $this->renderDouble($valeur);
            case 'datetime':
                return $this->renderDatetime($env, $valeur, $locale);
            case 'url':
                return $this->renderUrl($valeur);
            case 'mailto':
                return $this->renderMailTo($valeur);
            case 'entreprise':
                return $this->renderEntreprise($valeur['nom'], $valeur['siren'], $valeur['code_postal']);
            case 'etablissement':
                return $this->renderEtablissement($valeur['nom'], $valeur['siret']);
            case 'organisme_achat':
                return $this->renderOrganismeAchat($valeur['entite_publique'], $valeur['entite_achat']);
            case 'adresse':
                return $this->renderAdresse($valeur, true);
            default:
                return $valeur;
        }
    }

    private function renderBoolean($val)
    {
        return $val == true ? 'Oui' : 'Non';
    }

    private function renderDouble($val)
    {
        return number_format($val, 2, ',', ' ');
    }

    private function renderDatetime($env, $datetime, $locale)
    {
//        return local($env, $datetime, 'short', 'short', $locale);
        return $datetime;
    }

    private function renderUrl($url)
    {
        return '<a href="'.$url.'" target="_blank" title="Ouvrir dans un nouvel onglet">Ouvrir dans un nouvel onglet <i class="fa fa-external-link" aria-hidden="true"></i></a>';
    }

    private function renderMailTo($email)
    {
        return '<a href="mailto:'.$email.'" title="Écrire à...">'.$email.'</a>';
    }

    private function renderEntreprise($nom, $siren, $code_postal)
    {
        $html = '<span><i class="fa fa-industry"></i> '.$nom.'</span><br>';
        $html .= '<span>- SIREN : '.$siren.'</span><br>';
        $html .= '<span>- Code postal : '.$code_postal.'</span>';

        return $html;
    }

    private function renderEtablissement($nom, $siret)
    {
        $html = '<span><i class="fa fa-industry"></i> '.$nom.'</span><br><span>- SIRET : '.$siret.'</span>';

        return  $html;
    }

    private function renderOrganismeAchat($entite_publique, $entite_achat)
    {
        $html = '<span><i class="fa fa-university"></i> '.$entite_publique.'</span><br>';
        $html .= '<span><i class="fa fa-arrow-right" aria-hidden="true"></i> '.$entite_achat.'</span>';

        return $html;
    }

    private function renderAdresse($lignes, $lien_carte = false)
    {
        $nb_lignes = is_countable($lignes) ? count($lignes) : 0;
        $i = 0;
        $html = '<adress>';
        foreach ($lignes as $ligne) {
            if (++$i == $nb_lignes) {
                // Dernier élement
                $html .= $ligne;
            } else {
                $html .= $ligne.'<br>';
            }
        }
        if ($lien_carte) {
            $adresse_encodee = htmlspecialchars(join(', ', $lignes));
            $url = 'https://www.google.fr/maps/search/'.$adresse_encodee;
            $html .= ' <a href="'.$url.'" target="_blank" title="Ouvrir sur une carte (nouvel onglet)"><i class="fa fa-map" aria-hidden="true"></i></a>';
        }
        $html .= '</adress>';

        return $html;
    }

    public function getName()
    {
        return 'contexte_extension';
    }
}
