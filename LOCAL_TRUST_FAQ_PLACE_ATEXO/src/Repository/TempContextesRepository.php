<?php

namespace App\Repository;

/**
 * TempContextesRepository.
 */
class TempContextesRepository extends \Doctrine\ORM\EntityRepository
{
    public function deleteOldTempContextes($nbJours = 10)
    {
        $limitDate = (new \DateTime())->modify("-$nbJours days");

        $qb = $this->createQueryBuilder('tc');
        $qb->delete('App:TempContextes tc')
            ->where('tc.createdAt < :limitDate')
            ->setParameter('limitDate', $limitDate);

        return $qb->getQuery()->execute();
    }
}
