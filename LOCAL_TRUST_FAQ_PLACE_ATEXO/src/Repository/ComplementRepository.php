<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Complement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ComplementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Complement::class);
    }

    public function getComplementsByClientAndArticles(Client $client, array $articlesFAQ): array
    {
        $results = $this->createQueryBuilder('c')
            ->select('a.id as articleId, c.complement')
            ->leftJoin('c.clients', 'cl')
            ->leftJoin('c.article', 'a')
            ->where('cl.id = :clientId')
            ->andWhere('a.id IN (:articles)')
            ->setParameters(
                [
                    'clientId' => $client->getId(),
                    'articles' => $articlesFAQ
                ]
            )
            ->getQuery()
            ->getArrayResult()
        ;

        $complements = [];
        foreach ($results as $result) {
            $complements[$result['articleId']] = $result['complement'];
        }

        return $complements;
    }
}
