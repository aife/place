<?php

namespace App\Security;

use App\Service\EncoderService;
use App\Service\WebService;
use Psr\Log\LoggerInterface;

class KeycloakAuthChecker
{
    private readonly WebService $webService;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly EncoderService $encoderService,
        WebService $webService
    ) {
        $this->webService = $webService;
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \JsonException
     */
    public function checkApplicationAuthenticationValidity(?string $encodedContext): ?string
    {
        $this->logger->info(
            sprintf("Vérification de la validité du contexte : %s", $encodedContext)
        );

        if (
            is_null($encodedContext)
            || !$this->encoderService->isBase64Encoded($encodedContext)
            || !($decodedContent = $this->encoderService->getDecodedContext($encodedContext))
        ) {
            $errorMessage = "Erreur : Le contexte est manquant ou mal formé!";
            $this->logger->error($errorMessage);

            return $errorMessage;
        }

        $errorMessage = "Impossible de récupérer le contexte de votre requête. \n
         Veuillez recommencer votre demande d'assistance en ligne depuis le début ou contactez le support informatique.";
        if (
            !isset($decodedContent['applicatif']['champs']['access_token']['valeur'])
            || !($accessToken = $decodedContent['applicatif']['champs']['access_token']['valeur'])
        ) {
            $this->logger->error("Impossibilité de valider l'authentification avec keycloak car l'access_token est inexistant!");

            return $errorMessage;
        }

        if (!$this->webService->getAuthenticationConfirmation($accessToken)) {
            $this->logger->error("Impossibilité de valider l'authentification avec keycloak car l'access_token est invalide!");

            return $errorMessage;
        }

        $this->logger->info("Fin de vérification de la validité!");

        return null;
    }
}