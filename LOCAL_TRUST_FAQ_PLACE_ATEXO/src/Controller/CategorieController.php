<?php

namespace App\Controller;

use App\Entity\Acteur;
use App\Entity\Categorie;
use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * Faire le rendu d'un Widget de dropdown de catégorie selon la catégorie parente, le client, l'acteur...
     *
     * @param string         $id         attribut id de l'input
     * @param string         $label      Label du dropdown
     * @param null|Client   $client    Filtrage par client
     * @param null|Acteur    $acteur     Filtrage par acteur
     * @param null|Categorie $parent     La catégorie parente (null si catégorie racine)
     * @param bool           $autre      L'option "autre" est sélectionnable
     * @param bool           $required   Le champ est requis
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dropdownWidgetAction(string $id, string $label = 'Catégorie', Client $client = null,
                                         array  $progiciels = null, Acteur $acteur = null, Categorie $parent = null,
                                         bool   $autre = false, bool $required = false)
    {
        $repository = $this->getDoctrine()
            ->getRepository('App:Categorie');
        if (is_null($parent)) {
            //$categories = $repository->findCategoriesPremierNiveau($client, $acteur);
            $categories = $repository->findListByProgiciels($progiciels, $acteur);
        } else {
            $categories = $repository->findCategoriesFilles($parent);
        }

        return $this->render('fragments/categorie_dropdown.frag.html.twig', [
            'id' => $id,
            'label' => $label,
            'categories' => $categories,
            'autre' => $autre,
            'required' => $required,
        ]);
    }

    /**
     * Récupérer le dropdown de 2nde catégorie selon l'id de première catégorie.
     *
     * @Route("/ajax/cd/{id}", options={"expose"=true}, name="ajax_categorie_dropdown")
     *
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajaxDropdownWidgetAction(int $id)
    {
        $parent = $this->getDoctrine()->getEntityManager()
            ->getRepository('AppBundle:Categorie')->find($id);
        $option_autre = true;
        $option_required = false;

        return $this->dropdownWidgetAction('objet', 'Objet', null, null, $parent, $option_autre, $option_required);
    }
}
