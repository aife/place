<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Contexte;
use Symfony\Component\Routing\Annotation\Route;

class ContexteController extends AbstractController
{
    /**
     * Widget de Contexte.
     *
     * Effectue le rendu du fragment de contexte.
     *
     * @Route("/contexte/widget", name="contexte_widget")
     *
     * @param Contexte $contexte le contexte à afficher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetAction(Contexte $contexte)
    {
        return $this->render('fragments/contexte_widget.frag.html.twig', [
            'contexte' => $contexte,
        ]);
    }
}
