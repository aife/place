<?php
namespace App\Controller;

use App\Entity\TempContextes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TempContextesController extends AbstractController
{
    /**
     * @Route("/temp/contextes", name="add_temp_contextes",  methods={"GET","POST"})
     */
    public function addAction(Request $request)
    {
        $temp_contextes = new TempContextes();

        if ($request->getContentType() != 'json') {
            return new Response(json_encode(['error' => 'Content Type is not application/json.']));
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $temp_contextes->setData(json_encode($data, JSON_THROW_ON_ERROR));
        $temp_contextes->setIp($request->getClientIp());

        $em = $this->getDoctrine()->getManager();
        $em->persist($temp_contextes);
        $em->flush();

        return new Response(json_encode(['token' => $temp_contextes->getId()], JSON_THROW_ON_ERROR));
    }
}
