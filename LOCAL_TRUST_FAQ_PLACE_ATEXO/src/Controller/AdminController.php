<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AdminController extends Controller
{
    public function __construct(private readonly AuthorizationCheckerInterface $authorizationChecker)
    {
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('statistique_index');
    }

    /**
     * @Route("/admin/categorie", options={"expose"=true},name="administration_categorie")
     *
     * Query string:
     * 'confirm' Le type de message de confirmation
     */
    public function administrationCategorie(Request $request): Response
    {
        if (!$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->render('statistique_index.html.twig');
        }

        $filtre = [];
        // Filtres

        $filtre['filtre_client_id'] = $request->query->get('progiciel');
        $filtre['filtre_acteur_id'] = $request->query->get('acteur');
        $filtre['filtre_actif'] = $request->query->get('actif');
        $filtre['filtre_code'] = $request->query->get('code');
        $filtre['filtre_categorie_id'] = $request->query->get('categorie');

        // Sorting
        $filtre['sorting_field'] = $request->query->get('sort');
        $filtre['sorting_direction'] = $request->query->get('direction');

        //numéro de page
        $filtre['numero_page'] = $request->query->getInt('page', 1);

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $render_vars = $administrationReferentiel->getListeCategorie($filtre);

            $confirm = $request->query->get('confirm');
            if ($confirm == 1) {
                $message_confirm = 'La catégorie a bien été ajoutée';
            } elseif ($confirm == 2) {
                $message_confirm = 'La catégorie a bien été modifiée';
            } elseif ($confirm == 3) {
                $message_confirm = 'La catégorie a bien été supprimée';
            } else {
                $message_confirm = '';
            }
            $render_vars['message_confirm'] = $message_confirm;

            return $this->render('administration/administration_referentiel_categorie.html.twig', $render_vars);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Vérifier en AJAX les données de la catégorie, renvoyer les erreurs du formulaire
     * le cas échéant. l'enregistrer en Base.
     *
     * @Route("/ajax/categorie/process", options={"expose"=true}, name="ajax_categorie_process")
     *
     *
     */
    public function ajaxCategorieProcess(Request $request): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // ---------------------
        // Récupérer les données
        // ---------------------
        // Champs du formulaire
        $data = [];
        $data['categorie_nom'] = $request->get('categorie_nom');
        $data['categorie_id'] = $request->get('categorie_id');
        $data['progiciels'] = $request->get('progiciels');
        $data['acteurs'] = $request->get('acteurs');
        $data['actif'] = $request->get('actif');
        $data['code'] = $request->get('code');

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $json = $administrationReferentiel->traiterCategorie($data);
            $response->setContent($json);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Vérifier en AJAX les données de la catégorie, renvoyer les erreurs du formulaire
     * le cas échéant. le supprimer de la Base.
     *
     * @Route("/ajax/categorie/delete/process", options={"expose"=true}, name="ajax_supprimer_categorie_process")
     *
     *
     */
    public function ajaxCategorieDeleteProcess(Request $request): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // ---------------------
        // Récupérer les données
        // ---------------------
        // Champs du formulaire
        $data = [];
        $data['categorie_id'] = $request->get('categorie_id');

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $json = $administrationReferentiel->supprimerCategorie($data);
            $response->setContent($json);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @Route("/admin/faq",options={"expose"=true},name="administration_faq")
     *
     * Query string:
     * 'confirm' Le type de message de confirmation
     *
     * @return Response
     */
    public function administrationFaq(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_AGENT_SUPPORT')) {
            $authenticationUtils = $this->get('security.authentication_utils');
            $error = $authenticationUtils->getLastAuthenticationError();

            return $this->render('security/login.html.twig', ['error' => $error]);
        }

        $filtre = [];
        // Filtres
        $filtre['filtre_categorie_id'] = $request->query->get('categorie');
        $filtre['filtre_actif'] = $request->query->get('actif');

        // Sorting
        $filtre['sorting_field'] = $request->query->get('sort');
        $filtre['sorting_direction'] = $request->query->get('direction');

        //numéro de page
        $filtre['numero_page'] = $request->query->getInt('page', 1);

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $render_vars = $administrationReferentiel->getListeFaq($filtre);

            $confirm = $request->query->get('confirm');
            if ($confirm == 1) {
                $message_confirm = 'La question-reponse a bien été ajoutée';
            } elseif ($confirm == 2) {
                $message_confirm = 'La question-reponse a bien été modifiée';
            } elseif ($confirm == 3) {
                $message_confirm = 'La question-reponse a bien été supprimée';
            } else {
                $message_confirm = '';
            }
            $render_vars['message_confirm'] = $message_confirm;

            return $this->render('administration/administration_referentiel_faq.html.twig', $render_vars);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Vérifier en AJAX les données de l'article_faq, renvoyer les erreurs du formulaire
     * le cas échéant. le supprimer de la Base.
     *
     * @Route("/ajax/faq/delete/process", options={"expose"=true}, name="ajax_supprimer_faq_process")
     *
     *
     * @return Response
     */
    public function ajaxArticleFaqDeleteProcessAction(Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // ---------------------
        // Récupérer les données
        // ---------------------
        // Champs du formulaire
        $data = [];
        $data['articleFaq_id'] = $request->get('articleFaq_id');

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $json = $administrationReferentiel->supprimerArticleFaq($data);
            $response->setContent($json);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Vérifier en AJAX les données de l'article_faq, renvoyer les erreurs du formulaire
     * le cas échéant. l'enregistrer en Base.
     *
     * @Route("/ajax/faq/process", options={"expose"=true}, name="ajax_faq_process")
     *
     *
     * @return Response
     */
    public function ajaxFaqProcessAction(Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // ---------------------
        // Récupérer les données
        // ---------------------
        // Champs du formulaire
        $data = [];
        $data['articleFaq_id'] = $request->get('articleFaq_id');
        $data['categories'] = $request->get('categories');
        $data['question'] = $request->get('question');
        $data['reponse'] = $request->get('reponse');
        $data['actif'] = $request->get('actif');

        try {
            $administrationReferentiel = $this->get('app_bundle.management.administration');
            $json = $administrationReferentiel->traiterArticleFaq($data);
            $response->setContent($json);

            return $response;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
