<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * SecurityController constructor.
     * @param $authetificationUtils
     */
    public function __construct(private readonly AuthenticationUtils $authetificationUtils)
    {
    }

    /**
     * @Route("/login", name="login")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new RedirectResponse($this->generateUrl('sonata_admin_dashboard'));
        }

        $error = $this->authetificationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', ['error' => $error]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(): never
    {
        // On ne devrait pas entrer ici
        throw new \Exception('Security: Bad configuration');
    }
}
