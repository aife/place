<?php

namespace App\Controller;

use App\Entity\ArticleFAQ;
use App\Entity\Categorie;
use App\Entity\Client;
use App\Repository\ArticleFAQRepository;
use App\Repository\ClientRepository;
use App\Entity\Complement;
use App\Repository\ComplementRepository;
use App\Service\EncoderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleFAQController extends AbstractController
{

    final public const CONTEXT = 'context';

    public function __construct(
        private readonly EncoderService $encoderService,
        private readonly SessionInterface $session,
        private readonly EntityManagerInterface $entityManager,
        private readonly ClientRepository $clientRepository,
        private readonly ArticleFAQRepository $articleFAQRepository,
        private readonly ComplementRepository $complementRepository
    )
    {
    }


    /**
     * Faire un rendu du Widget de suggestions de FAQ selon un id de catégorie.
     *
     * @Route("/ajax/faq/suggestions/{id_cat}", options={"expose"=true}, name="ajax_suggestions_widget")
     *
     *
     * @return mixed
     */
    public function ajaxSuggestionsWidgetAction(int $id_cat)
    {
        $categorie = $this->entityManager->getRepository(Categorie::class)->find($id_cat);
        $clients = $this->clientRepository->findByProgiciel($categorie->getProgiciels());
        $suggestions = $this->articleFAQRepository->findByCategorieAndClients($categorie, $clients);
        $context = $this->encoderService->getDecodedContext($this->session->get(self::CONTEXT));
        $plateforme = $context['applicatif']['champs']['application']['valeur'];
        $emailPlateforme = $this->entityManager->getRepository(Client::class)->findOneByNom($plateforme);

        $complements = $this->complementRepository->getComplementsByClientAndArticles(
            $this->entityManager->getRepository(Client::class)->findOneByNom($plateforme),
            $suggestions
        );

        return $this->render('fragments/suggestions_faq.frag.html.twig', [
            'suggestions' => $suggestions,
            'rubrique' => $categorie,
            'emailPlateforme' => $emailPlateforme,
            'complements' => $complements,
        ]);
    }
}
