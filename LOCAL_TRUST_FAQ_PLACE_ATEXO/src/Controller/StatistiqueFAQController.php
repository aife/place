<?php

namespace App\Controller;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StatistiqueFAQController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @Route("/statistique/", name="statistique_index")
     */
    public function index()
    {
        return $this->render('statistique_index.html.twig');
    }

    /**
     * @Route("/statistique/{id}", name="statistique_faq")
     */
    public function statistique($id)
    {
        $clientAgentSupport = $this->entityManager->getRepository(Client::class)->findClientsByAgentSupportId($this->getUser()->getId());
        $client = $this->entityManager->getRepository(Client::class)->findOneById($id);

        if (
            $client === null
            || empty($this->entityManager->getRepository(Client::class)->findClientsByAgentSupportIdAndMatomoId($this->getUser(), $id))
        ) {
            throw new \Exception('Client inexistant ou ne faisant pas partie du périmétre de l\'agent support');
        }


        return $this->render('statistiqueFAQ.html.twig', [
            'client' => $client,
            'clientAgentSupport' => $clientAgentSupport,
        ]);
    }
}
