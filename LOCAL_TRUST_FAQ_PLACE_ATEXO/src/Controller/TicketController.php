<?php
namespace App\Controller;

use App\Entity\Acteur;
use App\Entity\ArticleFAQ;
use App\Entity\Categorie;
use App\Entity\Contexte;
use App\Entity\Client;
use App\Entity\Progiciel;
use App\Entity\Recurrence;
use App\Entity\Severite;
use App\Entity\TempContextes;
use App\Entity\Ticket;
use App\Management\Ticket\TraitementTicket;
use App\Security\KeycloakAuthChecker;
use App\Service\EncoderService;
use App\Service\External\Utah;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

class TicketController extends AbstractController
{
    final public const CONTEXT = 'context';
    final public const PARAM_TOKEN = 'token';

    private readonly KeycloakAuthChecker $keycloakAuthChecker;

    public function __construct(
        private readonly Utah $utah,
        private readonly EncoderService $encoderService,
        KeycloakAuthChecker $keycloakAuthChecker,
        private readonly SessionInterface $session
    ) {
        $this->keycloakAuthChecker = $keycloakAuthChecker;
    }

    /**
     * Écran de création d'une demande d'aide.
     *
     * @Route("", name="ticket_creation")
     *
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function creation(Request $request, SessionInterface $session): Response
    {
        $encodedContext = $request->get(self::CONTEXT);
        $encodedContext = $encodedContext !== null ? $this->encoderService->sanitizeEncodedContent($encodedContext) : null;

        // Retro-compatibilité avec identification token
        $paramToken = $request->get(self::PARAM_TOKEN);

        /***** Check context validity and auth keycloak *****/
        if (
            ($errorMessage = $this->keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext))
            && !$paramToken
        ) {
            return $this->render('error_authentication.html.twig', [
                'message_erreur' => $errorMessage
            ]);
        }

        if ($paramToken) {
            $encodedContext = $this->encoderService->getEncodedContext($this->getContextes($paramToken));
        }

        $session->set(self::CONTEXT, $encodedContext);

        return $this->loadCreationAction($encodedContext, $paramToken);
    }

    /**
     * @Route("/utah/{categorieId}", options={"expose"=true}, name="ajax_goto_utah")
     *
     * @entity("categorie", expr="repository.find(categorieId)")
     *
     *
     * @throws \JsonException
     */
    public function ajaxGotoUtahAction(Categorie $categorie, SessionInterface $session): Response
    {
        $context = $this->encoderService->getDecodedContext($session->get(self::CONTEXT));
        $contextes = $this->encoderService->getFormattedDataContextes($context);

        $urlUtah = $contextes['applicatif']->getValeur('url_utah');

        $newContextes = $this->encoderService->getEncodedContext($this->addContexte($context, $categorie));
        $session->set(self::CONTEXT, $newContextes);

        try {
            $content = json_encode([self::CONTEXT => $newContextes], JSON_THROW_ON_ERROR);
            $code = Response::HTTP_OK;
        } catch (\Exception){
            $content = json_encode(['message' => 'Il y a eu une erreur lors de la validation du formulaire !']);
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return (new Response())->setContent($content)->setStatusCode($code);
    }

    /**
     * @Route("/utah/{categorieId}/{id}", options={"expose"=true}, name="ajax_goto_utah_with_token")
     *
     * @entity("categorie", expr="repository.find(categorieId)")
     * @ParamConverter("tempContextes", options={"mapping": {"id": "id"}})
     *
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function ajaxGotoUtahWithToken(Categorie $categorie, TempContextes $tempContextes): Response
    {
        $contextes = $this->getDataContextes($tempContextes);

        $urlUtah = $contextes['applicatif']->getValeur('url_utah');
        $tempContextes = $this->addContexteViaToken($tempContextes, $categorie);

        try{
            $tokenUtah = $this->utah->getToken($urlUtah, $tempContextes);
            $content = json_encode(['tokenUtah' => $tokenUtah]);;
            $code = Response::HTTP_OK;
        } catch (\Exception){
            $content = json_encode(['message' => 'Il y a eu une erreur lors de la validation du formulaire !']);
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        return (new Response())->setContent($content)->setStatusCode($code);
    }

    /**
     * Redirection vers l'écran de création de ticket.
     *
     * @throws \JsonException
     */
    private function loadCreationAction(string $encodedContext, ?string $token = null): Response
    {
        $contexte = null;
        $displayedFields = [];
        $noSuivi = $client = $progiciels = $acteur = $contextes = [];

        $context = $this->encoderService->getDecodedContext($encodedContext);
        $contextes = $this->encoderService->getFormattedDataContextes($context);

        $nom_client = $contextes['applicatif']->getValeur('application');
        $type_acteur = $contextes['applicatif']->getValeur('type_acteur');
        $client = $this->getDoctrine()->getRepository(Client::class)->findOneByNom($nom_client);
        $progiciels = $this->getDoctrine()->getRepository(Progiciel::class)->findListByClient($client);
        $acteur = $this->getDoctrine()->getRepository(Acteur::class)->findOneByNom($type_acteur);
        $urlUtah = $contextes['applicatif']->getValeur('url_utah');

        // Mise en session de l'id matomo correspondant au client (client) pour l'injecter dans le header
        $this->session->set('matomoId', $client->getMatomoId());

        foreach($contextes as $key => $contexte){
            $displayedFields = [];
            if(is_array($contexte->getChamps())){
                foreach($contexte->getChamps() as $k => $champ){
                    if($champ['visible_utilisateur']){
                        $displayedFields[] = $champ;
                    }
                }
                $contexte->setDisplayedFields($displayedFields);
            }

        }

        $contexte->setDisplayedFields($displayedFields);

        return $this->render('index.html.twig', [
            'urlUtah' => $urlUtah,
            'noSuivi' => $noSuivi,
            'client' => $client,
            'progiciels' => $progiciels,
            'acteur' => $acteur,
            'contextes' =>  $contextes,
            'token' => $token,
        ]);
    }

    /**
     * @return array
     * @throws EntityNotFoundException
     */
    private function getContextes(string $token)
    {
        $em = $this->getDoctrine()->getManager();
        $tempContexte = $em->getRepository(TempContextes::class)->find($token);

        if (is_null($tempContexte) || !$tempContexte->getData()) {
            throw new EntityNotFoundException("Les contextes temporaires n'ont pas été trouvés.");
        }

        return json_decode((string) $tempContexte->getData(), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     */
    private function getDataContextes(TempContextes $tempContextes)
    {
        $data = json_decode($tempContextes->getData(), true, 512, JSON_THROW_ON_ERROR);
        $contextes = [];
        foreach ($data as $key => $value) {
            $contextes[$key] = Contexte::fromArray($value);
        }
        return $contextes;
    }

    public function addContexte(array $contextes, Categorie $categorie): array
    {
        $name = 'faq_categorie_id';
        $contextes['applicatif']['champs'][$name] = $this->getChamp($name, $categorie->getId());

        $name = 'faq_categorie_nom';
        $contextes['applicatif']['champs'][$name] = $this->getChamp($name, $categorie->getNom());

        $name = 'faq_sous_categorie_nom';
        $contextes['applicatif']['champs'][$name] = $this->getChamp($name, $this->session->get('sousCategorie'));

        $name = 'faq_categorie_code';
        $contextes['applicatif']['champs'][$name] = $this->getChamp($name, $categorie->getCode());

        return $contextes;
    }

    public function addContexteViaToken(TempContextes $tempContextes, Categorie $category): TempContextes
    {
        $data = json_decode($tempContextes->getData(), true, 512, JSON_THROW_ON_ERROR);

        $name = 'faq_categorie_id';
        $data['applicatif']['champs'][$name] = $this->getChamp($name, $category->getId());

        $name = 'faq_categorie_nom';
        $data['applicatif']['champs'][$name] = $this->getChamp($name, $category->getNom());

        $name = 'faq_sous_categorie_nom';
        $data['applicatif']['champs'][$name] =  $this->getChamp($name, $this->session->get('sousCategorie'));

        $name = 'faq_categorie_code';
        $data['applicatif']['champs'][$name] = $this->getChamp($name, $category->getCode());

        $tempContextes->setData(json_encode($data, JSON_THROW_ON_ERROR));

        return $tempContextes;
    }

    /**
     * @param $name
     * @param $valeur
     * @return array
     */
    public function getChamp($name, $valeur, bool $visibleUtilisateur = false, bool $visibleSupport = false)
    {
        return [
            'label' => $name,
            'description' => $name,
            'valeur' => $valeur,
            'type_valeur' => 'string',
            'visible_utilisateur' => $visibleUtilisateur,
            'visible_support' => $visibleSupport,
            'extras' => [],
        ];
    }

    /**
     *
     *
     * @Route("/ajax/sous_categorie/{idCategorie}", options={"expose"=true}, name="set_session_sous_categorie")
     *
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setSessionSousCategorie(int $idCategorie, SessionInterface $session)
    {
        $question = $this->getDoctrine()->getRepository(ArticleFAQ::class)->findOneById($idCategorie);

        $session->set('sousCategorie', $question->getTitle());

        return new JsonResponse('',204);
    }
}
