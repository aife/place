<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ClientRepository;

/**
 * Client.
 *
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 * @ORM\Table(name="client")

 */
class Client implements \Stringable
{

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $nom;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $host = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $icon_src = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $logo_src = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $bandeau_src = null;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $email_notif = null;

    /**
     * @ORM\ManyToMany(targetEntity="Progiciel", inversedBy="clients")
     */
    private Collection $progiciels;

    /**
     * @ORM\Column(name="matomoId", type="integer", nullable=true)
     */
    private ?int $matomoId = null;

    /**
     * Many Products have Many AgentSupports.
     *
     * @ORM\ManyToMany(targetEntity=AgentSupport::class, inversedBy="clients")
     */
    private Collection $agentSupports;

    /**
     * @ORM\OneToMany(targetEntity=ArticleFAQ::class, mappedBy="client")
     */
    private Collection $articles;

    /**
     * @ORM\ManyToMany(targetEntity=Complement::class, inversedBy="clients")
     */
    private Collection $complements;

    /**
     * @ORM\Column(name="num_support", type="string", length=32, nullable=true)
     */
    private ?string $num_support = null;


    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->progiciels = new ArrayCollection();
        $this->agentSupports = new ArrayCollection();
        $this->complements = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    public function setHost(string $host): Client
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getIconSrc()
    {
        return $this->icon_src;
    }

    public function setIconSrc(string $icon_src): Client
    {
        $this->icon_src = $icon_src;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogoSrc()
    {
        return $this->logo_src;
    }

    public function setLogoSrc(string $logo_src): Client
    {
        $this->logo_src = $logo_src;
        return $this;
    }

    /**
     * @return string
     */
    public function getBandeauSrc()
    {
        return $this->bandeau_src;
    }

    public function setBandeauSrc(string $bandeau_src): Client
    {
        $this->bandeau_src = $bandeau_src;
        return $this;
    }

    public function getProgiciels(): ArrayCollection
    {
        return $this->progiciels;
    }

    public function setProgiciels(ArrayCollection $progiciels)
    {
        $this->progiciels = $progiciels;
    }

        /**
     * @return string
     */
    public function getEmailNotif()
    {
        return $this->email_notif;
    }

    public function setEmailNotif(string $email_notif)
    {
        $this->email_notif = $email_notif;
    }

    /**
     * @return int
     */
    public function getMatomoId(): ?int
    {
        return $this->matomoId;
    }

    /**
     * @param int $matomoId
     */
    public function setMatomoId(?int $matomoId): void
    {
        $this->matomoId = $matomoId;
    }

    /**
     * @return mixed
     */
    public function getAgentSupports()
    {
        return $this->agentSupports;
    }

    public function setAgentSupports(ArrayCollection $agentSupports)
    {
        $this->agentSupports = $agentSupports;
    }

    /**
     * @return $this
     */
    public function addAgentSupport(AgentSupport $agentSupport): self
    {
        if (!$this->agentSupports->contains($agentSupport)) {
            $this->agentSupports->add($agentSupport);
            $agentSupport->addClient($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAgentSupport(AgentSupport $agentSupport): self
    {
        if ($this->agentSupports->contains($agentSupport)) {
            $this->agentSupports->removeElement($agentSupport);
            $agentSupport->removeClient($this);
        }

        return $this;
    }

    public function getComplements(): Collection
    {
        return $this->complements;
    }

    public function setComplements(Collection $complements): void
    {
        $this->complements = $complements;
    }

    public function addComplement(Complement $complement): self
    {
        if (!$this->complements->contains($complement)) {
            $this->complements->add($complement);
            $complement->addClient($this);
        }

        return $this;
    }

    public function removeComplement(Complement $complement): self
    {
        if ($this->complements->contains($complement)) {
            $this->complements->removeElement($complement);
            $complement->removeClient($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getNumSupport(): ?string
    {
        $this->num_support = (is_null($this->num_support)) ? '' : $this->num_support;

        return $this->num_support;
    }

    /**
     * @param string|null $num_support
     */
    public function setNumSupport(?string $num_support): void
    {
        $this->num_support = $num_support;
    }

    /**
     * @return string
     */
    public function __toString(): string {
        return $this->nom;
    }

    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(ArticleFAQ $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setClient($this);
        }

        return $this;
    }

    public function removeArticle(ArticleFAQ $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getClient() === $this) {
                $article->setClient(null);
            }
        }

        return $this;
    }
}
