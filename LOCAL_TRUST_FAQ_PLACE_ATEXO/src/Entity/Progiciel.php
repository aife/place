<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Progiciel.
 *
 * @ORM\Table(name="progiciel")
 * @ORM\Entity(repositoryClass="App\Repository\ProgicielRepository")
 */
class Progiciel implements \Stringable
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private string $nom;

    /**
     * @ORM\Column(name="libelle", type="string")
     */
    private string $libelle;

    /**
     * @ORM\ManyToMany(targetEntity="Categorie", inversedBy="progiciels")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Client", mappedBy="progiciels", cascade={"remove"})
     * @ORM\JoinTable(name="client_progiciel",
     *     joinColumns={@ORM\JoinColumn(name="progiciel_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="client_id", referencedColumnName="id")})
     */
    private $clients;

    /**
     * @ORM\ManyToMany(targetEntity=AgentSupport::class, inversedBy="progiciels")
     */
    private $agentSupport;


    /**
     * Progiciel constructor.
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->clients = new ArrayCollection();
        $this->agentSupport = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addCategorie(Categorie $categorie)
    {
        if ($this->categories->contains($categorie)) {
            return;
        }
        $this->categories->add($categorie);
        $categorie->addProgiciel($this);

        return $this;
    }

    public function removeCategorie(Categorie $categorie)
    {
        if (!$this->categories->contains($categorie)) {
            return;
        }
        $this->categories->removeElement($categorie);
        $categorie->removeProgiciel($this);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Progiciel
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Progiciel
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @return ArrayCollection
     */
    public function getAgentSupport()
    {
        return $this->agentSupport;
    }

    /**
     * @return $this
     */
    public function addAgentSupport(AgentSupport $agentSupport): self
    {
        if (!$this->agentSupport->contains($agentSupport)) {
            $this->agentSupport->add($agentSupport);
            $agentSupport->addProgiciel($this);
        }

        return $this;
    }

    public function removeAgentSupport(AgentSupport $agentSupport)
    {
        if (!$this->agentSupport->contains($agentSupport)) {
            return;
        }
        $this->agentSupport->removeElement($agentSupport);
        $agentSupport->removeProgiciel($this);

        return $this;
    }


    public function __toString(): string
    {
        return $this->getNom();
    }
}
