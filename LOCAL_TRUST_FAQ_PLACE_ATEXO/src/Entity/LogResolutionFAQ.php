<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LogResolutionFAQ.
 *
 * @ORM\Table(name="log_resolution_faq")
 * @ORM\Entity(repositoryClass="App\Repository\LogResolutionFAQRepository")
 */
class LogResolutionFAQ
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     */
    private ?\App\Entity\Client $client = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Acteur")
     */
    private ?\App\Entity\Acteur $acteur = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie")
     */
    private ?\App\Entity\Categorie $categorie = null;

    /**
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private \DateTime $createdAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    public function getActeur(): Acteur
    {
        return $this->acteur;
    }

    /**
     * @return $this
     */
    public function setActeur(Acteur $acteur)
    {
        $this->acteur = $acteur;

        return $this;
    }

    public function getCategorie(): Categorie
    {
        return $this->categorie;
    }

    /**
     * @return $this
     */
    public function setCategorie(Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return LogResolutionFAQ
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
