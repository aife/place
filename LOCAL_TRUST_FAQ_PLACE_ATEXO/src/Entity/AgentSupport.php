<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * AgentSupport.
 *
 * @ORM\Table(name="agent_support")
 * @ORM\Entity()
 */
class AgentSupport implements  UserInterface, \Serializable
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="username", type="string", length=64, unique=true)
     */
    private string $username;

    /**
     * @ORM\Column(name="password", type="string", length=64)
     */
    private string $password;

    /**
     * @ORM\Column(name="email", type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\Column(name="display_name", type="string", length=128)
     */
    private string $displayName;

    /**
     * @ORM\Column(name="expired", type="boolean", nullable=false)
     */
    private bool $expired;

    /**
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private bool $locked;

    /**
     * @ORM\Column(name="credentials_expired", type="boolean", nullable=false)
     */
    private bool $credentialsExpired;

    /**
     * @ORM\Column(name="disabled", type="boolean", nullable=false)
     */
    private bool $disabled;

    /**
     * @ORM\ManyToMany(targetEntity=Client::class, mappedBy="agentSupports")
     * @ORM\JoinTable(name="client_agent_support", joinColumns={@ORM\JoinColumn(name="agent_support_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="client_id", referencedColumnName="id")})
     */
    private $clients;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", mappedBy="agentsSupport", cascade="persist")
     * @ORM\JoinTable(name="agent_support_role", joinColumns={@ORM\JoinColumn(name="agent_support_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")})
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity=Progiciel::class, mappedBy="agentSupport")
     * @ORM\JoinTable(name="progiciel_agent_support", joinColumns={@ORM\JoinColumn(name="agent_support_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="progiciel_id", referencedColumnName="id")})
     */
    private $progiciels;

    
    /**
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private \DateTime $createdAt;

    public function __construct()
    {
        $this->setExpired(false);
        $this->setLocked(false);
        $this->setCredentialsExpired(false);
        $this->setDisabled(false);
        $this->clients = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->progiciels = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return AgentSupport
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return AgentSupport
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return AgentSupport
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     *
     * @return AgentSupport
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function isExpired(): bool
    {
        return $this->expired;
    }

    public function setExpired(bool $expired): AgentSupport
    {
        $this->expired = $expired;

        return $this;
    }

    public function isLocked(): bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): AgentSupport
    {
        $this->locked = $locked;

        return $this;
    }

    public function isCredentialsExpired(): bool
    {
        return $this->credentialsExpired;
    }

    public function setCredentialsExpired(bool $credentialsExpired): AgentSupport
    {
        $this->credentialsExpired = $credentialsExpired;

        return $this;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): AgentSupport
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return null|string The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // Do nothing
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return !$this->expired;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return !$this->credentialsExpired;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return !$this->disabled;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([$this->id, $this->username, $this->password]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        [$this->id, $this->username, $this->password, ] = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = [];
        foreach ($this->roles as $role) {
            $roles[] = $role->getNom();
        }

        return $roles;
    }

    /**
     * @return $this
     */
    public function addRole(Role $role)
    {
        try {
            if ($this->roles->contains($role)) {
                return;
            }
            $this->roles->add($role);
            $role->addAgentSupport($this);

            return $this;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function removeRole(Role $role)
    {
        try {
            if (!$this->roles->contains($role)) {
                return;
            }
            $this->roles->removeElement($role);
            $role->removeAgentSupport($this);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $newRoles
     */
    public function removeRoles($newRoles)
    {
        try {
            foreach ($this->roles as $role) {
                if (!in_array($role->getId(), $newRoles)) {
                    $this->removeRole($role);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @return $this
     */
    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients->add($client);
            $client->addAgentSupport($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setClients(Collection $clients)
    {
        foreach ($clients->toArray() as $p) {
            $this->addClient($p);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            $client->removeAgentSupport($this);
        }

        return $this;
    }


    /**
     * @return ArrayCollection
     */
    public function getProgiciels()
    {
        return $this->progiciels;
    }

    /**
     * @return $this
     */
    public function setProgiciels(ArrayCollection $progiciels): self
    {
        foreach ($progiciels->toArray() as $p) {
            $this->addProgiciel($p);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addProgiciel(Progiciel $progiciel): self
    {
        if (!$this->progiciels->contains($progiciel)) {
            $this->progiciels->add($progiciel);
            $progiciel->addAgentSupport($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeProgiciel(Progiciel $progiciel): self
    {
        if ($this->progiciels->contains($progiciel)) {
            $this->progiciels->removeElement($progiciel);
            $progiciel->removeAgentSupport($this);
        }

        return $this;
    }
}
