<?php

namespace App\Entity;

use App\Entity\Progiciel;
use App\Entity\ArticleFAQ;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CategorieRepository;
use App\Entity\Acteur;

/**
 * Categorie.
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass=CategorieRepository::class)
 */
class Categorie implements \Stringable
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private string $nom;

    /**
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     * )
     */
    private bool $locked;

    /**
     * @ORM\ManyToMany(targetEntity=Progiciel::class, mappedBy="categories")
     * @ORM\JoinTable(name="progiciel_categorie", joinColumns={@ORM\JoinColumn(name="categorie_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="progiciel_id", referencedColumnName="id")})
     */
    private $progiciels;

    /**
     * @ORM\ManyToMany(targetEntity=Acteur::class, mappedBy="categories")
     * @ORM\JoinTable(name="acteur_categorie", joinColumns={@ORM\JoinColumn(name="categorie_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="acteur_id", referencedColumnName="id")})
     */
    private $acteurs;

    /**
     * @ORM\ManyToMany(targetEntity="ArticleFAQ", mappedBy="categories", cascade="persist")
     * @ORM\JoinTable(name="article_faq_categorie", joinColumns={@ORM\JoinColumn(name="categorie_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="article_faq_id", referencedColumnName="id")})
     */
    private Collection $articles;

    /**
     * @ORM\Column(type="string", length=8, nullable=false)
     */
    private $code;

    /**
     * Categorie constructor.
     */
    public function __construct()
    {
        $this->progiciels = new ArrayCollection();
        $this->acteurs = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->setLocked(false);
    }

    /**
     * @return $this
     */
    public function addProgiciel(Progiciel $progiciel)
    {
        try {
            $this->progiciels->add($progiciel);
            $progiciel->addCategorie($this);
            return $this;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $progiciels
     * @return $this
     */
    public function setProgiciels($progiciels)
    {
        foreach ($progiciels->toArray() as $p) {
            $this->addProgiciel($p);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProgiciels()
    {
        return $this->progiciels;
    }

    public function removeProgiciel(Progiciel $progiciel)
    {
        try {
            $this->progiciels->removeElement($progiciel);
            $progiciel->removeCategorie($this);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function removeProgiciels(mixed $newProgiciels)
    {
        foreach ($this->progiciels as $progiciel) {
            if (in_array($progiciel->getId(), $newProgiciels)) {
                $this->removeProgiciel($progiciel);
            }
        }

        return $this;
    }


    public function removeActeur(Acteur $acteur)
    {
        try {
            if (!$this->acteurs->contains($acteur)) {
                return;
            }
            $this->acteurs->removeElement($acteur);
            $acteur->removeCategorie($this);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $newActeurs
     */
    public function removeActeurs($newActeurs)
    {
        try {
            foreach ($this->acteurs as $acteur) {
                if (!in_array($acteur->getId(), $newActeurs)) {
                    $this->removeActeur($acteur);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return $this
     */
    public function addActeur(Acteur $acteur)
    {
        try {
            $this->acteurs->add($acteur);
            $acteur->addCategorie($this);
            return $this;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $acteurs
     * @return $this
     */
    public function setActeurs($acteurs)
    {
        foreach ($acteurs->toArray() as $p) {
            $this->addActeur($p);
        }


        return $this;
    }

    /**
     * @return mixed
     */
    public function getActeurs()
    {
        return $this->acteurs;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function isLocked(): bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): Categorie
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @return $this
     */
    public function addArticle(ArticleFAQ $article)
    {
        if ($this->articles->contains($article)) {
            return;
        }
        $this->articles->add($article);
        $article->addCategorie($this);

        return $this;
    }

    public function removeArticle(ArticleFAQ $article)
    {
        try {
            $this->articles->removeElement($article);
            $article->removeCategorie($this);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getNom();
    }
}
