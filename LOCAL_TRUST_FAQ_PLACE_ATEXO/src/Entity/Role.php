<?php

namespace App\Entity;

use App\Entity\AgentSupport;
use App\Entity\Categorie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Role.
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    final public const ROLE_AGENT_SUPPORT = 1;
    final public const ROLE_ADMIN = 2;
    final public const ROLE_SUPER_ADMIN = 3;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private string $nom;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AgentSupport", inversedBy="roles")
     */
    private $agentsSupport;

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->agentsSupport = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addAgentSupport(AgentSupport $agentSupport)
    {
        if ($this->agentsSupport->contains($agentSupport)) {
            return;
        }
        $this->agentsSupport->add($agentSupport);
        $agentSupport->addRole($this);

        return $this;
    }

    public function removeAgentSupport(AgentSupport $agentSupport)
    {
        if (!$this->agentsSupport->contains($agentSupport)) {
            return;
        }
        $this->agentsSupport->removeElement($agentSupport);
        $agentSupport->removeRole($this);
    }

    /**
     * @return mixed
     */
    public function getAgentsSupport()
    {
        return $this->agentsSupport;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     *
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}