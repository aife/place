<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleFAQ.
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="article_faq")
 * @ORM\Entity(repositoryClass="App\Repository\ArticleFAQRepository")
 */
class ArticleFAQ implements \Stringable
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="text")
     */
    private string $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTime $createdAt = null;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private bool $isDraft = false;

    /**
     * @ORM\ManyToMany(targetEntity="Categorie", inversedBy="articles")
     *
     */
    private $categories;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * )
     */
    private bool $locked;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="articles")
     */
    private ?Client $client = null;

    /**
     * @ORM\OneToMany(targetEntity=Complement::class, mappedBy="article")
     */
    private Collection $complements;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->setLocked(false);
        $this->complements = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return ArticleFAQ
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return ArticleFAQ
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return ArticleFAQ
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isDraft.
     *
     * @param bool $isDraft
     *
     * @return ArticleFAQ
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    /**
     * Get isDraft.
     *
     * @return bool
     */
    public function getIsDraft()
    {
        return $this->isDraft;
    }

    public function isLocked(): bool
    {
        return $this->locked;
    }

    public function setLocked(bool $locked): ArticleFAQ
    {
        $this->locked = $locked;

        return $this;
    }


    /**
     * @return $this
     */
    public function addCategorie(Categorie $categorie)
    {
        if (!$this->categories->contains($categorie)) {
            $this->categories->add($categorie);
        }
            return $this;

    }

    /**
     * @param $categories
     * @return $this
     */
    public function setCategories($categories)
    {
        foreach ($categories->toArray() as $p) {
            $this->addCategorie($p);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function removeCategorie(Categorie $categorie)
    {
        if (!$this->categories->contains($categorie)) {
            return;
        }
        $this->categories->removeElement($categorie);
        $categorie->removeArticle($this);
    }

    public function removeCategories(array $newCategories)
    {
        try {
            foreach ($this->categories as $categorie) {
                if (!in_array($categorie->getId(), $newCategories)) {
                    $this->removeCategorie($categorie);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getTitle();
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getComplements(): Collection
    {
        return $this->complements;
    }

    public function setComplements(Collection $complements): ArticleFAQ
    {
        $this->complements = $complements;
        return $this;
    }
}
