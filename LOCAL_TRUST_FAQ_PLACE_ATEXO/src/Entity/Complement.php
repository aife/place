<?php

namespace App\Entity;

use App\Repository\ComplementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementRepository::class)
 */
class Complement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=ArticleFAQ::class, inversedBy="complements")
     */
    private ArticleFAQ $article;

    /**
     * @ORM\ManyToMany(targetEntity=Client::class, mappedBy="complements")
     */
    private Collection $clients;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $complement;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients->add($client);
            $client->addComplement($this);
        }

        return $this;
    }

    public function setClients(Collection $clients): self
    {
        foreach ($clients->toArray() as $p) {
            $this->addClient($p);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            $client->removeComplement($this);
        }

        return $this;
    }

    public function getArticle(): ArticleFAQ
    {
        return $this->article;
    }

    public function setArticle(ArticleFAQ $article): void
    {
        $this->article = $article;
    }
}
