<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Acteur.
 *
 * @ORM\Table(name="acteur")
 * @ORM\Entity(repositoryClass="App\Repository\ActeurRepository")
 */
class Acteur implements \Stringable
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private string $nom;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private string $libelle;

    /**
     * @ORM\ManyToMany(targetEntity="Categorie", inversedBy="acteurs")
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return $this
     */
    public function addCategorie(Categorie $categorie)
    {
        if ($this->categories->contains($categorie)) {
            return;
        }
        $this->categories->add($categorie);
        $categorie->addActeur($this);

        return $this;
    }

    public function removeCategorie(Categorie $categorie)
    {
        if (!$this->categories->contains($categorie)) {
            return;
        }
        $this->categories->removeElement($categorie);
        $categorie->removeActeur($this);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Acteur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Acteur
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    public function __toString(): string
    {
        return $this->getLibelle();
    }
}
