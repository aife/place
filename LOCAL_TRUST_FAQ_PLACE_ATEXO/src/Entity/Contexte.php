<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Entité Contexte.
 *
 * Un contexte est un ensemble d’information d’intérêt pour le support – extraites de l’application
 * cliente lors de la création d’une demande. Ces contextes peuvent être des contextes utilisateur,
 * métier ou technique. Par exemple un contexte technique contient des informations sur le système
 * d’exploitation, le navigateur, la version de Java, etc. de l’utilisateur. Un contexte métier contiendra
 * des informations « métier » tel qu’une référence de consultation, son intitulé, l’URL de la page
 * consultée, etc.
 *
 * @author David Delobel <david.delobel@atexo.com>
 *


 */
class Contexte
{
    /**
     * Types de valeurs acceptées dans les informations de contexte.
     */
    final const TYPE_VALEURS = [
        'boolean',  // Booléen true, false
        'integer',  // Nombre entier
        'double',   // Nombre flottant avec un point comme séparateur
        'string',   // Chaîne de caractères
        'array',    // Tableau de données
        'object',   // Objet
        'datetime', // Date/heure au format ISO 8601

        'url', // lien web
        'mailto', // Adresse email (pour faire un href mailto)

        // métier
        'entreprise', // array [nom, siren, code_postal]
        'etablissement',  // array [nom, siret]
        'organisme_achat', // array [entite_publique, entite_achat]
        'adresse', // array [, , ...] (ligne 1, ligne 2, ...)
    ];

    /**
     * Identifiant du contexte.
     *
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private readonly int $id;

    /**
     * Nom interne du contexte.
     *
     * @ORM\Column(name="nom", type="string", length=32, nullable=false)
     */
    private string $nom;

    /**
     * Label du contexte.
     *
     * @ORM\Column(name="label", type="string", length=32, nullable=false)
     */
    private string $label;

    /**
     * Description du contexte.
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private string $description;

    /**
     * Tableau associatif de champs du contexte (stocké en Json dans le SGBD).
     *
     * @var array
     * @ORM\Column(name="champs", type="json_array", nullable=true)
     */
    private $champs;

    /**
     * @ORM\Column(name="max_visible", type="integer", nullable=true)
     */
    private int $maxVisible;

    private $displayedFields;

    /**
     * Ticket associé au contexte.
     *
     * @var Ticket
     * @ORM\ManyToOne(targetEntity="App\Entity\Ticket", inversedBy="contextes")
     */
    private $ticket;

    /**
     * Date de création du contexte.
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private \DateTime $createdAt;

    /**
     * Constructeur de Contexte.
     *
     * @param null|string $nom         Nom interne du contexte
     * @param null|string $label       Label du contexte
     * @param null|string $description Description du contexte
     * @param null|int    $max_visible Nombre d'élements à afficher avant de masquer le reste
     */
    public function __construct(string $nom = null, string $label = null, string $description = null, int $max_visible = null)
    {
        $this->setNom($nom);
        $this->setLabel($label);
        $this->setDescription($description);
        $this->setMaxVisible($max_visible);
    }

    public static function fromArray(array $array)
    {
        $contexte = new Contexte(
            $array['nom'],
            $array['label'],
            $array['description'],
            $array['max_visible']
        );
        $contexte->champs = $array['champs'];

        return $contexte;
    }

    /**
     * Ajouter un champ au contexte.
     *
     * @param string      $nom_champ           Nom interne du champ
     * @param string      $label               Label du champ
     * @param mixed       $valeur              Valeur du champ
     * @param string      $type_valeur         Type de valeur
     * @param bool        $visible_utilisateur L'utilisateur peut-il voir cette information dans l'interface ?
     * @param bool        $visible_support     Le support peut-il voir cette information dans l'interface ?
     * @param null|string $description         Description du champ
     * @param $extras
     *
     * @return $this
     */
    public function add(string $nom_champ, string $label, mixed $valeur, string $type_valeur, bool $visible_utilisateur = true, bool $visible_support = true, string $description = null, $extras = null)
    {
        if (empty($nom_champ)) {
            throw new \InvalidArgumentException('Le nom du champ doit être défini');
        }
        if (empty($label)) {
            throw new \InvalidArgumentException('Le label du champ doit être défini');
        }
        //if (empty($valeur))
        //    throw new \InvalidArgumentException("La valeur doit être définie");
        if (!in_array($type_valeur, Contexte::TYPE_VALEURS)) {
            throw new \InvalidArgumentException("Le type de valeur '$type_valeur' n'existe pas");
        }
        if (!$visible_utilisateur && !$visible_support) {
            throw  new \InvalidArgumentException("Le champ n'est visible à personne");
        }
        $this->champs[$nom_champ] = [
            'label' => $label,
            'description' => $description,
            'valeur' => $valeur,
            'type_valeur' => $type_valeur,
            'visible_utilisateur' => $visible_utilisateur,
            'visible_support' => $visible_support,
            'extras' => $extras,
        ];

        return $this;
    }

    public function get(string $nom_champ, string $cle)
    {
        if (!array_key_exists($nom_champ, $this->champs)) {
            throw new \InvalidArgumentException("Le champ '$nom_champ' n'est pas défini");
        }

        return $this->champs[$nom_champ][$cle];
    }

    public function getChamp($nom_champ)
    {
        if (!array_key_exists($nom_champ, $this->champs)) {
            throw new \InvalidArgumentException("Le champ '$nom_champ' n'est pas défini");
        }

        return $this->champs[$nom_champ];
    }

    public function getValeur(string $nom_champ)
    {
        return $this->get($nom_champ, 'valeur');
    }

    public function getTypeValeur(string $nom_champ)
    {
        return $this->get($nom_champ, 'type_valeur');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom.
     *
     *
     * @return $this
     */
    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set label.
     *
     *
     * @return $this
     */
    public function setLabel(string $label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Set description.
     *
     *
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public function isVisibleSupport($champ)
    {
        return $champ['visible_support'] == true;
    }

    public function isVisibleUtilisateur($champ)
    {
        return $champ['visible_utilisateur'] == true;
    }

    public function getChampsUtilisateur()
    {
        if (is_null($this->champs)) {
            return null;
        }

        return array_filter($this->champs, self::isVisibleUtilisateur(...));
    }

    public function getChampsSupport()
    {
        if (is_null($this->champs)) {
            return null;
        }

        return array_filter($this->champs, self::isVisibleSupport(...));
    }

    /**
     * @return int
     */
    public function getMaxVisible()
    {
        return $this->maxVisible;
    }

    /**
     * @param int $maxVisible
     */
    public function setMaxVisible($maxVisible)
    {
        $this->maxVisible = $maxVisible;
    }

    /**
     * Set ticket.
     *
     * @param Ticket $ticket
     *
     * @return $this
     */
    public function setTicket(Ticket $ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket.
     *
     * @return Ticket
     */
    public function getTicket(): Ticket
    {
        return $this->ticket;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Contexte
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set champs
     *
     * @param array $champs
     *
     * @return Contexte
     */
    public function setChamps($champs)
    {
        $this->champs = $champs;

        return $this;
    }

    /**
     * Get champs
     *
     * @return array
     */
    public function getChamps()
    {
        return $this->champs;
    }

    /**
     * @return mixed
     */
    public function getDisplayedFields()
    {
        return $this->displayedFields;
    }

    public function setDisplayedFields(mixed $displayedFields): void
    {
        $this->displayedFields = $displayedFields;
    }


}
