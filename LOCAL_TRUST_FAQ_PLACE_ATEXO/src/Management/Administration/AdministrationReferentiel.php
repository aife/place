<?php
/**
 * Created by PhpStorm.
 * User: Hassouni
 * Date: 09/11/2016
 * Time: 10:15.
 */

namespace App\Management\Administration;

use App\Entity\Acteur;
use App\Entity\ArticleFAQ;
use App\Entity\Categorie;
use App\Entity\Progiciel;
use App\Entity\ReponseStandardisee;
use App\Repository\ActeurRepository;
use App\Repository\CategorieRepository;
use App\Repository\ProgicielRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdministrationReferentiel
{
    /**
     * Constructeur de la classe.
     *
     * @param $container : le container
     */
    public function __construct(
        private $container,
        private readonly Security $security,
        private readonly EntityManagerInterface $entityManager,
        private readonly CategorieRepository $categorieRepository,
        private readonly ProgicielRepository $progicielRepository,
        private readonly ActeurRepository $acteurRepository
    )
    {
    }

    public function getListeCategorie($filtre)
    {
        try {
            $agentSupport = $this->security->getUser();
            $progicielsRepoByAgentSupport = $this->progicielRepository->findListByAgentSupport($agentSupport);
            $categorieByProgiciel= $this->categorieRepository->findListByProgiciels($progicielsRepoByAgentSupport);

            // Sorting
            $default_sorting_order = ['c.nom' => 'asc'];
            if (isset($filtre['sorting_field']) && isset($filtre['sorting_direction'])) {
                $sorting_order = [$filtre['sorting_field'] => $filtre['sorting_direction']];
            } else {
                $sorting_order = $default_sorting_order;
            }

            $criteres = [];
            if (!empty($filtre['filtre_client_id'])) {
                $criteres['p.id'] = $filtre['filtre_client_id'];
            }
            if (!empty($filtre['filtre_acteur_id'])) {
                $criteres['a.id'] = $filtre['filtre_acteur_id'];
            }
            if (!empty($filtre['filtre_actif']) || $filtre['filtre_actif'] === '0') {
                $criteres['c.locked'] = $filtre['filtre_actif'];
            }
            if (!empty($filtre['filtre_code'])) {
                $criteres['c.code'] = $filtre['filtre_code'];
            }
            if (!empty($filtre['filtre_categorie_id'])) {
                $criteres['c.id'] = $filtre['filtre_categorie_id'];
            }

            $categories_filtres = $this->categorieRepository->getListCategories($criteres, $sorting_order, $agentSupport);
            $paginator = $this->container->get('knp_paginator');
            $pagination = $paginator->paginate(
                $categories_filtres,
                $filtre['numero_page'], // numéro de page
                $this->container->getParameter('liste_referentiels.limite_par_page') // limite par page
            );

            $render_vars = [
                'pagination' => $pagination,
                'progiciels' => $progicielsRepoByAgentSupport,
                'categories' => $categorieByProgiciel,
                'acteurs' => $this->acteurRepository->findAll(),
                'nbr_criteres' => count($criteres),
            ];

            foreach ($criteres as $critere => $entite) {
                $render_vars['filtres'][$critere] = $entite;
            }

            return $render_vars;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function traiterCategorie($data)
    {
        try {
            $agentSupport = $this->security->getUser();
            $progicielsRepoByAgentSupport = $this->progicielRepository->findListByAgentSupport($agentSupport);

            $categorie_nom = $data['categorie_nom'];
            $categorie_id = $data['categorie_id'];
            $progiciels = $data['progiciels'];
            $acteurs = $data['acteurs'];
            $actif = $data['actif'];
            $code = $data['code'];

            // --------------------
            // Vérifier les données
            // --------------------
            $errors = $this->verificationDonneesCategorie($data);

            // -----------------------------------------------
            // Retourner les éventuelles erreurs de formulaire
            // -----------------------------------------------
            if ((is_countable($errors) ? count($errors) : 0) > 0) {
                return json_encode(['errors' => $errors], JSON_THROW_ON_ERROR);
            }

            //-------------------------------
            // Sinon sauvegarder la catégorie
            //--------------------------------
            $confirm = 0;
            $categorie = null;

            if ($categorie_id) {
                $categorie = $this->entityManager->getRepository(Categorie::class)->find($categorie_id);
                $confirm = 2;
            }
            if (!$categorie instanceof Categorie) {
                $categorie = new Categorie();
                $confirm = 1;
            }
            $categorie->setNom($categorie_nom);
            if ($actif === 'true') {
                $categorie->setLocked(1);
            } elseif ($actif === 'false') {
                $categorie->setLocked(0);
            }

            $ids_progiciels = explode(',', (string) $progiciels);

            $progicielUser = [];
            foreach ($progicielsRepoByAgentSupport as $progicielId) {
                $progicielUser[] = $progicielId->getId();
            }

            $progicielToRemove = array_diff($progicielUser, $ids_progiciels);
            $categorie->removeProgiciels($progicielToRemove);

            foreach ($ids_progiciels as $id_progiciel) {
                if (!in_array($progicielToRemove,  $ids_progiciels)) {
                    $progiciel = $this->entityManager->getRepository(Progiciel::class)->find($id_progiciel);
                    $categorie->addProgiciel($progiciel);
                }
            }

            $ids_acteurs = explode(',', (string) $acteurs);
            $categorie->removeActeurs($ids_acteurs);
            foreach ($ids_acteurs as $id_acteur) {
                $acteur = $this->entityManager->getRepository(Acteur::class)->find($id_acteur);
                $categorie->addActeur($acteur);
            }

            if ($code) {
                $categorie->setCode($code);
            }

            // Persister
            $this->entityManager->persist($categorie);
            $this->entityManager->flush();

            return json_encode([
                'redirect' => $this->generateUrl('administration_categorie', [
                    'confirm' => $confirm,
                ]),
            ], JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function supprimerCategorie($data)
    {
        try {
            $categorie_id = $data['categorie_id'];

            // --------------------
            // Vérifier les données
            // --------------------
            $errors = $this->verificationDonneesCategorieToDelete($data);

            // -----------------------------------------------
            // Retourner les éventuelles erreurs de formulaire
            // -----------------------------------------------
            if ((is_countable($errors) ? count($errors) : 0) > 0) {
                return json_encode(['errors' => $errors], JSON_THROW_ON_ERROR);
            }

            //-------------------------------
            // Sinon supprimer la catégorie
            //--------------------------------
            $em = $this->getDoctrine()->getManager();

            $categorie = $em->getRepository(Categorie::class)->find($categorie_id);
            $categorie->removeProgiciels([]);
            $categorie->removeActeurs([]);

            // supprimer la categorie
            $em->remove($categorie);

            $em->flush();

            return json_encode([
                'redirect' => $this->generateUrl('administration_categorie', [
                    'confirm' => 3,
                ]),
            ], JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function verificationDonneesCategorieToDelete($data)
    {
        $errors = [];
        $categorie_id = $data['categorie_id'];
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository(Categorie::class)->find($categorie_id);
        if (is_null($categorie)) {
            $errors['categorie_id'.$categorie_id] = ['not_exists', 'La catégorie sélectionnée n\'existe pas'];
        }

        return $errors;
    }

    private function verificationDonneesCategorie($data)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $categorie_nom = $data['categorie_nom'];
        $code = $data['code'];
        $progiciels = $data['progiciels'];
        $acteurs = $data['acteurs'];

        if (!isset($categorie_nom) || trim((string) $categorie_nom) === '') {
            $errors['categorie_nom'] = ['undefined', 'Un nom de catégorie est requis'];
        }

        if (!isset($code) || trim((string) $code) === '') {
            $errors['code'] = ['undefined', 'Un code est requis'];
        } elseif (strlen($code) > 8) {
            $errors['code'] = ['undefined', 'Le code ne doit pas dépasser 8 caractères'];
        }

        $categorie = $this->categorieRepository->findOneBy(['code' => $code]);
        if ($categorie && $categorie->getId() !== intval($data['categorie_id'])) {
            $errors['code'] = ['undefined', 'Ce code a déjà été attribué à une catégorie, veuillez choisir un nouveau.'];
        }

        if (!isset($progiciels) || trim((string) $progiciels) === '' || !$progiciels) {
            $errors['progiciels'] = ['undefined', 'Au moins un progiciel est requis'];
        } else {
            $ids_progiciels = explode(',', (string) $progiciels);
            foreach ($ids_progiciels as $id_progiciel) {
                $progiciel = $em->getRepository(Progiciel::class)->find($id_progiciel);
                if (is_null($progiciel)) {
                    $errors['progiciel_id'.$id_progiciel] = ['not_exists', 'Le progiciel sélectionné n\'existe pas'];
                }
            }
        }
        if (!isset($acteurs) || trim((string) $acteurs) === '' || !$acteurs) {
            $errors['acteurs'] = ['undefined', 'Au moins un acteur est requis'];
        } else {
            $ids_acteurs = explode(',', (string) $acteurs);
            foreach ($ids_acteurs as $id_acteur) {
                $acteur = $em->getRepository(Acteur::class)->find($id_acteur);
                if (is_null($acteur)) {
                    $errors['acteur_id'.$id_acteur] = ['not_exists', 'L\'acteur sélectionné n\'existe pas'];
                }
            }
        }

        return $errors;
    }

    public function getListeFaq($filtre)
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $agentSupport = $this->security->getUser();
            $categories_repo = $em->getRepository('App:Categorie');
            $progiciels_repo = $em->getRepository('App:Progiciel');
            $progicielsRepoByAgentSupport = $progiciels_repo->findListByAgentSupport($agentSupport);
            $articles_faq_repo = $em->getRepository('App:ArticleFAQ');
            $categorieByProgiciel = $categories_repo->findListByProgiciels($progicielsRepoByAgentSupport);


            // Sorting
            $default_sorting_order = ['c.nom' => 'asc', 'a.title' => 'asc'];
            if (isset($filtre['sorting_field']) && isset($filtre['sorting_direction'])) {
                $sorting_order = [$filtre['sorting_field'] => $filtre['sorting_direction']];
            } else {
                $sorting_order = $default_sorting_order;
            }
            $criteres = [];
            if (!empty($filtre['filtre_categorie_id'])) {
                $criteres['c.id'] = $filtre['filtre_categorie_id'];
            }
            if (!empty($filtre['filtre_actif']) || $filtre['filtre_actif'] === '0') {
                $criteres['a.locked'] = $filtre['filtre_actif'];
            }
            $articles_faq_filtres = $articles_faq_repo->getListArticleFaq($criteres, $sorting_order, $categorieByProgiciel);
            $paginator = $this->container->get('knp_paginator');
            $pagination = $paginator->paginate(
                $articles_faq_filtres,
                $filtre['numero_page'], // numéro de page
                $this->container->getParameter('liste_referentiels.limite_par_page') // limite par page
            );
            $render_vars = [
                'pagination' => $pagination,
                'categories' => $categorieByProgiciel,
                'progiciels' => $progiciels_repo->findAll(),
                'nbr_criteres' => count($criteres),
            ];
            foreach ($criteres as $critere => $entite) {
                $render_vars['filtres'][$critere] = $entite;
            }
            return $render_vars;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function traiterArticleFaq($data)
    {
        try {
            $articleFaq_id = $data['articleFaq_id'];
            $categories = $data['categories'];
            $question = $data['question'];
            $reponse = $data['reponse'];
            $actif = $data['actif'];

            // --------------------
            // Vérifier les données
            // --------------------
            $errors = $this->verificationDonneesArticleFaq($data);

            // -----------------------------------------------
            // Retourner les éventuelles erreurs de formulaire
            // -----------------------------------------------
            if ((is_countable($errors) ? count($errors) : 0) > 0) {
                return json_encode(['errors' => $errors], JSON_THROW_ON_ERROR);
            }

            //-------------------------------
            // Sinon sauvegarder l'article faq
            //--------------------------------
            $confirm = 0;
            $em = $this->getDoctrine()->getManager();
            $articleFaq = null;
            if ($articleFaq_id) {
                $articleFaq = $em->getRepository('App:ArticleFAQ')->find($articleFaq_id);
                $confirm = 2;
            }
            if (!$articleFaq instanceof ArticleFAQ) {
                $articleFaq = new ArticleFAQ();
                $confirm = 1;
            }
            $articleFaq->setTitle($question);
            $articleFaq->setContent($reponse);
            if ($actif === 'true') {
                $articleFaq->setLocked(1);
            } elseif ($actif === 'false') {
                $articleFaq->setLocked(0);
            }

            $ids_categories = explode(',', (string) $categories);
            $articleFaq->removeCategories($ids_categories);
            foreach ($ids_categories as $id_categorie) {
                $categorie = $em->getRepository('App:Categorie')->find($id_categorie);
                $articleFaq->addCategorie($categorie);
            }

            // Persister
            $em->persist($articleFaq);
            $em->flush();

            return json_encode([
                'redirect' => $this->generateUrl('administration_faq', [
                    'confirm' => $confirm,
                ]),
            ], JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function verificationDonneesArticleFaq($data)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $categories = $data['categories'];
        $question = $data['question'];
        $reponse = $data['reponse'];

        if (!isset($question) || trim((string) $question) === '') {
            $errors['question'] = ['undefined', 'Une question est requise'];
        }

        if (!isset($reponse) || trim((string) $reponse) === '') {
            $errors['reponse'] = ['undefined', 'Une réponse est requise'];
        }

        if (!isset($categories) || trim((string) $categories) === '' || !$categories) {
            $errors['categories'] = ['undefined', 'Au moins une catégorie est requise'];
        } else {
            $ids_categories = explode(',', (string) $categories);
            foreach ($ids_categories as $id_categorie) {
                $categorie = $em->getRepository('App:Categorie')->find($id_categorie);
                if (is_null($categorie)) {
                    $errors['categorie_id'.$id_categorie] = ['not_exists', 'La catégorie sélectionnée n\'existe pas'];
                }
            }
        }

        return $errors;
    }

    public function supprimerArticleFaq($data)
    {
        try {
            $articleFaq_id = $data['articleFaq_id'];

            // --------------------
            // Vérifier les données
            // --------------------
            $errors = $this->verificationDonneesArticleFaqToDelete($data);

            // -----------------------------------------------
            // Retourner les éventuelles erreurs de formulaire
            // -----------------------------------------------
            if ((is_countable($errors) ? count($errors) : 0) > 0) {
                return json_encode(['errors' => $errors], JSON_THROW_ON_ERROR);
            }

            //-------------------------------
            // Sinon supprimer l'article faq
            //--------------------------------
            $em = $this->getDoctrine()->getManager();

            $articleFaq = $em->getRepository('App:ArticleFAQ')->find($articleFaq_id);
            // supprimer l'article faq
            $em->remove($articleFaq);

            $em->flush();

            return json_encode([
                'redirect' => $this->generateUrl('administration_faq', [
                    'confirm' => 3,
                ]),
            ], JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function verificationDonneesArticleFaqToDelete($data)
    {
        $errors = [];
        $articleFaq_id = $data['articleFaq_id'];
        $em = $this->getDoctrine()->getManager();
        $articleFaq = $em->getRepository('App:ArticleFAQ')->find($articleFaq_id);
        if (is_null($articleFaq)) {
            $errors['articleFaq_id'.$articleFaq_id] = ['not_exists', 'La question-réponse sélectionnée n\'existe pas'];
        }

        return $errors;
    }

    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param mixed  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    public function generateUrl($route, mixed $parameters = [], $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }
}
