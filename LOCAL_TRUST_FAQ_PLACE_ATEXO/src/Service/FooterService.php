<?php

    namespace App\Service;

    use Symfony\Component\HttpFoundation\RequestStack;
    use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
    use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
    use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
    use Symfony\Contracts\HttpClient\HttpClientInterface;

    class FooterService
    {
        private const FOOTER_ENDPOINT = '/footer/';
        private const FOOTER_CSS_ENDPOINT = '/themes/css/mpe-new-client.css?k=5f4d1cfda43049.06089948';

        public function __construct(
            private readonly RequestStack $requestStack,
            private readonly HttpClientInterface $httpClient
        )
        {
        }

        /**
         * @throws \Exception
         */
        public function determineOriginType(): string
        {
            $referrerUrl = $this->requestStack->getMasterRequest()->headers->get('referer') ??
                (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? 'https://'. $_SERVER['HTTP_X_FORWARDED_HOST'] : 'http://'. $_SERVER['HTTP_HOST']);

            if (!$referrerUrl) {
                throw new \Exception("Referrer URL is not available");
            }

            $urlParts = explode('/', $referrerUrl);

            return in_array('agent', $urlParts) ? 'agent' : 'entreprise';
        }


        /**
         * @throws ServerExceptionInterface
         * @throws RedirectionExceptionInterface
         * @throws ClientExceptionInterface|\Exception
         */
        public function getFooterFromMPE(): string
        {
            $endpoint = self::FOOTER_ENDPOINT . $this->determineOriginType() . '/' . 'faq';

            return $this->fetchContent($endpoint);
        }

        /**
         * @throws ServerExceptionInterface
         * @throws RedirectionExceptionInterface
         * @throws ClientExceptionInterface
         */
        public function getFooterStyleFromMPE(): string
        {
            try {
                return $this->fetchContent(self::FOOTER_CSS_ENDPOINT);
            } catch (\Exception $e) {
                return $this->getDefaultCss();
            }
        }

        private function fetchContent(string $endpoint): string
        {
            try {
                if (!$this->requestStack) {
                    throw new \Exception("Request object is not available");
                }

                $referrerUrl = $this->requestStack->getMasterRequest()->headers->get('referer') ??
                    (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? 'https://'. $_SERVER['HTTP_X_FORWARDED_HOST'] : 'http://'. $_SERVER['HTTP_HOST']);

                if (!$referrerUrl) {
                    throw new \Exception("Referrer URL is not available");
                }

                $parsedUrl = parse_url($referrerUrl);
                $baseUrl = $parsedUrl['scheme'] . '://' . $parsedUrl['host'];

                $fullUrl = $baseUrl . $endpoint;

                $response = $this->httpClient->request('GET', $fullUrl);

                if ($response->getStatusCode() === 200) {
                    return $response->getContent();
                } else {
                    throw new \Exception(
                        "Failed to fetch content from $endpoint, status code: " .
                        $response->getStatusCode()
                    );
                }
            } catch (\Exception $e) {
                throw new \Exception(
                    "An error occurred while fetching content from $endpoint: " .
                    $e->getMessage()
                );
            }
        }

        private function getDefaultCss(): string
        {
            return <<<CSS
@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Regular.woff2") format("woff2"), url("../fonts/Marianne-Regular.woff") format("woff");
  font-weight: 400;
  font-style: normal; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Regular_Italic.woff2") format("woff2"), url("../fonts/Marianne-Regular_Italic.woff") format("woff");
  font-weight: 400;
  font-style: italic; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Light.woff2") format("woff2"), url("../fonts/Marianne-Light.woff") format("woff");
  font-weight: 300;
  font-style: normal; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Light_Italic.woff2") format("woff2"), url("../fonts/Marianne-Light_Italic.woff") format("woff");
  font-weight: 300;
  font-style: italic; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Bold.woff2") format("woff2"), url("../fonts/Marianne-Bold.woff") format("woff");
  font-weight: 700;
  font-style: normal; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-Bold_Italic.woff2") format("woff2"), url("../fonts/Marianne-Bold_Italic.woff") format("woff");
  font-weight: 700;
  font-style: italic; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-ExtraBold.woff2") format("woff2"), url("../fonts/Marianne-ExtraBold.woff") format("woff");
  font-weight: 900;
  font-style: normal; }

@font-face {
  font-family: "Marianne";
  src: url("../fonts/Marianne-ExtraBold_Italic.woff2") format("woff2"), url("../fonts/Marianne-ExtraBold_Italic.woff") format("woff");
  font-weight: 900;
  font-style: italic; }

/*!
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
#MPE-nav .navbar-dark.navbar-horizontal,
#MPE-nav .dropdown.dropdown-user-link.nav-item .avatar-container .fa.fa-user,
#dashboardUserAgent .blocUserHome,
.primary-client-bg {
  background: #5FC3F0 !important; }

#MPE-nav .btn.btn-sm.mr-1.mb-1.btn-primary.btn-block,
#MPE-nav .dropdown-custom .btn.btn-primary,
.btn.btn-primary.btn-block,
.primary-client-bg-border,
.primary-client-bg-border:hover {
  background-color: #5FC3F0 !important;
  border-color: #5FC3F0 !important; }

.primary-client {
  color: #5FC3F0 !important; }

.badge-primary.badge-glow.primary-client-bg {
  box-shadow: 0 0 10px #5FC3F0; }

html body a,
#MPE-nav.horizontal-menu .navbar-horizontal #main-menu-navigation .dropdown-custom a span,
#dashboardUserAgent #description h3.primary {
  color: #5FC3F0; }

.btn-glow.primary-client-bg,
.btn-glow.primary-client-bg-border {
  box-shadow: 0 1px 20px 1px rgba(94, 194, 240, 0.6); }

.disable-bloc {
  display: none !important; }

html body a {
  color: #0c5476; }

body,
h1,
h2,
h3,
h4,
h5,
h6,
p,
a,
span,
li,
label,
input,
select,
textarea,
.bloc-filtres .ui-widget {
  font-family: "Marianne", Arial, sans-serif; }

.badge-primary.badge-glow.primary-client-bg,
.btn-glow.primary-client-bg-border,
.badge-danger.badge-glow,
.badge-secondary.badge-glow,
.badge-warning.badge-glow {
  box-shadow: none !important; }

footer.footer-agent {
  display: flex;
  width: 100%;
  margin: 0 auto;
  font-family: "Marianne", Arial;
  font-display: optional;
  font-weight: 400;
  text-align: left; }

footer {
  font-size: 11px;
  width: 984px;
  color: #fff;
  border: 0;
  height: 40px;
  padding: 0;
  text-align: center;
  font-family: "Marianne", Arial, sans-serif;
  font-weight: 400;
  text-align: left;
  border-top: 1px solid #E6E6E6; }

.footer-agent a {
  font-size: 1em;
  color: #000091;
  line-height: 0.5rem;
  text-decoration: underline;
  cursor: pointer; }

.footer-agent h2 {
  font-size: 2em !important;
  color: #000091;
  line-height: 1.5rem;
  text-decoration: none;
  cursor: default; }

.footer-agent a:hover {
  color: #000091;
  line-height: 0.5rem;
  text-decoration: none;
  cursor: pointer; }

.footer-agent a:visited {
  color: #40605B; }

.footer-agent .bloc-link h4 {
  padding-left: 0; }

.footer-agent .bloc-link {
  width: 80%; }

.footer-agent .bloc-link > div {
  width: calc(100% / 4); }

.footer-agent .bloc-link > div:first-child {
  width: calc(100% / 5); }

.footer-agent .footer-agent__left-part {
  padding-right: 4.4rem;
  margin-left: 2rem;
  margin-top: 2rem; }

.footer-agent .footer-agent__versionBlock {
  text-align: left;
  color: #666;
  margin: 2rem 0 0 7.6rem; }

.footer-agent li {
  line-height: 1.4; }

#acces-utah.help-bar ul li a {
  top: 260px !important;
  background-color: #5FC3F0 !important; }

.btn-info:active,
.btn-info:focus,
.btn-primary:active,
.btn-primary:focus {
  background-color: #5FC3F0 !important;
  border-color: #5FC3F0 !important; }

.footer-place {
  margin: 0 auto;
  padding: 0 2rem;
  padding-top: 20px;
  display: flex;
  width: 100%;
  height: 38%;
  flex-direction: row;
  font-family: "Marianne", Arial;
  font-display: optional;
  font-weight: 400;
  text-align: left;
  background: #FFF;
  border-top: 1px solid #E6E6E6; }
  .footer-place a {
    color: #000091 !important;
    font-size: 1em;
    cursor: pointer;
    text-decoration: underline;
    line-height: 1.5em !important; }
    .footer-place a:hover {
      color: #000091 !important;
      text-decoration: none; }
    .footer-place a:visited {
      color: #40605B !important;
      text-decoration: underline; }
  .footer-place h2 {
    font-size: 2em !important;
    font-weight: 400 !important;
    color: #464855;
    margin-bottom: 7px; }
  .footer-place .left-part {
    padding-right: 4.4rem;
    margin-right: 4.4rem; }
  .footer-place__menuContainer {
    margin: 0 auto;
    display: flex;
    width: 100%;
    padding-left: 2px; }

.footer-place {
  margin: 0 auto;
  padding: 0 2rem;
  padding-top: 20px;
  display: flex;
  width: 100%;
  height: 38%;
  flex-direction: row;
  font-family: "Marianne", Arial;
  font-display: optional;
  font-weight: 400;
  text-align: left;
  background: #FFF;
  border-top: 1px solid #E6E6E6; }
  .footer-place .versionPart {
    color: #000;
    margin: 2rem 0 0 4.5em;
    text-align: left; }
  .footer-place a {
    color: #000091 !important;
    font-size: 1em;
    cursor: pointer;
    text-decoration: underline;
    line-height: 1.5em !important; }
    .footer-place a:hover {
      color: #000091 !important;
      text-decoration: none; }
    .footer-place a:visited {
      color: #40605B !important;
      text-decoration: underline; }
  .footer-place h2 {
    font-size: 2em !important;
    font-weight: 400 !important;
    color: #464855;
    margin-bottom: 7px; }
  .footer-place .left-part {
    padding-right: 4.4rem;
    margin-right: 4.4rem; }
  .footer-place__menuContainer {
    margin: 0 auto;
    display: flex;
    width: 100%;
    padding-left: 2px; }

.showPlusCriterias,
.showMinusCriterias,
.hiddenDetails,
.collapsedDetails {
  color: #0c5476; }

.protection-donnees-table td {
  border: solid 1px #464855;
  padding: 5px; 
  }
CSS;
        }
    }
