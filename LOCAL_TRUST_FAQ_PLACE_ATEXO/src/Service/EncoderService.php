<?php

namespace App\Service;

use App\Entity\Contexte;

class EncoderService
{
    public function getFormattedDataContextes(array $tempContextes): array
    {
        $contextes = [];
        foreach ($tempContextes as $key => $value) {
            $contextes[$key] = Contexte::fromArray($value);
        }

        return $contextes;
    }

    public function getDecodedContext(string $encodedContext): array
    {
        return json_decode(base64_decode($encodedContext), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getEncodedContext(array $context): string
    {
        return base64_encode(json_encode($context, JSON_THROW_ON_ERROR));
    }

    function isBase64Encoded(?string $encodeData): bool
    {
        return base64_encode(base64_decode($encodeData, true)) === $encodeData;
    }

    /**
     * @throws \JsonException
     */
    public function sanitizeEncodedContent(string $encodedContent): string
    {
        $decodedContent = $this->getDecodedContext($encodedContent);

        array_walk_recursive($decodedContent, function (&$value) {
            $value = strip_tags($value);
            $value = htmlspecialchars($value, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        });

        return $this->getEncodedContext($decodedContent);
    }
}