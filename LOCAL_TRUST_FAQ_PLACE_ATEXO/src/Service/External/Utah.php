<?php
namespace App\Service\External;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\TempContextes;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Utah
{
    /**
     * Utah constructor.
     * @param null $em
     */
    public function __construct(private readonly LoggerInterface $logger, private readonly HttpClientInterface $client)
    {
    }

    /**
     * @param $url
     * @param $context
     * @return null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getToken($url, TempContextes $context)
    {
        $urlWs = $this->sanitizeUrl($url . '/temp/contextes');
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'body' => $context->getData(),
            'timeout' => 2.5
        ];
        $response = $this->client->request('POST', $urlWs, $options);
        $msg = "Token not found!";
        if(Response::HTTP_OK ===  $response->getStatusCode()){
            $content = json_decode($response->getContent(), null, 512, JSON_THROW_ON_ERROR);
            if (isset($content->token)){
                return $content->token;
            }
            $this->logger->error($msg . ' - content: ' . $content);
        }
        $this->logger->error($msg . ' - content: ' . $response->getContent());
        throw new \Exception($msg, Response::HTTP_UNAUTHORIZED);

    }

    /**
     * @param $url
     * @return string|string[]
     */
    private function sanitizeUrl($url): string|array{
        $res = str_replace("//", "/", (string) $url);
        $res = str_replace(":/", "://", $res);
        return $res;
    }
}
