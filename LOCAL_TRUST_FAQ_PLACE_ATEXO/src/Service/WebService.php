<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebService
{
    private const URL_UTAH_KEYCLOAK_OPEN_ID = '/auth/realms/utah/protocol/openid-connect/userinfo';

    public function __construct(private readonly HttpClientInterface $httpClient, private readonly ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function getAuthenticationConfirmation(string $accessToken): bool
    {
        $url = sprintf(
            "%s%s",
            $this->parameterBag->get('identity_base_url'),
           self::URL_UTAH_KEYCLOAK_OPEN_ID
        );

        $response = $this->httpClient->request(
            Request::METHOD_GET,
            $url,
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $accessToken,
                ],
            ]
        );

        return $response->getStatusCode() === Response::HTTP_OK;
    }
}