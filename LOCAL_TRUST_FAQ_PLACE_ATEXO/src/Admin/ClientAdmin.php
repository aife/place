<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ClientAdmin extends AbstractAdmin
{
    protected $formOptions = ['validation_groups' => false];
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('nom', TextType::class)
            ->add('matomoId', IntegerType::class, [
                'required' => false
            ])
            ->add('host',TextType::class)
            ->add('icon_src',TextType::class)
            ->add('logo_src', TextType::class)
            ->add('bandeau_src',TextType::class)
            ->add('email_notif',TextType::class)
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('nom')
            ->add('matomoId')
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('nom')
            ->add('matomoId')
            ->add('host')
            ->add('icon_src')
            ->add('logo_src')
            ->add('bandeau_src')
            ->add('email_notif')
            ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
    /**
     * @param object $article
     */
    public function prePersist($article)
    {
        $this->preUpdate($article);
    }

    /**
     * @param object $article
     */
    public function preUpdate($article)
    {
        $article->setMatomoId($article->getMatomoId());
    }
}
