<?php

namespace App\Admin;

use App\Entity\ArticleFAQ;
use App\Entity\Client;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class ArticleFAQComplementAdmin extends AbstractAdmin
{
    protected $formOptions = ['validation_groups' => false];

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('article', EntityType::class, [
                'class' => ArticleFAQ::class,
                'choice_label' => 'title',
                'multiple' => false,
                'constraints' => false
            ])
            ->add('clients', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'by_reference' => false,
                'constraints' => false
            ])
            ->add('complement', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper->add('article', null, ['label' => 'Question'])
            ->add('clients', null, ['label' => 'Client'])
            ->add('complement', null, ['label' => 'Complement']);
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper->add('article', null, [
            'label' => 'Question'
        ])
            ->add('clients', null, [
                'label' => 'Client'
            ])
            ->add('complement', null, [
                'label' => 'Complement'
            ])
            ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }

    public function prePersist($complement): void
    {
        $this->preUpdate($complement);
    }

    public function preUpdate($complement): void
    {
        $complement->setArticle($complement->getArticle());
        $complement->setClients($complement->getClients());
        $complement->setComplement($complement->getComplement());
    }
}