<?php

namespace App\Admin;

use App\Entity\Categorie;
use App\Entity\Client;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

final class ArticleFAQAdmin extends AbstractAdmin
{
    protected $formOptions = ['validation_groups' => false];
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('categories',  EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'by_reference' => false,
                'constraints' => false
            ])
            ->add('title', TextType::class, ['label' => 'Libelle'])
            ->add('client',  EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'nom',
                'label' => 'Client (Si vous n\'affectez pas de client, la question sera visible
                par tous les clients)',
                'placeholder' => 'Choisissez un client (ou laissez vide)',
                'multiple' => false,
                'required' => false,
            ])
            ->add('content', TextAreaType::class, ['label' => 'Message'])
            ->add('locked', CheckboxType::class, ['required' => false, 'label' => 'Actif']);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('categories')
            ->add('title', null, ['label' => 'Libelle'])
            ->add('client', null, ['label' => 'Client'])
            ->add('locked', null, ['label' => 'Actif']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('categories')
            ->add('title', null, ['label' => 'Libelle'])
            ->add('client', null, ['label' => 'Client'])
            ->add('locked', null, ['label' => 'Actif'])
            ->add('_action', 'actions', ['actions' => ['view' => [], 'edit' => [], 'delete' => []]]);
    }

    /**
     * @param object $article
     */
    public function prePersist($article)
    {
        $this->preUpdate($article);
    }

    /**
     * @param object $article
     */
    public function preUpdate($article)
    {
        $article->setCategories($article->getCategories());
        $article->setClient($article->getClient());
    }
}
