<?php

namespace App\Admin;

use App\Entity\AgentSupport;
use App\Entity\Client;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class AgentSupportAdmin extends AbstractAdmin
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(
        $code,
        $class,
        $baseControllerName = null,
        UserPasswordEncoderInterface $encoder
    ) {
        parent::__construct($code, $class, $baseControllerName);

        $this->encoder = $encoder;
    }

    protected $formOptions = ['validation_groups' => false];
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('displayName',)
            ->add('username', TextType::class, [
                'disabled' => $this->isCurrentRoute('edit'),
            ])
        ;

        if ($this->isCurrentRoute('create')) {
            $formMapper
                ->add('password', RepeatedType::class, ['type' => PasswordType::class, 'first_options' => ['label' => 'Mot de passe'], 'second_options' => ['label' => 'Confirmer votre mot de passe']])
            ;
        }

        $formMapper
            ->add('email')
            ->add('clients', EntityType::class, [
                'label' => 'Client(s)',
                'class' => Client::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'by_reference' => false,
                'required' => false,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('username')
            ->add('password')
            ->add('email')
            ->add('clients')
        ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('username')
            ->add('email')
            ->add('display_name')
            ->addIdentifier('clients')
            ->add('_action', 'actions', ['actions' => ['edit' => [], 'delete' => []]]);
    }
    /**
     * @param object $request
     */
    public function prePersist($request)
    {
        $this->preUpdate($request);
    }

    public function preUpdate($agentSupport)
    {
        $agentSupport->setClients($agentSupport->getClients());

        if ($this->isCurrentRoute('create')) {
            /** @var AgentSupport $agentSupport */
            $agentSupport->setPassword($this->encoder->encodePassword($agentSupport, $agentSupport->getPassword()));
        }
    }
}
