<?php

namespace App\Admin;


use App\Entity\Acteur;
use App\Entity\Progiciel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


final class CategorieAdmin extends AbstractAdmin
{
    protected $formOptions = ['validation_groups' => false];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('progiciels',  EntityType::class, [
                'class' => Progiciel::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'by_reference' => false,
                'constraints' => false
            ])
            ->add('nom', TextType::class, ['label' => 'Catégorie'])
            ->add('locked', CheckboxType::class, ['required' => false, 'label' => 'Actif'])
            ->add('acteurs', EntityType::class, [
                'class' => Acteur::class,
                'choice_label' => 'libelle',
                'multiple' => true,
                'by_reference' => false,
                'constraints' => false
            ])
            ->add('code', TextType::class)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('progiciels')
            ->add('nom', null, ['label' => 'Catégorie de demande'])
            ->add('acteurs')
            ->add('code')
            ->add('locked', null, ['label' => 'Actif']);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('progiciels')
            ->add('nom', null, ['label' => 'Catégorie de demande'])
            ->add('acteurs')
            ->add('locked', null, ['label' => 'Actif'])
            ->add('code', TextType::class)
            ->add('_action', 'actions', ['actions' => ['view' => [], 'edit' => [], 'delete' => []]]);
    }

    public function prePersist($categorie)
    {
        $this->preUpdate($categorie);
    }

    public function preUpdate($categorie)
    {
        $categorie->setProgiciels($categorie->getProgiciels());
        $categorie->setActeurs($categorie->getActeurs());
        $categorie->setCode($categorie->getCode());
    }
}
