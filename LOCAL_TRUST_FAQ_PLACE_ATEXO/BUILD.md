# Build

# Dépendances

Les logiciels suivant doivent être installés et disponible sur le poste réalisant l'opération de build : 

- Maven
- Composer
- php8.1 php8.1-bcmath php8.1-curl php8.1-gd php8.1-intl php8.1-ldap php8.1-mbstring php8.1-mysql php8.1-soap php8.1-xml php8.1-xsl php8.1-zip php8.1-apcu php8.1-msgpack php-pear
- node
- npm
- yarn
- nvm

# Compilation

La commande doit être lancée à la racine du dossier du module : 

- `mvn clean install -DskipTests --no-transfer-progress -U -Dmaven.wagon.http.ssl.insecure=true`