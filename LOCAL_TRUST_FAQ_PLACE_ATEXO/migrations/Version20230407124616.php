<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230407124616 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE categorie ADD COLUMN IF NOT EXISTS code VARCHAR(8) NOT NULL');
        $this->addSql('SET @num := 0;
        UPDATE categorie c1
        INNER JOIN (
            SELECT nom, COUNT(*) as count
            FROM categorie
            GROUP BY nom
            HAVING COUNT(*) > 1
        ) c2 ON c1.nom = c2.nom
        SET c1.code = CONCAT("KO", @num := @num + 1)
        WHERE c1.code = ""');
        $this->addSql('UPDATE categorie SET code = SUBSTRING(MD5(nom), 1, 8) WHERE code = ""');
        $this->addSql('CREATE UNIQUE INDEX IF NOT EXISTS code_unique ON categorie (code)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE categorie DROP COLUMN IF EXISTS code');
    }
}
