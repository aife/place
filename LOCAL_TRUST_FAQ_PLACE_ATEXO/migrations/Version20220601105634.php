<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220601105634 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'UTAH-351 - Amélioration des statistiques récupérées par le module UTAH - Création de la page et export XLS';
    }

    public function up(Schema $schema): void
    {
        $statement = 'insert into role (id, nom) values (?, ?) ON DUPLICATE KEY UPDATE nom = ?';

        $roles = [
            'ROLE_AGENT_SUPPORT',
            'ROLE_ADMIN',
            'ROLE_SUPER_ADMIN',
        ];

        foreach ($roles as $role) {
            $this->addSql($statement, [constant("App\Entity\Role::$role"), $role, $role]);
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('delete from role;');
    }
}
