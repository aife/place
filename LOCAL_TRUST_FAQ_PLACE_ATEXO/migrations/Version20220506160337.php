<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220506160337 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'UTAH-351 - Amélioration des statistiques récupérées par le module UTAH - Création de la page et export XLS';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS produit_agent_support (produit_id INT NOT NULL, agent_support_id INT NOT NULL, INDEX IDX_7C092FD1F347EFB (produit_id), INDEX IDX_7C092FD17253650A (agent_support_id), PRIMARY KEY(produit_id, agent_support_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE produit_agent_support ADD CONSTRAINT FK_7C092FD1F347EFB FOREIGN KEY IF NOT EXISTS (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_agent_support ADD CONSTRAINT FK_7C092FD17253650A FOREIGN KEY IF NOT EXISTS (agent_support_id) REFERENCES agent_support (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE produit_agent_support');
    }
}
