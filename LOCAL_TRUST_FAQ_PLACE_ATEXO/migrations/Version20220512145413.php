<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512145413 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'UTAH-351 - Amélioration des statistiques récupérées par le module UTAH - Création de la page et export XLS';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS role (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS role_agent_support (role_id INT NOT NULL, agent_support_id INT NOT NULL, INDEX IDX_DDA6419CD60322AC (role_id), INDEX IDX_DDA6419C7253650A (agent_support_id), PRIMARY KEY(role_id, agent_support_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE role_agent_support ADD CONSTRAINT FK_DDA6419CD60322AC FOREIGN KEY IF NOT EXISTS (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_agent_support ADD CONSTRAINT FK_DDA6419C7253650A FOREIGN KEY IF NOT EXISTS (agent_support_id) REFERENCES agent_support (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_agent_support DROP FOREIGN KEY FK_DDA6419CD60322AC');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_agent_support');
    }
}
