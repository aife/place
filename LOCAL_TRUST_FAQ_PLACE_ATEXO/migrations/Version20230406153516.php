<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
final class Version20230406153516 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS complement (id INT AUTO_INCREMENT NOT NULL, complement TEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS article_faq_complement (article_faq_id INT NOT NULL, complement_id INT NOT NULL, INDEX IDX_9F7DA030EB795F42 (article_faq_id), INDEX IDX_9F7DA03040D9D0AA (complement_id), PRIMARY KEY(article_faq_id, complement_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article_faq_complement ADD CONSTRAINT FK_9F7DA030EB795F42 FOREIGN KEY IF NOT EXISTS (article_faq_id) REFERENCES article_faq (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_faq_complement ADD CONSTRAINT FK_9F7DA03040D9D0AA FOREIGN KEY IF NOT EXISTS (complement_id) REFERENCES complement (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE IF NOT EXISTS client_complement (client_id INT NOT NULL, complement_id INT NOT NULL, INDEX IDX_68F0237519EB6921 (client_id), INDEX IDX_68F0237540D9D0AA (complement_id), PRIMARY KEY(client_id, complement_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_complement ADD CONSTRAINT FK_68F0237519EB6921 FOREIGN KEY IF NOT EXISTS (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_complement ADD CONSTRAINT FK_68F0237540D9D0AA FOREIGN KEY IF NOT EXISTS (complement_id) REFERENCES complement (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE article_faq_complement DROP FOREIGN KEY IF EXISTS FK_9F7DA03040D9D0AA');
        $this->addSql('DROP TABLE IF EXISTS article_faq_complement');
        $this->addSql('DROP TABLE IF EXISTS client_complement');
        $this->addSql('DROP TABLE IF EXISTS complement');
    }
}
