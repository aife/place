<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230407114247 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE article_faq ADD IF NOT EXISTS client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE article_faq ADD CONSTRAINT FK_7AE056B619EB6921 FOREIGN KEY IF NOT EXISTS (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IF NOT EXISTS IDX_7AE056B619EB6921 ON article_faq (client_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE article_faq DROP FOREIGN KEY IF EXISTS FK_7AE056B619EB6921');
        $this->addSql('DROP INDEX IF EXISTS IDX_7AE056B619EB6921 ON article_faq');
        $this->addSql('ALTER TABLE article_faq DROP COLUMN IF EXISTS clients_id');
    }
}
