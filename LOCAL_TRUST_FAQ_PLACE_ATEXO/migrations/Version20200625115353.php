<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200625115353 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout Comptes ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE `agent_support` (
                          `id` int(11) NOT NULL,
                          `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                          `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
                          `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                          `display_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
                          `expired` tinyint(1) NOT NULL,
                          `locked` tinyint(1) NOT NULL,
                          `credentials_expired` tinyint(1) NOT NULL,
                          `disabled` tinyint(1) NOT NULL,
                          `created_at` datetime NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
        $this->addSql('  INSERT INTO `agent_support` (`id`, `username`, `password`, `email`, `display_name`, `expired`, `locked`, `credentials_expired`, `disabled`, `created_at`) VALUES
    (1, \'admin\', \'$2y$10$xoxnAI/ZW5m7dIiAr7N4e.WyOsiYNwjIMaB8MJHiqz2tTTXnR6gDu\', \'admin@example.com\', \'Louis Adiret\', 0, 0, 0, 0, \'2016-08-31 14:18:00\');');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
