<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200625115355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout Comptes ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `agent_support` 
                          MODIFY `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                        CONVERT TO CHARACTER SET utf8mb4, COLLATE utf8mb4_unicode_ci;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
