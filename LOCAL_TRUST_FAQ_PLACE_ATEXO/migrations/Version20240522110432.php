<?php

    declare(strict_types=1);

    namespace DoctrineMigrations;

    use Doctrine\DBAL\Schema\Schema;
    use Doctrine\Migrations\AbstractMigration;

    final class Version20240522110432 extends AbstractMigration
    {
        public function up(Schema $schema): void
        {
            $this->warnIf(true !== (bool) $_ENV['PLACE_ENVIRONMENT'], 'Migration spécifique Place !');
            if (true !== (bool) $_ENV['PLACE_ENVIRONMENT']) {
                return;
            }

            $this->addSql('DROP TABLE IF EXISTS article_faq_complement');
            $this->addSql('ALTER TABLE complement ADD COLUMN IF NOT EXISTS article_id INT DEFAULT NULL');
            $this->addSql('CREATE INDEX IF NOT EXISTS IDX_F8A41E347294869C ON complement (article_id)');
        }

        public function down(Schema $schema): void
        {
            $this->addSql('CREATE TABLE IF NOT EXISTS article_faq_complement (article_faq_id INT NOT NULL, complement_id INT NOT NULL, INDEX IDX_9F7DA030EB795F42 (article_faq_id), INDEX IDX_9F7DA03040D9D0AA (complement_id), PRIMARY KEY(article_faq_id, complement_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
            $this->addSql('ALTER TABLE article_faq_complement ADD CONSTRAINT FK_9F7DA03040D9D0AA FOREIGN KEY (complement_id) REFERENCES complement (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE article_faq_complement ADD CONSTRAINT FK_9F7DA030EB795F42 FOREIGN KEY (article_faq_id) REFERENCES article_faq (id) ON DELETE CASCADE');
            $this->addSql('ALTER TABLE complement DROP FOREIGN KEY IF EXISTS FK_F8A41E347294869C');
            $this->addSql('DROP INDEX IF EXISTS IDX_F8A41E347294869C ON complement');
            $this->addSql('ALTER TABLE complement DROP COLUMN IF EXISTS article_id');
        }
    }
