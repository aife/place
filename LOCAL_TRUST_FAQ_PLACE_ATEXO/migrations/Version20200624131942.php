<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200624131942 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE acteur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, libelle VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_EAFAD3626C6E55B5 (nom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acteur_categorie (acteur_id INT NOT NULL, categorie_id INT NOT NULL, INDEX IDX_35022F43DA6F574A (acteur_id), INDEX IDX_35022F43BCF5E72D (categorie_id), PRIMARY KEY(acteur_id, categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_faq (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, is_draft TINYINT(1) DEFAULT \'0\' NOT NULL, locked TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_faq_categorie (article_faq_id INT NOT NULL, categorie_id INT NOT NULL, INDEX IDX_4D891EAFEB795F42 (article_faq_id), INDEX IDX_4D891EAFBCF5E72D (categorie_id), PRIMARY KEY(article_faq_id, categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, parent INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, locked TINYINT(1) NOT NULL, INDEX IDX_497DD6343D8E604F (parent), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE produit (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, host VARCHAR(255) NOT NULL, icon_src VARCHAR(255) NOT NULL, logo_src VARCHAR(255) NOT NULL, bandeau_src VARCHAR(255) NOT NULL, email_notif VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_29A5EC276C6E55B5 (nom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE progiciel (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, libelle VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_36E94D976C6E55B5 (nom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE progiciel_categorie (progiciel_id INT NOT NULL, categorie_id INT NOT NULL, INDEX IDX_5F67AECB21C00E7 (progiciel_id), INDEX IDX_5F67AECBBCF5E72D (categorie_id), PRIMARY KEY(progiciel_id, categorie_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE temp_contextes (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', data LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', ip VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, produit_id INT DEFAULT NULL, acteur_id INT DEFAULT NULL, id_externe INT DEFAULT NULL, nom_complet VARCHAR(64) NOT NULL, email VARCHAR(255) NOT NULL, email_suivi VARCHAR(255) NOT NULL, type_entite VARCHAR(64) NOT NULL, entite LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1D1C63B3F347EFB (produit_id), INDEX IDX_1D1C63B3DA6F574A (acteur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE acteur_categorie ADD CONSTRAINT FK_35022F43DA6F574A FOREIGN KEY (acteur_id) REFERENCES acteur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acteur_categorie ADD CONSTRAINT FK_35022F43BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_faq_categorie ADD CONSTRAINT FK_4D891EAFEB795F42 FOREIGN KEY (article_faq_id) REFERENCES article_faq (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_faq_categorie ADD CONSTRAINT FK_4D891EAFBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE categorie ADD CONSTRAINT FK_497DD6343D8E604F FOREIGN KEY (parent) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE progiciel_categorie ADD CONSTRAINT FK_5F67AECB21C00E7 FOREIGN KEY (progiciel_id) REFERENCES progiciel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE progiciel_categorie ADD CONSTRAINT FK_5F67AECBBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur ADD CONSTRAINT FK_1D1C63B3F347EFB FOREIGN KEY (produit_id) REFERENCES produit (id)');
        $this->addSql('ALTER TABLE utilisateur ADD CONSTRAINT FK_1D1C63B3DA6F574A FOREIGN KEY (acteur_id) REFERENCES acteur (id)');
        $this->addSql('CREATE TABLE produit_progiciel (produit_id INT NOT NULL, progiciel_id INT NOT NULL, INDEX IDX_B27E137BF347EFB (produit_id), INDEX IDX_B27E137B21C00E7 (progiciel_id), PRIMARY KEY(produit_id, progiciel_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE produit_progiciel ADD CONSTRAINT FK_B27E137BF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_progiciel ADD CONSTRAINT FK_B27E137B21C00E7 FOREIGN KEY (progiciel_id) REFERENCES progiciel (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE acteur_categorie DROP FOREIGN KEY FK_35022F43DA6F574A');
        $this->addSql('ALTER TABLE utilisateur DROP FOREIGN KEY FK_1D1C63B3DA6F574A');
        $this->addSql('ALTER TABLE article_faq_categorie DROP FOREIGN KEY FK_4D891EAFEB795F42');
        $this->addSql('ALTER TABLE acteur_categorie DROP FOREIGN KEY FK_35022F43BCF5E72D');
        $this->addSql('ALTER TABLE article_faq_categorie DROP FOREIGN KEY FK_4D891EAFBCF5E72D');
        $this->addSql('ALTER TABLE categorie DROP FOREIGN KEY FK_497DD6343D8E604F');
        $this->addSql('ALTER TABLE progiciel_categorie DROP FOREIGN KEY FK_5F67AECBBCF5E72D');
        $this->addSql('ALTER TABLE utilisateur DROP FOREIGN KEY FK_1D1C63B3F347EFB');
        $this->addSql('ALTER TABLE progiciel_categorie DROP FOREIGN KEY FK_5F67AECB21C00E7');
        $this->addSql('DROP TABLE acteur');
        $this->addSql('DROP TABLE acteur_categorie');
        $this->addSql('DROP TABLE article_faq');
        $this->addSql('DROP TABLE article_faq_categorie');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE produit');
        $this->addSql('DROP TABLE progiciel');
        $this->addSql('DROP TABLE progiciel_categorie');
        $this->addSql('DROP TABLE temp_contextes');
        $this->addSql('DROP TABLE utilisateur');
        $this->addSql('DROP TABLE produit_progiciel');
    }
}
