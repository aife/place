<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220217083745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'UTAH-355 - Séparation Technique de 2 projets FAQ et UTAH - ajout produit MPE';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `produit` SET nom = 'MPE' WHERE nom = 'PLACE-RECETTE';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `produit` SET nom = 'PLACE-RECETTE' WHERE nom = 'MPE';");
    }
}
