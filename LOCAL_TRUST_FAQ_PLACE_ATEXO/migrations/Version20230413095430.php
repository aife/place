<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230413095430 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE VIEW IF NOT EXISTS v_client_progiciel AS
        SELECT
            c.id as client_id,
            c.nom as client_nom,
            progiciel.id as progiciel_id,
            progiciel.nom as progiciel_nom
        FROM
            client_progiciel
        INNER JOIN
          client c on c.id = client_progiciel.client_id
        INNER JOIN
          progiciel  on progiciel.id = client_progiciel.progiciel_id'
        );
        $this->addSql('CREATE VIEW IF NOT EXISTS v_article_faq_diffusion AS
        SELECT
            article_faq.id AS articleFAQ_id,
            article_faq.title AS articleFAQ_question,
            article_faq.content AS articleFAQ_reponse,
            client.nom AS articleFAQ_client,
            complement.complement AS articleFAQ_complement,
            c.nom AS articleFAQ_categorie,
            acteur.libelle AS categorie_acteur,
            p.libelle as progiciel_categorie
         FROM
            article_faq
         INNER JOIN
          client ON article_faq.client_id = client.id
         LEFT JOIN
          article_faq_complement afc ON article_faq.id = afc.article_faq_id
         LEFT JOIN
          complement ON afc.complement_id = complement.id
         LEFT JOIN
          article_faq_categorie a on article_faq.id = a.article_faq_id
         LEFT JOIN
          categorie c on a.categorie_id = c.id
         LEFT JOIN
          acteur_categorie on c.id = acteur_categorie.categorie_id
         LEFT JOIN
          acteur on acteur_categorie.acteur_id = acteur.id
         LEFT JOIN
          progiciel_categorie pc on c.id = pc.categorie_id
         LEFT JOIN
          progiciel p on pc.progiciel_id = p.id'
        );
        $this->addSql('CREATE VIEW IF NOT EXISTS v_article_faq_complement AS
        SELECT
            article_faq.id AS articleFAQ_id,
            article_faq.title AS articleFAQ_question,
            article_faq.content AS articleFAQ_reponse,
            client.nom AS articleFAQ_client,
            complement.complement AS articleFAQ_complement,
            c.nom AS articleFAQ_categorie,
            acteur.libelle AS categorie_acteur,
            p.libelle as categorie_progiciel
        FROM
            article_faq
        INNER JOIN
         article_faq_complement afc ON article_faq.id = afc.article_faq_id
        INNER JOIN
         complement ON afc.complement_id = complement.id
        LEFT JOIN
         article_faq_categorie a on article_faq.id = a.article_faq_id
        LEFT JOIN
         categorie c on a.categorie_id = c.id
        LEFT JOIN
         acteur_categorie on c.id = acteur_categorie.categorie_id
        LEFT JOIN
         acteur on acteur_categorie.acteur_id = acteur.id
        LEFT JOIN
         client ON article_faq.client_id = client.id
        LEFT JOIN
         progiciel_categorie pc on c.id = pc.categorie_id
        LEFT JOIN
         progiciel p on pc.progiciel_id = p.id'
        );
    }


    public function down(Schema $schema): void
    {
        $this->addSql('DROP VIEW IF EXISTS v_client_progiciel');
        $this->addSql('DROP VIEW IF EXISTS v_article_faq_diffusion');
        $this->addSql('DROP VIEW IF EXISTS v_article_faq_complement');
    }
}
