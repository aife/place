<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221013134814 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Renomme de toutes les tables client.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE client LIKE produit');
        $this->addSql('CREATE TABLE client_agent_support LIKE produit_agent_support');
        $this->addSql('CREATE TABLE client_progiciel LIKE produit_progiciel');

        $this->addSql('INSERT INTO client SELECT * FROM produit');
        $this->addSql('INSERT INTO client_agent_support SELECT * FROM produit_agent_support');
        $this->addSql('INSERT INTO client_progiciel SELECT * FROM produit_progiciel');

        $this->addSql('ALTER TABLE client_agent_support CHANGE produit_id client_id INT NOT NULL');
        $this->addSql('ALTER TABLE client_progiciel CHANGE produit_id client_id INT NOT NULL');

        $this->addSql('ALTER TABLE client_agent_support ADD CONSTRAINT client_agent_support_ibfk_1 FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_agent_support ADD CONSTRAINT client_agent_support_ibfk_2 FOREIGN KEY (agent_support_id) REFERENCES agent_support (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_progiciel ADD CONSTRAINT client_progiciel_ibfk_1 FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_progiciel ADD CONSTRAINT client_progiciel_ibfk_2 FOREIGN KEY (progiciel_id) REFERENCES progiciel (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE produit_agent_support DROP FOREIGN KEY FK_7C092FD1F347EFB');
        $this->addSql('ALTER TABLE produit_agent_support DROP FOREIGN KEY FK_7C092FD17253650A');
        $this->addSql('ALTER TABLE produit_progiciel DROP FOREIGN KEY FK_B27E137BF347EFB');
        $this->addSql('ALTER TABLE produit_progiciel DROP FOREIGN KEY FK_B27E137B21C00E7');

        $this->addSql('DROP TABLE produit');
        $this->addSql('DROP TABLE produit_agent_support');
        $this->addSql('DROP TABLE produit_progiciel');

    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE produit LIKE client');
        $this->addSql('CREATE TABLE produit_agent_support LIKE client_agent_support');
        $this->addSql('CREATE TABLE produit_progiciel LIKE client_progiciel');

        $this->addSql('INSERT INTO produit SELECT * FROM client');
        $this->addSql('INSERT INTO produit_agent_support SELECT * FROM client_agent_support');
        $this->addSql('INSERT INTO produit_progiciel SELECT * FROM client_progiciel');

        $this->addSql('ALTER TABLE produit_agent_support CHANGE client_id produit_id INT NOT NULL');
        $this->addSql('ALTER TABLE produit_progiciel CHANGE client_id produit_id INT NOT NULL');

        $this->addSql('ALTER TABLE produit_agent_support ADD CONSTRAINT FK_7C092FD1F347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_agent_support ADD CONSTRAINT FK_7C092FD17253650A FOREIGN KEY (agent_support_id) REFERENCES agent_support (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_progiciel ADD CONSTRAINT FK_B27E137BF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produit_progiciel ADD CONSTRAINT FK_B27E137B21C00E7 FOREIGN KEY (progiciel_id) REFERENCES progiciel (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE client_agent_support DROP FOREIGN KEY client_agent_support_ibfk_1');
        $this->addSql('ALTER TABLE client_agent_support DROP FOREIGN KEY client_agent_support_ibfk_2');
        $this->addSql('ALTER TABLE client_progiciel DROP FOREIGN KEY client_progiciel_ibfk_1');
        $this->addSql('ALTER TABLE client_progiciel DROP FOREIGN KEY client_progiciel_ibfk_2');

        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE client_agent_support');
        $this->addSql('DROP TABLE client_progiciel');

    }
}