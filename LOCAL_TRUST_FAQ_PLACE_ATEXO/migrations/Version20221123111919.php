<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221123111919 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
    $this->addSql('SET @categorie_id := 0');
    $this->addSql('UPDATE `categorie` SET `nom`="Date limite de remise des offres (DLRO)", 
                       id = (SELECT @categorie_id := id) WHERE `nom`="DLRO dépassée";');
    $this->addSql('INSERT INTO `article_faq` (`title`, `content`, `created_at`, `is_draft`, `locked`) 
    VALUES ("La DLRO est dans moins de 3 heures",
    "Il est conseillé d\'ouvrir un ticket d’assistance afin que vos contextes technique et métiers soient récupérés de 
    façon automatique.\r\n Cela facilitera l\’analyse par notre équipe d\’assistance.\r\n Vous pouvez aussi contacter 
    le support par téléphone au {{num_support}}.\r\n Attention : Si votre DLRO n’est pas dans moins de 3h, le conseiller
    vous demandera d\’ouvrir un ticket d’assistance puis de rappeler l\’assistance téléphonique.", NOW(), 0, 1)');
    $this->addSql('SET @last_id_article_1 = LAST_INSERT_ID()');
    $this->addSql('INSERT INTO article_faq_categorie (article_faq_id, categorie_id) VALUES (@last_id_article_1, @categorie_id)');
    $this->addSql('ALTER TABLE client ADD num_support VARCHAR(32) DEFAULT NULL');
    $this->addSql('UPDATE `client` SET `num_support`="09 72 37 01 30" WHERE `nom`="PLACE"');
    }

    public function down(Schema $schema): void
    {
    $this->addSql('UPDATE `categorie` SET `nom`="DLRO dépassée" WHERE `nom`="Date limite de remise des offres (DLRO)"');
    $this->addSql('DELETE FROM article_faq WHERE title = "La DLRO est dans moins de 3 heures"');
    $this->addSql('ALTER TABLE client DROP COLUMN num_support');
    }
}
