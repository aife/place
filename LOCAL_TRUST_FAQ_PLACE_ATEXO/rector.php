<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src',
//        __DIR__ . '/templates',mms
//        __DIR__ . '/tests',
//        __DIR__ . '/translations',
//        __DIR__ . '/public',
//        __DIR__ . '/migrations',
//        __DIR__ . '/config',
    ]);

    // register a single rule
//    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_81
    ]);
};
