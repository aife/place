<?php

namespace App\Tests\Service;

use App\Service\WebService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServiceTest extends TestCase
{
    /**
     * @throws TransportExceptionInterface
     */
    public function testGetAuthenticationConfirmationSuccess(): void
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $baseUrl = 'https://identite-release.local-trust.com';
        $urlIdentity = '/auth/realms/utah/protocol/openid-connect/userinfo';
        $url = $baseUrl . $urlIdentity;

        $accessToken = 'FDSERT43HYTD5432SDPMLHRTDSFF';

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('identity_base_url')
            ->willReturn($baseUrl)
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '{"name":"name", "email":"email"}',
                ['http_code' => Response::HTTP_OK]
            )
        ]);

        $response = $client->request('GET', $url);

        $httpClient
            ->expects(self::once())
            ->method('request')
            ->with(
                Request::METHOD_GET,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer ' . $accessToken,
                    ],
                ]
            )
            ->willReturn($response)
        ;

        $webService = new WebService($httpClient, $parameterBag);

        self::assertTrue($webService->getAuthenticationConfirmation($accessToken));
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function testGetAuthenticationConfirmationFailure(): void
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $baseUrl = 'https://identite-release.local-trust.com';
        $urlIdentity = '/auth/realms/utah/protocol/openid-connect/userinfo';
        $url = $baseUrl . $urlIdentity;

        $accessToken = 'FDSERT43HYTD5432SDPMLHRTDSFF';

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('identity_base_url')
            ->willReturn($baseUrl)
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '{"name":"name", "email":"email"}',
                ['http_code' => Response::HTTP_UNAUTHORIZED]
            )
        ]);

        $response = $client->request('GET', $url);

        $httpClient
            ->expects(self::once())
            ->method('request')
            ->with(
                Request::METHOD_GET,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer ' . $accessToken,
                    ],
                ]
            )
            ->willReturn($response)
        ;

        $webService = new WebService($httpClient, $parameterBag);

        self::assertFalse($webService->getAuthenticationConfirmation($accessToken));
    }
}
