<?php

namespace App\Tests\Service;

use App\Entity\Contexte;
use App\Service\EncoderService;
use PHPUnit\Framework\TestCase;

class EncoderServiceTest extends TestCase
{
    public function testGetEncodedContext(): void
    {
        $array = ['applicatif' => ['MPE'], 'metier' => 'ticketing'];

        $encoderService = new EncoderService();

        $expectedResult = "eyJhcHBsaWNhdGlmIjpbIk1QRSJdLCJtZXRpZXIiOiJ0aWNrZXRpbmcifQ==";

        self::assertSame($encoderService->getEncodedContext($array), $expectedResult);
    }

    public function testGetDecodedContext(): void
    {
        $encodedString = "eyJhcHBsaWNhdGlmIjpbIk1QRSJdLCJtZXRpZXIiOiJ0aWNrZXRpbmcifQ==";

        $encoderService = new EncoderService();

        $expectedResult = ['applicatif' => ['MPE'], 'metier' => 'ticketing'];

        self::assertSame($encoderService->getDecodedContext($encodedString), $expectedResult);
    }

    public function testGetFormattedDataContextes(): void
    {
        $contexte = [
            "applicatif" => [
                "nom" => "applicatif",
                "label" => "applicatif",
                "description" => "Applicatif",
                "max_visible" => 1000,
                "champs" => [
                    "application" => [
                        "label" => "Plate-forme",
                        "description" => null,
                        "valeur" => "MPE",
                        "type_valeur" => "string",
                        "visible_utilisateur" => true,
                        "visible_support" => true,
                        "extras" => null,
                    ],
                    "utilisateur_email" => [
                        "label" => "Courriel de l'utilisateur",
                        "description" => null,
                        "valeur" => 'mail@mail.fr',
                        "type_valeur" => "mailto",
                        "visible_utilisateur" => true,
                        "visible_support" => true,
                        "extras" => null,
                    ],
                ]
            ],
            "technique" => [
                "nom" => "technique",
                "label" => "technique",
                "description" => "Technique",
                "max_visible" => 3,
                "champs" => [
                    "label" => "Système d'exploitation",
                    "description" => null,
                    "valeur" => "Linux",
                    "type_valeur" => "string",
                    "visible_utilisateur" => true,
                    "visible_support" => true,
                    "extras" => "Mozilla/5.0",
                ],
            ]
        ];

        $encoderService = new EncoderService();

        $contexteApplicatif = (new Contexte('applicatif', 'applicatif', 'Applicatif', 1000))
            ->setChamps([
                "application" => [
                    "label" => "Plate-forme",
                    "description" => null,
                    "valeur" => "MPE",
                    "type_valeur" => "string",
                    "visible_utilisateur" => true,
                    "visible_support" => true,
                    "extras" => null,
                ],
                "utilisateur_email" => [
                    "label" => "Courriel de l'utilisateur",
                    "description" => null,
                    "valeur" => "mail@mail.fr",
                    "type_valeur" => "mailto",
                    "visible_utilisateur" => true,
                    "visible_support" => true,
                    "extras" => null,
                ]
            ])
        ;

        $contexteTechnique = (new Contexte('technique', 'technique', 'Technique', 3))
            ->setChamps([
                "label" => "Système d'exploitation",
                "description" => null,
                "valeur" => "Linux",
                "type_valeur" => "string",
                "visible_utilisateur" => true,
                "visible_support" => true,
                "extras" => "Mozilla/5.0",
            ])
        ;

        $expectedResult = [
            'applicatif' => $contexteApplicatif,
            'technique' => $contexteTechnique,
        ];

        self::assertEquals($encoderService->getFormattedDataContextes($contexte), $expectedResult);
    }

    public function testIsBase64EncodedTrue(): void
    {
        $encodedString = "eyJhcHBsaWNhdGlmIjpbIk1QRSJdLCJtZXRpZXIiOiJ0aWNrZXRpbmcifQ==";

        self::assertTrue((new EncoderService())->isBase64Encoded($encodedString));
    }

    public function testIsBase64EncodedFalse(): void
    {
        $encodedString = "yJhcHGlmIjpbIk1QRSJdLCJtZXRpZ";

        self::assertFalse((new EncoderService())->isBase64Encoded($encodedString));
    }
}
