<?php

namespace App\Tests\Security;

use App\Security\KeycloakAuthChecker;
use App\Service\EncoderService;
use App\Service\WebService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class KeycloakAuthCheckerTest extends TestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValidityContextNull(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Vérification de la validité du contexte : ")
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Erreur : Le contexte est manquant ou mal formé!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);

        self::assertSame(
            $keycloakAuthChecker->checkApplicationAuthenticationValidity(null),
            "Erreur : Le contexte est manquant ou mal formé!"
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValidityErrorEncoding64(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $encodedContext = "yJhcHGlmIjpbIk1QRSJdLCJtZXRpZ";

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Vérification de la validité du contexte : yJhcHGlmIjpbIk1QRSJdLCJtZXRpZ")
        ;

        $encoderService
            ->expects(self::once())
            ->method('isBase64Encoded')
            ->with($encodedContext)
            ->willReturn(false)
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Erreur : Le contexte est manquant ou mal formé!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);

        self::assertSame(
            $keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext),
            "Erreur : Le contexte est manquant ou mal formé!"
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValidityContextDecodedSuccess(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $encodedContext = "yJhcHGlmIjpbIk1QRSJdLCJtZXRpZ";

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Vérification de la validité du contexte : yJhcHGlmIjpbIk1QRSJdLCJtZXRpZ")
        ;

        $encoderService
            ->expects(self::once())
            ->method('isBase64Encoded')
            ->with($encodedContext)
            ->willReturn(true)
        ;

        $encoderService
            ->expects(self::once())
            ->method('getDecodedContext')
            ->with($encodedContext)
            ->willReturn([])
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Erreur : Le contexte est manquant ou mal formé!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);

        self::assertSame(
            $keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext),
            "Erreur : Le contexte est manquant ou mal formé!"
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValidityAccessTokenNotFound(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $contextArray = [
            'applicatif' => [
                'utilisateur_email' => []
            ]
        ];
        $encodedContext = base64_encode(json_encode($contextArray));

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Vérification de la validité du contexte : " . $encodedContext)
        ;

        $encoderService
            ->expects(self::once())
            ->method('isBase64Encoded')
            ->with($encodedContext)
            ->willReturn(true)
        ;

        $encoderService
            ->expects(self::once())
            ->method('getDecodedContext')
            ->with($encodedContext)
            ->willReturn($contextArray)
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Impossibilité de valider l'authentification avec keycloak car l'access_token est inexistant!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);
        $errorMessage = "Impossible de récupérer le contexte de votre requête. \n
         Veuillez recommencer votre demande d'assistance en ligne depuis le début ou contactez le support informatique.";

        self::assertSame(
            $keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext),
            $errorMessage
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValidityKeycloakAuthFailure(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $accessToken = 'access_token';

        $contextArray = [
            "applicatif" => [
                "nom" => "applicatif",
                "label" => "applicatif",
                "description" => "Applicatif",
                "max_visible" => 1000,
                "champs" => [
                    "access_token" => [
                        "label" => "access_token",
                        "description" => 'access_token',
                        "valeur" => $accessToken,
                        "type_valeur" => "string",
                    ],
                ]
            ],
        ];
        $encodedContext = base64_encode(json_encode($contextArray));

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Vérification de la validité du contexte : " . $encodedContext)
        ;

        $encoderService
            ->expects(self::once())
            ->method('isBase64Encoded')
            ->with($encodedContext)
            ->willReturn(true)
        ;

        $encoderService
            ->expects(self::once())
            ->method('getDecodedContext')
            ->with($encodedContext)
            ->willReturn($contextArray)
        ;

        $webService
            ->expects(self::once())
            ->method('getAuthenticationConfirmation')
            ->with($accessToken)
            ->willReturn(false)
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Impossibilité de valider l'authentification avec keycloak car l'access_token est invalide!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);
        $errorMessage = "Impossible de récupérer le contexte de votre requête. \n
         Veuillez recommencer votre demande d'assistance en ligne depuis le début ou contactez le support informatique.";

        self::assertSame(
            $keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext),
            $errorMessage
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     */
    public function testCheckApplicationAuthenticationValiditySuccess(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $encoderService = $this->createMock(EncoderService::class);
        $webService = $this->createMock(WebService::class);

        $accessToken = 'access_token';

        $contextArray = [
            "applicatif" => [
                "nom" => "applicatif",
                "label" => "applicatif",
                "description" => "Applicatif",
                "max_visible" => 1000,
                "champs" => [
                    "access_token" => [
                        "label" => "access_token",
                        "description" => 'access_token',
                        "valeur" => $accessToken,
                        "type_valeur" => "string",
                    ],
                ]
            ],
        ];
        $encodedContext = base64_encode(json_encode($contextArray));

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Vérification de la validité du contexte : " . $encodedContext],
                ["Fin de vérification de la validité!"]
            )
        ;

        $encoderService
            ->expects(self::once())
            ->method('isBase64Encoded')
            ->with($encodedContext)
            ->willReturn(true)
        ;

        $encoderService
            ->expects(self::once())
            ->method('getDecodedContext')
            ->with($encodedContext)
            ->willReturn($contextArray)
        ;

        $webService
            ->expects(self::once())
            ->method('getAuthenticationConfirmation')
            ->with($accessToken)
            ->willReturn(true)
        ;

        $logger
            ->expects(self::never())
            ->method('error')
            ->with("Impossibilité de valider l'authentification avec keycloak car l'access_token est invalide!")
        ;

        $keycloakAuthChecker = new KeycloakAuthChecker($logger, $encoderService, $webService);

        self::assertNull($keycloakAuthChecker->checkApplicationAuthenticationValidity($encodedContext));
    }
}
