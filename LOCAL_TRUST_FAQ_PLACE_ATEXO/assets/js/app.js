require('../css/app.scss');

const $ = require('jquery');
global.$ = global.jQuery = $;

require('jquery');
require('jquery-ui');
require('bootstrap');
require('select2/dist/js/select2.min');
require('bootstrap-select');
require('chosen-js');

require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');


$(document).ready(function () {
    $('.js-select2').each(function () {
        let placeholder = $(this).data('placeholder');

        $(this).select2({
            theme: 'bootstrap-5',
            placeholder: placeholder,
        });
    });
    var demande = $('#dropdown_demande');
    var objet = $('#dropdown_objet');

    var wfaq = $('#widget_faq');
    var cfaq = $('#panel_cfaq');
    var courriel_suivi = $('#courriel_suivi');

    var form = $('#panel_demande, #fs_coordonnees');
    var specifier = $('#grp_specifier');

    var errors_tips = $('#errors_tips');

    var cfaq_non = $('#cfaq_non');
    var cfaq_oui = $('#cfaq_oui');

    // Etat initial
    objet.hide();
    wfaq.hide();
    cfaq.hide();
    form.hide();
    courriel_suivi.hide();

    $('#btn_mail').click(function () {
        if (courriel_suivi.is(':visible') == false) {
            $('#courriel_suivi').show();
        } else {
            $('#courriel_suivi').hide();
        }
    });

    function refreshFAQ(id_cat) {
        var result = true;
        //console.log('Suggestions FAQ, id_cat ' + id_cat);
        if (id_cat != "none" && id_cat != "other") {
            var path = Routing.generate('ajax_suggestions_widget', {'id_cat': id_cat});
            $.ajax({
                url: path, type: 'get', dataType: 'html', async: true, success: function (data) {
                    wfaq.html(data).show();
                    cfaq.show();
                    if (!data) {
                        cfaq.hide();
                        result = false;
                    }
                }
            });
        } else {
            console.error('refreshFAQ: l\'id de catégorie "' + id_cat + '" ne doit pas être none ou other.');
            wfaq.hide();
            cfaq.hide();
            retour = 0;
        }
        return result;
    }

    function resetCFAQ() {
        $('input[name="cfaq"]').prop('checked', false);
    }

    function refreshSubCat(id_cat) {
        var path = Routing.generate('ajax_categorie_dropdown', {'id': id_cat});
        $.get(path, function (data) {
            objet.html(data).show();
        });
    }

    // Changement cat. 1
    demande.change( function () {
        //hide validate message
        $('#alert_resolu').hide();
        var id_cat = demande.children('option:selected').val();
        // errors_tips.html('');
        if(id_cat == "none") {
            // Cacher objet, wfaq, cfac, form et btn envoyer
            objet.hide();
            objet.html('');
            wfaq.hide();
            cfaq.hide();
            form.hide();
        } else if(id_cat == "other") {
            // Cacher objet, wfaq, cfac
            objet.hide();
            objet.html('');
            wfaq.hide();
            cfaq.hide();
            // Afficher form, specifier, btn envoyer
            form.show();
            specifier.show();
        } else {
            // Cacher form
            form.hide();
            // Cacher specifier
            specifier.hide();
            // Afficher suggestion, cfaq
            refreshFAQ(id_cat);
            resetCFAQ();
            // Afficher les sous catégories
            // ToDO
            // This function is implemented in DB but the interface has not been developed yet
            //refreshSubCat(id_cat);
        }
    });

    // Changement cat. 2
    objet.on('change', '#objet', function () {
        var id_cat = $(this).children('option:selected').val();
        console.log("Changement de sous-catégorie : " + id_cat);
        // errors_tips.html('');
        if(id_cat == "none") {
            // Cacher form
            form.hide();
            // Cacher specifier
            specifier.hide();
            // Afficher suggestion, cfaq
            var parent_cat = demande.children('option:selected').val();
            refreshFAQ(parent_cat);
            resetCFAQ();
        } else if(id_cat == "other") {
            // Cacher wfaq, cfac
            wfaq.hide();
            cfaq.hide();
            // Afficher form, specifier, btn envoyer
            form.show();
            specifier.show();
        } else {
            // Cacher specifier
            specifier.hide();
            // Afficher suggestion, cfaq et btn envoyer
            if(refreshFAQ(id_cat)) { // Si suggestion trouvée
                resetCFAQ();
                wfaq.show();
                cfaq.show();
                form.hide();
            } else {
                form.show();
            }
        }
    });

    $('input[name="cfaq"]').click(function () {
        $('#alert_resolu').hide()
        if($(this).val() == 'oui') {
            form.hide();
            wfaq.show();
            // errors_tips.html('');
        } else {
            form.show();
            wfaq.hide();

            $('html').animate({ scrollTop :$('#panel_cfaq').offset().top + $('#panel_cfaq').height() - 25 }, 750);
        }
    });

    function getCategorie() {
        // Récupérer la catégorie
        var categorie_id;
        var demande_cat_id = demande.children('option:selected').val();
        categorie_id = demande_cat_id;
        return categorie_id;
    }

    window.setSessionSousCategorie =
    function setSessionSousCategorie(idCategorie) {
       var path = Routing.generate('set_session_sous_categorie', {'idCategorie': idCategorie});
        $.get(path, function (data) {
        });
    }

    cfaq_non.click(function () {
        var cfaq = false;
        var categorie_id = getCategorie();
        var urlUtah = $(this).data('urlUtah');
        const paramToken = $(this).data('token');

        $('#alert_resolu').hide();

        if (paramToken !== undefined && paramToken !== null && paramToken !== '') {
                // On gére la transmission rétrocompatible avec le token
                var url = Routing.generate('ajax_goto_utah_with_token') + '/' + categorie_id + '/' + paramToken;
                $.ajax(url, {
                    method: "POST",
                    data: {cfaq: cfaq},
                    success: function(data){
                        window.location = urlUtah + "?token=" + JSON.parse(data).tokenUtah;
                    },
                    error: function(data){
                        $('#error_msg').html (JSON.parse(data.responseText).message)
                        errors_tips.show()
                    }
                });
            } else {
                // On gére la transmission avec le contexte
                var url = Routing.generate('ajax_goto_utah') + '/' + categorie_id;
                $.ajax(url, {
                    method: "POST",
                    data: {cfaq: cfaq},
                    success: function(data){
                        window.location = urlUtah + "?context=" + JSON.parse(data).context;
                    },
                    error: function(data){
                        $('#error_msg').html (JSON.parse(data.responseText).message)
                        errors_tips.show()
                    }
                });
            }
    });

    cfaq_oui.click(function () {
        $('#alert_resolu').show();
    });

    window.addEventListener('DOMContentLoaded', function() {
        let inputNumSuivi = document.getElementById('input_num_suivi');
        let btnSuiviNav = document.getElementById('btn_suivi_nav');

        inputNumSuivi.addEventListener('input', function() {
            if (inputNumSuivi.value.trim() === '') {
                btnSuiviNav.disabled = true;
            } else {
                btnSuiviNav.disabled = false;
            }
        });
    });

    window.suivreTicketByNumSuivi =
    function suivreTicketByNumSuivi() {
        event.preventDefault();
        let currentUrl = window.location.href;
        currentUrl = currentUrl.slice(0, currentUrl.indexOf('faq/'));
        let noTicket = document.getElementsByName('input_no_suivi')[0].value;
        let ticketUrl = currentUrl + "assistance/demande/" + noTicket;
        window.open(ticketUrl, '_blank');
    }

});