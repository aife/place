function cleanString(str) {
    let sanitizedString = str.normalize("NFD").replace(/\p{Diacritic}/gu, "");
    sanitizedString = sanitizedString.replaceAll(/[^a-zA-Z0-9À-ž\s]/g, " ");
    sanitizedString = sanitizedString.replaceAll("  ", "-");
    sanitizedString = sanitizedString.replaceAll(" ", "-");
    sanitizedString = sanitizedString.replaceAll("--", "-");
    let lastChar = sanitizedString.substr(sanitizedString.length - 1);

    if (lastChar === "-") {
        sanitizedString = sanitizedString.slice(0, -1);
    }

    return sanitizedString.toLowerCase();
}

window.trackQuestionMatomo =
    function trackQuestionMatomo(question, rubrique) {
        let url = '/faq/rubrique/' + cleanString(rubrique) + '/' + cleanString(question);
        let title = 'FAQ - ' + rubrique.toUpperCase() + ' - ' + question;

        _paq.push(['setCustomUrl', url]);
        _paq.push(['setDocumentTitle', title]);
        _paq.push(['trackPageView']);
    }

window.trackBtnResolvedMatomo =
    function trackBtnResolvedMatomo() {
        _paq.push(['setCustomUrl', "/faq/action/la-faq-m-a-permis-de-resoudre-mon-probleme"]);
        _paq.push(['setDocumentTitle', "FAQ - ACTION - la-faq-m-a-permis-de-resoudre-mon-probleme"]);
        _paq.push(['trackPageView']);
    }

window.trackBtnNotResolvedMatomo =
    function trackBtnNotResolvedMatomo() {
        _paq.push(['setCustomUrl', "/faq/action/je-n-ai-pas-trouve-de-reponse-a-ma-recherche"]);
        _paq.push(['setDocumentTitle', "FAQ - ACTION - je-n-ai-pas-trouve-de-reponse-a-ma-recherche"]);
        _paq.push(['trackPageView']);
    }