package com.atexo.serveurCryptographique.utilitaire;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Classe d'utilitaire de gestion de l'internationalisation de l'applet.
 */
public abstract class I18nUtil {

    public static final String BUNDLE_PAR_DEFAUT = "messages";


    /**
     * Récupére la valeur associé à la clef en fonction de la locale par défaut.
     * Si la clef n'est pas trouvé, alors on renvoi la clef.
     *
     * @param key la clef
     * @return la valeur associé à la locale
     */
    public static String get(String key) {
        return get(BUNDLE_PAR_DEFAUT, key, Locale.getDefault());
    }

    /**
     * Récupére la valeur associé à la clef en fonction de la locale par défaut.
     * Si la clef n'est pas trouvé, alors on renvoi la clef.
     *
     * @param bundle le bundle contenant les messages
     * @param key    la clef
     * @return la valeur associé à la locale
     */
    public static String get(String bundle, String key) {
        return get(bundle, key, Locale.getDefault());
    }

    /**
     * Récupére la valeur associé à la clef en fonction de la locale selectionnée.
     * Si la clef n'est pas trouvé, alors on renvoi la clef.
     *
     * @param bundle le bundle contenant les messages
     * @param key    la clef
     * @param locale la locale
     * @return la valeur associé à la locale
     */
    public static String get(String bundle, String key, Locale locale) {
        String value = null;
        try {
            value = ResourceBundle.getBundle(bundle, locale).getString(key);
        } catch (MissingResourceException e) {
            value = "@@" + key + "@@";
        }
        return value;
    }

    /**
     * Converti un String en Locale.
     *
     * @param str le String à convertir
     * @return la locale correspondante
     * @throws IllegalArgumentException si le String est une valeur invalide.
     */
    public static Locale toLocale(String str) {
        if (str == null) {
            return null;
        }
        int len = str.length();
        if (len != 2 && len != 5 && len < 7) {
            throw new IllegalArgumentException("Invalid locale format: " + str);
        }
        char ch0 = str.charAt(0);
        char ch1 = str.charAt(1);
        if (ch0 < 'a' || ch0 > 'z' || ch1 < 'a' || ch1 > 'z') {
            throw new IllegalArgumentException("Invalid locale format: " + str);
        }
        if (len == 2) {
            return new Locale(str, "");
        } else {
            if (str.charAt(2) != '_') {
                throw new IllegalArgumentException("Invalid locale format: " + str);
            }
            char ch3 = str.charAt(3);
            if (ch3 == '_') {
                return new Locale(str.substring(0, 2), "", str.substring(4));
            }
            char ch4 = str.charAt(4);
            if (ch3 < 'A' || ch3 > 'Z' || ch4 < 'A' || ch4 > 'Z') {
                throw new IllegalArgumentException("Invalid locale format: " + str);
            }
            if (len == 5) {
                return new Locale(str.substring(0, 2), str.substring(3, 5));
            } else {
                if (str.charAt(5) != '_') {
                    throw new IllegalArgumentException("Invalid locale format: " + str);
                }
                return new Locale(str.substring(0, 2), str.substring(3, 5), str.substring(6));
            }
        }
    }
}
